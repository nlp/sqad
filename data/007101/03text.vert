<s>
Optická	optický	k2eAgFnSc1d1	optická
čočka	čočka	k1gFnSc1	čočka
je	být	k5eAaImIp3nS	být
optická	optický	k2eAgFnSc1d1	optická
soustava	soustava	k1gFnSc1	soustava
dvou	dva	k4xCgFnPc2	dva
centrovaných	centrovaný	k2eAgFnPc2d1	centrovaná
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kulových	kulový	k2eAgInPc6d1	kulový
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jedné	jeden	k4xCgFnSc2	jeden
kulové	kulový	k2eAgFnSc2d1	kulová
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
rovinné	rovinný	k2eAgFnSc2d1	rovinná
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Čočka	čočka	k1gFnSc1	čočka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
průhledného	průhledný	k2eAgInSc2d1	průhledný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
a	a	k8xC	a
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Čočky	čočka	k1gFnPc1	čočka
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
skleněné	skleněný	k2eAgFnPc1d1	skleněná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
plasty	plast	k1gInPc1	plast
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
čočky	čočka	k1gFnSc2	čočka
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
indexem	index	k1gInSc7	index
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
použitelnosti	použitelnost	k1gFnSc2	použitelnost
čočky	čočka	k1gFnSc2	čočka
blízký	blízký	k2eAgInSc1d1	blízký
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
popis	popis	k1gInSc1	popis
šíření	šíření	k1gNnSc2	šíření
paprsků	paprsek	k1gInPc2	paprsek
čočkou	čočka	k1gFnSc7	čočka
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
čočce	čočka	k1gFnSc6	čočka
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
Indie	Indie	k1gFnSc2	Indie
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
Ind	Ind	k1gMnSc1	Ind
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
čočku	čočka	k1gFnSc4	čočka
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgMnPc6	dva
kamínku	kamínek	k1gInSc2	kamínek
<g/>
.	.	kIx.	.
</s>
<s>
Paprsek	paprsek	k1gInSc1	paprsek
<g/>
,	,	kIx,	,
dopadající	dopadající	k2eAgInSc1d1	dopadající
na	na	k7c4	na
libovolné	libovolný	k2eAgNnSc4d1	libovolné
místo	místo	k1gNnSc4	místo
povrchu	povrch	k1gInSc2	povrch
čočky	čočka	k1gFnSc2	čočka
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
čočky	čočka	k1gFnSc2	čočka
láme	lámat	k5eAaImIp3nS	lámat
podle	podle	k7c2	podle
Snellova	Snellův	k2eAgInSc2d1	Snellův
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
podle	podle	k7c2	podle
stejného	stejný	k2eAgInSc2d1	stejný
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
lomí	lomit	k5eAaImIp3nS	lomit
na	na	k7c6	na
protilehlém	protilehlý	k2eAgInSc6d1	protilehlý
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
světla	světlo	k1gNnSc2	světlo
odráží	odrážet	k5eAaImIp3nS	odrážet
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
schematické	schematický	k2eAgNnSc4d1	schematické
znázornění	znázornění	k1gNnSc4	znázornění
čočky	čočka	k1gFnSc2	čočka
s	s	k7c7	s
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
přesností	přesnost	k1gFnSc7	přesnost
předpokládat	předpokládat	k5eAaImF	předpokládat
platnost	platnost	k1gFnSc4	platnost
vztahu	vztah	k1gInSc2	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
o	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o	o	k7c4	o
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
optická	optický	k2eAgFnSc1d1	optická
osa	osa	k1gFnSc1	osa
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
body	bod	k1gInPc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
představují	představovat	k5eAaImIp3nP	představovat
předmětové	předmětový	k2eAgNnSc4d1	předmětové
a	a	k8xC	a
obrazové	obrazový	k2eAgNnSc4d1	obrazové
ohnisko	ohnisko	k1gNnSc4	ohnisko
první	první	k4xOgFnSc2	první
lámavé	lámavý	k2eAgFnSc2d1	lámavá
plochy	plocha	k1gFnSc2	plocha
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
křivosti	křivost	k1gFnSc2	křivost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V_	V_	k1gMnSc6	V_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
,	,	kIx,	,
a	a	k8xC	a
body	bod	k1gInPc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
představují	představovat	k5eAaImIp3nP	představovat
předmětové	předmětový	k2eAgNnSc4d1	předmětové
a	a	k8xC	a
obrazové	obrazový	k2eAgNnSc4d1	obrazové
ohnisko	ohnisko	k1gNnSc4	ohnisko
druhé	druhý	k4xOgFnSc2	druhý
lámavé	lámavý	k2eAgFnSc2d1	lámavá
plochy	plocha	k1gFnSc2	plocha
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
křivosti	křivost	k1gFnSc2	křivost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc6	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V_	V_	k1gMnSc6	V_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
představují	představovat	k5eAaImIp3nP	představovat
předmětové	předmětový	k2eAgNnSc4d1	předmětové
a	a	k8xC	a
obrazové	obrazový	k2eAgNnSc4d1	obrazové
ohnisko	ohnisko	k1gNnSc4	ohnisko
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Předmětová	předmětový	k2eAgFnSc1d1	předmětová
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
čočky	čočka	k1gFnSc2	čočka
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
obrazová	obrazový	k2eAgFnSc1d1	obrazová
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
obrazového	obrazový	k2eAgNnSc2d1	obrazové
ohniska	ohnisko	k1gNnSc2	ohnisko
první	první	k4xOgFnSc2	první
lámavé	lámavý	k2eAgFnSc2d1	lámavá
plochy	plocha	k1gFnSc2	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
a	a	k8xC	a
předmětového	předmětový	k2eAgNnSc2d1	předmětové
ohniska	ohnisko	k1gNnSc2	ohnisko
druhé	druhý	k4xOgFnSc2	druhý
lámavé	lámavý	k2eAgFnSc2d1	lámavá
plochy	plocha	k1gFnSc2	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
optický	optický	k2eAgInSc4d1	optický
interval	interval	k1gInSc4	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Optickou	optický	k2eAgFnSc7d1	optická
mohutností	mohutnost	k1gFnSc7	mohutnost
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
převrácenou	převrácený	k2eAgFnSc4d1	převrácená
hodnotu	hodnota	k1gFnSc4	hodnota
obrazové	obrazový	k2eAgFnSc2d1	obrazová
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D	D	kA	D
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tloušťka	tloušťka	k1gFnSc1	tloušťka
čočky	čočka	k1gFnSc2	čočka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
}	}	kIx)	}
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
poloměry	poloměr	k1gInPc1	poloměr
křivosti	křivost	k1gFnSc2	křivost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
lámavých	lámavý	k2eAgFnPc2d1	lámavá
ploch	plocha	k1gFnPc2	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc6	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc6	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
≪	≪	k?	≪
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
ll	ll	k?	ll
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
≪	≪	k?	≪
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
ll	ll	k?	ll
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
čočka	čočka	k1gFnSc1	čočka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tlusté	tlustý	k2eAgFnSc6d1	tlustá
čočce	čočka	k1gFnSc6	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
ohniskových	ohniskový	k2eAgFnPc2d1	ohnisková
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
obou	dva	k4xCgFnPc6	dva
lámavých	lámavý	k2eAgFnPc2d1	lámavá
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
čočku	čočka	k1gFnSc4	čočka
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
,	,	kIx,	,
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
ohniskové	ohniskový	k2eAgFnPc4d1	ohnisková
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
čočky	čočka	k1gFnSc2	čočka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}}}	}}}	k?	}}}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
prostředí	prostředí	k1gNnSc4	prostředí
před	před	k7c7	před
čočkou	čočka	k1gFnSc7	čočka
i	i	k8xC	i
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
stejné	stejná	k1gFnSc2	stejná
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
platit	platit	k5eAaImF	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-f	-f	k?	-f
<g/>
}	}	kIx)	}
:	:	kIx,	:
Ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
čočky	čočka	k1gFnSc2	čočka
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
také	také	k9	také
pomocí	pomocí	k7c2	pomocí
poloměrů	poloměr	k1gInPc2	poloměr
křivosti	křivost	k1gFnSc2	křivost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
lámavých	lámavý	k2eAgFnPc2d1	lámavá
ploch	plocha	k1gFnPc2	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc6	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc6	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
a	a	k8xC	a
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
čočky	čočka	k1gFnSc2	čočka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Získáme	získat	k5eAaPmIp1nP	získat
tak	tak	k9	tak
tzv.	tzv.	kA	tzv.
zobrazovací	zobrazovací	k2eAgFnSc4d1	zobrazovací
rovnici	rovnice	k1gFnSc4	rovnice
tlusté	tlustý	k2eAgFnSc2d1	tlustá
čočky	čočka	k1gFnSc2	čočka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
[	[	kIx(	[
n	n	k0	n
(	(	kIx(	(
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
d	d	k?	d
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
nR_	nR_	k?	nR_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
R_	R_	k1gFnSc2	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s>
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
R_	R_	k1gMnSc6	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-R_	-R_	k?	-R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
]	]	kIx)	]
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-f	-f	k?	-f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
:	:	kIx,	:
Pro	pro	k7c4	pro
tenkou	tenký	k2eAgFnSc4d1	tenká
čočku	čočka	k1gFnSc4	čočka
lze	lze	k6eAd1	lze
tuto	tento	k3xDgFnSc4	tento
zobrazovací	zobrazovací	k2eAgFnSc4d1	zobrazovací
rovnici	rovnice	k1gFnSc4	rovnice
přepsat	přepsat	k5eAaPmF	přepsat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-R_	-R_	k?	-R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-f	-f	k?	-f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
:	:	kIx,	:
Pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s hack="1">
čočku	čočka	k1gFnSc4	čočka
platí	platit	k5eAaImIp3nP	platit
vztahy	vztah	k1gInPc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
-a	-a	k?	-a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
-f	-f	k?	-f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
příčná	příčný	k2eAgFnSc1d1	příčná
velikost	velikost	k1gFnSc1	velikost
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
příčná	příčný	k2eAgFnSc1d1	příčná
velikost	velikost	k1gFnSc1	velikost
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
předmětu	předmět	k1gInSc2	předmět
od	od	k7c2	od
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
obrazu	obraz	k1gInSc2	obraz
od	od	k7c2	od
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovací	zobrazovací	k2eAgFnSc4d1	zobrazovací
rovnici	rovnice	k1gFnSc4	rovnice
čočky	čočka	k1gFnSc2	čočka
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgInPc2	tento
vztahů	vztah	k1gInPc2	vztah
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Pro	pro	k7c4	pro
tenkou	tenký	k2eAgFnSc4d1	tenká
čočku	čočka	k1gFnSc4	čočka
<g />
.	.	kIx.	.
</s>
<s hack="1">
lze	lze	k6eAd1	lze
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
přepsat	přepsat	k5eAaPmF	přepsat
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
rovnice	rovnice	k1gFnSc1	rovnice
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
spojnou	spojný	k2eAgFnSc4d1	spojná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
rozptylnou	rozptylný	k2eAgFnSc4d1	rozptylná
čočku	čočka	k1gFnSc4	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
vztahu	vztah	k1gInSc2	vztah
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
optickou	optický	k2eAgFnSc4d1	optická
mohutnost	mohutnost	k1gFnSc4	mohutnost
můžeme	moct	k5eAaImIp1nP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
σ	σ	k?	σ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D	D	kA	D
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc4	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
sigma	sigma	k1gNnSc1	sigma
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
označuje	označovat	k5eAaImIp3nS	označovat
tzv.	tzv.	kA	tzv.
vypuklost	vypuklost	k1gFnSc4	vypuklost
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
objektivech	objektiv	k1gInPc6	objektiv
fotografických	fotografický	k2eAgInPc2d1	fotografický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
potřeba	potřeba	k6eAd1	potřeba
výrazně	výrazně	k6eAd1	výrazně
zmenšit	zmenšit	k5eAaPmF	zmenšit
ztráty	ztráta	k1gFnPc4	ztráta
způsobené	způsobený	k2eAgFnPc4d1	způsobená
odrazem	odraz	k1gInSc7	odraz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
povrch	povrch	k7c2wR	povrch
čoček	čočka	k1gFnPc2	čočka
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
vrstvami	vrstva	k1gFnPc7	vrstva
průhledných	průhledný	k2eAgFnPc2d1	průhledná
dielektrických	dielektrický	k2eAgFnPc2d1	dielektrická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
antireflexní	antireflexní	k2eAgNnSc1d1	antireflexní
pokrytí	pokrytí	k1gNnSc1	pokrytí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
rozsahu	rozsah	k1gInSc6	rozsah
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
světlo	světlo	k1gNnSc4	světlo
prochází	procházet	k5eAaImIp3nS	procházet
čočkou	čočka	k1gFnSc7	čočka
prakticky	prakticky	k6eAd1	prakticky
beze	beze	k7c2	beze
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Čočky	čočka	k1gFnPc1	čočka
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
kulové	kulový	k2eAgInPc1d1	kulový
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
jejich	jejich	k3xOp3gFnPc7	jejich
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
částí	část	k1gFnSc7	část
kulové	kulový	k2eAgFnSc2d1	kulová
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
čočky	čočka	k1gFnPc1	čočka
jiných	jiný	k2eAgInPc2d1	jiný
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
čoček	čočka	k1gFnPc2	čočka
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
prošlý	prošlý	k2eAgInSc4d1	prošlý
rovnoběžný	rovnoběžný	k2eAgInSc4d1	rovnoběžný
(	(	kIx(	(
<g/>
kolimovaný	kolimovaný	k2eAgInSc4d1	kolimovaný
<g/>
)	)	kIx)	)
optický	optický	k2eAgInSc4d1	optický
svazek	svazek	k1gInSc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Spojné	spojný	k2eAgFnPc1d1	spojná
čočky	čočka	k1gFnPc1	čočka
neboli	neboli	k8xC	neboli
spojky	spojka	k1gFnPc1	spojka
mění	měnit	k5eAaImIp3nP	měnit
svazek	svazek	k1gInSc4	svazek
na	na	k7c4	na
sbíhavý	sbíhavý	k2eAgInSc4d1	sbíhavý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
paprsky	paprsek	k1gInPc1	paprsek
se	se	k3xPyFc4	se
za	za	k7c7	za
nimi	on	k3xPp3gFnPc7	on
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
označovaném	označovaný	k2eAgInSc6d1	označovaný
jako	jako	k8xS	jako
ohnisko	ohnisko	k1gNnSc4	ohnisko
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
skutečný	skutečný	k2eAgInSc1d1	skutečný
obraz	obraz	k1gInSc1	obraz
předmětu	předmět	k1gInSc2	předmět
před	před	k7c7	před
čočkou	čočka	k1gFnSc7	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
rozptylné	rozptylný	k2eAgFnPc1d1	rozptylná
čočky	čočka	k1gFnPc1	čočka
neboli	neboli	k8xC	neboli
rozptylky	rozptylka	k1gFnPc1	rozptylka
svazek	svazek	k1gInSc1	svazek
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c4	na
rozbíhavý	rozbíhavý	k2eAgInSc4d1	rozbíhavý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
ohniska	ohnisko	k1gNnSc2	ohnisko
před	před	k7c7	před
čočkou	čočka	k1gFnSc7	čočka
–	–	k?	–
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Spojky	spojka	k1gFnPc1	spojka
(	(	kIx(	(
<g/>
též	též	k9	též
spojné	spojný	k2eAgFnSc2d1	spojná
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
konvexní	konvexní	k2eAgFnSc2d1	konvexní
čočky	čočka	k1gFnSc2	čočka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
uprostřed	uprostřed	k6eAd1	uprostřed
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
vypuklý	vypuklý	k2eAgInSc4d1	vypuklý
povrch	povrch	k1gInSc4	povrch
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
dvojvypuklé	dvojvypuklý	k2eAgFnSc6d1	dvojvypuklá
(	(	kIx(	(
<g/>
bikonvexní	bikonvexní	k2eAgFnSc6d1	bikonvexní
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
také	také	k9	také
vypuklý	vypuklý	k2eAgInSc1d1	vypuklý
ploskovypuklé	ploskovypuklý	k2eAgFnSc3d1	ploskovypuklá
(	(	kIx(	(
<g/>
plankonvexní	plankonvexní	k2eAgFnSc3d1	plankonvexní
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
rovinný	rovinný	k2eAgMnSc1d1	rovinný
dutovypuklé	dutovypuklý	k2eAgNnSc4d1	dutovypuklý
(	(	kIx(	(
<g/>
vydutovypuklé	vydutovypuklý	k2eAgNnSc4d1	vydutovypuklý
<g/>
,	,	kIx,	,
konkávkonvexní	konkávkonvexní	k2eAgNnSc4d1	konkávkonvexní
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgMnSc1d1	dutý
<g/>
.	.	kIx.	.
</s>
<s>
Rozptylka	rozptylka	k1gFnSc1	rozptylka
(	(	kIx(	(
<g/>
též	též	k9	též
rozptylné	rozptylný	k2eAgFnSc2d1	rozptylná
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
konkávní	konkávní	k2eAgFnSc2d1	konkávní
čočky	čočka	k1gFnSc2	čočka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
uprostřed	uprostřed	k7c2	uprostřed
tenčí	tenčí	k1gNnSc2	tenčí
než	než	k8xS	než
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgInSc4	jeden
povrch	povrch	k1gInSc4	povrch
dutý	dutý	k2eAgInSc4d1	dutý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
druhého	druhý	k4xOgInSc2	druhý
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
dvojduté	dvojdutý	k2eAgNnSc4d1	dvojdutý
(	(	kIx(	(
<g/>
dvojvyduté	dvojvydutý	k2eAgNnSc4d1	dvojvydutý
<g/>
,	,	kIx,	,
bikonkávní	bikonkávní	k2eAgNnSc4d1	bikonkávní
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
také	také	k9	také
dutý	dutý	k2eAgInSc1d1	dutý
ploskoduté	ploskodutý	k2eAgNnSc4d1	ploskodutý
(	(	kIx(	(
<g/>
ploskovyduté	ploskovydutý	k2eAgNnSc4d1	ploskovydutý
<g/>
,	,	kIx,	,
plankonkávní	plankonkávní	k2eAgNnSc4d1	plankonkávní
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
rovinný	rovinný	k2eAgMnSc1d1	rovinný
vypukloduté	vypuklodutý	k2eAgInPc4d1	vypuklodutý
(	(	kIx(	(
<g/>
konvexkonkávní	konvexkonkávní	k2eAgInPc4d1	konvexkonkávní
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
vypuklý	vypuklý	k2eAgMnSc1d1	vypuklý
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
čočky	čočka	k1gFnPc1	čočka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
jiný	jiný	k2eAgInSc4d1	jiný
tvar	tvar	k1gInSc4	tvar
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
kulová	kulový	k2eAgFnSc1d1	kulová
výseč	výseč	k1gFnSc1	výseč
<g/>
:	:	kIx,	:
válcová	válcový	k2eAgFnSc1d1	válcová
neboli	neboli	k8xC	neboli
cylindrická	cylindrický	k2eAgFnSc1d1	cylindrická
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
aspoň	aspoň	k9	aspoň
jeden	jeden	k4xCgMnSc1	jeden
její	její	k3xOp3gFnSc7	její
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
částí	část	k1gFnSc7	část
válce	válec	k1gInSc2	válec
<g/>
;	;	kIx,	;
taková	takový	k3xDgFnSc1	takový
čočka	čočka	k1gFnSc1	čočka
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
chod	chod	k1gInSc1	chod
paprsků	paprsek	k1gInPc2	paprsek
jen	jen	k9	jen
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kolmé	kolmý	k2eAgFnSc6d1	kolmá
na	na	k7c4	na
osu	osa	k1gFnSc4	osa
tohoto	tento	k3xDgInSc2	tento
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
určené	určený	k2eAgFnSc6d1	určená
směrem	směr	k1gInSc7	směr
paprsku	paprsek	k1gInSc2	paprsek
a	a	k8xC	a
osou	osa	k1gFnSc7	osa
válce	válec	k1gInSc2	válec
není	být	k5eNaImIp3nS	být
sbíhavost	sbíhavost	k1gFnSc1	sbíhavost
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
některých	některý	k3yIgFnPc2	některý
vad	vada	k1gFnPc2	vada
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
multifokální	multifokální	k2eAgFnSc1d1	multifokální
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
různou	různý	k2eAgFnSc4d1	různá
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
multifokálních	multifokální	k2eAgFnPc2d1	multifokální
brýlí	brýle	k1gFnPc2	brýle
<g/>
.	.	kIx.	.
</s>
<s>
Fresnelova	Fresnelův	k2eAgFnSc1d1	Fresnelova
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
plochá	plochý	k2eAgFnSc1d1	plochá
čočka	čočka	k1gFnSc1	čočka
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
rastrováním	rastrování	k1gNnSc7	rastrování
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
kulové	kulový	k2eAgFnSc2d1	kulová
nebo	nebo	k8xC	nebo
válcové	válcový	k2eAgFnSc2d1	válcová
čočky	čočka	k1gFnSc2	čočka
asférická	asférický	k2eAgFnSc1d1	asférická
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
je	být	k5eAaImIp3nS	být
rotačně	rotačně	k6eAd1	rotačně
symetrická	symetrický	k2eAgFnSc1d1	symetrická
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
navržené	navržený	k2eAgInPc4d1	navržený
tvary	tvar	k1gInPc4	tvar
takovýchto	takovýto	k3xDgFnPc2	takovýto
čoček	čočka	k1gFnPc2	čočka
umožnily	umožnit	k5eAaPmAgFnP	umožnit
například	například	k6eAd1	například
konstrukci	konstrukce	k1gFnSc4	konstrukce
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
fotografických	fotografický	k2eAgInPc2d1	fotografický
objektivů	objektiv	k1gInPc2	objektiv
a	a	k8xC	a
astronomických	astronomický	k2eAgInPc2d1	astronomický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
též	též	k9	též
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
některých	některý	k3yIgFnPc2	některý
forem	forma	k1gFnPc2	forma
astigmatismu	astigmatismus	k1gInSc2	astigmatismus
<g/>
.	.	kIx.	.
toroidní	toroidní	k2eAgFnSc1d1	toroidní
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
navzájem	navzájem	k6eAd1	navzájem
kolmých	kolmý	k2eAgFnPc6d1	kolmá
rovinách	rovina	k1gFnPc6	rovina
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
zakřivení	zakřivení	k1gNnSc4	zakřivení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
sbíhavost	sbíhavost	k1gFnSc1	sbíhavost
paprsků	paprsek	k1gInPc2	paprsek
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
astigmatismu	astigmatismus	k1gInSc2	astigmatismus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Optické	optický	k2eAgNnSc1d1	optické
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zobrazování	zobrazování	k1gNnSc4	zobrazování
pomocí	pomocí	k7c2	pomocí
čoček	čočka	k1gFnPc2	čočka
požíváme	požívat	k5eAaImIp1nP	požívat
chodu	chod	k1gInSc2	chod
tzv.	tzv.	kA	tzv.
význačných	význačný	k2eAgInPc2d1	význačný
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Význačné	význačný	k2eAgInPc1d1	význačný
paprsky	paprsek	k1gInPc1	paprsek
spojky	spojka	k1gFnSc2	spojka
<g/>
:	:	kIx,	:
Paprsek	paprsek	k1gInSc1	paprsek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
optickým	optický	k2eAgInSc7d1	optický
středem	střed	k1gInSc7	střed
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neláme	lámat	k5eNaImIp3nS	lámat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
paprsek	paprsek	k1gInSc4	paprsek
nazýváme	nazývat	k5eAaImIp1nP	nazývat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Paprsek	paprsek	k1gInSc1	paprsek
procházející	procházející	k2eAgInSc1d1	procházející
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
spojku	spojka	k1gFnSc4	spojka
ohniskem	ohnisko	k1gNnSc7	ohnisko
F1	F1	k1gMnSc2	F1
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
osou	osa	k1gFnSc7	osa
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Paprsek	paprsek	k1gInSc1	paprsek
dopadající	dopadající	k2eAgInSc1d1	dopadající
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
osou	osa	k1gFnSc7	osa
čočky	čočka	k1gFnSc2	čočka
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
do	do	k7c2	do
ohniska	ohnisko	k1gNnSc2	ohnisko
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Význačné	význačný	k2eAgInPc1d1	význačný
paprsky	paprsek	k1gInPc1	paprsek
rozptylky	rozptylka	k1gFnSc2	rozptylka
<g/>
:	:	kIx,	:
Paprsek	paprsek	k1gInSc1	paprsek
rovnoběžný	rovnoběžný	k2eAgInSc1d1	rovnoběžný
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
osou	osa	k1gFnSc7	osa
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
prochází	procházet	k5eAaImIp3nS	procházet
ohniskem	ohnisko	k1gNnSc7	ohnisko
F1	F1	k1gFnSc2	F1
(	(	kIx(	(
<g/>
Paprsky	paprsek	k1gInPc1	paprsek
se	se	k3xPyFc4	se
lámou	lámat	k5eAaImIp3nP	lámat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
ohniska	ohnisko	k1gNnSc2	ohnisko
F1	F1	k1gFnSc2	F1
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
ose	osa	k1gFnSc6	osa
<g/>
)	)	kIx)	)
Paprsek	paprsek	k1gInSc1	paprsek
mířící	mířící	k2eAgFnSc7d1	mířící
do	do	k7c2	do
ohniska	ohnisko	k1gNnSc2	ohnisko
F2	F2	k1gFnSc2	F2
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
osou	osa	k1gFnSc7	osa
Paprsek	paprsek	k1gInSc4	paprsek
procházející	procházející	k2eAgInSc4d1	procházející
optickým	optický	k2eAgInSc7d1	optický
středem	střed	k1gInSc7	střed
čočky	čočka	k1gFnSc2	čočka
nemění	měnit	k5eNaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
směr	směr	k1gInSc4	směr
Žádná	žádný	k3yNgFnSc1	žádný
čočka	čočka	k1gFnSc1	čočka
se	se	k3xPyFc4	se
nechová	chovat	k5eNaImIp3nS	chovat
ideálně	ideálně	k6eAd1	ideálně
–	–	k?	–
při	při	k7c6	při
zobrazování	zobrazování	k1gNnSc6	zobrazování
předmětů	předmět	k1gInPc2	předmět
vznikají	vznikat	k5eAaImIp3nP	vznikat
různé	různý	k2eAgFnPc4d1	různá
vady	vada	k1gFnPc4	vada
a	a	k8xC	a
deformace	deformace	k1gFnPc4	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgFnPc7d3	nejčastější
vadami	vada	k1gFnPc7	vada
čoček	čočka	k1gFnPc2	čočka
jsou	být	k5eAaImIp3nP	být
Barevná	barevný	k2eAgFnSc1d1	barevná
vada	vada	k1gFnSc1	vada
neboli	neboli	k8xC	neboli
chromatická	chromatický	k2eAgFnSc1d1	chromatická
aberace	aberace	k1gFnSc1	aberace
je	být	k5eAaImIp3nS	být
vada	vada	k1gFnSc1	vada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
čočky	čočka	k1gFnSc2	čočka
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
indexu	index	k1gInSc6	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
použitého	použitý	k2eAgNnSc2d1	Použité
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
však	však	k9	však
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
jeho	jeho	k3xOp3gFnSc1	jeho
složka	složka	k1gFnSc1	složka
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
každá	každý	k3xTgFnSc1	každý
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
čočkou	čočka	k1gFnSc7	čočka
láme	lámat	k5eAaImIp3nS	lámat
trochu	trochu	k6eAd1	trochu
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
čočkou	čočka	k1gFnSc7	čočka
s	s	k7c7	s
barevnou	barevný	k2eAgFnSc7d1	barevná
vadou	vada	k1gFnSc7	vada
tedy	tedy	k9	tedy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
vady	vada	k1gFnSc2	vada
je	být	k5eAaImIp3nS	být
obrazem	obraz	k1gInSc7	obraz
bodu	bůst	k5eAaImIp1nS	bůst
bod	bod	k1gInSc1	bod
určité	určitý	k2eAgFnSc2d1	určitá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
mezikružími	mezikruží	k1gNnPc7	mezikruží
jiných	jiný	k2eAgFnPc2d1	jiná
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Chromatickou	chromatický	k2eAgFnSc4d1	chromatická
vadu	vada	k1gFnSc4	vada
lze	lze	k6eAd1	lze
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
odstranit	odstranit	k5eAaPmF	odstranit
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
kombinací	kombinace	k1gFnSc7	kombinace
spojných	spojný	k2eAgFnPc2d1	spojná
a	a	k8xC	a
rozptylných	rozptylný	k2eAgFnPc2d1	rozptylná
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
achromatizací	achromatizace	k1gFnSc7	achromatizace
optické	optický	k2eAgFnSc2d1	optická
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Sférická	sférický	k2eAgFnSc1d1	sférická
(	(	kIx(	(
<g/>
též	též	k9	též
kulová	kulový	k2eAgFnSc1d1	kulová
nebo	nebo	k8xC	nebo
otvorová	otvorový	k2eAgFnSc1d1	otvorová
<g/>
)	)	kIx)	)
vada	vada	k1gFnSc1	vada
vzniká	vznikat	k5eAaImIp3nS	vznikat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
na	na	k7c4	na
čočku	čočka	k1gFnSc4	čočka
dopadá	dopadat	k5eAaImIp3nS	dopadat
široký	široký	k2eAgInSc1d1	široký
svazek	svazek	k1gInSc1	svazek
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
paraxiální	paraxiální	k2eAgInPc1d1	paraxiální
paprsky	paprsek	k1gInPc1	paprsek
se	se	k3xPyFc4	se
za	za	k7c7	za
čočkou	čočka	k1gFnSc7	čočka
setkávají	setkávat	k5eAaImIp3nP	setkávat
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
bodě	bod	k1gInSc6	bod
než	než	k8xS	než
okrajové	okrajový	k2eAgInPc1d1	okrajový
paprsky	paprsek	k1gInPc1	paprsek
širokého	široký	k2eAgInSc2d1	široký
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
x	x	k?	x
<g/>
}	}	kIx)	}
mezi	mezi	k7c7	mezi
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
paraxiální	paraxiální	k2eAgInPc1d1	paraxiální
a	a	k8xC	a
okrajové	okrajový	k2eAgInPc1d1	okrajový
paprsky	paprsek	k1gInPc1	paprsek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sférická	sférický	k2eAgFnSc1d1	sférická
aberace	aberace	k1gFnSc1	aberace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vada	vada	k1gFnSc1	vada
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obrazem	obraz	k1gInSc7	obraz
bodu	bod	k1gInSc2	bod
není	být	k5eNaImIp3nS	být
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozmazaná	rozmazaný	k2eAgFnSc1d1	rozmazaná
kruhová	kruhový	k2eAgFnSc1d1	kruhová
ploška	ploška	k1gFnSc1	ploška
<g/>
.	.	kIx.	.
</s>
<s>
Určitého	určitý	k2eAgNnSc2d1	určité
zostření	zostření	k1gNnSc2	zostření
obrazu	obraz	k1gInSc2	obraz
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
odcloněním	odclonění	k1gNnSc7	odclonění
okrajových	okrajový	k2eAgInPc2d1	okrajový
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Omezováním	omezování	k1gNnSc7	omezování
obrazu	obraz	k1gInSc2	obraz
však	však	k9	však
klesá	klesat	k5eAaImIp3nS	klesat
světlost	světlost	k1gFnSc4	světlost
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
vadu	vada	k1gFnSc4	vada
lze	lze	k6eAd1	lze
také	také	k9	také
částečně	částečně	k6eAd1	částečně
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
kombinací	kombinace	k1gFnPc2	kombinace
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
<g/>
-li	i	k?	-li
odstranění	odstranění	k1gNnSc4	odstranění
kulové	kulový	k2eAgFnSc2d1	kulová
vady	vada	k1gFnSc2	vada
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
}	}	kIx)	}
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
ose	osa	k1gFnSc6	osa
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tím	ten	k3xDgInSc7	ten
automaticky	automaticky	k6eAd1	automaticky
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
odstranění	odstranění	k1gNnSc1	odstranění
této	tento	k3xDgFnSc2	tento
vady	vada	k1gFnSc2	vada
také	také	k9	také
pro	pro	k7c4	pro
jiný	jiný	k2eAgInSc4d1	jiný
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gMnSc6	P_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
,	,	kIx,	,
který	který	k3yRgMnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
od	od	k7c2	od
optické	optický	k2eAgFnSc2d1	optická
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
bude	být	k5eAaImBp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
splnění	splnění	k1gNnSc6	splnění
tzv.	tzv.	kA	tzv.
sinové	sinový	k2eAgInPc1d1	sinový
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Abbeovy	Abbeův	k2eAgFnPc1d1	Abbeův
<g/>
)	)	kIx)	)
podmínky	podmínka	k1gFnPc1	podmínka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
y	y	k?	y
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
ny	ny	k?	ny
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
indexy	index	k1gInPc4	index
lomu	lom	k1gInSc2	lom
v	v	k7c6	v
předmětovém	předmětový	k2eAgInSc6d1	předmětový
a	a	k8xC	a
obrazovém	obrazový	k2eAgInSc6d1	obrazový
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
bodu	bod	k1gInSc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
}	}	kIx)	}
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
ose	osa	k1gFnSc6	osa
a	a	k8xC	a
bodu	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gMnSc6	P_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
v	v	k7c6	v
předmětovém	předmětový	k2eAgInSc6d1	předmětový
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
bodu	bod	k1gInSc2	bod
jejich	jejich	k3xOp3gInPc2	jejich
obrazů	obraz	k1gInPc2	obraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gMnSc6	P_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
,	,	kIx,	,
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
úhly	úhel	k1gInPc1	úhel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
svírají	svírat	k5eAaImIp3nP	svírat
sdružené	sdružený	k2eAgInPc1d1	sdružený
paprsky	paprsek	k1gInPc1	paprsek
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
osou	osa	k1gFnSc7	osa
<g/>
.	.	kIx.	.
</s>
<s>
Sinová	sinový	k2eAgFnSc1d1	sinová
podmínka	podmínka	k1gFnSc1	podmínka
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
především	především	k9	především
při	při	k7c6	při
zobrazování	zobrazování	k1gNnSc6	zobrazování
širokých	široký	k2eAgInPc2d1	široký
svazků	svazek	k1gInPc2	svazek
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
požadováno	požadován	k2eAgNnSc1d1	požadováno
nejen	nejen	k6eAd1	nejen
ostré	ostrý	k2eAgNnSc1d1	ostré
zobrazení	zobrazení	k1gNnSc1	zobrazení
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ostré	ostrý	k2eAgNnSc4d1	ostré
zobrazení	zobrazení	k1gNnSc4	zobrazení
malé	malý	k2eAgFnSc2d1	malá
plochy	plocha	k1gFnSc2	plocha
kolem	kolem	k7c2	kolem
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Sinová	sinový	k2eAgFnSc1d1	sinová
podmínka	podmínka	k1gFnSc1	podmínka
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
správné	správný	k2eAgNnSc4d1	správné
zobrazení	zobrazení	k1gNnSc4	zobrazení
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kolmé	kolmý	k2eAgFnSc2d1	kolmá
k	k	k7c3	k
optické	optický	k2eAgFnSc3d1	optická
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
nezajišťuje	zajišťovat	k5eNaImIp3nS	zajišťovat
však	však	k9	však
správné	správný	k2eAgNnSc1d1	správné
zobrazení	zobrazení	k1gNnSc1	zobrazení
bodů	bod	k1gInPc2	bod
odlišných	odlišný	k2eAgInPc2d1	odlišný
od	od	k7c2	od
bodu	bod	k1gInSc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yQgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
soustava	soustava	k1gFnSc1	soustava
bez	bez	k7c2	bez
sférické	sférický	k2eAgFnSc2d1	sférická
vady	vada	k1gFnSc2	vada
korigována	korigovat	k5eAaBmNgFnS	korigovat
také	také	k9	také
pro	pro	k7c4	pro
blízké	blízký	k2eAgInPc4d1	blízký
body	bod	k1gInPc4	bod
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
ose	osa	k1gFnSc6	osa
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
splněna	splnit	k5eAaPmNgFnS	splnit
tzv.	tzv.	kA	tzv.
Herschelova	Herschelův	k2eAgFnSc1d1	Herschelova
podmínka	podmínka	k1gFnSc1	podmínka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
y	y	k?	y
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ny	ny	k?	ny
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Sinová	sinový	k2eAgFnSc1d1	sinová
a	a	k8xC	a
Herschelova	Herschelův	k2eAgFnSc1d1	Herschelova
podmínka	podmínka	k1gFnSc1	podmínka
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
odporují	odporovat	k5eAaImIp3nP	odporovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zobrazit	zobrazit	k5eAaPmF	zobrazit
ostře	ostro	k6eAd1	ostro
objemovou	objemový	k2eAgFnSc4d1	objemová
část	část	k1gFnSc4	část
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
splněny	splnit	k5eAaPmNgFnP	splnit
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
zrcadlení	zrcadlení	k1gNnSc6	zrcadlení
širokých	široký	k2eAgInPc2d1	široký
svazků	svazek	k1gInPc2	svazek
paprsků	paprsek	k1gInPc2	paprsek
na	na	k7c6	na
rovinném	rovinný	k2eAgNnSc6d1	rovinné
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-n	-n	k?	-n
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
velmi	velmi	k6eAd1	velmi
úzkých	úzký	k2eAgInPc2d1	úzký
svazků	svazek	k1gInPc2	svazek
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
≈	≈	k?	≈
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
≈	≈	k?	≈
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
sinová	sinový	k2eAgFnSc1d1	sinová
a	a	k8xC	a
Herschelova	Herschelův	k2eAgFnSc1d1	Herschelova
podmínka	podmínka	k1gFnSc1	podmínka
v	v	k7c4	v
Helmholtz-Lagrangeovu	Helmholtz-Lagrangeův	k2eAgFnSc4d1	Helmholtz-Lagrangeův
rovnici	rovnice	k1gFnSc4	rovnice
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
y	y	k?	y
α	α	k?	α
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ny	ny	k?	ny
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
rovnice	rovnice	k1gFnSc1	rovnice
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc2	zobrazení
paraxiálními	paraxiální	k2eAgInPc7d1	paraxiální
svazky	svazek	k1gInPc7	svazek
kolineárních	kolineární	k2eAgInPc2d1	kolineární
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Astigmatická	astigmatický	k2eAgFnSc1d1	astigmatická
vada	vada	k1gFnSc1	vada
(	(	kIx(	(
<g/>
astigmatismus	astigmatismus	k1gInSc1	astigmatismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vada	vada	k1gFnSc1	vada
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
zobrazení	zobrazení	k1gNnSc6	zobrazení
roviny	rovina	k1gFnSc2	rovina
kolmé	kolmý	k2eAgFnSc2d1	kolmá
k	k	k7c3	k
optické	optický	k2eAgFnSc3d1	optická
ose	osa	k1gFnSc3	osa
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
body	bod	k1gInPc1	bod
v	v	k7c6	v
navzájem	navzájem	k6eAd1	navzájem
kolmých	kolmý	k2eAgFnPc6d1	kolmá
osách	osa	k1gFnPc6	osa
se	se	k3xPyFc4	se
nezobrazí	zobrazit	k5eNaPmIp3nS	zobrazit
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Astigmatismus	astigmatismus	k1gInSc1	astigmatismus
také	také	k9	také
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
paprsek	paprsek	k1gInSc1	paprsek
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
optickou	optický	k2eAgFnSc4d1	optická
soustavu	soustava	k1gFnSc4	soustava
kolmo	kolmo	k6eAd1	kolmo
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
body	bod	k1gInPc7	bod
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
ose	osa	k1gFnSc6	osa
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
paprsky	paprsek	k1gInPc1	paprsek
ze	z	k7c2	z
vzájemně	vzájemně	k6eAd1	vzájemně
kolmých	kolmý	k2eAgFnPc2d1	kolmá
os	osa	k1gFnPc2	osa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
astigmatický	astigmatický	k2eAgInSc1d1	astigmatický
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Úsečky	úsečka	k1gFnPc1	úsečka
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
bodech	bod	k1gInPc6	bod
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
fokály	fokála	k1gFnPc1	fokála
<g/>
.	.	kIx.	.
</s>
<s>
Astigmatismus	astigmatismus	k1gInSc4	astigmatismus
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odstranit	odstranit	k5eAaPmF	odstranit
kombinací	kombinace	k1gFnPc2	kombinace
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
soustava	soustava	k1gFnSc1	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
astigmatismus	astigmatismus	k1gInSc1	astigmatismus
projevuje	projevovat	k5eAaImIp3nS	projevovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
anastigmát	anastigmát	k5eAaPmF	anastigmát
<g/>
.	.	kIx.	.
</s>
<s>
Astigmatismus	astigmatismus	k1gInSc1	astigmatismus
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zejména	zejména	k9	zejména
při	při	k7c6	při
zobrazování	zobrazování	k1gNnSc6	zobrazování
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
pod	pod	k7c7	pod
velkým	velký	k2eAgInSc7d1	velký
zorným	zorný	k2eAgInSc7d1	zorný
úhlem	úhel	k1gInSc7	úhel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
u	u	k7c2	u
dalekohledů	dalekohled	k1gInPc2	dalekohled
je	být	k5eAaImIp3nS	být
zorný	zorný	k2eAgInSc1d1	zorný
úhel	úhel	k1gInSc1	úhel
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
astigmatismus	astigmatismus	k1gInSc1	astigmatismus
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Koma	koma	k1gFnSc1	koma
je	být	k5eAaImIp3nS	být
vada	vada	k1gFnSc1	vada
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
čočku	čočka	k1gFnSc4	čočka
dopadá	dopadat	k5eAaImIp3nS	dopadat
široký	široký	k2eAgInSc1d1	široký
svazek	svazek	k1gInSc1	svazek
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
rovnoběžný	rovnoběžný	k2eAgInSc1d1	rovnoběžný
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
osou	osa	k1gFnSc7	osa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dopadající	dopadající	k2eAgInSc1d1	dopadající
svazek	svazek	k1gInSc1	svazek
paprsků	paprsek	k1gInPc2	paprsek
dostatečně	dostatečně	k6eAd1	dostatečně
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
se	se	k3xPyFc4	se
bod	bod	k1gInSc4	bod
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
jako	jako	k9	jako
úsečka	úsečka	k1gFnSc1	úsečka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
různě	různě	k6eAd1	různě
vzdálených	vzdálený	k2eAgFnPc6d1	vzdálená
rovinách	rovina	k1gFnPc6	rovina
od	od	k7c2	od
optické	optický	k2eAgFnSc2d1	optická
soustavy	soustava	k1gFnSc2	soustava
vytvářet	vytvářet	k5eAaImF	vytvářet
složité	složitý	k2eAgInPc4d1	složitý
obrazce	obrazec	k1gInPc4	obrazec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvarem	tvar	k1gInSc7	tvar
připomínají	připomínat	k5eAaImIp3nP	připomínat
komety	kometa	k1gFnPc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Astigmatismus	astigmatismus	k1gInSc1	astigmatismus
pro	pro	k7c4	pro
široké	široký	k2eAgInPc4d1	široký
paprsky	paprsek	k1gInPc4	paprsek
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazýván	k2eAgMnSc1d1	nazýván
koma	koma	k1gNnSc4	koma
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zkreslení	zkreslení	k1gNnSc3	zkreslení
dochází	docházet	k5eAaImIp3nS	docházet
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
zvětšení	zvětšení	k1gNnSc4	zvětšení
vnějších	vnější	k2eAgFnPc2d1	vnější
částí	část	k1gFnPc2	část
předmětu	předmět	k1gInSc2	předmět
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
zvětšení	zvětšení	k1gNnSc2	zvětšení
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Zkreslení	zkreslení	k1gNnSc1	zkreslení
lze	lze	k6eAd1	lze
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
rastru	rastr	k1gInSc2	rastr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vnější	vnější	k2eAgFnPc4d1	vnější
části	část	k1gFnPc4	část
předmětu	předmět	k1gInSc2	předmět
zvětšeny	zvětšit	k5eAaPmNgInP	zvětšit
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
poduškovitém	poduškovitý	k2eAgNnSc6d1	poduškovité
zkreslení	zkreslení	k1gNnSc6	zkreslení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
naopak	naopak	k6eAd1	naopak
zvětšeny	zvětšit	k5eAaPmNgFnP	zvětšit
méně	málo	k6eAd2	málo
než	než	k8xS	než
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zkreslení	zkreslení	k1gNnSc4	zkreslení
soudkovité	soudkovitý	k2eAgNnSc4d1	soudkovité
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
zkreslení	zkreslení	k1gNnSc3	zkreslení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ortoskopická	ortoskopický	k2eAgFnSc1d1	ortoskopický
<g/>
.	.	kIx.	.
</s>
<s>
Zklenutí	zklenutí	k1gNnSc1	zklenutí
(	(	kIx(	(
<g/>
sklenutí	sklenutí	k1gNnSc4	sklenutí
<g/>
)	)	kIx)	)
zorného	zorný	k2eAgNnSc2d1	zorné
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
vada	vada	k1gFnSc1	vada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
body	bod	k1gInPc4	bod
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kolmé	kolmý	k2eAgFnSc2d1	kolmá
k	k	k7c3	k
optické	optický	k2eAgFnSc3d1	optická
ose	osa	k1gFnSc3	osa
se	se	k3xPyFc4	se
nezobrazují	zobrazovat	k5eNaImIp3nP	zobrazovat
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kolmé	kolmý	k2eAgFnSc6d1	kolmá
k	k	k7c3	k
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
zakřivené	zakřivený	k2eAgFnSc6d1	zakřivená
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kolmé	kolmý	k2eAgFnSc2d1	kolmá
k	k	k7c3	k
optické	optický	k2eAgFnSc3d1	optická
ose	osa	k1gFnSc3	osa
tak	tak	k6eAd1	tak
nelze	lze	k6eNd1	lze
získat	získat	k5eAaPmF	získat
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rozsahu	rozsah	k1gInSc6	rozsah
stejně	stejně	k6eAd1	stejně
ostrý	ostrý	k2eAgInSc1d1	ostrý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vada	vada	k1gFnSc1	vada
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
astigmatismem	astigmatismus	k1gInSc7	astigmatismus
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
anastigmátů	anastigmát	k1gInPc2	anastigmát
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
současně	současně	k6eAd1	současně
s	s	k7c7	s
astigmatismem	astigmatismus	k1gInSc7	astigmatismus
<g/>
.	.	kIx.	.
</s>
<s>
Brýle	brýle	k1gFnPc1	brýle
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
zraku	zrak	k1gInSc2	zrak
a	a	k8xC	a
lupy	lup	k1gInPc1	lup
představují	představovat	k5eAaImIp3nP	představovat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
využití	využití	k1gNnPc2	využití
čočky	čočka	k1gFnSc2	čočka
Prvním	první	k4xOgInPc3	první
fotografickým	fotografický	k2eAgInPc3d1	fotografický
přístrojům	přístroj	k1gInPc3	přístroj
stačila	stačit	k5eAaBmAgFnS	stačit
jediná	jediný	k2eAgFnSc1d1	jediná
čočka	čočka	k1gFnSc1	čočka
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
obrazu	obraz	k1gInSc2	obraz
na	na	k7c6	na
fotografickém	fotografický	k2eAgInSc6d1	fotografický
papíru	papír	k1gInSc6	papír
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
objektivech	objektiv	k1gInPc6	objektiv
většinou	většinou	k6eAd1	většinou
několik	několik	k4yIc4	několik
čoček	čočka	k1gFnPc2	čočka
za	za	k7c7	za
sebou	se	k3xPyFc7	se
Dalekohledy	dalekohled	k1gInPc1	dalekohled
a	a	k8xC	a
menší	malý	k2eAgInPc1d2	menší
astronomické	astronomický	k2eAgInPc1d1	astronomický
přístroje	přístroj	k1gInPc1	přístroj
Optické	optický	k2eAgInPc1d1	optický
mikroskopy	mikroskop	k1gInPc1	mikroskop
Rozličné	rozličný	k2eAgInPc1d1	rozličný
optické	optický	k2eAgInPc4d1	optický
a	a	k8xC	a
měřicí	měřicí	k2eAgInPc4d1	měřicí
přístroje	přístroj	k1gInPc4	přístroj
(	(	kIx(	(
<g/>
teodolit	teodolit	k1gInSc1	teodolit
<g/>
,	,	kIx,	,
heliograf	heliograf	k1gInSc1	heliograf
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
Mechaniky	mechanika	k1gFnPc1	mechanika
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
kompaktních	kompaktní	k2eAgInPc2d1	kompaktní
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
a	a	k8xC	a
disků	disk	k1gInPc2	disk
Blu-ray	Blua	k2eAgFnPc1d1	Blu-ra
(	(	kIx(	(
<g/>
používají	používat	k5eAaImIp3nP	používat
plastové	plastový	k2eAgFnPc4d1	plastová
čočky	čočka	k1gFnPc4	čočka
<g/>
)	)	kIx)	)
Lasery	laser	k1gInPc4	laser
Fresnelova	Fresnelův	k2eAgFnSc1d1	Fresnelova
čočka	čočka	k1gFnSc1	čočka
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
Einsteinem	Einstein	k1gMnSc7	Einstein
předpovězeném	předpovězený	k2eAgInSc6d1	předpovězený
ohybu	ohyb	k1gInSc6	ohyb
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
v	v	k7c6	v
astronomických	astronomický	k2eAgInPc6d1	astronomický
pozorováních	pozorování	k1gNnPc6	pozorování
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zdánlivé	zdánlivý	k2eAgInPc4d1	zdánlivý
posuny	posun	k1gInPc4	posun
některých	některý	k3yIgInPc2	některý
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
čočka	čočka	k1gFnSc1	čočka
–	–	k?	–
speciálně	speciálně	k6eAd1	speciálně
tvarované	tvarovaný	k2eAgNnSc4d1	tvarované
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
elektrony	elektron	k1gInPc4	elektron
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
klasické	klasický	k2eAgFnSc2d1	klasická
čočky	čočka	k1gFnSc2	čočka
na	na	k7c4	na
fotony	foton	k1gInPc4	foton
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
elektronového	elektronový	k2eAgInSc2d1	elektronový
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
čočka	čočka	k1gFnSc1	čočka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čočka	čočka	k1gFnSc1	čočka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Animace	animace	k1gFnSc1	animace
Feynmanovy	Feynmanův	k2eAgFnSc2d1	Feynmanova
teorie	teorie	k1gFnSc2	teorie
světla	světlo	k1gNnSc2	světlo
podle	podle	k7c2	podle
QED	QED	kA	QED
</s>
