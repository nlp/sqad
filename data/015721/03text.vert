<s>
Yetti	yetti	k1gMnSc1
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1
představa	představa	k1gFnSc1
yettiho	yetti	k1gMnSc2
</s>
<s>
Yetti	yetti	k1gMnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
neznámého	známý	k2eNgInSc2d1
druhu	druh	k1gInSc2
dvounohého	dvounohý	k2eAgMnSc4d1
hominida	hominid	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
údajně	údajně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Himálají	Himálaj	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
existence	existence	k1gFnSc2
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgFnS
spolehlivě	spolehlivě	k6eAd1
prokázána	prokázat	k5eAaPmNgFnS
nebo	nebo	k8xC
zdokumentována	zdokumentovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yetti	yetti	k1gMnSc1
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
až	až	k9
2,5	2,5	k4
metru	metr	k1gInSc2
vysoký	vysoký	k2eAgMnSc1d1
chlupatý	chlupatý	k2eAgMnSc1d1
tvor	tvor	k1gMnSc1
podobný	podobný	k2eAgInSc4d1
lidoopovi	lidoop	k1gMnSc3
nebo	nebo	k8xC
pravěkému	pravěký	k2eAgMnSc3d1
člověku	člověk	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Označení	označení	k1gNnSc1
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
tohoto	tento	k3xDgMnSc2
tvora	tvor	k1gMnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
příčinou	příčina	k1gFnSc7
nejasností	nejasnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
yetti	yetti	k1gMnSc1
(	(	kIx(
<g/>
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
první	první	k4xOgMnPc1
objevitelé	objevitel	k1gMnPc1
přinesli	přinést	k5eAaPmAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
jímž	jenž	k3xRgNnSc7
je	být	k5eAaImIp3nS
legendární	legendární	k2eAgMnSc1d1
lidoop	lidoop	k1gMnSc1
obvykle	obvykle	k6eAd1
označován	označován	k2eAgMnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
totiž	totiž	k9
v	v	k7c6
šerpštině	šerpština	k1gFnSc6
obecný	obecný	k2eAgInSc4d1
pojem	pojem	k1gInSc4
pro	pro	k7c4
"	"	kIx"
<g/>
divoké	divoký	k2eAgNnSc4d1
horské	horský	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
<g/>
"	"	kIx"
-	-	kIx~
a	a	k8xC
proto	proto	k8xC
mnozí	mnohý	k2eAgMnPc1d1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
yettiho	yetti	k1gMnSc2
<g/>
"	"	kIx"
mnohokrát	mnohokrát	k6eAd1
pozorovali	pozorovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
tak	tak	k6eAd1
označují	označovat	k5eAaImIp3nP
medvěda	medvěd	k1gMnSc4
tibetského	tibetský	k2eAgInSc2d1
(	(	kIx(
<g/>
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
říkají	říkat	k5eAaImIp3nP
dzuteh	dzuteh	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
pouze	pouze	k6eAd1
některá	některý	k3yIgNnPc1
svědectví	svědectví	k1gNnPc1
se	se	k3xPyFc4
skutečně	skutečně	k6eAd1
týkají	týkat	k5eAaImIp3nP
himálajského	himálajský	k2eAgMnSc4d1
lidoopa	lidoop	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
něj	on	k3xPp3gNnSc4
mají	mít	k5eAaImIp3nP
šerpové	šerpový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
miteh	miteha	k1gFnPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnPc4d2
jedince	jedinko	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
thelma	thelma	k1gNnSc4
(	(	kIx(
<g/>
teoreticky	teoreticky	k6eAd1
je	být	k5eAaImIp3nS
tedy	tedy	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
dokonce	dokonce	k9
o	o	k7c4
dva	dva	k4xCgInPc4
různé	různý	k2eAgInPc4d1
druhy	druh	k1gInPc4
živočichů	živočich	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
o	o	k7c6
původu	původ	k1gInSc6
</s>
<s>
Většina	většina	k1gFnSc1
odborníků	odborník	k1gMnPc2
existenci	existence	k1gFnSc4
tohoto	tento	k3xDgMnSc2
tvora	tvor	k1gMnSc2
zpochybňuje	zpochybňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
dosud	dosud	k6eAd1
nebyl	být	k5eNaImAgInS
podán	podat	k5eAaPmNgInS
vědecký	vědecký	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
(	(	kIx(
<g/>
jímž	jenž	k3xRgInSc7
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
např.	např.	kA
odchycení	odchycení	k1gNnSc2
živého	živý	k2eAgMnSc4d1
jedince	jedinec	k1gMnSc4
nebo	nebo	k8xC
odebrání	odebrání	k1gNnSc4
vzorku	vzorek	k1gInSc2
tkáně	tkáň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
tak	tak	k6eAd1
jít	jít	k5eAaImF
o	o	k7c4
medvěda	medvěd	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
existenci	existence	k1gFnSc4
yettiho	yetti	k1gMnSc2
připustíme	připustit	k5eAaPmIp1nP
<g/>
,	,	kIx,
mohlo	moct	k5eAaImAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
například	například	k6eAd1
o	o	k7c4
Gigantopitheca	Gigantopithecus	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
velkého	velký	k2eAgMnSc4d1
lidoopa	lidoop	k1gMnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc4
kosterní	kosterní	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc1
(	(	kIx(
<g/>
především	především	k6eAd1
části	část	k1gFnPc1
čelistí	čelist	k1gFnPc2
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
nalezeny	naleznout	k5eAaPmNgFnP,k5eAaBmNgFnP
v	v	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vyhynul	vyhynout	k5eAaPmAgInS
asi	asi	k9
před	před	k7c7
100.000	100.000	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zástupce	zástupce	k1gMnSc1
druhu	druh	k1gInSc2
Homo	Homo	k1gMnSc1
erectus	erectus	k1gMnSc1
neboli	neboli	k8xC
Pithecantropus	Pithecantropus	k1gMnSc1
erectus	erectus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
v	v	k7c6
evolučním	evoluční	k2eAgInSc6d1
postupu	postup	k1gInSc6
oddělil	oddělit	k5eAaPmAgInS
od	od	k7c2
linie	linie	k1gFnSc2
následného	následný	k2eAgNnSc2d1
Homo	Homo	k6eAd1
sapiens	sapiens	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
vyskytoval	vyskytovat	k5eAaImAgMnS
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
Asii	Asie	k1gFnSc6
i	i	k8xC
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
jeho	jeho	k3xOp3gInSc1
popis	popis	k1gInSc1
se	se	k3xPyFc4
yettimu	yetti	k1gMnSc3
příliš	příliš	k6eAd1
nepodobá	podobat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
jsou	být	k5eAaImIp3nP
denisované	denisovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc2
DNA	dno	k1gNnSc2
mají	mít	k5eAaImIp3nP
i	i	k9
současní	současný	k2eAgMnPc1d1
Tibeťané	Tibeťan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
dosud	dosud	k6eAd1
zcela	zcela	k6eAd1
neznámý	známý	k2eNgInSc1d1
druh	druh	k1gInSc1
živočicha	živočich	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podobní	podobný	k2eAgMnPc1d1
tvorové	tvor	k1gMnPc1
</s>
<s>
Zprávy	zpráva	k1gFnPc1
o	o	k7c6
podobných	podobný	k2eAgFnPc6d1
tvorech	tvor	k1gMnPc6
s	s	k7c7
různými	různý	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
mnoha	mnoho	k4c2
končin	končina	k1gFnPc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
odlehlých	odlehlý	k2eAgNnPc2d1
<g/>
,	,	kIx,
nepřístupných	přístupný	k2eNgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
na	na	k7c6
území	území	k1gNnSc6
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgMnSc1d1
tvor	tvor	k1gMnSc1
známý	známý	k1gMnSc1
jako	jako	k8xS,k8xC
bigfoot	bigfoot	k1gMnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gInSc1
popis	popis	k1gInSc1
se	se	k3xPyFc4
od	od	k7c2
yettiho	yetti	k1gMnSc2
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
vlastnostech	vlastnost	k1gFnPc6
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
velikost	velikost	k1gFnSc4
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
od	od	k7c2
180	#num#	k4
cm	cm	kA
po	po	k7c4
250	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
barva	barva	k1gFnSc1
srsti	srst	k1gFnSc2
od	od	k7c2
šedé	šedá	k1gFnSc2
přes	přes	k7c4
černou	černá	k1gFnSc4
až	až	k9
po	po	k7c4
červenou	červená	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
tvor	tvor	k1gMnSc1
se	se	k3xPyFc4
údajně	údajně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
(	(	kIx(
<g/>
yowie	yowie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
jeren	jeren	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
(	(	kIx(
<g/>
kaptar	kaptar	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pozorování	pozorování	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
existence	existence	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
především	především	k9
na	na	k7c4
vyprávění	vyprávění	k1gNnPc4
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
okolí	okolí	k1gNnSc2
himálajského	himálajský	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zprávy	zpráva	k1gFnPc1
o	o	k7c6
tomto	tento	k3xDgMnSc6
tvoru	tvor	k1gMnSc6
vyprávějí	vyprávět	k5eAaImIp3nP
již	již	k6eAd1
po	po	k7c4
mnoho	mnoho	k4c4
generací	generace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
evropskou	evropský	k2eAgFnSc4d1
civilizaci	civilizace	k1gFnSc4
ho	on	k3xPp3gNnSc2
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
anglický	anglický	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
Laurence	Laurence	k1gFnSc2
Waddell	Waddell	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c6
jednom	jeden	k4xCgNnSc6
himálajském	himálajský	k2eAgNnSc6d1
sedle	sedlo	k1gNnSc6
objevil	objevit	k5eAaPmAgInS
ve	v	k7c6
sněhu	sníh	k1gInSc6
obrovský	obrovský	k2eAgInSc4d1
otisk	otisk	k1gInSc4
bosého	bosý	k2eAgNnSc2d1
chlupatého	chlupatý	k2eAgNnSc2d1
chodidla	chodidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
fotografie	fotografia	k1gFnPc1
yettiho	yetti	k1gMnSc4
stop	stop	k1gInSc1
pořídil	pořídit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
Frank	Frank	k1gMnSc1
Smythe	Smyth	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
na	na	k7c4
důkaz	důkaz	k1gInSc4
nadměrné	nadměrný	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
k	k	k7c3
otisku	otisk	k1gInSc3
přiložil	přiložit	k5eAaPmAgMnS
horolezecký	horolezecký	k2eAgInSc4d1
cepín	cepín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
odborníků	odborník	k1gMnPc2
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
poškozené	poškozený	k1gMnPc4
či	či	k8xC
deformované	deformovaný	k2eAgInPc1d1
otisky	otisk	k1gInPc1
jiných	jiný	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jiní	jiný	k2eAgMnPc1d1
odborníci	odborník	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
jednat	jednat	k5eAaImF
o	o	k7c4
tvora	tvor	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
prozatím	prozatím	k6eAd1
není	být	k5eNaImIp3nS
odborně	odborně	k6eAd1
popsán	popsat	k5eAaPmNgInS
<g/>
,	,	kIx,
ale	ale	k8xC
jehož	jehož	k3xOyRp3gFnSc1
existence	existence	k1gFnSc1
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
sádrové	sádrový	k2eAgInPc1d1
odlitky	odlitek	k1gInPc1
stop	stopa	k1gFnPc2
udělali	udělat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
američtí	americký	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
Jeffrey	Jeffrea	k1gFnSc2
McNeely	McNeela	k1gFnSc2
a	a	k8xC
Edward	Edward	k1gMnSc1
Cronin	Cronina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgInPc1
typy	typ	k1gInPc1
yettiho	yetti	k1gMnSc2
</s>
<s>
Pro	pro	k7c4
himálajské	himálajský	k2eAgMnPc4d1
domorodce	domorodec	k1gMnPc4
věřící	věřící	k2eAgMnPc4d1
v	v	k7c6
existenci	existence	k1gFnSc6
yettiho	yetti	k1gMnSc2
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
více	hodně	k6eAd2
typů	typ	k1gInPc2
<g/>
:	:	kIx,
menší	malý	k2eAgInSc1d2
yeh-the	yeh-the	k1gInSc1
<g/>
,	,	kIx,
velký	velký	k2eAgInSc1d1
meh-the	meh-the	k1gInSc1
a	a	k8xC
obrovský	obrovský	k2eAgInSc1d1
dzu-the	dzu-the	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozlišení	rozlišení	k1gNnSc1
do	do	k7c2
tří	tři	k4xCgFnPc2
velikostí	velikost	k1gFnPc2
by	by	kYmCp3nS
vysvětlovalo	vysvětlovat	k5eAaImAgNnS
i	i	k9
odlišnou	odlišný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
stopy	stopa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
menších	malý	k2eAgInPc2d2
a	a	k8xC
středně	středně	k6eAd1
velkých	velký	k2eAgFnPc2d1
stop	stopa	k1gFnPc2
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
druhy	druh	k1gInPc4
opic	opice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
také	také	k9
považovány	považován	k2eAgInPc1d1
za	za	k7c2
yettiho	yetti	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
yetti	yetti	k1gMnSc1
skutečně	skutečně	k6eAd1
existuje	existovat	k5eAaImIp3nS
<g/>
,	,	kIx,
pak	pak	k6eAd1
zcela	zcela	k6eAd1
jistě	jistě	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
gigantopitéka	gigantopitéko	k1gNnSc2
<g/>
,	,	kIx,
vyhynulé	vyhynulý	k2eAgFnSc2d1
obří	obří	k2eAgFnSc2d1
opice	opice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
před	před	k7c4
půl	půl	k1xP
milionem	milion	k4xCgInSc7
let	léto	k1gNnPc2
vymřel	vymřít	k5eAaPmAgInS
<g/>
,	,	kIx,
vyrostl	vyrůst	k5eAaPmAgInS
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc4
právě	právě	k9
o	o	k7c4
500	#num#	k4
m.	m.	k?
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tak	tak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
izolaci	izolace	k1gFnSc3
mnoha	mnoho	k4c2
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
Yetti	yetti	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
postupně	postupně	k6eAd1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
světových	světový	k2eAgMnPc2d1
kryptidů	kryptid	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
o	o	k7c6
tomto	tento	k3xDgNnSc6
domnělém	domnělý	k2eAgNnSc6d1
primátovi	primát	k1gMnSc3
psal	psát	k5eAaImAgMnS
například	například	k6eAd1
Jaroslav	Jaroslav	k1gMnSc1
Mareš	Mareš	k1gMnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MAREŠ	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendární	legendární	k2eAgMnPc1d1
opolidé	opočlověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Motto	motto	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7246	#num#	k4
<g/>
-	-	kIx~
<g/>
375	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
86	#num#	k4
<g/>
-	-	kIx~
<g/>
87	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://phys.org/news/2017-11-abominable-snowman-nopestudy-ties-dna.html	https://phys.org/news/2017-11-abominable-snowman-nopestudy-ties-dna.html	k1gMnSc1
-	-	kIx~
Abominable	Abominable	k1gMnSc1
Snowman	Snowman	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nope	nop	k1gInSc5
<g/>
—	—	k?
<g/>
study	stud	k1gInPc4
ties	ties	k6eAd1
DNA	DNA	kA
samples	samplesa	k1gFnPc2
from	from	k1gMnSc1
purported	purported	k1gMnSc1
Yetis	Yetis	k1gFnSc2
to	ten	k3xDgNnSc4
Asian	Asian	k1gInSc1
bears	bears	k6eAd1
<g/>
↑	↑	k?
MAREŠ	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendární	legendární	k2eAgMnPc1d1
opolidé	opočlověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Motto	motto	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7246	#num#	k4
<g/>
-	-	kIx~
<g/>
375	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
86	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GENZMER	GENZMER	kA
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
<g/>
,	,	kIx,
Největší	veliký	k2eAgFnPc4d3
záhady	záhada	k1gFnPc4
světa	svět	k1gInSc2
<g/>
↑	↑	k?
https://dinosaurusblog.com/2021/05/07/vzpominka-na-jaroslava-marese/	https://dinosaurusblog.com/2021/05/07/vzpominka-na-jaroslava-marese/	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kryptozoologie	Kryptozoologie	k1gFnSc1
</s>
<s>
Hominidé	hominid	k1gMnPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MAREŠ	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendární	legendární	k2eAgMnPc1d1
opolidé	opočlověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Motto	motto	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7246	#num#	k4
<g/>
-	-	kIx~
<g/>
375	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MESSNER	MESSNER	kA
<g/>
,	,	kIx,
Reinhold	Reinhold	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yetti	yetti	k1gMnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
a	a	k8xC
skutečnost	skutečnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jaromír	Jaromír	k1gMnSc1
Povejšil	Povejšil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Mladá	mladá	k1gFnSc1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
188	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Kolumbus	Kolumbus	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
145	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
791	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Yetti	yetti	k1gMnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Článek	článek	k1gInSc1
v	v	k7c6
časopise	časopis	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Názor	názor	k1gInSc1
Reinholda	Reinhold	k1gMnSc2
Messnera	Messner	k1gMnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4309812-5	4309812-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85149134	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85149134	#num#	k4
</s>
