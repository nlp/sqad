<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
neznámý	známý	k2eNgInSc1d1
druh	druh	k1gInSc1
dvounohého	dvounohý	k2eAgMnSc2d1
hominida	hominid	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
údajně	údajně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
Himálajích	Himálaj	k1gFnPc6
<g/>
?	?	kIx.
</s>