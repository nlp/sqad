<s>
Yetti	yetti	k1gMnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
neznámého	známý	k2eNgInSc2d1
druhu	druh	k1gInSc2
dvounohého	dvounohý	k2eAgMnSc4d1
hominida	hominid	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
údajně	údajně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Himálají	Himálaj	k1gFnPc2
<g/>
.	.	kIx.
</s>