<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
došlo	dojít	k5eAaPmAgNnS	dojít
vládním	vládní	k2eAgMnPc3d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozšíření	rozšíření	k1gNnSc3	rozšíření
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
Maloskalska	Maloskalsk	k1gInSc2	Maloskalsk
a	a	k8xC	a
Prachovských	prachovský	k2eAgFnPc2d1	Prachovská
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
území	území	k1gNnSc4	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
cca	cca	kA	cca
181,5	[number]	k4	181,5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc6	výročí
byl	být	k5eAaImAgMnS	být
Českému	český	k2eAgInSc3d1	český
ráji	ráj	k1gInSc6	ráj
přidělen	přidělit	k5eAaPmNgInS	přidělit
status	status	k1gInSc1	status
Globální	globální	k2eAgInSc1d1	globální
geopark	geopark	k1gInSc1	geopark
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
CHKO	CHKO	kA	CHKO
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
tří	tři	k4xCgInPc2	tři
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
čtyř	čtyři	k4xCgInPc2	čtyři
okresů	okres	k1gInPc2	okres
-	-	kIx~	-
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Jičín	Jičín	k1gInSc1	Jičín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc1	okres
Semily	Semily	k1gInPc1	Semily
a	a	k8xC	a
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
<g/>
.	.	kIx.	.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
leží	ležet	k5eAaImIp3nS	ležet
uvnitř	uvnitř	k6eAd1	uvnitř
šířeji	šířej	k1gInPc7	šířej
pojaté	pojatý	k2eAgFnSc2d1	pojatá
oblasti	oblast	k1gFnSc2	oblast
turistického	turistický	k2eAgInSc2d1	turistický
regionu	region	k1gInSc2	region
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
samostatných	samostatný	k2eAgFnPc2d1	samostatná
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
Mnichovým	mnichův	k2eAgNnSc7d1	Mnichovo
Hradištěm	Hradiště	k1gNnSc7	Hradiště
<g/>
,	,	kIx,	,
Turnovem	Turnov	k1gInSc7	Turnov
a	a	k8xC	a
Sobotkou	Sobotka	k1gFnSc7	Sobotka
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
oblast	oblast	k1gFnSc1	oblast
Mužského	mužský	k2eAgNnSc2d1	mužské
a	a	k8xC	a
Příhrazských	Příhrazský	k2eAgFnPc2d1	Příhrazský
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
Hruboskalsko	Hruboskalsko	k1gNnSc1	Hruboskalsko
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
Trosek	troska	k1gFnPc2	troska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Turnovem	Turnov	k1gInSc7	Turnov
<g/>
,	,	kIx,	,
Malou	malý	k2eAgFnSc7d1	malá
skálou	skála	k1gFnSc7	skála
a	a	k8xC	a
Kozákovem	Kozákov	k1gInSc7	Kozákov
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Maloskalska	Maloskalska	k1gFnSc1	Maloskalska
<g/>
,	,	kIx,	,
Suchých	Suchých	k2eAgFnPc2d1	Suchých
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
vrchu	vrch	k1gInSc2	vrch
Kozákova	Kozákův	k2eAgInSc2d1	Kozákův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
Jičínem	Jičín	k1gInSc7	Jičín
a	a	k8xC	a
Mladějovem	Mladějov	k1gInSc7	Mladějov
(	(	kIx(	(
<g/>
sem	sem	k6eAd1	sem
spadá	spadat	k5eAaPmIp3nS	spadat
oblast	oblast	k1gFnSc4	oblast
Prachovských	prachovský	k2eAgFnPc2d1	Prachovská
skal	skála	k1gFnPc2	skála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
podloží	podloží	k1gNnSc2	podloží
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
kvádrové	kvádrový	k2eAgInPc1d1	kvádrový
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tu	ten	k3xDgFnSc4	ten
byly	být	k5eAaImAgInP	být
uloženy	uložen	k2eAgInPc1d1	uložen
v	v	k7c6	v
období	období	k1gNnSc6	období
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
terciéru	terciér	k1gInSc2	terciér
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
aktivní	aktivní	k2eAgFnSc1d1	aktivní
sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tu	tu	k6eAd1	tu
mnoho	mnoho	k4c4	mnoho
podpovrchových	podpovrchový	k2eAgNnPc2d1	podpovrchové
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgInPc1d1	okolní
křídové	křídový	k2eAgInPc1d1	křídový
sedimenty	sediment	k1gInPc1	sediment
byly	být	k5eAaImAgInP	být
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
rozrušeny	rozrušit	k5eAaPmNgInP	rozrušit
a	a	k8xC	a
odneseny	odnést	k5eAaPmNgInP	odnést
řekami	řeka	k1gFnPc7	řeka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
odolné	odolný	k2eAgFnPc1d1	odolná
než	než	k8xS	než
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
magmatické	magmatický	k2eAgFnPc1d1	magmatická
horniny	hornina	k1gFnPc1	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
nejznámější	známý	k2eAgInSc1d3	nejznámější
kopec	kopec	k1gInSc1	kopec
a	a	k8xC	a
dominanta	dominanta	k1gFnSc1	dominanta
Českého	český	k2eAgInSc2d1	český
ráje	ráj	k1gInSc2	ráj
–	–	k?	–
Trosky	troska	k1gFnSc2	troska
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
Kozákov	Kozákov	k1gInSc1	Kozákov
(	(	kIx(	(
<g/>
744	[number]	k4	744
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známým	známý	k2eAgNnSc7d1	známé
nalezištěm	naleziště	k1gNnSc7	naleziště
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
odrůd	odrůda	k1gFnPc2	odrůda
křemene	křemen	k1gInSc2	křemen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
CHKO	CHKO	kA	CHKO
jsou	být	k5eAaImIp3nP	být
evidovány	evidován	k2eAgFnPc1d1	evidována
stovky	stovka	k1gFnPc1	stovka
jeskyní	jeskyně	k1gFnPc2	jeskyně
a	a	k8xC	a
skalních	skalní	k2eAgFnPc2d1	skalní
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Klokočské	Klokočský	k2eAgFnSc2d1	Klokočský
skály	skála	k1gFnSc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
jeskyně	jeskyně	k1gFnPc1	jeskyně
v	v	k7c6	v
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
uzavřeny	uzavřít	k5eAaPmNgInP	uzavřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ochrany	ochrana	k1gFnSc2	ochrana
vrápence	vrápenec	k1gMnSc2	vrápenec
malého	malý	k1gMnSc2	malý
(	(	kIx(	(
<g/>
Sklepy	sklep	k1gInPc1	sklep
pod	pod	k7c7	pod
Troskami	troska	k1gFnPc7	troska
a	a	k8xC	a
Krtola	Krtola	k1gFnSc1	Krtola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
tesaná	tesaný	k2eAgFnSc1d1	tesaná
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
štola	štola	k1gFnSc1	štola
na	na	k7c4	na
úpatí	úpatí	k1gNnPc4	úpatí
Kozákova	Kozákův	k2eAgFnSc1d1	Kozákova
<g/>
.	.	kIx.	.
</s>
<s>
PR	pr	k0	pr
Apolena	Apolena	k1gFnSc1	Apolena
PR	pr	k0	pr
Bažantník	bažantník	k1gMnSc1	bažantník
PR	pr	k0	pr
Bučiny	bučina	k1gFnPc1	bučina
u	u	k7c2	u
Rakous	Rakousy	k1gInPc2	Rakousy
PR	pr	k0	pr
Hruboskalsko	Hruboskalska	k1gFnSc5	Hruboskalska
PP	PP	kA	PP
Libunecké	Libunecký	k2eAgNnSc1d1	Libunecký
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
PP	PP	kA	PP
Libuňka	Libuňka	k1gFnSc1	Libuňka
PR	pr	k0	pr
Klokočské	Klokočský	k2eAgFnSc2d1	Klokočský
skály	skála	k1gFnSc2	skála
NPP	NPP	kA	NPP
Kozákov	Kozákov	k1gInSc1	Kozákov
PP	PP	kA	PP
Meziluží	Meziluží	k1gMnSc2	Meziluží
PR	pr	k0	pr
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
PP	PP	kA	PP
Na	na	k7c6	na
Vápenici	vápenice	k1gFnSc6	vápenice
PP	PP	kA	PP
Oborská	Oborský	k2eAgFnSc1d1	Oborská
luka	luka	k1gNnPc1	luka
PP	PP	kA	PP
Ondříkovický	Ondříkovický	k2eAgInSc1d1	Ondříkovický
pseudokrasový	pseudokrasový	k2eAgInSc1d1	pseudokrasový
systém	systém	k1gInSc1	systém
PR	pr	k0	pr
Podtrosecká	Podtrosecký	k2eAgFnSc1d1	Podtrosecký
údolí	údolí	k1gNnPc1	údolí
PR	pr	k0	pr
Prachovské	prachovský	k2eAgFnPc1d1	Prachovská
skály	skála	k1gFnPc1	skála
PR	pr	k0	pr
Příhrazské	Příhrazský	k2eAgFnSc2d1	Příhrazský
skály	skála	k1gFnSc2	skála
PP	PP	kA	PP
Rybník	rybník	k1gInSc1	rybník
Vražda	vražda	k1gFnSc1	vražda
NPP	NPP	kA	NPP
Suché	Suché	k2eAgFnSc2d1	Suché
skály	skála	k1gFnSc2	skála
<g />
.	.	kIx.	.
</s>
<s>
PP	PP	kA	PP
Tachovský	tachovský	k2eAgInSc1d1	tachovský
vodopád	vodopád	k1gInSc1	vodopád
PP	PP	kA	PP
Trosky	troska	k1gFnSc2	troska
PR	pr	k0	pr
Údolí	údolí	k1gNnSc6	údolí
Plakánek	Plakánek	k1gMnSc1	Plakánek
PP	PP	kA	PP
V	v	k7c6	v
Dubech	dub	k1gInPc6	dub
PP	PP	kA	PP
Vústra	Vústr	k1gMnSc2	Vústr
PR	pr	k0	pr
Žabakor	Žabakor	k1gInSc4	Žabakor
Ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
CHKO	CHKO	kA	CHKO
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dvě	dva	k4xCgNnPc1	dva
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
mimo	mimo	k7c4	mimo
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
oblast	oblast	k1gFnSc4	oblast
<g/>
:	:	kIx,	:
NPP	NPP	kA	NPP
Bozkovské	Bozkovský	k2eAgFnSc2d1	Bozkovská
dolomitové	dolomitový	k2eAgFnSc2d1	dolomitová
jeskyně	jeskyně	k1gFnSc2	jeskyně
NPP	NPP	kA	NPP
Strážník	strážník	k1gMnSc1	strážník
Český	český	k2eAgMnSc1d1	český
ráj	ráj	k1gInSc4	ráj
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Český	český	k2eAgMnSc1d1	český
ráj	ráj	k1gInSc4	ráj
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
