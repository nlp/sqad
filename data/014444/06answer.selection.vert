<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
TČ	tč	kA
<g/>
,	,	kIx,
název	název	k1gInSc1
dle	dle	k7c2
VIVC	VIVC	kA
Traminer	Traminra	k1gInSc1
Rot	rota	k2eAgInSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
starobylá	starobylý	k2eAgFnSc1d1
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k2eAgFnSc1d1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
bílých	bílý	k2eAgFnPc2d1
vín	vína	k1gFnPc2
<g/>
.	.	kIx.
</s>