<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
</s>
<s>
Keř	keř	k1gInSc4
odrůdy	odrůda	k1gFnSc2
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc4
z	z	k7c2
publikace	publikace	k1gFnSc2
Ampélographie	Ampélographie	k1gFnSc2
<g/>
,	,	kIx,
Viala	Viala	k1gMnSc1
et	et	k?
Vermorel	Vermorel	k1gMnSc1
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
TČ	tč	kA
<g/>
,	,	kIx,
název	název	k1gInSc1
dle	dle	k7c2
VIVC	VIVC	kA
Traminer	Traminra	k1gInSc1
Rot	rota	k2eAgInSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
starobylá	starobylý	k2eAgFnSc1d1
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k2eAgFnSc1d1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
bílých	bílý	k2eAgFnPc2d1
vín	vína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
pěstovaných	pěstovaný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
podílela	podílet	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
vzniku	vznik	k1gInSc2
mnoha	mnoho	k4c2
klasických	klasický	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
oblíbeným	oblíbený	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
při	při	k7c6
šlechtění	šlechtění	k1gNnSc6
nových	nový	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
odrůdy	odrůda	k1gFnSc2
je	být	k5eAaImIp3nS
nejasný	jasný	k2eNgInSc1d1
a	a	k8xC
ztrácí	ztrácet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
dávné	dávný	k2eAgFnSc6d1
minulosti	minulost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tramín	tramín	k1gInSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
skupiny	skupina	k1gFnSc2
západoevropských	západoevropský	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
(	(	kIx(
<g/>
Proles	Proles	k1gMnSc1
occidentalis	occidentalis	k1gFnSc2
Negr	negr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
menším	malý	k2eAgInSc7d2
hroznem	hrozen	k1gInSc7
i	i	k8xC
bobulemi	bobule	k1gFnPc7
<g/>
,	,	kIx,
slabším	slabý	k2eAgInSc7d2
růstem	růst	k1gInSc7
a	a	k8xC
tenčím	tenký	k2eAgNnSc7d2
révím	réví	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
dobře	dobře	k6eAd1
vyzrává	vyzrávat	k5eAaImIp3nS
a	a	k8xC
odolává	odolávat	k5eAaImIp3nS
mrazu	mráz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
i	i	k9
vína	víno	k1gNnPc4
vysoké	vysoký	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
kořenitých	kořenitý	k2eAgInPc2d1
a	a	k8xC
vonných	vonný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tramín	tramín	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
též	též	k9
Savagnin	Savagnin	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
nazývána	nazýván	k2eAgFnSc1d1
v	v	k7c6
regionech	region	k1gInPc6
Jura	jura	k1gFnSc1
a	a	k8xC
Franche-Comté	Franche-Comta	k1gMnPc1
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
s	s	k7c7
velmi	velmi	k6eAd1
dlouhou	dlouhý	k2eAgFnSc7d1
historií	historie	k1gFnSc7
a	a	k8xC
navíc	navíc	k6eAd1
odrůda	odrůda	k1gFnSc1
se	s	k7c7
značným	značný	k2eAgInSc7d1
sklonem	sklon	k1gInSc7
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
mutací	mutace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
u	u	k7c2
každé	každý	k3xTgFnSc2
takové	takový	k3xDgFnSc2
odrůdy	odrůda	k1gFnSc2
(	(	kIx(
<g/>
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
můžeme	moct	k5eAaImIp1nP
například	například	k6eAd1
odrůdy	odrůda	k1gFnPc4
Pinot	Pinota	k1gFnPc2
<g/>
,	,	kIx,
Trebbiano	Trebbiana	k1gFnSc5
Toscano	Toscana	k1gFnSc5
či	či	k8xC
Gouais	Gouais	k1gFnPc6
blanc	blanc	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
u	u	k7c2
ní	on	k3xPp3gFnSc2
během	během	k7c2
staletí	staletí	k1gNnPc2
vegetativní	vegetativní	k2eAgFnSc2d1
reprodukce	reprodukce	k1gFnSc2
vyvinulo	vyvinout	k5eAaPmAgNnS
několik	několik	k4yIc1
různých	různý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
(	(	kIx(
<g/>
mutací	mutace	k1gFnPc2
<g/>
,	,	kIx,
klonů	klon	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozdílných	rozdílný	k2eAgFnPc2d1
co	co	k9
barvy	barva	k1gFnPc1
bobulí	bobule	k1gFnPc2
<g/>
,	,	kIx,
aroma	aroma	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
tvaru	tvar	k1gInSc2
a	a	k8xC
rozměru	rozměr	k1gInSc2
listů	list	k1gInPc2
<g/>
,	,	kIx,
hroznů	hrozen	k1gInPc2
atd.	atd.	kA
Některé	některý	k3yIgFnPc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
forem	forma	k1gFnPc2
máme	mít	k5eAaImIp1nP
dnes	dnes	k6eAd1
sklon	sklon	k1gInSc4
považovat	považovat	k5eAaImF
za	za	k7c4
samostatné	samostatný	k2eAgFnPc4d1
odrůdy	odrůda	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Gewürztraminer	Gewürztraminer	k1gInSc1
v	v	k7c6
Alsasku	Alsasko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
italském	italský	k2eAgInSc6d1
regionu	region	k1gInSc6
Südtirol	Südtirol	k1gInSc1
<g/>
/	/	kIx~
<g/>
Trentino-Alto	Trentino-Alto	k1gNnSc1
Adige	Adig	k1gInSc2
a	a	k8xC
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Heida	Heida	k1gFnSc1
či	či	k8xC
Paï	Paï	k1gInSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
pochází	pocházet	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
Tramín	tramín	k1gInSc4
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
pěstovaná	pěstovaný	k2eAgFnSc1d1
za	za	k7c4
hraběte	hrabě	k1gMnSc4
Šporka	Šporek	k1gMnSc4
na	na	k7c6
zámku	zámek	k1gInSc6
Kuks	Kuks	k1gInSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Brynšt	Brynšta	k1gFnPc2
a	a	k8xC
dnes	dnes	k6eAd1
zde	zde	k6eAd1
opět	opět	k6eAd1
na	na	k7c6
malé	malý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
vysazená	vysazený	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Traminer	Traminer	k1gMnSc1
či	či	k8xC
Traminer	Traminer	k1gMnSc1
Weisser	Weisser	k1gMnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Traminer	Traminer	k1gMnSc1
Aromatico	Aromatico	k1gMnSc1
v	v	k7c6
regionu	region	k1gInSc6
Südtirol	Südtirola	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Trentino-Alto	Trentino-Alt	k2eAgNnSc1d1
Adige	Adige	k1gNnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analýza	analýza	k1gFnSc1
DNA	DNA	kA
prokázala	prokázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgFnPc1
tyto	tento	k3xDgFnPc1
formy	forma	k1gFnPc1
mají	mít	k5eAaImIp3nP
stejný	stejný	k2eAgInSc4d1
genetický	genetický	k2eAgInSc4d1
základ	základ	k1gInSc4
s	s	k7c7
pouze	pouze	k6eAd1
nepatrnými	nepatrný	k2eAgInPc7d1,k2eNgInPc7d1
rozdíly	rozdíl	k1gInPc7
na	na	k7c6
úrovni	úroveň	k1gFnSc6
klonů	klon	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přes	přes	k7c4
výše	výše	k1gFnPc4,k1gFnPc4wB
uvedené	uvedený	k2eAgFnPc4d1
<g/>
,	,	kIx,
a	a	k8xC
nebo	nebo	k8xC
právě	právě	k9
proto	proto	k8xC
<g/>
,	,	kIx,
nám	my	k3xPp1nPc3
nic	nic	k3yNnSc1
nebrání	bránit	k5eNaImIp3nS
<g/>
,	,	kIx,
rozlišovat	rozlišovat	k5eAaImF
po	po	k7c6
vzoru	vzor	k1gInSc6
odrůd	odrůda	k1gFnPc2
Pinot	Pinota	k1gFnPc2
tradičně	tradičně	k6eAd1
alespoň	alespoň	k9
odrůdy	odrůda	k1gFnSc2
Tramín	tramín	k1gInSc1
bílý	bílý	k2eAgInSc1d1
a	a	k8xC
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
mimochodem	mimochodem	k9
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
"	"	kIx"
<g/>
Catalogue	Catalogue	k1gNnSc6
des	des	k1gNnSc2
variétés	variétésa	k1gFnPc2
de	de	k?
vigne	vignout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
"	"	kIx"
zapsány	zapsán	k2eAgFnPc1d1
samostatně	samostatně	k6eAd1
odrůdy	odrůda	k1gFnPc4
tři	tři	k4xCgFnPc4
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
Gewürztraminer	Gewürztraminer	k1gInSc1
<g/>
,	,	kIx,
Savagnin	Savagnin	k1gInSc1
Rosé	Rosá	k1gFnSc2
a	a	k8xC
Savagnin	Savagnin	k2eAgMnSc1d1
Blanc	Blanc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Hrozen	hrozen	k1gInSc4
odrůdy	odrůda	k1gFnSc2
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
</s>
<s>
Réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
odrůda	odrůda	k1gFnSc1
Tramín	tramín	k1gInSc4
červený	červený	k2eAgInSc1d1
je	on	k3xPp3gFnPc4
jednodomá	jednodomý	k2eAgFnSc1d1
dřevitá	dřevitý	k2eAgFnSc1d1
pnoucí	pnoucí	k2eAgFnSc1d1
liána	liána	k1gFnSc1
<g/>
,	,	kIx,
dorůstající	dorůstající	k2eAgFnSc1d1
v	v	k7c6
kultuře	kultura	k1gFnSc6
až	až	k9
několika	několik	k4yIc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kmen	kmen	k1gInSc4
tloušťky	tloušťka	k1gFnSc2
až	až	k9
několik	několik	k4yIc1
centimetrů	centimetr	k1gInPc2
je	být	k5eAaImIp3nS
pokryt	pokrýt	k5eAaPmNgInS
světlou	světlý	k2eAgFnSc7d1
borkou	borka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
loupe	loupat	k5eAaImIp3nS
v	v	k7c6
pruzích	pruh	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Úponky	úponka	k1gFnPc1
révy	réva	k1gFnSc2
jsou	být	k5eAaImIp3nP
kratší	krátký	k2eAgInPc1d2
až	až	k9
středně	středně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
umožňují	umožňovat	k5eAaImIp3nP
této	tento	k3xDgFnSc3
rostlině	rostlina	k1gFnSc3
pnout	pnout	k5eAaImF
se	se	k3xPyFc4
po	po	k7c6
pevných	pevný	k2eAgInPc6d1
předmětech	předmět	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
bujný	bujný	k2eAgInSc1d1
až	až	k6eAd1
bujný	bujný	k2eAgInSc1d1
s	s	k7c7
polovzpřímenými	polovzpřímený	k2eAgInPc7d1
letorosty	letorost	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholky	vrcholek	k1gInPc1
letorostů	letorost	k1gInPc2
jsou	být	k5eAaImIp3nP
otevřené	otevřený	k2eAgFnPc1d1
<g/>
,	,	kIx,
bělavě	bělavě	k6eAd1
zelené	zelený	k2eAgInPc1d1
<g/>
,	,	kIx,
slabě	slabě	k6eAd1
pigmentované	pigmentovaný	k2eAgFnSc2d1
antokyaniny	antokyanina	k1gFnSc2
<g/>
,	,	kIx,
silně	silně	k6eAd1
bíle	bíle	k6eAd1
vlnatě	vlnatě	k6eAd1
ochmýřené	ochmýřený	k2eAgFnPc1d1
až	až	k8xS
plstnaté	plstnatý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladé	mladý	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
zelené	zelený	k2eAgInPc1d1
s	s	k7c7
bronzovými	bronzový	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
<g/>
,	,	kIx,
slabě	slabě	k6eAd1
pigmentované	pigmentovaný	k2eAgFnPc1d1
antokyaniny	antokyanina	k1gFnPc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
narůžovělý	narůžovělý	k2eAgInSc4d1
nádech	nádech	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internodia	internodium	k1gNnPc1
jsou	být	k5eAaImIp3nP
krátká	krátký	k2eAgNnPc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
nodia	nodia	k1gFnSc1
jsou	být	k5eAaImIp3nP
zelená	zelené	k1gNnPc1
<g/>
,	,	kIx,
na	na	k7c6
osluněné	osluněný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
někdy	někdy	k6eAd1
hnědočerveně	hnědočerveně	k6eAd1
proužkovaná	proužkovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
ochmýření	ochmýření	k1gNnSc2
<g/>
,	,	kIx,
pupeny	pupen	k1gInPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
slabě	slabě	k6eAd1
pigmentované	pigmentovaný	k2eAgFnPc4d1
antokyaniny	antokyanina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olistění	olistěný	k2eAgMnPc5d1
je	on	k3xPp3gMnPc4
bohaté	bohatý	k2eAgNnSc1d1
<g/>
,	,	kIx,
listová	listový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
zahuštěná	zahuštěný	k2eAgFnSc1d1
množstvím	množství	k1gNnSc7
zálistků	zálistek	k1gInPc2
<g/>
.	.	kIx.
jednoleté	jednoletý	k2eAgNnSc1d1
réví	réví	k1gNnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
tenké	tenký	k2eAgNnSc1d1
<g/>
,	,	kIx,
eliptického	eliptický	k2eAgInSc2d1
průřezu	průřez	k1gInSc2
<g/>
,	,	kIx,
červenavě	červenavě	k6eAd1
až	až	k9
skořicově	skořicově	k6eAd1
hnědé	hnědý	k2eAgNnSc4d1
<g/>
,	,	kIx,
čárkované	čárkovaný	k2eAgNnSc4d1
i	i	k8xC
tečkované	tečkovaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimní	zimní	k2eAgInPc1d1
pupeny	pupen	k1gInPc1
jsou	být	k5eAaImIp3nP
malé	malý	k2eAgInPc1d1
<g/>
,	,	kIx,
širší	široký	k2eAgInPc1d2
<g/>
,	,	kIx,
hrotité	hrotitý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
List	list	k1gInSc1
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
až	až	k9
středně	středně	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
okrouhlý	okrouhlý	k2eAgInSc1d1
<g/>
,	,	kIx,
tří-	tří-	k?
až	až	k8xS
pětilaločnatý	pětilaločnatý	k2eAgInSc4d1
<g/>
,	,	kIx,
málo	málo	k6eAd1
členěný	členěný	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
mělkými	mělký	k2eAgInPc7d1
horními	horní	k2eAgInPc7d1
bočními	boční	k2eAgInPc7d1
výkroji	výkroj	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
otevřené	otevřený	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
oblým	oblý	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchní	vrchní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
čepele	čepel	k1gFnSc2
listu	list	k1gInSc2
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
puchýřnatá	puchýřnatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
drsnější	drsný	k2eAgFnSc1d2
<g/>
,	,	kIx,
mírně	mírně	k6eAd1
vrásčitá	vrásčitý	k2eAgFnSc1d1
a	a	k8xC
zvlněná	zvlněný	k2eAgFnSc1d1
<g/>
,	,	kIx,
tmavě	tmavě	k6eAd1
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
spodní	spodní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
jemně	jemně	k6eAd1
plstnatá	plstnatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
zoubkování	zoubkování	k1gNnSc1
na	na	k7c6
okraji	okraj	k1gInSc6
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
výrazné	výrazný	k2eAgNnSc1d1
<g/>
,	,	kIx,
konvexní	konvexní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řapíkový	řapíkový	k2eAgInSc1d1
výkroj	výkroj	k1gInSc1
je	být	k5eAaImIp3nS
buď	buď	k8xC
úzce	úzko	k6eAd1
otevřený	otevřený	k2eAgInSc1d1
<g/>
,	,	kIx,
klínovitý	klínovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
uzavřený	uzavřený	k2eAgInSc4d1
s	s	k7c7
průsvitem	průsvit	k1gInSc7
a	a	k8xC
ostrým	ostrý	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
překrytý	překrytý	k2eAgInSc1d1
<g/>
,	,	kIx,
řapík	řapík	k1gInSc1
je	být	k5eAaImIp3nS
krátký	krátký	k2eAgInSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
medián	medián	k1gInSc1
listu	list	k1gInSc2
<g/>
,	,	kIx,
zelené	zelený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
žilnatina	žilnatina	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
připojení	připojení	k1gNnSc2
řapíku	řapík	k1gInSc2
je	být	k5eAaImIp3nS
takřka	takřka	k6eAd1
bez	bez	k7c2
antokyaninové	antokyaninový	k2eAgFnSc2d1
pigmentace	pigmentace	k1gFnSc2
až	až	k9
velmi	velmi	k6eAd1
slabě	slabě	k6eAd1
pigmentovaná	pigmentovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Oboupohlavní	oboupohlavní	k2eAgInPc4d1
pětičetné	pětičetný	k2eAgInPc4d1
květy	květ	k1gInPc4
v	v	k7c6
hroznovitých	hroznovitý	k2eAgNnPc6d1
květenstvích	květenství	k1gNnPc6
jsou	být	k5eAaImIp3nP
žlutozelené	žlutozelený	k2eAgInPc1d1
<g/>
,	,	kIx,
samosprašné	samosprašný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
až	až	k9
středně	středně	k6eAd1
velká	velký	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
1,4	1,4	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kulatá	kulatý	k2eAgFnSc1d1
nebo	nebo	k8xC
lehce	lehko	k6eAd1
oválná	oválný	k2eAgFnSc1d1
bobule	bobule	k1gFnSc1
růžovo-	růžovo-	k?
až	až	k6eAd1
šedočervené	šedočervený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
na	na	k7c6
krátké	krátká	k1gFnSc6
<g/>
,	,	kIx,
obtížně	obtížně	k6eAd1
oddělitelné	oddělitelný	k2eAgFnSc3d1
stopečce	stopečka	k1gFnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
středně	středně	k6eAd1
silnou	silný	k2eAgFnSc4d1
až	až	k8xS
silnou	silný	k2eAgFnSc4d1
<g/>
,	,	kIx,
průměrně	průměrně	k6eAd1
ojíněnou	ojíněný	k2eAgFnSc4d1
slupku	slupka	k1gFnSc4
<g/>
,	,	kIx,
pod	pod	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
řídké	řídký	k2eAgFnSc6d1
dužině	dužina	k1gFnSc6
mnoho	mnoho	k6eAd1
aromatických	aromatický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bobule	bobule	k1gFnSc1
jsou	být	k5eAaImIp3nP
nejednotné	jednotný	k2eNgFnPc4d1
velikosti	velikost	k1gFnPc4
a	a	k8xC
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masitá	masitý	k2eAgFnSc1d1
až	až	k8xS
tekutá	tekutý	k2eAgFnSc1d1
dužina	dužina	k1gFnSc1
má	mít	k5eAaImIp3nS
jemnou	jemný	k2eAgFnSc4d1
a	a	k8xC
příjemnou	příjemný	k2eAgFnSc4d1
<g/>
,	,	kIx,
odrůdovou	odrůdový	k2eAgFnSc4d1
<g/>
,	,	kIx,
výraznou	výrazný	k2eAgFnSc4d1
aromatickou	aromatický	k2eAgFnSc4d1
<g/>
,	,	kIx,
muškátovou	muškátový	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
<g/>
,	,	kIx,
vysoký	vysoký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
kořenitých	kořenitý	k2eAgFnPc2d1
látek	látka	k1gFnPc2
a	a	k8xC
nižší	nízký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bez	bez	k7c2
zabarvení	zabarvení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozen	hrozen	k1gInSc1
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
až	až	k9
středně	středně	k6eAd1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
80-120	80-120	k4
mm	mm	kA
dlouhý	dlouhý	k2eAgMnSc1d1
<g/>
,	,	kIx,
70-100	70-100	k4
mm	mm	kA
široký	široký	k2eAgMnSc1d1
<g/>
,	,	kIx,
74-98	74-98	k4
g	g	kA
těžký	těžký	k2eAgMnSc1d1
<g/>
,	,	kIx,
hustý	hustý	k2eAgInSc1d1
<g/>
,	,	kIx,
cylindricko-kónický	cylindricko-kónický	k2eAgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
křidélky	křidélko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
krátkou	krátká	k1gFnSc4
<g/>
,	,	kIx,
4	#num#	k4
cm	cm	kA
dlouhou	dlouhý	k2eAgFnSc4d1
<g/>
,	,	kIx,
slabě	slabě	k6eAd1
lignifikovanou	lignifikovaný	k2eAgFnSc4d1
stopku	stopka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
nejnovějších	nový	k2eAgInPc2d3
genetických	genetický	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
poměrně	poměrně	k6eAd1
blízko	blízko	k6eAd1
k	k	k7c3
volně	volně	k6eAd1
rostoucí	rostoucí	k2eAgFnSc3d1
révě	réva	k1gFnSc3
lesní	lesní	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc2
vinifera	vinifer	k1gMnSc2
ssp	ssp	k?
<g/>
.	.	kIx.
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
nahodilým	nahodilý	k2eAgMnSc7d1
křížencem	kříženec	k1gMnSc7
dvou	dva	k4xCgInPc2
ještě	ještě	k6eAd1
starších	starý	k2eAgInPc2d2
<g/>
,	,	kIx,
neurčených	určený	k2eNgInPc2d1
a	a	k8xC
dnes	dnes	k6eAd1
již	již	k6eAd1
patrně	patrně	k6eAd1
neexistujících	existující	k2eNgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
křížencem	kříženec	k1gMnSc7
odrůdy	odrůda	k1gFnSc2
Pinot	Pinota	k1gFnPc2
a	a	k8xC
neznámé	neznámá	k1gFnSc2
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
neexistující	existující	k2eNgFnSc2d1
odrůdy	odrůda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dlouho	dlouho	k6eAd1
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
místem	místo	k1gNnSc7
původu	původ	k1gInSc2
odrůdy	odrůda	k1gFnSc2
je	být	k5eAaImIp3nS
sever	sever	k1gInSc4
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
region	region	k1gInSc1
Südtirol	Südtirol	k1gInSc1
<g/>
/	/	kIx~
<g/>
Trentino-Alto	Trentino-Alt	k2eAgNnSc1d1
Adige	Adige	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
obec	obec	k1gFnSc1
jménem	jméno	k1gNnSc7
Tramin	Tramin	k2eAgMnSc1d1
(	(	kIx(
<g/>
Tramino	Tramin	k2eAgNnSc4d1
<g/>
,	,	kIx,
Termeno	Termen	k2eAgNnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
odrůda	odrůda	k1gFnSc1
měla	mít	k5eAaImAgFnS
šířit	šířit	k5eAaImF
do	do	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
prostoru	prostor	k1gInSc2
Střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendu	legenda	k1gFnSc4
o	o	k7c6
původu	původ	k1gInSc6
odrůdy	odrůda	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
patrně	patrně	k6eAd1
německý	německý	k2eAgMnSc1d1
botanik	botanik	k1gMnSc1
Hieronymus	Hieronymus	k1gMnSc1
Bock	Bock	k1gMnSc1
v	v	k7c6
knize	kniha	k1gFnSc6
„	„	k?
<g/>
Kreuterbuch	Kreuterbucha	k1gFnPc2
<g/>
“	“	k?
z	z	k7c2
roku	rok	k1gInSc2
1539	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
bezpečně	bezpečně	k6eAd1
prokázaná	prokázaný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
„	„	k?
<g/>
vini	vini	k1gNnSc6
de	de	k?
Traminne	Traminn	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
vínech	víno	k1gNnPc6
z	z	k7c2
města	město	k1gNnSc2
Tramin	Tramin	k1gInSc4
<g/>
/	/	kIx~
<g/>
Termeno	Termen	k2eAgNnSc4d1
<g/>
)	)	kIx)
pochází	pocházet	k5eAaImIp3nS
sice	sice	k8xC
již	již	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
1242	#num#	k4
z	z	k7c2
Bolzana	Bolzan	k1gMnSc2
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
takové	takový	k3xDgFnPc1
zmínky	zmínka	k1gFnPc1
objevují	objevovat	k5eAaImIp3nP
stále	stále	k6eAd1
častěji	často	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
vína	víno	k1gNnSc2
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgNnP
vyráběna	vyrábět	k5eAaImNgNnP
z	z	k7c2
odrůdy	odrůda	k1gFnSc2
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pěstoval	pěstovat	k5eAaImAgMnS
v	v	k7c6
této	tento	k3xDgFnSc6
obci	obec	k1gFnSc6
odrůdu	odrůda	k1gFnSc4
pouze	pouze	k6eAd1
jediný	jediný	k2eAgMnSc1d1
pěstitel	pěstitel	k1gMnSc1
<g/>
,	,	kIx,
oblíbena	oblíben	k2eAgFnSc1d1
zde	zde	k6eAd1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
až	až	k9
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
sice	sice	k8xC
pěstována	pěstován	k2eAgFnSc1d1
hojně	hojně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
jde	jít	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
marketingový	marketingový	k2eAgInSc4d1
tah	tah	k1gInSc4
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc4
z	z	k7c2
obce	obec	k1gFnSc2
Tramin	Tramin	k2eAgInSc1d1
se	se	k3xPyFc4
prostě	prostě	k9
lépe	dobře	k6eAd2
prodává	prodávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Itálie	Itálie	k1gFnSc2
neexistují	existovat	k5eNaImIp3nP
ani	ani	k9
žádná	žádný	k3yNgNnPc1
lokální	lokální	k2eAgNnPc1d1
synonyma	synonymum	k1gNnPc1
odrůdy	odrůda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
zde	zde	k6eAd1
musela	muset	k5eAaImAgFnS
vzniknout	vzniknout	k5eAaPmF
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
tu	tu	k6eAd1
odrůda	odrůda	k1gFnSc1
pěstovala	pěstovat	k5eAaImAgFnS
po	po	k7c6
staletí	staletí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgMnPc1d1
ampelografové	ampelograf	k1gMnPc1
kladli	klást	k5eAaImAgMnP
postupně	postupně	k6eAd1
původ	původ	k1gInSc4
odrůdy	odrůda	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Maďarska	Maďarsko	k1gNnSc2
<g/>
,	,	kIx,
Rakouska	Rakousko	k1gNnSc2
či	či	k8xC
do	do	k7c2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnověji	nově	k6eAd3
<g/>
,	,	kIx,
dle	dle	k7c2
názoru	názor	k1gInSc2
švýcarského	švýcarský	k2eAgMnSc2d1
ampelografa	ampelograf	k1gMnSc2
Dr	dr	kA
<g/>
.	.	kIx.
José	Josý	k2eAgFnSc2d1
Vouillamoze	Vouillamoz	k1gInSc5
<g/>
,	,	kIx,
odrůda	odrůda	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
oblasti	oblast	k1gFnSc2
mezi	mezi	k7c7
severovýchodní	severovýchodní	k2eAgFnSc7d1
Francií	Francie	k1gFnSc7
a	a	k8xC
jihovýchodním	jihovýchodní	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
<g/>
,	,	kIx,
důkazy	důkaz	k1gInPc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
genetickém	genetický	k2eAgInSc6d1
profilu	profil	k1gInSc6
odrůdy	odrůda	k1gFnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
profilem	profil	k1gInSc7
a	a	k8xC
místem	místo	k1gNnSc7
původu	původ	k1gInSc2
autochtonních	autochtonní	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
je	být	k5eAaImIp3nS
Tramín	tramín	k1gInSc4
rodičovskou	rodičovský	k2eAgFnSc7d1
odrůdou	odrůda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
Francii	Francie	k1gFnSc6
je	být	k5eAaImIp3nS
zmiňován	zmiňován	k2eAgInSc4d1
Tramín	tramín	k1gInSc4
už	už	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
pod	pod	k7c7
synonymem	synonymum	k1gNnSc7
Savagnin	Savagnina	k1gFnPc2
a	a	k8xC
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc1
pěstování	pěstování	k1gNnSc1
značně	značně	k6eAd1
rozšířeno	rozšířit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Alsasku	Alsasko	k1gNnSc6
zůstal	zůstat	k5eAaPmAgMnS
významnou	významný	k2eAgFnSc7d1
odrůdou	odrůda	k1gFnSc7
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
sedm	sedm	k4xCc4
ušlechtilých	ušlechtilý	k2eAgFnPc2d1
(	(	kIx(
<g/>
noble	noble	k6eAd1
<g/>
)	)	kIx)
odrůd	odrůda	k1gFnPc2
Alsaska	Alsasko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Francie	Francie	k1gFnSc2
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
šířil	šířit	k5eAaImAgMnS
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
Falci	Falc	k1gFnSc6
v	v	k7c6
Německu	Německo	k1gNnSc6
je	být	k5eAaImIp3nS
zmiňován	zmiňován	k2eAgInSc1d1
už	už	k6eAd1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
dosud	dosud	k6eAd1
produkční	produkční	k2eAgFnSc1d1
vinice	vinice	k1gFnSc1
osázená	osázený	k2eAgFnSc1d1
Tramínem	tramín	k1gInSc7
již	již	k9
před	před	k7c4
400	#num#	k4
lety	léto	k1gNnPc7
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
není	být	k5eNaImIp3nS
nikde	nikde	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
odrůdou	odrůda	k1gFnSc7
dominantní	dominantní	k2eAgFnSc7d1
<g/>
,	,	kIx,
daleko	daleko	k6eAd1
spíše	spíše	k9
je	být	k5eAaImIp3nS
odrůdou	odrůda	k1gFnSc7
doplňkovou	doplňkový	k2eAgFnSc7d1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
ovšem	ovšem	k9
pyšní	pyšnit	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
výrobce	výrobce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
víno	víno	k1gNnSc1
není	být	k5eNaImIp3nS
komoditou	komodita	k1gFnSc7
<g/>
,	,	kIx,
typickou	typický	k2eAgFnSc4d1
pro	pro	k7c4
globální	globální	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
a	a	k8xC
regionech	region	k1gInPc6
ale	ale	k8xC
Tramín	tramín	k1gInSc1
obzvláště	obzvláště	k6eAd1
vynikl	vyniknout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
alsaský	alsaský	k2eAgInSc1d1
department	department	k1gInSc1
Vosges	Vosgesa	k1gFnPc2
v	v	k7c6
Lotrinsku	Lotrinsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
svazích	svah	k1gInPc6
stejnojmenného	stejnojmenný	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
Vosges	Vosges	k1gInSc1
(	(	kIx(
<g/>
Vogézy	Vogézy	k1gFnPc1
<g/>
)	)	kIx)
odrůdě	odrůda	k1gFnSc6
velmi	velmi	k6eAd1
daří	dařit	k5eAaImIp3nS
a	a	k8xC
dává	dávat	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
velmi	velmi	k6eAd1
kvalitním	kvalitní	k2eAgNnPc3d1
vínům	víno	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
nedalekého	daleký	k2eNgInSc2d1
Heiligensteinu	Heiligenstein	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
již	již	k6eAd1
ovšem	ovšem	k9
leží	ležet	k5eAaImIp3nS
v	v	k7c6
Alsasku	Alsasko	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
z	z	k7c2
Tramínu	tramín	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
mutace	mutace	k1gFnSc2
Savagnin	Savagnina	k1gFnPc2
Rosé	Rosá	k1gFnSc2
<g/>
,	,	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
Klevener	Klevenra	k1gFnPc2
de	de	k?
Heiligenstein	Heiligensteina	k1gFnPc2
(	(	kIx(
<g/>
pozor	pozor	k1gInSc4
ovšem	ovšem	k9
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
ještě	ještě	k9
vína	víno	k1gNnPc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Alsace	Alsace	k1gFnSc2
Klevner	Klevner	k1gMnSc1
AOC	AOC	kA
<g/>
,	,	kIx,
a	a	k8xC
ta	ten	k3xDgNnPc1
nevyrábí	vyrábět	k5eNaImIp3nP
ani	ani	k8xC
z	z	k7c2
Tramínu	tramín	k1gInSc2
<g/>
,	,	kIx,
ani	ani	k8xC
ze	z	k7c2
Savagninu	Savagnin	k1gInSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
z	z	k7c2
odrůd	odrůda	k1gFnPc2
Pinot	Pinot	k1gMnSc1
Noir	Noir	k1gMnSc1
<g/>
,	,	kIx,
Pinot	Pinot	k1gMnSc1
Blanc	Blanc	k1gMnSc1
či	či	k8xC
Auxerrois	Auxerrois	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
byla	být	k5eAaImAgFnS
odrůda	odrůda	k1gFnSc1
Tramín	tramín	k1gInSc4
červený	červený	k2eAgInSc1d1
roku	rok	k1gInSc2
2008	#num#	k4
pěstována	pěstován	k2eAgFnSc1d1
na	na	k7c4
2.920	2.920	k4
ha	ha	kA
v	v	k7c6
8	#num#	k4
klonech	klon	k1gInPc6
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
2.500	2.500	k4
ha	ha	kA
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
v	v	k7c6
Alsasku	Alsasko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
je	být	k5eAaImIp3nS
typickou	typický	k2eAgFnSc7d1
odrůdou	odrůda	k1gFnSc7
všech	všecek	k3xTgFnPc2
severních	severní	k2eAgFnPc2d1
vinohradnických	vinohradnický	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
Alsaska	Alsasko	k1gNnSc2
přes	přes	k7c4
Německo	Německo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
podíl	podíl	k1gInSc4
0,8	0,8	k4
%	%	kIx~
plochy	plocha	k1gFnPc1
všech	všecek	k3xTgFnPc2
vinic	vinice	k1gFnPc2
a	a	k8xC
vysazen	vysazen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
především	především	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
Bádensko	Bádensko	k1gNnSc1
a	a	k8xC
Falc	Falc	k1gFnSc1
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
na	na	k7c4
831	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
vinic	vinice	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
700	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
rozšířen	rozšířen	k2eAgInSc1d1
je	být	k5eAaImIp3nS
především	především	k9
ve	v	k7c6
Štýrsku	Štýrsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
(	(	kIx(
<g/>
48	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Itálii	Itálie	k1gFnSc4
(	(	kIx(
<g/>
Südtirol	Südtirol	k1gInSc1
<g/>
/	/	kIx~
<g/>
Trentino-Alto	Trentino-Alt	k2eAgNnSc1d1
Adige	Adige	k1gNnSc1
<g/>
,	,	kIx,
500	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
(	(	kIx(
<g/>
200	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc4
<g/>
,	,	kIx,
Moldávii	Moldávie	k1gFnSc4
<g/>
,	,	kIx,
Ukrajinu	Ukrajina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
svět	svět	k1gInSc1
ho	on	k3xPp3gMnSc4
pěstuje	pěstovat	k5eAaImIp3nS
v	v	k7c6
kanadské	kanadský	k2eAgFnSc6d1
Britské	britský	k2eAgFnSc6d1
Kolumbii	Kolumbie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Oregonu	Oregon	k1gInSc6
(	(	kIx(
<g/>
700	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
státě	stát	k1gInSc6
Washington	Washington	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
(	(	kIx(
<g/>
293	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
kde	kde	k6eAd1
to	ten	k3xDgNnSc4
chladnější	chladný	k2eAgNnSc4d2
mikroklima	mikroklima	k1gNnSc4
dovoluje	dovolovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
(	(	kIx(
<g/>
636	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
300	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
(	(	kIx(
<g/>
842	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhadem	odhad	k1gInSc7
byla	být	k5eAaImAgFnS
odrůda	odrůda	k1gFnSc1
pěstována	pěstován	k2eAgFnSc1d1
roku	rok	k1gInSc2
2010	#num#	k4
na	na	k7c4
cca	cca	kA
9.000	9.000	k4
hektarech	hektar	k1gInPc6
vinic	vinice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Státní	státní	k2eAgFnSc6d1
odrůdové	odrůdový	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
zapsána	zapsat	k5eAaPmNgFnS
od	od	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
je	být	k5eAaImIp3nS
zapsána	zapsat	k5eAaPmNgFnS
též	též	k9
v	v	k7c6
Listině	listina	k1gFnSc6
registrovaných	registrovaný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
Česku	Česko	k1gNnSc6
povolené	povolený	k2eAgInPc1d1
dva	dva	k4xCgInPc1
klony	klon	k1gInPc1
<g/>
,	,	kIx,
PO-	PO-	k1gFnSc1
<g/>
202	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
a	a	k8xC
klon	klon	k1gInSc1
PO-	PO-	k1gFnSc2
<g/>
209	#num#	k4
<g/>
/	/	kIx~
<g/>
K	K	kA
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgMnPc4d1
například	například	k6eAd1
bořkovické	bořkovický	k2eAgInPc1d1
<g/>
,	,	kIx,
mělnické	mělnický	k2eAgInPc1d1
<g/>
,	,	kIx,
mutěnické	mutěnický	k2eAgInPc1d1
<g/>
,	,	kIx,
sedlecké	sedlecký	k2eAgInPc1d1
a	a	k8xC
velkopavlovické	velkopavlovický	k2eAgInPc1d1
tramíny	tramín	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
tramíny	tramín	k1gInPc1
z	z	k7c2
nitranska	nitransko	k1gNnSc2
a	a	k8xC
vrábeľska	vrábeľsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
Tramín	tramín	k1gInSc1
v	v	k7c6
ČR	ČR	kA
zastoupen	zastoupen	k2eAgInSc1d1
4	#num#	k4
%	%	kIx~
z	z	k7c2
plochy	plocha	k1gFnSc2
vinohradů	vinohrad	k1gInPc2
<g/>
,	,	kIx,
počátkem	počátkem	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
plochy	plocha	k1gFnSc2
poklesly	poklesnout	k5eAaPmAgFnP
na	na	k7c4
2	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2010	#num#	k4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
asi	asi	k9
3,5	3,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
v	v	k7c6
podoblastech	podoblast	k1gFnPc6
Velkopavlovická	Velkopavlovický	k2eAgFnSc1d1
a	a	k8xC
Mikulovská	mikulovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
hojný	hojný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c6
Bzenecku	Bzenecko	k1gNnSc6
<g/>
,	,	kIx,
Brněnsku	Brněnsko	k1gNnSc6
<g/>
,	,	kIx,
Čáslavsku	Čáslavsko	k1gNnSc6
<g/>
,	,	kIx,
Mutěnicku	Mutěnicko	k1gNnSc6
<g/>
,	,	kIx,
Mostecku	Mostecko	k1gNnSc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2007	#num#	k4
byl	být	k5eAaImAgInS
vysazen	vysadit	k5eAaPmNgInS
na	na	k7c4
561	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
věk	věk	k1gInSc1
vinic	vinice	k1gFnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
ČR	ČR	kA
roku	rok	k1gInSc2
2010	#num#	k4
asi	asi	k9
16	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
na	na	k7c4
2	#num#	k4
%	%	kIx~
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
vinic	vinice	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
na	na	k7c4
318	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s>
Slovo	slovo	k1gNnSc1
červený	červený	k2eAgMnSc1d1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
v	v	k7c6
názvu	název	k1gInSc6
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
vyrobeného	vyrobený	k2eAgInSc2d1
z	z	k7c2
této	tento	k3xDgFnSc2
odrůdy	odrůda	k1gFnSc2
<g/>
,	,	kIx,
vypouští	vypouštět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
patří	patřit	k5eAaImIp3nS
k	k	k7c3
přípustnému	přípustný	k2eAgNnSc3d1
pojmenování	pojmenování	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
lze	lze	k6eAd1
uvádět	uvádět	k5eAaImF
na	na	k7c6
vinětě	viněta	k1gFnSc6
odrůdového	odrůdový	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
právě	právě	k6eAd1
„	„	k?
<g/>
Tramín	tramín	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
z	z	k7c2
němčiny	němčina	k1gFnSc2
pocházející	pocházející	k2eAgFnSc2d1
„	„	k?
<g/>
Gewürtztraminer	Gewürtztraminer	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
paradox	paradox	k1gInSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
právě	právě	k9
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
(	(	kIx(
<g/>
Traminer	Traminer	k1gInSc1
Rot	rota	k1gFnPc2
<g/>
,	,	kIx,
Savagnin	Savagnina	k1gFnPc2
Rosé	Rosá	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Gewürztraminer	Gewürztraminer	k1gInSc4
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
mutace	mutace	k1gFnPc1
odrůdy	odrůda	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
jejichž	jejichž	k3xOyRp3gFnSc6
rozdílnosti	rozdílnost	k1gFnSc6
či	či	k8xC
identicitě	identicita	k1gFnSc6
se	se	k3xPyFc4
jsou	být	k5eAaImIp3nP
někteří	některý	k3yIgMnPc1
milovníci	milovník	k1gMnPc1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
nemnozí	mnohý	k2eNgMnPc1d1
ampelografové	ampelograf	k1gMnPc1
<g/>
,	,	kIx,
přít	přít	k5eAaImF
až	až	k9
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
sklenky	sklenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Soudí	soudit	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Římané	Říman	k1gMnPc1
odrůdu	odrůda	k1gFnSc4
nazývali	nazývat	k5eAaImAgMnP
„	„	k?
<g/>
Vitis	Vitis	k1gFnPc6
aminea	amine	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
tu	tu	k6eAd1
zmiňuje	zmiňovat	k5eAaImIp3nS
již	již	k6eAd1
Gaius	Gaius	k1gMnSc1
Plinius	Plinius	k1gMnSc1
Secundus	Secundus	k1gMnSc1
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
po	po	k7c6
Kr.	Kr.	k1gFnPc6
Někteří	některý	k3yIgMnPc1
ampelografové	ampelograf	k1gMnPc1
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
předkem	předek	k1gMnSc7
Tramínu	tramín	k1gInSc2
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
<g/>
,	,	kIx,
nazývaná	nazývaný	k2eAgFnSc1d1
„	„	k?
<g/>
Uva	Uva	k1gFnSc1
Aminea	Aminea	k1gFnSc1
<g/>
“	“	k?
z	z	k7c2
Thesálie	Thesálie	k1gFnSc2
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
dnešní	dnešní	k2eAgInSc4d1
název	název	k1gInSc4
„	„	k?
<g/>
Tramín	tramín	k1gInSc1
<g/>
“	“	k?
odrůda	odrůda	k1gFnSc1
skutečně	skutečně	k6eAd1
vděčí	vděčit	k5eAaImIp3nS
obci	obec	k1gFnSc3
Tramin	Tramin	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
Termeno	Termen	k2eAgNnSc1d1
v	v	k7c6
italském	italský	k2eAgInSc6d1
regionu	region	k1gInSc6
Südtirol	Südtirol	k1gInSc1
<g/>
/	/	kIx~
<g/>
Trentino-Alto	Trentino-Alt	k2eAgNnSc1d1
Adige	Adige	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrůda	odrůda	k1gFnSc1
odsud	odsud	k6eAd1
sice	sice	k8xC
nepochází	pocházet	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
zmínka	zmínka	k1gFnSc1
o	o	k7c6
dobrých	dobrý	k2eAgNnPc6d1
vínech	víno	k1gNnPc6
z	z	k7c2
Tramínu	tramín	k1gInSc2
(	(	kIx(
<g/>
myšleno	myslet	k5eAaImNgNnS
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
nikoli	nikoli	k9
odrůdu	odrůda	k1gFnSc4
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
literatuře	literatura	k1gFnSc6
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
objevovala	objevovat	k5eAaImAgFnS
tak	tak	k9
často	často	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
v	v	k7c6
Německu	Německo	k1gNnSc6
snažili	snažit	k5eAaImAgMnP
napodobit	napodobit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
proč	proč	k6eAd1
nejpěstovanější	pěstovaný	k2eAgFnSc4d3
odrůdu	odrůda	k1gFnSc4
údolí	údolí	k1gNnSc2
Rýna	Rýn	k1gInSc2
pojmenovali	pojmenovat	k5eAaPmAgMnP
„	„	k?
<g/>
Traminer	Traminer	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
roku	rok	k1gInSc2
1483	#num#	k4
v	v	k7c6
klášteře	klášter	k1gInSc6
Bebenhausen	Bebenhausna	k1gFnPc2
poblíž	poblíž	k7c2
Stuttgartu	Stuttgart	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
pouze	pouze	k6eAd1
snaha	snaha	k1gFnSc1
přiživit	přiživit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c6
tehdejším	tehdejší	k2eAgInSc6d1
věhlasu	věhlas	k1gInSc6
a	a	k8xC
pověsti	pověst	k1gFnSc2
vín	víno	k1gNnPc2
z	z	k7c2
okolí	okolí	k1gNnSc2
obce	obec	k1gFnSc2
Tramin	Tramin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
mimochodem	mimochodem	k9
většinou	většinou	k6eAd1
vyráběna	vyrábět	k5eAaImNgFnS
z	z	k7c2
odrůd	odrůda	k1gFnPc2
Muscat	Muscat	k1gFnSc2
Blanc	Blanc	k1gMnSc1
à	à	k?
Petits	Petits	k1gInSc1
Grains	Grains	k1gInSc1
(	(	kIx(
<g/>
Moscato	Moscat	k2eAgNnSc1d1
Bianco	bianco	k2eAgNnSc1d1
<g/>
,	,	kIx,
Muškát	muškát	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Lagrein	Lagrein	k1gMnSc1
Weiss	Weiss	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc1
kořenný	kořenný	k2eAgInSc1d1
:	:	kIx,
Т	Т	k?
р	р	k?
<g/>
,	,	kIx,
Г	Г	k?
<g/>
,	,	kIx,
К	К	k?
Т	Т	k?
<g/>
,	,	kIx,
С	С	k?
р	р	k?
(	(	kIx(
<g/>
vše	všechen	k3xTgNnSc1
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Auvernas	Auvernas	k1gMnSc1
Rouge	rouge	k1gFnSc1
<g/>
,	,	kIx,
Blanc	Blanc	k1gMnSc1
Brun	Brun	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
Court	Court	k1gInSc4
<g/>
,	,	kIx,
Bon	bon	k1gInSc4
Blanc	Blanc	k1gMnSc1
<g/>
,	,	kIx,
Braunes	Braunes	k1gMnSc1
<g/>
,	,	kIx,
Christkindelstraube	Christkindelstraub	k1gInSc5
<g/>
,	,	kIx,
Clevener	Clevenero	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Clevner	Clevner	k1gInSc1
<g/>
,	,	kIx,
Crescentia	Crescentia	k1gFnSc1
Rotundifolia	Rotundifolia	k1gFnSc1
<g/>
,	,	kIx,
Crevena	Creven	k2eAgFnSc1d1
Ruziva	Ruziva	k1gFnSc1
<g/>
,	,	kIx,
Crvena	Crven	k2eAgFnSc1d1
Ruzica	Ruzica	k1gFnSc1
<g/>
,	,	kIx,
Crveni	Crven	k2eAgMnPc1d1
Traminac	Traminac	k1gFnSc4
<g/>
,	,	kIx,
Diseci	Disece	k1gFnSc4
Traminer	Traminero	k1gNnPc2
<g/>
,	,	kIx,
Dreimaenner	Dreimaenner	k1gInSc1
<g/>
,	,	kIx,
Dreimannen	Dreimannen	k1gInSc1
<g/>
,	,	kIx,
Dreipfennigholz	Dreipfennigholz	k1gInSc1
<g/>
,	,	kIx,
Drumin	Drumin	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
Ljbora	Ljbora	k1gFnSc1
<g/>
,	,	kIx,
Duret	Duret	k1gInSc1
Rouge	rouge	k1gFnSc1
<g/>
,	,	kIx,
Durmín	Durmín	k1gInSc1
<g/>
,	,	kIx,
Edeltraube	Edeltraub	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Fermentin	Fermentin	k1gInSc1
Rouge	rouge	k1gFnSc1
<g/>
,	,	kIx,
Fleischroth	Fleischroth	k1gInSc1
<g/>
,	,	kIx,
Fleischweiner	Fleischweiner	k1gInSc1
<g/>
,	,	kIx,
Formenteau	Formenteaa	k1gFnSc4
<g/>
,	,	kIx,
Formentin	Formentin	k1gInSc4
Rouge	rouge	k1gFnSc2
<g/>
,	,	kIx,
Fourmenteau	Fourmenteaus	k1gInSc2
Rouge	rouge	k1gFnPc2
<g/>
,	,	kIx,
Fränkisch	Fränkischa	k1gFnPc2
<g/>
,	,	kIx,
French	Frencha	k1gFnPc2
<g/>
,	,	kIx,
Frenscher	Frenschra	k1gFnPc2
<g/>
,	,	kIx,
Frentsch	Frentscha	k1gFnPc2
<g/>
,	,	kIx,
Fromenté	Fromentý	k2eAgFnPc4d1
Rosé	Rosá	k1gFnPc4
<g/>
,	,	kIx,
Fromenteau	Fromentea	k2eAgFnSc4d1
Rouge	rouge	k1gFnSc4
<g/>
,	,	kIx,
Fűszeres	Fűszeres	k1gMnSc1
Tramini	Tramin	k2eAgMnPc1d1
<g/>
,	,	kIx,
Gentil	Gentil	k1gMnSc1
Rosé	Rosý	k2eAgFnSc2d1
Aromatique	Aromatiqu	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Gentil-Duret	Gentil-Duret	k1gInSc1
Rouge	rouge	k1gFnSc1
<g/>
,	,	kIx,
Gentile	Gentil	k1gMnSc5
Blanc	Blanc	k1gMnSc1
<g/>
,	,	kIx,
Gewürztraminer	Gewürztraminer	k1gMnSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gewuerztraminer	Gewuerztraminer	k1gInSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
G.	G.	kA
Lenz	Lenz	k1gMnSc1
Moser	Moser	k1gMnSc1
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gringet	Gringet	k1gInSc1
<g/>
,	,	kIx,
Gris	Gris	k1gInSc1
Rouge	rouge	k1gFnPc2
<g/>
,	,	kIx,
Haiden	Haidna	k1gFnPc2
<g/>
,	,	kIx,
Heiligenstein	Heiligenstein	k1gMnSc1
Klevener	Klevener	k1gMnSc1
<g/>
,	,	kIx,
Kirmizi	Kirmih	k1gMnPc1
Traminer	Traminer	k1gMnSc1
<g/>
,	,	kIx,
Klaebinger	Klaebinger	k1gMnSc1
<g/>
,	,	kIx,
Klaevner	Klaevner	k1gMnSc1
<g/>
,	,	kIx,
Kleinbraun	Kleinbraun	k1gMnSc1
<g/>
,	,	kIx,
Kleinbrauner	Kleinbrauner	k1gMnSc1
<g/>
,	,	kIx,
Kleiner	Kleiner	k1gMnSc1
Traminer	Traminer	k1gMnSc1
<g/>
,	,	kIx,
Kleinwiener	Kleinwiener	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Livora	Livora	k1gFnSc1
<g/>
,	,	kIx,
L.	L.	kA
Červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
Mala	Mal	k2eAgFnSc1d1
Dinka	Dinka	k1gFnSc1
<g/>
,	,	kIx,
Marzimmer	Marzimmer	k1gInSc1
<g/>
,	,	kIx,
Mirisavi	Mirisaev	k1gFnSc6
Traminac	Traminac	k1gFnSc1
<g/>
,	,	kIx,
Muskattraminer	Muskattraminer	k1gInSc1
<g/>
,	,	kIx,
Naturé	Naturý	k2eAgFnSc2d1
<g/>
,	,	kIx,
N.	N.	kA
Rose	Ros	k1gMnSc2
<g/>
,	,	kIx,
Noble	Nobl	k1gMnSc2
Rosé	Rosá	k1gFnSc2
<g/>
,	,	kIx,
Nuernberger	Nuernberger	k1gInSc4
Rot	rota	k1gFnPc2
<g/>
,	,	kIx,
Pinat	Pinat	k1gInSc1
Cervena	Cerven	k2eAgFnSc1d1
<g/>
,	,	kIx,
Piros	Piros	k1gMnSc1
Tramini	Tramin	k2eAgMnPc1d1
<g/>
,	,	kIx,
Plant	planta	k1gFnPc2
Paien	Paina	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Princ	princ	k1gMnSc1
Červený	Červený	k1gMnSc1
<g/>
,	,	kIx,
Prinšt	Prinšt	k1gMnSc1
Červený	Červený	k1gMnSc1
<g/>
,	,	kIx,
Ranfoliza	Ranfoliza	k1gFnSc1
<g/>
,	,	kIx,
Rosentraminer	Rosentraminra	k1gFnPc2
<g/>
,	,	kIx,
Rotclevner	Rotclevnra	k1gFnPc2
<g/>
,	,	kIx,
Rotedel	Rotedlo	k1gNnPc2
<g/>
,	,	kIx,
Roter	Roter	k1gMnSc1
Nuernberger	Nuernberger	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
Traminer	Traminer	k1gInSc1
<g/>
,	,	kIx,
Roethlichter	Roethlichtra	k1gFnPc2
<g/>
,	,	kIx,
Rosentraminer	Rosentraminra	k1gFnPc2
<g/>
,	,	kIx,
Rotfranken	Rotfrankna	k1gFnPc2
<g/>
,	,	kIx,
Rothedel	Rothedlo	k1gNnPc2
<g/>
,	,	kIx,
Rothedl	Rothedl	k1gFnPc2
<g/>
,	,	kIx,
Rother	Rothra	k1gFnPc2
Clevner	Clevnra	k1gFnPc2
<g/>
,	,	kIx,
R.	R.	kA
Nürnberger	Nürnbergero	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
R.	R.	kA
Riesling	Riesling	k1gInSc1
<g/>
,	,	kIx,
Rothklaeber	Rothklaeber	k1gMnSc1
<g/>
,	,	kIx,
Rothklauser	Rothklauser	k1gMnSc1
<g/>
,	,	kIx,
Rothweiner	Rothweiner	k1gMnSc1
<g/>
,	,	kIx,
Rothwiener	Rothwiener	k1gMnSc1
<g/>
,	,	kIx,
Rotklaevler	Rotklaevler	k1gMnSc1
<g/>
,	,	kIx,
Rotklevner	Rotklevner	k1gMnSc1
<g/>
,	,	kIx,
Rousselett	Rousselett	k1gMnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Roz	Roz	k1gMnSc1
Taramin	Taramin	k1gInSc1
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Runziva	Runziv	k1gMnSc4
<g/>
,	,	kIx,
Rusa	Rus	k1gMnSc4
<g/>
,	,	kIx,
Ruska	Ruska	k1gFnSc1
<g/>
,	,	kIx,
Ryvola	Ryvola	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Salvagnin	Salvagnin	k1gInSc1
<g/>
,	,	kIx,
Sandtraminer	Sandtraminer	k1gInSc1
<g/>
,	,	kIx,
Sauvagnin	Sauvagnin	k1gInSc1
<g/>
,	,	kIx,
Savagnin	Savagnin	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
Jaune	Jaun	k1gInSc5
<g/>
,	,	kIx,
S.	S.	kA
Rosa	Rosa	k1gFnSc1
Aromatique	Aromatique	k1gFnSc1
<g/>
,	,	kIx,
S.	S.	kA
Rosé	Rosé	k1gNnSc1
<g/>
,	,	kIx,
S.	S.	kA
Rose	Ros	k1gMnSc2
Aromatique	Aromatiqu	k1gMnSc2
(	(	kIx(
<g/>
Chile	Chile	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S.	S.	kA
Rose	Ros	k1gMnSc2
Musque	Musqu	k1gMnSc2
<g/>
,	,	kIx,
Servoyen	Servoyen	k2eAgMnSc1d1
Rose	Rose	k1gMnSc1
<g/>
,	,	kIx,
Sivi	Sivi	k1gNnSc1
Traminac	Traminac	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Bulharsko	Bulharsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
St.	st.	kA
Klauser	Klauser	k1gInSc1
<g/>
,	,	kIx,
Svenié	Svenié	k1gNnSc1
<g/>
,	,	kIx,
Termeno	Termen	k2eAgNnSc1d1
Aromatico	Aromatico	k1gNnSc1
<g/>
,	,	kIx,
Tirolensis	Tirolensis	k1gInSc1
<g/>
,	,	kIx,
Tokaner	Tokaner	k1gInSc1
<g/>
,	,	kIx,
Tokayer	Tokayer	k1gInSc1
<g/>
,	,	kIx,
Tramin	Tramin	k1gInSc1
Kořenný	kořenný	k2eAgInSc1d1
<g/>
,	,	kIx,
Traminac	Traminac	k1gInSc1
Crveni	Crven	k2eAgMnPc1d1
(	(	kIx(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
T.	T.	kA
Diseci	Disece	k1gFnSc4
<g/>
,	,	kIx,
T.	T.	kA
Mirisavi	Mirisaev	k1gFnSc6
(	(	kIx(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
T.	T.	kA
M.	M.	kA
Crveni	Crven	k2eAgMnPc1d1
<g/>
,	,	kIx,
T.	T.	kA
Rosin	Rosin	k2eAgMnSc1d1
<g/>
,	,	kIx,
T.	T.	kA
Sivi	Sivi	k1gNnSc1
<g/>
,	,	kIx,
Traminec	Traminec	k1gInSc1
<g/>
,	,	kIx,
Traminer	Traminer	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
T.	T.	kA
Aromatico	Aromatico	k1gNnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
T.	T.	kA
Epice	epika	k1gFnSc6
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
T.	T.	kA
Musque	Musque	k1gFnPc2
<g/>
,	,	kIx,
T.	T.	kA
Parfume	Parfum	k1gInSc5
<g/>
,	,	kIx,
T.	T.	kA
Red	Red	k1gFnSc4
<g/>
,	,	kIx,
T.	T.	kA
Rose	Ros	k1gMnSc2
Aromatique	Aromatiqu	k1gMnSc2
<g/>
,	,	kIx,
T.	T.	kA
Rosso	Rossa	k1gFnSc5
<g/>
,	,	kIx,
T.	T.	kA
Roter	Roter	k1gInSc1
<g/>
,	,	kIx,
T.	T.	kA
Rouge	rouge	k1gFnSc4
<g/>
,	,	kIx,
T.	T.	kA
Roz	Roz	k1gFnSc1
(	(	kIx(
<g/>
Rumunsko	Rumunsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
T.	T.	kA
Rozovyi	Rozovyi	k1gNnSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc1
Kořeněný	kořeněný	k2eAgInSc1d1
<g/>
,	,	kIx,
Tramini	Tramin	k2eAgMnPc1d1
Piros	Pirosa	k1gFnPc2
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Trammener	Trammener	k1gMnSc1
<g/>
,	,	kIx,
Weihrauch	Weihrauch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Savagnin	Savagnin	k1gInSc1
Rosé	Rosá	k1gFnSc2
:	:	kIx,
Clevner	Clevner	k1gInSc1
<g/>
,	,	kIx,
Drumin	Drumin	k1gInSc1
<g/>
,	,	kIx,
Durbacher	Durbachra	k1gFnPc2
Clevner	Clevnra	k1gFnPc2
<g/>
,	,	kIx,
Fourmenteau	Fourmentea	k2eAgFnSc4d1
Rouge	rouge	k1gFnSc4
<g/>
,	,	kIx,
Heiligenststeiner	Heiligenststeiner	k1gMnSc1
Clevner	Clevner	k1gMnSc1
<g/>
,	,	kIx,
H.	H.	kA
Klevner	Klevner	k1gInSc1
<g/>
,	,	kIx,
Livora	Livora	k1gFnSc1
<g/>
,	,	kIx,
L.	L.	kA
červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
Princ	princ	k1gMnSc1
červený	červený	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ryvola	Ryvola	k1gFnSc1
<g/>
,	,	kIx,
Savagnin	Savagnin	k1gInSc1
Rosé	Rosá	k1gFnSc2
non	non	k?
Musque	Musque	k1gInSc1
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
T.	T.	kA
kořenný	kořenný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Tramin	Tramin	k1gInSc1
Diseci	Disece	k1gFnSc4
<g/>
,	,	kIx,
T.	T.	kA
Rdeci	Rdece	k1gFnSc4
<g/>
,	,	kIx,
Traminac	Traminac	k1gFnSc4
Crveni	Crven	k2eAgMnPc1d1
<g/>
,	,	kIx,
T.	T.	kA
Rdeci	Rdece	k1gFnSc4
<g/>
,	,	kIx,
Traminer	Traminer	k1gInSc4
Rosé	Rosá	k1gFnSc2
<g/>
,	,	kIx,
T.	T.	kA
Rot	rota	k1gFnPc2
<g/>
,	,	kIx,
T.	T.	kA
Roz	Roz	k1gMnPc1
<g/>
,	,	kIx,
Tramini	Tramin	k2eAgMnPc1d1
Piros	Piros	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Mutace	mutace	k1gFnPc1
a	a	k8xC
klony	klon	k1gInPc1
odrůdy	odrůda	k1gFnSc2
</s>
<s>
Odrůda	odrůda	k1gFnSc1
Gewürztraminer	Gewürztraminra	k1gFnPc2
(	(	kIx(
<g/>
Tramín	tramín	k1gInSc1
kořenný	kořenný	k2eAgInSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
aromatickou	aromatický	k2eAgFnSc4d1
mutaci	mutace	k1gFnSc4
odrůdy	odrůda	k1gFnSc2
Traminer	Traminra	k1gFnPc2
Rot	rota	k1gFnPc2
(	(	kIx(
<g/>
Savagnin	Savagnin	k1gInSc1
Rosé	Rosé	k1gNnSc2
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
změnu	změna	k1gFnSc4
barevnou	barevný	k2eAgFnSc7d1
mutací	mutace	k1gFnSc7
odrůdy	odrůda	k1gFnSc2
Traminer	Traminer	k1gInSc1
Weiss	Weiss	k1gMnSc1
(	(	kIx(
<g/>
Savagnin	Savagnin	k2eAgInSc1d1
blanc	blanc	k1gInSc1
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc1
bílý	bílý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
odrůdě	odrůda	k1gFnSc6
„	„	k?
<g/>
Gewürztraminer	Gewürztraminer	k1gInSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1827	#num#	k4
z	z	k7c2
oblasti	oblast	k1gFnSc2
Rheingau	Rheingaus	k1gInSc2
<g/>
,	,	kIx,
Johann	Johann	k1gInSc1
Metzger	Metzger	k1gInSc1
ji	on	k3xPp3gFnSc4
zde	zde	k6eAd1
označuje	označovat	k5eAaImIp3nS
za	za	k7c7
vzácně	vzácně	k6eAd1
pěstovanou	pěstovaný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
mutaci	mutace	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Rheinland-Pfalz	Rheinland-Pfalza	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Alsasku	Alsasko	k1gNnSc6
roku	rok	k1gInSc2
1886	#num#	k4
ji	on	k3xPp3gFnSc4
zmiňuje	zmiňovat	k5eAaImIp3nS
Oberlin	Oberlin	k2eAgMnSc1d1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
oblasti	oblast	k1gFnSc2
„	„	k?
<g/>
Palatinat	Palatinat	k1gMnSc1
<g/>
“	“	k?
tedy	tedy	k9
Rýnská	rýnský	k2eAgFnSc1d1
Falc	Falc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Problematika	problematika	k1gFnSc1
odlišování	odlišování	k1gNnSc2
„	„	k?
<g/>
taxonů	taxon	k1gInPc2
<g/>
“	“	k?
s	s	k7c7
názvem	název	k1gInSc7
Tramín	tramín	k1gInSc1
červený	červený	k2eAgMnSc1d1
a	a	k8xC
Gewürztraminer	Gewürztraminer	k1gInSc1
:	:	kIx,
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
německy	německy	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
nazývaný	nazývaný	k2eAgInSc1d1
Traminer	Traminer	k1gInSc1
Rot	rota	k1gFnPc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
historicky	historicky	k6eAd1
pěstován	pěstován	k2eAgInSc4d1
v	v	k7c6
zemích	zem	k1gFnPc6
bývalého	bývalý	k2eAgInSc2d1
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
a	a	k8xC
měl	mít	k5eAaImAgMnS
se	se	k3xPyFc4
vyznačovat	vyznačovat	k5eAaImF
červenou	červený	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
bobulí	bobule	k1gFnSc7
a	a	k8xC
umírněnou	umírněný	k2eAgFnSc7d1
kořenitostí	kořenitost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gewürztraminer	Gewürztraminer	k1gInSc1
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgInSc1d1
v	v	k7c6
Česku	Česko	k1gNnSc6
Tramín	tramín	k1gInSc1
kořeněný	kořeněný	k2eAgMnSc1d1
či	či	k8xC
kořenný	kořenný	k2eAgMnSc1d1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
pěstován	pěstovat	k5eAaImNgInS
v	v	k7c6
západoevropských	západoevropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
mít	mít	k5eAaImF
červenošedou	červenošedý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
bobulí	bobule	k1gFnPc2
a	a	k8xC
vyšší	vysoký	k2eAgFnSc4d2
kořenitost	kořenitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
taxony	taxon	k1gInPc1
byly	být	k5eAaImAgInP
různými	různý	k2eAgMnPc7d1
ampelografy	ampelograf	k1gMnPc7
v	v	k7c6
různém	různý	k2eAgNnSc6d1
období	období	k1gNnSc6
považovány	považován	k2eAgInPc1d1
za	za	k7c4
samostatné	samostatný	k2eAgFnPc4d1
odrůdy	odrůda	k1gFnPc4
<g/>
,	,	kIx,
mutace	mutace	k1gFnPc4
či	či	k8xC
klony	klon	k1gInPc4
<g/>
,	,	kIx,
místy	místy	k6eAd1
dokonce	dokonce	k9
převládl	převládnout	k5eAaPmAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
stanovištní	stanovištní	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
jedna	jeden	k4xCgFnSc1
a	a	k8xC
táž	týž	k3xTgFnSc1
odrůda	odrůda	k1gFnSc1
výrazně	výrazně	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
lze	lze	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
(	(	kIx(
<g/>
a	a	k8xC
podobně	podobně	k6eAd1
v	v	k7c6
zemích	zem	k1gFnPc6
bývalého	bývalý	k2eAgInSc2d1
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
<g/>
)	)	kIx)
po	po	k7c6
období	období	k1gNnSc6
horoucího	horoucí	k2eAgNnSc2d1
vysazování	vysazování	k1gNnSc2
vinohradů	vinohrad	k1gInPc2
před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
EU	EU	kA
a	a	k8xC
shánění	shánění	k1gNnSc2
klonů	klon	k1gInPc2
odrůd	odrůda	k1gFnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
oblastí	oblast	k1gFnPc2
a	a	k8xC
populací	populace	k1gFnPc2
mnohdy	mnohdy	k6eAd1
jen	jen	k9
těžko	těžko	k6eAd1
odlišit	odlišit	k5eAaPmF
<g/>
,	,	kIx,
o	o	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
z	z	k7c2
těch	ten	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
jmenovaných	jmenovaný	k2eAgFnPc2d1
pseudoodrůd	pseudoodrůda	k1gFnPc2
vlastně	vlastně	k9
jde	jít	k5eAaImIp3nS
<g/>
,	,	kIx,
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
být	být	k5eAaImF
odlišování	odlišování	k1gNnSc4
těchto	tento	k3xDgInPc2
zmíněných	zmíněný	k2eAgInPc2d1
„	„	k?
<g/>
taxonů	taxon	k1gInPc2
<g/>
“	“	k?
bezpředmětné	bezpředmětný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudě	soudě	k6eAd1
ovšem	ovšem	k9
dle	dle	k7c2
typu	typ	k1gInSc2
vyráběných	vyráběný	k2eAgNnPc2d1
vín	víno	k1gNnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
dnes	dnes	k6eAd1
pěstován	pěstovat	k5eAaImNgInS
v	v	k7c6
podstatě	podstata	k1gFnSc6
pouze	pouze	k6eAd1
taxon	taxon	k1gInSc4
Gewürztraminer	Gewürztraminra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Taxon	taxon	k1gInSc1
Savagnin	Savagnin	k1gInSc1
Rosé	Rosá	k1gFnSc2
je	být	k5eAaImIp3nS
pěstován	pěstovat	k5eAaImNgInS
takřka	takřka	k6eAd1
výlučně	výlučně	k6eAd1
v	v	k7c6
Alsasku	Alsasko	k1gNnSc6
(	(	kIx(
<g/>
roku	rok	k1gInSc2
2006	#num#	k4
na	na	k7c4
42,5	42,5	k4
ha	ha	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
německém	německý	k2eAgNnSc6d1
Bádensku	Bádensko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
Durbachu	Durbach	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
nejrozsáhlejší	rozsáhlý	k2eAgFnPc1d3
osazené	osazený	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
sliznatou	sliznatý	k2eAgFnSc4d1
až	až	k6eAd1
rozplývavou	rozplývavý	k2eAgFnSc4d1
<g/>
,	,	kIx,
sladkou	sladký	k2eAgFnSc4d1
dužinu	dužina	k1gFnSc4
neutrální	neutrální	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
<g/>
,	,	kIx,
bez	bez	k7c2
klasického	klasický	k2eAgNnSc2d1
<g/>
,	,	kIx,
florálně-kořenitého	florálně-kořenitý	k2eAgNnSc2d1
aroma	aroma	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
od	od	k7c2
odrůdy	odrůda	k1gFnSc2
Gewürztraminer	Gewürztraminra	k1gFnPc2
je	být	k5eAaImIp3nS
průsvitná	průsvitný	k2eAgFnSc1d1
slupka	slupka	k1gFnSc1
bobule	bobule	k1gFnSc2
v	v	k7c6
období	období	k1gNnSc6
zabarvování	zabarvování	k1gNnPc2
bobulí	bobule	k1gFnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
"	"	kIx"
<g/>
veraison	veraison	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrůdová	odrůdový	k2eAgFnSc1d1
vína	vína	k1gFnSc1
jsou	být	k5eAaImIp3nP
zlatožlutá	zlatožlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
plná	plný	k2eAgFnSc1d1
<g/>
,	,	kIx,
často	často	k6eAd1
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
zbytkového	zbytkový	k2eAgInSc2d1
cukru	cukr	k1gInSc2
a	a	k8xC
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
spíše	spíše	k9
neutrálního	neutrální	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
aroma	aroma	k1gNnSc6
nenajdeme	najít	k5eNaPmIp1nP
kořenité	kořenitý	k2eAgInPc1d1
a	a	k8xC
florální	florální	k2eAgInPc1d1
tóny	tón	k1gInPc1
<g/>
,	,	kIx,
typické	typický	k2eAgInPc1d1
pro	pro	k7c4
vína	víno	k1gNnPc4
Gewürztraminer	Gewürztraminra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Alsasku	Alsasko	k1gNnSc6
vyráběné	vyráběný	k2eAgNnSc4d1
víno	víno	k1gNnSc4
„	„	k?
<g/>
Klevener	Klevener	k1gInSc1
de	de	k?
Heiligenstein	Heiligenstein	k1gInSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
právě	právě	k9
z	z	k7c2
hroznů	hrozen	k1gInPc2
odrůdy	odrůda	k1gFnSc2
Savagnin	Savagnina	k1gFnPc2
Rosé	Rosá	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
charakteristiky	charakteristika	k1gFnSc2
lze	lze	k6eAd1
do	do	k7c2
synonymiky	synonymika	k1gFnSc2
odrůdy	odrůda	k1gFnSc2
Savagnin	Savagnina	k1gFnPc2
Rosé	Rosá	k1gFnSc2
zařadit	zařadit	k5eAaPmF
taxon	taxon	k1gInSc4
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
a	a	k8xC
odlišit	odlišit	k5eAaPmF
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
od	od	k7c2
taxonu	taxon	k1gInSc2
Gewürztraminer	Gewürztraminra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
ovšem	ovšem	k9
nahlédnete	nahlédnout	k5eAaPmIp2nP
do	do	k7c2
katalogu	katalog	k1gInSc2
francouzských	francouzský	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
„	„	k?
<g/>
Tramin	Tramin	k1gInSc1
cerveny	cerven	k2eAgInPc1d1
<g/>
“	“	k?
uveden	uveden	k2eAgInSc1d1
v	v	k7c6
synonymice	synonymika	k1gFnSc6
taxonu	taxon	k1gInSc2
Gewürztraminer	Gewürztraminra	k1gFnPc2
<g/>
,	,	kIx,
Francouzi	Francouz	k1gMnPc1
patrně	patrně	k6eAd1
předpokládají	předpokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
nejsme	být	k5eNaImIp1nP
zemí	zem	k1gFnSc7
bývalého	bývalý	k2eAgInSc2d1
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
a	a	k8xC
nearomatický	aromatický	k2eNgInSc1d1
typ	typ	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
Česku	Česko	k1gNnSc6
nepěstoval	pěstovat	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Tramín	tramín	k1gInSc1
muškátový	muškátový	k2eAgInSc1d1
(	(	kIx(
<g/>
Savagnin	Savagnin	k2eAgInSc1d1
blanc	blanc	k1gInSc1
musqué	musqué	k1gNnSc2
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
hrozny	hrozen	k1gInPc4
jantarové	jantarový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
intenzivnější	intenzivní	k2eAgFnSc2d2
<g/>
,	,	kIx,
zřetelně	zřetelně	k6eAd1
muškátový	muškátový	k2eAgInSc4d1
buket	buket	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
už	už	k6eAd1
pouze	pouze	k6eAd1
zmiňme	zminout	k5eAaPmRp1nP
mutace	mutace	k1gFnPc4
odrůdy	odrůda	k1gFnPc4
<g/>
,	,	kIx,
uvedené	uvedený	k2eAgFnPc4d1
samostatně	samostatně	k6eAd1
v	v	k7c6
katalogu	katalog	k1gInSc6
VIVC	VIVC	kA
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc4
žlutý	žlutý	k2eAgInSc4d1
(	(	kIx(
<g/>
Traminer	Traminer	k1gInSc4
Gelb	Gelba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tramín	tramín	k1gInSc4
Šedý	šedý	k2eAgInSc4d1
(	(	kIx(
<g/>
Traminer	Traminer	k1gInSc4
Gris	Grisa	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
tetraploidní	tetraploidní	k2eAgInSc4d1
klon	klon	k1gInSc4
odrůdy	odrůda	k1gFnSc2
Traminer	Traminra	k1gFnPc2
Rot	rota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vinicích	vinice	k1gFnPc6
se	se	k3xPyFc4
dále	daleko	k6eAd2
nehojně	hojně	k6eNd1
vyskytuje	vyskytovat	k5eAaImIp3nS
mutace	mutace	k1gFnSc1
Tramínu	tramín	k1gInSc2
s	s	k7c7
růžově	růžově	k6eAd1
zbarvenou	zbarvený	k2eAgFnSc7d1
dužinou	dužina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
kříženci	kříženec	k1gMnPc1
odrůdy	odrůda	k1gFnSc2
</s>
<s>
Odrůda	odrůda	k1gFnSc1
Tramín	tramín	k1gInSc1
byla	být	k5eAaImAgFnS
při	při	k7c6
křížení	křížení	k1gNnSc6
jedním	jeden	k4xCgMnSc7
z	z	k7c2
rodičů	rodič	k1gMnPc2
mnoha	mnoho	k4c2
známých	známá	k1gFnPc2
moštových	moštový	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramín	tramín	k1gInSc1
je	být	k5eAaImIp3nS
geneticky	geneticky	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
v	v	k7c6
Česku	Česko	k1gNnSc6
pěstovaných	pěstovaný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
<g/>
,	,	kIx,
stál	stát	k5eAaImAgMnS
při	při	k7c6
zrodu	zrod	k1gInSc6
skupiny	skupina	k1gFnSc2
západoevropských	západoevropský	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rodičem	rodič	k1gMnSc7
takových	takový	k3xDgFnPc2
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Ryzlink	ryzlink	k1gInSc4
rýnský	rýnský	k2eAgInSc4d1
<g/>
,	,	kIx,
Sauvignon	Sauvignon	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
následně	následně	k6eAd1
tedy	tedy	k9
Cabernet	Cabernet	k1gMnSc1
Sauvignon	Sauvignon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Veltlínské	veltlínský	k2eAgNnSc1d1
zelené	zelené	k1gNnSc1
<g/>
,	,	kIx,
Sylvánské	sylvánské	k1gNnSc1
zelené	zelená	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
moderní	moderní	k2eAgFnSc1d1
šlechtitelská	šlechtitelský	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Tramín	tramín	k1gInSc1
je	být	k5eAaImIp3nS
vynikající	vynikající	k2eAgMnSc1d1
rodič	rodič	k1gMnSc1
nových	nový	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c4
potomstvo	potomstvo	k1gNnSc4
přenáší	přenášet	k5eAaImIp3nS
dominantně	dominantně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
cukernatost	cukernatost	k1gFnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
komplexní	komplexní	k2eAgNnSc1d1
aroma	aroma	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
potomstvu	potomstvo	k1gNnSc6
štěpí	štěpit	k5eAaImIp3nS
do	do	k7c2
nejrůznějších	různý	k2eAgInPc2d3
odstínů	odstín	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
vůně	vůně	k1gFnSc2
růží	růž	k1gFnPc2
<g/>
,	,	kIx,
po	po	k7c4
klasický	klasický	k2eAgInSc4d1
muškát	muškát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Níže	nízce	k6eAd2
uvádíme	uvádět	k5eAaImIp1nP
některé	některý	k3yIgFnPc4
z	z	k7c2
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
vyšlechtěných	vyšlechtěný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Siegerrebe	Siegerrebat	k5eAaPmIp3nS
(	(	kIx(
<g/>
Madlenka	Madlenka	k1gFnSc1
raná	raný	k2eAgFnSc1d1
x	x	k?
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
raná	raný	k2eAgFnSc1d1
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
atraktivní	atraktivní	k2eAgFnSc1d1
chutí	chuť	k1gFnSc7
překonává	překonávat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
stolních	stolní	k2eAgMnPc2d1
hroznů	hrozen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
ní	on	k3xPp3gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
když	když	k8xS
nedozrává	dozrávat	k5eNaImIp3nS
jako	jako	k9
úplně	úplně	k6eAd1
první	první	k4xOgNnSc1
<g/>
,	,	kIx,
děti	dítě	k1gFnPc1
a	a	k8xC
vosy	vosa	k1gFnPc1
se	se	k3xPyFc4
postarají	postarat	k5eAaPmIp3nP
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
jako	jako	k9
první	první	k4xOgFnSc7
sklizena	sklizen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vrboska	Vrboska	k1gFnSc1
(	(	kIx(
<g/>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
x	x	k?
Čabaňská	Čabaňský	k2eAgFnSc1d1
perla	perla	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nová	nový	k2eAgFnSc1d1
moravská	moravský	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
burčáků	burčák	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Pálava	Pálava	k1gFnSc1
(	(	kIx(
<g/>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
x	x	k?
Müller	Müller	k1gInSc1
Thurgau	Thurgaus	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
úspěšné	úspěšný	k2eAgNnSc4d1
moravské	moravský	k2eAgNnSc4d1
novošlechtění	novošlechtění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
si	se	k3xPyFc3
dalo	dát	k5eAaPmAgNnS
za	za	k7c4
cíl	cíl	k1gInSc4
nabídnout	nabídnout	k5eAaPmF
úrodnější	úrodný	k2eAgInSc4d2
Tramín	tramín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
zcela	zcela	k6eAd1
nepovedlo	povést	k5eNaPmAgNnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
muškátová	muškátový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
si	se	k3xPyFc3
zajistila	zajistit	k5eAaPmAgFnS
stálé	stálý	k2eAgNnSc4d1
místo	místo	k1gNnSc4
na	na	k7c6
našich	náš	k3xOp1gInPc6
vinohradech	vinohrad	k1gInPc6
a	a	k8xC
stejně	stejně	k6eAd1
jako	jako	k9
Tramín	tramín	k1gInSc1
je	být	k5eAaImIp3nS
pýchou	pýcha	k1gFnSc7
výrobců	výrobce	k1gMnPc2
a	a	k8xC
ozdobou	ozdoba	k1gFnSc7
sklípků	sklípek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Milia	Milia	k1gFnSc1
(	(	kIx(
<g/>
Müller-Thurgau	Müller-Thurgaus	k1gInSc2
x	x	k?
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odpověď	odpověď	k1gFnSc1
slovenských	slovenský	k2eAgMnPc2d1
šlechtitelů	šlechtitel	k1gMnPc2
na	na	k7c4
Pálavu	Pálava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
více	hodně	k6eAd2
muškátová	muškátový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ještě	ještě	k9
více	hodně	k6eAd2
plodná	plodný	k2eAgFnSc1d1
<g/>
,	,	kIx,
i	i	k9
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
zajímavými	zajímavý	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Děvín	Děvín	k1gInSc4
(	(	kIx(
<g/>
Tramín	tramín	k1gInSc4
červený	červený	k2eAgInSc4d1
x	x	k?
Veltlínské	veltlínský	k2eAgNnSc1d1
červenobílé	červenobílý	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vydařená	vydařený	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
ze	z	k7c2
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
charakterem	charakter	k1gInSc7
vín	vína	k1gFnPc2
se	s	k7c7
Tramínu	tramín	k1gInSc6
podobající	podobající	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Aromína	Aromína	k1gFnSc1
(	(	kIx(
<g/>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
x	x	k?
Veltlínské	veltlínský	k2eAgFnSc3d1
červenobílé	červenobílý	k2eAgFnSc3d1
<g/>
)	)	kIx)
x	x	k?
Irsai	Irsai	k1gNnSc2
Oliver	Olivra	k1gFnPc2
<g/>
,	,	kIx,
slovenská	slovenský	k2eAgFnSc1d1
aromatická	aromatický	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Rosa	Rosa	k1gFnSc1
(	(	kIx(
<g/>
Picpoul	Picpoul	k1gInSc1
x	x	k?
Frankovka	frankovka	k1gFnSc1
<g/>
)	)	kIx)
x	x	k?
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
světový	světový	k2eAgInSc1d1
unikát	unikát	k1gInSc1
<g/>
,	,	kIx,
barvířka	barvířka	k1gFnSc1
s	s	k7c7
vůní	vůně	k1gFnSc7
růží	růž	k1gFnPc2
<g/>
,	,	kIx,
slovenská	slovenský	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Traminette	Traminette	k5eAaPmIp2nP
(	(	kIx(
<g/>
Gewürztraminer	Gewürztraminer	k1gInSc1
x	x	k?
Seyval	Seyval	k1gMnSc1
Blanc	Blanc	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
interspecifická	interspecifický	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
,	,	kIx,
vyšlechtěná	vyšlechtěný	k2eAgFnSc1d1
a	a	k8xC
pěstovaná	pěstovaný	k2eAgFnSc1d1
v	v	k7c6
USA	USA	kA
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
vín	víno	k1gNnPc2
<g/>
,	,	kIx,
připomínajících	připomínající	k2eAgMnPc2d1
Tramín	tramín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
odrůdy	odrůda	k1gFnPc4
Würzer	Würzra	k1gFnPc2
<g/>
,	,	kIx,
Zähringer	Zähringra	k1gFnPc2
<g/>
,	,	kIx,
Cserszegi	Cserszegi	k1gNnSc1
Füszeres	Füszeresa	k1gFnPc2
<g/>
,	,	kIx,
Ezerfürtü	Ezerfürtü	k1gFnPc7
<g/>
,	,	kIx,
Dneprovskii	Dneprovskium	k1gNnPc7
Rubin	Rubin	k1gInSc1
<g/>
,	,	kIx,
Riton	Riton	k1gInSc1
atd.	atd.	kA
</s>
<s>
Pěstování	pěstování	k1gNnSc1
</s>
<s>
Réví	réví	k1gNnSc1
vyzrává	vyzrávat	k5eAaImIp3nS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
(	(	kIx(
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
úrodných	úrodný	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
středně	středně	k6eAd1
silný	silný	k2eAgInSc4d1
až	až	k8xS
bujný	bujný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
chudých	chudý	k2eAgFnPc6d1
a	a	k8xC
suchých	suchý	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
roste	růst	k5eAaImIp3nS
slabě	slabě	k6eAd1
a	a	k8xC
degeneruje	degenerovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyžaduje	vyžadovat	k5eAaImIp3nS
pravidelné	pravidelný	k2eAgNnSc1d1
hnojení	hnojení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odolnost	odolnost	k1gFnSc1
vůči	vůči	k7c3
zimním	zimní	k2eAgInPc3d1
mrazům	mráz	k1gInPc3
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
dobrá	dobrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
odrůdou	odrůda	k1gFnSc7
Ryzlink	ryzlink	k1gInSc4
rýnský	rýnský	k2eAgInSc4d1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejodolnější	odolný	k2eAgFnPc4d3
odrůdy	odrůda	k1gFnPc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
první	první	k4xOgInSc4
3-4	3-4	k4
očka	očko	k1gNnPc1
na	na	k7c6
tažni	tažeň	k1gInSc6
nejsou	být	k5eNaImIp3nP
plodná	plodný	k2eAgFnSc1d1
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
jakékoliv	jakýkoliv	k3yIgNnSc1
poškození	poškození	k1gNnSc1
tažňů	tažeň	k1gInPc2
mrazem	mráz	k1gInSc7
výrazněji	výrazně	k6eAd2
poznat	poznat	k5eAaPmF
na	na	k7c6
úrodě	úroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jarní	jarní	k2eAgInPc1d1
mrazíky	mrazík	k1gInPc1
Tramín	tramín	k1gInSc4
prakticky	prakticky	k6eAd1
nepoškozují	poškozovat	k5eNaImIp3nP
<g/>
,	,	kIx,
pokud	pokud	k8xS
však	však	k9
přece	přece	k9
jen	jen	k9
na	na	k7c6
jaře	jaro	k1gNnSc6
zmrzne	zmrznout	k5eAaPmIp3nS
<g/>
,	,	kIx,
ztráty	ztráta	k1gFnPc1
jsou	být	k5eAaImIp3nP
velké	velký	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
podočka	podočka	k1gFnSc1
nejsou	být	k5eNaImIp3nP
skoro	skoro	k6eAd1
vůbec	vůbec	k9
plodná	plodný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sucho	sucho	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
špatným	špatný	k2eAgNnSc7d1
odkvétáním	odkvétání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
malé	malý	k2eAgInPc4d1
hrozny	hrozen	k1gInPc4
a	a	k8xC
neplodná	plodný	k2eNgNnPc4d1
bazální	bazální	k2eAgNnPc4d1
očka	očko	k1gNnPc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
řezat	řezat	k5eAaImF
na	na	k7c4
dva	dva	k4xCgInPc4
dlouhé	dlouhý	k2eAgInPc4d1
<g/>
,	,	kIx,
minimálně	minimálně	k6eAd1
desetiočkové	desetiočkový	k2eAgInPc1d1
tažně	tažeň	k1gInPc1
se	s	k7c7
zatížením	zatížení	k1gNnSc7
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
úrodných	úrodný	k2eAgInPc6d1
klonech	klon	k1gInPc6
8-10	8-10	k4
plodonosných	plodonosný	k2eAgNnPc2d1
oček	očko	k1gNnPc2
na	na	k7c4
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučuje	doporučovat	k5eAaImIp3nS
se	se	k3xPyFc4
vysoké	vysoký	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
s	s	k7c7
větším	veliký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
stařiny	stařina	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
se	se	k3xPyFc4
zlepšuje	zlepšovat	k5eAaImIp3nS
kvalita	kvalita	k1gFnSc1
sklizně	sklizeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnože	podnož	k1gInSc2
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgFnPc4d1
SO-	SO-	k1gFnPc4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
125	#num#	k4
AA	AA	kA
a	a	k8xC
T	T	kA
5	#num#	k4
<g/>
C.	C.	kA
Má	mít	k5eAaImIp3nS
sice	sice	k8xC
malé	malý	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
krátká	krátký	k2eAgNnPc4d1
internodia	internodium	k1gNnPc4
a	a	k8xC
proto	proto	k8xC
hrozí	hrozit	k5eAaImIp3nS
přehuštění	přehuštění	k1gNnSc1
keřů	keř	k1gInPc2
se	s	k7c7
všemi	všecek	k3xTgInPc7
negativními	negativní	k2eAgInPc7d1
následky	následek	k1gInPc7
<g/>
:	:	kIx,
sprchání	sprchání	k1gNnPc1
<g/>
,	,	kIx,
houbové	houbový	k2eAgFnPc1d1
choroby	choroba	k1gFnPc1
<g/>
,	,	kIx,
hnití	hnití	k1gNnSc1
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
pomalejší	pomalý	k2eAgNnSc1d2
dozrávání	dozrávání	k1gNnSc1
<g/>
,	,	kIx,
nižší	nízký	k2eAgFnSc1d2
cukernatost	cukernatost	k1gFnSc1
i	i	k8xC
nižší	nízký	k2eAgFnSc1d2
úrodnost	úrodnost	k1gFnSc1
v	v	k7c6
příštím	příští	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
větší	veliký	k2eAgInPc4d2
vzdušné	vzdušný	k2eAgInPc4d1
tvary	tvar	k1gInPc4
keřů	keř	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
vyšších	vysoký	k2eAgNnPc6d2
vedeních	vedení	k1gNnPc6
v	v	k7c6
širších	široký	k2eAgInPc6d2
sponech	spon	k1gInPc6
a	a	k8xC
důležité	důležitý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
zelené	zelený	k2eAgFnPc1d1
práce	práce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučují	doporučovat	k5eAaImIp3nP
se	se	k3xPyFc4
velké	velký	k2eAgInPc1d1
tvary	tvar	k1gInPc1
s	s	k7c7
větším	veliký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
starého	starý	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
<g/>
,	,	kIx,
čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
zvětšuje	zvětšovat	k5eAaImIp3nS
plodnost	plodnost	k1gFnSc1
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
Tramínu	tramín	k1gInSc3
někdy	někdy	k6eAd1
nedostává	dostávat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
málo	málo	k6eAd1
úrodná	úrodný	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
,	,	kIx,
neselektovaná	selektovaný	k2eNgFnSc1d1
populace	populace	k1gFnSc1
má	mít	k5eAaImIp3nS
úrodnost	úrodnost	k1gFnSc4
4-7	4-7	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
<g/>
,	,	kIx,
i	i	k8xC
u	u	k7c2
klonů	klon	k1gInPc2
se	se	k3xPyFc4
úroda	úroda	k1gFnSc1
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
7-9	7-9	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
je	být	k5eAaImIp3nS
úrodnost	úrodnost	k1gFnSc1
závislá	závislý	k2eAgFnSc1d1
i	i	k9
na	na	k7c6
poloze	poloha	k1gFnSc6
a	a	k8xC
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramín	tramín	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
především	především	k9
producentem	producent	k1gMnSc7
cukru	cukr	k1gInSc2
a	a	k8xC
cukernatost	cukernatost	k1gFnSc1
moštu	mošt	k1gInSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
podle	podle	k7c2
ročníku	ročník	k1gInSc2
nad	nad	k7c7
20	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
hl	hl	k?
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
však	však	k9
nad	nad	k7c7
22	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
hl	hl	k?
(	(	kIx(
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
roce	rok	k1gInSc6
dokáže	dokázat	k5eAaPmIp3nS
Tramín	tramín	k1gInSc4
zcela	zcela	k6eAd1
seschnout	seschnout	k5eAaPmF
na	na	k7c4
hrozinky	hrozinka	k1gFnPc4
a	a	k8xC
i	i	k9
proto	proto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
nejčastější	častý	k2eAgFnSc7d3
odrůdou	odrůda	k1gFnSc7
v	v	k7c6
jakosti	jakost	k1gFnSc6
výběr	výběr	k1gInSc1
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
celou	celý	k2eAgFnSc7d1
jednou	jeden	k4xCgFnSc7
třetinou	třetina	k1gFnSc7
na	na	k7c6
celkovém	celkový	k2eAgInSc6d1
objemu	objem	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acidita	acidita	k1gFnSc1
nebývá	bývat	k5eNaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
6-10	6-10	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nejčastěji	často	k6eAd3
6,5	6,5	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
Tramín	tramín	k1gInSc1
často	často	k6eAd1
sklízí	sklízet	k5eAaImIp3nS
ve	v	k7c6
dvou	dva	k4xCgInPc6
termínech	termín	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
prvního	první	k4xOgMnSc2
<g/>
,	,	kIx,
v	v	k7c6
první	první	k4xOgFnSc6
až	až	k8xS
druhé	druhý	k4xOgFnSc3
dekádě	dekáda	k1gFnSc3
září	září	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
víno	víno	k1gNnSc1
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
druhého	druhý	k4xOgMnSc2
<g/>
,	,	kIx,
v	v	k7c6
polovině	polovina	k1gFnSc6
října	říjen	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
kořenitější	kořenitý	k2eAgMnSc1d2
a	a	k8xC
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
cukru	cukr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
vína	víno	k1gNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
ve	v	k7c6
vhodném	vhodný	k2eAgInSc6d1
poměru	poměr	k1gInSc6
zcelí	zcelet	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
vznikne	vzniknout	k5eAaPmIp3nS
svěží	svěží	k2eAgNnSc1d1
a	a	k8xC
neobyčejně	obyčejně	k6eNd1
buketní	buketní	k2eAgNnSc4d1
víno	víno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Fenologie	fenologie	k1gFnSc1
</s>
<s>
Raší	rašit	k5eAaImIp3nP
mezi	mezi	k7c7
posledními	poslední	k2eAgFnPc7d1
odrůdami	odrůda	k1gFnPc7
a	a	k8xC
pozdě	pozdě	k6eAd1
i	i	k9
kvete	kvést	k5eAaImIp3nS
<g/>
,	,	kIx,
další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
je	být	k5eAaImIp3nS
už	už	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
<g/>
,	,	kIx,
zaměká	zaměkat	k5eAaImIp3nS
spolu	spolu	k6eAd1
se	s	k7c7
středně	středně	k6eAd1
pozdními	pozdní	k2eAgFnPc7d1
odrůdami	odrůda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
hromadí	hromadit	k5eAaImIp3nP
cukr	cukr	k1gInSc4
a	a	k8xC
technologická	technologický	k2eAgFnSc1d1
zralost	zralost	k1gFnSc1
nastává	nastávat	k5eAaImIp3nS
v	v	k7c6
SR	SR	kA
obvykle	obvykle	k6eAd1
začátkem	začátek	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
ČR	ČR	kA
v	v	k7c6
polovině	polovina	k1gFnSc6
října	říjen	k1gInSc2
a	a	k8xC
to	ten	k3xDgNnSc1
při	při	k7c6
vysoké	vysoký	k2eAgFnSc6d1
cukernatosti	cukernatost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozny	hrozen	k1gInPc4
vydrží	vydržet	k5eAaPmIp3nS
dlouho	dlouho	k6eAd1
na	na	k7c6
keřích	keř	k1gInPc6
<g/>
,	,	kIx,
což	což	k9
TČ	tč	kA
předurčuje	předurčovat	k5eAaImIp3nS
k	k	k7c3
výrobě	výroba	k1gFnSc3
přívlastkových	přívlastkový	k2eAgNnPc2d1
vín	víno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Choroby	choroba	k1gFnPc1
a	a	k8xC
škůdci	škůdce	k1gMnPc1
</s>
<s>
Houbovým	houbový	k2eAgFnPc3d1
chorobám	choroba	k1gFnPc3
odolává	odolávat	k5eAaImIp3nS
středně	středně	k6eAd1
<g/>
,	,	kIx,
citlivější	citlivý	k2eAgNnSc1d2
je	být	k5eAaImIp3nS
na	na	k7c4
padlí	padlí	k1gNnSc4
révové	révový	k2eAgFnSc2d1
(	(	kIx(
<g/>
Uncinula	Uncinula	k1gFnSc2
necator	necator	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tlusté	tlustý	k2eAgFnSc3d1
slupce	slupka	k1gFnSc3
bobulí	bobule	k1gFnPc2
hrozny	hrozen	k1gInPc4
nehnijí	hnít	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
k	k	k7c3
virovým	virový	k2eAgFnPc3d1
chorobám	choroba	k1gFnPc3
není	být	k5eNaImIp3nS
zvlášť	zvlášť	k6eAd1
náchylný	náchylný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
a	a	k8xC
půdy	půda	k1gFnPc1
</s>
<s>
Patří	patřit	k5eAaImIp3nS
do	do	k7c2
nejkvalitnějších	kvalitní	k2eAgFnPc2d3
„	„	k?
<g/>
ryzlinkových	ryzlinkový	k2eAgFnPc2d1
<g/>
“	“	k?
poloh	poloha	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
zajištěn	zajištěn	k2eAgInSc4d1
dostatek	dostatek	k1gInSc4
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
dozrává	dozrávat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
polohách	poloha	k1gFnPc6
horších	zlý	k2eAgFnPc6d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
chladnějších	chladný	k2eAgFnPc6d2
a	a	k8xC
větrných	větrný	k2eAgNnPc6d1
stanovištích	stanoviště	k1gNnPc6
nespolehlivě	spolehlivě	k6eNd1
odkvétá	odkvétat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
v	v	k7c6
oblibě	obliba	k1gFnSc6
hluboké	hluboký	k2eAgFnSc2d1
<g/>
,	,	kIx,
výživné	výživný	k2eAgFnSc2d1
<g/>
,	,	kIx,
záhřevné	záhřevný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dobře	dobře	k6eAd1
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mělkých	mělký	k2eAgFnPc6d1
<g/>
,	,	kIx,
chudých	chudý	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
i	i	k8xC
na	na	k7c6
velmi	velmi	k6eAd1
vápenatých	vápenatý	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
roste	růst	k5eAaImIp3nS
slabě	slabě	k6eAd1
<g/>
,	,	kIx,
keře	keř	k1gInPc1
degenerují	degenerovat	k5eAaBmIp3nP
a	a	k8xC
trpí	trpět	k5eAaImIp3nP
chlorózou	chloróza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
lehkých	lehký	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
jsou	být	k5eAaImIp3nP
vína	víno	k1gNnPc1
netypická	typický	k2eNgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Víno	víno	k1gNnSc1
</s>
<s>
Long	Long	k1gMnSc1
Nose	nos	k1gInSc6
<g/>
,	,	kIx,
láhev	láhev	k1gFnSc1
jihoafrického	jihoafrický	k2eAgInSc2d1
tramínu	tramín	k1gInSc2
se	s	k7c7
sklenkou	sklenka	k1gFnSc7
</s>
<s>
Technologicky	technologicky	k6eAd1
je	být	k5eAaImIp3nS
Tramín	tramín	k1gInSc1
náročný	náročný	k2eAgInSc1d1
<g/>
,	,	kIx,
mošt	mošt	k1gInSc1
mívá	mívat	k5eAaImIp3nS
menší	malý	k2eAgFnSc4d2
aciditu	acidita	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zachovat	zachovat	k5eAaPmF
a	a	k8xC
pomocí	pomocí	k7c2
vyšších	vysoký	k2eAgFnPc2d2
dávek	dávka	k1gFnPc2
síry	síra	k1gFnSc2
uchovat	uchovat	k5eAaPmF
víno	víno	k1gNnSc4
svěží	svěží	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Tramínu	tramín	k1gInSc6
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
požírač	požírač	k1gMnSc1
síry	síra	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ponechat	ponechat	k5eAaPmF
dostatečné	dostatečný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
zbytkového	zbytkový	k2eAgInSc2d1
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
harmonii	harmonie	k1gFnSc3
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
Tramínu	tramín	k1gInSc2
cukr	cukr	k1gInSc1
„	„	k?
<g/>
sluší	slušet	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastější	častý	k2eAgFnSc7d3
chybou	chyba	k1gFnSc7
u	u	k7c2
Tramínu	tramín	k1gInSc2
je	být	k5eAaImIp3nS
nedostatek	nedostatek	k1gInSc1
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
víno	víno	k1gNnSc1
pak	pak	k6eAd1
není	být	k5eNaImIp3nS
svěží	svěží	k2eAgNnSc1d1
<g/>
,	,	kIx,
převáží	převážet	k5eAaImIp3nS,k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc4
plnost	plnost	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
těžké	těžký	k2eAgNnSc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
až	až	k6eAd1
mdlé	mdlý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
Tramínem	tramín	k1gInSc7
z	z	k7c2
pozdních	pozdní	k2eAgFnPc2d1
sklizní	sklizeň	k1gFnPc2
s	s	k7c7
velmi	velmi	k6eAd1
vysokým	vysoký	k2eAgInSc7d1
zbytkovým	zbytkový	k2eAgInSc7d1
cukrem	cukr	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
bývá	bývat	k5eAaImIp3nS
obzvláště	obzvláště	k6eAd1
těžký	těžký	k2eAgInSc1d1
a	a	k8xC
ceněný	ceněný	k2eAgInSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
s	s	k7c7
víny	víno	k1gNnPc7
slámovými	slámový	k2eAgMnPc7d1
a	a	k8xC
s	s	k7c7
botrytickými	botrytický	k2eAgInPc7d1
výběry	výběr	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
přidávají	přidávat	k5eAaImIp3nP
tóny	tón	k1gInPc4
ušlechtilé	ušlechtilý	k2eAgFnSc2d1
plísně	plíseň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
setkáme	setkat	k5eAaPmIp1nP
i	i	k9
s	s	k7c7
Tramínem	tramín	k1gInSc7
zcela	zcela	k6eAd1
suchým	suchý	k2eAgInSc7d1
<g/>
,	,	kIx,
lehčím	lehký	k2eAgInSc7d2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
pít	pít	k5eAaImF
mladý	mladý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
snáze	snadno	k6eAd2
srovnává	srovnávat	k5eAaImIp3nS
s	s	k7c7
víny	vína	k1gFnPc1
jiných	jiný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
a	a	k8xC
více	hodně	k6eAd2
vyhovuje	vyhovovat	k5eAaImIp3nS
požadavkům	požadavek	k1gInPc3
obchodníků	obchodník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramín	tramín	k1gInSc1
výrazně	výrazně	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
stanoviště	stanoviště	k1gNnPc4
a	a	k8xC
tak	tak	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
typické	typický	k2eAgNnSc4d1
aroma	aroma	k1gNnSc4
<g/>
,	,	kIx,
ještě	ještě	k9
do	do	k7c2
něj	on	k3xPp3gMnSc2
přispívá	přispívat	k5eAaImIp3nS
nezaměnitelnými	zaměnitelný	k2eNgInPc7d1
tóny	tón	k1gInPc7
terroiru	terroir	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
nikdy	nikdy	k6eAd1
stejný	stejný	k2eAgInSc1d1
a	a	k8xC
nezevšední	zevšednit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
léčitelství	léčitelství	k1gNnSc6
je	být	k5eAaImIp3nS
Tramín	tramín	k1gInSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
jiné	jiný	k2eAgFnSc2d1
aromatické	aromatický	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
<g/>
,	,	kIx,
používán	používán	k2eAgMnSc1d1
k	k	k7c3
léčení	léčení	k1gNnSc3
horních	horní	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
dýchacích	dýchací	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Vína	víno	k1gNnPc1
nejsou	být	k5eNaImIp3nP
určena	určit	k5eAaPmNgNnP
pro	pro	k7c4
běžnou	běžný	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
vzácné	vzácný	k2eAgFnPc4d1
a	a	k8xC
slavnostní	slavnostní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypije	vypít	k5eAaPmIp3nS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
většinou	většinou	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
vín	víno	k1gNnPc2
z	z	k7c2
méně	málo	k6eAd2
aromatických	aromatický	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
často	často	k6eAd1
přímo	přímo	k6eAd1
pobízejí	pobízet	k5eAaImIp3nP
k	k	k7c3
pití	pití	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramín	tramín	k1gInSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
i	i	k9
poměrně	poměrně	k6eAd1
snadno	snadno	k6eAd1
přepije	přepít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vína	víno	k1gNnPc1
lze	lze	k6eAd1
často	často	k6eAd1
poznat	poznat	k5eAaPmF
už	už	k9
podle	podle	k7c2
jejich	jejich	k3xOp3gNnSc2
zabarvení	zabarvení	k1gNnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zlatožlutá	zlatožlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
intenzivnější	intenzivní	k2eAgFnPc1d2
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
jasnými	jasný	k2eAgInPc7d1
zlatavými	zlatavý	k2eAgInPc7d1
odlesky	odlesk	k1gInPc7
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
s	s	k7c7
jemným	jemný	k2eAgInSc7d1
růžovým	růžový	k2eAgInSc7d1
reflexem	reflex	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mívají	mívat	k5eAaImIp3nP
vyšší	vysoký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
alkoholu	alkohol	k1gInSc2
a	a	k8xC
glycerolu	glycerol	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
zakroužení	zakroužení	k1gNnSc6
ve	v	k7c6
sklence	sklenka	k1gFnSc6
zanechávají	zanechávat	k5eAaImIp3nP
výrazné	výrazný	k2eAgInPc1d1
oblouky	oblouk	k1gInPc1
(	(	kIx(
<g/>
mosty	most	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgFnSc1d2
viskozita	viskozita	k1gFnSc1
vína	víno	k1gNnSc2
ve	v	k7c6
sklence	sklenka	k1gFnSc6
a	a	k8xC
kapka	kapka	k1gFnSc1
<g/>
,	,	kIx,
rozetřená	rozetřený	k2eAgFnSc1d1
mezi	mezi	k7c4
bříšky	bříšek	k1gInPc4
prstů	prst	k1gInPc2
svědčí	svědčit	k5eAaImIp3nP
o	o	k7c6
olejnatosti	olejnatost	k1gFnSc6
vín	víno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
tramíny	tramín	k1gInPc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
aroma	aroma	k1gNnSc7
exotického	exotický	k2eAgNnSc2d1
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
liči	liči	k1gNnSc1
<g/>
,	,	kIx,
mango	mango	k1gNnSc1
či	či	k8xC
papája	papája	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
mísí	mísit	k5eAaImIp3nP
květnaté	květnatý	k2eAgInPc1d1
tóny	tón	k1gInPc1
muškátů	muškát	k1gInPc2
a	a	k8xC
růží	růž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chuť	chuť	k1gFnSc1
je	být	k5eAaImIp3nS
těžká	těžký	k2eAgFnSc1d1
<g/>
,	,	kIx,
plná	plný	k2eAgFnSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
kořenná	kořenný	k2eAgFnSc1d1
<g/>
,	,	kIx,
extraktivní	extraktivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
kyselostí	kyselost	k1gFnSc7
a	a	k8xC
s	s	k7c7
dominantním	dominantní	k2eAgInSc7d1
„	„	k?
<g/>
parfémovým	parfémový	k2eAgFnPc3d1
<g/>
“	“	k?
aroma	aroma	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocit	pocit	k1gInSc1
na	na	k7c6
patře	patro	k1gNnSc6
a	a	k8xC
v	v	k7c6
ústech	ústa	k1gNnPc6
je	být	k5eAaImIp3nS
jemný	jemný	k2eAgInSc4d1
<g/>
,	,	kIx,
plný	plný	k2eAgInSc4d1
<g/>
,	,	kIx,
bohatý	bohatý	k2eAgInSc4d1
<g/>
,	,	kIx,
hřejivě	hřejivě	k6eAd1
alkoholický	alkoholický	k2eAgInSc1d1
<g/>
,	,	kIx,
hutný	hutný	k2eAgInSc1d1
až	až	k6eAd1
olejnatý	olejnatý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochuť	dochutit	k5eAaPmRp2nS
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
příjemná	příjemný	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Typová	typový	k2eAgNnPc4d1
vína	víno	k1gNnPc4
Tramínu	tramín	k1gInSc2
jsou	být	k5eAaImIp3nP
ve	v	k7c4
vůni	vůně	k1gFnSc4
osobitá	osobitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
rozpoznatelná	rozpoznatelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
intenzívní	intenzívní	k2eAgInPc4d1
kořenitě	kořenitě	k6eAd1
medové	medový	k2eAgInPc4d1
až	až	k9
bezové	bezový	k2eAgFnSc2d1
vůně	vůně	k1gFnSc2
a	a	k8xC
koncentrované	koncentrovaný	k2eAgFnSc2d1
<g/>
,	,	kIx,
kořenité	kořenitý	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vůni	vůně	k1gFnSc6
a	a	k8xC
chuti	chuť	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
hledat	hledat	k5eAaImF
čajovou	čajový	k2eAgFnSc4d1
růži	růže	k1gFnSc4
<g/>
,	,	kIx,
koření	koření	k1gNnSc4
<g/>
,	,	kIx,
med	med	k1gInSc4
<g/>
,	,	kIx,
hrozinky	hrozinka	k1gFnPc4
<g/>
,	,	kIx,
tropické	tropický	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
kompotované	kompotovaný	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
meruňky	meruňka	k1gFnPc4
<g/>
,	,	kIx,
květ	květ	k1gInSc4
fialky	fialka	k1gFnSc2
či	či	k8xC
pivoňky	pivoňka	k1gFnSc2
<g/>
,	,	kIx,
zázvor	zázvor	k1gInSc4
<g/>
,	,	kIx,
skořici	skořice	k1gFnSc4
<g/>
,	,	kIx,
lékořici	lékořice	k1gFnSc4
<g/>
,	,	kIx,
karamel	karamel	k1gInSc4
<g/>
,	,	kIx,
liči	liči	k1gNnSc4
<g/>
,	,	kIx,
mango	mango	k1gNnSc4
<g/>
,	,	kIx,
mučenku	mučenka	k1gFnSc4
<g/>
,	,	kIx,
grapefruit	grapefruit	k1gInSc1
<g/>
,	,	kIx,
květ	květ	k1gInSc1
černého	černý	k2eAgInSc2d1
bezu	bez	k1gInSc2
<g/>
.	.	kIx.
ale	ale	k8xC
někdy	někdy	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
přidat	přidat	k5eAaPmF
také	také	k9
muškátové	muškátový	k2eAgInPc4d1
tóny	tón	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vína	víno	k1gNnPc1
jsou	být	k5eAaImIp3nP
charakterizována	charakterizován	k2eAgNnPc1d1
jako	jako	k8xS,k8xC
silná	silný	k2eAgFnSc1d1
<g/>
,	,	kIx,
alkohol	alkohol	k1gInSc1
není	být	k5eNaImIp3nS
v	v	k7c6
rovnováze	rovnováha	k1gFnSc6
s	s	k7c7
kyselinami	kyselina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodnost	vhodnost	k1gFnSc1
ke	k	k7c3
skladování	skladování	k1gNnSc3
je	být	k5eAaImIp3nS
průměrná	průměrný	k2eAgFnSc1d1
až	až	k6eAd1
dobrá	dobrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
víno	víno	k1gNnSc1
vyšší	vysoký	k2eAgFnSc4d2
aciditu	acidita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ležením	ležení	k1gNnSc7
v	v	k7c6
láhvi	láhev	k1gFnSc6
se	se	k3xPyFc4
aroma	aroma	k1gNnSc1
zušlechťuje	zušlechťovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Tramín	tramín	k1gInSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
našich	náš	k3xOp1gFnPc2
nejzajímavějších	zajímavý	k2eAgFnPc2d3
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
přímo	přímo	k6eAd1
předurčenou	předurčený	k2eAgFnSc7d1
pro	pro	k7c4
naše	naši	k1gMnPc4
severní	severní	k2eAgFnSc2d1
vinohradnické	vinohradnický	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednak	jednak	k8xC
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
nezaměnitelné	zaměnitelný	k2eNgNnSc1d1
víno	víno	k1gNnSc1
zcela	zcela	k6eAd1
výjimečné	výjimečný	k2eAgNnSc1d1
a	a	k8xC
nedaří	dařit	k5eNaImIp3nS
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
nahradit	nahradit	k5eAaPmF
jinou	jiný	k2eAgFnSc7d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
možno	možno	k6eAd1
plodnější	plodný	k2eAgMnSc1d2
a	a	k8xC
na	na	k7c4
pěstování	pěstování	k1gNnSc4
příjemnější	příjemný	k2eAgFnSc7d2
odrůdou	odrůda	k1gFnSc7
<g/>
,	,	kIx,
jednak	jednak	k8xC
nám	my	k3xPp1nPc3
dal	dát	k5eAaPmAgMnS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
skvělých	skvělý	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
ještě	ještě	k9
nějaké	nějaký	k3yIgInPc1
vyšlechtěny	vyšlechtěn	k2eAgInPc1d1
budou	být	k5eAaImBp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
koštech	košt	k1gInPc6
je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc7
odrůdou	odrůda	k1gFnSc7
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dojde	dojít	k5eAaPmIp3nS
<g/>
,	,	kIx,
hned	hned	k6eAd1
po	po	k7c6
Pálavě	pálavě	k6eAd1
<g/>
,	,	kIx,
zásluhu	zásluha	k1gFnSc4
na	na	k7c6
tom	ten	k3xDgNnSc6
mají	mít	k5eAaImIp3nP
v	v	k7c6
nemalé	malý	k2eNgFnSc6d1
míře	míra	k1gFnSc6
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
atraktivní	atraktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
ochutnávkách	ochutnávka	k1gFnPc6
ve	v	k7c6
sklípku	sklípek	k1gInSc6
na	na	k7c4
něj	on	k3xPp3gMnSc4
často	často	k6eAd1
ani	ani	k8xC
nedojde	dojít	k5eNaPmIp3nS
<g/>
,	,	kIx,
přestože	přestože	k8xS
víte	vědět	k5eAaImIp2nP
o	o	k7c4
v	v	k7c6
koutku	koutek	k1gInSc6
zastrčeném	zastrčený	k2eAgInSc6d1
demižonku	demižonek	k1gInSc6
<g/>
,	,	kIx,
vinař	vinař	k1gMnSc1
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc2
nechá	nechat	k5eAaPmIp3nS
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
nebo	nebo	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ještě	ještě	k6eAd1
vylepšil	vylepšit	k5eAaPmAgMnS
dojem	dojem	k1gInSc4
a	a	k8xC
zanechal	zanechat	k5eAaPmAgInS
výraznou	výrazný	k2eAgFnSc4d1
vzpomínku	vzpomínka	k1gFnSc4
<g/>
,	,	kIx,
nabídne	nabídnout	k5eAaPmIp3nS
ho	on	k3xPp3gInSc4
jako	jako	k8xC,k8xS
klenot	klenot	k1gInSc4
ke	k	k7c3
konci	konec	k1gInSc3
ochutnávky	ochutnávka	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
srovnání	srovnání	k1gNnSc1
s	s	k7c7
Tramínem	tramín	k1gInSc7
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
zbytkovým	zbytkový	k2eAgInSc7d1
cukrem	cukr	k1gInSc7
<g/>
,	,	kIx,
by	by	kYmCp3nP
ostatní	ostatní	k2eAgFnPc1d1
odrůdy	odrůda	k1gFnPc1
příliš	příliš	k6eAd1
poškodilo	poškodit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
plocha	plocha	k1gFnSc1
<g/>
,	,	kIx,
osazená	osazený	k2eAgFnSc1d1
Tramínem	tramín	k1gInSc7
<g/>
,	,	kIx,
časem	čas	k1gInSc7
ještě	ještě	k9
mírně	mírně	k6eAd1
stoupne	stoupnout	k5eAaPmIp3nS
a	a	k8xC
Tramín	tramín	k1gInSc1
si	se	k3xPyFc3
to	ten	k3xDgNnSc1
i	i	k9
přes	přes	k7c4
nižší	nízký	k2eAgFnSc4d2
úrodnost	úrodnost	k1gFnSc4
a	a	k8xC
vyšší	vysoký	k2eAgFnSc4d2
pracnost	pracnost	k1gFnSc4
jistě	jistě	k9
zaslouží	zasloužit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
nadarmo	nadarmo	k6eAd1
se	se	k3xPyFc4
ve	v	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Modre	Modr	k1gInSc5
traduje	tradovat	k5eAaImIp3nS
přísloví	přísloví	k1gNnSc1
:	:	kIx,
Ryvola	Ryvola	k1gFnSc1
<g/>
,	,	kIx,
kupca	kupca	k?
privolá	privolat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Stolování	stolování	k1gNnSc1
</s>
<s>
Vína	vína	k1gFnSc1
hlouběji	hluboko	k6eAd2
vychlazená	vychlazený	k2eAgFnSc1d1
lze	lze	k6eAd1
podávat	podávat	k5eAaImF
jako	jako	k8xS,k8xC
sklenku	sklenka	k1gFnSc4
aperitivu	aperitiv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
nabízejí	nabízet	k5eAaImIp3nP
jako	jako	k8xS,k8xC
doprovod	doprovod	k1gInSc1
předkrmů	předkrm	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
paštiky	paštika	k1gFnPc4
z	z	k7c2
husích	husí	k2eAgNnPc2d1
jater	játra	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suché	Suché	k2eAgInPc1d1
Tramíny	tramín	k1gInPc1
se	se	k3xPyFc4
doporučují	doporučovat	k5eAaImIp3nP
k	k	k7c3
cibulovému	cibulový	k2eAgInSc3d1
koláči	koláč	k1gInSc3
nebo	nebo	k8xC
k	k	k7c3
uzeným	uzený	k2eAgFnPc3d1
rybám	ryba	k1gFnPc3
<g/>
,	,	kIx,
k	k	k7c3
úhoři	úhoř	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sladké	Sladké	k2eAgInPc1d1
výběry	výběr	k1gInPc1
se	s	k7c7
zbytkem	zbytek	k1gInSc7
neprokvašeného	prokvašený	k2eNgInSc2d1
cukru	cukr	k1gInSc2
se	se	k3xPyFc4
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
sýrům	sýr	k1gInPc3
s	s	k7c7
modrou	modrý	k2eAgFnSc7d1
plísní	plíseň	k1gFnSc7
nebo	nebo	k8xC
k	k	k7c3
ovčím	ovčí	k2eAgInPc3d1
či	či	k8xC
kozím	kozí	k2eAgInPc3d1
sýrům	sýr	k1gInPc3
v	v	k7c6
závěru	závěr	k1gInSc6
stolování	stolování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
neobvyklému	obvyklý	k2eNgNnSc3d1
aroma	aroma	k1gNnSc3
a	a	k8xC
plnosti	plnost	k1gFnSc3
chuti	chuť	k1gFnSc2
po	po	k7c6
liči	liči	k1gNnSc6
patří	patřit	k5eAaImIp3nS
Tramín	tramín	k1gInSc1
k	k	k7c3
těm	ten	k3xDgMnPc3
málo	málo	k6eAd1
vínům	víno	k1gNnPc3
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
hodí	hodit	k5eAaPmIp3nP,k5eAaImIp3nP
ke	k	k7c3
kořeněnému	kořeněný	k2eAgNnSc3d1
jídlu	jídlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
lze	lze	k6eAd1
vína	víno	k1gNnSc2
Tramínu	tramín	k1gInSc2
podávat	podávat	k5eAaImF
i	i	k9
k	k	k7c3
některým	některý	k3yIgMnPc3
exotickým	exotický	k2eAgMnPc3d1
<g/>
,	,	kIx,
silně	silně	k6eAd1
kořeněným	kořeněný	k2eAgInPc3d1
pokrmům	pokrm	k1gInPc3
asijské	asijský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
ke	k	k7c3
sladkým	sladký	k2eAgFnPc3d1
úpravám	úprava	k1gFnPc3
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
sladké	sladký	k2eAgInPc1d1
Tramíny	tramín	k1gInPc1
se	se	k3xPyFc4
často	často	k6eAd1
podávají	podávat	k5eAaImIp3nP
na	na	k7c4
závěr	závěr	k1gInSc4
stolování	stolování	k1gNnPc2
ke	k	k7c3
sladkým	sladký	k2eAgInPc3d1
zákuskům	zákusek	k1gInPc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
tvarohový	tvarohový	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
švestkový	švestkový	k2eAgInSc1d1
koláč	koláč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Origine	Origin	k1gInSc5
del	del	k?
Gewürztraminer	Gewürztraminero	k1gNnPc2
:	:	kIx,
il	il	k?
DNA	dno	k1gNnSc2
incontra	incontr	k1gMnSc2
la	la	k1gNnSc2
storia	storium	k1gNnSc2
<g/>
,	,	kIx,
PDF	PDF	kA
Dr	dr	kA
José	José	k1gNnSc4
Vouillamoz	Vouillamoz	k1gInSc1
<g/>
,	,	kIx,
Università	Università	k1gMnSc1
di	di	k?
Neuchâtel	Neuchâtel	k1gMnSc1
<g/>
,	,	kIx,
Svizzera	Svizzera	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
↑	↑	k?
KOVÁŘ	Kovář	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VITIS	VITIS	kA
VINIFERA	VINIFERA	kA
L.	L.	kA
–	–	k?
réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
/	/	kIx~
vinič	vinič	k1gMnSc1
hroznorodý	hroznorodý	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botany	Botana	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2008-01-22	2008-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Origine	Origin	k1gInSc5
del	del	k?
Gewürztraminer	Gewürztraminero	k1gNnPc2
:	:	kIx,
il	il	k?
DNA	dno	k1gNnSc2
incontra	incontr	k1gMnSc2
la	la	k1gNnSc2
storia	storium	k1gNnSc2
<g/>
,	,	kIx,
PDF	PDF	kA
Dr	dr	kA
José	José	k1gNnSc4
Vouillamoz	Vouillamoz	k1gInSc1
<g/>
,	,	kIx,
Università	Università	k1gMnSc1
di	di	k?
Neuchâtel	Neuchâtel	k1gMnSc1
<g/>
,	,	kIx,
Svizzera	Svizzera	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
↑	↑	k?
Státní	státní	k2eAgFnSc1d1
odrůdová	odrůdový	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
vydaná	vydaný	k2eAgFnSc1d1
roku	rok	k1gInSc2
2016	#num#	k4
http://eagri.cz/public/web/file/408615/_32016.pdf	http://eagri.cz/public/web/file/408615/_32016.pdf	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
21	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Plant	planta	k1gFnPc2
grape	grape	k?
<g/>
,	,	kIx,
Le	Le	k1gMnSc1
catalogue	catalogue	k1gNnSc2
des	des	k1gNnSc2
vignes	vignes	k1gMnSc1
cultivées	cultivées	k1gMnSc1
en	en	k?
France	Franc	k1gMnSc4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
Foffová	Foffový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
Vurm	Vurm	k1gMnSc1
<g/>
,	,	kIx,
Dáša	Dáša	k1gFnSc1
Krausová	Krausová	k1gFnSc1
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
českého	český	k2eAgNnSc2d1
a	a	k8xC
moravského	moravský	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praga	Praga	k1gFnSc1
Mystica	Mystica	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86767	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pierre	Pierr	k1gMnSc5
Galet	Galet	k?
:	:	kIx,
Dictionnaire	Dictionnair	k1gMnSc5
encyclopédique	encyclopédiquus	k1gMnSc5
des	des	k1gNnSc3
cépages	cépages	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hachette	Hachett	k1gInSc5
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
2000	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
236331	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jancis	Jancis	k1gFnSc1
Robinson	Robinson	k1gMnSc1
:	:	kIx,
Das	Das	k1gFnSc1
Oxford	Oxford	k1gInSc1
Weinlexikon	Weinlexikon	k1gInSc1
<g/>
,	,	kIx,
Hallwag	Hallwag	k1gInSc1
<g/>
,	,	kIx,
Gräfe	Gräf	k1gInSc5
und	und	k?
Unzer	Unzer	k1gInSc1
<g/>
,	,	kIx,
München	München	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8338	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
691	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Walter	Walter	k1gMnSc1
Hillebrand	Hillebranda	k1gFnPc2
<g/>
,	,	kIx,
Heinz	Heinz	k1gMnSc1
Lott	Lott	k1gMnSc1
<g/>
,	,	kIx,
Franz	Franz	k1gMnSc1
Pfaff	Pfaff	k1gMnSc1
:	:	kIx,
Taschenbuch	Taschenbuch	k1gMnSc1
der	drát	k5eAaImRp2nS
Rebsorten	Rebsortno	k1gNnPc2
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auflage	Auflag	k1gInSc2
<g/>
,	,	kIx,
Fachverlag	Fachverlag	k1gMnSc1
Fraund	Fraund	k1gMnSc1
<g/>
,	,	kIx,
Mainz	Mainz	k1gMnSc1
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
921156	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Multimédia	multimédium	k1gNnPc1
</s>
<s>
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Sotolář	Sotolář	k1gMnSc1
:	:	kIx,
Multimediální	multimediální	k2eAgInSc1d1
atlas	atlas	k1gInSc1
podnožových	podnožový	k2eAgFnPc2d1
<g/>
,	,	kIx,
moštových	moštový	k2eAgFnPc2d1
a	a	k8xC
stolních	stolní	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
<g/>
,	,	kIx,
Mendelova	Mendelův	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
a	a	k8xC
lesnická	lesnický	k2eAgFnSc1d1
universita	universita	k1gFnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
zahradnická	zahradnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Lednici	Lednice	k1gFnSc6
</s>
<s>
Martin	Martin	k1gMnSc1
Šimek	Šimek	k1gMnSc1
:	:	kIx,
Encyklopédie	Encyklopédie	k1gFnSc1
všemožnejch	všemožnejch	k?
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
z	z	k7c2
celýho	celýho	k?
světa	svět	k1gInSc2
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
již	již	k6eAd1
ouplně	ouplně	k6eAd1
vymizely	vymizet	k5eAaPmAgFnP
<g/>
,	,	kIx,
2008-2012	2008-2012	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4329078-4	4329078-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2016002057	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2016002057	#num#	k4
</s>
