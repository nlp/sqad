<s>
Jazyk	jazyk	k1gInSc1
(	(	kIx(
<g/>
orgán	orgán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Faktická	faktický	k2eAgFnSc1d1
přesnost	přesnost	k1gFnSc1
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
byla	být	k5eAaImAgFnS
zpochybněna	zpochybnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgNnPc1d2
zdůvodnění	zdůvodnění	k1gNnPc1
najdete	najít	k5eAaPmIp2nP
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
neodstraňujte	odstraňovat	k5eNaImRp2nP
tuto	tento	k3xDgFnSc4
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
nebudou	být	k5eNaImBp3nP
pochybnosti	pochybnost	k1gFnPc1
vyřešeny	vyřešen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vkladatele	vkladatel	k1gMnPc4
šablony	šablona	k1gFnSc2
<g/>
:	:	kIx,
Na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
zdůvodněte	zdůvodnit	k5eAaPmRp2nP
vložení	vložení	k1gNnSc4
šablony	šablona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jazyk	jazyk	k1gInSc1
</s>
<s>
Lidský	lidský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
Lidský	lidský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
Latinsky	latinsky	k6eAd1
</s>
<s>
lingua	lingua	k6eAd1
</s>
<s>
Žíly	žíla	k1gFnPc1
</s>
<s>
v.	v.	k?
lingualis	lingualis	k1gInSc1
</s>
<s>
Nervy	nerv	k1gInPc1
</s>
<s>
n.	n.	k?
lingualis	lingualis	k1gInSc1
</s>
<s>
Gray	Graa	k1gFnPc1
</s>
<s>
1125	#num#	k4
</s>
<s>
Jazyk	jazyk	k1gInSc1
je	být	k5eAaImIp3nS
svalový	svalový	k2eAgInSc1d1
orgán	orgán	k1gInSc1
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
ústní	ústní	k2eAgFnSc6d1
dutině	dutina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
sliznice	sliznice	k1gFnSc2
je	být	k5eAaImIp3nS
kryta	krýt	k5eAaImNgFnS
mnohovrstevným	mnohovrstevný	k2eAgInSc7d1
dlaždicovým	dlaždicový	k2eAgInSc7d1
epitelem	epitel	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
četné	četný	k2eAgInPc1d1
výběžky	výběžek	k1gInPc1
(	(	kIx(
<g/>
papily	papila	k1gFnPc1
<g/>
)	)	kIx)
dodávají	dodávat	k5eAaImIp3nP
jazyku	jazyk	k1gInSc3
zvláštní	zvláštní	k2eAgInSc1d1
matný	matný	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
člověka	člověk	k1gMnSc2
jeho	jeho	k3xOp3gNnSc1
velká	velký	k2eAgFnSc1d1
ohebnost	ohebnost	k1gFnSc1
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
přesným	přesný	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
umožňuje	umožňovat	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
zvuků	zvuk	k1gInPc2
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
lidé	člověk	k1gMnPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
při	při	k7c6
mluvení	mluvení	k1gNnSc4
lidskou	lidský	k2eAgFnSc7d1
řečí	řeč	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaly	sval	k1gInPc1
jazyka	jazyk	k1gInSc2
vytvářejí	vytvářet	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
jazyka	jazyk	k1gInSc2
a	a	k8xC
dají	dát	k5eAaPmIp3nP
se	se	k3xPyFc4
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
vlastní	vlastní	k2eAgInPc4d1
svaly	sval	k1gInPc4
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
intraglosální	intraglosálnit	k5eAaPmIp3nP
<g/>
)	)	kIx)
a	a	k8xC
vnější	vnější	k2eAgInPc4d1
svaly	sval	k1gInPc4
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
extraglosální	extraglosální	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nejsilnější	silný	k2eAgInSc4d3
sval	sval	k1gInSc4
v	v	k7c6
těle	tělo	k1gNnSc6
<g/>
,	,	kIx,
ničím	ničit	k5eAaImIp1nS
to	ten	k3xDgNnSc1
však	však	k9
není	být	k5eNaImIp3nS
podloženo	podložit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
neexistují	existovat	k5eNaImIp3nP
objektivní	objektivní	k2eAgNnPc4d1
kritéria	kritérion	k1gNnPc4
<g/>
,	,	kIx,
podle	podle	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
síla	síla	k1gFnSc1
svalu	sval	k1gInSc2
dala	dát	k5eAaPmAgFnS
měřit	měřit	k5eAaImF
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc1
ani	ani	k8xC
není	být	k5eNaImIp3nS
jen	jen	k9
jedním	jeden	k4xCgInSc7
svalem	sval	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Jazyk	jazyk	k1gInSc1
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
chuťových	chuťový	k2eAgInPc2d1
receptorů	receptor	k1gInPc2
<g/>
,	,	kIx,
zvaných	zvaný	k2eAgInPc2d1
chuťové	chuťový	k2eAgInPc4d1
pohárky	pohárek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
rozlišuje	rozlišovat	k5eAaImIp3nS
čtyři	čtyři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
chutě	chuť	k1gFnPc4
<g/>
:	:	kIx,
slanou	slaný	k2eAgFnSc4d1
<g/>
,	,	kIx,
sladkou	sladký	k2eAgFnSc4d1
<g/>
,	,	kIx,
kyselou	kyselý	k2eAgFnSc4d1
a	a	k8xC
hořkou	hořký	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
uznávána	uznáván	k2eAgFnSc1d1
i	i	k8xC
další	další	k2eAgFnSc1d1
<g/>
,	,	kIx,
tzn.	tzn.	kA
pátá	pátá	k1gFnSc1
chuť	chuť	k1gFnSc1
umami	uma	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
hovoří	hovořit	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
dalších	další	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s>
Evoluce	evoluce	k1gFnSc1
</s>
<s>
Jazyk	jazyk	k1gInSc1
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
u	u	k7c2
obratlovců	obratlovec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
přešli	přejít	k5eAaPmAgMnP
na	na	k7c4
souš	souš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obratlovci	obratlovec	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
ryby	ryba	k1gFnPc4
<g/>
)	)	kIx)
zřejmě	zřejmě	k6eAd1
jazyk	jazyk	k1gInSc4
nepotřebovali	potřebovat	k5eNaImAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
ve	v	k7c6
vodním	vodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
potravu	potrava	k1gFnSc4
nadnáší	nadnášet	k5eAaImIp3nS
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
ze	z	k7c2
svalů	sval	k1gInPc2
ve	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
ústní	ústní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Anatomická	anatomický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
jazyk	jazyk	k1gInSc4
a	a	k8xC
ústní	ústní	k2eAgFnSc4d1
dutinu	dutina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gray	Graa	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Anatomy	anatom	k1gMnPc7
<g/>
,	,	kIx,
1918	#num#	k4
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1
část	část	k1gFnSc1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
facies	facies	k1gFnSc1
inferior	inferior	k1gMnSc1
linguae	linguae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
jazyku	jazyk	k1gInSc6
rozeznáváme	rozeznávat	k5eAaImIp1nP
:	:	kIx,
</s>
<s>
kořen	kořen	k1gInSc1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
radix	radix	k1gInSc1
linguae	lingua	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
část	část	k1gFnSc1
obrácená	obrácený	k2eAgFnSc1d1
do	do	k7c2
hltanu	hltan	k1gInSc2
</s>
<s>
tělo	tělo	k1gNnSc1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
corpus	corpus	k1gInSc1
linguae	lingua	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
;	;	kIx,
při	při	k7c6
zavřených	zavřený	k2eAgNnPc6d1
ústech	ústa	k1gNnPc6
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
o	o	k7c4
patro	patro	k1gNnSc4
</s>
<s>
hrot	hrot	k1gInSc1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
apex	apex	k1gInSc1
linguae	lingua	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
pohyblivá	pohyblivý	k2eAgFnSc1d1
přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Dále	daleko	k6eAd2
se	se	k3xPyFc4
popisují	popisovat	k5eAaImIp3nP
:	:	kIx,
</s>
<s>
hřbet	hřbet	k1gInSc4
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
dorsum	dorsum	k1gInSc1
linguae	linguaat	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
<g/>
:	:	kIx,
</s>
<s>
sulcus	sulcus	k1gMnSc1
medianus	medianus	k1gMnSc1
linguae	linguae	k6eAd1
–	–	k?
podélná	podélný	k2eAgFnSc1d1
střední	střední	k2eAgFnSc1d1
brázda	brázda	k1gFnSc1
</s>
<s>
sulcus	sulcus	k1gMnSc1
terminalis	terminalis	k1gFnSc2
–	–	k?
rýha	rýha	k1gFnSc1
tvaru	tvar	k1gInSc2
„	„	k?
<g/>
V	v	k7c6
<g/>
“	“	k?
<g/>
,	,	kIx,
dělí	dělit	k5eAaImIp3nP
tělo	tělo	k1gNnSc4
a	a	k8xC
kořen	kořen	k1gInSc4
jazyka	jazyk	k1gInSc2
</s>
<s>
foramen	foramen	k1gInSc1
caecum	caecum	k1gNnSc1
linguae	linguae	k1gInSc1
–	–	k?
vkleslina	vkleslina	k1gFnSc1
uprostřed	uprostřed	k7c2
hrotu	hrot	k1gInSc2
sulcus	sulcus	k1gInSc1
terminalis	terminalis	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c2
embryonálního	embryonální	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
se	se	k3xPyFc4
odsud	odsud	k6eAd1
oddělil	oddělit	k5eAaPmAgInS
základ	základ	k1gInSc1
štítné	štítný	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
a	a	k8xC
sestupoval	sestupovat	k5eAaImAgMnS
kaudálně	kaudálně	k6eAd1
jako	jako	k8xS,k8xC
ductus	ductus	k1gMnSc1
thyroglossus	thyroglossus	k1gMnSc1
</s>
<s>
boční	boční	k2eAgInSc4d1
okraj	okraj	k1gInSc4
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
margo	margo	k6eAd1
linguae	linguaat	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
spodní	spodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
facies	facies	k1gFnSc1
inferior	inferior	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
ní	on	k3xPp3gFnSc6
<g/>
:	:	kIx,
</s>
<s>
plica	plic	k2eAgFnSc1d1
fimbriata	fimbriata	k1gFnSc1
–	–	k?
slizniční	slizniční	k2eAgFnSc1d1
řasa	řasa	k1gFnSc1
na	na	k7c4
facies	facies	k1gFnSc4
inferior	inferior	k1gInSc1
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
sublingua	sublingua	k6eAd1
</s>
<s>
frenulum	frenulum	k1gInSc1
linguae	linguaat	k5eAaPmIp3nS
–	–	k?
uzdička	uzdička	k1gFnSc1
jazyka	jazyk	k1gInSc2
</s>
<s>
caruncula	caruncula	k1gFnSc1
sublingualis	sublingualis	k1gFnSc2
–	–	k?
vpředu	vpředu	k6eAd1
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
frenula	frenout	k5eAaPmAgFnS
<g/>
,	,	kIx,
ústí	ústit	k5eAaImIp3nS
zde	zde	k6eAd1
ductus	ductus	k1gMnSc1
submandibularis	submandibularis	k1gFnSc2
et	et	k?
ductus	ductus	k1gMnSc1
sublingualis	sublingualis	k1gFnSc2
major	major	k1gMnSc1
</s>
<s>
plica	plic	k2eAgFnSc1d1
sublingualis	sublingualis	k1gFnSc1
–	–	k?
řasa	řasa	k1gFnSc1
<g/>
,	,	kIx,
ústí	ústit	k5eAaImIp3nS
zde	zde	k6eAd1
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
ductus	ductus	k1gInSc1
sublinguales	sublinguales	k1gMnSc1
minores	minores	k1gMnSc1
</s>
<s>
plica	plic	k2eAgFnSc1d1
glossoepiglottica	glossoepiglottic	k2eAgFnSc1d1
mediana	mediana	k1gFnSc1
–	–	k?
střední	střední	k2eAgFnSc1d1
<g/>
,	,	kIx,
nepárová	párový	k2eNgFnSc1d1
řasa	řasa	k1gFnSc1
táhnoucí	táhnoucí	k2eAgFnSc1d1
se	se	k3xPyFc4
od	od	k7c2
kořene	kořen	k1gInSc2
jazyka	jazyk	k1gInSc2
k	k	k7c3
epiglottis	epiglottis	k1gFnSc3
</s>
<s>
plicae	plicae	k6eAd1
glossoepiglotticae	glossoepiglotticaat	k5eAaPmIp3nS
laterales	laterales	k1gInSc1
–	–	k?
pravá	pravý	k2eAgFnSc1d1
a	a	k8xC
levá	levý	k2eAgFnSc1d1
párová	párový	k2eAgFnSc1d1
řasa	řasa	k1gFnSc1
</s>
<s>
valleculae	valleculae	k1gFnSc1
epiglotticae	epiglottica	k1gFnSc2
–	–	k?
dvě	dva	k4xCgFnPc4
jamky	jamka	k1gFnPc1
mezi	mezi	k7c7
řasami	řasa	k1gFnPc7
</s>
<s>
Sliznice	sliznice	k1gFnSc1
jazyka	jazyk	k1gInSc2
</s>
<s>
Sliznice	sliznice	k1gFnSc1
jazyka	jazyk	k1gInSc2
je	být	k5eAaImIp3nS
kryta	krýt	k5eAaImNgFnS
vícevrstevným	vícevrstevný	k2eAgInSc7d1
dlaždicovým	dlaždicový	k2eAgInSc7d1
epitelem	epitel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epitel	epitel	k1gInSc1
hřbetu	hřbet	k1gInSc2
a	a	k8xC
hrotu	hrot	k1gInSc2
jazyka	jazyk	k1gInSc2
vybíhá	vybíhat	k5eAaImIp3nS
v	v	k7c6
papillae	papillae	k1gFnSc6
linguales	linguales	k1gInSc1
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gInSc1
epitel	epitel	k1gInSc1
na	na	k7c6
povrchu	povrch	k1gInSc6
rohovatí	rohovatět	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
papily	papila	k1gFnPc1
nitkovité	nitkovitý	k2eAgFnPc1d1
(	(	kIx(
<g/>
papillae	papillae	k1gInSc1
filiformes	filiformes	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
štíhlé	štíhlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
roztřepené	roztřepený	k2eAgFnSc2d1
</s>
<s>
papily	papila	k1gFnPc1
kuželovité	kuželovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
papillae	papilla	k1gFnSc2
conicae	conica	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
u	u	k7c2
člověka	člověk	k1gMnSc2
málo	málo	k1gNnSc4
<g/>
,	,	kIx,
hojné	hojný	k2eAgNnSc1d1
u	u	k7c2
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
</s>
<s>
papily	papila	k1gFnPc1
houbovité	houbovitý	k2eAgFnPc1d1
(	(	kIx(
<g/>
papillae	papillae	k1gInSc1
fungiformes	fungiformes	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
kyjovitého	kyjovitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
jednotlivě	jednotlivě	k6eAd1
mezi	mezi	k7c7
papillae	papillae	k1gFnSc7
filiformes	filiformesa	k1gFnPc2
<g/>
,	,	kIx,
červené	červený	k2eAgFnPc1d1
(	(	kIx(
<g/>
tenčí	tenký	k2eAgInSc1d2
epitel	epitel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
papily	papila	k1gFnPc1
listovité	listovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
papillae	papilla	k1gFnSc2
foliatae	foliata	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
rudimentární	rudimentární	k2eAgInSc1d1
<g/>
,	,	kIx,
na	na	k7c4
margo	margo	k1gNnSc4
linguae	linguae	k6eAd1
vzadu	vzadu	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
epitelu	epitel	k1gInSc6
nacházíme	nacházet	k5eAaImIp1nP
chuťové	chuťový	k2eAgInPc1d1
pohárky	pohárek	k1gInPc1
</s>
<s>
papily	papila	k1gFnPc1
hrazené	hrazený	k2eAgFnSc2d1
(	(	kIx(
<g/>
papillae	papilla	k1gFnSc2
vallatae	vallata	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
největší	veliký	k2eAgFnSc1d3
<g/>
,	,	kIx,
ve	v	k7c6
stěnách	stěna	k1gFnPc6
chuťové	chuťový	k2eAgInPc4d1
pohárky	pohárek	k1gInPc4
(	(	kIx(
<g/>
caliculi	calicule	k1gFnSc4
gustatorii	gustatorie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
počtu	počet	k1gInSc6
7	#num#	k4
sestaveny	sestavit	k5eAaPmNgInP
v	v	k7c4
řadu	řada	k1gFnSc4
tvaru	tvar	k1gInSc2
„	„	k?
<g/>
V	v	k7c6
<g/>
“	“	k?
těsně	těsně	k6eAd1
před	před	k7c4
sulcus	sulcus	k1gInSc4
terminalis	terminalis	k1gFnSc2
</s>
<s>
Na	na	k7c6
kořenu	kořen	k1gInSc6
jazyka	jazyk	k1gInSc2
nejsou	být	k5eNaImIp3nP
papily	papila	k1gFnPc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
tonsilla	tonsilla	k1gFnSc1
lingualis	lingualis	k1gFnSc1
(	(	kIx(
<g/>
jde	jít	k5eAaImIp3nS
o	o	k7c4
soubor	soubor	k1gInSc4
lymfatických	lymfatický	k2eAgInPc2d1
folikulů	folikul	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Extraglosální	Extraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
</s>
<s>
Intraglosální	Intraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
</s>
<s>
Svaly	sval	k1gInPc1
jazyka	jazyk	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Svaly	sval	k1gInPc4
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Svaly	sval	k1gInPc1
jazyka	jazyk	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
jazyka	jazyk	k1gInSc2
a	a	k8xC
připojují	připojovat	k5eAaImIp3nP
se	se	k3xPyFc4
ke	k	k7c3
dvěma	dva	k4xCgFnPc3
vazivovým	vazivový	k2eAgFnPc3d1
strukturám	struktura	k1gFnPc3
<g/>
:	:	kIx,
</s>
<s>
aponeurosis	aponeurosis	k1gFnSc1
linguae	lingua	k1gInSc2
–	–	k?
zpevněná	zpevněný	k2eAgFnSc1d1
spodina	spodina	k1gFnSc1
sliznice	sliznice	k1gFnSc2
hřbetní	hřbetní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
jazyka	jazyk	k1gInSc2
</s>
<s>
septum	septum	k1gNnSc1
linguae	lingua	k1gInSc2
–	–	k?
sagitální	sagitální	k2eAgFnSc1d1
vazivová	vazivový	k2eAgFnSc1d1
ploténka	ploténka	k1gFnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
čáře	čára	k1gFnSc6
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
připojena	připojen	k2eAgFnSc1d1
k	k	k7c3
aponeurosis	aponeurosis	k1gFnSc3
linguae	lingua	k1gInSc2
<g/>
,	,	kIx,
otvory	otvor	k1gInPc7
pro	pro	k7c4
některá	některý	k3yIgNnPc4
příčná	příčný	k2eAgNnPc4d1
svalová	svalový	k2eAgNnPc4d1
vlákna	vlákno	k1gNnPc4
a	a	k8xC
cévy	céva	k1gFnPc4
</s>
<s>
Extraglosální	Extraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
</s>
<s>
Extraglosální	Extraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
začínají	začínat	k5eAaImIp3nP
na	na	k7c6
útvarech	útvar	k1gInPc6
v	v	k7c6
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
vzařují	vzařovat	k5eAaImIp3nP
do	do	k7c2
jazyka	jazyk	k1gInSc2
a	a	k8xC
pohybují	pohybovat	k5eAaImIp3nP
jím	on	k3xPp3gNnSc7
jako	jako	k8xC,k8xS
celkem	celkem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišujeme	rozlišovat	k5eAaImIp1nP
<g/>
:	:	kIx,
</s>
<s>
musculus	musculus	k1gMnSc1
genioglossus	genioglossus	k1gMnSc1
–	–	k?
zač	zač	k6eAd1
<g/>
:	:	kIx,
spina	spina	k1gFnSc1
musculi	muscule	k1gFnSc3
genioglossi	genioglosse	k1gFnSc4
mandibulae	mandibula	k1gFnSc2
<g/>
;	;	kIx,
kaudální	kaudální	k2eAgInPc1d1
snopce	snopec	k1gInPc1
–	–	k?
táhnou	táhnout	k5eAaImIp3nP
kořen	kořen	k1gInSc4
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
radix	radix	k1gInSc1
linguae	lingua	k1gInSc2
<g/>
)	)	kIx)
dopředu	dopředu	k6eAd1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInPc1d1
snopce	snopec	k1gInPc1
–	–	k?
přitahují	přitahovat	k5eAaImIp3nP
jazyk	jazyk	k1gInSc4
ke	k	k7c3
spodině	spodina	k1gFnSc3
úst	ústa	k1gNnPc2
</s>
<s>
musculus	musculus	k1gMnSc1
hyoglossus	hyoglossus	k1gMnSc1
–	–	k?
zač	zač	k6eAd1
<g/>
:	:	kIx,
cornu	cornout	k5eAaPmIp1nS,k5eAaImIp1nS
majus	majus	k1gMnSc1
ossis	ossis	k1gFnSc2
hyoidei	hyoide	k1gFnSc2
<g/>
;	;	kIx,
táhne	táhnout	k5eAaImIp3nS
jazyk	jazyk	k1gInSc4
dozadu	dozadu	k6eAd1
a	a	k8xC
dolů	dolů	k6eAd1
</s>
<s>
musculus	musculus	k1gMnSc1
styloglossus	styloglossus	k1gMnSc1
–	–	k?
zač	zač	k6eAd1
<g/>
:	:	kIx,
proc	proc	k1gMnSc1
<g/>
.	.	kIx.
stylohyoideus	stylohyoideus	k1gMnSc1
(	(	kIx(
<g/>
partis	partis	k1gFnSc1
inferioris	inferioris	k1gFnSc1
pyramidis	pyramidis	k1gFnSc1
ossis	ossis	k1gFnSc1
temporalis	temporalis	k1gFnSc1
<g/>
)	)	kIx)
et	et	k?
lig	liga	k1gFnPc2
<g/>
.	.	kIx.
stylomandibulare	stylomandibular	k1gMnSc5
<g/>
;	;	kIx,
táhne	táhnout	k5eAaImIp3nS
hrot	hrot	k1gInSc1
jazyka	jazyk	k1gInSc2
dozadu	dozadu	k6eAd1
<g/>
,	,	kIx,
tělo	tělo	k1gNnSc1
a	a	k8xC
kořen	kořen	k1gInSc1
dozadu	dozadu	k6eAd1
vzhůru	vzhůru	k6eAd1
</s>
<s>
musculus	musculus	k1gMnSc1
palatoglossus	palatoglossus	k1gMnSc1
–	–	k?
patří	patřit	k5eAaImIp3nS
ke	k	k7c3
svalům	sval	k1gInPc3
měkkého	měkký	k2eAgNnSc2d1
patra	patro	k1gNnSc2
</s>
<s>
Inervace	inervace	k1gFnSc1
<g/>
:	:	kIx,
m.	m.	k?
genioglossus	genioglossus	k1gInSc1
<g/>
,	,	kIx,
m.	m.	k?
hyoglossus	hyoglossus	k1gInSc1
et	et	k?
m.	m.	k?
styloglossus	styloglossus	k1gInSc1
-	-	kIx~
n.	n.	k?
hypoglossus	hypoglossus	k1gMnSc1
(	(	kIx(
<g/>
XII	XII	kA
<g/>
.	.	kIx.
hlavový	hlavový	k2eAgInSc1d1
nerv	nerv	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
m.	m.	k?
palatoglossus	palatoglossus	k1gInSc1
-	-	kIx~
n.	n.	k?
glossopharyngeus	glossopharyngeus	k1gMnSc1
(	(	kIx(
<g/>
IX	IX	kA
<g/>
.	.	kIx.
hlavový	hlavový	k2eAgInSc1d1
nerv	nerv	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Intraglosální	Intraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
</s>
<s>
Intraglosální	Intraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
v	v	k7c6
jazyku	jazyk	k1gInSc6
začínají	začínat	k5eAaImIp3nP
i	i	k8xC
končí	končit	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgInP
ve	v	k7c6
třech	tři	k4xCgInPc6
navzájem	navzájem	k6eAd1
kolmých	kolmý	k2eAgInPc6d1
směrech	směr	k1gInPc6
<g/>
,	,	kIx,
mění	měnit	k5eAaImIp3nS
tvar	tvar	k1gInSc4
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišujeme	rozlišovat	k5eAaImIp1nP
</s>
<s>
musculus	musculus	k1gMnSc1
longitudinalis	longitudinalis	k1gFnSc2
superior	superior	k1gMnSc1
(	(	kIx(
<g/>
superficialis	superficialis	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
táhne	táhnout	k5eAaImIp3nS
se	se	k3xPyFc4
sagitálně	sagitálně	k6eAd1
pod	pod	k7c7
aponeurosis	aponeurosis	k1gFnSc7
linguae	lingua	k1gFnSc2
<g/>
,	,	kIx,
spojen	spojen	k2eAgMnSc1d1
s	s	k7c7
ní	on	k3xPp3gFnSc6
v	v	k7c6
celé	celý	k2eAgFnSc6d1
šíři	šíř	k1gFnSc6
hřbetu	hřbet	k1gInSc2
jazyka	jazyk	k1gInSc2
<g/>
;	;	kIx,
zkracuje	zkracovat	k5eAaImIp3nS
jazyk	jazyk	k1gInSc4
</s>
<s>
musculus	musculus	k1gMnSc1
longitudinalis	longitudinalis	k1gFnSc2
inferior	inferior	k1gMnSc1
(	(	kIx(
<g/>
profundus	profundus	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
jde	jít	k5eAaImIp3nS
předozadně	předozadně	k6eAd1
po	po	k7c6
zevní	zevní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
m.	m.	k?
genioglossus	genioglossus	k1gInSc1
<g/>
,	,	kIx,
propletený	propletený	k2eAgInSc1d1
se	s	k7c7
snopci	snopec	k1gInPc7
m.	m.	k?
genioglossus	genioglossus	k1gMnSc1
a	a	k8xC
hyoglossus	hyoglossus	k1gMnSc1
<g/>
;	;	kIx,
zkracuje	zkracovat	k5eAaImIp3nS
jazyk	jazyk	k1gInSc4
</s>
<s>
musculus	musculus	k1gMnSc1
transversus	transversus	k1gMnSc1
linguae	linguae	k6eAd1
–	–	k?
jde	jít	k5eAaImIp3nS
napříč	napříč	k7c7
jazykem	jazyk	k1gInSc7
od	od	k7c2
septum	septum	k1gNnSc4
linguae	linguae	k1gFnSc1
k	k	k7c3
okrajům	okraj	k1gInPc3
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
snopci	snopec	k1gInPc7
ostatních	ostatní	k2eAgInPc2d1
extraglosálních	extraglosální	k2eAgInPc2d1
a	a	k8xC
intraglosálních	intraglosální	k2eAgInPc2d1
svalů	sval	k1gInPc2
<g/>
;	;	kIx,
zužuje	zužovat	k5eAaImIp3nS
jazyk	jazyk	k1gInSc4
</s>
<s>
musculus	musculus	k1gInSc1
verticalis	verticalis	k1gFnSc2
linguae	lingua	k1gFnSc2
–	–	k?
jde	jít	k5eAaImIp3nS
od	od	k7c2
dorsum	dorsum	k1gInSc4
linguae	linguae	k1gFnSc3
ke	k	k7c3
spodině	spodina	k1gFnSc3
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
snopci	snopec	k1gInPc7
všech	všecek	k3xTgInPc2
ostatních	ostatní	k2eAgInPc2d1
svalů	sval	k1gInPc2
<g/>
;	;	kIx,
zplošťuje	zplošťovat	k5eAaImIp3nS
jazyk	jazyk	k1gInSc4
</s>
<s>
Intraglosální	Intraglosální	k2eAgInPc1d1
svaly	sval	k1gInPc1
nemají	mít	k5eNaImIp3nP
fasciae	fasciae	k1gFnSc7
propriae	propriae	k1gFnSc7
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gNnPc1
vlákna	vlákno	k1gNnPc1
jsou	být	k5eAaImIp3nP
propletená	propletený	k2eAgNnPc1d1
<g/>
,	,	kIx,
preparačně	preparačně	k6eAd1
neoddělitelná	oddělitelný	k2eNgFnSc1d1
a	a	k8xC
rozlišují	rozlišovat	k5eAaImIp3nP
se	se	k3xPyFc4
podle	podle	k7c2
převážujících	převážující	k2eAgInPc2d1
směrů	směr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Inervace	inervace	k1gFnSc1
-	-	kIx~
n.	n.	k?
hypoglossus	hypoglossus	k1gMnSc1
(	(	kIx(
<g/>
XII	XII	kA
<g/>
.	.	kIx.
hlavový	hlavový	k2eAgInSc1d1
nerv	nerv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vnímání	vnímání	k1gNnSc1
chuti	chuť	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Chuť	chuť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sladkou	sladký	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
vnímáme	vnímat	k5eAaImIp1nP
špičkou	špička	k1gFnSc7
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
slanou	slaný	k2eAgFnSc4d1
krajními	krajní	k2eAgFnPc7d1
částmi	část	k1gFnPc7
jazyka	jazyk	k1gMnSc2
za	za	k7c7
receptory	receptor	k1gInPc7
pro	pro	k7c4
vnímání	vnímání	k1gNnSc4
sladké	sladký	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
<g/>
,	,	kIx,
kyselou	kyselý	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
na	na	k7c6
krajích	kraj	k1gInPc6
jazyka	jazyk	k1gInSc2
v	v	k7c4
jeho	jeho	k3xOp3gFnPc4
střední	střední	k2eAgFnPc4d1
části	část	k1gFnPc4
a	a	k8xC
receptory	receptor	k1gInPc4
hořké	hořký	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
jsou	být	k5eAaImIp3nP
umístěné	umístěný	k2eAgInPc1d1
uprostřed	uprostřed	k6eAd1
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
je	být	k5eAaImIp3nS
situace	situace	k1gFnSc1
odlišná	odlišný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
chutě	chutě	k6eAd1
totiž	totiž	k9
vnímáme	vnímat	k5eAaImIp1nP
všemi	všecek	k3xTgFnPc7
částmi	část	k1gFnPc7
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
na	na	k7c6
něm	on	k3xPp3gInSc6
mohou	moct	k5eAaImIp3nP
existovat	existovat	k5eAaImF
plochy	plocha	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
daný	daný	k2eAgInSc4d1
typ	typ	k1gInSc4
chutě	chuť	k1gFnSc2
citlivější	citlivý	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
tak	tak	k9
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
dokáže	dokázat	k5eAaPmIp3nS
rozpoznat	rozpoznat	k5eAaPmF
i	i	k9
menší	malý	k2eAgInSc1d2
výskyt	výskyt	k1gInSc1
příslušné	příslušný	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Specializace	specializace	k1gFnSc1
jazyka	jazyk	k1gInSc2
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
</s>
<s>
Varan	varan	k1gMnSc1
komodský	komodský	k2eAgInSc4d1
s	s	k7c7
vyplazeným	vyplazený	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
</s>
<s>
Některá	některý	k3yIgNnPc1
zvířata	zvíře	k1gNnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
na	na	k7c6
jazyku	jazyk	k1gInSc6
i	i	k9
čichové	čichový	k2eAgInPc4d1
receptory	receptor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
neustále	neustále	k6eAd1
vyplazují	vyplazovat	k5eAaImIp3nP
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
zjišťují	zjišťovat	k5eAaImIp3nP
pachy	pach	k1gInPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
pes	pes	k1gMnSc1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
jazyk	jazyk	k1gInSc4
k	k	k7c3
ochlazování	ochlazování	k1gNnSc3
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlým	rychlý	k2eAgNnSc7d1
dýcháním	dýchání	k1gNnSc7
se	se	k3xPyFc4
z	z	k7c2
jazyka	jazyk	k1gInSc2
odpařují	odpařovat	k5eAaImIp3nP
sliny	slina	k1gFnPc4
a	a	k8xC
tím	ten	k3xDgNnSc7
ochlazují	ochlazovat	k5eAaImIp3nP
krev	krev	k1gFnSc4
v	v	k7c6
jazyku	jazyk	k1gInSc6
a	a	k8xC
následně	následně	k6eAd1
celém	celý	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Např.	např.	kA
chameleon	chameleon	k1gMnSc1
pomocí	pomocí	k7c2
dlouhého	dlouhý	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
i	i	k9
loví	lovit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
je	být	k5eAaImIp3nS
použit	použit	k2eAgInSc4d1
text	text	k1gInSc4
článku	článek	k1gInSc2
Jazyk	jazyk	k1gInSc1
ve	v	k7c6
WikiSkriptech	WikiSkript	k1gInPc6
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
lékařských	lékařský	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
zapojených	zapojený	k2eAgFnPc2d1
v	v	k7c6
MEFANETu	MEFANETus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ROČEK	Roček	k1gMnSc1
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecná	obecný	k2eAgFnSc1d1
morfologie	morfologie	k1gFnSc1
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
Trávicí	trávicí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČIHÁK	Čihák	k1gMnSc1
<g/>
,	,	kIx,
Radomír	Radomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anatomie	anatomie	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Avicenum	Avicenum	k1gNnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
60	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
jazyk	jazyk	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
jazyk	jazyk	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Beyond	Beyond	k1gInSc1
the	the	k?
Tongue	Tongue	k1gInSc1
Map	mapa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lidské	lidský	k2eAgInPc1d1
anatomické	anatomický	k2eAgInPc1d1
rysy	rys	k1gInPc1
Hlava	hlava	k1gFnSc1
</s>
<s>
lebka	lebka	k1gFnSc1
•	•	k?
obličej	obličej	k1gInSc1
•	•	k?
čelo	čelo	k1gNnSc1
•	•	k?
oko	oko	k1gNnSc1
•	•	k?
ucho	ucho	k1gNnSc4
•	•	k?
nos	nos	k1gInSc1
•	•	k?
ústa	ústa	k1gNnPc4
•	•	k?
jazyk	jazyk	k1gInSc4
•	•	k?
zuby	zub	k1gInPc4
•	•	k?
čelist	čelist	k1gFnSc1
•	•	k?
tvář	tvář	k1gFnSc1
•	•	k?
brada	brada	k1gFnSc1
Krk	krk	k1gInSc1
</s>
<s>
ohryzek	ohryzek	k1gInSc1
Trup	trupa	k1gFnPc2
</s>
<s>
rameno	rameno	k1gNnSc4
•	•	k?
páteř	páteř	k1gFnSc1
•	•	k?
prs	prs	k1gInSc1
(	(	kIx(
<g/>
bradavka	bradavka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
hrudník	hrudník	k1gInSc1
•	•	k?
hrudní	hrudní	k2eAgInSc1d1
koš	koš	k1gInSc1
•	•	k?
břicho	břicho	k1gNnSc4
•	•	k?
pupek	pupek	k1gInSc1
•	•	k?
záda	záda	k1gNnPc4
•	•	k?
boky	boka	k1gFnSc2
</s>
<s>
pohlavní	pohlavní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
(	(	kIx(
<g/>
klitoris	klitoris	k1gFnSc1
•	•	k?
pochva	pochva	k1gFnSc1
•	•	k?
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
•	•	k?
penis	penis	k1gInSc1
•	•	k?
šourek	šourek	k1gInSc1
•	•	k?
varle	varle	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
kyčelní	kyčelní	k2eAgInSc4d1
kloub	kloub	k1gInSc4
•	•	k?
konečník	konečník	k1gInSc1
•	•	k?
hýždě	hýždě	k1gFnSc2
Končetiny	končetina	k1gFnSc2
</s>
<s>
Horní	horní	k2eAgFnSc1d1
končetina	končetina	k1gFnSc1
</s>
<s>
paže	paže	k1gFnSc1
•	•	k?
loket	loket	k1gInSc4
•	•	k?
předloktí	předloktí	k1gNnSc2
•	•	k?
zápěstí	zápěstí	k1gNnSc2
•	•	k?
ruka	ruka	k1gFnSc1
•	•	k?
prst	prst	k1gInSc1
(	(	kIx(
<g/>
palec	palec	k1gInSc1
•	•	k?
ukazovák	ukazovák	k1gInSc1
•	•	k?
prostředník	prostředník	k1gInSc1
•	•	k?
prsteník	prsteník	k1gInSc1
•	•	k?
malíček	malíček	k1gInSc1
<g/>
)	)	kIx)
Dolní	dolní	k2eAgFnSc1d1
končetina	končetina	k1gFnSc1
</s>
<s>
noha	noha	k1gFnSc1
•	•	k?
stehno	stehno	k1gNnSc1
•	•	k?
koleno	koleno	k1gNnSc1
•	•	k?
lýtko	lýtko	k1gNnSc1
•	•	k?
pata	pata	k1gFnSc1
•	•	k?
kotník	kotník	k1gInSc4
•	•	k?
chodidlo	chodidlo	k1gNnSc4
•	•	k?
prstec	prstec	k1gInSc1
</s>
<s>
Kůže	kůže	k1gFnSc1
</s>
<s>
vlasy	vlas	k1gInPc1
<g/>
,	,	kIx,
ochlupení	ochlupení	k1gNnPc1
<g/>
,	,	kIx,
nehty	nehet	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4134301-3	4134301-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85135992	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85135992	#num#	k4
</s>
