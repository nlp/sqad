<p>
<s>
Kladivo	kladivo	k1gNnSc1	kladivo
<g/>
,	,	kIx,	,
kladívko	kladívko	k1gNnSc1	kladívko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
ruční	ruční	k2eAgInSc1d1	ruční
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
předat	předat	k5eAaPmF	předat
rázem	ráz	k1gInSc7	ráz
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
nějakému	nějaký	k3yIgNnSc3	nějaký
jinému	jiný	k2eAgNnSc3d1	jiné
tělesu	těleso	k1gNnSc3	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
k	k	k7c3	k
deformacím	deformace	k1gFnPc3	deformace
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
či	či	k8xC	či
předmětů	předmět	k1gInPc2	předmět
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
vyklepáváním	vyklepávání	k1gNnSc7	vyklepávání
či	či	k8xC	či
rozklepáváním	rozklepávání	k1gNnSc7	rozklepávání
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
pro	pro	k7c4	pro
dělení	dělení	k1gNnSc4	dělení
předmětů	předmět	k1gInPc2	předmět
t.j.	t.j.	k?	t.j.
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
rozbíjení	rozbíjení	k1gNnSc4	rozbíjení
na	na	k7c4	na
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Kladivo	kladivo	k1gNnSc1	kladivo
samozřejmě	samozřejmě	k6eAd1	samozřejmě
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
speciální	speciální	k2eAgFnSc4d1	speciální
ruční	ruční	k2eAgFnSc4d1	ruční
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgNnPc2d3	nejběžnější
použití	použití	k1gNnSc2	použití
je	být	k5eAaImIp3nS	být
zatloukání	zatloukání	k1gNnSc4	zatloukání
spojovacích	spojovací	k2eAgFnPc2d1	spojovací
součástek	součástka	k1gFnPc2	součástka
(	(	kIx(	(
<g/>
hřebík	hřebík	k1gInSc4	hřebík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
spojování	spojování	k1gNnSc3	spojování
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spojování	spojování	k1gNnSc4	spojování
dvou	dva	k4xCgInPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
předmětů	předmět	k1gInPc2	předmět
navzájem	navzájem	k6eAd1	navzájem
pomocí	pomocí	k7c2	pomocí
kladiva	kladivo	k1gNnSc2	kladivo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
spojovací	spojovací	k2eAgFnPc4d1	spojovací
součásti	součást	k1gFnPc4	součást
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
hřeby	hřeb	k1gInPc4	hřeb
<g/>
,	,	kIx,	,
hřebíky	hřebík	k1gInPc4	hřebík
<g/>
,	,	kIx,	,
nýty	nýt	k1gInPc4	nýt
<g/>
,	,	kIx,	,
cvoky	cvok	k1gInPc4	cvok
<g/>
,	,	kIx,	,
kramle	kramle	k1gFnPc4	kramle
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
spojovací	spojovací	k2eAgInPc4d1	spojovací
prvky	prvek	k1gInPc4	prvek
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
v	v	k7c6	v
tesařství	tesařství	k1gNnSc6	tesařství
<g/>
,	,	kIx,	,
truhlářství	truhlářství	k1gNnSc6	truhlářství
<g/>
,	,	kIx,	,
zednictví	zednictví	k1gNnSc6	zednictví
<g/>
,	,	kIx,	,
kovářství	kovářství	k1gNnSc6	kovářství
<g/>
,	,	kIx,	,
zámečnictví	zámečnictví	k1gNnSc6	zámečnictví
<g/>
,	,	kIx,	,
čalounictví	čalounictví	k1gNnSc6	čalounictví
<g/>
,	,	kIx,	,
brašnářství	brašnářství	k1gNnSc6	brašnářství
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgNnPc6d1	další
řemeslech	řemeslo	k1gNnPc6	řemeslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kladivo	kladivo	k1gNnSc1	kladivo
lze	lze	k6eAd1	lze
samozřejmě	samozřejmě	k6eAd1	samozřejmě
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
rozpojování	rozpojování	k1gNnSc4	rozpojování
navzájem	navzájem	k6eAd1	navzájem
spojených	spojený	k2eAgFnPc2d1	spojená
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgNnPc1d1	speciální
kladiva	kladivo	k1gNnPc1	kladivo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
poklep	poklep	k1gInSc4	poklep
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
cíleného	cílený	k2eAgInSc2d1	cílený
hluku	hluk	k1gInSc2	hluk
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
aukční	aukční	k2eAgNnSc1d1	aukční
kladívko	kladívko	k1gNnSc1	kladívko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kladivo	kladivo	k1gNnSc1	kladivo
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
speciálním	speciální	k2eAgInSc6d1	speciální
případě	případ	k1gInSc6	případ
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
paličky	palička	k1gFnSc2	palička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
ruční	ruční	k2eAgInSc1d1	ruční
nástroj	nástroj	k1gInSc1	nástroj
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
rukojeti	rukojeť	k1gFnSc2	rukojeť
respektive	respektive	k9	respektive
z	z	k7c2	z
topůrka	topůrko	k1gNnSc2	topůrko
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc2d1	vlastní
hlavy	hlava	k1gFnSc2	hlava
kladiva	kladivo	k1gNnSc2	kladivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Topůrko	topůrko	k1gNnSc1	topůrko
je	být	k5eAaImIp3nS	být
vsazeno	vsadit	k5eAaPmNgNnS	vsadit
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
kladiva	kladivo	k1gNnSc2	kladivo
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zde	zde	k6eAd1	zde
fixováno	fixován	k2eAgNnSc1d1	fixováno
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
sekery	sekera	k1gFnSc2	sekera
<g/>
,	,	kIx,	,
klínkem	klínek	k1gInSc7	klínek
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
klínkem	klínek	k1gInSc7	klínek
kovovým	kovový	k2eAgInSc7d1	kovový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
pracovních	pracovní	k2eAgNnPc2d1	pracovní
kladiv	kladivo	k1gNnPc2	kladivo
obvykle	obvykle	k6eAd1	obvykle
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
kladivo	kladivo	k1gNnSc4	kladivo
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
druhů	druh	k1gInPc2	druh
kladiv	kladivo	k1gNnPc2	kladivo
lišících	lišící	k2eAgInPc2d1	lišící
se	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
počtem	počet	k1gInSc7	počet
rukou	ruka	k1gFnPc2	ruka
nutných	nutný	k2eAgFnPc2d1	nutná
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
správnému	správný	k2eAgNnSc3d1	správné
držení	držení	k1gNnSc3	držení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaPmNgNnS	využívat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
druzích	druh	k1gInPc6	druh
řemesel	řemeslo	k1gNnPc2	řemeslo
i	i	k9	i
lidských	lidský	k2eAgFnPc2d1	lidská
činností	činnost	k1gFnPc2	činnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
geologické	geologický	k2eAgNnSc1d1	geologické
kladivo	kladivo	k1gNnSc1	kladivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nože	nůž	k1gInSc2	nůž
a	a	k8xC	a
sekery	sekera	k1gFnSc2	sekera
je	být	k5eAaImIp3nS	být
kladivo	kladivo	k1gNnSc1	kladivo
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgInPc2d3	nejběžnější
ručních	ruční	k2eAgInPc2d1	ruční
pracovních	pracovní	k2eAgInPc2d1	pracovní
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
aukční	aukční	k2eAgFnSc1d1	aukční
či	či	k8xC	či
soudní	soudní	k2eAgNnPc1d1	soudní
kladívka	kladívko	k1gNnPc1	kladívko
apod.	apod.	kA	apod.
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
vhodného	vhodný	k2eAgInSc2d1	vhodný
nekovového	kovový	k2eNgInSc2d1	nekovový
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
z	z	k7c2	z
pryže	pryž	k1gFnSc2	pryž
či	či	k8xC	či
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kladívka	kladívko	k1gNnPc4	kladívko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
spíše	spíše	k9	spíše
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
poklepové	poklepový	k2eAgFnSc2d1	poklepová
paličky	palička	k1gFnSc2	palička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vydává	vydávat	k5eAaPmIp3nS	vydávat
především	především	k6eAd1	především
hluk	hluk	k1gInSc1	hluk
a	a	k8xC	a
žádné	žádný	k3yNgInPc1	žádný
předměty	předmět	k1gInPc1	předmět
obvykle	obvykle	k6eAd1	obvykle
nijak	nijak	k6eAd1	nijak
nedeformuje	deformovat	k5eNaImIp3nS	deformovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
nástroji	nástroj	k1gInPc7	nástroj
kladiva	kladivo	k1gNnSc2	kladivo
jsou	být	k5eAaImIp3nP	být
palice	palice	k1gFnPc1	palice
a	a	k8xC	a
sekery	sekera	k1gFnPc1	sekera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
tak	tak	k9	tak
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgInP	označovat
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
tvářecích	tvářecí	k2eAgMnPc2d1	tvářecí
strojů	stroj	k1gInPc2	stroj
souhrnně	souhrnně	k6eAd1	souhrnně
označovaných	označovaný	k2eAgFnPc2d1	označovaná
coby	coby	k?	coby
strojní	strojní	k2eAgMnPc1d1	strojní
kladiva	kladivo	k1gNnSc2	kladivo
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
elektrické	elektrický	k2eAgNnSc1d1	elektrické
kladivo	kladivo	k1gNnSc1	kladivo
<g/>
,	,	kIx,	,
mechanické	mechanický	k2eAgNnSc1d1	mechanické
kladivo	kladivo	k1gNnSc1	kladivo
či	či	k8xC	či
pneumatické	pneumatický	k2eAgNnSc1d1	pneumatické
kladivo	kladivo	k1gNnSc1	kladivo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
stroj	stroj	k1gInSc1	stroj
také	také	k9	také
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
sbíječka	sbíječka	k1gFnSc1	sbíječka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
strojní	strojní	k2eAgNnPc4d1	strojní
kladiva	kladivo	k1gNnPc4	kladivo
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
parní	parní	k2eAgNnSc1d1	parní
kladivo	kladivo	k1gNnSc1	kladivo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznými	příbuzný	k2eAgFnPc7d1	příbuzná
strojními	strojní	k2eAgFnPc7d1	strojní
zařízeními	zařízení	k1gNnPc7	zařízení
všech	všecek	k3xTgNnPc2	všecek
strojních	strojní	k2eAgNnPc2d1	strojní
kladiv	kladivo	k1gNnPc2	kladivo
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
buchary	buchar	k1gInPc1	buchar
a	a	k8xC	a
lisy	lis	k1gInPc1	lis
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
strojní	strojní	k2eAgNnPc1d1	strojní
kladiva	kladivo	k1gNnPc1	kladivo
a	a	k8xC	a
buchary	buchar	k1gInPc1	buchar
bývaly	bývat	k5eAaImAgInP	bývat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k9	také
poháněny	pohánět	k5eAaImNgInP	pohánět
vodou	voda	k1gFnSc7	voda
ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
kovárnách	kovárna	k1gFnPc6	kovárna
-	-	kIx~	-
v	v	k7c6	v
hamrech	hamr	k1gInPc6	hamr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kladívko	kladívko	k1gNnSc4	kladívko
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
označovány	označovat	k5eAaImNgFnP	označovat
některé	některý	k3yIgFnPc1	některý
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
součásti	součást	k1gFnPc1	součást
některých	některý	k3yIgInPc2	některý
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
strojů	stroj	k1gInPc2	stroj
či	či	k8xC	či
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgNnSc1d1	klavírní
kladívko	kladívko	k1gNnSc1	kladívko
u	u	k7c2	u
kladívkového	kladívkový	k2eAgInSc2d1	kladívkový
klavíru	klavír	k1gInSc2	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
součásti	součást	k1gFnPc4	součást
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
mechanickým	mechanický	k2eAgInSc7d1	mechanický
poklepem	poklep	k1gInSc7	poklep
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
součásti	součást	k1gFnSc6	součást
téhož	týž	k3xTgNnSc2	týž
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kladívko	kladívko	k1gNnSc1	kladívko
je	být	k5eAaImIp3nS	být
i	i	k9	i
anatomické	anatomický	k2eAgNnSc1d1	anatomické
pojmenování	pojmenování	k1gNnSc1	pojmenování
části	část	k1gFnSc2	část
lidského	lidský	k2eAgInSc2d1	lidský
sluchu	sluch	k1gInSc2	sluch
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
ucho	ucho	k1gNnSc4	ucho
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
ucho	ucho	k1gNnSc4	ucho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
speciálních	speciální	k2eAgNnPc2d1	speciální
kladiv	kladivo	k1gNnPc2	kladivo
==	==	k?	==
</s>
</p>
<p>
<s>
Geologické	geologický	k2eAgNnSc1d1	geologické
kladivo	kladivo	k1gNnSc1	kladivo
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
geologický	geologický	k2eAgInSc4d1	geologický
průzkum	průzkum	k1gInSc4	průzkum
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
hornin	hornina	k1gFnPc2	hornina
poklepem	poklep	k1gInSc7	poklep
</s>
</p>
<p>
<s>
Obouruční	obouruční	k2eAgNnSc1d1	obouruční
kovářské	kovářský	k2eAgNnSc1d1	kovářské
kladivo	kladivo	k1gNnSc1	kladivo
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
perlík	perlík	k1gInSc1	perlík
</s>
</p>
<p>
<s>
Soudcovské	soudcovský	k2eAgNnSc1d1	soudcovské
kladívko	kladívko	k1gNnSc1	kladívko
-	-	kIx~	-
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
zjednávání	zjednávání	k1gNnSc4	zjednávání
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
soudní	soudní	k2eAgFnSc6d1	soudní
síni	síň	k1gFnSc6	síň
či	či	k8xC	či
pro	pro	k7c4	pro
odklepnutí	odklepnutí	k1gNnSc4	odklepnutí
vyneseného	vynesený	k2eAgInSc2d1	vynesený
rozsudku	rozsudek	k1gInSc2	rozsudek
</s>
</p>
<p>
<s>
Aukční	aukční	k2eAgNnSc1d1	aukční
kladívko	kladívko	k1gNnSc1	kladívko
-	-	kIx~	-
speciální	speciální	k2eAgNnSc1d1	speciální
kladívko	kladívko	k1gNnSc1	kladívko
používané	používaný	k2eAgNnSc1d1	používané
při	při	k7c6	při
aukcích	aukce	k1gFnPc6	aukce
a	a	k8xC	a
dražbách	dražba	k1gFnPc6	dražba
pro	pro	k7c4	pro
odklepnutí	odklepnutí	k1gNnSc4	odklepnutí
konečné	konečný	k2eAgFnSc2d1	konečná
ceny	cena	k1gFnSc2	cena
</s>
</p>
<p>
<s>
Chirurgické	chirurgický	k2eAgNnSc1d1	chirurgické
kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Neurologické	neurologický	k2eAgNnSc1d1	neurologický
kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Tesařské	tesařský	k2eAgNnSc1d1	tesařské
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Pokrývačské	pokrývačský	k2eAgNnSc1d1	pokrývačské
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Zámečnické	zámečnický	k2eAgNnSc1d1	zámečnické
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Podkovářské	podkovářský	k2eAgNnSc1d1	Podkovářské
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Zednické	zednický	k2eAgNnSc1d1	zednické
kladívko	kladívko	k1gNnSc1	kladívko
(	(	kIx(	(
<g/>
obkladačské	obkladačský	k2eAgNnSc1d1	Obkladačské
kladívko	kladívko	k1gNnSc1	kladívko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dlaždičské	dlaždičský	k2eAgNnSc1d1	dlaždičské
kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Hornické	hornický	k2eAgNnSc1d1	Hornické
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Kovotepecké	kovotepecký	k2eAgNnSc1d1	kovotepecký
kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Hodinářské	hodinářský	k2eAgNnSc1d1	hodinářské
kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Modelářské	modelářský	k2eAgNnSc1d1	modelářské
kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Pucka	pucka	k1gFnSc1	pucka
-	-	kIx~	-
kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
násadě	násada	k1gFnSc6	násada
používané	používaný	k2eAgFnSc2d1	používaná
např.	např.	kA	např.
při	při	k7c6	při
demolicích	demolice	k1gFnPc6	demolice
nebo	nebo	k8xC	nebo
při	při	k7c6	při
ručním	ruční	k2eAgNnSc6d1	ruční
štípání	štípání	k1gNnSc6	štípání
dřevěných	dřevěný	k2eAgNnPc2d1	dřevěné
polen	poleno	k1gNnPc2	poleno
klínem	klín	k1gInSc7	klín
</s>
</p>
<p>
<s>
Pemrlice	pemrlice	k1gFnSc1	pemrlice
-	-	kIx~	-
kamenické	kamenický	k2eAgNnSc1d1	kamenické
kladivo	kladivo	k1gNnSc1	kladivo
mající	mající	k2eAgNnSc1d1	mající
na	na	k7c4	na
plosce	plosko	k6eAd1	plosko
několik	několik	k4yIc4	několik
řad	řada	k1gFnPc2	řada
jehlanů	jehlan	k1gInPc2	jehlan
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
palička	palička	k1gFnSc1	palička
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
finální	finální	k2eAgFnSc3d1	finální
úpravě	úprava	k1gFnSc3	úprava
kamene	kámen	k1gInSc2	kámen
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
symbolice	symbolika	k1gFnSc6	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
Kladivo	kladivo	k1gNnSc1	kladivo
zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
symbolem	symbol	k1gInSc7	symbol
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
právu	právo	k1gNnSc6	právo
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
ukončení	ukončení	k1gNnSc2	ukončení
nějaké	nějaký	k3yIgFnSc2	nějaký
akce	akce	k1gFnSc2	akce
(	(	kIx(	(
<g/>
aukce	aukce	k1gFnSc2	aukce
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
papeže	papež	k1gMnSc2	papež
je	být	k5eAaImIp3nS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
třikrát	třikrát	k6eAd1	třikrát
udeří	udeřit	k5eAaPmIp3nP	udeřit
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
kladívkem	kladívko	k1gNnSc7	kladívko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
symbolicky	symbolicky	k6eAd1	symbolicky
ukončen	ukončen	k2eAgInSc1d1	ukončen
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
<g/>
Keltové	Kelt	k1gMnPc1	Kelt
považovali	považovat	k5eAaImAgMnP	považovat
kladivo	kladivo	k1gNnSc4	kladivo
za	za	k7c4	za
magický	magický	k2eAgInSc4d1	magický
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
kladivo	kladivo	k1gNnSc1	kladivo
stalo	stát	k5eAaPmAgNnS	stát
symbolem	symbol	k1gInSc7	symbol
kovářství	kovářství	k1gNnSc1	kovářství
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
<g/>
.	.	kIx.	.
<g/>
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
zednáři	zednář	k1gMnPc1	zednář
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
symbolice	symbolika	k1gFnSc6	symbolika
považují	považovat	k5eAaImIp3nP	považovat
kladivo	kladivo	k1gNnSc4	kladivo
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
rozumně	rozumně	k6eAd1	rozumně
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
volných	volný	k2eAgFnPc2d1	volná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
<g/>
Dvě	dva	k4xCgNnPc1	dva
zkřížená	zkřížený	k2eAgNnPc1d1	zkřížené
kladívka	kladívko	k1gNnPc1	kladívko
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
symbolice	symbolika	k1gFnSc6	symbolika
běžných	běžný	k2eAgInPc2d1	běžný
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
v	v	k7c6	v
autobusové	autobusový	k2eAgFnSc6d1	autobusová
i	i	k8xC	i
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
dvě	dva	k4xCgNnPc4	dva
zkřížená	zkřížený	k2eAgNnPc4d1	zkřížené
kladiva	kladivo	k1gNnPc4	kladivo
označuji	označovat	k5eAaImIp1nS	označovat
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
resp.	resp.	kA	resp.
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
-	-	kIx~	-
tedy	tedy	k9	tedy
symbol	symbol	k1gInSc1	symbol
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
==	==	k?	==
</s>
</p>
<p>
<s>
Kladivo	kladivo	k1gNnSc1	kladivo
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
srpem	srp	k1gInSc7	srp
a	a	k8xC	a
rudou	rudý	k2eAgFnSc7d1	rudá
hvězdou	hvězda	k1gFnSc7	hvězda
tvořilo	tvořit	k5eAaImAgNnS	tvořit
(	(	kIx(	(
<g/>
a	a	k8xC	a
stále	stále	k6eAd1	stále
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
)	)	kIx)	)
důležitou	důležitý	k2eAgFnSc4d1	důležitá
součást	součást	k1gFnSc4	součást
symboliky	symbolika	k1gFnSc2	symbolika
levicových	levicový	k2eAgFnPc2d1	levicová
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
těch	ten	k3xDgMnPc2	ten
komunistických	komunistický	k2eAgMnPc2d1	komunistický
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
užíváno	užívat	k5eAaImNgNnS	užívat
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
pracujících	pracující	k2eAgMnPc2d1	pracující
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
coby	coby	k?	coby
symbol	symbol	k1gInSc1	symbol
označující	označující	k2eAgFnSc4d1	označující
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
symbolem	symbol	k1gInSc7	symbol
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zdůrazněno	zdůraznit	k5eAaPmNgNnS	zdůraznit
jejich	jejich	k3xOp3gNnSc1	jejich
pevné	pevný	k2eAgNnSc1d1	pevné
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
dělníky	dělník	k1gMnPc7	dělník
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
s	s	k7c7	s
havíři	havíř	k1gMnPc7	havíř
a	a	k8xC	a
horníky	horník	k1gMnPc7	horník
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
součástí	součást	k1gFnPc2	součást
vlajky	vlajka	k1gFnSc2	vlajka
NDR	NDR	kA	NDR
a	a	k8xC	a
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
srpem	srp	k1gInSc7	srp
a	a	k8xC	a
přetrženými	přetržený	k2eAgInPc7d1	přetržený
okovy	okov	k1gInPc7	okov
<g/>
)	)	kIx)	)
například	například	k6eAd1	například
i	i	k9	i
součástí	součást	k1gFnSc7	součást
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
heraldika	heraldika	k1gFnSc1	heraldika
===	===	k?	===
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
nástrojem	nástroj	k1gInSc7	nástroj
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
heraldice	heraldika	k1gFnSc6	heraldika
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
znacích	znak	k1gInPc6	znak
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
hornické	hornický	k2eAgNnSc1d1	Hornické
kladívko	kladívko	k1gNnSc1	kladívko
bylo	být	k5eAaImAgNnS	být
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
pro	pro	k7c4	pro
znak	znak	k1gInSc4	znak
hornických	hornický	k2eAgNnPc2d1	Hornické
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
ve	v	k7c6	v
štítech	štít	k1gInPc6	štít
měst	město	k1gNnPc2	město
nehornických	hornický	k2eNgMnPc2d1	hornický
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
a	a	k8xC	a
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
lva	lev	k1gInSc2	lev
popř.	popř.	kA	popř.
lvů	lev	k1gInPc2	lev
(	(	kIx(	(
<g/>
Abertamy	Abertam	k1gInPc1	Abertam
<g/>
,	,	kIx,	,
Bečov	Bečov	k1gInSc1	Bečov
nad	nad	k7c7	nad
Teplou	Teplá	k1gFnSc7	Teplá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
kladiva	kladivo	k1gNnPc4	kladivo
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
např.	např.	kA	např.
Boží	božit	k5eAaImIp3nS	božit
Dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
Břidličná	břidličný	k2eAgFnSc1d1	Břidličná
<g/>
,	,	kIx,	,
Černý	černý	k2eAgInSc1d1	černý
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Havířov	Havířov	k1gInSc1	Havířov
(	(	kIx(	(
<g/>
2	[number]	k4	2
x	x	k?	x
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
kladivo	kladivo	k1gNnSc1	kladivo
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Bohdalov	Bohdalov	k1gInSc1	Bohdalov
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
palice	palice	k1gFnSc1	palice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnSc1d1	dolní
Poustevna	poustevna	k1gFnSc1	poustevna
<g/>
,	,	kIx,	,
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
nad	nad	k7c7	nad
Ostravicí	Ostravice	k1gFnSc7	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc4	tři
kladiva	kladivo	k1gNnPc4	kladivo
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Kašperských	kašperský	k2eAgFnPc2d1	Kašperská
Hor.	Hor.	k1gFnPc2	Hor.
</s>
</p>
<p>
<s>
====	====	k?	====
Seznam	seznam	k1gInSc4	seznam
českých	český	k2eAgInPc2d1	český
znaků	znak	k1gInPc2	znak
s	s	k7c7	s
kladivem	kladivo	k1gNnSc7	kladivo
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
těchto	tento	k3xDgInPc2	tento
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
aplikaci	aplikace	k1gFnSc4	aplikace
hornického	hornický	k2eAgInSc2d1	hornický
symbolu	symbol	k1gInSc2	symbol
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
kladiva	kladivo	k1gNnPc1	kladivo
(	(	kIx(	(
<g/>
mlátek	mlátek	k1gInSc1	mlátek
a	a	k8xC	a
želízko	želízko	k1gNnSc1	želízko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opačnému	opačný	k2eAgNnSc3d1	opačné
překrytí	překrytí	k1gNnSc3	překrytí
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
hornickému	hornický	k2eAgInSc3d1	hornický
znaku	znak	k1gInSc3	znak
<g/>
)	)	kIx)	)
i	i	k9	i
k	k	k7c3	k
zrcadlovému	zrcadlový	k2eAgNnSc3d1	zrcadlové
otočení	otočení	k1gNnSc3	otočení
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4	na
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
nářadí	nářadí	k1gNnPc4	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nehornických	hornický	k2eNgNnPc6d1	hornický
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
kladiva	kladivo	k1gNnPc1	kladivo
odlišná	odlišný	k2eAgNnPc1d1	odlišné
od	od	k7c2	od
hornických	hornický	k2eAgNnPc2d1	Hornické
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
žádného	žádný	k3yNgNnSc2	žádný
českého	český	k2eAgNnSc2d1	české
sídla	sídlo	k1gNnSc2	sídlo
není	být	k5eNaImIp3nS	být
kladivo	kladivo	k1gNnSc1	kladivo
rozdělené	rozdělený	k2eAgNnSc1d1	rozdělené
(	(	kIx(	(
<g/>
topůrko	topůrko	k1gNnSc1	topůrko
<g/>
,	,	kIx,	,
násada	násada	k1gFnSc1	násada
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
jinak	jinak	k6eAd1	jinak
poškozené	poškozený	k2eAgNnSc1d1	poškozené
<g/>
.	.	kIx.	.
</s>
<s>
Nikde	nikde	k6eAd1	nikde
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
kladivo	kladivo	k1gNnSc1	kladivo
zdvojené	zdvojený	k2eAgFnSc2d1	zdvojená
ani	ani	k8xC	ani
kladiva	kladivo	k1gNnSc2	kladivo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
kladivo	kladivo	k1gNnSc4	kladivo
drží	držet	k5eAaImIp3nP	držet
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
horníka	horník	k1gMnSc4	horník
nebo	nebo	k8xC	nebo
hutníka	hutník	k1gMnSc4	hutník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
běžné	běžný	k2eAgNnSc1d1	běžné
držení	držení	k1gNnPc2	držení
obuvníkem	obuvník	k1gMnSc7	obuvník
či	či	k8xC	či
kovářem	kovář	k1gMnSc7	kovář
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgNnSc1d1	vzácné
je	být	k5eAaImIp3nS	být
držení	držení	k1gNnPc2	držení
kolářem	kolář	k1gMnSc7	kolář
i	i	k8xC	i
tesařem	tesař	k1gMnSc7	tesař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Bohdalov	Bohdalov	k1gInSc1	Bohdalov
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
palice	palice	k1gFnSc1	palice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnSc1d1	dolní
Poustevna	poustevna	k1gFnSc1	poustevna
<g/>
,	,	kIx,	,
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
nad	nad	k7c7	nad
Ostravicí	Ostravice	k1gFnSc7	Ostravice
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
Beroun	Beroun	k1gInSc1	Beroun
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pod	pod	k7c7	pod
Sněžníkem	Sněžník	k1gInSc7	Sněžník
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgNnPc1	dva
kladiva	kladivo	k1gNnPc1	kladivo
</s>
</p>
<p>
<s>
Abertamy	Abertam	k1gInPc1	Abertam
<g/>
,	,	kIx,	,
Bečov	Bečov	k1gInSc1	Bečov
nad	nad	k7c7	nad
Teplou	Teplá	k1gFnSc7	Teplá
<g/>
,	,	kIx,	,
Boží	boží	k2eAgInSc1d1	boží
Dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
Brandýsek	Brandýsek	k1gInSc1	Brandýsek
<g/>
,	,	kIx,	,
Břidličná	břidličný	k2eAgFnSc1d1	Břidličná
<g/>
,	,	kIx,	,
Černý	černý	k2eAgInSc1d1	černý
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Hora	hora	k1gFnSc1	hora
Svatého	svatý	k2eAgMnSc2d1	svatý
Šebestiána	Šebestián	k1gMnSc2	Šebestián
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc1d1	horní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
Slavkov	Slavkov	k1gInSc1	Slavkov
<g/>
,	,	kIx,	,
Hory	hora	k1gFnPc1	hora
Matky	matka	k1gFnPc1	matka
Boží	božit	k5eAaImIp3nP	božit
<g/>
,	,	kIx,	,
Hostomice	Hostomika	k1gFnSc6	Hostomika
<g/>
,	,	kIx,	,
Hrob	hrob	k1gInSc1	hrob
<g/>
,	,	kIx,	,
Hůrky	hůrka	k1gFnPc1	hůrka
<g/>
,	,	kIx,	,
Choltice	Choltice	k1gFnSc1	Choltice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Chodov	Chodov	k1gInSc1	Chodov
<g/>
,	,	kIx,	,
Chrastava-Andělská	Chrastava-Andělský	k2eAgFnSc1d1	Chrastava-Andělský
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Chvaletice	Chvaletice	k1gFnPc1	Chvaletice
<g/>
,	,	kIx,	,
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Jindřichovice	Jindřichovice	k1gFnSc1	Jindřichovice
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Kladno-Švermov	Kladno-Švermov	k1gInSc1	Kladno-Švermov
<g/>
,	,	kIx,	,
Komárov	Komárov	k1gInSc1	Komárov
<g/>
,	,	kIx,	,
Kopisty	kopist	k1gInPc1	kopist
<g/>
,	,	kIx,	,
Kovářská	kovářský	k2eAgNnPc1d1	kovářské
<g/>
,	,	kIx,	,
Krajková	krajkový	k2eAgNnPc1d1	krajkové
<g/>
,	,	kIx,	,
Králíky	Králík	k1gMnPc4	Králík
<g/>
,	,	kIx,	,
Krásno	krásno	k1gNnSc4	krásno
<g/>
,	,	kIx,	,
Krupka	krupka	k1gFnSc1	krupka
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora-Kaňk	Hora-Kaňk	k1gInSc1	Hora-Kaňk
<g/>
,	,	kIx,	,
Košťany	Košťan	k1gMnPc4	Košťan
<g/>
,	,	kIx,	,
Lom	lom	k1gInSc1	lom
<g/>
,	,	kIx,	,
Loučná	Loučný	k2eAgFnSc1d1	Loučná
pod	pod	k7c7	pod
Klínovcem	Klínovec	k1gInSc7	Klínovec
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc4d1	Malé
Březno	Březno	k1gNnSc4	Březno
<g/>
,	,	kIx,	,
Měděnec	měděnec	k1gInSc4	měděnec
<g/>
,	,	kIx,	,
Michalovy	Michalův	k2eAgFnPc1d1	Michalova
Hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
,	,	kIx,	,
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
Mrákotín	Mrákotín	k1gInSc1	Mrákotín
<g/>
,	,	kIx,	,
Mutěnín	Mutěnín	k1gInSc1	Mutěnín
<g/>
,	,	kIx,	,
Nalžovské	Nalžovský	k2eAgFnSc2d1	Nalžovský
Hory-Stříbrné	Hory-Stříbrný	k2eAgFnSc2d1	Hory-Stříbrný
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c7	pod
Smrkem	smrk	k1gInSc7	smrk
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
Orlová	Orlová	k1gFnSc1	Orlová
<g/>
,	,	kIx,	,
Osek	Osek	k1gInSc1	Osek
<g/>
,	,	kIx,	,
Oslavany	Oslavany	k1gInPc1	Oslavany
<g/>
,	,	kIx,	,
Strašice	Strašice	k1gFnPc1	Strašice
<g/>
,	,	kIx,	,
Ostrava-Vítkovice	Ostrava-Vítkovice	k1gFnSc1	Ostrava-Vítkovice
<g/>
,	,	kIx,	,
Ostrava-Michálkovice	Ostrava-Michálkovice	k1gFnSc1	Ostrava-Michálkovice
<g/>
,	,	kIx,	,
Ostrava-Poruba	Ostrava-Poruba	k1gFnSc1	Ostrava-Poruba
<g/>
,	,	kIx,	,
Ostrava-Petřkovice	Ostrava-Petřkovice	k1gFnSc1	Ostrava-Petřkovice
<g/>
,	,	kIx,	,
Pernink	Pernink	k1gInSc1	Pernink
<g/>
,	,	kIx,	,
Přebuz	Přebuz	k1gInSc1	Přebuz
<g/>
,	,	kIx,	,
Přísečnice	Přísečnice	k1gFnSc1	Přísečnice
<g/>
,	,	kIx,	,
Rudolfov	Rudolfov	k1gInSc1	Rudolfov
<g/>
,	,	kIx,	,
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
Skalice	Skalice	k1gFnSc1	Skalice
<g/>
,	,	kIx,	,
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
<g/>
,	,	kIx,	,
Štěpánov	Štěpánov	k1gInSc1	Štěpánov
nad	nad	k7c7	nad
Svratkou	Svratka	k1gFnSc7	Svratka
<g/>
,	,	kIx,	,
Vlastějovice	Vlastějovice	k1gFnSc1	Vlastějovice
<g/>
,	,	kIx,	,
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
,	,	kIx,	,
Výsluní	výsluní	k1gNnSc1	výsluní
<g/>
,	,	kIx,	,
Zbýšov	Zbýšov	k1gInSc1	Zbýšov
<g/>
,	,	kIx,	,
Žacléř	Žacléř	k1gInSc1	Žacléř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgNnPc1	tři
kladiva	kladivo	k1gNnPc1	kladivo
</s>
</p>
<p>
<s>
Kašperské	kašperský	k2eAgFnSc2d1	Kašperská
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Rejštejn	Rejštejn	k1gMnSc1	Rejštejn
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
kladiva	kladivo	k1gNnPc4	kladivo
</s>
</p>
<p>
<s>
Havířov	Havířov	k1gInSc1	Havířov
<g/>
,	,	kIx,	,
Vodňany	Vodňan	k1gMnPc4	Vodňan
</s>
</p>
<p>
<s>
==	==	k?	==
Atletické	atletický	k2eAgNnSc1d1	atletické
kladivo	kladivo	k1gNnSc1	kladivo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
existuje	existovat	k5eAaImIp3nS	existovat
technická	technický	k2eAgFnSc1d1	technická
vrhačská	vrhačský	k2eAgFnSc1d1	vrhačská
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
hod	hod	k1gInSc4	hod
kladivem	kladivo	k1gNnSc7	kladivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
skutečné	skutečný	k2eAgNnSc4d1	skutečné
kladivo	kladivo	k1gNnSc4	kladivo
upevněné	upevněný	k2eAgFnSc2d1	upevněná
na	na	k7c6	na
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
topůrku	topůrek	k1gInSc6	topůrek
<g/>
;	;	kIx,	;
dnešní	dnešní	k2eAgNnSc1d1	dnešní
vrhačské	vrhačský	k2eAgNnSc1d1	vrhačský
náčiní	náčiní	k1gNnSc1	náčiní
<g/>
,	,	kIx,	,
zavedené	zavedený	k2eAgFnSc6d1	zavedená
od	od	k7c2	od
OH	OH	kA	OH
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
názvu	název	k1gInSc2	název
kladivo	kladivo	k1gNnSc4	kladivo
ničím	ničí	k3xOyNgNnSc7	ničí
nepřipomíná	připomínat	k5eNaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
kovovou	kovový	k2eAgFnSc4d1	kovová
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
železnou	železný	k2eAgFnSc4d1	železná
<g/>
)	)	kIx)	)
vrhačskou	vrhačský	k2eAgFnSc4d1	vrhačská
kouli	koule	k1gFnSc4	koule
(	(	kIx(	(
<g/>
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k9	jako
hlavice	hlavice	k1gFnSc1	hlavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připevněnou	připevněný	k2eAgFnSc4d1	připevněná
drátěným	drátěný	k2eAgInSc7d1	drátěný
prutem	prut	k1gInSc7	prut
k	k	k7c3	k
trojúhelníkovitému	trojúhelníkovitý	k2eAgNnSc3d1	trojúhelníkovité
držadlu	držadlo	k1gNnSc3	držadlo
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
kladiva	kladivo	k1gNnSc2	kladivo
<g/>
,	,	kIx,	,
stanovené	stanovený	k2eAgFnPc4d1	stanovená
IAAF	IAAF	kA	IAAF
v	v	k7c6	v
pravidle	pravidlo	k1gNnSc6	pravidlo
č.	č.	k?	č.
191	[number]	k4	191
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
:	:	kIx,	:
hmotnost	hmotnost	k1gFnSc1	hmotnost
koule	koule	k1gFnSc2	koule
7,265	[number]	k4	7,265
až	až	k9	až
7,285	[number]	k4	7,285
kg	kg	kA	kg
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
16	[number]	k4	16
liber	libra	k1gFnPc2	libra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
koule	koule	k1gFnSc2	koule
11	[number]	k4	11
až	až	k9	až
13	[number]	k4	13
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
úchytu	úchyt	k1gInSc2	úchyt
od	od	k7c2	od
vnitřku	vnitřek	k1gInSc2	vnitřek
držadla	držadlo	k1gNnSc2	držadlo
117,5	[number]	k4	117,5
až	až	k9	až
121,5	[number]	k4	121,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
:	:	kIx,	:
hmotnost	hmotnost	k1gFnSc1	hmotnost
4,005	[number]	k4	4,005
až	až	k9	až
4,025	[number]	k4	4,025
kg	kg	kA	kg
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc4	průměr
9,5	[number]	k4	9,5
až	až	k9	až
11	[number]	k4	11
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
úchytu	úchyt	k1gInSc2	úchyt
116	[number]	k4	116
až	až	k9	až
119,5	[number]	k4	119,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Spojovací	spojovací	k2eAgInSc1d1	spojovací
drát	drát	k1gInSc1	drát
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ocelový	ocelový	k2eAgMnSc1d1	ocelový
a	a	k8xC	a
mít	mít	k5eAaImF	mít
nejméně	málo	k6eAd3	málo
3	[number]	k4	3
mm	mm	kA	mm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1	pevné
držadlo	držadlo	k1gNnSc1	držadlo
má	mít	k5eAaImIp3nS	mít
předepsán	předepsán	k2eAgInSc4d1	předepsán
tvar	tvar	k1gInSc4	tvar
rovnostranného	rovnostranný	k2eAgInSc2d1	rovnostranný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
o	o	k7c6	o
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
délce	délka	k1gFnSc6	délka
strany	strana	k1gFnSc2	strana
11,5	[number]	k4	11,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
hlavice	hlavice	k1gFnSc2	hlavice
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
středu	střed	k1gInSc2	střed
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
6	[number]	k4	6
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kladivo	kladivo	k1gNnSc1	kladivo
jako	jako	k8xS	jako
zbraň	zbraň	k1gFnSc1	zbraň
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
kladivo	kladivo	k1gNnSc1	kladivo
skrývala	skrývat	k5eAaImAgFnS	skrývat
také	také	k9	také
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Podobala	podobat	k5eAaImAgFnS	podobat
se	se	k3xPyFc4	se
dnešním	dnešní	k2eAgMnSc7d1	dnešní
kladivům	kladivo	k1gNnPc3	kladivo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
však	však	k9	však
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
rukojeti	rukojeť	k1gFnPc4	rukojeť
a	a	k8xC	a
se	s	k7c7	s
zaostřenými	zaostřený	k2eAgInPc7d1	zaostřený
konci	konec	k1gInPc7	konec
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
zejména	zejména	k9	zejména
při	při	k7c6	při
střetnutích	střetnutí	k1gNnPc6	střetnutí
mezi	mezi	k7c7	mezi
opěšalými	opěšalý	k2eAgInPc7d1	opěšalý
těžce	těžce	k6eAd1	těžce
obrněnými	obrněný	k2eAgMnPc7d1	obrněný
rytíři	rytíř	k1gMnPc7	rytíř
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
dobrému	dobrý	k2eAgNnSc3d1	dobré
brnění	brnění	k1gNnSc3	brnění
účinnější	účinný	k2eAgFnSc2d2	účinnější
než	než	k8xS	než
meč	meč	k1gInSc1	meč
nebo	nebo	k8xC	nebo
sekera	sekera	k1gFnSc1	sekera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hamr	hamr	k1gInSc1	hamr
</s>
</p>
<p>
<s>
Krendle	Krendle	k6eAd1	Krendle
</s>
</p>
<p>
<s>
Srp	srp	k1gInSc1	srp
a	a	k8xC	a
kladivo	kladivo	k1gNnSc1	kladivo
</s>
</p>
<p>
<s>
Kladívko	kladívko	k1gNnSc1	kladívko
</s>
</p>
<p>
<s>
Hod	hod	k1gInSc4	hod
kladivem	kladivo	k1gNnSc7	kladivo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kladivo	kladivo	k1gNnSc4	kladivo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kladivo	kladivo	k1gNnSc4	kladivo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
