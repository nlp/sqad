<s>
Kladivo	kladivo	k1gNnSc1	kladivo
<g/>
,	,	kIx,	,
kladívko	kladívko	k1gNnSc1	kladívko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
ruční	ruční	k2eAgInSc1d1	ruční
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
předat	předat	k5eAaPmF	předat
rázem	ráz	k1gInSc7	ráz
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
nějakému	nějaký	k3yIgNnSc3	nějaký
jinému	jiný	k2eAgNnSc3d1	jiné
tělesu	těleso	k1gNnSc3	těleso
<g/>
.	.	kIx.	.
</s>
