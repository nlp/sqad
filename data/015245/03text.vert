<s>
Tamika	Tamika	k1gFnSc1
</s>
<s>
Tamika	Tamika	k1gFnSc1
</s>
<s>
ženské	ženský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Původ	původ	k1gInSc1
</s>
<s>
japonský	japonský	k2eAgInSc1d1
Četnost	četnost	k1gFnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
3	#num#	k4
Pořadí	pořadí	k1gNnPc1
podle	podle	k7c2
četnosti	četnost	k1gFnSc2
</s>
<s>
8003	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2016	#num#	k4
Četnost	četnost	k1gFnSc1
jmen	jméno	k1gNnPc2
a	a	k8xC
příjmení	příjmení	k1gNnSc2
graficky	graficky	k6eAd1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
četnost	četnost	k1gFnSc1
dle	dle	k7c2
ročníků	ročník	k1gInPc2
v	v	k7c6
grafu	graf	k1gInSc6
<g/>
,	,	kIx,
původ	původ	k1gInSc4
a	a	k8xC
osobnostiTento	osobnostiTento	k1gNnSc4
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Tamika	Tamika	k1gFnSc1
je	být	k5eAaImIp3nS
ženské	ženský	k2eAgNnSc4d1
křestní	křestní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
význam	význam	k1gInSc1
</s>
<s>
Tamika	Tamika	k1gFnSc1
v	v	k7c6
japonština	japonština	k1gFnSc1
hnamená	hnamený	k2eAgFnSc1d1
národ	národ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varianta	varianta	k1gFnSc1
Tamiko	Tamika	k1gFnSc5
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgNnPc2
japonských	japonský	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
多	多	k?
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
<g/>
)	)	kIx)
hodně	hodně	k6eAd1
<g/>
;	;	kIx,
美	美	k?
(	(	kIx(
<g/>
mi	já	k3xPp1nSc3
<g/>
)	)	kIx)
krásná	krásný	k2eAgFnSc1d1
a	a	k8xC
子	子	k?
(	(	kIx(
<g/>
ko	ko	k?
<g/>
)	)	kIx)
dítě	dítě	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
tedy	tedy	k9
znamená	znamenat	k5eAaImIp3nS
hodně	hodně	k6eAd1
krásné	krásný	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Domácké	domácký	k2eAgFnPc1d1
podoby	podoba	k1gFnPc1
</s>
<s>
Tammy	Tamma	k1gFnPc1
<g/>
,	,	kIx,
Taminka	Taminka	k1gFnSc1
<g/>
,	,	kIx,
Mika	Mik	k1gMnSc2
Tamikinka	Tamikinka	k1gFnSc1
</s>
<s>
Známé	známý	k2eAgFnPc1d1
nositelky	nositelka	k1gFnPc1
jména	jméno	k1gNnSc2
</s>
<s>
Tamiko	Tamika	k1gFnSc5
Jones	Jones	k1gMnSc1
–	–	k?
americká	americký	k2eAgFnSc1d1
jazzová	jazzový	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Tamika	Tamika	k1gFnSc1
Whitmore	Whitmor	k1gInSc5
–	–	k?
americká	americký	k2eAgFnSc1d1
basketbalistka	basketbalistka	k1gFnSc1
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
Holka	holka	k1gFnSc1
jménem	jméno	k1gNnSc7
Tamiko	Tamika	k1gFnSc5
–	–	k?
americký	americký	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
ztvárnila	ztvárnit	k5eAaPmAgFnS
France	Franc	k1gMnPc4
Nguyen	Nguyna	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Tamika	Tamika	k1gFnSc1
na	na	k7c4
Behind	Behind	k1gInSc4
The	The	k1gMnSc2
Name	Nam	k1gMnSc2
</s>
