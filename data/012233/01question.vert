<s>
Které	který	k3yRgNnSc1	který
záření	záření	k1gNnSc1	záření
má	mít	k5eAaImIp3nS	mít
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
delší	dlouhý	k2eAgNnSc4d2	delší
než	než	k8xS	než
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
?	?	kIx.	?
</s>
