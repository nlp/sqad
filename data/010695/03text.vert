<p>
<s>
Síťová	síťový	k2eAgFnSc1d1	síťová
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
network	network	k1gInSc1	network
layer	layer	k1gInSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
třetí	třetí	k4xOgFnSc2	třetí
vrstvy	vrstva	k1gFnSc2	vrstva
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
směrování	směrování	k1gNnSc4	směrování
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
a	a	k8xC	a
síťové	síťový	k2eAgNnSc1d1	síťové
adresování	adresování	k1gNnSc1	adresování
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spolu	spolu	k6eAd1	spolu
přímo	přímo	k6eAd1	přímo
nesousedí	sousedit	k5eNaImIp3nP	sousedit
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
překlenout	překlenout	k5eAaPmF	překlenout
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
přenosových	přenosový	k2eAgFnPc6d1	přenosová
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
je	být	k5eAaImIp3nS	být
protokol	protokol	k1gInSc4	protokol
Internet	Internet	k1gInSc1	Internet
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
IP	IP	kA	IP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Protocol	Protocola	k1gFnPc2	Protocola
</s>
</p>
<p>
<s>
ICMP	ICMP	kA	ICMP
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Datagram	Datagram	k1gInSc1	Datagram
Protocol	Protocol	k1gInSc4	Protocol
</s>
</p>
<p>
<s>
IPX	IPX	kA	IPX
</s>
</p>
<p>
<s>
NWLink	NWLink	k1gInSc1	NWLink
</s>
</p>
<p>
<s>
Appletalk	Appletalk	k1gMnSc1	Appletalk
<g/>
/	/	kIx~	/
<g/>
DDP	DDP	kA	DDP
</s>
</p>
<p>
<s>
IPsec	IPsec	k1gMnSc1	IPsec
</s>
</p>
<p>
<s>
Packet	Packet	k1gInSc1	Packet
Layer	Layer	k1gInSc1	Layer
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
.25	.25	k4	.25
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MTP-3	MTP-3	k4	MTP-3
-	-	kIx~	-
Message	Messag	k1gInSc2	Messag
Transfer	transfer	k1gInSc1	transfer
Part-	Part-	k1gFnSc1	Part-
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
SCCP	SCCP	kA	SCCP
-	-	kIx~	-
Signalling	Signalling	k1gInSc1	Signalling
Connection	Connection	k1gInSc1	Connection
Control	Controla	k1gFnPc2	Controla
Part	parta	k1gFnPc2	parta
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Síťová	síťový	k2eAgFnSc1d1	síťová
vrstva	vrstva	k1gFnSc1	vrstva
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
funkční	funkční	k2eAgInPc4d1	funkční
a	a	k8xC	a
procesní	procesní	k2eAgInPc4d1	procesní
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
přenášení	přenášení	k1gNnSc4	přenášení
datových	datový	k2eAgFnPc2d1	datová
sekvencí	sekvence	k1gFnPc2	sekvence
proměnné	proměnný	k2eAgFnSc2d1	proměnná
délky	délka	k1gFnSc2	délka
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
kvality	kvalita	k1gFnSc2	kvalita
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnSc1	funkce
síťové	síťový	k2eAgFnSc2d1	síťová
vrstvy	vrstva	k1gFnSc2	vrstva
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Spojový	spojový	k2eAgInSc1d1	spojový
model	model	k1gInSc1	model
–	–	k?	–
nespojovaná	spojovaný	k2eNgFnSc1d1	nespojovaná
komunikace	komunikace	k1gFnSc1	komunikace
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
IP	IP	kA	IP
je	být	k5eAaImIp3nS	být
nespojitá	spojitý	k2eNgFnSc1d1	nespojitá
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
datagram	datagram	k1gInSc1	datagram
může	moct	k5eAaImIp3nS	moct
cestovat	cestovat	k5eAaImF	cestovat
od	od	k7c2	od
odesílatele	odesílatel	k1gMnSc2	odesílatel
k	k	k7c3	k
příjemci	příjemce	k1gMnSc3	příjemce
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
příjemce	příjemce	k1gMnSc1	příjemce
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
doručení	doručení	k1gNnSc4	doručení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
přijetí	přijetí	k1gNnSc1	přijetí
datagramu	datagram	k1gInSc2	datagram
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc1d2	vyšší
vrstvy	vrstva	k1gFnPc1	vrstva
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adresování	adresování	k1gNnSc1	adresování
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
host	host	k1gMnSc1	host
sítě	síť	k1gFnSc2	síť
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
adresu	adresa	k1gFnSc4	adresa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
adresa	adresa	k1gFnSc1	adresa
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
z	z	k7c2	z
hierarchického	hierarchický	k2eAgInSc2d1	hierarchický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
jsou	být	k5eAaImIp3nP	být
adresy	adresa	k1gFnPc1	adresa
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k9	jako
adresy	adresa	k1gFnPc1	adresa
internetového	internetový	k2eAgInSc2d1	internetový
protokolu	protokol	k1gInSc2	protokol
(	(	kIx(	(
<g/>
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přeposílání	Přeposílání	k1gNnSc1	Přeposílání
zpráv	zpráva	k1gFnPc2	zpráva
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sítě	síť	k1gFnPc1	síť
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
podsítě	podsíť	k1gFnPc4	podsíť
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
sítím	síť	k1gFnPc3	síť
pro	pro	k7c4	pro
komunikace	komunikace	k1gFnPc4	komunikace
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
sítě	síť	k1gFnPc4	síť
používají	používat	k5eAaImIp3nP	používat
speciální	speciální	k2eAgNnPc1d1	speciální
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
brány	brána	k1gFnPc4	brána
nebo	nebo	k8xC	nebo
routery	router	k1gMnPc4	router
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
předaly	předat	k5eAaPmAgInP	předat
pakety	paket	k1gInPc1	paket
mezi	mezi	k7c7	mezi
sítěmi	síť	k1gFnPc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
mobilních	mobilní	k2eAgFnPc2d1	mobilní
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
druhého	druhý	k4xOgMnSc2	druhý
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vše	všechen	k3xTgNnSc4	všechen
nastaveno	nastaven	k2eAgNnSc4d1	nastaveno
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
zprávy	zpráva	k1gFnPc1	zpráva
následovaly	následovat	k5eAaImAgFnP	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
4	[number]	k4	4
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
internetového	internetový	k2eAgInSc2d1	internetový
protokolu	protokol	k1gInSc2	protokol
nebyla	být	k5eNaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
mobility	mobilita	k1gFnSc2	mobilita
<g/>
.	.	kIx.	.
</s>
<s>
IPv	IPv	k?	IPv
<g/>
6	[number]	k4	6
má	mít	k5eAaImIp3nS	mít
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
lepší	dobrý	k2eAgNnSc1d2	lepší
řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vrstev	vrstva	k1gFnPc2	vrstva
modelu	model	k1gInSc2	model
OSI	OSI	kA	OSI
síťová	síťový	k2eAgFnSc1d1	síťová
vrstva	vrstva	k1gFnSc1	vrstva
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
žádosti	žádost	k1gFnPc4	žádost
o	o	k7c4	o
služby	služba	k1gFnPc4	služba
z	z	k7c2	z
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
servisní	servisní	k2eAgInPc4d1	servisní
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
vrstvu	vrstva	k1gFnSc4	vrstva
datového	datový	k2eAgNnSc2d1	datové
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
AES67	AES67	k4	AES67
</s>
</p>
<p>
<s>
DECnet	DECnet	k1gMnSc1	DECnet
</s>
</p>
<p>
<s>
Router	Router	k1gMnSc1	Router
</s>
</p>
