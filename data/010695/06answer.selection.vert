<s>
Síťová	síťový	k2eAgFnSc1d1	síťová
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
network	network	k1gInSc1	network
layer	layer	k1gInSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
třetí	třetí	k4xOgFnSc2	třetí
vrstvy	vrstva	k1gFnSc2	vrstva
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
