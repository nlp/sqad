<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c4	za
postavu	postava	k1gFnSc4	postava
Hugha	Hugh	k1gMnSc2	Hugh
Glasse	Glass	k1gMnSc2	Glass
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Revenant	Revenant	k1gInSc1	Revenant
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
i	i	k8xC	i
Cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Zlatým	zlatý	k2eAgInSc7d1	zlatý
glóbem	glóbus	k1gInSc7	glóbus
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
oceněn	ocenit	k5eAaPmNgInS	ocenit
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Letec	letec	k1gMnSc1	letec
a	a	k8xC	a
cenou	cena	k1gFnSc7	cena
Berlínského	berlínský	k2eAgInSc2d1	berlínský
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
za	za	k7c4	za
úlohu	úloha	k1gFnSc4	úloha
Romea	Romeo	k1gMnSc2	Romeo
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
postavy	postava	k1gFnPc1	postava
explozivní	explozivní	k2eAgFnPc1d1	explozivní
<g/>
,	,	kIx,	,
temperamentní	temperamentní	k2eAgFnPc1d1	temperamentní
<g/>
,	,	kIx,	,
zranitelné	zranitelný	k2eAgFnPc1d1	zranitelná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
psychicky	psychicky	k6eAd1	psychicky
narušené	narušený	k2eAgFnPc4d1	narušená
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
nejznámější	známý	k2eAgInPc4d3	nejznámější
filmy	film	k1gInPc4	film
patří	patřit	k5eAaImIp3nS	patřit
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chyť	chytit	k5eAaPmRp2nS	chytit
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
dokážeš	dokázat	k5eAaPmIp2nS	dokázat
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Letec	letec	k1gMnSc1	letec
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Počátek	počátek	k1gInSc1	počátek
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc4d1	velký
Gatsby	Gatsb	k1gInPc4	Gatsb
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vlk	Vlk	k1gMnSc1	Vlk
z	z	k7c2	z
Wall	Walla	k1gFnPc2	Walla
Street	Streeta	k1gFnPc2	Streeta
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
poblíž	poblíž	k7c2	poblíž
Hollywoodu	Hollywood	k1gInSc2	Hollywood
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
a	a	k8xC	a
distributora	distributor	k1gMnSc2	distributor
undergroundových	undergroundový	k2eAgInPc2d1	undergroundový
komiksů	komiks	k1gInPc2	komiks
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
Itala	Ital	k1gMnSc4	Ital
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
Němce	Němec	k1gMnSc4	Němec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
sekretářky	sekretářka	k1gFnPc1	sekretářka
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
s	s	k7c7	s
ruskými	ruský	k2eAgInPc7d1	ruský
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
přistěhovala	přistěhovat	k5eAaPmAgFnS	přistěhovat
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
během	během	k7c2	během
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
ze	z	k7c2	z
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
generace	generace	k1gFnSc2	generace
americké	americký	k2eAgFnSc2d1	americká
rodiny	rodina	k1gFnSc2	rodina
původem	původ	k1gInSc7	původ
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Leonardova	Leonardův	k2eAgFnSc1d1	Leonardova
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Indenbirken	Indenbirkna	k1gFnPc2	Indenbirkna
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
jako	jako	k8xS	jako
Yelena	Yelen	k2eAgFnSc1d1	Yelena
Smirnova	Smirnův	k2eAgFnSc1d1	Smirnova
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
ruská	ruský	k2eAgFnSc1d1	ruská
imigrantka	imigrantka	k1gFnSc1	imigrantka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
až	až	k9	až
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
budovat	budovat	k5eAaImF	budovat
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hercovy	hercův	k2eAgInPc1d1	hercův
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
opravdu	opravdu	k6eAd1	opravdu
neskutečně	skutečně	k6eNd1	skutečně
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
křestním	křestní	k2eAgNnSc6d1	křestní
jménu	jméno	k1gNnSc6	jméno
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
historka	historka	k1gFnSc1	historka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
těhotná	těhotná	k1gFnSc1	těhotná
matka	matka	k1gFnSc1	matka
zrovna	zrovna	k6eAd1	zrovna
prohlížela	prohlížet	k5eAaImAgFnS	prohlížet
obraz	obraz	k1gInSc4	obraz
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
a	a	k8xC	a
prvně	prvně	k?	prvně
ucítila	ucítit	k5eAaPmAgFnS	ucítit
kopnutí	kopnutí	k1gNnSc4	kopnutí
svého	svůj	k3xOyFgNnSc2	svůj
malého	malý	k2eAgNnSc2d1	malé
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
tímto	tento	k3xDgInSc7	tento
incidentem	incident	k1gInSc7	incident
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
pojmenuje	pojmenovat	k5eAaPmIp3nS	pojmenovat
právě	právě	k9	právě
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
proslulém	proslulý	k2eAgInSc6d1	proslulý
renesančním	renesanční	k2eAgInSc6d1	renesanční
umělci	umělec	k1gMnPc1	umělec
-	-	kIx~	-
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
stár	stár	k2eAgInSc4d1	stár
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
dál	daleko	k6eAd2	daleko
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
se	se	k3xPyFc4	se
vídal	vídat	k5eAaImAgMnS	vídat
jen	jen	k9	jen
občas	občas	k6eAd1	občas
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Seeds	Seedsa	k1gFnPc2	Seedsa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c4	na
John	John	k1gMnSc1	John
Marshall	Marshalla	k1gFnPc2	Marshalla
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
a	a	k8xC	a
následně	následně	k6eAd1	následně
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c4	na
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Enriched	Enriched	k1gMnSc1	Enriched
Studies	Studies	k1gMnSc1	Studies
(	(	kIx(	(
<g/>
LACES	LACES	kA	LACES
<g/>
)	)	kIx)	)
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
čtyř	čtyři	k4xCgInPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc1	několik
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
modelkami	modelka	k1gFnPc7	modelka
těch	ten	k3xDgNnPc2	ten
nejzvučnějších	zvučný	k2eAgNnPc2d3	nejzvučnější
jmen	jméno	k1gNnPc2	jméno
jako	jako	k8xC	jako
Kristen	Kristen	k2eAgInSc1d1	Kristen
Zangová	Zangový	k2eAgFnSc1d1	Zangový
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gisele	Gisela	k1gFnSc3	Gisela
Bündchen	Bündchna	k1gFnPc2	Bündchna
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
izraelská	izraelský	k2eAgFnSc1d1	izraelská
modelka	modelka	k1gFnSc1	modelka
Bar	bar	k1gInSc1	bar
Refaeli	Refael	k1gInSc6	Refael
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
Bar	bar	k1gInSc1	bar
Rafaeli	Rafael	k1gMnSc5	Rafael
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
vídán	vídat	k5eAaImNgInS	vídat
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
americké	americký	k2eAgFnSc2d1	americká
herečky	herečka	k1gFnSc2	herečka
a	a	k8xC	a
modelky	modelka	k1gFnSc2	modelka
Blake	Blak	k1gFnSc2	Blak
Lively	Livela	k1gFnSc2	Livela
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
blízké	blízký	k2eAgMnPc4d1	blízký
přátele	přítel	k1gMnPc4	přítel
patří	patřit	k5eAaImIp3nP	patřit
herci	herec	k1gMnPc1	herec
Tobey	Tobea	k1gFnSc2	Tobea
Maguire	Maguir	k1gInSc5	Maguir
<g/>
,	,	kIx,	,
Kevin	Kevin	k1gMnSc1	Kevin
Connolly	Connolla	k1gFnSc2	Connolla
<g/>
,	,	kIx,	,
Lukas	Lukas	k1gInSc4	Lukas
Haas	Haasa	k1gFnPc2	Haasa
a	a	k8xC	a
Kate	kat	k1gInSc5	kat
Winslet	Winslet	k1gInSc4	Winslet
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velkého	velký	k2eAgMnSc4d1	velký
sympatizanta	sympatizant	k1gMnSc4	sympatizant
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
taktéž	taktéž	k?	taktéž
různým	různý	k2eAgFnPc3d1	různá
humanitárním	humanitární	k2eAgFnPc3d1	humanitární
činnostem	činnost	k1gFnPc3	činnost
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Georgem	Georg	k1gInSc7	Georg
Clooneym	Clooneym	k1gInSc4	Clooneym
daroval	darovat	k5eAaPmAgMnS	darovat
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Haiti	Haiti	k1gNnSc2	Haiti
zasažené	zasažený	k2eAgFnSc2d1	zasažená
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
OSN	OSN	kA	OSN
Pan	Pan	k1gMnSc1	Pan
Ki-munem	Kiun	k1gInSc7	Ki-mun
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
poslem	posel	k1gMnSc7	posel
míru	míra	k1gFnSc4	míra
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
s	s	k7c7	s
přednostním	přednostní	k2eAgNnSc7d1	přednostní
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
toho	ten	k3xDgNnSc2	ten
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
natočil	natočit	k5eAaBmAgMnS	natočit
film	film	k1gInSc4	film
Je	být	k5eAaImIp3nS	být
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
konec	konec	k1gInSc1	konec
<g/>
?	?	kIx.	?
</s>
<s>
o	o	k7c6	o
dopadech	dopad	k1gInPc6	dopad
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
způsobeného	způsobený	k2eAgInSc2d1	způsobený
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
reklamách	reklama	k1gFnPc6	reklama
na	na	k7c4	na
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
výrobky	výrobek	k1gInPc4	výrobek
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
obilnin	obilnina	k1gFnPc2	obilnina
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgFnPc4d1	dětská
hračky	hračka	k1gFnPc4	hračka
nebo	nebo	k8xC	nebo
žvýkačky	žvýkačka	k1gFnPc4	žvýkačka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
ve	v	k7c6	v
"	"	kIx"	"
<g/>
výchovných	výchovný	k2eAgInPc6d1	výchovný
filmech	film	k1gInPc6	film
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Mickyho	Micky	k1gMnSc4	Micky
klub	klub	k1gInSc1	klub
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
(	(	kIx(	(
<g/>
Micky	micka	k1gFnPc1	micka
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Safety	Safet	k1gMnPc7	Safet
Club	club	k1gInSc4	club
<g/>
)	)	kIx)	)
a	a	k8xC	a
Co	co	k3yQnSc4	co
dělat	dělat	k5eAaImF	dělat
s	s	k7c7	s
rodičem	rodič	k1gMnSc7	rodič
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
bere	brát	k5eAaImIp3nS	brát
drogy	droga	k1gFnPc4	droga
(	(	kIx(	(
<g/>
How	How	k1gMnSc1	How
to	ten	k3xDgNnSc4	ten
Deal	Deal	k1gMnSc1	Deal
with	with	k1gMnSc1	with
a	a	k8xC	a
Parent	Parent	k1gMnSc1	Parent
Who	Who	k1gFnSc2	Who
Takes	Takes	k1gMnSc1	Takes
Drugs	Drugs	k1gInSc1	Drugs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
filmová	filmový	k2eAgFnSc1d1	filmová
kariéra	kariéra	k1gFnSc1	kariéra
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
odlišných	odlišný	k2eAgFnPc2d1	odlišná
etap	etapa	k1gFnPc2	etapa
<g/>
:	:	kIx,	:
Jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevoval	objevovat	k5eAaImAgInS	objevovat
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
The	The	k1gFnSc1	The
Outsiders	Outsiders	k1gInSc1	Outsiders
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parenthood	Parenthood	k1gInSc1	Parenthood
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Barbara	Barbara	k1gFnSc1	Barbara
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
či	či	k8xC	či
Growing	Growing	k1gInSc1	Growing
Pains	Painsa	k1gFnPc2	Painsa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
samotný	samotný	k2eAgInSc4d1	samotný
filmový	filmový	k2eAgInSc4d1	filmový
debut	debut	k1gInSc4	debut
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
počkat	počkat	k5eAaPmF	počkat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
Joshe	Josh	k1gFnSc2	Josh
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
hororového	hororový	k2eAgInSc2d1	hororový
snímku	snímek	k1gInSc2	snímek
Critters	Critters	k1gInSc1	Critters
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
následoval	následovat	k5eAaImAgInS	následovat
další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
thriller	thriller	k1gInSc1	thriller
Jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
břečťan	břečťan	k1gInSc4	břečťan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
jako	jako	k9	jako
osmnáctiletý	osmnáctiletý	k2eAgMnSc1d1	osmnáctiletý
zahrál	zahrát	k5eAaPmAgMnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
o	o	k7c4	o
rok	rok	k1gInSc4	rok
mladší	mladý	k2eAgFnSc2d2	mladší
Drew	Drew	k1gFnSc2	Drew
Barrymore	Barrymor	k1gMnSc5	Barrymor
avšak	avšak	k8xC	avšak
až	až	k9	až
snímkem	snímek	k1gInSc7	snímek
Dospívání	dospívání	k1gNnSc2	dospívání
po	po	k7c6	po
americku	americko	k1gNnSc6	americko
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
de	de	k?	de
Niro	Niro	k1gMnSc1	Niro
a	a	k8xC	a
Ellen	Ellen	k2eAgMnSc1d1	Ellen
Barkin	Barkin	k1gMnSc1	Barkin
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
vrýt	vrýt	k5eAaPmF	vrýt
divákům	divák	k1gMnPc3	divák
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
jako	jako	k8xS	jako
více	hodně	k6eAd2	hodně
než	než	k8xS	než
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
mladý	mladý	k2eAgMnSc1d1	mladý
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
byl	být	k5eAaImAgMnS	být
také	také	k9	také
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
oceněn	oceněn	k2eAgMnSc1d1	oceněn
-	-	kIx~	-
konkrétně	konkrétně	k6eAd1	konkrétně
cenou	cena	k1gFnSc7	cena
Asociace	asociace	k1gFnSc2	asociace
chicagských	chicagský	k2eAgMnPc2d1	chicagský
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
natočil	natočit	k5eAaBmAgMnS	natočit
snímek	snímek	k1gInSc4	snímek
Co	co	k9	co
žere	žrát	k5eAaImIp3nS	žrát
Gilberta	Gilbert	k1gMnSc4	Gilbert
Grapea	Grapeum	k1gNnSc2	Grapeum
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Johnnyho	Johnny	k1gMnSc2	Johnny
Deppa	Depp	k1gMnSc2	Depp
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bravurně	bravurně	k6eAd1	bravurně
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
jeho	jeho	k3xOp3gNnSc4	jeho
mentálně	mentálně	k6eAd1	mentálně
handicapovaného	handicapovaný	k2eAgMnSc2d1	handicapovaný
bratra	bratr	k1gMnSc2	bratr
Arnieho	Arnie	k1gMnSc2	Arnie
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
právem	právem	k6eAd1	právem
přisouzena	přisouzen	k2eAgFnSc1d1	přisouzena
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
DiCapriovým	DiCapriový	k2eAgInSc7d1	DiCapriový
milníkem	milník	k1gInSc7	milník
v	v	k7c6	v
herecké	herecký	k2eAgFnSc6d1	herecká
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
finančně	finančně	k6eAd1	finančně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
chválený	chválený	k2eAgInSc1d1	chválený
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
Dicaprio	Dicaprio	k1gMnSc1	Dicaprio
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
nejen	nejen	k6eAd1	nejen
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vysoce	vysoce	k6eAd1	vysoce
oceňovaný	oceňovaný	k2eAgMnSc1d1	oceňovaný
mladý	mladý	k2eAgMnSc1d1	mladý
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
letech	léto	k1gNnPc6	léto
pochopitelně	pochopitelně	k6eAd1	pochopitelně
přitáhlo	přitáhnout	k5eAaPmAgNnS	přitáhnout
obrovský	obrovský	k2eAgInSc4d1	obrovský
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
dokonce	dokonce	k9	dokonce
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
westernu	western	k1gInSc2	western
Rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
smrt	smrt	k1gFnSc1	smrt
režiséra	režisér	k1gMnSc2	režisér
Sama	Sam	k1gMnSc2	Sam
Raimiho	Raimi	k1gMnSc2	Raimi
s	s	k7c7	s
hereckými	herecký	k2eAgFnPc7d1	herecká
veličinami	veličina	k1gFnPc7	veličina
Sharon	Sharon	k1gInSc1	Sharon
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
Genem	gen	k1gInSc7	gen
Hackmanam	Hackmanam	k1gInSc1	Hackmanam
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
doby	doba	k1gFnPc1	doba
téměř	téměř	k6eAd1	téměř
neznámým	známý	k2eNgMnSc7d1	neznámý
Russellem	Russell	k1gMnSc7	Russell
Crowem	Crow	k1gMnSc7	Crow
<g/>
,	,	kIx,	,
Basketbalových	basketbalový	k2eAgInPc2d1	basketbalový
deníků	deník	k1gInPc2	deník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahrál	zahrát	k5eAaPmAgMnS	zahrát
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
mladistvého	mladistvý	k2eAgMnSc4d1	mladistvý
narkomana	narkoman	k1gMnSc4	narkoman
a	a	k8xC	a
Úplné	úplný	k2eAgNnSc1d1	úplné
zatmění	zatmění	k1gNnSc1	zatmění
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
prokletého	prokletý	k2eAgMnSc2d1	prokletý
básníka	básník	k1gMnSc2	básník
Arthura	Arthur	k1gMnSc2	Arthur
Rimbauda	Rimbaud	k1gMnSc2	Rimbaud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trojice	trojice	k1gFnSc1	trojice
snímků	snímek	k1gInPc2	snímek
sice	sice	k8xC	sice
nezískala	získat	k5eNaPmAgFnS	získat
větší	veliký	k2eAgInSc4d2	veliký
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jednoznačně	jednoznačně	k6eAd1	jednoznačně
posilovala	posilovat	k5eAaImAgFnS	posilovat
DiCapriův	DiCapriův	k2eAgInSc4d1	DiCapriův
status	status	k1gInSc4	status
velice	velice	k6eAd1	velice
nadějného	nadějný	k2eAgInSc2d1	nadějný
herce	herc	k1gInSc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
předvedených	předvedený	k2eAgInPc6d1	předvedený
výkonech	výkon	k1gInPc6	výkon
ve	v	k7c6	v
filmových	filmový	k2eAgNnPc6d1	filmové
dílech	dílo	k1gNnPc6	dílo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
a	a	k8xC	a
pochopitelně	pochopitelně	k6eAd1	pochopitelně
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
půvabnou	půvabný	k2eAgFnSc4d1	půvabná
<g/>
,	,	kIx,	,
zranitelnou	zranitelný	k2eAgFnSc4d1	zranitelná
tvář	tvář	k1gFnSc4	tvář
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
režisér	režisér	k1gMnSc1	režisér
Baz	Baz	k1gMnSc1	Baz
Luhrmann	Luhrmann	k1gMnSc1	Luhrmann
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
natočil	natočit	k5eAaBmAgMnS	natočit
moderní	moderní	k2eAgFnSc4d1	moderní
verzi	verze	k1gFnSc4	verze
Romea	Romeo	k1gMnSc2	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc2	Julie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
DiCaprio	DiCaprio	k6eAd1	DiCaprio
hrát	hrát	k5eAaImF	hrát
postavu	postava	k1gFnSc4	postava
samotného	samotný	k2eAgMnSc4d1	samotný
Romea	Romeo	k1gMnSc4	Romeo
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
rolí	role	k1gFnSc7	role
rebelujícího	rebelující	k2eAgMnSc2d1	rebelující
milovníka	milovník	k1gMnSc2	milovník
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
vkradl	vkradnout	k5eAaPmAgMnS	vkradnout
do	do	k7c2	do
podvědomí	podvědomí	k1gNnSc2	podvědomí
mnohých	mnohý	k2eAgFnPc2d1	mnohá
mladých	mladý	k2eAgFnPc2d1	mladá
slečen	slečna	k1gFnPc2	slečna
jakožto	jakožto	k8xS	jakožto
nový	nový	k2eAgInSc4d1	nový
sexsymbol	sexsymbol	k1gInSc4	sexsymbol
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
dával	dávat	k5eAaImAgMnS	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
pojmu	pojem	k1gInSc3	pojem
tzv.	tzv.	kA	tzv.
leománie	leománie	k1gFnSc2	leománie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
naplno	naplno	k6eAd1	naplno
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
až	až	k9	až
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
této	tento	k3xDgFnSc2	tento
nabyté	nabytý	k2eAgFnSc2d1	nabytá
popularity	popularita	k1gFnSc2	popularita
byl	být	k5eAaImAgInS	být
taktéž	taktéž	k?	taktéž
odměněn	odměnit	k5eAaPmNgInS	odměnit
Stříbrným	stříbrný	k2eAgMnSc7d1	stříbrný
medvědem	medvěd	k1gMnSc7	medvěd
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
vystřihnul	vystřihnout	k5eAaPmAgMnS	vystřihnout
roli	role	k1gFnSc4	role
problémového	problémový	k2eAgMnSc2d1	problémový
mladíka	mladík	k1gMnSc2	mladík
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Marvinův	Marvinův	k2eAgInSc4d1	Marvinův
pokoj	pokoj	k1gInSc4	pokoj
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Meryl	Meryl	k1gInSc1	Meryl
Streep	Streep	k1gInSc4	Streep
<g/>
,	,	kIx,	,
Dianou	Diana	k1gFnSc7	Diana
Keaton	Keaton	k1gInSc1	Keaton
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
de	de	k?	de
Niro	Niro	k1gMnSc1	Niro
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vybírá	vybírat	k5eAaImIp3nS	vybírat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
velkoprojektu	velkoprojekt	k1gInSc2	velkoprojekt
režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
do	do	k7c2	do
role	role	k1gFnSc2	role
chudého	chudý	k1gMnSc2	chudý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srdečného	srdečný	k2eAgMnSc2d1	srdečný
mladíka	mladík	k1gMnSc2	mladík
Jacka	Jacek	k1gMnSc2	Jacek
Dawsona	Dawson	k1gMnSc2	Dawson
ve	v	k7c6	v
velkorozpočtovém	velkorozpočtový	k2eAgInSc6d1	velkorozpočtový
romanticko-katastrofickém	romantickoatastrofický	k2eAgInSc6d1	romanticko-katastrofický
blockbusteru	blockbuster	k1gInSc6	blockbuster
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
Jack	Jack	k1gMnSc1	Jack
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
DiCapria	DiCaprium	k1gNnSc2	DiCaprium
bezhlavě	bezhlavě	k6eAd1	bezhlavě
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
dívky	dívka	k1gFnSc2	dívka
z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
fantasticky	fantasticky	k6eAd1	fantasticky
ztvárněnou	ztvárněný	k2eAgFnSc4d1	ztvárněná
Kate	kat	k1gInSc5	kat
Winslet	Winslet	k1gInSc1	Winslet
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
čelí	čelit	k5eAaImIp3nS	čelit
osudu	osud	k1gInSc3	osud
v	v	k7c6	v
nejslavnějším	slavný	k2eAgInSc6d3	nejslavnější
příběhu	příběh	k1gInSc6	příběh
o	o	k7c6	o
pýše	pýcha	k1gFnSc6	pýcha
a	a	k8xC	a
pádu	pád	k1gInSc6	pád
nejznámější	známý	k2eAgFnSc2d3	nejznámější
parní	parní	k2eAgFnSc2d1	parní
lodě	loď	k1gFnSc2	loď
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Fenomén	fenomén	k1gInSc1	fenomén
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vydělal	vydělat	k5eAaPmAgMnS	vydělat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
nehynoucí	hynoucí	k2eNgFnSc4d1	nehynoucí
slávu	sláva	k1gFnSc4	sláva
jeho	on	k3xPp3gMnSc2	on
stvořitele	stvořitel	k1gMnSc2	stvořitel
(	(	kIx(	(
<g/>
úžasných	úžasný	k2eAgFnPc2d1	úžasná
14	[number]	k4	14
oscarových	oscarový	k2eAgFnPc2d1	oscarová
nominací	nominace	k1gFnPc2	nominace
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
11	[number]	k4	11
proměněných	proměněný	k2eAgFnPc2d1	proměněná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
dotkl	dotknout	k5eAaPmAgInS	dotknout
i	i	k9	i
hlavních	hlavní	k2eAgFnPc2d1	hlavní
hereckých	herecký	k2eAgFnPc2d1	herecká
hvězd	hvězda	k1gFnPc2	hvězda
Leonarda	Leonardo	k1gMnSc2	Leonardo
DiCapria	DiCaprium	k1gNnSc2	DiCaprium
a	a	k8xC	a
Kate	kat	k1gInSc5	kat
Winslet	Winslet	k1gInSc1	Winslet
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
okamžiku	okamžik	k1gInSc6	okamžik
těmi	ten	k3xDgFnPc7	ten
nejslavnějšími	slavný	k2eAgFnPc7d3	nejslavnější
<g/>
,	,	kIx,	,
nejobletovanějšími	obletovaný	k2eAgFnPc7d3	nejobletovanější
mladými	mladý	k2eAgFnPc7d1	mladá
celebritami	celebrita	k1gFnPc7	celebrita
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
(	(	kIx(	(
<g/>
oběma	dva	k4xCgMnPc7	dva
bylo	být	k5eAaImAgNnS	být
lehce	lehko	k6eAd1	lehko
nad	nad	k7c4	nad
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
přicházelo	přicházet	k5eAaImAgNnS	přicházet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
filmových	filmový	k2eAgFnPc2d1	filmová
nabídek	nabídka	k1gFnPc2	nabídka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
špatná	špatný	k2eAgFnSc1d1	špatná
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
sebe-odcizení	sebedcizení	k1gNnSc1	sebe-odcizení
a	a	k8xC	a
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
pocit	pocit	k1gInSc1	pocit
vlastního	vlastní	k2eAgNnSc2d1	vlastní
sebeuspokojení	sebeuspokojení	k1gNnSc2	sebeuspokojení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
postihlo	postihnout	k5eAaPmAgNnS	postihnout
i	i	k9	i
samotného	samotný	k2eAgNnSc2d1	samotné
DiCapria	DiCaprium	k1gNnSc2	DiCaprium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
první	první	k4xOgFnSc6	první
etapě	etapa	k1gFnSc6	etapa
své	svůj	k3xOyFgFnPc4	svůj
kariéry	kariéra	k1gFnPc4	kariéra
DiCaprio	DiCaprio	k6eAd1	DiCaprio
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
z	z	k7c2	z
extrémně	extrémně	k6eAd1	extrémně
talentovaného	talentovaný	k2eAgMnSc2d1	talentovaný
mladého	mladý	k2eAgMnSc2d1	mladý
herce	herec	k1gMnSc2	herec
s	s	k7c7	s
obsazením	obsazení	k1gNnSc7	obsazení
v	v	k7c6	v
převážně	převážně	k6eAd1	převážně
nízkorozpočtových	nízkorozpočtový	k2eAgNnPc6d1	nízkorozpočtové
dramatech	drama	k1gNnPc6	drama
ve	v	k7c4	v
hvězdu	hvězda	k1gFnSc4	hvězda
první	první	k4xOgFnSc2	první
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
dovršena	dovršit	k5eAaPmNgFnS	dovršit
spektakulární	spektakulární	k2eAgFnSc1d1	spektakulární
<g/>
,	,	kIx,	,
mnoha	mnoho	k4c7	mnoho
Oscarami	Oscara	k1gFnPc7	Oscara
ověnčenou	ověnčený	k2eAgFnSc7d1	ověnčená
podívanou	podívaná	k1gFnSc7	podívaná
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
DiCaprio	DiCaprio	k6eAd1	DiCaprio
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
maximální	maximální	k2eAgFnPc4d1	maximální
možné	možný	k2eAgFnPc4d1	možná
popularity	popularita	k1gFnPc4	popularita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
pomalu	pomalu	k6eAd1	pomalu
utápět	utápět	k5eAaImF	utápět
ve	v	k7c6	v
snímcích	snímek	k1gInPc6	snímek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnSc3	některý
možná	možná	k9	možná
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
)	)	kIx)	)
rychle	rychle	k6eAd1	rychle
zapomenuty	zapomenut	k2eAgFnPc1d1	zapomenuta
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Woodyho	Woody	k1gMnSc2	Woody
Allena	Allen	k1gMnSc2	Allen
Celebrity	celebrita	k1gFnSc2	celebrita
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
uštěpačně	uštěpačně	k6eAd1	uštěpačně
zesměšňovat	zesměšňovat	k5eAaImF	zesměšňovat
poměry	poměr	k1gInPc4	poměr
ve	v	k7c6	v
filmovém	filmový	k2eAgInSc6d1	filmový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
vystřihnul	vystřihnout	k5eAaPmAgMnS	vystřihnout
roli	role	k1gFnSc4	role
mladé	mladý	k2eAgFnSc2d1	mladá
<g/>
,	,	kIx,	,
drzé	drzý	k2eAgFnSc2d1	drzá
<g/>
,	,	kIx,	,
domýšlivé	domýšlivý	k2eAgFnSc2d1	domýšlivá
superhvězdy	superhvězda	k1gFnSc2	superhvězda
-	-	kIx~	-
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
DiCaprio	DiCaprio	k6eAd1	DiCaprio
sdělil	sdělit	k5eAaPmAgMnS	sdělit
tisku	tisk	k1gInSc3	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
prvně	prvně	k?	prvně
zkusil	zkusit	k5eAaPmAgMnS	zkusit
i	i	k9	i
dvojroli	dvojrole	k1gFnSc4	dvojrole
v	v	k7c6	v
Muži	muž	k1gMnSc6	muž
se	se	k3xPyFc4	se
železnou	železný	k2eAgFnSc7d1	železná
maskou	maska	k1gFnSc7	maska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
zlodušského	zlodušský	k2eAgMnSc4d1	zlodušský
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
sympatického	sympatický	k2eAgInSc2d1	sympatický
bratra-dvojče	bratravojec	k1gMnSc5	bratra-dvojec
Filipa	Filip	k1gMnSc2	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
stal	stát	k5eAaPmAgInS	stát
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
kasovním	kasovní	k2eAgInSc7d1	kasovní
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
hvězdy	hvězda	k1gFnSc2	hvězda
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc1	Jerem
Irons	Irons	k1gInSc1	Irons
nebo	nebo	k8xC	nebo
Gerard	Gerard	k1gInSc1	Gerard
Depardieu	Depardieus	k1gInSc2	Depardieus
nezabránili	zabránit	k5eNaPmAgMnP	zabránit
negativním	negativní	k2eAgInPc3d1	negativní
ohlasům	ohlas	k1gInPc3	ohlas
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
nemilosrdně	milosrdně	k6eNd1	milosrdně
strhali	strhat	k5eAaPmAgMnP	strhat
<g/>
.	.	kIx.	.
</s>
<s>
DiCapriovým	DiCapriový	k2eAgInSc7d1	DiCapriový
dalším	další	k2eAgInSc7d1	další
projektem	projekt	k1gInSc7	projekt
byla	být	k5eAaImAgFnS	být
Pláž	pláž	k1gFnSc1	pláž
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Dannyho	Danny	k1gMnSc4	Danny
Boyla	Boyl	k1gMnSc4	Boyl
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Alexe	Alex	k1gMnSc5	Alex
Garlanda	Garlando	k1gNnPc1	Garlando
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
postavu	postava	k1gFnSc4	postava
amerického	americký	k2eAgMnSc2d1	americký
turisty	turista	k1gMnSc2	turista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
na	na	k7c6	na
tajném	tajný	k2eAgInSc6d1	tajný
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
ráj	ráj	k1gInSc1	ráj
nakonec	nakonec	k6eAd1	nakonec
nevydrží	vydržet	k5eNaPmIp3nS	vydržet
kvůli	kvůli	k7c3	kvůli
přirozené	přirozený	k2eAgFnSc3d1	přirozená
lidské	lidský	k2eAgFnSc3d1	lidská
destruktivní	destruktivní	k2eAgFnSc3d1	destruktivní
povaze	povaha	k1gFnSc3	povaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pláži	pláž	k1gFnSc6	pláž
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
objevila	objevit	k5eAaPmAgFnS	objevit
Tilda	Tilda	k1gFnSc1	Tilda
Swinton	Swinton	k1gInSc1	Swinton
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
podroben	podroben	k2eAgInSc1d1	podroben
tvrdé	tvrdý	k2eAgFnSc3d1	tvrdá
kritice	kritika	k1gFnSc3	kritika
a	a	k8xC	a
DiCaprio	DiCaprio	k6eAd1	DiCaprio
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vinou	vinou	k7c2	vinou
tlaku	tlak	k1gInSc2	tlak
na	na	k7c4	na
rok	rok	k1gInSc4	rok
odsunul	odsunout	k5eAaPmAgInS	odsunout
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
a	a	k8xC	a
filmového	filmový	k2eAgInSc2d1	filmový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
údobí	údobí	k1gNnSc2	údobí
tak	tak	k9	tak
Leonardo	Leonardo	k1gMnSc1	Leonardo
stačil	stačit	k5eAaBmAgMnS	stačit
spadnout	spadnout	k5eAaPmF	spadnout
z	z	k7c2	z
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
piedestalu	piedestal	k1gInSc2	piedestal
zbožňované	zbožňovaný	k2eAgFnSc2d1	zbožňovaná
star	star	k1gFnSc2	star
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vydobýt	vydobýt	k5eAaPmF	vydobýt
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Režiséry	režisér	k1gMnPc4	režisér
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
pomohli	pomoct	k5eAaPmAgMnP	pomoct
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
odpíchnout	odpíchnout	k5eAaPmF	odpíchnout
se	se	k3xPyFc4	se
ode	ode	k7c2	ode
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Scorsese	Scorsese	k1gFnSc2	Scorsese
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
jej	on	k3xPp3gInSc4	on
obsadil	obsadit	k5eAaPmAgMnS	obsadit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
kriminální	kriminální	k2eAgFnSc2d1	kriminální
komedie	komedie	k1gFnSc2	komedie
Chyť	chytit	k5eAaPmRp2nS	chytit
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
dokážeš	dokázat	k5eAaPmIp2nS	dokázat
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
roli	role	k1gFnSc4	role
Franka	Frank	k1gMnSc2	Frank
Abagnalea	Abagnaleus	k1gMnSc2	Abagnaleus
Jr	Jr	k1gMnSc2	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mladistvého	mladistvý	k1gMnSc4	mladistvý
podvodníčka	podvodníček	k1gMnSc4	podvodníček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
svým	svůj	k3xOyFgInSc7	svůj
šarmem	šarm	k1gInSc7	šarm
a	a	k8xC	a
sebevědomím	sebevědomí	k1gNnSc7	sebevědomí
podmaňuje	podmaňovat	k5eAaImIp3nS	podmaňovat
všechny	všechen	k3xTgMnPc4	všechen
okolo	okolo	k6eAd1	okolo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
sebejistě	sebejistě	k6eAd1	sebejistě
okrádá	okrádat	k5eAaImIp3nS	okrádat
stát	stát	k5eAaImF	stát
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
falešných	falešný	k2eAgMnPc2d1	falešný
šeků	šek	k1gMnPc2	šek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
part	part	k1gInSc4	part
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgMnSc1d1	legendární
filmový	filmový	k2eAgMnSc1d1	filmový
tvůrce	tvůrce	k1gMnSc1	tvůrce
Martin	Martin	k1gMnSc1	Martin
Scorsese	Scorsese	k1gFnSc1	Scorsese
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
pak	pak	k9	pak
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
Ganzích	Ganges	k1gInPc6	Ganges
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
obsadil	obsadit	k5eAaPmAgMnS	obsadit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
svých	svůj	k3xOyFgInPc2	svůj
dalších	další	k2eAgInPc2d1	další
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Letce	letec	k1gMnSc2	letec
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
oscarová	oscarový	k2eAgFnSc1d1	oscarová
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Skryté	skrytý	k2eAgFnSc2d1	skrytá
identity	identita	k1gFnSc2	identita
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Prokletého	prokletý	k2eAgInSc2d1	prokletý
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
o	o	k7c6	o
znovunabytí	znovunabytí	k1gNnSc6	znovunabytí
jeho	jeho	k3xOp3gFnSc2	jeho
proslulosti	proslulost	k1gFnSc2	proslulost
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
půlky	půlka	k1gFnSc2	půlka
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Krvavý	krvavý	k2eAgInSc4d1	krvavý
diamant	diamant	k1gInSc4	diamant
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
oscarová	oscarový	k2eAgFnSc1d1	oscarová
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Labyrint	labyrint	k1gInSc1	labyrint
lží	lež	k1gFnPc2	lež
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Kate	kat	k1gInSc5	kat
Winslet	Winslet	k1gInSc1	Winslet
v	v	k7c6	v
Nouzovém	nouzový	k2eAgInSc6d1	nouzový
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
veleúspěšném	veleúspěšný	k2eAgNnSc6d1	veleúspěšné
sci-fi	scii	k1gNnSc6	sci-fi
Christophera	Christophera	k1gFnSc1	Christophera
Nolana	Nolana	k1gFnSc1	Nolana
Počátek	počátek	k1gInSc1	počátek
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
již	již	k6eAd1	již
filmová	filmový	k2eAgFnSc1d1	filmová
superstar	superstar	k1gFnSc1	superstar
první	první	k4xOgFnSc2	první
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
žádaný	žádaný	k2eAgInSc1d1	žádaný
publikem	publikum	k1gNnSc7	publikum
<g/>
,	,	kIx,	,
chválený	chválený	k2eAgInSc1d1	chválený
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
oceňován	oceňován	k2eAgMnSc1d1	oceňován
(	(	kIx(	(
<g/>
4	[number]	k4	4
oscarové	oscarový	k2eAgFnSc2d1	oscarová
nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
herecké	herecký	k2eAgFnPc4d1	herecká
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
10	[number]	k4	10
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
proměněné	proměněný	k2eAgInPc4d1	proměněný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohé	mnohý	k2eAgInPc4d1	mnohý
jeho	jeho	k3xOp3gInPc4	jeho
snímky	snímek	k1gInPc4	snímek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Prokletý	prokletý	k2eAgInSc1d1	prokletý
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
Počátek	počátek	k1gInSc1	počátek
<g/>
)	)	kIx)	)
taktéž	taktéž	k?	taktéž
trhají	trhat	k5eAaImIp3nP	trhat
rekordy	rekord	k1gInPc1	rekord
v	v	k7c6	v
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ostatně	ostatně	k6eAd1	ostatně
pravidelně	pravidelně	k6eAd1	pravidelně
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
vrchní	vrchní	k2eAgFnPc4d1	vrchní
příčky	příčka	k1gFnPc4	příčka
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgInPc2d1	placený
herců	herc	k1gInPc2	herc
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
mu	on	k3xPp3gMnSc3	on
opět	opět	k6eAd1	opět
utekl	utéct	k5eAaPmAgInS	utéct
Oscar	Oscar	k1gInSc1	Oscar
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vlk	Vlk	k1gMnSc1	Vlk
z	z	k7c2	z
Wall	Walla	k1gFnPc2	Walla
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
tak	tak	k6eAd1	tak
po	po	k7c6	po
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
pouze	pouze	k6eAd1	pouze
nominovaným	nominovaný	k2eAgNnSc7d1	nominované
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
i	i	k9	i
první	první	k4xOgFnSc4	první
nominaci	nominace	k1gFnSc4	nominace
za	za	k7c4	za
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
4	[number]	k4	4
producentů	producent	k1gMnPc2	producent
filmu	film	k1gInSc2	film
Vlk	Vlk	k1gMnSc1	Vlk
z	z	k7c2	z
Wall	Walla	k1gFnPc2	Walla
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Revenant	Revenant	k1gInSc1	Revenant
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
<g/>
.	.	kIx.	.
</s>
