<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c4	za
postavu	postava	k1gFnSc4	postava
Hugha	Hugh	k1gMnSc2	Hugh
Glasse	Glass	k1gMnSc2	Glass
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Revenant	Revenant	k1gInSc1	Revenant
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
i	i	k8xC	i
Cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
