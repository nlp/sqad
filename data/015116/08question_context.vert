<s>
Barentsovo	Barentsův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
Barentshavet	Barentshavet	k1gInSc1
;	;	kIx,
rusky	rusky	k6eAd1
Б	Б	k?
м	м	k?
Barencevo	Barencevo	k1gNnSc4
more	mor	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
okrajové	okrajový	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
mezi	mezi	k7c7
severním	severní	k2eAgNnSc7d1
pobřežím	pobřeží	k1gNnSc7
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
Norska	Norsko	k1gNnSc2
a	a	k8xC
Ruska	Rusko	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Medvědím	medvědí	k2eAgInSc7d1
ostrovem	ostrov	k1gInSc7
<g/>
,	,	kIx,
Špicberky	Špicberky	k1gInPc7
<g/>
,	,	kIx,
Zemí	zem	k1gFnSc7
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
a	a	k8xC
Novou	nový	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>