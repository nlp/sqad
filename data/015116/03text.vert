<s>
Barentsovo	Barentsův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Barentsovo	Barentsův	k2eAgNnSc4d1
moře	moře	k1gNnSc4
Maximální	maximální	k2eAgFnSc1d1
hloubka	hloubka	k1gFnSc1
</s>
<s>
600	#num#	k4
m	m	kA
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
405	#num#	k4
000	#num#	k4
km²	km²	k?
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
75	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
40	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadřazený	nadřazený	k2eAgInSc4d1
celek	celek	k1gInSc4
</s>
<s>
Severní	severní	k2eAgInSc1d1
ledový	ledový	k2eAgInSc1d1
oceán	oceán	k1gInSc1
Sousední	sousední	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
Norské	norský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Grónské	grónský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Bílé	bílý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Karské	karský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgInSc1d1
ledový	ledový	k2eAgInSc1d1
oceán	oceán	k1gInSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
Přítoky	přítok	k1gInPc4
</s>
<s>
Tuloma	Tuloma	k1gFnSc1
<g/>
,	,	kIx,
Kola	Kola	k1gFnSc1
<g/>
,	,	kIx,
Jokanga	Jokanga	k1gFnSc1
<g/>
,	,	kIx,
Pasvikelva	Pasvikelva	k1gFnSc1
<g/>
,	,	kIx,
Tanaelva	Tanaelva	k1gFnSc1
<g/>
,	,	kIx,
Pečora	Pečora	k1gFnSc1
</s>
<s>
Barentsovo	Barentsův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
Barentshavet	Barentshavet	k1gInSc1
;	;	kIx,
rusky	rusky	k6eAd1
Б	Б	k?
м	м	k?
Barencevo	Barencevo	k1gNnSc4
more	mor	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
okrajové	okrajový	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
mezi	mezi	k7c7
severním	severní	k2eAgNnSc7d1
pobřežím	pobřeží	k1gNnSc7
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
Norska	Norsko	k1gNnSc2
a	a	k8xC
Ruska	Rusko	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Medvědím	medvědí	k2eAgInSc7d1
ostrovem	ostrov	k1gInSc7
<g/>
,	,	kIx,
Špicberky	Špicberky	k1gInPc7
<g/>
,	,	kIx,
Zemí	zem	k1gFnSc7
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
a	a	k8xC
Novou	nový	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
záhadologů	záhadolog	k1gMnPc2
se	se	k3xPyFc4
pod	pod	k7c4
hladinu	hladina	k1gFnSc4
dnešního	dnešní	k2eAgNnSc2d1
Barenstova	Barenstův	k2eAgNnSc2d1
moře	moře	k1gNnSc2
kdysi	kdysi	k6eAd1
propadla	propadnout	k5eAaPmAgFnS
bájná	bájný	k2eAgFnSc1d1
Hyperborea	Hyperborea	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
podle	podle	k7c2
nich	on	k3xPp3gMnPc2
dnes	dnes	k6eAd1
nad	nad	k7c4
hladinu	hladina	k1gFnSc4
vyčnívají	vyčnívat	k5eAaImIp3nP
její	její	k3xOp3gFnPc4
někdejší	někdejší	k2eAgFnPc4d1
nejvyšší	vysoký	k2eAgFnPc4d3
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
jimiž	jenž	k3xRgMnPc7
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
Špicberky	Špicberky	k1gInPc1
<g/>
,	,	kIx,
Země	zem	k1gFnPc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
,	,	kIx,
či	či	k8xC
Nová	nový	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Od	od	k7c2
starověku	starověk	k1gInSc2
žily	žít	k5eAaImAgFnP
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Barentsova	Barentsův	k2eAgNnSc2d1
moře	moře	k1gNnSc2
ugrofinské	ugrofinský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
-	-	kIx~
Samové	Sam	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
oblasti	oblast	k1gFnSc2
začali	začít	k5eAaPmAgMnP
pronikat	pronikat	k5eAaImF
Vikingové	Viking	k1gMnPc1
<g/>
,	,	kIx,
Novgoroďané	Novgoroďan	k1gMnPc1
a	a	k8xC
Pomorové	Pomor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
starých	starý	k2eAgInPc2d1
časů	čas	k1gInPc2
nazývali	nazývat	k5eAaImAgMnP
námořníci	námořník	k1gMnPc1
a	a	k8xC
kartografové	kartograf	k1gMnPc1
Barentsovo	Barentsův	k2eAgNnSc4d1
moře	moře	k1gNnSc4
jako	jako	k8xC,k8xS
-	-	kIx~
Severní	severní	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Siverské	Siverský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Moskevské	moskevský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Ruské	ruský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Studené	Studené	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Pečorské	Pečorský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Temné	temný	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
nejrozšířenějším	rozšířený	k2eAgInSc7d3
názvem	název	k1gInSc7
bylo	být	k5eAaImAgNnS
Murmanské	murmanský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgInSc4d1
pojmenování	pojmenování	k1gNnSc1
moře	moře	k1gNnSc4
bylo	být	k5eAaImAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1853	#num#	k4
po	po	k7c6
holandském	holandský	k2eAgMnSc6d1
mořeplavci	mořeplavec	k1gMnSc6
Willemu	Willem	k1gMnSc6
Barentsovi	Barents	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Námořníky	námořník	k1gMnPc4
bylo	být	k5eAaImAgNnS
moře	moře	k1gNnSc1
přezdíváno	přezdíván	k2eAgNnSc1d1
"	"	kIx"
<g/>
Ďáblův	ďáblův	k2eAgInSc1d1
taneční	taneční	k2eAgInSc1d1
parket	parket	k1gInSc1
<g/>
"	"	kIx"
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gFnSc3
nepředvídatelnosti	nepředvídatelnost	k1gFnSc3
a	a	k8xC
obtížnosti	obtížnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Geografická	geografický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
405	#num#	k4
000	#num#	k4
km²	km²	k?
</s>
<s>
Hloubka	hloubka	k1gFnSc1
<g/>
:	:	kIx,
průměrná	průměrný	k2eAgFnSc1d1
hloubka	hloubka	k1gFnSc1
229	#num#	k4
m	m	kA
<g/>
,	,	kIx,
maximální	maximální	k2eAgFnSc1d1
600	#num#	k4
m	m	kA
(	(	kIx(
<g/>
na	na	k7c6
západě	západ	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Příliv	příliv	k1gInSc1
<g/>
:	:	kIx,
max	max	kA
<g/>
.	.	kIx.
11	#num#	k4
m	m	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
vody	voda	k1gFnSc2
na	na	k7c6
povrchu	povrch	k1gInSc6
<g/>
:	:	kIx,
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
:	:	kIx,
7-12	7-12	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
0	#num#	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
zimě	zima	k1gFnSc6
3-5	3-5	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
<g/>
,	,	kIx,
méně	málo	k6eAd2
než	než	k8xS
-1	-1	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
</s>
<s>
Podíl	podíl	k1gInSc1
plovoucího	plovoucí	k2eAgInSc2d1
ledu	led	k1gInSc2
<g/>
:	:	kIx,
méně	málo	k6eAd2
než	než	k8xS
75	#num#	k4
%	%	kIx~
povrchu	povrch	k1gInSc2
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
létě	léto	k1gNnSc6
pouze	pouze	k6eAd1
malé	malý	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
na	na	k7c6
severu	sever	k1gInSc6
</s>
<s>
Salinita	salinita	k1gFnSc1
<g/>
:	:	kIx,
35	#num#	k4
promile	promile	k1gNnPc2
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
<g/>
,	,	kIx,
32	#num#	k4
promile	promile	k1gNnPc2
na	na	k7c6
severu	sever	k1gInSc6
</s>
<s>
Klimatické	klimatický	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
:	:	kIx,
subarktické	subarktický	k2eAgNnSc1d1
<g/>
,	,	kIx,
ovlivněné	ovlivněný	k2eAgNnSc1d1
teplým	teplý	k2eAgInSc7d1
Golfským	golfský	k2eAgInSc7d1
proudem	proud	k1gInSc7
</s>
<s>
Mořské	mořský	k2eAgInPc1d1
proudy	proud	k1gInPc1
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golfský	golfský	k2eAgInSc1d1
(	(	kIx(
<g/>
obtéká	obtékat	k5eAaImIp3nS
jižní	jižní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
a	a	k8xC
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
k	k	k7c3
Nové	Nové	k2eAgFnSc3d1
zemi	zem	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgInSc1d1
studený	studený	k2eAgInSc1d1
proud	proud	k1gInSc1
od	od	k7c2
Karského	karský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
(	(	kIx(
<g/>
teče	téct	k5eAaImIp3nS
opačným	opačný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
<g/>
:	:	kIx,
většinou	většinou	k6eAd1
skalnaté	skalnatý	k2eAgInPc1d1
(	(	kIx(
<g/>
kromě	kromě	k7c2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fjordy	fjord	k1gInPc1
(	(	kIx(
<g/>
zejména	zejména	k9
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Norska	Norsko	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
<g/>
:	:	kIx,
souostroví	souostroví	k1gNnSc1
Špicberky	Špicberky	k1gFnPc1
<g/>
,	,	kIx,
ostrov	ostrov	k1gInSc1
Kolgujev	Kolgujev	k1gFnSc2
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
<g/>
,	,	kIx,
delta	delta	k1gFnSc1
řeky	řeka	k1gFnSc2
Pečory	Pečora	k1gFnSc2
<g/>
,	,	kIx,
poloostrov	poloostrov	k1gInSc4
Kanin	Kanin	k2eAgInSc4d1
na	na	k7c6
jihu	jih	k1gInSc6
</s>
<s>
Významné	významný	k2eAgInPc1d1
přístavy	přístav	k1gInPc1
<g/>
:	:	kIx,
Murmansk	Murmansk	k1gInSc1
-	-	kIx~
v	v	k7c6
zimě	zima	k1gFnSc6
nezamrzá	zamrzat	k5eNaImIp3nS
<g/>
,	,	kIx,
Vadsø	Vadsø	k1gMnSc1
<g/>
,	,	kIx,
Kirkenes	Kirkenes	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matrix	Matrix	k1gInSc1
2001	#num#	k4
<g/>
↑	↑	k?
М	М	k?
п	п	k?
и	и	k?
Е	Е	k?
(	(	kIx(
<g/>
З	З	k?
Е	Е	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
antarctic	antarctice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
su	su	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Barentsovo	Barentsův	k2eAgNnSc4d1
moře	moře	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Barentsovo	Barentsův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Severní	severní	k2eAgInSc1d1
ledový	ledový	k2eAgInSc1d1
oceán	oceán	k1gInSc1
Moře	moře	k1gNnSc2
</s>
<s>
Baffinovo	Baffinův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Barentsovo	Barentsův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Beaufortovo	Beaufortův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Bílé	bílý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Čukotské	čukotský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Grónské	grónský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Karské	karský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Lincolnovo	Lincolnův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
moře	moře	k1gNnSc1
Laptěvů	Laptěv	k1gInPc2
</s>
<s>
Norské	norský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Pečorské	Pečorský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Východosibiřské	východosibiřský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Wandelovo	Wandelův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Zálivy	záliv	k1gInPc7
</s>
<s>
Amundsenův	Amundsenův	k2eAgInSc1d1
záliv	záliv	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
huba	huba	k1gFnSc1
</s>
<s>
Dvinský	Dvinský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
</s>
<s>
Hudsonův	Hudsonův	k2eAgInSc1d1
záliv	záliv	k1gInSc1
</s>
<s>
Jamesova	Jamesův	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
</s>
<s>
Kandalakšský	Kandalakšský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
</s>
<s>
Oněžský	oněžský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
Průlivy	průliv	k1gInPc1
</s>
<s>
Barrowův	Barrowův	k2eAgInSc1d1
</s>
<s>
Bellotův	Bellotův	k2eAgInSc1d1
</s>
<s>
Beringův	Beringův	k2eAgInSc1d1
</s>
<s>
Britský	britský	k2eAgInSc1d1
kanál	kanál	k1gInSc1
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1
</s>
<s>
Davisův	Davisův	k2eAgInSc1d1
</s>
<s>
D.	D.	kA
Laptěva	Laptěva	k1gFnSc1
</s>
<s>
Dolphin	Dolphin	k2eAgInSc1d1
and	and	k?
Union	union	k1gInSc1
</s>
<s>
Franklinův	Franklinův	k2eAgInSc1d1
</s>
<s>
Fury	Fur	k2eAgInPc1d1
a	a	k8xC
Hekla	heknout	k5eAaPmAgFnS
</s>
<s>
Hudsonův	Hudsonův	k2eAgInSc1d1
</s>
<s>
Jonesův	Jonesův	k2eAgInSc1d1
</s>
<s>
Jugorskij	Jugorskít	k5eAaPmRp2nS
šar	šar	k?
</s>
<s>
Karská	karský	k2eAgNnPc4d1
vrata	vrata	k1gNnPc4
</s>
<s>
Kennedyho	Kennedyze	k6eAd1
kanál	kanál	k1gInSc1
</s>
<s>
Lancasterův	Lancasterův	k2eAgInSc1d1
</s>
<s>
Langův	Langův	k2eAgInSc1d1
</s>
<s>
Malygina	Malygina	k1gFnSc1
</s>
<s>
Matočkin	Matočkin	k1gInSc1
šar	šar	k?
</s>
<s>
McClintockův	McClintockův	k2eAgInSc1d1
kanál	kanál	k1gInSc1
</s>
<s>
M	M	kA
<g/>
'	'	kIx"
<g/>
Clureho	Clure	k1gMnSc2
</s>
<s>
Ovcyna	Ovcyna	k6eAd1
</s>
<s>
Parryho	Parryze	k6eAd1
kanál	kanál	k1gInSc1
</s>
<s>
Pearyho	Pearyze	k6eAd1
kanál	kanál	k1gInSc1
</s>
<s>
Prince	princ	k1gMnSc2
regenta	regens	k1gMnSc2
</s>
<s>
Prince	princ	k1gMnSc4
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
Robesonův	Robesonův	k2eAgInSc1d1
</s>
<s>
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
</s>
<s>
Sannikova	Sannikův	k2eAgFnSc1d1
</s>
<s>
Sokolského	sokolský	k2eAgInSc2d1
</s>
<s>
Vikomta	vikomt	k1gMnSc4
Melvilla	Melvill	k1gMnSc4
</s>
<s>
Vilkického	Vilkický	k2eAgInSc2d1
</s>
<s>
Viktoriin	Viktoriin	k2eAgInSc1d1
</s>
<s>
Wellingtonův	Wellingtonův	k2eAgInSc1d1
kanál	kanál	k1gInSc1
Proudy	proud	k1gInPc1
</s>
<s>
Golfský	golfský	k2eAgInSc1d1
proud	proud	k1gInSc1
Příkopy	příkop	k1gInPc1
</s>
<s>
Litkeho	Litkeze	k6eAd1
hlubina	hlubina	k1gFnSc1
Hřbety	hřbet	k1gInPc7
</s>
<s>
val	val	k1gInSc1
Alpha	Alph	k1gMnSc2
</s>
<s>
Čukotská	čukotský	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
</s>
<s>
Gakkelův	Gakkelův	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
</s>
<s>
Lomonosovův	Lomonosovův	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
</s>
<s>
Mendělejevův	Mendělejevův	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
183234	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4004511-0	4004511-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85011827	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315126299	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85011827	#num#	k4
</s>
