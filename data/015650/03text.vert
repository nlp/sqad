<s>
Piazza	Piazza	k1gFnSc1
Armerina	Armerina	k1gFnSc1
</s>
<s>
Piazza	Piazza	k1gFnSc1
Armerina	Armerin	k1gMnSc2
Piazza	Piazz	k1gMnSc2
Armerina	Armerin	k1gMnSc2
–	–	k?
panorama	panorama	k1gNnSc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
697	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Piazza	Piazza	k1gFnSc1
Armerina	Armerina	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
304,5	304,5	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
21	#num#	k4
775	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
71,5	71,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.comune.piazzaarmerina.en.it	www.comune.piazzaarmerina.en.it	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
0935	#num#	k4
PSČ	PSČ	kA
</s>
<s>
94015	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
EN	EN	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Piazza	Piazza	k1gFnSc1
Armerina	Armerino	k1gNnSc2
je	být	k5eAaImIp3nS
italské	italský	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
,	,	kIx,
součást	součást	k1gFnSc4
volného	volný	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
obcí	obec	k1gFnPc2
Enna	Enn	k1gInSc2
v	v	k7c6
autonomní	autonomní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Sicílie	Sicílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
21	#num#	k4
439	#num#	k4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Situace	situace	k1gFnSc1
</s>
<s>
Piazza	Piazza	k1gFnSc1
Armerina	Armerino	k1gNnSc2
leží	ležet	k5eAaImIp3nS
32	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
Enny	Enna	k1gFnSc2
ve	v	k7c6
vulkanickém	vulkanický	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
pracují	pracovat	k5eAaImIp3nP
hlavně	hlavně	k9
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
(	(	kIx(
<g/>
pěstování	pěstování	k1gNnSc1
oliv	oliva	k1gFnPc2
<g/>
,	,	kIx,
vinařství	vinařství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
oděvním	oděvní	k2eAgMnSc6d1
a	a	k8xC
stavebním	stavební	k2eAgNnSc6d1
odvětví	odvětví	k1gNnSc6
průmyslu	průmysl	k1gInSc2
a	a	k8xC
v	v	k7c6
cestovním	cestovní	k2eAgInSc6d1
ruchu	ruch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
okresu	okres	k1gInSc2
Piazza	Piazz	k1gMnSc2
Armerina	Armerin	k1gMnSc2
patří	patřit	k5eAaImIp3nP
obce	obec	k1gFnPc1
Monte	Mont	k1gInSc5
s	s	k7c7
Colle	Colle	k1gNnSc7
Mira	Mira	k1gFnSc1
<g/>
,	,	kIx,
Floristella	Floristella	k1gFnSc1
<g/>
,	,	kIx,
Grottacalda	Grottacalda	k1gFnSc1
<g/>
,	,	kIx,
Polleri	Polleri	k1gNnSc1
<g/>
,	,	kIx,
Santa	Santa	k1gFnSc1
Croce	Croce	k1gFnSc1
<g/>
,	,	kIx,
Ileano	Ileana	k1gFnSc5
<g/>
,	,	kIx,
Azzolina	Azzolina	k1gFnSc1
<g/>
,	,	kIx,
Farrugio	Farrugio	k1gNnSc1
a	a	k8xC
Serrafina	Serrafina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousedními	sousední	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
jsou	být	k5eAaImIp3nP
Aidone	Aidon	k1gInSc5
<g/>
,	,	kIx,
Assoro	Assora	k1gFnSc5
<g/>
,	,	kIx,
Barrafranca	Barrafranca	k1gMnSc1
<g/>
,	,	kIx,
Caltagirone	Caltagiron	k1gMnSc5
(	(	kIx(
<g/>
CT	CT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Enna	Enna	k1gFnSc1
<g/>
,	,	kIx,
Mazzarino	Mazzarino	k1gNnSc1
(	(	kIx(
<g/>
CL	CL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mineo	Mineo	k1gMnSc1
(	(	kIx(
<g/>
CT	CT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mirabella	Mirabella	k1gMnSc1
Imbaccari	Imbaccar	k1gFnSc2
(	(	kIx(
<g/>
CT	CT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pietraperzia	Pietraperzius	k1gMnSc4
<g/>
,	,	kIx,
Raddusa	Raddus	k1gMnSc4
(	(	kIx(
<g/>
CT	CT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
San	San	k1gMnSc1
Cono	Cono	k1gMnSc1
(	(	kIx(
<g/>
CT	CT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
San	San	k1gMnSc1
Michele	Michel	k1gInSc2
di	di	k?
Ganzaria	Ganzarium	k1gNnSc2
(	(	kIx(
CT	CT	kA
<g/>
)	)	kIx)
a	a	k8xC
Valguarnera	Valguarner	k1gMnSc2
Caropepe	Caropep	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
osídlena	osídlit	k5eAaPmNgFnS
lidmi	člověk	k1gMnPc7
od	od	k7c2
prehistorických	prehistorický	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
byly	být	k5eAaImAgInP
nalezeny	nalézt	k5eAaBmNgInP,k5eAaPmNgInP
archeologické	archeologický	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc1
pravěké	pravěký	k2eAgFnSc2d1
osady	osada	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
vrchu	vrch	k1gInSc6
byla	být	k5eAaImAgFnS
nekropole	nekropole	k1gFnSc1
osady	osada	k1gFnSc2
Hybla	Hybla	k1gFnSc1
Geleatis	Geleatis	k1gFnSc1
z	z	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
podle	podle	k7c2
arabského	arabský	k2eAgInSc2d1
nápisu	nápis	k1gInSc2
'	'	kIx"
<g/>
Iblâtasah	Iblâtasah	k1gInSc1
obsazená	obsazený	k2eAgNnPc1d1
Araby	Arab	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římané	Říman	k1gMnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
době	doba	k1gFnSc6
helénské	helénský	k2eAgFnSc6d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dosvědčuje	dosvědčovat	k5eAaImIp3nS
blízká	blízký	k2eAgFnSc1d1
Villa	Villa	k1gFnSc1
Romana	Roman	k1gMnSc2
del	del	k?
Casale	Casala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1076	#num#	k4
obec	obec	k1gFnSc1
dobyli	dobýt	k5eAaPmAgMnP
Normané	Norman	k1gMnPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Rogera	Rogero	k1gNnSc2
I.	I.	kA
a	a	k8xC
osvobodili	osvobodit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
od	od	k7c2
Arabů	Arab	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1161	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
hoře	hora	k1gFnSc6
usadili	usadit	k5eAaPmAgMnP
Langobardi	Langobard	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
za	za	k7c2
vlády	vláda	k1gFnSc2
Viléma	Vilém	k1gMnSc2
I.	I.	kA
a	a	k8xC
Viléma	Vilém	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
sídlo	sídlo	k1gNnSc4
přestavěli	přestavět	k5eAaPmAgMnP
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
sicilskou	sicilský	k2eAgFnSc4d1
metropoli	metropole	k1gFnSc4
<g/>
,	,	kIx,
pochvalně	pochvalně	k6eAd1
označovanou	označovaný	k2eAgFnSc4d1
nobilissimum	nobilissimum	k1gInSc4
Lombardorum	Lombardorum	k1gInSc1
oppidum	oppidum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
13	#num#	k4
<g/>
.	.	kIx.
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
po	po	k7c6
získání	získání	k1gNnSc6
městských	městský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
opevnění	opevnění	k1gNnSc1
hradební	hradební	k2eAgMnSc1d1
zdí	zdít	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
město	město	k1gNnSc1
rychle	rychle	k6eAd1
rozrůstalo	rozrůstat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
brzy	brzy	k6eAd1
nemohlo	moct	k5eNaImAgNnS
pojmout	pojmout	k5eAaPmF
všechny	všechen	k3xTgInPc4
nově	nově	k6eAd1
obydlené	obydlený	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
a	a	k8xC
rozrostlo	rozrůst	k5eAaPmAgNnS
se	se	k3xPyFc4
na	na	k7c4
okolní	okolní	k2eAgInPc4d1
kopce	kopec	k1gInPc4
a	a	k8xC
svahy	svah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1296	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
sešel	sejít	k5eAaPmAgInS
sicilský	sicilský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
Bourbonů	bourbon	k1gInPc2
se	se	k3xPyFc4
město	město	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1817	#num#	k4
stalo	stát	k5eAaPmAgNnS
sídlem	sídlo	k1gNnSc7
biskupství	biskupství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
zdejších	zdejší	k2eAgFnPc2d1
minerálních	minerální	k2eAgFnPc2d1
lázní	lázeň	k1gFnPc2
byly	být	k5eAaImAgInP
v	v	k7c6
letech	let	k1gInPc6
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
zrušeny	zrušit	k5eAaPmNgFnP
také	také	k9
železniční	železniční	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
do	do	k7c2
města	město	k1gNnSc2
lze	lze	k6eAd1
dostat	dostat	k5eAaPmF
pouze	pouze	k6eAd1
po	po	k7c6
silnici	silnice	k1gFnSc6
<g/>
,	,	kIx,
odbočkou	odbočka	k1gFnSc7
z	z	k7c2
dálnice	dálnice	k1gFnSc2
Katánie-Palermo	Katánie-Palerma	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejná	veřejný	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
funguje	fungovat	k5eAaImIp3nS
omezeně	omezeně	k6eAd1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
pro	pro	k7c4
dopravu	doprava	k1gFnSc4
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
do	do	k7c2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
katedrála	katedrála	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Vítězné	vítězný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Maria	Maria	k1gFnSc1
Santissima	Santissima	k1gFnSc1
delle	delle	k1gFnSc1
Vittorie	Vittorie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
trojlodní	trojlodní	k2eAgFnSc1d1
bazilika	bazilika	k1gFnSc1
s	s	k7c7
kupolí	kupole	k1gFnSc7
v	v	k7c6
křížení	křížení	k1gNnSc6
<g/>
,	,	kIx,
dominanta	dominanta	k1gFnSc1
panoramatu	panorama	k1gNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
biskupství	biskupství	k1gNnSc1
diecéze	diecéze	k1gFnSc2
Piazza	Piazz	k1gMnSc2
Armerina	Armerin	k1gMnSc2
<g/>
;	;	kIx,
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
na	na	k7c6
středověkých	středověký	k2eAgInPc6d1
základech	základ	k1gInPc6
od	od	k7c2
roku	rok	k1gInSc2
1604	#num#	k4
<g/>
;	;	kIx,
pozdně	pozdně	k6eAd1
gotická	gotický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
jižní	jižní	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
;	;	kIx,
uvnitř	uvnitř	k6eAd1
cenná	cenný	k2eAgFnSc1d1
sikulo-byzantská	sikulo-byzantský	k2eAgFnSc1d1
poutní	poutní	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
ve	v	k7c6
stříbrném	stříbrný	k2eAgInSc6d1
barokním	barokní	k2eAgInSc6d1
oltáři	oltář	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Museo	Musea	k1gFnSc5
Diocesano	Diocesana	k1gFnSc5
–	–	k?
sídlí	sídlet	k5eAaImIp3nS
při	při	k7c6
katedrále	katedrála	k1gFnSc6
<g/>
,	,	kIx,
vystavují	vystavovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
sakrální	sakrální	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
<g/>
,	,	kIx,
sochy	socha	k1gFnPc1
<g/>
,	,	kIx,
preciosa	preciosa	k1gFnSc1
a	a	k8xC
paramenta	paramenta	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Palazzo	Palazza	k1gFnSc5
Trigona	Trigona	k1gFnSc1
–	–	k?
na	na	k7c6
náměstí	náměstí	k1gNnSc6
u	u	k7c2
katedrály	katedrála	k1gFnSc2
<g/>
,	,	kIx,
stavba	stavba	k1gFnSc1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
obrazárna	obrazárna	k1gFnSc1
</s>
<s>
Palazzo	Palazza	k1gFnSc5
di	di	k?
Citta	Citt	k1gMnSc2
–	–	k?
radnice	radnice	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1612	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
sále	sál	k1gInSc6
freska	fresko	k1gNnSc2
</s>
<s>
Palazzo	Palazza	k1gFnSc5
Senatoro	Senatora	k1gFnSc5
–	–	k?
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Piazza	Piazz	k1gMnSc2
Garibaldi	Garibald	k1gMnPc5
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Ondřeje	Ondřej	k1gMnSc2
(	(	kIx(
<g/>
Chiesa	Chiesa	k1gFnSc1
di	di	k?
San	San	k1gFnSc1
Andrea	Andrea	k1gFnSc1
<g/>
)	)	kIx)
nejstarší	starý	k2eAgFnSc1d3
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
,	,	kIx,
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1096	#num#	k4
<g/>
,	,	kIx,
uvnitř	uvnitř	k6eAd1
cyklus	cyklus	k1gInSc1
fresek	freska	k1gFnPc2
ze	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
oslava	oslava	k1gFnSc1
krále	král	k1gMnSc2
Rogera	Roger	k1gMnSc2
I.	I.	kA
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Ignáce	Ignác	k1gMnSc2
(	(	kIx(
<g/>
Chiesa	Chiesa	k1gFnSc1
di	di	k?
Sant	Sant	k1gInSc1
<g/>
'	'	kIx"
<g/>
Ignazio	Ignazio	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
barokní	barokní	k2eAgInSc4d1
chrám	chrám	k1gInSc4
s	s	k7c7
někdejší	někdejší	k2eAgFnSc7d1
jezuitskou	jezuitský	k2eAgFnSc7d1
kolejí	kolej	k1gFnSc7
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Štěpána	Štěpána	k1gFnSc1
(	(	kIx(
<g/>
chiesa	chiesa	k1gFnSc1
di	di	k?
San	San	k1gFnSc1
Stefano	Stefana	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Rocha	Rocha	k1gFnSc1
(	(	kIx(
<g/>
Chiesa	Chiesa	k1gFnSc1
di	di	k?
San	San	k1gMnSc1
Rocco	Rocco	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Hrad	hrad	k1gInSc4
Spinelli	Spinell	k1gMnPc1
z	z	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
aragonský	aragonský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
</s>
<s>
Garibaldiho	Garibaldize	k6eAd1
divadlo	divadlo	k1gNnSc1
–	–	k?
novorenesanční	novorenesanční	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
čtvrtiny	čtvrtina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
připomíná	připomínat	k5eAaImIp3nS
zdejší	zdejší	k2eAgMnSc1d1
Garibaldiho	Garibaldi	k1gMnSc4
pobyt	pobyt	k1gInSc4
</s>
<s>
Dva	dva	k4xCgInPc4
pomníky	pomník	k1gInPc4
generála	generál	k1gMnSc4
Antonia	Antonio	k1gMnSc2
Gascina	Gascin	k1gMnSc2
</s>
<s>
Okolí	okolí	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
atrakcí	atrakce	k1gFnSc7
je	být	k5eAaImIp3nS
antická	antický	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
Villa	Villa	k1gFnSc1
Romana	Roman	k1gMnSc2
del	del	k?
Casale	Casala	k1gFnSc6
s	s	k7c7
unikátně	unikátně	k6eAd1
dochovaným	dochovaný	k2eAgInSc7d1
souborem	soubor	k1gInSc7
mozaiky	mozaika	k1gFnSc2
a	a	k8xC
nástěnných	nástěnný	k2eAgFnPc2d1
maleb	malba	k1gFnPc2
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
4	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
</s>
<s>
Ikona	ikona	k1gFnSc1
v	v	k7c6
katedrále	katedrála	k1gFnSc6
</s>
<s>
San	San	k?
Stefano	Stefana	k1gFnSc5
</s>
<s>
San	San	k?
Rocco	Rocco	k6eAd1
</s>
<s>
Palazzo	Palazza	k1gFnSc5
Trigona	Trigona	k1gFnSc1
</s>
<s>
Radnice	radnice	k1gFnSc1
</s>
<s>
Garibaldiho	Garibaldize	k6eAd1
divadlo	divadlo	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Popolazione	Popolazion	k1gInSc5
Residente	resident	k1gMnSc5
al	ala	k1gFnPc2
1	#num#	k4
<g/>
°	°	k?
Gennaio	Gennaio	k6eAd1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Sicilia	Sicilia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guide	Guid	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touring	Touring	k1gInSc1
Club	club	k1gInSc4
Milano	Milana	k1gFnSc5
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
-	-	kIx~
<g/>
259	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Piazza	Piazz	k1gMnSc2
Armerina	Armerin	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.enna-sicilia.it/english/piazza_armerina.htm	http://www.enna-sicilia.it/english/piazza_armerina.htm	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Sicílie	Sicílie	k1gFnSc1
•	•	k?
Obce	obec	k1gFnSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Enna	Enn	k1gInSc2
</s>
<s>
Agira	Agira	k1gFnSc1
•	•	k?
Aidone	Aidon	k1gInSc5
•	•	k?
Assoro	Assora	k1gFnSc5
•	•	k?
Barrafranca	Barrafranc	k2eAgFnSc1d1
•	•	k?
Calascibetta	Calascibetta	k1gFnSc1
•	•	k?
Catenanuova	Catenanuov	k1gInSc2
•	•	k?
Centuripe	Centurip	k1gInSc5
•	•	k?
Cerami	Cera	k1gFnPc7
•	•	k?
Enna	Enn	k1gInSc2
•	•	k?
Gagliano	Gagliana	k1gFnSc5
Castelferrato	Castelferrat	k2eAgNnSc1d1
•	•	k?
Leonforte	Leonfort	k1gInSc5
•	•	k?
Nicosia	Nicosium	k1gNnSc2
•	•	k?
Nissoria	Nissorium	k1gNnSc2
•	•	k?
Piazza	Piazz	k1gMnSc2
Armerina	Armerin	k1gMnSc2
•	•	k?
Pietraperzia	Pietraperzius	k1gMnSc2
•	•	k?
Regalbuto	Regalbut	k2eAgNnSc1d1
•	•	k?
Sperlinga	Sperling	k1gMnSc2
•	•	k?
Troina	Troin	k1gMnSc2
•	•	k?
Valguarnera	Valguarner	k1gMnSc2
Caropepe	Caropep	k1gInSc5
•	•	k?
Villarosa	Villarosa	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4045823-4	4045823-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83137436	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
196207912	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83137436	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
