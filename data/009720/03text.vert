<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
památník	památník	k1gInSc1	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
je	být	k5eAaImIp3nS	být
funkcionalistický	funkcionalistický	k2eAgInSc1d1	funkcionalistický
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hrubá	hrubý	k2eAgFnSc1d1	hrubá
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Vítkově	Vítkův	k2eAgInSc6d1	Vítkův
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Jana	Jan	k1gMnSc2	Jan
Zázvorky	zázvorka	k1gMnSc2	zázvorka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
uctění	uctění	k1gNnSc1	uctění
památky	památka	k1gFnSc2	památka
československých	československý	k2eAgMnPc2d1	československý
legionářů	legionář	k1gMnPc2	legionář
a	a	k8xC	a
československého	československý	k2eAgInSc2d1	československý
odboje	odboj	k1gInSc2	odboj
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
tu	tu	k6eAd1	tu
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
památníku	památník	k1gInSc6	památník
uloženy	uložen	k2eAgInPc1d1	uložen
ostatky	ostatek	k1gInPc1	ostatek
neznámého	známý	k2eNgMnSc2d1	neznámý
vojína	vojín	k1gMnSc2	vojín
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
a	a	k8xC	a
z	z	k7c2	z
dukelské	dukelský	k2eAgFnSc2d1	Dukelská
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
památníku	památník	k1gInSc6	památník
instalována	instalovat	k5eAaBmNgFnS	instalovat
expozice	expozice	k1gFnSc1	expozice
Křižovatky	křižovatka	k1gFnSc2	křižovatka
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
památníku	památník	k1gInSc2	památník
je	být	k5eAaImIp3nS	být
i	i	k9	i
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
bronzová	bronzový	k2eAgFnSc1d1	bronzová
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
kopce	kopec	k1gInSc2	kopec
==	==	k?	==
</s>
</p>
<p>
<s>
Husité	husita	k1gMnPc1	husita
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
válečný	válečný	k2eAgInSc1d1	válečný
úspěch	úspěch	k1gInSc1	úspěch
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1420	[number]	k4	1420
zde	zde	k6eAd1	zde
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
porazili	porazit	k5eAaPmAgMnP	porazit
křižácké	křižácký	k2eAgNnSc4d1	křižácké
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
známý	známý	k2eAgInSc1d1	známý
pražský	pražský	k2eAgInSc1d1	pražský
kopec	kopec	k1gInSc1	kopec
zhruba	zhruba	k6eAd1	zhruba
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Vítkov	Vítkov	k1gInSc4	Vítkov
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
pražském	pražský	k2eAgMnSc6d1	pražský
měšťanovi	měšťan	k1gMnSc6	měšťan
Vítkovi	Vítek	k1gMnSc6	Vítek
z	z	k7c2	z
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgMnS	mít
vinici	vinice	k1gFnSc4	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
doložená	doložený	k2eAgFnSc1d1	doložená
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
pražským	pražský	k2eAgInSc7d1	pražský
Žižkovem	Žižkov	k1gInSc7	Žižkov
pochází	pocházet	k5eAaImIp3nS	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1041	[number]	k4	1041
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Národní	národní	k2eAgInSc1d1	národní
památník	památník	k1gInSc1	památník
==	==	k?	==
</s>
</p>
<p>
<s>
Idea	idea	k1gFnSc1	idea
národního	národní	k2eAgInSc2d1	národní
památníku	památník	k1gInSc2	památník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
připomenutí	připomenutí	k1gNnSc4	připomenutí
porážek	porážka	k1gFnPc2	porážka
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
hrdost	hrdost	k1gFnSc4	hrdost
a	a	k8xC	a
odvahu	odvaha	k1gFnSc4	odvaha
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
zpočátku	zpočátku	k6eAd1	zpočátku
ryze	ryze	k6eAd1	ryze
lokální	lokální	k2eAgInSc4d1	lokální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1882	[number]	k4	1882
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
místních	místní	k2eAgMnPc2d1	místní
občanů	občan	k1gMnPc2	občan
založen	založen	k2eAgInSc4d1	založen
Spolek	spolek	k1gInSc4	spolek
pro	pro	k7c4	pro
zbudování	zbudování	k1gNnSc4	zbudování
pomníku	pomník	k1gInSc2	pomník
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Žižkově	Žižkov	k1gInSc6	Žižkov
a	a	k8xC	a
nebýt	být	k5eNaImF	být
budovatelského	budovatelský	k2eAgInSc2d1	budovatelský
étosu	étos	k1gInSc2	étos
nové	nový	k2eAgFnSc2d1	nová
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
všechno	všechen	k3xTgNnSc4	všechen
úplně	úplně	k6eAd1	úplně
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
nekropoli	nekropole	k1gFnSc4	nekropole
synů	syn	k1gMnPc2	syn
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
legionářů	legionář	k1gMnPc2	legionář
a	a	k8xC	a
osvoboditelů	osvoboditel	k1gMnPc2	osvoboditel
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
pouze	pouze	k6eAd1	pouze
otevírat	otevírat	k5eAaImF	otevírat
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
Památník	památník	k1gInSc1	památník
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
v	v	k7c6	v
letech	let	k1gInPc6	let
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
skladiště	skladiště	k1gNnSc1	skladiště
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
památník	památník	k1gInSc1	památník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
poničen	poničen	k2eAgMnSc1d1	poničen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
památník	památník	k1gInSc1	památník
využit	využít	k5eAaPmNgInS	využít
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
tu	tu	k6eAd1	tu
pohřbíváni	pohřbíván	k2eAgMnPc1d1	pohřbíván
významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
tu	tu	k6eAd1	tu
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
,	,	kIx,	,
zrušené	zrušený	k2eAgInPc1d1	zrušený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Gottwaldovo	Gottwaldův	k2eAgNnSc1d1	Gottwaldovo
tělo	tělo	k1gNnSc1	tělo
zpopelněno	zpopelněn	k2eAgNnSc1d1	zpopelněno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
členů	člen	k1gMnPc2	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
odvezeny	odvezen	k2eAgInPc4d1	odvezen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nynější	nynější	k2eAgFnSc6d1	nynější
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
nově	nově	k6eAd1	nově
rekonstruovaném	rekonstruovaný	k2eAgInSc6d1	rekonstruovaný
památníku	památník	k1gInSc6	památník
nachází	nacházet	k5eAaImIp3nS	nacházet
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
Křižovatky	křižovatka	k1gFnSc2	křižovatka
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
historické	historický	k2eAgInPc4d1	historický
okamžiky	okamžik	k1gInPc4	okamžik
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
podepsání	podepsání	k1gNnSc4	podepsání
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
a	a	k8xC	a
období	období	k1gNnSc4	období
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
komunistický	komunistický	k2eAgInSc1d1	komunistický
převrat	převrat	k1gInSc1	převrat
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
invazi	invaze	k1gFnSc6	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
federalizace	federalizace	k1gFnPc4	federalizace
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
socialismu	socialismus	k1gInSc2	socialismus
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zánik	zánik	k1gInSc1	zánik
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
také	také	k9	také
nově	nově	k6eAd1	nově
otevřená	otevřený	k2eAgFnSc1d1	otevřená
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrob	hrob	k1gInSc1	hrob
neznámého	známý	k2eNgMnSc2d1	neznámý
vojína	vojín	k1gMnSc2	vojín
==	==	k?	==
</s>
</p>
<p>
<s>
Hrob	hrob	k1gInSc1	hrob
neznámého	známý	k2eNgMnSc2d1	neznámý
vojína	vojín	k1gMnSc2	vojín
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
provizorně	provizorně	k6eAd1	provizorně
zřízen	zřídit	k5eAaPmNgInS	zřídit
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
uloženy	uložen	k2eAgInPc1d1	uložen
ostatky	ostatek	k1gInPc1	ostatek
bojovníka	bojovník	k1gMnSc2	bojovník
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
místem	místo	k1gNnSc7	místo
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
nacistické	nacistický	k2eAgFnSc3d1	nacistická
okupaci	okupace	k1gFnSc3	okupace
<g/>
;	;	kIx,	;
roku	rok	k1gInSc3	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
hrob	hrob	k1gInSc1	hrob
nacisty	nacista	k1gMnSc2	nacista
zničen	zničen	k2eAgMnSc1d1	zničen
a	a	k8xC	a
ostatky	ostatek	k1gInPc1	ostatek
odvezeny	odvezen	k2eAgInPc1d1	odvezen
a	a	k8xC	a
zpopelněny	zpopelněn	k2eAgInPc1d1	zpopelněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
hrobu	hrob	k1gInSc2	hrob
neznámého	známý	k2eNgMnSc4d1	neznámý
vojína	vojín	k1gMnSc4	vojín
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
však	však	k9	však
nedaly	dát	k5eNaPmAgInP	dát
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
převezení	převezení	k1gNnSc3	převezení
ostatků	ostatek	k1gInPc2	ostatek
od	od	k7c2	od
Zborova	Zborov	k1gInSc2	Zborov
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
padlý	padlý	k2eAgMnSc1d1	padlý
voják	voják	k1gMnSc1	voják
Karpatsko-dukelské	karpatskoukelský	k2eAgFnSc2d1	karpatsko-dukelská
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Hrob	hrob	k1gInSc1	hrob
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
před	před	k7c7	před
jezdeckou	jezdecký	k2eAgFnSc7d1	jezdecká
sochou	socha	k1gFnSc7	socha
kryje	krýt	k5eAaImIp3nS	krýt
šikmá	šikmý	k2eAgFnSc1d1	šikmá
žulová	žulový	k2eAgFnSc1d1	Žulová
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Sláva	Sláv	k1gMnSc2	Sláv
hrdinům	hrdina	k1gMnPc3	hrdina
padlým	padlý	k1gMnSc7	padlý
za	za	k7c4	za
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hrobu	hrob	k1gInSc2	hrob
neznámého	známý	k2eNgMnSc2d1	neznámý
vojína	vojín	k1gMnSc2	vojín
uloženy	uložit	k5eAaPmNgInP	uložit
také	také	k9	také
nově	nově	k6eAd1	nově
exhumované	exhumovaný	k2eAgInPc1d1	exhumovaný
ostatky	ostatek	k1gInPc1	ostatek
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zborovských	zborovský	k2eAgMnPc2d1	zborovský
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
původní	původní	k2eAgInSc4d1	původní
úmysl	úmysl	k1gInSc4	úmysl
během	během	k7c2	během
výstavby	výstavba	k1gFnSc2	výstavba
památníku	památník	k1gInSc2	památník
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Žižkova	Žižkův	k2eAgFnSc1d1	Žižkova
socha	socha	k1gFnSc1	socha
==	==	k?	==
</s>
</p>
<p>
<s>
Žižkova	Žižkův	k2eAgFnSc1d1	Žižkova
socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
bronzová	bronzový	k2eAgFnSc1d1	bronzová
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Žižkovu	Žižkův	k2eAgFnSc4d1	Žižkova
sochu	socha	k1gFnSc4	socha
si	se	k3xPyFc3	se
Vítkov	Vítkov	k1gInSc1	Vítkov
musel	muset	k5eAaImAgInS	muset
počkat	počkat	k5eAaPmF	počkat
<g/>
:	:	kIx,	:
po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
dvou	dva	k4xCgFnPc6	dva
soutěžích	soutěž	k1gFnPc6	soutěž
stále	stále	k6eAd1	stále
nespokojený	spokojený	k2eNgInSc4d1	nespokojený
sbor	sbor	k1gInSc4	sbor
porotců	porotce	k1gMnPc2	porotce
přímo	přímo	k6eAd1	přímo
oslovil	oslovit	k5eAaPmAgInS	oslovit
tři	tři	k4xCgMnPc4	tři
sochaře	sochař	k1gMnPc4	sochař
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
<g/>
:	:	kIx,	:
Ladislava	Ladislav	k1gMnSc2	Ladislav
Šalouna	Šaloun	k1gMnSc2	Šaloun
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Mařatku	Mařatek	k1gInSc2	Mařatek
a	a	k8xC	a
Bohumila	Bohumil	k1gMnSc4	Bohumil
Kafku	Kafka	k1gMnSc4	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Klasicky	klasicky	k6eAd1	klasicky
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
návrh	návrh	k1gInSc1	návrh
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
sochy	socha	k1gFnSc2	socha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
skicoval	skicovat	k5eAaImAgMnS	skicovat
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
monumentální	monumentální	k2eAgFnSc4d1	monumentální
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
sochu	socha	k1gFnSc4	socha
<g/>
.	.	kIx.	.
</s>
<s>
Kafkovy	Kafkův	k2eAgInPc1d1	Kafkův
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
pečlivě	pečlivě	k6eAd1	pečlivě
vedl	vést	k5eAaImAgMnS	vést
<g/>
,	,	kIx,	,
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
dokonalost	dokonalost	k1gFnSc4	dokonalost
a	a	k8xC	a
historickou	historický	k2eAgFnSc4d1	historická
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konzultacích	konzultace	k1gFnPc6	konzultace
se	s	k7c7	s
sochařem	sochař	k1gMnSc7	sochař
se	se	k3xPyFc4	se
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
historikové	historik	k1gMnPc1	historik
i	i	k8xC	i
hipologové	hipolog	k1gMnPc1	hipolog
<g/>
:	:	kIx,	:
kůň-model	kůňodlo	k1gNnPc2	kůň-modlo
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
ze	z	k7c2	z
státních	státní	k2eAgInPc2d1	státní
hřebčinců	hřebčinec	k1gInPc2	hřebčinec
<g/>
.	.	kIx.	.
</s>
<s>
Vzrušené	vzrušený	k2eAgFnPc1d1	vzrušená
diskuse	diskuse	k1gFnPc1	diskuse
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
týkaly	týkat	k5eAaImAgInP	týkat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
Žižka	Žižka	k1gMnSc1	Žižka
neměl	mít	k5eNaImAgMnS	mít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
mít	mít	k5eAaImF	mít
raději	rád	k6eAd2	rád
napřaženou	napřažený	k2eAgFnSc4d1	napřažená
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
mají	mít	k5eAaImIp3nP	mít
vlát	vlát	k5eAaImF	vlát
vlasy	vlas	k1gInPc7	vlas
atd.	atd.	kA	atd.
Kafka	Kafka	k1gMnSc1	Kafka
dokončil	dokončit	k5eAaPmAgMnS	dokončit
sádrový	sádrový	k2eAgInSc4d1	sádrový
model	model	k1gInSc4	model
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
modelu	model	k1gInSc2	model
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Odlita	odlit	k2eAgFnSc1d1	odlita
byla	být	k5eAaImAgFnS	být
až	až	k9	až
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
a	a	k8xC	a
odhalena	odhalit	k5eAaPmNgFnS	odhalit
na	na	k7c4	na
výroční	výroční	k2eAgInSc4d1	výroční
den	den	k1gInSc4	den
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
váží	vážit	k5eAaImIp3nS	vážit
16,5	[number]	k4	16,5
tuny	tuna	k1gFnSc2	tuna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
9.6	[number]	k4	9.6
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
120	[number]	k4	120
bronzových	bronzový	k2eAgFnPc2d1	bronzová
částí	část	k1gFnPc2	část
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
5000	[number]	k4	5000
šroubů	šroub	k1gInPc2	šroub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
restaurována	restaurován	k2eAgFnSc1d1	restaurována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnSc1d1	další
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
průčelí	průčelí	k1gNnSc6	průčelí
podstavce	podstavec	k1gInSc2	podstavec
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
sochy	socha	k1gFnSc2	socha
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
původní	původní	k2eAgInSc1d1	původní
velký	velký	k2eAgInSc1d1	velký
československý	československý	k2eAgInSc1d1	československý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Otakar	Otakar	k1gMnSc1	Otakar
Španiel	Španiel	k1gMnSc1	Španiel
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgInSc7d1	nový
socialistickým	socialistický	k2eAgInSc7d1	socialistický
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
sem	sem	k6eAd1	sem
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
kopie	kopie	k1gFnSc1	kopie
původního	původní	k2eAgInSc2d1	původní
československého	československý	k2eAgInSc2d1	československý
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
Otakara	Otakar	k1gMnSc2	Otakar
Španiela	Španiel	k1gMnSc2	Španiel
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
kopie	kopie	k1gFnSc2	kopie
<g/>
:	:	kIx,	:
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Oppl	Oppl	k1gMnSc1	Oppl
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Ceplecha	Ceplecha	k1gMnSc1	Ceplecha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
za	za	k7c7	za
jezdeckou	jezdecký	k2eAgFnSc7d1	jezdecká
sochou	socha	k1gFnSc7	socha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgNnPc4d1	velké
bronzová	bronzový	k2eAgNnPc4d1	bronzové
vrata	vrata	k1gNnPc4	vrata
vstupní	vstupní	k2eAgFnSc2d1	vstupní
brány	brána	k1gFnSc2	brána
do	do	k7c2	do
památníku	památník	k1gInSc2	památník
Josefa	Josef	k1gMnSc2	Josef
Malejovského	Malejovský	k1gMnSc2	Malejovský
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
ozdobena	ozdobit	k5eAaPmNgFnS	ozdobit
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
výjevům	výjev	k1gInPc3	výjev
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
husitského	husitský	k2eAgNnSc2d1	husitské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
padlých	padlý	k1gMnPc2	padlý
je	být	k5eAaImIp3nS	být
instalována	instalován	k2eAgFnSc1d1	instalována
socha	socha	k1gFnSc1	socha
Raněný	raněný	k1gMnSc1	raněný
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umělec	umělec	k1gMnSc1	umělec
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Umělecky	umělecky	k6eAd1	umělecky
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
svícen	svícen	k1gInSc1	svícen
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Horejce	Horejec	k1gMnSc2	Horejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
za	za	k7c7	za
památníkem	památník	k1gInSc7	památník
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgFnSc1d1	velká
nefunkční	funkční	k2eNgFnSc1d1	nefunkční
fontána	fontána	k1gFnSc1	fontána
Karla	Karel	k1gMnSc2	Karel
Štipla	Štipla	k1gMnSc2	Štipla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgFnPc7	dva
kruhovými	kruhový	k2eAgFnPc7d1	kruhová
mísami	mísa	k1gFnPc7	mísa
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
uprostřed	uprostřed	k7c2	uprostřed
kruhové	kruhový	k2eAgFnSc2d1	kruhová
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Zdobena	zdoben	k2eAgFnSc1d1	zdobena
je	být	k5eAaImIp3nS	být
plastikami	plastika	k1gFnPc7	plastika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
soustavou	soustava	k1gFnSc7	soustava
trysek	tryska	k1gFnPc2	tryska
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
fontány	fontána	k1gFnSc2	fontána
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
dvě	dva	k4xCgFnPc4	dva
sousoší	sousoší	k1gNnPc2	sousoší
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sousoší	sousoší	k1gNnSc1	sousoší
Karla	Karel	k1gMnSc2	Karel
Pokorného	Pokorný	k1gMnSc2	Pokorný
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
představuje	představovat	k5eAaImIp3nS	představovat
alegorii	alegorie	k1gFnSc4	alegorie
Vítězství	vítězství	k1gNnSc2	vítězství
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
jej	on	k3xPp3gMnSc4	on
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Jiří	Jiří	k1gMnSc1	Jiří
Dušek	Dušek	k1gMnSc1	Dušek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
sousoší	sousoší	k1gNnSc1	sousoší
Karla	Karel	k1gMnSc2	Karel
Lidického	lidický	k2eAgMnSc2d1	lidický
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1964-1972	[number]	k4	1964-1972
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
vzletně	vzletně	k6eAd1	vzletně
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výstavba	výstavba	k1gFnSc1	výstavba
památníku	památník	k1gInSc2	památník
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
památníku	památník	k1gInSc2	památník
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
míře	míra	k1gFnSc6	míra
použito	použít	k5eAaPmNgNnS	použít
přírodního	přírodní	k2eAgInSc2d1	přírodní
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
žul	žula	k1gFnPc2	žula
a	a	k8xC	a
mramorů	mramor	k1gInPc2	mramor
domácího	domácí	k2eAgInSc2d1	domácí
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgNnSc4d1	architektonické
řešení	řešení	k1gNnSc4	řešení
památníku	památník	k1gInSc2	památník
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Jan	Jan	k1gMnSc1	Jan
Zázvorka	zázvorka	k1gMnSc1	zázvorka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
výzdobu	výzdoba	k1gFnSc4	výzdoba
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Vincenc	Vincenc	k1gMnSc1	Vincenc
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Malejovský	Malejovský	k1gMnSc1	Malejovský
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Štípl	štípnout	k5eAaPmAgMnS	štípnout
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Švec	Švec	k1gMnSc1	Švec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Štursa	Štursa	k1gFnSc1	Štursa
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
kopce	kopec	k1gInSc2	kopec
Vítkova	Vítkov	k1gInSc2	Vítkov
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
východo-západním	východoápadní	k2eAgInSc7d1	východo-západní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
143	[number]	k4	143
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
27,6	[number]	k4	27,6
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
31,5	[number]	k4	31,5
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologický	chronologický	k2eAgInSc4d1	chronologický
přehled	přehled	k1gInSc4	přehled
vývoje	vývoj	k1gInSc2	vývoj
památníku	památník	k1gInSc2	památník
==	==	k?	==
</s>
</p>
<p>
<s>
1882	[number]	k4	1882
–	–	k?	–
založen	založit	k5eAaPmNgInS	založit
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
zbudování	zbudování	k1gNnSc4	zbudování
pomníku	pomník	k1gInSc2	pomník
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
</s>
</p>
<p>
<s>
1907	[number]	k4	1907
–	–	k?	–
město	město	k1gNnSc1	město
daruje	darovat	k5eAaPmIp3nS	darovat
pozemek	pozemek	k1gInSc4	pozemek
</s>
</p>
<p>
<s>
1914	[number]	k4	1914
–	–	k?	–
první	první	k4xOgFnSc1	první
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
a	a	k8xC	a
1927	[number]	k4	1927
–	–	k?	–
další	další	k2eAgFnSc2d1	další
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
1931	[number]	k4	1931
–	–	k?	–
vybrán	vybrán	k2eAgInSc1d1	vybrán
návrh	návrh	k1gInSc1	návrh
B.	B.	kA	B.
Kafky	Kafka	k1gMnSc2	Kafka
na	na	k7c4	na
jezdeckou	jezdecký	k2eAgFnSc4d1	jezdecká
sochu	socha	k1gFnSc4	socha
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
–	–	k?	–
odhalena	odhalen	k2eAgFnSc1d1	odhalena
socha	socha	k1gFnSc1	socha
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
–	–	k?	–
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
K.	K.	kA	K.
Gottwalda	Gottwald	k1gMnSc2	Gottwald
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
odstraněny	odstraněn	k2eAgInPc4d1	odstraněn
ostatky	ostatek	k1gInPc4	ostatek
funkcionářů	funkcionář	k1gMnPc2	funkcionář
KSČ	KSČ	kA	KSČ
</s>
</p>
<p>
<s>
od	od	k7c2	od
1990	[number]	k4	1990
–	–	k?	–
hledání	hledání	k1gNnSc2	hledání
dalšího	další	k2eAgNnSc2d1	další
využití	využití	k1gNnSc2	využití
</s>
</p>
<p>
<s>
červen	červen	k1gInSc1	červen
2007	[number]	k4	2007
–	–	k?	–
ohlášen	ohlášen	k2eAgInSc4d1	ohlášen
záměr	záměr	k1gInSc4	záměr
využití	využití	k1gNnSc4	využití
památníku	památník	k1gInSc2	památník
Národním	národní	k2eAgNnSc7d1	národní
muzeem	muzeum	k1gNnSc7	muzeum
pro	pro	k7c4	pro
expozici	expozice	k1gFnSc4	expozice
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc2	otevření
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
únor	únor	k1gInSc1	únor
2012	[number]	k4	2012
–	–	k?	–
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
expozice	expozice	k1gFnSc1	expozice
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
s	s	k7c7	s
názvem	název	k1gInSc7	název
Laboratoř	laboratoř	k1gFnSc4	laboratoř
moci	moct	k5eAaImF	moct
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Národní	národní	k2eAgInSc4d1	národní
památník	památník	k1gInSc4	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
památník	památník	k1gInSc1	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
–	–	k?	–
domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
památníku	památník	k1gInSc2	památník
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
Mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
</s>
</p>
