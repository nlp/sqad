<p>
<s>
Supervulkán	Supervulkán	k2eAgMnSc1d1	Supervulkán
je	být	k5eAaImIp3nS	být
sopka	sopka	k1gFnSc1	sopka
schopná	schopný	k2eAgFnSc1d1	schopná
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
erupcí	erupce	k1gFnSc7	erupce
produkovat	produkovat	k5eAaImF	produkovat
ejekta	ejekta	k1gFnSc1	ejekta
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
1	[number]	k4	1
tisíc	tisíc	k4xCgInSc4	tisíc
kubických	kubický	k2eAgInPc2d1	kubický
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
má	mít	k5eAaImIp3nS	mít
index	index	k1gInSc1	index
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
explozivity	explozivita	k1gFnSc2	explozivita
VEI	VEI	kA	VEI
roven	roven	k2eAgInSc1d1	roven
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
tisíckrát	tisíckrát	k6eAd1	tisíckrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
historických	historický	k2eAgFnPc2d1	historická
erupcí	erupce	k1gFnPc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Supervulkán	Supervulkán	k2eAgMnSc1d1	Supervulkán
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
magma	magma	k1gNnSc1	magma
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
z	z	k7c2	z
horkých	horký	k2eAgFnPc2d1	horká
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
proniknout	proniknout	k5eAaPmF	proniknout
skrze	skrze	k?	skrze
zemskou	zemský	k2eAgFnSc4d1	zemská
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
a	a	k8xC	a
magma	magma	k1gNnSc1	magma
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
schopná	schopný	k2eAgFnSc1d1	schopná
udržet	udržet	k5eAaPmF	udržet
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Supervulkán	Supervulkán	k2eAgMnSc1d1	Supervulkán
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
konvergentní	konvergentní	k2eAgFnSc6d1	konvergentní
zóně	zóna	k1gFnSc6	zóna
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jezero	jezero	k1gNnSc1	jezero
Toba	Tob	k1gInSc2	Tob
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
kontinentálních	kontinentální	k2eAgFnPc6d1	kontinentální
horkých	horký	k2eAgFnPc6d1	horká
skvrnách	skvrna	k1gFnPc6	skvrna
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známé	známý	k2eAgFnPc1d1	známá
supervulkány	supervulkán	k2eAgFnPc1d1	supervulkán
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
erupce	erupce	k1gFnPc1	erupce
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
doložených	doložený	k2eAgInPc2d1	doložený
supervulkánů	supervulkán	k1gInPc2	supervulkán
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
dřívější	dřívější	k2eAgFnPc1d1	dřívější
erupce	erupce	k1gFnPc1	erupce
způsobily	způsobit	k5eAaPmAgFnP	způsobit
katastrofy	katastrofa	k1gFnPc4	katastrofa
globálních	globální	k2eAgMnPc2d1	globální
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
ovlivněno	ovlivněn	k2eAgNnSc4d1	ovlivněno
jen	jen	k6eAd1	jen
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
katastrofy	katastrofa	k1gFnPc4	katastrofa
nemusejí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
zničující	zničující	k2eAgMnSc1d1	zničující
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
exploze	exploze	k1gFnPc1	exploze
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
častější	častý	k2eAgNnSc1d2	častější
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Exploze	exploze	k1gFnPc1	exploze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
uvolní	uvolnit	k5eAaPmIp3nP	uvolnit
přes	přes	k7c4	přes
1000	[number]	k4	1000
Gt	Gt	k1gFnPc2	Gt
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
opakovat	opakovat	k5eAaImF	opakovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
17	[number]	k4	17
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
evidovaných	evidovaný	k2eAgMnPc2d1	evidovaný
supervulkánů	supervulkán	k1gMnPc2	supervulkán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
erupcí	erupce	k1gFnPc2	erupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Explozivní	explozivní	k2eAgFnSc1d1	explozivní
síla	síla	k1gFnSc1	síla
VEI	VEI	kA	VEI
8	[number]	k4	8
(	(	kIx(	(
<g/>
>	>	kIx)	>
1000	[number]	k4	1000
km3	km3	k4	km3
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Caldera	Calder	k1gMnSc2	Calder
Garita	Garit	k1gMnSc2	Garit
<g/>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Fish	Fish	k1gInSc1	Fish
Canyon	Canyon	k1gMnSc1	Canyon
Tuff	Tuff	k1gMnSc1	Tuff
<g/>
,	,	kIx,	,
před	před	k7c7	před
~	~	kIx~	~
27,8	[number]	k4	27,8
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
5000	[number]	k4	5000
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
dosud	dosud	k6eAd1	dosud
doložené	doložený	k2eAgFnSc2d1	doložená
erupce	erupce	k1gFnSc2	erupce
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
erupce	erupce	k1gFnSc1	erupce
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
největším	veliký	k2eAgNnSc7d3	veliký
hromadným	hromadný	k2eAgNnSc7d1	hromadné
vymíráním	vymírání	k1gNnSc7	vymírání
v	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
ochlazování	ochlazování	k1gNnSc4	ochlazování
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
erupce	erupce	k1gFnSc2	erupce
absolutně	absolutně	k6eAd1	absolutně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
podobnou	podobný	k2eAgFnSc4d1	podobná
katastrofu	katastrofa	k1gFnSc4	katastrofa
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
erupce	erupce	k1gFnSc2	erupce
byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
silnější	silný	k2eAgFnSc2d2	silnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
výbuch	výbuch	k1gInSc1	výbuch
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
bomby	bomba	k1gFnSc2	bomba
(	(	kIx(	(
<g/>
Car	car	k1gMnSc1	car
57	[number]	k4	57
Mt	Mt	k1gFnSc2	Mt
TNT	TNT	kA	TNT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
katastrofální	katastrofální	k2eAgInSc1d1	katastrofální
výbuch	výbuch	k1gInSc1	výbuch
sopky	sopka	k1gFnSc2	sopka
St.	st.	kA	st.
Helens	Helens	k1gInSc1	Helens
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vyvrhl	vyvrhnout	k5eAaPmAgInS	vyvrhnout
1	[number]	k4	1
km3	km3	k4	km3
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
přibližně	přibližně	k6eAd1	přibližně
5000	[number]	k4	5000
<g/>
×	×	k?	×
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
pohromou	pohroma	k1gFnSc7	pohroma
pro	pro	k7c4	pro
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
svět	svět	k1gInSc4	svět
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
na	na	k7c4	na
statisíce	statisíce	k1gInPc4	statisíce
let	léto	k1gNnPc2	léto
výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
hromadné	hromadný	k2eAgNnSc4d1	hromadné
vymírání	vymírání	k1gNnSc4	vymírání
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flory	flora	k1gFnSc2	flora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lake	Lake	k1gFnSc1	Lake
Toba	Toba	k1gFnSc1	Toba
,	,	kIx,	,
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
před	před	k7c7	před
~	~	kIx~	~
74	[number]	k4	74
000	[number]	k4	000
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
2800	[number]	k4	2800
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Erupce	erupce	k1gFnPc1	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
Toba	Tobum	k1gNnSc2	Tobum
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
erupcí	erupce	k1gFnSc7	erupce
za	za	k7c4	za
posledních	poslední	k2eAgInPc2d1	poslední
27	[number]	k4	27
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
dramatičtější	dramatický	k2eAgNnSc4d2	dramatičtější
pro	pro	k7c4	pro
život	život	k1gInSc4	život
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnSc2d1	ledová
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgNnP	způsobit
další	další	k2eAgNnPc1d1	další
zhoršení	zhoršení	k1gNnPc1	zhoršení
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
až	až	k9	až
o	o	k7c4	o
5	[number]	k4	5
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
začaly	začít	k5eAaPmAgInP	začít
růst	růst	k1gInSc4	růst
polární	polární	k2eAgInPc1d1	polární
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
značné	značný	k2eAgNnSc1d1	značné
ochlazení	ochlazení	k1gNnSc1	ochlazení
zejména	zejména	k9	zejména
severních	severní	k2eAgFnPc2d1	severní
šířek	šířka	k1gFnPc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Rose	Rose	k1gMnSc1	Rose
a	a	k8xC	a
Craig	Craig	k1gMnSc1	Craig
Chesner	Chesner	k1gMnSc1	Chesner
z	z	k7c2	z
Michiganské	michiganský	k2eAgFnSc2d1	Michiganská
technologické	technologický	k2eAgFnSc2d1	technologická
univerzity	univerzita	k1gFnSc2	univerzita
vyvodili	vyvodit	k5eAaPmAgMnP	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
2800	[number]	k4	2800
km3	km3	k4	km3
–	–	k?	–
zhruba	zhruba	k6eAd1	zhruba
2000	[number]	k4	2000
km3	km3	k4	km3
teklo	téct	k5eAaImAgNnS	téct
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
lávy	láva	k1gFnSc2	láva
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
kolem	kolem	k7c2	kolem
800	[number]	k4	800
km3	km3	k4	km3
spadlo	spadnout	k5eAaPmAgNnS	spadnout
jako	jako	k9	jako
popel	popel	k1gInSc1	popel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
vulkánu	vulkán	k1gInSc2	vulkán
<g/>
.	.	kIx.	.
</s>
<s>
Pyroklastické	pyroklastický	k2eAgInPc1d1	pyroklastický
proudy	proud	k1gInPc1	proud
z	z	k7c2	z
erupce	erupce	k1gFnSc2	erupce
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
od	od	k7c2	od
vulkánu	vulkán	k1gInSc2	vulkán
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
zničily	zničit	k5eAaPmAgFnP	zničit
oblast	oblast	k1gFnSc4	oblast
20	[number]	k4	20
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
třetina	třetina	k1gFnSc1	třetina
území	území	k1gNnSc2	území
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Asie	Asie	k1gFnSc1	Asie
se	se	k3xPyFc4	se
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
15	[number]	k4	15
<g/>
cm	cm	kA	cm
vrstvou	vrstva	k1gFnSc7	vrstva
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
nedozírné	dozírný	k2eNgInPc4d1	nedozírný
následky	následek	k1gInPc4	následek
pro	pro	k7c4	pro
faunu	fauna	k1gFnSc4	fauna
a	a	k8xC	a
flóru	flóra	k1gFnSc4	flóra
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
přilehlé	přilehlý	k2eAgNnSc1d1	přilehlé
k	k	k7c3	k
Tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgInPc4d1	sahající
na	na	k7c4	na
západ	západ	k1gInSc4	západ
k	k	k7c3	k
Indii	Indie	k1gFnSc3	Indie
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
Malajsii	Malajsie	k1gFnSc3	Malajsie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
cca	cca	kA	cca
1000	[number]	k4	1000
km	km	kA	km
místy	místy	k6eAd1	místy
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
popelem	popel	k1gInSc7	popel
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
m.	m.	k?	m.
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
vyvrženo	vyvrhnout	k5eAaPmNgNnS	vyvrhnout
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
tun	tuna	k1gFnPc2	tuna
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
proměnil	proměnit	k5eAaPmAgInS	proměnit
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
sírovou	sírový	k2eAgFnSc4d1	sírová
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgInPc4d1	silný
kyselé	kyselý	k2eAgInPc4d1	kyselý
deště	dešť	k1gInPc4	dešť
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
decimující	decimující	k2eAgFnSc1d1	decimující
vše	všechen	k3xTgNnSc4	všechen
živé	živý	k2eAgNnSc4d1	živé
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
trvající	trvající	k2eAgFnSc3d1	trvající
sopečné	sopečný	k2eAgFnSc3d1	sopečná
erupci	erupce	k1gFnSc3	erupce
nastala	nastat	k5eAaPmAgFnS	nastat
stovky	stovka	k1gFnPc1	stovka
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc1d1	trvající
sopečná	sopečný	k2eAgFnSc1d1	sopečná
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
živočišných	živočišný	k2eAgInPc2d1	živočišný
a	a	k8xC	a
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
znamenala	znamenat	k5eAaImAgFnS	znamenat
erupce	erupce	k1gFnSc1	erupce
Toby	Toba	k1gFnSc2	Toba
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vymření	vymření	k1gNnSc4	vymření
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
globální	globální	k2eAgNnSc1d1	globální
hromadné	hromadný	k2eAgNnSc1d1	hromadné
vymírání	vymírání	k1gNnSc1	vymírání
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgNnSc7d1	poslední
větším	veliký	k2eAgNnSc7d2	veliký
vymíráním	vymírání	k1gNnSc7	vymírání
zaznamenaným	zaznamenaný	k2eAgNnSc7d1	zaznamenané
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
slibně	slibně	k6eAd1	slibně
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
lidský	lidský	k2eAgInSc1d1	lidský
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
DNA	dno	k1gNnSc2	dno
studií	studie	k1gFnPc2	studie
zdecimován	zdecimován	k2eAgMnSc1d1	zdecimován
na	na	k7c4	na
posledních	poslední	k2eAgInPc2d1	poslední
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
5000	[number]	k4	5000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
toto	tento	k3xDgNnSc4	tento
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
snadno	snadno	k6eAd1	snadno
přežil	přežít	k5eAaPmAgMnS	přežít
i	i	k9	i
neandrtálec	neandrtálec	k1gMnSc1	neandrtálec
či	či	k8xC	či
Homo	Homo	k1gMnSc1	Homo
floresiensis	floresiensis	k1gFnSc2	floresiensis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pacana	Pacana	k1gFnSc1	Pacana
Caldera	Caldera	k1gFnSc1	Caldera
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc1d1	severní
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
před	před	k7c7	před
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
2500	[number]	k4	2500
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
ničivá	ničivý	k2eAgFnSc1d1	ničivá
katastrofa	katastrofa	k1gFnSc1	katastrofa
jako	jako	k8xS	jako
událost	událost	k1gFnSc1	událost
Toba	Tob	k1gInSc2	Tob
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
těžištěm	těžiště	k1gNnSc7	těžiště
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
Park	park	k1gInSc1	park
Caldera	Calder	k1gMnSc2	Calder
,	,	kIx,	,
Huckleberry	Huckleberra	k1gMnSc2	Huckleberra
Ridge	Ridge	k1gNnSc2	Ridge
Tuff	Tuff	k1gMnSc1	Tuff
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc2	Ida
<g/>
/	/	kIx~	/
<g/>
Wyoming	Wyoming	k1gInSc1	Wyoming
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
před	před	k7c7	před
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
2500	[number]	k4	2500
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
ničivá	ničivý	k2eAgFnSc1d1	ničivá
katastrofa	katastrofa	k1gFnSc1	katastrofa
jako	jako	k8xS	jako
událost	událost	k1gFnSc1	událost
Toba	Tob	k1gInSc2	Tob
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
těžištěm	těžiště	k1gNnSc7	těžiště
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
známá	známý	k2eAgFnSc1d1	známá
erupce	erupce	k1gFnSc1	erupce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc1	hotspot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Whakamaru	Whakamar	k1gInSc2	Whakamar
<g/>
,	,	kIx,	,
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
zóny	zóna	k1gFnSc2	zóna
Taupo	Taupa	k1gFnSc5	Taupa
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
,	,	kIx,	,
Whakamaru	Whakamara	k1gFnSc4	Whakamara
<g/>
,	,	kIx,	,
před	před	k7c7	před
~	~	kIx~	~
254	[number]	k4	254
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc1	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
2000	[number]	k4	2000
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
"	"	kIx"	"
<g/>
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
"	"	kIx"	"
erupce	erupce	k1gFnSc1	erupce
než	než	k8xS	než
Toba	Toba	k1gFnSc1	Toba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dramatickým	dramatický	k2eAgInSc7d1	dramatický
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	nejvíc	k6eAd1	nejvíc
postiženou	postižený	k2eAgFnSc7d1	postižená
oblastí	oblast	k1gFnSc7	oblast
byla	být	k5eAaImAgFnS	být
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Asie	Asie	k1gFnSc1	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kilgore	Kilgor	k1gMnSc5	Kilgor
Tuff	Tuff	k1gMnSc1	Tuff
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc2	Ida
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
před	před	k7c7	před
4,5	[number]	k4	4,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
1800	[number]	k4	1800
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
"	"	kIx"	"
<g/>
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
"	"	kIx"	"
erupce	erupce	k1gFnSc1	erupce
než	než	k8xS	než
Toba	Toba	k1gFnSc1	Toba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dramatickým	dramatický	k2eAgInSc7d1	dramatický
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
postiženou	postižený	k2eAgFnSc7d1	postižená
oblastí	oblast	k1gFnSc7	oblast
byla	být	k5eAaImAgFnS	být
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
známá	známý	k2eAgFnSc1d1	známá
erupce	erupce	k1gFnSc1	erupce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc1	hotspot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blacktail	Blacktail	k1gMnSc1	Blacktail
Tuff	Tuff	k1gMnSc1	Tuff
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc2	Ida
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
před	před	k7c7	před
6,6	[number]	k4	6,6
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
1500	[number]	k4	1500
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
"	"	kIx"	"
<g/>
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
"	"	kIx"	"
erupce	erupce	k1gFnSc1	erupce
než	než	k8xS	než
Toba	Toba	k1gFnSc1	Toba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
s	s	k7c7	s
dramatickým	dramatický	k2eAgInSc7d1	dramatický
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
postiženou	postižený	k2eAgFnSc7d1	postižená
oblastí	oblast	k1gFnSc7	oblast
byla	být	k5eAaImAgFnS	být
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
známá	známý	k2eAgFnSc1d1	známá
erupce	erupce	k1gFnSc1	erupce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lake	Lake	k1gFnSc5	Lake
Taupo	Taupa	k1gFnSc5	Taupa
<g/>
,	,	kIx,	,
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
zóny	zóna	k1gFnSc2	zóna
Taupo	Taupa	k1gFnSc5	Taupa
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Oruanui	Oruanui	k1gNnSc1	Oruanui
<g/>
,	,	kIx,	,
před	před	k7c7	před
26	[number]	k4	26
500	[number]	k4	500
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
1170	[number]	k4	1170
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
známá	známý	k2eAgFnSc1d1	známá
velká	velký	k2eAgFnSc1d1	velká
erupce	erupce	k1gFnSc1	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
erupce	erupce	k1gFnSc1	erupce
mohla	moct	k5eAaImAgFnS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
prodloužení	prodloužení	k1gNnSc4	prodloužení
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
o	o	k7c6	o
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vymření	vymření	k1gNnSc2	vymření
další	další	k2eAgFnSc2d1	další
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
čtvrtohorní	čtvrtohorní	k2eAgFnSc2d1	čtvrtohorní
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cerro	Cerro	k6eAd1	Cerro
Galan	Galan	k1gInSc1	Galan
<g/>
,	,	kIx,	,
provincie	provincie	k1gFnSc1	provincie
Catamarca	Catamarca	k1gFnSc1	Catamarca
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
před	před	k7c7	před
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
1050	[number]	k4	1050
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
<g/>
×	×	k?	×
"	"	kIx"	"
<g/>
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
"	"	kIx"	"
erupce	erupce	k1gFnSc1	erupce
než	než	k8xS	než
Toba	Toba	k1gFnSc1	Toba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
s	s	k7c7	s
dramatickým	dramatický	k2eAgInSc7d1	dramatický
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
těžištěm	těžiště	k1gNnSc7	těžiště
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Yellowstone	Yellowston	k1gInSc5	Yellowston
Caldera	Caldera	k1gFnSc1	Caldera
<g/>
,	,	kIx,	,
Lava	lava	k1gFnSc1	lava
Creek	Creek	k1gMnSc1	Creek
Tuff	Tuff	k1gMnSc1	Tuff
<g/>
,	,	kIx,	,
Wyoming	Wyoming	k1gInSc1	Wyoming
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
před	před	k7c7	před
640	[number]	k4	640
000	[number]	k4	000
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc3	množství
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
~	~	kIx~	~
1000	[number]	k4	1000
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
známá	známý	k2eAgFnSc1d1	známá
erupce	erupce	k1gFnSc1	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
3	[number]	k4	3
<g/>
×	×	k?	×
slabší	slabý	k2eAgMnPc1d2	slabší
než	než	k8xS	než
událost	událost	k1gFnSc4	událost
Toba	Tobum	k1gNnSc2	Tobum
a	a	k8xC	a
na	na	k7c6	na
samé	samý	k3xTgFnSc6	samý
hranici	hranice	k1gFnSc6	hranice
explozivní	explozivní	k2eAgFnSc2d1	explozivní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tento	tento	k3xDgInSc4	tento
výbuch	výbuch	k1gInSc1	výbuch
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
VEI	VEI	kA	VEI
8	[number]	k4	8
supervulkány	supervulkán	k1gInPc7	supervulkán
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
km3	km3	k4	km3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
erupce	erupce	k1gFnSc1	erupce
ničivý	ničivý	k2eAgInSc4d1	ničivý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
veškerou	veškerý	k3xTgFnSc4	veškerý
faunu	fauna	k1gFnSc4	fauna
a	a	k8xC	a
flóru	flóra	k1gFnSc4	flóra
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
silné	silný	k2eAgInPc4d1	silný
globální	globální	k2eAgInPc4d1	globální
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
globálního	globální	k2eAgNnSc2d1	globální
ochlazení	ochlazení	k1gNnSc2	ochlazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
Středosibiřská	středosibiřský	k2eAgFnSc1d1	Středosibiřská
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
před	před	k7c7	před
250	[number]	k4	250
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedávné	dávný	k2eNgInPc1d1	nedávný
výzkumy	výzkum	k1gInPc1	výzkum
poukázaly	poukázat	k5eAaPmAgInP	poukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sibiře	Sibiř	k1gFnSc2	Sibiř
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
vrstva	vrstva	k1gFnSc1	vrstva
ztuhlé	ztuhlý	k2eAgFnSc2d1	ztuhlá
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
někdy	někdy	k6eAd1	někdy
až	až	k9	až
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
mocná	mocný	k2eAgFnSc1d1	mocná
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
okolo	okolo	k7c2	okolo
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masivní	masivní	k2eAgFnSc3d1	masivní
erupci	erupce	k1gFnSc3	erupce
právě	právě	k9	právě
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Sibiře	Sibiř	k1gFnSc2	Sibiř
(	(	kIx(	(
<g/>
sibiřské	sibiřský	k2eAgInPc1d1	sibiřský
trapy	trap	k1gInPc1	trap
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
supervulkán	supervulkán	k2eAgInSc1d1	supervulkán
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
událost	událost	k1gFnSc4	událost
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
Velké	velký	k2eAgNnSc4d1	velké
permské	permský	k2eAgNnSc4d1	Permské
vymírání	vymírání	k1gNnSc4	vymírání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
hromadných	hromadný	k2eAgInPc2d1	hromadný
vymírání	vymírání	k1gNnSc1	vymírání
tím	ten	k3xDgNnSc7	ten
největším	veliký	k2eAgNnSc7d3	veliký
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
vymřelo	vymřít	k5eAaPmAgNnS	vymřít
okolo	okolo	k6eAd1	okolo
90	[number]	k4	90
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
žijících	žijící	k2eAgInPc2d1	žijící
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
<g/>
Určit	určit	k5eAaPmF	určit
přesný	přesný	k2eAgInSc4d1	přesný
odhad	odhad	k1gInSc4	odhad
síly	síla	k1gFnSc2	síla
erupce	erupce	k1gFnPc1	erupce
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
obtížné	obtížný	k2eAgNnSc1d1	obtížné
určit	určit	k5eAaPmF	určit
(	(	kIx(	(
<g/>
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
stopy	stopa	k1gFnPc1	stopa
a	a	k8xC	a
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
sobě	se	k3xPyFc3	se
tato	tento	k3xDgFnSc1	tento
erupce	erupce	k1gFnSc1	erupce
zanechala	zanechat	k5eAaPmAgFnS	zanechat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
působení	působení	k1gNnSc3	působení
eroze	eroze	k1gFnSc2	eroze
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
geologických	geologický	k2eAgInPc2d1	geologický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odhady	odhad	k1gInPc4	odhad
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
a	a	k8xC	a
zkresluje	zkreslovat	k5eAaImIp3nS	zkreslovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
mocnost	mocnost	k1gFnSc1	mocnost
lávové	lávový	k2eAgFnSc2d1	lávová
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
drastické	drastický	k2eAgInPc4d1	drastický
dopady	dopad	k1gInPc4	dopad
exploze	exploze	k1gFnSc2	exploze
na	na	k7c4	na
biosféru	biosféra	k1gFnSc4	biosféra
dávají	dávat	k5eAaImIp3nP	dávat
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
největší	veliký	k2eAgFnSc4d3	veliký
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
erupci	erupce	k1gFnSc4	erupce
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
nejnižší	nízký	k2eAgInPc1d3	nejnižší
odhady	odhad	k1gInPc1	odhad
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
horniny	hornina	k1gFnSc2	hornina
znamenají	znamenat	k5eAaImIp3nP	znamenat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
300	[number]	k4	300
<g/>
násobek	násobek	k1gInSc1	násobek
materiálu	materiál	k1gInSc2	materiál
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
supervulkánem	supervulkán	k1gInSc7	supervulkán
Toba	Tob	k1gInSc2	Tob
a	a	k8xC	a
200	[number]	k4	200
<g/>
násobek	násobek	k1gInSc1	násobek
materiálu	materiál	k1gInSc2	materiál
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
supervulkánem	supervulkán	k1gInSc7	supervulkán
La	la	k1gNnSc2	la
Caldera	Caldero	k1gNnSc2	Caldero
Garita	Garitum	k1gNnSc2	Garitum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
výlev	výlev	k1gInSc1	výlev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
trapů	trap	k1gInPc2	trap
trval	trvat	k5eAaImAgInS	trvat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	k9	kdyby
k	k	k7c3	k
tak	tak	k6eAd1	tak
mohutnému	mohutný	k2eAgInSc3d1	mohutný
výlevu	výlev	k1gInSc3	výlev
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
jistý	jistý	k2eAgInSc1d1	jistý
zánik	zánik	k1gInSc1	zánik
života	život	k1gInSc2	život
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
život	život	k1gInSc1	život
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc3	zem
neměl	mít	k5eNaImAgInS	mít
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Explozivní	explozivní	k2eAgFnSc1d1	explozivní
síla	síla	k1gFnSc1	síla
VEI	VEI	kA	VEI
7	[number]	k4	7
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
km3	km3	k4	km3
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Explozivní	explozivní	k2eAgFnSc1d1	explozivní
síla	síla	k1gFnSc1	síla
vulkánů	vulkán	k1gInPc2	vulkán
VEI	VEI	kA	VEI
7	[number]	k4	7
sice	sice	k8xC	sice
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
ničivosti	ničivost	k1gFnPc4	ničivost
vulkánů	vulkán	k1gInPc2	vulkán
VEI	VEI	kA	VEI
8	[number]	k4	8
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
mívají	mívat	k5eAaImIp3nP	mívat
vždy	vždy	k6eAd1	vždy
globální	globální	k2eAgInSc4d1	globální
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
erupce	erupce	k1gFnPc1	erupce
se	se	k3xPyFc4	se
také	také	k9	také
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
supermasivní	supermasivní	k2eAgFnPc1d1	supermasivní
<g/>
.	.	kIx.	.
</s>
<s>
Níže	níže	k1gFnSc1	níže
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
těch	ten	k3xDgMnPc2	ten
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bennett	Bennett	k2eAgInSc1d1	Bennett
Lake	Lake	k1gInSc1	Lake
<g/>
,	,	kIx,	,
sopečný	sopečný	k2eAgInSc1d1	sopečný
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
/	/	kIx~	/
<g/>
Yukon	Yukon	k1gInSc1	Yukon
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
před	před	k7c7	před
~	~	kIx~	~
50	[number]	k4	50
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
850	[number]	k4	850
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
masivní	masivní	k2eAgFnSc1d1	masivní
erupce	erupce	k1gFnSc1	erupce
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
a	a	k8xC	a
ničivým	ničivý	k2eAgInSc7d1	ničivý
dopadem	dopad	k1gInSc7	dopad
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
×	×	k?	×
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
událost	událost	k1gFnSc1	událost
Toba	Toba	k1gFnSc1	Toba
<g/>
,	,	kIx,	,
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
explozí	exploze	k1gFnSc7	exploze
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
v	v	k7c6	v
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pastos	Pastos	k1gMnSc1	Pastos
Grandes	Grandes	k1gMnSc1	Grandes
<g/>
,	,	kIx,	,
Pastos	Pastos	k1gMnSc1	Pastos
Grandes	Grandes	k1gMnSc1	Grandes
Caldera	Caldera	k1gFnSc1	Caldera
<g/>
,	,	kIx,	,
před	před	k7c7	před
2,9	[number]	k4	2,9
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
820	[number]	k4	820
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
masivní	masivní	k2eAgFnSc1d1	masivní
erupce	erupce	k1gFnSc1	erupce
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
<g/>
,	,	kIx,	,
3,5	[number]	k4	3,5
<g/>
×	×	k?	×
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
událost	událost	k1gFnSc1	událost
Toba	Toba	k1gFnSc1	Toba
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
poslední	poslední	k2eAgFnSc1d1	poslední
erupce	erupce	k1gFnSc1	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
v	v	k7c4	v
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heise	Heise	k1gFnSc1	Heise
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
Walcott	Walcott	k2eAgInSc4d1	Walcott
Tuff	Tuff	k1gInSc4	Tuff
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc4	Ida
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
před	před	k7c7	před
6,4	[number]	k4	6,4
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
750	[number]	k4	750
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masivní	masivní	k2eAgFnPc1d1	masivní
erupce	erupce	k1gFnPc1	erupce
s	s	k7c7	s
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
a	a	k8xC	a
ničivým	ničivý	k2eAgInSc7d1	ničivý
dopadem	dopad	k1gInSc7	dopad
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
poslední	poslední	k2eAgFnSc1d1	poslední
erupce	erupce	k1gFnSc1	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
v	v	k7c4	v
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Long	Long	k1gMnSc1	Long
Valley	Vallea	k1gFnSc2	Vallea
Caldera	Caldera	k1gFnSc1	Caldera
<g/>
,	,	kIx,	,
Bishop	Bishop	k1gInSc1	Bishop
Tuff	Tuff	k1gInSc1	Tuff
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
před	před	k7c7	před
~	~	kIx~	~
760	[number]	k4	760
000	[number]	k4	000
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
600	[number]	k4	600
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masivní	masivní	k2eAgFnPc1d1	masivní
erupce	erupce	k1gFnPc1	erupce
s	s	k7c7	s
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
a	a	k8xC	a
ničivým	ničivý	k2eAgInSc7d1	ničivý
dopadem	dopad	k1gInSc7	dopad
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Campi	Campi	k6eAd1	Campi
Flegrei	Flegrei	k1gNnSc1	Flegrei
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
před	před	k7c7	před
39	[number]	k4	39
280	[number]	k4	280
roky	rok	k1gInPc7	rok
(	(	kIx(	(
<g/>
500	[number]	k4	500
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
erupcí	erupce	k1gFnPc2	erupce
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
s	s	k7c7	s
globálními	globální	k2eAgInPc7d1	globální
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
silně	silně	k6eAd1	silně
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
dobou	doba	k1gFnSc7	doba
ledovou	ledový	k2eAgFnSc7d1	ledová
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
erupce	erupce	k1gFnSc1	erupce
vážnou	vážný	k2eAgFnSc4d1	vážná
pohromou	pohroma	k1gFnSc7	pohroma
a	a	k8xC	a
znamenala	znamenat	k5eAaImAgFnS	znamenat
zhoršení	zhoršení	k1gNnSc4	zhoršení
klimatu	klima	k1gNnSc2	klima
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
slabší	slabý	k2eAgFnSc4d2	slabší
explozi	exploze	k1gFnSc4	exploze
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
exploze	exploze	k1gFnSc1	exploze
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
v	v	k7c4	v
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mangakino	Mangakino	k1gNnSc1	Mangakino
<g/>
,	,	kIx,	,
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
oblast	oblast	k1gFnSc1	oblast
Taupo	Taupa	k1gFnSc5	Taupa
<g/>
,	,	kIx,	,
North	North	k1gMnSc1	North
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
erupce	erupce	k1gFnSc2	erupce
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
před	před	k7c7	před
0,97	[number]	k4	0,97
do	do	k7c2	do
1,23	[number]	k4	1,23
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
300	[number]	k4	300
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Silná	silný	k2eAgFnSc1d1	silná
erupce	erupce	k1gFnSc1	erupce
s	s	k7c7	s
dramatickým	dramatický	k2eAgInSc7d1	dramatický
dopadem	dopad	k1gInSc7	dopad
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
s	s	k7c7	s
patrným	patrný	k2eAgInSc7d1	patrný
globálním	globální	k2eAgInSc7d1	globální
dopadem	dopad	k1gInSc7	dopad
(	(	kIx(	(
<g/>
ochlazení	ochlazení	k1gNnSc1	ochlazení
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
o	o	k7c4	o
několik	několik	k4yIc4	několik
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bruneau-Jarbidge	Bruneau-Jarbidge	k6eAd1	Bruneau-Jarbidge
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc4	Ida
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
před	před	k7c7	před
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
250	[number]	k4	250
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Silná	silný	k2eAgFnSc1d1	silná
erupce	erupce	k1gFnSc1	erupce
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
poslední	poslední	k2eAgFnSc1d1	poslední
exploze	exploze	k1gFnSc1	exploze
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
v	v	k7c4	v
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
<s>
Poškozena	poškozen	k2eAgFnSc1d1	poškozena
část	část	k1gFnSc1	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
mírný	mírný	k2eAgInSc4d1	mírný
globální	globální	k2eAgInSc4d1	globální
dopad	dopad	k1gInSc4	dopad
(	(	kIx(	(
<g/>
ochlazení	ochlazení	k1gNnSc1	ochlazení
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
o	o	k7c4	o
několik	několik	k4yIc4	několik
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tambora	tambor	k1gMnSc2	tambor
<g/>
,	,	kIx,	,
Sumbawa	Sumbawus	k1gMnSc2	Sumbawus
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
West	West	k1gInSc4	West
Nusa	Nus	k2eAgFnSc1d1	Nusa
Tenggara	Tenggara	k1gFnSc1	Tenggara
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
rok	rok	k1gInSc1	rok
1815	[number]	k4	1815
(	(	kIx(	(
<g/>
160	[number]	k4	160
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
erupce	erupce	k1gFnSc1	erupce
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
lidskou	lidský	k2eAgFnSc7d1	lidská
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
×	×	k?	×
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
exploze	exploze	k1gFnSc1	exploze
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
v	v	k7c4	v
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
,	,	kIx,	,
a	a	k8xC	a
18	[number]	k4	18
<g/>
×	×	k?	×
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
událost	událost	k1gFnSc1	událost
Toba	Toba	k1gFnSc1	Toba
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
globální	globální	k2eAgInSc4d1	globální
dopad	dopad	k1gInSc4	dopad
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
mírný	mírný	k2eAgInSc1d1	mírný
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
se	se	k3xPyFc4	se
ochladila	ochladit	k5eAaPmAgFnS	ochladit
o	o	k7c4	o
cca	cca	kA	cca
1,5	[number]	k4	1,5
stupně	stupeň	k1gInSc2	stupeň
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
o	o	k7c6	o
roku	rok	k1gInSc2	rok
bez	bez	k7c2	bez
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kikai	Kikai	k6eAd1	Kikai
Caldera	Caldera	k1gFnSc1	Caldera
<g/>
,	,	kIx,	,
Rjúkjú	Rjúkjú	k1gNnSc1	Rjúkjú
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
před	před	k7c7	před
6300	[number]	k4	6300
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
~	~	kIx~	~
4300	[number]	k4	4300
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
150	[number]	k4	150
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
silná	silný	k2eAgFnSc1d1	silná
exploze	exploze	k1gFnSc1	exploze
jako	jako	k9	jako
Tambora	tambor	k1gMnSc2	tambor
<g/>
.	.	kIx.	.
</s>
<s>
Ničivý	ničivý	k2eAgInSc4d1	ničivý
lokální	lokální	k2eAgInSc4d1	lokální
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
globální	globální	k2eAgInSc4d1	globální
dopad	dopad	k1gInSc4	dopad
mírný	mírný	k2eAgInSc4d1	mírný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Macauley	Macaulea	k1gFnPc1	Macaulea
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Kermadec	Kermadec	k1gMnSc1	Kermadec
Islands	Islands	k1gInSc1	Islands
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
před	před	k7c7	před
6300	[number]	k4	6300
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
~	~	kIx~	~
4300	[number]	k4	4300
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
100	[number]	k4	100
km3	km3	k4	km3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Exploze	exploze	k1gFnSc1	exploze
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
explozivní	explozivní	k2eAgFnSc2d1	explozivní
síly	síla	k1gFnSc2	síla
VEI	VEI	kA	VEI
7	[number]	k4	7
–	–	k?	–
silné	silný	k2eAgInPc4d1	silný
lokální	lokální	k2eAgInPc4d1	lokální
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
mírný	mírný	k2eAgInSc4d1	mírný
globální	globální	k2eAgInSc4d1	globální
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
exploze	exploze	k1gFnSc1	exploze
nastala	nastat	k5eAaPmAgFnS	nastat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xS	jako
Kikai	Kikai	k1gNnSc6	Kikai
Caldera	Caldero	k1gNnSc2	Caldero
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc4	součet
explozí	exploze	k1gFnPc2	exploze
těchto	tento	k3xDgInPc2	tento
vulkánů	vulkán	k1gInPc2	vulkán
mohl	moct	k5eAaImAgMnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
silnější	silný	k2eAgInSc4d2	silnější
globální	globální	k2eAgInSc4d1	globální
dopad	dopad	k1gInSc4	dopad
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
patrné	patrný	k2eAgInPc1d1	patrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současné	současný	k2eAgNnSc1d1	současné
ohrožení	ohrožení	k1gNnSc1	ohrožení
supervulkány	supervulkán	k2eAgFnPc1d1	supervulkán
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
seznamu	seznam	k1gInSc2	seznam
historických	historický	k2eAgFnPc2d1	historická
erupcí	erupce	k1gFnPc2	erupce
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovéto	takovýto	k3xDgFnPc1	takovýto
události	událost	k1gFnPc1	událost
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
ničím	ničit	k5eAaImIp1nS	ničit
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
lidstva	lidstvo	k1gNnSc2	lidstvo
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
síle	síla	k1gFnSc3	síla
supervulkánů	supervulkán	k1gInPc2	supervulkán
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nemožný	možný	k2eNgInSc1d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Erupci	erupce	k1gFnSc4	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
nelze	lze	k6eNd1	lze
ani	ani	k8xC	ani
zastavit	zastavit	k5eAaPmF	zastavit
či	či	k8xC	či
zmírnit	zmírnit	k5eAaPmF	zmírnit
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
nelze	lze	k6eNd1	lze
ani	ani	k8xC	ani
schovat	schovat	k5eAaPmF	schovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dopady	dopad	k1gInPc4	dopad
erupce	erupce	k1gFnSc2	erupce
jsou	být	k5eAaImIp3nP	být
globální	globální	k2eAgFnPc1d1	globální
a	a	k8xC	a
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
varují	varovat	k5eAaImIp3nP	varovat
před	před	k7c7	před
možnou	možný	k2eAgFnSc7d1	možná
erupcí	erupce	k1gFnSc7	erupce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Yellowstone	Yellowston	k1gInSc5	Yellowston
hotspot	hotspot	k1gInSc4	hotspot
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
hromadění	hromadění	k1gNnSc4	hromadění
magmatu	magma	k1gNnSc2	magma
a	a	k8xC	a
mírné	mírný	k2eAgNnSc4d1	mírné
nadzvedávání	nadzvedávání	k1gNnSc4	nadzvedávání
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
erupci	erupce	k1gFnSc3	erupce
tohoto	tento	k3xDgInSc2	tento
vulkánu	vulkán	k1gInSc2	vulkán
dochází	docházet	k5eAaImIp3nS	docházet
přibližně	přibližně	k6eAd1	přibližně
každých	každý	k3xTgFnPc2	každý
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgFnSc1d1	poslední
erupce	erupce	k1gFnSc1	erupce
nastala	nastat	k5eAaPmAgFnS	nastat
před	před	k7c7	před
640	[number]	k4	640
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
stejně	stejně	k6eAd1	stejně
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
ta	ten	k3xDgFnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
civilizaci	civilizace	k1gFnSc4	civilizace
absolutní	absolutní	k2eAgFnSc4d1	absolutní
pohromu	pohroma	k1gFnSc4	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c4	po
erupci	erupce	k1gFnSc4	erupce
by	by	kYmCp3nP	by
zahynuly	zahynout	k5eAaPmAgFnP	zahynout
statisíce	statisíce	k1gInPc4	statisíce
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
lávových	lávový	k2eAgInPc6d1	lávový
<g/>
,	,	kIx,	,
laharových	laharův	k2eAgInPc6d1	laharův
a	a	k8xC	a
pyroklastických	pyroklastický	k2eAgInPc6d1	pyroklastický
proudech	proud	k1gInPc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řádu	řád	k1gInSc6	řád
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masivnímu	masivní	k2eAgInSc3d1	masivní
spadu	spad	k1gInSc3	spad
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
500	[number]	k4	500
km	km	kA	km
až	až	k9	až
1	[number]	k4	1
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
1000	[number]	k4	1000
km	km	kA	km
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pomalému	pomalý	k2eAgNnSc3d1	pomalé
udušení	udušení	k1gNnSc3	udušení
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zničení	zničení	k1gNnSc3	zničení
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
ke	k	k7c3	k
zřícení	zřícení	k1gNnSc3	zřícení
střech	střecha	k1gFnPc2	střecha
většiny	většina	k1gFnSc2	většina
zasažených	zasažený	k2eAgFnPc2d1	zasažená
budov	budova	k1gFnPc2	budova
pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
popela	popel	k1gInSc2	popel
<g/>
.	.	kIx.	.
</s>
<s>
Vyvržený	vyvržený	k2eAgInSc1d1	vyvržený
materiál	materiál	k1gInSc1	materiál
by	by	kYmCp3nS	by
během	během	k7c2	během
týdnů	týden	k1gInPc2	týden
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
atmosféru	atmosféra	k1gFnSc4	atmosféra
kolem	kolem	k7c2	kolem
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
významně	významně	k6eAd1	významně
snížil	snížit	k5eAaPmAgInS	snížit
intenzitu	intenzita	k1gFnSc4	intenzita
dopadajícího	dopadající	k2eAgInSc2d1	dopadající
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
ochlazení	ochlazení	k1gNnSc3	ochlazení
a	a	k8xC	a
stmívání	stmívání	k1gNnSc3	stmívání
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
by	by	kYmCp3nS	by
vlivem	vlivem	k7c2	vlivem
všudypřítomného	všudypřítomný	k2eAgInSc2d1	všudypřítomný
popílku	popílek	k1gInSc2	popílek
prakticky	prakticky	k6eAd1	prakticky
přestala	přestat	k5eAaPmAgFnS	přestat
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
výnosy	výnos	k1gInPc1	výnos
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
několikanásobně	několikanásobně	k6eAd1	několikanásobně
snížily	snížit	k5eAaPmAgFnP	snížit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
decimovaly	decimovat	k5eAaBmAgFnP	decimovat
globální	globální	k2eAgInPc4d1	globální
kyselé	kyselý	k2eAgInPc4d1	kyselý
deště	dešť	k1gInPc4	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
by	by	kYmCp3nP	by
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
globální	globální	k2eAgInPc4d1	globální
hladomory	hladomor	k1gInPc4	hladomor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
přelidněných	přelidněný	k2eAgInPc6d1	přelidněný
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
rovnováha	rovnováha	k1gFnSc1	rovnováha
mezi	mezi	k7c7	mezi
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
produkcí	produkce	k1gFnSc7	produkce
a	a	k8xC	a
poptávkou	poptávka	k1gFnSc7	poptávka
velmi	velmi	k6eAd1	velmi
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
<g/>
.	.	kIx.	.
</s>
<s>
Případný	případný	k2eAgInSc1d1	případný
výbuch	výbuch	k1gInSc1	výbuch
vulkánu	vulkán	k1gInSc2	vulkán
Toba	Tob	k1gInSc2	Tob
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
vymření	vymření	k1gNnSc3	vymření
lidského	lidský	k2eAgInSc2d1	lidský
druhu	druh	k1gInSc2	druh
či	či	k8xC	či
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Supervolcano	Supervolcana	k1gFnSc5	Supervolcana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Mason	mason	k1gMnSc1	mason
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Pyle	pyl	k1gInSc5	pyl
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
M.	M.	kA	M.
<g/>
;	;	kIx,	;
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
<g/>
,	,	kIx,	,
Clive	Cliev	k1gFnPc1	Cliev
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
size	size	k1gInSc1	size
and	and	k?	and
frequency	frequenca	k1gFnSc2	frequenca
of	of	k?	of
the	the	k?	the
largest	largest	k1gInSc1	largest
explosive	explosivat	k5eAaPmIp3nS	explosivat
eruptions	eruptions	k6eAd1	eruptions
on	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
<g/>
.	.	kIx.	.
</s>
<s>
Bulletin	bulletin	k1gInSc1	bulletin
of	of	k?	of
Volcanology	Volcanolog	k1gMnPc7	Volcanolog
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
66	[number]	k4	66
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
735	[number]	k4	735
<g/>
–	–	k?	–
<g/>
748	[number]	k4	748
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.100	[number]	k4	10.100
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
445	[number]	k4	445
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
355	[number]	k4	355
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TIMMRECK	TIMMRECK	kA	TIMMRECK
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
,	,	kIx,	,
Graf	graf	k1gInSc1	graf
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
-	-	kIx~	-
<g/>
F.	F.	kA	F.
The	The	k1gMnSc1	The
initial	initial	k1gMnSc1	initial
dispersal	dispersat	k5eAaImAgMnS	dispersat
and	and	k?	and
radiative	radiativ	k1gInSc5	radiativ
forcing	forcing	k1gInSc4	forcing
of	of	k?	of
a	a	k8xC	a
Northern	Northern	k1gMnSc1	Northern
Hemisphere	Hemispher	k1gInSc5	Hemispher
mid-latitude	midatitud	k1gInSc5	mid-latitud
super	super	k2eAgNnSc1d1	super
volcano	volcana	k1gFnSc5	volcana
<g/>
:	:	kIx,	:
a	a	k8xC	a
model	model	k1gInSc4	model
study	stud	k1gInPc1	stud
<g/>
.	.	kIx.	.
</s>
<s>
Atmospheric	Atmospheric	k1gMnSc1	Atmospheric
Chemistry	Chemistr	k1gMnPc4	Chemistr
and	and	k?	and
Physics	Physics	k1gInSc1	Physics
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.519	[number]	k4	10.519
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
acp-	acp-	k?	acp-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
zima	zima	k1gFnSc1	zima
</s>
</p>
<p>
<s>
Tobská	Tobský	k2eAgFnSc1d1	Tobský
katastrofa	katastrofa	k1gFnSc1	katastrofa
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Supervulkán	Supervulkán	k2eAgMnSc1d1	Supervulkán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
USGS	USGS	kA	USGS
Fact	Fact	k2eAgInSc1d1	Fact
Sheet	Sheet	k1gInSc1	Sheet
-	-	kIx~	-
Steam	Steam	k1gInSc1	Steam
Explosions	Explosions	k1gInSc1	Explosions
<g/>
,	,	kIx,	,
Earthquakes	Earthquakes	k1gInSc1	Earthquakes
<g/>
,	,	kIx,	,
and	and	k?	and
Volcanic	Volcanice	k1gFnPc2	Volcanice
Eruptions	Eruptions	k1gInSc1	Eruptions
-	-	kIx~	-
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
in	in	k?	in
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Future	Futur	k1gInSc5	Futur
<g/>
?	?	kIx.	?
</s>
</p>
