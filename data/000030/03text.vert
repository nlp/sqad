<s>
Whitney	Whitney	k1gInPc1	Whitney
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Houston	Houston	k1gInSc1	Houston
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
Newark	Newark	k1gInSc1	Newark
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
Beverly	Beverla	k1gMnSc2	Beverla
Hills	Hills	k1gInSc4	Hills
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
popová	popový	k2eAgFnSc1d1	popová
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
a	a	k8xC	a
gospelová	gospelový	k2eAgFnSc1d1	gospelová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
producentka	producentka	k1gFnSc1	producentka
<g/>
,	,	kIx,	,
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
textařka	textařka	k1gFnSc1	textařka
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
modelka	modelka	k1gFnSc1	modelka
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
a	a	k8xC	a
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
světových	světový	k2eAgFnPc2d1	světová
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ji	on	k3xPp3gFnSc4	on
Guinnessova	Guinnessův	k2eAgFnSc1d1	Guinnessova
kniha	kniha	k1gFnSc1	kniha
rekordů	rekord	k1gInPc2	rekord
uvedla	uvést	k5eAaPmAgFnS	uvést
jako	jako	k8xC	jako
nejoceňovanější	oceňovaný	k2eAgFnSc4d3	nejoceňovanější
umělkyni	umělkyně	k1gFnSc4	umělkyně
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
získala	získat	k5eAaPmAgFnS	získat
celkem	celkem	k6eAd1	celkem
415	[number]	k4	415
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
dvě	dva	k4xCgFnPc1	dva
ceny	cena	k1gFnPc1	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
třicet	třicet	k4xCc4	třicet
hudebních	hudební	k2eAgNnPc2d1	hudební
ocenění	ocenění	k1gNnPc2	ocenění
Billboard	billboard	k1gInSc1	billboard
a	a	k8xC	a
dvacet	dvacet	k4xCc1	dvacet
dva	dva	k4xCgInPc4	dva
Amerických	americký	k2eAgFnPc2d1	americká
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Řadila	řadit	k5eAaImAgFnS	řadit
se	se	k3xPyFc4	se
ke	k	k7c3	k
světově	světově	k6eAd1	světově
nejlépe	dobře	k6eAd3	dobře
prodávaným	prodávaný	k2eAgMnPc3d1	prodávaný
umělcům	umělec	k1gMnPc3	umělec
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
170	[number]	k4	170
miliony	milion	k4xCgInPc4	milion
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
debutovala	debutovat	k5eAaBmAgFnS	debutovat
eponymním	eponymní	k2eAgNnSc7d1	eponymní
albem	album	k1gNnSc7	album
Whitney	Whitnea	k1gFnSc2	Whitnea
Houston	Houston	k1gInSc1	Houston
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
interpretku	interpretka	k1gFnSc4	interpretka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
otevřela	otevřít	k5eAaPmAgFnS	otevřít
dveře	dveře	k1gFnPc4	dveře
do	do	k7c2	do
světa	svět	k1gInSc2	svět
hudby	hudba	k1gFnSc2	hudba
dalším	další	k2eAgMnPc3d1	další
afroamerickým	afroamerický	k2eAgMnPc3d1	afroamerický
umělcům	umělec	k1gMnPc3	umělec
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
také	také	k9	také
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
mnoho	mnoho	k4c4	mnoho
svých	svůj	k3xOyFgFnPc2	svůj
následovnic	následovnice	k1gFnPc2	následovnice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
charismatickému	charismatický	k2eAgNnSc3d1	charismatické
vystupování	vystupování	k1gNnSc3	vystupování
<g/>
,	,	kIx,	,
hlasovým	hlasový	k2eAgFnPc3d1	hlasová
dispozicím	dispozice	k1gFnPc3	dispozice
a	a	k8xC	a
úspěchům	úspěch	k1gInPc3	úspěch
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
stala	stát	k5eAaPmAgFnS	stát
legendou	legenda	k1gFnSc7	legenda
a	a	k8xC	a
pěveckou	pěvecký	k2eAgFnSc7d1	pěvecká
veličinou	veličina	k1gFnSc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Beverly	Beverla	k1gFnSc2	Beverla
Hilton	Hilton	k1gInSc1	Hilton
v	v	k7c4	v
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k8xS	jako
malá	malý	k2eAgFnSc1d1	malá
zpívala	zpívat	k5eAaImAgFnS	zpívat
v	v	k7c6	v
gospelovém	gospelový	k2eAgInSc6d1	gospelový
sboru	sbor	k1gInSc6	sbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vedla	vést	k5eAaImAgFnS	vést
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Cissy	Cissa	k1gFnSc2	Cissa
Houston	Houston	k1gInSc1	Houston
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mladistvá	mladistvý	k2eAgFnSc1d1	mladistvá
zpívala	zpívat	k5eAaImAgFnS	zpívat
po	po	k7c6	po
nocích	noc	k1gFnPc6	noc
v	v	k7c6	v
barech	bar	k1gInPc6	bar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc2	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
Clive	Cliev	k1gFnPc4	Cliev
Davis	Davis	k1gFnSc2	Davis
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
pozdější	pozdní	k2eAgMnSc1d2	pozdější
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
doprovodných	doprovodný	k2eAgInPc6d1	doprovodný
vokálech	vokál	k1gInPc6	vokál
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
I	i	k9	i
Am	Am	k1gFnSc4	Am
Every	Evera	k1gFnSc2	Evera
Woman	Woman	k1gInSc1	Woman
<g/>
"	"	kIx"	"
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Chaka	Chaka	k1gMnSc1	Chaka
Khan	Khan	k1gMnSc1	Khan
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
udělala	udělat	k5eAaPmAgFnS	udělat
hit	hit	k1gInSc4	hit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
soundtrackovém	soundtrackový	k2eAgNnSc6d1	soundtrackový
albu	album	k1gNnSc6	album
k	k	k7c3	k
filmu	film	k1gInSc3	film
The	The	k1gMnSc1	The
Bodyguard	bodyguard	k1gMnSc1	bodyguard
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
slibnou	slibný	k2eAgFnSc4d1	slibná
kariéru	kariéra	k1gFnSc4	kariéra
modelky	modelka	k1gFnSc2	modelka
a	a	k8xC	a
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
černošek	černoška	k1gFnPc2	černoška
se	se	k3xPyFc4	se
také	také	k9	také
objevovala	objevovat	k5eAaImAgFnS	objevovat
v	v	k7c6	v
modním	modní	k2eAgInSc6d1	modní
magazínu	magazín	k1gInSc6	magazín
Seventeen	Seventeen	k1gInSc1	Seventeen
<g/>
.	.	kIx.	.
</s>
<s>
Debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
albem	album	k1gNnSc7	album
Whitney	Whitnea	k1gFnSc2	Whitnea
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
a	a	k8xC	a
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
albem	album	k1gNnSc7	album
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
ho	on	k3xPp3gMnSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
několik	několik	k4yIc4	několik
hitů	hit	k1gInPc2	hit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
How	How	k1gMnPc1	How
Will	Will	k1gMnSc1	Will
I	i	k8xC	i
Know	Know	k1gMnSc1	Know
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Give	Giv	k1gFnSc2	Giv
Good	Good	k1gMnSc1	Good
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Greatest	Greatest	k1gMnSc1	Greatest
Love	lov	k1gInSc5	lov
Of	Of	k1gFnSc3	Of
All	All	k1gFnPc3	All
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Saving	Saving	k1gInSc1	Saving
All	All	k1gFnSc2	All
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
For	forum	k1gNnPc2	forum
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
vynesla	vynést	k5eAaPmAgFnS	vynést
její	její	k3xOp3gFnSc4	její
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
stejný	stejný	k2eAgInSc4d1	stejný
úspěch	úspěch	k1gInSc4	úspěch
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
druhé	druhý	k4xOgNnSc1	druhý
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
jednoduše	jednoduše	k6eAd1	jednoduše
Whitney	Whitne	k2eAgFnPc1d1	Whitne
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnSc7	první
číslem	číslo	k1gNnSc7	číslo
1	[number]	k4	1
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
byly	být	k5eAaImAgInP	být
takové	takový	k3xDgInPc1	takový
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
So	So	kA	So
Emotional	Emotional	k1gFnSc2	Emotional
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Didn	Didn	k1gInSc1	Didn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
We	We	k1gFnSc1	We
Almost	Almost	k1gFnSc1	Almost
Have	Have	k1gFnSc1	Have
It	It	k1gFnSc1	It
All	All	k1gFnSc1	All
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Where	Wher	k1gInSc5	Wher
Do	do	k7c2	do
Broken	Brokna	k1gFnPc2	Brokna
Hearts	Heartsa	k1gFnPc2	Heartsa
Go	Go	k1gFnSc2	Go
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
I	i	k8xC	i
Wanna	Wanna	k1gFnSc1	Wanna
Dance	Danka	k1gFnSc3	Danka
With	With	k1gInSc4	With
Somebody	Someboda	k1gFnSc2	Someboda
(	(	kIx(	(
<g/>
Who	Who	k1gMnSc1	Who
Loves	Loves	k1gMnSc1	Loves
Me	Me	k1gMnSc1	Me
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vynesl	vynést	k5eAaPmAgInS	vynést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
druhou	druhý	k4xOgFnSc4	druhý
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
hymnu	hymna	k1gFnSc4	hymna
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1988	[number]	k4	1988
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
Moment	moment	k1gInSc1	moment
In	In	k1gMnSc1	In
Time	Time	k1gInSc1	Time
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
se	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
řadovým	řadový	k2eAgInSc7d1	řadový
albem	album	k1gNnSc7	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Your	Your	k1gInSc1	Your
Baby	baby	k1gNnSc1	baby
Tonight	Tonight	k1gInSc1	Tonight
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
měřítku	měřítko	k1gNnSc6	měřítko
podobu	podoba	k1gFnSc4	podoba
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
takové	takový	k3xDgFnPc1	takový
hvězdy	hvězda	k1gFnPc1	hvězda
jako	jako	k8xC	jako
Babyface	Babyface	k1gFnPc1	Babyface
<g/>
,	,	kIx,	,
Stevie	Stevie	k1gFnPc1	Stevie
Wonder	Wondero	k1gNnPc2	Wondero
nebo	nebo	k8xC	nebo
Luther	Luthra	k1gFnPc2	Luthra
Vandross	Vandrossa	k1gFnPc2	Vandrossa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
hity	hit	k1gInPc1	hit
bodovaly	bodovat	k5eAaImAgInP	bodovat
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
[[	[[	k?	[[
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Your	Your	k1gInSc1	Your
Baby	baby	k1gNnSc1	baby
Tonight	Tonighta	k1gFnPc2	Tonighta
a	a	k8xC	a
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
That	That	k1gMnSc1	That
I	i	k8xC	i
Need	Need	k1gMnSc1	Need
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
zpívala	zpívat	k5eAaImAgFnS	zpívat
na	na	k7c6	na
největší	veliký	k2eAgFnSc6d3	veliký
americké	americký	k2eAgFnSc6d1	americká
sportovní	sportovní	k2eAgFnSc6d1	sportovní
události	událost	k1gFnSc6	událost
Super	super	k2eAgInSc2d1	super
Bowlu	Bowl	k1gInSc2	Bowl
státní	státní	k2eAgFnSc4d1	státní
hymnu	hymna	k1gFnSc4	hymna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
dostala	dostat	k5eAaPmAgFnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Bodyguard	bodyguard	k1gMnSc1	bodyguard
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
trhákem	trhák	k1gInSc7	trhák
a	a	k8xC	a
ji	on	k3xPp3gFnSc4	on
vynesl	vynést	k5eAaPmAgInS	vynést
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Kevina	Kevin	k1gMnSc2	Kevin
Costnera	Costner	k1gMnSc2	Costner
do	do	k7c2	do
světa	svět	k1gInSc2	svět
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
šest	šest	k4xCc4	šest
zpěvaččiných	zpěvaččin	k2eAgInPc2d1	zpěvaččin
hitů	hit	k1gInPc2	hit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Every	Evera	k1gMnSc2	Evera
Woman	Woman	k1gMnSc1	Woman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k9	i
Have	Have	k1gInSc1	Have
Nothing	Nothing	k1gInSc1	Nothing
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Run	run	k1gInSc4	run
To	to	k9	to
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Queen	Queen	k2eAgMnSc1d1	Queen
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jesus	Jesus	k1gMnSc1	Jesus
Loves	Loves	k1gMnSc1	Loves
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
píseň	píseň	k1gFnSc1	píseň
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
"	"	kIx"	"
<g/>
I	i	k9	i
Will	Will	k1gInSc1	Will
Always	Alwaysa	k1gFnPc2	Alwaysa
Love	lov	k1gInSc5	lov
You	You	k1gFnSc3	You
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
soundtrackem	soundtrack	k1gInSc7	soundtrack
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
-	-	kIx~	-
44	[number]	k4	44
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
něj	on	k3xPp3gMnSc2	on
obdržela	obdržet	k5eAaPmAgFnS	obdržet
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc1	tři
Ceny	cena	k1gFnPc1	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
turné	turné	k1gNnSc1	turné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
úspěchu	úspěch	k1gInSc3	úspěch
muselo	muset	k5eAaImAgNnS	muset
několikrát	několikrát	k6eAd1	několikrát
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
soundtrack	soundtrack	k1gInSc1	soundtrack
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
k	k	k7c3	k
filmu	film	k1gInSc3	film
Waiting	Waiting	k1gInSc1	Waiting
to	ten	k3xDgNnSc1	ten
Exhale	Exhala	k1gFnSc6	Exhala
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Angely	Angela	k1gFnSc2	Angela
Bassetové	Bassetový	k2eAgFnSc2d1	Bassetová
<g/>
.	.	kIx.	.
</s>
<s>
Nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
úvodní	úvodní	k2eAgFnSc4d1	úvodní
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Waiting	Waiting	k1gInSc1	Waiting
To	ten	k3xDgNnSc1	ten
Exhale	Exhala	k1gFnSc3	Exhala
(	(	kIx(	(
<g/>
Shoop	Shoop	k1gInSc1	Shoop
Shoop	Shoop	k1gInSc1	Shoop
<g/>
)	)	kIx)	)
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
boduje	bodovat	k5eAaImIp3nS	bodovat
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
na	na	k7c6	na
první	první	k4xOgFnSc6	první
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
zpěvačkami	zpěvačka	k1gFnPc7	zpěvačka
podílejícími	podílející	k2eAgFnPc7d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
byly	být	k5eAaImAgInP	být
Aretha	Aretha	k1gFnSc1	Aretha
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
J.	J.	kA	J.
Blige	Blig	k1gFnPc1	Blig
<g/>
,	,	kIx,	,
Toni	Toni	k1gFnPc1	Toni
Braxton	Braxton	k1gInSc1	Braxton
<g/>
,	,	kIx,	,
Brandy	brandy	k1gFnSc1	brandy
a	a	k8xC	a
Cece	cecat	k5eAaImIp3nS	cecat
Winans	Winans	k1gInSc4	Winans
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
Whitney	Whitne	k2eAgFnPc4d1	Whitne
na	na	k7c6	na
albu	album	k1gNnSc6	album
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Count	Count	k1gInSc4	Count
On	on	k3xPp3gMnSc1	on
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
zpěvačky	zpěvačka	k1gFnPc1	zpěvačka
poté	poté	k6eAd1	poté
společně	společně	k6eAd1	společně
vystoupily	vystoupit	k5eAaPmAgFnP	vystoupit
na	na	k7c6	na
předávání	předávání	k1gNnSc6	předávání
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
z	z	k7c2	z
téhož	týž	k3xTgNnSc2	týž
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgInSc7d1	poslední
soundtrackem	soundtrack	k1gInSc7	soundtrack
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc4	album
k	k	k7c3	k
filmu	film	k1gInSc3	film
Kazatelova	kazatelův	k2eAgFnSc1d1	Kazatelova
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Whitney	Whitnea	k1gFnSc2	Whitnea
hraje	hrát	k5eAaImIp3nS	hrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Denzela	Denzela	k1gMnSc4	Denzela
Washingtona	Washington	k1gMnSc4	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
alba	album	k1gNnSc2	album
tvoří	tvořit	k5eAaImIp3nP	tvořit
gospelové	gospelový	k2eAgFnPc1d1	gospelová
písně	píseň	k1gFnPc1	píseň
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
podání	podání	k1gNnSc6	podání
a	a	k8xC	a
také	také	k9	také
její	její	k3xOp3gFnPc4	její
matky	matka	k1gFnPc4	matka
Cissy	Cissa	k1gFnSc2	Cissa
Houstonové	Houstonový	k2eAgFnSc2d1	Houstonová
a	a	k8xC	a
gospelové	gospelový	k2eAgFnSc2d1	gospelová
legendy	legenda	k1gFnSc2	legenda
Shirley	Shirlea	k1gFnSc2	Shirlea
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
gospelovou	gospelův	k2eAgFnSc7d1	gospelův
deskou	deska	k1gFnSc7	deska
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
hity	hit	k1gInPc1	hit
z	z	k7c2	z
alba	album	k1gNnSc2	album
jsou	být	k5eAaImIp3nP	být
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Step	step	k1gInSc1	step
by	by	kYmCp3nS	by
Step	step	k1gInSc1	step
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vokály	vokál	k1gInPc4	vokál
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
Annie	Annie	k1gFnSc1	Annie
Lennox	Lennox	k1gInSc1	Lennox
a	a	k8xC	a
balada	balada	k1gFnSc1	balada
s	s	k7c7	s
gospelovým	gospelův	k2eAgInSc7d1	gospelův
nádechem	nádech	k1gInSc7	nádech
"	"	kIx"	"
<g/>
I	i	k9	i
Believe	Belieev	k1gFnSc2	Belieev
In	In	k1gFnSc2	In
You	You	k1gFnSc2	You
And	Anda	k1gFnPc2	Anda
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
řadovým	řadový	k2eAgNnSc7d1	řadové
albem	album	k1gNnSc7	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
Is	Is	k1gMnSc1	Is
Your	Your	k1gInSc1	Your
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xS	jako
Wyclef	Wyclef	k1gMnSc1	Wyclef
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
Lauryn	Lauryn	k1gMnSc1	Lauryn
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Missy	Miss	k1gMnPc4	Miss
Elliott	Elliotta	k1gFnPc2	Elliotta
nebo	nebo	k8xC	nebo
Rodney	Rodnea	k1gFnSc2	Rodnea
Jerkins	Jerkinsa	k1gFnPc2	Jerkinsa
<g/>
.	.	kIx.	.
</s>
<s>
Úvodním	úvodní	k2eAgInSc7d1	úvodní
hitem	hit	k1gInSc7	hit
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
Is	Is	k1gMnSc1	Is
Your	Your	k1gInSc1	Your
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejhranější	hraný	k2eAgFnSc7d3	nejhranější
písní	píseň	k1gFnSc7	píseň
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
hitem	hit	k1gInSc7	hit
byl	být	k5eAaImAgInS	být
song	song	k1gInSc1	song
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Not	nota	k1gFnPc2	nota
Right	Right	k1gInSc4	Right
But	But	k1gFnSc4	But
Is	Is	k1gMnSc2	Is
Okay	Okaa	k1gMnSc2	Okaa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
odnesla	odnést	k5eAaPmAgFnS	odnést
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
B	B	kA	B
výkon	výkon	k1gInSc1	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Heartbreak	Heartbreak	k1gMnSc1	Heartbreak
Hotel	hotel	k1gInSc1	hotel
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
společně	společně	k6eAd1	společně
se	s	k7c7	s
zpěvačkami	zpěvačka	k1gFnPc7	zpěvačka
Faith	Faitha	k1gFnPc2	Faitha
Evans	Evansa	k1gFnPc2	Evansa
a	a	k8xC	a
Kelly	Kella	k1gFnSc2	Kella
Price	Priec	k1gInSc2	Priec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
si	se	k3xPyFc3	se
také	také	k9	také
společně	společně	k6eAd1	společně
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
duet	duet	k1gInSc4	duet
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Mariah	Mariaha	k1gFnPc2	Mariaha
Carey	Carea	k1gFnSc2	Carea
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
When	When	k1gInSc1	When
You	You	k1gFnSc2	You
Believe	Belieev	k1gFnSc2	Belieev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
vydáno	vydán	k2eAgNnSc4d1	vydáno
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
a	a	k8xC	a
DVD	DVD	kA	DVD
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
od	od	k7c2	od
rychlých	rychlý	k2eAgFnPc2d1	rychlá
pecek	pecka	k1gFnPc2	pecka
po	po	k7c4	po
táhlé	táhlý	k2eAgFnPc4d1	táhlá
balady	balada	k1gFnPc4	balada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
její	její	k3xOp3gFnSc7	její
doménou	doména	k1gFnSc7	doména
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
několik	několik	k4yIc1	několik
nových	nový	k2eAgFnPc2d1	nová
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
duet	duet	k1gInSc4	duet
s	s	k7c7	s
Georgem	Georg	k1gMnSc7	Georg
Michaelem	Michael	k1gMnSc7	Michael
(	(	kIx(	(
<g/>
If	If	k1gMnSc1	If
I	i	k8xC	i
Told	Told	k1gMnSc1	Told
You	You	k1gMnSc1	You
That	That	k1gMnSc1	That
<g/>
)	)	kIx)	)
a	a	k8xC	a
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Enriquem	Enriqu	k1gMnSc7	Enriqu
Iglesiasem	Iglesiasma	k1gFnPc2	Iglesiasma
Could	Could	k1gInSc1	Could
I	i	k8xC	i
Have	Have	k1gInSc1	Have
The	The	k1gFnSc2	The
Kiss	Kissa	k1gFnPc2	Kissa
Forever	Forever	k1gMnSc1	Forever
<g/>
.	.	kIx.	.
</s>
<s>
Alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
na	na	k7c4	na
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
hudebních	hudební	k2eAgInPc2d1	hudební
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začala	začít	k5eAaPmAgFnS	začít
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
nejsou	být	k5eNaImIp3nP	být
výjimkou	výjimka	k1gFnSc7	výjimka
odříkávání	odříkávání	k1gNnSc2	odříkávání
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
stávala	stávat	k5eAaImAgFnS	stávat
terčem	terč	k1gInSc7	terč
bulvárních	bulvární	k2eAgInPc2d1	bulvární
plátků	plátek	k1gInPc2	plátek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přinášely	přinášet	k5eAaImAgFnP	přinášet
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
drogové	drogový	k2eAgFnSc6d1	drogová
závislosti	závislost	k1gFnSc6	závislost
<g/>
,	,	kIx,	,
rodinných	rodinný	k2eAgInPc6d1	rodinný
problémech	problém	k1gInPc6	problém
a	a	k8xC	a
fyzických	fyzický	k2eAgInPc6d1	fyzický
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
manželovy	manželův	k2eAgFnSc2d1	manželova
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
náhlém	náhlý	k2eAgNnSc6d1	náhlé
úmrtí	úmrtí	k1gNnSc6	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
podepsala	podepsat	k5eAaPmAgFnS	podepsat
nejdražší	drahý	k2eAgInSc4d3	nejdražší
kontrakt	kontrakt	k1gInSc4	kontrakt
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
na	na	k7c4	na
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c6	na
natočení	natočení	k1gNnSc6	natočení
dalších	další	k2eAgNnPc2d1	další
6	[number]	k4	6
nových	nový	k2eAgNnPc2d1	nové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jejím	její	k3xOp3gNnSc7	její
posledním	poslední	k2eAgNnSc7d1	poslední
řadovým	řadový	k2eAgNnSc7d1	řadové
albem	album	k1gNnSc7	album
je	být	k5eAaImIp3nS	být
Just	just	k6eAd1	just
Whitney	Whitnea	k1gFnPc4	Whitnea
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
hlase	hlas	k1gInSc6	hlas
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přece	přece	k9	přece
jen	jen	k9	jen
znát	znát	k5eAaImF	znát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
Babyface	Babyface	k1gFnSc1	Babyface
<g/>
,	,	kIx,	,
Missy	Missa	k1gFnPc1	Missa
Elliott	Elliott	k1gInSc1	Elliott
<g/>
,	,	kIx,	,
Puff	puff	k1gInSc1	puff
Daddy	Dadda	k1gFnSc2	Dadda
a	a	k8xC	a
také	také	k9	také
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Bobby	Bobba	k1gFnSc2	Bobba
Brown	Brown	k1gMnSc1	Brown
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Whatchulookinat	Whatchulookinat	k1gFnPc6	Whatchulookinat
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
navezla	navézt	k5eAaPmAgFnS	navézt
do	do	k7c2	do
světa	svět	k1gInSc2	svět
bulváru	bulvár	k1gInSc2	bulvár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jí	jíst	k5eAaImIp3nS	jíst
ztěžoval	ztěžovat	k5eAaImAgMnS	ztěžovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
situaci	situace	k1gFnSc3	situace
album	album	k1gNnSc4	album
nebylo	být	k5eNaImAgNnS	být
tak	tak	k9	tak
komerčně	komerčně	k6eAd1	komerčně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
ještě	ještě	k9	ještě
vydala	vydat	k5eAaPmAgFnS	vydat
vánoční	vánoční	k2eAgNnSc4d1	vánoční
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
One	One	k1gMnSc1	One
Wish	Wish	k1gMnSc1	Wish
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Holiday	Holidaa	k1gFnSc2	Holidaa
Album	album	k1gNnSc4	album
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
pět	pět	k4xCc4	pět
let	let	k1gInSc4	let
opustila	opustit	k5eAaPmAgFnS	opustit
svět	svět	k1gInSc4	svět
showbusinessu	showbusiness	k1gInSc2	showbusiness
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
křesťansky	křesťansky	k6eAd1	křesťansky
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
je	být	k5eAaImIp3nS	být
gospelová	gospelový	k2eAgFnSc1d1	gospelová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zpívala	zpívat	k5eAaImAgFnS	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
Elvisu	Elvisu	k?	Elvisu
Presleymu	Presleym	k1gInSc2	Presleym
a	a	k8xC	a
Arethě	Aretha	k1gFnSc3	Aretha
Franklinové	Franklinový	k2eAgFnPc1d1	Franklinová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
nalezena	nalezen	k2eAgFnSc1d1	nalezena
bezvládná	bezvládný	k2eAgFnSc1d1	bezvládná
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Beverly	Beverla	k1gFnSc2	Beverla
Hilton	Hilton	k1gInSc1	Hilton
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Beverly	Beverl	k1gMnPc4	Beverl
Hills	Hills	k1gInSc4	Hills
<g/>
.	.	kIx.	.
</s>
<s>
Záchranáři	záchranář	k1gMnPc1	záchranář
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
snažili	snažit	k5eAaImAgMnP	snažit
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
neúspěšně	úspěšně	k6eNd1	úspěšně
oživit	oživit	k5eAaPmF	oživit
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
SEČ	SEČ	kA	SEČ
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Bobbi	Bobb	k1gFnSc2	Bobb
Kristina	Kristina	k1gFnSc1	Kristina
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Whitney	Whitnea	k1gFnSc2	Whitnea
Houston	Houston	k1gInSc1	Houston
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
Whitney	Whitney	k1gInPc4	Whitney
Houston	Houston	k1gInSc1	Houston
1987	[number]	k4	1987
Whitney	Whitnea	k1gFnSc2	Whitnea
1990	[number]	k4	1990
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Your	Your	k1gInSc4	Your
Baby	baba	k1gFnSc2	baba
Tonight	Tonight	k2eAgInSc4d1	Tonight
1998	[number]	k4	1998
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
Is	Is	k1gMnSc1	Is
Your	Your	k1gInSc1	Your
Love	lov	k1gInSc5	lov
2002	[number]	k4	2002
Just	just	k6eAd1	just
Whitney	Whitnea	k1gFnPc1	Whitnea
2003	[number]	k4	2003
One	One	k1gFnPc2	One
Wish	Wisha	k1gFnPc2	Wisha
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Holiday	Holidaa	k1gFnSc2	Holidaa
Album	album	k1gNnSc1	album
2009	[number]	k4	2009
I	i	k8xC	i
Look	Look	k1gInSc4	Look
to	ten	k3xDgNnSc1	ten
You	You	k1gFnSc1	You
1992	[number]	k4	1992
The	The	k1gMnSc1	The
Bodyguard	bodyguard	k1gMnSc1	bodyguard
<g/>
:	:	kIx,	:
Original	Original	k1gFnSc1	Original
Soundtrack	soundtrack	k1gInSc1	soundtrack
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
Bodyguard	bodyguard	k1gMnSc1	bodyguard
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
Až	až	k6eAd1	až
si	se	k3xPyFc3	se
vydechnu	vydechnout	k5eAaPmIp1nS	vydechnout
(	(	kIx(	(
<g/>
Waiting	Waiting	k1gInSc1	Waiting
to	ten	k3xDgNnSc1	ten
Exhale	Exhala	k1gFnSc6	Exhala
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
Kazatelova	kazatelův	k2eAgFnSc1d1	Kazatelova
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
Preacher	Preachra	k1gFnPc2	Preachra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wife	Wife	k1gFnSc7	Wife
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Whitney	Whitnea	k1gFnSc2	Whitnea
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Greatest	Greatest	k1gMnSc1	Greatest
Hits	Hits	k1gInSc4	Hits
2001	[number]	k4	2001
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Whitney	Whitne	k2eAgFnPc1d1	Whitne
2007	[number]	k4	2007
Ultimate	Ultimat	k1gInSc5	Ultimat
Collection	Collection	k1gInSc4	Collection
</s>
