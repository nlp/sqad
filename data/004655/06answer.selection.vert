<s>
Říše	říše	k1gFnSc1	říše
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
pojetí	pojetí	k1gNnSc6	pojetí
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
podříší	podříše	k1gFnSc7	podříše
"	"	kIx"	"
<g/>
mnohobuněční	mnohobuněční	k2eAgMnSc1d1	mnohobuněční
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Metazoa	Metazoa	k1gFnSc1	Metazoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
tedy	tedy	k9	tedy
žádné	žádný	k3yNgMnPc4	žádný
prvoky	prvok	k1gMnPc4	prvok
<g/>
.	.	kIx.	.
</s>
