<p>
<s>
Indigovník	indigovník	k1gInSc1	indigovník
pravý	pravý	k2eAgInSc1d1	pravý
(	(	kIx(	(
<g/>
Indigofera	Indigofera	k1gFnSc1	Indigofera
tinctoria	tinctorium	k1gNnSc2	tinctorium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
subtropická	subtropický	k2eAgFnSc1d1	subtropická
kulturní	kulturní	k2eAgFnSc1d1	kulturní
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
velmi	velmi	k6eAd1	velmi
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
rodu	rod	k1gInSc2	rod
indigovník	indigovník	k1gInSc1	indigovník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lidmi	člověk	k1gMnPc7	člověk
pěstován	pěstován	k2eAgInSc4d1	pěstován
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
jasně	jasně	k6eAd1	jasně
modré	modré	k1gNnSc1	modré
barvivo	barvivo	k1gNnSc1	barvivo
"	"	kIx"	"
<g/>
indigo	indigo	k1gNnSc1	indigo
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
barvení	barvení	k1gNnSc4	barvení
textilií	textilie	k1gFnPc2	textilie
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
inkoustu	inkoust	k1gInSc2	inkoust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
tropických	tropický	k2eAgFnPc2d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc2d1	subtropická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
rozšíření	rozšíření	k1gNnSc2	rozšíření
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
pěstována	pěstován	k2eAgFnSc1d1	pěstována
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
většiny	většina	k1gFnSc2	většina
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
i	i	k8xC	i
tropické	tropický	k2eAgFnSc2d1	tropická
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vysazována	vysazovat	k5eAaImNgFnS	vysazovat
i	i	k9	i
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
k	k	k7c3	k
vůli	vůle	k1gFnSc3	vůle
chladnému	chladný	k2eAgNnSc3d1	chladné
klimatu	klima	k1gNnSc3	klima
nedoputovala	doputovat	k5eNaPmAgFnS	doputovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgNnSc4d1	přírodní
indigo	indigo	k1gNnSc4	indigo
při	při	k7c6	při
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
barvení	barvení	k1gNnSc6	barvení
látek	látka	k1gFnPc2	látka
vytlačeno	vytlačit	k5eAaPmNgNnS	vytlačit
chemickými	chemický	k2eAgNnPc7d1	chemické
barvivy	barvivo	k1gNnPc7	barvivo
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
jen	jen	k9	jen
řídce	řídce	k6eAd1	řídce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
zaplavovaných	zaplavovaný	k2eAgFnPc6d1	zaplavovaná
travnatých	travnatý	k2eAgFnPc6d1	travnatá
oblastech	oblast	k1gFnPc6	oblast
jen	jen	k9	jen
rozptýleně	rozptýleně	k6eAd1	rozptýleně
porostlých	porostlý	k2eAgFnPc2d1	porostlá
vzrostlými	vzrostlý	k2eAgInPc7d1	vzrostlý
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
do	do	k7c2	do
1000	[number]	k4	1000
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
500	[number]	k4	500
do	do	k7c2	do
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhotrvající	dlouhotrvající	k2eAgInPc1d1	dlouhotrvající
deště	dešť	k1gInPc1	dešť
a	a	k8xC	a
záplavy	záplava	k1gFnPc1	záplava
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
suché	suchý	k2eAgInPc1d1	suchý
větry	vítr	k1gInPc1	vítr
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vadnutí	vadnutí	k1gNnSc4	vadnutí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Porost	porost	k1gInSc1	porost
indigovníku	indigovník	k1gInSc2	indigovník
pravého	pravý	k2eAgInSc2d1	pravý
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
výsevem	výsev	k1gInSc7	výsev
skarifikovaných	skarifikovaný	k2eAgNnPc2d1	skarifikovaný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
předem	předem	k6eAd1	předem
namáčených	namáčený	k2eAgNnPc2d1	namáčené
semen	semeno	k1gNnPc2	semeno
do	do	k7c2	do
sponu	spon	k1gInSc2	spon
30	[number]	k4	30
×	×	k?	×
30	[number]	k4	30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
klíčí	klíčit	k5eAaImIp3nP	klíčit
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
také	také	k9	také
vysazovat	vysazovat	k5eAaImF	vysazovat
semenáče	semenáč	k1gInPc4	semenáč
vypěstované	vypěstovaný	k2eAgInPc4d1	vypěstovaný
ve	v	k7c6	v
školce	školka	k1gFnSc6	školka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyrašení	vyrašení	k1gNnSc6	vyrašení
již	již	k6eAd1	již
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
mnoho	mnoho	k4c4	mnoho
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
pleje	plít	k5eAaImIp3nS	plít
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dokáže	dokázat	k5eAaPmIp3nS	dokázat
kořenovými	kořenový	k2eAgFnPc7d1	kořenová
bakteriemi	bakterie	k1gFnPc7	bakterie
fixovat	fixovat	k5eAaImF	fixovat
vzdušný	vzdušný	k2eAgInSc1d1	vzdušný
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
pěstovat	pěstovat	k5eAaImF	pěstovat
i	i	k9	i
na	na	k7c6	na
nevyhnojených	vyhnojený	k2eNgFnPc6d1	vyhnojený
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Polokeřovitá	polokeřovitý	k2eAgFnSc1d1	polokeřovitá
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
bylinnými	bylinný	k2eAgInPc7d1	bylinný
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
rozvětvenými	rozvětvený	k2eAgInPc7d1	rozvětvený
prýty	prýt	k1gInPc7	prýt
dorůstající	dorůstající	k2eAgFnSc2d1	dorůstající
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
m.	m.	k?	m.
Stonky	stonka	k1gFnPc1	stonka
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgFnPc1d1	porostlá
krátkými	krátká	k1gFnPc7	krátká
<g/>
,	,	kIx,	,
štětinovými	štětinový	k2eAgInPc7d1	štětinový
palisty	palist	k1gInPc7	palist
a	a	k8xC	a
spirálovitě	spirálovitě	k6eAd1	spirálovitě
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
<g/>
,	,	kIx,	,
lichozpeřenými	lichozpeřený	k2eAgInPc7d1	lichozpeřený
<g/>
,	,	kIx,	,
řapíkatými	řapíkatý	k2eAgInPc7d1	řapíkatý
listy	list	k1gInPc7	list
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
4	[number]	k4	4
až	až	k9	až
8	[number]	k4	8
cm	cm	kA	cm
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
známému	známý	k1gMnSc3	známý
trnovku	trnovka	k1gFnSc4	trnovka
akátu	akát	k1gInSc2	akát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oválné	oválný	k2eAgInPc1d1	oválný
až	až	k8xS	až
obvejčité	obvejčitý	k2eAgInPc1d1	obvejčitý
lístky	lístek	k1gInPc1	lístek
<g/>
,	,	kIx,	,
vyrůstající	vyrůstající	k2eAgFnSc1d1	vyrůstající
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
až	až	k8xS	až
sedmi	sedm	k4xCc6	sedm
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
1,5	[number]	k4	1,5
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
a	a	k8xC	a
široké	široký	k2eAgNnSc1d1	široké
1	[number]	k4	1
až	až	k9	až
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
u	u	k7c2	u
krátkého	krátký	k2eAgInSc2d1	krátký
řapíčku	řapíček	k1gInSc2	řapíček
jsou	být	k5eAaImIp3nP	být
čepele	čepel	k1gFnPc1	čepel
lístků	lístek	k1gInPc2	lístek
tupé	tupý	k2eAgFnPc1d1	tupá
nebo	nebo	k8xC	nebo
špičaté	špičatý	k2eAgFnPc1d1	špičatá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
zaoblené	zaoblený	k2eAgFnSc2d1	zaoblená
neb	neb	k8xC	neb
hrotité	hrotitý	k2eAgFnSc2d1	hrotitá
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
mají	mít	k5eAaImIp3nP	mít
jemně	jemně	k6eAd1	jemně
chlupatou	chlupatý	k2eAgFnSc4d1	chlupatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drobné	drobný	k2eAgNnSc1d1	drobné
bílé	bílé	k1gNnSc1	bílé
<g/>
,	,	kIx,	,
červenavě	červenavě	k6eAd1	červenavě
žluté	žlutý	k2eAgFnPc1d1	žlutá
nebo	nebo	k8xC	nebo
lososově	lososově	k6eAd1	lososově
růžové	růžový	k2eAgInPc4d1	růžový
květy	květ	k1gInPc4	květ
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
v	v	k7c6	v
8	[number]	k4	8
až	až	k9	až
16	[number]	k4	16
cm	cm	kA	cm
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
hroznech	hrozen	k1gInPc6	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavné	oboupohlavný	k2eAgInPc4d1	oboupohlavný
květy	květ	k1gInPc4	květ
mají	mít	k5eAaImIp3nP	mít
srostlý	srostlý	k2eAgMnSc1d1	srostlý
<g/>
,	,	kIx,	,
stříbřitě	stříbřitě	k6eAd1	stříbřitě
pýřitý	pýřitý	k2eAgInSc4d1	pýřitý
kalich	kalich	k1gInSc4	kalich
asi	asi	k9	asi
2	[number]	k4	2
mm	mm	kA	mm
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
do	do	k7c2	do
5	[number]	k4	5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
plátky	plátek	k1gInPc4	plátek
volné	volný	k2eAgInPc4d1	volný
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
srostlé	srostlý	k2eAgInPc4d1	srostlý
v	v	k7c4	v
člunek	člunek	k1gInSc4	člunek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květu	květ	k1gInSc6	květ
je	být	k5eAaImIp3nS	být
deset	deset	k4xCc1	deset
dvoubratrých	dvoubratrý	k2eAgFnPc2d1	dvoubratrý
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
horní	horní	k2eAgFnSc1d1	horní
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgInSc1d1	vrchní
jednodílný	jednodílný	k2eAgInSc1d1	jednodílný
semeník	semeník	k1gInSc1	semeník
nese	nést	k5eAaImIp3nS	nést
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
čnělku	čnělka	k1gFnSc4	čnělka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
,	,	kIx,	,
válcovité	válcovitý	k2eAgInPc1d1	válcovitý
lusky	lusk	k1gInPc1	lusk
2	[number]	k4	2
až	až	k8xS	až
4	[number]	k4	4
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
průměrně	průměrně	k6eAd1	průměrně
10	[number]	k4	10
kulovitých	kulovitý	k2eAgFnPc2d1	kulovitá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2	[number]	k4	2
mm	mm	kA	mm
velkých	velká	k1gFnPc2	velká
semen	semeno	k1gNnPc2	semeno
s	s	k7c7	s
pevným	pevný	k2eAgNnSc7d1	pevné
osemením	osemení	k1gNnSc7	osemení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Indigovník	indigovník	k1gInSc4	indigovník
pravý	pravý	k2eAgInSc4d1	pravý
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
pro	pro	k7c4	pro
modré	modrý	k2eAgNnSc4d1	modré
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkvalitnějších	kvalitní	k2eAgNnPc2d3	nejkvalitnější
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nadrcených	nadrcený	k2eAgInPc2d1	nadrcený
stonků	stonek	k1gInPc2	stonek
keřů	keř	k1gInPc2	keř
pokosených	pokosený	k2eAgInPc2d1	pokosený
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
kvetení	kvetení	k1gNnSc2	kvetení
<g/>
.	.	kIx.	.
</s>
<s>
Drť	drť	k1gFnSc1	drť
se	se	k3xPyFc4	se
asi	asi	k9	asi
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
fermentuje	fermentovat	k5eAaBmIp3nS	fermentovat
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následkem	následkem	k7c2	následkem
enzymové	enzymový	k2eAgFnSc2d1	enzymová
hydrolýzy	hydrolýza	k1gFnSc2	hydrolýza
vzniká	vznikat	k5eAaImIp3nS	vznikat
žlutý	žlutý	k2eAgInSc4d1	žlutý
indoxyl	indoxyl	k1gInSc4	indoxyl
<g/>
.	.	kIx.	.
</s>
<s>
Výluh	výluh	k1gInSc1	výluh
se	se	k3xPyFc4	se
přefiltruje	přefiltrovat	k5eAaPmIp3nS	přefiltrovat
a	a	k8xC	a
za	za	k7c2	za
stálého	stálý	k2eAgNnSc2d1	stálé
míchání	míchání	k1gNnSc2	míchání
se	se	k3xPyFc4	se
indoxyl	indoxyl	k1gInSc4	indoxyl
působením	působení	k1gNnSc7	působení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
v	v	k7c4	v
modré	modrý	k2eAgNnSc4d1	modré
indigo	indigo	k1gNnSc4	indigo
usazující	usazující	k2eAgFnSc2d1	usazující
se	se	k3xPyFc4	se
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kalu	kal	k1gInSc2	kal
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
přečistí	přečistit	k5eAaPmIp3nS	přečistit
a	a	k8xC	a
pevný	pevný	k2eAgInSc4d1	pevný
zbytek	zbytek	k1gInSc4	zbytek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc4d1	nerozpustný
pigment	pigment	k1gInSc4	pigment
indigo	indigo	k1gNnSc4	indigo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
usuší	usušit	k5eAaPmIp3nS	usušit
a	a	k8xC	a
slisuje	slisovat	k5eAaPmIp3nS	slisovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
z	z	k7c2	z
hektaru	hektar	k1gInSc2	hektar
sklidí	sklidit	k5eAaPmIp3nS	sklidit
asi	asi	k9	asi
10	[number]	k4	10
tun	tuna	k1gFnPc2	tuna
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kilogram	kilogram	k1gInSc4	kilogram
barviva	barvivo	k1gNnSc2	barvivo
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
asi	asi	k9	asi
250	[number]	k4	250
kg	kg	kA	kg
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokosené	pokosený	k2eAgInPc1d1	pokosený
keře	keř	k1gInPc1	keř
brzy	brzy	k6eAd1	brzy
obrůstají	obrůstat	k5eAaImIp3nP	obrůstat
<g/>
,	,	kIx,	,
sklizeň	sklizeň	k1gFnSc1	sklizeň
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zaorají	zaorat	k5eAaImIp3nP	zaorat
<g/>
.	.	kIx.	.
</s>
<s>
Barvivo	barvivo	k1gNnSc1	barvivo
nesprávně	správně	k6eNd1	správně
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
"	"	kIx"	"
<g/>
indigo	indigo	k1gNnSc1	indigo
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
také	také	k9	také
z	z	k7c2	z
borytu	boryt	k1gInSc2	boryt
barvířského	barvířský	k2eAgInSc2d1	barvířský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
objevu	objev	k1gInSc2	objev
syntetických	syntetický	k2eAgNnPc2d1	syntetické
barviv	barvivo	k1gNnPc2	barvivo
používání	používání	k1gNnSc1	používání
indiga	indigo	k1gNnSc2	indigo
silně	silně	k6eAd1	silně
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
domácích	domácí	k2eAgFnPc2d1	domácí
textilií	textilie	k1gFnPc2	textilie
u	u	k7c2	u
původních	původní	k2eAgMnPc2d1	původní
afro-asijských	afrosijský	k2eAgMnPc2d1	afro-asijský
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
restaurátorství	restaurátorství	k1gNnSc6	restaurátorství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
indigovník	indigovník	k1gInSc1	indigovník
pravý	pravý	k2eAgInSc1d1	pravý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
