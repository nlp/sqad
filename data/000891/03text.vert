<s>
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Giulio	Giulio	k1gMnSc1	Giulio
di	di	k?	di
Guiliano	Guiliana	k1gFnSc5	Guiliana
de	de	k?	de
Medici	medik	k1gMnPc1	medik
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1478	[number]	k4	1478
Florencie	Florencie	k1gFnSc1	Florencie
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1534	[number]	k4	1534
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1523	[number]	k4	1523
<g/>
-	-	kIx~	-
<g/>
1534	[number]	k4	1534
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálem	kardinál	k1gMnSc7	kardinál
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1513	[number]	k4	1513
<g/>
-	-	kIx~	-
<g/>
1523	[number]	k4	1523
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
strýcem	strýc	k1gMnSc7	strýc
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Medicejské	Medicejský	k2eAgInPc1d1	Medicejský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Kateřininy	Kateřinin	k2eAgFnSc2d1	Kateřinina
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
čtrnáctiletou	čtrnáctiletý	k2eAgFnSc4d1	čtrnáctiletá
provdat	provdat	k5eAaPmF	provdat
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1533	[number]	k4	1533
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
za	za	k7c4	za
stejně	stejně	k6eAd1	stejně
starého	starý	k2eAgMnSc2d1	starý
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vévodu	vévoda	k1gMnSc4	vévoda
Orleánského	orleánský	k2eAgMnSc4d1	orleánský
<g/>
,	,	kIx,	,
druhorozeného	druhorozený	k2eAgMnSc4d1	druhorozený
syna	syn	k1gMnSc4	syn
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
Františka	František	k1gMnSc4	František
I.	I.	kA	I.
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c4	v
Florencii	Florencie	k1gFnSc4	Florencie
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Giuliano	Giuliana	k1gFnSc5	Giuliana
de	de	k?	de
Medici	medik	k1gMnPc1	medik
zabit	zabít	k5eAaPmNgMnS	zabít
při	při	k7c6	při
spiknutí	spiknutí	k1gNnSc6	spiknutí
Pazziů	Pazzi	k1gInPc2	Pazzi
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
neuzavřeli	uzavřít	k5eNaPmAgMnP	uzavřít
formální	formální	k2eAgNnSc4d1	formální
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
byli	být	k5eAaImAgMnP	být
zasnoubeni	zasnouben	k2eAgMnPc1d1	zasnouben
per	pero	k1gNnPc2	pero
sponsalia	sponsalius	k1gMnSc2	sponsalius
de	de	k?	de
presenti	present	k1gMnPc1	present
<g/>
,	,	kIx,	,
skulina	skulina	k1gFnSc1	skulina
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
umožnila	umožnit	k5eAaPmAgFnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Guilio	Guilio	k6eAd1	Guilio
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
legitimního	legitimní	k2eAgMnSc4d1	legitimní
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synovcem	synovec	k1gMnSc7	synovec
Lorenza	Lorenza	k?	Lorenza
Medicejského	Medicejský	k2eAgInSc2d1	Medicejský
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
<g/>
.	.	kIx.	.
</s>
<s>
Giulio	Giulio	k1gMnSc1	Giulio
byl	být	k5eAaImAgMnS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
rytířem	rytíř	k1gMnSc7	rytíř
z	z	k7c2	z
Rhodu	Rhodos	k1gInSc2	Rhodos
a	a	k8xC	a
velkopřevorem	velkopřevor	k1gMnSc7	velkopřevor
z	z	k7c2	z
Capuy	Capua	k1gFnSc2	Capua
a	a	k8xC	a
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
bratrance	bratranec	k1gMnSc2	bratranec
Giovanniho	Giovanni	k1gMnSc2	Giovanni
de	de	k?	de
Medici	medik	k1gMnPc1	medik
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
papeže	papež	k1gMnSc2	papež
jako	jako	k8xS	jako
Lva	lev	k1gInSc2	lev
X.	X.	kA	X.
se	se	k3xPyFc4	se
brzo	brzo	k6eAd1	brzo
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
mocnou	mocný	k2eAgFnSc7d1	mocná
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
do	do	k7c2	do
papežství	papežství	k1gNnSc2	papežství
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Giulio	Giulio	k6eAd1	Giulio
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
ministrem	ministr	k1gMnSc7	ministr
a	a	k8xC	a
důvěrníkem	důvěrník	k1gMnSc7	důvěrník
<g/>
,	,	kIx,	,
především	především	k9	především
při	při	k7c6	při
prohlubování	prohlubování	k1gNnSc6	prohlubování
zájmů	zájem	k1gInPc2	zájem
rodiny	rodina	k1gFnSc2	rodina
Medici	medik	k1gMnPc1	medik
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
a	a	k8xC	a
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1513	[number]	k4	1513
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kardinálem	kardinál	k1gMnSc7	kardinál
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byl	být	k5eAaImAgMnS	být
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
pověst	pověst	k1gFnSc4	pověst
hlavního	hlavní	k2eAgMnSc2d1	hlavní
ředitele	ředitel	k1gMnSc2	ředitel
papežské	papežský	k2eAgFnSc2d1	Papežská
politiky	politika	k1gFnSc2	politika
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
pontifikátu	pontifikát	k1gInSc2	pontifikát
Lva	lev	k1gInSc2	lev
X.	X.	kA	X.
V	v	k7c6	v
době	doba	k1gFnSc6	doba
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
konkláve	konkláve	k1gNnSc2	konkláve
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Lva	lev	k1gInSc2	lev
X.	X.	kA	X.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
kardinál	kardinál	k1gMnSc1	kardinál
Medici	medik	k1gMnPc1	medik
za	za	k7c7	za
zvláště	zvláště	k6eAd1	zvláště
pravděpodobného	pravděpodobný	k2eAgMnSc4d1	pravděpodobný
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
získat	získat	k5eAaPmF	získat
papežství	papežství	k1gNnSc4	papežství
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
nebo	nebo	k8xC	nebo
svého	svůj	k3xOyFgMnSc4	svůj
spojence	spojenec	k1gMnPc4	spojenec
Alessandra	Alessandra	k1gFnSc1	Alessandra
Farnese	Farnese	k1gFnSc2	Farnese
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
upřednostňovaní	upřednostňovaný	k2eAgMnPc1d1	upřednostňovaný
kandidáti	kandidát	k1gMnPc1	kandidát
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
slovo	slovo	k1gNnSc4	slovo
při	při	k7c6	při
neočekávaném	očekávaný	k2eNgNnSc6d1	neočekávané
zvolení	zvolení	k1gNnSc6	zvolení
Hadriána	Hadrián	k1gMnSc2	Hadrián
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yRgMnSc4	který
měl	mít	k5eAaImAgInS	mít
rovněž	rovněž	k9	rovněž
obrovský	obrovský	k2eAgInSc1d1	obrovský
vliv	vliv	k1gInSc1	vliv
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
krátkém	krátký	k2eAgInSc6d1	krátký
pontifikátu	pontifikát	k1gInSc6	pontifikát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Hadriána	Hadrián	k1gMnSc2	Hadrián
VI	VI	kA	VI
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1523	[number]	k4	1523
Medici	medik	k1gMnPc1	medik
konečně	konečně	k6eAd1	konečně
uspěl	uspět	k5eAaPmAgMnS	uspět
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
papežské	papežský	k2eAgFnSc6d1	Papežská
volbě	volba	k1gFnSc6	volba
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1523	[number]	k4	1523
<g/>
)	)	kIx)	)
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
trůn	trůn	k1gInSc4	trůn
si	se	k3xPyFc3	se
přinesl	přinést	k5eAaPmAgMnS	přinést
vysokou	vysoký	k2eAgFnSc4d1	vysoká
reputaci	reputace	k1gFnSc4	reputace
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
politické	politický	k2eAgFnPc4d1	politická
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
měl	mít	k5eAaImAgMnS	mít
všechny	všechen	k3xTgFnPc4	všechen
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mazaného	mazaný	k2eAgMnSc2d1	mazaný
diplomata	diplomat	k1gMnSc2	diplomat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
světáckého	světácký	k2eAgMnSc4d1	světácký
a	a	k8xC	a
netečného	tečný	k2eNgMnSc4d1	tečný
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gNnSc2	on
dělo	dít	k5eAaBmAgNnS	dít
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pokračující	pokračující	k2eAgFnSc2d1	pokračující
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
poslal	poslat	k5eAaPmAgMnS	poslat
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
z	z	k7c2	z
Kapuy	Kapua	k1gMnSc2	Kapua
<g/>
,	,	kIx,	,
kardinála	kardinál	k1gMnSc2	kardinál
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
ze	z	k7c2	z
Schönbergu	Schönberg	k1gInSc2	Schönberg
<g/>
,	,	kIx,	,
ke	k	k7c3	k
králům	král	k1gMnPc3	král
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pokus	pokus	k1gInSc1	pokus
ale	ale	k9	ale
nevyšel	vyjít	k5eNaPmAgInS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pope	pop	k1gInSc5	pop
Clement	Clement	k1gInSc4	Clement
VII	VII	kA	VII
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Dílo	dílo	k1gNnSc1	dílo
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
Pope	pop	k1gMnSc5	pop
Clement	Clement	k1gInSc4	Clement
VII	VII	kA	VII
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Hierarchy	hierarcha	k1gMnSc2	hierarcha
of	of	k?	of
the	the	k?	the
Catholic	Catholice	k1gFnPc2	Catholice
Church	Church	k1gMnSc1	Church
-	-	kIx~	-
Pope	pop	k1gInSc5	pop
Clement	Clement	k1gInSc4	Clement
VII	VII	kA	VII
</s>
