<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
(	(	kIx(	(
<g/>
Centaurea	Centaure	k2eAgFnSc1d1	Centaurea
jacea	jacea	k1gFnSc1	jacea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
chrpa	chrpa	k1gFnSc1	chrpa
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Hranice	hranice	k1gFnSc1	hranice
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Balkánem	Balkán	k1gInSc7	Balkán
a	a	k8xC	a
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Druhotně	druhotně	k6eAd1	druhotně
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
zavlečen	zavlečen	k2eAgInSc4d1	zavlečen
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
především	především	k9	především
v	v	k7c6	v
termofytiku	termofytikum	k1gNnSc6	termofytikum
a	a	k8xC	a
mezofytiku	mezofytikum	k1gNnSc6	mezofytikum
<g/>
,	,	kIx,	,
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
roste	růst	k5eAaImIp3nS	růst
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
světlomilnou	světlomilný	k2eAgFnSc7d1	světlomilná
rostlinou	rostlina	k1gFnSc7	rostlina
bez	bez	k7c2	bez
výrazného	výrazný	k2eAgNnSc2d1	výrazné
ekologického	ekologický	k2eAgNnSc2d1	ekologické
vyhranění	vyhranění	k1gNnSc2	vyhranění
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
obvyklými	obvyklý	k2eAgMnPc7d1	obvyklý
stanovišti	stanoviště	k1gNnPc7	stanoviště
jsou	být	k5eAaImIp3nP	být
místa	místo	k1gNnPc4	místo
se	s	k7c7	s
suššími	suchý	k2eAgFnPc7d2	sušší
i	i	k8xC	i
vlhčími	vlhký	k2eAgFnPc7d2	vlhčí
půdami	půda	k1gFnPc7	půda
které	který	k3yIgFnSc2	který
mají	mít	k5eAaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
neutrální	neutrální	k2eAgInPc1d1	neutrální
až	až	k9	až
slabě	slabě	k6eAd1	slabě
kyselé	kyselý	k2eAgNnSc1d1	kyselé
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
poměrně	poměrně	k6eAd1	poměrně
hodně	hodně	k6eAd1	hodně
na	na	k7c6	na
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
mezích	mez	k1gFnPc6	mez
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
v	v	k7c6	v
příkopech	příkop	k1gInPc6	příkop
i	i	k8xC	i
na	na	k7c6	na
ruderalizovaných	ruderalizovaný	k2eAgNnPc6d1	ruderalizované
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
diagnostickým	diagnostický	k2eAgInSc7d1	diagnostický
druhem	druh	k1gInSc7	druh
lučních	luční	k2eAgNnPc2d1	luční
společenstev	společenstvo	k1gNnPc2	společenstvo
svazů	svaz	k1gInPc2	svaz
Arrhenaterion	Arrhenaterion	k1gInSc1	Arrhenaterion
a	a	k8xC	a
Molinion	Molinion	k1gInSc1	Molinion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
hranatou	hranatý	k2eAgFnSc7d1	hranatá
lodyhou	lodyha	k1gFnSc7	lodyha
která	který	k3yRgFnSc1	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
tlustého	tlustý	k2eAgInSc2d1	tlustý
<g/>
,	,	kIx,	,
vytrvalého	vytrvalý	k2eAgInSc2d1	vytrvalý
oddenku	oddenek	k1gInSc2	oddenek
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
50	[number]	k4	50
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
nebo	nebo	k8xC	nebo
vystoupavá	vystoupavý	k2eAgFnSc1d1	vystoupavá
a	a	k8xC	a
často	často	k6eAd1	často
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
řídce	řídce	k6eAd1	řídce
rozvětvená	rozvětvený	k2eAgFnSc1d1	rozvětvená
<g/>
,	,	kIx,	,
porostlá	porostlý	k2eAgFnSc1d1	porostlá
na	na	k7c4	na
omak	omak	k1gInSc4	omak
drsnými	drsný	k2eAgInPc7d1	drsný
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zelenými	zelené	k1gNnPc7	zelené
<g/>
,	,	kIx,	,
dosti	dosti	k6eAd1	dosti
variabilními	variabilní	k2eAgInPc7d1	variabilní
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
<g/>
,	,	kIx,	,
5	[number]	k4	5
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
v	v	k7c4	v
růžici	růžice	k1gFnSc4	růžice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kopinaté	kopinatý	k2eAgInPc4d1	kopinatý
<g/>
,	,	kIx,	,
zúžené	zúžený	k2eAgInPc4d1	zúžený
v	v	k7c4	v
řapík	řapík	k1gInSc4	řapík
<g/>
,	,	kIx,	,
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
dlouze	dlouho	k6eAd1	dlouho
zubaté	zubatý	k2eAgInPc1d1	zubatý
a	a	k8xC	a
při	při	k7c6	při
kvetení	kvetení	k1gNnSc6	kvetení
již	již	k9	již
většinou	většinou	k6eAd1	většinou
zaschlé	zaschlý	k2eAgNnSc1d1	zaschlé
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zmenšující	zmenšující	k2eAgFnSc1d1	zmenšující
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
<g/>
,	,	kIx,	,
vejčitého	vejčitý	k2eAgInSc2d1	vejčitý
až	až	k8xS	až
kopinatého	kopinatý	k2eAgInSc2d1	kopinatý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
celistvé	celistvý	k2eAgFnPc1d1	celistvá
nebo	nebo	k8xC	nebo
peřenolaločnaté	peřenolaločnatý	k2eAgFnPc1d1	peřenolaločnatý
až	až	k6eAd1	až
peřenoklané	peřenoklaný	k2eAgFnPc1d1	peřenoklaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
koncích	konec	k1gInPc6	konec
větví	větev	k1gFnPc2	větev
odbočujících	odbočující	k2eAgMnPc2d1	odbočující
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodyhy	lodyha	k1gFnSc2	lodyha
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
velké	velký	k2eAgInPc1d1	velký
květní	květní	k2eAgInPc1d1	květní
úbory	úbor	k1gInPc1	úbor
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
4	[number]	k4	4
cm	cm	kA	cm
s	s	k7c7	s
pěticípými	pěticípý	k2eAgFnPc7d1	pěticípá
růžovými	růžový	k2eAgFnPc7d1	růžová
až	až	k8xS	až
růžově	růžově	k6eAd1	růžově
fialovými	fialový	k2eAgFnPc7d1	fialová
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
bílými	bílý	k2eAgInPc7d1	bílý
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
terče	terč	k1gFnSc2	terč
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
trubkovité	trubkovitý	k2eAgInPc4d1	trubkovitý
protandrické	protandrický	k2eAgInPc4d1	protandrický
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pět	pět	k4xCc4	pět
tyčinek	tyčinka	k1gFnPc2	tyčinka
a	a	k8xC	a
pestík	pestík	k1gInSc1	pestík
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgInPc1d1	okrajový
paprskovité	paprskovitý	k2eAgInPc1d1	paprskovitý
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
neplodné	plodný	k2eNgInPc1d1	neplodný
(	(	kIx(	(
<g/>
nemají	mít	k5eNaImIp3nP	mít
pestíky	pestík	k1gInPc4	pestík
nebo	nebo	k8xC	nebo
tyčinky	tyčinka	k1gFnPc4	tyčinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
až	až	k8xS	až
vejčitý	vejčitý	k2eAgInSc1d1	vejčitý
zákrov	zákrov	k1gInSc1	zákrov
je	být	k5eAaImIp3nS	být
víceřadý	víceřadý	k2eAgInSc1d1	víceřadý
se	s	k7c7	s
světle	světle	k6eAd1	světle
hnědými	hnědý	k2eAgInPc7d1	hnědý
listeny	listen	k1gInPc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
zřetelně	zřetelně	k6eAd1	zřetelně
oddělené	oddělený	k2eAgNnSc1d1	oddělené
<g/>
,	,	kIx,	,
namnoze	namnoze	k6eAd1	namnoze
vyduté	vydutý	k2eAgFnPc1d1	vydutá
<g/>
,	,	kIx,	,
okrouhlé	okrouhlý	k2eAgFnPc1d1	okrouhlá
přívěsky	přívěska	k1gFnPc1	přívěska
s	s	k7c7	s
blanitými	blanitý	k2eAgInPc7d1	blanitý
okraji	okraj	k1gInPc7	okraj
<g/>
;	;	kIx,	;
u	u	k7c2	u
středních	střední	k2eAgInPc2d1	střední
zákrovních	zákrovní	k2eAgInPc2d1	zákrovní
listenů	listen	k1gInPc2	listen
jsou	být	k5eAaImIp3nP	být
přívěsky	přívěsek	k1gInPc1	přívěsek
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
4	[number]	k4	4
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vykvétá	vykvétat	k5eAaImIp3nS	vykvétat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Opylována	opylován	k2eAgFnSc1d1	opylována
je	být	k5eAaImIp3nS	být
včelami	včela	k1gFnPc7	včela
<g/>
,	,	kIx,	,
motýly	motýl	k1gMnPc7	motýl
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
neopylené	opylený	k2eNgInPc1d1	opylený
květy	květ	k1gInPc1	květ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
opylit	opylit	k5eAaPmF	opylit
i	i	k9	i
autogamicky	autogamicky	k6eAd1	autogamicky
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
obvejčitá	obvejčitý	k2eAgFnSc1d1	obvejčitá
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
<g/>
,	,	kIx,	,
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
lehká	lehký	k2eAgFnSc1d1	lehká
nažka	nažka	k1gFnSc1	nažka
bez	bez	k7c2	bez
chmýří	chmýří	k1gNnSc2	chmýří
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
límečkovitým	límečkovitý	k2eAgInSc7d1	límečkovitý
lemem	lem	k1gInSc7	lem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzrání	uzrání	k1gNnSc6	uzrání
se	se	k3xPyFc4	se
zákrov	zákrov	k1gInSc1	zákrov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosud	dosud	k6eAd1	dosud
nažky	nažka	k1gFnSc2	nažka
chránil	chránit	k5eAaImAgMnS	chránit
<g/>
,	,	kIx,	,
rozloží	rozložit	k5eAaPmIp3nS	rozložit
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
semena	semeno	k1gNnSc2	semeno
rozfouká	rozfoukat	k5eAaPmIp3nS	rozfoukat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Včelařství	včelařství	k1gNnSc2	včelařství
==	==	k?	==
</s>
</p>
<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiné	jiný	k2eAgFnPc4d1	jiná
chrpy	chrpa	k1gFnPc4	chrpa
výborná	výborný	k2eAgFnSc1d1	výborná
nektarodárná	ktarodárný	k2eNgFnSc1d1	nektarodárná
i	i	k8xC	i
pylodárná	pylodárný	k2eAgFnSc1d1	pylodárný
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Nektarium	nektarium	k1gNnSc1	nektarium
chrupy	chrup	k1gInPc4	chrup
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
0,42	[number]	k4	0,42
mg	mg	kA	mg
nektaru	nektar	k1gInSc2	nektar
s	s	k7c7	s
cukernatostí	cukernatost	k1gFnSc7	cukernatost
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cukerná	cukerný	k2eAgFnSc1d1	cukerná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
množství	množství	k1gNnSc1	množství
cukru	cukr	k1gInSc2	cukr
vyprodukovaného	vyprodukovaný	k2eAgInSc2d1	vyprodukovaný
v	v	k7c6	v
květu	květ	k1gInSc6	květ
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
0,19	[number]	k4	0,19
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nektaru	nektar	k1gInSc2	nektar
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
včelám	včela	k1gFnPc3	včela
množství	množství	k1gNnSc2	množství
pylu	pyl	k1gInSc2	pyl
bohatého	bohatý	k2eAgInSc2d1	bohatý
na	na	k7c4	na
dusíkaté	dusíkatý	k2eAgFnPc4d1	dusíkatá
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnSc2	včela
jej	on	k3xPp3gMnSc4	on
sbírají	sbírat	k5eAaImIp3nP	sbírat
v	v	k7c6	v
šedých	šedý	k2eAgFnPc6d1	šedá
rouskách	rouska	k1gFnPc6	rouska
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhový	druhový	k2eAgInSc1d1	druhový
med	med	k1gInSc1	med
chrpy	chrpa	k1gFnSc2	chrpa
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jantarově	jantarově	k6eAd1	jantarově
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgFnPc1d1	jemná
vůně	vůně	k1gFnPc1	vůně
a	a	k8xC	a
příjemné	příjemný	k2eAgFnPc1d1	příjemná
chuti	chuť	k1gFnPc1	chuť
<g/>
,	,	kIx,	,
a	a	k8xC	a
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
hrubých	hrubý	k2eAgInPc6d1	hrubý
krystalech	krystal	k1gInPc6	krystal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
(	(	kIx(	(
<g/>
Centaurea	Centaure	k2eAgFnSc1d1	Centaurea
jacea	jacea	k1gFnSc1	jacea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součásti	součást	k1gFnPc4	součást
skupiny	skupina	k1gFnSc2	skupina
druhů	druh	k1gInPc2	druh
Centaurea	Centaurea	k1gMnSc1	Centaurea
jacea	jacea	k1gMnSc1	jacea
agg	agg	k?	agg
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
taxonů	taxon	k1gInPc2	taxon
uvnitř	uvnitř	k7c2	uvnitř
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
náročné	náročný	k2eAgNnSc1d1	náročné
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
ne	ne	k9	ne
dost	dost	k6eAd1	dost
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hry	hra	k1gFnSc2	hra
zde	zde	k6eAd1	zde
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
i	i	k9	i
snadné	snadný	k2eAgNnSc1d1	snadné
křížení	křížení	k1gNnSc1	křížení
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
složité	složitý	k2eAgFnSc2d1	složitá
hybridní	hybridní	k2eAgFnSc2d1	hybridní
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
vícenásobných	vícenásobný	k2eAgMnPc2d1	vícenásobný
kříženců	kříženec	k1gMnPc2	kříženec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
řazeno	řazen	k2eAgNnSc1d1	řazeno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
taxonů	taxon	k1gInPc2	taxon
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většina	většina	k1gFnSc1	většina
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
mezi	mezi	k7c7	mezi
odborníky	odborník	k1gMnPc7	odborník
není	být	k5eNaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
poddruh	poddruh	k1gInSc1	poddruh
nebo	nebo	k8xC	nebo
hybrid	hybrid	k1gInSc1	hybrid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
novějších	nový	k2eAgInPc2d2	novější
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
platných	platný	k2eAgInPc6d1	platný
poddruzích	poddruh	k1gInPc6	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
pravá	pravý	k2eAgFnSc1d1	pravá
(	(	kIx(	(
<g/>
Centaurea	Centaurea	k1gFnSc1	Centaurea
jacea	jacea	k1gMnSc1	jacea
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
jacea	jacea	k1gMnSc1	jacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
(	(	kIx(	(
<g/>
Centaurea	Centaurea	k1gFnSc1	Centaurea
jacea	jacea	k1gMnSc1	jacea
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
angustifolia	angustifolia	k1gFnSc1	angustifolia
<g/>
)	)	kIx)	)
Gremli	Greml	k1gMnPc1	Greml
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
starších	starý	k2eAgInPc2d2	starší
pramenů	pramen	k1gInPc2	pramen
byly	být	k5eAaImAgFnP	být
za	za	k7c7	za
poddruhy	poddruh	k1gInPc7	poddruh
chrpy	chrpa	k1gFnSc2	chrpa
luční	luční	k2eAgInPc1d1	luční
považovány	považován	k2eAgFnPc1d1	považována
ještě	ještě	k6eAd1	ještě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
ostroperá	ostroperý	k2eAgFnSc1d1	ostroperý
(	(	kIx(	(
<g/>
Centaurea	Centaurea	k1gFnSc1	Centaurea
jacea	jacea	k1gMnSc1	jacea
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
oxylepis	oxylepis	k1gFnSc1	oxylepis
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
W.	W.	kA	W.
et	et	k?	et
Gr	Gr	k1gMnSc1	Gr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Haye	Haye	k1gFnSc1	Haye
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hodnocená	hodnocený	k2eAgFnSc1d1	hodnocená
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
druh	druh	k1gInSc1	druh
chrpa	chrpa	k1gFnSc1	chrpa
ostroperá	ostroperat	k5eAaPmIp3nS	ostroperat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
hřebenitá	hřebenitý	k2eAgFnSc1d1	hřebenitá
(	(	kIx(	(
<g/>
Centaurea	Centaurea	k1gFnSc1	Centaurea
jacea	jacea	k1gMnSc1	jacea
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
subjacea	subjacea	k1gMnSc1	subjacea
<g/>
)	)	kIx)	)
u	u	k7c2	u
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgMnS	prokázat
hybridní	hybridní	k2eAgMnSc1d1	hybridní
původ	původ	k1gMnSc1	původ
(	(	kIx(	(
<g/>
kříženec	kříženec	k1gMnSc1	kříženec
Centaurea	Centaurea	k1gMnSc1	Centaurea
jacea	jacea	k1gMnSc1	jacea
×	×	k?	×
Centaurea	Centaurea	k1gFnSc1	Centaurea
macroptilon	macroptilon	k1gInSc1	macroptilon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
polní	polní	k2eAgFnSc2d1	polní
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
