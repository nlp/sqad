<s>
Chrpa	chrpa	k1gFnSc1	chrpa
luční	luční	k2eAgFnSc1d1	luční
(	(	kIx(	(
<g/>
Centaurea	Centaure	k2eAgFnSc1d1	Centaurea
jacea	jacea	k1gFnSc1	jacea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
chrpa	chrpa	k1gFnSc1	chrpa
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
