<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
řečený	řečený	k2eAgMnSc1d1	řečený
Král	Král	k1gMnSc1	Král
Slunce	slunce	k1gNnSc2	slunce
–	–	k?	–
Roi-Soleil	Roi-Soleil	k1gMnSc1	Roi-Soleil
<g/>
;	;	kIx,	;
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1638	[number]	k4	1638
Saint-Germain-en-Laye	Saint-Germainn-Lay	k1gInSc2	Saint-Germain-en-Lay
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1715	[number]	k4	1715
Versailles	Versailles	k1gFnSc4	Versailles
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
v	v	k7c6	v
letech	let	k1gInPc6	let
1643	[number]	k4	1643
<g/>
–	–	k?	–
<g/>
1715	[number]	k4	1715
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
Anna	Anna	k1gFnSc1	Anna
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
linie	linie	k1gFnSc2	linie
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
panoval	panovat	k5eAaImAgInS	panovat
plných	plný	k2eAgInPc2d1	plný
sedmdesát	sedmdesát	k4xCc4	sedmdesát
dva	dva	k4xCgInPc4	dva
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
již	již	k9	již
jako	jako	k9	jako
čtyřletý	čtyřletý	k2eAgMnSc1d1	čtyřletý
chlapec	chlapec	k1gMnSc1	chlapec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Korunován	korunovat	k5eAaBmNgInS	korunovat
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1654	[number]	k4	1654
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
choť	choť	k1gFnSc1	choť
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
starat	starat	k5eAaImF	starat
o	o	k7c4	o
výchovu	výchova	k1gFnSc4	výchova
obou	dva	k4xCgMnPc2	dva
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Ludvíka	Ludvík	k1gMnSc2	Ludvík
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Filipa	Filip	k1gMnSc2	Filip
Orléanského	Orléanský	k2eAgMnSc2d1	Orléanský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řízení	řízení	k1gNnSc1	řízení
státu	stát	k1gInSc2	stát
má	mít	k5eAaImIp3nS	mít
přenechat	přenechat	k5eAaPmF	přenechat
radě	rada	k1gFnSc6	rada
pěti	pět	k4xCc2	pět
jmenovaných	jmenovaný	k2eAgMnPc2d1	jmenovaný
hodnostářů	hodnostář	k1gMnPc2	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
však	však	k9	však
manželovu	manželův	k2eAgFnSc4d1	manželova
závěť	závěť	k1gFnSc4	závěť
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
a	a	k8xC	a
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
požádala	požádat	k5eAaPmAgFnS	požádat
parlament	parlament	k1gInSc4	parlament
i	i	k8xC	i
královskou	královský	k2eAgFnSc4d1	královská
radu	rada	k1gFnSc4	rada
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
stát	stát	k5eAaPmF	stát
regentkou	regentka	k1gFnSc7	regentka
s	s	k7c7	s
neomezenou	omezený	k2eNgFnSc7d1	neomezená
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
panování	panování	k1gNnSc2	panování
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
tak	tak	k9	tak
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vládla	vládnout	k5eAaImAgFnS	vládnout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
královna	královna	k1gFnSc1	královna
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
mocný	mocný	k2eAgMnSc1d1	mocný
první	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
Jules	Julesa	k1gFnPc2	Julesa
Mazarin	Mazarin	k1gInSc1	Mazarin
(	(	kIx(	(
<g/>
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Giulio	Giulio	k1gMnSc1	Giulio
Mazzarini	Mazzarin	k2eAgMnPc1d1	Mazzarin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nepotvrzené	potvrzený	k2eNgFnSc2d1	nepotvrzená
teorie	teorie	k1gFnSc2	teorie
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
milencem	milenec	k1gMnSc7	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Obratnému	obratný	k2eAgMnSc3d1	obratný
Mazarinovi	Mazarin	k1gMnSc3	Mazarin
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1652	[number]	k4	1652
<g/>
/	/	kIx~	/
<g/>
1653	[number]	k4	1653
podařilo	podařit	k5eAaPmAgNnS	podařit
potlačit	potlačit	k5eAaPmF	potlačit
šlechtickou	šlechtický	k2eAgFnSc4d1	šlechtická
opozici	opozice	k1gFnSc4	opozice
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
frondu	fronda	k1gFnSc4	fronda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
vážně	vážně	k6eAd1	vážně
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
celou	celý	k2eAgFnSc4d1	celá
regentskou	regentský	k2eAgFnSc4d1	regentská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Mazarin	Mazarin	k1gInSc1	Mazarin
také	také	k9	také
za	za	k7c4	za
Ludvíka	Ludvík	k1gMnSc4	Ludvík
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ukončil	ukončit	k5eAaPmAgMnS	ukončit
letitý	letitý	k2eAgInSc4d1	letitý
konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
králem	král	k1gMnSc7	král
Filipem	Filip	k1gMnSc7	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
bratrem	bratr	k1gMnSc7	bratr
regentky	regentka	k1gFnSc2	regentka
<g/>
.	.	kIx.	.
</s>
<s>
Mazarin	Mazarin	k1gInSc1	Mazarin
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
plně	plně	k6eAd1	plně
oddaný	oddaný	k2eAgMnSc1d1	oddaný
králi	král	k1gMnSc6	král
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
řídil	řídit	k5eAaImAgMnS	řídit
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
politickým	politický	k2eAgInSc7d1	politický
činem	čin	k1gInSc7	čin
bylo	být	k5eAaImAgNnS	být
uzavření	uzavření	k1gNnSc1	uzavření
sňatku	sňatek	k1gInSc2	sňatek
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Terezou	Tereza	k1gFnSc7	Tereza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
španělských	španělský	k2eAgMnPc2d1	španělský
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
Ludvíkovou	Ludvíkův	k2eAgFnSc7d1	Ludvíkova
sestřenicí	sestřenice	k1gFnSc7	sestřenice
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nevěsta	nevěsta	k1gFnSc1	nevěsta
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
mladého	mladý	k2eAgMnSc4d1	mladý
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
hledala	hledat	k5eAaImAgFnS	hledat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
do	do	k7c2	do
neteře	neteř	k1gFnSc2	neteř
kardinála	kardinál	k1gMnSc2	kardinál
Mazarina	Mazarino	k1gNnSc2	Mazarino
Marie	Maria	k1gFnSc2	Maria
Manciniové	Manciniový	k2eAgFnPc1d1	Manciniová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
dokonce	dokonce	k9	dokonce
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
mu	on	k3xPp3gInSc3	on
ovšem	ovšem	k9	ovšem
zabránili	zabránit	k5eAaPmAgMnP	zabránit
královna	královna	k1gFnSc1	královna
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
Mazarin	Mazarin	k1gInSc1	Mazarin
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
sestřenici	sestřenice	k1gFnSc4	sestřenice
Markétě	Markéta	k1gFnSc3	Markéta
Savojské	savojský	k2eAgFnSc3d1	Savojská
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
adeptkou	adeptka	k1gFnSc7	adeptka
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
sestřenice	sestřenice	k1gFnSc1	sestřenice
Anna	Anna	k1gFnSc1	Anna
Marie	Marie	k1gFnSc1	Marie
Louisa	Louisa	k?	Louisa
Orleánská	orleánský	k2eAgFnSc1d1	Orleánská
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Marií	Maria	k1gFnSc7	Maria
Terezou	Tereza	k1gFnSc7	Tereza
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
děti	dítě	k1gFnPc1	dítě
mu	on	k3xPp3gNnSc3	on
porodily	porodit	k5eAaPmAgFnP	porodit
jeho	jeho	k3xOp3gNnSc1	jeho
milenky	milenka	k1gFnPc1	milenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Louise	Louis	k1gMnSc2	Louis
de	de	k?	de
La	la	k1gNnSc1	la
Valliè	Valliè	k1gFnSc2	Valliè
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
Françoise	Françoise	k1gFnPc4	Françoise
Athénaï	Athénaï	k1gFnPc2	Athénaï
de	de	k?	de
Montespan	Montespana	k1gFnPc2	Montespana
sedm	sedm	k4xCc4	sedm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
choti	choť	k1gFnSc2	choť
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
Ludvík	Ludvík	k1gMnSc1	Ludvík
údajně	údajně	k6eAd1	údajně
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc1	jeden
manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přísně	přísně	k6eAd1	přísně
utajené	utajený	k2eAgNnSc1d1	utajené
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
zámku	zámek	k1gInSc2	zámek
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
vychovatelku	vychovatelka	k1gFnSc4	vychovatelka
svých	svůj	k3xOyFgFnPc2	svůj
nemanželských	manželský	k2eNgFnPc2d1	nemanželská
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
markýzu	markýza	k1gFnSc4	markýza
Françoise	Françoise	k1gFnSc2	Françoise
de	de	k?	de
Maintenon	Maintenon	k1gMnSc1	Maintenon
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc4	veškerý
písemné	písemný	k2eAgInPc4d1	písemný
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
společensky	společensky	k6eAd1	společensky
nerovném	rovný	k2eNgInSc6d1	nerovný
sňatku	sňatek	k1gInSc6	sňatek
byly	být	k5eAaImAgInP	být
už	už	k6eAd1	už
za	za	k7c2	za
Ludvíkova	Ludvíkův	k2eAgInSc2d1	Ludvíkův
života	život	k1gInSc2	život
zničeny	zničen	k2eAgInPc1d1	zničen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
přesvědčivých	přesvědčivý	k2eAgNnPc2d1	přesvědčivé
svědectví	svědectví	k1gNnPc2	svědectví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
paní	paní	k1gFnSc1	paní
de	de	k?	de
Maintenon	Maintenon	k1gNnSc1	Maintenon
byla	být	k5eAaImAgFnS	být
Ludvíkovou	Ludvíková	k1gFnSc4	Ludvíková
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
osobou	osoba	k1gFnSc7	osoba
a	a	k8xC	a
důvěrnou	důvěrný	k2eAgFnSc7d1	důvěrná
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
až	až	k8xS	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
kardinála	kardinál	k1gMnSc2	kardinál
Mazarina	Mazarino	k1gNnSc2	Mazarino
nechal	nechat	k5eAaPmAgMnS	nechat
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
úřad	úřad	k1gInSc1	úřad
prvního	první	k4xOgMnSc2	první
ministra	ministr	k1gMnSc2	ministr
neobsazený	obsazený	k2eNgMnSc1d1	neobsazený
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
převzal	převzít	k5eAaPmAgMnS	převzít
veškerá	veškerý	k3xTgNnPc4	veškerý
jeho	jeho	k3xOp3gNnPc4	jeho
práva	právo	k1gNnPc4	právo
i	i	k8xC	i
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
dal	dát	k5eAaPmAgMnS	dát
zatknout	zatknout	k5eAaPmF	zatknout
ministra	ministr	k1gMnSc4	ministr
financí	finance	k1gFnPc2	finance
Nicolase	Nicolasa	k1gFnSc6	Nicolasa
Fouqueta	Fouquet	k1gMnSc4	Fouquet
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Colberta	Colbert	k1gMnSc2	Colbert
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
snažil	snažit	k5eAaImAgMnS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
katastrofální	katastrofální	k2eAgInSc4d1	katastrofální
stav	stav	k1gInSc4	stav
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
politicky	politicky	k6eAd1	politicky
zcela	zcela	k6eAd1	zcela
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládnout	vládnout	k5eAaImF	vládnout
Francii	Francie	k1gFnSc4	Francie
není	být	k5eNaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
úkol	úkol	k1gInSc1	úkol
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mazarin	Mazarin	k1gInSc1	Mazarin
už	už	k6eAd1	už
před	před	k7c7	před
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
politickou	politický	k2eAgFnSc4d1	politická
opozici	opozice	k1gFnSc4	opozice
vůči	vůči	k7c3	vůči
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
existovala	existovat	k5eAaImAgFnS	existovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohly	moct	k5eAaImAgInP	moct
Ludvíkovu	Ludvíkův	k2eAgFnSc4d1	Ludvíkova
moc	moc	k1gFnSc4	moc
ohrozit	ohrozit	k5eAaPmF	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tu	tu	k6eAd1	tu
odbojná	odbojný	k2eAgNnPc4d1	odbojné
města	město	k1gNnPc4	město
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Orléans	Orléans	k1gInSc1	Orléans
a	a	k8xC	a
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sporů	spor	k1gInPc2	spor
často	často	k6eAd1	často
přikláněla	přiklánět	k5eAaImAgFnS	přiklánět
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
složitě	složitě	k6eAd1	složitě
vynutit	vynutit	k5eAaPmF	vynutit
loajalitu	loajalita	k1gFnSc4	loajalita
měst	město	k1gNnPc2	město
i	i	k9	i
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
zkrotit	zkrotit	k5eAaPmF	zkrotit
mocenské	mocenský	k2eAgFnPc4d1	mocenská
choutky	choutka	k1gFnPc4	choutka
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
rodině	rodina	k1gFnSc6	rodina
(	(	kIx(	(
<g/>
poměrně	poměrně	k6eAd1	poměrně
nedávného	dávný	k2eNgNnSc2d1	nedávné
povstání	povstání	k1gNnSc2	povstání
frondy	fronda	k1gFnSc2	fronda
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gMnPc2	jeho
příbuzných	příbuzný	k1gMnPc2	příbuzný
–	–	k?	–
např.	např.	kA	např.
strýc	strýc	k1gMnSc1	strýc
Gaston	Gaston	k1gInSc4	Gaston
Orléanský	Orléanský	k2eAgInSc4d1	Orléanský
a	a	k8xC	a
princové	princ	k1gMnPc1	princ
de	de	k?	de
Conti	Conti	k1gNnSc2	Conti
a	a	k8xC	a
de	de	k?	de
Condé	Condý	k2eAgInPc1d1	Condý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
často	často	k6eAd1	často
měnily	měnit	k5eAaImAgFnP	měnit
vlivem	vlivem	k7c2	vlivem
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
vůbec	vůbec	k9	vůbec
nemluvila	mluvit	k5eNaImAgFnS	mluvit
francouzštinou	francouzština	k1gFnSc7	francouzština
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
–	–	k?	–
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
převládala	převládat	k5eAaImAgFnS	převládat
italština	italština	k1gFnSc1	italština
a	a	k8xC	a
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
původně	původně	k6eAd1	původně
německých	německý	k2eAgFnPc2d1	německá
oblastí	oblast	k1gFnPc2	oblast
Alsasko	Alsasko	k1gNnSc1	Alsasko
a	a	k8xC	a
Lotrinsko	Lotrinsko	k1gNnSc1	Lotrinsko
také	také	k9	také
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
čítala	čítat	k5eAaImAgFnS	čítat
Francie	Francie	k1gFnSc1	Francie
stovky	stovka	k1gFnSc2	stovka
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
jednota	jednota	k1gFnSc1	jednota
země	zem	k1gFnSc2	zem
závisela	záviset	k5eAaImAgFnS	záviset
jen	jen	k9	jen
na	na	k7c4	na
vůli	vůle	k1gFnSc4	vůle
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
symbolem	symbol	k1gInSc7	symbol
jejího	její	k3xOp3gNnSc2	její
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkova	Ludvíkův	k2eAgFnSc1d1	Ludvíkova
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
posílení	posílení	k1gNnSc4	posílení
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
postavení	postavení	k1gNnSc2	postavení
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
postupně	postupně	k6eAd1	postupně
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
konflikt	konflikt	k1gInSc4	konflikt
snad	snad	k9	snad
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
mocnostmi	mocnost	k1gFnPc7	mocnost
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
zástěrkou	zástěrka	k1gFnSc7	zástěrka
tzv.	tzv.	kA	tzv.
reunií	reunie	k1gFnSc7	reunie
obsadila	obsadit	k5eAaPmAgFnS	obsadit
bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
vojska	vojsko	k1gNnSc2	vojsko
řadu	řada	k1gFnSc4	řada
držav	država	k1gFnPc2	država
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Alsasko	Alsasko	k1gNnSc1	Alsasko
včetně	včetně	k7c2	včetně
Štrasburku	Štrasburk	k1gInSc2	Štrasburk
<g/>
,	,	kIx,	,
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
zhruba	zhruba	k6eAd1	zhruba
svou	svůj	k3xOyFgFnSc4	svůj
dnešní	dnešní	k2eAgFnSc4d1	dnešní
rozlohu	rozloha	k1gFnSc4	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
válek	válka	k1gFnPc2	válka
s	s	k7c7	s
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Ludvíkova	Ludvíkův	k2eAgFnSc1d1	Ludvíkova
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
(	(	kIx(	(
<g/>
nakonec	nakonec	k6eAd1	nakonec
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
)	)	kIx)	)
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
svého	svůj	k3xOyFgMnSc2	svůj
vnuka	vnuk	k1gMnSc2	vnuk
Filipa	Filip	k1gMnSc2	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Absolutismus	absolutismus	k1gInSc1	absolutismus
<g/>
,	,	kIx,	,
ztělesněný	ztělesněný	k2eAgInSc1d1	ztělesněný
údajným	údajný	k2eAgInSc7d1	údajný
Ludvíkovým	Ludvíkův	k2eAgInSc7d1	Ludvíkův
výrokem	výrok	k1gInSc7	výrok
"	"	kIx"	"
<g/>
Stát	stát	k1gInSc1	stát
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
état	état	k1gMnSc1	état
c	c	k0	c
<g/>
'	'	kIx"	'
<g/>
est	est	k?	est
moi	moi	k?	moi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
oblasti	oblast	k1gFnSc6	oblast
podporoval	podporovat	k5eAaImAgMnS	podporovat
král	král	k1gMnSc1	král
rozvoj	rozvoj	k1gInSc4	rozvoj
manufaktur	manufaktura	k1gFnPc2	manufaktura
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
docílit	docílit	k5eAaPmF	docílit
aktivní	aktivní	k2eAgFnSc3d1	aktivní
bilanci	bilance	k1gFnSc3	bilance
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Opíral	opírat	k5eAaImAgMnS	opírat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
myšlenky	myšlenka	k1gFnPc4	myšlenka
již	již	k6eAd1	již
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
Jeana-Baptista	Jeana-Baptista	k1gMnSc1	Jeana-Baptista
Colberta	Colberta	k1gFnSc1	Colberta
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc1	název
merkantilismus	merkantilismus	k1gInSc1	merkantilismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
Francie	Francie	k1gFnSc2	Francie
získávala	získávat	k5eAaImAgFnS	získávat
nové	nový	k2eAgFnPc4d1	nová
kolonie	kolonie	k1gFnPc4	kolonie
a	a	k8xC	a
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
území	území	k1gNnPc4	území
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Francii	Francie	k1gFnSc6	Francie
i	i	k8xC	i
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkova	Ludvíkův	k2eAgFnSc1d1	Ludvíkova
náboženská	náboženský	k2eAgFnSc1d1	náboženská
politika	politika	k1gFnSc1	politika
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nezávislosti	nezávislost	k1gFnSc2	nezávislost
francouzské	francouzský	k2eAgFnSc2d1	francouzská
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
papeži	papež	k1gMnSc6	papež
a	a	k8xC	a
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
náboženských	náboženský	k2eAgFnPc2d1	náboženská
záležitostí	záležitost	k1gFnPc2	záležitost
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vydáním	vydání	k1gNnSc7	vydání
ediktu	edikt	k1gInSc2	edikt
z	z	k7c2	z
Fontainebleau	Fontainebleaus	k1gInSc2	Fontainebleaus
zrušil	zrušit	k5eAaPmAgMnS	zrušit
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
edikt	edikt	k1gInSc1	edikt
nantský	nantský	k2eAgInSc1d1	nantský
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
jeho	jeho	k3xOp3gNnSc1	jeho
děd	děd	k1gMnSc1	děd
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
povolil	povolit	k5eAaPmAgMnS	povolit
hugenotům	hugenot	k1gMnPc3	hugenot
vyznávat	vyznávat	k5eAaImF	vyznávat
jejich	jejich	k3xOp3gFnSc4	jejich
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
exodus	exodus	k1gInSc4	exodus
mnoha	mnoho	k4c2	mnoho
hugenotů	hugenot	k1gMnPc2	hugenot
zejména	zejména	k9	zejména
do	do	k7c2	do
protestantských	protestantský	k2eAgFnPc2d1	protestantská
oblastí	oblast	k1gFnPc2	oblast
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
vojenství	vojenství	k1gNnSc6	vojenství
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
Ludvík	Ludvík	k1gMnSc1	Ludvík
dokázal	dokázat	k5eAaPmAgMnS	dokázat
nalézt	nalézt	k5eAaPmF	nalézt
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
učinily	učinit	k5eAaPmAgFnP	učinit
nejmodernější	moderní	k2eAgInSc4d3	nejmodernější
útvar	útvar	k1gInSc4	útvar
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
zavedení	zavedení	k1gNnSc1	zavedení
uniforem	uniforma	k1gFnPc2	uniforma
a	a	k8xC	a
díky	díky	k7c3	díky
osobě	osoba	k1gFnSc3	osoba
maršála	maršál	k1gMnSc2	maršál
Sébastiena	Sébastien	k1gMnSc2	Sébastien
Vaubana	Vauban	k1gMnSc2	Vauban
nových	nový	k2eAgInPc2d1	nový
způsobů	způsob	k1gInPc2	způsob
fortifikace	fortifikace	k1gFnSc2	fortifikace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníky	důstojník	k1gMnPc4	důstojník
odměňoval	odměňovat	k5eAaImAgMnS	odměňovat
král	král	k1gMnSc1	král
jím	on	k3xPp3gMnSc7	on
zřízeným	zřízený	k2eAgInSc7d1	zřízený
Řádem	řád	k1gInSc7	řád
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc2d1	velká
podpory	podpora	k1gFnSc2	podpora
se	se	k3xPyFc4	se
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
dostávalo	dostávat	k5eAaImAgNnS	dostávat
také	také	k6eAd1	také
vědě	věda	k1gFnSc3	věda
–	–	k?	–
kartografii	kartografie	k1gFnSc3	kartografie
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc3	astronomie
<g/>
,	,	kIx,	,
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byly	být	k5eAaImAgFnP	být
francouzskými	francouzský	k2eAgNnPc7d1	francouzské
vědci	vědec	k1gMnSc6	vědec
objeveny	objevit	k5eAaPmNgInP	objevit
čtyři	čtyři	k4xCgInPc1	čtyři
měsíce	měsíc	k1gInPc1	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
spočítána	spočítán	k2eAgFnSc1d1	spočítána
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
první	první	k4xOgInSc1	první
systém	systém	k1gInSc1	systém
rozdělení	rozdělení	k1gNnSc2	rozdělení
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
vynalezena	vynalezen	k2eAgFnSc1d1	vynalezena
technika	technika	k1gFnSc1	technika
odlévání	odlévání	k1gNnSc2	odlévání
velkých	velký	k2eAgNnPc2d1	velké
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
se	se	k3xPyFc4	se
mnohé	mnohý	k2eAgInPc1d1	mnohý
další	další	k2eAgInPc1d1	další
význačné	význačný	k2eAgInPc1d1	význačný
objevy	objev	k1gInPc1	objev
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
aktivně	aktivně	k6eAd1	aktivně
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
činnost	činnost	k1gFnSc4	činnost
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
sféře	sféra	k1gFnSc6	sféra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
vláda	vláda	k1gFnSc1	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
"	"	kIx"	"
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
dvorského	dvorský	k2eAgInSc2d1	dvorský
života	život	k1gInSc2	život
i	i	k9	i
písemnictví	písemnictví	k1gNnPc4	písemnictví
přední	přední	k2eAgNnPc4d1	přední
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
"	"	kIx"	"
<g/>
galantní	galantní	k2eAgMnSc1d1	galantní
kavalír	kavalír	k1gMnSc1	kavalír
<g/>
"	"	kIx"	"
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
definitivně	definitivně	k6eAd1	definitivně
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
z	z	k7c2	z
obecného	obecný	k2eAgNnSc2d1	obecné
povědomí	povědomí	k1gNnSc2	povědomí
španělského	španělský	k2eAgNnSc2d1	španělské
"	"	kIx"	"
<g/>
granda	grand	k1gMnSc2	grand
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
Versailles	Versailles	k1gFnSc2	Versailles
jako	jako	k8xS	jako
nové	nový	k2eAgFnSc2d1	nová
rezidence	rezidence	k1gFnSc2	rezidence
absolutního	absolutní	k2eAgMnSc2d1	absolutní
monarchy	monarcha	k1gMnSc2	monarcha
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
evropské	evropský	k2eAgInPc4d1	evropský
panovnické	panovnický	k2eAgInPc4d1	panovnický
dvory	dvůr	k1gInPc4	dvůr
<g/>
;	;	kIx,	;
francouzština	francouzština	k1gFnSc1	francouzština
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
řečí	řeč	k1gFnSc7	řeč
urozených	urozený	k2eAgInPc2d1	urozený
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
,	,	kIx,	,
distingovaný	distingovaný	k2eAgMnSc1d1	distingovaný
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
vznešené	vznešený	k2eAgNnSc4d1	vznešené
vystupování	vystupování	k1gNnSc4	vystupování
a	a	k8xC	a
kultivovaný	kultivovaný	k2eAgInSc4d1	kultivovaný
projev	projev	k1gInSc4	projev
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgMnS	vžít
do	do	k7c2	do
ceremoniální	ceremoniální	k2eAgFnSc2d1	ceremoniální
role	role	k1gFnSc2	role
barokního	barokní	k2eAgMnSc2d1	barokní
vládce	vládce	k1gMnSc2	vládce
a	a	k8xC	a
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
byl	být	k5eAaImAgMnS	být
i	i	k9	i
jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
milovníkem	milovník	k1gMnSc7	milovník
baletu	balet	k1gInSc2	balet
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
baletu	balet	k1gInSc3	balet
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
intenzivně	intenzivně	k6eAd1	intenzivně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
tančil	tančit	k5eAaImAgInS	tančit
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
velkolepém	velkolepý	k2eAgInSc6d1	velkolepý
Baletu	balet	k1gInSc6	balet
Noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
Ballet	Ballet	k1gInSc1	Ballet
de	de	k?	de
la	la	k1gNnSc7	la
Nuit	Nuita	k1gFnPc2	Nuita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
hral	hral	k1gInSc4	hral
boha	bůh	k1gMnSc2	bůh
Apollóna	Apollón	k1gMnSc2	Apollón
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
roli	role	k1gFnSc3	role
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tančil	tančit	k5eAaImAgInS	tančit
ve	v	k7c6	v
slunečním	sluneční	k2eAgInSc6d1	sluneční
kostýmu	kostým	k1gInSc6	kostým
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
přízvisko	přízvisko	k1gNnSc4	přízvisko
Král	Král	k1gMnSc1	Král
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgInS	podporovat
mnohé	mnohý	k2eAgMnPc4d1	mnohý
spisovatele	spisovatel	k1gMnPc4	spisovatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jeana	Jean	k1gMnSc2	Jean
Racina	Racin	k1gMnSc2	Racin
nebo	nebo	k8xC	nebo
Pierra	Pierr	k1gMnSc2	Pierr
Corneille	Corneill	k1gMnSc2	Corneill
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíře	malíř	k1gMnSc4	malíř
<g/>
,	,	kIx,	,
sochaře	sochař	k1gMnPc4	sochař
a	a	k8xC	a
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
dvorním	dvorní	k2eAgMnSc7d1	dvorní
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
a	a	k8xC	a
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
byl	být	k5eAaImAgInS	být
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gMnPc7	Lull
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
tvůrce	tvůrce	k1gMnSc1	tvůrce
francouzské	francouzský	k2eAgFnSc2d1	francouzská
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
tragédie	tragédie	k1gFnSc2	tragédie
lyrique	lyriqu	k1gInSc2	lyriqu
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
spisovatel	spisovatel	k1gMnSc1	spisovatel
Moliè	Moliè	k1gMnSc1	Moliè
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
synovi	syn	k1gMnSc3	syn
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
kmotra	kmotr	k1gMnSc4	kmotr
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
Lullyho	Lully	k1gMnSc4	Lully
synovi	syn	k1gMnSc6	syn
Louisovi	Louis	k1gMnSc6	Louis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odvrácenou	odvrácený	k2eAgFnSc7d1	odvrácená
stranou	strana	k1gFnSc7	strana
Ludvíkovy	Ludvíkův	k2eAgFnSc2d1	Ludvíkova
politiky	politika	k1gFnSc2	politika
byl	být	k5eAaImAgInS	být
takřka	takřka	k6eAd1	takřka
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
řetěz	řetěz	k1gInSc4	řetěz
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
obrovské	obrovský	k2eAgInPc4d1	obrovský
finanční	finanční	k2eAgInPc4d1	finanční
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
státní	státní	k2eAgFnSc4d1	státní
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
provoz	provoz	k1gInSc1	provoz
Versailles	Versailles	k1gFnSc2	Versailles
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nepříliš	příliš	k6eNd1	příliš
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
válkou	válka	k1gFnSc7	válka
devítiletou	devítiletý	k2eAgFnSc7d1	devítiletá
a	a	k8xC	a
válkou	válka	k1gFnSc7	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgNnSc1d1	španělské
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
pauzou	pauza	k1gFnSc7	pauza
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1688	[number]	k4	1688
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
těžce	těžce	k6eAd1	těžce
vykoupené	vykoupený	k2eAgInPc4d1	vykoupený
remízy	remíz	k1gInPc4	remíz
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
králův	králův	k2eAgMnSc1d1	králův
vnuk	vnuk	k1gMnSc1	vnuk
Filip	Filip	k1gMnSc1	Filip
stal	stát	k5eAaPmAgMnS	stát
španělským	španělský	k2eAgMnSc7d1	španělský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
nikdy	nikdy	k6eAd1	nikdy
nespojí	spojit	k5eNaPmIp3nS	spojit
pod	pod	k7c7	pod
jedním	jeden	k4xCgMnSc7	jeden
panovníkem	panovník	k1gMnSc7	panovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatížily	zatížit	k5eAaPmAgFnP	zatížit
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
na	na	k7c4	na
neúnosnou	únosný	k2eNgFnSc4d1	neúnosná
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
zadlužování	zadlužování	k1gNnSc3	zadlužování
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
důsledky	důsledek	k1gInPc1	důsledek
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
výši	výše	k1gFnSc6	výše
pocítili	pocítit	k5eAaPmAgMnP	pocítit
až	až	k9	až
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1	Ludvíkův
nástupci	nástupce	k1gMnPc1	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Sporným	sporný	k2eAgInSc7d1	sporný
bodem	bod	k1gInSc7	bod
Ludvíkovy	Ludvíkův	k2eAgFnSc2d1	Ludvíkova
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
zrušení	zrušení	k1gNnSc1	zrušení
ediktu	edikt	k1gInSc2	edikt
nantského	nantský	k2eAgInSc2d1	nantský
<g/>
,	,	kIx,	,
zavedeného	zavedený	k2eAgInSc2d1	zavedený
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
Jindřichem	Jindřich	k1gMnSc7	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zaručoval	zaručovat	k5eAaImAgMnS	zaručovat
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Francie	Francie	k1gFnSc2	Francie
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
krále	král	k1gMnSc2	král
šlo	jít	k5eAaImAgNnS	jít
nicméně	nicméně	k8xC	nicméně
o	o	k7c4	o
logický	logický	k2eAgInSc4d1	logický
krok	krok	k1gInSc4	krok
<g/>
:	:	kIx,	:
tak	tak	k9	tak
jako	jako	k9	jako
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
politické	politický	k2eAgNnSc4d1	politické
sjednocení	sjednocení	k1gNnSc4	sjednocení
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnPc3	jeho
přáním	přání	k1gNnPc3	přání
i	i	k8xC	i
jednota	jednota	k1gFnSc1	jednota
náboženská	náboženský	k2eAgFnSc1d1	náboženská
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
obrátit	obrátit	k5eAaPmF	obrátit
téměř	téměř	k6eAd1	téměř
milion	milion	k4xCgInSc1	milion
protestantů	protestant	k1gMnPc2	protestant
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
domlouváním	domlouvání	k1gNnSc7	domlouvání
<g/>
,	,	kIx,	,
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
misionářů	misionář	k1gMnPc2	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
zastrašování	zastrašování	k1gNnSc3	zastrašování
a	a	k8xC	a
výhrůžkám	výhrůžka	k1gFnPc3	výhrůžka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
se	se	k3xPyFc4	se
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
–	–	k?	–
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
1685	[number]	k4	1685
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
protestantů	protestant	k1gMnPc2	protestant
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc4	čtvrtina
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1685	[number]	k4	1685
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
zrušil	zrušit	k5eAaPmAgInS	zrušit
edikt	edikt	k1gInSc1	edikt
nantský	nantský	k2eAgInSc1d1	nantský
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nařizoval	nařizovat	k5eAaImAgInS	nařizovat
zbořit	zbořit	k5eAaPmF	zbořit
protestantské	protestantský	k2eAgInPc4d1	protestantský
kostely	kostel	k1gInPc4	kostel
<g/>
,	,	kIx,	,
uzavřít	uzavřít	k5eAaPmF	uzavřít
protestantské	protestantský	k2eAgFnPc4d1	protestantská
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
děti	dítě	k1gFnPc1	dítě
narozené	narozený	k2eAgFnPc1d1	narozená
v	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
rodinách	rodina	k1gFnPc6	rodina
pokřtít	pokřtít	k5eAaPmF	pokřtít
jako	jako	k9	jako
katolíky	katolík	k1gMnPc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
opustilo	opustit	k5eAaPmAgNnS	opustit
Francii	Francie	k1gFnSc4	Francie
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nalezli	nalézt	k5eAaBmAgMnP	nalézt
azyl	azyl	k1gInSc4	azyl
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
protestantských	protestantský	k2eAgFnPc6d1	protestantská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Odešlo	odejít	k5eAaPmAgNnS	odejít
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
finančníků	finančník	k1gMnPc2	finančník
<g/>
,	,	kIx,	,
podnikatelů	podnikatel	k1gMnPc2	podnikatel
i	i	k8xC	i
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
výrobci	výrobce	k1gMnPc1	výrobce
hedvábí	hedvábí	k1gNnSc2	hedvábí
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
,	,	kIx,	,
skláři	sklář	k1gMnPc1	sklář
do	do	k7c2	do
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
šest	šest	k4xCc4	šest
set	sto	k4xCgNnPc2	sto
důstojníků	důstojník	k1gMnPc2	důstojník
nalezlo	naleznout	k5eAaPmAgNnS	naleznout
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povstání	povstání	k1gNnSc6	povstání
tzv.	tzv.	kA	tzv.
camisardů	camisard	k1gMnPc2	camisard
v	v	k7c6	v
převážně	převážně	k6eAd1	převážně
protestantském	protestantský	k2eAgInSc6d1	protestantský
regionu	region	k1gInSc6	region
Cevenny	Cevenna	k1gFnSc2	Cevenna
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1702	[number]	k4	1702
až	až	k9	až
1715	[number]	k4	1715
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masakrům	masakr	k1gInPc3	masakr
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
protestanty	protestant	k1gMnPc7	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
komplikovanou	komplikovaný	k2eAgFnSc4d1	komplikovaná
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
také	také	k9	také
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
válku	válka	k1gFnSc4	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Francii	Francie	k1gFnSc4	Francie
finančně	finančně	k6eAd1	finančně
i	i	k9	i
morálně	morálně	k6eAd1	morálně
velmi	velmi	k6eAd1	velmi
zdevastovala	zdevastovat	k5eAaPmAgFnS	zdevastovat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
katolíka	katolík	k1gMnSc2	katolík
Jakuba	Jakub	k1gMnSc2	Jakub
Františka	František	k1gMnSc2	František
Stuarta	Stuart	k1gMnSc2	Stuart
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
sesazeného	sesazený	k2eAgMnSc4d1	sesazený
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
bratrancem	bratranec	k1gMnSc7	bratranec
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ranou	rána	k1gFnSc7	rána
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
silné	silný	k2eAgInPc1d1	silný
mrazy	mráz	k1gInPc1	mráz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1709	[number]	k4	1709
a	a	k8xC	a
následná	následný	k2eAgNnPc1d1	následné
léta	léto	k1gNnPc1	léto
neúrody	neúroda	k1gFnSc2	neúroda
a	a	k8xC	a
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
začal	začít	k5eAaPmAgInS	začít
propadat	propadat	k5eAaImF	propadat
depresím	deprese	k1gFnPc3	deprese
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
obávalo	obávat	k5eAaImAgNnS	obávat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Utrpení	utrpení	k1gNnSc4	utrpení
svých	svůj	k3xOyFgMnPc2	svůj
poddaných	poddaný	k1gMnPc2	poddaný
nesl	nést	k5eAaImAgInS	nést
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
penězi	peníze	k1gInPc7	peníze
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
řešit	řešit	k5eAaImF	řešit
vážnou	vážný	k2eAgFnSc4d1	vážná
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
nebyl	být	k5eNaImAgMnS	být
tento	tento	k3xDgMnSc1	tento
mocný	mocný	k2eAgMnSc1d1	mocný
panovník	panovník	k1gMnSc1	panovník
ušetřen	ušetřen	k2eAgMnSc1d1	ušetřen
ani	ani	k8xC	ani
tragédií	tragédie	k1gFnSc7	tragédie
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
rodině	rodina	k1gFnSc6	rodina
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1711	[number]	k4	1711
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
jediný	jediný	k2eAgMnSc1d1	jediný
legitimní	legitimní	k2eAgMnSc1d1	legitimní
syn	syn	k1gMnSc1	syn
dauphin	dauphin	k1gMnSc1	dauphin
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Následníkem	následník	k1gMnSc7	následník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vnuk	vnuk	k1gMnSc1	vnuk
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
burgundský	burgundský	k2eAgMnSc1d1	burgundský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1712	[number]	k4	1712
na	na	k7c4	na
záhadnou	záhadný	k2eAgFnSc4d1	záhadná
nemoc	nemoc	k1gFnSc4	nemoc
(	(	kIx(	(
<g/>
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
otravě	otrava	k1gFnSc6	otrava
jedem	jed	k1gInSc7	jed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgInS	zemřít
i	i	k9	i
třetí	třetí	k4xOgMnSc1	třetí
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
pětiletý	pětiletý	k2eAgMnSc1d1	pětiletý
pravnuk	pravnuk	k1gMnSc1	pravnuk
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
bretaňský	bretaňský	k2eAgMnSc1d1	bretaňský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
děsivých	děsivý	k2eAgFnPc6d1	děsivá
událostech	událost	k1gFnPc6	událost
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
upnul	upnout	k5eAaPmAgMnS	upnout
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
–	–	k?	–
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
oblékat	oblékat	k5eAaImF	oblékat
velmi	velmi	k6eAd1	velmi
prostě	prostě	k6eAd1	prostě
<g/>
,	,	kIx,	,
jedl	jíst	k5eAaImAgMnS	jíst
střídmě	střídmě	k6eAd1	střídmě
a	a	k8xC	a
radikálně	radikálně	k6eAd1	radikálně
omezil	omezit	k5eAaPmAgInS	omezit
své	svůj	k3xOyFgInPc4	svůj
osobní	osobní	k2eAgInPc4d1	osobní
výdaje	výdaj	k1gInPc4	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Zastavil	zastavit	k5eAaPmAgMnS	zastavit
rovněž	rovněž	k9	rovněž
výstavbu	výstavba	k1gFnSc4	výstavba
dalších	další	k2eAgInPc2d1	další
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
demobilizoval	demobilizovat	k5eAaBmAgMnS	demobilizovat
většinu	většina	k1gFnSc4	většina
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zvýšit	zvýšit	k5eAaPmF	zvýšit
hodnotu	hodnota	k1gFnSc4	hodnota
upadající	upadající	k2eAgFnSc2d1	upadající
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
se	se	k3xPyFc4	se
válečné	válečný	k2eAgNnSc1d1	válečné
štěstí	štěstí	k1gNnSc1	štěstí
opět	opět	k6eAd1	opět
pomalu	pomalu	k6eAd1	pomalu
obracelo	obracet	k5eAaImAgNnS	obracet
čelem	čelo	k1gNnSc7	čelo
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Ludvíkův	Ludvíkův	k2eAgMnSc1d1	Ludvíkův
vnuk	vnuk	k1gMnSc1	vnuk
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgInS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
již	již	k6eAd1	již
smířil	smířit	k5eAaPmAgInS	smířit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
osudem	osud	k1gInSc7	osud
i	i	k8xC	i
postupujícím	postupující	k2eAgNnSc7d1	postupující
stářím	stáří	k1gNnSc7	stáří
a	a	k8xC	a
užíval	užívat	k5eAaImAgInS	užívat
si	se	k3xPyFc3	se
poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
života	život	k1gInSc2	život
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
–	–	k?	–
střídavě	střídavě	k6eAd1	střídavě
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
a	a	k8xC	a
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
druhém	druhý	k4xOgInSc6	druhý
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Marly-le-Roi	Marlye-Ro	k1gFnSc6	Marly-le-Ro
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1715	[number]	k4	1715
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
na	na	k7c4	na
následky	následek	k1gInPc4	následek
gangrény	gangréna	k1gFnSc2	gangréna
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgMnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1715	[number]	k4	1715
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Terezou	Tereza	k1gFnSc7	Tereza
Španělskou	španělský	k2eAgFnSc7d1	španělská
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
Velký	velký	k2eAgMnSc1d1	velký
Dauphin	dauphin	k1gMnSc1	dauphin
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
–	–	k?	–
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1662	[number]	k4	1662
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Tereza	Tereza	k1gFnSc1	Tereza
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1667	[number]	k4	1667
<g/>
–	–	k?	–
<g/>
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
–	–	k?	–
<g/>
1671	[number]	k4	1671
<g/>
)	)	kIx)	)
Ludvík	Ludvík	k1gMnSc1	Ludvík
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
Nemanželské	manželský	k2eNgFnPc1d1	nemanželská
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
uznal	uznat	k5eAaPmAgInS	uznat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
:	:	kIx,	:
Ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Luisou	Luisa	k1gFnSc7	Luisa
de	de	k?	de
La	la	k0	la
Baume	Baum	k1gInSc5	Baum
Le	Le	k1gFnPc7	Le
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
,	,	kIx,	,
vévodkyní	vévodkyně	k1gFnPc2	vévodkyně
de	de	k?	de
La	la	k1gNnSc4	la
Valliére	Valliér	k1gMnSc5	Valliér
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
de	de	k?	de
Conti	Conť	k1gFnSc2	Conť
(	(	kIx(	(
<g/>
1666	[number]	k4	1666
<g/>
–	–	k?	–
<g/>
1739	[number]	k4	1739
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Ludvíka	Ludvík	k1gMnSc4	Ludvík
I.	I.	kA	I.
Armanda	Armanda	k1gFnSc1	Armanda
de	de	k?	de
Conti	Conť	k1gFnSc2	Conť
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
1667	[number]	k4	1667
<g/>
–	–	k?	–
<g/>
1683	[number]	k4	1683
<g/>
)	)	kIx)	)
Ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Františkou	Františka	k1gFnSc7	Františka
Athénou	Athéna	k1gFnSc7	Athéna
de	de	k?	de
Rochechouart	Rochechouarta	k1gFnPc2	Rochechouarta
de	de	k?	de
Mortemart	Mortemarta	k1gFnPc2	Mortemarta
<g/>
,	,	kIx,	,
markýzou	markýza	k1gFnSc7	markýza
de	de	k?	de
Montespan	Montespan	k1gMnSc1	Montespan
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Maine	Main	k1gInSc5	Main
(	(	kIx(	(
<g/>
1670	[number]	k4	1670
<g/>
–	–	k?	–
<g/>
1736	[number]	k4	1736
<g/>
)	)	kIx)	)
Ludvík	Ludvík	k1gMnSc1	Ludvík
César	César	k1gMnSc1	César
(	(	kIx(	(
<g/>
1672	[number]	k4	1672
<g/>
–	–	k?	–
<g/>
1683	[number]	k4	1683
<g/>
)	)	kIx)	)
Luisa	Luisa	k1gFnSc1	Luisa
Františka	Františka	k1gFnSc1	Františka
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
(	(	kIx(	(
<g/>
1673	[number]	k4	1673
<g/>
–	–	k?	–
<g/>
1743	[number]	k4	1743
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Ludvíka	Ludvík	k1gMnSc4	Ludvík
III	III	kA	III
<g/>
.	.	kIx.	.
de	de	k?	de
Condé	Condý	k2eAgFnSc2d1	Condý
<g/>
.	.	kIx.	.
</s>
<s>
Luisa	Luisa	k1gFnSc1	Luisa
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1674	[number]	k4	1674
<g/>
–	–	k?	–
<g/>
1681	[number]	k4	1681
<g/>
)	)	kIx)	)
Františka	Františka	k1gFnSc1	Františka
Marie	Maria	k1gFnSc2	Maria
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
(	(	kIx(	(
<g/>
1677	[number]	k4	1677
<g/>
–	–	k?	–
<g/>
1749	[number]	k4	1749
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Orleánského	orleánský	k2eAgInSc2d1	orleánský
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
(	(	kIx(	(
<g/>
1678	[number]	k4	1678
<g/>
–	–	k?	–
<g/>
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
románu	román	k1gInSc6	román
od	od	k7c2	od
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dumase	Dumas	k1gInSc6	Dumas
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
ještě	ještě	k6eAd1	ještě
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Louisy	louis	k1gInPc1	louis
de	de	k?	de
la	la	k1gNnSc3	la
Valliè	Valliè	k1gFnSc2	Valliè
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
unesen	unést	k5eAaPmNgMnS	unést
a	a	k8xC	a
vyměněn	vyměnit	k5eAaPmNgMnS	vyměnit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
dvojče	dvojče	k1gNnSc4	dvojče
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
však	však	k9	však
ztroskotá	ztroskotat	k5eAaPmIp3nS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
knižní	knižní	k2eAgFnSc6d1	knižní
sérii	série	k1gFnSc6	série
Angelika	Angelika	k1gFnSc1	Angelika
<g/>
,	,	kIx,	,
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Anne	Ann	k1gFnSc2	Ann
Golonové	Golonová	k1gFnSc2	Golonová
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
romantický	romantický	k2eAgInSc4d1	romantický
příběh	příběh	k1gInSc4	příběh
mapující	mapující	k2eAgInPc4d1	mapující
důležité	důležitý	k2eAgInPc4d1	důležitý
historické	historický	k2eAgInPc4d1	historický
milníky	milník	k1gInPc4	milník
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
