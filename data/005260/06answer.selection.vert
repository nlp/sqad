<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
řečený	řečený	k2eAgMnSc1d1	řečený
Král	Král	k1gMnSc1	Král
Slunce	slunce	k1gNnSc2	slunce
–	–	k?	–
Roi-Soleil	Roi-Soleil	k1gMnSc1	Roi-Soleil
<g/>
;	;	kIx,	;
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1638	[number]	k4	1638
Saint-Germain-en-Laye	Saint-Germainn-Lay	k1gInSc2	Saint-Germain-en-Lay
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1715	[number]	k4	1715
Versailles	Versailles	k1gFnSc4	Versailles
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
v	v	k7c6	v
letech	let	k1gInPc6	let
1643	[number]	k4	1643
<g/>
–	–	k?	–
<g/>
1715	[number]	k4	1715
<g/>
.	.	kIx.	.
</s>
