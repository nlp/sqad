<s>
Podhradské	podhradský	k2eAgFnPc1d1
skály	skála	k1gFnPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
rezervacePodhradské	rezervacePodhradský	k2eAgFnPc4d1
skályIUCN	skályIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
výhled	výhled	k1gInSc1
z	z	k7c2
hradu	hrad	k1gInSc2
Frejštejna	Frejštejn	k1gInSc2
na	na	k7c6
Podhradí	Podhradí	k1gNnSc6
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
a	a	k8xC
PR	pr	k0
Podhradské	podhradský	k2eAgFnPc1d1
skályZákladní	skályZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
22.1	22.1	k4
<g/>
.1998	.1998	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
350	#num#	k4
-	-	kIx~
447	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
11,85	11,85	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Korolupy	Korolupa	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
15,48	15,48	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
13,92	13,92	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Podhradské	podhradský	k2eAgFnPc1d1
skály	skála	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
1948	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Podhradské	podhradský	k2eAgFnPc4d1
skály	skála	k1gFnPc4
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
ev.	ev.	k?
č.	č.	k?
1948	#num#	k4
poblíž	poblíž	k7c2
obce	obec	k1gFnSc2
Korolupy	Korolupa	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Znojmo	Znojmo	k1gNnSc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
350	#num#	k4
<g/>
–	–	k?
<g/>
447	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
spravuje	spravovat	k5eAaImIp3nS
AOPK	AOPK	kA
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
kolmé	kolmý	k2eAgFnPc1d1
<g/>
,	,	kIx,
výslunné	výslunný	k2eAgFnPc1d1
skalní	skalní	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
a	a	k8xC
strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
nad	nad	k7c7
levým	levý	k2eAgInSc7d1
břehem	břeh	k1gInSc7
Dyje	Dyje	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
významná	významný	k2eAgNnPc4d1
rostlinná	rostlinný	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Znojmo	Znojmo	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Podhradské	podhradský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Znojmo	Znojmo	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
parky	park	k1gInPc1
</s>
<s>
Podyjí	Podyjí	k1gNnSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Jevišovka	Jevišovka	k1gFnSc1
•	•	k?
Rokytná	Rokytný	k2eAgFnSc1d1
•	•	k?
Střední	střední	k2eAgFnSc1d1
Pojihlaví	Pojihlavý	k2eAgMnPc1d1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Krumlovsko-rokytenské	krumlovsko-rokytenský	k2eAgInPc1d1
slepence	slepenec	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Miroslavské	Miroslavský	k2eAgInPc4d1
kopce	kopec	k1gInPc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
kříž	kříž	k1gInSc1
•	•	k?
Cornštejn	Cornštejn	k1gInSc1
•	•	k?
Karlov	Karlov	k1gInSc1
•	•	k?
Na	na	k7c6
Bítovské	Bítovský	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
•	•	k?
Na	na	k7c6
Kocourkách	Kocourkách	k?
•	•	k?
Pod	pod	k7c7
Havranem	Havran	k1gMnSc7
•	•	k?
Podhradské	podhradský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Růžový	růžový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Templštejn	Templštejn	k1gInSc1
•	•	k?
Tisová	tisový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
U	u	k7c2
doutné	doutný	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bau	Bau	k?
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
u	u	k7c2
Jamolic	Jamolice	k1gFnPc2
•	•	k?
Božický	Božický	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
•	•	k?
Cínová	cínový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Čekal	čekat	k5eAaImAgMnS
•	•	k?
Černice	černice	k1gFnPc4
•	•	k?
Červený	červený	k2eAgInSc4d1
rybníček	rybníček	k1gInSc4
•	•	k?
Dyjské	dyjský	k2eAgInPc4d1
svahy	svah	k1gInPc4
•	•	k?
Fládnitzské	Fládnitzský	k2eAgNnSc4d1
vřesoviště	vřesoviště	k1gNnSc4
•	•	k?
Hevlínské	Hevlínský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Horáčkův	Horáčkův	k2eAgInSc1d1
kopeček	kopeček	k1gInSc1
•	•	k?
Horecký	Horecký	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
Karlov	Karlov	k1gInSc1
•	•	k?
Ječmeniště	ječmeniště	k1gNnSc2
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
hora	hora	k1gFnSc1
u	u	k7c2
Derflic	Derflice	k1gFnPc2
•	•	k?
Kaolinka	kaolinka	k1gFnSc1
•	•	k?
Kopečky	kopeček	k1gInPc1
u	u	k7c2
Únanova	Únanov	k1gInSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lapikus	Lapikus	k1gInSc1
•	•	k?
Lom	lom	k1gInSc1
u	u	k7c2
Žerůtek	Žerůtka	k1gFnPc2
•	•	k?
Losolosy	Losolosa	k1gFnSc2
•	•	k?
Mandloňová	mandloňový	k2eAgFnSc1d1
mez	mez	k1gFnSc1
•	•	k?
Mašovický	Mašovický	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Mikulovické	Mikulovický	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Načeratický	Načeratický	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Oleksovická	Oleksovický	k2eAgFnSc1d1
mokřina	mokřina	k1gFnSc1
•	•	k?
Oleksovické	Oleksovický	k2eAgNnSc4d1
vřesoviště	vřesoviště	k1gNnSc4
•	•	k?
Orlí	orlí	k2eAgNnSc1d1
hnízdo	hnízdo	k1gNnSc1
•	•	k?
Pod	pod	k7c7
Šibeničním	šibeniční	k2eAgInSc7d1
kopcem	kopec	k1gInSc7
•	•	k?
Petrovy	Petrův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Protržený	protržený	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Pustý	pustý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
u	u	k7c2
Konic	Konice	k1gFnPc2
•	•	k?
Rudlické	Rudlický	k2eAgInPc4d1
kopce	kopec	k1gInPc4
•	•	k?
Skalky	skalka	k1gFnSc2
•	•	k?
Spálená	spálený	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Střebovský	Střebovský	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Stříbrný	stříbrný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Šafářka	šafářka	k1gFnSc1
•	•	k?
Šidlovy	Šidlův	k2eAgFnSc2d1
skalky	skalka	k1gFnSc2
•	•	k?
Široký	široký	k2eAgInSc1d1
•	•	k?
Štěpánovský	Štěpánovský	k2eAgInSc4d1
lom	lom	k1gInSc4
•	•	k?
Tasovické	Tasovický	k2eAgInPc4d1
svahy	svah	k1gInPc4
•	•	k?
Trávní	trávní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
•	•	k?
U	u	k7c2
Huberta	Hubert	k1gMnSc2
•	•	k?
U	u	k7c2
kapličky	kaplička	k1gFnSc2
•	•	k?
U	u	k7c2
Michálka	Michálek	k1gMnSc2
•	•	k?
Uherčická	Uherčický	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Ve	v	k7c6
Žlebě	žleb	k1gInSc6
•	•	k?
Vraní	vraní	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Vrbovecký	Vrbovecký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Výrovické	Výrovický	k2eAgFnSc3d1
kopce	kopka	k1gFnSc3
•	•	k?
Zmijiště	Zmijiště	k1gNnSc2
•	•	k?
Žleby	žleb	k1gInPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
