<s>
Podhradské	podhradský	k2eAgFnPc4d1
skály	skála	k1gFnPc4
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
ev.	ev.	k?
č.	č.	k?
1948	#num#	k4
poblíž	poblíž	k7c2
obce	obec	k1gFnSc2
Korolupy	Korolupa	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Znojmo	Znojmo	k1gNnSc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
350	#num#	k4
<g/>
–	–	k?
<g/>
447	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>