<s>
Virová	virový	k2eAgFnSc1d1	virová
hepatitida	hepatitida	k1gFnSc1	hepatitida
(	(	kIx(	(
<g/>
též	též	k9	též
infekční	infekční	k2eAgFnSc1d1	infekční
hepatitida	hepatitida	k1gFnSc1	hepatitida
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
infekční	infekční	k2eAgFnSc1d1	infekční
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc4	onemocnění
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
způsobující	způsobující	k2eAgInSc1d1	způsobující
zánět	zánět	k1gInSc1	zánět
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
,,	,,	k?	,,
<g/>
nemoc	nemoc	k1gFnSc1	nemoc
špinavých	špinavý	k2eAgFnPc2d1	špinavá
rukou	ruka	k1gFnPc2	ruka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
nemoci	nemoc	k1gFnSc2	nemoc
jsou	být	k5eAaImIp3nP	být
viry	vir	k1gInPc1	vir
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Prevalence	prevalence	k1gFnSc1	prevalence
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
2,5	[number]	k4	2,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
chybně	chybně	k6eAd1	chybně
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
symptom	symptom	k1gInSc4	symptom
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hepatitidu	hepatitida	k1gFnSc4	hepatitida
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
doprovázet	doprovázet	k5eAaImF	doprovázet
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
může	moct	k5eAaImIp3nS	moct
doprovázet	doprovázet	k5eAaImF	doprovázet
řadu	řada	k1gFnSc4	řada
jiných	jiný	k2eAgNnPc2d1	jiné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
zejm.	zejm.	k?	zejm.
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
žlučníku	žlučník	k1gInSc2	žlučník
a	a	k8xC	a
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnPc1	první
byly	být	k5eAaImAgFnP	být
izolovány	izolovat	k5eAaBmNgFnP	izolovat
viry	vira	k1gFnPc1	vira
hepatitidy	hepatitida	k1gFnSc2	hepatitida
A	a	k8xC	a
a	a	k8xC	a
B.	B.	kA	B.
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
hepatitidy	hepatitida	k1gFnPc1	hepatitida
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
ani	ani	k8xC	ani
virus	virus	k1gInSc1	virus
hepatitidy	hepatitida	k1gFnSc2	hepatitida
A	A	kA	A
ani	ani	k8xC	ani
virus	virus	k1gInSc1	virus
hepatitidy	hepatitida	k1gFnSc2	hepatitida
B	B	kA	B
<g/>
;	;	kIx,	;
nazvaly	nazvat	k5eAaBmAgFnP	nazvat
se	se	k3xPyFc4	se
nonA-nonB	nonAonB	k?	nonA-nonB
hepatitidy	hepatitida	k1gFnPc1	hepatitida
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
uznáváno	uznávat	k5eAaImNgNnS	uznávat
5	[number]	k4	5
druhů	druh	k1gInPc2	druh
infekčních	infekční	k2eAgFnPc2d1	infekční
hepatitid	hepatitida	k1gFnPc2	hepatitida
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
E.	E.	kA	E.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
o	o	k7c6	o
hepatitidách	hepatitida	k1gFnPc6	hepatitida
F	F	kA	F
a	a	k8xC	a
G	G	kA	G
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žloutenka	žloutenka	k1gFnSc1	žloutenka
typu	typ	k1gInSc2	typ
F	F	kA	F
nemá	mít	k5eNaImIp3nS	mít
potvrzenou	potvrzený	k2eAgFnSc4d1	potvrzená
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
hepatitidy	hepatitida	k1gFnSc2	hepatitida
G	G	kA	G
brzdí	brzdit	k5eAaImIp3nS	brzdit
rozvoj	rozvoj	k1gInSc4	rozvoj
infekce	infekce	k1gFnSc2	infekce
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
snáze	snadno	k6eAd2	snadno
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
místa	místo	k1gNnPc1	místo
vhodná	vhodný	k2eAgNnPc1d1	vhodné
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
těchto	tento	k3xDgInPc2	tento
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
G	G	kA	G
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
virus	virus	k1gInSc1	virus
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Flaviviridae	Flavivirida	k1gFnSc2	Flavivirida
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
A	a	k9	a
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgNnSc1d1	akutní
infekční	infekční	k2eAgNnSc1d1	infekční
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
játra	játra	k1gNnPc4	játra
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
průvodním	průvodní	k2eAgInSc7d1	průvodní
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
RNA	RNA	kA	RNA
virem	vir	k1gInSc7	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
A	A	kA	A
(	(	kIx(	(
<g/>
HAV	HAV	kA	HAV
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Picornaviridae	Picornavirida	k1gFnSc2	Picornavirida
<g/>
.	.	kIx.	.
</s>
<s>
Přenáší	přenášet	k5eAaImIp3nS	přenášet
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
fekálně-orální	fekálněrální	k2eAgFnSc7d1	fekálně-orální
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
označována	označován	k2eAgFnSc1d1	označována
laicky	laicky	k6eAd1	laicky
jako	jako	k8xC	jako
nemoc	nemoc	k1gFnSc1	nemoc
špinavých	špinavý	k2eAgFnPc2d1	špinavá
rukou	ruka	k1gFnPc2	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
přenosu	přenos	k1gInSc2	přenos
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hepatitidy	hepatitida	k1gFnSc2	hepatitida
A	a	k9	a
nebyl	být	k5eNaImAgInS	být
prokázán	prokázán	k2eAgInSc1d1	prokázán
přechod	přechod	k1gInSc1	přechod
do	do	k7c2	do
chronického	chronický	k2eAgNnSc2d1	chronické
stádia	stádium	k1gNnSc2	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
mezi	mezi	k7c7	mezi
15	[number]	k4	15
a	a	k8xC	a
45	[number]	k4	45
dnem	den	k1gInSc7	den
od	od	k7c2	od
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
hepatitidy	hepatitida	k1gFnSc2	hepatitida
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tmavá	tmavý	k2eAgFnSc1d1	tmavá
moč	moč	k1gFnSc1	moč
a	a	k8xC	a
světlá	světlý	k2eAgFnSc1d1	světlá
stolice	stolice	k1gFnSc1	stolice
<g/>
,	,	kIx,	,
zažloutlá	zažloutlý	k2eAgNnPc1d1	zažloutlé
bělma	bělmo	k1gNnPc1	bělmo
a	a	k8xC	a
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
projevit	projevit	k5eAaPmF	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
vůbec	vůbec	k9	vůbec
nezaregistrují	zaregistrovat	k5eNaPmIp3nP	zaregistrovat
a	a	k8xC	a
přechodí	přechodit	k5eAaPmIp3nP	přechodit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dobrou	dobrý	k2eAgFnSc4d1	dobrá
regeneraci	regenerace	k1gFnSc4	regenerace
jater	játra	k1gNnPc2	játra
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
zánětu	zánět	k1gInSc2	zánět
striktně	striktně	k6eAd1	striktně
dodržovat	dodržovat	k5eAaImF	dodržovat
alkoholovou	alkoholový	k2eAgFnSc4d1	alkoholová
abstinenci	abstinence	k1gFnSc4	abstinence
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
A	a	k9	a
lze	lze	k6eAd1	lze
očkovat	očkovat	k5eAaImF	očkovat
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
B	B	kA	B
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
infekčních	infekční	k2eAgFnPc2d1	infekční
hepatitid	hepatitida	k1gFnPc2	hepatitida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
DNA	dna	k1gFnSc1	dna
virem	vir	k1gInSc7	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
B	B	kA	B
(	(	kIx(	(
<g/>
HBV	HBV	kA	HBV
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Hepadnaviridae	Hepadnavirida	k1gFnSc2	Hepadnavirida
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
akutní	akutní	k2eAgMnSc1d1	akutní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
chronická	chronický	k2eAgFnSc1d1	chronická
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
přenosná	přenosný	k2eAgFnSc1d1	přenosná
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
(	(	kIx(	(
<g/>
sliny	slina	k1gFnPc1	slina
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
sperma	sperma	k1gNnSc1	sperma
<g/>
,	,	kIx,	,
poševní	poševní	k2eAgInSc1d1	poševní
sekret	sekret	k1gInSc1	sekret
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnPc1	nebezpečí
nákazy	nákaza	k1gFnSc2	nákaza
představují	představovat	k5eAaImIp3nP	představovat
také	také	k9	také
nekvalitní	kvalitní	k2eNgNnPc4d1	nekvalitní
tetovací	tetovací	k2eAgNnPc4d1	tetovací
a	a	k8xC	a
piercingová	piercingový	k2eAgNnPc4d1	piercingové
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitidou	hepatitida	k1gFnSc7	hepatitida
B	B	kA	B
bylo	být	k5eAaImAgNnS	být
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
infikováno	infikovat	k5eAaBmNgNnS	infikovat
přes	přes	k7c4	přes
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
lze	lze	k6eAd1	lze
očkovat	očkovat	k5eAaImF	očkovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
očkují	očkovat	k5eAaImIp3nP	očkovat
děti	dítě	k1gFnPc4	dítě
plošně	plošně	k6eAd1	plošně
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
B	B	kA	B
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
hus	husa	k1gFnPc2	husa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
infekce	infekce	k1gFnSc1	infekce
probíhá	probíhat	k5eAaImIp3nS	probíhat
většinou	většina	k1gFnSc7	většina
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
,	,	kIx,	,
přenos	přenos	k1gInSc1	přenos
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
vertikální	vertikální	k2eAgFnSc1d1	vertikální
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
virémii	virémie	k1gFnSc4	virémie
bez	bez	k7c2	bez
tvorby	tvorba	k1gFnSc2	tvorba
cirkulujících	cirkulující	k2eAgFnPc2d1	cirkulující
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
infekce	infekce	k1gFnPc1	infekce
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
komerčních	komerční	k2eAgInPc6d1	komerční
chovech	chov	k1gInPc6	chov
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
značně	značně	k6eAd1	značně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
ani	ani	k8xC	ani
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
význam	význam	k1gInSc4	význam
infekce	infekce	k1gFnPc1	infekce
hepadnaviry	hepadnavira	k1gFnSc2	hepadnavira
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
C	C	kA	C
je	být	k5eAaImIp3nS	být
přenosná	přenosný	k2eAgFnSc1d1	přenosná
především	především	k6eAd1	především
krví	krvit	k5eAaImIp3nS	krvit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
alternativní	alternativní	k2eAgFnPc1d1	alternativní
formy	forma	k1gFnPc1	forma
přenosu	přenos	k1gInSc2	přenos
popsány	popsat	k5eAaPmNgFnP	popsat
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
a	a	k8xC	a
přenos	přenos	k1gInSc4	přenos
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jedná	jednat	k5eAaImIp3nS	jednat
zase	zase	k9	zase
o	o	k7c4	o
přenos	přenos	k1gInSc4	přenos
krví	krev	k1gFnPc2	krev
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
sexu	sex	k1gInSc2	sex
resp.	resp.	kA	resp.
porodu	porod	k1gInSc2	porod
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
krvavým	krvavý	k2eAgNnPc3d1	krvavé
poraněním	poranění	k1gNnPc3	poranění
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
obou	dva	k4xCgMnPc2	dva
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
epidemickému	epidemický	k2eAgNnSc3d1	epidemické
rozšíření	rozšíření	k1gNnSc3	rozšíření
hepatitidy	hepatitida	k1gFnSc2	hepatitida
C	C	kA	C
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
při	při	k7c6	při
plošném	plošný	k2eAgNnSc6d1	plošné
očkování	očkování	k1gNnSc6	očkování
lidí	člověk	k1gMnPc2	člověk
proti	proti	k7c3	proti
schistosomóze	schistosomóza	k1gFnSc3	schistosomóza
nesterilními	sterilní	k2eNgFnPc7d1	nesterilní
jehlami	jehla	k1gFnPc7	jehla
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hepatitida	hepatitida	k1gFnSc1	hepatitida
C	C	kA	C
se	se	k3xPyFc4	se
v	v	k7c6	v
70	[number]	k4	70
–	–	k?	–
85	[number]	k4	85
%	%	kIx~	%
případů	případ	k1gInPc2	případ
stává	stávat	k5eAaImIp3nS	stávat
chronickou	chronický	k2eAgFnSc7d1	chronická
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
RNA	RNA	kA	RNA
virů	vir	k1gInPc2	vir
Flaviviridae	Flavivirida	k1gFnSc2	Flavivirida
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
virus	virus	k1gInSc1	virus
hepatitidy	hepatitida	k1gFnSc2	hepatitida
C	C	kA	C
(	(	kIx(	(
<g/>
HCV	HCV	kA	HCV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitidou	hepatitida	k1gFnSc7	hepatitida
C	C	kA	C
trpí	trpět	k5eAaImIp3nS	trpět
asi	asi	k9	asi
170	[number]	k4	170
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
velmi	velmi	k6eAd1	velmi
pomalou	pomalý	k2eAgFnSc4d1	pomalá
progresi	progrese	k1gFnSc4	progrese
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
zánět	zánět	k1gInSc1	zánět
jater	játra	k1gNnPc2	játra
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
člověka	člověk	k1gMnSc4	člověk
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
uplynout	uplynout	k5eAaPmF	uplynout
i	i	k8xC	i
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
stav	stav	k1gInSc4	stav
jater	játra	k1gNnPc2	játra
má	mít	k5eAaImIp3nS	mít
životospráva	životospráva	k1gFnSc1	životospráva
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
přepáleným	přepálený	k2eAgInPc3d1	přepálený
tukům	tuk	k1gInPc3	tuk
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
nemoc	nemoc	k1gFnSc1	nemoc
takřka	takřka	k6eAd1	takřka
asymptomaticky	asymptomaticky	k6eAd1	asymptomaticky
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
C	C	kA	C
nelze	lze	k6eNd1	lze
očkovat	očkovat	k5eAaImF	očkovat
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
převážně	převážně	k6eAd1	převážně
nakaženou	nakažený	k2eAgFnSc7d1	nakažená
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
rizikovou	rizikový	k2eAgFnSc4d1	riziková
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
pacienti	pacient	k1gMnPc1	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
transfuzi	transfuze	k1gFnSc4	transfuze
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
uživatelé	uživatel	k1gMnPc1	uživatel
nitrožilních	nitrožilní	k2eAgFnPc2d1	nitrožilní
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
krevní	krevní	k2eAgFnPc1d1	krevní
transfuze	transfuze	k1gFnPc1	transfuze
prováděly	provádět	k5eAaImAgFnP	provádět
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
široké	široký	k2eAgNnSc4d1	široké
rozšíření	rozšíření	k1gNnSc4	rozšíření
si	se	k3xPyFc3	se
vynutila	vynutit	k5eAaPmAgFnS	vynutit
až	až	k6eAd1	až
zranění	zranění	k1gNnSc4	zranění
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
vývojem	vývoj	k1gInSc7	vývoj
medicíny	medicína	k1gFnSc2	medicína
se	se	k3xPyFc4	se
krevní	krevní	k2eAgFnSc1d1	krevní
transfuze	transfuze	k1gFnSc1	transfuze
staly	stát	k5eAaPmAgFnP	stát
rutinně	rutinně	k6eAd1	rutinně
prováděným	prováděný	k2eAgInSc7d1	prováděný
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Šedesátá	šedesátý	k4xOgNnPc4	šedesátý
léta	léto	k1gNnPc4	léto
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
společenské	společenský	k2eAgFnSc2d1	společenská
revoluce	revoluce	k1gFnSc2	revoluce
přinesla	přinést	k5eAaPmAgFnS	přinést
masivní	masivní	k2eAgNnSc4d1	masivní
rozšíření	rozšíření	k1gNnSc4	rozšíření
uživatelů	uživatel	k1gMnPc2	uživatel
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
společné	společný	k2eAgFnSc2d1	společná
jehly	jehla	k1gFnSc2	jehla
několika	několik	k4yIc2	několik
lidmi	člověk	k1gMnPc7	člověk
bylo	být	k5eAaImAgNnS	být
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
nástup	nástup	k1gInSc4	nástup
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vleklý	vleklý	k2eAgInSc4d1	vleklý
průběh	průběh	k1gInSc4	průběh
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
viditelných	viditelný	k2eAgInPc2d1	viditelný
příznaků	příznak	k1gInPc2	příznak
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
pacientů	pacient	k1gMnPc2	pacient
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
generace	generace	k1gFnSc2	generace
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
chorobě	choroba	k1gFnSc6	choroba
až	až	k9	až
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
C	C	kA	C
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
další	další	k2eAgFnSc1d1	další
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
"	"	kIx"	"
<g/>
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
viru	vir	k1gInSc2	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
vyvolanému	vyvolaný	k2eAgNnSc3d1	vyvolané
onemocnění	onemocnění	k1gNnSc3	onemocnění
AIDS	AIDS	kA	AIDS
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
právem	právem	k6eAd1	právem
odsunula	odsunout	k5eAaPmAgFnS	odsunout
hepatitidu	hepatitida	k1gFnSc4	hepatitida
C	C	kA	C
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
zájmu	zájem	k1gInSc2	zájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
C	C	kA	C
se	se	k3xPyFc4	se
však	však	k9	však
stává	stávat	k5eAaImIp3nS	stávat
velkým	velký	k2eAgInSc7d1	velký
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
problémem	problém	k1gInSc7	problém
dneška	dnešek	k1gInSc2	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
chronickou	chronický	k2eAgFnSc7d1	chronická
formou	forma	k1gFnSc7	forma
trpí	trpět	k5eAaImIp3nS	trpět
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
přes	přes	k7c4	přes
175	[number]	k4	175
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
–	–	k?	–
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
žijí	žít	k5eAaImIp3nP	žít
odhadem	odhad	k1gInSc7	odhad
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
nemocných	mocný	k2eNgInPc2d1	mocný
hepatitidou	hepatitida	k1gFnSc7	hepatitida
C.	C.	kA	C.
To	to	k9	to
představuje	představovat	k5eAaImIp3nS	představovat
téměř	téměř	k6eAd1	téměř
1,3	[number]	k4	1,3
%	%	kIx~	%
tamní	tamní	k2eAgFnSc2d1	tamní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
výskytem	výskyt	k1gInSc7	výskyt
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
"	"	kIx"	"
<g/>
céčko	céčko	k1gNnSc1	céčko
<g/>
"	"	kIx"	"
v	v	k7c6	v
USA	USA	kA	USA
rozšířenější	rozšířený	k2eAgFnPc4d2	rozšířenější
než	než	k8xS	než
infekce	infekce	k1gFnPc4	infekce
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
přibývá	přibývat	k5eAaImIp3nS	přibývat
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
akutní	akutní	k2eAgFnSc2d1	akutní
hepatitidy	hepatitida	k1gFnSc2	hepatitida
typu	typ	k1gInSc2	typ
C.	C.	kA	C.
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
nízkým	nízký	k2eAgInSc7d1	nízký
výskytem	výskyt	k1gInSc7	výskyt
hepatitidy	hepatitida	k1gFnSc2	hepatitida
C.	C.	kA	C.
Ze	z	k7c2	z
statistik	statistika	k1gFnPc2	statistika
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
virem	vir	k1gInSc7	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
C	C	kA	C
(	(	kIx(	(
<g/>
HCV	HCV	kA	HCV
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
infikovány	infikovat	k5eAaBmNgFnP	infikovat
přibližně	přibližně	k6eAd1	přibližně
0,2	[number]	k4	0,2
%	%	kIx~	%
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemocí	nemoc	k1gFnPc2	nemoc
trpí	trpět	k5eAaImIp3nS	trpět
každý	každý	k3xTgMnSc1	každý
pětistý	pětistý	k2eAgMnSc1d1	pětistý
občan	občan	k1gMnSc1	občan
naší	náš	k3xOp1gFnSc2	náš
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
se	se	k3xPyFc4	se
však	však	k9	však
očekává	očekávat	k5eAaImIp3nS	očekávat
strmý	strmý	k2eAgInSc4d1	strmý
nárůst	nárůst	k1gInSc4	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
bylo	být	k5eAaImAgNnS	být
hlášeno	hlásit	k5eAaImNgNnS	hlásit
158	[number]	k4	158
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
hepatitidy	hepatitida	k1gFnSc2	hepatitida
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
846	[number]	k4	846
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
už	už	k9	už
1012	[number]	k4	1012
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
C	C	kA	C
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
rizikových	rizikový	k2eAgFnPc6d1	riziková
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
členové	člen	k1gMnPc1	člen
nejsou	být	k5eNaImIp3nP	být
většinou	většinou	k6eAd1	většinou
vyšetřeni	vyšetřen	k2eAgMnPc1d1	vyšetřen
<g/>
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
D	D	kA	D
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
RNA	RNA	kA	RNA
virem	vir	k1gInSc7	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
Hepatitis	hepatitis	k1gFnSc1	hepatitis
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
HDV	HDV	kA	HDV
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgMnS	vázat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
viru	vir	k1gInSc2	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
B.	B.	kA	B.
Očkováním	očkování	k1gNnSc7	očkování
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
.	.	kIx.	.
</s>
<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
E	E	kA	E
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
RNA	RNA	kA	RNA
virem	vir	k1gInSc7	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
E	E	kA	E
(	(	kIx(	(
<g/>
HEV	HEV	kA	HEV
<g/>
)	)	kIx)	)
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Hepeviridae	Hepevirida	k1gFnSc2	Hepevirida
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgInPc4d1	podobný
příznaky	příznak	k1gInPc4	příznak
jako	jako	k8xC	jako
hepatitida	hepatitida	k1gFnSc1	hepatitida
A	A	kA	A
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
průběh	průběh	k1gInSc4	průběh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
bouřlivý	bouřlivý	k2eAgMnSc1d1	bouřlivý
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
rozvojovém	rozvojový	k2eAgInSc6d1	rozvojový
světě	svět	k1gInSc6	svět
a	a	k8xC	a
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ji	on	k3xPp3gFnSc4	on
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
zavlečou	zavléct	k5eAaPmIp3nP	zavléct
cestovatelé	cestovatel	k1gMnPc1	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
E	E	kA	E
existuje	existovat	k5eAaImIp3nS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vakcína	vakcína	k1gFnSc1	vakcína
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
čínskou	čínský	k2eAgFnSc7d1	čínská
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
infekčních	infekční	k2eAgFnPc2d1	infekční
hepatitid	hepatitida	k1gFnPc2	hepatitida
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akutní	akutní	k2eAgFnSc6d1	akutní
fázi	fáze	k1gFnSc6	fáze
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
držet	držet	k5eAaImF	držet
dietu	dieta	k1gFnSc4	dieta
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
se	se	k3xPyFc4	se
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
přepálených	přepálený	k2eAgInPc2d1	přepálený
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Pacientům	pacient	k1gMnPc3	pacient
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
vitamíny	vitamín	k1gInPc1	vitamín
<g/>
,	,	kIx,	,
flavobion	flavobion	k1gInSc1	flavobion
<g/>
,	,	kIx,	,
lipovitan	lipovitan	k1gInSc1	lipovitan
<g/>
,	,	kIx,	,
esenciale	esenciale	k6eAd1	esenciale
a	a	k8xC	a
odvar	odvar	k1gInSc4	odvar
z	z	k7c2	z
ostropeřce	ostropeřka	k1gFnSc3	ostropeřka
mariánského	mariánský	k2eAgInSc2d1	mariánský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronické	chronický	k2eAgFnSc6d1	chronická
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
diet	dieta	k1gFnPc2	dieta
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
léčba	léčba	k1gFnSc1	léčba
interferonem	interferon	k1gInSc7	interferon
a	a	k8xC	a
pegylovaným	pegylovaný	k2eAgInSc7d1	pegylovaný
interferonem	interferon	k1gInSc7	interferon
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
ribavirinem	ribavirin	k1gInSc7	ribavirin
<g/>
.	.	kIx.	.
</s>
<s>
Interferon	interferon	k1gInSc1	interferon
je	být	k5eAaImIp3nS	být
bílkovina	bílkovina	k1gFnSc1	bílkovina
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
buňkami	buňka	k1gFnPc7	buňka
našeho	náš	k3xOp1gInSc2	náš
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bránila	bránit	k5eAaImAgFnS	bránit
množení	množení	k1gNnSc2	množení
virů	vir	k1gInPc2	vir
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dávkách	dávka	k1gFnPc6	dávka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopen	schopit	k5eAaPmNgInS	schopit
viry	vir	k1gInPc7	vir
i	i	k9	i
likvidovat	likvidovat	k5eAaBmF	likvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
vědci	vědec	k1gMnPc1	vědec
Alickem	Alicko	k1gNnSc7	Alicko
Isaacsem	Isaacs	k1gInSc7	Isaacs
(	(	kIx(	(
<g/>
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jeanem	Jean	k1gMnSc7	Jean
Lindenmanem	Lindenman	k1gMnSc7	Lindenman
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
slepičích	slepičí	k2eAgNnPc6d1	slepičí
vejcích	vejce	k1gNnPc6	vejce
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přenesli	přenést	k5eAaPmAgMnP	přenést
viry	vir	k1gInPc4	vir
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
spontánnímu	spontánní	k2eAgNnSc3d1	spontánní
zastavení	zastavení	k1gNnSc3	zastavení
množení	množení	k1gNnSc2	množení
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
izolován	izolován	k2eAgInSc1d1	izolován
interferon	interferon	k1gInSc1	interferon
<g/>
.	.	kIx.	.
</s>
<s>
Ribavirin	Ribavirin	k1gInSc1	Ribavirin
také	také	k9	také
brání	bránit	k5eAaImIp3nS	bránit
množení	množení	k1gNnSc4	množení
virů	vir	k1gInPc2	vir
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
léky	lék	k1gInPc1	lék
samostatně	samostatně	k6eAd1	samostatně
působí	působit	k5eAaImIp3nP	působit
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc7d2	menší
účinností	účinnost	k1gFnSc7	účinnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
podány	podán	k2eAgFnPc1d1	podána
dohromady	dohromady	k6eAd1	dohromady
(	(	kIx(	(
<g/>
synergie	synergie	k1gFnSc1	synergie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
léky	lék	k1gInPc1	lék
mají	mít	k5eAaImIp3nP	mít
ale	ale	k9	ale
nepříznivé	příznivý	k2eNgInPc4d1	nepříznivý
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
léčbě	léčba	k1gFnSc3	léčba
stále	stále	k6eAd1	stále
hledá	hledat	k5eAaImIp3nS	hledat
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Interferon	interferon	k1gInSc1	interferon
se	se	k3xPyFc4	se
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
podkožně	podkožně	k6eAd1	podkožně
a	a	k8xC	a
ribavirin	ribavirin	k1gInSc4	ribavirin
perorálně	perorálně	k6eAd1	perorálně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
fulminantním	fulminantní	k2eAgInSc6d1	fulminantní
průběhu	průběh	k1gInSc6	průběh
onemocnění	onemocnění	k1gNnPc2	onemocnění
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přehnané	přehnaný	k2eAgFnSc3d1	přehnaná
imunitní	imunitní	k2eAgFnSc3d1	imunitní
reakci	reakce	k1gFnSc3	reakce
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začne	začít	k5eAaPmIp3nS	začít
likvidovat	likvidovat	k5eAaBmF	likvidovat
postižené	postižený	k2eAgFnPc4d1	postižená
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
tisíce	tisíc	k4xCgInSc2	tisíc
případů	případ	k1gInPc2	případ
a	a	k8xC	a
pacient	pacient	k1gMnSc1	pacient
během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
hygiena	hygiena	k1gFnSc1	hygiena
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
)	)	kIx)	)
intravenózní	intravenózní	k2eAgNnSc1d1	intravenózní
užívání	užívání	k1gNnSc1	užívání
drog	droga	k1gFnPc2	droga
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
)	)	kIx)	)
promiskuitní	promiskuitní	k2eAgNnSc1d1	promiskuitní
praktikování	praktikování	k1gNnSc1	praktikování
análního	anální	k2eAgInSc2d1	anální
sexu	sex	k1gInSc2	sex
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
)	)	kIx)	)
promiskuita	promiskuita	k1gFnSc1	promiskuita
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
)	)	kIx)	)
další	další	k2eAgFnPc1d1	další
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
choroby	choroba	k1gFnPc1	choroba
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
)	)	kIx)	)
infuze	infuze	k1gFnSc1	infuze
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
C	C	kA	C
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
virová	virový	k2eAgFnSc1d1	virová
hepatitida	hepatitida	k1gFnSc1	hepatitida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
HUSA	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
hepatitida	hepatitida	k1gFnSc1	hepatitida
C.	C.	kA	C.
Klin	Klin	k1gNnSc1	Klin
Farmakol	Farmakol	k1gInSc1	Farmakol
Farm	Farm	k1gMnSc1	Farm
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
5353	[number]	k4	5353
<g/>
.	.	kIx.	.
</s>
<s>
HUSA	Husa	k1gMnSc1	Husa
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
možnosti	možnost	k1gFnPc1	možnost
léčby	léčba	k1gFnSc2	léčba
virových	virový	k2eAgFnPc2d1	virová
hepatitid	hepatitida	k1gFnPc2	hepatitida
<g/>
.	.	kIx.	.
</s>
<s>
Interni	Internit	k5eAaPmRp2nS	Internit
Med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
s.	s.	k?	s.
342	[number]	k4	342
<g/>
-	-	kIx~	-
<g/>
345	[number]	k4	345
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
5256	[number]	k4	5256
<g/>
.	.	kIx.	.
</s>
<s>
ROŽNOVSKÝ	Rožnovský	k1gMnSc1	Rožnovský
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
<g/>
.	.	kIx.	.
</s>
<s>
Virové	virový	k2eAgFnPc1d1	virová
hepatitidy	hepatitida	k1gFnPc1	hepatitida
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladistvých	mladistvý	k1gMnPc2	mladistvý
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
prevence	prevence	k1gFnSc1	prevence
vakcinací	vakcinace	k1gFnPc2	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Pediatr	pediatr	k1gMnSc1	pediatr
pro	pro	k7c4	pro
Prax	Prax	k1gInSc4	Prax
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
228	[number]	k4	228
<g/>
-	-	kIx~	-
<g/>
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
5264	[number]	k4	5264
<g/>
.	.	kIx.	.
</s>
