<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra-drama	hrarama	k1gFnSc1	hra-drama
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
nastupujícím	nastupující	k2eAgInSc7d1	nastupující
fašismem	fašismus	k1gInSc7	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
varujících	varující	k2eAgMnPc2d1	varující
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
i	i	k8xC	i
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
autorovy	autorův	k2eAgFnSc2d1	autorova
perzekuce	perzekuce	k1gFnSc2	perzekuce
gestapem	gestapo	k1gNnSc7	gestapo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
diktátor	diktátor	k1gMnSc1	diktátor
(	(	kIx(	(
<g/>
vůdce	vůdce	k1gMnSc1	vůdce
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
slovem	slovem	k6eAd1	slovem
"	"	kIx"	"
<g/>
maršál	maršál	k1gMnSc1	maršál
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chce	chtít	k5eAaImIp3nS	chtít
zahájit	zahájit	k5eAaPmF	zahájit
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
dobová	dobový	k2eAgFnSc1d1	dobová
paralela	paralela	k1gFnSc1	paralela
na	na	k7c4	na
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nakažlivá	nakažlivý	k2eAgFnSc1d1	nakažlivá
neznámá	známý	k2eNgFnSc1d1	neznámá
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
dost	dost	k6eAd1	dost
podobná	podobný	k2eAgFnSc1d1	podobná
lepře	lepra	k1gFnSc3	lepra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mírumilovný	mírumilovný	k2eAgMnSc1d1	mírumilovný
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
)	)	kIx)	)
nachází	nacházet	k5eAaImIp3nS	nacházet
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Symptomem	symptom	k1gInSc7	symptom
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
necitlivá	citlivý	k2eNgFnSc1d1	necitlivá
bílá	bílý	k2eAgFnSc1d1	bílá
skvrna	skvrna	k1gFnSc1	skvrna
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
původně	původně	k6eAd1	původně
léčí	léčit	k5eAaImIp3nS	léčit
pouze	pouze	k6eAd1	pouze
chudé	chudý	k2eAgMnPc4d1	chudý
lidi	člověk	k1gMnPc4	člověk
<g/>
;	;	kIx,	;
každý	každý	k3xTgInSc1	každý
mu	on	k3xPp3gMnSc3	on
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgMnPc4d1	bohatý
lidi	člověk	k1gMnPc4	člověk
vyléčit	vyléčit	k5eAaPmF	vyléčit
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
zbrojit	zbrojit	k5eAaImF	zbrojit
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Maršálovým	maršálův	k2eAgMnSc7d1	maršálův
přítelem	přítel	k1gMnSc7	přítel
je	být	k5eAaImIp3nS	být
také	také	k9	také
majitel	majitel	k1gMnSc1	majitel
zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bílou	bílý	k2eAgFnSc7d1	bílá
nemocí	nemoc	k1gFnSc7	nemoc
rovněž	rovněž	k9	rovněž
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zažil	zažít	k5eAaPmAgMnS	zažít
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
léčit	léčit	k5eAaImF	léčit
–	–	k?	–
dá	dát	k5eAaPmIp3nS	dát
továrníkovi	továrník	k1gMnSc3	továrník
ultimátum	ultimátum	k1gNnSc4	ultimátum
–	–	k?	–
nechce	chtít	k5eNaImIp3nS	chtít
jej	on	k3xPp3gMnSc4	on
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepřestane	přestat	k5eNaPmIp3nS	přestat
vyrábět	vyrábět	k5eAaImF	vyrábět
zbraně	zbraň	k1gFnPc4	zbraň
(	(	kIx(	(
<g/>
dokud	dokud	k6eAd1	dokud
nezačne	začít	k5eNaPmIp3nS	začít
zbrojit	zbrojit	k5eAaImF	zbrojit
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
sám	sám	k3xTgMnSc1	sám
předvolán	předvolat	k5eAaPmNgInS	předvolat
maršálem	maršál	k1gMnSc7	maršál
<g/>
,	,	kIx,	,
maršálova	maršálův	k2eAgMnSc4d1	maršálův
přítele	přítel	k1gMnSc4	přítel
nadále	nadále	k6eAd1	nadále
odmítá	odmítat	k5eAaImIp3nS	odmítat
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
maršál	maršál	k1gMnSc1	maršál
bude	být	k5eAaImBp3nS	být
dál	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc3	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
sám	sám	k3xTgMnSc1	sám
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
později	pozdě	k6eAd2	pozdě
maršál	maršál	k1gMnSc1	maršál
promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
k	k	k7c3	k
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
najde	najít	k5eAaPmIp3nS	najít
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
jej	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
továrníka	továrník	k1gMnSc2	továrník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
válku	válka	k1gFnSc4	válka
zrušil	zrušit	k5eAaPmAgMnS	zrušit
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
podmínky	podmínka	k1gFnPc4	podmínka
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gNnSc4	on
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
léky	lék	k1gInPc4	lék
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
zpět	zpět	k6eAd1	zpět
prochází	procházet	k5eAaImIp3nS	procházet
skrz	skrz	k7c4	skrz
zfanatizovaný	zfanatizovaný	k2eAgInSc4d1	zfanatizovaný
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
ubit	ubit	k2eAgInSc1d1	ubit
(	(	kIx(	(
<g/>
křičel	křičet	k5eAaImAgInS	křičet
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
válka	válka	k1gFnSc1	válka
zlá	zlý	k2eAgFnSc1d1	zlá
<g/>
,	,	kIx,	,
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zfanatizovaný	zfanatizovaný	k2eAgMnSc1d1	zfanatizovaný
člověk	člověk	k1gMnSc1	člověk
z	z	k7c2	z
davu	dav	k1gInSc2	dav
vysype	vysypat	k5eAaPmIp3nS	vysypat
obsah	obsah	k1gInSc1	obsah
doktorova	doktorův	k2eAgInSc2d1	doktorův
kufříku	kufřík	k1gInSc2	kufřík
<g/>
,	,	kIx,	,
a	a	k8xC	a
nalézá	nalézat	k5eAaImIp3nS	nalézat
ampulky	ampulka	k1gFnPc4	ampulka
s	s	k7c7	s
lékem	lék	k1gInSc7	lék
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
rozšlape	rozšlapat	k5eAaPmIp3nS	rozšlapat
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
sám	sám	k3xTgInSc1	sám
zahyne	zahynout	k5eAaPmIp3nS	zahynout
ušlapán	ušlapat	k5eAaPmNgInS	ušlapat
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přesnější	přesný	k2eAgInSc1d2	přesnější
děj	děj	k1gInSc1	děj
s	s	k7c7	s
rozborem	rozbor	k1gInSc7	rozbor
==	==	k?	==
</s>
</p>
<p>
<s>
Drama	drama	k1gNnSc1	drama
o	o	k7c6	o
3	[number]	k4	3
aktech	akt	k1gInPc6	akt
ve	v	k7c4	v
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
obrazech	obraz	k1gInPc6	obraz
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
===	===	k?	===
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Galén	Galén	k1gInSc4	Galén
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
</s>
</p>
<p>
<s>
Baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
</s>
</p>
<p>
<s>
Maršálova	maršálův	k2eAgFnSc1d1	Maršálova
dcera	dcera	k1gFnSc1	dcera
Anetta	Anett	k1gInSc2	Anett
</s>
</p>
<p>
<s>
Synovec	synovec	k1gMnSc1	synovec
barona	baron	k1gMnSc2	baron
Krüga	Krüg	k1gMnSc2	Krüg
Pavel	Pavel	k1gMnSc1	Pavel
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
</s>
</p>
<p>
<s>
Dcera	dcera	k1gFnSc1	dcera
</s>
</p>
<p>
<s>
Syn	syn	k1gMnSc1	syn
</s>
</p>
<p>
<s>
Sousedka	sousedka	k1gFnSc1	sousedka
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
3	[number]	k4	3
malomocní	malomocný	k1gMnPc1	malomocný
uvádějí	uvádět	k5eAaImIp3nP	uvádět
do	do	k7c2	do
děje	děj	k1gInSc2	děj
popisem	popis	k1gInSc7	popis
choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
projevy	projev	k1gInPc7	projev
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
bílá	bílý	k2eAgFnSc1d1	bílá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znecitliví	znecitlivit	k5eAaPmIp3nS	znecitlivit
a	a	k8xC	a
poté	poté	k6eAd1	poté
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
tkáně	tkáň	k1gFnSc2	tkáň
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
nemocného	nemocný	k1gMnSc2	nemocný
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	nemocný	k1gMnSc1	nemocný
přitom	přitom	k6eAd1	přitom
prožívá	prožívat	k5eAaImIp3nS	prožívat
velké	velký	k2eAgFnPc4d1	velká
bolesti	bolest	k1gFnPc4	bolest
a	a	k8xC	a
silně	silně	k6eAd1	silně
páchne	páchnout	k5eAaImIp3nS	páchnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Dvorní	dvorní	k2eAgFnSc1d1	dvorní
rada	rada	k1gFnSc1	rada
<g/>
/	/	kIx~	/
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
(	(	kIx(	(
<g/>
vede	vést	k5eAaImIp3nS	vést
Lilienthalovu	Lilienthalův	k2eAgFnSc4d1	Lilienthalův
kliniku	klinika	k1gFnSc4	klinika
<g/>
)	)	kIx)	)
debatuje	debatovat	k5eAaImIp3nS	debatovat
s	s	k7c7	s
novinářem	novinář	k1gMnSc7	novinář
o	o	k7c4	o
bílé	bílý	k2eAgFnPc4d1	bílá
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
se	se	k3xPyFc4	se
další	další	k2eAgInPc1d1	další
názvy	název	k1gInPc1	název
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
:	:	kIx,	:
Pejpinské	Pejpinský	k2eAgNnSc1d1	Pejpinský
malomocenství	malomocenství	k1gNnSc1	malomocenství
<g/>
,	,	kIx,	,
Čengova	Čengův	k2eAgFnSc1d1	Čengova
nemoc	nemoc	k1gFnSc1	nemoc
(	(	kIx(	(
<g/>
Morbus	Morbus	k1gInSc1	Morbus
Tshengi	Tsheng	k1gFnSc2	Tsheng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
popisuje	popisovat	k5eAaImIp3nS	popisovat
nemoc	nemoc	k1gFnSc4	nemoc
jako	jako	k8xC	jako
neléčitelnou	léčitelný	k2eNgFnSc4d1	neléčitelná
(	(	kIx(	(
<g/>
jediné	jediný	k2eAgNnSc1d1	jediné
co	co	k8xS	co
nemocným	nemocný	k1gMnPc3	nemocný
podávají	podávat	k5eAaImIp3nP	podávat
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc4	látka
proti	proti	k7c3	proti
zápachu	zápach	k1gInSc3	zápach
a	a	k8xC	a
morfium	morfium	k1gNnSc4	morfium
proti	proti	k7c3	proti
bolesti	bolest	k1gFnSc3	bolest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
bezohledně	bezohledně	k6eAd1	bezohledně
hrdý	hrdý	k2eAgMnSc1d1	hrdý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
objevil	objevit	k5eAaPmAgMnS	objevit
právě	právě	k9	právě
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
klinice	klinika	k1gFnSc6	klinika
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
symptom	symptom	k1gInSc1	symptom
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
po	po	k7c6	po
něm.	něm.	k?	něm.
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
tímto	tento	k3xDgInSc7	tento
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
jeho	jeho	k3xOp3gInPc4	jeho
povahové	povahový	k2eAgInPc4d1	povahový
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
trvá	trvat	k5eAaImIp3nS	trvat
cca	cca	kA	cca
3-5	[number]	k4	3-5
měsíců	měsíc	k1gInPc2	měsíc
než	než	k8xS	než
nastane	nastat	k5eAaPmIp3nS	nastat
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
postihuje	postihovat	k5eAaImIp3nS	postihovat
jen	jen	k9	jen
osoby	osoba	k1gFnPc1	osoba
starší	starý	k2eAgFnPc1d2	starší
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
nemůže	moct	k5eNaImIp3nS	moct
dát	dát	k5eAaPmF	dát
žádnou	žádný	k3yNgFnSc4	žádný
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
prognózu	prognóza	k1gFnSc4	prognóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigeliem	Sigelium	k1gNnSc7	Sigelium
přichází	přicházet	k5eAaImIp3nS	přicházet
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
řeči	řeč	k1gFnSc2	řeč
jemně	jemně	k6eAd1	jemně
naznačí	naznačit	k5eAaPmIp3nS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
správnou	správný	k2eAgFnSc4d1	správná
léčbu	léčba	k1gFnSc4	léčba
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
nenašel	najít	k5eNaPmAgMnS	najít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
výrazněji	výrazně	k6eAd2	výrazně
potom	potom	k6eAd1	potom
vyzní	vyznět	k5eAaImIp3nS	vyznět
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
jeho	jeho	k3xOp3gFnSc4	jeho
metodu	metoda	k1gFnSc4	metoda
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
asistentem	asistent	k1gMnSc7	asistent
jeho	jeho	k3xOp3gMnSc2	jeho
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
tchána	tchán	k1gMnSc2	tchán
Lilienthala	Lilienthal	k1gMnSc2	Lilienthal
<g/>
,	,	kIx,	,
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
dá	dát	k5eAaPmIp3nS	dát
možnost	možnost	k1gFnSc4	možnost
metodu	metoda	k1gFnSc4	metoda
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénovi	Galén	k1gMnSc6	Galén
říkal	říkat	k5eAaImAgMnS	říkat
Lilienthal	Lilienthal	k1gMnSc1	Lilienthal
Dětina	dětina	k1gMnSc1	dětina
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
pasáž	pasáž	k1gFnSc1	pasáž
smlouvání	smlouvání	k1gNnSc2	smlouvání
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
odmítá	odmítat	k5eAaImIp3nS	odmítat
sdělit	sdělit	k5eAaPmF	sdělit
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ji	on	k3xPp3gFnSc4	on
nevyzkouší	vyzkoušet	k5eNaPmIp3nP	vyzkoušet
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galéna	k1gFnSc1	Galéna
i	i	k9	i
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
zahrává	zahrávat	k5eAaImIp3nS	zahrávat
do	do	k7c2	do
autu	aut	k1gInSc2	aut
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gInSc1	Sigelius
bude	být	k5eAaImBp3nS	být
taky	taky	k6eAd1	taky
možná	možná	k9	možná
jednou	jednou	k6eAd1	jednou
potřebovat	potřebovat	k5eAaImF	potřebovat
lék	lék	k1gInSc4	lék
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
mu	on	k3xPp3gMnSc3	on
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
pokoj	pokoj	k1gInSc4	pokoj
13	[number]	k4	13
s	s	k7c7	s
nejchudšími	chudý	k2eAgMnPc7d3	nejchudší
pacienty	pacient	k1gMnPc7	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
za	za	k7c4	za
léčbu	léčba	k1gFnSc4	léčba
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
slibuje	slibovat	k5eAaImIp3nS	slibovat
výsledky	výsledek	k1gInPc4	výsledek
do	do	k7c2	do
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nepředstavitelné	představitelný	k2eNgNnSc1d1	nepředstavitelné
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
minulými	minulý	k2eAgInPc7d1	minulý
komentáři	komentář	k1gInPc7	komentář
k	k	k7c3	k
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obraze	obraz	k1gInSc6	obraz
poměrně	poměrně	k6eAd1	poměrně
jasně	jasně	k6eAd1	jasně
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelia	Sigelius	k1gMnSc4	Sigelius
jako	jako	k8xS	jako
hrdou	hrdý	k2eAgFnSc4d1	hrdá
hlavu	hlava	k1gFnSc4	hlava
kliniky	klinika	k1gFnSc2	klinika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ráda	rád	k2eAgFnSc1d1	ráda
opravuje	opravovat	k5eAaImIp3nS	opravovat
a	a	k8xC	a
hůře	zle	k6eAd2	zle
snáší	snášet	k5eAaImIp3nS	snášet
znalejší	znalý	k2eAgFnSc4d2	znalejší
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
evidentní	evidentní	k2eAgNnSc1d1	evidentní
<g/>
,	,	kIx,	,
že	že	k8xS	že
bere	brát	k5eAaImIp3nS	brát
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galéna	k1gFnSc1	Galéna
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výdělek	výdělek	k1gInSc4	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
projevuje	projevovat	k5eAaImIp3nS	projevovat
méně	málo	k6eAd2	málo
sebejistě	sebejistě	k6eAd1	sebejistě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
mírnou	mírný	k2eAgFnSc4d1	mírná
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
jednání	jednání	k1gNnSc6	jednání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
moc	moc	k6eAd1	moc
dobře	dobře	k6eAd1	dobře
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
lék	lék	k1gInSc1	lék
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obraze	obraz	k1gInSc6	obraz
líčí	líčit	k5eAaImIp3nS	líčit
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zdánlivě	zdánlivě	k6eAd1	zdánlivě
děje	dít	k5eAaImIp3nS	dít
přímo	přímo	k6eAd1	přímo
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc6	chvíle
plní	plnit	k5eAaImIp3nP	plnit
účel	účel	k1gInSc4	účel
náhledu	náhled	k1gInSc2	náhled
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
této	tento	k3xDgFnSc2	tento
jednoduše	jednoduše	k6eAd1	jednoduše
popsané	popsaný	k2eAgFnSc2d1	popsaná
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
–	–	k?	–
všichni	všechen	k3xTgMnPc1	všechen
bez	bez	k7c2	bez
jmen	jméno	k1gNnPc2	jméno
<g/>
)	)	kIx)	)
nás	my	k3xPp1nPc2	my
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
náhledy	náhled	k1gInPc7	náhled
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
jsou	být	k5eAaImIp3nP	být
amorálně	amorálně	k6eAd1	amorálně
rádi	rád	k2eAgMnPc1d1	rád
za	za	k7c4	za
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemohou	moct	k5eNaImIp3nP	moct
najít	najít	k5eAaPmF	najít
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
nemoc	nemoc	k1gFnSc4	nemoc
dělá	dělat	k5eAaImIp3nS	dělat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatkem	nedostatek	k1gInSc7	nedostatek
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
naráží	narážet	k5eAaImIp3nS	narážet
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
na	na	k7c4	na
stav	stav	k1gInSc4	stav
Německa	Německo	k1gNnSc2	Německo
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
A.	A.	kA	A.
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pasáži	pasáž	k1gFnSc6	pasáž
proti	proti	k7c3	proti
názoru	názor	k1gInSc3	názor
mladých	mladá	k1gFnPc2	mladá
a	a	k8xC	a
proti	proti	k7c3	proti
bílé	bílý	k2eAgFnSc3d1	bílá
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
asistent	asistent	k1gMnSc1	asistent
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelia	Sigelia	k1gFnSc1	Sigelia
chce	chtít	k5eAaImIp3nS	chtít
vydělávat	vydělávat	k5eAaImF	vydělávat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
podvodně	podvodně	k6eAd1	podvodně
léčit	léčit	k5eAaImF	léčit
nemocné	nemocný	k1gMnPc4	nemocný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadá	vypadat	k5eAaPmIp3nS	vypadat
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Nezná	neznat	k5eAaImIp3nS	neznat
jeho	jeho	k3xOp3gNnSc4	jeho
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
barvou	barva	k1gFnSc7	barva
hořčici	hořčice	k1gFnSc6	hořčice
podobná	podobný	k2eAgFnSc1d1	podobná
látka	látka	k1gFnSc1	látka
do	do	k7c2	do
injekcí	injekce	k1gFnPc2	injekce
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc3	svůj
léčbě	léčba	k1gFnSc3	léčba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
neuvěřitelné	uvěřitelný	k2eNgInPc4d1	neuvěřitelný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
oddává	oddávat	k5eAaImIp3nS	oddávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
jí	jíst	k5eAaImIp3nS	jíst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
jsou	být	k5eAaImIp3nP	být
profesoři	profesor	k1gMnPc1	profesor
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
států	stát	k1gInPc2	stát
a	a	k8xC	a
gratulují	gratulovat	k5eAaImIp3nP	gratulovat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigeliovi	Sigelius	k1gMnSc3	Sigelius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
paradoxně	paradoxně	k6eAd1	paradoxně
o	o	k7c6	o
léčbě	léčba	k1gFnSc6	léčba
neví	vědět	k5eNaImIp3nS	vědět
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
sice	sice	k8xC	sice
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úspěch	úspěch	k1gInSc1	úspěch
kliniky	klinika	k1gFnSc2	klinika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
nemá	mít	k5eNaImIp3nS	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nervózní	nervózní	k2eAgMnSc1d1	nervózní
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
nic	nic	k6eAd1	nic
o	o	k7c6	o
léčbě	léčba	k1gFnSc6	léčba
neřekne	říct	k5eNaPmIp3nS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Žádá	žádat	k5eAaImIp3nS	žádat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
léčil	léčit	k5eAaImAgMnS	léčit
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
majetnou	majetný	k2eAgFnSc4d1	majetná
známost	známost	k1gFnSc4	známost
jednoho	jeden	k4xCgMnSc2	jeden
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
asistent	asistent	k1gMnSc1	asistent
si	se	k3xPyFc3	se
potom	potom	k6eAd1	potom
s	s	k7c7	s
požehnáním	požehnání	k1gNnSc7	požehnání
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelia	Sigelia	k1gFnSc1	Sigelia
otevírá	otevírat	k5eAaImIp3nS	otevírat
soukromou	soukromý	k2eAgFnSc4d1	soukromá
ordinaci	ordinace	k1gFnSc4	ordinace
a	a	k8xC	a
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
také	také	k9	také
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
podání	podání	k1gNnSc2	podání
<g/>
,	,	kIx,	,
půjde	jít	k5eAaImIp3nS	jít
majetný	majetný	k2eAgMnSc1d1	majetný
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigeliovi	Sigelius	k1gMnSc3	Sigelius
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Maršál	maršál	k1gMnSc1	maršál
(	(	kIx(	(
<g/>
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přichází	přicházet	k5eAaImIp3nS	přicházet
podívat	podívat	k5eAaImF	podívat
na	na	k7c6	na
nemocné	nemocná	k1gFnSc6	nemocná
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galéna	k1gFnSc1	Galéna
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
zřízenci	zřízenec	k1gMnPc7	zřízenec
nechtějí	chtít	k5eNaImIp3nP	chtít
pustit	pustit	k5eAaPmF	pustit
k	k	k7c3	k
nemocným	nemocný	k1gMnPc3	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
tak	tak	k6eAd1	tak
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
moment	moment	k1gInSc4	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejznalejší	znalý	k2eAgFnSc1d3	znalý
je	být	k5eAaImIp3nS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
činnosti	činnost	k1gFnSc6	činnost
někým	někdo	k3yInSc7	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
neví	vědět	k5eNaImIp3nS	vědět
o	o	k7c6	o
problému	problém	k1gInSc6	problém
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
před	před	k7c7	před
Maršálem	maršál	k1gMnSc7	maršál
svůj	svůj	k3xOyFgInSc4	svůj
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
jeho	jeho	k3xOp3gFnSc4	jeho
náklonnost	náklonnost	k1gFnSc4	náklonnost
k	k	k7c3	k
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
se	se	k3xPyFc4	se
před	před	k7c7	před
novináři	novinář	k1gMnPc7	novinář
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
o	o	k7c4	o
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénovi	Galénův	k2eAgMnPc1d1	Galénův
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
o	o	k7c6	o
zasloužilém	zasloužilý	k2eAgMnSc6d1	zasloužilý
spolubojovníkovi	spolubojovník	k1gMnSc6	spolubojovník
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
povýšenectví	povýšenectví	k1gNnSc1	povýšenectví
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
proti	proti	k7c3	proti
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénovi	Galén	k1gMnSc3	Galén
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
bílé	bílý	k2eAgFnSc3d1	bílá
nemoci	nemoc	k1gFnSc3	nemoc
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
po	po	k7c6	po
novinářích	novinář	k1gMnPc6	novinář
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
napsali	napsat	k5eAaBmAgMnP	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
léčit	léčit	k5eAaImF	léčit
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
státům	stát	k1gInPc3	stát
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
aby	aby	kYmCp3nP	aby
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
věčný	věčný	k2eAgInSc4d1	věčný
mír	mír	k1gInSc4	mír
a	a	k8xC	a
odzbrojily	odzbrojit	k5eAaPmAgInP	odzbrojit
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
odmítá	odmítat	k5eAaImIp3nS	odmítat
vydat	vydat	k5eAaPmF	vydat
svůj	svůj	k3xOyFgInSc4	svůj
lék	lék	k1gInSc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
léčit	léčit	k5eAaImF	léčit
jen	jen	k6eAd1	jen
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jindy	jindy	k6eAd1	jindy
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgInPc1d1	bohatý
léčit	léčit	k5eAaImF	léčit
nebude	být	k5eNaImBp3nS	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
ať	ať	k9	ať
použijí	použít	k5eAaPmIp3nP	použít
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
státníků	státník	k1gMnPc2	státník
k	k	k7c3	k
věčnému	věčné	k1gNnSc3	věčné
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
rezoluci	rezoluce	k1gFnSc4	rezoluce
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
<g/>
,	,	kIx,	,
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
mu	on	k3xPp3gMnSc3	on
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
znát	znát	k5eAaImF	znát
složení	složení	k1gNnSc4	složení
a	a	k8xC	a
přípravu	příprava	k1gFnSc4	příprava
léku	lék	k1gInSc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
odmítá	odmítat	k5eAaImIp3nS	odmítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohle	tenhle	k3xDgNnSc4	tenhle
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gFnPc4	jeho
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nepomohou	pomoct	k5eNaPmIp3nP	pomoct
ani	ani	k9	ani
výčitky	výčitka	k1gFnPc4	výčitka
<g/>
,	,	kIx,	,
že	že	k8xS	že
doktor	doktor	k1gMnSc1	doktor
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
lidem	lid	k1gInSc7	lid
a	a	k8xC	a
nestarat	starat	k5eNaImF	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galéna	k1gFnSc1	Galéna
definitivně	definitivně	k6eAd1	definitivně
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sám	sám	k3xTgMnSc1	sám
slouží	sloužit	k5eAaImIp3nS	sloužit
nejen	nejen	k6eAd1	nejen
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
svému	svůj	k3xOyFgInSc3	svůj
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénovi	Galén	k1gMnSc3	Galén
závislý	závislý	k2eAgMnSc1d1	závislý
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
zároveň	zároveň	k6eAd1	zároveň
popřel	popřít	k5eAaPmAgMnS	popřít
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
o	o	k7c6	o
neangažování	neangažování	k1gNnSc6	neangažování
se	se	k3xPyFc4	se
lékaře	lékař	k1gMnPc4	lékař
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Opět	opět	k6eAd1	opět
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
má	mít	k5eAaImIp3nS	mít
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
ředitele	ředitel	k1gMnPc4	ředitel
účtárny	účtárna	k1gFnSc2	účtárna
Krügových	Krügův	k2eAgInPc2d1	Krügův
závodů	závod	k1gInPc2	závod
na	na	k7c4	na
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
představují	představovat	k5eAaImIp3nP	představovat
německou	německý	k2eAgFnSc4d1	německá
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
materiál	materiál	k1gInSc4	materiál
Krupp	Krupp	k1gMnSc1	Krupp
<g/>
.	.	kIx.	.
</s>
<s>
Děkuje	děkovat	k5eAaImIp3nS	děkovat
Bohu	bůh	k1gMnSc3	bůh
za	za	k7c2	za
malomocenství	malomocenství	k1gNnSc2	malomocenství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
nedostal	dostat	k5eNaPmAgMnS	dostat
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
tak	tak	k9	tak
svým	svůj	k3xOyFgInSc7	svůj
názorem	názor	k1gInSc7	názor
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
týdenní	týdenní	k2eAgFnSc1d1	týdenní
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
metodu	metoda	k1gFnSc4	metoda
Blitzkriegu	Blitzkriega	k1gFnSc4	Blitzkriega
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
také	také	k9	také
potom	potom	k6eAd1	potom
Němci	Němec	k1gMnPc1	Němec
používali	používat	k5eAaImAgMnP	používat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
podmínkám	podmínka	k1gFnPc3	podmínka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
maršála	maršál	k1gMnSc4	maršál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
razantně	razantně	k6eAd1	razantně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
válku	válka	k1gFnSc4	válka
stát	stát	k5eAaImF	stát
vybaven	vybavit	k5eAaPmNgMnS	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
navíc	navíc	k6eAd1	navíc
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
přerušením	přerušení	k1gNnSc7	přerušení
zbrojení	zbrojení	k1gNnPc2	zbrojení
sebrala	sebrat	k5eAaPmAgFnS	sebrat
práce	práce	k1gFnSc1	práce
200	[number]	k4	200
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
takto	takto	k6eAd1	takto
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
právě	právě	k9	právě
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
armádní	armádní	k2eAgFnSc2d1	armádní
výroby	výroba	k1gFnSc2	výroba
dal	dát	k5eAaPmAgMnS	dát
mnoha	mnoho	k4c3	mnoho
lidem	člověk	k1gMnPc3	člověk
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
rozběhu	rozběh	k1gInSc3	rozběh
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Otcova	otcův	k2eAgFnSc1d1	otcova
bezohlednost	bezohlednost	k1gFnSc1	bezohlednost
a	a	k8xC	a
spokojenost	spokojenost	k1gFnSc1	spokojenost
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
nemocí	nemoc	k1gFnSc7	nemoc
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
kontrastu	kontrast	k1gInSc2	kontrast
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
matka	matka	k1gFnSc1	matka
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Otec	otec	k1gMnSc1	otec
jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
za	za	k7c4	za
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénem	Galén	k1gMnSc7	Galén
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
říká	říkat	k5eAaImIp3nS	říkat
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
dá	dát	k5eAaPmIp3nS	dát
všechno	všechen	k3xTgNnSc1	všechen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přimluvil	přimluvit	k5eAaPmAgMnS	přimluvit
za	za	k7c4	za
mír	mír	k1gInSc4	mír
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
taky	taky	k6eAd1	taky
živí	živit	k5eAaImIp3nP	živit
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
označí	označit	k5eAaPmIp3nS	označit
za	za	k7c4	za
bezcitného	bezcitný	k2eAgMnSc4d1	bezcitný
padoucha	padouch	k1gMnSc4	padouch
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
jako	jako	k9	jako
padouch	padouch	k1gMnSc1	padouch
nepřipadá	připadat	k5eNaImIp3nS	připadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
dvorního	dvorní	k2eAgMnSc2d1	dvorní
rady	rada	k1gMnSc2	rada
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelia	Sigelia	k1gFnSc1	Sigelia
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
mu	on	k3xPp3gMnSc3	on
finance	finance	k1gFnSc1	finance
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
bílé	bílý	k2eAgFnSc2d1	bílá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
může	moct	k5eAaImIp3nS	moct
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigelius	Sigelius	k1gMnSc1	Sigelius
mu	on	k3xPp3gMnSc3	on
vykládá	vykládat	k5eAaImIp3nS	vykládat
o	o	k7c6	o
nařízení	nařízení	k1gNnSc6	nařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
brzy	brzy	k6eAd1	brzy
vyjde	vyjít	k5eAaPmIp3nS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
izolace	izolace	k1gFnSc1	izolace
nemocných	nemocný	k1gMnPc2	nemocný
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postupně	postupně	k6eAd1	postupně
pod	pod	k7c7	pod
lékařským	lékařský	k2eAgInSc7d1	lékařský
dozorem	dozor	k1gInSc7	dozor
zemřou	zemřít	k5eAaPmIp3nP	zemřít
a	a	k8xC	a
nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nebude	být	k5eNaImBp3nS	být
šířit	šířit	k5eAaImF	šířit
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
upozornil	upozornit	k5eAaPmAgMnS	upozornit
velice	velice	k6eAd1	velice
obratně	obratně	k6eAd1	obratně
na	na	k7c4	na
koncentrační	koncentrační	k2eAgInPc4d1	koncentrační
tábory	tábor	k1gInPc4	tábor
v	v	k7c4	v
nadcházející	nadcházející	k2eAgFnPc4d1	nadcházející
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Krüg	Krüg	k1gInSc1	Krüg
je	být	k5eAaImIp3nS	být
taky	taky	k6eAd1	taky
stižen	stihnout	k5eAaPmNgMnS	stihnout
bílou	bílý	k2eAgFnSc7d1	bílá
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
plný	plný	k2eAgInSc1d1	plný
strachu	strach	k1gInSc2	strach
vydává	vydávat	k5eAaImIp3nS	vydávat
za	za	k7c4	za
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénem	Galén	k1gMnSc7	Galén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
boji	boj	k1gInSc6	boj
za	za	k7c4	za
mír	mír	k1gInSc4	mír
dobré	dobrý	k2eAgMnPc4d1	dobrý
spojence	spojenec	k1gMnPc4	spojenec
<g/>
:	:	kIx,	:
Bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
strach	strach	k1gInSc4	strach
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Barona	baron	k1gMnSc4	baron
Krüga	Krüg	k1gMnSc4	Krüg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nemoci	nemoc	k1gFnSc3	nemoc
velmi	velmi	k6eAd1	velmi
bojí	bát	k5eAaImIp3nP	bát
<g/>
,	,	kIx,	,
pozná	poznat	k5eAaPmIp3nS	poznat
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
mu	on	k3xPp3gMnSc3	on
může	moct	k5eAaImIp3nS	moct
dát	dát	k5eAaPmF	dát
<g/>
.	.	kIx.	.
</s>
<s>
Krüg	Krüg	k1gMnSc1	Krüg
nemůže	moct	k5eNaImIp3nS	moct
dát	dát	k5eAaPmF	dát
nic	nic	k3yNnSc1	nic
než	než	k8xS	než
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Podmínka	podmínka	k1gFnSc1	podmínka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galéna	k1gFnSc1	Galéna
je	být	k5eAaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
musí	muset	k5eAaImIp3nS	muset
zastavit	zastavit	k5eAaPmF	zastavit
výrobu	výroba	k1gFnSc4	výroba
zbraní	zbraň	k1gFnPc2	zbraň
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
továrnách	továrna	k1gFnPc6	továrna
a	a	k8xC	a
propagovat	propagovat	k5eAaImF	propagovat
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
maršála	maršál	k1gMnSc2	maršál
a	a	k8xC	a
radí	radit	k5eAaImIp3nS	radit
se	se	k3xPyFc4	se
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
dodávek	dodávka	k1gFnPc2	dodávka
armádního	armádní	k2eAgInSc2d1	armádní
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
chce	chtít	k5eAaImIp3nS	chtít
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
baronu	baron	k1gMnSc6	baron
Krügovi	Krüg	k1gMnSc6	Krüg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
podmínku	podmínka	k1gFnSc4	podmínka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galéna	k1gFnSc1	Galéna
přistoupit	přistoupit	k5eAaPmF	přistoupit
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
daleko	daleko	k6eAd1	daleko
jeho	jeho	k3xOp3gNnSc4	jeho
přátelství	přátelství	k1gNnSc4	přátelství
zkrátka	zkrátka	k6eAd1	zkrátka
nesahá	sahat	k5eNaImIp3nS	sahat
<g/>
.	.	kIx.	.
</s>
<s>
Plánují	plánovat	k5eAaImIp3nP	plánovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc4	Galén
přelstili	přelstít	k5eAaPmAgMnP	přelstít
pouze	pouze	k6eAd1	pouze
dočasným	dočasný	k2eAgNnSc7d1	dočasné
pozastavením	pozastavení	k1gNnSc7	pozastavení
výroby	výroba	k1gFnSc2	výroba
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
léčení	léčení	k1gNnSc4	léčení
protahovat	protahovat	k5eAaImF	protahovat
<g/>
,	,	kIx,	,
nestihla	stihnout	k5eNaPmAgFnS	stihnout
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
si	se	k3xPyFc3	se
podá	podat	k5eAaPmIp3nS	podat
s	s	k7c7	s
baronem	baron	k1gMnSc7	baron
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
evidentně	evidentně	k6eAd1	evidentně
stane	stanout	k5eAaPmIp3nS	stanout
osudným	osudný	k2eAgInSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Maršál	maršál	k1gMnSc1	maršál
si	se	k3xPyFc3	se
pozval	pozvat	k5eAaPmAgMnS	pozvat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
nejdříve	dříve	k6eAd3	dříve
uplatit	uplatit	k5eAaPmF	uplatit
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
donutit	donutit	k5eAaPmF	donutit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikam	nikam	k6eAd1	nikam
to	ten	k3xDgNnSc1	ten
nevede	vést	k5eNaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
vojně	vojna	k1gFnSc6	vojna
viděl	vidět	k5eAaImAgMnS	vidět
mnoho	mnoho	k4c4	mnoho
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
maršál	maršál	k1gMnSc1	maršál
mu	on	k3xPp3gMnSc3	on
oponuje	oponovat	k5eAaImIp3nS	oponovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
viděl	vidět	k5eAaImAgMnS	vidět
více	hodně	k6eAd2	hodně
vítězů	vítěz	k1gMnPc2	vítěz
a	a	k8xC	a
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přichází	přicházet	k5eAaImIp3nS	přicházet
telefonát	telefonát	k1gInSc1	telefonát
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
maršál	maršál	k1gMnSc1	maršál
s	s	k7c7	s
klidem	klid	k1gInSc7	klid
a	a	k8xC	a
povděkem	povděk	k1gInSc7	povděk
kvituje	kvitovat	k5eAaBmIp3nS	kvitovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
baron	baron	k1gMnSc1	baron
Krüg	Krüg	k1gMnSc1	Krüg
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Nemusí	muset	k5eNaImIp3nP	muset
vyhovět	vyhovět	k5eAaPmF	vyhovět
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galénovi	Galén	k1gMnSc3	Galén
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgMnS	moct
zachovat	zachovat	k5eAaPmF	zachovat
život	život	k1gInSc4	život
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Při	při	k7c6	při
poradě	porada	k1gFnSc6	porada
u	u	k7c2	u
maršála	maršál	k1gMnSc2	maršál
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
občané	občan	k1gMnPc1	občan
chtějí	chtít	k5eAaImIp3nP	chtít
lék	lék	k1gInSc4	lék
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jdou	jít	k5eAaImIp3nP	jít
spíše	spíše	k9	spíše
s	s	k7c7	s
maršálem	maršál	k1gMnSc7	maršál
a	a	k8xC	a
starší	starší	k1gMnPc1	starší
chtějí	chtít	k5eAaImIp3nP	chtít
více	hodně	k6eAd2	hodně
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgInPc1d1	okolní
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
taky	taky	k6eAd1	taky
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
si	se	k3xPyFc3	se
domluví	domluvit	k5eAaPmIp3nS	domluvit
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
propagandy	propaganda	k1gFnSc2	propaganda
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
ovlivnitelnost	ovlivnitelnost	k1gFnSc4	ovlivnitelnost
mladých	mladý	k1gMnPc2	mladý
a	a	k8xC	a
opět	opět	k6eAd1	opět
na	na	k7c4	na
stav	stav	k1gInSc4	stav
v	v	k7c6	v
předválečném	předválečný	k2eAgNnSc6d1	předválečné
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Maršál	maršál	k1gMnSc1	maršál
provolává	provolávat	k5eAaImIp3nS	provolávat
před	před	k7c7	před
národem	národ	k1gInSc7	národ
na	na	k7c6	na
balkóně	balkón	k1gInSc6	balkón
při	při	k7c6	při
agitaci	agitace	k1gFnSc6	agitace
za	za	k7c4	za
právě	právě	k6eAd1	právě
rozběhlou	rozběhlý	k2eAgFnSc4d1	rozběhlá
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Bije	bít	k5eAaImIp3nS	bít
se	se	k3xPyFc4	se
hrdě	hrdě	k6eAd1	hrdě
do	do	k7c2	do
prsou	prsa	k1gNnPc2	prsa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
necítí	cítit	k5eNaImIp3nS	cítit
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	se	k3xPyFc4	se
zarazí	zarazit	k5eAaPmIp3nS	zarazit
a	a	k8xC	a
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
on	on	k3xPp3gMnSc1	on
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
minutách	minuta	k1gFnPc6	minuta
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
(	(	kIx(	(
<g/>
Anetta	Anetta	k1gFnSc1	Anetta
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
milý	milý	k1gMnSc1	milý
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
barona	baron	k1gMnSc2	baron
Krüga	Krüg	k1gMnSc2	Krüg
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
)	)	kIx)	)
uklidnit	uklidnit	k5eAaPmF	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
špatné	špatný	k2eAgFnPc4d1	špatná
zprávy	zpráva	k1gFnPc4	zpráva
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
národ	národ	k1gInSc1	národ
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
jako	jako	k9	jako
křečci	křeček	k1gMnPc1	křeček
<g/>
.	.	kIx.	.
</s>
<s>
K	K	kA	K
Čapek	Čapek	k1gMnSc1	Čapek
tak	tak	k6eAd1	tak
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
český	český	k2eAgInSc1d1	český
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
očekával	očekávat	k5eAaImAgInS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
budeme	být	k5eAaImBp1nP	být
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
válce	válka	k1gFnSc6	válka
vojensky	vojensky	k6eAd1	vojensky
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
maršál	maršál	k1gMnSc1	maršál
v	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
mluví	mluvit	k5eAaImIp3nP	mluvit
nesmysly	nesmysl	k1gInPc4	nesmysl
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
s	s	k7c7	s
mladým	mladý	k2eAgNnSc7d1	mladé
Krügem	Krügo	k1gNnSc7	Krügo
ho	on	k3xPp3gMnSc4	on
přemlouvají	přemlouvat	k5eAaImIp3nP	přemlouvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijal	přijmout	k5eAaPmAgMnS	přijmout
podmínky	podmínka	k1gFnPc4	podmínka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
a	a	k8xC	a
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
nakonec	nakonec	k6eAd1	nakonec
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
když	když	k8xS	když
nemá	mít	k5eNaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
východisko	východisko	k1gNnSc4	východisko
a	a	k8xC	a
smíří	smířit	k5eAaPmIp3nS	smířit
se	se	k3xPyFc4	se
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
dělat	dělat	k5eAaImF	dělat
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
zavolá	zavolat	k5eAaPmIp3nS	zavolat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
se	se	k3xPyFc4	se
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
prodírá	prodírat	k5eAaImIp3nS	prodírat
zfanatizovaným	zfanatizovaný	k2eAgInSc7d1	zfanatizovaný
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Udělá	udělat	k5eAaPmIp3nS	udělat
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
když	když	k8xS	když
začne	začít	k5eAaPmIp3nS	začít
vykřikovat	vykřikovat	k5eAaImF	vykřikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
válka	válka	k1gFnSc1	válka
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
ho	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
vzteku	vztek	k1gInSc6	vztek
ušlape	ušlapat	k5eAaPmIp3nS	ušlapat
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
známe	znát	k5eAaImIp1nP	znát
z	z	k7c2	z
pasáže	pasáž	k1gFnSc2	pasáž
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
zničí	zničit	k5eAaPmIp3nS	zničit
jeho	jeho	k3xOp3gFnPc4	jeho
injekce	injekce	k1gFnPc4	injekce
s	s	k7c7	s
lékem	lék	k1gInSc7	lék
<g/>
.	.	kIx.	.
</s>
<s>
Ironií	ironie	k1gFnSc7	ironie
osudu	osud	k1gInSc2	osud
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
lék	lék	k1gInSc1	lék
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
maršál	maršál	k1gMnSc1	maršál
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
tak	tak	k6eAd1	tak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zfanatizovaná	zfanatizovaný	k2eAgFnSc1d1	zfanatizovaná
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tragickým	tragický	k2eAgInSc7d1	tragický
koncem	konec	k1gInSc7	konec
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
bezvýchodné	bezvýchodný	k2eAgFnSc2d1	bezvýchodná
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
lék	lék	k1gInSc1	lék
a	a	k8xC	a
rozbíhá	rozbíhat	k5eAaImIp3nS	rozbíhat
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
docílil	docílit	k5eAaPmAgMnS	docílit
K.	K.	kA	K.
Čapek	Čapek	k1gMnSc1	Čapek
umocnění	umocnění	k1gNnSc2	umocnění
pointy	pointa	k1gFnSc2	pointa
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
větší	veliký	k2eAgInSc4d2	veliký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
nutnost	nutnost	k1gFnSc4	nutnost
zamyslet	zamyslet	k5eAaPmF	zamyslet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
situací	situace	k1gFnSc7	situace
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
dílo	dílo	k1gNnSc4	dílo
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operní	operní	k2eAgFnSc1d1	operní
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
adaptoval	adaptovat	k5eAaBmAgInS	adaptovat
do	do	k7c2	do
opery	opera	k1gFnSc2	opera
Biela	Bielo	k1gNnSc2	Bielo
nemoc	nemoc	k1gFnSc4	nemoc
slovenský	slovenský	k2eAgMnSc1d1	slovenský
skladatel	skladatel	k1gMnSc1	skladatel
Tibor	Tibor	k1gMnSc1	Tibor
Andrašovan	Andrašovan	k1gMnSc1	Andrašovan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
filmová	filmový	k2eAgFnSc1d1	filmová
podoba	podoba	k1gFnSc1	podoba
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Hugo	Hugo	k1gMnSc1	Hugo
Haas	Haasa	k1gFnPc2	Haasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galéna	Galén	k1gMnSc2	Galén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uvedení	uvedení	k1gNnSc4	uvedení
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1937	[number]	k4	1937
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1938	[number]	k4	1938
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
představení	představení	k1gNnSc1	představení
<g/>
:	:	kIx,	:
84	[number]	k4	84
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Dostal	dostat	k5eAaPmAgMnS	dostat
</s>
</p>
<p>
<s>
scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Hofman	Hofman	k1gMnSc1	Hofman
</s>
</p>
<p>
<s>
obsazení	obsazení	k1gNnSc1	obsazení
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sigelius	Sigelius	k1gMnSc1	Sigelius
<g/>
:	:	kIx,	:
Bedřich	Bedřich	k1gMnSc1	Bedřich
Karen	Karna	k1gFnPc2	Karna
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
:	:	kIx,	:
Hugo	Hugo	k1gMnSc1	Hugo
Haas	Haasa	k1gFnPc2	Haasa
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1957	[number]	k4	1957
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
96	[number]	k4	96
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Salzer	Salzer	k1gMnSc1	Salzer
</s>
</p>
<p>
<s>
scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Tröster	Tröster	k1gMnSc1	Tröster
</s>
</p>
<p>
<s>
obsazení	obsazení	k1gNnSc1	obsazení
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sigelius	Sigelius	k1gMnSc1	Sigelius
<g/>
:	:	kIx,	:
Bedřich	Bedřich	k1gMnSc1	Bedřich
Karen	Karen	k2eAgMnSc1d1	Karen
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Boháč	Boháč	k1gMnSc1	Boháč
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Smolík	Smolík	k1gMnSc1	Smolík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Průcha	Průcha	k1gMnSc1	Průcha
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Vejražka	Vejražka	k?	Vejražka
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Važanský	Važanský	k2eAgMnSc1d1	Važanský
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
58	[number]	k4	58
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Evžen	Evžen	k1gMnSc1	Evžen
Sokolovský	sokolovský	k2eAgMnSc1d1	sokolovský
</s>
</p>
<p>
<s>
scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vychodil	vychodit	k5eAaImAgMnS	vychodit
</s>
</p>
<p>
<s>
obsazení	obsazení	k1gNnSc1	obsazení
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sigelius	Sigelius	k1gMnSc1	Sigelius
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pešek	Pešek	k1gMnSc1	Pešek
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
<g/>
:	:	kIx,	:
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Vejražka	Vejražka	k?	Vejražka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pivec	Pivec	k1gMnSc1	Pivec
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1980	[number]	k4	1980
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
×	×	k?	×
/	/	kIx~	/
<g/>
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
KSČ	KSČ	kA	KSČ
zakázano	zakázana	k1gFnSc5	zakázana
a	a	k8xC	a
staženo	stažen	k2eAgNnSc1d1	staženo
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macháček	Macháček	k1gMnSc1	Macháček
</s>
</p>
<p>
<s>
scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Svoboda	Svoboda	k1gMnSc1	Svoboda
</s>
</p>
<p>
<s>
obsazení	obsazení	k1gNnSc1	obsazení
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sigelius	Sigelius	k1gMnSc1	Sigelius
<g/>
:	:	kIx,	:
Radovan	Radovan	k1gMnSc1	Radovan
Lukavský	Lukavský	k2eAgMnSc1d1	Lukavský
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galén	Galén	k1gInSc1	Galén
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Kemr	Kemr	k1gMnSc1	Kemr
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Somr	Somr	k1gMnSc1	Somr
</s>
</p>
<p>
<s>
Krog	Krog	k1gMnSc1	Krog
<g/>
:	:	kIx,	:
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Měnivá	měnivý	k2eAgFnSc1d1	měnivá
tvář	tvář	k1gFnSc1	tvář
divadla	divadlo	k1gNnSc2	divadlo
aneb	aneb	k?	aneb
Dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc2	století
s	s	k7c7	s
pražskými	pražský	k2eAgMnPc7d1	pražský
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
147	[number]	k4	147
<g/>
,	,	kIx,	,
154	[number]	k4	154
<g/>
,	,	kIx,	,
159	[number]	k4	159
<g/>
,	,	kIx,	,
176	[number]	k4	176
<g/>
,	,	kIx,	,
222	[number]	k4	222
<g/>
,	,	kIx,	,
226	[number]	k4	226
<g/>
,	,	kIx,	,
228	[number]	k4	228
<g/>
,	,	kIx,	,
232	[number]	k4	232
<g/>
,	,	kIx,	,
239	[number]	k4	239
<g/>
,	,	kIx,	,
246	[number]	k4	246
<g/>
,	,	kIx,	,
262	[number]	k4	262
<g/>
,	,	kIx,	,
267	[number]	k4	267
<g/>
,	,	kIx,	,
275	[number]	k4	275
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
<g/>
/	/	kIx~	/
<g/>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
278	[number]	k4	278
<g/>
,	,	kIx,	,
335	[number]	k4	335
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
340	[number]	k4	340
<g/>
,	,	kIx,	,
342	[number]	k4	342
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
356	[number]	k4	356
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
8	[number]	k4	8
<g/>
,	,	kIx,	,
378	[number]	k4	378
<g/>
,	,	kIx,	,
396	[number]	k4	396
<g/>
,	,	kIx,	,
402	[number]	k4	402
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
405	[number]	k4	405
<g/>
,	,	kIx,	,
409	[number]	k4	409
<g/>
,	,	kIx,	,
411	[number]	k4	411
<g/>
,	,	kIx,	,
438	[number]	k4	438
<g/>
,	,	kIx,	,
442	[number]	k4	442
<g/>
,	,	kIx,	,
564	[number]	k4	564
<g/>
,	,	kIx,	,
595	[number]	k4	595
<g/>
,	,	kIx,	,
619	[number]	k4	619
<g/>
,	,	kIx,	,
623	[number]	k4	623
<g/>
,	,	kIx,	,
657	[number]	k4	657
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
661	[number]	k4	661
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
</s>
</p>
<p>
<s>
Krakatit	Krakatit	k1gInSc1	Krakatit
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
