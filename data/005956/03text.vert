<s>
Avesta	Avesta	k1gFnSc1	Avesta
(	(	kIx(	(
<g/>
snad	snad	k9	snad
z	z	k7c2	z
perského	perský	k2eAgMnSc2d1	perský
abestág	abestág	k1gMnSc1	abestág
<g/>
,	,	kIx,	,
chvály	chvála	k1gFnPc1	chvála
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
staroíránských	staroíránský	k2eAgInPc2d1	staroíránský
posvátných	posvátný	k2eAgInPc2d1	posvátný
spisů	spis	k1gInPc2	spis
zoroastrického	zoroastrický	k2eAgNnSc2d1	zoroastrický
či	či	k8xC	či
parského	parský	k2eAgNnSc2d1	parský
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uctívá	uctívat	k5eAaImIp3nS	uctívat
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
Ahura	Ahur	k1gMnSc4	Ahur
Mazdu	Mazda	k1gFnSc4	Mazda
a	a	k8xC	a
jeho	on	k3xPp3gMnSc4	on
proroka	prorok	k1gMnSc4	prorok
Zarathuštru	Zarathuštr	k1gInSc2	Zarathuštr
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
žánrově	žánrově	k6eAd1	žánrově
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
spisy	spis	k1gInPc4	spis
rozličného	rozličný	k2eAgNnSc2d1	rozličné
stáří	stáří	k1gNnSc2	stáří
(	(	kIx(	(
<g/>
žalmy	žalm	k1gInPc4	žalm
<g/>
,	,	kIx,	,
modlitby	modlitba	k1gFnPc4	modlitba
<g/>
,	,	kIx,	,
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
právní	právní	k2eAgInPc4d1	právní
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
lyrické	lyrický	k2eAgFnPc4d1	lyrická
písně	píseň	k1gFnPc4	píseň
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
pouze	pouze	k6eAd1	pouze
zlomek	zlomek	k1gInSc4	zlomek
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
liturgické	liturgický	k2eAgNnSc4d1	liturgické
užívání	užívání	k1gNnSc4	užívání
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
rukopis	rukopis	k1gInSc1	rukopis
Avesty	Avesta	k1gFnSc2	Avesta
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1288	[number]	k4	1288
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
kodifikace	kodifikace	k1gFnSc1	kodifikace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
zřejmě	zřejmě	k6eAd1	zřejmě
už	už	k6eAd1	už
v	v	k7c6	v
sásánovském	sásánovský	k2eAgNnSc6d1	sásánovský
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
224	[number]	k4	224
-	-	kIx~	-
651	[number]	k4	651
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
Avesty	Avesta	k1gFnSc2	Avesta
však	však	k9	však
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
předtím	předtím	k6eAd1	předtím
předáván	předáván	k2eAgInSc1d1	předáván
ústním	ústní	k2eAgNnSc7d1	ústní
podáním	podání	k1gNnSc7	podání
<g/>
.	.	kIx.	.
</s>
<s>
Jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
avestánština	avestánština	k1gFnSc1	avestánština
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
perština	perština	k1gFnSc1	perština
<g/>
,	,	kIx,	,
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
blízká	blízký	k2eAgFnSc1d1	blízká
jazyku	jazyk	k1gInSc3	jazyk
indických	indický	k2eAgFnPc2d1	indická
Véd	véda	k1gFnPc2	véda
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
Avesty	Avesta	k1gFnSc2	Avesta
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInSc1	jeho
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
patrně	patrně	k6eAd1	patrně
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
původní	původní	k2eAgFnSc2d1	původní
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
řecké	řecký	k2eAgInPc4d1	řecký
a	a	k8xC	a
parthské	parthský	k2eAgNnSc4d1	parthský
panství	panství	k1gNnSc4	panství
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
i	i	k8xC	i
islámské	islámský	k2eAgNnSc1d1	islámské
pronásledování	pronásledování	k1gNnSc1	pronásledování
parsismu	parsismus	k1gInSc2	parsismus
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
především	především	k6eAd1	především
liturgickým	liturgický	k2eAgInPc3d1	liturgický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
mladší	mladý	k2eAgFnPc1d2	mladší
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
napsány	napsat	k5eAaBmNgFnP	napsat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
písmu	písmo	k1gNnSc6	písmo
staré	starý	k2eAgFnSc2d1	stará
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k9	už
lidé	člověk	k1gMnPc1	člověk
nerozuměli	rozumět	k5eNaImAgMnP	rozumět
a	a	k8xC	a
který	který	k3yQgInSc4	který
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
komentovat	komentovat	k5eAaBmF	komentovat
a	a	k8xC	a
vykládat	vykládat	k5eAaImF	vykládat
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
"	"	kIx"	"
<g/>
starou	stará	k1gFnSc7	stará
Avestu	Avest	k1gInSc2	Avest
<g/>
"	"	kIx"	"
v	v	k7c6	v
72	[number]	k4	72
kapitolách	kapitola	k1gFnPc6	kapitola
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
:	:	kIx,	:
Gáthy	Gáth	k1gInPc1	Gáth
(	(	kIx(	(
<g/>
zpěvy	zpěv	k1gInPc1	zpěv
<g/>
,	,	kIx,	,
Jasna	jasno	k1gNnSc2	jasno
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
43-51	[number]	k4	43-51
a	a	k8xC	a
53	[number]	k4	53
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
krátkých	krátký	k2eAgInPc2d1	krátký
veršovaných	veršovaný	k2eAgInPc2d1	veršovaný
hymnů	hymnus	k1gInPc2	hymnus
(	(	kIx(	(
<g/>
Jasna	jasno	k1gNnSc2	jasno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
<g />
.	.	kIx.	.
</s>
<s>
připisovaných	připisovaný	k2eAgInPc2d1	připisovaný
samému	samý	k3xTgMnSc3	samý
Zarathuštrovi	Zarathuštr	k1gMnSc3	Zarathuštr
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Jasna	jasno	k1gNnSc2	jasno
Haptanhaiti	Haptanhait	k2eAgMnPc1d1	Haptanhait
(	(	kIx(	(
<g/>
Jasna	jasno	k1gNnPc4	jasno
o	o	k7c6	o
sedmi	sedm	k4xCc6	sedm
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
Jasna	jasno	k1gNnSc2	jasno
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modlitby	modlitba	k1gFnPc4	modlitba
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
mladší	mladý	k2eAgFnPc4d2	mladší
než	než	k8xS	než
Gáthy	Gátha	k1gFnPc4	Gátha
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
a	a	k8xC	a
oběti	oběť	k1gFnPc4	oběť
Ahura	Ahuro	k1gNnSc2	Ahuro
Mazdovi	Mazdův	k2eAgMnPc1d1	Mazdův
<g/>
,	,	kIx,	,
ohni	oheň	k1gInSc6	oheň
<g/>
,	,	kIx,	,
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
stručný	stručný	k2eAgInSc4d1	stručný
výklad	výklad	k1gInSc4	výklad
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
a	a	k8xC	a
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
Jasna	jasno	k1gNnSc2	jasno
1-27	[number]	k4	1-27
a	a	k8xC	a
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
modlitby	modlitba	k1gFnPc4	modlitba
a	a	k8xC	a
chvály	chvála	k1gFnPc4	chvála
<g/>
,	,	kIx,	,
sepsané	sepsaný	k2eAgInPc4d1	sepsaný
patrně	patrně	k6eAd1	patrně
za	za	k7c2	za
sásánovců	sásánovec	k1gMnPc2	sásánovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
jazyce	jazyk	k1gInSc6	jazyk
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
.	.	kIx.	.
23	[number]	k4	23
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
24	[number]	k4	24
<g/>
)	)	kIx)	)
kapitol	kapitola	k1gFnPc2	kapitola
doplňků	doplněk	k1gInPc2	doplněk
a	a	k8xC	a
dodatků	dodatek	k1gInPc2	dodatek
k	k	k7c3	k
Jasnám	Jasna	k1gFnPc3	Jasna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spolu	spolu	k6eAd1	spolu
nesouvisejí	souviset	k5eNaImIp3nP	souviset
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Jasnami	Jasna	k1gFnPc7	Jasna
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Mladší	mladý	k2eAgFnSc2d2	mladší
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
není	být	k5eNaImIp3nS	být
liturgický	liturgický	k2eAgInSc4d1	liturgický
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
náboženský	náboženský	k2eAgInSc1d1	náboženský
zákoník	zákoník	k1gInSc1	zákoník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
různá	různý	k2eAgNnPc4d1	různé
zla	zlo	k1gNnPc4	zlo
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
tresty	trest	k1gInPc4	trest
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
návody	návod	k1gInPc4	návod
na	na	k7c4	na
očistné	očistný	k2eAgInPc4d1	očistný
rituály	rituál	k1gInPc4	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gNnSc4	on
22	[number]	k4	22
fargardů	fargard	k1gMnPc2	fargard
(	(	kIx(	(
<g/>
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
a	a	k8xC	a
ničivé	ničivý	k2eAgFnSc6d1	ničivá
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
dévové	dévová	k1gFnPc4	dévová
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
zákazům	zákaz	k1gInPc3	zákaz
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
<g/>
,	,	kIx,	,
nemocem	nemoc	k1gFnPc3	nemoc
a	a	k8xC	a
kouzlům	kouzlo	k1gNnPc3	kouzlo
<g/>
,	,	kIx,	,
správnému	správný	k2eAgNnSc3d1	správné
provádění	provádění	k1gNnSc3	provádění
rituálů	rituál	k1gInPc2	rituál
<g/>
,	,	kIx,	,
dělení	dělení	k1gNnSc1	dělení
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
čisté	čistý	k2eAgNnSc4d1	čisté
a	a	k8xC	a
nečisté	čistý	k2eNgNnSc4d1	nečisté
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
chvále	chvála	k1gFnSc3	chvála
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
štědrosti	štědrost	k1gFnSc2	štědrost
<g/>
.	.	kIx.	.
</s>
<s>
Zařazeno	zařazen	k2eAgNnSc1d1	zařazeno
je	být	k5eAaImIp3nS	být
i	i	k9	i
několik	několik	k4yIc4	několik
pozdních	pozdní	k2eAgInPc2d1	pozdní
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
breviáře	breviář	k1gInPc1	breviář
<g/>
,	,	kIx,	,
modlitební	modlitební	k2eAgFnPc1d1	modlitební
knížky	knížka	k1gFnPc1	knížka
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
42	[number]	k4	42
oslavných	oslavný	k2eAgInPc2d1	oslavný
hymnů	hymnus	k1gInPc2	hymnus
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
božstva	božstvo	k1gNnPc4	božstvo
a	a	k8xC	a
božské	božský	k2eAgFnPc4d1	božská
bytosti	bytost	k1gFnPc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
přepracováním	přepracování	k1gNnSc7	přepracování
archaické	archaický	k2eAgFnSc2d1	archaická
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Tematickou	tematický	k2eAgFnSc7d1	tematická
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
hrdinské	hrdinský	k2eAgInPc1d1	hrdinský
zpěvy	zpěv	k1gInPc1	zpěv
a	a	k8xC	a
písně	píseň	k1gFnPc1	píseň
o	o	k7c6	o
činech	čin	k1gInPc6	čin
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
několikrát	několikrát	k6eAd1	několikrát
přerušené	přerušený	k2eAgFnSc3d1	přerušená
tradici	tradice	k1gFnSc3	tradice
zoroastrovského	zoroastrovský	k2eAgNnSc2d1	zoroastrovský
náboženství	náboženství	k1gNnSc2	náboženství
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
historii	historie	k1gFnSc4	historie
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Ahura	Ahura	k1gMnSc1	Ahura
Mazdovi	Mazdův	k2eAgMnPc1d1	Mazdův
(	(	kIx(	(
<g/>
Hormuzdovi	Hormuzdův	k2eAgMnPc1d1	Hormuzdův
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zarathuštrovi	Zarathuštrův	k2eAgMnPc1d1	Zarathuštrův
(	(	kIx(	(
<g/>
Zoroastrovi	Zoroastrův	k2eAgMnPc1d1	Zoroastrův
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
i	i	k8xC	i
platónský	platónský	k2eAgInSc1d1	platónský
dialog	dialog	k1gInSc1	dialog
"	"	kIx"	"
<g/>
Alkibiadés	Alkibiadés	k1gInSc1	Alkibiadés
I.	I.	kA	I.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
122	[number]	k4	122
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
kladl	klást	k5eAaImAgInS	klást
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Toto	tento	k3xDgNnSc1	tento
datování	datování	k1gNnSc1	datování
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pozdějších	pozdní	k2eAgInPc6d2	pozdější
zoroastrických	zoroastrický	k2eAgInPc6d1	zoroastrický
spisech	spis	k1gInPc6	spis
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
evropští	evropský	k2eAgMnPc1d1	evropský
badatelé	badatel	k1gMnPc1	badatel
toto	tento	k3xDgNnSc4	tento
datování	datování	k1gNnSc4	datování
zpochybnili	zpochybnit	k5eAaPmAgMnP	zpochybnit
z	z	k7c2	z
jazykových	jazykový	k2eAgInPc2d1	jazykový
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
zjistili	zjistit	k5eAaPmAgMnP	zjistit
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
avestská	avestský	k2eAgFnSc1d1	avestský
perština	perština	k1gFnSc1	perština
blízká	blízký	k2eAgFnSc1d1	blízká
jazyku	jazyk	k1gMnSc3	jazyk
Rigvéd	Rigvéda	k1gFnPc2	Rigvéda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pak	pak	k6eAd1	pak
Zarathuštru	Zarathuštr	k1gInSc3	Zarathuštr
kladli	klást	k5eAaImAgMnP	klást
až	až	k9	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
badatelů	badatel	k1gMnPc2	badatel
kloní	klonit	k5eAaImIp3nS	klonit
k	k	k7c3	k
11	[number]	k4	11
<g/>
.	.	kIx.	.
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
Íránu	Írán	k1gInSc6	Írán
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nejstarší	starý	k2eAgFnPc1d3	nejstarší
části	část	k1gFnPc1	část
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
gáthy	gáth	k1gInPc7	gáth
<g/>
.	.	kIx.	.
</s>
<s>
Avesta	Avesta	k1gFnSc1	Avesta
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
tradovala	tradovat	k5eAaImAgFnS	tradovat
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jeden	jeden	k4xCgInSc1	jeden
perský	perský	k2eAgInSc1d1	perský
pramen	pramen	k1gInSc1	pramen
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
spálil	spálit	k5eAaPmAgInS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Achaimenovské	Achaimenovský	k2eAgFnSc2d1	Achaimenovský
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
tradice	tradice	k1gFnSc1	tradice
zřejmě	zřejmě	k6eAd1	zřejmě
přerušila	přerušit	k5eAaPmAgFnS	přerušit
a	a	k8xC	a
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
Avesty	Avesta	k1gFnSc2	Avesta
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
závazné	závazný	k2eAgFnSc3d1	závazná
kodifikaci	kodifikace	k1gFnSc3	kodifikace
textu	text	k1gInSc2	text
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
období	období	k1gNnSc4	období
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
(	(	kIx(	(
<g/>
224	[number]	k4	224
-	-	kIx~	-
651	[number]	k4	651
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zoroastrismus	zoroastrismus	k1gInSc1	zoroastrismus
stal	stát	k5eAaPmAgInS	stát
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
Avesty	Avesta	k1gFnSc2	Avesta
však	však	k9	však
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
zápis	zápis	k1gInSc4	zápis
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
samohlásky	samohláska	k1gFnPc4	samohláska
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
v	v	k7c6	v
parské	parský	k2eAgFnSc6d1	parský
liturgii	liturgie	k1gFnSc6	liturgie
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
islámském	islámský	k2eAgNnSc6d1	islámské
obsazení	obsazení	k1gNnSc6	obsazení
Persie	Persie	k1gFnSc2	Persie
bylo	být	k5eAaImAgNnS	být
zoroastrovské	zoroastrovský	k2eAgNnSc1d1	zoroastrovský
náboženství	náboženství	k1gNnSc1	náboženství
krutě	krutě	k6eAd1	krutě
pronásledováno	pronásledovat	k5eAaImNgNnS	pronásledovat
a	a	k8xC	a
zbytky	zbytek	k1gInPc1	zbytek
věřících	věřící	k1gFnPc2	věřící
uprchly	uprchnout	k5eAaPmAgInP	uprchnout
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pochází	pocházet	k5eAaImIp3nS	pocházet
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
rukopis	rukopis	k1gInSc1	rukopis
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
,	,	kIx,	,
přeložené	přeložený	k2eAgFnSc2d1	přeložená
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
perštiny	perština	k1gFnSc2	perština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
také	také	k9	také
Denkard	Denkard	k1gInSc1	Denkard
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
sbírka	sbírka	k1gFnSc1	sbírka
komentářů	komentář	k1gInPc2	komentář
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
perštině	perština	k1gFnSc6	perština
<g/>
,	,	kIx,	,
jakási	jakýsi	k3yIgFnSc1	jakýsi
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmínek	zmínka	k1gFnPc2	zmínka
u	u	k7c2	u
starověkých	starověký	k2eAgMnPc2d1	starověký
autorů	autor	k1gMnPc2	autor
nebyla	být	k5eNaImAgFnS	být
Avesta	Avesta	k1gFnSc1	Avesta
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známa	známo	k1gNnSc2	známo
a	a	k8xC	a
první	první	k4xOgFnSc2	první
přesnější	přesný	k2eAgFnSc2d2	přesnější
zprávy	zpráva	k1gFnSc2	zpráva
přinesli	přinést	k5eAaPmAgMnP	přinést
až	až	k9	až
francouzští	francouzský	k2eAgMnPc1d1	francouzský
misionáři	misionář	k1gMnPc1	misionář
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
překlad	překlad	k1gInSc1	překlad
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
jazyka	jazyk	k1gInSc2	jazyk
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědec	vědec	k1gMnSc1	vědec
Abraham	Abraham	k1gMnSc1	Abraham
Anquetil-Duperron	Anquetil-Duperron	k1gMnSc1	Anquetil-Duperron
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
strávil	strávit	k5eAaPmAgInS	strávit
10	[number]	k4	10
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
přivezl	přivézt	k5eAaPmAgMnS	přivézt
odtud	odtud	k6eAd1	odtud
asi	asi	k9	asi
180	[number]	k4	180
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
objevu	objev	k1gInSc6	objev
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
pochybovalo	pochybovat	k5eAaImAgNnS	pochybovat
<g/>
,	,	kIx,	,
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
poutal	poutat	k5eAaImAgMnS	poutat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
překládán	překládat	k5eAaImNgInS	překládat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
čten	číst	k5eAaImNgInS	číst
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
popularitě	popularita	k1gFnSc6	popularita
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsche	k1gFnPc2	Nietzsche
vybral	vybrat	k5eAaPmAgMnS	vybrat
právě	právě	k9	právě
Zarathustru	Zarathustra	k1gFnSc4	Zarathustra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc4	jeho
ústy	ústa	k1gNnPc7	ústa
tlumočil	tlumočit	k5eAaImAgMnS	tlumočit
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
poselství	poselství	k1gNnSc4	poselství
<g/>
.	.	kIx.	.
</s>
<s>
Krásný	krásný	k2eAgInSc1d1	krásný
rukopis	rukopis	k1gInSc1	rukopis
Avesty	Avesta	k1gFnSc2	Avesta
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
Orientálního	orientální	k2eAgInSc2d1	orientální
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
jazykovědy	jazykověda	k1gFnSc2	jazykověda
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
jeho	jeho	k3xOp3gFnSc1	jeho
digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
