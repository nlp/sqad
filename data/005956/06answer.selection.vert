<s>
Avesta	Avesta	k1gFnSc1	Avesta
(	(	kIx(	(
<g/>
snad	snad	k9	snad
z	z	k7c2	z
perského	perský	k2eAgMnSc2d1	perský
abestág	abestág	k1gMnSc1	abestág
<g/>
,	,	kIx,	,
chvály	chvála	k1gFnPc1	chvála
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
staroíránských	staroíránský	k2eAgInPc2d1	staroíránský
posvátných	posvátný	k2eAgInPc2d1	posvátný
spisů	spis	k1gInPc2	spis
zoroastrického	zoroastrický	k2eAgNnSc2d1	zoroastrický
či	či	k8xC	či
parského	parský	k2eAgNnSc2d1	parský
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
uctívá	uctívat	k5eAaImIp3nS	uctívat
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
Ahura	Ahur	k1gMnSc4	Ahur
Mazdu	Mazda	k1gFnSc4	Mazda
a	a	k8xC	a
jeho	on	k3xPp3gMnSc4	on
proroka	prorok	k1gMnSc4	prorok
Zarathuštru	Zarathuštr	k1gInSc2	Zarathuštr
<g/>
.	.	kIx.	.
</s>
