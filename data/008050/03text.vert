<s>
Kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
castellum	castellum	k1gInSc1	castellum
s	s	k7c7	s
významem	význam	k1gInSc7	význam
hrad	hrad	k1gInSc1	hrad
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
užívaného	užívaný	k2eAgNnSc2d1	užívané
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
též	též	k9	též
chrám	chrám	k1gInSc1	chrám
či	či	k8xC	či
chrám	chrám	k1gInSc1	chrám
Páně	páně	k2eAgInSc1d1	páně
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sakrální	sakrální	k2eAgFnSc1d1	sakrální
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
křesťanům	křesťan	k1gMnPc3	křesťan
k	k	k7c3	k
modlitbám	modlitba	k1gFnPc3	modlitba
a	a	k8xC	a
bohoslužbě	bohoslužba	k1gFnSc3	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
sakrální	sakrální	k2eAgFnSc2d1	sakrální
architektury	architektura	k1gFnSc2	architektura
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
z	z	k7c2	z
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
setkávání	setkávání	k1gNnSc2	setkávání
prvních	první	k4xOgMnPc2	první
křesťanů	křesťan	k1gMnPc2	křesťan
v	v	k7c6	v
soukromých	soukromý	k2eAgInPc6d1	soukromý
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
velikost	velikost	k1gFnSc1	velikost
jednotlivého	jednotlivý	k2eAgNnSc2d1	jednotlivé
společenství	společenství	k1gNnSc2	společenství
přerostlo	přerůst	k5eAaPmAgNnS	přerůst
soukromé	soukromý	k2eAgInPc4d1	soukromý
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
scházelo	scházet	k5eAaImAgNnS	scházet
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
budovy	budova	k1gFnPc1	budova
přímo	přímo	k6eAd1	přímo
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
bohoslužbě	bohoslužba	k1gFnSc3	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozmachu	rozmach	k1gInSc3	rozmach
bohoslužebných	bohoslužebný	k2eAgFnPc2d1	bohoslužebná
prostor	prostora	k1gFnPc2	prostora
došlo	dojít	k5eAaPmAgNnS	dojít
posléze	posléze	k6eAd1	posléze
s	s	k7c7	s
ediktem	edikt	k1gInSc7	edikt
milánským	milánský	k2eAgInSc7d1	milánský
císaře	císař	k1gMnSc4	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
křesťané	křesťan	k1gMnPc1	křesťan
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Judey	Judea	k1gFnSc2	Judea
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jakožto	jakožto	k8xS	jakožto
židé	žid	k1gMnPc1	žid
byli	být	k5eAaImAgMnP	být
uvyklí	uvyklý	k2eAgMnPc1d1	uvyklý
bohoslužbě	bohoslužba	k1gFnSc3	bohoslužba
v	v	k7c6	v
jeruzalémském	jeruzalémský	k2eAgInSc6d1	jeruzalémský
chrámu	chrám	k1gInSc6	chrám
a	a	k8xC	a
týdenní	týdenní	k2eAgFnSc3d1	týdenní
bohoslužbě	bohoslužba	k1gFnSc3	bohoslužba
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
synagogách	synagoga	k1gFnPc6	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Chrámová	chrámový	k2eAgFnSc1d1	chrámová
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
též	též	k9	též
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
oběti	oběť	k1gFnSc2	oběť
zvířat	zvíře	k1gNnPc2	zvíře
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Ježíše	Ježíš	k1gMnSc4	Ježíš
nebo	nebo	k8xC	nebo
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
učedníků	učedník	k1gMnPc2	učedník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
chrámové	chrámový	k2eAgFnPc4d1	chrámová
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
synagogy	synagoga	k1gFnSc2	synagoga
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
veřejné	veřejný	k2eAgFnSc2d1	veřejná
židovské	židovský	k2eAgFnSc2d1	židovská
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
během	během	k7c2	během
babylónského	babylónský	k2eAgNnSc2d1	babylónské
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tato	tento	k3xDgFnSc1	tento
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
nezahrnovala	zahrnovat	k5eNaImAgFnS	zahrnovat
oběť	oběť	k1gFnSc4	oběť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čtení	čtení	k1gNnSc1	čtení
z	z	k7c2	z
Tóry	tóra	k1gFnSc2	tóra
a	a	k8xC	a
proroků	prorok	k1gMnPc2	prorok
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
výkladem	výklad	k1gInSc7	výklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
takováto	takovýto	k3xDgFnSc1	takovýto
synagogální	synagogální	k2eAgFnSc1d1	synagogální
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
mohla	moct	k5eAaImAgFnS	moct
odehrávat	odehrávat	k5eAaImF	odehrávat
v	v	k7c6	v
soukromých	soukromý	k2eAgInPc6d1	soukromý
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
náročné	náročný	k2eAgFnPc1d1	náročná
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
v	v	k7c6	v
synagoze	synagoga	k1gFnSc6	synagoga
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
evangelií	evangelium	k1gNnPc2	evangelium
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
Ježíš	Ježíš	k1gMnSc1	Ježíš
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lk	Lk	k1gFnSc1	Lk
4,16	[number]	k4	4,16
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
praxi	praxe	k1gFnSc6	praxe
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
i	i	k9	i
první	první	k4xOgMnPc1	první
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
během	během	k7c2	během
první	první	k4xOgFnSc2	první
židovské	židovský	k2eAgFnSc2d1	židovská
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
70	[number]	k4	70
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
synagogy	synagoga	k1gFnSc2	synagoga
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
křesťanů	křesťan	k1gMnPc2	křesťan
původem	původ	k1gInSc7	původ
z	z	k7c2	z
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
měla	mít	k5eAaImAgFnS	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
vliv	vliv	k1gInSc4	vliv
zvláště	zvláště	k6eAd1	zvláště
synagogální	synagogální	k2eAgFnSc1d1	synagogální
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
liturgie	liturgie	k1gFnSc2	liturgie
většiny	většina	k1gFnSc2	většina
církví	církev	k1gFnPc2	církev
vychází	vycházet	k5eAaImIp3nS	vycházet
právě	právě	k9	právě
ze	z	k7c2	z
synagogálního	synagogální	k2eAgNnSc2d1	synagogální
uspořádání	uspořádání	k1gNnSc2	uspořádání
takového	takový	k3xDgNnSc2	takový
náboženského	náboženský	k2eAgNnSc2d1	náboženské
setkání	setkání	k1gNnSc2	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Syrské	syrský	k2eAgNnSc1d1	syrské
město	město	k1gNnSc1	město
Dura	Durum	k1gNnSc2	Durum
Európos	Európosa	k1gFnPc2	Európosa
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
Eufratu	Eufrat	k1gInSc2	Eufrat
leželo	ležet	k5eAaImAgNnS	ležet
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
parthské	parthský	k2eAgFnSc2d1	parthský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obléháno	obléhat	k5eAaImNgNnS	obléhat
roku	rok	k1gInSc2	rok
257	[number]	k4	257
Parthy	Partha	k1gFnSc2	Partha
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
vnější	vnější	k2eAgFnSc1d1	vnější
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
zachován	zachován	k2eAgInSc4d1	zachován
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgInPc1	obojí
vyzdobené	vyzdobený	k2eAgInPc1d1	vyzdobený
nástěnnými	nástěnný	k2eAgFnPc7d1	nástěnná
malbami	malba	k1gFnPc7	malba
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
užití	užití	k1gNnSc3	užití
proměněny	proměněn	k2eAgMnPc4d1	proměněn
ze	z	k7c2	z
soukromých	soukromý	k2eAgInPc2d1	soukromý
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
v	v	k7c4	v
Dura	Durus	k1gMnSc4	Durus
Europos	Europosa	k1gFnPc2	Europosa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
místnost	místnost	k1gFnSc4	místnost
užívanou	užívaný	k2eAgFnSc4d1	užívaná
pro	pro	k7c4	pro
udělování	udělování	k1gNnSc4	udělování
křtu	křest	k1gInSc2	křest
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
křtitelnicí	křtitelnice	k1gFnSc7	křtitelnice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ediktem	edikt	k1gInSc7	edikt
milánským	milánský	k2eAgInSc7d1	milánský
a	a	k8xC	a
výraznou	výrazný	k2eAgFnSc7d1	výrazná
podporou	podpora	k1gFnSc7	podpora
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Constantinus	Constantinus	k1gMnSc1	Constantinus
I.	I.	kA	I.
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
církevní	církevní	k2eAgFnSc2d1	církevní
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
staletí	staletí	k1gNnSc4	staletí
a	a	k8xC	a
zanechala	zanechat	k5eAaPmAgFnS	zanechat
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
kostela	kostel	k1gInSc2	kostel
stopu	stopa	k1gFnSc4	stopa
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klasickým	klasický	k2eAgFnPc3d1	klasická
bazilikám	bazilika	k1gFnPc3	bazilika
<g/>
,	,	kIx,	,
vystavěným	vystavěný	k2eAgInPc3d1	vystavěný
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
Lateránu	Laterán	k1gInSc6	Laterán
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
dedikovaná	dedikovaný	k2eAgFnSc1d1	dedikovaná
jako	jako	k8xC	jako
Bazilika	bazilika	k1gFnSc1	bazilika
Spasitele	spasitel	k1gMnSc2	spasitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
(	(	kIx(	(
<g/>
též	též	k9	též
Vatikánská	vatikánský	k2eAgFnSc1d1	Vatikánská
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
)	)	kIx)	)
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Pavla	Pavla	k1gFnSc1	Pavla
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Řím	Řím	k1gInSc1	Řím
je	být	k5eAaImIp3nS	být
dalšími	další	k2eAgFnPc7d1	další
bazilikami	bazilika	k1gFnPc7	bazilika
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
počátku	počátek	k1gInSc6	počátek
středověku	středověk	k1gInSc2	středověk
zaplněno	zaplnit	k5eAaPmNgNnS	zaplnit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
městům	město	k1gNnPc3	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
bazilikální	bazilikální	k2eAgFnSc3d1	bazilikální
výstavbě	výstavba	k1gFnSc3	výstavba
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Ravenna	Ravenna	k1gFnSc1	Ravenna
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
západořímského	západořímský	k2eAgMnSc2d1	západořímský
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
baziliky	bazilika	k1gFnPc4	bazilika
zde	zde	k6eAd1	zde
patří	patřit	k5eAaImIp3nP	patřit
Sant	Sant	k1gInSc4	Sant
<g/>
'	'	kIx"	'
<g/>
Apollinare	Apollinar	k1gMnSc5	Apollinar
in	in	k?	in
Classe	Classe	k1gFnPc4	Classe
a	a	k8xC	a
Sant	Sant	k1gInSc4	Sant
<g/>
'	'	kIx"	'
<g/>
Apollinare	Apollinar	k1gMnSc5	Apollinar
Nuovo	Nuovo	k1gNnSc1	Nuovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
architektonické	architektonický	k2eAgFnPc1d1	architektonická
podoby	podoba	k1gFnPc1	podoba
kostelů	kostel	k1gInPc2	kostel
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
výrazných	výrazný	k2eAgInPc2d1	výrazný
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
románský	románský	k2eAgInSc4d1	románský
sloh	sloh	k1gInSc4	sloh
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
půlkruhovým	půlkruhový	k2eAgInSc7d1	půlkruhový
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
pojmenování	pojmenování	k1gNnSc1	pojmenování
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
románských	románský	k2eAgInPc2d1	románský
kostelů	kostel	k1gInPc2	kostel
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
,	,	kIx,	,
stěny	stěna	k1gFnPc4	stěna
nejsou	být	k5eNaImIp3nP	být
prozatím	prozatím	k6eAd1	prozatím
příliš	příliš	k6eAd1	příliš
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
ze	z	k7c2	z
stylů	styl	k1gInPc2	styl
příznačných	příznačný	k2eAgInPc2d1	příznačný
pro	pro	k7c4	pro
středověk	středověk	k1gInSc4	středověk
je	být	k5eAaImIp3nS	být
gotika	gotika	k1gFnSc1	gotika
<g/>
,	,	kIx,	,
vybroušené	vybroušený	k2eAgNnSc4d1	vybroušené
architektonické	architektonický	k2eAgNnSc4d1	architektonické
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
základního	základní	k2eAgInSc2d1	základní
prvku	prvek	k1gInSc2	prvek
oblouku	oblouk	k1gInSc2	oblouk
lomeného	lomený	k2eAgInSc2d1	lomený
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
příznačný	příznačný	k2eAgInSc1d1	příznačný
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
katedrály	katedrál	k1gMnPc4	katedrál
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
gotických	gotický	k2eAgInPc2d1	gotický
kostelů	kostel	k1gInPc2	kostel
je	být	k5eAaImIp3nS	být
vznešený	vznešený	k2eAgInSc1d1	vznešený
a	a	k8xC	a
přivádí	přivádět	k5eAaImIp3nS	přivádět
pohled	pohled	k1gInSc4	pohled
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc1	užití
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
kleneb	klenba	k1gFnPc2	klenba
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
též	též	k9	též
stavby	stavba	k1gFnPc1	stavba
ještě	ještě	k6eAd1	ještě
odvážnější	odvážný	k2eAgFnPc1d2	odvážnější
a	a	k8xC	a
strukturovanější	strukturovaný	k2eAgFnPc1d2	strukturovanější
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
gotické	gotický	k2eAgInPc4d1	gotický
kostely	kostel	k1gInPc4	kostel
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
příznačné	příznačný	k2eAgFnPc1d1	příznačná
malované	malovaný	k2eAgFnPc1d1	malovaná
stěny	stěna	k1gFnPc1	stěna
<g/>
,	,	kIx,	,
bohatá	bohatý	k2eAgFnSc1d1	bohatá
oltářní	oltářní	k2eAgFnSc1d1	oltářní
a	a	k8xC	a
sochařská	sochařský	k2eAgFnSc1d1	sochařská
polychromovaná	polychromovaný	k2eAgFnSc1d1	polychromovaná
výzdoba	výzdoba	k1gFnSc1	výzdoba
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc1d1	vysoká
mnohobarevné	mnohobarevný	k2eAgFnPc1d1	mnohobarevná
vitráže	vitráž	k1gFnPc1	vitráž
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
typ	typ	k1gInSc1	typ
kostela	kostel	k1gInSc2	kostel
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
řeckého	řecký	k2eAgInSc2d1	řecký
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
s	s	k7c7	s
kupolí	kupole	k1gFnSc7	kupole
či	či	k8xC	či
dómem	dóm	k1gInSc7	dóm
uprostřed	uprostřed	k6eAd1	uprostřed
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
města	město	k1gNnSc2	město
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
Justiniánova	Justiniánův	k2eAgFnSc1d1	Justiniánova
výstavba	výstavba	k1gFnSc1	výstavba
chrámu	chrám	k1gInSc2	chrám
Boží	boží	k2eAgFnSc2d1	boží
Moudrosti	moudrost	k1gFnSc2	moudrost
(	(	kIx(	(
<g/>
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
)	)	kIx)	)
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
nejen	nejen	k6eAd1	nejen
výstavbu	výstavba	k1gFnSc4	výstavba
východního	východní	k2eAgInSc2d1	východní
typu	typ	k1gInSc2	typ
kostelů	kostel	k1gInPc2	kostel
(	(	kIx(	(
<g/>
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc3	typ
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
mešit	mešita	k1gFnPc2	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
půdorysem	půdorys	k1gInSc7	půdorys
kostela	kostel	k1gInSc2	kostel
bývá	bývat	k5eAaImIp3nS	bývat
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozložení	rozložení	k1gNnSc1	rozložení
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
klasického	klasický	k2eAgNnSc2d1	klasické
schématu	schéma	k1gNnSc2	schéma
římské	římský	k2eAgFnSc2d1	římská
baziliky	bazilika	k1gFnSc2	bazilika
<g/>
,	,	kIx,	,
veřejného	veřejný	k2eAgInSc2d1	veřejný
prostoru	prostor	k1gInSc2	prostor
určeného	určený	k2eAgInSc2d1	určený
buď	buď	k8xC	buď
k	k	k7c3	k
audiencím	audience	k1gFnPc3	audience
či	či	k8xC	či
obchodním	obchodní	k2eAgFnPc3d1	obchodní
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
samotné	samotný	k2eAgNnSc4d1	samotné
město	město	k1gNnSc4	město
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
starověkých	starověký	k2eAgFnPc2d1	starověká
kostelů-bazilik	kostelůazilika	k1gFnPc2	kostelů-bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
půdorysu	půdorys	k1gInSc6	půdorys
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
středem	střed	k1gInSc7	střed
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
také	také	k9	také
kupole	kupole	k1gFnSc1	kupole
coby	coby	k?	coby
symbol	symbol	k1gInSc1	symbol
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
klasických	klasický	k2eAgNnPc2d1	klasické
půdorysných	půdorysný	k2eAgNnPc2d1	půdorysné
schémat	schéma	k1gNnPc2	schéma
bývá	bývat	k5eAaImIp3nS	bývat
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
věčnosti	věčnost	k1gFnSc2	věčnost
a	a	k8xC	a
společenství	společenství	k1gNnPc1	společenství
křesťanů	křesťan	k1gMnPc2	křesťan
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
stolu	stol	k1gInSc2	stol
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
rotunda	rotunda	k1gFnSc1	rotunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kostely	kostel	k1gInPc1	kostel
bývají	bývat	k5eAaImIp3nP	bývat
pojmenovány	pojmenován	k2eAgMnPc4d1	pojmenován
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
určitěji	určitě	k6eAd2	určitě
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yQgInSc4	jaký
druh	druh	k1gInSc4	druh
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
:	:	kIx,	:
Katedrála	katedrála	k1gFnSc1	katedrála
–	–	k?	–
církevní	církevní	k2eAgInSc4d1	církevní
pojem	pojem	k1gInSc4	pojem
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
stolec	stolec	k1gInSc4	stolec
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
katedra	katedra	k1gFnSc1	katedra
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
kostelem	kostel	k1gInSc7	kostel
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
se	se	k3xPyFc4	se
jako	jako	k9	jako
katedrála	katedrála	k1gFnSc1	katedrála
označuje	označovat	k5eAaImIp3nS	označovat
takový	takový	k3xDgInSc4	takový
typ	typ	k1gInSc4	typ
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
presbytář	presbytář	k1gInSc4	presbytář
obklopen	obklopen	k2eAgInSc4d1	obklopen
věncem	věnec	k1gInSc7	věnec
bočních	boční	k2eAgFnPc2d1	boční
kaplí	kaple	k1gFnPc2	kaple
–	–	k?	–
takový	takový	k3xDgInSc4	takový
kostel	kostel	k1gInSc4	kostel
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
katedrálou	katedrála	k1gFnSc7	katedrála
podle	podle	k7c2	podle
církevní	církevní	k2eAgFnSc2d1	církevní
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
gotický	gotický	k2eAgInSc4d1	gotický
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
–	–	k?	–
církevní	církevní	k2eAgInSc4d1	církevní
pojem	pojem	k1gInSc4	pojem
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
nadaný	nadaný	k2eAgMnSc1d1	nadaný
určitými	určitý	k2eAgFnPc7d1	určitá
privilegii	privilegium	k1gNnPc7	privilegium
Svatým	svatý	k1gMnSc7	svatý
stolcem	stolec	k1gInSc7	stolec
–	–	k?	–
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
bazilika	bazilika	k1gFnSc1	bazilika
maior	maior	k1gInSc1	maior
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
papežské	papežský	k2eAgFnPc4d1	Papežská
baziliky	bazilika	k1gFnPc4	bazilika
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
minor	minor	k2eAgFnSc1d1	minor
<g/>
.	.	kIx.	.
</s>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
typ	typ	k1gInSc4	typ
kostela	kostel	k1gInSc2	kostel
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
bazilice	bazilika	k1gFnSc6	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
hlavní	hlavní	k2eAgInSc1d1	hlavní
kostel	kostel	k1gInSc1	kostel
farnosti	farnost	k1gFnSc2	farnost
<g/>
,	,	kIx,	,
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
místo	místo	k1gNnSc1	místo
křtu	křest	k1gInSc2	křest
či	či	k8xC	či
uzavření	uzavření	k1gNnSc2	uzavření
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Filiální	filiální	k2eAgInSc1d1	filiální
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
kostelem	kostel	k1gInSc7	kostel
farnosti	farnost	k1gFnSc2	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Specifikem	specifikon	k1gNnSc7	specifikon
bývají	bývat	k5eAaImIp3nP	bývat
chórové	chórový	k2eAgFnSc2d1	chórová
lavice	lavice	k1gFnSc2	lavice
určené	určený	k2eAgFnSc2d1	určená
k	k	k7c3	k
denní	denní	k2eAgFnSc3d1	denní
modlitbě	modlitba	k1gFnSc3	modlitba
řeholní	řeholní	k2eAgFnSc2d1	řeholní
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Kolegiátní	kolegiátní	k2eAgInSc1d1	kolegiátní
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
sídlem	sídlo	k1gNnSc7	sídlo
kolegiátní	kolegiátní	k2eAgFnSc2d1	kolegiátní
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Špitální	špitální	k2eAgInSc1d1	špitální
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
špitálu	špitál	k1gInSc3	špitál
či	či	k8xC	či
nemocnici	nemocnice	k1gFnSc3	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
určitým	určitý	k2eAgNnSc7d1	určité
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
cíl	cíl	k1gInSc1	cíl
pouti	pouť	k1gFnSc2	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
chlubit	chlubit	k5eAaImF	chlubit
např.	např.	kA	např.
cennou	cenný	k2eAgFnSc4d1	cenná
či	či	k8xC	či
zázračnou	zázračný	k2eAgFnSc4d1	zázračná
sochou	socha	k1gFnSc7	socha
či	či	k8xC	či
obrazem	obraz	k1gInSc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
s	s	k7c7	s
vyhlídkou	vyhlídka	k1gFnSc7	vyhlídka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
poutníky	poutník	k1gMnPc4	poutník
působila	působit	k5eAaImAgFnS	působit
i	i	k8xC	i
krása	krása	k1gFnSc1	krása
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
či	či	k8xC	či
hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
kaple	kaple	k1gFnSc1	kaple
–	–	k?	–
kostel	kostel	k1gInSc4	kostel
určený	určený	k2eAgInSc4d1	určený
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
pohřebním	pohřební	k2eAgInPc3d1	pohřební
obřadům	obřad	k1gInPc3	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Rektorátní	rektorátní	k2eAgInSc1d1	rektorátní
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nenáleží	náležet	k5eNaImIp3nS	náležet
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
žádné	žádný	k3yNgFnSc2	žádný
farnosti	farnost	k1gFnSc2	farnost
nebo	nebo	k8xC	nebo
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
řeholnímu	řeholní	k2eAgInSc3d1	řeholní
domu	dům	k1gInSc3	dům
<g/>
.	.	kIx.	.
</s>
<s>
Spravuje	spravovat	k5eAaImIp3nS	spravovat
jej	on	k3xPp3gInSc4	on
kněz-rektor	knězektor	k1gInSc4	kněz-rektor
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgInSc4d1	jmenovaný
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
rektorátního	rektorátní	k2eAgInSc2d1	rektorátní
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
seminární	seminární	k2eAgInSc1d1	seminární
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
rektorem	rektor	k1gMnSc7	rektor
je	být	k5eAaImIp3nS	být
rektor	rektor	k1gMnSc1	rektor
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Rotunda	rotunda	k1gFnSc1	rotunda
–	–	k?	–
kostel	kostel	k1gInSc4	kostel
kruhového	kruhový	k2eAgInSc2d1	kruhový
půdorysu	půdorys	k1gInSc2	půdorys
<g/>
,	,	kIx,	,
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
příznačný	příznačný	k2eAgInSc1d1	příznačný
pro	pro	k7c4	pro
románský	románský	k2eAgInSc4d1	románský
sloh	sloh	k1gInSc4	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
–	–	k?	–
"	"	kIx"	"
<g/>
kostel	kostel	k1gInSc1	kostel
<g/>
"	"	kIx"	"
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
menšímu	malý	k2eAgNnSc3d2	menší
společenství	společenství	k1gNnSc3	společenství
anebo	anebo	k8xC	anebo
řeholní	řeholní	k2eAgFnSc3d1	řeholní
komunitě	komunita	k1gFnSc3	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Emporový	Emporový	k2eAgInSc1d1	Emporový
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
opatřený	opatřený	k2eAgInSc1d1	opatřený
emporou	empora	k1gFnSc7	empora
(	(	kIx(	(
<g/>
panskou	panský	k2eAgFnSc7d1	Panská
tribunou	tribuna	k1gFnSc7	tribuna
<g/>
)	)	kIx)	)
z	z	k7c2	z
níž	jenž	k3xRgFnSc6	jenž
majitel	majitel	k1gMnSc1	majitel
přihlížel	přihlížet	k5eAaImAgMnS	přihlížet
bohoslužbám	bohoslužba	k1gFnPc3	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Emporové	Emporový	k2eAgInPc1d1	Emporový
kostely	kostel	k1gInPc1	kostel
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
od	od	k7c2	od
románského	románský	k2eAgNnSc2d1	románské
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Románské	románský	k2eAgInPc1d1	románský
emporové	emporový	k2eAgInPc1d1	emporový
kostely	kostel	k1gInPc1	kostel
měly	mít	k5eAaImAgInP	mít
též	též	k6eAd1	též
útočištnou	útočištný	k2eAgFnSc4d1	útočištná
a	a	k8xC	a
obrannou	obranný	k2eAgFnSc4d1	obranná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Megakostel	Megakostel	k1gMnSc1	Megakostel
–	–	k?	–
moderní	moderní	k2eAgNnSc1d1	moderní
velkokapacitní	velkokapacitní	k2eAgNnSc1d1	velkokapacitní
protestantské	protestantský	k2eAgInPc1d1	protestantský
kostely	kostel	k1gInPc1	kostel
(	(	kIx(	(
<g/>
pojmou	pojmout	k5eAaPmIp3nP	pojmout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
Kostely	kostel	k1gInPc1	kostel
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
,	,	kIx,	,
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
na	na	k7c6	na
kulturních	kulturní	k2eAgFnPc6d1	kulturní
zvyklostech	zvyklost	k1gFnPc6	zvyklost
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Opevněný	opevněný	k2eAgInSc4d1	opevněný
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Opevněný	opevněný	k2eAgInSc1d1	opevněný
kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
mimo	mimo	k7c4	mimo
náboženské	náboženský	k2eAgFnPc4d1	náboženská
a	a	k8xC	a
církevní	církevní	k2eAgFnPc4d1	církevní
funkce	funkce	k1gFnPc4	funkce
i	i	k8xC	i
funkci	funkce	k1gFnSc4	funkce
obrannou	obranný	k2eAgFnSc4d1	obranná
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
fortifikaci	fortifikace	k1gFnSc4	fortifikace
(	(	kIx(	(
<g/>
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
)	)	kIx)	)
refugiálního	refugiální	k2eAgInSc2d1	refugiální
(	(	kIx(	(
<g/>
útočištného	útočištný	k2eAgInSc2d1	útočištný
<g/>
)	)	kIx)	)
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
několika	několik	k4yIc2	několik
stupni	stupeň	k1gInPc7	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
je	být	k5eAaImIp3nS	být
hradba	hradba	k1gFnSc1	hradba
s	s	k7c7	s
obranným	obranný	k2eAgInSc7d1	obranný
ochozem	ochoz	k1gInSc7	ochoz
<g/>
,	,	kIx,	,
baštami	bašta	k1gFnPc7	bašta
nebo	nebo	k8xC	nebo
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
vstupní	vstupní	k2eAgFnSc1d1	vstupní
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
stavba	stavba	k1gFnSc1	stavba
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
obrannou	obranný	k2eAgFnSc4d1	obranná
nástavbu	nástavba	k1gFnSc4	nástavba
mezipatra	mezipatro	k1gNnSc2	mezipatro
<g/>
,	,	kIx,	,
podsebitím	podsebití	k1gNnSc7	podsebití
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
bývala	bývat	k5eAaImAgFnS	bývat
taktéž	taktéž	k?	taktéž
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
podsebití	podsebití	k1gNnSc4	podsebití
nebo	nebo	k8xC	nebo
ochozem	ochoz	k1gInSc7	ochoz
s	s	k7c7	s
cimbuřím	cimbuří	k1gNnSc7	cimbuří
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
jsou	být	k5eAaImIp3nP	být
zdi	zeď	k1gFnPc1	zeď
vybaveny	vybaven	k2eAgFnPc1d1	vybavena
střílnami	střílna	k1gFnPc7	střílna
<g/>
.	.	kIx.	.
</s>
<s>
Opevněné	opevněný	k2eAgInPc1d1	opevněný
kostely	kostel	k1gInPc1	kostel
byly	být	k5eAaImAgInP	být
budovány	budovat	k5eAaImNgInP	budovat
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
neopevněných	opevněný	k2eNgNnPc6d1	neopevněné
městech	město	k1gNnPc6	město
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Rakovník	Rakovník	k1gInSc4	Rakovník
a	a	k8xC	a
Sedlčany	Sedlčany	k1gInPc4	Sedlčany
nebo	nebo	k8xC	nebo
na	na	k7c6	na
majetcích	majetek	k1gInPc6	majetek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
spravovala	spravovat	k5eAaImAgFnS	spravovat
církevní	církevní	k2eAgFnSc1d1	církevní
vrchnost	vrchnost	k1gFnSc1	vrchnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Trhový	trhový	k2eAgInSc1d1	trhový
Štěpánov	Štěpánov	k1gInSc1	Štěpánov
<g/>
.	.	kIx.	.
</s>
<s>
Opevněné	opevněný	k2eAgInPc1d1	opevněný
kostely	kostel	k1gInPc1	kostel
byly	být	k5eAaImAgInP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
na	na	k7c6	na
kostele	kostel	k1gInSc6	kostel
vidět	vidět	k5eAaImF	vidět
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
symbolika	symbolika	k1gFnSc1	symbolika
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgNnP	být
<g/>
)	)	kIx)	)
aktuální	aktuální	k2eAgInSc4d1	aktuální
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
symbolika	symbolika	k1gFnSc1	symbolika
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýrazněji	výrazně	k6eAd3	výrazně
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
rysy	rys	k1gInPc1	rys
projevují	projevovat	k5eAaImIp3nP	projevovat
ve	v	k7c6	v
slohu	sloh	k1gInSc6	sloh
románském	románský	k2eAgNnSc6d1	románské
<g/>
,	,	kIx,	,
barokním	barokní	k2eAgNnSc6d1	barokní
<g/>
,	,	kIx,	,
gotickém	gotický	k2eAgNnSc6d1	gotické
a	a	k8xC	a
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
ve	v	k7c6	v
výzdobě	výzdoba	k1gFnSc6	výzdoba
je	být	k5eAaImIp3nS	být
též	též	k9	též
mezi	mezi	k7c7	mezi
kostely	kostel	k1gInPc7	kostel
katolickými	katolický	k2eAgMnPc7d1	katolický
a	a	k8xC	a
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
<g/>
.	.	kIx.	.
</s>
<s>
Katolické	katolický	k2eAgInPc1d1	katolický
kostely	kostel	k1gInPc1	kostel
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
honosně	honosně	k6eAd1	honosně
vyzdobené	vyzdobený	k2eAgNnSc1d1	vyzdobené
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
výzdoba	výzdoba	k1gFnSc1	výzdoba
kostelů	kostel	k1gInPc2	kostel
protestantských	protestantský	k2eAgInPc2d1	protestantský
bývá	bývat	k5eAaImIp3nS	bývat
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velmi	velmi	k6eAd1	velmi
střídmá	střídmý	k2eAgFnSc1d1	střídmá
<g/>
.	.	kIx.	.
</s>
<s>
Orientace	orientace	k1gFnSc1	orientace
kostela	kostel	k1gInSc2	kostel
–	–	k?	–
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
orientován	orientovat	k5eAaBmNgMnS	orientovat
od	od	k7c2	od
západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
vchod	vchod	k1gInSc1	vchod
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východu	východ	k1gInSc3	východ
(	(	kIx(	(
<g/>
oltář	oltář	k1gInSc1	oltář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
druhý	druhý	k4xOgInSc1	druhý
příchod	příchod	k1gInSc1	příchod
Krista	Kristus	k1gMnSc2	Kristus
v	v	k7c4	v
Soudný	soudný	k2eAgInSc4d1	soudný
den	den	k1gInSc4	den
má	mít	k5eAaImIp3nS	mít
přijít	přijít	k5eAaPmF	přijít
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
přijímat	přijímat	k5eAaImF	přijímat
první	první	k4xOgInSc4	první
a	a	k8xC	a
poslední	poslední	k2eAgInPc1d1	poslední
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc4	tvar
kostela	kostel	k1gInSc2	kostel
Románský	románský	k2eAgInSc4d1	románský
sloh	sloh	k1gInSc4	sloh
Baroko	baroko	k1gNnSc1	baroko
<g/>
,	,	kIx,	,
gotika	gotika	k1gFnSc1	gotika
–	–	k?	–
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Příchozí	příchozí	k1gMnSc1	příchozí
člověk	člověk	k1gMnSc1	člověk
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
kostelem	kostel	k1gInSc7	kostel
jde	jít	k5eAaImIp3nS	jít
symbolicky	symbolicky	k6eAd1	symbolicky
od	od	k7c2	od
Ježíšových	Ježíšův	k2eAgFnPc2d1	Ježíšova
nohou	noha	k1gFnPc2	noha
k	k	k7c3	k
Jeho	jeho	k3xOp3gNnSc3	jeho
srdci	srdce	k1gNnSc3	srdce
a	a	k8xC	a
hlavě	hlava	k1gFnSc3	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
Klenba	klenba	k1gFnSc1	klenba
Kupole	kupole	k1gFnSc2	kupole
Před	před	k7c7	před
čtením	čtení	k1gNnSc7	čtení
Bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
nese	nést	k5eAaImIp3nS	nést
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
značí	značit	k5eAaImIp3nS	značit
plnost	plnost	k1gFnSc1	plnost
a	a	k8xC	a
nadpřirozený	nadpřirozený	k2eAgInSc1d1	nadpřirozený
jas	jas	k1gInSc1	jas
<g/>
,	,	kIx,	,
sever	sever	k1gInSc1	sever
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
chladu	chlad	k1gInSc2	chlad
a	a	k8xC	a
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Boží	božit	k5eAaImIp3nS	božit
tedy	tedy	k8xC	tedy
přichází	přicházet	k5eAaImIp3nS	přicházet
ze	z	k7c2	z
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
proniká	pronikat	k5eAaImIp3nS	pronikat
tmou	tma	k1gFnSc7	tma
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgInSc1d1	vstupní
portál	portál	k1gInSc1	portál
–	–	k?	–
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
duchovní	duchovní	k2eAgInSc1d1	duchovní
prostor	prostor	k1gInSc1	prostor
od	od	k7c2	od
vnějšího	vnější	k2eAgInSc2d1	vnější
konzumního	konzumní	k2eAgInSc2d1	konzumní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
bývá	bývat	k5eAaImIp3nS	bývat
bohatě	bohatě	k6eAd1	bohatě
zdobená	zdobený	k2eAgFnSc1d1	zdobená
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
<g/>
.	.	kIx.	.
</s>
<s>
Branou	brána	k1gFnSc7	brána
se	se	k3xPyFc4	se
vchází	vcházet	k5eAaImIp3nP	vcházet
do	do	k7c2	do
posvátného	posvátný	k2eAgInSc2d1	posvátný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
–	–	k?	–
směřuje	směřovat	k5eAaImIp3nS	směřovat
od	od	k7c2	od
země	zem	k1gFnSc2	zem
vzhůru	vzhůru	k6eAd1	vzhůru
–	–	k?	–
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Evokuje	evokovat	k5eAaBmIp3nS	evokovat
spojení	spojení	k1gNnSc4	spojení
posvátného	posvátný	k2eAgInSc2d1	posvátný
prostoru	prostor	k1gInSc2	prostor
kostela	kostel	k1gInSc2	kostel
s	s	k7c7	s
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
věže	věž	k1gFnPc1	věž
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
stejné	stejné	k1gNnSc1	stejné
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
znamenat	znamenat	k5eAaImF	znamenat
sepnuté	sepnutý	k2eAgFnPc1d1	sepnutá
modlící	modlící	k2eAgFnPc1d1	modlící
se	se	k3xPyFc4	se
ruce	ruka	k1gFnPc1	ruka
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
věže	věž	k1gFnPc1	věž
dvě	dva	k4xCgFnPc4	dva
různě	různě	k6eAd1	různě
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
Adama	Adam	k1gMnSc4	Adam
a	a	k8xC	a
Evu	Eva	k1gFnSc4	Eva
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
kostelní	kostelní	k2eAgFnSc1d1	kostelní
věž	věž	k1gFnSc1	věž
postavena	postaven	k2eAgFnSc1d1	postavena
samostatně	samostatně	k6eAd1	samostatně
vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
Presbytář	presbytář	k1gInSc1	presbytář
(	(	kIx(	(
<g/>
presbyterium	presbyterium	k1gNnSc1	presbyterium
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
osa	osa	k1gFnSc1	osa
presbytáře	presbytář	k1gInSc2	presbytář
odkloněna	odklonit	k5eAaPmNgFnS	odklonit
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
osy	osa	k1gFnSc2	osa
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
odklonění	odklonění	k1gNnSc1	odklonění
představuje	představovat	k5eAaImIp3nS	představovat
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
hlavu	hlava	k1gFnSc4	hlava
skloněnou	skloněný	k2eAgFnSc4d1	skloněná
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
Sakristie	sakristie	k1gFnSc1	sakristie
Oltář	Oltář	k1gInSc1	Oltář
–	–	k?	–
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
(	(	kIx(	(
<g/>
katolickém	katolický	k2eAgInSc6d1	katolický
<g/>
)	)	kIx)	)
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oltáři	oltář	k1gInSc6	oltář
se	se	k3xPyFc4	se
přináší	přinášet	k5eAaImIp3nS	přinášet
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
masivní	masivní	k2eAgInSc1d1	masivní
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
na	na	k7c6	na
oltáři	oltář	k1gInSc6	oltář
obětovala	obětovat	k5eAaBmAgFnS	obětovat
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oltář	Oltář	k1gInSc1	Oltář
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
také	také	k9	také
místo	místo	k6eAd1	místo
k	k	k7c3	k
hostině	hostina	k1gFnSc3	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Boční	boční	k2eAgMnPc1d1	boční
oltář-	oltář-	k?	oltář-
Většinou	většinou	k6eAd1	většinou
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
přemístěn	přemístěn	k2eAgInSc4d1	přemístěn
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
zrušeného	zrušený	k2eAgInSc2d1	zrušený
kostela	kostel	k1gInSc2	kostel
nebo	nebo	k8xC	nebo
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Gotika	gotika	k1gFnSc1	gotika
–	–	k?	–
oltář	oltář	k1gInSc1	oltář
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
vpředu	vpředu	k6eAd1	vpředu
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
vyvýšen	vyvýšen	k2eAgMnSc1d1	vyvýšen
<g/>
.	.	kIx.	.
</s>
<s>
Věřící	věřící	k1gMnSc1	věřící
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přichází	přicházet	k5eAaImIp3nS	přicházet
a	a	k8xC	a
vzhlíží	vzhlížet	k5eAaImIp3nS	vzhlížet
jako	jako	k9	jako
k	k	k7c3	k
Ježíši	Ježíš	k1gMnSc3	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
–	–	k?	–
oltář	oltář	k1gInSc1	oltář
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
věřící	věřící	k1gMnPc1	věřící
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
do	do	k7c2	do
společenství	společenství	k1gNnSc2	společenství
kolem	kolem	k6eAd1	kolem
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
:	:	kIx,	:
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
bližním	bližní	k1gMnSc7	bližní
<g/>
.	.	kIx.	.
</s>
<s>
Ambon	ambon	k1gInSc1	ambon
–	–	k?	–
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
protestantských	protestantský	k2eAgInPc6d1	protestantský
kostelích	kostel	k1gInPc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ambonu	ambon	k1gInSc2	ambon
se	se	k3xPyFc4	se
hlásá	hlásat	k5eAaImIp3nS	hlásat
Boží	boží	k2eAgNnSc1d1	boží
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Ambon	ambon	k1gInSc1	ambon
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
oltář	oltář	k1gInSc1	oltář
dostatečně	dostatečně	k6eAd1	dostatečně
masivní	masivní	k2eAgInSc1d1	masivní
a	a	k8xC	a
nepřenositelný	přenositelný	k2eNgInSc1d1	nepřenositelný
(	(	kIx(	(
<g/>
zakotvený	zakotvený	k2eAgInSc1d1	zakotvený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ambonu	ambon	k1gInSc2	ambon
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
znít	znít	k5eAaImF	znít
veškeré	veškerý	k3xTgNnSc4	veškerý
Boží	boží	k2eAgNnSc4d1	boží
slovo	slovo	k1gNnSc4	slovo
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
čtení	čtení	k1gNnSc1	čtení
<g/>
,	,	kIx,	,
žalm	žalm	k1gInSc1	žalm
<g/>
,	,	kIx,	,
evangelium	evangelium	k1gNnSc1	evangelium
a	a	k8xC	a
kázání	kázání	k1gNnSc1	kázání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
–	–	k?	–
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
a	a	k8xC	a
všem	všecek	k3xTgMnPc3	všecek
přítomným	přítomný	k1gMnPc3	přítomný
má	mít	k5eAaImIp3nS	mít
neustále	neustále	k6eAd1	neustále
připomínat	připomínat	k5eAaImF	připomínat
Krista	Krista	k1gFnSc1	Krista
Kazatelna	kazatelna	k1gFnSc1	kazatelna
Křtitelnice	křtitelnice	k1gFnSc2	křtitelnice
Sedile	sedile	k1gNnSc2	sedile
Kropenka	kropenka	k1gFnSc1	kropenka
–	–	k?	–
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
má	mít	k5eAaImIp3nS	mít
připomenout	připomenout	k5eAaPmF	připomenout
křest	křest	k1gInSc4	křest
Zpovědnice	zpovědnice	k1gFnSc2	zpovědnice
Křížová	Křížová	k1gFnSc1	Křížová
cesta	cesta	k1gFnSc1	cesta
Věčné	věčný	k2eAgNnSc1d1	věčné
světlo	světlo	k1gNnSc1	světlo
–	–	k?	–
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
Krista	Kristus	k1gMnSc2	Kristus
ve	v	k7c6	v
svatostánku	svatostánek	k1gInSc6	svatostánek
Svatostánek	svatostánek	k1gInSc1	svatostánek
–	–	k?	–
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
proměněné	proměněný	k2eAgFnPc4d1	proměněná
hostie	hostie	k1gFnPc4	hostie
<g/>
.	.	kIx.	.
</s>
<s>
Svatostánek	svatostánek	k1gInSc1	svatostánek
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
,	,	kIx,	,
neprůhledný	průhledný	k2eNgInSc1d1	neprůhledný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
že	že	k8xS	že
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
proměnění	proměnění	k1gNnSc4	proměnění
je	být	k5eAaImIp3nS	být
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
praktické	praktický	k2eAgNnSc1d1	praktické
jej	on	k3xPp3gMnSc4	on
umístit	umístit	k5eAaPmF	umístit
např.	např.	kA	např.
do	do	k7c2	do
postranní	postranní	k2eAgFnSc2d1	postranní
části	část	k1gFnSc2	část
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prostor	prostor	k1gInSc4	prostor
kolem	kolem	k7c2	kolem
oltáře	oltář	k1gInSc2	oltář
nebyl	být	k5eNaImAgInS	být
znesvěcen	znesvětit	k5eAaPmNgInS	znesvětit
pohybem	pohyb	k1gInSc7	pohyb
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
–	–	k?	–
průrazný	průrazný	k2eAgInSc1d1	průrazný
zvuk	zvuk	k1gInSc1	zvuk
zvonu	zvon	k1gInSc2	zvon
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
a	a	k8xC	a
dálky	dálka	k1gFnSc2	dálka
tento	tento	k3xDgInSc1	tento
zvuk	zvuk	k1gInSc1	zvuk
také	také	k9	také
evokuje	evokovat	k5eAaBmIp3nS	evokovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvuk	zvuk	k1gInSc1	zvuk
představuje	představovat	k5eAaImIp3nS	představovat
poselství	poselství	k1gNnSc4	poselství
dálky	dálka	k1gFnSc2	dálka
–	–	k?	–
poselství	poselství	k1gNnSc6	poselství
o	o	k7c6	o
Bohu	bůh	k1gMnSc3	bůh
bez	bez	k7c2	bez
mezí	mez	k1gFnPc2	mez
a	a	k8xC	a
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc1	varhany
–	–	k?	–
liturgický	liturgický	k2eAgInSc4d1	liturgický
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Píšťaly	píšťala	k1gFnPc1	píšťala
varhan	varhany	k1gFnPc2	varhany
mají	mít	k5eAaImIp3nP	mít
navodit	navodit	k5eAaBmF	navodit
náladu	nálada	k1gFnSc4	nálada
k	k	k7c3	k
soustředění	soustředění	k1gNnSc3	soustředění
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
rejstříků	rejstřík	k1gInPc2	rejstřík
možnosti	možnost	k1gFnSc2	možnost
mnoha	mnoho	k4c2	mnoho
duchovních	duchovní	k2eAgFnPc2d1	duchovní
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Sedes	Sedes	k1gMnSc1	Sedes
–	–	k?	–
Sedadlo	sedadlo	k1gNnSc4	sedadlo
pro	pro	k7c4	pro
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
Kostelní	kostelní	k2eAgFnSc2d1	kostelní
lavice	lavice	k1gFnSc2	lavice
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
stalla	stalla	k1gFnSc1	stalla
Gotika	gotik	k1gMnSc2	gotik
–	–	k?	–
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
východu	východ	k1gInSc3	východ
i	i	k9	i
k	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
Baroko	baroko	k1gNnSc4	baroko
–	–	k?	–
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
lodi	loď	k1gFnSc6	loď
směřují	směřovat	k5eAaImIp3nP	směřovat
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
do	do	k7c2	do
středu	střed	k1gInSc2	střed
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
k	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
<g/>
)	)	kIx)	)
Dnes	dnes	k6eAd1	dnes
–	–	k?	–
většinou	většinou	k6eAd1	většinou
zaplňují	zaplňovat	k5eAaImIp3nP	zaplňovat
s	s	k7c7	s
oltářem	oltář	k1gInSc7	oltář
jeden	jeden	k4xCgInSc1	jeden
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
kolem	kolem	k7c2	kolem
Svíce	svíce	k1gFnSc2	svíce
Kalich	kalich	k1gInSc4	kalich
Patena	Paten	k2eAgNnPc4d1	Paten
Kadidlo	kadidlo	k1gNnSc4	kadidlo
a	a	k8xC	a
kadidelnice	kadidelnice	k1gFnSc1	kadidelnice
Plátno	plátno	k1gNnSc1	plátno
Monstrance	monstrance	k1gFnSc2	monstrance
Kněžské	kněžský	k2eAgNnSc1d1	kněžské
roucho	roucho	k1gNnSc1	roucho
Hostie	hostie	k1gFnSc1	hostie
Ciborium	ciborium	k1gNnSc1	ciborium
Svěcená	svěcený	k2eAgFnSc1d1	svěcená
voda	voda	k1gFnSc1	voda
Popel	popel	k1gInSc1	popel
Chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
víno	víno	k1gNnSc1	víno
</s>
