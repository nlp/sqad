<p>
<s>
Golden	Goldna	k1gFnPc2	Goldna
State	status	k1gInSc5	status
Warriors	Warriors	k1gInSc1	Warriors
je	být	k5eAaImIp3nS	být
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
ligu	liga	k1gFnSc4	liga
National	National	k1gFnSc1	National
Basketball	Basketball	k1gMnSc1	Basketball
Association	Association	k1gInSc1	Association
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
divize	divize	k1gFnSc2	divize
Západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tým	tým	k1gInSc1	tým
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Warriors	Warriors	k1gInSc1	Warriors
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
nosil	nosit	k5eAaImAgInS	nosit
tyto	tento	k3xDgInPc4	tento
názvy	název	k1gInPc4	název
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Warriors	Warriorsa	k1gFnPc2	Warriorsa
<g/>
:	:	kIx,	:
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
Warriors	Warriorsa	k1gFnPc2	Warriorsa
<g/>
:	:	kIx,	:
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Golden	Goldna	k1gFnPc2	Goldna
State	status	k1gInSc5	status
Warriors	Warriorsa	k1gFnPc2	Warriorsa
<g/>
:	:	kIx,	:
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
současnostZa	současnostZa	k1gMnSc1	současnostZa
svou	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
dokázali	dokázat	k5eAaPmAgMnP	dokázat
Warriors	Warriors	k1gInSc4	Warriors
celkem	celkem	k6eAd1	celkem
sedmkrát	sedmkrát	k6eAd1	sedmkrát
vyhrát	vyhrát	k5eAaPmF	vyhrát
play-off	playff	k1gInSc4	play-off
své	svůj	k3xOyFgFnSc2	svůj
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pětkrát	pětkrát	k6eAd1	pětkrát
následně	následně	k6eAd1	následně
i	i	k9	i
finále	finále	k1gNnSc4	finále
celé	celý	k2eAgFnSc2d1	celá
NBA	NBA	kA	NBA
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
NBA	NBA	kA	NBA
<g/>
/	/	kIx~	/
<g/>
BAA	BAA	kA	BAA
<g/>
:	:	kIx,	:
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
Ostatní	ostatní	k2eAgNnSc1d1	ostatní
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
konferenci	konference	k1gFnSc6	konference
<g/>
:	:	kIx,	:
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1967V	[number]	k4	1967V
sezoně	sezona	k1gFnSc6	sezona
2015-16	[number]	k4	2015-16
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
73	[number]	k4	73
utkání	utkání	k1gNnPc2	utkání
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
rekord	rekord	k1gInSc4	rekord
pro	pro	k7c4	pro
nejvíce	nejvíce	k6eAd1	nejvíce
výher	výhra	k1gFnPc2	výhra
jednoho	jeden	k4xCgInSc2	jeden
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistika	statistika	k1gFnSc1	statistika
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
NBA	NBA	kA	NBA
<g/>
/	/	kIx~	/
<g/>
BAA	BAA	kA	BAA
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Golden	Goldna	k1gFnPc2	Goldna
State	status	k1gInSc5	status
Warriors	Warriors	k1gInSc4	Warriors
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
