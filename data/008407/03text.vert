<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Alexander	Alexandra	k1gFnPc2	Alexandra
Bazovský	Bazovský	k1gMnSc1	Bazovský
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Turany	turan	k1gInPc1	turan
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
lidovým	lidový	k2eAgNnSc7d1	lidové
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
slovenské	slovenský	k2eAgFnSc2d1	slovenská
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
moderny	moderna	k1gFnSc2	moderna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
malířské	malířský	k2eAgFnSc6d1	malířská
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Detvianska	Detviansko	k1gNnPc1	Detviansko
melódia	melódium	k1gNnSc2	melódium
</s>
</p>
<p>
<s>
Detviansky	Detviansky	k6eAd1	Detviansky
laz	lazit	k5eAaImRp2nS	lazit
</s>
</p>
<p>
<s>
Na	na	k7c6	na
horný	horný	k1gMnSc5	horný
koniec	koniec	k1gInSc1	koniec
</s>
</p>
<p>
<s>
Horúci	Horúce	k1gMnPc1	Horúce
deň	deň	k?	deň
</s>
</p>
<p>
<s>
Námestie	Námestie	k1gFnSc1	Námestie
</s>
</p>
<p>
<s>
Z	z	k7c2	z
paše	paš	k1gInSc2	paš
</s>
</p>
<p>
<s>
Žltý	Žltý	k?	Žltý
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Malatiná	Malatinat	k5eAaPmIp3nS	Malatinat
</s>
</p>
<p>
<s>
Večer	večer	k6eAd1	večer
na	na	k7c6	na
dedine	dedin	k1gMnSc5	dedin
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Miloš	Miloš	k1gMnSc1	Miloš
Alexander	Alexandra	k1gFnPc2	Alexandra
Bazovský	Bazovský	k1gMnSc1	Bazovský
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Miloš	Miloš	k1gMnSc1	Miloš
Alexander	Alexandra	k1gFnPc2	Alexandra
Bazovský	Bazovský	k1gMnSc1	Bazovský
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Monografie	monografie	k1gFnSc1	monografie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Krátká	krátký	k2eAgFnSc1d1	krátká
biografie	biografie	k1gFnSc1	biografie
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnPc1	ukázka
děl	dělo	k1gNnPc2	dělo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
13	[number]	k4	13
Sk	Sk	kA	Sk
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
-	-	kIx~	-
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Bazovský	Bazovský	k2eAgMnSc1d1	Bazovský
Miloš	Miloš	k1gMnSc1	Miloš
Alexander	Alexandra	k1gFnPc2	Alexandra
</s>
</p>
