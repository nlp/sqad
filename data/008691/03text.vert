<p>
<s>
George	Georg	k1gMnSc2	Georg
Herbert	Herbert	k1gMnSc1	Herbert
Mead	Mead	k1gMnSc1	Mead
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1863	[number]	k4	1863
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
sociální	sociální	k2eAgMnSc1d1	sociální
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
filosofického	filosofický	k2eAgInSc2d1	filosofický
pragmatismu	pragmatismus	k1gInSc2	pragmatismus
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
řazený	řazený	k2eAgInSc4d1	řazený
ke	k	k7c3	k
klíčovým	klíčový	k2eAgFnPc3d1	klíčová
postavám	postava	k1gFnPc3	postava
symbolického	symbolický	k2eAgInSc2d1	symbolický
interakcionismu	interakcionismus	k1gInSc2	interakcionismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
osoba	osoba	k1gFnSc1	osoba
bývá	bývat	k5eAaImIp3nS	bývat
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
Univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
sociální	sociální	k2eAgFnSc2d1	sociální
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
větve	větev	k1gFnSc2	větev
americké	americký	k2eAgFnSc2d1	americká
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Mead	Mead	k1gMnSc1	Mead
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1863	[number]	k4	1863
ve	v	k7c6	v
městě	město	k1gNnSc6	město
South	Southa	k1gFnPc2	Southa
Hadley	Hadlea	k1gFnSc2	Hadlea
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
v	v	k7c6	v
protestantské	protestantský	k2eAgFnSc6d1	protestantská
úplné	úplný	k2eAgFnSc3d1	úplná
rodině	rodina	k1gFnSc3	rodina
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
otce	otec	k1gMnSc2	otec
Hirama	Hiram	k1gMnSc2	Hiram
Meada	Mead	k1gMnSc2	Mead
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
protestantským	protestantský	k2eAgMnSc7d1	protestantský
pastorem	pastor	k1gMnSc7	pastor
<g/>
,	,	kIx,	,
matky	matka	k1gFnPc4	matka
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Meadové	Meadový	k2eAgFnPc4d1	Meadová
rozené	rozený	k2eAgFnPc4d1	rozená
Billingsové	Billingsová	k1gFnPc4	Billingsová
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
sestry	sestra	k1gFnPc1	sestra
Alice	Alice	k1gFnSc2	Alice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
se	se	k3xPyFc4	se
George	George	k1gInSc1	George
Mead	Meada	k1gFnPc2	Meada
zapsal	zapsat	k5eAaPmAgInS	zapsat
na	na	k7c4	na
Oberlin	Oberlin	k2eAgInSc4d1	Oberlin
College	College	k1gInSc4	College
<g/>
,	,	kIx,	,
promoval	promovat	k5eAaBmAgMnS	promovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
s	s	k7c7	s
dobrými	dobrý	k2eAgInPc7d1	dobrý
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
na	na	k7c4	na
Oberlin	Oberlin	k2eAgInSc4d1	Oberlin
College	College	k1gInSc4	College
a	a	k8xC	a
následující	následující	k2eAgInPc4d1	následující
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
inspektor	inspektor	k1gMnSc1	inspektor
pro	pro	k7c4	pro
Wisconsin	Wisconsin	k1gInSc4	Wisconsin
Central	Central	k1gMnPc2	Central
Rail	Rail	k1gInSc1	Rail
Road	Roada	k1gFnPc2	Roada
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
se	se	k3xPyFc4	se
Mead	Mead	k1gMnSc1	Mead
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Harvardu	Harvard	k1gInSc6	Harvard
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
zajímala	zajímat	k5eAaImAgFnS	zajímat
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
studoval	studovat	k5eAaImAgMnS	studovat
s	s	k7c7	s
Josiahnem	Josiahn	k1gMnSc7	Josiahn
Roycem	Royce	k1gMnSc7	Royce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nejvíce	hodně	k6eAd3	hodně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
jeho	jeho	k3xOp3gNnSc4	jeho
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
Williamem	William	k1gInSc7	William
Jamesem	James	k1gMnSc7	James
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc3	jehož
dětem	dítě	k1gFnPc3	dítě
dělal	dělat	k5eAaImAgMnS	dělat
soukromého	soukromý	k2eAgMnSc4d1	soukromý
učitele	učitel	k1gMnSc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
Mead	Mead	k1gMnSc1	Mead
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
s	s	k7c7	s
magisterským	magisterský	k2eAgNnSc7d1	magisterské
titul	titul	k1gInSc4	titul
z	z	k7c2	z
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
studovat	studovat	k5eAaImF	studovat
u	u	k7c2	u
psychologa	psycholog	k1gMnSc2	psycholog
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Wundta	Wundt	k1gMnSc2	Wundt
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
koncept	koncept	k1gInSc4	koncept
"	"	kIx"	"
<g/>
gestikulace	gestikulace	k1gFnSc1	gestikulace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
pozdější	pozdní	k2eAgFnSc4d2	pozdější
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
Mead	Mead	k1gMnSc1	Mead
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Helen	Helena	k1gFnPc2	Helena
Castleovou	Castleův	k2eAgFnSc7d1	Castleův
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
jednoho	jeden	k4xCgMnSc4	jeden
přítele	přítel	k1gMnSc4	přítel
z	z	k7c2	z
Oberlin	Oberlina	k1gFnPc2	Oberlina
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mead	Mead	k1gMnSc1	Mead
nikdy	nikdy	k6eAd1	nikdy
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
svou	svůj	k3xOyFgFnSc4	svůj
dizertační	dizertační	k2eAgFnSc4d1	dizertační
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
post	post	k1gInSc4	post
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
H.	H.	kA	H.
Cooleym	Cooleym	k1gInSc4	Cooleym
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Deweym	Deweym	k1gInSc4	Deweym
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měli	mít	k5eAaImAgMnP	mít
nemalý	malý	k2eNgInSc4d1	nemalý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
Mead	Mead	k1gMnSc1	Mead
společně	společně	k6eAd1	společně
s	s	k7c7	s
Deweym	Deweym	k1gInSc1	Deweym
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
na	na	k7c4	na
Universitu	universita	k1gFnSc4	universita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Mead	Mead	k1gInSc1	Mead
učil	učít	k5eAaPmAgInS	učít
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Deweyův	Deweyův	k2eAgInSc1d1	Deweyův
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
Meada	Mead	k1gMnSc4	Mead
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c6	v
edukační	edukační	k2eAgFnSc6d1	edukační
theorii	theorie	k1gFnSc6	theorie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
názory	názor	k1gInPc4	názor
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
rozešly	rozejít	k5eAaPmAgInP	rozejít
a	a	k8xC	a
Mead	Mead	k1gInSc1	Mead
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
známou	známý	k2eAgFnSc4d1	známá
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
teorii	teorie	k1gFnSc4	teorie
o	o	k7c4	o
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
self	self	k1gInSc1	self
(	(	kIx(	(
<g/>
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mead	Mead	k1gMnSc1	Mead
nebyl	být	k5eNaImAgMnS	být
"	"	kIx"	"
<g/>
uzavřeným	uzavřený	k2eAgMnSc7d1	uzavřený
<g/>
"	"	kIx"	"
filozofem	filozof	k1gMnSc7	filozof
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
aktivity	aktivita	k1gFnPc4	aktivita
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
City	City	k1gFnSc4	City
Club	club	k1gInSc1	club
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věda	věda	k1gFnSc1	věda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
sociálních	sociální	k2eAgInPc2d1	sociální
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
výzkumu	výzkum	k1gInSc2	výzkum
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc1	srdce
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
mu	on	k3xPp3gMnSc3	on
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
žádná	žádný	k3yNgFnSc1	žádný
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgFnPc1	veškerý
jeho	jeho	k3xOp3gFnPc1	jeho
publikace	publikace	k1gFnPc1	publikace
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
rukopisů	rukopis	k1gInPc2	rukopis
či	či	k8xC	či
vydaných	vydaný	k2eAgInPc2d1	vydaný
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
popularizaci	popularizace	k1gFnSc6	popularizace
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílel	podílet	k5eAaImAgMnS	podílet
mj.	mj.	kA	mj.
Herbert	Herbert	k1gMnSc1	Herbert
Blumer	Blumer	k1gMnSc1	Blumer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
Meadovo	Meadův	k2eAgNnSc4d1	Meadův
dílo	dílo	k1gNnSc4	dílo
renesanci	renesance	k1gFnSc4	renesance
zájmu	zájem	k1gInSc2	zájem
především	především	k9	především
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
George	George	k1gNnSc4	George
Herbert	Herbert	k1gMnSc1	Herbert
Mead	Mead	k1gMnSc1	Mead
</s>
</p>
<p>
<s>
Myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
Filosofie	filosofie	k1gFnSc1	filosofie
činu	čin	k1gInSc2	čin
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
Filosofie	filosofie	k1gFnSc1	filosofie
přítomnosti	přítomnost	k1gFnSc2	přítomnost
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sociální	sociální	k2eAgInSc1d1	sociální
behaviorismus	behaviorismus	k1gInSc1	behaviorismus
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
své	svůj	k3xOyFgFnPc4	svůj
přírodní	přírodní	k2eAgFnPc4d1	přírodní
a	a	k8xC	a
přirozené	přirozený	k2eAgFnPc4d1	přirozená
dispozice	dispozice	k1gFnPc4	dispozice
a	a	k8xC	a
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
vědomí	vědomí	k1gNnSc1	vědomí
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
produktem	produkt	k1gInSc7	produkt
interakce	interakce	k1gFnSc2	interakce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zvířete	zvíře	k1gNnSc2	zvíře
není	být	k5eNaImIp3nS	být
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
přírodní	přírodní	k2eAgFnSc7d1	přírodní
bytostí	bytost	k1gFnSc7	bytost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
plánovat	plánovat	k5eAaImF	plánovat
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
možnostmi	možnost	k1gFnPc7	možnost
<g/>
,	,	kIx,	,
odkládat	odkládat	k5eAaImF	odkládat
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
reflektovat	reflektovat	k5eAaImF	reflektovat
svou	svůj	k3xOyFgFnSc4	svůj
minulost	minulost	k1gFnSc4	minulost
a	a	k8xC	a
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meadovým	Meadův	k2eAgInSc7d1	Meadův
klíčovým	klíčový	k2eAgInSc7d1	klíčový
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
sociální	sociální	k2eAgFnSc7d1	sociální
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
skupině	skupina	k1gFnSc6	skupina
probíhá	probíhat	k5eAaImIp3nS	probíhat
sociální	sociální	k2eAgFnSc1d1	sociální
interakce	interakce	k1gFnSc1	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
interakce	interakce	k1gFnSc1	interakce
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
je	být	k5eAaImIp3nS	být
klíčová	klíčový	k2eAgFnSc1d1	klíčová
pro	pro	k7c4	pro
utváření	utváření	k1gNnSc4	utváření
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
člověkem	člověk	k1gMnSc7	člověk
pouze	pouze	k6eAd1	pouze
pokud	pokud	k6eAd1	pokud
má	mít	k5eAaImIp3nS	mít
osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
utváření	utváření	k1gNnSc4	utváření
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
klíčová	klíčový	k2eAgFnSc1d1	klíčová
interakce	interakce	k1gFnSc1	interakce
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
člověkem	člověk	k1gMnSc7	člověk
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1	sociální
prostředí	prostředí	k1gNnSc1	prostředí
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
různými	různý	k2eAgInPc7d1	různý
stimuly	stimul	k1gInPc7	stimul
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kterých	který	k3yRgFnPc2	který
člověk	člověk	k1gMnSc1	člověk
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Stimuly	stimul	k1gInPc1	stimul
pocházející	pocházející	k2eAgInPc1d1	pocházející
ze	z	k7c2	z
sociálního	sociální	k2eAgNnSc2d1	sociální
prostředí	prostředí	k1gNnSc2	prostředí
však	však	k9	však
nevedou	vést	k5eNaImIp3nP	vést
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
přímo	přímo	k6eAd1	přímo
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
stimulem	stimul	k1gInSc7	stimul
a	a	k8xC	a
reakcí	reakce	k1gFnSc7	reakce
je	být	k5eAaImIp3nS	být
vsunut	vsunut	k2eAgInSc1d1	vsunut
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
člověk	člověk	k1gMnSc1	člověk
tento	tento	k3xDgInSc4	tento
stimul	stimul	k1gInSc4	stimul
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Způsoby	způsob	k1gInPc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
interpretovat	interpretovat	k5eAaBmF	interpretovat
stimuly	stimul	k1gInPc4	stimul
<g/>
,	,	kIx,	,
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
libovolné	libovolný	k2eAgFnPc1d1	libovolná
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
stimuly	stimul	k1gInPc4	stimul
přicházející	přicházející	k2eAgInPc4d1	přicházející
ze	z	k7c2	z
sociálního	sociální	k2eAgNnSc2d1	sociální
prostředí	prostředí	k1gNnSc2	prostředí
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byl	být	k5eAaImAgInS	být
sociální	sociální	k2eAgFnSc7d1	sociální
skupinou	skupina	k1gFnSc7	skupina
naučen	naučen	k2eAgMnSc1d1	naučen
je	on	k3xPp3gInPc4	on
interpretovat	interpretovat	k5eAaBmF	interpretovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Self	Self	k1gInSc4	Self
===	===	k?	===
</s>
</p>
<p>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
skupině	skupina	k1gFnSc6	skupina
učí	učit	k5eAaImIp3nS	učit
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
Mead	Mead	k1gMnSc1	Mead
Self	Self	k1gMnSc1	Self
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
osobnost	osobnost	k1gFnSc4	osobnost
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
procesu	proces	k1gInSc2	proces
utváření	utváření	k1gNnSc2	utváření
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
osobnost	osobnost	k1gFnSc1	osobnost
se	se	k3xPyFc4	se
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Mead	Mead	k6eAd1	Mead
totiž	totiž	k9	totiž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Self	Self	k1gInSc1	Self
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nikdy	nikdy	k6eAd1	nikdy
nekončí	končit	k5eNaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
hotová	hotový	k2eAgFnSc1d1	hotová
sociální	sociální	k2eAgFnSc1d1	sociální
bytost	bytost	k1gFnSc1	bytost
vybavená	vybavený	k2eAgFnSc1d1	vybavená
nějakou	nějaký	k3yIgFnSc7	nějaký
stálou	stálý	k2eAgFnSc7d1	stálá
strukturou	struktura	k1gFnSc7	struktura
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
utváření	utváření	k1gNnSc2	utváření
osobnosti	osobnost	k1gFnSc2	osobnost
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
socializace	socializace	k1gFnSc2	socializace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dvě	dva	k4xCgNnPc4	dva
stadia	stadion	k1gNnPc4	stadion
socializace	socializace	k1gFnSc2	socializace
–	–	k?	–
play	play	k0	play
x	x	k?	x
game	game	k1gInSc1	game
===	===	k?	===
</s>
</p>
<p>
<s>
Self	Self	k1gInSc1	Self
se	se	k3xPyFc4	se
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
socializace	socializace	k1gFnSc2	socializace
<g/>
.	.	kIx.	.
</s>
<s>
Mead	Mead	k6eAd1	Mead
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
dvě	dva	k4xCgNnPc4	dva
stadia	stadion	k1gNnPc4	stadion
socializace	socializace	k1gFnSc2	socializace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
na	na	k7c6	na
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Play	play	k0	play
====	====	k?	====
</s>
</p>
<p>
<s>
Prvním	první	k4xOgNnSc7	první
stadiem	stadion	k1gNnSc7	stadion
socializace	socializace	k1gFnSc2	socializace
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
typu	typ	k1gInSc2	typ
play	play	k0	play
(	(	kIx(	(
<g/>
hraní	hraní	k1gNnSc2	hraní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hraní	k1gNnSc1	hraní
je	být	k5eAaImIp3nS	být
pojímáno	pojímán	k2eAgNnSc1d1	pojímáno
jako	jako	k8xC	jako
něco	něco	k3yInSc1	něco
poměrně	poměrně	k6eAd1	poměrně
volného	volný	k2eAgNnSc2d1	volné
a	a	k8xC	a
nezávazného	závazný	k2eNgNnSc2d1	nezávazné
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
děti	dítě	k1gFnPc1	dítě
mohly	moct	k5eAaImAgFnP	moct
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
jen	jen	k9	jen
a	a	k8xC	a
<g/>
)	)	kIx)	)
existence	existence	k1gFnSc1	existence
rozmanitých	rozmanitý	k2eAgFnPc2d1	rozmanitá
různorodých	různorodý	k2eAgFnPc2d1	různorodá
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
)	)	kIx)	)
schopnost	schopnost	k1gFnSc1	schopnost
hrajících	hrající	k2eAgMnPc2d1	hrající
si	se	k3xPyFc3	se
přebírat	přebírat	k5eAaImF	přebírat
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hranit	k5eAaImIp3nS	hranit
je	on	k3xPp3gNnSc4	on
vždy	vždy	k6eAd1	vždy
hraním	hranit	k5eAaImIp1nS	hranit
si	se	k3xPyFc3	se
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	Dítě	k1gMnSc1	Dítě
si	se	k3xPyFc3	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
na	na	k7c4	na
doktora	doktor	k1gMnSc4	doktor
atd.	atd.	kA	atd.
V	v	k7c6	v
procesu	proces	k1gInSc6	proces
tohoto	tento	k3xDgNnSc2	tento
hraní	hraní	k1gNnSc2	hraní
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
přebírat	přebírat	k5eAaImF	přebírat
role	role	k1gFnPc4	role
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
si	se	k3xPyFc3	se
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
si	se	k3xPyFc3	se
dítě	dítě	k1gNnSc1	dítě
hraje	hrát	k5eAaImIp3nS	hrát
např.	např.	kA	např.
na	na	k7c4	na
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
roli	role	k1gFnSc4	role
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hraní	k1gNnSc1	hraní
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
definováno	definovat	k5eAaBmNgNnS	definovat
pouze	pouze	k6eAd1	pouze
rolemi	role	k1gFnPc7	role
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
nijak	nijak	k6eAd1	nijak
závislé	závislý	k2eAgNnSc4d1	závislé
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
sehrávají	sehrávat	k5eAaImIp3nP	sehrávat
tzv.	tzv.	kA	tzv.
významní	významný	k2eAgMnPc1d1	významný
druzí	druhý	k4xOgMnPc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Významní	významný	k2eAgMnPc1d1	významný
druzí	druhý	k4xOgMnPc1	druhý
jsou	být	k5eAaImIp3nP	být
konkrétní	konkrétní	k2eAgMnPc1d1	konkrétní
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
má	mít	k5eAaImIp3nS	mít
dítě	dítě	k1gNnSc4	dítě
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
konkrétní	konkrétní	k2eAgMnSc1d1	konkrétní
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
učitelé	učitel	k1gMnPc1	učitel
apod.	apod.	kA	apod.
Dítě	Dítě	k1gMnSc1	Dítě
přejímá	přejímat	k5eAaImIp3nS	přejímat
jejich	jejich	k3xOp3gInSc4	jejich
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
výkon	výkon	k1gInSc4	výkon
role	role	k1gFnSc2	role
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Game	game	k1gInSc4	game
====	====	k?	====
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
stadiem	stadion	k1gNnSc7	stadion
socializace	socializace	k1gFnSc2	socializace
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
typu	typ	k1gInSc2	typ
game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stadiu	stadion	k1gNnSc6	stadion
si	se	k3xPyFc3	se
už	už	k9	už
dítě	dítě	k1gNnSc4	dítě
nehraje	hrát	k5eNaImIp3nS	hrát
samo	sám	k3xTgNnSc1	sám
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
ostatní	ostatní	k2eAgNnSc4d1	ostatní
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
hraje	hrát	k5eAaImIp3nS	hrát
hru	hra	k1gFnSc4	hra
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
se	s	k7c7	s
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
nějaká	nějaký	k3yIgNnPc4	nějaký
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vždy	vždy	k6eAd1	vždy
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hrána	hrát	k5eAaImNgFnS	hrát
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
typu	typ	k1gInSc2	typ
game	game	k1gInSc1	game
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
současně	současně	k6eAd1	současně
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pravidly	pravidlo	k1gNnPc7	pravidlo
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
;	;	kIx,	;
naše	náš	k3xOp1gFnPc1	náš
reakce	reakce	k1gFnPc1	reakce
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
musíme	muset	k5eAaImIp1nP	muset
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
také	také	k9	také
reakce	reakce	k1gFnSc2	reakce
ostatních	ostatní	k2eAgMnPc2d1	ostatní
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
typu	typ	k1gInSc2	typ
game	game	k1gInSc1	game
je	být	k5eAaImIp3nS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
postoj	postoj	k1gInSc1	postoj
jednoho	jeden	k4xCgNnSc2	jeden
individua	individuum	k1gNnSc2	individuum
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
přiměřenou	přiměřený	k2eAgFnSc4d1	přiměřená
reakci	reakce	k1gFnSc4	reakce
druhého	druhý	k4xOgMnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Činnosti	činnost	k1gFnPc1	činnost
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
závislé	závislý	k2eAgInPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
všichni	všechen	k3xTgMnPc1	všechen
účastníci	účastník	k1gMnPc1	účastník
hry	hra	k1gFnSc2	hra
chtějí	chtít	k5eAaImIp3nP	chtít
hrát	hrát	k5eAaImF	hrát
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
typu	typ	k1gInSc2	typ
game	game	k1gInSc4	game
už	už	k6eAd1	už
nehrají	hrát	k5eNaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
významní	významný	k2eAgMnPc1d1	významný
druzí	druhý	k4xOgMnPc1	druhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tzv.	tzv.	kA	tzv.
generalizovaný	generalizovaný	k2eAgMnSc1d1	generalizovaný
(	(	kIx(	(
<g/>
zobecněný	zobecněný	k2eAgMnSc1d1	zobecněný
<g/>
)	)	kIx)	)
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Generalizovaný	generalizovaný	k2eAgInSc4d1	generalizovaný
druhý	druhý	k4xOgInSc4	druhý
je	být	k5eAaImIp3nS	být
souhrnem	souhrnem	k6eAd1	souhrnem
postojů	postoj	k1gInPc2	postoj
dané	daný	k2eAgFnSc2d1	daná
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
stálost	stálost	k1gFnSc1	stálost
generalizovaného	generalizovaný	k2eAgInSc2d1	generalizovaný
druhého	druhý	k4xOgMnSc4	druhý
je	být	k5eAaImIp3nS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
především	především	k9	především
abstraktností	abstraktnost	k1gFnSc7	abstraktnost
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zosobněná	zosobněný	k2eAgFnSc1d1	zosobněná
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
postoje	postoj	k1gInPc1	postoj
generalizovaného	generalizovaný	k2eAgInSc2d1	generalizovaný
druhého	druhý	k4xOgMnSc4	druhý
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
socializace	socializace	k1gFnSc2	socializace
stávají	stávat	k5eAaImIp3nP	stávat
součástí	součást	k1gFnSc7	součást
Self	Self	k1gInSc1	Self
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
součásti	součást	k1gFnSc3	součást
Self	Self	k1gMnSc1	Self
říká	říkat	k5eAaImIp3nS	říkat
Mead	Mead	k1gMnSc1	Mead
Me	Me	k1gMnSc1	Me
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
generalizovaného	generalizovaný	k2eAgInSc2d1	generalizovaný
druhého	druhý	k4xOgInSc2	druhý
nemůže	moct	k5eNaImIp3nS	moct
Self	Self	k1gInSc4	Self
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Meada	Meado	k1gNnSc2	Meado
člověk	člověk	k1gMnSc1	člověk
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zvířete	zvíře	k1gNnSc2	zvíře
není	být	k5eNaImIp3nS	být
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
přírodní	přírodní	k2eAgFnSc7d1	přírodní
bytostí	bytost	k1gFnSc7	bytost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
osobností	osobnost	k1gFnSc7	osobnost
vybavenou	vybavený	k2eAgFnSc7d1	vybavená
Self	Self	k1gInSc4	Self
a	a	k8xC	a
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Mít	mít	k5eAaImF	mít
Self	Self	k1gInSc4	Self
a	a	k8xC	a
vědomí	vědomí	k1gNnSc4	vědomí
podle	podle	k7c2	podle
Meada	Meado	k1gNnSc2	Meado
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
vědomí	vědomí	k1gNnSc4	vědomí
sama	sám	k3xTgMnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pohlédnout	pohlédnout	k5eAaPmF	pohlédnout
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
jako	jako	k8xS	jako
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
reflektovat	reflektovat	k5eAaImF	reflektovat
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
plánovat	plánovat	k5eAaImF	plánovat
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
možnostmi	možnost	k1gFnPc7	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
mít	mít	k5eAaImF	mít
Self	Self	k1gInSc4	Self
a	a	k8xC	a
vědomí	vědomí	k1gNnSc4	vědomí
také	také	k9	také
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pohlédnout	pohlédnout	k5eAaPmF	pohlédnout
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
jako	jako	k8xC	jako
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zaujmout	zaujmout	k5eAaPmF	zaujmout
roli	role	k1gFnSc4	role
druhého	druhý	k4xOgInSc2	druhý
<g/>
,	,	kIx,	,
představit	představit	k5eAaPmF	představit
si	se	k3xPyFc3	se
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
roli	role	k1gFnSc6	role
druhého	druhý	k4xOgMnSc2	druhý
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
rozumět	rozumět	k5eAaImF	rozumět
sociálním	sociální	k2eAgFnPc3d1	sociální
situacím	situace	k1gFnPc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
schopnosti	schopnost	k1gFnPc1	schopnost
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
učí	učit	k5eAaImIp3nS	učit
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
socializace	socializace	k1gFnSc2	socializace
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
hrách	hra	k1gFnPc6	hra
typu	typ	k1gInSc2	typ
play	play	k0	play
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Self	Self	k1gInSc1	Self
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
===	===	k?	===
</s>
</p>
<p>
<s>
Ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
Self	Self	k1gInSc4	Self
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Mead	Mead	k1gInSc1	Mead
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Me	Me	k1gFnSc1	Me
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
Self	Self	k1gInSc1	Self
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
relativní	relativní	k2eAgFnSc4d1	relativní
stálost	stálost	k1gFnSc4	stálost
<g/>
.	.	kIx.	.
</s>
<s>
Self	Self	k1gInSc1	Self
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
mimo	mimo	k7c4	mimo
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
nositel	nositel	k1gMnSc1	nositel
Self	Self	k1gMnSc1	Self
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
osobnost	osobnost	k1gFnSc1	osobnost
(	(	kIx(	(
<g/>
Self	Self	k1gInSc1	Self
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jen	jen	k9	jen
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
používají	používat	k5eAaImIp3nP	používat
abstraktní	abstraktní	k2eAgNnPc1d1	abstraktní
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Me	Me	k?	Me
dává	dávat	k5eAaImIp3nS	dávat
Self	Self	k1gInSc1	Self
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
relativní	relativní	k2eAgFnSc4d1	relativní
stálost	stálost	k1gFnSc4	stálost
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
postoje	postoj	k1gInSc2	postoj
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Me	Me	k1gFnSc1	Me
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
individuální	individuální	k2eAgFnSc1d1	individuální
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Mead	Mead	k1gInSc1	Mead
nazývá	nazývat	k5eAaImIp3nS	nazývat
I	i	k9	i
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
složky	složka	k1gFnPc1	složka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
uvnitř	uvnitř	k6eAd1	uvnitř
Self	Self	k1gInSc4	Self
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
je	být	k5eAaImIp3nS	být
tvořivým	tvořivý	k2eAgInSc7d1	tvořivý
a	a	k8xC	a
aktivním	aktivní	k2eAgInSc7d1	aktivní
činitelem	činitel	k1gInSc7	činitel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
interpretaci	interpretace	k1gFnSc4	interpretace
a	a	k8xC	a
jednání	jednání	k1gNnSc4	jednání
nositele	nositel	k1gMnSc2	nositel
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
Self	Self	k1gMnSc1	Self
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Meada	Mead	k1gMnSc2	Mead
probíhá	probíhat	k5eAaImIp3nS	probíhat
uvnitř	uvnitř	k6eAd1	uvnitř
Self	Self	k1gInSc1	Self
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
I	I	kA	I
tvořivě	tvořivě	k6eAd1	tvořivě
a	a	k8xC	a
aktivně	aktivně	k6eAd1	aktivně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
stimuly	stimul	k1gInPc4	stimul
pocházející	pocházející	k2eAgInPc4d1	pocházející
ze	z	k7c2	z
sociálního	sociální	k2eAgNnSc2d1	sociální
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
sice	sice	k8xC	sice
reaguje	reagovat	k5eAaBmIp3nS	reagovat
tvořivě	tvořivě	k6eAd1	tvořivě
a	a	k8xC	a
aktivně	aktivně	k6eAd1	aktivně
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nikoliv	nikoliv	k9	nikoliv
libovolně	libovolně	k6eAd1	libovolně
<g/>
;	;	kIx,	;
reaguje	reagovat	k5eAaBmIp3nS	reagovat
totiž	totiž	k9	totiž
skrze	skrze	k?	skrze
Me	Me	k1gFnSc1	Me
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
skrze	skrze	k?	skrze
postoje	postoj	k1gInSc2	postoj
skupiny	skupina	k1gFnSc2	skupina
nebo	nebo	k8xC	nebo
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
postupně	postupně	k6eAd1	postupně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
socializace	socializace	k1gFnSc2	socializace
a	a	k8xC	a
sociálního	sociální	k2eAgNnSc2d1	sociální
učení	učení	k1gNnSc2	učení
člověk	člověk	k1gMnSc1	člověk
učí	učit	k5eAaImIp3nS	učit
nereagovat	reagovat	k5eNaBmF	reagovat
na	na	k7c4	na
situace	situace	k1gFnPc4	situace
instinktivně	instinktivně	k6eAd1	instinktivně
jako	jako	k8xS	jako
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgFnPc2	svůj
sociálních	sociální	k2eAgFnPc2d1	sociální
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
během	během	k7c2	během
života	život	k1gInSc2	život
získává	získávat	k5eAaImIp3nS	získávat
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
člověku	člověk	k1gMnSc6	člověk
jeho	jeho	k3xOp3gInSc1	jeho
instinkty	instinkt	k1gInPc1	instinkt
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c4	v
sociální	sociální	k2eAgInPc4d1	sociální
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Meada	Meado	k1gNnSc2	Meado
je	být	k5eAaImIp3nS	být
Self	Self	k1gInSc1	Self
vždy	vždy	k6eAd1	vždy
rozpornou	rozporný	k2eAgFnSc7d1	rozporná
jednotou	jednota	k1gFnSc7	jednota
I	i	k9	i
a	a	k8xC	a
Me	Me	k1gFnSc1	Me
<g/>
.	.	kIx.	.
</s>
<s>
Me	Me	k?	Me
není	být	k5eNaImIp3nS	být
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
stimul	stimul	k1gInSc4	stimul
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
morální	morální	k2eAgInSc1d1	morální
závazek	závazek	k1gInSc1	závazek
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
způsob	způsob	k1gInSc4	způsob
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
komunitou	komunita	k1gFnSc7	komunita
očekáváno	očekávat	k5eAaImNgNnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Me	Me	k?	Me
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jakousi	jakýsi	k3yIgFnSc7	jakýsi
sociální	sociální	k2eAgFnSc7d1	sociální
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
limity	limita	k1gFnPc4	limita
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složka	složka	k1gFnSc1	složka
Me	Me	k1gFnSc2	Me
v	v	k7c4	v
Self	Self	k1gInSc4	Self
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
postoji	postoj	k1gInPc7	postoj
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
nebo	nebo	k8xC	nebo
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
postoje	postoj	k1gInPc1	postoj
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
souhrn	souhrn	k1gInSc4	souhrn
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
Mead	Mead	k1gInSc1	Mead
říká	říkat	k5eAaImIp3nS	říkat
generalizovaný	generalizovaný	k2eAgInSc1d1	generalizovaný
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Generalizovaného	generalizovaný	k2eAgInSc2d1	generalizovaný
druhého	druhý	k4xOgMnSc4	druhý
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
sociální	sociální	k2eAgFnSc4d1	sociální
skupinu	skupina	k1gFnSc4	skupina
nebo	nebo	k8xC	nebo
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
postoje	postoj	k1gInPc1	postoj
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
nebo	nebo	k8xC	nebo
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
stálé	stálý	k2eAgMnPc4d1	stálý
jen	jen	k9	jen
relativně	relativně	k6eAd1	relativně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
přetvářejí	přetvářet	k5eAaImIp3nP	přetvářet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
individua	individuum	k1gNnPc1	individuum
jednají	jednat	k5eAaImIp3nP	jednat
a	a	k8xC	a
tvořivě	tvořivě	k6eAd1	tvořivě
(	(	kIx(	(
<g/>
svou	svůj	k3xOyFgFnSc7	svůj
složkou	složka	k1gFnSc7	složka
I	i	k8xC	i
<g/>
)	)	kIx)	)
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
stimuly	stimul	k1gInPc4	stimul
pocházející	pocházející	k2eAgInPc4d1	pocházející
ze	z	k7c2	z
sociálního	sociální	k2eAgNnSc2d1	sociální
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejenom	nejenom	k6eAd1	nejenom
Self	Self	k1gInSc4	Self
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
nebo	nebo	k8xC	nebo
společnost	společnost	k1gFnSc1	společnost
jsou	být	k5eAaImIp3nP	být
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
postoje	postoj	k1gInPc1	postoj
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
Self	Self	k1gMnSc1	Self
člověka	člověk	k1gMnSc2	člověk
klíčové	klíčový	k2eAgNnSc4d1	klíčové
<g/>
.	.	kIx.	.
</s>
<s>
Mead	Mead	k6eAd1	Mead
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
vliv	vliv	k1gInSc4	vliv
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
<g/>
.	.	kIx.	.
</s>
<s>
Složku	složka	k1gFnSc4	složka
Me	Me	k1gFnSc2	Me
uvnitř	uvnitř	k6eAd1	uvnitř
Self	Self	k1gMnSc1	Self
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
prvořadou	prvořadý	k2eAgFnSc4d1	prvořadá
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
se	se	k3xPyFc4	se
však	však	k9	však
vzdát	vzdát	k5eAaPmF	vzdát
ani	ani	k8xC	ani
individuální	individuální	k2eAgFnPc4d1	individuální
tvořivosti	tvořivost	k1gFnPc4	tvořivost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
aktivní	aktivní	k2eAgFnSc6d1	aktivní
a	a	k8xC	a
tvořivé	tvořivý	k2eAgFnSc6d1	tvořivá
složce	složka	k1gFnSc6	složka
I.	I.	kA	I.
Svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
však	však	k9	však
věnuje	věnovat	k5eAaPmIp3nS	věnovat
především	především	k6eAd1	především
složce	složka	k1gFnSc6	složka
Me	Me	k1gFnSc6	Me
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
přisoudit	přisoudit	k5eAaPmF	přisoudit
tendencím	tendence	k1gFnPc3	tendence
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
kulturně	kulturně	k6eAd1	kulturně
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
rozrůzněné	rozrůzněný	k2eAgFnSc2d1	rozrůzněná
situace	situace	k1gFnSc2	situace
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
nutnost	nutnost	k1gFnSc4	nutnost
hledat	hledat	k5eAaImF	hledat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
společnost	společnost	k1gFnSc4	společnost
integrovat	integrovat	k5eAaBmF	integrovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
and	and	k?	and
the	the	k?	the
'	'	kIx"	'
<g/>
me	me	k?	me
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
George_Herbert_Mead	George_Herbert_Mead	k1gInSc1	George_Herbert_Mead
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
MEAD	MEAD	kA	MEAD
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Mysl	mysl	k1gFnSc1	mysl
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
1180	[number]	k4	1180
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUBÁTOVÁ	Kubátová	k1gFnSc1	Kubátová
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
2314	[number]	k4	2314
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
