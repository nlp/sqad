<s>
George	Georg	k1gMnSc2	Georg
Herbert	Herbert	k1gMnSc1	Herbert
Mead	Mead	k1gMnSc1	Mead
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1863	[number]	k4	1863
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
sociální	sociální	k2eAgMnSc1d1	sociální
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
filosofického	filosofický	k2eAgInSc2d1	filosofický
pragmatismu	pragmatismus	k1gInSc2	pragmatismus
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
řazený	řazený	k2eAgInSc4d1	řazený
ke	k	k7c3	k
klíčovým	klíčový	k2eAgFnPc3d1	klíčová
postavám	postava	k1gFnPc3	postava
symbolického	symbolický	k2eAgInSc2d1	symbolický
interakcionismu	interakcionismus	k1gInSc2	interakcionismus
<g/>
.	.	kIx.	.
</s>
