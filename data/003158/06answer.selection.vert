<s>
Sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
s	s	k7c7	s
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nesprávně	správně	k6eNd1	správně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
sec	sec	k1gInSc1	sec
<g/>
,	,	kIx,	,
sek	sek	k1gInSc1	sek
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
<g/>
:	:	kIx,	:
doby	doba	k1gFnPc1	doba
<g/>
)	)	kIx)	)
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
