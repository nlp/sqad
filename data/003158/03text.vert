<s>
Sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
s	s	k7c7	s
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nesprávně	správně	k6eNd1	správně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
sec	sec	k1gInSc1	sec
<g/>
,	,	kIx,	,
sek	sek	k1gInSc1	sek
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
<g/>
:	:	kIx,	:
doby	doba	k1gFnPc1	doba
<g/>
)	)	kIx)	)
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
bývá	bývat	k5eAaImIp3nS	bývat
užíváno	užíván	k2eAgNnSc4d1	užíváno
i	i	k8xC	i
původní	původní	k2eAgNnSc4d1	původní
označení	označení	k1gNnSc4	označení
vteřina	vteřina	k1gFnSc1	vteřina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ale	ale	k8xC	ale
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
není	být	k5eNaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
.	.	kIx.	.
</s>
<s>
Sekunda	sekunda	k1gFnSc1	sekunda
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
9	[number]	k4	9
192	[number]	k4	192
631	[number]	k4	631
770	[number]	k4	770
period	perioda	k1gFnPc2	perioda
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hladinami	hladina	k1gFnPc7	hladina
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgFnPc4d1	jemná
struktury	struktura	k1gFnPc4	struktura
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
atomu	atom	k1gInSc2	atom
133	[number]	k4	133
<g/>
Cs	Cs	k1gFnPc2	Cs
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
cesiový	cesiový	k2eAgInSc4d1	cesiový
atom	atom	k1gInSc4	atom
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
jednotku	jednotka	k1gFnSc4	jednotka
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
vnějších	vnější	k2eAgInPc2d1	vnější
vlivů	vliv	k1gInPc2	vliv
(	(	kIx(	(
<g/>
toho	ten	k3xDgMnSc4	ten
nelze	lze	k6eNd1	lze
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
měří	měřit	k5eAaImIp3nS	měřit
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
řádu	řád	k1gInSc2	řád
mikrokelvinů	mikrokelvin	k1gMnPc2	mikrokelvin
a	a	k8xC	a
zavádějí	zavádět	k5eAaImIp3nP	zavádět
se	se	k3xPyFc4	se
korekce	korekce	k1gFnSc2	korekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
jednotky	jednotka	k1gFnSc2	jednotka
je	být	k5eAaImIp3nS	být
odvozen	odvozen	k2eAgMnSc1d1	odvozen
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
po	po	k7c6	po
minutách	minuta	k1gFnPc6	minuta
o	o	k7c4	o
druhé	druhý	k4xOgNnSc4	druhý
dělení	dělení	k1gNnSc4	dělení
hodiny	hodina	k1gFnSc2	hodina
-	-	kIx~	-
latinsky	latinsky	k6eAd1	latinsky
pars	pars	k6eAd1	pars
minuta	minut	k2eAgFnSc1d1	minuta
secunda	secunda	k1gFnSc1	secunda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Minuta	minuta	k1gFnSc1	minuta
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
pars	pars	k6eAd1	pars
minuta	minut	k2eAgFnSc1d1	minuta
prima	prima	k1gFnSc1	prima
-	-	kIx~	-
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc1	první
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
(	(	kIx(	(
<g/>
Aliboron	Aliboron	k1gMnSc1	Aliboron
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
<g />
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gInSc1	Bacon
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
ještě	ještě	k9	ještě
další	další	k2eAgNnSc1d1	další
šedesátinné	šedesátinný	k2eAgNnSc1d1	šedesátinný
dělení	dělení	k1gNnSc1	dělení
např.	např.	kA	např.
tercie	tercie	k1gFnSc2	tercie
(	(	kIx(	(
<g/>
pars	pars	k6eAd1	pars
minuta	minut	k2eAgFnSc1d1	minuta
tertia	tertia	k1gFnSc1	tertia
<g/>
)	)	kIx)	)
či	či	k8xC	či
kvarta	kvarta	k1gFnSc1	kvarta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
už	už	k9	už
sekunda	sekunda	k1gFnSc1	sekunda
v	v	k7c6	v
SI	si	k1gNnSc6	si
dělí	dělit	k5eAaImIp3nS	dělit
pouze	pouze	k6eAd1	pouze
desetinně	desetinně	k6eAd1	desetinně
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
však	však	k9	však
slovo	slovo	k1gNnSc1	slovo
pro	pro	k7c4	pro
šedesátinu	šedesátina	k1gFnSc4	šedesátina
sekundy	sekunda	k1gFnSc2	sekunda
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
neužívané	užívaný	k2eNgFnPc1d1	neužívaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
tercja	tercjus	k1gMnSc2	tercjus
<g/>
,	,	kIx,	,
kwarta	kwart	k1gMnSc2	kwart
či	či	k8xC	či
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
ث	ث	k?	ث
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
vzoru	vzor	k1gInSc2	vzor
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
i	i	k8xC	i
nové	nový	k2eAgNnSc1d1	nové
české	český	k2eAgNnSc1d1	české
synonymum	synonymum	k1gNnSc1	synonymum
vteřina	vteřina	k1gFnSc1	vteřina
(	(	kIx(	(
<g/>
podrobněji	podrobně	k6eAd2	podrobně
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
článku	článek	k1gInSc6	článek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Dělitel	dělitel	k1gInSc1	dělitel
60	[number]	k4	60
(	(	kIx(	(
<g/>
sekund	sekunda	k1gFnPc2	sekunda
v	v	k7c6	v
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Babyloňanů	Babyloňan	k1gMnPc2	Babyloňan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
používali	používat	k5eAaImAgMnP	používat
šedesátkovou	šedesátkový	k2eAgFnSc4d1	šedesátková
číselnou	číselný	k2eAgFnSc4d1	číselná
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
na	na	k7c4	na
šedesátiny	šedesátina	k1gFnPc4	šedesátina
nedělili	dělit	k5eNaImAgMnP	dělit
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodina	hodina	k1gFnSc1	hodina
byla	být	k5eAaImAgFnS	být
definována	definován	k2eAgFnSc1d1	definována
starověkými	starověký	k2eAgMnPc7d1	starověký
Egypťany	Egypťan	k1gMnPc7	Egypťan
jako	jako	k8xC	jako
dvanáctina	dvanáctina	k1gFnSc1	dvanáctina
trvání	trvání	k1gNnSc2	trvání
dne	den	k1gInSc2	den
nebo	nebo	k8xC	nebo
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
kolísání	kolísání	k1gNnSc3	kolísání
délky	délka	k1gFnSc2	délka
dne	den	k1gInSc2	den
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Helénští	helénský	k2eAgMnPc1d1	helénský
astronomové	astronom	k1gMnPc1	astronom
včetně	včetně	k7c2	včetně
Hipparcha	Hipparch	k1gMnSc2	Hipparch
a	a	k8xC	a
Ptolemaia	Ptolemaios	k1gMnSc4	Ptolemaios
definovali	definovat	k5eAaBmAgMnP	definovat
hodinu	hodina	k1gFnSc4	hodina
jako	jako	k8xC	jako
čtyřiadvacetinu	čtyřiadvacetin	k2eAgFnSc4d1	čtyřiadvacetina
středního	střední	k2eAgInSc2d1	střední
slunečního	sluneční	k2eAgInSc2d1	sluneční
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dvojím	dvojit	k5eAaImIp1nS	dvojit
šedesátinným	šedesátinný	k2eAgNnSc7d1	šedesátinný
dělením	dělení	k1gNnSc7	dělení
této	tento	k3xDgFnSc2	tento
hodiny	hodina	k1gFnSc2	hodina
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
definice	definice	k1gFnSc1	definice
sekundy	sekunda	k1gFnSc2	sekunda
jako	jako	k9	jako
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
86	[number]	k4	86
400	[number]	k4	400
středního	střední	k2eAgInSc2d1	střední
slunečního	sluneční	k2eAgInSc2d1	sluneční
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Helénské	helénský	k2eAgFnPc1d1	helénská
časové	časový	k2eAgFnPc1d1	časová
periody	perioda	k1gFnPc1	perioda
jako	jako	k8xC	jako
např.	např.	kA	např.
synodický	synodický	k2eAgInSc4d1	synodický
měsíc	měsíc	k1gInSc4	měsíc
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
stanoveny	stanovit	k5eAaPmNgInP	stanovit
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byly	být	k5eAaImAgFnP	být
vypočítány	vypočítat	k5eAaPmNgFnP	vypočítat
z	z	k7c2	z
pečlivě	pečlivě	k6eAd1	pečlivě
vybraných	vybraný	k2eAgNnPc2d1	vybrané
zatmění	zatmění	k1gNnPc2	zatmění
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgMnPc7	který
uběhly	uběhnout	k5eAaPmAgInP	uběhnout
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
-	-	kIx~	-
samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
průměrné	průměrný	k2eAgInPc1d1	průměrný
synodické	synodický	k2eAgInPc1d1	synodický
měsíce	měsíc	k1gInPc1	měsíc
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
časové	časový	k2eAgFnPc1d1	časová
periody	perioda	k1gFnPc1	perioda
nedají	dát	k5eNaPmIp3nP	dát
změřit	změřit	k5eAaPmF	změřit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
hodinového	hodinový	k2eAgNnSc2d1	hodinové
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
průměrného	průměrný	k2eAgInSc2d1	průměrný
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
zdánlivému	zdánlivý	k2eAgInSc3d1	zdánlivý
času	čas	k1gInSc3	čas
zobrazovaném	zobrazovaný	k2eAgInSc6d1	zobrazovaný
slunečními	sluneční	k2eAgFnPc7d1	sluneční
hodinami	hodina	k1gFnPc7	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sekunda	sekunda	k1gFnSc1	sekunda
stala	stát	k5eAaPmAgFnS	stát
měřitelnou	měřitelný	k2eAgFnSc7d1	měřitelná
<g/>
.	.	kIx.	.
</s>
<s>
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
používat	používat	k5eAaImF	používat
sekundové	sekundový	k2eAgNnSc4d1	sekundové
kyvadlo	kyvadlo	k1gNnSc4	kyvadlo
jako	jako	k8xS	jako
jednotku	jednotka	k1gFnSc4	jednotka
délky	délka	k1gFnSc2	délka
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
předložili	předložit	k5eAaPmAgMnP	předložit
Adolf	Adolf	k1gMnSc1	Adolf
Scheibe	Scheib	k1gInSc5	Scheib
a	a	k8xC	a
Udo	Udo	k1gMnSc1	Udo
Adelsberger	Adelsberger	k1gMnSc1	Adelsberger
u	u	k7c2	u
Physikalisch-Technische	Physikalisch-Technisch	k1gInSc2	Physikalisch-Technisch
Bundesanstaltu	Bundesanstalt	k1gInSc2	Bundesanstalt
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
proveden	proveden	k2eAgInSc1d1	proveden
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
a	a	k8xC	a
po	po	k7c6	po
očištění	očištění	k1gNnSc6	očištění
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
pochybností	pochybnost	k1gFnPc2	pochybnost
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc1	rotace
Země	země	k1gFnSc1	země
není	být	k5eNaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
zpomalování	zpomalování	k1gNnSc1	zpomalování
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
některým	některý	k3yIgFnPc3	některý
nepravidelným	pravidelný	k2eNgFnPc3d1	nepravidelná
změnám	změna	k1gFnPc3	změna
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
prouděním	proudění	k1gNnSc7	proudění
magmatu	magma	k1gNnSc2	magma
mezi	mezi	k7c7	mezi
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
zemským	zemský	k2eAgNnSc7d1	zemské
jádrem	jádro	k1gNnSc7	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1	astronomická
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
špatným	špatný	k2eAgInSc7d1	špatný
základem	základ	k1gInSc7	základ
časových	časový	k2eAgFnPc2d1	časová
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zpomalování	zpomalování	k1gNnSc3	zpomalování
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
se	se	k3xPyFc4	se
tak	tak	k9	tak
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
oproti	oproti	k7c3	oproti
dennímu	denní	k2eAgInSc3d1	denní
času	čas	k1gInSc3	čas
posouvá	posouvat	k5eAaImIp3nS	posouvat
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kompenzaci	kompenzace	k1gFnSc3	kompenzace
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
přestupné	přestupný	k2eAgInPc1d1	přestupný
sekundy	sekund	k1gInPc1	sekund
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
běžně	běžně	k6eAd1	běžně
užívaný	užívaný	k2eAgInSc1d1	užívaný
čas	čas	k1gInSc1	čas
dostatečně	dostatečně	k6eAd1	dostatečně
přesný	přesný	k2eAgInSc1d1	přesný
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
nelišil	lišit	k5eNaImAgInS	lišit
od	od	k7c2	od
pohybu	pohyb	k1gInSc2	pohyb
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byla	být	k5eAaImAgFnS	být
sekunda	sekunda	k1gFnSc1	sekunda
definována	definovat	k5eAaBmNgFnS	definovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
doby	doba	k1gFnSc2	doba
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
ekvinokcium	ekvinokcium	k1gNnSc4	ekvinokcium
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
už	už	k6eAd1	už
zemská	zemský	k2eAgFnSc1d1	zemská
rotace	rotace	k1gFnSc1	rotace
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
nepovažovala	považovat	k5eNaImAgFnS	považovat
za	za	k7c4	za
dostatečně	dostatečně	k6eAd1	dostatečně
rovnoměrnou	rovnoměrný	k2eAgFnSc4d1	rovnoměrná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
Země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
Newcombových	Newcombův	k2eAgFnPc6d1	Newcombův
slunečních	sluneční	k2eAgFnPc6d1	sluneční
tabulkách	tabulka	k1gFnPc6	tabulka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vzorec	vzorec	k1gInSc4	vzorec
popisující	popisující	k2eAgInSc1d1	popisující
pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
k	k	k7c3	k
ekvinokciu	ekvinokcium	k1gNnSc3	ekvinokcium
1900.0	[number]	k4	1900.0
na	na	k7c6	na
základě	základ	k1gInSc6	základ
astronomických	astronomický	k2eAgNnPc2d1	astronomické
pozorování	pozorování	k1gNnPc2	pozorování
provedených	provedený	k2eAgNnPc2d1	provedené
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1750	[number]	k4	1750
a	a	k8xC	a
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Sekunda	sekunda	k1gFnSc1	sekunda
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xS	jako
1	[number]	k4	1
/	/	kIx~	/
31	[number]	k4	31
556	[number]	k4	556
925,974	[number]	k4	925,974
<g/>
7	[number]	k4	7
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
pro	pro	k7c4	pro
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
0	[number]	k4	0
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1900	[number]	k4	1900
efemeridového	efemeridový	k2eAgInSc2d1	efemeridový
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
byla	být	k5eAaImAgFnS	být
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
generální	generální	k2eAgFnSc4d1	generální
konferenci	konference	k1gFnSc4	konference
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnPc4d1	konaná
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Tropický	tropický	k2eAgInSc4d1	tropický
rok	rok	k1gInSc4	rok
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
nebyl	být	k5eNaImAgInS	být
změřen	změřen	k2eAgInSc1d1	změřen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
vypočítán	vypočítat	k5eAaPmNgInS	vypočítat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzorce	vzorec	k1gInSc2	vzorec
popisujícího	popisující	k2eAgMnSc2d1	popisující
tropický	tropický	k2eAgInSc4d1	tropický
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
lineárně	lineárně	k6eAd1	lineárně
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
podivného	podivný	k2eAgInSc2d1	podivný
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
specifický	specifický	k2eAgInSc4d1	specifický
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
tropický	tropický	k2eAgInSc4d1	tropický
rok	rok	k1gInSc4	rok
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tato	tento	k3xDgFnSc1	tento
sekunda	sekunda	k1gFnSc1	sekunda
byla	být	k5eAaImAgFnS	být
nezávisle	závisle	k6eNd1	závisle
proměnná	proměnný	k2eAgFnSc1d1	proměnná
na	na	k7c6	na
čase	čas	k1gInSc6	čas
ve	v	k7c6	v
slunečních	sluneční	k2eAgFnPc6d1	sluneční
a	a	k8xC	a
měsíčních	měsíční	k2eAgFnPc6d1	měsíční
efemeridách	efemerida	k1gFnPc6	efemerida
během	během	k7c2	během
většiny	většina	k1gFnSc2	většina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Newcombovy	Newcombův	k2eAgFnPc1d1	Newcombův
tabulky	tabulka	k1gFnPc1	tabulka
Slunce	slunce	k1gNnSc2	slunce
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
až	až	k9	až
1983	[number]	k4	1983
a	a	k8xC	a
Brownovy	Brownův	k2eAgFnPc1d1	Brownova
tabulky	tabulka	k1gFnPc1	tabulka
Měsíce	měsíc	k1gInSc2	měsíc
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
mezi	mezi	k7c4	mezi
1920	[number]	k4	1920
až	až	k9	až
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
efemeridová	efemeridový	k2eAgFnSc1d1	efemeridový
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
atomových	atomový	k2eAgFnPc2d1	atomová
hodin	hodina	k1gFnPc2	hodina
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
užití	užití	k1gNnSc1	užití
bude	být	k5eAaImBp3nS	být
vhodnějším	vhodný	k2eAgInSc7d2	vhodnější
základem	základ	k1gInSc7	základ
definice	definice	k1gFnSc2	definice
sekundy	sekunda	k1gFnSc2	sekunda
než	než	k8xS	než
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
pracovali	pracovat	k5eAaImAgMnP	pracovat
Louis	Louis	k1gMnSc1	Louis
Essen	Essen	k1gInSc4	Essen
z	z	k7c2	z
anglické	anglický	k2eAgFnSc2d1	anglická
National	National	k1gFnSc2	National
Physical	Physical	k1gFnSc2	Physical
Laboratory	Laborator	k1gInPc1	Laborator
a	a	k8xC	a
William	William	k1gInSc1	William
Markowitz	Markowitza	k1gFnPc2	Markowitza
z	z	k7c2	z
United	United	k1gInSc1	United
States	States	k1gMnSc1	States
Naval	navalit	k5eAaPmRp2nS	navalit
Observatory	Observator	k1gMnPc7	Observator
(	(	kIx(	(
<g/>
USNO	USNO	kA	USNO
<g/>
)	)	kIx)	)
na	na	k7c4	na
určení	určení	k1gNnSc4	určení
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
frekvencí	frekvence	k1gFnSc7	frekvence
přechodů	přechod	k1gInPc2	přechod
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgFnPc1d1	jemná
struktury	struktura	k1gFnPc1	struktura
atomu	atom	k1gInSc2	atom
cesia	cesium	k1gNnSc2	cesium
a	a	k8xC	a
efemeridové	efemeridový	k2eAgFnSc2d1	efemeridový
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
měřící	měřící	k2eAgFnSc2d1	měřící
metody	metoda	k1gFnSc2	metoda
společného	společný	k2eAgNnSc2d1	společné
pozorování	pozorování	k1gNnSc2	pozorování
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
common-view	commoniew	k?	common-view
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
přijatých	přijatý	k2eAgInPc6d1	přijatý
signálech	signál	k1gInPc6	signál
z	z	k7c2	z
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
WWV	WWV	kA	WWV
určili	určit	k5eAaPmAgMnP	určit
oběžný	oběžný	k2eAgInSc4d1	oběžný
pohyb	pohyb	k1gInSc4	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
odvodit	odvodit	k5eAaPmF	odvodit
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
pohyb	pohyb	k1gInSc4	pohyb
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
odměřených	odměřený	k2eAgFnPc2d1	odměřená
atomovými	atomový	k2eAgFnPc7d1	atomová
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
výsledkem	výsledek	k1gInSc7	výsledek
přijetí	přijetí	k1gNnSc1	přijetí
definice	definice	k1gFnSc2	definice
tzv.	tzv.	kA	tzv.
atomové	atomový	k2eAgFnSc2d1	atomová
sekundy	sekunda	k1gFnSc2	sekunda
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
generální	generální	k2eAgFnSc4d1	generální
konferenci	konference	k1gFnSc4	konference
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
:	:	kIx,	:
doba	doba	k1gFnSc1	doba
časového	časový	k2eAgInSc2d1	časový
intervalu	interval	k1gInSc2	interval
vymezeného	vymezený	k2eAgNnSc2d1	vymezené
9	[number]	k4	9
192	[number]	k4	192
631	[number]	k4	631
770	[number]	k4	770
kmity	kmit	k1gInPc1	kmit
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
základního	základní	k2eAgInSc2d1	základní
izotopu	izotop	k1gInSc2	izotop
cesia	cesium	k1gNnSc2	cesium
133	[number]	k4	133
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
jeho	jeho	k3xOp3gInSc2	jeho
energetického	energetický	k2eAgInSc2d1	energetický
stavu	stav	k1gInSc2	stav
mezi	mezi	k7c7	mezi
hladinami	hladina	k1gFnPc7	hladina
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
3,0	[number]	k4	3,0
<g/>
)	)	kIx)	)
a	a	k8xC	a
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
4,0	[number]	k4	4,0
<g/>
)	)	kIx)	)
v	v	k7c6	v
nulovém	nulový	k2eAgNnSc6d1	nulové
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
definovaná	definovaný	k2eAgFnSc1d1	definovaná
sekunda	sekunda	k1gFnSc1	sekunda
je	být	k5eAaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
k	k	k7c3	k
efemeridové	efemeridový	k2eAgFnSc3d1	efemeridový
sekundě	sekunda	k1gFnSc3	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
sekundy	sekunda	k1gFnSc2	sekunda
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
vylepšena	vylepšit	k5eAaPmNgFnS	vylepšit
na	na	k7c4	na
setkání	setkání	k1gNnSc4	setkání
BIPM	BIPM	kA	BIPM
následujícím	následující	k2eAgInSc7d1	následující
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
atom	atom	k1gInSc4	atom
cesia	cesium	k1gNnSc2	cesium
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
K.	K.	kA	K.
Revidovaná	revidovaný	k2eAgFnSc1d1	revidovaná
definice	definice	k1gFnSc1	definice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ideální	ideální	k2eAgFnPc4d1	ideální
atomové	atomový	k2eAgFnPc4d1	atomová
hodiny	hodina	k1gFnPc4	hodina
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
jediný	jediný	k2eAgInSc4d1	jediný
atom	atom	k1gInSc4	atom
cesia	cesium	k1gNnSc2	cesium
emitující	emitující	k2eAgFnPc4d1	emitující
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
oddílu	oddíl	k1gInSc3	oddíl
Jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Juliánský	juliánský	k2eAgInSc1d1	juliánský
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
IAU	IAU	kA	IAU
definován	definován	k2eAgMnSc1d1	definován
jako	jako	k8xS	jako
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
31	[number]	k4	31
557	[number]	k4	557
600	[number]	k4	600
s	s	k7c7	s
Den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
d	d	k?	d
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
24	[number]	k4	24
hodiny	hodina	k1gFnSc2	hodina
<g/>
:	:	kIx,	:
1	[number]	k4	1
d	d	k?	d
=	=	kIx~	=
86	[number]	k4	86
400	[number]	k4	400
s	s	k7c7	s
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
Hodina	hodina	k1gFnSc1	hodina
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
h	h	k?	h
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
šedesát	šedesát	k4xCc1	šedesát
minut	minuta	k1gFnPc2	minuta
<g/>
:	:	kIx,	:
1	[number]	k4	1
h	h	k?	h
=	=	kIx~	=
3600	[number]	k4	3600
s	s	k7c7	s
Minuta	minut	k2eAgFnSc1d1	minuta
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
min	mina	k1gFnPc2	mina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šedesát	šedesát	k4xCc1	šedesát
sekund	sekunda	k1gFnPc2	sekunda
<g/>
:	:	kIx,	:
1	[number]	k4	1
min	mina	k1gFnPc2	mina
=	=	kIx~	=
60	[number]	k4	60
s	s	k7c7	s
Milisekunda	milisekunda	k1gFnSc1	milisekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ms	ms	k?	ms
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tisícina	tisícina	k1gFnSc1	tisícina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
1	[number]	k4	1
s	s	k7c7	s
=	=	kIx~	=
1000	[number]	k4	1000
ms.	ms.	k?	ms.
Mikrosekunda	mikrosekunda	k1gFnSc1	mikrosekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
μ	μ	k?	μ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
miliontina	miliontina	k1gFnSc1	miliontina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
1	[number]	k4	1
s	s	k7c7	s
=	=	kIx~	=
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Nanosekunda	nanosekunda	k1gFnSc1	nanosekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ns	ns	k?	ns
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
miliardtina	miliardtina	k1gFnSc1	miliardtina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
1	[number]	k4	1
s	s	k7c7	s
=	=	kIx~	=
109	[number]	k4	109
ns	ns	k?	ns
<g/>
.	.	kIx.	.
</s>
<s>
Pikosekunda	pikosekunda	k1gFnSc1	pikosekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ps	ps	k0	ps
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
biliontina	biliontina	k1gFnSc1	biliontina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
1	[number]	k4	1
s	s	k7c7	s
=	=	kIx~	=
1012	[number]	k4	1012
ps	ps	k0	ps
<g/>
.	.	kIx.	.
</s>
<s>
Femtosekunda	Femtosekunda	k1gFnSc1	Femtosekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
fs	fs	k?	fs
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tisícina	tisícina	k1gFnSc1	tisícina
biliontiny	biliontina	k1gFnSc2	biliontina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
1	[number]	k4	1
s	s	k7c7	s
=	=	kIx~	=
1015	[number]	k4	1015
fs	fs	k?	fs
<g/>
.	.	kIx.	.
</s>
<s>
Attosekunda	Attosekunda	k1gFnSc1	Attosekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
as	as	k1gInSc1	as
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
miliontina	miliontina	k1gFnSc1	miliontina
biliontiny	biliontina	k1gFnSc2	biliontina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
1	[number]	k4	1
s	s	k7c7	s
=	=	kIx~	=
1018	[number]	k4	1018
as	as	k1gNnPc2	as
<g/>
.	.	kIx.	.
</s>
