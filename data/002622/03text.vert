<s>
Tabák	tabák	k1gInSc1	tabák
(	(	kIx(	(
<g/>
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lilkovité	lilkovitý	k2eAgFnSc2d1	lilkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
také	také	k9	také
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
známý	známý	k2eAgInSc4d1	známý
produkt	produkt	k1gInSc4	produkt
z	z	k7c2	z
listů	list	k1gInPc2	list
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listech	list	k1gInPc6	list
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
alkaloid	alkaloid	k1gInSc1	alkaloid
nikotin	nikotin	k1gInSc1	nikotin
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
tabák	tabák	k1gInSc1	tabák
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
,	,	kIx,	,
doutníků	doutník	k1gInPc2	doutník
a	a	k8xC	a
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
dýmkách	dýmka	k1gFnPc6	dýmka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
tabák	tabák	k1gInSc1	tabák
dostal	dostat	k5eAaPmAgInS	dostat
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
díky	díky	k7c3	díky
Walterovi	Walter	k1gMnSc3	Walter
Raleighovi	Raleigh	k1gMnSc3	Raleigh
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tabák	tabák	k1gInSc1	tabák
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
117	[number]	k4	117
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
milionů	milion	k4xCgInPc2	milion
pěstitelů	pěstitel	k1gMnPc2	pěstitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
45	[number]	k4	45
<g/>
°	°	k?	°
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
a	a	k8xC	a
30	[number]	k4	30
<g/>
°	°	k?	°
j.š.	j.š.	k?	j.š.
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
přibližně	přibližně	k6eAd1	přibližně
5,14	[number]	k4	5,14
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Tabáková	tabákový	k2eAgNnPc4d1	tabákové
pole	pole	k1gNnPc4	pole
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
tabák	tabák	k1gInSc1	tabák
také	také	k9	také
pěstoval	pěstovat	k5eAaImAgInS	pěstovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
tabáku	tabák	k1gInSc2	tabák
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
-	-	kIx~	-
zejména	zejména	k9	zejména
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
velkými	velký	k2eAgMnPc7d1	velký
pěstiteli	pěstitel	k1gMnPc7	pěstitel
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
africké	africký	k2eAgFnPc1d1	africká
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
početné	početný	k2eAgFnSc2d1	početná
řady	řada	k1gFnSc2	řada
druhů	druh	k1gInPc2	druh
tabáků	tabák	k1gInPc2	tabák
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
hodí	hodit	k5eAaPmIp3nP	hodit
k	k	k7c3	k
výrobním	výrobní	k2eAgInPc3d1	výrobní
účelům	účel	k1gInPc3	účel
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tabák	tabák	k1gInSc1	tabák
virginský	virginský	k2eAgInSc1d1	virginský
(	(	kIx(	(
<g/>
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
tabaccum	tabaccum	k1gInSc1	tabaccum
<g/>
)	)	kIx)	)
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
selský	selský	k2eAgInSc1d1	selský
(	(	kIx(	(
<g/>
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
rustica	rustica	k1gMnSc1	rustica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
hodnotíme	hodnotit	k5eAaImIp1nP	hodnotit
tabák	tabák	k1gInSc4	tabák
jako	jako	k8xC	jako
důležitou	důležitý	k2eAgFnSc4d1	důležitá
surovinu	surovina	k1gFnSc4	surovina
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
průmyslu	průmysl	k1gInSc2	průmysl
nejen	nejen	k6eAd1	nejen
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
semena	semeno	k1gNnPc1	semeno
a	a	k8xC	a
stvoly	stvol	k1gInPc1	stvol
<g/>
.	.	kIx.	.
</s>
<s>
Tabákové	tabákový	k2eAgInPc1d1	tabákový
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
tabákový	tabákový	k2eAgInSc4d1	tabákový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
důležitou	důležitý	k2eAgFnSc7d1	důležitá
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
listů	list	k1gInPc2	list
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nikotin	nikotin	k1gInSc1	nikotin
<g/>
,	,	kIx,	,
anabasin	anabasin	k1gInSc1	anabasin
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
jablečná	jablečný	k2eAgFnSc1d1	jablečná
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
citrónová	citrónový	k2eAgFnSc1d1	citrónová
a	a	k8xC	a
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výrobky	výrobek	k1gInPc1	výrobek
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyrábět	vyrábět	k5eAaImF	vyrábět
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
tabákových	tabákový	k2eAgInPc2d1	tabákový
odpadů	odpad	k1gInPc2	odpad
nebo	nebo	k8xC	nebo
z	z	k7c2	z
celých	celý	k2eAgInPc2d1	celý
listů	list	k1gInPc2	list
speciálně	speciálně	k6eAd1	speciálně
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
rustica	rustica	k1gFnSc1	rustica
-	-	kIx~	-
alkaloid	alkaloid	k1gInSc1	alkaloid
nikotin	nikotin	k1gInSc1	nikotin
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
jablečná	jablečný	k2eAgFnSc1d1	jablečná
a	a	k8xC	a
citrónová	citrónový	k2eAgFnSc1d1	citrónová
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
glauca	glauca	k1gFnSc1	glauca
-	-	kIx~	-
alkaloid	alkaloid	k1gInSc1	alkaloid
anabasin	anabasin	k1gMnSc1	anabasin
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
glutinosa	glutinosa	k1gFnSc1	glutinosa
-	-	kIx~	-
alkaloid	alkaloid	k1gInSc1	alkaloid
nornikotin	nornikotina	k1gFnPc2	nornikotina
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
longiflora	longiflora	k1gFnSc1	longiflora
-	-	kIx~	-
alkaloid	alkaloid	k1gInSc1	alkaloid
nornikotin	nornikotina	k1gFnPc2	nornikotina
Tabákové	tabákový	k2eAgInPc1d1	tabákový
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
vonných	vonný	k2eAgFnPc2d1	vonná
látek	látka	k1gFnPc2	látka
pro	pro	k7c4	pro
kosmetický	kosmetický	k2eAgInSc4d1	kosmetický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
odrůdy	odrůda	k1gFnPc4	odrůda
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
affinis	affinis	k1gFnSc1	affinis
a	a	k8xC	a
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
sandereae	sandereae	k1gFnSc1	sandereae
<g/>
.	.	kIx.	.
</s>
<s>
Tabákové	tabákový	k2eAgNnSc1d1	tabákové
semeno	semeno	k1gNnSc1	semeno
se	se	k3xPyFc4	se
lisuje	lisovat	k5eAaImIp3nS	lisovat
a	a	k8xC	a
vylisovaný	vylisovaný	k2eAgInSc1d1	vylisovaný
olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgFnSc7d1	výchozí
surovinou	surovina	k1gFnSc7	surovina
laků	lak	k1gInPc2	lak
a	a	k8xC	a
fermeží	fermež	k1gFnPc2	fermež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
úpravě	úprava	k1gFnSc6	úprava
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
i	i	k9	i
konzumovat	konzumovat	k5eAaBmF	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokrutiny	pokrutiny	k1gFnPc1	pokrutiny
po	po	k7c6	po
lisování	lisování	k1gNnSc6	lisování
semen	semeno	k1gNnPc2	semeno
jsou	být	k5eAaImIp3nP	být
dobrým	dobrý	k2eAgNnSc7d1	dobré
krmivem	krmivo	k1gNnSc7	krmivo
pro	pro	k7c4	pro
zemědělská	zemědělský	k2eAgNnPc4d1	zemědělské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Stvol	stvol	k1gInSc1	stvol
je	být	k5eAaImIp3nS	být
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
celulózy	celulóza	k1gFnSc2	celulóza
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
otop	otop	k1gInSc4	otop
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tabák	tabák	k1gInSc1	tabák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Botanika	botanika	k1gFnSc1	botanika
Wendys	Wendys	k1gInSc1	Wendys
Botany	Botana	k1gFnSc2	Botana
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
