<s>
Thor	Thor	k1gInSc1	Thor
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Kenneth	Kenneth	k1gMnSc1	Kenneth
Branagh	Branagh	k1gMnSc1	Branagh
podle	podle	k7c2	podle
komiksů	komiks	k1gInPc2	komiks
o	o	k7c6	o
Thorovi	Thor	k1gMnSc6	Thor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Asgardu	Asgard	k1gInSc2	Asgard
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
Chris	Chris	k1gFnSc2	Chris
Hemsworth	Hemsworth	k1gInSc1	Hemsworth
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
tuto	tento	k3xDgFnSc4	tento
postavu	postava	k1gFnSc4	postava
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
i	i	k9	i
v	v	k7c6	v
navazujícím	navazující	k2eAgInSc6d1	navazující
filmu	film	k1gInSc6	film
Thor	Thor	k1gInSc1	Thor
<g/>
:	:	kIx,	:
Temný	temný	k2eAgInSc1d1	temný
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
snímek	snímek	k1gInSc4	snímek
filmové	filmový	k2eAgFnSc2d1	filmová
série	série	k1gFnSc2	série
Marvel	Marvela	k1gFnPc2	Marvela
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
965	[number]	k4	965
vedl	vést	k5eAaImAgMnS	vést
Odin	Odin	k1gMnSc1	Odin
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Asgardu	Asgard	k1gInSc2	Asgard
<g/>
,	,	kIx,	,
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
Mrazivým	mrazivý	k2eAgMnPc3d1	mrazivý
obrům	obr	k1gMnPc3	obr
z	z	k7c2	z
Jotunheimu	Jotunheim	k1gInSc2	Jotunheim
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
zabránil	zabránit	k5eAaPmAgMnS	zabránit
dobýt	dobýt	k5eAaPmF	dobýt
devět	devět	k4xCc4	devět
světů	svět	k1gInPc2	svět
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
a	a	k8xC	a
obrům	obr	k1gMnPc3	obr
vzal	vzít	k5eAaPmAgMnS	vzít
zdroj	zdroj	k1gInSc4	zdroj
jejich	jejich	k3xOp3gFnSc2	jejich
síly	síla	k1gFnSc2	síla
–	–	k?	–
Truhlici	truhlice	k1gFnSc3	truhlice
prastarých	prastarý	k2eAgFnPc2d1	prastará
zim	zima	k1gFnPc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Odinův	Odinův	k2eAgMnSc1d1	Odinův
syn	syn	k1gMnSc1	syn
Thor	Thor	k1gMnSc1	Thor
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
asgardského	asgardský	k2eAgInSc2d1	asgardský
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
korunovace	korunovace	k1gFnSc1	korunovace
je	být	k5eAaImIp3nS	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
pokusem	pokus	k1gInSc7	pokus
Mrazivých	mrazivý	k2eAgMnPc2d1	mrazivý
obrů	obr	k1gMnPc2	obr
získat	získat	k5eAaPmF	získat
Truhlici	truhlice	k1gFnSc3	truhlice
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
Odinovu	Odinův	k2eAgNnSc3d1	Odinovo
nařízení	nařízení	k1gNnSc3	nařízení
se	se	k3xPyFc4	se
Thor	Thor	k1gInSc1	Thor
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
Jotunheimu	Jotunheim	k1gInSc2	Jotunheim
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
jotunským	jotunský	k1gMnSc7	jotunský
vůdcem	vůdce	k1gMnSc7	vůdce
Laufeym	Laufeym	k1gInSc4	Laufeym
<g/>
.	.	kIx.	.
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nP	doprovázet
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Loki	Lok	k1gFnSc2	Lok
<g/>
,	,	kIx,	,
kamarádka	kamarádka	k1gFnSc1	kamarádka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Sif	Sif	k1gFnSc2	Sif
a	a	k8xC	a
Trojice	trojice	k1gFnSc2	trojice
válečníků	válečník	k1gMnPc2	válečník
<g/>
:	:	kIx,	:
Volstagg	Volstagg	k1gMnSc1	Volstagg
<g/>
,	,	kIx,	,
Fandral	Fandral	k1gMnSc1	Fandral
a	a	k8xC	a
Hogun	Hogun	k1gMnSc1	Hogun
<g/>
.	.	kIx.	.
</s>
<s>
Proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
Asgarďané	Asgarďaná	k1gFnSc6	Asgarďaná
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
mále	málo	k1gNnSc6	málo
<g/>
.	.	kIx.	.
</s>
<s>
Zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
však	však	k9	však
sám	sám	k3xTgMnSc1	sám
Odin	Odin	k1gMnSc1	Odin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tak	tak	k6eAd1	tak
poruší	porušit	k5eAaPmIp3nS	porušit
dočasné	dočasný	k2eAgNnSc1d1	dočasné
příměří	příměří	k1gNnSc1	příměří
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
rasami	rasa	k1gFnPc7	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
následně	následně	k6eAd1	následně
svému	svůj	k1gMnSc3	svůj
synovi	syn	k1gMnSc3	syn
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
aroganci	arogance	k1gFnSc3	arogance
odebere	odebrat	k5eAaPmIp3nS	odebrat
božskou	božský	k2eAgFnSc4d1	božská
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
jako	jako	k8xS	jako
smrtelníka	smrtelník	k1gMnSc4	smrtelník
jej	on	k3xPp3gMnSc4	on
vyhostí	vyhostit	k5eAaPmIp3nP	vyhostit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
pošle	poslat	k5eAaPmIp3nS	poslat
i	i	k9	i
Thorovo	Thorův	k2eAgNnSc1d1	Thorovo
kladivo	kladivo	k1gNnSc1	kladivo
Mjolnir	Mjolnira	k1gFnPc2	Mjolnira
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
nyní	nyní	k6eAd1	nyní
chráněno	chráněn	k2eAgNnSc1d1	chráněno
kouzlem	kouzlo	k1gNnSc7	kouzlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pouze	pouze	k6eAd1	pouze
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
hoden	hoden	k2eAgInSc1d1	hoden
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Thor	Thor	k1gInSc1	Thor
se	se	k3xPyFc4	se
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
najdou	najít	k5eAaPmIp3nP	najít
astrofyzička	astrofyzička	k1gFnSc1	astrofyzička
Jane	Jan	k1gMnSc5	Jan
Fosterová	Fosterový	k2eAgFnSc1d1	Fosterová
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
asistentkou	asistentka	k1gFnSc7	asistentka
Darcy	Darca	k1gMnSc2	Darca
Lewisovou	Lewisový	k2eAgFnSc7d1	Lewisová
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
Erikem	Erik	k1gMnSc7	Erik
Selvigem	Selvig	k1gMnSc7	Selvig
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
městečka	městečko	k1gNnSc2	městečko
naleznou	naleznout	k5eAaPmIp3nP	naleznout
také	také	k9	také
Mjolnir	Mjolnir	k1gInSc4	Mjolnir
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
agent	agent	k1gMnSc1	agent
Phil	Phil	k1gMnSc1	Phil
Coulson	Coulson	k1gMnSc1	Coulson
z	z	k7c2	z
agentury	agentura	k1gFnSc2	agentura
S.	S.	kA	S.
<g/>
H.I.E.L.D.	H.I.E.L.D.	k1gMnSc1	H.I.E.L.D.
nechá	nechat	k5eAaPmIp3nS	nechat
střežit	střežit	k5eAaImF	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zabaví	zabavit	k5eAaPmIp3nP	zabavit
i	i	k9	i
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
Jane	Jan	k1gMnSc5	Jan
získala	získat	k5eAaPmAgFnS	získat
o	o	k7c6	o
červí	červí	k2eAgFnSc6d1	červí
díře	díra	k1gFnSc6	díra
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
Thor	Thor	k1gMnSc1	Thor
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Asgarďan	Asgarďan	k1gMnSc1	Asgarďan
objeví	objevit	k5eAaPmIp3nS	objevit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jeho	jeho	k3xOp3gNnSc4	jeho
kladivo	kladivo	k1gNnSc4	kladivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
ze	z	k7c2	z
zabezpečeného	zabezpečený	k2eAgInSc2d1	zabezpečený
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
rychle	rychle	k6eAd1	rychle
postavil	postavit	k5eAaPmAgMnS	postavit
agentura	agentura	k1gFnSc1	agentura
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mjolnir	Mjolnir	k1gMnSc1	Mjolnir
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zvednout	zvednout	k5eAaPmF	zvednout
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
zajat	zajat	k2eAgInSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Selvigově	Selvigův	k2eAgFnSc3d1	Selvigův
pomoci	pomoc	k1gFnSc3	pomoc
je	být	k5eAaImIp3nS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
,	,	kIx,	,
smíří	smířit	k5eAaPmIp3nS	smířit
se	se	k3xPyFc4	se
s	s	k7c7	s
exilem	exil	k1gInSc7	exil
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
romanticky	romanticky	k6eAd1	romanticky
sblížit	sblížit	k5eAaPmF	sblížit
s	s	k7c7	s
Jane	Jan	k1gMnSc5	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Loki	Loki	k6eAd1	Loki
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
Laufeyho	Laufey	k1gMnSc4	Laufey
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
adoptoval	adoptovat	k5eAaPmAgInS	adoptovat
Odin	Odin	k1gInSc1	Odin
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpaný	vyčerpaný	k2eAgMnSc1d1	vyčerpaný
král	král	k1gMnSc1	král
mezitím	mezitím	k6eAd1	mezitím
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
hlubokého	hluboký	k2eAgInSc2d1	hluboký
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
dodat	dodat	k5eAaPmF	dodat
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Loki	Loki	k6eAd1	Loki
se	se	k3xPyFc4	se
zmocní	zmocnit	k5eAaPmIp3nS	zmocnit
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
Laufeymu	Laufeym	k1gInSc2	Laufeym
šanci	šance	k1gFnSc4	šance
zabít	zabít	k5eAaPmF	zabít
Odina	Odin	k1gMnSc4	Odin
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
Truhlici	truhlice	k1gFnSc4	truhlice
<g/>
.	.	kIx.	.
</s>
<s>
Sif	Sif	k?	Sif
a	a	k8xC	a
Trojice	trojice	k1gFnSc1	trojice
válečníků	válečník	k1gMnPc2	válečník
není	být	k5eNaImIp3nS	být
spokojená	spokojený	k2eAgFnSc1d1	spokojená
s	s	k7c7	s
Lokiho	Lokiha	k1gFnSc5	Lokiha
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
Thorův	Thorův	k2eAgInSc4d1	Thorův
návrat	návrat	k1gInSc4	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
Heimdalla	Heimdalla	k1gMnSc1	Heimdalla
<g/>
,	,	kIx,	,
Strážce	strážce	k1gMnSc1	strážce
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
hlídá	hlídat	k5eAaImIp3nS	hlídat
Bifröst	Bifröst	k1gInSc4	Bifröst
(	(	kIx(	(
<g/>
způsob	způsob	k1gInSc1	způsob
cestování	cestování	k1gNnSc2	cestování
mezi	mezi	k7c7	mezi
světy	svět	k1gInPc7	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
povolil	povolit	k5eAaPmAgMnS	povolit
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Loki	Loki	k6eAd1	Loki
se	se	k3xPyFc4	se
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
plánu	plán	k1gInSc6	plán
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
Ničitele	ničitel	k1gMnPc4	ničitel
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nezničitelného	zničitelný	k2eNgInSc2d1	nezničitelný
automata	automa	k1gNnPc4	automa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
a	a	k8xC	a
Thora	Thora	k1gMnSc1	Thora
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Bojovníci	bojovník	k1gMnPc1	bojovník
skutečně	skutečně	k6eAd1	skutečně
Asgarďana	Asgarďan	k1gMnSc4	Asgarďan
najdou	najít	k5eAaPmIp3nP	najít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ničitel	ničitel	k1gMnSc1	ničitel
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
a	a	k8xC	a
porazí	porazit	k5eAaPmIp3nP	porazit
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Thor	Thor	k1gInSc1	Thor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ničiteli	ničitel	k1gMnPc7	ničitel
sám	sám	k3xTgMnSc1	sám
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oběť	oběť	k1gFnSc1	oběť
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
smrti	smrt	k1gFnSc2	smrt
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hoden	hoden	k2eAgMnSc1d1	hoden
používat	používat	k5eAaImF	používat
kladivo	kladivo	k1gNnSc4	kladivo
Mjolnir	Mjolnira	k1gFnPc2	Mjolnira
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
přistane	přistat	k5eAaPmIp3nS	přistat
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
navrátí	navrátit	k5eAaPmIp3nS	navrátit
jeho	jeho	k3xOp3gFnSc1	jeho
božská	božský	k2eAgFnSc1d1	božská
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
nad	nad	k7c7	nad
Ničitelem	ničitel	k1gMnSc7	ničitel
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Polibkem	polibek	k1gInSc7	polibek
se	se	k3xPyFc4	se
rozloučí	rozloučit	k5eAaPmIp3nS	rozloučit
s	s	k7c7	s
Jane	Jan	k1gMnSc5	Jan
a	a	k8xC	a
se	se	k3xPyFc4	se
slibem	slib	k1gInSc7	slib
návratu	návrat	k1gInSc2	návrat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
Asgarďany	Asgarďan	k1gMnPc7	Asgarďan
vydá	vydat	k5eAaPmIp3nS	vydat
střetnout	střetnout	k5eAaPmF	střetnout
se	se	k3xPyFc4	se
s	s	k7c7	s
Lokim	Loki	k1gNnSc7	Loki
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asgardu	Asgard	k1gInSc6	Asgard
mezitím	mezitím	k6eAd1	mezitím
Loki	Lok	k1gMnPc1	Lok
zradí	zradit	k5eAaPmIp3nP	zradit
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
Laufeyho	Laufey	k1gMnSc4	Laufey
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
skutečným	skutečný	k2eAgInSc7d1	skutečný
plánem	plán	k1gInSc7	plán
totiž	totiž	k9	totiž
je	být	k5eAaImIp3nS	být
využít	využít	k5eAaPmF	využít
Laufeyho	Laufey	k1gMnSc4	Laufey
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
zabití	zabití	k1gNnSc6	zabití
Odina	Odin	k1gInSc2	Odin
jako	jako	k8xC	jako
záminku	záminka	k1gFnSc4	záminka
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
Jotunheimu	Jotunheim	k1gInSc2	Jotunheim
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
má	mít	k5eAaImIp3nS	mít
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hoden	hoden	k2eAgInSc1d1	hoden
svého	svůj	k3xOyFgMnSc2	svůj
adoptivního	adoptivní	k2eAgMnSc2d1	adoptivní
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
ale	ale	k9	ale
Thor	Thor	k1gMnSc1	Thor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
svede	svést	k5eAaPmIp3nS	svést
souboj	souboj	k1gInSc1	souboj
a	a	k8xC	a
který	který	k3yRgMnSc1	který
zabrání	zabránit	k5eAaPmIp3nS	zabránit
Lokiho	Loki	k1gMnSc4	Loki
plánu	plán	k1gInSc2	plán
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zničí	zničit	k5eAaPmIp3nS	zničit
Bifröst	Bifröst	k1gFnSc1	Bifröst
<g/>
.	.	kIx.	.
</s>
<s>
Probuzený	probuzený	k2eAgInSc1d1	probuzený
Odin	Odin	k1gInSc1	Odin
zachrání	zachránit	k5eAaPmIp3nS	zachránit
oba	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
před	před	k7c7	před
pádem	pád	k1gInSc7	pád
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Loki	Lok	k1gMnPc1	Lok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
král	král	k1gMnSc1	král
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
pustí	pustit	k5eAaPmIp3nS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Thor	Thor	k1gMnSc1	Thor
odčiní	odčinit	k5eAaPmIp3nS	odčinit
svůj	svůj	k3xOyFgInSc4	svůj
skutek	skutek	k1gInSc4	skutek
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
připustí	připustit	k5eAaPmIp3nS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
připravený	připravený	k2eAgMnSc1d1	připravený
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
vládcem	vládce	k1gMnSc7	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
Jane	Jan	k1gMnSc5	Jan
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
snaží	snažit	k5eAaImIp3nP	snažit
nalézt	nalézt	k5eAaBmF	nalézt
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
otevřít	otevřít	k5eAaPmF	otevřít
portál	portál	k1gInSc4	portál
vedoucí	vedoucí	k1gMnSc1	vedoucí
do	do	k7c2	do
Asgardu	Asgard	k1gInSc2	Asgard
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
S.	S.	kA	S.
<g/>
H.I.E.L.D.u	H.I.E.L.D.a	k1gFnSc4	H.I.E.L.D.a
Nick	Nicka	k1gFnPc2	Nicka
Fury	Fura	k1gFnSc2	Fura
vezme	vzít	k5eAaPmIp3nS	vzít
doktora	doktor	k1gMnSc4	doktor
Selviga	Selvig	k1gMnSc4	Selvig
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
agenturního	agenturní	k2eAgNnSc2d1	agenturní
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
kufřík	kufřík	k1gInSc4	kufřík
a	a	k8xC	a
požádá	požádat	k5eAaPmIp3nS	požádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
záhadný	záhadný	k2eAgInSc4d1	záhadný
krychlovitý	krychlovitý	k2eAgInSc4d1	krychlovitý
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
možná	možná	k9	možná
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nevýslovnou	výslovný	k2eNgFnSc4d1	nevýslovná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Neviditelný	viditelný	k2eNgInSc1d1	neviditelný
Loki	Loke	k1gFnSc4	Loke
pobídne	pobídnout	k5eAaPmIp3nS	pobídnout
vědce	vědec	k1gMnPc4	vědec
a	a	k8xC	a
doktor	doktor	k1gMnSc1	doktor
skutečně	skutečně	k6eAd1	skutečně
s	s	k7c7	s
ředitelovým	ředitelův	k2eAgInSc7d1	ředitelův
návrhem	návrh	k1gInSc7	návrh
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Chris	Chris	k1gInSc1	Chris
Hemsworth	Hemsworth	k1gInSc1	Hemsworth
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Maxián	Maxián	k1gMnSc1	Maxián
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Thor	Thor	k1gMnSc1	Thor
Natalie	Natalie	k1gFnSc2	Natalie
Portmanová	Portmanová	k1gFnSc1	Portmanová
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Andrea	Andrea	k1gFnSc1	Andrea
Elsnerová	Elsnerová	k1gFnSc1	Elsnerová
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Jane	Jan	k1gMnSc5	Jan
Fosterová	Fosterová	k1gFnSc1	Fosterová
Tom	Tom	k1gMnSc1	Tom
Hiddleston	Hiddleston	k1gInSc1	Hiddleston
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Lumír	Lumír	k1gMnSc1	Lumír
Olšovský	Olšovský	k1gMnSc1	Olšovský
<g/>
)	)	kIx)	)
jako	jako	k9	jako
Loki	Lok	k1gMnPc1	Lok
Stellan	Stellan	k1gMnSc1	Stellan
Skarsgå	Skarsgå	k1gMnSc1	Skarsgå
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Hromada	Hromada	k1gMnSc1	Hromada
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Erik	Erik	k1gMnSc1	Erik
Selvig	Selvig	k1gMnSc1	Selvig
Kat	kat	k1gMnSc1	kat
Denningsová	Denningsová	k1gFnSc1	Denningsová
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Nikola	Nikola	k1gFnSc1	Nikola
Votočková	Votočková	k1gFnSc1	Votočková
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Darcy	Darca	k1gFnPc4	Darca
Lewisová	Lewisový	k2eAgFnSc1d1	Lewisová
Clark	Clark	k1gInSc1	Clark
Gregg	Gregg	k1gInSc4	Gregg
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Mahdal	Mahdal	k1gMnSc1	Mahdal
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
agent	agent	k1gMnSc1	agent
Phil	Phil	k1gMnSc1	Phil
Coulson	Coulson	k1gMnSc1	Coulson
Colm	Colm	k1gMnSc1	Colm
Feore	Feor	k1gInSc5	Feor
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Klem	Klema	k1gFnPc2	Klema
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
jako	jako	k8xS	jako
Laufey	Laufe	k2eAgFnPc4d1	Laufe
Ray	Ray	k1gFnPc4	Ray
Stevenson	Stevenson	k1gInSc1	Stevenson
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Gelnar	Gelnar	k1gMnSc1	Gelnar
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Volstagg	Volstagg	k1gMnSc1	Volstagg
Idris	Idris	k1gFnSc2	Idris
Elba	Elba	k1gMnSc1	Elba
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Zahálka	Zahálka	k1gMnSc1	Zahálka
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Heimdall	Heimdall	k1gMnSc1	Heimdall
Jaimie	Jaimie	k1gFnSc2	Jaimie
Alexanderová	Alexanderová	k1gFnSc1	Alexanderová
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
René	René	k1gFnSc1	René
Slováčková	Slováčková	k1gFnSc1	Slováčková
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Sif	Sif	k1gMnSc1	Sif
Tadanobu	Tadanoba	k1gFnSc4	Tadanoba
Asano	Asano	k6eAd1	Asano
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Matouš	Matouš	k1gMnSc1	Matouš
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Hogun	Hogun	k1gMnSc1	Hogun
Joshua	Joshua	k1gMnSc1	Joshua
Dallas	Dallas	k1gMnSc1	Dallas
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Lněnička	Lněnička	k1gFnSc1	Lněnička
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Fandral	Fandral	k1gFnSc1	Fandral
Rene	Rene	k1gFnSc1	Rene
Russoová	Russoová	k1gFnSc1	Russoová
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Ilona	Ilona	k1gFnSc1	Ilona
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Frigga	Frigga	k1gFnSc1	Frigga
Anthony	Anthona	k1gFnSc2	Anthona
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Plachý	Plachý	k1gMnSc1	Plachý
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Odin	Odin	k1gInSc4	Odin
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
také	také	k9	také
Maximiliano	Maximiliana	k1gFnSc5	Maximiliana
Hernández	Hernández	k1gMnSc1	Hernández
(	(	kIx(	(
<g/>
agent	agent	k1gMnSc1	agent
Jasper	Jasper	k1gMnSc1	Jasper
Sitwell	Sitwell	k1gMnSc1	Sitwell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Gatt	Gatt	k1gMnSc1	Gatt
<g/>
,	,	kIx,	,
Joshua	Joshua	k1gMnSc1	Joshua
Cox	Cox	k1gMnSc1	Cox
a	a	k8xC	a
Douglas	Douglas	k1gMnSc1	Douglas
Tait	Tait	k1gMnSc1	Tait
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
jako	jako	k8xC	jako
Mraziví	mrazivět	k5eAaImIp3nP	mrazivět
obři	obr	k1gMnPc1	obr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cameo	cameo	k6eAd1	cameo
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
Stan	stan	k1gInSc4	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Michael	Michael	k1gMnSc1	Michael
Straczynski	Straczynsk	k1gFnSc2	Straczynsk
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
jako	jako	k8xC	jako
řidiči	řidič	k1gMnPc1	řidič
pick-upů	pickp	k1gInPc2	pick-up
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc1	Jerem
Renner	Renner	k1gInSc1	Renner
(	(	kIx(	(
<g/>
agent	agent	k1gMnSc1	agent
Clint	Clint	k1gMnSc1	Clint
Barton	Barton	k1gInSc1	Barton
/	/	kIx~	/
Hawkeye	Hawkeye	k1gInSc1	Hawkeye
<g/>
)	)	kIx)	)
a	a	k8xC	a
Samuel	Samuel	k1gMnSc1	Samuel
L.	L.	kA	L.
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
Nick	Nick	k1gMnSc1	Nick
Fury	Fura	k1gFnSc2	Fura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
o	o	k7c6	o
Thorovi	Thor	k1gMnSc6	Thor
byl	být	k5eAaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
scénáře	scénář	k1gInSc2	scénář
napsal	napsat	k5eAaPmAgInS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Mark	Mark	k1gMnSc1	Mark
Protosevich	Protosevich	k1gMnSc1	Protosevich
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
režisérem	režisér	k1gMnSc7	režisér
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
snímku	snímek	k1gInSc2	snímek
stal	stát	k5eAaPmAgMnS	stát
Matthew	Matthew	k1gMnSc1	Matthew
Vaughn	Vaughn	k1gMnSc1	Vaughn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smlouva	smlouva	k1gFnSc1	smlouva
však	však	k9	však
vypršela	vypršet	k5eAaPmAgFnS	vypršet
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
a	a	k8xC	a
studio	studio	k1gNnSc1	studio
začalo	začít	k5eAaPmAgNnS	začít
hledat	hledat	k5eAaImF	hledat
nového	nový	k2eAgMnSc4d1	nový
režiséra	režisér	k1gMnSc4	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
stal	stát	k5eAaPmAgMnS	stát
Kenneth	Kenneth	k1gMnSc1	Kenneth
Branagh	Branagh	k1gMnSc1	Branagh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
obsazeni	obsadit	k5eAaPmNgMnP	obsadit
Chris	Chris	k1gFnSc3	Chris
Hemsworth	Hemsworth	k1gMnSc1	Hemsworth
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
Hiddleston	Hiddleston	k1gInSc4	Hiddleston
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
Natalie	Natalie	k1gFnSc1	Natalie
Portmanová	Portmanová	k1gFnSc1	Portmanová
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
i	i	k9	i
další	další	k2eAgMnPc1d1	další
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
snímku	snímek	k1gInSc2	snímek
s	s	k7c7	s
rozpočtem	rozpočet	k1gInSc7	rozpočet
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Scénu	scéna	k1gFnSc4	scéna
po	po	k7c6	po
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
titulcích	titulek	k1gInPc6	titulek
režíroval	režírovat	k5eAaImAgInS	režírovat
Joss	Joss	k1gInSc1	Joss
Whedon	Whedon	k1gInSc1	Whedon
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
Thor	Thora	k1gFnPc2	Thora
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Sydney	Sydney	k1gNnPc6	Sydney
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kin	kino	k1gNnPc2	kino
byl	být	k5eAaImAgInS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
v	v	k7c6	v
kinodistribuci	kinodistribuce	k1gFnSc6	kinodistribuce
objevil	objevit	k5eAaPmAgInS	objevit
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
utržil	utržit	k5eAaPmAgInS	utržit
snímek	snímek	k1gInSc1	snímek
181	[number]	k4	181
030	[number]	k4	030
624	[number]	k4	624
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
dalších	další	k2eAgInPc2d1	další
268	[number]	k4	268
295	[number]	k4	295
994	[number]	k4	994
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgFnPc1d1	celosvětová
tržby	tržba	k1gFnPc1	tržba
tak	tak	k6eAd1	tak
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
449	[number]	k4	449
326	[number]	k4	326
618	[number]	k4	618
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
15	[number]	k4	15
<g/>
.	.	kIx.	.
nejvýdělečnějším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
filmem	film	k1gInSc7	film
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
film	film	k1gInSc4	film
společnost	společnost	k1gFnSc1	společnost
Bontonfilm	Bontonfilm	k1gInSc1	Bontonfilm
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
promítací	promítací	k2eAgInSc4d1	promítací
víkend	víkend	k1gInSc4	víkend
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
snímek	snímek	k1gInSc4	snímek
25	[number]	k4	25
609	[number]	k4	609
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
tržby	tržba	k1gFnPc1	tržba
činily	činit	k5eAaImAgFnP	činit
kolem	kolem	k7c2	kolem
čtyř	čtyři	k4xCgInPc2	čtyři
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
kinech	kino	k1gNnPc6	kino
celkově	celkově	k6eAd1	celkově
utržil	utržit	k5eAaPmAgInS	utržit
přibližně	přibližně	k6eAd1	přibližně
735	[number]	k4	735
600	[number]	k4	600
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Kinobox	Kinobox	k1gInSc1	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
35	[number]	k4	35
recenzí	recenze	k1gFnPc2	recenze
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
ohodnotil	ohodnotit	k5eAaPmAgInS	ohodnotit
film	film	k1gInSc1	film
Thor	Thor	k1gInSc1	Thor
74	[number]	k4	74
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Rotten	Rotten	k2eAgMnSc1d1	Rotten
Tomatoes	Tomatoes	k1gMnSc1	Tomatoes
udělil	udělit	k5eAaPmAgMnS	udělit
snímku	snímka	k1gFnSc4	snímka
známku	známka	k1gFnSc4	známka
6,7	[number]	k4	6,7
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
268	[number]	k4	268
recenzí	recenze	k1gFnPc2	recenze
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
206	[number]	k4	206
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
spokojených	spokojený	k2eAgMnPc2d1	spokojený
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
77	[number]	k4	77
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
serveru	server	k1gInSc2	server
Metacritic	Metacritice	k1gFnPc2	Metacritice
získal	získat	k5eAaPmAgMnS	získat
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
40	[number]	k4	40
recenzí	recenze	k1gFnPc2	recenze
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
57	[number]	k4	57
ze	z	k7c2	z
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
žánrové	žánrový	k2eAgFnPc4d1	žánrová
ceny	cena	k1gFnPc4	cena
Saturn	Saturn	k1gInSc1	Saturn
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
kategorie	kategorie	k1gFnSc2	kategorie
Nejlepší	dobrý	k2eAgInPc4d3	nejlepší
fantasy	fantas	k1gInPc4	fantas
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
proměnil	proměnit	k5eAaPmAgMnS	proměnit
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
kostýmy	kostým	k1gInPc4	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
komerčnímu	komerční	k2eAgInSc3d1	komerční
úspěchu	úspěch	k1gInSc3	úspěch
snímku	snímek	k1gInSc2	snímek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
filmový	filmový	k2eAgInSc1d1	filmový
sequel	sequel	k1gInSc1	sequel
Thor	Thor	k1gInSc1	Thor
<g/>
:	:	kIx,	:
Temný	temný	k2eAgInSc1d1	temný
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnSc7	součást
série	série	k1gFnSc2	série
Marvel	Marvela	k1gFnPc2	Marvela
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
MCU	MCU	kA	MCU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zopakoval	zopakovat	k5eAaPmAgInS	zopakovat
Chris	Chris	k1gInSc1	Chris
Hemsworth	Hemsworth	k1gInSc1	Hemsworth
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
Thor	Thor	k1gMnSc1	Thor
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
celovečerních	celovečerní	k2eAgInPc6d1	celovečerní
filmech	film	k1gInPc6	film
MCU	MCU	kA	MCU
<g/>
.	.	kIx.	.
</s>
