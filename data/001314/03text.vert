<s>
Valkounský	Valkounský	k2eAgInSc1d1	Valkounský
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Nerudova	Nerudův	k2eAgFnSc1d1	Nerudova
211	[number]	k4	211
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
renesančně	renesančně	k6eAd1	renesančně
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
barokně	barokně	k6eAd1	barokně
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
spočívá	spočívat	k5eAaImIp3nS	spočívat
mj.	mj.	kA	mj.
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
a	a	k8xC	a
také	také	k6eAd1	také
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
Jan	Jan	k1gMnSc1	Jan
Blažej	Blažej	k1gMnSc1	Blažej
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
za	za	k7c4	za
3000	[number]	k4	3000
zlatých	zlatá	k1gFnPc2	zlatá
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
jej	on	k3xPp3gInSc4	on
obýval	obývat	k5eAaImAgInS	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
dům	dům	k1gInSc1	dům
prodala	prodat	k5eAaPmAgFnS	prodat
Santiniho	Santini	k1gMnSc4	Santini
vdova	vdova	k1gFnSc1	vdova
za	za	k7c4	za
4580	[number]	k4	4580
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Santini	Santin	k2eAgMnPc1d1	Santin
dům	dům	k1gInSc1	dům
přestavěl	přestavět	k5eAaPmAgInS	přestavět
zřejmě	zřejmě	k6eAd1	zřejmě
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zakoupení	zakoupení	k1gNnSc6	zakoupení
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
dům	dům	k1gInSc4	dům
významní	významný	k2eAgMnPc1d1	významný
malostranští	malostranský	k2eAgMnPc1d1	malostranský
zlatníci	zlatník	k1gMnPc1	zlatník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
pekárna	pekárna	k1gFnSc1	pekárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
slouží	sloužit	k5eAaImIp3nS	sloužit
dům	dům	k1gInSc4	dům
jako	jako	k8xC	jako
hotel	hotel	k1gInSc4	hotel
Santini	Santin	k2eAgMnPc1d1	Santin
Residence	residence	k1gFnSc2	residence
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
situovaný	situovaný	k2eAgInSc1d1	situovaný
na	na	k7c4	na
hluboké	hluboký	k2eAgNnSc4d1	hluboké
<g/>
,	,	kIx,	,
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
pravidelné	pravidelný	k2eAgFnSc3d1	pravidelná
parcele	parcela	k1gFnSc3	parcela
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
trojpatrové	trojpatrový	k2eAgFnSc2d1	trojpatrová
obytné	obytný	k2eAgFnSc2d1	obytná
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
zadního	zadní	k2eAgInSc2d1	zadní
dvorka	dvorek	k1gInSc2	dvorek
obestavěného	obestavěný	k2eAgInSc2d1	obestavěný
dalšími	další	k2eAgFnPc7d1	další
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Levou	levý	k2eAgFnSc4d1	levá
část	část	k1gFnSc4	část
přízemí	přízemí	k1gNnSc2	přízemí
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
dnes	dnes	k6eAd1	dnes
předělený	předělený	k2eAgInSc4d1	předělený
průjezd	průjezd	k1gInSc4	průjezd
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
komponované	komponovaný	k2eAgNnSc1d1	komponované
průčelí	průčelí	k1gNnSc1	průčelí
do	do	k7c2	do
Nerudovy	Nerudův	k2eAgFnSc2d1	Nerudova
ulice	ulice	k1gFnSc2	ulice
je	být	k5eAaImIp3nS	být
čtyřosé	čtyřosý	k2eAgNnSc1d1	čtyřosé
<g/>
.	.	kIx.	.
</s>
<s>
Parter	parter	k2eAgFnSc1d1	parter
prolamuje	prolamovat	k5eAaImIp3nS	prolamovat
dvojice	dvojice	k1gFnSc1	dvojice
shodně	shodně	k6eAd1	shodně
plošně	plošně	k6eAd1	plošně
utvářených	utvářený	k2eAgInPc2d1	utvářený
portálů	portál	k1gInPc2	portál
vjezdu	vjezd	k1gInSc2	vjezd
a	a	k8xC	a
obchodního	obchodní	k2eAgInSc2d1	obchodní
výkladce	výkladec	k1gInSc2	výkladec
<g/>
.	.	kIx.	.
</s>
<s>
Okna	okno	k1gNnSc2	okno
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
jsou	být	k5eAaImIp3nP	být
zdůrazněna	zdůraznit	k5eAaPmNgFnS	zdůraznit
nadokenními	nadokenní	k2eAgFnPc7d1	nadokenní
římsami	římsa	k1gFnPc7	římsa
segmentově	segmentově	k6eAd1	segmentově
či	či	k8xC	či
kýlovitě	kýlovitě	k6eAd1	kýlovitě
projmutými	projmutý	k2eAgInPc7d1	projmutý
v	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
ABBA	ABBA	kA	ABBA
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
rozeklaným	rozeklaný	k2eAgInSc7d1	rozeklaný
štítem	štít	k1gInSc7	štít
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
vsazen	vsadit	k5eAaPmNgInS	vsadit
mohutný	mohutný	k2eAgInSc1d1	mohutný
vikýř	vikýř	k1gInSc1	vikýř
s	s	k7c7	s
oválným	oválný	k2eAgInSc7d1	oválný
okulem	okul	k1gInSc7	okul
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnPc6	jeho
stranách	strana	k1gFnPc6	strana
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
soklech	sokl	k1gInPc6	sokl
navíc	navíc	k6eAd1	navíc
umístěna	umístit	k5eAaPmNgFnS	umístit
dvojice	dvojice	k1gFnSc1	dvojice
ozdobných	ozdobný	k2eAgFnPc2d1	ozdobná
váz	váza	k1gFnPc2	váza
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
dekorováno	dekorovat	k5eAaBmNgNnS	dekorovat
pozdně	pozdně	k6eAd1	pozdně
barokním	barokní	k2eAgInSc7d1	barokní
štukem	štuk	k1gInSc7	štuk
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výzdoba	výzdoba	k1gFnSc1	výzdoba
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
především	především	k9	především
na	na	k7c4	na
bohaté	bohatý	k2eAgNnSc4d1	bohaté
rámování	rámování	k1gNnSc4	rámování
oken	okno	k1gNnPc2	okno
tvořené	tvořený	k2eAgFnPc1d1	tvořená
kromě	kromě	k7c2	kromě
šambrán	šambrána	k1gFnPc2	šambrána
a	a	k8xC	a
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
říms	římsa	k1gFnPc2	římsa
ještě	ještě	k9	ještě
suprafenestrami	suprafenestra	k1gFnPc7	suprafenestra
a	a	k8xC	a
čabrakovými	čabrakový	k2eAgNnPc7d1	čabrakový
podokeními	podokení	k1gNnPc7	podokení
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ose	osa	k1gFnSc6	osa
fasády	fasáda	k1gFnSc2	fasáda
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
okny	okno	k1gNnPc7	okno
druhého	druhý	k4xOgMnSc4	druhý
patra	patro	k1gNnSc2	patro
umístěna	umístěn	k2eAgFnSc1d1	umístěna
oválná	oválný	k2eAgFnSc1d1	oválná
štuková	štukový	k2eAgFnSc1d1	štuková
kartuš	kartuš	k1gFnSc1	kartuš
rámující	rámující	k2eAgFnSc4d1	rámující
malbu	malba	k1gFnSc4	malba
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
se	s	k7c7	s
Svatou	svatý	k2eAgFnSc7d1	svatá
trojicí	trojice	k1gFnSc7	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
části	část	k1gFnPc1	část
fasády	fasáda	k1gFnSc2	fasáda
mohou	moct	k5eAaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
již	již	k9	již
ze	z	k7c2	z
Santiniho	Santini	k1gMnSc2	Santini
přestavby	přestavba	k1gFnSc2	přestavba
<g/>
.	.	kIx.	.
</s>
<s>
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
:	:	kIx,	:
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Prahy	Praha	k1gFnSc2	Praha
-	-	kIx~	-
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
771	[number]	k4	771
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
S.	S.	kA	S.
314	[number]	k4	314
<g/>
-	-	kIx~	-
<g/>
316	[number]	k4	316
<g/>
.	.	kIx.	.
</s>
<s>
HORYNA	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
Mojmír	Mojmír	k1gMnSc1	Mojmír
J.	J.	kA	J.
B.	B.	kA	B.
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
-	-	kIx~	-
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
664	[number]	k4	664
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
S.	S.	kA	S.
234	[number]	k4	234
<g/>
-	-	kIx~	-
<g/>
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Valkounský	Valkounský	k2eAgInSc4d1	Valkounský
dům	dům	k1gInSc4	dům
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
