<s>
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Opat	opat	k1gMnSc1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
vyzývá	vyzývat	k5eAaImIp3nS
k	k	k7c3
zahájení	zahájení	k1gNnSc3
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
Salles	Salles	k1gInSc1
des	des	k1gNnSc2
Croisades	Croisadesa	k1gFnPc2
<g/>
,	,	kIx,
Versailles	Versailles	k1gFnPc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Anatolie	Anatolie	k1gFnSc1
<g/>
,	,	kIx,
Levanta	Levanta	k1gFnSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Pyrenejský	pyrenejský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
–	–	k?
porážka	porážka	k1gFnSc1
křižáků	křižák	k1gInPc2
v	v	k7c6
Levantě	Levanta	k1gFnSc6
<g/>
,	,	kIx,
<g/>
–	–	k?
vítězství	vítězství	k1gNnSc2
křižáků	křižák	k1gInPc2
na	na	k7c6
Iberském	iberský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
<g/>
–	–	k?
vítězství	vítězství	k1gNnSc2
křižáků	křižák	k1gInPc2
v	v	k7c6
Polabí	Polabí	k1gNnSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
zde	zde	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
–	–	k?
Lisabon	Lisabon	k1gInSc1
dobyt	dobýt	k5eAaPmNgInS
Portugalci	Portugalec	k1gMnSc3
<g/>
,	,	kIx,
Tarragona	Tarragona	k1gFnSc1
a	a	k8xC
Tortosa	Tortosa	k1gFnSc1
dobyta	dobýt	k5eAaPmNgFnS
Katalánci	Katalánec	k1gMnPc7
<g/>
,	,	kIx,
<g/>
–	–	k?
Vagrie	Vagrie	k1gFnPc4
a	a	k8xC
Polabí	Polabí	k1gNnPc4
obsazeny	obsazen	k2eAgFnPc4d1
saskými	saský	k2eAgInPc7d1
křižáky	křižák	k1gInPc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
zde	zde	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Flandry	Flandry	k1gInPc1
</s>
<s>
Bar	bar	k1gInSc1
</s>
<s>
Burgundsko	Burgundsko	k1gNnSc1
</s>
<s>
Bretaň	Bretaň	k1gFnSc1
</s>
<s>
Champagne	Champagnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Akvitánie	Akvitánie	k1gFnSc1
</s>
<s>
Auvergne	Auvergnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Toulouse	Toulouse	k1gInSc1
</s>
<s>
vikomtství	vikomtství	k1gNnSc1
Trencavelů	Trencavel	k1gInPc2
</s>
<s>
Říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Německé	německý	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Švábsko	Švábsko	k1gNnSc1
</s>
<s>
Bádensko	Bádensko	k1gNnSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Štýrsko	Štýrsko	k1gNnSc1
</s>
<s>
Franky	Franky	k1gInPc1
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Lotrinsko	Lotrinsko	k1gNnSc1
</s>
<s>
Horní	horní	k2eAgNnSc1d1
Lotrinsko	Lotrinsko	k1gNnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Montferrat	Montferrat	k1gMnSc1
</s>
<s>
Savojsko	Savojsko	k1gNnSc1
</s>
<s>
Provensálsko	Provensálsko	k6eAd1
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Kilikijská	Kilikijský	k2eAgFnSc1d1
Arménie	Arménie	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
a	a	k8xC
Plantagenetská	Plantagenetský	k2eAgFnSc1d1
Francie	Francie	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Normandie	Normandie	k1gFnSc1
</s>
<s>
Anjou	Anjou	k6eAd1
</s>
<s>
Maine	Mainout	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Sicilské	sicilský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
v	v	k7c6
Levantě	Levanta	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Tripolské	tripolský	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
</s>
<s>
Antiochijské	antiochijský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
rytířsko-vojenské	rytířsko-vojenský	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Řád	řád	k1gInSc1
templářů	templář	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
johanitů	johanita	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
sv.	sv.	kA
Lazara	Lazar	k1gMnSc2
</s>
<s>
Řád	řád	k1gInSc1
Božího	boží	k2eAgInSc2d1
Hrobu	hrob	k1gInSc2
</s>
<s>
Řád	řád	k1gInSc1
rytířů	rytíř	k1gMnPc2
Montjoie	Montjoie	k1gFnSc2
</s>
<s>
muslimští	muslimský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
syrští	syrský	k2eAgMnPc1d1
asasíni	asasín	k1gMnPc1
(	(	kIx(
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
reconquista	reconquista	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Barcelonské	barcelonský	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
</s>
<s>
León-Kastilie	León-Kastilie	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Janovská	janovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Pisánská	pisánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
angličtí	anglický	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
němečtí	německý	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
vlámští	vlámský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
účastníci	účastník	k1gMnPc1
výpravy	výprava	k1gFnSc2
proti	proti	k7c3
Venedům	Vened	k1gMnPc3
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
zde	zde	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
sunnité	sunnita	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Seldžucká	Seldžucký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Seldžucká	Seldžucký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Rúmský	Rúmský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
</s>
<s>
Zengíovský	Zengíovský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Damašský	damašský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Abbásovský	Abbásovský	k2eAgInSc1d1
chalífát	chalífát	k1gInSc1
</s>
<s>
ší	ší	k?
<g/>
'	'	kIx"
<g/>
ité	ité	k?
<g/>
:	:	kIx,
</s>
<s>
Fátimovský	fátimovský	k2eAgInSc1d1
chalífát	chalífát	k1gInSc1
</s>
<s>
ší	ší	k?
<g/>
'	'	kIx"
<g/>
itští	itštit	k5eAaPmIp3nS,k5eAaImIp3nS
ismá	ismá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ílité	ílitý	k2eAgNnSc1d1
<g/>
:	:	kIx,
</s>
<s>
syrští	syrský	k2eAgMnPc1d1
asasíni	asasín	k1gMnPc1
</s>
<s>
reconquista	reconquista	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Almorávidé	Almorávidý	k2eAgInPc1d1
Almorávidé	Almorávidý	k2eAgInPc1d1
</s>
<s>
taifa	taif	k1gMnSc4
Badajoz	Badajoz	k1gMnSc1
</s>
<s>
taifa	taif	k1gMnSc4
Valencie	Valencie	k1gFnSc2
</s>
<s>
taifa	taif	k1gMnSc4
Murcia	Murcius	k1gMnSc4
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Otakar	Otakar	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
V.	V.	kA
</s>
<s>
Heřman	Heřman	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Přemyslovec	Přemyslovec	k1gMnSc1
</s>
<s>
Matyáš	Matyáš	k1gMnSc1
I.	I.	kA
</s>
<s>
Godfrey	Godfrey	k1gInPc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Amadeus	Amadeus	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Trixenu	Trixen	k1gInSc2
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
</s>
<s>
Ota	Ota	k1gMnSc1
z	z	k7c2
Freisingu	Freising	k1gInSc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
I.	I.	kA
</s>
<s>
Theodwin	Theodwin	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
z	z	k7c2
Courtenay	Courtenaa	k1gFnSc2
</s>
<s>
Robert	Robert	k1gMnSc1
z	z	k7c2
Dreux	Dreux	k1gInSc1
</s>
<s>
Vít	Vít	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladší	mladý	k2eAgFnSc7d2
</s>
<s>
Hugo	Hugo	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Hugues	Hugues	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Reginald	Reginald	k6eAd1
z	z	k7c2
Bar	bar	k1gInSc1
</s>
<s>
Geoffroy	Geoffroy	k1gInPc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Filip	Filip	k1gMnSc1
z	z	k7c2
Milly	Milla	k1gFnSc2
</s>
<s>
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Yves	Yves	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Dětřich	Dětřich	k1gMnSc1
Alsaský	alsaský	k2eAgMnSc1d1
</s>
<s>
Renaud	Renaud	k6eAd1
z	z	k7c2
Châtillonu	Châtillon	k1gInSc2
</s>
<s>
Balduin	Balduin	k1gInSc1
z	z	k7c2
Antiochie	Antiochie	k1gFnSc2
</s>
<s>
Alfons	Alfons	k1gMnSc1
Jordan	Jordan	k1gMnSc1
</s>
<s>
Raimond	Raimond	k1gMnSc1
Trencavel	Trencavel	k1gMnSc1
</s>
<s>
Jaufre	Jaufr	k1gMnSc5
Rudel	Rudel	k1gMnSc1
</s>
<s>
Thoros	Thorosa	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Manuel	Manuel	k1gMnSc1
Komnenos	Komnenos	k1gMnSc1
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Akvitánská	Akvitánský	k2eAgFnSc1d1
</s>
<s>
Geoffroy	Geoffroy	k1gInPc1
V.	V.	kA
</s>
<s>
Roger	Roger	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhnanec	vyhnanec	k1gMnSc1
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
Vysoký	vysoký	k2eAgMnSc1d1
</s>
<s>
Levanta	Levanta	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Balduin	Balduin	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Melisenda	Melisenda	k1gFnSc1
</s>
<s>
Raimond	Raimond	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Raimond	Raimond	k1gInSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
†	†	k?
</s>
<s>
Robert	Robert	k1gMnSc1
z	z	k7c2
Craonu	Craon	k1gInSc2
</s>
<s>
Evrard	Evrard	k1gInSc1
z	z	k7c2
Barres	Barresa	k1gFnPc2
</s>
<s>
Raimond	Raimond	k1gMnSc1
du	du	k?
Puy	Puy	k1gMnSc1
</s>
<s>
Manasses	Manasses	k1gInSc1
z	z	k7c2
Hierges	Hiergesa	k1gFnPc2
</s>
<s>
Fulko	Fulko	k6eAd1
z	z	k7c2
Angoulê	Angoulê	k1gInSc5
</s>
<s>
Joscelin	Joscelina	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
muslimští	muslimský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Alí	Alí	k?
ibn	ibn	k?
Wafa	Wafa	k1gMnSc1
†	†	k?
</s>
<s>
reconquista	reconquista	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Alfons	Alfons	k1gMnSc1
I.	I.	kA
</s>
<s>
Alfons	Alfons	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
R.	R.	kA
Berenguer	Berenguero	k1gNnPc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Arnout	Arnout	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Hervey	Hervea	k1gFnPc1
de	de	k?
Glanvill	Glanvill	k1gMnSc1
</s>
<s>
vůdcové	vůdce	k1gMnPc1
výpravy	výprava	k1gFnSc2
proti	proti	k7c3
Venedům	Vened	k1gMnPc3
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
zde	zde	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
sunnité	sunnita	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Mesud	Mesud	k6eAd1
</s>
<s>
Muín	Muín	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Unur	Unur	k1gMnSc1
</s>
<s>
Nadžm	Nadžm	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Ajjúb	Ajjúb	k1gMnSc1
</s>
<s>
Imád	Imád	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Zengí	Zengí	k1gMnSc1
</s>
<s>
Núr	Núr	k?
ad-Dín	ad-Dín	k1gInSc1
</s>
<s>
Saíf	Saíf	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Ghazi	Ghaze	k1gFnSc4
</s>
<s>
Al-Muktafi	Al-Muktafi	k6eAd1
</s>
<s>
ší	ší	k?
<g/>
'	'	kIx"
<g/>
ité	ité	k?
</s>
<s>
Al-Hafiz	Al-Hafiz	k1gMnSc1
</s>
<s>
reconquista	reconquista	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Tashfin	Tashfin	k2eAgMnSc1d1
Ibn	Ibn	k1gMnSc1
Alibr	Alibr	k1gMnSc1
</s>
<s>
Ibrahim	Ibrahim	k1gMnSc1
ibn	ibn	k?
Tashfin	Tashfin	k1gMnSc1
</s>
<s>
Ishaq	Ishaq	k?
ibn	ibn	k?
Ali	Ali	k1gFnSc2
</s>
<s>
Abd	Abd	k?
al-Mu	al-Mus	k1gInSc2
<g/>
'	'	kIx"
<g/>
min	mina	k1gFnPc2
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Němců	Němec	k1gMnPc2
<g/>
15	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Francouzů	Francouz	k1gMnPc2
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
značné	značný	k2eAgNnSc1d1
</s>
<s>
lehké	lehký	k2eAgNnSc1d1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
bylo	být	k5eAaImAgNnS
vojenské	vojenský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
vyhlášené	vyhlášený	k2eAgNnSc1d1
papežem	papež	k1gMnSc7
Evženem	Evžen	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1146	#num#	k4
jako	jako	k9
reakce	reakce	k1gFnPc1
na	na	k7c4
dobytí	dobytí	k1gNnSc4
Edessy	Edessa	k1gFnSc2
mosulským	mosulský	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
Zengím	Zengí	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edessa	Edessa	k1gFnSc1
byla	být	k5eAaImAgFnS
prvním	první	k4xOgInSc7
křižáckým	křižácký	k2eAgInSc7d1
státem	stát	k1gInSc7
založeným	založený	k2eAgInSc7d1
v	v	k7c6
průběhu	průběh	k1gInSc6
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
a	a	k8xC
i	i	k9
prvním	první	k4xOgInSc7
státem	stát	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
muslimy	muslim	k1gMnPc4
vyvrácen	vyvrácen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
výpravou	výprava	k1gFnSc7
ve	v	k7c6
znamení	znamení	k1gNnSc6
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
čele	čelo	k1gNnSc6
stanuli	stanout	k5eAaPmAgMnP
evropští	evropský	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
francouzský	francouzský	k2eAgMnSc1d1
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnilo	účastnit	k5eAaImAgNnS
se	se	k3xPyFc4
také	také	k9
množství	množství	k1gNnSc1
významných	významný	k2eAgMnPc2d1
evropských	evropský	k2eAgMnPc2d1
feudálů	feudál	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
českého	český	k2eAgNnSc2d1
knížete	kníže	k1gNnSc2wR
Vladislava	Vladislav	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armády	armáda	k1gFnSc2
obou	dva	k4xCgMnPc2
králů	král	k1gMnPc2
pochodovaly	pochodovat	k5eAaImAgFnP
odděleně	odděleně	k6eAd1
napříč	napříč	k7c7
Evropou	Evropa	k1gFnSc7
až	až	k9
do	do	k7c2
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
turecké	turecký	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
jako	jako	k8xC,k8xS
první	první	k4xOgMnPc1
vstoupili	vstoupit	k5eAaPmAgMnP
Němci	Němec	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
u	u	k7c2
Dorylaia	Dorylaium	k1gNnSc2
byla	být	k5eAaImAgFnS
armáda	armáda	k1gFnSc1
přepadena	přepadnout	k5eAaPmNgFnS
a	a	k8xC
téměř	téměř	k6eAd1
vyhlazena	vyhlazen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	s	k7c7
zbytky	zbytek	k1gInPc7
německé	německý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
spojily	spojit	k5eAaPmAgFnP
s	s	k7c7
Francouzi	Francouz	k1gMnPc7
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
dorazili	dorazit	k5eAaPmAgMnP
až	až	k9
do	do	k7c2
Antiochie	Antiochie	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jeruzalémským	jeruzalémský	k2eAgInSc7d1
dvorem	dvůr	k1gInSc7
křižáci	křižák	k1gMnPc1
naplánovali	naplánovat	k5eAaBmAgMnP
výpravu	výprava	k1gFnSc4
na	na	k7c4
Damašek	Damašek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
však	však	k9
skončila	skončit	k5eAaPmAgFnS
naprostým	naprostý	k2eAgNnSc7d1
fiaskem	fiasko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
muslimy	muslim	k1gMnPc4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
a	a	k8xC
Zengí	Zengí	k1gNnSc1
se	se	k3xPyFc4
po	po	k7c6
neúspěšné	úspěšný	k2eNgFnSc6d1
výpravě	výprava	k1gFnSc6
Damašku	Damašek	k1gInSc2
zmocnil	zmocnit	k5eAaPmAgInS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
muslimská	muslimský	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
proti	proti	k7c3
křižáckým	křižácký	k2eAgInPc3d1
státům	stát	k1gInPc3
opět	opět	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
jednotnější	jednotný	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sjednocení	sjednocení	k1gNnSc1
muslimů	muslim	k1gMnPc2
na	na	k7c6
Předním	přední	k2eAgInSc6d1
východě	východ	k1gInSc6
dokončil	dokončit	k5eAaPmAgInS
Saladin	Saladin	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
symbolem	symbol	k1gInSc7
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
stalo	stát	k5eAaPmAgNnS
dobytí	dobytí	k1gNnSc1
Jeruzaléma	Jeruzalém	k1gInSc2
roku	rok	k1gInSc2
1187	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
této	tento	k3xDgFnSc2
výpravy	výprava	k1gFnSc2
bylo	být	k5eAaImAgNnS
dobytí	dobytí	k1gNnSc1
Lisabonu	Lisabon	k1gInSc2
roku	rok	k1gInSc2
1147	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgMnSc3
přispěl	přispět	k5eAaPmAgInS
kontingent	kontingent	k1gInSc1
vlámských	vlámský	k2eAgMnPc2d1
<g/>
,	,	kIx,
anglických	anglický	k2eAgMnPc2d1
a	a	k8xC
německých	německý	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
při	při	k7c6
cestě	cesta	k1gFnSc6
ze	z	k7c2
Severního	severní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
do	do	k7c2
východního	východní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
na	na	k7c4
žádost	žádost	k1gFnSc4
místního	místní	k2eAgMnSc2d1
biskupa	biskup	k1gMnSc2
zastavil	zastavit	k5eAaPmAgInS
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
němečtí	německý	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
s	s	k7c7
králem	král	k1gMnSc7
Konrádem	Konrád	k1gMnSc7
táhnout	táhnout	k5eAaImF
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
započali	započnout	k5eAaPmAgMnP
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
první	první	k4xOgFnSc1
z	z	k7c2
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
proti	proti	k7c3
pohanským	pohanský	k2eAgInPc3d1
kmenům	kmen	k1gInPc3
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
opakovaně	opakovaně	k6eAd1
staly	stát	k5eAaPmAgFnP
součástí	součást	k1gFnSc7
evropských	evropský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
na	na	k7c4
celá	celý	k2eAgNnPc4d1
staletí	staletí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Předchozí	předchozí	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
Pád	Pád	k1gInSc1
Edessy	Edessa	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Obléhání	obléhání	k1gNnSc2
Edessy	Edessa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
v	v	k7c6
Levantě	Levanta	k1gFnSc6
před	před	k7c7
druhou	druhý	k4xOgFnSc7
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
</s>
<s>
Po	po	k7c6
první	první	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
a	a	k8xC
menším	malý	k2eAgNnSc6d2
křižáckém	křižácký	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
roku	rok	k1gInSc2
1101	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
utvořily	utvořit	k5eAaPmAgInP
tři	tři	k4xCgInPc1
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Antiochijské	antiochijský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
a	a	k8xC
Edesské	Edesský	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtý	čtvrtý	k4xOgInSc1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
Hrabství	hrabství	k1gNnSc1
Tripolis	Tripolis	k1gInSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1109	#num#	k4
po	po	k7c4
dobytí	dobytí	k1gNnSc4
Tripolisu	Tripolis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edessa	Edessa	k1gFnSc1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
nejseverněji	severně	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
největší	veliký	k2eAgMnSc1d3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
nejslabší	slabý	k2eAgMnSc1d3
a	a	k8xC
nejméně	málo	k6eAd3
zalidněný	zalidněný	k2eAgInSc1d1
křižácký	křižácký	k2eAgInSc1d1
stát	stát	k1gInSc1
byl	být	k5eAaImAgInS
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc1d1
obklopen	obklopen	k2eAgInSc1d1
muslimskými	muslimský	k2eAgInPc7d1
státy	stát	k1gInPc7
artukských	artukský	k2eAgInPc2d1
<g/>
,	,	kIx,
danišmendovských	danišmendovský	k2eAgInPc2d1
i	i	k8xC
seldžuckých	seldžucký	k2eAgInPc2d1
Turků	turek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrabě	Hrabě	k1gMnSc1
Balduin	Balduin	k1gMnSc1
Le	Le	k1gMnSc1
Bourg	Bourg	k1gMnSc1
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Joscelin	Joscelina	k1gFnPc2
z	z	k7c2
Courtenay	Courtenaa	k1gFnSc2
byli	být	k5eAaImAgMnP
po	po	k7c6
porážce	porážka	k1gFnSc6
edessko-antiochijské	edessko-antiochijský	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
u	u	k7c2
Harranu	Harran	k1gInSc2
zajati	zajat	k2eAgMnPc1d1
Jekermichem	Jekermich	k1gMnSc7
z	z	k7c2
Mosulu	Mosul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
dva	dva	k4xCgInPc1
<g/>
,	,	kIx,
Balduin	Balduin	k2eAgInSc4d1
již	již	k6eAd1
jako	jako	k9
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
král	král	k1gMnSc1
a	a	k8xC
Joscelin	Joscelin	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Edessy	Edessa	k1gFnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1122	#num#	k4
upadli	upadnout	k5eAaPmAgMnP
do	do	k7c2
zajetí	zajetí	k1gNnSc2
znovu	znovu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1131	#num#	k4
Joscelin	Joscelina	k1gFnPc2
z	z	k7c2
Courtenay	Courtenaa	k1gFnSc2
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
Joscelin	Joscelin	k2eAgMnSc1d1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
proti	proti	k7c3
turecké	turecký	k2eAgFnSc3d1
hrozbě	hrozba	k1gFnSc3
spojit	spojit	k5eAaPmF
s	s	k7c7
Byzantskou	byzantský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1143	#num#	k4
však	však	k9
zemřel	zemřít	k5eAaPmAgMnS
byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
i	i	k8xC
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fulko	Fulko	k1gNnSc4
z	z	k7c2
Anjou	Anjá	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
Joscelin	Joscelin	k2eAgMnSc1d1
II	II	kA
<g/>
.	.	kIx.
ztratil	ztratit	k5eAaPmAgMnS
mocné	mocný	k2eAgMnPc4d1
spojence	spojenec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
znesvářen	znesvářen	k2eAgInSc1d1
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
křižáckými	křižácký	k2eAgMnPc7d1
vládci	vládce	k1gMnPc7
<g/>
,	,	kIx,
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
Raimondem	Raimond	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
zejména	zejména	k9
s	s	k7c7
knížetem	kníže	k1gMnSc7
Raimondem	Raimond	k1gMnSc7
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Mezitím	mezitím	k6eAd1
seldžucký	seldžucký	k2eAgInSc1d1
atabeg	atabeg	k1gInSc1
z	z	k7c2
Mosulu	Mosul	k1gInSc2
Zengí	Zengí	k1gFnSc2
roku	rok	k1gInSc2
1128	#num#	k4
připojil	připojit	k5eAaPmAgMnS
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
říši	říš	k1gFnSc3
Aleppo	Aleppa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zengí	Zengí	k1gNnSc1
s	s	k7c7
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Balduinem	Balduin	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
poté	poté	k6eAd1
zaměřili	zaměřit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
na	na	k7c4
Damašek	Damašek	k1gInSc4
<g/>
,	,	kIx,
před	před	k7c7
jehož	jehož	k3xOyRp3gFnPc7
hradbami	hradba	k1gFnPc7
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1129	#num#	k4
Balduin	Balduina	k1gFnPc2
poražen	porazit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dynastie	dynastie	k1gFnSc1
Búríjovců	Búríjovec	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
městu	město	k1gNnSc3
vládla	vládnout	k5eAaImAgFnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
později	pozdě	k6eAd2
spojila	spojit	k5eAaPmAgFnS
s	s	k7c7
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Fulkem	Fulek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zengí	Zeng	k1gFnPc2
město	město	k1gNnSc4
oblehl	oblehnout	k5eAaPmAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1139	#num#	k4
a	a	k8xC
1140	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Papež	Papež	k1gMnSc1
Evžen	Evžen	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1144	#num#	k4
se	se	k3xPyFc4
hrabě	hrabě	k1gMnSc1
Joscelin	Joscelina	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
spojil	spojit	k5eAaPmAgInS
s	s	k7c7
artukskými	artukský	k2eAgMnPc7d1
Turky	Turek	k1gMnPc7
a	a	k8xC
vytáhl	vytáhnout	k5eAaPmAgInS
s	s	k7c7
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc7d1
edesskou	edesský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
na	na	k7c4
pomoc	pomoc	k1gFnSc4
artukskému	artukský	k1gMnSc3
vládci	vládce	k1gMnSc3
Kara	kara	k1gFnSc1
Arslanovi	Arslan	k1gMnSc6
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
Aleppem	Alepp	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
krále	král	k1gMnSc2
Fulka	Fulek	k1gMnSc2
měl	mít	k5eAaImAgInS
Zengí	Zengí	k2eAgMnSc1d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
převahu	převaha	k1gFnSc4
<g/>
,	,	kIx,
pospíšil	pospíšit	k5eAaPmAgMnS
si	se	k3xPyFc3
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
obešel	obejít	k5eAaPmAgInS
Joscelinovu	Joscelinův	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
napadl	napadnout	k5eAaPmAgMnS
Edessu	Edessa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joscelin	Joscelina	k1gFnPc2
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
i	i	k8xC
Raimonda	Raimonda	k1gFnSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
Joscelinovi	Joscelin	k1gMnSc3
pomoc	pomoc	k1gFnSc4
poskytnout	poskytnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
měsíčním	měsíční	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1144	#num#	k4
město	město	k1gNnSc1
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
konetábl	konetábnout	k5eAaPmAgInS
Manassés	Manassés	k1gInSc1
de	de	k?
Hierges	Hierges	k1gMnSc1
okamžitě	okamžitě	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
posily	posila	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ty	ten	k3xDgFnPc1
nemohly	moct	k5eNaImAgFnP
dorazit	dorazit	k5eAaPmF
včas	včas	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joscelin	Joscelina	k1gFnPc2
se	se	k3xPyFc4
stáhl	stáhnout	k5eAaPmAgInS
do	do	k7c2
západních	západní	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
hrabství	hrabství	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
pevnosti	pevnost	k1gFnSc2
Turbessel	Turbessel	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
celý	celý	k2eAgInSc1d1
východ	východ	k1gInSc1
byl	být	k5eAaImAgInS
ztracen	ztratit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zengí	Zengí	k1gNnSc1
po	po	k7c6
úspěchu	úspěch	k1gInSc6
u	u	k7c2
Edessy	Edessa	k1gFnSc2
začal	začít	k5eAaPmAgMnS
být	být	k5eAaImF
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
al-Malik	al-Malik	k1gMnSc1
al-Mansúr	al-Mansúr	k1gMnSc1
–	–	k?
Vítězný	vítězný	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
roku	rok	k1gInSc2
1146	#num#	k4
byl	být	k5eAaImAgMnS
Zengí	Zengí	k1gMnSc1
zavražděn	zavraždit	k5eAaPmNgMnS
svým	svůj	k3xOyFgMnSc7
sluhou	sluha	k1gMnSc7
Jarankešem	Jarankeš	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říši	říše	k1gFnSc4
si	se	k3xPyFc3
rozdělili	rozdělit	k5eAaPmAgMnP
synové	syn	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mosulu	Mosul	k1gInSc6
vládl	vládnout	k5eAaImAgMnS
starší	starý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Saíf	Saíf	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Ghazi	Ghaze	k1gFnSc3
I.	I.	kA
<g/>
,	,	kIx,
v	v	k7c6
Aleppu	Alepp	k1gInSc6
nastoupil	nastoupit	k5eAaPmAgMnS
mladší	mladý	k2eAgMnSc1d2
Núr	Núr	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hortense	Hortense	k1gFnSc1
Haudebourt-Lescot	Haudebourt-Lescot	k1gMnSc1
<g/>
:	:	kIx,
Hugo	Hugo	k1gMnSc1
z	z	k7c2
Jabaly	Jabala	k1gFnSc2
u	u	k7c2
papeže	papež	k1gMnSc4
Evžena	Evžen	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Zengího	Zengí	k1gMnSc2
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
hrabě	hrabě	k1gMnSc1
Joscelin	Joscelina	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
pokusil	pokusit	k5eAaPmAgMnS
v	v	k7c6
listopadu	listopad	k1gInSc6
1146	#num#	k4
s	s	k7c7
pomocí	pomoc	k1gFnSc7
oddílů	oddíl	k1gInPc2
z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c4
znovudobytí	znovudobytí	k1gNnSc4
Edessy	Edessa	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
však	však	k9
vojsky	vojsky	k6eAd1
Núr	Núr	k1gMnSc1
ad-Dína	ad-Dín	k1gInSc2
odražen	odražen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Antiochijský	antiochijský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Raimond	Raimond	k1gMnSc1
z	z	k7c2
Poitiers	Poitiers	k1gInSc4
ohroženému	ohrožený	k2eAgNnSc3d1
Edesskému	Edesský	k2eAgNnSc3d1
hrabství	hrabství	k1gNnSc3
nepomohl	pomoct	k5eNaPmAgMnS
<g/>
,	,	kIx,
pochopil	pochopit	k5eAaPmAgMnS
však	však	k9
<g/>
,	,	kIx,
jaké	jaký	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
nebezpečí	nebezpečí	k1gNnSc1
hrozí	hrozit	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc3
vlastnímu	vlastní	k2eAgNnSc3d1
knížectví	knížectví	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Edessy	Edessa	k1gFnSc2
odcestoval	odcestovat	k5eAaPmAgMnS
okamžitě	okamžitě	k6eAd1
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
složil	složit	k5eAaPmAgMnS
císaři	císař	k1gMnSc3
Manuelovi	Manuel	k1gMnSc3
I.	I.	kA
požadovaný	požadovaný	k2eAgInSc1d1
lenní	lenní	k2eAgInSc1d1
hold	hold	k1gInSc1
a	a	k8xC
požádal	požádat	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
putovali	putovat	k5eAaImAgMnP
do	do	k7c2
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
antiochijští	antiochijský	k2eAgMnPc1d1
a	a	k8xC
jeruzalémští	jeruzalémský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Quantum	Quantum	k1gNnSc1
praedecessores	praedecessoresa	k1gFnPc2
</s>
<s>
Zprávy	zpráva	k1gFnPc1
o	o	k7c6
pádu	pád	k1gInSc6
Edessy	Edessa	k1gFnSc2
donesli	donést	k5eAaPmAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
jako	jako	k8xS,k8xC
první	první	k4xOgMnPc1
poutníci	poutník	k1gMnPc1
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1145	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
následovala	následovat	k5eAaImAgFnS
je	on	k3xPp3gNnPc4
poselstva	poselstvo	k1gNnPc4
z	z	k7c2
Antiochie	Antiochie	k1gFnSc2
<g/>
,	,	kIx,
Jeruzaléma	Jeruzalém	k1gInSc2
i	i	k8xC
Malé	Malé	k2eAgFnSc2d1
Arménie	Arménie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskup	biskup	k1gMnSc1
Hugo	Hugo	k1gMnSc1
z	z	k7c2
Jabaly	Jabala	k1gFnSc2
zprávy	zpráva	k1gFnSc2
přednesl	přednést	k5eAaPmAgInS
papeži	papež	k1gMnSc3
Evženu	Evžen	k1gMnSc3
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1145	#num#	k4
vydal	vydat	k5eAaPmAgMnS
bulu	bula	k1gFnSc4
Quantum	Quantum	k1gNnSc4
praedecessores	praedecessoresa	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
svolával	svolávat	k5eAaImAgMnS
druhou	druhý	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
Evžen	Evžen	k1gMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nesídlil	sídlit	k5eNaImAgMnS
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
ten	ten	k3xDgInSc4
držela	držet	k5eAaImAgFnS
protipapežská	protipapežský	k2eAgFnSc1d1
komuna	komuna	k1gFnSc1
Arnolda	Arnold	k1gMnSc2
z	z	k7c2
Brescie	Brescie	k1gFnSc2
<g/>
,	,	kIx,
sídelním	sídelní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Svatého	svatý	k2eAgMnSc2d1
otce	otec	k1gMnSc2
bylo	být	k5eAaImAgNnS
Viterbo	Viterba	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počáteční	počáteční	k2eAgFnSc1d1
odezva	odezva	k1gFnSc1
na	na	k7c4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
byla	být	k5eAaImAgFnS
malá	malý	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jen	jen	k9
vykreslilo	vykreslit	k5eAaPmAgNnS
papežovu	papežův	k2eAgFnSc4d1
vlastní	vlastní	k2eAgFnSc4d1
malou	malý	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
bulu	bula	k1gFnSc4
papež	papež	k1gMnSc1
adresoval	adresovat	k5eAaBmAgMnS
především	především	k6eAd1
francouzskému	francouzský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Ludvíkovi	Ludvík	k1gMnSc3
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
ten	ten	k3xDgInSc4
svůj	svůj	k3xOyFgInSc4
záměr	záměr	k1gInSc4
oznámil	oznámit	k5eAaPmAgMnS
dvoru	dvůr	k1gInSc6
v	v	k7c4
Bourges	Bourges	k1gInSc4
o	o	k7c6
Vánocích	Vánoce	k1gFnPc6
roku	rok	k1gInSc2
1145	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
prohlášeních	prohlášení	k1gNnPc6
o	o	k7c6
papežovi	papež	k1gMnSc6
ani	ani	k8xC
nezmínil	zmínit	k5eNaPmAgInS
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
otázkou	otázka	k1gFnSc7
<g/>
,	,	kIx,
zda	zda	k8xS
o	o	k7c4
Quantum	Quantum	k1gNnSc4
praedecessores	praedecessoresa	k1gFnPc2
<g/>
,	,	kIx,
vydané	vydaný	k2eAgInPc4d1
jen	jen	k9
tři	tři	k4xCgInPc4
týdny	týden	k1gInPc4
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
skutečně	skutečně	k6eAd1
věděl	vědět	k5eAaImAgMnS
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Francouzští	francouzský	k2eAgMnPc1d1
feudálové	feudál	k1gMnPc1
však	však	k9
králův	králův	k2eAgInSc4d1
záměr	záměr	k1gInSc4
přijali	přijmout	k5eAaPmAgMnP
chladně	chladně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
nábožensky	nábožensky	k6eAd1
založeného	založený	k2eAgMnSc4d1
Ludvíka	Ludvík	k1gMnSc4
velmi	velmi	k6eAd1
pohoršilo	pohoršit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účast	účast	k1gFnSc1
šlechticů	šlechtic	k1gMnPc2
na	na	k7c6
kruciátě	kruciáta	k1gFnSc6
si	se	k3xPyFc3
takticky	takticky	k6eAd1
nevynucoval	vynucovat	k5eNaImAgMnS
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
přesvědčit	přesvědčit	k5eAaPmF
je	on	k3xPp3gMnPc4
po	po	k7c6
dobrém	dobré	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Velikonoce	Velikonoce	k1gFnPc4
král	král	k1gMnSc1
připravil	připravit	k5eAaPmAgInS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
opata	opat	k1gMnSc2
Bernarda	Bernard	k1gMnSc2
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
další	další	k2eAgNnPc4d1
shromáždění	shromáždění	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
vazaly	vazal	k1gMnPc4
opět	opět	k6eAd1
získat	získat	k5eAaPmF
pro	pro	k7c4
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Počátkem	počátkem	k7c2
března	březen	k1gInSc2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
papežská	papežský	k2eAgFnSc1d1
bula	bula	k1gFnSc1
v	v	k7c6
lehce	lehko	k6eAd1
pozměněné	pozměněný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
znovu	znovu	k6eAd1
a	a	k8xC
tento	tento	k3xDgInSc1
dokument	dokument	k1gInSc1
byl	být	k5eAaImAgInS
již	již	k6eAd1
přijímán	přijímat	k5eAaImNgInS
jako	jako	k8xS,k8xC
oficiální	oficiální	k2eAgInSc1d1
podnět	podnět	k1gInSc1
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Agitace	agitace	k1gFnSc1
Bernarda	Bernard	k1gMnSc2
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
na	na	k7c4
kostelní	kostelní	k2eAgFnSc4d1
vitraji	vitraje	k1gFnSc4
<g/>
,	,	kIx,
Horní	horní	k2eAgFnSc4d1
Falc	Falc	k1gFnSc4
kolem	kolem	k7c2
roku	rok	k1gInSc2
1450	#num#	k4
</s>
<s>
Papež	Papež	k1gMnSc1
Bernarda	Bernard	k1gMnSc4
pověřil	pověřit	k5eAaPmAgMnS
propagací	propagace	k1gFnSc7
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
a	a	k8xC
udílením	udílení	k1gNnSc7
odpustků	odpustek	k1gInPc2
pro	pro	k7c4
účastníky	účastník	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
schvaloval	schvalovat	k5eAaImAgMnS
i	i	k9
papež	papež	k1gMnSc1
Urban	Urban	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
během	během	k7c2
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Shromáždění	shromáždění	k1gNnSc1
bylo	být	k5eAaImAgNnS
svoláno	svolat	k5eAaPmNgNnS
do	do	k7c2
města	město	k1gNnSc2
Vézelay	Vézelaa	k1gFnSc2
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
rozneslo	roznést	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
řečnit	řečnit	k5eAaImF
bude	být	k5eAaImBp3nS
i	i	k9
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
<g/>
,	,	kIx,
přijížděli	přijíždět	k5eAaImAgMnP
lidé	člověk	k1gMnPc1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Přítomen	přítomno	k1gNnPc2
byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
král	král	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
<g/>
,	,	kIx,
Eleonora	Eleonora	k1gFnSc1
Akvitánská	Akvitánský	k2eAgFnSc1d1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
francouzští	francouzský	k2eAgMnPc1d1
feudálové	feudál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernard	Bernard	k1gMnSc1
k	k	k7c3
zástupům	zástup	k1gInPc3
promluvil	promluvit	k5eAaPmAgMnS
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
Clermontu	Clermont	k1gInSc6
roku	rok	k1gInSc2
1095	#num#	k4
se	se	k3xPyFc4
sešlo	sejít	k5eAaPmAgNnS
tolik	tolik	k4yIc1,k4xDc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nevešli	vejít	k5eNaPmAgMnP
do	do	k7c2
katedrály	katedrála	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Přesná	přesný	k2eAgFnSc1d1
Bernardova	Bernardův	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
se	se	k3xPyFc4
nezachovala	zachovat	k5eNaPmAgFnS
<g/>
,	,	kIx,
ví	vědět	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
přečetl	přečíst	k5eAaPmAgMnS
Quantum	Quantum	k1gNnSc4
praedecessores	praedecessores	k1gMnSc1
a	a	k8xC
vyzval	vyzvat	k5eAaPmAgMnS
prosté	prostý	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
papežovu	papežův	k2eAgFnSc4d1
výzvu	výzva	k1gFnSc4
uposlechli	uposlechnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakce	reakce	k1gFnSc1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
šlechticů	šlechtic	k1gMnPc2
i	i	k8xC
lidí	člověk	k1gMnPc2
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
bouřlivá	bouřlivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
si	se	k3xPyFc3
začali	začít	k5eAaPmAgMnP
horlivě	horlivě	k6eAd1
přišívat	přišívat	k5eAaImF
kříže	kříž	k1gInPc4
na	na	k7c4
oděv	oděv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernard	Bernard	k1gMnSc1
sám	sám	k3xTgMnSc1
provedl	provést	k5eAaPmAgMnS
velmi	velmi	k6eAd1
emotivní	emotivní	k2eAgNnSc4d1
gesto	gesto	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
sundal	sundat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
mnišskou	mnišský	k2eAgFnSc4d1
kutnu	kutna	k1gFnSc4
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
roztrhat	roztrhat	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
z	z	k7c2
jejích	její	k3xOp3gInPc2
cárů	cár	k1gInPc2
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
našívat	našívat	k5eAaImF
kříže	kříž	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěch	úspěch	k1gInSc1
Evženovy	Evženův	k2eAgFnSc2d1
výzvy	výzva	k1gFnSc2
byl	být	k5eAaImAgInS
od	od	k7c2
té	ten	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2
zaručen	zaručen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
byl	být	k5eAaImAgMnS
dobrým	dobrý	k2eAgMnSc7d1
řečníkem	řečník	k1gMnSc7
<g/>
,	,	kIx,
filosofem	filosof	k1gMnSc7
i	i	k8xC
organizátorem	organizátor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
agitace	agitace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
významně	významně	k6eAd1
doplňovala	doplňovat	k5eAaImAgFnS
i	i	k9
Bernardova	Bernardův	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
na	na	k7c6
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgFnSc6d2
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
plamenná	plamenný	k2eAgNnPc4d1
prohlášení	prohlášení	k1gNnSc4
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
té	ten	k3xDgFnSc2
první	první	k4xOgFnSc2
výrazně	výrazně	k6eAd1
lišila	lišit	k5eAaImAgFnS
především	především	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
záležitostí	záležitost	k1gFnSc7
vysoké	vysoký	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
a	a	k8xC
mocných	mocný	k2eAgMnPc2d1
feudálů	feudál	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
se	se	k3xPyFc4
v	v	k7c6
mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
objevovali	objevovat	k5eAaImAgMnP
lidoví	lidový	k2eAgMnPc1d1
kazatelé	kazatel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
rozdmýchávali	rozdmýchávat	k5eAaImAgMnP
náboženskou	náboženský	k2eAgFnSc4d1
euforii	euforie	k1gFnSc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tomu	ten	k3xDgMnSc3
sama	sám	k3xTgMnSc4
církev	církev	k1gFnSc1
snažila	snažit	k5eAaImAgFnS
zabránit	zabránit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
protižidovské	protižidovský	k2eAgFnPc1d1
vášně	vášeň	k1gFnPc1
nebyly	být	k5eNaImAgFnP
tak	tak	k6eAd1
intenzivní	intenzivní	k2eAgFnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
koncem	koncem	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
během	běh	k1gInSc7
lidových	lidový	k2eAgFnPc2d1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
<g/>
,	,	kIx,
když	když	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
Bernard	Bernard	k1gMnSc1
pobýval	pobývat	k5eAaImAgMnS
ve	v	k7c6
Flandrech	Flandry	k1gInPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
donesly	donést	k5eAaPmAgInP
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
zprávy	zpráva	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
a	a	k8xC
Porýní	Porýní	k1gNnSc4
člen	člen	k1gMnSc1
Bernardova	Bernardův	k2eAgInSc2d1
vlastního	vlastní	k2eAgInSc2d1
řádu	řád	k1gInSc2
cisterciáků	cisterciák	k1gMnPc2
Rudolf	Rudolf	k1gMnSc1
rozněcuje	rozněcovat	k5eAaImIp3nS
protižidovské	protižidovský	k2eAgInPc4d1
nepokoje	nepokoj	k1gInPc4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
pod	pod	k7c7
záminkou	záminka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
Židé	Žid	k1gMnPc1
nepřispěli	přispět	k5eNaPmAgMnP
k	k	k7c3
záchraně	záchrana	k1gFnSc3
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arcibiskupové	arcibiskup	k1gMnPc1
Mohuče	Mohuč	k1gFnSc2
a	a	k8xC
Kolína	Kolín	k1gInSc2
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
proti	proti	k7c3
útokům	útok	k1gInPc3
na	na	k7c4
Židy	Žid	k1gMnPc4
důrazně	důrazně	k6eAd1
protestovali	protestovat	k5eAaBmAgMnP
a	a	k8xC
Bernard	Bernard	k1gMnSc1
okamžitě	okamžitě	k6eAd1
odcestoval	odcestovat	k5eAaPmAgMnS
do	do	k7c2
Kolína	Kolín	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Rudolfova	Rudolfův	k2eAgNnPc4d1
kázání	kázání	k1gNnPc4
odsoudil	odsoudit	k5eAaPmAgMnS
a	a	k8xC
samotnému	samotný	k2eAgMnSc3d1
mnichovi	mnich	k1gMnSc3
nařídil	nařídit	k5eAaPmAgMnS
návrat	návrat	k1gInSc4
do	do	k7c2
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Násilí	násilí	k1gNnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
rychle	rychle	k6eAd1
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Král	Král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
ve	v	k7c4
Vézelay	Vézela	k1gMnPc4
přijímá	přijímat	k5eAaImIp3nS
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
středověká	středověký	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
</s>
<s>
Bernard	Bernard	k1gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
vydal	vydat	k5eAaPmAgInS
na	na	k7c4
cestu	cesta	k1gFnSc4
po	po	k7c6
jižním	jižní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
a	a	k8xC
o	o	k7c6
Vánocích	Vánoce	k1gFnPc6
roku	rok	k1gInSc2
1146	#num#	k4
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgInS
ke	k	k7c3
dvoru	dvůr	k1gInSc3
německého	německý	k2eAgMnSc2d1
krále	král	k1gMnSc2
Konráda	Konrád	k1gMnSc2
ve	v	k7c6
Špýru	Špýr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
měl	mít	k5eAaImAgMnS
Bernard	Bernard	k1gMnSc1
kázání	kázání	k1gNnSc2
před	před	k7c7
králem	král	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
dvorem	dvůr	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
překvapivě	překvapivě	k6eAd1
na	na	k7c4
Konráda	Konrád	k1gMnSc4
nedokázal	dokázat	k5eNaPmAgMnS
svým	svůj	k3xOyFgInSc7
projevem	projev	k1gInSc7
zapůsobit	zapůsobit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Bernardovo	Bernardův	k2eAgNnSc1d1
kázání	kázání	k1gNnSc1
o	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
plamenější	plamený	k2eAgNnSc1d2
a	a	k8xC
tentokrát	tentokrát	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
již	již	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
Konráda	Konrád	k1gMnSc4
nadchnout	nadchnout	k5eAaPmF
pro	pro	k7c4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Spolu	spolu	k6eAd1
s	s	k7c7
Konrádem	Konrád	k1gMnSc7
přijal	přijmout	k5eAaPmAgMnS
kříž	kříž	k1gInSc4
z	z	k7c2
Bernardových	Bernardových	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
i	i	k8xC
jeho	jeho	k3xOp3gFnSc4
synovec	synovec	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Barbarossa	Barbarossa	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říši	říš	k1gFnSc6
předcházelo	předcházet	k5eAaImAgNnS
zahájení	zahájení	k1gNnSc1
tažení	tažení	k1gNnSc2
na	na	k7c6
jaře	jaro	k1gNnSc6
1147	#num#	k4
vyhlášení	vyhlášení	k1gNnSc2
zemského	zemský	k2eAgInSc2d1
míru	mír	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zklidnil	zklidnit	k5eAaPmAgInS
situaci	situace	k1gFnSc4
a	a	k8xC
odvrátil	odvrátit	k5eAaPmAgMnS
hrozící	hrozící	k2eAgFnSc4d1
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1
konfrontaci	konfrontace	k1gFnSc4
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
roku	rok	k1gInSc2
1147	#num#	k4
Konrád	Konrád	k1gMnSc1
svolal	svolat	k5eAaPmAgMnS
sněm	sněm	k1gInSc4
do	do	k7c2
Frankfurtu	Frankfurt	k1gInSc2
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
myšlenka	myšlenka	k1gFnSc1
uspořádat	uspořádat	k5eAaPmF
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
i	i	k9
do	do	k7c2
jiných	jiný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgFnS
Svatá	svatý	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Bernard	Bernard	k1gMnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
také	také	k9
do	do	k7c2
země	zem	k1gFnSc2
německé	německý	k2eAgFnSc2d1
a	a	k8xC
přišel	přijít	k5eAaPmAgInS
na	na	k7c4
slavný	slavný	k2eAgInSc4d1
sněm	sněm	k1gInSc4
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
právě	právě	k9
tehdy	tehdy	k6eAd1
slavnostně	slavnostně	k6eAd1
zavítal	zavítat	k5eAaPmAgMnS
král	král	k1gMnSc1
Konrád	Konrád	k1gMnSc1
se	s	k7c7
všemi	všecek	k3xTgMnPc7
knížaty	kníže	k1gMnPc7wR
<g/>
.	.	kIx.
...	...	k?
Ten	ten	k3xDgMnSc1
světec	světec	k1gMnSc1
začal	začít	k5eAaPmAgMnS
–	–	k?
nevím	vědět	k5eNaImIp1nS
<g/>
,	,	kIx,
jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
znamení	znamení	k1gNnSc4
je	být	k5eAaImIp3nS
poučila	poučit	k5eAaPmAgFnS
–	–	k?
vyzývat	vyzývat	k5eAaImF
knížata	kníže	k1gMnPc4wR
a	a	k8xC
ostatní	ostatní	k2eAgInSc1d1
křesťanský	křesťanský	k2eAgInSc1d1
lid	lid	k1gInSc1
k	k	k7c3
tažení	tažení	k1gNnSc3
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
podrobili	podrobit	k5eAaPmAgMnP
východní	východní	k2eAgInPc4d1
barbarské	barbarský	k2eAgInPc4d1
národy	národ	k1gInPc4
a	a	k8xC
podřídili	podřídit	k5eAaPmAgMnP
je	být	k5eAaImIp3nS
křesťanským	křesťanský	k2eAgInPc3d1
zákonům	zákon	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říkal	říkat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nastal	nastat	k5eAaPmAgInS
čas	čas	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
musí	muset	k5eAaImIp3nS
vstoupit	vstoupit	k5eAaPmF
[	[	kIx(
<g/>
do	do	k7c2
říše	říš	k1gFnSc2
Boží	boží	k2eAgNnSc1d1
<g/>
]	]	kIx)
spousta	spousta	k1gFnSc1
pohanů	pohan	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
bude	být	k5eAaImBp3nS
spasen	spasen	k2eAgInSc1d1
celý	celý	k2eAgInSc1d1
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1
<g/>
,	,	kIx,
jaké	jaký	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
davy	dav	k1gInPc1
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
na	na	k7c4
jeho	jeho	k3xOp3gFnPc4
výzvy	výzva	k1gFnPc4
hned	hned	k6eAd1
k	k	k7c3
tomu	ten	k3xDgNnSc3
tažení	tažení	k1gNnSc3
zaslíbily	zaslíbit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
a	a	k8xC
nejpřednější	přední	k2eAgMnSc1d3
byli	být	k5eAaImAgMnP
král	král	k1gMnSc1
Konrád	Konrád	k1gMnSc1
<g/>
,	,	kIx,
švábský	švábský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgInS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Welf	Welf	k1gMnSc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
biskupy	biskup	k1gInPc7
a	a	k8xC
knížaty	kníže	k1gNnPc7
a	a	k8xC
vojskem	vojsko	k1gNnSc7
urozených	urozený	k2eAgInPc2d1
<g/>
,	,	kIx,
neurozených	urozený	k2eNgInPc2d1
i	i	k8xC
prostých	prostý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
v	v	k7c6
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
nelze	lze	k6eNd1
spočítat	spočítat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Helmold	Helmold	k1gInSc1
z	z	k7c2
Bosau	Bosaus	k1gInSc2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
tak	tak	k6eAd1
již	již	k6eAd1
měla	mít	k5eAaImAgFnS
konkrétní	konkrétní	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Francouzi	Francouz	k1gMnPc1
se	se	k3xPyFc4
rozhodovali	rozhodovat	k5eAaImAgMnP
mezi	mezi	k7c7
cestami	cesta	k1gFnPc7
přes	přes	k7c4
Itálii	Itálie	k1gFnSc4
a	a	k8xC
přes	přes	k7c4
Balkán	Balkán	k1gInSc4
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
zvolil	zvolit	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
přes	přes	k7c4
Uhersko	Uhersko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
a	a	k8xC
pád	pád	k1gInSc1
Lisabonu	Lisabon	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Obléhání	obléhání	k1gNnSc2
Lisabonu	Lisabon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Lisabonu	Lisabon	k1gInSc2
<g/>
,	,	kIx,
Alfredo	Alfredo	k1gNnSc4
Roque	Roqu	k1gFnSc2
Gameiro	Gameiro	k1gNnSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Italské	italský	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
se	se	k3xPyFc4
od	od	k7c2
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
distancovaly	distancovat	k5eAaBmAgFnP
<g/>
,	,	kIx,
načež	načež	k6eAd1
si	se	k3xPyFc3
Janov	Janov	k1gInSc4
od	od	k7c2
papeže	papež	k1gMnSc2
vyžádal	vyžádat	k5eAaPmAgMnS
svolení	svolení	k1gNnSc4
soustředit	soustředit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
válku	válka	k1gFnSc4
proti	proti	k7c3
Maurům	Maur	k1gMnPc3
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Papež	Papež	k1gMnSc1
na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1147	#num#	k4
schválil	schválit	k5eAaPmAgInS
rozšíření	rozšíření	k1gNnSc4
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
reconquisty	reconquista	k1gMnSc2
také	také	k9
na	na	k7c4
Pyrenejský	pyrenejský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
a	a	k8xC
králi	král	k1gMnSc3
Alfonsi	Alfons	k1gMnSc3
VII	VII	kA
<g/>
.	.	kIx.
dovolil	dovolit	k5eAaPmAgMnS
považovat	považovat	k5eAaImF
jeho	jeho	k3xOp3gNnSc4
tažení	tažení	k1gNnSc4
proti	proti	k7c3
Maurům	Maur	k1gMnPc3
za	za	k7c4
součást	součást	k1gFnSc4
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
1147	#num#	k4
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
cestu	cesta	k1gFnSc4
po	po	k7c6
moři	moře	k1gNnSc6
do	do	k7c2
Svaté	svatá	k1gFnSc2
země	země	k1gFnSc1
kontingent	kontingent	k1gInSc1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Dartmouthu	Dartmouth	k1gInSc2
s	s	k7c7
anglickými	anglický	k2eAgInPc7d1
a	a	k8xC
vlámskými	vlámský	k2eAgInPc7d1
křižáky	křižák	k1gInPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
špatnému	špatný	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
byli	být	k5eAaImAgMnP
Angličané	Angličan	k1gMnPc1
donuceni	donucen	k2eAgMnPc1d1
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1147	#num#	k4
přistát	přistát	k5eAaImF,k5eAaPmF
v	v	k7c6
severoportugalském	severoportugalský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Porto	porto	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Duero	Duero	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Portu	port	k1gInSc6
byli	být	k5eAaImAgMnP
křižáci	křižák	k1gMnPc1
místním	místní	k2eAgFnPc3d1
biskupem	biskup	k1gInSc7
požádáni	požádat	k5eAaPmNgMnP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
aby	aby	kYmCp3nP
se	se	k3xPyFc4
setkali	setkat	k5eAaPmAgMnP
s	s	k7c7
králem	král	k1gMnSc7
Alfonsem	Alfons	k1gMnSc7
Portugalským	portugalský	k2eAgMnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
Alfonsovi	Alfonsův	k2eAgMnPc1d1
přislíbili	přislíbit	k5eAaPmAgMnP
za	za	k7c4
podíl	podíl	k1gInSc4
na	na	k7c6
kořisti	kořist	k1gFnSc6
pomoc	pomoc	k1gFnSc4
při	při	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Lisabon	Lisabon	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
centrem	centr	k1gMnSc7
muslimské	muslimský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
základnou	základna	k1gFnSc7
pirátů	pirát	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
Alfons	Alfons	k1gMnSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
portugalský	portugalský	k2eAgMnSc1d1
<g/>
,	,	kIx,
se	s	k7c7
souhlasem	souhlas	k1gInSc7
všech	všecek	k3xTgMnPc2
svých	svůj	k3xOyFgMnPc2
věrných	věrný	k2eAgMnPc2d1
a	a	k8xC
pro	pro	k7c4
věčné	věčný	k2eAgNnSc4d1
uchování	uchování	k1gNnSc4
v	v	k7c6
paměti	paměť	k1gFnSc6
všech	všecek	k3xTgMnPc2
budoucích	budoucí	k2eAgMnPc2d1
<g/>
,	,	kIx,
potvrzuji	potvrzovat	k5eAaImIp1nS
v	v	k7c6
této	tento	k3xDgFnSc6
listině	listina	k1gFnSc6
následující	následující	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
<g/>
:	:	kIx,
Frankové	Frank	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
po	po	k7c6
mém	můj	k3xOp1gInSc6
boku	bok	k1gInSc6
zúčastní	zúčastnit	k5eAaPmIp3nS
obléhání	obléhání	k1gNnSc1
města	město	k1gNnSc2
Lisabonu	Lisabon	k1gInSc2
<g/>
,	,	kIx,
převedou	převést	k5eAaPmIp3nP
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
vlastnictví	vlastnictví	k1gNnSc2
a	a	k8xC
moci	moct	k5eAaImF
nepřátelský	přátelský	k2eNgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
podrží	podržet	k5eAaPmIp3nS
jej	on	k3xPp3gInSc4
bez	bez	k7c2
nároku	nárok	k1gInSc2
na	na	k7c4
podíl	podíl	k1gInSc4
ze	z	k7c2
strany	strana	k1gFnSc2
mé	můj	k3xOp1gNnSc1
či	či	k9wB
mých	můj	k3xOp1gMnPc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
zajatí	zajatý	k2eAgMnPc1d1
nepřátelé	nepřítel	k1gMnPc1
projeví	projevit	k5eAaPmIp3nP
přání	přání	k1gNnSc4
vykoupit	vykoupit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
<g/>
,	,	kIx,
připadne	připadnout	k5eAaPmIp3nS
Frankům	Frank	k1gMnPc3
bez	bez	k7c2
výhrad	výhrada	k1gFnPc2
také	také	k9
výkupné	výkupné	k1gNnSc1
<g/>
,	,	kIx,
samotné	samotný	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
však	však	k9
vrátí	vrátit	k5eAaPmIp3nS
mně	já	k3xPp1nSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podaří	podařit	k5eAaPmIp3nS
zmocnit	zmocnit	k5eAaPmF
se	se	k3xPyFc4
města	město	k1gNnPc1
<g/>
,	,	kIx,
dostanou	dostat	k5eAaPmIp3nP
je	on	k3xPp3gInPc4
a	a	k8xC
podrží	podržet	k5eAaPmIp3nS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
bude	být	k5eAaImBp3nS
prohledáno	prohledán	k2eAgNnSc1d1
a	a	k8xC
vyrabováno	vyrabován	k2eAgNnSc1d1
<g/>
...	...	k?
<g/>
Teprve	teprve	k6eAd1
potom	potom	k6eAd1
<g/>
,	,	kIx,
až	až	k8xS
město	město	k1gNnSc1
prohledají	prohledat	k5eAaPmIp3nP
podle	podle	k7c2
své	svůj	k3xOyFgFnSc2
libovůle	libovůle	k1gFnSc2
<g/>
,	,	kIx,
předají	předat	k5eAaPmIp3nP
je	on	k3xPp3gInPc4
mně	já	k3xPp1nSc6
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
úryvek	úryvek	k1gInSc1
z	z	k7c2
dopisu	dopis	k1gInSc2
křižáka	křižák	k1gMnSc2
Raula	Raul	k1gMnSc2
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pyrenejský	pyrenejský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
roku	rok	k1gInSc2
1150	#num#	k4
<g/>
Joaquim	Joaquimo	k1gNnPc2
Rodrigues	Rodriguesa	k1gFnPc2
Braga	Braga	k1gFnSc1
<g/>
:	:	kIx,
kapitulace	kapitulace	k1gFnSc1
Lisabonu	Lisabon	k1gInSc2
<g/>
,	,	kIx,
romantický	romantický	k2eAgInSc1d1
portrét	portrét	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1840	#num#	k4
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Lisabonu	Lisabon	k1gInSc2
začalo	začít	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
čtyřech	čtyři	k4xCgInPc6
měsících	měsíc	k1gInPc6
bojů	boj	k1gInPc2
<g/>
,	,	kIx,
maurští	maurský	k2eAgMnPc1d1
vládcové	vládce	k1gMnPc1
souhlasili	souhlasit	k5eAaImAgMnP
s	s	k7c7
kapitulací	kapitulace	k1gFnSc7
vyhladovělého	vyhladovělý	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
slíbili	slíbit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Maury	Maur	k1gMnPc7
nechají	nechat	k5eAaPmIp3nP
v	v	k7c6
klidu	klid	k1gInSc6
odejít	odejít	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
jakmile	jakmile	k8xS
obránci	obránce	k1gMnPc1
otevřeli	otevřít	k5eAaPmAgMnP
brány	brána	k1gFnPc4
<g/>
,	,	kIx,
křižáci	křižák	k1gMnPc1
vtrhli	vtrhnout	k5eAaPmAgMnP
do	do	k7c2
města	město	k1gNnSc2
a	a	k8xC
všechny	všechen	k3xTgMnPc4
muslimy	muslim	k1gMnPc4
zmasakrovali	zmasakrovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Lisabonu	Lisabon	k1gInSc2
představovalo	představovat	k5eAaImAgNnS
pro	pro	k7c4
křesťanstvo	křesťanstvo	k1gNnSc4
trvalý	trvalý	k2eAgInSc4d1
a	a	k8xC
cenný	cenný	k2eAgInSc4d1
zisk	zisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
velké	velký	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
úrodné	úrodný	k2eAgFnPc1d1
půdy	půda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nyní	nyní	k6eAd1
ležely	ležet	k5eAaImAgFnP
ladem	ladem	k6eAd1
a	a	k8xC
pro	pro	k7c4
Portugalsko	Portugalsko	k1gNnSc4
byli	být	k5eAaImAgMnP
zapotřebí	zapotřebí	k6eAd1
křesťanští	křesťanský	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Angličanů	Angličan	k1gMnPc2
a	a	k8xC
Vlámů	Vlám	k1gMnPc2
proto	proto	k8xC
již	již	k6eAd1
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
nepokračovala	pokračovat	k5eNaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
natrvalo	natrvalo	k6eAd1
se	se	k3xPyFc4
usadila	usadit	k5eAaPmAgFnS
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
prvním	první	k4xOgInSc7
lisabonským	lisabonský	k2eAgInSc7d1
biskupem	biskup	k1gInSc7
se	se	k3xPyFc4
po	po	k7c6
dobytí	dobytí	k1gNnSc6
města	město	k1gNnSc2
stal	stát	k5eAaPmAgInS
Angličan	Angličan	k1gMnSc1
a	a	k8xC
samotný	samotný	k2eAgInSc1d1
Lisabon	Lisabon	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
stal	stát	k5eAaPmAgInS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Portugalského	portugalský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
křižáků	křižák	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
rozhodla	rozhodnout	k5eAaPmAgFnS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
křížové	křížový	k2eAgFnSc6d1
vpravě	vprava	k1gFnSc6
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
další	další	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
křižáci	křižák	k1gMnPc1
s	s	k7c7
Portugalci	Portugalec	k1gMnPc7
bojovali	bojovat	k5eAaImAgMnP
u	u	k7c2
Lisabonu	Lisabon	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
král	král	k1gMnSc1
Alfons	Alfons	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
švagrem	švagr	k1gMnSc7
Ramonem	Ramon	k1gMnSc7
Berenguerem	Berenguer	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
z	z	k7c2
Barcelony	Barcelona	k1gFnSc2
a	a	k8xC
kontingenty	kontingent	k1gInPc1
katalánských	katalánský	k2eAgMnPc2d1
a	a	k8xC
francouzských	francouzský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
pokoušel	pokoušet	k5eAaImAgInS
dobýt	dobýt	k5eAaPmF
bohaté	bohatý	k2eAgNnSc1d1
maurské	maurský	k2eAgNnSc1d1
přístavní	přístavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Almería	Almerí	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
podporou	podpora	k1gFnSc7
spojeného	spojený	k2eAgNnSc2d1
pisánsko-janovského	pisánsko-janovský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
se	se	k3xPyFc4
křižákům	křižák	k1gMnPc3
v	v	k7c6
říjnu	říjen	k1gInSc6
1147	#num#	k4
město	město	k1gNnSc1
podařilo	podařit	k5eAaPmAgNnS
obsadit	obsadit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ramon	Ramona	k1gFnPc2
Berenguer	Berengura	k1gFnPc2
poté	poté	k6eAd1
vpadl	vpadnout	k5eAaPmAgMnS
na	na	k7c6
území	území	k1gNnSc6
taifátů	taifát	k1gInPc2
Valencie	Valencie	k1gFnSc2
a	a	k8xC
Murcie	Murcie	k1gFnSc2
roku	rok	k1gInSc2
1145	#num#	k4
se	se	k3xPyFc4
odtrhnuvších	odtrhnuvší	k2eAgMnPc2d1
od	od	k7c2
Almorávidů	Almorávid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
1148	#num#	k4
barcelonský	barcelonský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
po	po	k7c6
pětiměsíčním	pětiměsíční	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
<g/>
,	,	kIx,
za	za	k7c2
pomoci	pomoc	k1gFnSc2
francouzských	francouzský	k2eAgInPc2d1
a	a	k8xC
janovských	janovský	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
<g/>
,	,	kIx,
dobyl	dobýt	k5eAaPmAgInS
Tortosu	Tortosa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
do	do	k7c2
Berenguerových	Berenguerův	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
také	také	k9
Fraga	Fraga	k1gFnSc1
<g/>
,	,	kIx,
Lleida	Lleida	k1gFnSc1
a	a	k8xC
Mequinenza	Mequinenza	k1gFnSc1
na	na	k7c6
soutoku	soutok	k1gInSc6
řek	řeka	k1gFnPc2
Segre	Segr	k1gInSc5
a	a	k8xC
Ebro	Ebro	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Venedská	Venedský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Venedská	Venedský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Zajmutí	Zajmutí	k1gNnSc1
Venedů	Vened	k1gMnPc2
od	od	k7c2
Wojciecha	Wojciech	k1gMnSc2
Gersona	Gerson	k1gMnSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1147	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Polabí	Polabí	k1gNnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgNnSc1d1
Meklenbursko	Meklenbursko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vítězství	vítězství	k1gNnSc1
křižáků	křižák	k1gInPc2
<g/>
,	,	kIx,
částečná	částečný	k2eAgFnSc1d1
konverze	konverze	k1gFnSc1
Polabských	polabský	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
ke	k	k7c3
katolictví	katolictví	k1gNnSc3
<g/>
,	,	kIx,
k	k	k7c3
přestupu	přestup	k1gInSc3
na	na	k7c4
novou	nový	k2eAgFnSc4d1
víru	víra	k1gFnSc4
rovněž	rovněž	k9
souhlasili	souhlasit	k5eAaImAgMnP
obodritská	obodritský	k2eAgNnPc4d1
knížata	kníže	k1gNnPc4
Niklot	Niklota	k1gFnPc2
a	a	k8xC
Pribislav	Pribislava	k1gFnPc2
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Braniborsko	Braniborsko	k1gNnSc1
znovudobylo	znovudobýt	k5eAaPmAgNnS
město	město	k1gNnSc1
Havelberg	Havelberg	k1gInSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Sasko	Sasko	k1gNnSc1
</s>
<s>
Míšeňsko	Míšeňsko	k6eAd1
</s>
<s>
Lužice	Lužice	k1gFnSc1
</s>
<s>
Havelberské	Havelberský	k2eAgNnSc4d1
biskupství	biskupství	k1gNnSc4
</s>
<s>
Braniborsko	Braniborsko	k1gNnSc1
</s>
<s>
Holštýnsko	Holštýnsko	k1gNnSc1
</s>
<s>
Brémské	brémský	k2eAgNnSc1d1
arcibiskupství	arcibiskupství	k1gNnSc1
</s>
<s>
Mohučské	mohučský	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc1
</s>
<s>
Halberstadtské	Halberstadtský	k2eAgNnSc4d1
biskupství	biskupství	k1gNnSc4
</s>
<s>
Münsterské	Münsterský	k2eAgNnSc1d1
arcibiskupství	arcibiskupství	k1gNnSc1
</s>
<s>
Olomoucké	olomoucký	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
</s>
<s>
Braniborské	braniborský	k2eAgNnSc4d1
biskupství	biskupství	k1gNnSc4
</s>
<s>
Merseburské	Merseburský	k2eAgNnSc4d1
biskupství	biskupství	k1gNnSc4
</s>
<s>
Zähringenské	Zähringenský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
</s>
<s>
Dánské	dánský	k2eAgNnSc1d1
Jutsko	Jutsko	k1gNnSc1
Knuta	knuta	k1gFnSc1
V.	V.	kA
</s>
<s>
Dánský	dánský	k2eAgInSc1d1
Sjæ	Sjæ	k1gInSc1
a	a	k8xC
Skå	Skå	k1gInSc5
Svena	Sveno	k1gNnPc4
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
polská	polský	k2eAgNnPc1d1
knížectví	knížectví	k1gNnPc1
</s>
<s>
Venedové	Vened	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Obodritský	Obodritský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Obodritský	Obodritský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
(	(	kIx(
<g/>
Obodrité	Obodrita	k1gMnPc1
<g/>
,	,	kIx,
Vágrové	Vágrové	k2eAgMnPc1d1
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
<s>
Lutický	Lutický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Lutický	Lutický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Pomořansko	Pomořansko	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Anselm	Anselm	k6eAd1
z	z	k7c2
Havelbergu	Havelberg	k1gInSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Lev	Lev	k1gMnSc1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
Medvěd	medvěd	k1gMnSc1
</s>
<s>
Knut	knuta	k1gFnPc2
V.	V.	kA
</s>
<s>
Sven	Sven	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Adalbert	Adalbert	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Mohučský	mohučský	k2eAgMnSc1d1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
z	z	k7c2
Halberstadtu	Halberstadt	k1gInSc2
</s>
<s>
Werner	Werner	k1gMnSc1
z	z	k7c2
Münsteru	Münster	k1gInSc2
</s>
<s>
Wiggar	Wiggar	k1gInSc1
z	z	k7c2
Brandenburku	Brandenburk	k1gInSc2
</s>
<s>
Reinhard	Reinhard	k1gInSc1
z	z	k7c2
Merseburku	Merseburk	k1gInSc2
</s>
<s>
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Wettinu	Wettin	k1gInSc2
</s>
<s>
Adolf	Adolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Zdík	Zdík	k1gMnSc1
</s>
<s>
Konrád	Konrád	k1gMnSc1
ze	z	k7c2
Zähringenu	Zähringen	k1gInSc2
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
ze	z	k7c2
Salzwedelu	Salzwedel	k1gInSc2
</s>
<s>
neznámý	známý	k2eNgMnSc1d1
polský	polský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
</s>
<s>
Venedové	Vened	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Niklot	Niklot	k1gMnSc1
</s>
<s>
Pribislav	Pribislav	k1gMnSc1
z	z	k7c2
Vagrie	Vagrie	k1gFnSc2
</s>
<s>
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Ratibor	Ratibor	k1gInSc1
Pomořanský	pomořanský	k2eAgInSc1d1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Ke	k	k7c3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
se	se	k3xPyFc4
přihlásilo	přihlásit	k5eAaPmAgNnS
množství	množství	k1gNnSc3
jihoněmeckých	jihoněmecký	k2eAgMnPc2d1
feudálů	feudál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
velmoži	velmož	k1gMnSc3
ze	z	k7c2
severoněmeckých	severoněmecký	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
nebyli	být	k5eNaImAgMnP
tak	tak	k6eAd1
nadšení	nadšený	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáli	stát	k5eAaImAgMnP
před	před	k7c7
volbou	volba	k1gFnSc7
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
přidají	přidat	k5eAaPmIp3nP
ke	k	k7c3
Konrádově	Konrádův	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
či	či	k8xC
k	k	k7c3
výpravě	výprava	k1gFnSc3
proti	proti	k7c3
pohanským	pohanský	k2eAgInPc3d1
slovanským	slovanský	k2eAgInPc3d1
kmenům	kmen	k1gInPc3
žijícím	žijící	k2eAgInPc3d1
v	v	k7c6
povodí	povodí	k1gNnSc6
Labe	Labe	k1gNnSc2
a	a	k8xC
Odry	Odra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Papež	Papež	k1gMnSc1
Evžen	Evžen	k1gMnSc1
německým	německý	k2eAgMnPc3d1
rytířům	rytíř	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
pro	pro	k7c4
tažení	tažení	k1gNnSc4
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
schválil	schválit	k5eAaPmAgInS
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
vydal	vydat	k5eAaPmAgMnS
bulu	bula	k1gFnSc4
Divina	divin	k2eAgNnSc2d1
dispensatione	dispensation	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bula	bula	k1gFnSc1
stanovovala	stanovovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
rozdíl	rozdíl	k1gInSc4
v	v	k7c6
duchovní	duchovní	k2eAgFnSc6d1
odměně	odměna	k1gFnSc6
pro	pro	k7c4
křižáky	křižák	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
táhnou	táhnout	k5eAaImIp3nP
na	na	k7c4
sever	sever	k1gInSc4
místo	místo	k6eAd1
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytíři	Rytíř	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
připravovali	připravovat	k5eAaImAgMnP
na	na	k7c4
křížové	křížový	k2eAgNnSc4d1
tažení	tažení	k1gNnSc4
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
byli	být	k5eAaImAgMnP
především	především	k9
Dánové	Dán	k1gMnPc1
<g/>
,	,	kIx,
Sasové	Sas	k1gMnPc1
a	a	k8xC
Poláci	Polák	k1gMnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
nechyběli	chybět	k5eNaImAgMnP
také	také	k9
mnozí	mnohý	k2eAgMnPc1d1
Češi	Čech	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Vrchní	vrchní	k1gMnSc1
velení	velení	k1gNnSc2
získal	získat	k5eAaPmAgMnS
papežský	papežský	k2eAgMnSc1d1
legát	legát	k1gMnSc1
Anselm	Anselm	k1gMnSc1
z	z	k7c2
Havelbergu	Havelberg	k1gInSc2
<g/>
,	,	kIx,
samotnému	samotný	k2eAgNnSc3d1
tažení	tažení	k1gNnSc3
však	však	k9
prakticky	prakticky	k6eAd1
veleli	velet	k5eAaImAgMnP
jednotliví	jednotlivý	k2eAgMnPc1d1
velmoži	velmož	k1gMnPc1
ze	z	k7c2
saských	saský	k2eAgInPc2d1
rodů	rod	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
Askánců	Askánec	k1gMnPc2
<g/>
,	,	kIx,
Wettinů	Wettin	k1gMnPc2
či	či	k8xC
hrabat	hrabě	k1gNnPc2
ze	z	k7c2
Schauenburgu	Schauenburg	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Třetí	třetí	k4xOgNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
na	na	k7c4
sebe	sebe	k3xPyFc4
vzalo	vzít	k5eAaPmAgNnS
znamení	znamení	k1gNnSc1
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
zasvětilo	zasvětit	k5eAaPmAgNnS
[	[	kIx(
<g/>
tažení	tažení	k1gNnSc1
<g/>
]	]	kIx)
proti	proti	k7c3
kmeni	kmen	k1gInSc3
Slovanů	Slovan	k1gMnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
proti	proti	k7c3
našim	náš	k3xOp1gMnPc3
sousedům	soused	k1gMnPc3
Obodritům	Obodrit	k1gInPc3
a	a	k8xC
Luticům	Lutice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodlalo	hodlat	k5eAaImAgNnS
pomstít	pomstít	k5eAaPmF
vraždy	vražda	k1gFnSc2
a	a	k8xC
stíhání	stíhání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
[	[	kIx(
<g/>
Slované	Slovan	k1gMnPc5
<g/>
]	]	kIx)
způsobili	způsobit	k5eAaPmAgMnP
křesťanům	křesťan	k1gMnPc3
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
pak	pak	k6eAd1
Dánům	Dán	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původci	původce	k1gMnPc1
této	tento	k3xDgFnSc2
výpravy	výprava	k1gFnSc2
byli	být	k5eAaImAgMnP
[	[	kIx(
<g/>
arcibiskup	arcibiskup	k1gMnSc1
<g/>
]	]	kIx)
Adalbero	Adalbero	k1gNnSc1
Hamburský	hamburský	k2eAgMnSc1d1
a	a	k8xC
všichni	všechen	k3xTgMnPc1
biskupové	biskup	k1gMnPc1
Saska	Sasko	k1gNnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
dospívající	dospívající	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
[	[	kIx(
<g/>
Lev	Lev	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Zähringenu	Zähringen	k1gInSc2
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
ze	z	k7c2
Salzwedelu	Salzwedel	k1gInSc2
a	a	k8xC
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Wettinu	Wettin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Helmold	Helmold	k1gInSc1
z	z	k7c2
Bosau	Bosaus	k1gInSc2
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Památník	památník	k1gInSc1
Abrechta	Abrecht	k1gMnSc2
I.	I.	kA
Medvěda	medvěd	k1gMnSc2
na	na	k7c6
hradě	hrad	k1gInSc6
Špandava	Špandava	k1gFnSc1
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
</s>
<s>
Znervoznění	Znervoznění	k1gNnSc1
německou	německý	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
,	,	kIx,
Obodrité	Obodrita	k1gMnPc1
<g/>
,	,	kIx,
kmenový	kmenový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
polabských	polabský	k2eAgMnPc2d1
Slovanů	Slovan	k1gMnPc2
<g/>
,	,	kIx,
podnikl	podniknout	k5eAaPmAgMnS
v	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
1147	#num#	k4
vpád	vpád	k1gInSc1
na	na	k7c4
německé	německý	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
proti	proti	k7c3
Obodritům	Obodrit	k1gInPc3
vytáhli	vytáhnout	k5eAaPmAgMnP
v	v	k7c6
pozdním	pozdní	k2eAgNnSc6d1
létě	léto	k1gNnSc6
1147	#num#	k4
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
úspěšně	úspěšně	k6eAd1
vytlačit	vytlačit	k5eAaPmF
Slovany	Slovan	k1gInPc4
z	z	k7c2
křesťanských	křesťanský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
zaměřili	zaměřit	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
obodritskou	obodritský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Dobin	Dobina	k1gFnPc2
a	a	k8xC
veletskou	veletský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
v	v	k7c6
Demminu	Demmin	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
byl	být	k5eAaImAgMnS
i	i	k9
dánský	dánský	k2eAgMnSc1d1
král	král	k1gMnSc1
Knut	knuta	k1gFnPc2
V.	V.	kA
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
spoluvládce	spoluvládce	k1gMnSc1
Sven	Sven	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
Severní	severní	k2eAgFnSc2d1
marky	marka	k1gFnSc2
Albrecht	Albrecht	k1gMnSc1
Medvěd	medvěd	k1gMnSc1
i	i	k8xC
saský	saský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Lev	Lev	k1gMnSc1
<g/>
,	,	kIx,
zaútočili	zaútočit	k5eAaPmAgMnP
nejprve	nejprve	k6eAd1
na	na	k7c4
Dobin	Dobin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
z	z	k7c2
křižáků	křižák	k1gMnPc2
začali	začít	k5eAaPmAgMnP
drancovat	drancovat	k5eAaImF
okolí	okolí	k1gNnSc4
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
ale	ale	k8xC
plundrování	plundrování	k1gNnPc4
neschvalovali	schvalovat	k5eNaImAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
tuto	tento	k3xDgFnSc4
zemi	zem	k1gFnSc4
nevnímali	vnímat	k5eNaImAgMnP
jako	jako	k8xS,k8xC
zemi	zem	k1gFnSc4
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k9
dobyté	dobytý	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jim	on	k3xPp3gMnPc3
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Saské	saský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
Jidřicha	Jidřich	k1gMnSc2
Lva	Lev	k1gMnSc2
u	u	k7c2
Dobinu	Dobin	k1gInSc2
ustoupilo	ustoupit	k5eAaPmAgNnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
kníže	kníže	k1gMnSc1
Niklot	Niklot	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
přijme	přijmout	k5eAaPmIp3nS
křest	křest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obléhání	obléhání	k1gNnSc1
Demminu	Demmin	k1gInSc2
však	však	k9
úspěšné	úspěšný	k2eAgNnSc1d1
nebylo	být	k5eNaImAgNnS
a	a	k8xC
křižácká	křižácký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
tedy	tedy	k9
nechala	nechat	k5eAaPmAgFnS
markrabaty	markrabě	k1gNnPc7
odklonit	odklonit	k5eAaPmF
do	do	k7c2
Pomořanska	Pomořansko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
táhla	táhnout	k5eAaImAgFnS
na	na	k7c4
východ	východ	k1gInSc4
až	až	k9
dosáhla	dosáhnout	k5eAaPmAgFnS
města	město	k1gNnSc2
Štětín	Štětín	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
však	však	k9
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
křesťanské	křesťanský	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
setkání	setkání	k1gNnSc6
křižáků	křižák	k1gInPc2
s	s	k7c7
místním	místní	k2eAgMnSc7d1
biskupem	biskup	k1gMnSc7
Albrechtem	Albrecht	k1gMnSc7
a	a	k8xC
knížetem	kníže	k1gMnSc7
Ratiborem	Ratibor	k1gMnSc7
I.	I.	kA
se	se	k3xPyFc4
křižácká	křižácký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzva	výzva	k1gFnSc1
Bernarda	Bernard	k1gMnSc2
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
ke	k	k7c3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
proti	proti	k7c3
Slovanům	Slovan	k1gInPc3
byla	být	k5eAaImAgFnS
nesmlouvavá	smlouvavý	k2eNgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Kategoricky	kategoricky	k6eAd1
zakazujeme	zakazovat	k5eAaImIp1nP
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k9
z	z	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
příměří	příměří	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
uzavřeno	uzavřen	k2eAgNnSc1d1
s	s	k7c7
těmito	tento	k3xDgMnPc7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
buď	buď	k8xC
kvůli	kvůli	k7c3
penězům	peníze	k1gInPc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
kvůli	kvůli	k7c3
poplatku	poplatek	k1gInSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
takové	takový	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
s	s	k7c7
boží	boží	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
budou	být	k5eAaImBp3nP
buď	buď	k8xC
obráceni	obrátit	k5eAaPmNgMnP
na	na	k7c6
křesťanství	křesťanství	k1gNnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
vyhubeni	vyhuben	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nicméně	nicméně	k8xC
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
selhala	selhat	k5eAaPmAgFnS
i	i	k9
při	při	k7c6
pokusu	pokus	k1gInSc6
obrátit	obrátit	k5eAaPmF
na	na	k7c4
křesťanství	křesťanství	k1gNnSc4
slovanské	slovanský	k2eAgMnPc4d1
Venedy	Vened	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasové	Sas	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
sice	sice	k8xC
obrácení	obrácení	k1gNnSc4
pohanů	pohan	k1gMnPc2
v	v	k7c6
Dobinu	Dobin	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
Slované	Slovan	k1gMnPc1
se	se	k3xPyFc4
ke	k	k7c3
svým	svůj	k3xOyFgInPc3
pohanským	pohanský	k2eAgInPc3d1
zvykům	zvyk	k1gInPc3
vrátili	vrátit	k5eAaPmAgMnP
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
se	se	k3xPyFc4
křižácká	křižácký	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
rozpadla	rozpadnout	k5eAaPmAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Albrecht	Albrecht	k1gMnSc1
z	z	k7c2
Pomořan	Pomořan	k1gMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Pokud	pokud	k8xS
by	by	kYmCp3nP
přišli	přijít	k5eAaPmAgMnP
posílit	posílit	k5eAaPmF
křesťanskou	křesťanský	k2eAgFnSc4d1
víru	víra	k1gFnSc4
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
měli	mít	k5eAaImAgMnP
tak	tak	k6eAd1
učinit	učinit	k5eAaImF,k5eAaPmF
kázáním	kázání	k1gNnSc7
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Albrecht	Albrecht	k1gMnSc1
z	z	k7c2
Pomořan	Pomořan	k1gMnSc1
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
dohra	dohra	k1gFnSc1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Meklenbursko	Meklenbursko	k1gNnSc1
a	a	k8xC
Pomořansko	Pomořansko	k1gNnSc1
především	především	k6eAd1
saskými	saský	k2eAgInPc7d1
křižáky	křižák	k1gInPc7
Jidřicha	Jidřich	k1gMnSc2
Lva	Lev	k1gMnSc2
vydrancováno	vydrancován	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
čin	čin	k1gInSc1
oslabil	oslabit	k5eAaPmAgInS
vliv	vliv	k1gInSc4
Slovanů	Slovan	k1gMnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
omezilo	omezit	k5eAaPmAgNnS
jejich	jejich	k3xOp3gInSc4
odpor	odpor	k1gInSc4
vůči	vůči	k7c3
křesťanské	křesťanský	k2eAgFnSc3d1
expanzi	expanze	k1gFnSc3
a	a	k8xC
napomohlo	napomoct	k5eAaPmAgNnS
křesťanům	křesťan	k1gMnPc3
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
přinést	přinést	k5eAaPmF
další	další	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Tažení	tažení	k1gNnSc1
na	na	k7c4
východ	východ	k1gInSc4
</s>
<s>
Tažení	tažení	k1gNnSc4
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
François-Marius	François-Marius	k1gMnSc1
Granet	Granet	k1gMnSc1
<g/>
:	:	kIx,
Sněm	sněm	k1gInSc1
templářských	templářský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
papeže	papež	k1gMnSc2
Evžena	Evžen	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1147	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1147	#num#	k4
se	se	k3xPyFc4
francouzští	francouzský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
shromáždili	shromáždit	k5eAaPmAgMnP
v	v	k7c4
Étampes	Étampes	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
projednali	projednat	k5eAaPmAgMnP
detaily	detail	k1gInPc4
tažení	tažení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
již	již	k6eAd1
měli	mít	k5eAaImAgMnP
trasu	trasa	k1gFnSc4
naplánovanou	naplánovaný	k2eAgFnSc4d1
přes	přes	k7c4
Uhersko	Uhersko	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
sicilský	sicilský	k2eAgMnSc1d1
král	král	k1gMnSc1
Roger	Roger	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgMnS
Konrádovým	Konrádův	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgMnS
nepřítelem	nepřítel	k1gMnSc7
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
francouzští	francouzský	k2eAgMnPc1d1
páni	pan	k1gMnPc1
prosazovali	prosazovat	k5eAaImAgMnP
cestu	cesta	k1gFnSc4
přes	přes	k7c4
Itálii	Itálie	k1gFnSc4
a	a	k8xC
následně	následně	k6eAd1
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
především	především	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
neměli	mít	k5eNaImAgMnP
s	s	k7c7
Byzancí	Byzanc	k1gFnSc7
nejlepší	dobrý	k2eAgMnPc4d3
vztahy	vztah	k1gInPc4
a	a	k8xC
dávali	dávat	k5eAaImAgMnP
přednost	přednost	k1gFnSc4
spojenectví	spojenectví	k1gNnSc2
s	s	k7c7
Rogerem	Roger	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnPc7
sicilskými	sicilský	k2eAgMnPc7d1
Normany	Norman	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
nakonec	nakonec	k6eAd1
převážil	převážit	k5eAaPmAgInS
názor	názor	k1gInSc1
táhnout	táhnout	k5eAaImF
přes	přes	k7c4
Uhersko	Uhersko	k1gNnSc4
stejnou	stejný	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
táhli	táhnout	k5eAaImAgMnP
Konrádovi	Konrádův	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tažení	tažení	k1gNnSc1
bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
nepřítomnosti	nepřítomnost	k1gFnSc2
krále	král	k1gMnPc4
byli	být	k5eAaImAgMnP
zvoleni	zvolen	k2eAgMnPc1d1
regenty	regens	k1gMnPc4
království	království	k1gNnSc2
hrabě	hrabě	k1gMnSc1
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Nevers	Neversa	k1gFnPc2
a	a	k8xC
opat	opat	k1gMnSc1
Suger	Suger	k1gMnSc1
a	a	k8xC
sama	sám	k3xTgFnSc1
církev	církev	k1gFnSc1
se	se	k3xPyFc4
zaručila	zaručit	k5eAaPmAgFnS
za	za	k7c4
bezpečnost	bezpečnost	k1gFnSc4
Francie	Francie	k1gFnSc2
po	po	k7c4
dobu	doba	k1gFnSc4
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
vzhledem	vzhledem	k7c3
k	k	k7c3
vnitřním	vnitřní	k2eAgFnPc3d1
komplikacím	komplikace	k1gFnPc3
v	v	k7c6
počátečním	počáteční	k2eAgNnSc6d1
období	období	k1gNnSc6
Ludvíkovy	Ludvíkův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
posílení	posílení	k1gNnSc4
ústřední	ústřední	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
prestiže	prestiž	k1gFnSc2
trůnu	trůn	k1gInSc2
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
potvrzením	potvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
spojenectví	spojenectví	k1gNnSc4
mezi	mezi	k7c7
pařížským	pařížský	k2eAgInSc7d1
dvorem	dvůr	k1gInSc7
a	a	k8xC
papežskou	papežský	k2eAgFnSc7d1
kurií	kurie	k1gFnSc7
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Konrádova	Konrádův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
procházela	procházet	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
vnitřní	vnitřní	k2eAgMnSc1d1
i	i	k8xC
vnější	vnější	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Rogerově	Rogerův	k2eAgFnSc3d1
hrozbě	hrozba	k1gFnSc3
říšským	říšský	k2eAgInPc3d1
zájmům	zájem	k1gInPc3
v	v	k7c6
Itálii	Itálie	k1gFnSc6
se	se	k3xPyFc4
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
byzantskou	byzantský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
Roger	Roger	k1gInSc1
Sicilský	sicilský	k2eAgInSc1d1
zpočátku	zpočátku	k6eAd1
přislíbil	přislíbit	k5eAaPmAgInS
svoji	svůj	k3xOyFgFnSc4
účast	účast	k1gFnSc4
na	na	k7c6
připravované	připravovaný	k2eAgFnSc6d1
kruciátě	kruciáta	k1gFnSc6
<g/>
,	,	kIx,
ztratil	ztratit	k5eAaPmAgInS
brzy	brzy	k6eAd1
zájem	zájem	k1gInSc1
a	a	k8xC
roku	rok	k1gInSc2
1146	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
dobytím	dobytí	k1gNnSc7
ostrova	ostrov	k1gInSc2
Korfu	Korfu	k1gNnSc2
expanzi	expanze	k1gFnSc3
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
vyplenil	vyplenit	k5eAaPmAgInS
ostrov	ostrov	k1gInSc1
Kefalonii	Kefalonie	k1gFnSc4
a	a	k8xC
pronikl	proniknout	k5eAaPmAgInS
až	až	k9
ke	k	k7c3
Korintu	Korint	k1gInSc3
<g/>
,	,	kIx,
Thébám	Théby	k1gFnPc3
a	a	k8xC
na	na	k7c6
Euboiu	Euboium	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
poškodil	poškodit	k5eAaPmAgInS
tak	tak	k9
Byzanc	Byzanc	k1gFnSc4
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
její	její	k3xOp3gNnPc1
vojska	vojsko	k1gNnPc1
soustřeďovala	soustřeďovat	k5eAaImAgNnP
na	na	k7c6
východě	východ	k1gInSc6
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
říši	říš	k1gFnSc6
římské	římský	k2eAgFnSc6d1
byl	být	k5eAaImAgInS
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1147	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Konrádův	Konrádův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
regentem	regens	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
arcibiskup	arcibiskup	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Mohuče	Mohuč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgMnPc7d1
propagátory	propagátor	k1gMnPc7
výpravy	výprava	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Německu	Německo	k1gNnSc6
stali	stát	k5eAaPmAgMnP
opat	opat	k1gMnSc1
Adam	Adam	k1gMnSc1
z	z	k7c2
Ebrachu	Ebrach	k1gInSc2
a	a	k8xC
biskup	biskup	k1gMnSc1
Ota	Ota	k1gMnSc1
z	z	k7c2
Freisingu	Freising	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
plánoval	plánovat	k5eAaImAgMnS
vydat	vydat	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
cestu	cesta	k1gFnSc4
již	již	k6eAd1
o	o	k7c6
Velikonocích	Velikonoce	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nakonec	nakonec	k6eAd1
vyrazila	vyrazit	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
květnu	květen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konrád	Konrád	k1gMnSc1
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
<g/>
,	,	kIx,
doprovázení	doprovázení	k1gNnSc1
papežským	papežský	k2eAgInSc7d1
legátem	legát	k1gInSc7
<g/>
,	,	kIx,
kardinálem	kardinál	k1gMnSc7
Theodwinem	Theodwin	k1gMnSc7
<g/>
,	,	kIx,
vyrazili	vyrazit	k5eAaPmAgMnP
z	z	k7c2
Řezna	Řezno	k1gNnSc2
v	v	k7c6
květnu	květen	k1gInSc6
1147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrádovo	Konrádův	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
čítalo	čítat	k5eAaImAgNnS
na	na	k7c4
20	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
tvořili	tvořit	k5eAaImAgMnP
ho	on	k3xPp3gInSc4
velmoži	velmož	k1gMnPc1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
Češi	Čech	k1gMnPc1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
knížetem	kníže	k1gMnSc7
Vladislavem	Vladislav	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
frankofonní	frankofonní	k2eAgMnPc1d1
Lotrinčané	Lotrinčan	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
se	se	k3xPyFc4
ke	k	k7c3
Konrádovi	Konrád	k1gMnSc3
připojil	připojit	k5eAaPmAgMnS
i	i	k8xC
štýrský	štýrský	k2eAgMnSc1d1
markrabě	markrabě	k1gMnSc1
Otakar	Otakar	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgMnS
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Gejza	Gejz	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrádovým	Konrádův	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
<g/>
,	,	kIx,
výpravu	výprava	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
klidu	klid	k1gInSc6
projít	projít	k5eAaPmF
zemí	zem	k1gFnSc7
a	a	k8xC
vydatně	vydatně	k6eAd1
ji	on	k3xPp3gFnSc4
podporoval	podporovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
tentokrát	tentokrát	k6eAd1
křižáci	křižák	k1gMnPc1
nepřicházeli	přicházet	k5eNaImAgMnP
na	na	k7c4
pozvání	pozvání	k1gNnSc4
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
setkalo	setkat	k5eAaPmAgNnS
se	se	k3xPyFc4
ještě	ještě	k9
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
s	s	k7c7
Konrádem	Konrád	k1gMnSc7
byzantské	byzantský	k2eAgFnSc2d1
poselstvo	poselstvo	k1gNnSc1
s	s	k7c7
dotazy	dotaz	k1gInPc7
<g/>
,	,	kIx,
zda	zda	k8xS
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
přichází	přicházet	k5eAaImIp3nS
v	v	k7c6
míru	mír	k1gInSc6
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Byzantinci	Byzantinec	k1gMnPc1
krále	král	k1gMnSc2
Konráda	Konrád	k1gMnSc2
přiměli	přimět	k5eAaPmAgMnP
k	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
přísaze	přísaha	k1gFnSc6
loajality	loajalita	k1gFnSc2
vůči	vůči	k7c3
císaři	císař	k1gMnSc3
Manuelovi	Manuel	k1gMnSc3
I.	I.	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
Konrád	Konrád	k1gMnSc1
se	se	k3xPyFc4
odmítl	odmítnout	k5eAaPmAgMnS
zavázat	zavázat	k5eAaPmF
navrátit	navrátit	k5eAaPmF
všechna	všechen	k3xTgNnPc4
potenciálně	potenciálně	k6eAd1
dobytá	dobytý	k2eAgNnPc4d1
města	město	k1gNnPc4
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
Byzanci	Byzanc	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
sám	sám	k3xTgMnSc1
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
na	na	k7c6
válečném	válečný	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
uzavřel	uzavřít	k5eAaPmAgInS
příměří	příměří	k1gNnSc4
s	s	k7c7
ikonyjským	ikonyjský	k2eAgMnSc7d1
sultánem	sultán	k1gMnSc7
a	a	k8xC
s	s	k7c7
vojskem	vojsko	k1gNnSc7
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
křižáky	křižák	k1gMnPc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
větší	veliký	k2eAgFnSc4d2
hrozbu	hrozba	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
Turky	Turek	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Byzantinci	Byzantinec	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
naproti	naproti	k6eAd1
křižákům	křižák	k1gMnPc3
menší	malý	k2eAgInPc4d2
pořádkové	pořádkový	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
křižáky	křižák	k1gInPc1
měly	mít	k5eAaImAgInP
doprovázet	doprovázet	k5eAaImF
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Tak	tak	k6eAd1
šířili	šířit	k5eAaImAgMnP
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
pochodovali	pochodovat	k5eAaImAgMnP
před	před	k7c7
námi	my	k3xPp1nPc7
<g/>
,	,	kIx,
všude	všude	k6eAd1
takový	takový	k3xDgInSc4
zmatek	zmatek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Řekové	Řek	k1gMnPc1
utíkali	utíkat	k5eAaImAgMnP
před	před	k7c7
naším	náš	k3xOp1gNnSc7
vojskem	vojsko	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
táhlo	táhnout	k5eAaImAgNnS
později	pozdě	k6eAd2
<g/>
,	,	kIx,
jakkoli	jakkoli	k8xS
bylo	být	k5eAaImAgNnS
mírumilovné	mírumilovný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Odo	Odo	k1gMnSc1
z	z	k7c2
Deuil	Deuil	k1gMnSc1
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Manuel	Manuel	k1gMnSc1
I.	I.	kA
<g/>
(	(	kIx(
<g/>
Kodex	kodex	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1178	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
špatně	špatně	k6eAd1
ukázněná	ukázněný	k2eAgFnSc1d1
a	a	k8xC
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
častým	častý	k2eAgFnPc3d1
bitkám	bitka	k1gFnPc3
mezi	mezi	k7c7
Němci	Němec	k1gMnPc7
a	a	k8xC
Byzantinci	Byzantinec	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
napadli	napadnout	k5eAaPmAgMnP
Filipopolis	Filipopolis	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
vydrancovali	vydrancovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
pár	pár	k4xCyI
dnů	den	k1gInPc2
později	pozdě	k6eAd2
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
kláštery	klášter	k1gInPc4
u	u	k7c2
Adrianopole	Adrianopole	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zmasakrovali	zmasakrovat	k5eAaPmAgMnP
všechny	všechen	k3xTgInPc4
mnichy	mnich	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
Adrianopole	Adrianopole	k1gFnSc2
podnikl	podniknout	k5eAaPmAgMnS
byzantský	byzantský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Prosuch	Prosucha	k1gFnPc2
protiútok	protiútok	k1gInSc4
a	a	k8xC
strhla	strhnout	k5eAaPmAgFnS
se	se	k3xPyFc4
bitva	bitva	k1gFnSc1
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
Konrádovým	Konrádův	k2eAgMnSc7d1
synovcem	synovec	k1gMnSc7
Fridrichem	Fridrich	k1gMnSc7
Barbarossou	Barbarossa	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
Konrádem	Konrád	k1gMnSc7
a	a	k8xC
Manuelem	Manuel	k1gMnSc7
byly	být	k5eAaImAgFnP
napjaté	napjatý	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
císař	císař	k1gMnSc1
Konráda	Konrád	k1gMnSc2
požádal	požádat	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
i	i	k9
s	s	k7c7
armádou	armáda	k1gFnSc7
rychle	rychle	k6eAd1
přeplavil	přeplavit	k5eAaPmAgMnS
přes	přes	k7c4
Helespont	Helespont	k1gInSc4
do	do	k7c2
Anatolie	Anatolie	k1gFnSc2
a	a	k8xC
ke	k	k7c3
Konstantinopoli	Konstantinopol	k1gInSc3
se	se	k3xPyFc4
vůbec	vůbec	k9
nepřibližoval	přibližovat	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
září	zářit	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
armáda	armáda	k1gFnSc1
dorazila	dorazit	k5eAaPmAgFnS
k	k	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
městu	město	k1gNnSc3
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
Konrád	Konrád	k1gMnSc1
ubytován	ubytován	k2eAgMnSc1d1
v	v	k7c6
předměstském	předměstský	k2eAgInSc6d1
paláci	palác	k1gInSc6
Filopatheion	Filopatheion	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Němci	Němec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
v	v	k7c6
okolí	okolí	k1gNnSc6
Konstantinopole	Konstantinopol	k1gInSc2
rabovat	rabovat	k5eAaImF
a	a	k8xC
když	když	k8xS
Manuel	Manuel	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Konrád	Konrád	k1gMnSc1
chování	chování	k1gNnSc4
svých	svůj	k3xOyFgMnPc2
vojáků	voják	k1gMnPc2
zarazil	zarazit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
arogantně	arogantně	k6eAd1
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
příští	příští	k2eAgInSc4d1
rok	rok	k1gInSc4
přitáhne	přitáhnout	k5eAaPmIp3nS
znovu	znovu	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Konstantinopol	Konstantinopol	k1gInSc1
dobyl	dobýt	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Král	Král	k1gMnSc1
se	se	k3xPyFc4
vynořil	vynořit	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
kometa	kometa	k1gFnSc1
ohlašující	ohlašující	k2eAgNnSc1d1
neštěstí	neštěstí	k1gNnSc1
a	a	k8xC
k	k	k7c3
velké	velký	k2eAgFnSc3d1
úlevě	úleva	k1gFnSc3
Romájů	Romáj	k1gMnPc2
zase	zase	k9
zmizel	zmizet	k5eAaPmAgMnS
na	na	k7c4
východ	východ	k1gInSc4
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Niketas	Niketas	k1gMnSc1
Choniates	Choniates	k1gMnSc1
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spor	spor	k1gInSc1
se	se	k3xPyFc4
však	však	k9
díky	díky	k7c3
císařovně	císařovna	k1gFnSc3
Bertě	Berta	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
Konrádovou	Konrádův	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
<g/>
,	,	kIx,
podařilo	podařit	k5eAaPmAgNnS
urovnat	urovnat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Manuel	Manuel	k1gMnSc1
pak	pak	k8xC
Konráda	Konrád	k1gMnSc2
požádal	požádat	k5eAaPmAgMnS
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
mu	on	k3xPp3gMnSc3
půjčil	půjčit	k5eAaPmAgMnS
část	část	k1gFnSc4
své	svůj	k3xOyFgFnSc2
armády	armáda	k1gFnSc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
sicilským	sicilský	k2eAgMnPc3d1
Normanům	Norman	k1gMnPc3
Rogera	Rogero	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
drancovali	drancovat	k5eAaImAgMnP
řecká	řecký	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
Manuelovu	Manuelův	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
ještě	ještě	k9
většího	veliký	k2eAgNnSc2d2
nepřátelství	nepřátelství	k1gNnSc2
sicilského	sicilský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Navíc	navíc	k6eAd1
měl	mít	k5eAaImAgMnS
Konrád	Konrád	k1gMnSc1
potíže	potíž	k1gFnSc2
se	s	k7c7
svými	svůj	k3xOyFgInPc7
lotrinskými	lotrinský	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lotrinčané	Lotrinčan	k1gMnPc1
se	se	k3xPyFc4
chtěli	chtít	k5eAaImAgMnP
připojit	připojit	k5eAaPmF
k	k	k7c3
vojsku	vojsko	k1gNnSc3
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
přibližovalo	přibližovat	k5eAaImAgNnS
ke	k	k7c3
Konstantinopoli	Konstantinopol	k1gInSc3
a	a	k8xC
naopak	naopak	k6eAd1
dříve	dříve	k6eAd2
dorazivší	dorazivší	k2eAgInPc1d1
boje	boj	k1gInPc1
nedočkaví	dočkavý	k2eNgMnPc1d1,k2eAgMnPc1d1
francouzští	francouzský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
se	se	k3xPyFc4
chtěli	chtít	k5eAaImAgMnP
připojit	připojit	k5eAaPmF
ke	k	k7c3
Konrádovým	Konrádův	k2eAgMnPc3d1
mužům	muž	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Nakonec	nakonec	k6eAd1
německý	německý	k2eAgMnSc1d1
král	král	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
přesunem	přesun	k1gInSc7
vojska	vojsko	k1gNnSc2
do	do	k7c2
Anatolie	Anatolie	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Lotrinčané	Lotrinčan	k1gMnPc1
zůstali	zůstat	k5eAaPmAgMnP
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
počkali	počkat	k5eAaPmAgMnP
na	na	k7c4
Francouze	Francouz	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gustave	Gustav	k1gMnSc5
Doré	Dorý	k2eAgFnSc3d1
<g/>
:	:	kIx,
Konrádova	Konrádův	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
přepadena	přepaden	k2eAgFnSc1d1
u	u	k7c2
Dorylaia	Dorylaium	k1gNnSc2
</s>
<s>
Manuel	Manuel	k1gMnSc1
Konrádovi	Konrád	k1gMnSc3
radil	radit	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
vydal	vydat	k5eAaPmAgMnS
cestou	cesta	k1gFnSc7
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
sice	sice	k8xC
delší	dlouhý	k2eAgFnSc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
byzantského	byzantský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Nikaie	Nikaie	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
táhnout	táhnout	k5eAaImF
na	na	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
rúmských	rúmský	k2eAgInPc2d1
Seldžuků	Seldžuk	k1gInPc2
Iconium	Iconium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
října	říjen	k1gInSc2
1147	#num#	k4
vyrazil	vyrazit	k5eAaPmAgMnS
z	z	k7c2
Nikaie	Nikaie	k1gFnSc2
a	a	k8xC
své	svůj	k3xOyFgNnSc4
vojsko	vojsko	k1gNnSc4
rozdělil	rozdělit	k5eAaPmAgMnS
do	do	k7c2
dvou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
<g/>
:	:	kIx,
první	první	k4xOgFnSc1
<g/>
,	,	kIx,
složená	složený	k2eAgFnSc1d1
převážně	převážně	k6eAd1
z	z	k7c2
civilistů	civilista	k1gMnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
Oty	Ota	k1gMnSc2
z	z	k7c2
Freisingu	Freising	k1gInSc2
táhla	táhlo	k1gNnSc2
podél	podél	k7c2
egejského	egejský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
král	král	k1gMnSc1
s	s	k7c7
rytíři	rytíř	k1gMnPc7
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
deset	deset	k4xCc4
dnů	den	k1gInPc2
postupoval	postupovat	k5eAaImAgInS
byzantským	byzantský	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc4
nepřipravená	připravený	k2eNgFnSc1d1
armáda	armáda	k1gFnSc1
u	u	k7c2
Dorylaia	Dorylaium	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
první	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
rozdrtila	rozdrtit	k5eAaPmAgFnS
Kilič	Kilič	k1gInSc4
Arslanovy	Arslanův	k2eAgMnPc4d1
Turky	Turek	k1gMnPc4
<g/>
,	,	kIx,
přepadena	přepadnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
se	se	k3xPyFc4
vrhli	vrhnout	k5eAaImAgMnP,k5eAaPmAgMnP
na	na	k7c4
roztroušené	roztroušený	k2eAgMnPc4d1
křižáky	křižák	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
hrnuli	hrnout	k5eAaImAgMnP
po	po	k7c6
celodenním	celodenní	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
k	k	k7c3
řece	řeka	k1gFnSc3
napít	napít	k5eAaBmF,k5eAaPmF
<g/>
,	,	kIx,
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
typickou	typický	k2eAgFnSc7d1
nájezdnickou	nájezdnický	k2eAgFnSc7d1
taktikou	taktika	k1gFnSc7
útok-ústup	útok-ústup	k1gInSc1
a	a	k8xC
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
křesťanskou	křesťanský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
zničili	zničit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
se	se	k3xPyFc4
asi	asi	k9
s	s	k7c7
desetinou	desetina	k1gFnSc7
vojska	vojsko	k1gNnSc2
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
probil	probít	k5eAaPmAgInS
z	z	k7c2
pasti	past	k1gFnSc2
a	a	k8xC
o	o	k7c4
několik	několik	k4yIc4
dnů	den	k1gInPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Nikaie	Nikaie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cesta	k1gFnSc7
byl	být	k5eAaImAgInS
zadní	zadní	k2eAgInSc1d1
voj	voj	k1gInSc1
neustále	neustále	k6eAd1
napadán	napadat	k5eAaPmNgInS,k5eAaBmNgInS
tureckými	turecký	k2eAgInPc7d1
jezdci	jezdec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Konrádovy	Konrádův	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
zbyl	zbýt	k5eAaPmAgInS
jen	jen	k9
sbor	sbor	k1gInSc1
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
prakticky	prakticky	k6eAd1
celá	celý	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
u	u	k7c2
Dorylaia	Dorylaium	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
pobita	pobit	k2eAgFnSc1d1
a	a	k8xC
přeživší	přeživší	k2eAgFnSc1d1
odvlečeni	odvléct	k5eAaPmNgMnP
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ota	Ota	k1gMnSc1
z	z	k7c2
Freisingu	Freising	k1gInSc2
s	s	k7c7
vojenským	vojenský	k2eAgInSc7d1
doprovodem	doprovod	k1gInSc7
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
byzantské	byzantský	k2eAgFnSc3d1
pevnosti	pevnost	k1gFnSc3
Attalia	Attalium	k1gNnSc2
<g/>
,	,	kIx,
podél	podél	k7c2
byzantsko-tureckých	byzantsko-turecký	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
Laodikeje	Laodikej	k1gInSc2
byla	být	k5eAaImAgFnS
výprava	výprava	k1gFnSc1
napadena	napadnout	k5eAaPmNgFnS
a	a	k8xC
částečně	částečně	k6eAd1
pozabíjena	pozabíjen	k2eAgFnSc1d1
Turky	turek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
zde	zde	k6eAd1
i	i	k9
hrabě	hrabě	k1gMnSc1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Trixenu	Trixen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Přeživší	přeživší	k2eAgFnSc2d1
se	se	k3xPyFc4
poté	poté	k6eAd1
dali	dát	k5eAaPmAgMnP
na	na	k7c4
ústup	ústup	k1gInSc4
k	k	k7c3
jižnímu	jižní	k2eAgNnSc3d1
pobřeží	pobřeží	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
mnoho	mnoho	k4c1
jich	on	k3xPp3gMnPc2
cestou	cesta	k1gFnSc7
zemřelo	zemřít	k5eAaPmAgNnS
hladem	hlad	k1gInSc7
a	a	k8xC
vyčerpáním	vyčerpání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1148	#num#	k4
byla	být	k5eAaImAgFnS
výprava	výprava	k1gFnSc1
napadena	napadnout	k5eAaPmNgFnS
znovu	znovu	k6eAd1
a	a	k8xC
Turci	Turek	k1gMnPc1
jí	on	k3xPp3gFnSc2
způsobili	způsobit	k5eAaPmAgMnP
další	další	k2eAgFnPc4d1
těžké	těžký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
biskup	biskup	k1gMnSc1
byl	být	k5eAaImAgMnS
s	s	k7c7
malou	malý	k2eAgFnSc7d1
družinou	družina	k1gFnSc7
oddělen	oddělit	k5eAaPmNgInS
od	od	k7c2
hlavní	hlavní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
až	až	k9
do	do	k7c2
Attaleie	Attaleie	k1gFnSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
moři	moře	k1gNnSc6
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Mauzaisse	Mauzaiss	k1gMnSc5
<g/>
:	:	kIx,
Král	Král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
přijímá	přijímat	k5eAaImIp3nS
Oriflamme	Oriflamme	k1gFnSc4
–	–	k?
královskou	královský	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
standartu	standarta	k1gFnSc4
před	před	k7c7
tažením	tažení	k1gNnSc7
na	na	k7c4
východ	východ	k1gInSc4
</s>
<s>
Shromaždištěm	shromaždiště	k1gNnSc7
francouzské	francouzský	k2eAgFnSc2d1
křižácké	křižácký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
byly	být	k5eAaImAgFnP
Mety	Mety	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
menší	malý	k2eAgNnSc1d2
než	než	k8xS
německé	německý	k2eAgNnSc1d1
<g/>
,	,	kIx,
asi	asi	k9
patnáctitisícové	patnáctitisícový	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
I	i	k8xC
přesto	přesto	k8xC
byla	být	k5eAaImAgFnS
Ludvíkova	Ludvíkův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
velkolepá	velkolepý	k2eAgFnSc1d1
<g/>
,	,	kIx,
kromě	kromě	k7c2
samotného	samotný	k2eAgMnSc2d1
krále	král	k1gMnSc2
byla	být	k5eAaImAgFnS
přítomna	přítomen	k2eAgFnSc1d1
i	i	k8xC
královna	královna	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
Akvitánská	Akvitánský	k2eAgFnSc1d1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
vazaly	vazal	k1gMnPc7
a	a	k8xC
dvorními	dvorní	k2eAgFnPc7d1
dámami	dáma	k1gFnPc7
a	a	k8xC
mnoho	mnoho	k4c1
francouzských	francouzský	k2eAgMnPc2d1
feudálů	feudál	k1gMnPc2
z	z	k7c2
Lotrinska	Lotrinsko	k1gNnSc2
<g/>
,	,	kIx,
Bretaně	Bretaň	k1gFnSc2
<g/>
,	,	kIx,
Burgundska	Burgundsko	k1gNnSc2
a	a	k8xC
Akvitánie	Akvitánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddíly	oddíl	k1gInPc7
z	z	k7c2
Provence	Provence	k1gFnSc2
vedl	vést	k5eAaImAgMnS
hrabě	hrabě	k1gMnSc1
Alfons	Alfons	k1gMnSc1
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
však	však	k9
počkal	počkat	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
srpna	srpen	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
králi	král	k1gMnSc3
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
také	také	k9
velmistr	velmistr	k1gMnSc1
řádu	řád	k1gInSc2
templářů	templář	k1gMnPc2
Everard	Everarda	k1gFnPc2
des	des	k1gNnSc1
Barres	Barres	k1gMnSc1
se	se	k3xPyFc4
sborem	sborem	k6eAd1
rekrutů	rekrut	k1gMnPc2
určených	určený	k2eAgFnPc2d1
pro	pro	k7c4
Svatou	svatý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Francouzští	francouzský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
stejnou	stejný	k2eAgFnSc7d1
trasou	trasa	k1gFnSc7
jako	jako	k8xS,k8xC
Němci	Němec	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Wormsu	Worms	k1gInSc6
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gFnPc3
připojily	připojit	k5eAaPmAgInP
oddíly	oddíl	k1gInPc1
normanských	normanský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
z	z	k7c2
Anglie	Anglie	k1gFnSc2
a	a	k8xC
Normandie	Normandie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
byli	být	k5eAaImAgMnP
mnohem	mnohem	k6eAd1
ukázněnější	ukázněný	k2eAgMnPc1d2
<g/>
,	,	kIx,
ke	k	k7c3
králi	král	k1gMnSc3
vzhlíželi	vzhlížet	k5eAaImAgMnP
s	s	k7c7
obdivem	obdiv	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Svůj	svůj	k3xOyFgInSc4
podíl	podíl	k1gInSc4
na	na	k7c6
atmosféře	atmosféra	k1gFnSc6
provázející	provázející	k2eAgNnSc4d1
tažení	tažení	k1gNnSc4
měl	mít	k5eAaImAgInS
i	i	k9
v	v	k7c6
Metách	Mety	k1gFnPc6
Ludvíkem	Ludvík	k1gMnSc7
vyhlášený	vyhlášený	k2eAgInSc1d1
táborový	táborový	k2eAgInSc4d1
řád	řád	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgMnSc4
museli	muset	k5eAaImAgMnP
přísahat	přísahat	k5eAaImF
všichni	všechen	k3xTgMnPc1
vůdci	vůdce	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
cesta	cesta	k1gFnSc1
byla	být	k5eAaImAgFnS
klidnější	klidný	k2eAgFnSc1d2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
uherská	uherský	k2eAgFnSc1d1
země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
válečném	válečný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
králem	král	k1gMnSc7
Gejzou	Gejza	k1gFnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Borisem	Boris	k1gMnSc7
<g/>
,	,	kIx,
synem	syn	k1gMnSc7
zemřelého	zemřelý	k1gMnSc2
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Kolomana	Koloman	k1gMnSc2
vznikla	vzniknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
o	o	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poražený	poražený	k1gMnSc1
Boris	Boris	k1gMnSc1
prchl	prchnout	k5eAaPmAgMnS
k	k	k7c3
Ludvíkovi	Ludvík	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mu	on	k3xPp3gMnSc3
poskytl	poskytnout	k5eAaPmAgMnS
azyl	azyl	k1gInSc4
a	a	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
vydat	vydat	k5eAaPmF
králi	král	k1gMnSc3
Gejzovi	Gejz	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
však	však	k9
přislíbil	přislíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nechá	nechat	k5eAaPmIp3nS
Borise	Boris	k1gMnSc2
uvěznit	uvěznit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
táhl	táhnout	k5eAaImAgMnS
územím	území	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
předtím	předtím	k6eAd1
vyplenili	vyplenit	k5eAaPmAgMnP
Konrádovi	Konrádův	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
tak	tak	k6eAd1
měli	mít	k5eAaImAgMnP
nedostatek	nedostatek	k1gInSc4
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
nezačali	začít	k5eNaPmAgMnP
drancovat	drancovat	k5eAaImF
a	a	k8xC
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
byl	být	k5eAaImAgMnS
Ludvík	Ludvík	k1gMnSc1
přijat	přijat	k2eAgMnSc1d1
přátelsky	přátelsky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Francouzi	Francouz	k1gMnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
setkali	setkat	k5eAaPmAgMnP
s	s	k7c7
německou	německý	k2eAgFnSc7d1
částí	část	k1gFnSc7
kruciáty	kruciáta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
na	na	k7c6
příchozí	příchozí	k1gFnSc6
pohlíželi	pohlížet	k5eAaImAgMnP
s	s	k7c7
nevraživostí	nevraživost	k1gFnSc7
a	a	k8xC
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
Lotrinčané	Lotrinčan	k1gMnPc1
vypověděli	vypovědět	k5eAaPmAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
špatně	špatně	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
Němci	Němec	k1gMnPc7
nakládali	nakládat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
francouzským	francouzský	k2eAgNnSc7d1
a	a	k8xC
německým	německý	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
se	se	k3xPyFc4
rychle	rychle	k6eAd1
zhoršily	zhoršit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Ludvíkem	Ludvík	k1gMnSc7
se	se	k3xPyFc4
Manuelovi	Manuel	k1gMnSc3
vyjednávalo	vyjednávat	k5eAaImAgNnS
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
s	s	k7c7
Konrádem	Konrád	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
i	i	k9
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
odmítl	odmítnout	k5eAaPmAgMnS
zavázat	zavázat	k5eAaPmF
vydání	vydání	k1gNnSc4
dobytých	dobytý	k2eAgNnPc2d1
anatolských	anatolský	k2eAgNnPc2d1
území	území	k1gNnPc2
Manuelovi	Manuel	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
francouzští	francouzský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
byli	být	k5eAaImAgMnP
navíc	navíc	k6eAd1
pobouřeni	pobouřit	k5eAaPmNgMnP
<g/>
,	,	kIx,
že	že	k8xS
císař	císař	k1gMnSc1
uzavřel	uzavřít	k5eAaPmAgMnS
příměří	příměří	k1gNnSc4
se	s	k7c7
sultánem	sultán	k1gMnSc7
Mesudem	Mesud	k1gInSc7
I.	I.	kA
a	a	k8xC
volali	volat	k5eAaImAgMnP
po	po	k7c6
spojenectví	spojenectví	k1gNnSc6
s	s	k7c7
Rogerem	Rogero	k1gNnSc7
Sicilským	sicilský	k2eAgNnSc7d1
a	a	k8xC
útoku	útok	k1gInSc6
na	na	k7c4
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Ludvíkovi	Ludvík	k1gMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
je	on	k3xPp3gFnPc4
zklidnit	zklidnit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Král	Král	k1gMnSc1
dal	dát	k5eAaPmAgMnS
často	často	k6eAd1
mnohým	mnohé	k1gNnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
uříznout	uříznout	k5eAaPmF
uši	ucho	k1gNnPc4
<g/>
,	,	kIx,
ruce	ruka	k1gFnPc4
a	a	k8xC
nohy	noha	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
neschopen	schopit	k5eNaPmNgMnS
krotit	krotit	k5eAaImF
jejich	jejich	k3xOp3gNnSc4
bláznovství	bláznovství	k1gNnSc4
na	na	k7c6
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Odo	Odo	k1gMnSc1
z	z	k7c2
Deuil	Deuil	k1gMnSc1
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
příjezdu	příjezd	k1gInSc2
křižáků	křižák	k1gMnPc2
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
k	k	k7c3
Ludvíkovi	Ludvík	k1gMnSc3
připojily	připojit	k5eAaPmAgInP
i	i	k9
armády	armáda	k1gFnSc2
z	z	k7c2
Auvergne	Auvergn	k1gInSc5
<g/>
,	,	kIx,
Savojska	Savojsko	k1gNnSc2
a	a	k8xC
Montferratu	Montferrat	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
pěst	pěst	k1gFnSc4
prošly	projít	k5eAaPmAgFnP
Itálií	Itálie	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
Brindisi	Brindisi	k1gNnSc6
se	se	k3xPyFc4
nalodily	nalodit	k5eAaPmAgFnP
a	a	k8xC
nechaly	nechat	k5eAaPmAgFnP
se	se	k3xPyFc4
přeplavit	přeplavit	k5eAaPmF
do	do	k7c2
Dyrrhachionu	Dyrrhachion	k1gInSc2
na	na	k7c6
balkánském	balkánský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
křižácká	křižácký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
byzantských	byzantský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
přeplavena	přeplaven	k2eAgFnSc1d1
přes	přes	k7c4
Bospor	Bospor	k1gInSc4
do	do	k7c2
Anatolie	Anatolie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
poté	poté	k6eAd1
Manuel	Manuel	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
přerušit	přerušit	k5eAaPmF
zásobování	zásobování	k1gNnSc4
Francouzů	Francouz	k1gMnPc2
a	a	k8xC
když	když	k8xS
tak	tak	k6eAd1
nakonec	nakonec	k6eAd1
donutil	donutit	k5eAaPmAgMnS
Ludvíka	Ludvík	k1gMnSc4
ke	k	k7c3
složení	složení	k1gNnSc3
požadované	požadovaný	k2eAgFnSc2d1
přísahy	přísaha	k1gFnSc2
<g/>
,	,	kIx,
zásobování	zásobování	k1gNnSc2
obnovil	obnovit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Nikaii	Nikaie	k1gFnSc6
Ludvíka	Ludvík	k1gMnSc4
očekávalo	očekávat	k5eAaImAgNnS
německé	německý	k2eAgNnSc1d1
poselstvo	poselstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
žádalo	žádat	k5eAaImAgNnS
o	o	k7c6
setkání	setkání	k1gNnSc6
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
s	s	k7c7
německým	německý	k2eAgMnSc7d1
králem	král	k1gMnSc7
Konrádem	Konrád	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
svolil	svolit	k5eAaPmAgMnS
a	a	k8xC
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Konrádova	Konrádův	k2eAgNnSc2d1
ležení	ležení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
střežené	střežený	k2eAgInPc4d1
byzantskými	byzantský	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
ztratili	ztratit	k5eAaPmAgMnP
všechny	všechen	k3xTgFnPc4
své	svůj	k3xOyFgFnPc4
zásoby	zásoba	k1gFnPc4
a	a	k8xC
zavazadla	zavazadlo	k1gNnPc4
<g/>
,	,	kIx,
nevydrancovali	vydrancovat	k5eNaPmAgMnP
široké	široký	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
pod	pod	k7c7
podmínkou	podmínka	k1gFnSc7
cesty	cesta	k1gFnSc2
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
vyslyšel	vyslyšet	k5eAaPmAgMnS
Konrádovu	Konrádův	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
o	o	k7c6
spojení	spojení	k1gNnSc6
zbytků	zbytek	k1gInPc2
německého	německý	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
s	s	k7c7
francouzským	francouzský	k2eAgMnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
Francouzi	Francouz	k1gMnPc1
lépe	dobře	k6eAd2
bojují	bojovat	k5eAaImIp3nP
koňmo	koňmo	k6eAd1
v	v	k7c6
semknutých	semknutý	k2eAgFnPc6d1
řadách	řada	k1gFnPc6
<g/>
,	,	kIx,
dovedněji	dovedně	k6eAd2
útočí	útočit	k5eAaImIp3nS
kopími	kopí	k1gNnPc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
jízda	jízda	k1gFnSc1
předčí	předčit	k5eAaBmIp3nS,k5eAaPmIp3nS
německou	německý	k2eAgFnSc7d1
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
lepší	dobrý	k2eAgMnSc1d2
v	v	k7c6
boji	boj	k1gInSc6
pěšky	pěšky	k6eAd1
a	a	k8xC
skvěle	skvěle	k6eAd1
zacházejí	zacházet	k5eAaImIp3nP
se	s	k7c7
svými	svůj	k3xOyFgInPc7
pádnými	pádný	k2eAgInPc7d1
meči	meč	k1gInPc7
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Jan	Jan	k1gMnSc1
Kinnamos	Kinnamos	k1gMnSc1
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
Anatolií	Anatolie	k1gFnPc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Byzantinci	Byzantinec	k1gMnPc1
nemohli	moct	k5eNaImAgMnP
křižákům	křižák	k1gMnPc3
poskytnout	poskytnout	k5eAaPmF
žádné	žádný	k3yNgFnPc4
doprovodné	doprovodný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
říšské	říšský	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
byly	být	k5eAaImAgFnP
vázány	vázat	k5eAaImNgFnP
obrannou	obranný	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
proti	proti	k7c3
sicilským	sicilský	k2eAgMnPc3d1
Normanům	Norman	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
vstup	vstup	k1gInSc4
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
do	do	k7c2
Asie	Asie	k1gFnSc2
lišil	lišit	k5eAaImAgMnS
od	od	k7c2
té	ten	k3xDgFnSc2
první	první	k4xOgFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
jako	jako	k9
první	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
tentokrát	tentokrát	k6eAd1
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
nakonec	nakonec	k6eAd1
zavázali	zavázat	k5eAaPmAgMnP
navrátit	navrátit	k5eAaPmF
všechna	všechen	k3xTgNnPc1
dobytá	dobytý	k2eAgNnPc1d1
anatolská	anatolský	k2eAgNnPc1d1
města	město	k1gNnPc1
Byzantské	byzantský	k2eAgFnPc1d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
si	se	k3xPyFc3
zvolil	zvolit	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
územím	území	k1gNnSc7
jež	jenž	k3xRgNnSc1
sice	sice	k8xC
patřilo	patřit	k5eAaImAgNnS
Byzanci	Byzanc	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
východní	východní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
říše	říše	k1gFnSc1
byly	být	k5eAaImAgFnP
velmi	velmi	k6eAd1
neklidné	klidný	k2eNgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
pravděpodobnost	pravděpodobnost	k1gFnSc1
tureckého	turecký	k2eAgNnSc2d1
přepadení	přepadení	k1gNnSc2
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
došli	dojít	k5eAaPmAgMnP
do	do	k7c2
Efesu	Efes	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
Konrád	Konrád	k1gMnSc1
onemocněl	onemocnět	k5eAaPmAgMnS
a	a	k8xC
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
vrátit	vrátit	k5eAaPmF
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vlídně	vlídně	k6eAd1
přijat	přijmout	k5eAaPmNgMnS
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
poté	poté	k6eAd1
ignoroval	ignorovat	k5eAaImAgMnS
všechny	všechen	k3xTgFnPc4
Manuelovy	Manuelův	k2eAgFnPc4d1
rady	rada	k1gFnPc4
a	a	k8xC
stočil	stočit	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Skupina	skupina	k1gFnSc1
Turků	Turek	k1gMnPc2
křižáky	křižák	k1gMnPc4
přepadla	přepadnout	k5eAaPmAgFnS
nedaleko	nedaleko	k7c2
Efesu	Efes	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
malé	malý	k2eAgFnSc6d1
potyčce	potyčka	k1gFnSc6
Francouzi	Francouz	k1gMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
zimy	zima	k1gFnSc2
Turci	Turek	k1gMnPc1
pořádali	pořádat	k5eAaImAgMnP
na	na	k7c4
křižácké	křižácký	k2eAgFnPc4d1
kolony	kolona	k1gFnPc4
guerillové	guerillový	k2eAgFnPc1d1
nájezdy	nájezd	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vánoce	Vánoce	k1gFnPc4
křižáci	křižák	k1gMnPc1
strávili	strávit	k5eAaPmAgMnP
v	v	k7c6
maiandsroském	maiandsroský	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
se	se	k3xPyFc4
utkali	utkat	k5eAaPmAgMnP
s	s	k7c7
tureckým	turecký	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
u	u	k7c2
pisidijské	pisidijský	k2eAgFnSc2d1
Antiochie	Antiochie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
byzantských	byzantský	k2eAgFnPc6d1
rukách	ruka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
zaujali	zaujmout	k5eAaPmAgMnP
obranné	obranný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
na	na	k7c6
mostě	most	k1gInSc6
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Anthius	Anthius	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
znemožnili	znemožnit	k5eAaPmAgMnP
rytířům	rytíř	k1gMnPc3
přímý	přímý	k2eAgInSc4d1
úder	úder	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
se	se	k3xPyFc4
křižákům	křižák	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
Turky	Turek	k1gMnPc4
zatlačit	zatlačit	k5eAaPmF
a	a	k8xC
obrátit	obrátit	k5eAaPmF
na	na	k7c4
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
následně	následně	k6eAd1
hledali	hledat	k5eAaImAgMnP
útočiště	útočiště	k1gNnSc4
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
a	a	k8xC
byzantský	byzantský	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
Turky	turek	k1gInPc4
vpustil	vpustit	k5eAaPmAgMnS
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
u	u	k7c2
křižáků	křižák	k1gMnPc2
vyvolalo	vyvolat	k5eAaPmAgNnS
opovržení	opovržení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
poté	poté	k6eAd1
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
Laodikeje	Laodikej	k1gInSc2
a	a	k8xC
odtud	odtud	k6eAd1
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Attaleie	Attaleie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
do	do	k7c2
Attaleie	Attaleie	k1gFnSc2
byla	být	k5eAaImAgFnS
těžká	těžký	k2eAgFnSc1d1
<g/>
,	,	kIx,
křižáci	křižák	k1gMnPc1
měli	mít	k5eAaImAgMnP
málo	málo	k4c4
zásob	zásoba	k1gFnPc2
<g/>
,	,	kIx,
horské	horský	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
byly	být	k5eAaImAgFnP
nebezpečné	bezpečný	k2eNgInPc1d1
a	a	k8xC
turecké	turecký	k2eAgInPc1d1
útoky	útok	k1gInPc1
byly	být	k5eAaImAgInP
na	na	k7c6
denním	denní	k2eAgInSc6d1
pořádku	pořádek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poutníci	poutník	k1gMnPc1
po	po	k7c6
cestě	cesta	k1gFnSc6
naházeli	naházet	k5eAaBmAgMnP,k5eAaPmAgMnP
mrtvoly	mrtvola	k1gFnPc4
německých	německý	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
ze	z	k7c2
skupiny	skupina	k1gFnSc2
Oty	Ota	k1gMnSc2
z	z	k7c2
Freisingu	Freising	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
oblastí	oblast	k1gFnSc7
prošel	projít	k5eAaPmAgInS
krátce	krátce	k6eAd1
před	před	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
jednoho	jeden	k4xCgInSc2
přechodu	přechod	k1gInSc2
horského	horský	k2eAgInSc2d1
průsmyku	průsmyk	k1gInSc2
spatřil	spatřit	k5eAaPmAgMnS
předvoj	předvoj	k1gInSc4
konečně	konečně	k6eAd1
zelené	zelené	k1gNnSc1
údolí	údolí	k1gNnSc2
a	a	k8xC
ačkoliv	ačkoliv	k8xS
měli	mít	k5eAaImAgMnP
vojáci	voják	k1gMnPc1
nařízeno	nařídit	k5eAaPmNgNnS
průsmyk	průsmyk	k1gInSc4
držet	držet	k5eAaImF
<g/>
,	,	kIx,
dokud	dokud	k8xS
nedorazí	dorazit	k5eNaPmIp3nP
ostatní	ostatní	k2eAgMnPc1d1
<g/>
,	,	kIx,
nechali	nechat	k5eAaPmAgMnP
se	s	k7c7
královnou	královna	k1gFnSc7
Eleonorou	Eleonora	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
nesena	nést	k5eAaImNgFnS
v	v	k7c6
nosítkách	nosítka	k1gNnPc6
<g/>
,	,	kIx,
přemluvit	přemluvit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
opustili	opustit	k5eAaPmAgMnP
nehostinný	hostinný	k2eNgInSc4d1
vrchol	vrchol	k1gInSc4
průsmyku	průsmyk	k1gInSc2
a	a	k8xC
vydali	vydat	k5eAaPmAgMnP
se	se	k3xPyFc4
do	do	k7c2
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
čekal	čekat	k5eAaImAgMnS
oddíl	oddíl	k1gInSc4
Turků	Turek	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
okamžitě	okamžitě	k6eAd1
napadl	napadnout	k5eAaPmAgMnS
střed	střed	k1gInSc4
kolony	kolona	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
jen	jen	k9
neozbrojené	ozbrojený	k2eNgInPc4d1
karavany	karavan	k1gInPc4
mezků	mezek	k1gMnPc2
se	se	k3xPyFc4
zavazadly	zavazadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
až	až	k9
v	v	k7c6
zadním	zadní	k2eAgNnSc6d1
voji	voje	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
místo	místo	k6eAd1
běžně	běžně	k6eAd1
považované	považovaný	k2eAgNnSc1d1
za	za	k7c4
nejvíce	nejvíce	k6eAd1,k6eAd3
ohrožené	ohrožený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
k	k	k7c3
místu	místo	k1gNnSc3
boje	boj	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
cesta	cesta	k1gFnSc1
byla	být	k5eAaImAgFnS
příliš	příliš	k6eAd1
úzká	úzký	k2eAgFnSc1d1
a	a	k8xC
doprovodné	doprovodný	k2eAgMnPc4d1
rytíře	rytíř	k1gMnPc4
zastavila	zastavit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
až	až	k9
na	na	k7c4
místo	místo	k1gNnSc4
střetu	střet	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
stalo	stát	k5eAaPmAgNnS
téměř	téměř	k6eAd1
osudným	osudný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
králi	král	k1gMnSc3
zabili	zabít	k5eAaPmAgMnP
koně	kůň	k1gMnPc1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
si	se	k3xPyFc3
proklestil	proklestit	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	s	k7c7
zády	záda	k1gNnPc7
postavil	postavit	k5eAaPmAgMnS
ke	k	k7c3
skále	skála	k1gFnSc3
a	a	k8xC
vzdoroval	vzdorovat	k5eAaImAgMnS
přesile	přesila	k1gFnSc3
až	až	k6eAd1
do	do	k7c2
tmy	tma	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
protivníci	protivník	k1gMnPc1
boj	boj	k1gInSc4
ukončili	ukončit	k5eAaPmAgMnP
a	a	k8xC
ustoupili	ustoupit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následné	následný	k2eAgNnSc1d1
vyšetřování	vyšetřování	k1gNnSc1
bylo	být	k5eAaImAgNnS
neúspěšné	úspěšný	k2eNgNnSc1d1
<g/>
,	,	kIx,
viník	viník	k1gMnSc1
pohromy	pohroma	k1gFnSc2
nebyl	být	k5eNaImAgMnS
nalezen	nalézt	k5eAaBmNgMnS,k5eAaPmNgMnS
a	a	k8xC
hlavním	hlavní	k2eAgNnSc7d1
velením	velení	k1gNnSc7
výpravy	výprava	k1gFnSc2
byl	být	k5eAaImAgMnS
pověřen	pověřen	k2eAgMnSc1d1
velmistr	velmistr	k1gMnSc1
templářského	templářský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
rozpadat	rozpadat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Attaleie	Attaleie	k1gFnSc2
křižáci	křižák	k1gMnPc1
dospěli	dochvít	k5eAaPmAgMnP
v	v	k7c6
únoru	únor	k1gInSc6
1148	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nahoře	nahoře	k6eAd1
<g/>
:	:	kIx,
Křižáci	křižák	k1gMnPc1
před	před	k7c7
Damaškem	Damašek	k1gInSc7
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
<g/>
Dole	dole	k6eAd1
<g/>
:	:	kIx,
Ludvíkův	Ludvíkův	k2eAgInSc1d1
odjezd	odjezd	k1gInSc1
z	z	k7c2
Attaleie	Attaleie	k1gFnSc2
<g/>
,	,	kIx,
miniatura	miniatura	k1gFnSc1
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
Attaleii	Attaleie	k1gFnSc6
však	však	k9
křižáci	křižák	k1gMnPc1
nebyli	být	k5eNaImAgMnP
přijati	přijat	k2eAgMnPc1d1
s	s	k7c7
nadšením	nadšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
na	na	k7c4
křižáky	křižák	k1gInPc4
pohlíželi	pohlížet	k5eAaImAgMnP
jako	jako	k9
na	na	k7c4
barbary	barbar	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
na	na	k7c4
ně	on	k3xPp3gMnPc4
jen	jen	k9
přitahují	přitahovat	k5eAaImIp3nP
tureckou	turecký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
dostatek	dostatek	k1gInSc4
ubytovacích	ubytovací	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
ani	ani	k8xC
zásob	zásoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
obviňovali	obviňovat	k5eAaImAgMnP
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
tíživé	tíživý	k2eAgFnSc2d1
situace	situace	k1gFnSc2
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
jim	on	k3xPp3gMnPc3
dle	dle	k7c2
jejich	jejich	k3xOp3gInSc2
názoru	názor	k1gInSc2
neposkytl	poskytnout	k5eNaPmAgMnS
pomoc	pomoc	k1gFnSc4
a	a	k8xC
paktoval	paktovat	k5eAaImAgMnS
se	se	k3xPyFc4
s	s	k7c7
Turky	Turek	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
pokračovat	pokračovat	k5eAaImF
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
však	však	k9
zuřily	zuřit	k5eAaImAgFnP
zimní	zimní	k2eAgFnPc1d1
bouře	bouř	k1gFnPc1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
okolní	okolní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
byla	být	k5eAaImAgFnS
nechvalně	chvalně	k6eNd1
proslulá	proslulý	k2eAgFnSc1d1
díky	díky	k7c3
pirátům	pirát	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Ludvíkem	Ludvík	k1gMnSc7
objednaných	objednaný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
nakonec	nakonec	k6eAd1
nedorazila	dorazit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
tedy	tedy	k9
nechal	nechat	k5eAaPmAgMnS
nalodit	nalodit	k5eAaPmF
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
dvůr	dvůr	k1gInSc1
a	a	k8xC
část	část	k1gFnSc1
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velení	velení	k1gNnSc1
nad	nad	k7c7
zbytkem	zbytek	k1gInSc7
vojska	vojsko	k1gNnSc2
přenechal	přenechat	k5eAaPmAgMnS
hraběti	hrabě	k1gMnSc3
Thierrymu	Thierrym	k1gInSc2
Flanderskému	flanderský	k2eAgInSc3d1
s	s	k7c7
rozkazem	rozkaz	k1gInSc7
armádu	armáda	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
dostat	dostat	k5eAaPmF
na	na	k7c6
dalších	další	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
připlout	připlout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Ludvíkův	Ludvíkův	k2eAgInSc1d1
odchod	odchod	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
interpretován	interpretovat	k5eAaBmNgMnS
jako	jako	k8xS,k8xC
porušení	porušení	k1gNnSc4
povinností	povinnost	k1gFnPc2
velitele	velitel	k1gMnSc2
a	a	k8xC
pozdější	pozdní	k2eAgMnPc1d2
kronikáři	kronikář	k1gMnPc1
našli	najít	k5eAaPmAgMnP
viníka	viník	k1gMnSc4
v	v	k7c6
byzantském	byzantský	k2eAgInSc6d1
císaři	císař	k1gMnSc3
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
králově	králův	k2eAgInSc6d1
odjezdu	odjezd	k1gInSc6
se	se	k3xPyFc4
attalejští	attalejský	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
nad	nad	k7c4
křižáky	křižák	k1gInPc4
slitovali	slitovat	k5eAaPmAgMnP
a	a	k8xC
poskytli	poskytnout	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
přístřeší	přístřeší	k1gNnSc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
později	pozdě	k6eAd2
pak	pak	k6eAd1
dorazily	dorazit	k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
hrabě	hrabě	k1gMnSc1
Thierry	Thierra	k1gFnSc2
se	s	k7c7
zbytkem	zbytek	k1gInSc7
rytířů	rytíř	k1gMnPc2
nalodil	nalodit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěším	pěší	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
a	a	k8xC
poutníkům	poutník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
na	na	k7c4
zaplacení	zaplacení	k1gNnSc4
přepravy	přeprava	k1gFnSc2
neměli	mít	k5eNaImAgMnP
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
poradil	poradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
do	do	k7c2
Antiochie	Antiochie	k1gFnSc2
dostali	dostat	k5eAaPmAgMnP
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
pěst	pěst	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Téměř	téměř	k6eAd1
všichni	všechen	k3xTgMnPc1
opěšalí	opěšalý	k2eAgMnPc1d1
poutníci	poutník	k1gMnPc1
bez	bez	k7c2
velitelů	velitel	k1gMnPc2
a	a	k8xC
bez	bez	k7c2
zásob	zásoba	k1gFnPc2
zemřeli	zemřít	k5eAaPmAgMnP
vyčerpáním	vyčerpání	k1gNnSc7
<g/>
,	,	kIx,
hlady	hlad	k1gInPc7
nebo	nebo	k8xC
byli	být	k5eAaImAgMnP
pozabíjeni	pozabíjet	k5eAaPmNgMnP
tureckými	turecký	k2eAgMnPc7d1
nájezdníky	nájezdník	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Zlomek	zlomek	k1gInSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nakonec	nakonec	k9
koncem	konec	k1gInSc7
jara	jaro	k1gNnSc2
skutečně	skutečně	k6eAd1
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Antiochie	Antiochie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
</s>
<s>
Levanta	Levanta	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1148	#num#	k4
se	se	k3xPyFc4
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
vylodil	vylodit	k5eAaPmAgMnS
v	v	k7c6
přístavu	přístav	k1gInSc6
Saint-Symeon	Saint-Symeona	k1gFnPc2
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
uvítán	uvítat	k5eAaPmNgMnS
antiochijským	antiochijský	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
Raimondem	Raimond	k1gMnSc7
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
zároveň	zároveň	k6eAd1
strýcem	strýc	k1gMnSc7
královny	královna	k1gFnSc2
Eleonory	Eleonora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
obtížné	obtížný	k2eAgFnSc3d1
cestě	cesta	k1gFnSc3
a	a	k8xC
utrpěným	utrpěný	k2eAgFnPc3d1
újmám	újma	k1gFnPc3
představovali	představovat	k5eAaImAgMnP
francouzští	francouzský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
neutrpěli	utrpět	k5eNaPmAgMnP
zdaleka	zdaleka	k6eAd1
takové	takový	k3xDgFnPc4
ztráty	ztráta	k1gFnPc4
jako	jako	k8xC,k8xS
pěchota	pěchota	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
značnou	značný	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
téměř	téměř	k6eAd1
všichni	všechen	k3xTgMnPc1
křižáčtí	křižácký	k2eAgMnPc1d1
vládci	vládce	k1gMnPc1
krále	král	k1gMnSc4
žádali	žádat	k5eAaImAgMnP
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joscelin	Joscelina	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
žádal	žádat	k5eAaImAgMnS
o	o	k7c4
podporu	podpora	k1gFnSc4
při	při	k7c6
znovudobytí	znovudobytí	k1gNnSc6
Edessy	Edessa	k1gFnSc2
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
hrabě	hrabě	k1gMnSc1
Raimond	Raimond	k1gMnSc1
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
Ludvíka	Ludvík	k1gMnSc2
chtěl	chtít	k5eAaImAgMnS
požádat	požádat	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
Francouzi	Francouz	k1gMnPc1
pomohli	pomoct	k5eAaPmAgMnP
při	při	k7c6
znovudobývání	znovudobývání	k1gNnSc6
hradu	hrad	k1gInSc2
Montferrand	Montferranda	k1gFnPc2
a	a	k8xC
několika	několik	k4yIc2
dalších	další	k2eAgFnPc2d1
pohraničních	pohraniční	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
zatím	zatím	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
osobně	osobně	k6eAd1
dorazil	dorazit	k5eAaPmAgMnS
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
s	s	k7c7
pozváním	pozvání	k1gNnSc7
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
a	a	k8xC
také	také	k9
se	s	k7c7
zprávou	zpráva	k1gFnSc7
o	o	k7c4
uzdravení	uzdravení	k1gNnSc4
německého	německý	k2eAgMnSc2d1
krále	král	k1gMnSc2
Konráda	Konrád	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Joscelin	Joscelina	k1gFnPc2
z	z	k7c2
Edessy	Edessa	k1gFnSc2
a	a	k8xC
Raimond	Raimonda	k1gFnPc2
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
stále	stále	k6eAd1
znesváření	znesvářený	k2eAgMnPc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
možnost	možnost	k1gFnSc1
případné	případný	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
pro	pro	k7c4
Joscelinovo	Joscelinův	k2eAgNnSc4d1
hrabství	hrabství	k1gNnSc4
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
nijak	nijak	k6eAd1
zvláště	zvláště	k6eAd1
neprojednávala	projednávat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
pomoc	pomoc	k1gFnSc4
Raimondovi	Raimondův	k2eAgMnPc1d1
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
při	při	k7c6
dobývání	dobývání	k1gNnSc6
jeho	jeho	k3xOp3gInPc2
hradů	hrad	k1gInPc2
na	na	k7c6
tripolsko-muslimském	tripolsko-muslimský	k2eAgNnSc6d1
pomezí	pomezí	k1gNnSc6
nevypadala	vypadat	k5eNaPmAgFnS,k5eNaImAgFnS
pro	pro	k7c4
Ludvíka	Ludvík	k1gMnSc4
prestižně	prestižně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatně	ostatně	k6eAd1
Raimond	Raimond	k1gInSc1
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
nenaléhal	naléhat	k5eNaImAgInS
<g/>
;	;	kIx,
důvodem	důvod	k1gInSc7
byla	být	k5eAaImAgFnS
účast	účast	k1gFnSc1
hraběte	hrabě	k1gMnSc2
Alfonse	Alfons	k1gMnSc2
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Tripolisu	Tripolis	k1gInSc6
měli	mít	k5eAaImAgMnP
stále	stále	k6eAd1
na	na	k7c6
paměti	paměť	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
rod	rod	k1gInSc4
hrabat	hrabě	k1gNnPc2
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
<g/>
,	,	kIx,
větev	větev	k1gFnSc1
rodu	rod	k1gInSc2
Raimundovců	Raimundovec	k1gMnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
nelegitimním	legitimní	k2eNgMnSc7d1
synem	syn	k1gMnSc7
hraběte	hrabě	k1gMnSc2
Raimonda	Raimond	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
Bernardem	Bernard	k1gMnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Alfons	Alfons	k1gMnSc1
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
byl	být	k5eAaImAgInS
legitimním	legitimní	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Raimonda	Raimond	k1gMnSc2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
mladším	mladý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
Bernarda	Bernard	k1gMnSc2
<g/>
;	;	kIx,
pro	pro	k7c4
Raimonda	Raimond	k1gMnSc4
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
tu	tu	k6eAd1
bylo	být	k5eAaImAgNnS
riziko	riziko	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
si	se	k3xPyFc3
Alfons	Alfons	k1gMnSc1
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
mohl	moct	k5eAaImAgMnS
činit	činit	k5eAaImF
nárok	nárok	k1gInSc4
na	na	k7c4
Hrabství	hrabství	k1gNnSc4
Tripolis	Tripolis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kníže	kníže	k1gMnSc1
Raimond	Raimond	k1gMnSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
vítá	vítat	k5eAaImIp3nS
krále	král	k1gMnSc4
Ludvíka	Ludvík	k1gMnSc4
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
<g/>
,	,	kIx,
Passages	Passages	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Outremer	Outremer	k1gMnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Raimond	Raimond	k1gInSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
králi	král	k1gMnSc3
navrhl	navrhnout	k5eAaPmAgInS
společný	společný	k2eAgInSc1d1
útok	útok	k1gInSc1
francouzských	francouzský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
a	a	k8xC
antiochijské	antiochijský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c6
Núr	Núr	k1gFnSc6
ad-Dínovo	ad-Dínův	k2eAgNnSc1d1
mocenské	mocenský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Aleppo	Aleppa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raimondovi	Raimond	k1gMnSc3
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
muslimové	muslim	k1gMnPc1
proti	proti	k7c3
křižáckým	křižácký	k2eAgInPc3d1
státům	stát	k1gInPc3
sjednotí	sjednotit	k5eAaPmIp3nS
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
konec	konec	k1gInSc1
křižáků	křižák	k1gMnPc2
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
neodvratný	odvratný	k2eNgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Útok	útok	k1gInSc1
na	na	k7c4
Aleppo	Aleppa	k1gFnSc5
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
následné	následný	k2eAgNnSc4d1
ovládnutí	ovládnutí	k1gNnSc4
měl	mít	k5eAaImAgInS
Núr	Núr	k1gFnSc4
ad-Dínovým	ad-Dínův	k2eAgFnPc3d1
snahám	snaha	k1gFnPc3
silou	síla	k1gFnSc7
sjednotit	sjednotit	k5eAaPmF
muslimy	muslim	k1gMnPc4
učinit	učinit	k5eAaImF,k5eAaPmF
přítrž	přítrž	k1gFnSc4
<g/>
,	,	kIx,
křižáci	křižák	k1gMnPc1
by	by	kYmCp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
naopak	naopak	k6eAd1
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
na	na	k7c6
Východě	východ	k1gInSc6
značně	značně	k6eAd1
posílili	posílit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
Raimond	Raimond	k1gInSc1
počítal	počítat	k5eAaImAgInS
také	také	k9
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Núr	Núr	k1gFnSc4
ad-Dínovi	ad-Dínův	k2eAgMnPc1d1
muslimští	muslimský	k2eAgMnPc1d1
rivalové	rival	k1gMnPc1
ho	on	k3xPp3gInSc4
v	v	k7c6
sevření	sevření	k1gNnSc6
křižáků	křižák	k1gInPc2
buď	buď	k8xC
nechají	nechat	k5eAaPmIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
k	k	k7c3
válce	válka	k1gFnSc3
proti	proti	k7c3
němu	on	k3xPp3gInSc3
sami	sám	k3xTgMnPc1
připojí	připojit	k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Raimond	Raimond	k1gMnSc1
sám	sám	k3xTgMnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
podnikl	podniknout	k5eAaPmAgMnS
průzkum	průzkum	k1gInSc4
až	až	k9
pod	pod	k7c4
samotné	samotný	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
Aleppa	Aleppa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Francouzští	francouzský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
byli	být	k5eAaImAgMnP
plánem	plán	k1gInSc7
útoku	útok	k1gInSc6
nadšeni	nadšen	k2eAgMnPc1d1
a	a	k8xC
podpořila	podpořit	k5eAaPmAgFnS
jej	on	k3xPp3gMnSc4
také	také	k9
královna	královna	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
dle	dle	k7c2
jejíchž	jejíž	k3xOyRp3gFnPc2
rad	rada	k1gFnPc2
se	se	k3xPyFc4
král	král	k1gMnSc1
obvykle	obvykle	k6eAd1
řídil	řídit	k5eAaImAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Tentokrát	tentokrát	k6eAd1
Ludvík	Ludvík	k1gMnSc1
váhal	váhat	k5eAaImAgMnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc4
důvody	důvod	k1gInPc4
proti	proti	k7c3
byly	být	k5eAaImAgFnP
náboženské	náboženský	k2eAgFnPc1d1
<g/>
,	,	kIx,
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
chápal	chápat	k5eAaImAgMnS
jako	jako	k8xC,k8xS
především	především	k6eAd1
náboženskou	náboženský	k2eAgFnSc4d1
pouť	pouť	k1gFnSc4
na	na	k7c4
svatá	svatý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
křesťanství	křesťanství	k1gNnSc2
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Aleppo	Aleppa	k1gFnSc5
nebylo	být	k5eNaImAgNnS
poutním	poutní	k2eAgNnSc7d1
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
ani	ani	k8xC
nebylo	být	k5eNaImAgNnS
zmiňováno	zmiňovat	k5eAaImNgNnS
v	v	k7c6
Bibli	bible	k1gFnSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Ostatně	ostatně	k6eAd1
to	ten	k3xDgNnSc4
samé	samý	k3xTgNnSc4
byl	být	k5eAaImAgInS
jeden	jeden	k4xCgInSc1
z	z	k7c2
předních	přední	k2eAgInPc2d1
argumentů	argument	k1gInPc2
odpůrců	odpůrce	k1gMnPc2
tažení	tažení	k1gNnSc2
na	na	k7c6
Edessu	Edess	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInSc1d1
důvod	důvod	k1gInSc1
byl	být	k5eAaImAgInS
politický	politický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
byl	být	k5eAaImAgMnS
nervózní	nervózní	k2eAgMnSc1d1
z	z	k7c2
delší	dlouhý	k2eAgFnSc2d2
doby	doba	k1gFnSc2
strávené	strávený	k2eAgFnSc2d1
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
nebo	nebo	k8xC
na	na	k7c6
nějakém	nějaký	k3yIgNnSc6
vojenském	vojenský	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgInS
již	již	k6eAd1
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
německý	německý	k2eAgMnSc1d1
král	král	k1gMnSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
vést	vést	k5eAaImF
politiku	politika	k1gFnSc4
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
jej	on	k3xPp3gMnSc4
zaráželo	zarážet	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Raimond	Raimond	k1gInSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
tráví	trávit	k5eAaImIp3nP
hodně	hodně	k6eAd1
času	čas	k1gInSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
ženy	žena	k1gFnSc2
královny	královna	k1gFnSc2
Eleonory	Eleonora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
důvěrné	důvěrný	k2eAgNnSc4d1
chování	chování	k1gNnSc4
knížete	kníže	k1gNnSc2wR
ke	k	k7c3
královně	královna	k1gFnSc3
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc1
neúnavné	únavný	k2eNgNnSc1d1
a	a	k8xC
skoro	skoro	k6eAd1
nepřetržité	přetržitý	k2eNgInPc1d1
rozhovory	rozhovor	k1gInPc1
<g/>
,	,	kIx,
vzbudily	vzbudit	k5eAaPmAgInP
u	u	k7c2
krále	král	k1gMnSc2
podezření	podezření	k1gNnSc4
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Jan	Jan	k1gMnSc1
ze	z	k7c2
Salisbury	Salisbura	k1gFnSc2
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
žárlivý	žárlivý	k2eAgMnSc1d1
král	král	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
s	s	k7c7
armádou	armáda	k1gFnSc7
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Eleonora	Eleonora	k1gFnSc1
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
a	a	k8xC
pohrozila	pohrozit	k5eAaPmAgFnS
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1
rozvodem	rozvod	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
vzpurnou	vzpurný	k2eAgFnSc4d1
choť	choť	k1gFnSc4
odvézt	odvézt	k5eAaPmF
násilím	násilí	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
což	což	k9
knížete	kníže	k1gNnSc2wR
Raimonda	Raimond	k1gMnSc2
rozzuřilo	rozzuřit	k5eAaPmAgNnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dále	daleko	k6eAd2
odmítl	odmítnout	k5eAaPmAgMnS
podílet	podílet	k5eAaImF
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Francouzští	francouzský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
Hrabství	hrabství	k1gNnSc2
Tripolisu	Tripolis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sněm	sněm	k1gInSc1
v	v	k7c6
Akkonu	Akkon	k1gInSc6
</s>
<s>
Nahoře	nahoře	k6eAd1
<g/>
:	:	kIx,
Králové	Králové	k2eAgMnSc1d1
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
Balduin	Balduin	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
Dole	dole	k6eAd1
<g/>
:	:	kIx,
Útok	útok	k1gInSc1
křižáků	křižák	k1gMnPc2
na	na	k7c4
Damašek	Damašek	k1gInSc4
</s>
<s>
Ani	ani	k8xC
hrabě	hrabě	k1gMnSc1
Raimond	Raimond	k1gMnSc1
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
se	se	k3xPyFc4
k	k	k7c3
výpravě	výprava	k1gFnSc3
nepřipojil	připojit	k5eNaPmAgInS
<g/>
,	,	kIx,
nestál	stát	k5eNaImAgInS
o	o	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
hrabětem	hrabě	k1gMnSc7
Alfonsem	Alfons	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Flotila	flotila	k1gFnSc1
z	z	k7c2
Provence	Provence	k1gFnSc2
vedená	vedený	k2eAgFnSc1d1
hrabětem	hrabě	k1gMnSc7
Alfonsem	Alfons	k1gMnSc7
zatím	zatím	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
Caesareje	Caesarej	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Alfons	Alfons	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
obviňován	obviňován	k2eAgInSc1d1
Raimond	Raimond	k1gInSc1
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ho	on	k3xPp3gNnSc4
údajně	údajně	k6eAd1
nechal	nechat	k5eAaPmAgMnS
otrávit	otrávit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
příchodu	příchod	k1gInSc6
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
Ludvík	Ludvík	k1gMnSc1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
Konráda	Konrád	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
navštívil	navštívit	k5eAaPmAgInS
svatá	svatý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
a	a	k8xC
poté	poté	k6eAd1
byli	být	k5eAaImAgMnP
jeruzalémskou	jeruzalémský	k2eAgFnSc7d1
královnou	královna	k1gFnSc7
Melisendou	Melisenda	k1gFnSc7
a	a	k8xC
králem	král	k1gMnSc7
Balduinem	Balduin	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
přizváni	přizvat	k5eAaPmNgMnP
ke	k	k7c3
sněmu	sněm	k1gInSc3
křižáckých	křižácký	k2eAgMnPc2d1
velitelů	velitel	k1gMnPc2
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komitét	komitét	k1gInSc1
v	v	k7c6
Akkonu	Akkon	k1gInSc6
byl	být	k5eAaImAgInS
největším	veliký	k2eAgNnSc7d3
shromážděním	shromáždění	k1gNnSc7
urozenců	urozenec	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
kdy	kdy	k6eAd1
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
konal	konat	k5eAaImAgMnS
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
navzdory	navzdory	k7c3
nepřítomnosti	nepřítomnost	k1gFnSc3
antiochijského	antiochijský	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
<g/>
,	,	kIx,
hraběte	hrabě	k1gMnSc2
z	z	k7c2
Tripolisu	Tripolis	k1gInSc2
a	a	k8xC
Joscelina	Joscelina	k1gFnSc1
z	z	k7c2
Edessy	Edessa	k1gFnSc2
dokázali	dokázat	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
dát	dát	k5eAaPmF
dohromady	dohromady	k6eAd1
největší	veliký	k2eAgFnSc4d3
armádu	armáda	k1gFnSc4
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Akkonské	Akkonský	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
se	se	k3xPyFc4
odehrálo	odehrát	k5eAaPmAgNnS
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1148	#num#	k4
v	v	k7c6
Palmareji	Palmarej	k1gInSc6
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
velkého	velký	k2eAgInSc2d1
palestinského	palestinský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
Akkon	Akkona	k1gFnPc2
a	a	k8xC
rychle	rychle	k6eAd1
přijalo	přijmout	k5eAaPmAgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
o	o	k7c6
cíli	cíl	k1gInSc6
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
Damašek	Damašek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Směr	směr	k1gInSc1
Damašek	Damašek	k1gInSc1
podporoval	podporovat	k5eAaImAgMnS
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
král	král	k1gMnSc1
i	i	k8xC
templáři	templář	k1gMnPc1
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
a	a	k8xC
Konrád	Konrád	k1gMnSc1
s	s	k7c7
Ludvíkem	Ludvík	k1gMnSc7
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
nechali	nechat	k5eAaPmAgMnP
přesvědčit	přesvědčit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Damašek	Damašek	k1gInSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
evropské	evropský	k2eAgMnPc4d1
křižáky	křižák	k1gMnPc4
přitažlivým	přitažlivý	k2eAgNnSc7d1
náboženským	náboženský	k2eAgNnSc7d1
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
často	často	k6eAd1
zmiňovaným	zmiňovaný	k2eAgInSc7d1
v	v	k7c6
Bibli	bible	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mohlo	moct	k5eAaImAgNnS
vyvolat	vyvolat	k5eAaPmF
náboženskou	náboženský	k2eAgFnSc4d1
euforii	euforie	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
navíc	navíc	k6eAd1
jeho	jeho	k3xOp3gNnSc4
obsazení	obsazení	k1gNnSc4
křesťany	křesťan	k1gMnPc4
by	by	kYmCp3nS
přerušilo	přerušit	k5eAaPmAgNnS
důležité	důležitý	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
mezi	mezi	k7c7
muslimskými	muslimský	k2eAgInPc7d1
centry	centr	k1gInPc7
Káhirou	Káhira	k1gFnSc7
a	a	k8xC
Bagdádem	Bagdád	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
ležel	ležet	k5eAaImAgInS
cíl	cíl	k1gInSc1
výpravy	výprava	k1gFnSc2
v	v	k7c6
úrodné	úrodný	k2eAgFnSc6d1
bohaté	bohatý	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
rytíři	rytíř	k1gMnPc1
mohli	moct	k5eAaImAgMnP
získat	získat	k5eAaPmF
výnosná	výnosný	k2eAgNnPc4d1
léna	léno	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Damašský	damašský	k2eAgMnSc1d1
emír	emír	k1gMnSc1
Muín	Muín	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Unur	Unur	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
udržovat	udržovat	k5eAaImF
s	s	k7c7
Jeruzalémským	jeruzalémský	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
přátelské	přátelský	k2eAgInPc4d1
vztahy	vztah	k1gInPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
stejně	stejně	k6eAd1
jako	jako	k9
křižáci	křižák	k1gMnPc1
obával	obávat	k5eAaImAgMnS
Núr	Núr	k1gMnSc7
ad-Dínovy	ad-Dínův	k2eAgFnSc2d1
expanzivní	expanzivní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Fulka	Fulek	k1gMnSc2
(	(	kIx(
<g/>
†	†	k?
1143	#num#	k4
<g/>
)	)	kIx)
však	však	k9
regentská	regentský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
královny	královna	k1gFnSc2
Melisendy	Melisenda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
spojenectví	spojenectví	k1gNnSc4
odmítla	odmítnout	k5eAaPmAgFnS
jako	jako	k9
nepotřebné	potřebný	k2eNgInPc4d1
a	a	k8xC
roku	rok	k1gInSc2
1147	#num#	k4
podnikla	podniknout	k5eAaPmAgFnS
proti	proti	k7c3
městu	město	k1gNnSc3
neúspěšnou	úspěšný	k2eNgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémští	jeruzalémský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
tak	tak	k6eAd1
možná	možná	k9
viděli	vidět	k5eAaImAgMnP
možnost	možnost	k1gFnSc4
svou	svůj	k3xOyFgFnSc4
reputaci	reputace	k1gFnSc4
napravit	napravit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Damašku	Damašek	k1gInSc2
</s>
<s>
Obléhání	obléhání	k1gNnSc1
DamaškuLes	DamaškuLesa	k1gFnPc2
Passages	Passagesa	k1gFnPc2
Faits	Faits	k1gInSc1
Outremer	Outremer	k1gMnSc1
par	para	k1gFnPc2
les	les	k1gInSc4
Français	Français	k1gFnSc4
contre	contr	k1gInSc5
les	les	k1gInSc1
Turcs	Turcs	k1gInSc1
et	et	k?
Autres	Autres	k1gInSc1
Sarazaines	Sarazaines	k1gInSc1
et	et	k?
Maures	Maures	k1gInSc1
Outremarins	Outremarinsa	k1gFnPc2
<g/>
,	,	kIx,
1490	#num#	k4
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
největších	veliký	k2eAgNnPc2d3
palestinských	palestinský	k2eAgNnPc2d1
veder	vedro	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
křižácké	křižácký	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
shromáždily	shromáždit	k5eAaPmAgFnP
v	v	k7c6
Tiberiadě	Tiberiada	k1gFnSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
vytáhly	vytáhnout	k5eAaPmAgInP
proti	proti	k7c3
Damašku	Damašek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojená	spojený	k2eAgFnSc1d1
francouzsko-jeruzalémská	francouzsko-jeruzalémský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
čítala	čítat	k5eAaImAgFnS
asi	asi	k9
50	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Vojsko	vojsko	k1gNnSc1
prošlo	projít	k5eAaPmAgNnS
okolo	okolo	k7c2
Galilejského	galilejský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
směrem	směr	k1gInSc7
na	na	k7c4
Banias	Banias	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
směřován	směřován	k2eAgInSc1d1
od	od	k7c2
západu	západ	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
rozprostíraly	rozprostírat	k5eAaImAgFnP
ovocné	ovocný	k2eAgFnPc1d1
sady	sada	k1gFnPc1
a	a	k8xC
tudíž	tudíž	k8xC
by	by	kYmCp3nP
křižáci	křižák	k1gMnPc1
měli	mít	k5eAaImAgMnP
dobré	dobrý	k2eAgNnSc4d1
zásobování	zásobování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
k	k	k7c3
městu	město	k1gNnSc3
dorazili	dorazit	k5eAaPmAgMnP
v	v	k7c4
pátek	pátek	k1gInSc4
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Damašský	damašský	k2eAgInSc1d1
emír	emír	k1gMnSc1
byl	být	k5eAaImAgInS
útokem	útok	k1gInSc7
křižáků	křižák	k1gMnPc2
překvapen	překvapen	k2eAgMnSc1d1
<g/>
,	,	kIx,
posádka	posádka	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
byla	být	k5eAaImAgFnS
nepočetná	početný	k2eNgFnSc1d1
a	a	k8xC
musel	muset	k5eAaImAgMnS
proto	proto	k8xC
požádat	požádat	k5eAaPmF
o	o	k7c4
pomoc	pomoc	k1gFnSc4
Saífa	Saíf	k1gMnSc2
ad-Dína	ad-Dín	k1gMnSc2
Ghaziho	Ghazi	k1gMnSc2
z	z	k7c2
Mosulu	Mosul	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
Núr	Núr	k1gMnSc2
ad-Dína	ad-Dín	k1gMnSc2
z	z	k7c2
Aleppa	Alepp	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Núr	Núr	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
žádost	žádost	k1gFnSc4
vyslyšel	vyslyšet	k5eAaPmAgMnS
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
čele	čelo	k1gNnSc6
armády	armáda	k1gFnSc2
na	na	k7c4
cestu	cesta	k1gFnSc4
k	k	k7c3
obleženému	obležený	k2eAgInSc3d1
Damašku	Damašek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Křižáčtí	křižácký	k2eAgMnPc1d1
útočníci	útočník	k1gMnPc1
byli	být	k5eAaImAgMnP
od	od	k7c2
hradeb	hradba	k1gFnPc2
zatlačeni	zatlačen	k2eAgMnPc1d1
zpátky	zpátky	k6eAd1
do	do	k7c2
sadů	sad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
velel	velet	k5eAaImAgInS
Nadžm	Nadžm	k1gInSc1
ad-Dín	ad-Dín	k1gMnSc1
Ajjúb	Ajjúb	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
slavného	slavný	k2eAgMnSc2d1
válečníka	válečník	k1gMnSc2
Saladina	Saladina	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
vynalézavě	vynalézavě	k6eAd1
pořádala	pořádat	k5eAaImAgFnS
partyzánské	partyzánský	k2eAgInPc4d1
útoky	útok	k1gInPc4
do	do	k7c2
křižáckého	křižácký	k2eAgNnSc2d1
ležení	ležení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
popisu	popis	k1gInSc2
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
kronikáře	kronikář	k1gMnSc2
Viléma	Vilém	k1gMnSc2
z	z	k7c2
Tyru	Tyrus	k1gInSc2
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
rozhodli	rozhodnout	k5eAaPmAgMnP
přesunout	přesunout	k5eAaPmF
na	na	k7c4
východní	východní	k2eAgFnSc4d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
opevněnou	opevněný	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebyly	být	k5eNaImAgInP
žádné	žádný	k3yNgFnPc4
sady	sada	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
ani	ani	k8xC
voda	voda	k1gFnSc1
a	a	k8xC
jídlo	jídlo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
přesun	přesun	k1gInSc1
na	na	k7c4
východní	východní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
nevýhodný	výhodný	k2eNgMnSc1d1
<g/>
,	,	kIx,
vzklíčilo	vzklíčit	k5eAaPmAgNnS
mezi	mezi	k7c7
Evropany	Evropan	k1gMnPc7
podezření	podezření	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
některý	některý	k3yIgMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
z	z	k7c2
jeruzalémských	jeruzalémský	k2eAgMnPc2d1
velitelů	velitel	k1gMnPc2
je	být	k5eAaImIp3nS
s	s	k7c7
muslimy	muslim	k1gMnPc7
spolčený	spolčený	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
emír	emír	k1gMnSc1
Unur	Unur	k1gMnSc1
křižákům	křižák	k1gMnPc3
nabídl	nabídnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zruší	zrušit	k5eAaPmIp3nS
své	svůj	k3xOyFgNnSc4
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
Núr	Núr	k1gMnSc7
ad-Dínem	ad-Dín	k1gMnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
ustoupí	ustoupit	k5eAaPmIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
navíc	navíc	k6eAd1
začali	začít	k5eAaPmAgMnP
hádat	hádat	k5eAaImF
<g/>
,	,	kIx,
komu	kdo	k3yQnSc3,k3yRnSc3,k3yInSc3
vlastně	vlastně	k9
Damašek	Damašek	k1gInSc4
po	po	k7c4
dobytí	dobytí	k1gNnSc4
připadne	připadnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
obdržení	obdržení	k1gNnSc6
zpráv	zpráva	k1gFnPc2
o	o	k7c6
postupu	postup	k1gInSc6
Saíf	Saíf	k1gInSc1
ad-Dína	ad-Dín	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
odmítli	odmítnout	k5eAaPmAgMnP
jeruzalémští	jeruzalémský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
dále	daleko	k6eAd2
podílet	podílet	k5eAaImF
na	na	k7c4
obléhání	obléhání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Němci	Němec	k1gMnPc1
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
sice	sice	k8xC
protestovali	protestovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
tři	tři	k4xCgMnPc1
králové	král	k1gMnPc1
nyní	nyní	k6eAd1
neměli	mít	k5eNaImAgMnP
na	na	k7c4
výběr	výběr	k1gInSc4
a	a	k8xC
museli	muset	k5eAaImAgMnP
dát	dát	k5eAaPmF
rozkaz	rozkaz	k1gInSc4
k	k	k7c3
ústupu	ústup	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Středomoří	středomoří	k1gNnSc4
po	po	k7c6
druhé	druhý	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
</s>
<s>
Křesťané	křesťan	k1gMnPc1
se	se	k3xPyFc4
poté	poté	k6eAd1
obviňovali	obviňovat	k5eAaImAgMnP
navzájem	navzájem	k6eAd1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
je	on	k3xPp3gNnSc4
vinen	vinen	k2eAgMnSc1d1
neúspěchem	neúspěch	k1gInSc7
tažení	tažení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Evropané	Evropan	k1gMnPc1
jeruzalémským	jeruzalémský	k2eAgMnPc3d1
rytířům	rytíř	k1gMnPc3
vyčítali	vyčítat	k5eAaImAgMnP
nedostatek	nedostatek	k1gInSc4
odvahy	odvaha	k1gFnSc2
a	a	k8xC
nadšení	nadšení	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ti	ten	k3xDgMnPc1
evropským	evropský	k2eAgMnPc3d1
rytířům	rytíř	k1gMnPc3
vyčítali	vyčítat	k5eAaImAgMnP
nezkušenost	nezkušenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
měli	mít	k5eAaImAgMnP
snahu	snaha	k1gFnSc4
v	v	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
pokračovat	pokračovat	k5eAaImF
a	a	k8xC
naplánovali	naplánovat	k5eAaBmAgMnP
novou	nový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
proti	proti	k7c3
Askalonu	Askalon	k1gInSc3
<g/>
,	,	kIx,
mocnému	mocný	k2eAgNnSc3d1
palestinskému	palestinský	k2eAgNnSc3d1
městu	město	k1gNnSc3
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
fátimovského	fátimovský	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
se	se	k3xPyFc4
zbytkem	zbytek	k1gInSc7
armády	armáda	k1gFnSc2
vyrazil	vyrazit	k5eAaPmAgInS
proti	proti	k7c3
Askalonu	Askalon	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
nedorazily	dorazit	k5eNaPmAgInP
mu	on	k3xPp3gMnSc3
žádné	žádný	k3yNgFnPc4
posily	posila	k1gFnPc4
kvůli	kvůli	k7c3
nedůvěře	nedůvěra	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
vůči	vůči	k7c3
sobě	se	k3xPyFc3
křižáčtí	křižácký	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
pociťovali	pociťovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
v	v	k7c6
září	září	k1gNnSc6
1148	#num#	k4
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1
království	království	k1gNnSc4
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
císařovo	císařův	k2eAgNnSc4d1
pozvání	pozvání	k1gNnSc4
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
strávil	strávit	k5eAaPmAgMnS
vánoční	vánoční	k2eAgInPc4d1
svátky	svátek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc1
spojenectví	spojenectví	k1gNnSc1
namířené	namířený	k2eAgNnSc1d1
proti	proti	k7c3
sicilským	sicilský	k2eAgMnPc3d1
Normanům	Norman	k1gMnPc3
bylo	být	k5eAaImAgNnS
stvrzeno	stvrzen	k2eAgNnSc1d1
sňatkem	sňatek	k1gInSc7
Manuelovy	Manuelův	k2eAgFnSc2d1
neteře	neteř	k1gFnSc2
Theodory	Theodora	k1gFnSc2
a	a	k8xC
Konrádova	Konrádův	k2eAgMnSc2d1
nevlastního	vlastní	k2eNgMnSc2d1
bratra	bratr	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
odkládal	odkládat	k5eAaImAgMnS
svůj	svůj	k3xOyFgInSc4
odjezd	odjezd	k1gInSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
čekal	čekat	k5eAaImAgInS
rozvod	rozvod	k1gInSc1
s	s	k7c7
Eleonorou	Eleonora	k1gFnSc7
Akvitánskou	Akvitánský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
Jeruzalémském	jeruzalémský	k2eAgNnSc6d1
království	království	k1gNnSc6
nabrala	nabrat	k5eAaPmAgFnS
králova	králův	k2eAgFnSc1d1
politika	politika	k1gFnSc1
jasně	jasně	k6eAd1
protibyzantský	protibyzantský	k2eAgInSc4d1
kurs	kurs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
roku	rok	k1gInSc2
1149	#num#	k4
Palestinu	Palestina	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
na	na	k7c6
palubě	paluba	k1gFnSc6
sicilské	sicilský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
a	a	k8xC
s	s	k7c7
Rogerem	Roger	k1gInSc7
Sicilským	sicilský	k2eAgInSc7d1
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
naplánoval	naplánovat	k5eAaBmAgMnS
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papeže	Papež	k1gMnSc2
Evžena	Evžen	k1gMnSc2
však	však	k9
tímto	tento	k3xDgInSc7
plánem	plán	k1gInSc7
nezískal	získat	k5eNaPmAgInS
a	a	k8xC
z	z	k7c2
výpravy	výprava	k1gFnSc2
nakonec	nakonec	k6eAd1
sešlo	sejít	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
porážku	porážka	k1gFnSc4
křižáků	křižák	k1gMnPc2
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
selhání	selhání	k1gNnSc4
a	a	k8xC
svatému	svatý	k2eAgMnSc3d1
otci	otec	k1gMnSc3
poslal	poslat	k5eAaPmAgInS
omluvný	omluvný	k2eAgInSc1d1
dopis	dopis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
vložený	vložený	k2eAgInSc1d1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
části	část	k1gFnSc6
Bernadovy	Bernadův	k2eAgFnSc2d1
Knihy	kniha	k1gFnSc2
úvah	úvaha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
příčinu	příčina	k1gFnSc4
fiaska	fiasko	k1gNnSc2
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
přílišná	přílišný	k2eAgFnSc1d1
hříšnost	hříšnost	k1gFnSc1
křižáků	křižák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgMnSc1d1
letopisec	letopisec	k1gMnSc1
Jarloch	Jarloch	k1gMnSc1
okomentoval	okomentovat	k5eAaPmAgMnS
výpravu	výprava	k1gFnSc4
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
Vždyť	vždyť	k9
Bůh	bůh	k1gMnSc1
pokořuje	pokořovat	k5eAaImIp3nS
všechno	všechen	k3xTgNnSc4
pyšné	pyšný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
svrchuřečení	svrchuřečený	k2eAgMnPc1d1
králové	král	k1gMnPc1
vydali	vydat	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
takovou	takový	k3xDgFnSc4
cestu	cesta	k1gFnSc4
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
manželkami	manželka	k1gFnPc7
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
pánové	pán	k1gMnPc1
vyhledávali	vyhledávat	k5eAaImAgMnP
styk	styk	k1gInSc4
s	s	k7c7
lehkými	lehký	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
tam	tam	k6eAd1
vznikaly	vznikat	k5eAaImAgInP
přemnohé	přemnohý	k2eAgInPc1d1
neřády	neřády	k?
Bohu	bůh	k1gMnSc3
ohavné	ohavný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehodí	hodit	k5eNaPmIp3nS
se	se	k3xPyFc4
dobře	dobře	k6eAd1
k	k	k7c3
sobě	se	k3xPyFc3
a	a	k8xC
nesnášejí	snášet	k5eNaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
jednom	jeden	k4xCgNnSc6
místě	místo	k1gNnSc6
válečné	válečný	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
a	a	k8xC
obcování	obcování	k1gNnSc2
s	s	k7c7
nevěstkami	nevěstka	k1gFnPc7
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Jarloch	Jarloch	k1gInSc1
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
Bernard	Bernard	k1gMnSc1
zkoušel	zkoušet	k5eAaImAgMnS
později	pozdě	k6eAd2
propagovat	propagovat	k5eAaImF
novou	nový	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
Byzantské	byzantský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
,	,	kIx,
snažil	snažit	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
očistit	očistit	k5eAaPmF
od	od	k7c2
zodpovědnosti	zodpovědnost	k1gFnSc2
za	za	k7c4
kruciátu	kruciáta	k1gFnSc4
předchozí	předchozí	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1152	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výprava	výprava	k1gFnSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
přinesla	přinést	k5eAaPmAgFnS
smíšené	smíšený	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Sasové	Sas	k1gMnPc1
udrželi	udržet	k5eAaPmAgMnP
dobyté	dobytý	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
v	v	k7c6
zemi	zem	k1gFnSc6
Vagrů	Vagr	k1gInPc2
a	a	k8xC
v	v	k7c6
Polabí	Polabí	k1gNnSc6
<g/>
,	,	kIx,
pohanští	pohanský	k2eAgMnPc1d1
Obodrité	Obodrita	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
zpět	zpět	k6eAd1
pod	pod	k7c4
svou	svůj	k3xOyFgFnSc4
kontrolu	kontrola	k1gFnSc4
území	území	k1gNnSc4
východně	východně	k6eAd1
od	od	k7c2
Lübecku	Lübeck	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasům	Sas	k1gMnPc3
se	se	k3xPyFc4
také	také	k9
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
od	od	k7c2
knížete	kníže	k1gMnSc2
Niklota	Niklot	k1gMnSc2
tribut	tribut	k1gInSc1
a	a	k8xC
osvobodit	osvobodit	k5eAaPmF
některé	některý	k3yIgMnPc4
dánské	dánský	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
roztříštění	roztříštěný	k2eAgMnPc1d1
křesťanští	křesťanský	k2eAgMnPc1d1
vojevůdci	vojevůdce	k1gMnPc1
na	na	k7c6
sebe	sebe	k3xPyFc4
pohlíželi	pohlížet	k5eAaImAgMnP
s	s	k7c7
nedůvěrou	nedůvěra	k1gFnSc7
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
obviňovali	obviňovat	k5eAaImAgMnP
ze	z	k7c2
zmaření	zmaření	k1gNnSc2
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
křižácká	křižácký	k2eAgFnSc1d1
vítězství	vítězství	k1gNnSc1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
byla	být	k5eAaImAgFnS
vybojována	vybojovat	k5eAaPmNgFnS
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
o	o	k7c4
dobytí	dobytí	k1gNnSc4
Lisabonu	Lisabon	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
se	se	k3xPyFc4
nahlíží	nahlížet	k5eAaImIp3nP
jako	jako	k9
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
bitev	bitva	k1gFnPc2
reconquisty	reconquista	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
završena	završit	k5eAaPmNgFnS
pádem	pád	k1gInSc7
Granady	Granada	k1gFnSc2
roku	rok	k1gInSc2
1492	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Fons	Fonsa	k1gFnPc2
Muratu	Murat	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
Raimond	Raimond	k1gInSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
<g/>
,	,	kIx,
passage	passagat	k5eAaPmIp3nS
outremer	outremer	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Colombe	Colomb	k1gInSc5
a	a	k8xC
S.	S.	kA
Momerota	Momerota	k1gFnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
vydali	vydat	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
pouť	pouť	k1gFnSc4
za	za	k7c2
nešťastných	šťastný	k2eNgFnPc2d1
okolností	okolnost	k1gFnPc2
a	a	k8xC
pod	pod	k7c7
zlověstnými	zlověstný	k2eAgNnPc7d1
znameními	znamení	k1gNnPc7
<g/>
...	...	k?
Jen	jen	k9
zhoršili	zhoršit	k5eAaPmAgMnP
situaci	situace	k1gFnSc4
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
měli	mít	k5eAaImAgMnP
přinést	přinést	k5eAaPmF
prospěch	prospěch	k1gInSc4
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Tyru	Tyrus	k1gInSc2
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
východě	východ	k1gInSc6
měla	mít	k5eAaImAgFnS
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
nepříznivé	příznivý	k2eNgInPc4d1
dlouhodobé	dlouhodobý	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Núr	Núr	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
využil	využít	k5eAaPmAgMnS
křižáckého	křižácký	k2eAgInSc2d1
neúspěchu	neúspěch	k1gInSc2
a	a	k8xC
napadl	napadnout	k5eAaPmAgMnS
Antiochijské	antiochijský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raimond	Raimond	k1gInSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Fons	Fonsa	k1gFnPc2
Muratu	Murat	k1gInSc2
poražen	poražen	k2eAgMnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
hlavu	hlava	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
Núr	Núr	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
poslat	poslat	k5eAaPmF
bagdádskému	bagdádský	k2eAgMnSc3d1
chalífovi	chalífa	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Balduinovi	Balduin	k1gMnSc3
III	III	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zachránit	zachránit	k5eAaPmF
samotnou	samotný	k2eAgFnSc4d1
Antiochii	Antiochie	k1gFnSc4
a	a	k8xC
vyjednat	vyjednat	k5eAaPmF
s	s	k7c7
Núr	Núr	k1gMnSc1
ad-Dínem	ad-Dín	k1gMnSc7
příměří	příměří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
muslimové	muslim	k1gMnPc1
zajali	zajmout	k5eAaPmAgMnP
i	i	k8xC
Joscelina	Joscelina	k1gFnSc1
z	z	k7c2
Edessy	Edessa	k1gFnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
Núr	Núr	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
oslepit	oslepit	k5eAaPmF
a	a	k8xC
uvěznit	uvěznit	k5eAaPmF
v	v	k7c6
Aleppu	Alepp	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
o	o	k7c4
devět	devět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Beatrice	Beatrice	k1gFnSc2
zbytek	zbytek	k1gInSc1
křižáky	křižák	k1gInPc1
ovládaných	ovládaný	k2eAgNnPc2d1
měst	město	k1gNnPc2
v	v	k7c6
Edesském	Edesský	k2eAgNnSc6d1
hrabství	hrabství	k1gNnSc6
prodala	prodat	k5eAaPmAgFnS
Byzantincům	Byzantinec	k1gMnPc3
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Hrabství	hrabství	k1gNnSc4
Edessa	Edess	k1gMnSc2
přestalo	přestat	k5eAaPmAgNnS
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Damašek	Damašek	k1gInSc1
již	již	k6eAd1
s	s	k7c7
Jeruzalémským	jeruzalémský	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
staré	staré	k1gNnSc4
spojenectví	spojenectví	k1gNnSc2
neuzavřel	uzavřít	k5eNaPmAgMnS
a	a	k8xC
roku	rok	k1gInSc2
1154	#num#	k4
se	se	k3xPyFc4
města	město	k1gNnSc2
zmocnil	zmocnit	k5eAaPmAgMnS
Núr	Núr	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
<g/>
;	;	kIx,
muslimové	muslim	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
křižákům	křižák	k1gInPc3
opět	opět	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
jednotnější	jednotný	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balduin	Balduin	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1153	#num#	k4
po	po	k7c6
dlouhém	dlouhý	k2eAgInSc6d1
a	a	k8xC
vyčerpávajícím	vyčerpávající	k2eAgInSc6d1
boji	boj	k1gInSc6
dobyl	dobýt	k5eAaPmAgInS
od	od	k7c2
Egypťanů	Egypťan	k1gMnPc2
Askalon	Askalon	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
do	do	k7c2
konfliktu	konflikt	k1gInSc2
zatáhl	zatáhnout	k5eAaPmAgInS
i	i	k9
egyptské	egyptský	k2eAgInPc4d1
Fátimovce	Fátimovec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémské	jeruzalémský	k2eAgInPc1d1
království	království	k1gNnSc4
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
zaměřilo	zaměřit	k5eAaPmAgNnS
svou	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
na	na	k7c4
Egypt	Egypt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Amaury	Amaura	k1gFnSc2
I.	I.	kA
sblížil	sblížit	k5eAaPmAgMnS
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1
království	království	k1gNnSc4
s	s	k7c7
Byzancí	Byzanc	k1gFnSc7
a	a	k8xC
s	s	k7c7
byzantskou	byzantský	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
vedl	vést	k5eAaImAgMnS
pět	pět	k4xCc4
vojenských	vojenský	k2eAgNnPc2d1
tažení	tažení	k1gNnPc2
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
však	však	k9
nepřinesly	přinést	k5eNaPmAgFnP
žádný	žádný	k3yNgInSc4
zisk	zisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1171	#num#	k4
se	se	k3xPyFc4
Saladin	Saladin	k2eAgMnSc1d1
<g/>
,	,	kIx,
synovec	synovec	k1gMnSc1
Núr	Núr	k1gFnSc4
ad-Dínova	ad-Dínův	k2eAgMnSc2d1
vojevůdce	vojevůdce	k1gMnSc2
Šírkúha	Šírkúh	k1gMnSc2
se	se	k3xPyFc4
prohlásil	prohlásit	k5eAaPmAgMnS
egyptským	egyptský	k2eAgMnSc7d1
sultánem	sultán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
sjednotil	sjednotit	k5eAaPmAgInS
Egypt	Egypt	k1gInSc1
a	a	k8xC
Sýrii	Sýrie	k1gFnSc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byly	být	k5eAaImAgFnP
křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
obklopené	obklopený	k2eAgNnSc1d1
jednotným	jednotný	k2eAgNnSc7d1
muslimským	muslimský	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1187	#num#	k4
Saladin	Saladin	k2eAgInSc4d1
křižáky	křižák	k1gInPc7
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hattínu	Hattín	k1gInSc2
a	a	k8xC
o	o	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
kapituloval	kapitulovat	k5eAaBmAgInS
i	i	k9
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
na	na	k7c4
záchranu	záchrana	k1gFnSc4
křižáckého	křižácký	k2eAgNnSc2d1
panství	panství	k1gNnSc2
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
vypravila	vypravit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Second	Seconda	k1gFnPc2
Crusade	Crusad	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
NORWICH	NORWICH	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Julius	Julius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantium	Byzantium	k1gNnSc1
:	:	kIx,
The	The	k1gFnSc1
Decline	Declin	k1gInSc5
and	and	k?
Fall	Falla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Alfred	Alfred	k1gMnSc1
A.	A.	kA
Knopf	Knopf	k1gMnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
679416501	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
94	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
„	„	k?
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Norwich	Norwich	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Norwich	Norwicha	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
95	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Levantě	Levanta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
109	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Hrochová	Hrochová	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
109	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Bridge	Bridge	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
DURANT	DURANT	kA
<g/>
,	,	kIx,
Will	Will	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Age	Age	k1gMnSc1
of	of	k?
Faith	Faith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Simon	Simon	k1gMnSc1
and	and	k?
Schuster	Schuster	k1gMnSc1
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
594	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Durant	Durant	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
111	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
110	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
110	#num#	k4
<g/>
–	–	k?
<g/>
111.1	111.1	k4
2	#num#	k4
3	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
112.1	112.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
111	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BUNSON	BUNSON	kA
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
<g/>
;	;	kIx,
AUMANN	AUMANN	kA
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
<g/>
;	;	kIx,
BUNSON	BUNSON	kA
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
<g/>
;	;	kIx,
BUNSON	BUNSON	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Our	Our	k1gFnSc1
Sunday	Sundaa	k1gFnSc2
Visitor	Visitor	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Saints	Saintsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huntington	Huntington	k1gInSc1
<g/>
:	:	kIx,
Our	Our	k1gMnSc1
Sunday	Sundaa	k1gFnSc2
Visitor	Visitor	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
879735880	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
130	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
DUGGAN	DUGGAN	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
93	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Duggan	Duggan	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
112	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
112	#num#	k4
<g/>
–	–	k?
<g/>
113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MAYER	Mayer	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitoly	Kapitol	k1gInPc7
z	z	k7c2
židovských	židovský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
:	:	kIx,
Od	od	k7c2
starověku	starověk	k1gInSc2
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústřední	ústřední	k2eAgNnSc1d1
církevní	církevní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Evropské	evropský	k2eAgNnSc4d1
židovstvo	židovstvo	k1gNnSc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
do	do	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
187	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
94	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Durant	Durant	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
391	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
112	#num#	k4
<g/>
–	–	k?
<g/>
113.1	113.1	k4
2	#num#	k4
3	#num#	k4
RILEY-SMITH	RILEY-SMITH	k1gFnPc2
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Facts	Facts	k1gInSc1
on	on	k3xPp3gInSc1
File	File	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
816021864	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
48	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Riley-Smith	Riley-Smith	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Helmold	Helmold	k1gInSc1
z	z	k7c2
Bosau	Bosaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
298	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
786	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
113	#num#	k4
<g/>
–	–	k?
<g/>
114	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Kronika	kronika	k1gFnSc1
Slovanů	Slovan	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
113.1	113.1	k4
2	#num#	k4
3	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
113.1	113.1	k4
2	#num#	k4
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc2
Kingdom	Kingdom	k1gInSc1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
and	and	k?
the	the	k?
Frankish	Frankish	k1gMnSc1
East	East	k1gMnSc1
<g/>
,	,	kIx,
1100	#num#	k4
<g/>
-	-	kIx~
<g/>
1187	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
258	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dálen	dálen	k2eAgMnSc1d1
jen	jen	k9
Runciman	Runciman	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
115.1	115.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
95	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dobytí	dobytí	k1gNnSc1
Lisabonu	Lisabon	k1gInSc2
a	a	k8xC
reconquista	reconquista	k1gMnSc1
Portugalska	Portugalsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
967	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Riley-Smith	Riley-Smitha	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
126	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DAVIES	DAVIES	kA
<g/>
,	,	kIx,
Norman	Norman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
:	:	kIx,
dějiny	dějiny	k1gFnPc4
jednoho	jeden	k4xCgInSc2
kontinentu	kontinent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Prostor	prostor	k1gInSc1
;	;	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
1365	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7260	#num#	k4
<g/>
-	-	kIx~
<g/>
138	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
379	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HERRMANN	HERRMANN	kA
<g/>
,	,	kIx,
Joachim	Joachim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Slawen	Slawna	k1gFnPc2
in	in	k?
Deutschland	Deutschlanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlín	Berlín	k1gInSc1
<g/>
:	:	kIx,
Akademie-Verlag	Akademie-Verlag	k1gMnSc1
GmbH	GmbH	k1gMnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
530	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Herrmann	Herrmann	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Herrmann	Herrmann	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
328	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kronika	kronika	k1gFnSc1
Slovanů	Slovan	k1gInPc2
<g/>
,	,	kIx,
s.	s.	k?
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CHRISTIANSEN	CHRISTIANSEN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Northern	Northern	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
26653	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
287	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Christiansen	Christiansen	k2eAgMnSc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
14	#num#	k4
<g/>
-	-	kIx~
<g/>
409	#num#	k4
<g/>
-	-	kIx~
<g/>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
89	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Christiansen	Christiansen	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
54	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BARRACLOUGH	BARRACLOUGH	kA
<g/>
,	,	kIx,
Geoffrey	Geoffrey	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Origins	Originsa	k1gFnPc2
of	of	k?
Modern	Moderna	k1gFnPc2
Germany	German	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.	W.	kA
W.	W.	kA
Norton	Norton	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
30153	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
263	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
257	#num#	k4
<g/>
–	–	k?
<g/>
259.1	259.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
96	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
91	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Kroniky	kronika	k1gFnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
113	#num#	k4
<g/>
–	–	k?
<g/>
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
115	#num#	k4
<g/>
–	–	k?
<g/>
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kroniky	kronika	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
971	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
259	#num#	k4
<g/>
–	–	k?
<g/>
267.1	267.1	k4
2	#num#	k4
3	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
114.1	114.1	k4
2	#num#	k4
Riley-Smith	Riley-Smitha	k1gFnPc2
str	str	kA
<g/>
.	.	kIx.
50.1	50.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
97	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOSI	kos	k1gMnPc1
<g/>
,	,	kIx,
Miha	Miha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Age	Age	k1gMnSc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
in	in	k?
the	the	k?
South-East	South-East	k1gInSc1
of	of	k?
the	the	k?
Empire	empir	k1gInSc5
(	(	kIx(
<g/>
Between	Between	k2eAgInSc4d1
the	the	k?
Alps	Alps	k1gInSc4
and	and	k?
the	the	k?
Adriatic	Adriatice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
HUNYADI	HUNYADI	kA
<g/>
,	,	kIx,
Zsolt	Zsolt	k1gMnSc1
<g/>
;	;	kIx,
LASZLOVSZKY	LASZLOVSZKY	kA
<g/>
,	,	kIx,
József	József	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
crusades	crusades	k1gMnSc1
and	and	k?
the	the	k?
military	militara	k1gFnSc2
orders	ordersa	k1gFnPc2
expanding	expanding	k1gInSc1
the	the	k?
frontiers	frontiers	k1gInSc1
of	of	k?
medieval	medieval	k1gInSc1
latin	latina	k1gFnPc2
christianity	christianita	k1gFnSc2
:	:	kIx,
in	in	k?
memoriam	memoriam	k1gInSc1
Sir	sir	k1gMnSc1
Steven	Stevna	k1gFnPc2
Runciman	Runciman	k1gMnSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budapest	Budapest	k1gInSc1
<g/>
:	:	kIx,
Central	Central	k1gFnPc1
european	european	k1gInSc4
university	universita	k1gFnSc2
<g/>
,	,	kIx,
Department	department	k1gInSc1
of	of	k?
medieval	medieval	k1gInSc1
studies	studiesa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
963	#num#	k4
<g/>
-	-	kIx~
<g/>
9241	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
128	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
97	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1148	#num#	k4
:	:	kIx,
pohroma	pohroma	k1gFnSc1
před	před	k7c7
branami	brána	k1gFnPc7
Damašku	Damašek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
3413	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1148	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
98	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
268	#num#	k4
<g/>
–	–	k?
<g/>
269	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kroniky	kronika	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
931	#num#	k4
2	#num#	k4
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
5	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
117	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1148	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
269	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
270	#num#	k4
<g/>
–	–	k?
<g/>
271	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Duggan	Duggan	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
99.1	99.1	k4
2	#num#	k4
3	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
118	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
99	#num#	k4
<g/>
–	–	k?
<g/>
100.1	100.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
100	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
272	#num#	k4
<g/>
–	–	k?
<g/>
273	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
116	#num#	k4
<g/>
–	–	k?
<g/>
117.1	117.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
121.1	121.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
117.1	117.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
101.1	101.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
BRUNDAGE	BRUNDAGE	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
:	:	kIx,
A	a	k8xC
Documentary	Documentara	k1gFnPc1
History	Histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milwaukee	Milwaukee	k1gNnSc1
<g/>
:	:	kIx,
Marquette	Marquett	k1gMnSc5
University	universita	k1gFnPc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
115	#num#	k4
<g/>
–	–	k?
<g/>
121	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DUBY	dub	k1gInPc7
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytíř	Rytíř	k1gMnSc1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
a	a	k8xC
kněz	kněz	k1gMnSc1
:	:	kIx,
manželství	manželství	k1gNnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
době	doba	k1gFnSc6
feudalismu	feudalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Garamond	garamond	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
238	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86379	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
158	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Rytíř	Rytíř	k1gMnSc1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
a	a	k8xC
kněz	kněz	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Riley-Smith	Riley-Smitha	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
50.1	50.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc4
a	a	k8xC
kříž	kříž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1289	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
208	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Kovařík	Kovařík	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
228	#num#	k4
<g/>
–	–	k?
<g/>
229	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
122.1	122.1	k4
2	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
123	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JARLOCH	JARLOCH	kA
<g/>
;	;	kIx,
BELLOVACENSIS	BELLOVACENSIS	kA
<g/>
,	,	kIx,
Vincentius	Vincentius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letopis	letopis	k1gInSc1
Vincenciův	Vincenciův	k2eAgInSc1d1
a	a	k8xC
Jarlochův	Jarlochův	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SNKLHU	SNKLHU	kA
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
61	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
232	#num#	k4
<g/>
–	–	k?
<g/>
234	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Runciman	Runciman	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
277	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1148	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
81	#num#	k4
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
208	#num#	k4
<g/>
–	–	k?
<g/>
209	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
146	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
209	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
160	#num#	k4
<g/>
–	–	k?
<g/>
161	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Lisabonu	Lisabon	k1gInSc2
a	a	k8xC
reconquista	reconquista	k1gMnSc1
Portugalska	Portugalsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Simona	Simona	k1gFnSc1
Binková	Binková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Havlíková	Havlíková	k1gFnSc1
<g/>
;	;	kIx,
překlad	překlad	k1gInSc1
Pavel	Pavel	k1gMnSc1
Zavadil	Zavadil	k1gMnSc1
<g/>
,	,	kIx,
Magdaléna	Magdaléna	k1gFnSc1
Moravová	Moravový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Memoria	Memorium	k1gNnSc2
medii	medium	k1gNnPc7
aevi	aevi	k1gNnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
967	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BALDWIN	BALDWIN	kA
<g/>
,	,	kIx,
Marshall	Marshall	k1gInSc1
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
first	first	k1gMnSc1
hundred	hundred	k1gMnSc1
years	years	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
707	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
228	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DUGGAN	DUGGAN	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
214	#num#	k4
s.	s.	k?
</s>
<s>
Z	z	k7c2
FREISINGU	FREISINGU	kA
<g/>
,	,	kIx,
Ota	Ota	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
aneb	aneb	k?
O	o	k7c6
dvou	dva	k4xCgFnPc6
obcích	obec	k1gFnPc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Argo	Argo	k1gMnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
460	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
2291	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GABRIELI	Gabriel	k1gMnSc5
<g/>
,	,	kIx,
Francesco	Francesco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
očima	oko	k1gNnPc7
arabských	arabský	k2eAgInPc2d1
kronikářů	kronikář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
333	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
;	;	kIx,
HROCH	Hroch	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
621	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
</s>
<s>
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1148	#num#	k4
:	:	kIx,
pohroma	pohroma	k1gFnSc1
před	před	k7c7
branami	brána	k1gFnPc7
Damašku	Damašek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
3413	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PHILLIPS	PHILLIPS	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
second	second	k1gMnSc1
crusade	crusást	k5eAaPmIp3nS
:	:	kIx,
extending	extending	k1gInSc1
the	the	k?
frontiers	frontiers	k1gInSc1
of	of	k?
Christendom	Christendom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
Haven	Havna	k1gFnPc2
<g/>
:	:	kIx,
Yale	Yale	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
364	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
300	#num#	k4
<g/>
-	-	kIx~
<g/>
11274	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
REGAN	REGAN	kA
<g/>
,	,	kIx,
Geoffrey	Geoffrey	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
:	:	kIx,
padesát	padesát	k4xCc1
dvě	dva	k4xCgFnPc1
bitvy	bitva	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
změnily	změnit	k5eAaPmAgFnP
svět	svět	k1gInSc4
:	:	kIx,
od	od	k7c2
Salaminy	Salamin	k2eAgFnSc2d1
k	k	k7c3
válce	válka	k1gFnSc3
v	v	k7c6
Perském	perský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
263	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
824	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
crusades	crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gFnSc1
Kingdom	Kingdom	k1gInSc1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
and	and	k?
the	the	k?
Frankish	Frankish	k1gMnSc1
East	East	k1gMnSc1
:	:	kIx,
1100	#num#	k4
<g/>
-	-	kIx~
<g/>
1187	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
536	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
13704	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SMAIL	SMAIL	kA
<g/>
,	,	kIx,
Raymond	Raymond	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crusading	Crusading	k1gInSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
1097	#num#	k4
<g/>
-	-	kIx~
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Barnes	Barnes	k1gInSc1
&	&	k?
Noble	Noble	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56619	#num#	k4
<g/>
-	-	kIx~
<g/>
769	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Křižácká	křižácký	k2eAgNnPc1d1
tažení	tažení	k1gNnPc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Výzva	výzva	k1gFnSc1
k	k	k7c3
účasti	účast	k1gFnSc3
Čechů	Čech	k1gMnPc2
na	na	k7c6
druhé	druhý	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
</s>
<s>
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc5
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gInPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgMnPc3d1
Turkům	turek	k1gInPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
|	|	kIx~
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
545371	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4139117-2	4139117-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034382	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034382	#num#	k4
</s>
