<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
postupně	postupně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozkolu	rozkol	k1gInSc3
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
demokracii	demokracie	k1gFnSc6
a	a	k8xC
ustavení	ustavení	k1gNnSc6
dvou	dva	k4xCgFnPc2
paralelních	paralelní	k2eAgFnPc2d1
sociálně	sociálně	k6eAd1
demokratických	demokratický	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
levicové	levicový	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
ustavilo	ustavit	k5eAaPmAgNnS
v	v	k7c6
květnu	květen	k1gInSc6
1921	#num#	k4
jako	jako	k8xS,k8xC
samostatná	samostatný	k2eAgFnSc1d1
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
potom	potom	k6eAd1
na	na	k7c6
slučovacím	slučovací	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
na	na	k7c4
podzim	podzim	k1gInSc4
1921	#num#	k4
integrovala	integrovat	k5eAaBmAgFnS
i	i	k9
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jiných	jiný	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>