<s>
Dějiny	dějiny	k1gFnPc1
českých	český	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
českých	český	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
reflektují	reflektovat	k5eAaImIp3nP
postupný	postupný	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
politických	politický	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
reprezentují	reprezentovat	k5eAaImIp3nP
etnicky	etnicky	k6eAd1
převážně	převážně	k6eAd1
českou	český	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
do	do	k7c2
zániku	zánik	k1gInSc2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
</s>
<s>
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
během	během	k7c2
revoluce	revoluce	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
</s>
<s>
Parlamentní	parlamentní	k2eAgInSc1d1
život	život	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
rozvinul	rozvinout	k5eAaPmAgInS
během	během	k7c2
revolučního	revoluční	k2eAgInSc2d1
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
přechodu	přechod	k1gInSc2
na	na	k7c4
ústavní	ústavní	k2eAgFnSc4d1
formu	forma	k1gFnSc4
vlády	vláda	k1gFnSc2
byl	být	k5eAaImAgInS
utvořen	utvořen	k2eAgInSc1d1
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
coby	coby	k?
celostátní	celostátní	k2eAgInSc1d1
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
a	a	k8xC
plánovalo	plánovat	k5eAaImAgNnS
se	se	k3xPyFc4
též	též	k9
svolání	svolání	k1gNnSc1
Českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
proběhly	proběhnout	k5eAaPmAgFnP
sice	sice	k8xC
zemské	zemský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
sněm	sněm	k1gInSc1
nebyl	být	k5eNaImAgInS
po	po	k7c6
nepokojích	nepokoj	k1gInPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
nikdy	nikdy	k6eAd1
svolán	svolat	k5eAaPmNgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
ovšem	ovšem	k9
byl	být	k5eAaImAgInS
konstituován	konstituován	k2eAgInSc1d1
Moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
i	i	k8xC
Slezský	slezský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
respektive	respektive	k9
takzvaný	takzvaný	k2eAgInSc1d1
Slezský	slezský	k2eAgInSc1d1
konvent	konvent	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
moderním	moderní	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
nicméně	nicméně	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
ještě	ještě	k6eAd1
neexistoval	existovat	k5eNaImAgInS
<g/>
,	,	kIx,
celá	celý	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
tvořila	tvořit	k5eAaImAgFnS
širokou	široký	k2eAgFnSc4d1
Národní	národní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
předáky	předák	k1gMnPc7
byl	být	k5eAaImAgMnS
František	František	k1gMnSc1
Palacký	Palacký	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
koexistovaly	koexistovat	k5eAaImAgFnP
osobnosti	osobnost	k1gFnPc1
demokratické	demokratický	k2eAgFnPc1d1
<g/>
,	,	kIx,
liberální	liberální	k2eAgFnSc1d1
i	i	k8xC
konzervativní	konzervativní	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistovalo	existovat	k5eNaImAgNnS
formální	formální	k2eAgNnSc4d1
stranické	stranický	k2eAgNnSc4d1
členství	členství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volná	volný	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
navíc	navíc	k6eAd1
zanikla	zaniknout	k5eAaPmAgFnS
po	po	k7c6
potlačení	potlačení	k1gNnSc6
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
neoabsolutismu	neoabsolutismus	k1gInSc2
byla	být	k5eAaImAgFnS
aktivní	aktivní	k2eAgFnSc1d1
jen	jen	k9
volná	volný	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
konzervativně	konzervativně	k6eAd1
orientovaných	orientovaný	k2eAgMnPc2d1
vlastenců	vlastenec	k1gMnPc2
jako	jako	k9
Václav	Václav	k1gMnSc1
Vladivoj	Vladivoj	k1gInSc4
Tomek	Tomek	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Erben	Erben	k1gMnSc1
nebo	nebo	k8xC
Leopold	Leopold	k1gMnSc1
Lev	Lev	k1gMnSc1
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
důsledně	důsledně	k6eAd1
loajální	loajální	k2eAgInPc4d1
k	k	k7c3
monarchii	monarchie	k1gFnSc3
<g/>
,	,	kIx,
odmítali	odmítat	k5eAaImAgMnP
demokratický	demokratický	k2eAgInSc4d1
radikalismus	radikalismus	k1gInSc4
roku	rok	k1gInSc2
1848	#num#	k4
a	a	k8xC
podporovali	podporovat	k5eAaImAgMnP
rozvoj	rozvoj	k1gInSc4
národního	národní	k2eAgInSc2d1
svérázu	svéráz	k1gInSc2
české	český	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obnovení	obnovení	k1gNnSc1
parlamentarismu	parlamentarismus	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1861	#num#	k4
</s>
<s>
Říjnový	říjnový	k2eAgInSc1d1
diplom	diplom	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1860	#num#	k4
a	a	k8xC
následná	následný	k2eAgFnSc1d1
únorová	únorový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
z	z	k7c2
počátku	počátek	k1gInSc2
roku	rok	k1gInSc2
1861	#num#	k4
obnovily	obnovit	k5eAaPmAgInP
parlamentní	parlamentní	k2eAgInPc1d1
a	a	k8xC
ústavní	ústavní	k2eAgInPc1d1
systém	systém	k1gInSc1
vlády	vláda	k1gFnSc2
v	v	k7c6
Rakouském	rakouský	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
definována	definovat	k5eAaBmNgFnS
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
coby	coby	k?
nejvyšší	vysoký	k2eAgInSc1d3
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
volený	volený	k2eAgInSc1d1
nepřímo	přímo	k6eNd1
jako	jako	k8xS,k8xC
soubor	soubor	k1gInSc4
delegátů	delegát	k1gMnPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
1861	#num#	k4
proto	proto	k8xC
proběhly	proběhnout	k5eAaPmAgFnP
volby	volba	k1gFnPc1
do	do	k7c2
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemské	zemský	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
utvořily	utvořit	k5eAaPmAgFnP
zemské	zemský	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
reprezentace	reprezentace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
českou	český	k2eAgFnSc7d1
populací	populace	k1gFnSc7
se	se	k3xPyFc4
opět	opět	k6eAd1
zformovala	zformovat	k5eAaPmAgFnS
široká	široký	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
volební	volební	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
vedli	vést	k5eAaImAgMnP
František	František	k1gMnSc1
Palacký	Palacký	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogicky	analogicky	k6eAd1
německojazyčná	německojazyčný	k2eAgFnSc1d1
populace	populace	k1gFnSc1
byla	být	k5eAaImAgFnS
zastoupena	zastoupit	k5eAaPmNgFnS
liberální	liberální	k2eAgFnSc7d1
Ústavní	ústavní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemské	zemský	k2eAgInPc1d1
sněmy	sněm	k1gInPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byly	být	k5eAaImAgFnP
voleny	volit	k5eAaImNgFnP
na	na	k7c6
základě	základ	k1gInSc6
kuriového	kuriový	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
oddělenou	oddělený	k2eAgFnSc4d1
volební	volební	k2eAgFnSc4d1
kurii	kurie	k1gFnSc4
tak	tak	k6eAd1
tvořili	tvořit	k5eAaImAgMnP
i	i	k9
velcí	velký	k2eAgMnPc1d1
pozemkoví	pozemkový	k2eAgMnPc1d1
vlastníci	vlastník	k1gMnPc1
(	(	kIx(
<g/>
velkostatkářská	velkostatkářský	k2eAgFnSc1d1
kurie	kurie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
sdružovali	sdružovat	k5eAaImAgMnP
v	v	k7c6
samostatných	samostatný	k2eAgInPc6d1
politických	politický	k2eAgInPc6d1
táborech	tábor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlechta	šlechta	k1gFnSc1
sympatizující	sympatizující	k2eAgFnSc2d1
s	s	k7c7
českým	český	k2eAgInSc7d1
federalistickým	federalistický	k2eAgInSc7d1
programem	program	k1gInSc7
a	a	k8xC
identifikující	identifikující	k2eAgNnSc1d1
se	se	k3xPyFc4
s	s	k7c7
českým	český	k2eAgNnSc7d1
národním	národní	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
utvořila	utvořit	k5eAaPmAgFnS
Stranu	strana	k1gFnSc4
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
(	(	kIx(
<g/>
provídeňsky	provídeňsky	k6eAd1
<g/>
,	,	kIx,
německorakousky	německorakousky	k6eAd1
smýšlející	smýšlející	k2eAgMnPc1d1
velkostatkáři	velkostatkář	k1gMnPc1
z	z	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
byli	být	k5eAaImAgMnP
členy	člen	k1gInPc4
Strany	strana	k1gFnSc2
ústavověrného	ústavověrný	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
stranou	stranou	k6eAd1
honoračního	honorační	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
masového	masový	k2eAgNnSc2d1
členství	členství	k1gNnSc2
<g/>
,	,	kIx,
sestávající	sestávající	k2eAgMnSc1d1
z	z	k7c2
jednotlivých	jednotlivý	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
a	a	k8xC
volné	volný	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
aktivistů	aktivista	k1gMnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
formálnějších	formální	k2eAgFnPc2d2
rozhodovacích	rozhodovací	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezenému	omezený	k2eAgNnSc3d1
volebnímu	volební	k2eAgNnSc3d1
právu	právo	k1gNnSc3
(	(	kIx(
<g/>
volební	volební	k2eAgInSc1d1
cenzus	cenzus	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
obracela	obracet	k5eAaImAgFnS
na	na	k7c4
vzdělanější	vzdělaný	k2eAgFnPc4d2
<g/>
,	,	kIx,
majetnější	majetný	k2eAgFnPc4d2
vrstvy	vrstva	k1gFnPc4
a	a	k8xC
zahrnovala	zahrnovat	k5eAaImAgFnS
opět	opět	k6eAd1
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
široké	široký	k2eAgNnSc4d1
názorové	názorový	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
se	se	k3xPyFc4
již	již	k6eAd1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vyhraňoval	vyhraňovat	k5eAaImAgInS
pozdější	pozdní	k2eAgInSc1d2
mladočeský	mladočeský	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
více	hodně	k6eAd2
demokraticky	demokraticky	k6eAd1
orientovaný	orientovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominance	dominance	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
fakticky	fakticky	k6eAd1
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
let	léto	k1gNnPc2
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
byla	být	k5eAaImAgFnS
jejím	její	k3xOp3gMnSc7
trvalým	trvalý	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nástup	nástup	k1gInSc1
mladočechů	mladočech	k1gMnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
oficiálně	oficiálně	k6eAd1
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
svobodomyslná	svobodomyslný	k2eAgFnSc1d1
(	(	kIx(
<g/>
mladočeská	mladočeský	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
na	na	k7c4
Český	český	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
roku	rok	k1gInSc2
1874	#num#	k4
poprvé	poprvé	k6eAd1
vystoupila	vystoupit	k5eAaPmAgFnS
se	s	k7c7
samostatnou	samostatný	k2eAgFnSc7d1
kandidaturou	kandidatura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definovala	definovat	k5eAaBmAgNnP
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
liberální	liberální	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokroková	pokrokový	k2eAgFnSc1d1
a	a	k8xC
demokratická	demokratický	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
konzervativní	konzervativní	k2eAgFnSc2d1
Národní	národní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
staročeské	staročeský	k2eAgNnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladočeši	mladočech	k1gMnPc1
odmítali	odmítat	k5eAaImAgMnP
politiku	politika	k1gFnSc4
pasivní	pasivní	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
razil	razit	k5eAaImAgMnS
František	František	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Rieger	Rieger	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
bojkot	bojkot	k1gInSc1
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
i	i	k8xC
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
kvůli	kvůli	k7c3
opozici	opozice	k1gFnSc3
proti	proti	k7c3
ústavnímu	ústavní	k2eAgNnSc3d1
směřování	směřování	k1gNnSc3
státu	stát	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
po	po	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1879	#num#	k4
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
utrpěly	utrpět	k5eAaPmAgFnP
porážku	porážka	k1gFnSc4
německorakouské	německorakouský	k2eAgFnSc2d1
liberální	liberální	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
posílila	posílit	k5eAaPmAgFnS
konzervativní	konzervativní	k2eAgFnSc1d1
pravice	pravice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
politici	politik	k1gMnPc1
potom	potom	k6eAd1
utvořili	utvořit	k5eAaPmAgMnP
alianci	aliance	k1gFnSc4
s	s	k7c7
německými	německý	k2eAgMnPc7d1
konzervativci	konzervativec	k1gMnPc7
a	a	k8xC
představiteli	představitel	k1gMnPc7
Poláků	polák	k1gInPc2
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pasivní	pasivní	k2eAgFnSc1d1
rezistence	rezistence	k1gFnSc1
staročechů	staročech	k1gMnPc2
skončila	skončit	k5eAaPmAgFnS
a	a	k8xC
všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
proudy	proud	k1gInPc4
české	český	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
(	(	kIx(
<g/>
staročeši	staročech	k1gMnPc1
<g/>
,	,	kIx,
mladočeši	mladočech	k1gMnPc1
i	i	k8xC
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
spojily	spojit	k5eAaPmAgFnP
do	do	k7c2
jednotného	jednotný	k2eAgInSc2d1
Českého	český	k2eAgInSc2d1
klubu	klub	k1gInSc2
na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
za	za	k7c2
vlády	vláda	k1gFnSc2
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
proto	proto	k8xC
samostatná	samostatný	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
mladočechů	mladočech	k1gMnPc2
ustala	ustat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oživení	oživení	k1gNnSc1
činnosti	činnost	k1gFnSc2
mladočeské	mladočeský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
nastalo	nastat	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
narůstala	narůstat	k5eAaImAgFnS
nespokojenost	nespokojenost	k1gFnSc4
s	s	k7c7
výsledky	výsledek	k1gInPc7
české	český	k2eAgFnSc2d1
účasti	účast	k1gFnSc2
na	na	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladočeši	mladočech	k1gMnPc1
byli	být	k5eAaImAgMnP
kritičtější	kritický	k2eAgNnSc4d2
k	k	k7c3
roli	role	k1gFnSc3
šlechty	šlechta	k1gFnSc2
<g/>
,	,	kIx,
podporovali	podporovat	k5eAaImAgMnP
zásadnější	zásadní	k2eAgFnPc4d2
volební	volební	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
a	a	k8xC
byli	být	k5eAaImAgMnP
důslednější	důsledný	k2eAgMnPc1d2
v	v	k7c6
obhajobě	obhajoba	k1gFnSc6
českých	český	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemské	zemský	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
1889	#num#	k4
už	už	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
ve	v	k7c6
vyostřené	vyostřený	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
konfrontace	konfrontace	k1gFnSc2
mezi	mezi	k7c7
staročechy	staročech	k1gMnPc7
a	a	k8xC
mladočechy	mladočech	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Staročeši	Staročech	k1gMnPc1
sice	sice	k8xC
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
mírnou	mírný	k2eAgFnSc7d1
převahou	převaha	k1gFnSc7
nad	nad	k7c7
mladočeskými	mladočeský	k2eAgMnPc7d1
kandidáty	kandidát	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sněmu	sněm	k1gInSc6
pak	pak	k6eAd1
fungovaly	fungovat	k5eAaImAgInP
dva	dva	k4xCgInPc1
české	český	k2eAgInPc1d1
konkurenční	konkurenční	k2eAgInPc1d1
poslanecké	poslanecký	k2eAgInPc1d1
kluby	klub	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
poprvé	poprvé	k6eAd1
nabylo	nabýt	k5eAaPmAgNnS
rysů	rys	k1gInPc2
systému	systém	k1gInSc2
dvou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
stran	strana	k1gFnPc2
ve	v	k7c6
velkostatkářské	velkostatkářský	k2eAgFnSc6d1
kurii	kurie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkol	rozkol	k1gInSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
vystupňoval	vystupňovat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
kvůli	kvůli	k7c3
takzvaným	takzvaný	k2eAgFnPc3d1
punktacím	punktace	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
pokusem	pokus	k1gInSc7
o	o	k7c4
česko-německý	česko-německý	k2eAgInSc4d1
smír	smír	k1gInSc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1891	#num#	k4
byli	být	k5eAaImAgMnP
staročeši	staročech	k1gMnPc1
téměř	téměř	k6eAd1
vymazáni	vymazat	k5eAaPmNgMnP
z	z	k7c2
politické	politický	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
a	a	k8xC
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
mandátů	mandát	k1gInPc2
získali	získat	k5eAaPmAgMnP
mladočeští	mladočeský	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
politika	politika	k1gFnSc1
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
krátkého	krátký	k2eAgNnSc2d1
období	období	k1gNnSc2
mladočeské	mladočeský	k2eAgFnSc2d1
dominance	dominance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
rozrůzňování	rozrůzňování	k1gNnSc1
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
</s>
<s>
Již	již	k6eAd1
během	během	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
ale	ale	k8xC
české	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
dále	daleko	k6eAd2
rozčlenilo	rozčlenit	k5eAaPmAgNnS
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
utvořilo	utvořit	k5eAaPmAgNnS
několik	několik	k4yIc4
stavovsky	stavovsky	k6eAd1
a	a	k8xC
ideologicky	ideologicky	k6eAd1
definovaných	definovaný	k2eAgInPc2d1
politických	politický	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
i	i	k9
reformy	reforma	k1gFnSc2
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
zůstával	zůstávat	k5eAaImAgInS
bez	bez	k7c2
větších	veliký	k2eAgFnPc2d2
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
celostátní	celostátní	k2eAgFnSc2d1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
procházel	procházet	k5eAaImAgMnS
značnými	značný	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
Taafeho	Taafe	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1882	#num#	k4
zvýšila	zvýšit	k5eAaPmAgFnS
skupinu	skupina	k1gFnSc4
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
o	o	k7c4
maloměstské	maloměstský	k2eAgFnPc4d1
<g/>
,	,	kIx,
malopodnikatelské	malopodnikatelský	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraznější	výrazný	k2eAgFnSc7d2
změnou	změna	k1gFnSc7
byla	být	k5eAaImAgFnS
Badeniho	Badeni	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
roku	rok	k1gInSc2
1896	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
ta	ten	k3xDgFnSc1
zachovávala	zachovávat	k5eAaImAgFnS
kuriální	kuriální	k2eAgInSc4d1
volební	volební	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
dosavadním	dosavadní	k2eAgFnPc3d1
čtyřem	čtyři	k4xCgFnPc3
kuriím	kurie	k1gFnPc3
(	(	kIx(
<g/>
velkostatkářská	velkostatkářský	k2eAgFnSc1d1
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
<g/>
,	,	kIx,
kurie	kurie	k1gFnSc1
obchodních	obchodní	k2eAgFnPc2d1
a	a	k8xC
živnostenských	živnostenský	k2eAgFnPc2d1
komor	komora	k1gFnPc2
a	a	k8xC
kurie	kurie	k1gFnSc2
venkovských	venkovský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
přibyla	přibýt	k5eAaPmAgFnS
kurie	kurie	k1gFnSc1
pátá	pátý	k4xOgFnSc1
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
v	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
původních	původní	k2eAgFnPc6d1
kuriích	kurie	k1gFnPc6
omezoval	omezovat	k5eAaImAgInS
daňově	daňově	k6eAd1
definovaný	definovaný	k2eAgInSc1d1
volební	volební	k2eAgInSc1d1
cenzus	cenzus	k1gInSc1
<g/>
,	,	kIx,
pátá	pátý	k4xOgFnSc1
kurie	kurie	k1gFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
všechny	všechen	k3xTgMnPc4
muže	muž	k1gMnPc4
starší	starý	k2eAgMnPc1d2
24	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Čechách	Čechy	k1gFnPc6
v	v	k7c6
krátkém	krátký	k2eAgInSc6d1
sledu	sled	k1gInSc6
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
nových	nový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
Křesťansko-sociální	křesťansko-sociální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1897	#num#	k4
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
katolická	katolický	k2eAgFnSc1d1
v	v	k7c6
království	království	k1gNnSc6
Českém	český	k2eAgNnSc6d1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1897	#num#	k4
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
národně	národně	k6eAd1
sociální	sociální	k2eAgFnSc1d1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1897	#num#	k4
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
radikálně	radikálně	k6eAd1
pokroková	pokrokový	k2eAgFnSc1d1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1899	#num#	k4
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
státoprávní	státoprávní	k2eAgFnSc2d1
a	a	k8xC
roku	rok	k1gInSc2
1900	#num#	k4
i	i	k8xC
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
realistická	realistický	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1893	#num#	k4
po	po	k7c6
delší	dlouhý	k2eAgFnSc6d2
předchozí	předchozí	k2eAgFnSc6d1
existenci	existence	k1gFnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
celostátní	celostátní	k2eAgFnSc2d1
Sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
dělnické	dělnický	k2eAgFnSc2d1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
oddělila	oddělit	k5eAaPmAgFnS
na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
do	do	k7c2
samostatného	samostatný	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
subjektu	subjekt	k1gInSc2
pro	pro	k7c4
českou	český	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
organizační	organizační	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
Badeniho	Badeniha	k1gFnSc5
volební	volební	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
se	se	k3xPyFc4
navíc	navíc	k6eAd1
již	již	k6eAd1
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1897	#num#	k4
do	do	k7c2
vídeňského	vídeňský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
dostalo	dostat	k5eAaPmAgNnS
několik	několik	k4yIc1
českých	český	k2eAgMnPc2d1
sociálně	sociálně	k6eAd1
demokratických	demokratický	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zásadní	zásadní	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
vznik	vznik	k1gInSc4
samostatné	samostatný	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
agrární	agrární	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
původně	původně	k6eAd1
jako	jako	k8xC,k8xS
Sdružení	sdružení	k1gNnSc1
českých	český	k2eAgMnPc2d1
zemědělců	zemědělec	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
mladočeského	mladočeský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1899	#num#	k4
jako	jako	k8xS,k8xC
zcela	zcela	k6eAd1
samostatný	samostatný	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
subjekt	subjekt	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
Stanislav	Stanislav	k1gMnSc1
Kubr	Kubr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Moravě	Morava	k1gFnSc6
probíhal	probíhat	k5eAaImAgInS
proces	proces	k1gInSc4
diferenciace	diferenciace	k1gFnSc2
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
s	s	k7c7
jistým	jistý	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rivalita	rivalita	k1gFnSc1
mezi	mezi	k7c4
staročechy	staročech	k1gMnPc4
a	a	k8xC
mladočechy	mladočech	k1gMnPc4
tu	tu	k6eAd1
nebyla	být	k5eNaImAgFnS
tak	tak	k6eAd1
výrazná	výrazný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
moravské	moravský	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
staročeské	staročeský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
Moravská	moravský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
vedl	vést	k5eAaImAgMnS
Alois	Alois	k1gMnSc1
Pražák	Pražák	k1gMnSc1
<g/>
,	,	kIx,
tu	tu	k6eAd1
dlouho	dlouho	k6eAd1
působilo	působit	k5eAaImAgNnS
jako	jako	k8xC,k8xS
ústřední	ústřední	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
formace	formace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zde	zde	k6eAd1
vyčlenila	vyčlenit	k5eAaPmAgFnS
mladočesky	mladočesky	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
Lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Adolfa	Adolf	k1gMnSc2
Stránského	Stránský	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1899	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
Moravsko-slezská	moravsko-slezský	k2eAgFnSc1d1
křesťansko-sociální	křesťansko-sociální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
již	již	k6eAd1
předtím	předtím	k6eAd1
roku	rok	k1gInSc2
1896	#num#	k4
Katolická	katolický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
národní	národní	k2eAgFnSc1d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
století	století	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
také	také	k9
(	(	kIx(
<g/>
nepříliš	příliš	k6eNd1
výrazně	výrazně	k6eAd1
<g/>
)	)	kIx)
etablovali	etablovat	k5eAaBmAgMnP
národní	národní	k2eAgMnPc1d1
sociálové	sociál	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
byl	být	k5eAaImAgInS
rozvoj	rozvoj	k1gInSc4
českého	český	k2eAgInSc2d1
stranického	stranický	k2eAgInSc2d1
života	život	k1gInSc2
výrazně	výrazně	k6eAd1
opožděn	opožděn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
nevýrazné	výrazný	k2eNgFnSc3d1
demografické	demografický	k2eAgFnSc3d1
základně	základna	k1gFnSc3
(	(	kIx(
<g/>
Češi	Čech	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
jen	jen	k9
menšinu	menšina	k1gFnSc4
populace	populace	k1gFnSc2
této	tento	k3xDgFnSc2
korunní	korunní	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
nebyli	být	k5eNaImAgMnP
výrazněji	výrazně	k6eAd2
zastoupeni	zastoupit	k5eAaPmNgMnP
ani	ani	k8xC
na	na	k7c6
Slezském	slezský	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
zde	zde	k6eAd1
jednotlivé	jednotlivý	k2eAgInPc1d1
ideologické	ideologický	k2eAgInPc1d1
tábory	tábor	k1gInPc1
vyčlenily	vyčlenit	k5eAaPmAgInP
až	až	k9
v	v	k7c6
první	první	k4xOgFnSc6
dekádě	dekáda	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnohé	mnohé	k1gNnSc1
z	z	k7c2
nově	nově	k6eAd1
vzniklých	vzniklý	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
se	se	k3xPyFc4
etablovaly	etablovat	k5eAaBmAgFnP
již	již	k6eAd1
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1901	#num#	k4
nebo	nebo	k8xC
ve	v	k7c6
volbách	volba	k1gFnPc6
na	na	k7c4
Český	český	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nich	on	k3xPp3gInPc6
byla	být	k5eAaImAgFnS
sice	sice	k8xC
na	na	k7c6
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
zachována	zachovat	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
etnicky	etnicky	k6eAd1
českého	český	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
systému	systém	k1gInSc2
dominance	dominance	k1gFnSc2
mladočechů	mladočech	k1gMnPc2
s	s	k7c7
67	#num#	k4
mandáty	mandát	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
18	#num#	k4
mandátů	mandát	k1gInPc2
si	se	k3xPyFc3
připsali	připsat	k5eAaPmAgMnP
agrárníci	agrárník	k1gMnPc1
<g/>
,	,	kIx,
6	#num#	k4
staročeši	staročech	k1gMnPc1
<g/>
,	,	kIx,
3	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
<g/>
,	,	kIx,
2	#num#	k4
radikální	radikální	k2eAgMnPc1d1
pokrokáři	pokrokář	k1gMnPc1
a	a	k8xC
1	#num#	k4
státoprávníci	státoprávník	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Zásadní	zásadní	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
bylo	být	k5eAaImAgNnS
ovšem	ovšem	k9
zavedení	zavedení	k1gNnSc1
rovného	rovný	k2eAgNnSc2d1
a	a	k8xC
všeobecného	všeobecný	k2eAgNnSc2d1
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
volilo	volit	k5eAaImAgNnS
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominance	dominance	k1gFnSc1
mladočeských	mladočeský	k2eAgFnPc2d1
elit	elita	k1gFnPc2
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
systém	systém	k1gInSc4
více	hodně	k6eAd2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
jediné	jediný	k2eAgFnSc2d1
převažující	převažující	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkem	celkem	k6eAd1
108	#num#	k4
českých	český	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
byl	být	k5eAaImAgInS
28	#num#	k4
agrárníků	agrárník	k1gMnPc2
<g/>
,	,	kIx,
26	#num#	k4
mladočechů	mladočech	k1gMnPc2
a	a	k8xC
staročechů	staročech	k1gMnPc2
<g/>
,	,	kIx,
24	#num#	k4
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
a	a	k8xC
17	#num#	k4
klerikálů	klerikál	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Volby	volba	k1gFnPc1
probíhaly	probíhat	k5eAaImAgFnP
ve	v	k7c6
většinovém	většinový	k2eAgInSc6d1
volebním	volební	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
,	,	kIx,
dvoukolově	dvoukolově	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
před	před	k7c7
druhým	druhý	k4xOgNnSc7
kolem	kolo	k1gNnSc7
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
utváření	utváření	k1gNnSc3
širších	široký	k2eAgFnPc2d2
volebních	volební	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
trend	trend	k1gInSc1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
prohloubil	prohloubit	k5eAaPmAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
společný	společný	k2eAgInSc1d1
postup	postup	k1gInSc1
několika	několik	k4yIc2
stran	strana	k1gFnPc2
proti	proti	k7c3
klerikálním	klerikální	k2eAgMnPc3d1
kandidátům	kandidát	k1gMnPc3
vedl	vést	k5eAaImAgInS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
nakonec	nakonec	k6eAd1
získali	získat	k5eAaPmAgMnP
daleko	daleko	k6eAd1
méně	málo	k6eAd2
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
odpovídalo	odpovídat	k5eAaImAgNnS
zisku	zisk	k1gInSc3
hlasů	hlas	k1gInPc2
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
celek	celek	k1gInSc1
si	se	k3xPyFc3
ale	ale	k8xC
české	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
zachovalo	zachovat	k5eAaPmAgNnS
charakter	charakter	k1gInSc4
pluralitního	pluralitní	k2eAgInSc2d1
<g/>
,	,	kIx,
až	až	k9
roztříštěného	roztříštěný	k2eAgInSc2d1
stranického	stranický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračoval	pokračovat	k5eAaImAgMnS
i	i	k9
ústup	ústup	k1gInSc4
mladočechů	mladočech	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Čechách	Čechy	k1gFnPc6
získali	získat	k5eAaPmAgMnP
nejvíce	nejvíce	k6eAd1,k6eAd3
mandátů	mandát	k1gInPc2
agrárníci	agrárník	k1gMnPc1
<g/>
,	,	kIx,
následovaní	následovaný	k2eAgMnPc1d1
národními	národní	k2eAgFnPc7d1
sociály	sociál	k1gMnPc7
<g/>
,	,	kIx,
sociálními	sociální	k2eAgMnPc7d1
demokraty	demokrat	k1gMnPc7
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
pak	pak	k6eAd1
následovali	následovat	k5eAaImAgMnP
mladočeši	mladočech	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moravě	Morava	k1gFnSc6
vyhráli	vyhrát	k5eAaPmAgMnP
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
<g/>
,	,	kIx,
druzí	druhý	k4xOgMnPc1
byli	být	k5eAaImAgMnP
klerikálové	klerikál	k1gMnPc1
a	a	k8xC
třetí	třetí	k4xOgMnPc1
agrárníci	agrárník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neúspěšně	úspěšně	k6eNd1
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1911	#num#	k4
skončil	skončit	k5eAaPmAgInS
projekt	projekt	k1gInSc1
nové	nový	k2eAgFnSc2d1
sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
takzvaní	takzvaný	k2eAgMnPc1d1
centralisté	centralista	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
odmítala	odmítat	k5eAaImAgFnS
separaci	separace	k1gFnSc4
stranických	stranický	k2eAgFnPc2d1
a	a	k8xC
odborových	odborový	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
sociálně	sociálně	k6eAd1
demokratického	demokratický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
podle	podle	k7c2
národnostních	národnostní	k2eAgInPc2d1
táborů	tábor	k1gInPc2
a	a	k8xC
prosazovala	prosazovat	k5eAaImAgFnS
integrovanější	integrovaný	k2eAgNnSc4d2
celorakouské	celorakouský	k2eAgNnSc4d1
dělnické	dělnický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
ale	ale	k9
jen	jen	k9
zanedbatelný	zanedbatelný	k2eAgInSc1d1
počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
a	a	k8xC
mandátů	mandát	k1gInPc2
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
obdržela	obdržet	k5eAaPmAgFnS
1	#num#	k4
z	z	k7c2
celkem	celkem	k6eAd1
3	#num#	k4
českých	český	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odlišný	odlišný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
prožíval	prožívat	k5eAaImAgMnS
stranický	stranický	k2eAgInSc4d1
systém	systém	k1gInSc4
na	na	k7c6
zemské	zemský	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
byl	být	k5eAaImAgInS
volen	volit	k5eAaImNgInS
nadále	nadále	k6eAd1
podle	podle	k7c2
zastaralého	zastaralý	k2eAgInSc2d1
kuriového	kuriový	k2eAgInSc2d1
systému	systém	k1gInSc2
z	z	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
značné	značný	k2eAgNnSc1d1
zastoupení	zastoupení	k1gNnSc1
šlechty	šlechta	k1gFnSc2
<g/>
,	,	kIx,
omezené	omezený	k2eAgNnSc1d1
volební	volební	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
,	,	kIx,
úplná	úplný	k2eAgFnSc1d1
absence	absence	k1gFnSc1
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
a	a	k8xC
některých	některý	k3yIgFnPc2
dalších	další	k2eAgFnPc2d1
masových	masový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
)	)	kIx)
stále	stále	k6eAd1
více	hodně	k6eAd2
paralyzován	paralyzovat	k5eAaBmNgInS
německými	německý	k2eAgFnPc7d1
obstrukcemi	obstrukce	k1gFnPc7
(	(	kIx(
<g/>
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
následovaly	následovat	k5eAaImAgFnP
po	po	k7c6
předchozích	předchozí	k2eAgFnPc6d1
etapách	etapa	k1gFnPc6
českých	český	k2eAgFnPc2d1
obstrukcí	obstrukce	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
na	na	k7c4
Český	český	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
roku	rok	k1gInSc2
1908	#num#	k4
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
zcela	zcela	k6eAd1
ochromen	ochromit	k5eAaPmNgInS
a	a	k8xC
nemohl	moct	k5eNaImAgInS
řádně	řádně	k6eAd1
fungovat	fungovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
ho	on	k3xPp3gMnSc4
vláda	vláda	k1gFnSc1
nechala	nechat	k5eAaPmAgFnS
rozpustit	rozpustit	k5eAaPmF
(	(	kIx(
<g/>
takzvané	takzvaný	k2eAgInPc4d1
Anenské	anenský	k2eAgInPc4d1
patenty	patent	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
nahradit	nahradit	k5eAaPmF
technokratickou	technokratický	k2eAgFnSc7d1
zemskou	zemský	k2eAgFnSc7d1
správní	správní	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Moravě	Morava	k1gFnSc6
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
jiná	jiný	k2eAgFnSc1d1
<g/>
;	;	kIx,
Moravské	moravský	k2eAgNnSc1d1
vyrovnání	vyrovnání	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
dosáhlo	dosáhnout	k5eAaPmAgNnS
kompromisu	kompromis	k1gInSc3
mezi	mezi	k7c7
českou	český	k2eAgFnSc7d1
a	a	k8xC
německou	německý	k2eAgFnSc7d1
populací	populace	k1gFnSc7
a	a	k8xC
uskutečnily	uskutečnit	k5eAaPmAgInP
se	se	k3xPyFc4
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
volby	volba	k1gFnPc4
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Slezském	slezský	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
byli	být	k5eAaImAgMnP
Češi	Čech	k1gMnPc1
nevýznamnou	významný	k2eNgFnSc7d1
menšinou	menšina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
rozvojem	rozvoj	k1gInSc7
stranického	stranický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
se	se	k3xPyFc4
posouvaly	posouvat	k5eAaImAgFnP
i	i	k9
metody	metoda	k1gFnPc1
stranické	stranický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
a	a	k8xC
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Honorační	honorační	k2eAgFnPc4d1
strany	strana	k1gFnPc4
byly	být	k5eAaImAgFnP
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nahrazovány	nahrazovat	k5eAaImNgInP
stranami	strana	k1gFnPc7
masového	masový	k2eAgInSc2d1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
politologické	politologický	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
versäulung	versäulung	k1gInSc1
-	-	kIx~
„	„	k?
<g/>
sloupovatění	sloupovatění	k1gNnSc1
<g/>
“	“	k?
politického	politický	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
jasně	jasně	k6eAd1
definovaným	definovaný	k2eAgNnSc7d1
členstvím	členství	k1gNnSc7
a	a	k8xC
satelitními	satelitní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
(	(	kIx(
<g/>
odbory	odbor	k1gInPc1
<g/>
,	,	kIx,
profesní	profesní	k2eAgNnPc1d1
sdružení	sdružení	k1gNnPc1
<g/>
,	,	kIx,
hospodářská	hospodářský	k2eAgNnPc1d1
družstva	družstvo	k1gNnPc1
<g/>
,	,	kIx,
tisk	tisk	k1gInSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Identické	identický	k2eAgInPc1d1
procesy	proces	k1gInPc1
probíhaly	probíhat	k5eAaImAgInP
i	i	k9
mezi	mezi	k7c7
německým	německý	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
původní	původní	k2eAgFnSc1d1
převaha	převaha	k1gFnSc1
liberální	liberální	k2eAgFnSc2d1
německé	německý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
stavovskou	stavovský	k2eAgFnSc7d1
a	a	k8xC
ideologickou	ideologický	k2eAgFnSc7d1
pluralitou	pluralita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
v	v	k7c6
období	období	k1gNnSc6
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
</s>
<s>
Proměny	proměna	k1gFnPc1
stranického	stranický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
po	po	k7c6
vzniku	vznik	k1gInSc6
ČSR	ČSR	kA
</s>
<s>
V	v	k7c6
měsících	měsíc	k1gInPc6
před	před	k7c7
i	i	k9
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
samostatného	samostatný	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
probíhalo	probíhat	k5eAaImAgNnS
v	v	k7c6
českém	český	k2eAgInSc6d1
politickém	politický	k2eAgInSc6d1
systému	systém	k1gInSc6
přeskupování	přeskupování	k1gNnSc2
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1918	#num#	k4
se	se	k3xPyFc4
sloučilo	sloučit	k5eAaPmAgNnS
několik	několik	k4yIc1
měšťanských	měšťanský	k2eAgFnPc2d1
<g/>
,	,	kIx,
národně-liberálních	národně-liberální	k2eAgFnPc2d1
formací	formace	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
mladočechů	mladočech	k1gMnPc2
a	a	k8xC
staročechů	staročech	k1gMnPc2
<g/>
)	)	kIx)
do	do	k7c2
České	český	k2eAgFnSc2d1
státoprávní	státoprávní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
proměnila	proměnit	k5eAaPmAgFnS
na	na	k7c4
Československou	československý	k2eAgFnSc4d1
národní	národní	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1918	#num#	k4
se	se	k3xPyFc4
národně	národně	k6eAd1
sociální	sociální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
Českou	český	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
socialistickou	socialistický	k2eAgFnSc4d1
a	a	k8xC
upravila	upravit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
program	program	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
od	od	k7c2
původního	původní	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
posunula	posunout	k5eAaPmAgFnS
k	k	k7c3
levicovým	levicový	k2eAgFnPc3d1
pozicím	pozice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformou	reforma	k1gFnSc7
prošel	projít	k5eAaPmAgInS
i	i	k9
katolický	katolický	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
několik	několik	k4yIc1
křesťansko-sociálních	křesťansko-sociální	k2eAgMnPc2d1
a	a	k8xC
katolicko-konzervativních	katolicko-konzervativní	k2eAgFnPc2d1
stran	strana	k1gFnPc2
spojilo	spojit	k5eAaPmAgNnS
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
volebním	volební	k2eAgInSc7d1
soubojem	souboj	k1gInSc7
v	v	k7c6
novém	nový	k2eAgInSc6d1
státě	stát	k1gInSc6
byly	být	k5eAaImAgFnP
komunální	komunální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
roku	rok	k1gInSc2
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhaly	probíhat	k5eAaImAgInP
jen	jen	k6eAd1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
přinesly	přinést	k5eAaPmAgFnP
drtivé	drtivý	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
levice	levice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
etnicky	etnicky	k6eAd1
českých	český	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
získala	získat	k5eAaPmAgFnS
nejvíce	nejvíce	k6eAd1,k6eAd3
hlasů	hlas	k1gInPc2
Československá	československý	k2eAgFnSc1d1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
dělnická	dělnický	k2eAgFnSc1d1
(	(	kIx(
<g/>
32,5	32,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
následovaná	následovaný	k2eAgFnSc1d1
další	další	k2eAgFnSc7d1
levicovou	levicový	k2eAgFnSc7d1
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
socialistická	socialistický	k2eAgFnSc1d1
(	(	kIx(
<g/>
17,3	17,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
skončila	skončit	k5eAaPmAgFnS
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
československého	československý	k2eAgInSc2d1
venkova	venkov	k1gInSc2
(	(	kIx(
<g/>
15,3	15,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
další	další	k2eAgNnPc4d1
místa	místo	k1gNnPc4
obsadila	obsadit	k5eAaPmAgFnS
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
9,8	9,8	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Československá	československý	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
9,6	9,6	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
roztříštěné	roztříštěný	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
se	se	k3xPyFc4
dočasně	dočasně	k6eAd1
proměnilo	proměnit	k5eAaPmAgNnS
na	na	k7c4
stranický	stranický	k2eAgInSc4d1
systém	systém	k1gInSc4
s	s	k7c7
dominancí	dominance	k1gFnSc7
levice	levice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
vítězné	vítězný	k2eAgFnPc1d1
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
pak	pak	k6eAd1
společně	společně	k6eAd1
s	s	k7c7
třetí	třetí	k4xOgFnSc7
agrární	agrární	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
utvořily	utvořit	k5eAaPmAgInP
takzvanou	takzvaný	k2eAgFnSc4d1
rudozelenou	rudozelený	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Podobné	podobný	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
přinesly	přinést	k5eAaPmAgInP
i	i	k9
první	první	k4xOgFnPc4
celostátní	celostátní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
roku	rok	k1gInSc2
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
utvoření	utvoření	k1gNnSc3
unitárního	unitární	k2eAgInSc2d1
československého	československý	k2eAgInSc2d1
státu	stát	k1gInSc2
se	se	k3xPyFc4
české	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
proměňovat	proměňovat	k5eAaImF
i	i	k9
integrací	integrace	k1gFnSc7
se	s	k7c7
stranickými	stranický	k2eAgInPc7d1
subjekty	subjekt	k1gInPc7
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
(	(	kIx(
<g/>
později	pozdě	k6eAd2
i	i	k9
na	na	k7c6
Podkarpatské	podkarpatský	k2eAgFnSc6d1
Rusi	Rus	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
v	v	k7c6
tom	ten	k3xDgNnSc6
v	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
pokročili	pokročit	k5eAaPmAgMnP
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
a	a	k8xC
agrárníci	agrárník	k1gMnPc1
(	(	kIx(
<g/>
fúze	fúze	k1gFnSc1
se	s	k7c7
Slovenskou	slovenský	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
a	a	k8xC
rolnickou	rolnický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dočasně	dočasně	k6eAd1
se	se	k3xPyFc4
se	s	k7c7
slovenskými	slovenský	k2eAgMnPc7d1
katolíky	katolík	k1gMnPc7
(	(	kIx(
<g/>
Slovenská	slovenský	k2eAgFnSc1d1
ľudová	ľudový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
integrovali	integrovat	k5eAaBmAgMnP
i	i	k9
čeští	český	k2eAgMnPc1d1
lidovci	lidovec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
uspěla	uspět	k5eAaPmAgFnS
pětice	pětice	k1gFnSc1
tradičních	tradiční	k2eAgInPc2d1
českých	český	k2eAgInPc2d1
politických	politický	k2eAgInPc2d1
stranických	stranický	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pak	pak	k6eAd1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
první	první	k4xOgFnSc4
republiku	republika	k1gFnSc4
reprezentovala	reprezentovat	k5eAaImAgFnS
převážnou	převážný	k2eAgFnSc4d1
část	část	k1gFnSc4
českých	český	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
strany	strana	k1gFnPc4
sociálně	sociálně	k6eAd1
demokratickou	demokratický	k2eAgFnSc4d1
<g/>
,	,	kIx,
socialistickou	socialistický	k2eAgFnSc4d1
(	(	kIx(
<g/>
národně	národně	k6eAd1
socialistickou	socialistický	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
agrární	agrární	k2eAgFnSc4d1
(	(	kIx(
<g/>
republikánskou	republikánský	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lidovou	lidový	k2eAgFnSc4d1
a	a	k8xC
národně	národně	k6eAd1
demokratickou	demokratický	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nim	on	k3xPp3gFnPc3
se	se	k3xPyFc4
přidala	přidat	k5eAaPmAgFnS
poprvé	poprvé	k6eAd1
i	i	k9
Československá	československý	k2eAgFnSc1d1
živnostensko-obchodnická	živnostensko-obchodnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
středostavovská	středostavovský	k2eAgFnSc1d1
vzniklá	vzniklý	k2eAgFnSc1d1
v	v	k7c6
listopadu	listopad	k1gInSc6
1919	#num#	k4
sloučením	sloučení	k1gNnSc7
českých	český	k2eAgFnPc2d1
a	a	k8xC
moravských	moravský	k2eAgFnPc2d1
menších	malý	k2eAgFnPc2d2
a	a	k8xC
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nepříliš	příliš	k6eNd1
vlivných	vlivný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
postupně	postupně	k6eAd1
posilovala	posilovat	k5eAaImAgFnS
a	a	k8xC
zařadila	zařadit	k5eAaPmAgFnS
se	se	k3xPyFc4
rovněž	rovněž	k9
mezi	mezi	k7c4
etablované	etablovaný	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
subjekty	subjekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
KSČ	KSČ	kA
</s>
<s>
Zásadní	zásadní	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
českém	český	k2eAgInSc6d1
stranickém	stranický	k2eAgInSc6d1
životě	život	k1gInSc6
přinesl	přinést	k5eAaPmAgInS
roku	rok	k1gInSc2
1921	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
postupně	postupně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozkolu	rozkol	k1gInSc3
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
demokracii	demokracie	k1gFnSc6
a	a	k8xC
ustavení	ustavení	k1gNnSc6
dvou	dva	k4xCgFnPc2
paralelních	paralelní	k2eAgFnPc2d1
sociálně	sociálně	k6eAd1
demokratických	demokratický	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
levicové	levicový	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
ustavilo	ustavit	k5eAaPmAgNnS
v	v	k7c6
květnu	květen	k1gInSc6
1921	#num#	k4
jako	jako	k8xS,k8xC
samostatná	samostatný	k2eAgFnSc1d1
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
potom	potom	k6eAd1
na	na	k7c6
slučovacím	slučovací	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
na	na	k7c4
podzim	podzim	k1gInSc4
1921	#num#	k4
integrovala	integrovat	k5eAaBmAgFnS
i	i	k9
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jiných	jiný	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
KSČ	KSČ	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
jedinou	jediný	k2eAgFnSc7d1
z	z	k7c2
českých	český	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
plně	plně	k6eAd1
supraetnická	supraetnický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
se	s	k7c7
vznikem	vznik	k1gInSc7
KSČ	KSČ	kA
rozšířilo	rozšířit	k5eAaPmAgNnS
na	na	k7c4
sedm	sedm	k4xCc4
relevantních	relevantní	k2eAgInPc2d1
stranických	stranický	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
stranického	stranický	k2eAgInSc2d1
systému	systém	k1gInSc2
během	během	k7c2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgInPc6d1
letech	let	k1gInPc6
nebyla	být	k5eNaImAgFnS
KSČ	KSČ	kA
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
radikálně	radikálně	k6eAd1
opoziční	opoziční	k2eAgFnSc4d1
a	a	k8xC
revoluční	revoluční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
schopna	schopen	k2eAgFnSc1d1
se	se	k3xPyFc4
začleňovat	začleňovat	k5eAaImF
do	do	k7c2
vládních	vládní	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
proto	proto	k8xC
opakovaně	opakovaně	k6eAd1
uzavíraly	uzavírat	k5eAaImAgFnP,k5eAaPmAgFnP
široké	široký	k2eAgFnPc1d1
pravolevé	pravolevý	k2eAgFnPc1d1
koalice	koalice	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
předáci	předák	k1gMnPc1
sdružení	sdružení	k1gNnPc2
v	v	k7c6
takzvané	takzvaný	k2eAgFnSc6d1
pětce	pětka	k1gFnSc6
(	(	kIx(
<g/>
pět	pět	k4xCc4
hlavních	hlavní	k2eAgFnPc2d1
nekomunistických	komunistický	k2eNgFnPc2d1
českých	český	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
živnostníků	živnostník	k1gMnPc2
<g/>
)	)	kIx)
drželi	držet	k5eAaImAgMnP
zásadní	zásadní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkol	rozkol	k1gInSc1
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
demokracii	demokracie	k1gFnSc6
také	také	k9
rozbil	rozbít	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgMnSc1d1
krátce	krátce	k6eAd1
existující	existující	k2eAgFnSc4d1
dominanci	dominance	k1gFnSc4
levice	levice	k1gFnSc2
a	a	k8xC
české	český	k2eAgNnSc1d1
stranické	stranický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
poté	poté	k6eAd1
až	až	k9
do	do	k7c2
zániku	zánik	k1gInSc2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
opět	opět	k6eAd1
vykazovalo	vykazovat	k5eAaImAgNnS
rozptýlený	rozptýlený	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
bez	bez	k7c2
jediné	jediný	k2eAgFnSc2d1
převládající	převládající	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fragmentaci	fragmentace	k1gFnSc4
stranického	stranický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
posiloval	posilovat	k5eAaImAgInS
i	i	k9
nově	nově	k6eAd1
zavedený	zavedený	k2eAgInSc4d1
poměrný	poměrný	k2eAgInSc4d1
volební	volební	k2eAgInSc4d1
systém	systém	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většinového	většinový	k2eAgInSc2d1
<g/>
,	,	kIx,
vícekolového	vícekolový	k2eAgInSc2d1
za	za	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
)	)	kIx)
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
uzavírací	uzavírací	k2eAgFnSc7d1
klauzulí	klauzule	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praxe	praxe	k1gFnSc1
širokých	široký	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
byla	být	k5eAaImAgFnS
porušena	porušit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1926	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
takzvaná	takzvaný	k2eAgFnSc1d1
panská	panský	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
poprvé	poprvé	k6eAd1
a	a	k8xC
naposledy	naposledy	k6eAd1
sdružila	sdružit	k5eAaPmAgFnS
výlučně	výlučně	k6eAd1
pravicové	pravicový	k2eAgFnSc2d1
<g/>
,	,	kIx,
občanské	občanský	k2eAgFnSc2d1
a	a	k8xC
nesocialistické	socialistický	k2eNgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
obdobných	obdobný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
německé	německý	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
opět	opět	k6eAd1
systém	systém	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
k	k	k7c3
širším	široký	k2eAgFnPc3d2
koalicím	koalice	k1gFnPc3
<g/>
,	,	kIx,
za	za	k7c2
účasti	účast	k1gFnSc2
levice	levice	k1gFnSc2
i	i	k8xC
pravice	pravice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
okrajově	okrajově	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
prosazovaly	prosazovat	k5eAaImAgInP
nové	nový	k2eAgInPc1d1
politické	politický	k2eAgInPc1d1
proudy	proud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
vznikla	vzniknout	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
fašistická	fašistický	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
volbách	volba	k1gFnPc6
získávala	získávat	k5eAaImAgFnS
jen	jen	k6eAd1
několik	několik	k4yIc4
poslaneckých	poslanecký	k2eAgNnPc2d1
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
zásadně	zásadně	k6eAd1
neuchytily	uchytit	k5eNaPmAgFnP
ani	ani	k8xC
radikálně	radikálně	k6eAd1
nacionalistické	nacionalistický	k2eAgFnSc2d1
formace	formace	k1gFnSc2
jako	jako	k8xC,k8xS
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
nebo	nebo	k8xC
Liga	liga	k1gFnSc1
proti	proti	k7c3
vázaným	vázaný	k2eAgFnPc3d1
kandidátním	kandidátní	k2eAgFnPc3d1
listinám	listina	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
okolo	okolo	k6eAd1
sebe	sebe	k3xPyFc4
vytvářel	vytvářet	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Stříbrný	stříbrný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Pokusem	pokus	k1gInSc7
o	o	k7c4
posun	posun	k1gInSc4
ve	v	k7c6
stranickém	stranický	k2eAgInSc6d1
životě	život	k1gInSc6
byla	být	k5eAaImAgFnS
také	také	k9
integrace	integrace	k1gFnSc1
národních	národní	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
s	s	k7c7
některými	některý	k3yIgFnPc7
nacionalistickými	nacionalistický	k2eAgFnPc7d1
formacemi	formace	k1gFnPc7
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
Národní	národní	k2eAgNnSc1d1
sjednocení	sjednocení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
to	ten	k3xDgNnSc1
ale	ale	k9
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1935	#num#	k4
nezaznamenalo	zaznamenat	k5eNaPmAgNnS
větší	veliký	k2eAgInPc4d2
přírůstky	přírůstek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zase	zase	k9
neuspěla	uspět	k5eNaPmAgFnS
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
koncipovaná	koncipovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
liberální	liberální	k2eAgFnSc1d1
<g/>
,	,	kIx,
prohradní	prohradní	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
k	k	k7c3
národním	národní	k2eAgMnPc3d1
demokratům	demokrat	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Efemérní	efemérní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
měly	mít	k5eAaImAgInP
i	i	k9
některé	některý	k3yIgFnPc1
odštěpenecké	odštěpenecký	k2eAgFnPc1d1
levicové	levicový	k2eAgFnPc1d1
formace	formace	k1gFnPc1
jako	jako	k8xS,k8xC
Socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
československého	československý	k2eAgInSc2d1
lidu	lid	k1gInSc2
pracujícího	pracující	k2eAgInSc2d1
nebo	nebo	k8xC
Neodvislá	odvislý	k2eNgFnSc1d1
strana	strana	k1gFnSc1
komunistická	komunistický	k2eAgFnSc1d1
v	v	k7c6
ČSR	ČSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sedm	sedm	k4xCc1
základních	základní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
komunistů	komunista	k1gMnPc2
<g/>
)	)	kIx)
si	se	k3xPyFc3
tak	tak	k6eAd1
udrželo	udržet	k5eAaPmAgNnS
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
lidovém	lidový	k2eAgNnSc6d1
hlasování	hlasování	k1gNnSc6
za	za	k7c2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
byly	být	k5eAaImAgFnP
komunální	komunální	k2eAgFnPc4d1
volby	volba	k1gFnPc4
roku	rok	k1gInSc2
1938	#num#	k4
konané	konaný	k2eAgFnSc2d1
již	již	k6eAd1
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
mezinárodního	mezinárodní	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
českého	český	k2eAgNnSc2d1
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
proběhl	proběhnout	k5eAaPmAgInS
v	v	k7c6
německém	německý	k2eAgInSc6d1
táboře	tábor	k1gInSc6
integrační	integrační	k2eAgInSc1d1
zlom	zlom	k1gInSc1
a	a	k8xC
dominantní	dominantní	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
silou	síla	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
mezi	mezi	k7c4
strany	strana	k1gFnPc4
v	v	k7c6
období	období	k1gNnSc6
1918	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
</s>
<s>
volby	volba	k1gFnPc1
1920	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
ČSDSD	ČSDSD	kA
</s>
<s>
DSAP	DSAP	kA
</s>
<s>
m	m	kA
</s>
<s>
s	s	k7c7
</s>
<s>
ČSS	ČSS	kA
</s>
<s>
rep	rep	k?
</s>
<s>
BdL	BdL	k?
</s>
<s>
m	m	kA
</s>
<s>
SNR	SNR	kA
</s>
<s>
ČSL	ČSL	kA
</s>
<s>
DCV	DCV	kA
</s>
<s>
m	m	kA
</s>
<s>
ž	ž	k?
</s>
<s>
ČSND	ČSND	kA
</s>
<s>
f	f	k?
</s>
<s>
DNP	DNP	kA
</s>
<s>
volby	volba	k1gFnPc1
1925	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
ČSDSD	ČSDSD	kA
</s>
<s>
DSAP	DSAP	kA
</s>
<s>
ČSS	ČSS	kA
</s>
<s>
rep	rep	k?
</s>
<s>
BdL	BdL	k?
</s>
<s>
a	a	k8xC
</s>
<s>
ČSL	ČSL	kA
</s>
<s>
DCV	DCV	kA
</s>
<s>
SĽS	SĽS	kA
</s>
<s>
m	m	kA
</s>
<s>
p	p	k?
</s>
<s>
živ	živ	k2eAgMnSc1d1
</s>
<s>
ČSND	ČSND	kA
</s>
<s>
nac	nac	k?
<g/>
.	.	kIx.
</s>
<s>
DNP	DNP	kA
</s>
<s>
volby	volba	k1gFnPc1
1929	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
ČSDSD	ČSDSD	kA
</s>
<s>
DSAP	DSAP	kA
</s>
<s>
ČSNS	ČSNS	kA
</s>
<s>
rep	rep	k?
</s>
<s>
BdL	BdL	k?
</s>
<s>
ČSL	ČSL	kA
</s>
<s>
DCV	DCV	kA
</s>
<s>
HSĽS	HSĽS	kA
</s>
<s>
m	m	kA
</s>
<s>
pž	pž	k?
</s>
<s>
živ	živ	k2eAgMnSc1d1
</s>
<s>
ČSND	ČSND	kA
</s>
<s>
L	L	kA
</s>
<s>
nac	nac	k?
<g/>
.	.	kIx.
</s>
<s>
DNP	DNP	kA
</s>
<s>
volby	volba	k1gFnPc1
1935	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
ČSDSD	ČSDSD	kA
</s>
<s>
DSAP	DSAP	kA
</s>
<s>
ČSNS	ČSNS	kA
</s>
<s>
rep	rep	k?
</s>
<s>
BdL	BdL	k?
</s>
<s>
ČSL	ČSL	kA
</s>
<s>
DCV	DCV	kA
</s>
<s>
AB	AB	kA
(	(	kIx(
<g/>
HSĽS	HSĽS	kA
<g/>
)	)	kIx)
</s>
<s>
m	m	kA
</s>
<s>
živ	živ	k2eAgMnSc1d1
</s>
<s>
NSj	NSj	k?
</s>
<s>
NOF	NOF	kA
</s>
<s>
SdP	SdP	k?
</s>
<s>
České	český	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
v	v	k7c6
období	období	k1gNnSc6
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
přinesla	přinést	k5eAaPmAgFnS
radikální	radikální	k2eAgFnPc4d1
územní	územní	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
ale	ale	k8xC
i	i	k9
otřes	otřes	k1gInSc4
politického	politický	k2eAgInSc2d1
a	a	k8xC
stranického	stranický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
cílené	cílený	k2eAgFnSc3d1
redukci	redukce	k1gFnSc3
počtu	počet	k1gInSc2
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
právě	právě	k6eAd1
těžkopádná	těžkopádný	k2eAgFnSc1d1
a	a	k8xC
komplikovaná	komplikovaný	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
rozhodování	rozhodování	k1gNnSc2
v	v	k7c6
prvorepublikových	prvorepublikový	k2eAgFnPc6d1
koalicích	koalice	k1gFnPc6
byla	být	k5eAaImAgFnS
označována	označován	k2eAgFnSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
chyb	chyba	k1gFnPc2
předmnichovského	předmnichovský	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
utvořily	utvořit	k5eAaPmAgInP
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
:	:	kIx,
Strana	strana	k1gFnSc1
národní	národní	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zahrnula	zahrnout	k5eAaPmAgFnS
bývalé	bývalý	k2eAgMnPc4d1
agrárníky	agrárník	k1gMnPc4
<g/>
,	,	kIx,
živnostníky	živnostník	k1gMnPc4
<g/>
,	,	kIx,
lidovce	lidovec	k1gMnPc4
<g/>
,	,	kIx,
národní	národní	k2eAgMnPc4d1
demokraty	demokrat	k1gMnPc4
(	(	kIx(
<g/>
národně	národně	k6eAd1
sjednocené	sjednocený	k2eAgFnSc2d1
<g/>
)	)	kIx)
a	a	k8xC
část	část	k1gFnSc4
národních	národní	k2eAgInPc2d1
socialistů	socialist	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
soustředili	soustředit	k5eAaPmAgMnP
bývalí	bývalý	k2eAgMnPc1d1
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
a	a	k8xC
část	část	k1gFnSc1
národních	národní	k2eAgMnPc2d1
socialistů	socialist	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloučily	sloučit	k5eAaPmAgInP
se	se	k3xPyFc4
i	i	k9
některé	některý	k3yIgFnPc1
satelitní	satelitní	k2eAgFnPc1d1
stranické	stranický	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
nemohla	moct	k5eNaImAgFnS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
fungování	fungování	k1gNnSc6
<g/>
,	,	kIx,
mandáty	mandát	k1gInPc1
jejích	její	k3xOp3gMnPc2
poslanců	poslanec	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
zvolených	zvolený	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
zanikly	zaniknout	k5eAaPmAgInP
<g/>
,	,	kIx,
řadoví	řadový	k2eAgMnPc1d1
členové	člen	k1gMnPc1
KSČ	KSČ	kA
se	se	k3xPyFc4
ovšem	ovšem	k9
mohli	moct	k5eAaImAgMnP
angažovat	angažovat	k5eAaBmF
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
zbylé	zbylý	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
byly	být	k5eAaImAgFnP
výlučně	výlučně	k6eAd1
české	český	k2eAgNnSc4d1
(	(	kIx(
<g/>
vzhledem	vzhledem	k7c3
k	k	k7c3
federalizaci	federalizace	k1gFnSc3
Česko-Slovenska	Česko-Slovensko	k1gNnSc2
na	na	k7c4
podzim	podzim	k1gInSc4
1938	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
utvořil	utvořit	k5eAaPmAgInS
separátní	separátní	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
a	a	k8xC
stranický	stranický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
označován	označovat	k5eAaImNgInS
jako	jako	k9
autoritativní	autoritativní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
existoval	existovat	k5eAaImAgInS
jen	jen	k9
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
nich	on	k3xPp3gMnPc2
postupně	postupně	k6eAd1
v	v	k7c6
Straně	strana	k1gFnSc6
národní	národní	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
sílily	sílit	k5eAaImAgFnP
protidemokratické	protidemokratický	k2eAgFnPc1d1
<g/>
,	,	kIx,
totalitární	totalitární	k2eAgFnPc1d1
a	a	k8xC
pronacistické	pronacistický	k2eAgFnPc1d1
tendence	tendence	k1gFnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
ztrácela	ztrácet	k5eAaImAgFnS
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádné	žádný	k3yNgFnSc2
volby	volba	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
nekonaly	konat	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Role	role	k1gFnSc1
parlamentu	parlament	k1gInSc2
byla	být	k5eAaImAgFnS
oslabena	oslabit	k5eAaPmNgFnS
zmocňovacím	zmocňovací	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
se	se	k3xPyFc4
naposledy	naposledy	k6eAd1
sešlo	sejít	k5eAaPmAgNnS
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
německé	německý	k2eAgFnSc6d1
okupaci	okupace	k1gFnSc6
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
v	v	k7c6
březnu	březen	k1gInSc6
1939	#num#	k4
stranické	stranický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
zcela	zcela	k6eAd1
zaniklo	zaniknout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
povolenou	povolený	k2eAgFnSc4d1
v	v	k7c6
Protektorátu	protektorát	k1gInSc6
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
bylo	být	k5eAaImAgNnS
Národní	národní	k2eAgNnSc1d1
souručenství	souručenství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
ale	ale	k9
mělo	mít	k5eAaImAgNnS
rys	rys	k1gInSc4
masového	masový	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
(	(	kIx(
<g/>
členy	člen	k1gMnPc7
byli	být	k5eAaImAgMnP
téměř	téměř	k6eAd1
všichni	všechen	k3xTgMnPc1
dospělí	dospělý	k2eAgMnPc1d1
muži	muž	k1gMnPc1
české	český	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
)	)	kIx)
působícího	působící	k2eAgMnSc2d1
v	v	k7c6
totalitních	totalitní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
role	role	k1gFnSc2
se	se	k3xPyFc4
během	během	k7c2
okupace	okupace	k1gFnSc2
navíc	navíc	k6eAd1
postupně	postupně	k6eAd1
vytrácela	vytrácet	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
obnovil	obnovit	k5eAaPmAgInS
částečný	částečný	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
pluralismus	pluralismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
1945	#num#	k4
byla	být	k5eAaImAgFnS
ustavena	ustaven	k2eAgFnSc1d1
Národní	národní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
Čechů	Čech	k1gMnPc2
a	a	k8xC
Slováků	Slovák	k1gMnPc2
jako	jako	k8xC,k8xS
politické	politický	k2eAgNnSc1d1
ústředí	ústředí	k1gNnSc1
československých	československý	k2eAgFnPc2d1
stran	strana	k1gFnPc2
hlásících	hlásící	k2eAgFnPc2d1
se	se	k3xPyFc4
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
obnovené	obnovený	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
fungovala	fungovat	k5eAaImAgFnS
jako	jako	k9
všenárodní	všenárodní	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ale	ale	k9
nezahrnovala	zahrnovat	k5eNaImAgFnS
všechny	všechen	k3xTgInPc4
politické	politický	k2eAgInPc4d1
proudy	proud	k1gInPc4
předmnichovského	předmnichovský	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
nových	nový	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
podléhal	podléhat	k5eAaImAgMnS
navíc	navíc	k6eAd1
souhlasu	souhlas	k1gInSc2
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1946	#num#	k4
tak	tak	k6eAd1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
kandidovaly	kandidovat	k5eAaImAgFnP
jen	jen	k6eAd1
čtyři	čtyři	k4xCgFnPc4
strany	strana	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
voleb	volba	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
dominantní	dominantní	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
výrazným	výrazný	k2eAgInSc7d1
odstupem	odstup	k1gInSc7
skončila	skončit	k5eAaPmAgFnS
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
a	a	k8xC
jako	jako	k8xS,k8xC
poslední	poslední	k2eAgFnSc1d1
Československá	československý	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
čtyř	čtyři	k4xCgMnPc2
těchto	tento	k3xDgMnPc2
subjektů	subjekt	k1gInPc2
jen	jen	k9
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nebyla	být	k5eNaImAgFnS
stranou	stranou	k6eAd1
levicovou	levicový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
zanikla	zaniknout	k5eAaPmAgFnS
agrární	agrární	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
živnostenská	živnostenský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
i	i	k9
politické	politický	k2eAgFnPc4d1
formace	formace	k1gFnPc4
národně-liberálního	národně-liberální	k2eAgMnSc2d1
a	a	k8xC
národně-konzervativního	národně-konzervativní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
jakými	jaký	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
byli	být	k5eAaImAgMnP
meziváleční	meziválečný	k2eAgMnPc1d1
národní	národní	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Paralelně	paralelně	k6eAd1
s	s	k7c7
utvořením	utvoření	k1gNnSc7
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
se	se	k3xPyFc4
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
integrovaly	integrovat	k5eAaBmAgFnP
i	i	k9
dosavadní	dosavadní	k2eAgFnPc1d1
stranické	stranický	k2eAgFnPc1d1
satelitní	satelitní	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
tak	tak	k6eAd1
již	již	k6eAd1
nebyly	být	k5eNaImAgInP
obnoveny	obnovit	k5eAaPmNgInP
samostatné	samostatný	k2eAgInPc1d1
stranické	stranický	k2eAgInPc1d1
odborové	odborový	k2eAgFnSc2d1
a	a	k8xC
profesní	profesní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgInSc2
vznikly	vzniknout	k5eAaPmAgFnP
celonárodní	celonárodní	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
jednotné	jednotný	k2eAgFnPc1d1
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
nadstranické	nadstranický	k2eAgInPc1d1
(	(	kIx(
<g/>
Svaz	svaz	k1gInSc1
české	český	k2eAgFnSc2d1
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
Ústřední	ústřední	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odborů	odbor	k1gInPc2
<g/>
,	,	kIx,
Ústřední	ústřední	k2eAgInSc1d1
svaz	svaz	k1gInSc1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
Jednotný	jednotný	k2eAgInSc1d1
svaz	svaz	k1gInSc1
českých	český	k2eAgMnPc2d1
zemědělců	zemědělec	k1gMnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
stranické	stranický	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
zůstal	zůstat	k5eAaPmAgInS
politický	politický	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změnou	změna	k1gFnSc7
oproti	oproti	k7c3
období	období	k1gNnSc3
před	před	k7c7
rokem	rok	k1gInSc7
1938	#num#	k4
bylo	být	k5eAaImAgNnS
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
vznikly	vzniknout	k5eAaPmAgInP
dva	dva	k4xCgInPc1
paralelní	paralelní	k2eAgInPc1d1
stranické	stranický	k2eAgInPc1d1
systémy	systém	k1gInPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
i	i	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dominantní	dominantní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
dočasně	dočasně	k6eAd1
stala	stát	k5eAaPmAgFnS
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
kde	kde	k6eAd1
tamní	tamní	k2eAgFnSc1d1
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Slovenska	Slovensko	k1gNnSc2
byla	být	k5eAaImAgFnS
formálně	formálně	k6eAd1
nezávislým	závislý	k2eNgInSc7d1
subjektem	subjekt	k1gInSc7
<g/>
,	,	kIx,
bez	bez	k7c2
vazby	vazba	k1gFnSc2
na	na	k7c6
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
separace	separace	k1gFnSc1
ale	ale	k9
nebyla	být	k5eNaImAgFnS
úplná	úplný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
KSČ	KSČ	kA
s	s	k7c7
KSS	KSS	kA
úzce	úzko	k6eAd1
spolupracovala	spolupracovat	k5eAaImAgFnS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nacházela	nacházet	k5eAaImAgFnS
spojenectví	spojenectví	k1gNnSc4
u	u	k7c2
českých	český	k2eAgFnPc2d1
nemarxistických	marxistický	k2eNgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnSc1d2
slovenská	slovenský	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
se	se	k3xPyFc4
dokonce	dokonce	k9
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
integrovala	integrovat	k5eAaBmAgFnS
plně	plně	k6eAd1
do	do	k7c2
celostátní	celostátní	k2eAgFnSc2d1
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
Zemská	zemský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
mezi	mezi	k7c4
strany	strana	k1gFnPc4
v	v	k7c6
období	období	k1gNnSc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
</s>
<s>
volby	volba	k1gFnPc1
1946	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
93	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
KSS	KSS	kA
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
SP	SP	kA
</s>
<s>
ČSNS	ČSNS	kA
</s>
<s>
SS	SS	kA
</s>
<s>
ČSL	ČSL	kA
</s>
<s>
DS	DS	kA
</s>
<s>
České	český	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
v	v	k7c6
období	období	k1gNnSc6
komunistické	komunistický	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Únor	únor	k1gInSc1
1948	#num#	k4
a	a	k8xC
jednotná	jednotný	k2eAgFnSc1d1
kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
</s>
<s>
Všenárodní	všenárodní	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
předjaří	předjaří	k1gNnSc6
1948	#num#	k4
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgMnPc1d1
socialisté	socialist	k1gMnPc1
<g/>
,	,	kIx,
lidovci	lidovec	k1gMnPc1
a	a	k8xC
slovenští	slovenský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
se	se	k3xPyFc4
rezignacemi	rezignace	k1gFnPc7
na	na	k7c4
křesla	křeslo	k1gNnPc4
v	v	k7c6
kabinetu	kabinet	k1gInSc6
pokusili	pokusit	k5eAaPmAgMnP
vynutit	vynutit	k5eAaPmF
změnu	změna	k1gFnSc4
vládního	vládní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mocenském	mocenský	k2eAgInSc6d1
souboji	souboj	k1gInSc6
(	(	kIx(
<g/>
únorový	únorový	k2eAgInSc4d1
převrat	převrat	k1gInSc4
<g/>
)	)	kIx)
ale	ale	k8xC
nakonec	nakonec	k6eAd1
triumfovali	triumfovat	k5eAaBmAgMnP
komunisté	komunista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
mobilizovali	mobilizovat	k5eAaBmAgMnP
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
aktivizovali	aktivizovat	k5eAaImAgMnP
své	svůj	k3xOyFgMnPc4
spojence	spojenec	k1gMnPc4
<g/>
,	,	kIx,
provedli	provést	k5eAaPmAgMnP
čistky	čistka	k1gFnPc4
a	a	k8xC
následně	následně	k6eAd1
sestavili	sestavit	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Klementa	Klement	k1gMnSc2
Gottwalda	Gottwald	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
formálně	formálně	k6eAd1
hlásila	hlásit	k5eAaImAgFnS
ke	k	k7c3
kontinuitě	kontinuita	k1gFnSc3
s	s	k7c7
politikou	politika	k1gFnSc7
poválečné	poválečný	k2eAgFnSc2d1
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
politická	politický	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
projevilo	projevit	k5eAaPmAgNnS
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Národní	národní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
předložila	předložit	k5eAaPmAgFnS
jednotnou	jednotný	k2eAgFnSc4d1
kandidátní	kandidátní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
získala	získat	k5eAaPmAgFnS
přes	přes	k7c4
86	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výlučnou	výlučný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
silou	síla	k1gFnSc7
v	v	k7c6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
po	po	k7c6
Únoru	únor	k1gInSc6
transformována	transformován	k2eAgFnSc1d1
na	na	k7c4
Československou	československý	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
socialistickou	socialistický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
činnosti	činnost	k1gFnSc6
pokračovala	pokračovat	k5eAaImAgFnS
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Československá	československý	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
sloučila	sloučit	k5eAaPmAgFnS
s	s	k7c7
KSČ	KSČ	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Formálně	formálně	k6eAd1
tak	tak	k9
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
existoval	existovat	k5eAaImAgInS
systém	systém	k1gInSc1
dominantní	dominantní	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
KSČ	KSČ	kA
<g/>
)	)	kIx)
a	a	k8xC
dvou	dva	k4xCgFnPc2
menších	malý	k2eAgFnPc2d2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
fakticky	fakticky	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
totalitní	totalitní	k2eAgInSc4d1
systém	systém	k1gInSc4
ovládaný	ovládaný	k2eAgInSc1d1
komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
toto	tento	k3xDgNnSc4
zakotvila	zakotvit	k5eAaPmAgFnS
coby	coby	k?
vedoucí	vedoucí	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
KSČ	KSČ	kA
až	až	k9
Ústava	ústava	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgFnPc1
volby	volba	k1gFnPc1
na	na	k7c6
území	území	k1gNnSc6
Československa	Československo	k1gNnSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
pak	pak	k6eAd1
probíhaly	probíhat	k5eAaImAgInP
v	v	k7c6
systému	systém	k1gInSc6
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
jednotnou	jednotný	k2eAgFnSc7d1
kandidátní	kandidátní	k2eAgFnSc7d1
listinou	listina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Náznaky	náznak	k1gInPc1
politického	politický	k2eAgInSc2d1
pluralismu	pluralismus	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
a	a	k8xC
normalizace	normalizace	k1gFnSc1
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
náznaky	náznak	k1gInPc1
narušení	narušení	k1gNnSc1
totalitního	totalitní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
stranického	stranický	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
zákon	zákon	k1gInSc1
č.	č.	k?
113	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
počítal	počítat	k5eAaImAgInS
s	s	k7c7
volbami	volba	k1gFnPc7
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ve	v	k7c6
vícemandátových	vícemandátův	k2eAgInPc6d1
okrscích	okrsek	k1gInPc6
<g/>
,	,	kIx,
tedy	tedy	k9
s	s	k7c7
převahou	převaha	k1gFnSc7
počtu	počet	k1gInSc2
kandidátů	kandidát	k1gMnPc2
nad	nad	k7c7
počtem	počet	k1gInSc7
mandátů	mandát	k1gInPc2
a	a	k8xC
tudíž	tudíž	k8xC
limitovanou	limitovaný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
soutěží	soutěž	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
měl	mít	k5eAaImAgInS
ovšem	ovšem	k9
být	být	k5eAaImF
zachován	zachován	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
pak	pak	k6eAd1
sílily	sílit	k5eAaImAgFnP
emancipační	emancipační	k2eAgFnPc1d1
tendence	tendence	k1gFnPc1
v	v	k7c6
nekomunistických	komunistický	k2eNgFnPc6d1
stranách	strana	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
počítaly	počítat	k5eAaImAgFnP
s	s	k7c7
rovnoprávnějším	rovnoprávní	k2eAgNnSc7d2
postavením	postavení	k1gNnSc7
vůči	vůči	k7c3
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivizovaly	aktivizovat	k5eAaImAgInP
se	s	k7c7
skupiny	skupina	k1gFnPc1
bývalých	bývalý	k2eAgMnPc2d1
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
obnovení	obnovení	k1gNnSc3
samostatné	samostatný	k2eAgFnSc2d1
sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
pro	pro	k7c4
nesouhlas	nesouhlas	k1gInSc4
KSČ	KSČ	kA
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Výrazný	výrazný	k2eAgInSc1d1
vliv	vliv	k1gInSc1
měl	mít	k5eAaImAgInS
zdola	zdola	k6eAd1
budovaný	budovaný	k2eAgInSc1d1
a	a	k8xC
mimo	mimo	k7c4
Národní	národní	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
působící	působící	k2eAgInSc1d1
Klub	klub	k1gInSc1
angažovaných	angažovaný	k2eAgMnPc2d1
nestraníků	nestraník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Chystané	chystaný	k2eAgFnPc1d1
volby	volba	k1gFnPc1
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
roku	rok	k1gInSc2
1968	#num#	k4
se	se	k3xPyFc4
ale	ale	k9
nikdy	nikdy	k6eAd1
neuskutečnily	uskutečnit	k5eNaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
provedení	provedení	k1gNnSc6
invaze	invaze	k1gFnSc2
vojsk	vojsko	k1gNnPc2
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
do	do	k7c2
Československa	Československo	k1gNnSc2
a	a	k8xC
nástupu	nástup	k1gInSc2
normalizace	normalizace	k1gFnSc2
byly	být	k5eAaImAgInP
tyto	tento	k3xDgInPc1
reformní	reformní	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
zastaveny	zastaven	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
pak	pak	k6eAd1
opět	opět	k6eAd1
fungoval	fungovat	k5eAaImAgInS
systém	systém	k1gInSc1
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
s	s	k7c7
vedoucí	vedoucí	k2eAgFnSc7d1
úlohou	úloha	k1gFnSc7
KSČ	KSČ	kA
a	a	k8xC
dvěma	dva	k4xCgFnPc7
dalšími	další	k2eAgFnPc7d1
loajálními	loajální	k2eAgFnPc7d1
<g/>
,	,	kIx,
závislými	závislý	k2eAgFnPc7d1
politickými	politický	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
byla	být	k5eAaImAgFnS
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Slovenska	Slovensko	k1gNnSc2
její	její	k3xOp3gFnSc2
součástí	součást	k1gFnPc2
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
rozhodující	rozhodující	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
nejvyšší	vysoký	k2eAgInSc1d3
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
Ústřední	ústřední	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
měl	mít	k5eAaImAgInS
například	například	k6eAd1
odborné	odborný	k2eAgNnSc4d1
<g/>
,	,	kIx,
rezortní	rezortní	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pokrývala	pokrývat	k5eAaImAgFnS
všechny	všechen	k3xTgFnPc4
oblasti	oblast	k1gFnPc4
správy	správa	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
drobným	drobný	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
došlo	dojít	k5eAaPmAgNnS
až	až	k9
krátce	krátce	k6eAd1
před	před	k7c7
pádem	pád	k1gInSc7
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
reformní	reformní	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
(	(	kIx(
<g/>
perestrojka	perestrojka	k1gFnSc1
<g/>
)	)	kIx)
přineslo	přinést	k5eAaPmAgNnS
opět	opět	k6eAd1
myšlenky	myšlenka	k1gFnSc2
na	na	k7c6
zavedení	zavedení	k1gNnSc6
limitované	limitovaný	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1989	#num#	k4
se	se	k3xPyFc4
tak	tak	k6eAd1
konaly	konat	k5eAaImAgFnP
v	v	k7c6
několika	několik	k4yIc6
okrscích	okrsek	k1gInPc6
doplňovací	doplňovací	k2eAgFnPc1d1
volby	volba	k1gFnPc1
do	do	k7c2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
bylo	být	k5eAaImAgNnS
nominováno	nominovat	k5eAaBmNgNnS
Národní	národní	k2eAgFnSc7d1
frontou	fronta	k1gFnSc7
více	hodně	k6eAd2
kandidátů	kandidát	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
</s>
<s>
Sametová	sametový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
a	a	k8xC
dominance	dominance	k1gFnSc1
Občanského	občanský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
</s>
<s>
Obnovení	obnovení	k1gNnSc1
politického	politický	k2eAgInSc2d1
pluralismu	pluralismus	k1gInSc2
přinesla	přinést	k5eAaPmAgFnS
sametová	sametový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
vzniklo	vzniknout	k5eAaPmAgNnS
Občanské	občanský	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
pak	pak	k6eAd1
vedlo	vést	k5eAaImAgNnS
jednání	jednání	k1gNnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
i	i	k9
na	na	k7c6
vládní	vládní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
listopadu	listopad	k1gInSc2
a	a	k8xC
prosince	prosinec	k1gInSc2
1989	#num#	k4
vyústil	vyústit	k5eAaPmAgInS
tlak	tlak	k1gInSc1
veřejnosti	veřejnost	k1gFnSc2
v	v	k7c6
přijetí	přijetí	k1gNnSc6
zásadních	zásadní	k2eAgInPc2d1
ústavních	ústavní	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
Federálním	federální	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
zrušilo	zrušit	k5eAaPmAgNnS
článek	článek	k1gInSc4
ústavy	ústava	k1gFnSc2
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
vedoucí	vedoucí	k2eAgFnSc2d1
úlohy	úloha	k1gFnSc2
KSČ	KSČ	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
přelomu	přelom	k1gInSc6
roku	rok	k1gInSc2
1989	#num#	k4
a	a	k8xC
1990	#num#	k4
pak	pak	k6eAd1
začal	začít	k5eAaPmAgInS
proces	proces	k1gInSc1
kooptací	kooptace	k1gFnPc2
do	do	k7c2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
a	a	k8xC
kooptací	kooptace	k1gFnPc2
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
kterými	který	k3yIgNnPc7,k3yRgNnPc7,k3yQgNnPc7
se	se	k3xPyFc4
do	do	k7c2
zákonodárných	zákonodárný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
jmenováním	jmenování	k1gNnSc7
dostali	dostat	k5eAaPmAgMnP
představitelé	představitel	k1gMnPc1
Občanského	občanský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
<g/>
,	,	kIx,
nestraníků	nestraník	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
některých	některý	k3yIgMnPc2
nových	nový	k2eAgMnPc2d1
komunistů	komunista	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1990	#num#	k4
proběhly	proběhnout	k5eAaPmAgFnP
Volby	volba	k1gFnPc1
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
lidu	lid	k1gInSc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
,	,	kIx,
Volby	volba	k1gFnPc1
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
národů	národ	k1gInPc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
i	i	k8xC
Volby	volba	k1gFnPc1
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občanské	občanský	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
v	v	k7c6
nich	on	k3xPp3gMnPc6
získalo	získat	k5eAaPmAgNnS
okolo	okolo	k7c2
poloviny	polovina	k1gFnSc2
hlasů	hlas	k1gInPc2
a	a	k8xC
dočasně	dočasně	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
dominantní	dominantní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
české	český	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Relevantní	relevantní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
zůstala	zůstat	k5eAaPmAgFnS
nadále	nadále	k6eAd1
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
výrazné	výrazný	k2eAgInPc4d1
zisky	zisk	k1gInPc4
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
si	se	k3xPyFc3
připsalo	připsat	k5eAaPmAgNnS
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
samosprávnou	samosprávný	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
-	-	kIx~
Společnost	společnost	k1gFnSc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
Slezsko	Slezsko	k1gNnSc4
a	a	k8xC
do	do	k7c2
zákonodárných	zákonodárný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
i	i	k9
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
coby	coby	k?
Křesťanská	křesťanský	k2eAgFnSc1d1
a	a	k8xC
demokratická	demokratický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
sloučily	sloučit	k5eAaPmAgFnP
i	i	k9
některé	některý	k3yIgFnPc1
další	další	k2eAgFnPc1d1
křesťanské	křesťanský	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
parlament	parlament	k1gInSc4
zatím	zatím	k6eAd1
zůstali	zůstat	k5eAaPmAgMnP
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Dotvoření	dotvoření	k1gNnSc1
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
probíhaly	probíhat	k5eAaImAgInP
v	v	k7c6
stranickém	stranický	k2eAgInSc6d1
systému	systém	k1gInSc6
další	další	k2eAgFnSc2d1
hluboké	hluboký	k2eAgFnSc2d1
změny	změna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračoval	pokračovat	k5eAaImAgMnS
separátní	separátní	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
celostátní	celostátní	k2eAgInPc1d1
KSČ	KSČ	kA
se	se	k3xPyFc4
už	už	k6eAd1
v	v	k7c6
březnu	březen	k1gInSc6
1990	#num#	k4
rozdělila	rozdělit	k5eAaPmAgFnS
na	na	k7c4
Komunistickou	komunistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Slovenska	Slovensko	k1gNnSc2
a	a	k8xC
Komunistickou	komunistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vazby	vazba	k1gFnPc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
stranami	strana	k1gFnPc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
prakticky	prakticky	k6eAd1
přetrhly	přetrhnout	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Podstatné	podstatný	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
hlavně	hlavně	k6eAd1
pnutí	pnutí	k1gNnSc1
v	v	k7c6
Občanském	občanský	k2eAgNnSc6d1
fóru	fórum	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
krystalizovaly	krystalizovat	k5eAaImAgFnP
názorové	názorový	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
jako	jako	k8xC,k8xS
Meziparlamentní	meziparlamentní	k2eAgInSc4d1
klub	klub	k1gInSc4
demokratické	demokratický	k2eAgFnSc2d1
pravice	pravice	k1gFnSc2
nebo	nebo	k8xC
Klub	klub	k1gInSc1
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
Občanského	občanský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
rozkol	rozkol	k1gInSc1
postihl	postihnout	k5eAaPmAgInS
hlavní	hlavní	k2eAgFnSc4d1
masu	masa	k1gFnSc4
Občanského	občanský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
na	na	k7c6
jaře	jaro	k1gNnSc6
1991	#num#	k4
jeho	jeho	k3xOp3gNnSc1
rozdělení	rozdělení	k1gNnSc1
na	na	k7c4
dva	dva	k4xCgInPc4
nástupnické	nástupnický	k2eAgInPc4d1
subjekty	subjekt	k1gInPc4
<g/>
:	:	kIx,
Občanské	občanský	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
a	a	k8xC
Občanskou	občanský	k2eAgFnSc4d1
demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Samostatně	samostatně	k6eAd1
postupovala	postupovat	k5eAaImAgFnS
i	i	k9
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
volby	volba	k1gFnPc4
1990	#num#	k4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
(	(	kIx(
<g/>
ČNR	ČNR	kA
<g/>
)	)	kIx)
</s>
<s>
33	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
124	#num#	k4
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
KDU	KDU	kA
</s>
<s>
HSD	HSD	kA
</s>
<s>
OF	OF	kA
</s>
<s>
volby	volba	k1gFnPc4
1992	#num#	k4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
(	(	kIx(
<g/>
ČNR	ČNR	kA
<g/>
)	)	kIx)
</s>
<s>
35	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
76	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
LB	LB	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
LSU	LSU	kA
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
HSD	HSD	kA
</s>
<s>
ODS-KDS	ODS-KDS	k?
</s>
<s>
ODA	ODA	kA
</s>
<s>
SPR	SPR	kA
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1992	#num#	k4
vystřídala	vystřídat	k5eAaPmAgFnS
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
kandidující	kandidující	k2eAgFnSc1d1
v	v	k7c6
koalici	koalice	k1gFnSc6
s	s	k7c7
Křesťanskodemokratickou	křesťanskodemokratický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
)	)	kIx)
Občanské	občanský	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
v	v	k7c6
roli	role	k1gFnSc6
dominantní	dominantní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
byť	byť	k8xS
s	s	k7c7
daleko	daleko	k6eAd1
nižším	nízký	k2eAgInSc7d2
výsledkem	výsledek	k1gInSc7
(	(	kIx(
<g/>
cca	cca	kA
30	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnSc1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byly	být	k5eAaImAgFnP
nadále	nadále	k6eAd1
roztříštěné	roztříštěný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandáty	mandát	k1gInPc4
obhájili	obhájit	k5eAaPmAgMnP
komunisté	komunista	k1gMnPc1
<g/>
,	,	kIx,
sdružení	sdružený	k2eAgMnPc1d1
nyní	nyní	k6eAd1
do	do	k7c2
koalice	koalice	k1gFnSc2
Levý	levý	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
parlamentu	parlament	k1gInSc2
pronikla	proniknout	k5eAaPmAgFnS
nově	nově	k6eAd1
i	i	k9
Československá	československý	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
,	,	kIx,
Liberálně	liberálně	k6eAd1
sociální	sociální	k2eAgFnSc2d1
unie	unie	k1gFnSc2
(	(	kIx(
<g/>
aliance	aliance	k1gFnSc2
několika	několik	k4yIc2
středových	středový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Křesťanská	křesťanský	k2eAgFnSc1d1
a	a	k8xC
demokratická	demokratický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
-	-	kIx~
Československá	československý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
lidová	lidový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
a	a	k8xC
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
samosprávnou	samosprávný	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
-	-	kIx~
Společnost	společnost	k1gFnSc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
Slezsko	Slezsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
mandáty	mandát	k1gInPc4
získala	získat	k5eAaPmAgFnS
i	i	k9
radikální	radikální	k2eAgFnSc1d1
formace	formace	k1gFnSc1
Sdružení	sdružení	k1gNnSc2
pro	pro	k7c4
republiku	republika	k1gFnSc4
-	-	kIx~
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
dosud	dosud	k6eAd1
vlivné	vlivný	k2eAgNnSc1d1
Občanské	občanský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
těsně	těsně	k6eAd1
nepřekonalo	překonat	k5eNaPmAgNnS
hranici	hranice	k1gFnSc4
5	#num#	k4
%	%	kIx~
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1992	#num#	k4
zaniklo	zaniknout	k5eAaPmAgNnS
Československo	Československo	k1gNnSc1
a	a	k8xC
politický	politický	k2eAgInSc1d1
prostor	prostor	k1gInSc1
českých	český	k2eAgFnPc2d1
stran	strana	k1gFnPc2
se	se	k3xPyFc4
zúžil	zúžit	k5eAaPmAgInS
výlučně	výlučně	k6eAd1
na	na	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gInSc4
ústavní	ústavní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
mezi	mezi	k7c4
strany	strana	k1gFnPc4
v	v	k7c6
období	období	k1gNnSc6
1996	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
volby	volba	k1gFnPc1
1996	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
61	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
68	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
ODS	ODS	kA
</s>
<s>
ODA	ODA	kA
</s>
<s>
SPR-RSČ	SPR-RSČ	k?
</s>
<s>
volby	volba	k1gFnPc1
1998	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
US	US	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
volby	volba	k1gFnPc1
2002	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
70	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
58	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
US	US	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
volby	volba	k1gFnPc1
2006	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
81	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
ODS	ODS	kA
</s>
<s>
volby	volba	k1gFnPc1
2010	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
56	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
VV	VV	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
ODS	ODS	kA
</s>
<s>
Během	během	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
vývoj	vývoj	k1gInSc4
stranického	stranický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
od	od	k7c2
dominance	dominance	k1gFnSc2
Občanského	občanský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
a	a	k8xC
později	pozdě	k6eAd2
Občanské	občanský	k2eAgFnSc6d1
demokratické	demokratický	k2eAgFnSc6d1
straně	strana	k1gFnSc6
dospělo	dochvít	k5eAaPmAgNnS
k	k	k7c3
paritě	parita	k1gFnSc3
mezi	mezi	k7c7
pravicí	pravice	k1gFnSc7
a	a	k8xC
levicí	levice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
dekády	dekáda	k1gFnSc2
se	se	k3xPyFc4
jako	jako	k9
nejsilnější	silný	k2eAgFnSc1d3
levicová	levicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
etablovala	etablovat	k5eAaBmAgFnS
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c4
pravici	pravice	k1gFnSc4
zůstala	zůstat	k5eAaPmAgFnS
hlavním	hlavní	k2eAgInSc7d1
subjektem	subjekt	k1gInSc7
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
jiné	jiný	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
se	se	k3xPyFc4
z	z	k7c2
vrcholové	vrcholový	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
vytrácely	vytrácet	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1996	#num#	k4
takto	takto	k6eAd1
z	z	k7c2
parlamentu	parlament	k1gInSc2
vymizely	vymizet	k5eAaPmAgInP
Liberálně	liberálně	k6eAd1
sociální	sociální	k2eAgFnSc2d1
unie	unie	k1gFnSc2
i	i	k8xC
moravistické	moravistický	k2eAgFnSc2d1
formace	formace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1998	#num#	k4
pak	pak	k6eAd1
o	o	k7c4
mandáty	mandát	k1gInPc4
přišlo	přijít	k5eAaPmAgNnS
i	i	k9
Sdružení	sdružení	k1gNnSc1
pro	pro	k7c4
republiku	republika	k1gFnSc4
-	-	kIx~
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkol	rozkol	k1gInSc1
v	v	k7c6
ODS	ODS	kA
z	z	k7c2
přelomu	přelom	k1gInSc2
let	léto	k1gNnPc2
1997	#num#	k4
a	a	k8xC
1998	#num#	k4
přinesl	přinést	k5eAaPmAgInS
vznik	vznik	k1gInSc1
Unie	unie	k1gFnSc2
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ale	ale	k9
neohrozila	ohrozit	k5eNaPmAgFnS
bipolární	bipolární	k2eAgInSc4d1
charakter	charakter	k1gInSc4
české	český	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
ustavený	ustavený	k2eAgInSc1d1
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Trvale	trvale	k6eAd1
na	na	k7c6
okraji	okraj	k1gInSc6
vysoké	vysoký	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
zůstaly	zůstat	k5eAaPmAgFnP
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dekádu	dekáda	k1gFnSc4
některé	některý	k3yIgFnPc4
další	další	k2eAgFnPc4d1
formace	formace	k1gFnPc4
jako	jako	k8xC,k8xS
Důchodci	důchodce	k1gMnPc1
za	za	k7c4
životní	životní	k2eAgFnPc4d1
jistoty	jistota	k1gFnPc4
<g/>
,	,	kIx,
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
či	či	k8xC
Demokratická	demokratický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
dočasný	dočasný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
měly	mít	k5eAaImAgInP
reformní	reformní	k2eAgInPc1d1
levicové	levicový	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
odštěpené	odštěpený	k2eAgInPc1d1
od	od	k7c2
KSČM	KSČM	kA
jako	jako	k8xC,k8xS
Levý	levý	k2eAgInSc1d1
blok	blok	k1gInSc1
nebo	nebo	k8xC
Strana	strana	k1gFnSc1
demokratické	demokratický	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásadní	zásadní	k2eAgFnSc4d1
proměnu	proměna	k1gFnSc4
stranického	stranický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
nepřinesly	přinést	k5eNaPmAgFnP
ani	ani	k9
volby	volba	k1gFnPc1
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
volby	volba	k1gFnSc2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Trval	trvat	k5eAaImAgInS
systém	systém	k1gInSc1
křehké	křehký	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
mezi	mezi	k7c7
pravicí	pravice	k1gFnSc7
a	a	k8xC
levicí	levice	k1gFnSc7
a	a	k8xC
dominance	dominance	k1gFnSc1
ODS	ODS	kA
a	a	k8xC
ČSSD	ČSSD	kA
v	v	k7c6
příslušných	příslušný	k2eAgInPc6d1
pólech	pól	k1gInPc6
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
Čtyřkoalice	Čtyřkoalice	k1gFnSc2
z	z	k7c2
počátku	počátek	k1gInSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
rozpadl	rozpadnout	k5eAaPmAgInS
a	a	k8xC
do	do	k7c2
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
šel	jít	k5eAaImAgMnS
v	v	k7c6
redukované	redukovaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
jako	jako	k8xC,k8xS
Koalice	koalice	k1gFnSc1
(	(	kIx(
<g/>
aliance	aliance	k1gFnSc1
lidovců	lidovec	k1gMnPc2
a	a	k8xC
Unie	unie	k1gFnSc1
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
integrované	integrovaný	k2eAgFnSc2d1
Demokratické	demokratický	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgInSc7d1
politickým	politický	k2eAgInSc7d1
subjektem	subjekt	k1gInSc7
v	v	k7c6
parlamentu	parlament	k1gInSc6
se	se	k3xPyFc4
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
stala	stát	k5eAaPmAgFnS
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc4d2
proměnu	proměna	k1gFnSc4
stranické	stranický	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
přinesly	přinést	k5eAaPmAgFnP
sněmovní	sněmovní	k2eAgFnPc1d1
volby	volba	k1gFnPc1
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ČSSD	ČSSD	kA
a	a	k8xC
ODS	ODS	kA
oslabily	oslabit	k5eAaPmAgFnP
a	a	k8xC
do	do	k7c2
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
pronikly	proniknout	k5eAaPmAgFnP
nové	nový	k2eAgFnPc1d1
formace	formace	k1gFnPc1
jako	jako	k8xC,k8xS
Věci	věc	k1gFnPc1
veřejné	veřejný	k2eAgFnPc1d1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
z	z	k7c2
nich	on	k3xPp3gMnPc6
odštěpená	odštěpený	k2eAgFnSc1d1
formace	formace	k1gFnSc1
LIDEM	člověk	k1gMnPc3
<g/>
)	)	kIx)
a	a	k8xC
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
KDU-ČSL	KDU-ČSL	k1gMnPc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
do	do	k7c2
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
nedostala	dostat	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Mimo	mimo	k7c4
parlament	parlament	k1gInSc4
zůstala	zůstat	k5eAaPmAgFnS
Strana	strana	k1gFnSc1
práv	právo	k1gNnPc2
občanů	občan	k1gMnPc2
ZEMANOVCI	zemanovec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
propadu	propad	k1gInSc6
Sdružení	sdružení	k1gNnSc2
pro	pro	k7c4
republiku	republika	k1gFnSc4
-	-	kIx~
Republikánské	republikánský	k2eAgFnPc1d1
strany	strana	k1gFnPc1
Československa	Československo	k1gNnSc2
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1998	#num#	k4
trvale	trvale	k6eAd1
mimo	mimo	k7c4
zastupitelské	zastupitelský	k2eAgInPc4d1
sbory	sbor	k1gInPc4
zůstala	zůstat	k5eAaPmAgFnS
i	i	k9
nacionalistická	nacionalistický	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
reprezentovaná	reprezentovaný	k2eAgFnSc1d1
například	například	k6eAd1
Dělnickou	dělnický	k2eAgFnSc4d1
stranou	strana	k1gFnSc7
či	či	k8xC
Národní	národní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Stranický	stranický	k2eAgInSc1d1
systém	systém	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dále	daleko	k6eAd2
rozvíjel	rozvíjet	k5eAaImAgInS
i	i	k9
na	na	k7c6
vedlejších	vedlejší	k2eAgInPc6d1
volebních	volební	k2eAgInPc6d1
fórech	fór	k1gInPc6
a	a	k8xC
zastupitelských	zastupitelský	k2eAgInPc6d1
sborech	sbor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
senátu	senát	k1gInSc2
<g/>
,	,	kIx,
krajských	krajský	k2eAgNnPc2d1
zastupitelstev	zastupitelstvo	k1gNnPc2
<g/>
,	,	kIx,
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
či	či	k8xC
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
uspěly	uspět	k5eAaPmAgInP
celostátní	celostátní	k2eAgInPc1d1
politické	politický	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
regionální	regionální	k2eAgFnPc4d1
formace	formace	k1gFnPc4
(	(	kIx(
<g/>
Severočeši	Severočech	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Starostové	Starosta	k1gMnPc1
pro	pro	k7c4
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
ad	ad	k7c4
hoc	hoc	k?
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
SNK	SNK	kA
sdružení	sdružení	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
individuální	individuální	k2eAgMnPc1d1
nezávislí	závislý	k2eNgMnPc1d1
kandidáti	kandidát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
stranictví	stranictví	k1gNnSc1
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
nenavázalo	navázat	k5eNaPmAgNnS
výrazněji	výrazně	k6eAd2
na	na	k7c4
předmnichovskou	předmnichovský	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
u	u	k7c2
některých	některý	k3yIgFnPc2
stran	strana	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
návaznosti	návaznost	k1gFnSc3
na	na	k7c4
prvorepublikové	prvorepublikový	k2eAgFnPc4d1
předchůdkyně	předchůdkyně	k1gFnPc4
(	(	kIx(
<g/>
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
<g/>
,	,	kIx,
komunisté	komunista	k1gMnPc1
<g/>
,	,	kIx,
lidovci	lidovec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmizel	zmizet	k5eAaPmAgInS
koncept	koncept	k1gInSc1
satelitních	satelitní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
;	;	kIx,
polistopadové	polistopadový	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jen	jen	k9
zřídka	zřídka	k6eAd1
vytvářely	vytvářet	k5eAaImAgFnP
vlastní	vlastní	k2eAgFnPc1d1
zájmové	zájmový	k2eAgFnPc1d1
<g/>
,	,	kIx,
odborové	odborový	k2eAgFnPc1d1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
politický	politický	k2eAgInSc1d1
tisk	tisk	k1gInSc1
nebyl	být	k5eNaImAgInS
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
přímo	přímo	k6eAd1
propojen	propojen	k2eAgInSc1d1
s	s	k7c7
politickými	politický	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Členská	členský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
stran	strana	k1gFnPc2
zůstala	zůstat	k5eAaPmAgFnS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
meziválečným	meziválečný	k2eAgNnSc7d1
Československem	Československo	k1gNnSc7
výrazně	výrazně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
ty	ten	k3xDgFnPc4
největší	veliký	k2eAgFnPc4d3
české	český	k2eAgFnPc4d1
politické	politický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
mají	mít	k5eAaImIp3nP
maximálně	maximálně	k6eAd1
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
tisíc	tisíc	k4xCgInSc1
členů	člen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Změna	změna	k1gFnSc1
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2013	#num#	k4
</s>
<s>
Předčasné	předčasný	k2eAgFnPc4d1
sněmovní	sněmovní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
roku	rok	k1gInSc2
2013	#num#	k4
přinesly	přinést	k5eAaPmAgFnP
zásadní	zásadní	k2eAgFnSc4d1
proměnu	proměna	k1gFnSc4
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hlasování	hlasování	k1gNnSc6
zvítězila	zvítězit	k5eAaPmAgFnS
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
20	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejnižší	nízký	k2eAgInSc4d3
výsledek	výsledek	k1gInSc4
pro	pro	k7c4
vítěznou	vítězný	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
v	v	k7c6
polistopadových	polistopadový	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
zcela	zcela	k6eAd1
nový	nový	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
ANO	ano	k9
2011	#num#	k4
a	a	k8xC
do	do	k7c2
parlamentu	parlament	k1gInSc2
pronikla	proniknout	k5eAaPmAgFnS
nově	nově	k6eAd1
též	též	k9
formace	formace	k1gFnSc1
Úsvit	úsvit	k1gInSc1
přímé	přímý	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
Tomia	Tomium	k1gNnSc2
Okamury	Okamura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
ODS	ODS	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
obsazovala	obsazovat	k5eAaImAgFnS
první	první	k4xOgFnSc4
nebo	nebo	k8xC
druhou	druhý	k4xOgFnSc4
pozici	pozice	k1gFnSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
nejsilnějším	silný	k2eAgInSc7d3
subjektem	subjekt	k1gInSc7
pravicové	pravicový	k2eAgFnSc2d1
části	část	k1gFnSc2
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
obdržela	obdržet	k5eAaPmAgFnS
jen	jen	k9
necelých	celý	k2eNgInPc2d1
8	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
zařadila	zařadit	k5eAaPmAgFnS
se	se	k3xPyFc4
mezi	mezi	k7c4
středně	středně	k6eAd1
velké	velký	k2eAgFnPc4d1
politické	politický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
sněmovny	sněmovna	k1gFnSc2
se	se	k3xPyFc4
po	po	k7c6
přestávce	přestávka	k1gFnSc6
vrátila	vrátit	k5eAaPmAgFnS
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinkou	novinka	k1gFnSc7
bylo	být	k5eAaImAgNnS
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
koaliční	koaliční	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Bohuslava	Bohuslava	k1gFnSc1
Sobotky	Sobotka	k1gFnSc2
vzniklá	vzniklý	k2eAgFnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
těchto	tento	k3xDgFnPc2
voleb	volba	k1gFnPc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
v	v	k7c6
polistopadovém	polistopadový	k2eAgNnSc6d1
období	období	k1gNnSc6
zahrnovala	zahrnovat	k5eAaImAgFnS
první	první	k4xOgFnSc4
i	i	k8xC
druhou	druhý	k4xOgFnSc4
nejsilnější	silný	k2eAgFnSc4d3
stranu	strana	k1gFnSc4
(	(	kIx(
<g/>
ČSSD	ČSSD	kA
a	a	k8xC
hnutí	hnutí	k1gNnSc6
ANO	ano	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
doplněné	doplněný	k2eAgInPc1d1
o	o	k7c6
KDU-ČSL	KDU-ČSL	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Bipolární	bipolární	k2eAgInSc1d1
charakter	charakter	k1gInSc1
stranického	stranický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
narušený	narušený	k2eAgInSc1d1
již	již	k6eAd1
výsledky	výsledek	k1gInPc1
sněmovních	sněmovní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
více	hodně	k6eAd2
fragmentovaným	fragmentovaný	k2eAgInSc7d1
stranickým	stranický	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
trend	trend	k1gInSc1
ještě	ještě	k9
zesílil	zesílit	k5eAaPmAgInS
ve	v	k7c6
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
dostalo	dostat	k5eAaPmAgNnS
rekordních	rekordní	k2eAgInPc2d1
devět	devět	k4xCc1
politických	politický	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
ve	v	k7c6
volbách	volba	k1gFnPc6
nezvítězila	zvítězit	k5eNaPmAgFnS
ODS	ODS	kA
ani	ani	k8xC
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
poprvé	poprvé	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
posunulo	posunout	k5eAaPmAgNnS
od	od	k7c2
bipolarity	bipolarita	k1gFnSc2
trvající	trvající	k2eAgInSc4d1
směrem	směr	k1gInSc7
k	k	k7c3
dominanci	dominance	k1gFnSc3
jednoho	jeden	k4xCgMnSc2
subjektu	subjekt	k1gInSc2
(	(	kIx(
<g/>
hnutí	hnutí	k1gNnPc4
ANO	ano	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
naposledy	naposledy	k6eAd1
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
takto	takto	k6eAd1
dominovala	dominovat	k5eAaImAgFnS
ODS	ODS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
pronikly	proniknout	k5eAaPmAgInP
další	další	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
přímá	přímý	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
tu	tu	k6eAd1
ale	ale	k8xC
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
formací	formace	k1gFnSc7
Úsvit	úsvit	k1gInSc4
přímé	přímý	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
osoba	osoba	k1gFnSc1
jejího	její	k3xOp3gMnSc2
předsedy	předseda	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazný	výrazný	k2eAgInSc4d1
propad	propad	k1gInSc4
zaznamenaly	zaznamenat	k5eAaPmAgFnP
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
ČSSD	ČSSD	kA
a	a	k8xC
KSČM	KSČM	kA
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
pravicová	pravicový	k2eAgFnSc1d1
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečné	částečný	k2eAgInPc1d1
zotavení	zotavení	k1gNnSc4
po	po	k7c6
volebním	volební	k2eAgInSc6d1
propadu	propad	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
přinesly	přinést	k5eAaPmAgFnP
volby	volba	k1gFnPc1
roku	rok	k1gInSc2
2017	#num#	k4
pravicové	pravicový	k2eAgFnSc2d1
ODS	ODS	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
druhou	druhý	k4xOgFnSc7
nejsilnější	silný	k2eAgFnSc7d3
stranou	strana	k1gFnSc7
v	v	k7c6
nové	nový	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
<g/>
,	,	kIx,
byť	byť	k8xS
s	s	k7c7
velkým	velký	k2eAgInSc7d1
odstupem	odstup	k1gInSc7
za	za	k7c7
vítězným	vítězný	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
ANO	ano	k9
<g/>
.	.	kIx.
</s>
<s>
volby	volba	k1gFnPc4
2013	#num#	k4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
</s>
<s>
33	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
ANO	ano	k9
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
TOP	topit	k5eAaImRp2nS
</s>
<s>
ODS	ODS	kA
</s>
<s>
Úsvit	úsvit	k1gInSc1
</s>
<s>
volby	volba	k1gFnPc4
2017	#num#	k4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc1
mandátů	mandát	k1gInPc2
</s>
<s>
15	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
78	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
Piráti	pirát	k1gMnPc1
</s>
<s>
ANO	ano	k9
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
ST	St	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
</s>
<s>
ODS	ODS	kA
</s>
<s>
SPD	SPD	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Národní	národní	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červen	červen	k1gInSc1
1848	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
63	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
249	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
27	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Československé	československý	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
269	#num#	k4
<g/>
-	-	kIx~
<g/>
278	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Dále	daleko	k6eAd2
jen	jen	k6eAd1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
↑	↑	k?
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
94	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
125	#num#	k4
<g/>
-	-	kIx~
<g/>
127	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
29	#num#	k4
<g/>
,	,	kIx,
59	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
18	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
↑	↑	k?
Politische	Politische	k1gNnSc2
Tageschronik	Tageschronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenec	červenec	k1gInSc1
1874	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
47	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
180	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
První	první	k4xOgInSc4
den	den	k1gInSc4
volebního	volební	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenec	červenec	k1gInSc1
1874	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
180	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
328	#num#	k4
<g/>
-	-	kIx~
<g/>
342	#num#	k4
<g/>
,	,	kIx,
370	#num#	k4
<g/>
-	-	kIx~
<g/>
372	#num#	k4
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
385	#num#	k4
<g/>
-	-	kIx~
<g/>
390	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
32	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
213	#num#	k4
<g/>
-	-	kIx~
<g/>
234	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
413	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
29	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
260	#num#	k4
<g/>
-	-	kIx~
<g/>
268	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
258	#num#	k4
<g/>
-	-	kIx~
<g/>
261	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
404	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
32	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
↑	↑	k?
Seskupení	seskupení	k1gNnSc4
poslanců	poslanec	k1gMnPc2
sněmu	sněm	k1gInSc2
král	král	k1gMnSc1
<g/>
.	.	kIx.
českého	český	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1901	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
41	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
289	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
330-3311	330-3311	k4
2	#num#	k4
Dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
333	#num#	k4
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
331	#num#	k4
<g/>
-	-	kIx~
<g/>
332	#num#	k4
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
329	#num#	k4
<g/>
-	-	kIx~
<g/>
330	#num#	k4
<g/>
↑	↑	k?
KOŘALKA	kořalka	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Češi	Čech	k1gMnPc1
v	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
a	a	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
1815	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
168	#num#	k4
<g/>
-	-	kIx~
<g/>
173	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
24	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
↑	↑	k?
Klimek	Klimek	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
:	:	kIx,
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
Svazek	svazek	k1gInSc1
XIII	XIII	kA
<g/>
.	.	kIx.
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
328	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
553	#num#	k4
<g/>
-	-	kIx~
<g/>
555	#num#	k4
<g/>
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
dějinách	dějiny	k1gFnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901579	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
497	#num#	k4
<g/>
-	-	kIx~
<g/>
498	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
610	#num#	k4
<g/>
-	-	kIx~
<g/>
612	#num#	k4
<g/>
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
729	#num#	k4
<g/>
↑	↑	k?
Kandidátní	kandidátní	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
pro	pro	k7c4
volbu	volba	k1gFnSc4
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
czso	czso	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Klimek	Klimek	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
:	:	kIx,
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
Svazek	svazek	k1gInSc1
XIV	XIV	kA
<g/>
.	.	kIx.
1929	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
425	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
575	#num#	k4
<g/>
-	-	kIx~
<g/>
577	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1057	#num#	k4
<g/>
-	-	kIx~
<g/>
1090	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1091	#num#	k4
<g/>
-	-	kIx~
<g/>
1103	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1216	#num#	k4
<g/>
-	-	kIx~
<g/>
1221	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ke	k	k7c3
125	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Přijata	přijat	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
nová	nový	k2eAgFnSc1d1
"	"	kIx"
<g/>
socialistická	socialistický	k2eAgFnSc1d1
<g/>
"	"	kIx"
Ústava	ústava	k1gFnSc1
ČSSR	ČSSR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ceskatelevize	ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fejtl	Fejtl	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Vývoj	vývoj	k1gInSc1
politického	politický	k2eAgInSc2d1
systému	systém	k1gInSc2
po	po	k7c6
Listopadu	listopad	k1gInSc6
1989	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zákon	zákon	k1gInSc1
o	o	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pražské	pražský	k2eAgNnSc1d1
jaro	jaro	k1gNnSc1
1968	#num#	k4
a	a	k8xC
probuzení	probuzení	k1gNnSc1
občanské	občanský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
III	III	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
radio	radio	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pražské	pražský	k2eAgNnSc1d1
jaro	jaro	k1gNnSc1
1968	#num#	k4
a	a	k8xC
probuzení	probuzení	k1gNnSc1
občanské	občanský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
IV	IV	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
radio	radio	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
187	#num#	k4
Návrh	návrh	k1gInSc1
výboru	výbor	k1gInSc2
ústavně	ústavně	k6eAd1
právního	právní	k2eAgInSc2d1
na	na	k7c6
vydání	vydání	k1gNnSc6
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
skončení	skončení	k1gNnSc6
volebního	volební	k2eAgNnSc2d1
období	období	k1gNnSc2
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
a	a	k8xC
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Noví	nový	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
zvoleni	zvolit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
1989	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
69	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
95	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Po	po	k7c6
40	#num#	k4
letech	léto	k1gNnPc6
ztratila	ztratit	k5eAaPmAgFnS
KSČ	KSČ	kA
vedoucí	vedoucí	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
v	v	k7c6
zemi	zem	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ceskatelevize	ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1414	#num#	k4
<g/>
-	-	kIx~
<g/>
1416	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1364	#num#	k4
<g/>
,	,	kIx,
1492	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kopeček	Kopeček	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
-	-	kIx~
Pšeja	Pšeja	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
ČSSD	ČSSD	kA
a	a	k8xC
KSČM	KSČM	kA
<g/>
:	:	kIx,
na	na	k7c6
cestě	cesta	k1gFnSc6
ke	k	k7c3
spojenectví	spojenectví	k1gNnSc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nástin	nástin	k1gInSc1
souvztažností	souvztažnost	k1gFnPc2
vývoje	vývoj	k1gInSc2
a	a	k8xC
vzájemných	vzájemný	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
<g/>
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fss	fss	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Kopeček	Kopeček	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
:	:	kIx,
Éra	éra	k1gFnSc1
nevinnosti	nevinnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
and	and	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87029	#num#	k4
<g/>
-	-	kIx~
<g/>
98	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
98	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1503	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
konané	konaný	k2eAgFnSc2d1
ve	v	k7c6
dnech	den	k1gInPc6
5	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
6.6	6.6	k4
<g/>
.1992	.1992	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
31.5	31.5	k4
<g/>
.	.	kIx.
-	-	kIx~
1.6	1.6	k4
<g/>
.1996	.1996	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
19	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
20.6	20.6	k4
<g/>
.1998	.1998	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
14	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
15.6	15.6	k4
<g/>
.2002	.2002	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
2	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
3.6	3.6	k4
<g/>
.2006	.2006	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
28.05	28.05	k4
<g/>
.	.	kIx.
–	–	k?
29.05	29.05	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Typologie	typologie	k1gFnPc4
českého	český	k2eAgInSc2d1
stranického	stranický	k2eAgInSc2d1
systému	systém	k1gInSc2
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
e-polis	e-polis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JIRÁK	Jirák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
:	:	kIx,
Média	médium	k1gNnPc1
a	a	k8xC
politika	politika	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rvp	rvp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Strany	strana	k1gFnPc1
přicházejí	přicházet	k5eAaImIp3nP
o	o	k7c4
členy	člen	k1gMnPc4
<g/>
,	,	kIx,
nejvíce	nejvíce	k6eAd1,k6eAd3
uteklo	utéct	k5eAaPmAgNnS
Věcem	věc	k1gFnPc3
veřejným	veřejný	k2eAgFnPc3d1
a	a	k8xC
ODS	ODS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MALÍŘ	malíř	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
,	,	kIx,
Vývoj	vývoj	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
1024	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
německé	německý	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
