<s>
Současné	současný	k2eAgNnSc1d1	současné
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
38	[number]	k4	38
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
osmý	osmý	k4xOgMnSc1	osmý
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
;	;	kIx,	;
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
dobám	doba	k1gFnPc3	doba
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
nábožensky	nábožensky	k6eAd1	nábožensky
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
