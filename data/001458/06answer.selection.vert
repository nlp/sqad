<s>
K	k	k7c3	k
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
Koperníkovi	Koperník	k1gMnSc3	Koperník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1539	[number]	k4	1539
přijel	přijet	k5eAaPmAgMnS	přijet
Georg	Georg	k1gMnSc1	Georg
Joachim	Joachim	k1gMnSc1	Joachim
von	von	k1gInSc4	von
Lauchen	Lauchen	k2eAgInSc4d1	Lauchen
zvaný	zvaný	k2eAgInSc4d1	zvaný
Rhaeticus	Rhaeticus	k1gInSc4	Rhaeticus
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
matematiky	matematika	k1gFnSc2	matematika
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
naukou	nauka	k1gFnSc7	nauka
<g/>
.	.	kIx.	.
</s>
