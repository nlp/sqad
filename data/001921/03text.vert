<s>
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
během	během	k7c2	během
transkripce	transkripce	k1gFnSc2	transkripce
DNA	DNA	kA	DNA
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
předpis	předpis	k1gInSc4	předpis
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
bílkoviny	bílkovina	k1gFnSc2	bílkovina
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
přepsané	přepsaný	k2eAgFnPc4d1	přepsaná
podle	podle	k7c2	podle
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
mRNA	mRNA	k?	mRNA
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
molekula	molekula	k1gFnSc1	molekula
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
messenger	messenger	k1gInSc1	messenger
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
poslíček	poslíček	k1gMnSc1	poslíček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
informační	informační	k2eAgFnSc1d1	informační
nebo	nebo	k8xC	nebo
mediátorová	mediátorový	k2eAgFnSc1d1	mediátorová
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Transkripce_	Transkripce_	k1gFnSc2	Transkripce_
<g/>
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
mRNA	mRNA	k?	mRNA
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
jádře	jádro	k1gNnSc6	jádro
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
transkripce	transkripce	k1gFnSc1	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
působením	působení	k1gNnSc7	působení
enzymu	enzym	k1gInSc2	enzym
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k8xS	jako
templát	templát	k1gInSc1	templát
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
vlákna	vlákno	k1gNnSc2	vlákno
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Nukleotidy	nukleotid	k1gInPc1	nukleotid
se	se	k3xPyFc4	se
přikládají	přikládat	k5eAaImIp3nP	přikládat
podle	podle	k7c2	podle
párování	párování	k1gNnSc2	párování
s	s	k7c7	s
originálem	originál	k1gInSc7	originál
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
replikace	replikace	k1gFnSc2	replikace
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
několika	několik	k4yIc6	několik
ohledech	ohled	k1gInPc6	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgInPc1d1	bakteriální
geny	gen	k1gInPc1	gen
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
jediným	jediný	k2eAgInSc7d1	jediný
nepřerušeným	přerušený	k2eNgInSc7d1	nepřerušený
úsekem	úsek	k1gInSc7	úsek
úseku	úsek	k1gInSc2	úsek
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
zkopíruje	zkopírovat	k5eAaPmIp3nS	zkopírovat
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
molekulu	molekula	k1gFnSc4	molekula
mRNA	mRNA	k?	mRNA
Eukaryotní	Eukaryotní	k2eAgInPc4d1	Eukaryotní
geny	gen	k1gInPc4	gen
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
nekódující	kódující	k2eNgFnPc1d1	nekódující
sekvence	sekvence	k1gFnPc1	sekvence
(	(	kIx(	(
<g/>
introny	intron	k1gInPc1	intron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
do	do	k7c2	do
primárního	primární	k2eAgInSc2d1	primární
transkriptu	transkript	k1gInSc2	transkript
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
transportem	transport	k1gInSc7	transport
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
vystřihnout	vystřihnout	k5eAaPmF	vystřihnout
(	(	kIx(	(
<g/>
splicing	splicing	k1gInSc1	splicing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
otevřeného	otevřený	k2eAgInSc2d1	otevřený
čtecího	čtecí	k2eAgInSc2d1	čtecí
rámce	rámec	k1gInSc2	rámec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kóduje	kódovat	k5eAaBmIp3nS	kódovat
protein	protein	k1gInSc4	protein
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
typická	typický	k2eAgFnSc1d1	typická
mRNA	mRNA	k?	mRNA
oblasti	oblast	k1gFnSc3	oblast
chránící	chránící	k2eAgFnSc2d1	chránící
5	[number]	k4	5
<g/>
'	'	kIx"	'
a	a	k8xC	a
3	[number]	k4	3
<g/>
'	'	kIx"	'
konec	konec	k1gInSc4	konec
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
5	[number]	k4	5
<g/>
'	'	kIx"	'
čepičku	čepička	k1gFnSc4	čepička
a	a	k8xC	a
3	[number]	k4	3
<g/>
'	'	kIx"	'
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
konec	konec	k1gInSc1	konec
<g/>
)	)	kIx)	)
Poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
tvořena	tvořen	k2eAgFnSc1d1	tvořena
5	[number]	k4	5
<g/>
'	'	kIx"	'
a	a	k8xC	a
3	[number]	k4	3
<g/>
'	'	kIx"	'
nepřekládanými	překládaný	k2eNgFnPc7d1	nepřekládaná
oblastmi	oblast	k1gFnPc7	oblast
(	(	kIx(	(
<g/>
UTR	UTR	kA	UTR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
řadu	řada	k1gFnSc4	řada
regulačních	regulační	k2eAgFnPc2d1	regulační
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Posttranskripční	Posttranskripční	k2eAgFnSc2d1	Posttranskripční
modifikace	modifikace	k1gFnSc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
transkripci	transkripce	k1gFnSc6	transkripce
genů	gen	k1gInPc2	gen
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
primární	primární	k2eAgInSc1d1	primární
transkript	transkript	k1gInSc1	transkript
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
upraven	upravit	k5eAaPmNgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Splicing	Splicing	k1gInSc1	Splicing
<g/>
.	.	kIx.	.
</s>
<s>
Úseky	úsek	k1gInPc1	úsek
kódované	kódovaný	k2eAgInPc1d1	kódovaný
introny	intron	k1gInPc1	intron
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vystřiženy	vystřihnout	k5eAaPmNgInP	vystřihnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
správný	správný	k2eAgInSc1d1	správný
protein	protein	k1gInSc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
Sestřihu	sestřih	k1gInSc2	sestřih
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
jaderné	jaderný	k2eAgInPc1d1	jaderný
enzymové	enzymový	k2eAgInPc1d1	enzymový
komplexy	komplex	k1gInPc1	komplex
zvané	zvaný	k2eAgInPc1d1	zvaný
spliceozomy	spliceozom	k1gInPc1	spliceozom
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
pět	pět	k4xCc4	pět
malých	malá	k1gFnPc2	malá
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
asociovány	asociovat	k5eAaBmNgInP	asociovat
s	s	k7c7	s
proteiny	protein	k1gInPc7	protein
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
snRNPs	snRNPs	k6eAd1	snRNPs
(	(	kIx(	(
<g/>
small	smalnout	k5eAaPmAgMnS	smalnout
nuclear	nuclear	k1gMnSc1	nuclear
ribonucleoprotein	ribonucleoprotein	k1gMnSc1	ribonucleoprotein
particles	particles	k1gMnSc1	particles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
snRNPs	snRNPs	k6eAd1	snRNPs
rozpoznávají	rozpoznávat	k5eAaImIp3nP	rozpoznávat
sekvenci	sekvence	k1gFnSc4	sekvence
AGGU	AGGU	kA	AGGU
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
exonem	exon	k1gMnSc7	exon
a	a	k8xC	a
intronem	intron	k1gMnSc7	intron
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
důsledků	důsledek	k1gInPc2	důsledek
-	-	kIx~	-
možnost	možnost	k1gFnSc4	možnost
alternativního	alternativní	k2eAgInSc2d1	alternativní
splicingu	splicing	k1gInSc2	splicing
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc4	možnost
vytvořit	vytvořit	k5eAaPmF	vytvořit
různé	různý	k2eAgInPc4d1	různý
mRNA	mRNA	k?	mRNA
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
genu	gen	k1gInSc2	gen
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
a	a	k8xC	a
rekombinaci	rekombinace	k1gFnSc4	rekombinace
mezi	mezi	k7c7	mezi
exony	exon	k1gInPc7	exon
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
evoluce	evoluce	k1gFnSc2	evoluce
genů	gen	k1gInPc2	gen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
tkáních	tkáň	k1gFnPc6	tkáň
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
editaci	editace	k1gFnSc3	editace
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
délka	délka	k1gFnSc1	délka
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
konec	konec	k1gInSc1	konec
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
přidána	přidán	k2eAgFnSc1d1	přidána
sekvence	sekvence	k1gFnSc1	sekvence
asi	asi	k9	asi
200	[number]	k4	200
adenylových	adenylový	k2eAgInPc2d1	adenylový
zbytků	zbytek	k1gInPc2	zbytek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
konec	konec	k1gInSc1	konec
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
chrání	chránit	k5eAaImIp3nS	chránit
molekulu	molekula	k1gFnSc4	molekula
RNA	RNA	kA	RNA
před	před	k7c7	před
enzymatickým	enzymatický	k2eAgNnSc7d1	enzymatické
rozložením	rozložení	k1gNnSc7	rozložení
exonukleázami	exonukleáza	k1gFnPc7	exonukleáza
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
transport	transport	k1gInSc4	transport
mRNA	mRNA	k?	mRNA
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
polyadenylaci	polyadenylace	k1gFnSc3	polyadenylace
dochází	docházet	k5eAaImIp3nS	docházet
hned	hned	k6eAd1	hned
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odpojení	odpojení	k1gNnSc6	odpojení
syntetizujících	syntetizující	k2eAgInPc2d1	syntetizující
enzymů	enzym	k1gInPc2	enzym
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
mRNA	mRNA	k?	mRNA
50-250	[number]	k4	50-250
adenylových	adenylový	k2eAgInPc2d1	adenylový
zbytků	zbytek	k1gInPc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Poly-A	Poly-A	k?	Poly-A
konec	konec	k1gInSc1	konec
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
biotechnologii	biotechnologie	k1gFnSc6	biotechnologie
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
mRNA	mRNA	k?	mRNA
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
molekul	molekula	k1gFnPc2	molekula
-	-	kIx~	-
mRNA	mRNA	k?	mRNA
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
knihoven	knihovna	k1gFnPc2	knihovna
cDNA	cDNA	k?	cDNA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
na	na	k7c4	na
trifosfát	trifosfát	k1gInSc4	trifosfát
naváže	navázat	k5eAaPmIp3nS	navázat
zbytek	zbytek	k1gInSc4	zbytek
7	[number]	k4	7
<g/>
-methylguanosinu	ethylguanosina	k1gFnSc4	-methylguanosina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
5	[number]	k4	5
<g/>
'	'	kIx"	'
čepička	čepička	k1gFnSc1	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
modifikace	modifikace	k1gFnSc1	modifikace
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
rozpoznání	rozpoznání	k1gNnSc4	rozpoznání
mRNA	mRNA	k?	mRNA
ribozomem	ribozom	k1gInSc7	ribozom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
malá	malý	k2eAgFnSc1d1	malá
podjednotka	podjednotka	k1gFnSc1	podjednotka
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
právě	právě	k9	právě
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
navázání	navázání	k1gNnSc6	navázání
prochází	procházet	k5eAaImIp3nS	procházet
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nenajde	najít	k5eNaPmIp3nS	najít
startovní	startovní	k2eAgInSc1d1	startovní
AUG	AUG	kA	AUG
kodón	kodón	k1gInSc1	kodón
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgMnSc2	který
začne	začít	k5eAaPmIp3nS	začít
translaci	translace	k1gFnSc4	translace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
kodony	kodon	k1gInPc1	kodon
AUG	AUG	kA	AUG
už	už	k6eAd1	už
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
translace	translace	k1gFnSc2	translace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
konci	konec	k1gInSc6	konec
není	být	k5eNaImIp3nS	být
přidána	přidat	k5eAaPmNgFnS	přidat
žádná	žádný	k3yNgFnSc1	žádný
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
struktura	struktura	k1gFnSc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Ribozom	Ribozom	k1gInSc1	Ribozom
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
počátek	počátek	k1gInSc4	počátek
translace	translace	k1gFnSc2	translace
podle	podle	k7c2	podle
speciální	speciální	k2eAgFnSc2d1	speciální
sekvence	sekvence	k1gFnSc2	sekvence
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
kolem	kolem	k7c2	kolem
6	[number]	k4	6
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
několik	několik	k4yIc4	několik
nukleotidů	nukleotid	k1gInPc2	nukleotid
za	za	k7c4	za
AUG	AUG	kA	AUG
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
se	se	k3xPyFc4	se
na	na	k7c4	na
speciální	speciální	k2eAgNnPc4d1	speciální
místa	místo	k1gNnPc4	místo
ribozomu	ribozom	k1gInSc2	ribozom
a	a	k8xC	a
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
tak	tak	k6eAd1	tak
translaci	translace	k1gFnSc4	translace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bakteriím	bakterie	k1gFnPc3	bakterie
vytvářet	vytvářet	k5eAaImF	vytvářet
polycistronickou	polycistronický	k2eAgFnSc4d1	polycistronický
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kóduje	kódovat	k5eAaBmIp3nS	kódovat
několik	několik	k4yIc4	několik
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
eukaryotní	eukaryotní	k2eAgFnSc2d1	eukaryotní
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jediný	jediný	k2eAgInSc4d1	jediný
protein	protein	k1gInSc4	protein
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
monocystronická	monocystronický	k2eAgNnPc1d1	monocystronický
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
mRNA	mRNA	k?	mRNA
prochází	procházet	k5eAaImIp3nS	procházet
jadernými	jaderný	k2eAgInPc7d1	jaderný
póry	pór	k1gInPc7	pór
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
cytosolu	cytosol	k1gInSc2	cytosol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
překladu	překlad	k1gInSc3	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
postrádají	postrádat	k5eAaImIp3nP	postrádat
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
vznikající	vznikající	k2eAgFnSc3d1	vznikající
mRNA	mRNA	k?	mRNA
mohou	moct	k5eAaImIp3nP	moct
vázat	vázat	k5eAaImF	vázat
ribozomy	ribozom	k1gInPc7	ribozom
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
transkripce	transkripce	k1gFnSc2	transkripce
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
tak	tak	k9	tak
polyzomy	polyzom	k1gInPc4	polyzom
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
syntézu	syntéza	k1gFnSc4	syntéza
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
složitější	složitý	k2eAgFnSc4d2	složitější
úpravu	úprava	k1gFnSc4	úprava
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
jádro	jádro	k1gNnSc1	jádro
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
translaci	translace	k1gFnSc3	translace
z	z	k7c2	z
mRNA	mRNA	k?	mRNA
do	do	k7c2	do
bílkovin	bílkovina	k1gFnPc2	bílkovina
na	na	k7c6	na
ribozomu	ribozom	k1gInSc6	ribozom
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
konci	konec	k1gInSc3	konec
a	a	k8xC	a
přikládají	přikládat	k5eAaImIp3nP	přikládat
aminokyselinové	aminokyselinový	k2eAgInPc1d1	aminokyselinový
zbytky	zbytek	k1gInPc1	zbytek
ke	k	k7c3	k
kodónům	kodón	k1gMnPc3	kodón
podle	podle	k7c2	podle
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
syntéza	syntéza	k1gFnSc1	syntéza
bílkoviny	bílkovina	k1gFnSc2	bílkovina
trvá	trvat	k5eAaImIp3nS	trvat
průměrně	průměrně	k6eAd1	průměrně
20-60	[number]	k4	20-60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
jediného	jediný	k2eAgInSc2d1	jediný
ribozomu	ribozom	k1gInSc2	ribozom
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
translace	translace	k1gFnSc1	translace
zdržována	zdržovat	k5eAaImNgFnS	zdržovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
na	na	k7c6	na
RNA	RNA	kA	RNA
dosedá	dosedat	k5eAaImIp3nS	dosedat
více	hodně	k6eAd2	hodně
ribozomů	ribozom	k1gInPc2	ribozom
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
struktura	struktura	k1gFnSc1	struktura
zvaná	zvaný	k2eAgFnSc1d1	zvaná
polyribozom	polyribozom	k1gInSc1	polyribozom
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
translaci	translace	k1gFnSc3	translace
dochází	docházet	k5eAaImIp3nS	docházet
buď	buď	k8xC	buď
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
nebo	nebo	k8xC	nebo
na	na	k7c6	na
hrubém	hrubý	k2eAgNnSc6d1	hrubé
endoplasmatickém	endoplasmatický	k2eAgNnSc6d1	endoplasmatické
retikulu	retikulum	k1gNnSc6	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
mRNA	mRNA	k?	mRNA
mají	mít	k5eAaImIp3nP	mít
jistou	jistý	k2eAgFnSc4d1	jistá
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jsou	být	k5eAaImIp3nP	být
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
nukleotidy	nukleotid	k1gInPc4	nukleotid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
následně	následně	k6eAd1	následně
recyklovány	recyklován	k2eAgFnPc4d1	recyklována
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
setrvání	setrvání	k1gNnSc2	setrvání
dané	daný	k2eAgFnSc2d1	daná
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
důležitá	důležitý	k2eAgFnSc1d1	důležitá
a	a	k8xC	a
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
dané	daný	k2eAgNnSc1d1	dané
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
genovou	genový	k2eAgFnSc4d1	genová
expresi	exprese	k1gFnSc4	exprese
<g/>
.	.	kIx.	.
</s>
<s>
Přežívání	přežívání	k1gNnSc1	přežívání
mRNA	mRNA	k?	mRNA
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
může	moct	k5eAaImIp3nS	moct
např.	např.	kA	např.
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
buňkách	buňka	k1gFnPc6	buňka
sahat	sahat	k5eAaImF	sahat
od	od	k7c2	od
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
buňkách	buňka	k1gFnPc6	buňka
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
degradace	degradace	k1gFnSc2	degradace
mRNA	mRNA	k?	mRNA
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
běžný	běžný	k2eAgInSc4d1	běžný
rozklad	rozklad	k1gInSc4	rozklad
nepodléhající	podléhající	k2eNgNnSc1d1	nepodléhající
žádným	žádný	k3yNgInPc3	žádný
stimulátorům	stimulátor	k1gInPc3	stimulátor
či	či	k8xC	či
represorům	represor	k1gInPc3	represor
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
degradace	degradace	k1gFnSc1	degradace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
např.	např.	kA	např.
na	na	k7c4	na
stimuly	stimul	k1gInPc4	stimul
z	z	k7c2	z
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
různé	různý	k2eAgFnPc1d1	různá
degradační	degradační	k2eAgFnPc1d1	degradační
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zodpovídají	zodpovídat	k5eAaPmIp3nP	zodpovídat
za	za	k7c4	za
odstranění	odstranění	k1gNnSc4	odstranění
vadných	vadný	k2eAgInPc2d1	vadný
či	či	k8xC	či
poškozených	poškozený	k2eAgInPc2d1	poškozený
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
rysem	rys	k1gInSc7	rys
degradace	degradace	k1gFnSc2	degradace
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
hlídání	hlídání	k1gNnSc4	hlídání
délky	délka	k1gFnSc2	délka
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
ocásku	ocásek	k1gInSc2	ocásek
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
konci	konec	k1gInSc6	konec
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
zkracován	zkracovat	k5eAaImNgMnS	zkracovat
deadenylázami	deadenyláza	k1gFnPc7	deadenyláza
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
mimo	mimo	k7c4	mimo
poly	pola	k1gFnPc4	pola
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
konec	konec	k1gInSc4	konec
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
U	U	kA	U
bohaté	bohatý	k2eAgInPc1d1	bohatý
elementy	element	k1gInPc1	element
(	(	kIx(	(
<g/>
ARE	ar	k1gInSc5	ar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
konci	konec	k1gInSc6	konec
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
antisense	antisense	k1gFnSc2	antisense
RNA	RNA	kA	RNA
a	a	k8xC	a
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vznikají	vznikat	k5eAaImIp3nP	vznikat
úseky	úsek	k1gInPc7	úsek
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
komplementární	komplementární	k2eAgFnPc1d1	komplementární
k	k	k7c3	k
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
navážou	navázat	k5eAaPmIp3nP	navázat
<g/>
,	,	kIx,	,
buňka	buňka	k1gFnSc1	buňka
je	on	k3xPp3gNnSc4	on
rozloží	rozložit	k5eAaPmIp3nS	rozložit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
danou	daný	k2eAgFnSc4d1	daná
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
zastaví	zastavit	k5eAaPmIp3nS	zastavit
se	se	k3xPyFc4	se
syntéza	syntéza	k1gFnSc1	syntéza
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pochod	pochod	k1gInSc1	pochod
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládání	rozkládání	k1gNnSc1	rozkládání
dvoušroubovicové	dvoušroubovicový	k2eAgFnSc2d1	dvoušroubovicová
RNA	RNA	kA	RNA
často	často	k6eAd1	často
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
RNA	RNA	kA	RNA
virům	vir	k1gInPc3	vir
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
šroubovici	šroubovice	k1gFnSc4	šroubovice
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
