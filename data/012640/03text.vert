<p>
<s>
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Sitting	Sitting	k1gInSc1	Sitting
Bull	bulla	k1gFnPc2	bulla
<g/>
,	,	kIx,	,
lakotsky	lakotsky	k6eAd1	lakotsky
Tȟ	Tȟ	k1gFnSc1	Tȟ
Íyotake	Íyotake	k1gFnSc1	Íyotake
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
River	Rivra	k1gFnPc2	Rivra
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Standing	Standing	k1gInSc1	Standing
Rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
siouxský	siouxský	k1gMnSc1	siouxský
náčelník	náčelník	k1gMnSc1	náčelník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
wičháša	wičháša	k1gFnSc1	wičháša
wakȟ	wakȟ	k?	wakȟ
(	(	kIx(	(
<g/>
svatý	svatý	k1gMnSc1	svatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
šaman	šaman	k1gMnSc1	šaman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
lakotského	lakotský	k2eAgInSc2d1	lakotský
kmene	kmen	k1gInSc2	kmen
Hunkpapů	Hunkpap	k1gInPc2	Hunkpap
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
Indiány	Indián	k1gMnPc4	Indián
a	a	k8xC	a
během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
siouxské	siouxský	k2eAgFnSc2d1	siouxský
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
válečným	válečný	k2eAgInSc7d1	válečný
náčelníkem	náčelník	k1gInSc7	náčelník
spojených	spojený	k2eAgFnPc2d1	spojená
siouxských	siouxská	k1gFnPc2	siouxská
a	a	k8xC	a
šajenských	šajenský	k2eAgInPc2d1	šajenský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
roku	rok	k1gInSc3	rok
1876	[number]	k4	1876
přiměly	přimět	k5eAaPmAgFnP	přimět
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
armádu	armáda	k1gFnSc4	armáda
USA	USA	kA	USA
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Rosebudu	Rosebud	k1gInSc2	Rosebud
a	a	k8xC	a
následně	následně	k6eAd1	následně
drtivě	drtivě	k6eAd1	drtivě
porazili	porazit	k5eAaPmAgMnP	porazit
7	[number]	k4	7
<g/>
.	.	kIx.	.
kavalérii	kavalérie	k1gFnSc4	kavalérie
podplukovníka	podplukovník	k1gMnSc2	podplukovník
Custera	Custer	k1gMnSc2	Custer
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Little	Little	k1gFnSc2	Little
Bighornu	Bighorn	k1gInSc2	Bighorn
<g/>
.	.	kIx.	.
</s>
<s>
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
symbolem	symbol	k1gInSc7	symbol
posledního	poslední	k2eAgInSc2d1	poslední
odporu	odpor	k1gInSc2	odpor
Indiánů	Indián	k1gMnPc2	Indián
z	z	k7c2	z
plání	pláň	k1gFnPc2	pláň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
vzestup	vzestup	k1gInSc1	vzestup
==	==	k?	==
</s>
</p>
<p>
<s>
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
náčelníka	náčelník	k1gMnSc2	náčelník
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
lakotštině	lakotština	k1gFnSc6	lakotština
zní	znět	k5eAaImIp3nS	znět
Tĥ	Tĥ	k1gFnSc1	Tĥ
Íyotake	Íyotake	k1gFnSc1	Íyotake
(	(	kIx(	(
<g/>
Tatanka	Tatanka	k1gFnSc1	Tatanka
Iotanka	Iotanka	k1gFnSc1	Iotanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vychován	vychovat	k5eAaPmNgInS	vychovat
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
svými	svůj	k3xOyFgMnPc7	svůj
strýci	strýc	k1gMnPc7	strýc
v	v	k7c4	v
obratného	obratný	k2eAgMnSc4d1	obratný
lovce	lovec	k1gMnSc4	lovec
a	a	k8xC	a
bojovníka	bojovník	k1gMnSc4	bojovník
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
složil	složit	k5eAaPmAgMnS	složit
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgMnSc2	první
bizona	bizon	k1gMnSc2	bizon
a	a	k8xC	a
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
poprvé	poprvé	k6eAd1	poprvé
zabil	zabít	k5eAaPmAgMnS	zabít
nepřítele	nepřítel	k1gMnSc4	nepřítel
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
jeho	on	k3xPp3gInSc4	on
skalp	skalp	k1gInSc4	skalp
<g/>
.	.	kIx.	.
</s>
<s>
Hrdý	Hrdý	k1gMnSc1	Hrdý
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
daroval	darovat	k5eAaPmAgMnS	darovat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Splašený	splašený	k2eAgMnSc1d1	splašený
býk	býk	k1gMnSc1	býk
<g/>
.	.	kIx.	.
</s>
<s>
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
postupně	postupně	k6eAd1	postupně
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyniká	vynikat	k5eAaImIp3nS	vynikat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
čtyřech	čtyři	k4xCgFnPc6	čtyři
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Lakotové	lakota	k1gMnPc1	lakota
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
<g/>
:	:	kIx,	:
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
,	,	kIx,	,
štědrosti	štědrost	k1gFnSc2	štědrost
<g/>
,	,	kIx,	,
moudrosti	moudrost	k1gFnSc3	moudrost
a	a	k8xC	a
síle	síla	k1gFnSc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
tajných	tajný	k2eAgInPc2d1	tajný
elitních	elitní	k2eAgInPc2d1	elitní
válečnických	válečnický	k2eAgInPc2d1	válečnický
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
Statečná	statečný	k2eAgNnPc4d1	statečné
srdce	srdce	k1gNnPc4	srdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
veřejné	veřejný	k2eAgMnPc4d1	veřejný
kmenové	kmenový	k2eAgMnPc4d1	kmenový
funkcionáře	funkcionář	k1gMnPc4	funkcionář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
oblečení	oblečení	k1gNnSc1	oblečení
v	v	k7c6	v
košilích	košile	k1gFnPc6	košile
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Války	válek	k1gInPc4	válek
==	==	k?	==
</s>
</p>
<p>
<s>
Vedl	vést	k5eAaImAgInS	vést
často	často	k6eAd1	často
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
bělochům	běloch	k1gMnPc3	běloch
(	(	kIx(	(
<g/>
lakotsky	lakotsky	k6eAd1	lakotsky
wašíču	wašíču	k6eAd1	wašíču
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
proti	proti	k7c3	proti
tradičním	tradiční	k2eAgMnPc3d1	tradiční
indiánským	indiánský	k2eAgMnPc3d1	indiánský
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Arikarové	Arikar	k1gMnPc1	Arikar
<g/>
,	,	kIx,	,
Vraní	vraní	k2eAgMnPc1d1	vraní
Indiáni	Indián	k1gMnPc1	Indián
či	či	k8xC	či
Šošoni	Šošon	k1gMnPc1	Šošon
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Američany	Američan	k1gMnPc7	Američan
nepodepsal	podepsat	k5eNaPmAgMnS	podepsat
nikdy	nikdy	k6eAd1	nikdy
žádnou	žádný	k3yNgFnSc4	žádný
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
necítil	cítit	k5eNaImAgMnS	cítit
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
ani	ani	k8xC	ani
žádnou	žádný	k3yNgFnSc4	žádný
vázán	vázán	k2eAgInSc1d1	vázán
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
Laramie	Laramie	k1gFnSc2	Laramie
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
žádné	žádný	k3yNgFnPc4	žádný
výhody	výhoda	k1gFnPc4	výhoda
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
ani	ani	k8xC	ani
závazky	závazek	k1gInPc4	závazek
<g/>
)	)	kIx)	)
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
plynoucí	plynoucí	k2eAgFnSc2d1	plynoucí
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
tradičním	tradiční	k2eAgInSc6d1	tradiční
loveckém	lovecký	k2eAgInSc6d1	lovecký
a	a	k8xC	a
válečnickém	válečnický	k2eAgInSc6d1	válečnický
způsobu	způsob	k1gInSc6	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
představovalo	představovat	k5eAaImAgNnS	představovat
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
vlády	vláda	k1gFnSc2	vláda
porušení	porušení	k1gNnSc2	porušení
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlivní	vlivný	k2eAgMnPc1d1	vlivný
náčelníci	náčelník	k1gMnPc1	náčelník
velkých	velký	k2eAgInPc2d1	velký
kmenů	kmen	k1gInPc2	kmen
Bruléů	Brulé	k1gInPc2	Brulé
a	a	k8xC	a
Oglalů	Oglal	k1gInPc2	Oglal
Skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
ohon	ohon	k1gInSc4	ohon
a	a	k8xC	a
zejména	zejména	k9	zejména
Rudý	rudý	k2eAgInSc4d1	rudý
oblak	oblak	k1gInSc4	oblak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgMnS	vynutit
podmínky	podmínka	k1gFnPc4	podmínka
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
dotkli	dotknout	k5eAaPmAgMnP	dotknout
pera	pero	k1gNnSc2	pero
<g/>
"	"	kIx"	"
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
celého	celý	k2eAgInSc2d1	celý
národa	národ	k1gInSc2	národ
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
za	za	k7c4	za
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
Lakoty	lakota	k1gMnPc4	lakota
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
záminka	záminka	k1gFnSc1	záminka
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
válku	válka	k1gFnSc4	válka
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
siouxská	siouxský	k2eAgFnSc1d1	siouxský
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
Black	Black	k1gInSc4	Black
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
posvátných	posvátný	k2eAgFnPc6d1	posvátná
horách	hora	k1gFnPc6	hora
na	na	k7c6	na
území	území	k1gNnSc6	území
Velké	velký	k2eAgFnSc2d1	velká
siouxské	siouxský	k2eAgFnSc2d1	siouxský
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
objeveno	objeven	k2eAgNnSc1d1	objeveno
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
Vláda	vláda	k1gFnSc1	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
snaze	snaha	k1gFnSc6	snaha
přimět	přimět	k5eAaPmF	přimět
Lakoty	lakota	k1gFnPc4	lakota
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
prosadit	prosadit	k5eAaPmF	prosadit
své	svůj	k3xOyFgInPc4	svůj
požadavky	požadavek	k1gInPc4	požadavek
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1875	[number]	k4	1875
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
všechny	všechen	k3xTgMnPc4	všechen
Indiány	Indián	k1gMnPc4	Indián
dlící	dlící	k2eAgInSc1d1	dlící
mimo	mimo	k7c4	mimo
rezervaci	rezervace	k1gFnSc4	rezervace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
hlásili	hlásit	k5eAaImAgMnP	hlásit
u	u	k7c2	u
svých	svůj	k3xOyFgFnPc2	svůj
rezervačních	rezervační	k2eAgFnPc2d1	rezervační
správ	správa	k1gFnPc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Stěhování	stěhování	k1gNnSc1	stěhování
táborů	tábor	k1gInPc2	tábor
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nesmírně	smírně	k6eNd1	smírně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Indiáni	Indián	k1gMnPc1	Indián
pochopitelně	pochopitelně	k6eAd1	pochopitelně
příkazu	příkaz	k1gInSc2	příkaz
neuposlechli	uposlechnout	k5eNaPmAgMnP	uposlechnout
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
proto	proto	k8xC	proto
hned	hned	k6eAd1	hned
po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
termínu	termín	k1gInSc2	termín
dostala	dostat	k5eAaPmAgFnS	dostat
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
přivést	přivést	k5eAaPmF	přivést
je	on	k3xPp3gMnPc4	on
do	do	k7c2	do
rezervace	rezervace	k1gFnSc2	rezervace
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
zásah	zásah	k1gInSc1	zásah
však	však	k9	však
vedl	vést	k5eAaImAgInS	vést
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
umírnění	umírnění	k1gNnPc2	umírnění
Indiáni	Indián	k1gMnPc1	Indián
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
opouštět	opouštět	k5eAaImF	opouštět
rezervaci	rezervace	k1gFnSc4	rezervace
a	a	k8xC	a
připojovali	připojovat	k5eAaImAgMnP	připojovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
svobodným	svobodný	k2eAgMnPc3d1	svobodný
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Sdružovali	sdružovat	k5eAaImAgMnP	sdružovat
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
Sedícího	sedící	k2eAgMnSc2d1	sedící
býka	býk	k1gMnSc2	býk
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
zvolili	zvolit	k5eAaPmAgMnP	zvolit
svým	svůj	k3xOyFgInSc7	svůj
hlavním	hlavní	k2eAgInSc7d1	hlavní
náčelníkem	náčelník	k1gInSc7	náčelník
a	a	k8xC	a
podřídili	podřídit	k5eAaPmAgMnP	podřídit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
se	se	k3xPyFc4	se
asi	asi	k9	asi
sedm	sedm	k4xCc1	sedm
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Siouxů	Sioux	k1gInPc2	Sioux
a	a	k8xC	a
Šajenů	Šajen	k1gInPc2	Šajen
<g/>
,	,	kIx,	,
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
plus	plus	k6eAd1	plus
jejich	jejich	k3xOp3gFnSc2	jejich
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1876	[number]	k4	1876
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Rosebudu	Rosebud	k1gInSc2	Rosebud
přimělo	přimět	k5eAaPmAgNnS	přimět
1500	[number]	k4	1500
bojovníků	bojovník	k1gMnPc2	bojovník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Splašeného	splašený	k2eAgMnSc2d1	splašený
koně	kůň	k1gMnSc2	kůň
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
oddíl	oddíl	k1gInSc1	oddíl
tříhvězdičkového	tříhvězdičkový	k2eAgMnSc2d1	tříhvězdičkový
generála	generál	k1gMnSc2	generál
Crooka	Crooek	k1gMnSc2	Crooek
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
uštědřily	uštědřit	k5eAaPmAgInP	uštědřit
spojené	spojený	k2eAgInPc1d1	spojený
kmeny	kmen	k1gInPc1	kmen
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
7	[number]	k4	7
<g/>
.	.	kIx.	.
kavalérii	kavalérie	k1gFnSc4	kavalérie
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
podplukovníka	podplukovník	k1gMnSc2	podplukovník
Custera	Custer	k1gMnSc2	Custer
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Little	Little	k1gFnSc2	Little
Bighornu	Bighorn	k1gInSc2	Bighorn
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
zničen	zničit	k5eAaPmNgInS	zničit
hlavní	hlavní	k2eAgInSc1d1	hlavní
oddíl	oddíl	k1gInSc1	oddíl
vedený	vedený	k2eAgInSc1d1	vedený
samotným	samotný	k2eAgInSc7d1	samotný
Custerem	Custer	k1gInSc7	Custer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
měl	mít	k5eAaImAgInS	mít
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
vidění	vidění	k1gNnSc2	vidění
velkého	velký	k2eAgNnSc2d1	velké
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
napadnou	napadnout	k5eAaPmIp3nP	napadnout
jejich	jejich	k3xOp3gInSc1	jejich
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značně	značně	k6eAd1	značně
posílilo	posílit	k5eAaPmAgNnS	posílit
bojovou	bojový	k2eAgFnSc4d1	bojová
morálku	morálka	k1gFnSc4	morálka
shromážděných	shromážděný	k2eAgMnPc2d1	shromážděný
Indiánů	Indián	k1gMnPc2	Indián
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
vize	vize	k1gFnSc2	vize
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
i	i	k9	i
varování	varování	k1gNnSc1	varování
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmí	smět	k5eNaImIp3nS	smět
zabitým	zabitý	k2eAgMnPc3d1	zabitý
vojákům	voják	k1gMnPc3	voják
nic	nic	k6eAd1	nic
brát	brát	k5eAaImF	brát
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
zohavit	zohavit	k5eAaPmF	zohavit
jejich	jejich	k3xOp3gNnPc4	jejich
těla	tělo	k1gNnPc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
však	však	k9	však
varování	varování	k1gNnSc4	varování
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
názorů	názor	k1gInPc2	názor
některých	některý	k3yIgMnPc2	některý
soukmenovců	soukmenovec	k1gMnPc2	soukmenovec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
konečné	konečný	k2eAgFnSc3d1	konečná
porážce	porážka	k1gFnSc3	porážka
a	a	k8xC	a
uvržení	uvržení	k1gNnSc3	uvržení
národa	národ	k1gInSc2	národ
do	do	k7c2	do
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
bitvy	bitva	k1gFnSc2	bitva
aktivně	aktivně	k6eAd1	aktivně
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
ji	on	k3xPp3gFnSc4	on
nikterak	nikterak	k6eAd1	nikterak
neřídil	řídit	k5eNaImAgMnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
ochranu	ochrana	k1gFnSc4	ochrana
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
boj	boj	k1gInSc1	boj
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
přenechal	přenechat	k5eAaPmAgMnS	přenechat
mladším	mladý	k2eAgMnSc7d2	mladší
bojovníkům	bojovník	k1gMnPc3	bojovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stanuli	stanout	k5eAaPmAgMnP	stanout
zejména	zejména	k9	zejména
Splašený	splašený	k2eAgMnSc1d1	splašený
kůň	kůň	k1gMnSc1	kůň
(	(	kIx(	(
<g/>
Tašunka	Tašunka	k1gFnSc1	Tašunka
witkó	witkó	k?	witkó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žluč	žluč	k1gFnSc1	žluč
(	(	kIx(	(
<g/>
Pizi	Pizi	k1gNnSc1	Pizi
<g/>
)	)	kIx)	)
a	a	k8xC	a
náčelník	náčelník	k1gMnSc1	náčelník
severních	severní	k2eAgMnPc2d1	severní
Šajenů	Šajen	k1gMnPc2	Šajen
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
protiútok	protiútok	k1gInSc1	protiútok
však	však	k9	však
byl	být	k5eAaImAgInS	být
především	především	k9	především
spíše	spíše	k9	spíše
spontánní	spontánní	k2eAgFnSc7d1	spontánní
obrannou	obranný	k2eAgFnSc7d1	obranná
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
nájezd	nájezd	k1gInSc4	nájezd
Renových	Renův	k2eAgMnPc2d1	Renův
a	a	k8xC	a
Custerových	Custerův	k2eAgMnPc2d1	Custerův
kavaleristů	kavalerist	k1gMnPc2	kavalerist
a	a	k8xC	a
o	o	k7c6	o
nějaké	nějaký	k3yIgFnSc6	nějaký
větší	veliký	k2eAgFnSc6d2	veliký
organizaci	organizace	k1gFnSc6	organizace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Indiánů	Indián	k1gMnPc2	Indián
tedy	tedy	k8xC	tedy
nelze	lze	k6eNd1	lze
příliš	příliš	k6eAd1	příliš
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
spotřebovali	spotřebovat	k5eAaPmAgMnP	spotřebovat
Indiáni	Indián	k1gMnPc1	Indián
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc4	veškerý
střelivo	střelivo	k1gNnSc4	střelivo
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
střet	střet	k1gInSc4	střet
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byl	být	k5eAaImAgMnS	být
riskantní	riskantní	k2eAgMnSc1d1	riskantní
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
bojiště	bojiště	k1gNnSc2	bojiště
proto	proto	k8xC	proto
brzo	brzo	k6eAd1	brzo
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
ale	ale	k9	ale
pohromadě	pohromadě	k6eAd1	pohromadě
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
uživila	uživit	k5eAaPmAgFnS	uživit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozdělení	rozdělení	k1gNnSc3	rozdělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Porážka	porážka	k1gFnSc1	porážka
a	a	k8xC	a
život	život	k1gInSc1	život
v	v	k7c4	v
rezervaci	rezervace	k1gFnSc4	rezervace
==	==	k?	==
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
bitva	bitva	k1gFnSc1	bitva
však	však	k9	však
válku	válka	k1gFnSc4	válka
nevyhrává	vyhrávat	k5eNaImIp3nS	vyhrávat
a	a	k8xC	a
někdejší	někdejší	k2eAgMnSc1d1	někdejší
vítězové	vítěz	k1gMnPc1	vítěz
byli	být	k5eAaImAgMnP	být
nemilosrdně	milosrdně	k6eNd1	milosrdně
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyli	být	k5eNaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
pobiti	pobit	k2eAgMnPc1d1	pobit
nebo	nebo	k8xC	nebo
deportováni	deportován	k2eAgMnPc1d1	deportován
do	do	k7c2	do
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
000	[number]	k4	000
věrnými	věrný	k2eAgFnPc7d1	věrná
našel	najít	k5eAaPmAgMnS	najít
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
azyl	azyl	k1gInSc4	azyl
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vláda	vláda	k1gFnSc1	vláda
k	k	k7c3	k
místním	místní	k2eAgMnPc3d1	místní
Indiánům	Indián	k1gMnPc3	Indián
chovala	chovat	k5eAaImAgFnS	chovat
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
příchozím	příchozí	k1gMnPc3	příchozí
však	však	k9	však
neposkytla	poskytnout	k5eNaPmAgFnS	poskytnout
tytéž	týž	k3xTgFnPc4	týž
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
měly	mít	k5eAaImAgInP	mít
domácí	domácí	k2eAgInPc1d1	domácí
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlíci	uprchlík	k1gMnPc1	uprchlík
strádali	strádat	k5eAaImAgMnP	strádat
zimou	zima	k1gFnSc7	zima
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
po	po	k7c6	po
skupinkách	skupinka	k1gFnPc6	skupinka
vytráceli	vytrácet	k5eAaImAgMnP	vytrácet
domů	domů	k6eAd1	domů
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
úřadům	úřada	k1gMnPc3	úřada
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
přijmout	přijmout	k5eAaPmF	přijmout
život	život	k1gInSc4	život
v	v	k7c4	v
rezervaci	rezervace	k1gFnSc4	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Týpí	týpí	k1gNnSc1	týpí
musel	muset	k5eAaImAgInS	muset
vyměnit	vyměnit	k5eAaPmF	vyměnit
za	za	k7c4	za
srub	srub	k1gInSc4	srub
a	a	k8xC	a
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
rezervační	rezervační	k2eAgFnSc3d1	rezervační
správě	správa	k1gFnSc3	správa
ve	v	k7c4	v
Standing	Standing	k1gInSc4	Standing
Rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
řízené	řízený	k2eAgNnSc1d1	řízené
indiánským	indiánský	k2eAgInSc7d1	indiánský
agentem	agens	k1gInSc7	agens
McLaughlinem	McLaughlin	k1gInSc7	McLaughlin
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
Indiáni	Indián	k1gMnPc1	Indián
říkali	říkat	k5eAaImAgMnP	říkat
Šedý	šedý	k2eAgInSc4d1	šedý
vlas	vlas	k1gInSc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
Sedícím	sedící	k2eAgMnSc7d1	sedící
býkem	býk	k1gMnSc7	býk
nebyly	být	k5eNaImAgInP	být
vůbec	vůbec	k9	vůbec
dobré	dobré	k1gNnSc4	dobré
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgInSc4d1	slavný
a	a	k8xC	a
stále	stále	k6eAd1	stále
značně	značně	k6eAd1	značně
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
náčelník	náčelník	k1gMnSc1	náčelník
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nežádoucím	žádoucí	k2eNgInPc3d1	nežádoucí
potížistou	potížistý	k2eAgFnSc4d1	potížistý
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
autoritu	autorita	k1gFnSc4	autorita
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
podkopat	podkopat	k5eAaPmF	podkopat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
jezdil	jezdit	k5eAaImAgMnS	jezdit
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
s	s	k7c7	s
cirkusovou	cirkusový	k2eAgFnSc7d1	cirkusová
Wild	Wild	k1gInSc1	Wild
West	West	k1gMnSc1	West
Show	show	k1gFnSc1	show
Williama	William	k1gMnSc2	William
Codyho	Cody	k1gMnSc2	Cody
(	(	kIx(	(
<g/>
známějšího	známý	k2eAgNnSc2d2	známější
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Buffalo	Buffalo	k1gMnSc1	Buffalo
Bill	Bill	k1gMnSc1	Bill
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
show	show	k1gFnSc1	show
business	business	k1gInSc1	business
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
celkem	celkem	k6eAd1	celkem
líbil	líbit	k5eAaImAgInS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
mu	on	k3xPp3gMnSc3	on
platili	platit	k5eAaImAgMnP	platit
za	za	k7c4	za
podpis	podpis	k1gInSc4	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Nezapomněl	zapomenout	k5eNaPmAgMnS	zapomenout
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
lakotských	lakotský	k2eAgFnPc2d1	lakotská
vlastností	vlastnost	k1gFnPc2	vlastnost
–	–	k?	–
štědrost	štědrost	k1gFnSc4	štědrost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
většinu	většina	k1gFnSc4	většina
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vydělal	vydělat	k5eAaPmAgMnS	vydělat
<g/>
,	,	kIx,	,
rozdal	rozdat	k5eAaPmAgMnS	rozdat
<g/>
.	.	kIx.	.
</s>
<s>
Nechápal	chápat	k5eNaImAgMnS	chápat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohou	moct	k5eAaImIp3nP	moct
běloši	běloch	k1gMnPc1	běloch
nechávat	nechávat	k5eAaImF	nechávat
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
a	a	k8xC	a
zejména	zejména	k9	zejména
děti	dítě	k1gFnPc1	dítě
<g/>
)	)	kIx)	)
strádat	strádat	k5eAaImF	strádat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
kmeni	kmen	k1gInSc6	kmen
stát	stát	k5eAaPmF	stát
nemohlo	moct	k5eNaImAgNnS	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ostře	ostro	k6eAd1	ostro
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
odprodeji	odprodej	k1gInSc3	odprodej
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
Velké	velký	k2eAgFnSc2d1	velká
siouxské	siouxský	k2eAgFnSc2d1	siouxský
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
rozdrobení	rozdrobený	k2eAgMnPc1d1	rozdrobený
na	na	k7c4	na
izolovaná	izolovaný	k2eAgNnPc4d1	izolované
území	území	k1gNnPc4	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rezervačních	rezervační	k2eAgFnPc2d1	rezervační
správ	správa	k1gFnPc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nejednou	jednou	k6eNd1	jednou
podařilo	podařit	k5eAaPmAgNnS	podařit
jednání	jednání	k1gNnSc1	jednání
zhatit	zhatit	k5eAaPmF	zhatit
<g/>
,	,	kIx,	,
rozparcelování	rozparcelování	k1gNnSc1	rozparcelování
siouxského	siouxský	k2eAgNnSc2d1	siouxský
území	území	k1gNnSc2	území
nakonec	nakonec	k6eAd1	nakonec
zabránit	zabránit	k5eAaPmF	zabránit
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
rezervacích	rezervace	k1gFnPc6	rezervace
vyvolaných	vyvolaný	k2eAgInPc2d1	vyvolaný
novým	nový	k2eAgNnSc7d1	nové
náboženstvím	náboženství	k1gNnSc7	náboženství
Tance	tanec	k1gInSc2	tanec
duchů	duch	k1gMnPc2	duch
dostala	dostat	k5eAaPmAgFnS	dostat
indiánská	indiánský	k2eAgFnSc1d1	indiánská
policie	policie	k1gFnSc1	policie
ve	v	k7c4	v
Standing	Standing	k1gInSc4	Standing
Rocku	rock	k1gInSc2	rock
příkaz	příkaz	k1gInSc1	příkaz
Sedícího	sedící	k2eAgMnSc2d1	sedící
býka	býk	k1gMnSc2	býk
zatknout	zatknout	k5eAaPmF	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
aktu	akt	k1gInSc2	akt
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
šarvátce	šarvátka	k1gFnSc3	šarvátka
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
Sedící	sedící	k2eAgMnSc1d1	sedící
býk	býk	k1gMnSc1	býk
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
UTLEY	UTLEY	kA	UTLEY
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
M.	M.	kA	M.
Kopí	kopí	k1gNnSc1	kopí
a	a	k8xC	a
štít	štít	k1gInSc1	štít
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
Sedícího	sedící	k2eAgMnSc2d1	sedící
býka	býk	k1gMnSc2	býk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
angl.	angl.	k?	angl.
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Lance	lance	k1gNnSc1	lance
and	and	k?	and
the	the	k?	the
shield	shield	k1gInSc1	shield
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
life	lifat	k5eAaPmIp3nS	lifat
and	and	k?	and
times	times	k1gInSc1	times
of	of	k?	of
Sitting	Sitting	k1gInSc4	Sitting
Bull	bulla	k1gFnPc2	bulla
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Markéta	Markéta	k1gFnSc1	Markéta
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Indians	Indians	k1gInSc1	Indians
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
421	[number]	k4	421
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-85906-96-1	[number]	k4	80-85906-96-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sedící	sedící	k2eAgFnSc2d1	sedící
býk	býk	k1gMnSc1	býk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
Siouxů	Sioux	k1gInPc2	Sioux
proti	proti	k7c3	proti
stavbě	stavba	k1gFnSc3	stavba
ropovodu	ropovod	k1gInSc2	ropovod
u	u	k7c2	u
Standing	Standing	k1gInSc4	Standing
Rock	rock	k1gInSc1	rock
</s>
</p>
