<s>
Albatrosi	albatros	k1gMnPc1	albatros
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
