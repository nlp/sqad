<s>
Cval	cval	k1gInSc1	cval
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krokem	krok	k1gInSc7	krok
a	a	k8xC	a
klusem	klusem	k6eAd1	klusem
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
<g/>
,	,	kIx,	,
přirozených	přirozený	k2eAgInPc2d1	přirozený
chodů	chod	k1gInPc2	chod
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
skoků	skok	k1gInPc2	skok
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
třídobý	třídobý	k2eAgInSc1d1	třídobý
chod	chod	k1gInSc1	chod
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
a	a	k8xC	a
nejnamáhavějším	namáhavý	k2eAgInSc7d3	nejnamáhavější
chodem	chod	k1gInSc7	chod
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
koně	kůň	k1gMnPc1	kůň
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
cval	cval	k1gInSc4	cval
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
nohosled	nohosled	k1gInSc1	nohosled
<g/>
:	:	kIx,	:
pravá	pravý	k2eAgFnSc1d1	pravá
zadní	zadní	k2eAgFnSc1d1	zadní
-	-	kIx~	-
současně	současně	k6eAd1	současně
levá	levý	k2eAgFnSc1d1	levá
zadní	zadní	k2eAgFnSc1d1	zadní
a	a	k8xC	a
pravá	pravý	k2eAgFnSc1d1	pravá
přední	přední	k2eAgFnSc1d1	přední
-	-	kIx~	-
levá	levý	k2eAgFnSc1d1	levá
přední	přední	k2eAgFnSc1d1	přední
-	-	kIx~	-
fáze	fáze	k1gFnSc1	fáze
vznosu	vznos	k1gInSc2	vznos
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
končetina	končetina	k1gFnSc1	končetina
<g/>
)	)	kIx)	)
a	a	k8xC	a
cval	cval	k1gInSc1	cval
vpravo	vpravo	k6eAd1	vpravo
s	s	k7c7	s
nohosledem	nohosled	k1gInSc7	nohosled
<g/>
:	:	kIx,	:
levá	levý	k2eAgFnSc1d1	levá
zadní	zadní	k2eAgFnSc1d1	zadní
-	-	kIx~	-
současně	současně	k6eAd1	současně
pravá	pravý	k2eAgFnSc1d1	pravá
zadní	zadní	k2eAgFnSc1d1	zadní
a	a	k8xC	a
levá	levý	k2eAgFnSc1d1	levá
přední	přední	k2eAgFnSc1d1	přední
-	-	kIx~	-
pravá	pravý	k2eAgFnSc1d1	pravá
přední	přední	k2eAgFnSc1d1	přední
-	-	kIx~	-
fáze	fáze	k1gFnSc1	fáze
vznosu	vznos	k1gInSc2	vznos
<g/>
.	.	kIx.	.
</s>
<s>
Cval	cval	k1gInSc1	cval
je	být	k5eAaImIp3nS	být
nejrychlejší	rychlý	k2eAgMnSc1d3	nejrychlejší
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
chodů	chod	k1gInPc2	chod
koně	kůň	k1gMnSc2	kůň
-	-	kIx~	-
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
na	na	k7c6	na
jízdárně	jízdárna	k1gFnSc6	jízdárna
<g/>
,	,	kIx,	,
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dostizích	dostih	k1gInPc6	dostih
i	i	k9	i
50-60	[number]	k4	50-60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Při	při	k7c6	při
takto	takto	k6eAd1	takto
rychlém	rychlý	k2eAgInSc6d1	rychlý
cvalu	cval	k1gInSc6	cval
může	moct	k5eAaImIp3nS	moct
zadní	zadní	k2eAgFnSc1d1	zadní
noha	noha	k1gFnSc1	noha
dopadat	dopadat	k5eAaImF	dopadat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
přední	přední	k2eAgMnPc1d1	přední
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
cval	cval	k1gInSc1	cval
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
trysk	trysk	k1gInSc1	trysk
<g/>
.	.	kIx.	.
</s>
<s>
Druhem	druh	k1gInSc7	druh
cvalového	cvalový	k2eAgInSc2d1	cvalový
skoku	skok	k1gInSc2	skok
je	být	k5eAaImIp3nS	být
i	i	k9	i
skok	skok	k1gInSc4	skok
přes	přes	k7c4	přes
překážku	překážka	k1gFnSc4	překážka
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
překážky	překážka	k1gFnPc4	překážka
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
skákat	skákat	k5eAaImF	skákat
i	i	k9	i
v	v	k7c6	v
klusu	klus	k1gInSc6	klus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
jízdárně	jízdárna	k1gFnSc6	jízdárna
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
drezurních	drezurní	k2eAgFnPc6d1	drezurní
soutěžích	soutěž	k1gFnPc6	soutěž
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
pracovní	pracovní	k2eAgInSc4d1	pracovní
cval	cval	k1gInSc4	cval
<g/>
:	:	kIx,	:
základní	základní	k2eAgInSc4d1	základní
cval	cval	k1gInSc4	cval
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
jízdárně	jízdárna	k1gFnSc6	jízdárna
<g/>
,	,	kIx,	,
spíš	spíš	k9	spíš
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
takové	takový	k3xDgNnSc4	takový
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Shromážděný	shromážděný	k2eAgInSc1d1	shromážděný
cval	cval	k1gInSc1	cval
<g/>
:	:	kIx,	:
nejvznosnější	vznosný	k2eAgInSc1d3	vznosný
a	a	k8xC	a
nejpomalejší	pomalý	k2eAgInSc1d3	nejpomalejší
cval	cval	k1gInSc1	cval
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
co	co	k9	co
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
váhy	váha	k1gFnSc2	váha
nesou	nést	k5eAaImIp3nP	nést
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
záď	záď	k1gFnSc1	záď
je	být	k5eAaImIp3nS	být
snížená	snížený	k2eAgFnSc1d1	snížená
<g/>
,	,	kIx,	,
ohnutí	ohnutí	k1gNnSc1	ohnutí
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
budí	budit	k5eAaImIp3nS	budit
dojem	dojem	k1gInSc4	dojem
směru	směr	k1gInSc2	směr
do	do	k7c2	do
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc4d1	střední
cval	cval	k1gInSc4	cval
<g/>
:	:	kIx,	:
cvalové	cvalový	k2eAgInPc4d1	cvalový
skoky	skok	k1gInPc4	skok
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
fáze	fáze	k1gFnSc1	fáze
vznosu	vznos	k1gInSc2	vznos
trvá	trvat	k5eAaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
musí	muset	k5eAaImIp3nS	muset
celkově	celkově	k6eAd1	celkově
prodloužit	prodloužit	k5eAaPmF	prodloužit
rámec	rámec	k1gInSc4	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužený	prodloužený	k2eAgInSc4d1	prodloužený
cval	cval	k1gInSc4	cval
<g/>
:	:	kIx,	:
maximální	maximální	k2eAgNnSc4d1	maximální
možné	možný	k2eAgNnSc4d1	možné
prodloužení	prodloužení	k1gNnSc4	prodloužení
cvalových	cvalový	k2eAgInPc2d1	cvalový
skoků	skok	k1gInPc2	skok
<g/>
.	.	kIx.	.
</s>
<s>
Nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
cvalových	cvalový	k2eAgInPc2d1	cvalový
skoků	skok	k1gInPc2	skok
<g/>
.	.	kIx.	.
</s>
<s>
Kontracval	Kontracvat	k5eAaBmAgMnS	Kontracvat
je	být	k5eAaImIp3nS	být
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
cval	cval	k1gInSc4	cval
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
koně	kůň	k1gMnSc4	kůň
přirozené	přirozený	k2eAgInPc1d1	přirozený
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cval	cval	k1gInSc4	cval
vlevo	vlevo	k6eAd1	vlevo
na	na	k7c6	na
oblouku	oblouk	k1gInSc6	oblouk
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předvádí	předvádět	k5eAaImIp3nS	předvádět
se	se	k3xPyFc4	se
v	v	k7c6	v
drezurních	drezurní	k2eAgFnPc6d1	drezurní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Křižování	křižování	k1gNnSc1	křižování
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůň	kůň	k1gMnSc1	kůň
cválá	cválat	k5eAaImIp3nS	cválat
předními	přední	k2eAgFnPc7d1	přední
končetinami	končetina	k1gFnPc7	končetina
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
stranu	strana	k1gFnSc4	strana
než	než	k8xS	než
zadními	zadní	k2eAgFnPc7d1	zadní
-	-	kIx~	-
například	například	k6eAd1	například
levá	levý	k2eAgFnSc1d1	levá
zadní	zadní	k2eAgFnSc1d1	zadní
-	-	kIx~	-
pravá	pravý	k2eAgFnSc1d1	pravá
zadní	zadní	k2eAgFnSc1d1	zadní
+	+	kIx~	+
pravá	pravý	k2eAgFnSc1d1	pravá
přední	přední	k2eAgFnSc1d1	přední
-	-	kIx~	-
levá	levý	k2eAgFnSc1d1	levá
přední	přední	k2eAgFnSc1d1	přední
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
správně	správně	k6eAd1	správně
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
levá	levý	k2eAgFnSc1d1	levá
zadní	zadní	k2eAgFnSc1d1	zadní
-	-	kIx~	-
pravá	pravý	k2eAgFnSc1d1	pravá
zadní	zadní	k2eAgFnSc1d1	zadní
+	+	kIx~	+
levá	levý	k2eAgFnSc1d1	levá
přední	přední	k2eAgFnSc1d1	přední
-	-	kIx~	-
pravá	pravá	k1gFnSc1	pravá
přední	přední	k2eAgFnSc1d1	přední
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cval	cval	k1gInSc1	cval
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jezdce	jezdec	k1gMnSc4	jezdec
nepohodlný	pohodlný	k2eNgInSc1d1	nepohodlný
<g/>
,	,	kIx,	,
negativně	negativně	k6eAd1	negativně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
stabilitu	stabilita	k1gFnSc4	stabilita
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Odbornou	odborný	k2eAgFnSc7d1	odborná
a	a	k8xC	a
trpělivou	trpělivý	k2eAgFnSc7d1	trpělivá
prací	práce	k1gFnSc7	práce
na	na	k7c4	na
lonži	lonže	k1gFnSc4	lonže
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
nebo	nebo	k8xC	nebo
nepřiježděných	přiježděný	k2eNgMnPc2d1	nepřiježděný
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
