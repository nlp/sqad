<s>
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1895	[number]	k4	1895
Aspang	Aspanga	k1gFnPc2	Aspanga
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
Langen	Langen	k1gInSc4	Langen
u	u	k7c2	u
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
síťkař	síťkař	k1gMnSc1	síťkař
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
tašky	taška	k1gFnSc2	taška
síťovky	síťovka	k1gFnSc2	síťovka
<g/>
.	.	kIx.	.
</s>
<s>
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
síťovat	síťovat	k5eAaImF	síťovat
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc7	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
živit	živit	k5eAaImF	živit
rodinu	rodina	k1gFnSc4	rodina
výrobou	výroba	k1gFnSc7	výroba
síťovaných	síťovaný	k2eAgInPc2d1	síťovaný
čepců	čepec	k1gInPc2	čepec
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chudobě	chudoba	k1gFnSc3	chudoba
si	se	k3xPyFc3	se
nemohl	moct	k5eNaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
jít	jít	k5eAaImF	jít
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
na	na	k7c4	na
stolaře	stolař	k1gMnSc4	stolař
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
stal	stát	k5eAaPmAgMnS	stát
zemědělským	zemědělský	k2eAgMnSc7d1	zemědělský
dělníkem	dělník	k1gMnSc7	dělník
v	v	k7c6	v
panském	panský	k2eAgInSc6d1	panský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
fagot	fagot	k1gInSc4	fagot
u	u	k7c2	u
dechové	dechový	k2eAgFnSc2d1	dechová
hudby	hudba	k1gFnSc2	hudba
23	[number]	k4	23
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sanitním	sanitní	k2eAgMnSc7d1	sanitní
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
krční	krční	k2eAgFnSc7d1	krční
chorobou	choroba	k1gFnSc7	choroba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
trvalé	trvalý	k2eAgInPc4d1	trvalý
následky	následek	k1gInPc4	následek
mu	on	k3xPp3gMnSc3	on
zabránily	zabránit	k5eAaPmAgInP	zabránit
v	v	k7c6	v
setrvání	setrvání	k1gNnSc6	setrvání
u	u	k7c2	u
vojenské	vojenský	k2eAgFnSc2d1	vojenská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Zámku	zámek	k1gInSc2	zámek
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
obchodním	obchodní	k2eAgMnSc7d1	obchodní
zástupcem	zástupce	k1gMnSc7	zástupce
firmy	firma	k1gFnSc2	firma
JARO	jaro	k6eAd1	jaro
J.	J.	kA	J.
Rousek	rouska	k1gFnPc2	rouska
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
TOKOZ	TOKOZ	kA	TOKOZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
živnostenský	živnostenský	k2eAgInSc1d1	živnostenský
list	list	k1gInSc1	list
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
galanterního	galanterní	k2eAgNnSc2d1	galanterní
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
vlasových	vlasový	k2eAgFnPc2d1	vlasová
sítěk	síťka	k1gFnPc2	síťka
a	a	k8xC	a
navázal	navázat	k5eAaPmAgInS	navázat
osobní	osobní	k2eAgInPc4d1	osobní
kontakty	kontakt	k1gInPc4	kontakt
se	s	k7c7	s
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
odběrateli	odběratel	k1gMnPc7	odběratel
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
síťky	síťka	k1gFnSc2	síťka
prodával	prodávat	k5eAaImAgInS	prodávat
pod	pod	k7c7	pod
obchodní	obchodní	k2eAgFnSc7d1	obchodní
značkou	značka	k1gFnSc7	značka
SAARENSE	SAARENSE	kA	SAARENSE
(	(	kIx(	(
<g/>
EKV	EKV	kA	EKV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
v	v	k7c6	v
Zámku	zámek	k1gInSc6	zámek
Žďáře	Žďár	k1gInSc6	Žďár
výstavu	výstava	k1gFnSc4	výstava
vlasových	vlasový	k2eAgFnPc2d1	vlasová
sítěk	síťka	k1gFnPc2	síťka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
seznámila	seznámit	k5eAaPmAgFnS	seznámit
síťkařky	síťkařka	k1gFnPc4	síťkařka
s	s	k7c7	s
představami	představa	k1gFnPc7	představa
a	a	k8xC	a
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
klientely	klientela	k1gFnSc2	klientela
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
vyšly	vyjít	k5eAaPmAgFnP	vyjít
vlasové	vlasový	k2eAgFnPc1d1	vlasová
síťky	síťka	k1gFnPc1	síťka
z	z	k7c2	z
módy	móda	k1gFnSc2	móda
(	(	kIx(	(
<g/>
nastal	nastat	k5eAaPmAgInS	nastat
čas	čas	k1gInSc1	čas
krátkých	krátký	k2eAgInPc2d1	krátký
sestřihů	sestřih	k1gInPc2	sestřih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
konkurent	konkurent	k1gMnSc1	konkurent
–	–	k?	–
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
země	země	k1gFnSc1	země
vychrlila	vychrlit	k5eAaPmAgFnS	vychrlit
za	za	k7c4	za
rok	rok	k1gInSc4	rok
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
vlasových	vlasový	k2eAgFnPc2d1	vlasová
sítěk	síťka	k1gFnPc2	síťka
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
a	a	k8xC	a
přivedla	přivést	k5eAaPmAgFnS	přivést
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
cca	cca	kA	cca
98	[number]	k4	98
%	%	kIx~	%
naší	náš	k3xOp1gFnSc2	náš
síťkařské	síťkařský	k2eAgFnSc2d1	síťkařský
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
síťkovaných	síťkovaný	k2eAgFnPc2d1	síťkovaná
tašek	taška	k1gFnPc2	taška
(	(	kIx(	(
<g/>
síťovek	síťovka	k1gFnPc2	síťovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jeho	on	k3xPp3gInSc4	on
podnik	podnik	k1gInSc4	podnik
zachránily	zachránit	k5eAaPmAgFnP	zachránit
a	a	k8xC	a
dovedly	dovést	k5eAaPmAgFnP	dovést
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
prosperitě	prosperita	k1gFnSc3	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Ručně	ručně	k6eAd1	ručně
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
nákupní	nákupní	k2eAgFnPc1d1	nákupní
tašky	taška	k1gFnPc1	taška
již	již	k9	již
nebyly	být	k5eNaImAgFnP	být
zhotovovány	zhotovovat	k5eAaImNgFnP	zhotovovat
z	z	k7c2	z
vlasů	vlas	k1gInPc2	vlas
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
hlavní	hlavní	k2eAgFnPc1d1	hlavní
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
jedinou	jediný	k2eAgFnSc7d1	jediná
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vlasových	vlasový	k2eAgFnPc2d1	vlasová
sítěk	síťka	k1gFnPc2	síťka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
umělé	umělý	k2eAgFnSc2d1	umělá
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
příze	příz	k1gFnSc2	příz
<g/>
.	.	kIx.	.
</s>
<s>
Nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
budily	budit	k5eAaImAgInP	budit
u	u	k7c2	u
kupujících	kupující	k2eAgFnPc2d1	kupující
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byly	být	k5eAaImAgFnP	být
levné	levný	k2eAgFnPc1d1	levná
<g/>
,	,	kIx,	,
lehké	lehký	k2eAgFnPc1d1	lehká
a	a	k8xC	a
skladné	skladný	k2eAgFnPc1d1	skladná
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
si	se	k3xPyFc3	se
je	on	k3xPp3gNnPc4	on
zákazníci	zákazník	k1gMnPc1	zákazník
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
síťovky	síťovka	k1gFnPc1	síťovka
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
nákupní	nákupní	k2eAgFnPc1d1	nákupní
<g/>
,	,	kIx,	,
vycházkové	vycházkový	k2eAgFnPc1d1	vycházková
<g/>
,	,	kIx,	,
loketní	loketní	k2eAgFnPc1d1	loketní
<g/>
,	,	kIx,	,
ramenovky	ramenovka	k1gFnPc1	ramenovka
<g/>
,	,	kIx,	,
tlumokové	tlumokový	k2eAgFnPc1d1	tlumokový
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
a	a	k8xC	a
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
na	na	k7c4	na
dámská	dámský	k2eAgNnPc4d1	dámské
kola	kolo	k1gNnPc4	kolo
<g/>
;	;	kIx,	;
také	také	k6eAd1	také
na	na	k7c4	na
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Vavřín	vavřín	k1gInSc1	vavřín
Krčil	krčit	k5eAaImAgInS	krčit
patrně	patrně	k6eAd1	patrně
neměl	mít	k5eNaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
peníze	peníz	k1gInPc1	peníz
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
patentu	patent	k1gInSc2	patent
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
převratný	převratný	k2eAgInSc4d1	převratný
autorský	autorský	k2eAgInSc4d1	autorský
návrh	návrh	k1gInSc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pár	pár	k4xCyI	pár
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začaly	začít	k5eAaPmAgFnP	začít
síťovky	síťovka	k1gFnPc1	síťovka
vyrábět	vyrábět	k5eAaImF	vyrábět
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
za	za	k7c4	za
čas	čas	k1gInSc4	čas
doslova	doslova	k6eAd1	doslova
zaplavily	zaplavit	k5eAaPmAgInP	zaplavit
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Krčil	krčit	k5eAaImAgInS	krčit
je	on	k3xPp3gMnPc4	on
vyvážel	vyvážet	k5eAaImAgInS	vyvážet
mj.	mj.	kA	mj.
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
odběratelem	odběratel	k1gMnSc7	odběratel
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgMnPc3	který
však	však	k9	však
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přerušil	přerušit	k5eAaPmAgMnS	přerušit
obchodní	obchodní	k2eAgInPc4d1	obchodní
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
neváhal	váhat	k5eNaImAgMnS	váhat
rozšířit	rozšířit	k5eAaPmF	rozšířit
výrobu	výroba	k1gFnSc4	výroba
o	o	k7c4	o
hedvábné	hedvábný	k2eAgFnPc4d1	hedvábná
síťky	síťka	k1gFnPc4	síťka
na	na	k7c4	na
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
závoje	závoj	k1gInPc4	závoj
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
účesu	účes	k1gInSc2	účes
po	po	k7c6	po
ondulaci	ondulace	k1gFnSc6	ondulace
<g/>
,	,	kIx,	,
síťky	síťka	k1gFnPc1	síťka
na	na	k7c4	na
míče	míč	k1gInPc4	míč
a	a	k8xC	a
kuličky	kulička	k1gFnPc4	kulička
<g/>
,	,	kIx,	,
na	na	k7c4	na
tenisové	tenisový	k2eAgInPc4d1	tenisový
míčky	míček	k1gInPc4	míček
<g/>
,	,	kIx,	,
na	na	k7c4	na
míčky	míček	k1gInPc4	míček
pro	pro	k7c4	pro
ping-pong	pingong	k1gInSc4	ping-pong
<g/>
,	,	kIx,	,
na	na	k7c4	na
dámská	dámský	k2eAgNnPc4d1	dámské
kola	kolo	k1gNnPc4	kolo
<g/>
;	;	kIx,	;
nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
rybářským	rybářský	k2eAgNnSc7d1	rybářské
a	a	k8xC	a
tenisovým	tenisový	k2eAgNnSc7d1	tenisové
sítím	sítí	k1gNnSc7	sítí
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Leopoldovou	Leopoldův	k2eAgFnSc7d1	Leopoldova
ze	z	k7c2	z
Stržanova	Stržanův	k2eAgInSc2d1	Stržanův
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
oporou	opora	k1gFnSc7	opora
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
odbornicí	odbornice	k1gFnSc7	odbornice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
síťkování	síťkování	k1gNnSc2	síťkování
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
sběratelkou	sběratelka	k1gFnSc7	sběratelka
lidových	lidový	k2eAgFnPc2d1	lidová
tkanin	tkanina	k1gFnPc2	tkanina
na	na	k7c6	na
Horácku	Horácko	k1gNnSc6	Horácko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zachránila	zachránit	k5eAaPmAgFnS	zachránit
četné	četný	k2eAgInPc4d1	četný
vzácné	vzácný	k2eAgInPc4d1	vzácný
originály	originál	k1gInPc4	originál
tkanin	tkanina	k1gFnPc2	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
manželčina	manželčin	k2eAgInSc2d1	manželčin
domu	dům	k1gInSc2	dům
ve	v	k7c6	v
Stržanově	Stržanův	k2eAgInSc6d1	Stržanův
č.	č.	k?	č.
8	[number]	k4	8
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
řídil	řídit	k5eAaImAgMnS	řídit
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
obce	obec	k1gFnSc2	obec
převedl	převést	k5eAaPmAgInS	převést
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
také	také	k6eAd1	také
provozovnu	provozovna	k1gFnSc4	provozovna
a	a	k8xC	a
sklad	sklad	k1gInSc4	sklad
pro	pro	k7c4	pro
podomácké	podomácký	k2eAgNnSc4d1	podomácké
síťování	síťování	k1gNnSc4	síťování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
stal	stát	k5eAaPmAgInS	stát
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
SLUM	slum	k1gInSc1	slum
(	(	kIx(	(
<g/>
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
lidové	lidový	k2eAgNnSc1d1	lidové
umění	umění	k1gNnSc1	umění
moravské	moravský	k2eAgNnSc1d1	Moravské
<g/>
)	)	kIx)	)
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
Ústředí	ústředí	k1gNnPc2	ústředí
lidové	lidový	k2eAgFnSc2d1	lidová
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
ÚLUV	ÚLUV	kA	ÚLUV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
jehož	jehož	k3xOyRp3gFnSc4	jehož
národní	národní	k2eAgFnSc4d1	národní
správu	správa	k1gFnSc4	správa
přešel	přejít	k5eAaPmAgMnS	přejít
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
Krčilův	Krčilův	k2eAgInSc1d1	Krčilův
stržanovský	stržanovský	k2eAgInSc1d1	stržanovský
podnik	podnik	k1gInSc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ÚLUV	ÚLUV	kA	ÚLUV
pracoval	pracovat	k5eAaImAgInS	pracovat
také	také	k9	také
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
společnosti	společnost	k1gFnSc2	společnost
zřízení	zřízení	k1gNnSc4	zřízení
pracoviště	pracoviště	k1gNnSc2	pracoviště
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
se	se	k3xPyFc4	se
výrobě	výroba	k1gFnSc3	výroba
ručně	ručně	k6eAd1	ručně
necovaných	covaný	k2eNgInPc2d1	covaný
a	a	k8xC	a
vyšívaných	vyšívaný	k2eAgFnPc2d1	vyšívaná
záclon	záclona	k1gFnPc2	záclona
pro	pro	k7c4	pro
naše	náš	k3xOp1gMnPc4	náš
exkluzivní	exkluzivní	k2eAgMnPc4d1	exkluzivní
hotely	hotel	k1gInPc4	hotel
a	a	k8xC	a
lázně	lázeň	k1gFnPc4	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
pracoviště	pracoviště	k1gNnSc1	pracoviště
tehdy	tehdy	k6eAd1	tehdy
neexistovalo	existovat	k5eNaImAgNnS	existovat
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
vedlejší	vedlejší	k2eAgNnSc4d1	vedlejší
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
mnoha	mnoho	k4c2	mnoho
ženám	žena	k1gFnPc3	žena
z	z	k7c2	z
místního	místní	k2eAgInSc2d1	místní
JZD	JZD	kA	JZD
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
veřejný	veřejný	k2eAgInSc4d1	veřejný
kurz	kurz	k1gInSc4	kurz
síťování	síťování	k1gNnSc2	síťování
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc1	námět
vzorů	vzor	k1gInPc2	vzor
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
renomované	renomovaný	k2eAgFnPc1d1	renomovaná
výtvarnice	výtvarnice	k1gFnPc1	výtvarnice
(	(	kIx(	(
<g/>
Luba	Lub	k2eAgFnSc1d1	Luba
Krejčí	Krejčí	k1gFnSc1	Krejčí
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
Voštová-Reichlová	Voštová-Reichlová	k1gFnSc1	Voštová-Reichlová
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Sláčiková	Sláčikový	k2eAgFnSc1d1	Sláčiková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
předal	předat	k5eAaPmAgMnS	předat
vedení	vedení	k1gNnSc4	vedení
bystřického	bystřický	k2eAgNnSc2d1	bystřické
pracoviště	pracoviště	k1gNnSc2	pracoviště
"	"	kIx"	"
<g/>
Pernštejn	Pernštejn	k1gInSc4	Pernštejn
<g/>
"	"	kIx"	"
svému	svůj	k1gMnSc3	svůj
synu	syn	k1gMnSc3	syn
Oldřichovi	Oldřich	k1gMnSc3	Oldřich
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1928	[number]	k4	1928
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
,	,	kIx,	,
jezdil	jezdit	k5eAaImAgMnS	jezdit
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
NSR	NSR	kA	NSR
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
získávat	získávat	k5eAaImF	získávat
pro	pro	k7c4	pro
ART	ART	kA	ART
CENTRUM	centrum	k1gNnSc4	centrum
a	a	k8xC	a
UNICOOP	UNICOOP	kA	UNICOOP
exportní	exportní	k2eAgFnPc4d1	exportní
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
1966	[number]	k4	1966
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
výstavu	výstav	k1gInSc2	výstav
podomáckého	podomácký	k2eAgNnSc2d1	podomácké
síťování	síťování	k1gNnSc2	síťování
na	na	k7c6	na
Horácku	Horácko	k1gNnSc6	Horácko
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
exponáty	exponát	k1gInPc4	exponát
vybrali	vybrat	k5eAaPmAgMnP	vybrat
<g/>
,	,	kIx,	,
utřídili	utřídit	k5eAaPmAgMnP	utřídit
a	a	k8xC	a
instalovali	instalovat	k5eAaBmAgMnP	instalovat
manželé	manžel	k1gMnPc1	manžel
Krčilovi	Krčilův	k2eAgMnPc1d1	Krčilův
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
NSR	NSR	kA	NSR
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
Vavřín	Vavřín	k1gMnSc1	Vavřín
Krčil	krčit	k5eAaImAgMnS	krčit
v	v	k7c6	v
Langenu	Langen	k1gInSc6	Langen
u	u	k7c2	u
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
uloženy	uložit	k5eAaPmNgInP	uložit
na	na	k7c6	na
zelenohorském	zelenohorský	k2eAgInSc6d1	zelenohorský
hřbitově	hřbitov	k1gInSc6	hřbitov
ve	v	k7c6	v
Žďáře	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc4	pomník
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
Vavřína	Vavřín	k1gMnSc2	Vavřín
Krčila	krčit	k5eAaImAgFnS	krčit
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
rodinný	rodinný	k2eAgMnSc1d1	rodinný
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Břetislav	Břetislav	k1gMnSc1	Břetislav
Benda	Benda	k1gMnSc1	Benda
<g/>
.	.	kIx.	.
</s>
<s>
Pracoviště	pracoviště	k1gNnSc1	pracoviště
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
přemístil	přemístit	k5eAaPmAgMnS	přemístit
Oldřich	Oldřich	k1gMnSc1	Oldřich
Krčil	krčit	k5eAaImAgInS	krčit
do	do	k7c2	do
Víru	Vír	k1gInSc2	Vír
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
výrobní	výrobní	k2eAgInSc1d1	výrobní
sortiment	sortiment	k1gInSc1	sortiment
o	o	k7c4	o
závěsy	závěsa	k1gFnPc4	závěsa
zdobené	zdobený	k2eAgNnSc1d1	zdobené
vyvazovanou	vyvazovaný	k2eAgFnSc7d1	vyvazovaná
batikou	batika	k1gFnSc7	batika
z	z	k7c2	z
tvorby	tvorba	k1gFnSc2	tvorba
brněnské	brněnský	k2eAgFnSc2d1	brněnská
lidové	lidový	k2eAgFnSc2d1	lidová
výtvarnice	výtvarnice	k1gFnSc2	výtvarnice
Anny	Anna	k1gFnSc2	Anna
Orlové	Orlová	k1gFnSc2	Orlová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
nenadálé	nenadálý	k2eAgFnSc6d1	nenadálá
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
bylo	být	k5eAaImAgNnS	být
pracoviště	pracoviště	k1gNnSc1	pracoviště
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
lidová	lidový	k2eAgFnSc1d1	lidová
výroba	výroba	k1gFnSc1	výroba
tak	tak	k6eAd1	tak
zcela	zcela	k6eAd1	zcela
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
ručního	ruční	k2eAgNnSc2d1	ruční
síťkování	síťkování	k1gNnSc2	síťkování
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
věstník	věstník	k1gMnSc1	věstník
moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
,	,	kIx,	,
34	[number]	k4	34
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
s.	s.	k?	s.
166	[number]	k4	166
–	–	k?	–
177	[number]	k4	177
<g/>
.	.	kIx.	.
</s>
