<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
americké	americký	k2eAgFnSc2d1	americká
série	série	k1gFnSc2	série
devíti	devět	k4xCc2	devět
bezpilotních	bezpilotní	k2eAgFnPc2d1	bezpilotní
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
až	až	k9	až
1965	[number]	k4	1965
<g/>
?	?	kIx.	?
</s>
