<p>
<s>
Ranger	Ranger	k1gInSc4	Ranger
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
americké	americký	k2eAgFnSc2d1	americká
série	série	k1gFnSc2	série
devíti	devět	k4xCc2	devět
bezpilotních	bezpilotní	k2eAgFnPc2d1	bezpilotní
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
až	až	k9	až
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cíl	cíl	k1gInSc1	cíl
programu	program	k1gInSc2	program
==	==	k?	==
</s>
</p>
<p>
<s>
Program	program	k1gInSc4	program
byl	být	k5eAaImAgMnS	být
projektem	projekt	k1gInSc7	projekt
Národního	národní	k2eAgInSc2d1	národní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
nepříliš	příliš	k6eNd1	příliš
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
průzkum	průzkum	k1gInSc4	průzkum
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Programu	program	k1gInSc2	program
Pioneer	Pioneer	kA	Pioneer
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
přinést	přinést	k5eAaPmF	přinést
poznatky	poznatek	k1gInPc1	poznatek
potřebné	potřebný	k2eAgInPc1d1	potřebný
k	k	k7c3	k
chystanému	chystaný	k2eAgNnSc3d1	chystané
přistání	přistání	k1gNnSc3	přistání
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
řídila	řídit	k5eAaImAgFnS	řídit
organizace	organizace	k1gFnSc1	organizace
NASA	NASA	kA	NASA
Jet	jet	k2eAgInSc1d1	jet
Propulsion	Propulsion	k1gInSc1	Propulsion
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
JPL	JPL	kA	JPL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pasadena	Pasaden	k2eAgFnSc1d1	Pasadena
<g/>
,	,	kIx,	,
CA	ca	kA	ca
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sondy	sonda	k1gFnPc4	sonda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
dopadnout	dopadnout	k5eAaPmF	dopadnout
na	na	k7c4	na
měsíční	měsíční	k2eAgInSc4d1	měsíční
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
poslat	poslat	k5eAaPmF	poslat
snímky	snímek	k1gInPc4	snímek
pořízené	pořízený	k2eAgInPc4d1	pořízený
šesti	šest	k4xCc7	šest
kamerami	kamera	k1gFnPc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopadem	dopad	k1gInSc7	dopad
se	se	k3xPyFc4	se
sonda	sonda	k1gFnSc1	sonda
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Rangerů	Ranger	k1gInPc2	Ranger
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
měkkému	měkký	k2eAgNnSc3d1	měkké
přistání	přistání	k1gNnSc3	přistání
přístrojového	přístrojový	k2eAgNnSc2d1	přístrojové
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
hodnoty	hodnota	k1gFnPc1	hodnota
170	[number]	k4	170
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
</s>
</p>
<p>
<s>
==	==	k?	==
Stručně	stručně	k6eAd1	stručně
k	k	k7c3	k
sondám	sonda	k1gFnPc3	sonda
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
sond	sonda	k1gFnPc2	sonda
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
dvoustupňová	dvoustupňový	k2eAgFnSc1d1	dvoustupňová
raketa	raketa	k1gFnSc1	raketa
Atlas	Atlas	k1gInSc1	Atlas
-	-	kIx~	-
Agena	Agena	k1gFnSc1	Agena
B.	B.	kA	B.
Všechny	všechen	k3xTgFnPc1	všechen
mise	mise	k1gFnPc1	mise
startovaly	startovat	k5eAaBmAgFnP	startovat
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Eastern	Easterna	k1gFnPc2	Easterna
Test	test	k1gInSc1	test
Range	Rang	k1gInSc2	Rang
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
1	[number]	k4	1
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
orbitě	orbita	k1gFnSc6	orbita
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
neúspěch	neúspěch	k1gInSc4	neúspěch
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
2	[number]	k4	2
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgFnSc2d1	předchozí
mise	mise	k1gFnSc2	mise
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
3	[number]	k4	3
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
Měsíc	měsíc	k1gInSc4	měsíc
přeletěla	přeletět	k5eAaPmAgFnS	přeletět
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
neúspěch	neúspěch	k1gInSc1	neúspěch
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
4	[number]	k4	4
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
zásah	zásah	k1gInSc4	zásah
Měsíce	měsíc	k1gInSc2	měsíc
sondou	sonda	k1gFnSc7	sonda
USA	USA	kA	USA
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
úspěch	úspěch	k1gInSc4	úspěch
částečný	částečný	k2eAgInSc4d1	částečný
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
5	[number]	k4	5
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Ranger	Ranger	k1gInSc1	Ranger
3	[number]	k4	3
<g/>
,	,	kIx,	,
neúspěch	neúspěch	k1gInSc4	neúspěch
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
6	[number]	k4	6
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
televizní	televizní	k2eAgInSc1d1	televizní
přenos	přenos	k1gInSc1	přenos
nefungoval	fungovat	k5eNaImAgInS	fungovat
<g/>
,	,	kIx,	,
neúspěch	neúspěch	k1gInSc4	neúspěch
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
7	[number]	k4	7
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
přenos	přenos	k1gInSc1	přenos
televizních	televizní	k2eAgInPc2d1	televizní
snímků	snímek	k1gInPc2	snímek
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
8	[number]	k4	8
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
jako	jako	k8xC	jako
předchozí	předchozí	k2eAgFnSc1d1	předchozí
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
pořídila	pořídit	k5eAaPmAgFnS	pořídit
tato	tento	k3xDgFnSc1	tento
bezpilotní	bezpilotní	k2eAgFnSc1d1	bezpilotní
sonda	sonda	k1gFnSc1	sonda
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
fotografie	fotografia	k1gFnSc2	fotografia
Moře	moře	k1gNnSc1	moře
Klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
mise	mise	k1gFnPc4	mise
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc1	Apollo
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
přistát	přistát	k5eAaImF	přistát
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ranger	Ranger	k1gInSc1	Ranger
9	[number]	k4	9
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
i	i	k8xC	i
ta	ten	k3xDgFnSc1	ten
poslala	poslat	k5eAaPmAgFnS	poslat
velice	velice	k6eAd1	velice
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
snímky	snímek	k1gInPc4	snímek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc1	konstrukce
sond	sonda	k1gFnPc2	sonda
==	==	k?	==
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
všech	všecek	k3xTgFnPc2	všecek
sond	sonda	k1gFnPc2	sonda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
306	[number]	k4	306
-	-	kIx~	-
365	[number]	k4	365
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
mj.	mj.	kA	mj.
panely	panel	k1gInPc4	panel
slunečních	sluneční	k2eAgFnPc2d1	sluneční
baterií	baterie	k1gFnPc2	baterie
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc1d1	chemická
baterie	baterie	k1gFnPc1	baterie
s	s	k7c7	s
výdrží	výdrž	k1gFnSc7	výdrž
na	na	k7c4	na
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc4d1	televizní
aparaturu	aparatura	k1gFnSc4	aparatura
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
širokoúhlové	širokoúhlová	k1gFnPc4	širokoúhlová
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
úzkoúhlé	úzkoúhlý	k2eAgFnPc4d1	úzkoúhlá
kamery	kamera	k1gFnPc4	kamera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Program	program	k1gInSc1	program
Ranger	Rangero	k1gNnPc2	Rangero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Na	na	k7c6	na
webu	web	k1gInSc6	web
Kosmo	Kosma	k1gMnSc5	Kosma
</s>
</p>
