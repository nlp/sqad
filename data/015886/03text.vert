<s>
Mydlokor	Mydlokor	k1gMnSc1
</s>
<s>
Mydlokor	Mydlokor	k1gMnSc1
Mydlokor	Mydlokor	k1gMnSc1
Quillaja	Quillaja	k1gMnSc1
saponaria	saponarium	k1gNnSc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
bobotvaré	bobotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Fabales	Fabales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
mydlokorovité	mydlokorovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Quillajaceae	Quillajacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
mydlokor	mydlokor	k1gInSc4
(	(	kIx(
<g/>
Quillaja	Quillaj	k2eAgNnPc4d1
<g/>
)	)	kIx)
<g/>
Molina	molino	k1gNnPc4
<g/>
,	,	kIx,
1782	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Plody	plod	k1gInPc1
mydlokoru	mydlokor	k1gInSc2
Quillaja	Quillaj	k1gInSc2
brasiliensis	brasiliensis	k1gFnSc2
</s>
<s>
Mydlokor	Mydlokor	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Quillaja	Quillaja	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
rod	rod	k1gInSc1
čeledi	čeleď	k1gFnSc2
mydlokorovité	mydlokorovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Quillajaceae	Quillajacea	k1gFnSc2
<g/>
)	)	kIx)
z	z	k7c2
řádu	řád	k1gInSc2
bobotvaré	bobotvarý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Fabales	Fabalesa	k1gFnPc2
<g/>
)	)	kIx)
vyšších	vysoký	k2eAgFnPc2d2
dvouděložných	dvouděložný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
2	#num#	k4
nebo	nebo	k8xC
3	#num#	k4
druhy	druh	k1gInPc7
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgFnSc7d1
v	v	k7c6
mírných	mírný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mydlokor	Mydlokor	k1gInSc1
tupolistý	tupolistý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
využíván	využíván	k2eAgInSc1d1
jako	jako	k8xC,k8xS
přírodní	přírodní	k2eAgNnSc4d1
mýdlo	mýdlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Mydlokory	Mydlokora	k1gFnPc1
jsou	být	k5eAaImIp3nP
stálezelené	stálezelený	k2eAgFnPc1d1
<g/>
,	,	kIx,
až	až	k9
25	#num#	k4
metrů	metr	k1gInPc2
vysoké	vysoký	k2eAgInPc1d1
stromy	strom	k1gInPc1
se	s	k7c7
střídavými	střídavý	k2eAgInPc7d1
jednoduchými	jednoduchý	k2eAgInPc7d1
listy	list	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
zubaté	zubatý	k2eAgFnPc1d1
<g/>
,	,	kIx,
kožovité	kožovitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
s	s	k7c7
palisty	palist	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
uspořádané	uspořádaný	k2eAgFnPc1d1
v	v	k7c6
krátkých	krátký	k2eAgFnPc6d1
úžlabních	úžlabní	k2eAgFnPc6d1
latách	lata	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalich	kalich	k1gInSc4
i	i	k8xC
koruna	koruna	k1gFnSc1
jsou	být	k5eAaImIp3nP
pětičetné	pětičetný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gyneceum	Gyneceum	k1gInSc1
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
srostlé	srostlý	k2eAgNnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
5	#num#	k4
plodolistů	plodolist	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
mají	mít	k5eAaImIp3nP
zajímavou	zajímavý	k2eAgFnSc4d1
morfologii	morfologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinky	tyčinka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
naproti	naproti	k7c3
korunním	korunní	k2eAgInPc3d1
lístkům	lístek	k1gInPc3
<g/>
,	,	kIx,
vyrůstají	vyrůstat	k5eAaImIp3nP
od	od	k7c2
báze	báze	k1gFnSc2
hvězdovitého	hvězdovitý	k2eAgInSc2d1
semeníku	semeník	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
tyčinky	tyčinka	k1gFnSc2
naproti	naproti	k7c3
kališním	kališní	k2eAgInPc3d1
lístkům	lístek	k1gInPc3
vyrůstají	vyrůstat	k5eAaImIp3nP
z	z	k7c2
disku	disk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
rozšířen	rozšířit	k5eAaPmNgInS
na	na	k7c4
plochu	plocha	k1gFnSc4
kališních	kališní	k2eAgInPc2d1
lístků	lístek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rod	rod	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
2	#num#	k4
nebo	nebo	k8xC
3	#num#	k4
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgInPc1d1
v	v	k7c6
temperátních	temperátní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Brazílii	Brazílie	k1gFnSc6
<g/>
,	,	kIx,
Bolívii	Bolívie	k1gFnSc6
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc6
a	a	k8xC
Chile	Chile	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
V	v	k7c6
dřívějších	dřívější	k2eAgInPc6d1
taxonomických	taxonomický	k2eAgInPc6d1
systémech	systém	k1gInPc6
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
Quillaja	Quillaj	k1gInSc2
řazen	řadit	k5eAaImNgInS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
růžovité	růžovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Rosaceae	Rosacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
mydlokor	mydlokor	k1gInSc1
tupolistý	tupolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Quillaja	Quillaja	k1gFnSc1
saponaria	saponarium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
kůra	kůra	k1gFnSc1
mydlokoru	mydlokor	k1gInSc2
tupolistého	tupolistý	k2eAgInSc2d1
(	(	kIx(
<g/>
Quillaja	Quillaj	k1gInSc2
saponaria	saponarium	k1gNnSc2
<g/>
)	)	kIx)
obsahuje	obsahovat	k5eAaImIp3nS
pěnivé	pěnivý	k2eAgInPc4d1
saponiny	saponin	k1gInPc4
a	a	k8xC
je	on	k3xPp3gNnPc4
využívána	využíván	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
přírodní	přírodní	k2eAgNnPc4d1
mýdlo	mýdlo	k1gNnSc4
a	a	k8xC
v	v	k7c6
kosmetice	kosmetika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
lékařství	lékařství	k1gNnSc6
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
např.	např.	kA
při	při	k7c6
bronchitidě	bronchitida	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SKALICKÁ	Skalická	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
;	;	kIx,
VĚTVIČKA	větvička	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
;	;	kIx,
ZELENÝ	Zelený	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botanický	botanický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
rodových	rodový	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
cévnatých	cévnatý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7442	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
31	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
STEVENS	STEVENS	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
F.	F.	kA
Angiosperm	Angiosperm	k1gInSc1
Phylogeny	Phylogen	k1gInPc1
Website	Websit	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missouri	Missouri	k1gNnSc1
Botanical	Botanical	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
:	:	kIx,
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
REICHE	REICHE	kA
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flora	Flora	k1gFnSc1
de	de	k?
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Santiago	Santiago	k1gNnSc4
de	de	k?
Chile	Chile	k1gNnSc2
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1898	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VALÍČEK	VALÍČEK	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užitkové	užitkový	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
tropů	trop	k1gInPc2
a	a	k8xC
subtropů	subtropy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
939	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Quillaja	Quillaj	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Angiosperm	Angiosperm	k1gInSc1
Phylogeny	Phylogen	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
