<s>
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
LF	LF	kA
MU	MU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
deseti	deset	k4xCc2
fakult	fakulta	k1gFnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
výuku	výuka	k1gFnSc4
lékařských	lékařský	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
zdravotnických	zdravotnický	k2eAgFnPc2d1
specializací	specializace	k1gFnPc2
a	a	k8xC
vědecko-výzkumnou	vědecko-výzkumný	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
čtyři	čtyři	k4xCgFnPc4
nejstarší	starý	k2eAgFnPc4d3
fakulty	fakulta	k1gFnPc4
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>