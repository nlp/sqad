<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
též	též	k9	též
petrklíč	petrklíč	k1gInSc1	petrklíč
<g/>
,	,	kIx,	,
bukvice	bukvice	k1gFnSc1	bukvice
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
bylina	bylina	k1gFnSc1	bylina
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
atd	atd	kA	atd
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drobnější	drobný	k2eAgFnSc1d2	drobnější
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
prvosenkovité	prvosenkovitý	k2eAgFnSc2d1	prvosenkovitý
(	(	kIx(	(
<g/>
Primulaceae	Primulacea	k1gFnSc2	Primulacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
i	i	k9	i
s	s	k7c7	s
květem	květ	k1gInSc7	květ
výšky	výška	k1gFnSc2	výška
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podzemního	podzemní	k2eAgInSc2d1	podzemní
oddenku	oddenek	k1gInSc2	oddenek
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
měkké	měkký	k2eAgInPc1d1	měkký
a	a	k8xC	a
svraskalé	svraskalý	k2eAgInPc1d1	svraskalý
listy	list	k1gInPc1	list
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Vonné	vonný	k2eAgInPc1d1	vonný
žluté	žlutý	k2eAgInPc1d1	žlutý
květy	květ	k1gInPc1	květ
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
jednostranném	jednostranný	k2eAgInSc6d1	jednostranný
okolíku	okolík	k1gInSc6	okolík
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
vegetativně	vegetativně	k6eAd1	vegetativně
rozdělením	rozdělení	k1gNnSc7	rozdělení
oddenku	oddenek	k1gInSc2	oddenek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
pohlavně	pohlavně	k6eAd1	pohlavně
<g/>
,	,	kIx,	,
tvorbou	tvorba	k1gFnSc7	tvorba
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
polohách	poloha	k1gFnPc6	poloha
nížin	nížina	k1gFnPc2	nížina
a	a	k8xC	a
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
hájích	háj	k1gInPc6	háj
<g/>
,	,	kIx,	,
prosluněných	prosluněný	k2eAgFnPc6d1	prosluněná
stráních	stráň	k1gFnPc6	stráň
a	a	k8xC	a
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
vápencových	vápencový	k2eAgInPc6d1	vápencový
podkladech	podklad	k1gInPc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
však	však	k9	však
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
chladné	chladný	k2eAgNnSc4d1	chladné
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Červený	červený	k2eAgInSc1d1	červený
seznam	seznam	k1gInSc1	seznam
ji	on	k3xPp3gFnSc4	on
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
C3	C3	k1gFnSc2	C3
(	(	kIx(	(
<g/>
vzácnější	vzácný	k2eAgInSc4d2	vzácnější
až	až	k8xS	až
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
populace	populace	k1gFnSc2	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc4d1	jarní
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
nárůstem	nárůst	k1gInSc7	nárůst
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
holosečnou	holosečný	k2eAgFnSc7d1	holosečná
těžbou	těžba	k1gFnSc7	těžba
a	a	k8xC	a
výsadbou	výsadba	k1gFnSc7	výsadba
kultur	kultura	k1gFnPc2	kultura
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
,	,	kIx,	,
používáním	používání	k1gNnSc7	používání
herbicidů	herbicid	k1gInPc2	herbicid
<g/>
,	,	kIx,	,
hnojením	hnojení	k1gNnSc7	hnojení
(	(	kIx(	(
<g/>
hlavně	hlavně	k6eAd1	hlavně
dusíkatými	dusíkatý	k2eAgNnPc7d1	dusíkaté
hnojivy	hnojivo	k1gNnPc7	hnojivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eutrofizací	eutrofizace	k1gFnPc2	eutrofizace
stálých	stálý	k2eAgFnPc2d1	stálá
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
orbou	orba	k1gFnSc7	orba
travních	travní	k2eAgInPc2d1	travní
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
nálevů	nálev	k1gInPc2	nálev
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsažené	obsažený	k2eAgInPc1d1	obsažený
(	(	kIx(	(
<g/>
glykosidy	glykosid	k1gInPc1	glykosid
a	a	k8xC	a
saponiny	saponin	k1gInPc1	saponin
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
expektorans	expektorans	k1gNnSc1	expektorans
při	při	k7c6	při
onemocnění	onemocnění	k1gNnSc6	onemocnění
horních	horní	k2eAgFnPc2d1	horní
a	a	k8xC	a
dolních	dolní	k2eAgFnPc2d1	dolní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
škůdců	škůdce	k1gMnPc2	škůdce
prvosenky	prvosenka	k1gFnSc2	prvosenka
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
a	a	k8xC	a
květy	květ	k1gInPc4	květ
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
poškozovány	poškozován	k2eAgFnPc1d1	poškozována
měkkýši	měkkýš	k1gMnPc1	měkkýš
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
druhy	druh	k1gInPc7	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přílišné	přílišný	k2eAgFnSc6d1	přílišná
vlhkosti	vlhkost	k1gFnSc6	vlhkost
a	a	k8xC	a
hustém	hustý	k2eAgInSc6d1	hustý
zápoji	zápoj	k1gInSc6	zápoj
prvosenku	prvosenka	k1gFnSc4	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
napadají	napadat	k5eAaImIp3nP	napadat
plísně	plíseň	k1gFnPc4	plíseň
a	a	k8xC	a
houbové	houbový	k2eAgFnPc4d1	houbová
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
prvosenka	prvosenka	k1gFnSc1	prvosenka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
polštiny	polština	k1gFnSc2	polština
(	(	kIx(	(
<g/>
pierwosnka	pierwosnka	k6eAd1	pierwosnka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
bylina	bylina	k1gFnSc1	bylina
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
clavis	clavis	k1gFnSc1	clavis
sancti	sancť	k1gFnSc2	sancť
Petri	Petr	k1gFnSc2	Petr
či	či	k8xC	či
německy	německy	k6eAd1	německy
Peterschlüssel	Peterschlüssela	k1gFnPc2	Peterschlüssela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnSc2	velikost
20	[number]	k4	20
cm	cm	kA	cm
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
až	až	k9	až
25	[number]	k4	25
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uzlovitého	uzlovitý	k2eAgInSc2d1	uzlovitý
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
cm	cm	kA	cm
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
oddenku	oddenek	k1gInSc2	oddenek
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
měkké	měkký	k2eAgNnSc4d1	měkké
a	a	k8xC	a
svraskalé	svraskalý	k2eAgNnSc4d1	svraskalé
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
listy	list	k1gInPc4	list
uspořádané	uspořádaný	k2eAgInPc4d1	uspořádaný
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Vonné	vonný	k2eAgInPc1d1	vonný
žluté	žlutý	k2eAgInPc1d1	žlutý
květy	květ	k1gInPc1	květ
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
uskupeny	uskupit	k5eAaPmNgInP	uskupit
do	do	k7c2	do
jednostranného	jednostranný	k2eAgInSc2d1	jednostranný
okolíku	okolík	k1gInSc2	okolík
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
hmyzosnubná	hmyzosnubný	k2eAgFnSc1d1	hmyzosnubná
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
generativně	generativně	k6eAd1	generativně
<g/>
,	,	kIx,	,
semenem	semeno	k1gNnSc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
se	s	k7c7	s
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
prochází	procházet	k5eAaImIp3nS	procházet
stádiem	stádium	k1gNnSc7	stádium
dormance	dormanka	k1gFnSc6	dormanka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
listy	list	k1gInPc1	list
usychají	usychat	k5eAaImIp3nP	usychat
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
chladem	chlad	k1gInSc7	chlad
narušena	narušen	k2eAgFnSc1d1	narušena
(	(	kIx(	(
<g/>
stratifikována	stratifikován	k2eAgFnSc1d1	stratifikována
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
klíčení	klíčení	k1gNnSc3	klíčení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
semen	semeno	k1gNnPc2	semeno
klíčí	klíčit	k5eAaImIp3nS	klíčit
až	až	k9	až
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
prvosenka	prvosenka	k1gFnSc1	prvosenka
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
dožít	dožít	k5eAaPmF	dožít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
zástupci	zástupce	k1gMnPc1	zástupce
sekce	sekce	k1gFnSc2	sekce
Primula	Primula	k?	Primula
je	být	k5eAaImIp3nS	být
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
diploidní	diploidní	k2eAgFnSc1d1	diploidní
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
somatických	somatický	k2eAgInPc2d1	somatický
chromozómů	chromozóm	k1gInPc2	chromozóm
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
staré	starý	k2eAgInPc4d1	starý
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
listy	list	k1gInPc1	list
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zimy	zima	k1gFnSc2	zima
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
až	až	k8xS	až
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vegetačním	vegetační	k2eAgNnSc6d1	vegetační
období	období	k1gNnSc6	období
a	a	k8xC	a
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
velikosti	velikost	k1gFnSc2	velikost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvláště	zvláště	k6eAd1	zvláště
suchých	suchý	k2eAgFnPc6d1	suchá
a	a	k8xC	a
horkých	horký	k2eAgFnPc6d1	horká
podmínkách	podmínka	k1gFnPc6	podmínka
stárne	stárnout	k5eAaImIp3nS	stárnout
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
listů	list	k1gInPc2	list
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vegetačního	vegetační	k2eAgNnSc2d1	vegetační
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc1	srpen
až	až	k8xS	až
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbytek	zbytek	k1gInSc1	zbytek
listů	list	k1gInPc2	list
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nevyvinou	vyvinout	k5eNaPmIp3nP	vyvinout
nové	nový	k2eAgInPc1d1	nový
listy	list	k1gInPc1	list
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
<g/>
Základy	základ	k1gInPc1	základ
květů	květ	k1gInPc2	květ
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
květní	květní	k2eAgInPc1d1	květní
pupeny	pupen	k1gInPc1	pupen
na	na	k7c6	na
stopkách	stopka	k1gFnPc6	stopka
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
viditelné	viditelný	k2eAgFnPc1d1	viditelná
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgNnSc1d1	jarní
kvete	kvést	k5eAaImIp3nS	kvést
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Kvetení	kvetení	k1gNnSc1	kvetení
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
silně	silně	k6eAd1	silně
lišit	lišit	k5eAaImF	lišit
během	během	k7c2	během
let	léto	k1gNnPc2	léto
a	a	k8xC	a
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
zrají	zrát	k5eAaImIp3nP	zrát
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
po	po	k7c4	po
oplodnění	oplodnění	k1gNnSc4	oplodnění
a	a	k8xC	a
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
roznášena	roznášet	k5eAaImNgNnP	roznášet
do	do	k7c2	do
okolí	okolí	k1gNnPc2	okolí
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vegetativní	vegetativní	k2eAgInPc1d1	vegetativní
orgány	orgán	k1gInPc1	orgán
===	===	k?	===
</s>
</p>
<p>
<s>
Vegetativními	vegetativní	k2eAgInPc7d1	vegetativní
orgány	orgán	k1gInPc7	orgán
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc4d1	jarní
jsou	být	k5eAaImIp3nP	být
oddenek	oddenek	k1gInSc1	oddenek
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
kořenovým	kořenový	k2eAgNnSc7d1	kořenové
vlášením	vlášení	k1gNnSc7	vlášení
a	a	k8xC	a
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
klimaticky	klimaticky	k6eAd1	klimaticky
příznivé	příznivý	k2eAgFnSc6d1	příznivá
době	doba	k1gFnSc6	doba
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
listové	listový	k2eAgFnSc2d1	listová
růžice	růžice	k1gFnSc2	růžice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddenky	oddenek	k1gInPc1	oddenek
jsou	být	k5eAaImIp3nP	být
nepravidelně	pravidelně	k6eNd1	pravidelně
pokroucené	pokroucený	k2eAgFnSc2d1	pokroucená
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
široké	široký	k2eAgFnSc2d1	široká
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
sivě	sivě	k6eAd1	sivě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
maximálně	maximálně	k6eAd1	maximálně
1	[number]	k4	1
mm	mm	kA	mm
tlusté	tlustý	k2eAgFnPc1d1	tlustá
a	a	k8xC	a
několik	několik	k4yIc1	několik
centimetrů	centimetr	k1gInPc2	centimetr
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
žluté	žlutý	k2eAgFnPc1d1	žlutá
až	až	k8xS	až
běložluté	běložlutý	k2eAgFnPc1d1	běložlutá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
oddenků	oddenek	k1gInPc2	oddenek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obnovovací	obnovovací	k2eAgInPc1d1	obnovovací
pupeny	pupen	k1gInPc1	pupen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
schovány	schovat	k5eAaPmNgInP	schovat
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
krček	krček	k1gInSc1	krček
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
části	část	k1gFnSc6	část
pokryt	pokryt	k2eAgMnSc1d1	pokryt
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
masitými	masitý	k2eAgFnPc7d1	masitá
šupinami	šupina	k1gFnPc7	šupina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
uschlých	uschlý	k2eAgInPc2d1	uschlý
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
na	na	k7c6	na
příčném	příčný	k2eAgInSc6d1	příčný
řezu	řez	k1gInSc6	řez
oddenku	oddenek	k1gInSc2	oddenek
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
pod	pod	k7c7	pod
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
endodermem	endoderm	k1gInSc7	endoderm
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
cévní	cévní	k2eAgInPc1d1	cévní
svazky	svazek	k1gInPc1	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
zrna	zrno	k1gNnPc4	zrno
škrobu	škrob	k1gInSc2	škrob
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
velikost	velikost	k1gFnSc1	velikost
kolísá	kolísat	k5eAaImIp3nS	kolísat
okolo	okolo	k7c2	okolo
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kořenech	kořen	k1gInPc6	kořen
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
byla	být	k5eAaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
arbuskulární	arbuskulární	k2eAgFnSc1d1	arbuskulární
mykorhiza	mykorhiza	k1gFnSc1	mykorhiza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
křídlatým	křídlatý	k2eAgInPc3d1	křídlatý
řapíkem	řapík	k1gInSc7	řapík
rozšiřujícím	rozšiřující	k2eAgFnPc3d1	rozšiřující
se	se	k3xPyFc4	se
v	v	k7c4	v
pochvu	pochva	k1gFnSc4	pochva
a	a	k8xC	a
vejčitou	vejčitý	k2eAgFnSc7d1	vejčitá
podvinutou	podvinutý	k2eAgFnSc7d1	podvinutá
čepelí	čepel	k1gFnSc7	čepel
s	s	k7c7	s
chloupky	chloupek	k1gInPc7	chloupek
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgFnSc2d1	měkká
<g/>
,	,	kIx,	,
svraskalé	svraskalý	k2eAgFnSc2d1	svraskalá
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
(	(	kIx(	(
<g/>
i	i	k9	i
s	s	k7c7	s
řapíky	řapík	k1gInPc7	řapík
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
cm	cm	kA	cm
široké	široký	k2eAgNnSc4d1	široké
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kvetení	kvetený	k2eAgMnPc1d1	kvetený
<g/>
,	,	kIx,	,
nejširší	široký	k2eAgMnPc1d3	nejširší
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
plození	plození	k1gNnSc2	plození
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
až	až	k9	až
vejčitě	vejčitě	k6eAd1	vejčitě
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
<g/>
,	,	kIx,	,
k	k	k7c3	k
bázi	báze	k1gFnSc3	báze
pozvolna	pozvolna	k6eAd1	pozvolna
nebo	nebo	k8xC	nebo
náhle	náhle	k6eAd1	náhle
zúžená	zúžený	k2eAgFnSc1d1	zúžená
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
slabě	slabě	k6eAd1	slabě
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepravidelně	pravidelně	k6eNd1	pravidelně
zoubkovaná	zoubkovaný	k2eAgFnSc1d1	zoubkovaná
<g/>
,	,	kIx,	,
až	až	k9	až
téměř	téměř	k6eAd1	téměř
celokrajná	celokrajný	k2eAgFnSc1d1	celokrajná
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
straně	strana	k1gFnSc6	strana
pýřitá	pýřitý	k2eAgFnSc1d1	pýřitá
až	až	k6eAd1	až
olysalá	olysalý	k2eAgFnSc1d1	olysalá
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
vystouplou	vystouplý	k2eAgFnSc7d1	vystouplá
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Řapík	řapík	k1gInSc1	řapík
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgInSc1d2	kratší
než	než	k8xS	než
čepel	čepel	k1gInSc1	čepel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
křídlatý	křídlatý	k2eAgMnSc1d1	křídlatý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
řapík	řapík	k1gInSc1	řapík
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
v	v	k7c4	v
pochvu	pochva	k1gFnSc4	pochva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Generativní	generativní	k2eAgInPc1d1	generativní
orgány	orgán	k1gInPc1	orgán
===	===	k?	===
</s>
</p>
<p>
<s>
Generativními	generativní	k2eAgInPc7d1	generativní
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
prvosenky	prvosenka	k1gFnSc2	prvosenka
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
stvolech	stvol	k1gInPc6	stvol
obnovovacích	obnovovací	k2eAgInPc2d1	obnovovací
pupenů	pupen	k1gInPc2	pupen
oddenků	oddenek	k1gInPc2	oddenek
a	a	k8xC	a
následně	následně	k6eAd1	následně
plody	plod	k1gInPc4	plod
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tobolek	tobolka	k1gFnPc2	tobolka
se	s	k7c7	s
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
<g/>
Jemně	jemně	k6eAd1	jemně
žláznatý	žláznatý	k2eAgInSc1d1	žláznatý
a	a	k8xC	a
pýřitý	pýřitý	k2eAgInSc1d1	pýřitý
stvol	stvol	k1gInSc1	stvol
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
chlupatý	chlupatý	k2eAgInSc1d1	chlupatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květenství	květenství	k1gNnSc6	květenství
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
jednostranný	jednostranný	k2eAgInSc1d1	jednostranný
převislý	převislý	k2eAgInSc1d1	převislý
okolík	okolík	k1gInSc1	okolík
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
1	[number]	k4	1
až	až	k9	až
30	[number]	k4	30
květů	květ	k1gInPc2	květ
nebo	nebo	k8xC	nebo
květních	květní	k2eAgInPc2d1	květní
pupenů	pupen	k1gInPc2	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
květů	květ	k1gInPc2	květ
během	během	k7c2	během
kvetení	kvetení	k1gNnSc2	kvetení
je	být	k5eAaImIp3nS	být
flexibilní	flexibilní	k2eAgNnSc1d1	flexibilní
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
plodů	plod	k1gInPc2	plod
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Listeny	listen	k1gInPc1	listen
jsou	být	k5eAaImIp3nP	být
čárkovitě	čárkovitě	k6eAd1	čárkovitě
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
,	,	kIx,	,
bleděžluté	bleděžlutý	k2eAgFnPc1d1	bleděžlutá
<g/>
,	,	kIx,	,
při	při	k7c6	při
bázi	báze	k1gFnSc6	báze
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
hrbolku	hrbolek	k1gInSc2	hrbolek
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Stopky	stopka	k1gFnPc1	stopka
květní	květní	k2eAgFnPc1d1	květní
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
tyčinek	tyčinka	k1gFnPc2	tyčinka
stejně	stejně	k6eAd1	stejně
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
jako	jako	k8xS	jako
kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
objevuje	objevovat	k5eAaImIp3nS	objevovat
tzv.	tzv.	kA	tzv.
různočnělečnost	různočnělečnost	k1gFnSc1	různočnělečnost
−	−	k?	−
heterostylie	heterostylie	k1gFnSc2	heterostylie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
nitek	nitka	k1gFnPc2	nitka
výrazně	výrazně	k6eAd1	výrazně
zredukovaná	zredukovaný	k2eAgFnSc1d1	zredukovaná
<g/>
.	.	kIx.	.
</s>
<s>
Prašníky	prašník	k1gInPc1	prašník
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
2	[number]	k4	2
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Pestík	pestík	k1gInSc1	pestík
má	mít	k5eAaImIp3nS	mít
svrchní	svrchní	k2eAgInSc4d1	svrchní
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
semeník	semeník	k1gInSc4	semeník
a	a	k8xC	a
rovnou	rovný	k2eAgFnSc4d1	rovná
čnělku	čnělka	k1gFnSc4	čnělka
s	s	k7c7	s
paličkovitou	paličkovitý	k2eAgFnSc7d1	paličkovitý
bliznou	blizna	k1gFnSc7	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prvosenky	prvosenka	k1gFnSc2	prvosenka
vyšší	vysoký	k2eAgInPc4d2	vyšší
květy	květ	k1gInPc4	květ
charakteristicky	charakteristicky	k6eAd1	charakteristicky
voní	vonět	k5eAaImIp3nP	vonět
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
zvonkovitý	zvonkovitý	k2eAgInSc1d1	zvonkovitý
<g/>
,	,	kIx,	,
nálevkovitý	nálevkovitý	k2eAgInSc1d1	nálevkovitý
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
trubkovitý	trubkovitý	k2eAgMnSc1d1	trubkovitý
nebo	nebo	k8xC	nebo
baňkovitý	baňkovitý	k2eAgMnSc1d1	baňkovitý
<g/>
,	,	kIx,	,
hranatý	hranatý	k2eAgMnSc1d1	hranatý
<g/>
,	,	kIx,	,
od	od	k7c2	od
korunní	korunní	k2eAgFnSc2d1	korunní
trubky	trubka	k1gFnSc2	trubka
odstávající	odstávající	k2eAgFnSc2d1	odstávající
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
mm	mm	kA	mm
má	mít	k5eAaImIp3nS	mít
bleděžlutou	bleděžlutý	k2eAgFnSc4d1	bleděžlutá
nebo	nebo	k8xC	nebo
bledězelenou	bledězelený	k2eAgFnSc4d1	bledězelený
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vroubkovaný	vroubkovaný	k2eAgMnSc1d1	vroubkovaný
a	a	k8xC	a
chlupatý	chlupatý	k2eAgMnSc1d1	chlupatý
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
podél	podél	k7c2	podél
úžlabí	úžlabí	k1gNnSc2	úžlabí
vroubků	vroubek	k1gInPc2	vroubek
<g/>
.	.	kIx.	.
</s>
<s>
Cípy	cíp	k1gInPc1	cíp
květů	květ	k1gInPc2	květ
jsou	být	k5eAaImIp3nP	být
trojúhelníkovité	trojúhelníkovitý	k2eAgFnPc1d1	trojúhelníkovitá
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgFnPc1d1	špičatá
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc1d1	dosahující
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
až	až	k9	až
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
délky	délka	k1gFnSc2	délka
kalicha	kalich	k1gInSc2	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
korunní	korunní	k2eAgFnSc2d1	korunní
trubky	trubka	k1gFnSc2	trubka
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
nebo	nebo	k8xC	nebo
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
délka	délka	k1gFnSc1	délka
kalicha	kalich	k1gInSc2	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Lem	lem	k1gInSc1	lem
miskovitého	miskovitý	k2eAgInSc2d1	miskovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
mm	mm	kA	mm
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Cípy	cíp	k1gInPc1	cíp
lemu	lem	k1gInSc2	lem
jsou	být	k5eAaImIp3nP	být
obsrdčité	obsrdčitý	k2eAgFnPc1d1	obsrdčitý
<g/>
,	,	kIx,	,
při	při	k7c6	při
bázi	báze	k1gFnSc6	báze
mají	mít	k5eAaImIp3nP	mít
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
obligátně	obligátně	k6eAd1	obligátně
cizosprašná	cizosprašný	k2eAgFnSc1d1	cizosprašná
<g/>
,	,	kIx,	,
opylení	opylení	k1gNnSc1	opylení
zcela	zcela	k6eAd1	zcela
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hmyzích	hmyzí	k2eAgInPc6d1	hmyzí
opylovačích	opylovač	k1gInPc6	opylovač
(	(	kIx(	(
<g/>
entomogamie	entomogamie	k1gFnPc1	entomogamie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
různí	různý	k2eAgMnPc1d1	různý
čmeláci	čmelák	k1gMnPc1	čmelák
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Bombus	Bombus	k1gInSc1	Bombus
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
blanokřídlí	blanokřídlí	k1gMnPc1	blanokřídlí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
motýli	motýl	k1gMnPc1	motýl
(	(	kIx(	(
<g/>
žluťásek	žluťásek	k1gMnSc1	žluťásek
rodu	rod	k1gInSc2	rod
Gonepteryx	Gonepteryx	k1gInSc1	Gonepteryx
či	či	k8xC	či
např.	např.	kA	např.
kukléřky	kukléřka	k1gFnSc2	kukléřka
rodu	rod	k1gInSc2	rod
Cucullia	Cucullia	k1gFnSc1	Cucullia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
přirozených	přirozený	k2eAgFnPc2d1	přirozená
variant	varianta	k1gFnPc2	varianta
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
kolísá	kolísat	k5eAaImIp3nS	kolísat
poměr	poměr	k1gInSc1	poměr
opylených	opylený	k2eAgInPc2d1	opylený
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
plody	plod	k1gInPc1	plod
okolo	okolo	k7c2	okolo
61	[number]	k4	61
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
se	s	k7c7	s
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Tobolka	tobolka	k1gFnSc1	tobolka
je	být	k5eAaImIp3nS	být
vejcovitá	vejcovitý	k2eAgFnSc1d1	vejcovitá
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgInSc1d2	kratší
než	než	k8xS	než
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
zakryta	zakryt	k2eAgFnSc1d1	zakryta
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
plodů	plod	k1gInPc2	plod
na	na	k7c6	na
rostlině	rostlina	k1gFnSc6	rostlina
kolísá	kolísat	k5eAaImIp3nS	kolísat
kolem	kolem	k7c2	kolem
devatenácti	devatenáct	k4xCc2	devatenáct
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
108	[number]	k4	108
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
<g/>
,	,	kIx,	,
krychlovitá	krychlovitý	k2eAgFnSc1d1	krychlovitá
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
kulatá	kulatý	k2eAgNnPc1d1	kulaté
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
tmavohnědá	tmavohnědý	k2eAgNnPc1d1	tmavohnědé
<g/>
,	,	kIx,	,
nelepkavá	lepkavý	k2eNgNnPc1d1	lepkavý
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
bradavčitá	bradavčitý	k2eAgFnSc1d1	bradavčitá
nebo	nebo	k8xC	nebo
uzlovitá	uzlovitý	k2eAgFnSc1d1	uzlovitá
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnSc2	délka
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
masíčka	masíčko	k1gNnSc2	masíčko
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc4d1	průměrné
množství	množství	k1gNnSc4	množství
semen	semeno	k1gNnPc2	semeno
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
semen	semeno	k1gNnPc2	semeno
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
hmoty	hmota	k1gFnSc2	hmota
semen	semeno	k1gNnPc2	semeno
je	být	k5eAaImIp3nS	být
negativně	negativně	k6eAd1	negativně
souvztažný	souvztažný	k2eAgInSc1d1	souvztažný
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
váží	vážit	k5eAaImIp3nS	vážit
pouze	pouze	k6eAd1	pouze
0,69	[number]	k4	0,69
<g/>
–	–	k?	–
<g/>
1,24	[number]	k4	1,24
mg	mg	kA	mg
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
mechanismem	mechanismus	k1gInSc7	mechanismus
kadidelnice	kadidelnice	k1gFnPc4	kadidelnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
semena	semeno	k1gNnPc1	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
podmínkách	podmínka	k1gFnPc6	podmínka
klíčit	klíčit	k5eAaImF	klíčit
brzy	brzy	k6eAd1	brzy
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
vyseta	vyset	k2eAgFnSc1d1	vyseta
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
semen	semeno	k1gNnPc2	semeno
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
a	a	k8xC	a
naklíčí	naklíčit	k5eAaPmIp3nS	naklíčit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
semen	semeno	k1gNnPc2	semeno
získaných	získaný	k2eAgInPc2d1	získaný
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
být	být	k5eAaImF	být
životaschopná	životaschopný	k2eAgFnSc1d1	životaschopná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
měla	mít	k5eAaImAgFnS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
klíčivost	klíčivost	k1gFnSc4	klíčivost
88	[number]	k4	88
%	%	kIx~	%
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
klíčení	klíčení	k1gNnSc2	klíčení
semen	semeno	k1gNnPc2	semeno
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
překvapivě	překvapivě	k6eAd1	překvapivě
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
klíčivost	klíčivost	k1gFnSc1	klíčivost
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
kvůli	kvůli	k7c3	kvůli
období	období	k1gNnSc3	období
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
čerstvá	čerstvý	k2eAgNnPc1d1	čerstvé
semena	semeno	k1gNnPc1	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jsou	být	k5eAaImIp3nP	být
spící	spící	k2eAgMnPc1d1	spící
v	v	k7c6	v
době	doba	k1gFnSc6	doba
výsevu	výsev	k1gInSc2	výsev
(	(	kIx(	(
<g/>
přeléhavá	přeléhavý	k2eAgFnSc1d1	přeléhavý
<g/>
)	)	kIx)	)
a	a	k8xC	a
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
stratifikaci	stratifikace	k1gFnSc4	stratifikace
chladem	chlad	k1gInSc7	chlad
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
aktivováno	aktivován	k2eAgNnSc1d1	aktivováno
klíčení	klíčení	k1gNnSc1	klíčení
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
semena	semeno	k1gNnPc1	semeno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vodorozpustné	vodorozpustný	k2eAgInPc1d1	vodorozpustný
obaly	obal	k1gInPc1	obal
<g/>
,	,	kIx,	,
období	období	k1gNnSc1	období
klidu	klid	k1gInSc2	klid
u	u	k7c2	u
semen	semeno	k1gNnPc2	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
není	být	k5eNaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
při	při	k7c6	při
zrání	zrání	k1gNnSc6	zrání
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
stimulováno	stimulován	k2eAgNnSc1d1	stimulováno
klíčení	klíčení	k1gNnSc1	klíčení
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
preferují	preferovat	k5eAaImIp3nP	preferovat
malý	malý	k2eAgInSc4d1	malý
rozsah	rozsah	k1gInSc4	rozsah
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
optimum	optimum	k1gNnSc1	optimum
16,1	[number]	k4	16,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnSc2	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
vystavená	vystavená	k1gFnSc1	vystavená
dostatečně	dostatečně	k6eAd1	dostatečně
dlouho	dlouho	k6eAd1	dlouho
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
16,1	[number]	k4	16,1
°	°	k?	°
<g/>
C	C	kA	C
tedy	tedy	k9	tedy
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
začnou	začít	k5eAaPmIp3nP	začít
klíčit	klíčit	k5eAaImF	klíčit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
semena	semeno	k1gNnSc2	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
vystavená	vystavený	k2eAgFnSc1d1	vystavená
vyšším	vysoký	k2eAgFnPc3d2	vyšší
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
přechovávaná	přechovávaný	k2eAgFnSc1d1	přechovávaná
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
podmínkách	podmínka	k1gFnPc6	podmínka
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
znovu	znovu	k6eAd1	znovu
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
období	období	k1gNnSc2	období
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
semen	semeno	k1gNnPc2	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
se	se	k3xPyFc4	se
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
klíčení	klíčení	k1gNnPc2	klíčení
může	moct	k5eAaImIp3nS	moct
hromadit	hromadit	k5eAaImF	hromadit
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
nálezy	nález	k1gInPc1	nález
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
životaschopných	životaschopný	k2eAgNnPc2d1	životaschopné
semen	semeno	k1gNnPc2	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
v	v	k7c6	v
zásobě	zásoba	k1gFnSc6	zásoba
semen	semeno	k1gNnPc2	semeno
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
po	po	k7c4	po
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
zůstala	zůstat	k5eAaPmAgFnS	zůstat
životaschopná	životaschopný	k2eAgFnSc1d1	životaschopná
a	a	k8xC	a
podržela	podržet	k5eAaPmAgFnS	podržet
si	se	k3xPyFc3	se
85	[number]	k4	85
%	%	kIx~	%
klíčivost	klíčivost	k1gFnSc1	klíčivost
<g/>
.	.	kIx.	.
</s>
<s>
Ošetření	ošetření	k1gNnSc4	ošetření
semen	semeno	k1gNnPc2	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
gibereliny	giberelina	k1gFnSc2	giberelina
významně	významně	k6eAd1	významně
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
klíčení	klíčení	k1gNnSc2	klíčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
uváděná	uváděný	k2eAgNnPc4d1	uváděné
synonyma	synonymum	k1gNnPc4	synonymum
k	k	k7c3	k
prvosence	prvosenka	k1gFnSc3	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Sp	Sp	k1gFnSc1	Sp
<g/>
.	.	kIx.	.
</s>
<s>
Pl.	Pl.	k?	Pl.
1	[number]	k4	1
<g/>
:	:	kIx,	:
142	[number]	k4	142
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
Lehm	Lehm	k1gInSc1	Lehm
<g/>
.	.	kIx.	.
</s>
<s>
Monogr	Monogr	k1gMnSc1	Monogr
<g/>
.	.	kIx.	.
</s>
<s>
Primul	Primul	k?	Primul
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
,	,	kIx,	,
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
isonym	isonym	k1gInSc1	isonym
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Primula	Primula	k?	Primula
officinalis	officinalis	k1gFnSc1	officinalis
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Hill	Hill	k1gMnSc1	Hill
Veg	Veg	k1gMnSc1	Veg
Syst	Syst	k1gMnSc1	Syst
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
:	:	kIx,	:
25	[number]	k4	25
<g/>
,	,	kIx,	,
1765	[number]	k4	1765
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Primula	Primula	k?	Primula
odorata	odorata	k1gFnSc1	odorata
Gilib	Gilib	k1gInSc1	Gilib
<g/>
.	.	kIx.	.
</s>
<s>
Fl	Fl	k?	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Lit.	Lit.	k?	Lit.
Inch	Inch	k1gInSc1	Inch
<g/>
.	.	kIx.	.
i.	i.	k?	i.
32	[number]	k4	32
<g/>
,	,	kIx,	,
1782	[number]	k4	1782
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Primula	Primula	k?	Primula
cordifolia	cordifolia	k1gFnSc1	cordifolia
Kit	kit	k1gInSc1	kit
<g/>
.	.	kIx.	.
</s>
<s>
Linnaea	Linnaea	k6eAd1	Linnaea
32	[number]	k4	32
<g/>
:	:	kIx,	:
451	[number]	k4	451
<g/>
,	,	kIx,	,
1864	[number]	k4	1864
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Primula	Primula	k?	Primula
coronaria	coronarium	k1gNnSc2	coronarium
Salisb	Salisba	k1gFnPc2	Salisba
<g/>
.	.	kIx.	.
</s>
<s>
Prodr	Prodr	k1gInSc1	Prodr
<g/>
.	.	kIx.	.
</s>
<s>
Stirp	Stirp	k1gMnSc1	Stirp
<g/>
.	.	kIx.	.
</s>
<s>
Chap	Chap	k1gMnSc1	Chap
<g/>
.	.	kIx.	.
</s>
<s>
Allerton	Allerton	k1gInSc1	Allerton
117	[number]	k4	117
<g/>
,	,	kIx,	,
1796	[number]	k4	1796
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Variabilita	variabilita	k1gFnSc1	variabilita
==	==	k?	==
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
variabilitu	variabilita	k1gFnSc4	variabilita
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
a	a	k8xC	a
chlupatosti	chlupatost	k1gFnSc3	chlupatost
listů	list	k1gInPc2	list
i	i	k8xC	i
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
tvaru	tvar	k1gInSc6	tvar
kalichu	kalich	k1gInSc2	kalich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
termofytiku	termofytikum	k1gNnSc6	termofytikum
výrazně	výrazně	k6eAd1	výrazně
převládají	převládat	k5eAaImIp3nP	převládat
prvosenky	prvosenka	k1gFnPc4	prvosenka
jarní	jarní	k2eAgFnSc4d1	jarní
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
délkou	délka	k1gFnSc7	délka
kalichů	kalich	k1gInPc2	kalich
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
termofytikum	termofytikum	k1gNnSc4	termofytikum
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
i	i	k9	i
do	do	k7c2	do
teplejších	teplý	k2eAgFnPc2d2	teplejší
poloh	poloha	k1gFnPc2	poloha
mezofytika	mezofytikum	k1gNnSc2	mezofytikum
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Panonském	panonský	k2eAgNnSc6d1	panonské
termofytiku	termofytikum	k1gNnSc6	termofytikum
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
hustěji	husto	k6eAd2	husto
chlupaté	chlupatý	k2eAgFnPc1d1	chlupatá
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
plstnaté	plstnatý	k2eAgFnSc3d1	plstnatá
(	(	kIx(	(
<g/>
var.	var.	k?	var.
hardeggensis	hardeggensis	k1gInSc1	hardeggensis
Beck	Beck	k1gInSc1	Beck
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tendence	tendence	k1gFnSc1	tendence
k	k	k7c3	k
olysávání	olysávání	k1gNnSc3	olysávání
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
dlouhokališné	dlouhokališný	k2eAgFnSc2d1	dlouhokališný
variety	varieta	k1gFnSc2	varieta
poměrně	poměrně	k6eAd1	poměrně
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
nejteplejších	teplý	k2eAgNnPc6d3	nejteplejší
územích	území	k1gNnPc6	území
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
či	či	k8xC	či
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
prvosenky	prvosenka	k1gFnPc1	prvosenka
jarní	jarní	k2eAgFnPc1d1	jarní
skoro	skoro	k6eAd1	skoro
úplně	úplně	k6eAd1	úplně
lysé	lysý	k2eAgInPc1d1	lysý
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
plstnatými	plstnatý	k2eAgMnPc7d1	plstnatý
<g/>
.	.	kIx.	.
<g/>
Prvosenky	prvosenka	k1gFnPc4	prvosenka
jarní	jarní	k2eAgFnPc4d1	jarní
s	s	k7c7	s
krátkými	krátký	k2eAgInPc7d1	krátký
kalichy	kalich	k1gInPc7	kalich
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
mm	mm	kA	mm
<g/>
)	)	kIx)	)
s	s	k7c7	s
listy	list	k1gInPc7	list
naspodu	naspodu	k6eAd1	naspodu
chlupatými	chlupatý	k2eAgInPc7d1	chlupatý
až	až	k8xS	až
olysalými	olysalý	k2eAgInPc7d1	olysalý
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
mezofytiku	mezofytikum	k1gNnSc6	mezofytikum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
někdy	někdy	k6eAd1	někdy
tvoří	tvořit	k5eAaImIp3nP	tvořit
čisté	čistý	k2eAgFnPc1d1	čistá
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhokališný	Dlouhokališný	k2eAgInSc1d1	Dlouhokališný
i	i	k8xC	i
krátkokališný	krátkokališný	k2eAgInSc1d1	krátkokališný
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
plynulými	plynulý	k2eAgInPc7d1	plynulý
přechody	přechod	k1gInPc7	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
silnému	silný	k2eAgNnSc3d1	silné
prolínání	prolínání	k1gNnSc3	prolínání
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
rostlin	rostlina	k1gFnPc2	rostlina
možné	možný	k2eAgNnSc4d1	možné
jen	jen	k9	jen
určení	určení	k1gNnSc4	určení
do	do	k7c2	do
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
lokalitách	lokalita	k1gFnPc6	lokalita
v	v	k7c6	v
Polabí	Polabí	k1gNnSc6	Polabí
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
populacích	populace	k1gFnPc6	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
šedavé	šedavý	k2eAgFnPc4d1	šedavá
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
baňkovitým	baňkovitý	k2eAgInSc7d1	baňkovitý
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
mm	mm	kA	mm
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
kalichem	kalich	k1gInSc7	kalich
a	a	k8xC	a
korunní	korunní	k2eAgFnSc7d1	korunní
trubkou	trubka	k1gFnSc7	trubka
nápadně	nápadně	k6eAd1	nápadně
vyniklou	vyniklý	k2eAgFnSc7d1	vyniklá
(	(	kIx(	(
<g/>
var.	var.	k?	var.
velenovskyi	velenovskyi	k1gNnSc4	velenovskyi
Domin	domino	k1gNnPc2	domino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jinde	jinde	k6eAd1	jinde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
široké	široký	k2eAgFnSc3d1	široká
ekologické	ekologický	k2eAgFnSc3d1	ekologická
amplitudě	amplituda	k1gFnSc3	amplituda
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhu	druh	k1gInSc2	druh
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ekotypickému	ekotypický	k2eAgNnSc3d1	ekotypický
rozrůzňování	rozrůzňování	k1gNnSc3	rozrůzňování
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
však	však	k9	však
chybí	chybit	k5eAaPmIp3nS	chybit
experimentální	experimentální	k2eAgInSc1d1	experimentální
důkaz	důkaz	k1gInSc1	důkaz
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
více	hodně	k6eAd2	hodně
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Nominátní	Nominátní	k2eAgInSc1d1	Nominátní
poddruh	poddruh	k1gInSc1	poddruh
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
pravá	pravá	k1gFnSc1	pravá
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
veris	veris	k1gInSc1	veris
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
areálu	areál	k1gInSc2	areál
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
po	po	k7c4	po
Alpy	Alpy	k1gFnPc4	Alpy
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
podhůří	podhůří	k1gNnSc2	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgFnSc1d2	častější
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
šedavá	šedavý	k2eAgFnSc1d1	šedavá
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
canescens	canescens	k1gInSc1	canescens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
západ	západ	k1gInSc4	západ
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
až	až	k9	až
po	po	k7c4	po
jih	jih	k1gInSc4	jih
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
sever	sever	k1gInSc4	sever
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
..	..	k?	..
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
libovonná	libovonný	k2eAgFnSc1d1	libovonná
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
columnae	columnae	k1gInSc1	columnae
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
velkokališná	velkokališný	k2eAgFnSc1d1	velkokališná
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
macrocalyx	macrocalyx	k1gInSc1	macrocalyx
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
areálu	areál	k1gInSc2	areál
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
i	i	k8xC	i
kříženci	kříženec	k1gMnPc1	kříženec
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
prvosenka	prvosenka	k1gFnSc1	prvosenka
prostřední	prostřední	k2eAgFnSc1d1	prostřední
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
×	×	k?	×
media	medium	k1gNnPc4	medium
<g/>
,	,	kIx,	,
p.	p.	k?	p.
jarní	jarní	k2eAgFnPc1d1	jarní
×	×	k?	×
p.	p.	k?	p.
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Primula	Primula	k?	Primula
×	×	k?	×
polyantha	polyantha	k1gMnSc1	polyantha
(	(	kIx(	(
<g/>
p.	p.	k?	p.
jarní	jarní	k2eAgFnPc4d1	jarní
×	×	k?	×
p.	p.	k?	p.
bezlodyžná	bezlodyžný	k2eAgFnSc1d1	bezlodyžná
<g/>
)	)	kIx)	)
a	a	k8xC	a
Primula	Primula	k?	Primula
×	×	k?	×
murbeckii	murbeckie	k1gFnSc4	murbeckie
(	(	kIx(	(
<g/>
p.	p.	k?	p.
jarní	jarní	k2eAgFnPc4d1	jarní
×	×	k?	×
p.	p.	k?	p.
vyšší	vysoký	k2eAgFnPc4d2	vyšší
×	×	k?	×
p.	p.	k?	p.
bezlodyžná	bezlodyžný	k2eAgFnSc1d1	bezlodyžná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ušlechtilé	ušlechtilý	k2eAgInPc4d1	ušlechtilý
kultivary	kultivar	k1gInPc4	kultivar
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgInPc4d1	jarní
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Primula	Primula	k?	Primula
anglica	anglicus	k1gMnSc2	anglicus
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
květy	květ	k1gInPc7	květ
většinou	většinou	k6eAd1	většinou
červených	červený	k2eAgFnPc2d1	červená
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
často	často	k6eAd1	často
smíšené	smíšený	k2eAgInPc1d1	smíšený
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
hybridy	hybrid	k1gInPc7	hybrid
prvosenky	prvosenka	k1gFnSc2	prvosenka
vyšší	vysoký	k2eAgInPc1d2	vyšší
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
elatior	elatior	k1gInSc1	elatior
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exempláře	exemplář	k1gInPc1	exemplář
s	s	k7c7	s
obrovskými	obrovský	k2eAgInPc7d1	obrovský
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
školkách	školka	k1gFnPc6	školka
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Primula	Primula	k?	Primula
anglica	anglica	k1gMnSc1	anglica
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
projevuje	projevovat	k5eAaImIp3nS	projevovat
heterózní	heterózní	k2eAgInSc4d1	heterózní
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velikost	velikost	k1gFnSc1	velikost
květů	květ	k1gInPc2	květ
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
generacích	generace	k1gFnPc6	generace
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejpodobnějším	podobný	k2eAgInSc7d3	nejpodobnější
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
někdy	někdy	k6eAd1	někdy
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
<g/>
,	,	kIx,	,
prvosenka	prvosenka	k1gFnSc1	prvosenka
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
elatior	elatior	k1gInSc1	elatior
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pro	pro	k7c4	pro
prvosenku	prvosenka	k1gFnSc4	prvosenka
jarní	jarní	k2eAgFnSc4d1	jarní
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
užívá	užívat	k5eAaImIp3nS	užívat
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
petrklíč	petrklíč	k1gInSc1	petrklíč
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
bledě	bledě	k6eAd1	bledě
žlutými	žlutý	k2eAgFnPc7d1	žlutá
slabě	slabě	k6eAd1	slabě
vonnými	vonný	k2eAgInPc7d1	vonný
květy	květ	k1gInPc7	květ
a	a	k8xC	a
ke	k	k7c3	k
koruně	koruna	k1gFnSc3	koruna
přitisklými	přitisklý	k2eAgNnPc7d1	přitisklé
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
odstálými	odstálý	k2eAgFnPc7d1	odstálá
a	a	k8xC	a
nafouklými	nafouklý	k2eAgFnPc7d1	nafouklá
<g/>
,	,	kIx,	,
kalichy	kalich	k1gInPc7	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
ekologické	ekologický	k2eAgFnPc4d1	ekologická
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
vlhkomilnější	vlhkomilný	k2eAgFnSc1d2	vlhkomilný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
druhem	druh	k1gInSc7	druh
i	i	k8xC	i
luhů	luh	k1gInPc2	luh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
většinou	většina	k1gFnSc7	většina
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
květy	květ	k1gInPc4	květ
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
prvosenka	prvosenka	k1gFnSc1	prvosenka
bezlodyžná	bezlodyžný	k2eAgFnSc1d1	bezlodyžná
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
vulgaris	vulgaris	k1gInSc1	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
však	však	k9	však
pozná	poznat	k5eAaPmIp3nS	poznat
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
lodyhu	lodyha	k1gFnSc4	lodyha
a	a	k8xC	a
květy	květ	k1gInPc4	květ
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
tenkých	tenký	k2eAgFnPc6d1	tenká
stopkách	stopka	k1gFnPc6	stopka
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
růžice	růžice	k1gFnSc2	růžice
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
roztroušeně	roztroušeně	k6eAd1	roztroušeně
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
po	po	k7c6	po
podhorské	podhorský	k2eAgFnSc6d1	podhorská
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
asi	asi	k9	asi
800	[number]	k4	800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
vyjma	vyjma	k7c2	vyjma
Středozemí	středozemí	k1gNnSc2	středozemí
a	a	k8xC	a
nejsevernějších	severní	k2eAgFnPc2d3	nejsevernější
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Norska	Norsko	k1gNnPc1	Norsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
severního	severní	k2eAgNnSc2d1	severní
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
části	část	k1gFnSc2	část
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
najít	najít	k5eAaPmF	najít
i	i	k9	i
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
po	po	k7c4	po
severní	severní	k2eAgInSc4d1	severní
Írán	Írán	k1gInSc4	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Druhotně	druhotně	k6eAd1	druhotně
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
nejčastěji	často	k6eAd3	často
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
a	a	k8xC	a
teplomilných	teplomilný	k2eAgFnPc2d1	teplomilná
doubrav	doubrava	k1gFnPc2	doubrava
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInPc6	jejich
okrajích	okraj	k1gInPc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
v	v	k7c6	v
květnatých	květnatý	k2eAgFnPc6d1	květnatá
bučinách	bučina	k1gFnPc6	bučina
<g/>
,	,	kIx,	,
vápnomilných	vápnomilný	k2eAgFnPc6d1	vápnomilná
bučinách	bučina	k1gFnPc6	bučina
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
suťových	suťový	k2eAgInPc2d1	suťový
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nalézt	nalézt	k5eAaBmF	nalézt
prvosenku	prvosenka	k1gFnSc4	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	on	k3xPp3gNnSc4	on
možné	možný	k2eAgNnSc4d1	možné
i	i	k9	i
v	v	k7c6	v
semixerotermních	semixerotermní	k2eAgNnPc6d1	semixerotermní
travinobylinných	travinobylinný	k2eAgNnPc6d1	travinobylinný
společenstvech	společenstvo	k1gNnPc6	společenstvo
(	(	kIx(	(
<g/>
sušší	suchý	k2eAgFnPc1d2	sušší
druhově	druhově	k6eAd1	druhově
bohaté	bohatý	k2eAgFnPc1d1	bohatá
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
někdy	někdy	k6eAd1	někdy
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
na	na	k7c6	na
stabilizovaných	stabilizovaný	k2eAgFnPc6d1	stabilizovaná
(	(	kIx(	(
<g/>
nepřesýpavých	přesýpavý	k2eNgFnPc6d1	přesýpavý
<g/>
)	)	kIx)	)
dunách	duna	k1gFnPc6	duna
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
luhům	luh	k1gInPc3	luh
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
převažujícími	převažující	k2eAgInPc7d1	převažující
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
někdy	někdy	k6eAd1	někdy
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
suchých	suchý	k2eAgNnPc6d1	suché
hlínou	hlína	k1gFnSc7	hlína
pokrytých	pokrytý	k2eAgFnPc6d1	pokrytá
písčitých	písčitý	k2eAgFnPc6d1	písčitá
vyvýšeninách	vyvýšenina	k1gFnPc6	vyvýšenina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hrúdech	hrúdo	k1gNnPc6	hrúdo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fytocenologie	fytocenologie	k1gFnSc2	fytocenologie
===	===	k?	===
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
často	často	k6eAd1	často
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
panonských	panonský	k2eAgFnPc6d1	Panonská
dubohabřinách	dubohabřina	k1gFnPc6	dubohabřina
asociace	asociace	k1gFnSc1	asociace
Primulo	Primulo	k?	Primulo
veris-Carpinetum	veris-Carpinetum	k1gNnSc1	veris-Carpinetum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
teplomilnější	teplomilný	k2eAgInPc1d2	teplomilnější
typy	typ	k1gInPc1	typ
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
ČR	ČR	kA	ČR
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
růst	růst	k5eAaImF	růst
i	i	k9	i
v	v	k7c6	v
teplomilnějších	teplomilný	k2eAgInPc6d2	teplomilnější
typech	typ	k1gInPc6	typ
hercynských	hercynský	k2eAgFnPc2d1	hercynská
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xC	jako
subasociace	subasociace	k1gFnSc2	subasociace
Melampyro	Melampyro	k1gNnSc1	Melampyro
nemorosi-Carpinetum	nemorosi-Carpinetum	k1gNnSc4	nemorosi-Carpinetum
primuletosum	primuletosum	k1gNnSc4	primuletosum
veris	veris	k1gFnSc2	veris
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgInPc1d1	častý
i	i	k9	i
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
částech	část	k1gFnPc6	část
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
analogicky	analogicky	k6eAd1	analogicky
ji	on	k3xPp3gFnSc4	on
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k8xC	i
v	v	k7c6	v
teplomilnějších	teplomilný	k2eAgFnPc6d2	teplomilnější
variantách	varianta	k1gFnPc6	varianta
karpatských	karpatský	k2eAgFnPc2d1	Karpatská
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
as	as	k1gNnSc2	as
<g/>
.	.	kIx.	.
</s>
<s>
Carici	Carik	k1gMnPc1	Carik
pilosae-Carpinetum	pilosae-Carpinetum	k1gNnSc1	pilosae-Carpinetum
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
bazifilních	bazifilní	k2eAgFnPc6d1	bazifilní
teplomilných	teplomilný	k2eAgFnPc6d1	teplomilná
doubravách	doubrava	k1gFnPc6	doubrava
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
lesy	les	k1gInPc1	les
s	s	k7c7	s
dominací	dominace	k1gFnSc7	dominace
dubu	dub	k1gInSc2	dub
šipáku	šipák	k1gInSc2	šipák
a	a	k8xC	a
dubu	dub	k1gInSc2	dub
zimního	zimní	k2eAgInSc2d1	zimní
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
bazických	bazický	k2eAgInPc6d1	bazický
substrátech	substrát	k1gInPc6	substrát
teplých	teplý	k2eAgFnPc2d1	teplá
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
vápenec	vápenec	k1gInSc1	vápenec
a	a	k8xC	a
čedič	čedič	k1gInSc1	čedič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
asociace	asociace	k1gFnSc1	asociace
Lathyro	Lathyro	k1gNnSc4	Lathyro
versicoloris-Kvercetum	versicoloris-Kvercetum	k1gNnSc4	versicoloris-Kvercetum
pubescentis	pubescentis	k1gFnSc2	pubescentis
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Pruno	Pruen	k2eAgNnSc4d1	Pruen
mahaleb-Kvercetum	mahaleb-Kvercetum	k1gNnSc4	mahaleb-Kvercetum
pubescentis	pubescentis	k1gFnSc2	pubescentis
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k6eAd1	něco
mezofilnější	mezofilní	k2eAgInPc4d2	mezofilní
typy	typ	k1gInPc4	typ
řazené	řazený	k2eAgInPc4d1	řazený
do	do	k7c2	do
as	as	k1gNnSc2	as
<g/>
.	.	kIx.	.
</s>
<s>
Euphorbio-Kvercetum	Euphorbio-Kvercetum	k1gNnSc4	Euphorbio-Kvercetum
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Corno-Kvercetum	Corno-Kvercetum	k1gNnSc4	Corno-Kvercetum
<g/>
)	)	kIx)	)
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
růst	růst	k1gInSc4	růst
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
typech	typ	k1gInPc6	typ
teplomilných	teplomilný	k2eAgFnPc2d1	teplomilná
doubrav	doubrava	k1gFnPc2	doubrava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
mochnových	mochnová	k1gFnPc2	mochnová
doubravách	doubrava	k1gFnPc6	doubrava
as	as	k1gNnSc4	as
<g/>
.	.	kIx.	.
</s>
<s>
Melico	Melico	k6eAd1	Melico
pictae-Kvercetum	pictae-Kvercetum	k1gNnSc1	pictae-Kvercetum
roboris	roboris	k1gFnSc2	roboris
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Potentillo	Potentillo	k1gNnSc1	Potentillo
albae-Kvercetum	albae-Kvercetum	k1gNnSc1	albae-Kvercetum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
i	i	k9	i
pro	pro	k7c4	pro
suťové	suťový	k2eAgInPc4d1	suťový
lesy	les	k1gInPc4	les
subasociace	subasociace	k1gFnSc2	subasociace
Aceri-Carpinetum	Aceri-Carpinetum	k1gNnSc1	Aceri-Carpinetum
aconitetosum	aconitetosum	k1gInSc4	aconitetosum
vulpariae	vulparia	k1gFnSc2	vulparia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
karbonátových	karbonátový	k2eAgFnPc6d1	karbonátová
horninách	hornina	k1gFnPc6	hornina
(	(	kIx(	(
<g/>
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
mramor	mramor	k1gInSc4	mramor
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
a	a	k8xC	a
Českém	český	k2eAgInSc6d1	český
krasu	kras	k1gInSc6	kras
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
také	také	k9	také
v	v	k7c6	v
borech	bor	k1gInPc6	bor
svazu	svaz	k1gInSc2	svaz
Cytiso	Cytisa	k1gFnSc5	Cytisa
ruthenici-Pinion	ruthenici-Pinion	k1gInSc4	ruthenici-Pinion
sylvestris	sylvestris	k1gFnPc4	sylvestris
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
lesostepní	lesostepní	k2eAgInPc1d1	lesostepní
bory	bor	k1gInPc1	bor
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
dubu	dub	k1gInSc2	dub
nebo	nebo	k8xC	nebo
břízy	bříza	k1gFnSc2	bříza
<g/>
)	)	kIx)	)
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
na	na	k7c6	na
opukách	opuka	k1gFnPc6	opuka
a	a	k8xC	a
vápenitých	vápenitý	k2eAgInPc6d1	vápenitý
pískovcích	pískovec	k1gInPc6	pískovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
tzv.	tzv.	kA	tzv.
bílých	bílý	k2eAgFnPc2d1	bílá
strání	stráň	k1gFnPc2	stráň
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
až	až	k8xS	až
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
častá	častý	k2eAgFnSc1d1	častá
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
suchých	suchý	k2eAgInPc2d1	suchý
trávníků	trávník	k1gInPc2	trávník
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
pěchavové	pěchavový	k2eAgInPc1d1	pěchavový
trávníky	trávník	k1gInPc1	trávník
sv.	sv.	kA	sv.
Diantho	Diant	k1gMnSc2	Diant
lumnitzeri-Seslerion	lumnitzeri-Seslerion	k1gInSc4	lumnitzeri-Seslerion
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
zapojené	zapojený	k2eAgInPc1d1	zapojený
trávníky	trávník	k1gInPc1	trávník
s	s	k7c7	s
dominancí	dominance	k1gFnSc7	dominance
pěchavy	pěchava	k1gFnSc2	pěchava
vápnomilné	vápnomilný	k2eAgFnSc2d1	vápnomilná
(	(	kIx(	(
<g/>
Sesleria	Seslerium	k1gNnSc2	Seslerium
caerulea	caerule	k1gInSc2	caerule
<g/>
)	)	kIx)	)
v	v	k7c6	v
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
oblastech	oblast	k1gFnPc6	oblast
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
vápencích	vápenec	k1gInPc6	vápenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
vyšších	vysoký	k2eAgFnPc2d2	vyšší
hor	hora	k1gFnPc2	hora
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
širokolistých	širokolistý	k2eAgInPc6d1	širokolistý
suchých	suchý	k2eAgInPc6d1	suchý
trávnících	trávník	k1gInPc6	trávník
sv.	sv.	kA	sv.
Bromion	Bromion	k1gInSc4	Bromion
erecti	erecti	k1gNnSc2	erecti
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
dominanta	dominanta	k1gFnSc1	dominanta
často	často	k6eAd1	často
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
sveřep	sveřep	k1gInSc1	sveřep
vzpřímený	vzpřímený	k2eAgInSc1d1	vzpřímený
(	(	kIx(	(
<g/>
Bromus	Bromus	k1gInSc1	Bromus
erectus	erectus	k1gInSc1	erectus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitat	k5eAaPmIp3nS	prapořitat
(	(	kIx(	(
<g/>
Brachypodium	Brachypodium	k1gNnSc1	Brachypodium
pinnatum	pinnatum	k1gNnSc1	pinnatum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
asociaci	asociace	k1gFnSc6	asociace
Brachypodio	Brachypodio	k6eAd1	Brachypodio
pinnati-Molinietum	pinnati-Molinietum	k1gNnSc1	pinnati-Molinietum
arundinaceae	arundinaceae	k1gFnPc2	arundinaceae
ze	z	k7c2	z
sv.	sv.	kA	sv.
Bromion	Bromion	k1gInSc1	Bromion
erecti	erecti	k1gNnSc2	erecti
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
dokonce	dokonce	k9	dokonce
jako	jako	k8xS	jako
diagnostický	diagnostický	k2eAgInSc4d1	diagnostický
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
suchých	suchý	k2eAgFnPc2d1	suchá
luk	louka	k1gFnPc2	louka
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
růst	růst	k5eAaImF	růst
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
typech	typ	k1gInPc6	typ
trávníků	trávník	k1gInPc2	trávník
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
sušší	suchý	k2eAgFnPc1d2	sušší
varianty	varianta	k1gFnPc1	varianta
mezofilních	mezofilní	k2eAgFnPc2d1	mezofilní
luk	louka	k1gFnPc2	louka
sv.	sv.	kA	sv.
Arrhenatherion	Arrhenatherion	k1gInSc1	Arrhenatherion
elatioris	elatioris	k1gInSc1	elatioris
<g/>
,	,	kIx,	,
pastvin	pastvina	k1gFnPc2	pastvina
sv.	sv.	kA	sv.
Cynosurion	Cynosurion	k1gInSc1	Cynosurion
cristati	cristat	k5eAaImF	cristat
nebo	nebo	k8xC	nebo
lesních	lesní	k2eAgInPc2d1	lesní
lemů	lem	k1gInPc2	lem
sv.	sv.	kA	sv.
Trifolion	Trifolion	k1gInSc1	Trifolion
medii	medium	k1gNnPc7	medium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Optimální	optimální	k2eAgFnPc4d1	optimální
podmínky	podmínka	k1gFnPc4	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgNnSc1d1	jarní
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc4d1	typický
druh	druh	k1gInSc4	druh
dobře	dobře	k6eAd1	dobře
prosvětlených	prosvětlený	k2eAgNnPc2d1	prosvětlené
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
přežít	přežít	k5eAaPmF	přežít
pod	pod	k7c7	pod
částečným	částečný	k2eAgInSc7d1	částečný
stínem	stín	k1gInSc7	stín
i	i	k8xC	i
když	když	k8xS	když
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
sníženou	snížený	k2eAgFnSc4d1	snížená
životaschopnost	životaschopnost	k1gFnSc4	životaschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
experimentálních	experimentální	k2eAgNnPc2d1	experimentální
měření	měření	k1gNnPc2	měření
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
absorpce	absorpce	k1gFnSc1	absorpce
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
u	u	k7c2	u
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
<g/>
,	,	kIx,	,
prvosenky	prvosenka	k1gFnPc4	prvosenka
bezlodyžné	bezlodyžný	k2eAgFnPc4d1	bezlodyžná
a	a	k8xC	a
prvosenky	prvosenka	k1gFnPc1	prvosenka
vyšší	vysoký	k2eAgFnPc1d2	vyšší
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zvyšujícího	zvyšující	k2eAgNnSc2d1	zvyšující
zastínění	zastínění	k1gNnSc2	zastínění
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
byla	být	k5eAaImAgFnS	být
nejméně	málo	k6eAd3	málo
<g/>
,	,	kIx,	,
a	a	k8xC	a
prvosenka	prvosenka	k1gFnSc1	prvosenka
bezlodyžná	bezlodyžný	k2eAgFnSc1d1	bezlodyžná
nejvíce	hodně	k6eAd3	hodně
snášenlivá	snášenlivý	k2eAgFnSc1d1	snášenlivá
vůči	vůči	k7c3	vůči
zastínění	zastínění	k1gNnSc3	zastínění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
většinou	většina	k1gFnSc7	většina
chybí	chybit	k5eAaPmIp3nS	chybit
na	na	k7c6	na
pastvinách	pastvina	k1gFnPc6	pastvina
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hladinou	hladina	k1gFnSc7	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přírůstek	přírůstek	k1gInSc1	přírůstek
sušiny	sušina	k1gFnSc2	sušina
u	u	k7c2	u
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
snižuje	snižovat	k5eAaImIp3nS	snižovat
při	při	k7c6	při
experimentálně	experimentálně	k6eAd1	experimentálně
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
hladině	hladina	k1gFnSc3	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
už	už	k6eAd1	už
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
růstu	růst	k1gInSc2	růst
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
rostlin	rostlina	k1gFnPc2	rostlina
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
anoxických	anoxický	k2eAgFnPc2d1	anoxický
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k2eAgFnPc1d1	Kořena
prvosenky	prvosenka	k1gFnPc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
prorůstají	prorůstat	k5eAaImIp3nP	prorůstat
jen	jen	k9	jen
horními	horní	k2eAgFnPc7d1	horní
půdními	půdní	k2eAgFnPc7d1	půdní
vrstvami	vrstva	k1gFnPc7	vrstva
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
druhy	druh	k1gInPc4	druh
nesnášenlivé	snášenlivý	k2eNgInPc4d1	nesnášenlivý
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgNnSc3d1	dlouhodobé
zamokření	zamokření	k1gNnSc3	zamokření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
prvosenkou	prvosenka	k1gFnSc7	prvosenka
vyšší	vysoký	k2eAgFnSc7d2	vyšší
a	a	k8xC	a
prvosenkou	prvosenka	k1gFnSc7	prvosenka
bezlodyžnou	bezlodyžný	k2eAgFnSc7d1	bezlodyžná
je	být	k5eAaImIp3nS	být
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
většinou	většinou	k6eAd1	většinou
více	hodně	k6eAd2	hodně
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
k	k	k7c3	k
suchu	sucho	k1gNnSc3	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
O.	O.	kA	O.
Inghe	Inghe	k1gInSc1	Inghe
a	a	k8xC	a
C.	C.	kA	C.
O.	O.	kA	O.
Tamm	Tamm	k1gMnSc1	Tamm
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
dočasné	dočasný	k2eAgNnSc4d1	dočasné
chřadnutí	chřadnutí	k1gNnSc4	chřadnutí
listů	list	k1gInPc2	list
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
během	během	k7c2	během
suchého	suchý	k2eAgNnSc2d1	suché
léta	léto	k1gNnSc2	léto
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
nezjistili	zjistit	k5eNaPmAgMnP	zjistit
významný	významný	k2eAgInSc4d1	významný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
přežití	přežití	k1gNnSc2	přežití
takových	takový	k3xDgMnPc2	takový
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výjimečně	výjimečně	k6eAd1	výjimečně
suchých	suchý	k2eAgFnPc6d1	suchá
a	a	k8xC	a
horkých	horký	k2eAgFnPc6d1	horká
podmínkách	podmínka	k1gFnPc6	podmínka
během	během	k7c2	během
kvetení	kvetení	k1gNnSc2	kvetení
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
významnému	významný	k2eAgNnSc3d1	významné
zmenšení	zmenšení	k1gNnSc3	zmenšení
množství	množství	k1gNnSc2	množství
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
<g/>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
kyprých	kyprý	k2eAgInPc6d1	kyprý
eutrofních	eutrofní	k2eAgInPc6d1	eutrofní
až	až	k8xS	až
mezotrofních	mezotrofní	k2eAgInPc6d1	mezotrofní
<g/>
,	,	kIx,	,
půdách	půda	k1gFnPc6	půda
s	s	k7c7	s
neutrální	neutrální	k2eAgFnSc7d1	neutrální
až	až	k8xS	až
mírně	mírně	k6eAd1	mírně
kyselou	kyselý	k2eAgFnSc7d1	kyselá
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Kyselé	kyselý	k2eAgFnPc1d1	kyselá
půdy	půda	k1gFnPc1	půda
nesnáší	snášet	k5eNaImIp3nP	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Snese	snést	k5eAaPmIp3nS	snést
půdy	půda	k1gFnPc4	půda
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
osídlit	osídlit	k5eAaPmF	osídlit
i	i	k9	i
půdy	půda	k1gFnPc1	půda
dosti	dosti	k6eAd1	dosti
těžké	těžký	k2eAgFnPc1d1	těžká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
optimum	optimum	k1gNnSc1	optimum
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
relativně	relativně	k6eAd1	relativně
kyprých	kyprý	k2eAgMnPc2d1	kyprý
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
hojně	hojně	k6eAd1	hojně
i	i	k9	i
na	na	k7c6	na
vrcholových	vrcholový	k2eAgFnPc6d1	vrcholová
sutích	suť	k1gFnPc6	suť
<g/>
)	)	kIx)	)
a	a	k8xC	a
minerálně	minerálně	k6eAd1	minerálně
dosti	dosti	k6eAd1	dosti
bohatých	bohatý	k2eAgMnPc2d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
se	se	k3xPyFc4	se
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
mladých	mladý	k2eAgFnPc6d1	mladá
vyvřelinách	vyvřelina	k1gFnPc6	vyvřelina
(	(	kIx(	(
<g/>
čedič	čedič	k1gInSc4	čedič
<g/>
,	,	kIx,	,
andezit	andezit	k1gInSc4	andezit
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
vápenci	vápenec	k1gInSc6	vápenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
půdy	půda	k1gFnPc4	půda
chudé	chudý	k2eAgFnPc4d1	chudá
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
hodnoty	hodnota	k1gFnPc1	hodnota
dusíku	dusík	k1gInSc2	dusík
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
klesání	klesání	k1gNnSc2	klesání
velikosti	velikost	k1gFnSc2	velikost
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
výsledek	výsledek	k1gInSc1	výsledek
sníženého	snížený	k2eAgInSc2d1	snížený
počtu	počet	k1gInSc2	počet
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
tedy	tedy	k9	tedy
produkce	produkce	k1gFnSc1	produkce
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
projekce	projekce	k1gFnPc1	projekce
také	také	k9	také
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
hodnotám	hodnota	k1gFnPc3	hodnota
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
(	(	kIx(	(
<g/>
9	[number]	k4	9
g	g	kA	g
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
m	m	kA	m
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
rok	rok	k1gInSc1	rok
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
během	během	k7c2	během
30	[number]	k4	30
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
běžný	běžný	k2eAgInSc1d1	běžný
jednoletý	jednoletý	k2eAgInSc1d1	jednoletý
populační	populační	k2eAgInSc1d1	populační
pokles	pokles	k1gInSc1	pokles
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
18,2	[number]	k4	18,2
%	%	kIx~	%
a	a	k8xC	a
15,5	[number]	k4	15,5
%	%	kIx~	%
a	a	k8xC	a
běžný	běžný	k2eAgInSc4d1	běžný
čas	čas	k1gInSc4	čas
zániku	zánik	k1gInSc2	zánik
od	od	k7c2	od
13,7	[number]	k4	13,7
a	a	k8xC	a
24,4	[number]	k4	24,4
léta	léto	k1gNnSc2	léto
při	při	k7c6	při
kosení	kosení	k1gNnSc6	kosení
a	a	k8xC	a
pastvě	pastva	k1gFnSc6	pastva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
porosty	porost	k1gInPc1	porost
bez	bez	k7c2	bez
přihnojování	přihnojování	k1gNnPc2	přihnojování
měly	mít	k5eAaImAgFnP	mít
běžný	běžný	k2eAgInSc4d1	běžný
přírůstek	přírůstek	k1gInSc4	přírůstek
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
příznivé	příznivý	k2eAgFnPc1d1	příznivá
<g/>
,	,	kIx,	,
populace	populace	k1gFnPc1	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgNnSc1d1	jarní
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
velké	velký	k2eAgInPc4d1	velký
a	a	k8xC	a
viditelné	viditelný	k2eAgMnPc4d1	viditelný
jedince	jedinec	k1gMnPc4	jedinec
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
porostu	porost	k1gInSc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
není	být	k5eNaImIp3nS	být
nikdy	nikdy	k6eAd1	nikdy
dominantní	dominantní	k2eAgFnSc7d1	dominantní
rostlinou	rostlina	k1gFnSc7	rostlina
v	v	k7c6	v
porostu	porost	k1gInSc6	porost
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hustoty	hustota	k1gFnSc2	hustota
mezi	mezi	k7c7	mezi
10	[number]	k4	10
a	a	k8xC	a
34	[number]	k4	34
jedinci	jedinec	k1gMnPc7	jedinec
na	na	k7c6	na
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
výsevu	výsev	k1gInSc6	výsev
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hustoty	hustota	k1gFnSc2	hustota
až	až	k9	až
do	do	k7c2	do
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
jedinců	jedinec	k1gMnPc2	jedinec
na	na	k7c4	na
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
omezené	omezený	k2eAgFnSc3d1	omezená
možnosti	možnost	k1gFnSc3	možnost
rozptylování	rozptylování	k1gNnSc2	rozptylování
semen	semeno	k1gNnPc2	semeno
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
šíření	šíření	k1gNnSc2	šíření
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mladé	mladý	k2eAgFnPc1d1	mladá
rostliny	rostlina	k1gFnPc1	rostlina
většinou	většinou	k6eAd1	většinou
spíše	spíše	k9	spíše
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
seskupené	seskupený	k2eAgInPc1d1	seskupený
kolem	kolem	k7c2	kolem
dospělých	dospělý	k2eAgFnPc2d1	dospělá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
shluky	shluk	k1gInPc1	shluk
prvosenek	prvosenka	k1gFnPc2	prvosenka
jarních	jarní	k2eAgFnPc2d1	jarní
vzniknout	vzniknout	k5eAaPmF	vzniknout
následkem	následek	k1gInSc7	následek
vegetativního	vegetativní	k2eAgNnSc2d1	vegetativní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
málo	málo	k6eAd1	málo
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
tvoří	tvořit	k5eAaImIp3nS	tvořit
často	často	k6eAd1	často
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
pestře	pestro	k6eAd1	pestro
žlutě	žlutě	k6eAd1	žlutě
zbarvené	zbarvený	k2eAgInPc4d1	zbarvený
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
pospolitě	pospolitě	k6eAd1	pospolitě
<g/>
,	,	kIx,	,
přirozené	přirozený	k2eAgFnSc6d1	přirozená
obnově	obnova	k1gFnSc6	obnova
dřevin	dřevina	k1gFnPc2	dřevina
však	však	k9	však
nezbraňuje	zbraňovat	k5eNaImIp3nS	zbraňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
rostou	růst	k5eAaImIp3nP	růst
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
prvosenky	prvosenka	k1gFnPc1	prvosenka
jarní	jarní	k2eAgFnPc1d1	jarní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
šedavá	šedavý	k2eAgFnSc1d1	šedavá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
kategorii	kategorie	k1gFnSc4	kategorie
VU	VU	kA	VU
<g/>
)	)	kIx)	)
a	a	k8xC	a
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
pravá	pravá	k1gFnSc1	pravá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
vzácnější	vzácný	k2eAgInPc4d2	vzácnější
taxony	taxon	k1gInPc4	taxon
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
kategorii	kategorie	k1gFnSc4	kategorie
NT	NT	kA	NT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
původního	původní	k2eAgInSc2d1	původní
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
populace	populace	k1gFnSc2	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
klesají	klesat	k5eAaImIp3nP	klesat
hlavně	hlavně	k6eAd1	hlavně
následkem	následkem	k7c2	následkem
změněného	změněný	k2eAgNnSc2d1	změněné
využití	využití	k1gNnSc2	využití
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
pokračujícím	pokračující	k2eAgNnSc7d1	pokračující
zvýšením	zvýšení	k1gNnSc7	zvýšení
obsahu	obsah	k1gInSc2	obsah
živin	živina	k1gFnPc2	živina
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
(	(	kIx(	(
<g/>
hlavně	hlavně	k6eAd1	hlavně
dusíku	dusík	k1gInSc3	dusík
<g/>
)	)	kIx)	)
pronikajících	pronikající	k2eAgMnPc2d1	pronikající
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
populace	populace	k1gFnSc1	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
dramatický	dramatický	k2eAgInSc4d1	dramatický
pokles	pokles	k1gInSc4	pokles
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1930	[number]	k4	1930
a	a	k8xC	a
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
výskytu	výskyt	k1gInSc2	výskyt
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
zejména	zejména	k9	zejména
nárůstem	nárůst	k1gInSc7	nárůst
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
používáním	používání	k1gNnSc7	používání
herbicidů	herbicid	k1gInPc2	herbicid
<g/>
,	,	kIx,	,
eutrofizací	eutrofizace	k1gFnPc2	eutrofizace
stálých	stálý	k2eAgFnPc2d1	stálá
luk	louka	k1gFnPc2	louka
<g/>
,	,	kIx,	,
a	a	k8xC	a
orbou	orba	k1gFnSc7	orba
travních	travní	k2eAgInPc2d1	travní
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
<g/>
Velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
důvodem	důvod	k1gInSc7	důvod
úbytku	úbytek	k1gInSc2	úbytek
prvosenek	prvosenka	k1gFnPc2	prvosenka
jarních	jarní	k2eAgFnPc2d1	jarní
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
lesního	lesní	k2eAgNnSc2d1	lesní
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Dubohabřiny	Dubohabřina	k1gFnPc1	Dubohabřina
jsou	být	k5eAaImIp3nP	být
káceny	kácen	k2eAgFnPc1d1	kácen
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
jsou	být	k5eAaImIp3nP	být
vysazovány	vysazován	k2eAgFnPc1d1	vysazována
kultury	kultura	k1gFnPc1	kultura
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dubohabřiny	dubohabřina	k1gFnPc1	dubohabřina
ponechány	ponechat	k5eAaPmNgFnP	ponechat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
prosvětlovány	prosvětlovat	k5eAaImNgInP	prosvětlovat
probírkou	probírka	k1gFnSc7	probírka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přirozeně	přirozeně	k6eAd1	přirozeně
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
rozvoje	rozvoj	k1gInSc2	rozvoj
keřového	keřový	k2eAgNnSc2d1	keřové
patra	patro	k1gNnSc2	patro
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
bylinné	bylinný	k2eAgNnSc1d1	bylinné
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prvosenka	prvosenka	k1gFnSc1	prvosenka
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
zarostlé	zarostlý	k2eAgInPc1d1	zarostlý
křovinami	křovina	k1gFnPc7	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
světlomilná	světlomilný	k2eAgFnSc1d1	světlomilná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ubývá	ubývat	k5eAaImIp3nS	ubývat
i	i	k9	i
v	v	k7c6	v
takto	takto	k6eAd1	takto
udržovaných	udržovaný	k2eAgFnPc6d1	udržovaná
přirozených	přirozený	k2eAgFnPc6d1	přirozená
stanovištích	stanoviště	k1gNnPc6	stanoviště
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
mnoho	mnoho	k4c1	mnoho
míst	místo	k1gNnPc2	místo
výskytu	výskyt	k1gInSc2	výskyt
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
je	být	k5eAaImIp3nS	být
omezené	omezený	k2eAgNnSc4d1	omezené
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
lokality	lokalita	k1gFnPc4	lokalita
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc4	rostlina
jsou	být	k5eAaImIp3nP	být
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
vysoce	vysoce	k6eAd1	vysoce
citlivé	citlivý	k2eAgInPc4d1	citlivý
na	na	k7c4	na
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
příliv	příliv	k1gInSc4	příliv
živin	živina	k1gFnPc2	živina
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
ať	ať	k8xS	ať
už	už	k6eAd1	už
vlivem	vlivem	k7c2	vlivem
zemědělského	zemědělský	k2eAgNnSc2d1	zemědělské
a	a	k8xC	a
lesnického	lesnický	k2eAgNnSc2d1	lesnické
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nakládání	nakládání	k1gNnSc1	nakládání
s	s	k7c7	s
odpady	odpad	k1gInPc7	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
pastevectví	pastevectví	k1gNnSc1	pastevectví
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
ujmutí	ujmutí	k1gNnSc4	ujmutí
semenáčků	semenáček	k1gInPc2	semenáček
a	a	k8xC	a
růst	růst	k1gInSc4	růst
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pastva	pastva	k1gFnSc1	pastva
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivní	efektivní	k2eAgFnSc4d1	efektivní
produkci	produkce	k1gFnSc4	produkce
semen	semeno	k1gNnPc2	semeno
a	a	k8xC	a
optimální	optimální	k2eAgFnSc4d1	optimální
strategii	strategie	k1gFnSc4	strategie
udržení	udržení	k1gNnSc2	udržení
populace	populace	k1gFnSc2	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
horizontu	horizont	k1gInSc6	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
populace	populace	k1gFnPc4	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgNnSc1d1	jarní
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
přiměřené	přiměřený	k2eAgNnSc1d1	přiměřené
hospodaření	hospodaření	k1gNnSc1	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
kosení	kosení	k1gNnSc1	kosení
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
pastva	pastva	k1gFnSc1	pastva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
úpravy	úprava	k1gFnPc1	úprava
brání	bránit	k5eAaImIp3nP	bránit
sukcesi	sukcese	k1gFnSc4	sukcese
do	do	k7c2	do
lesa	les	k1gInSc2	les
a	a	k8xC	a
hustých	hustý	k2eAgFnPc2d1	hustá
křovin	křovina	k1gFnPc2	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Pastva	pastva	k1gFnSc1	pastva
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
také	také	k9	také
mozaikovitou	mozaikovitý	k2eAgFnSc4d1	mozaikovitá
krajinu	krajina	k1gFnSc4	krajina
s	s	k7c7	s
trávníky	trávník	k1gInPc7	trávník
a	a	k8xC	a
řídkými	řídký	k2eAgFnPc7d1	řídká
křovinami	křovina	k1gFnPc7	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ponechání	ponechání	k1gNnSc6	ponechání
krajiny	krajina	k1gFnSc2	krajina
s	s	k7c7	s
trávníky	trávník	k1gInPc7	trávník
a	a	k8xC	a
řídkými	řídký	k2eAgFnPc7d1	řídká
křovinami	křovina	k1gFnPc7	křovina
ladem	ladem	k6eAd1	ladem
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
zarůstání	zarůstání	k1gNnSc3	zarůstání
luk	luka	k1gNnPc2	luka
expanzívními	expanzívní	k2eAgInPc7d1	expanzívní
druhy	druh	k1gInPc7	druh
a	a	k8xC	a
postupné	postupný	k2eAgFnSc3d1	postupná
sukcesi	sukcese	k1gFnSc3	sukcese
ke	k	k7c3	k
stinnému	stinný	k2eAgInSc3d1	stinný
lesu	les	k1gInSc3	les
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
prvosenku	prvosenka	k1gFnSc4	prvosenka
jarní	jarní	k2eAgFnSc4d1	jarní
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
prvosenek	prvosenka	k1gFnPc2	prvosenka
jarních	jarní	k2eAgInPc2d1	jarní
a	a	k8xC	a
rozkvetlých	rozkvetlý	k2eAgInPc2d1	rozkvetlý
květů	květ	k1gInPc2	květ
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
snížil	snížit	k5eAaPmAgInS	snížit
po	po	k7c6	po
ustoupení	ustoupení	k1gNnSc6	ustoupení
od	od	k7c2	od
tradičního	tradiční	k2eAgNnSc2d1	tradiční
pastevectví	pastevectví	k1gNnSc2	pastevectví
a	a	k8xC	a
kosení	kosení	k1gNnSc2	kosení
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
následkem	následkem	k7c2	následkem
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
lesnictví	lesnictví	k1gNnSc6	lesnictví
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
stín	stín	k1gInSc1	stín
také	také	k9	také
redukuje	redukovat	k5eAaBmIp3nS	redukovat
semenné	semenný	k2eAgNnSc4d1	semenné
zrání	zrání	k1gNnSc4	zrání
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc4d1	mající
za	za	k7c4	za
následek	následek	k1gInSc4	následek
významně	významně	k6eAd1	významně
nižší	nízký	k2eAgFnPc1d2	nižší
semenné	semenný	k2eAgFnPc1d1	semenná
produkční	produkční	k2eAgFnPc1d1	produkční
dávky	dávka	k1gFnPc1	dávka
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
necloněnými	cloněný	k2eNgMnPc7d1	cloněný
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Sukcese	sukcese	k1gFnSc1	sukcese
lesa	les	k1gInSc2	les
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
snížený	snížený	k2eAgInSc4d1	snížený
přírůstek	přírůstek	k1gInSc4	přírůstek
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
sníženému	snížený	k2eAgNnSc3d1	snížené
pohlavnímu	pohlavní	k2eAgNnSc3d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
a	a	k8xC	a
stárnutí	stárnutí	k1gNnSc3	stárnutí
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jedinci	jedinec	k1gMnPc7	jedinec
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgNnSc4d1	jarní
nalezení	nalezení	k1gNnSc4	nalezení
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
lesa	les	k1gInSc2	les
a	a	k8xC	a
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
prosvětlených	prosvětlený	k2eAgInPc6d1	prosvětlený
lesích	les	k1gInPc6	les
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lesích	les	k1gInPc6	les
s	s	k7c7	s
řídkými	řídký	k2eAgFnPc7d1	řídká
křovinami	křovina	k1gFnPc7	křovina
mohou	moct	k5eAaImIp3nP	moct
kvést	kvést	k5eAaImF	kvést
hojně	hojně	k6eAd1	hojně
a	a	k8xC	a
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vysévány	vyséván	k2eAgFnPc1d1	vysévána
prvosenky	prvosenka	k1gFnPc1	prvosenka
jarní	jarní	k2eAgFnPc1d1	jarní
u	u	k7c2	u
krajnic	krajnice	k1gFnPc2	krajnice
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
přidávána	přidávat	k5eAaImNgNnP	přidávat
do	do	k7c2	do
směsi	směs	k1gFnSc2	směs
osiv	osiva	k1gFnPc2	osiva
s	s	k7c7	s
divokými	divoký	k2eAgFnPc7d1	divoká
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
zvětšila	zvětšit	k5eAaPmAgNnP	zvětšit
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
prvosenek	prvosenka	k1gFnPc2	prvosenka
jarních	jarní	k2eAgFnPc2d1	jarní
se	se	k3xPyFc4	se
také	také	k6eAd1	také
vrátily	vrátit	k5eAaPmAgFnP	vrátit
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
pastva	pastva	k1gFnSc1	pastva
výrazně	výrazně	k6eAd1	výrazně
snižovala	snižovat	k5eAaImAgFnS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
hřbitovů	hřbitov	k1gInPc2	hřbitov
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
udržovány	udržovat	k5eAaImNgFnP	udržovat
v	v	k7c6	v
přírodním	přírodní	k2eAgInSc6d1	přírodní
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
vhodném	vhodný	k2eAgMnSc6d1	vhodný
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
přirozených	přirozený	k2eAgNnPc2d1	přirozené
společenstev	společenstvo	k1gNnPc2	společenstvo
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
této	tento	k3xDgFnSc3	tento
oblíbené	oblíbený	k2eAgFnSc3d1	oblíbená
jarní	jarní	k2eAgFnSc3d1	jarní
květině	květina	k1gFnSc3	květina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc1	škůdce
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
prvosence	prvosenka	k1gFnSc6	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
různé	různý	k2eAgFnPc4d1	různá
choroby	choroba	k1gFnPc4	choroba
způsobené	způsobený	k2eAgFnPc1d1	způsobená
houbami	houba	k1gFnPc7	houba
či	či	k8xC	či
jim	on	k3xPp3gMnPc3	on
podobnými	podobný	k2eAgInPc7d1	podobný
organizmy	organizmus	k1gInPc7	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
plíseň	plíseň	k1gFnSc1	plíseň
šedá	šedý	k2eAgFnSc1d1	šedá
(	(	kIx(	(
<g/>
Botrytis	Botrytis	k1gFnSc1	Botrytis
cinerea	cinerea	k1gMnSc1	cinerea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
botrytidu	botrytida	k1gFnSc4	botrytida
<g/>
.	.	kIx.	.
</s>
<s>
Napadá	napadat	k5eAaBmIp3nS	napadat
různé	různý	k2eAgFnSc2d1	různá
části	část	k1gFnSc2	část
rostlin	rostlina	k1gFnPc2	rostlina
zejména	zejména	k9	zejména
při	při	k7c6	při
převlhčení	převlhčení	k1gNnSc6	převlhčení
a	a	k8xC	a
hustém	hustý	k2eAgInSc6d1	hustý
zápoji	zápoj	k1gInSc6	zápoj
<g/>
.	.	kIx.	.
</s>
<s>
Skvrny	skvrna	k1gFnPc1	skvrna
na	na	k7c6	na
listech	list	k1gInPc6	list
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
např.	např.	kA	např.
houby	houba	k1gFnSc2	houba
větevnatky	větevnatka	k1gFnSc2	větevnatka
Ramularia	Ramularium	k1gNnSc2	Ramularium
primulae	primula	k1gFnSc2	primula
a	a	k8xC	a
Ramularia	Ramularium	k1gNnSc2	Ramularium
interstitialis	interstitialis	k1gFnSc2	interstitialis
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
napadá	napadat	k5eAaPmIp3nS	napadat
i	i	k9	i
Cercosporella	Cercosporella	k1gFnSc1	Cercosporella
primulae	primulae	k1gFnSc1	primulae
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
rostlinu	rostlina	k1gFnSc4	rostlina
napadá	napadat	k5eAaBmIp3nS	napadat
oomyceta	oomycet	k2eAgFnSc1d1	oomycet
Peronospora	peronospora	k1gFnSc1	peronospora
oerteliana	oertelian	k1gMnSc2	oertelian
<g/>
,	,	kIx,	,
semeníky	semeník	k1gInPc7	semeník
pak	pak	k6eAd1	pak
sněť	sněť	k1gFnSc1	sněť
Urocystis	Urocystis	k1gFnSc1	Urocystis
primulae	primulae	k1gFnSc1	primulae
<g/>
.	.	kIx.	.
</s>
<s>
Hnědnutí	hnědnutí	k1gNnSc1	hnědnutí
kořenů	kořen	k1gInPc2	kořen
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
úhyn	úhyn	k1gInSc1	úhyn
rostlin	rostlina	k1gFnPc2	rostlina
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
houba	houba	k1gFnSc1	houba
Thielavia	Thielavia	k1gFnSc1	Thielavia
vasicola	vasicola	k1gFnSc1	vasicola
<g/>
.	.	kIx.	.
</s>
<s>
Řidčeji	řídce	k6eAd2	řídce
se	se	k3xPyFc4	se
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
čepelí	čepel	k1gFnPc2	čepel
objevují	objevovat	k5eAaImIp3nP	objevovat
rzi	rez	k1gFnSc2	rez
Uromyces	Uromycesa	k1gFnPc2	Uromycesa
primulae	primulae	k1gInSc1	primulae
a	a	k8xC	a
rez	rez	k1gFnSc1	rez
primulková	primulkový	k2eAgFnSc1d1	primulkový
(	(	kIx(	(
<g/>
Puccinia	Puccinium	k1gNnPc1	Puccinium
primulae	primulae	k1gFnPc2	primulae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
provosence	provosenka	k1gFnSc6	provosenka
jarní	jarní	k2eAgNnSc4d1	jarní
parazitují	parazitovat	k5eAaImIp3nP	parazitovat
i	i	k8xC	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
žijí	žít	k5eAaImIp3nP	žít
háďátka	háďátko	k1gNnPc1	háďátko
(	(	kIx(	(
<g/>
Aphelenchus	Aphelenchus	k1gInSc1	Aphelenchus
olesistus	olesistus	k1gInSc1	olesistus
a	a	k8xC	a
rod	rod	k1gInSc1	rod
Heterodora	Heterodora	k1gFnSc1	Heterodora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc4	část
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
jsou	být	k5eAaImIp3nP	být
požírány	požírat	k5eAaImNgInP	požírat
či	či	k8xC	či
poškozovány	poškozovat	k5eAaImNgInP	poškozovat
více	hodně	k6eAd2	hodně
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
jejich	jejich	k3xOp3gFnPc7	jejich
larvami	larva	k1gFnPc7	larva
<g/>
)	)	kIx)	)
a	a	k8xC	a
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnSc2	semeno
např.	např.	kA	např.
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
larva	larva	k1gFnSc1	larva
mola	mol	k1gMnSc2	mol
Amblyptilia	Amblyptilius	k1gMnSc2	Amblyptilius
punctidactyla	punctidactýt	k5eAaPmAgFnS	punctidactýt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvosenkách	prvosenka	k1gFnPc6	prvosenka
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
třásněnky	třásněnka	k1gFnSc2	třásněnka
(	(	kIx(	(
<g/>
Thysanoptera	Thysanopter	k1gMnSc2	Thysanopter
<g/>
)	)	kIx)	)
a	a	k8xC	a
housenky	housenka	k1gFnSc2	housenka
některých	některý	k3yIgMnPc2	některý
píďalkovitých	píďalkovití	k1gMnPc2	píďalkovití
<g/>
.	.	kIx.	.
</s>
<s>
Molice	molice	k1gFnSc1	molice
(	(	kIx(	(
<g/>
Aleyrodidae	Aleyrodidae	k1gFnSc1	Aleyrodidae
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
sají	sát	k5eAaImIp3nP	sát
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnSc2	housenka
můry	můra	k1gFnSc2	můra
osenní	osenní	k2eAgMnSc1d1	osenní
(	(	kIx(	(
<g/>
Agrotis	Agrotis	k1gInSc1	Agrotis
segetum	segetum	k1gNnSc1	segetum
<g/>
)	)	kIx)	)
příležitostně	příležitostně	k6eAd1	příležitostně
požírají	požírat	k5eAaImIp3nP	požírat
listy	list	k1gInPc4	list
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
kořínky	kořínek	k1gInPc1	kořínek
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
je	být	k5eAaImIp3nS	být
živnou	živný	k2eAgFnSc7d1	živná
rostlinou	rostlina	k1gFnSc7	rostlina
housenek	housenka	k1gFnPc2	housenka
ohroženého	ohrožený	k2eAgMnSc2d1	ohrožený
motýla	motýl	k1gMnSc2	motýl
pestrobarvce	pestrobarvce	k1gMnSc2	pestrobarvce
petrklíčového	petrklíčový	k2eAgMnSc2d1	petrklíčový
(	(	kIx(	(
<g/>
Hamearis	Hamearis	k1gFnSc1	Hamearis
lucina	lucina	k1gFnSc1	lucina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
prvosence	prvosenka	k1gFnSc6	prvosenka
jarní	jarní	k2eAgNnSc4d1	jarní
parazitují	parazitovat	k5eAaImIp3nP	parazitovat
i	i	k8xC	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
mšice	mšice	k1gFnPc1	mšice
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
kořenové	kořenový	k2eAgNnSc1d1	kořenové
jako	jako	k8xC	jako
Pemphigus	Pemphigus	k1gMnSc1	Pemphigus
auriculae	auriculae	k1gFnSc1	auriculae
<g/>
,	,	kIx,	,
Pemphigus	Pemphigus	k1gInSc1	Pemphigus
psoudoauriculae	psoudoauriculaat	k5eAaPmIp3nS	psoudoauriculaat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
listové	listový	k2eAgFnPc1d1	listová
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
i	i	k9	i
sekundární	sekundární	k2eAgFnPc4d1	sekundární
nákazy	nákaza	k1gFnPc4	nákaza
virovými	virový	k2eAgFnPc7d1	virová
a	a	k8xC	a
bakteriálními	bakteriální	k2eAgFnPc7d1	bakteriální
chorobami	choroba	k1gFnPc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rubu	rub	k1gInSc6	rub
listů	list	k1gInPc2	list
saje	sát	k5eAaImIp3nS	sát
pidikřísek	pidikřísek	k1gMnSc1	pidikřísek
skleníkový	skleníkový	k2eAgMnSc1d1	skleníkový
(	(	kIx(	(
<g/>
Erythroneura	Erythroneura	k1gFnSc1	Erythroneura
bellidiformis	bellidiformis	k1gFnSc1	bellidiformis
Edw	Edw	k1gFnSc1	Edw
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
larvy	larva	k1gFnSc2	larva
much	moucha	k1gFnPc2	moucha
Chromatomyia	Chromatomyium	k1gNnSc2	Chromatomyium
primulae	primulae	k6eAd1	primulae
vyhlodávají	vyhlodávat	k5eAaImIp3nP	vyhlodávat
v	v	k7c6	v
listech	list	k1gInPc6	list
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
tunely	tunel	k1gInPc1	tunel
<g/>
,	,	kIx,	,
kořínky	kořínek	k1gInPc1	kořínek
požírají	požírat	k5eAaImIp3nP	požírat
larvy	larva	k1gFnPc4	larva
lakonosce	lakonoska	k1gFnSc3	lakonoska
rýhovaného	rýhovaný	k2eAgMnSc2d1	rýhovaný
(	(	kIx(	(
<g/>
Otiorrhynchus	Otiorrhynchus	k1gInSc1	Otiorrhynchus
sulcatus	sulcatus	k1gMnSc1	sulcatus
Fabr	Fabr	k1gMnSc1	Fabr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
mladších	mladý	k2eAgInPc2d2	mladší
semenáčků	semenáček	k1gInPc2	semenáček
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
rostlin	rostlina	k1gFnPc2	rostlina
prvosenky	prvosenka	k1gFnSc2	prvosenka
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
chloróza	chloróza	k1gFnSc1	chloróza
(	(	kIx(	(
<g/>
žloutenka	žloutenka	k1gFnSc1	žloutenka
čili	čili	k8xC	čili
blednička	blednička	k1gFnSc1	blednička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
choroba	choroba	k1gFnSc1	choroba
způsobená	způsobený	k2eAgFnSc1d1	způsobená
fyziologickými	fyziologický	k2eAgInPc7d1	fyziologický
problémy	problém	k1gInPc7	problém
jako	jako	k8xC	jako
přelití	přelití	k1gNnSc1	přelití
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
přístupné	přístupný	k2eAgFnSc6d1	přístupná
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
V	v	k7c6	v
krajních	krajní	k2eAgInPc6d1	krajní
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
nejmladší	mladý	k2eAgInPc1d3	nejmladší
listy	list	k1gInPc1	list
na	na	k7c6	na
rostlině	rostlina	k1gFnSc6	rostlina
téměř	téměř	k6eAd1	téměř
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
..	..	k?	..
Různé	různý	k2eAgNnSc1d1	různé
žloutnutí	žloutnutí	k1gNnSc1	žloutnutí
a	a	k8xC	a
špatný	špatný	k2eAgInSc1d1	špatný
růst	růst	k1gInSc1	růst
rostlin	rostlina	k1gFnPc2	rostlina
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
i	i	k9	i
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
znečištěný	znečištěný	k2eAgInSc1d1	znečištěný
vzduch	vzduch	k1gInSc1	vzduch
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsažené	obsažený	k2eAgFnPc1d1	obsažená
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Léčivý	léčivý	k2eAgInSc1d1	léčivý
efekt	efekt	k1gInSc1	efekt
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
rostliny	rostlina	k1gFnSc2	rostlina
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
přítomností	přítomnost	k1gFnSc7	přítomnost
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
metabolitů	metabolit	k1gInPc2	metabolit
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
<g/>
,	,	kIx,	,
listech	list	k1gInPc6	list
či	či	k8xC	či
květu	květ	k1gInSc2	květ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
látek	látka	k1gFnPc2	látka
rovněž	rovněž	k9	rovněž
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
nebo	nebo	k8xC	nebo
varietě	varieta	k1gFnSc6	varieta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
látky	látka	k1gFnPc4	látka
obsažené	obsažený	k2eAgFnPc4d1	obsažená
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
patří	patřit	k5eAaImIp3nP	patřit
saponiny	saponin	k1gInPc1	saponin
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
maximální	maximální	k2eAgFnSc4d1	maximální
hodnotu	hodnota	k1gFnSc4	hodnota
14,9	[number]	k4	14,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc4	obsah
až	až	k9	až
29,5	[number]	k4	29,5
mg	mg	kA	mg
triterpenických	triterpenický	k2eAgInPc2d1	triterpenický
saponinů	saponin	k1gInPc2	saponin
na	na	k7c4	na
gram	gram	k1gInSc4	gram
sušiny	sušina	k1gFnSc2	sušina
(	(	kIx(	(
<g/>
vysušené	vysušený	k2eAgFnSc2d1	vysušená
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
kořene	kořen	k1gInSc2	kořen
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kyselinu	kyselina	k1gFnSc4	kyselina
primulové	primulový	k2eAgFnSc2d1	primulový
A	A	kA	A
a	a	k8xC	a
B.	B.	kA	B.
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
glykosidy	glykosid	k1gInPc1	glykosid
(	(	kIx(	(
<g/>
primverin	primverin	k1gInSc1	primverin
a	a	k8xC	a
primulaverin	primulaverin	k1gInSc1	primulaverin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
silice	silice	k1gFnSc1	silice
(	(	kIx(	(
<g/>
0,25	[number]	k4	0,25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pyrokatechinové	pyrokatechinový	k2eAgInPc4d1	pyrokatechinový
taniny	tanin	k1gInPc4	tanin
<g/>
,	,	kIx,	,
škrob	škrob	k1gInSc4	škrob
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
volemit	volemit	k1gInSc4	volemit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
<g/>
,	,	kIx,	,
provedených	provedený	k2eAgInPc2d1	provedený
na	na	k7c6	na
90	[number]	k4	90
jedincích	jedinec	k1gMnPc6	jedinec
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
různých	různý	k2eAgNnPc2d1	různé
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
jedinci	jedinec	k1gMnPc7	jedinec
a	a	k8xC	a
stanovišti	stanoviště	k1gNnSc3	stanoviště
značná	značný	k2eAgFnSc1d1	značná
kolísavost	kolísavost	k1gFnSc1	kolísavost
účinku	účinek	k1gInSc2	účinek
drogy	droga	k1gFnSc2	droga
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
požadavek	požadavek	k1gInSc4	požadavek
biologické	biologický	k2eAgFnSc2d1	biologická
titrace	titrace	k1gFnSc2	titrace
terapeuticky	terapeuticky	k6eAd1	terapeuticky
používané	používaný	k2eAgFnPc4d1	používaná
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
variability	variabilita	k1gFnSc2	variabilita
účinku	účinek	k1gInSc2	účinek
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrná	průměrný	k2eAgFnSc1d1	průměrná
účinnost	účinnost	k1gFnSc1	účinnost
kořenů	kořen	k1gInPc2	kořen
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
110	[number]	k4	110
až	až	k9	až
210	[number]	k4	210
h.	h.	k?	h.
j.	j.	k?	j.
(	(	kIx(	(
<g/>
hemolytickými	hemolytický	k2eAgFnPc7d1	hemolytická
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
kořenová	kořenový	k2eAgFnSc1d1	kořenová
droga	droga	k1gFnSc1	droga
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
silice	silice	k1gFnSc2	silice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
glykosidy	glykosid	k1gInPc1	glykosid
primverin	primverin	k1gInSc1	primverin
a	a	k8xC	a
primulaverin	primulaverin	k1gInSc1	primulaverin
<g/>
.	.	kIx.	.
</s>
<s>
Sušením	sušení	k1gNnSc7	sušení
a	a	k8xC	a
skladováním	skladování	k1gNnSc7	skladování
drogy	droga	k1gFnSc2	droga
se	se	k3xPyFc4	se
enzymem	enzym	k1gInSc7	enzym
primverosidázou	primverosidáza	k1gFnSc7	primverosidáza
glykosid	glykosid	k1gInSc4	glykosid
primverin	primverin	k1gInSc1	primverin
a	a	k8xC	a
primulaverin	primulaverin	k1gInSc1	primulaverin
štěpí	štěpit	k5eAaImIp3nS	štěpit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hemolytické	hemolytický	k2eAgFnPc1d1	hemolytická
zkoušky	zkouška	k1gFnPc1	zkouška
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kořen	kořen	k1gInSc1	kořen
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
saponinů	saponin	k1gInPc2	saponin
než	než	k8xS	než
prvosenka	prvosenka	k1gFnSc1	prvosenka
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samotné	samotný	k2eAgInPc1d1	samotný
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
saponiny	saponin	k1gInPc4	saponin
bohatší	bohatý	k2eAgFnSc1d2	bohatší
než	než	k8xS	než
oddenek	oddenek	k1gInSc1	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Brody	Brod	k1gInPc4	Brod
je	být	k5eAaImIp3nS	být
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
veris	veris	k1gFnSc1	veris
<g/>
)	)	kIx)	)
z	z	k7c2	z
farmaceutického	farmaceutický	k2eAgNnSc2d1	farmaceutické
hlediska	hledisko	k1gNnSc2	hledisko
cennější	cenný	k2eAgMnSc1d2	cennější
než	než	k8xS	než
prvosenka	prvosenka	k1gFnSc1	prvosenka
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
elatior	elatior	k1gInSc1	elatior
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
cennější	cenný	k2eAgMnSc1d2	cennější
i	i	k9	i
terapeuticky	terapeuticky	k6eAd1	terapeuticky
<g/>
,	,	kIx,	,
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
pouze	pouze	k6eAd1	pouze
srovnávací	srovnávací	k2eAgFnPc4d1	srovnávací
klinické	klinický	k2eAgFnPc4d1	klinická
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
látky	látka	k1gFnPc4	látka
obsažené	obsažený	k2eAgFnPc4d1	obsažená
v	v	k7c6	v
květech	květ	k1gInPc6	květ
patří	patřit	k5eAaImIp3nP	patřit
rovněž	rovněž	k9	rovněž
saponiny	saponin	k1gInPc1	saponin
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
silice	silice	k1gFnSc2	silice
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
stejného	stejný	k2eAgNnSc2d1	stejné
složení	složení	k1gNnSc2	složení
jako	jako	k8xC	jako
silice	silice	k1gFnSc2	silice
<g />
.	.	kIx.	.
</s>
<s>
kořene	kořen	k1gInSc2	kořen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
glykosidy	glykosid	k1gInPc1	glykosid
primverin	primverin	k1gInSc1	primverin
a	a	k8xC	a
primulaverin	primulaverin	k1gInSc1	primulaverin
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc1	flavonoid
a	a	k8xC	a
flavony	flavon	k1gInPc1	flavon
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
<g/>
,5	,5	k4	,5
<g/>
-trimethoxyflavon	rimethoxyflavona	k1gFnPc2	-trimethoxyflavona
<g/>
,	,	kIx,	,
kvercetin	kvercetin	k2eAgInSc1d1	kvercetin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
odvozeniny	odvozenina	k1gFnPc1	odvozenina
<g/>
,	,	kIx,	,
kaempferol	kaempferol	k1gInSc1	kaempferol
<g/>
,	,	kIx,	,
a	a	k8xC	a
3	[number]	k4	3
<g/>
-limocitrin	imocitrin	k1gInSc1	-limocitrin
glykosid	glykosid	k1gInSc4	glykosid
<g/>
,	,	kIx,	,
rutin	rutina	k1gFnPc2	rutina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karoten	karoten	k1gInSc1	karoten
<g/>
,	,	kIx,	,
enzym	enzym	k1gInSc1	enzym
primverosidázu	primverosidáza	k1gFnSc4	primverosidáza
a	a	k8xC	a
vitamín	vitamín	k1gInSc1	vitamín
C.	C.	kA	C.
Saponiny	saponin	k1gInPc1	saponin
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Koflera	Kofler	k1gMnSc2	Kofler
a	a	k8xC	a
Steidla	Steidlo	k1gNnPc1	Steidlo
přítomny	přítomen	k2eAgFnPc1d1	přítomna
jen	jen	k9	jen
v	v	k7c6	v
kalichu	kalich	k1gInSc6	kalich
a	a	k8xC	a
květní	květní	k2eAgFnSc3d1	květní
stopce	stopka	k1gFnSc3	stopka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
považují	považovat	k5eAaImIp3nP	považovat
květní	květní	k2eAgFnSc4d1	květní
drogu	droga	k1gFnSc4	droga
bez	bez	k7c2	bez
kalichu	kalich	k1gInSc2	kalich
(	(	kIx(	(
<g/>
jen	jen	k9	jen
z	z	k7c2	z
květních	květní	k2eAgFnPc2d1	květní
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
za	za	k7c4	za
terapeuticky	terapeuticky	k6eAd1	terapeuticky
bezcennou	bezcenný	k2eAgFnSc4d1	bezcenná
<g/>
.	.	kIx.	.
<g/>
Obsah	obsah	k1gInSc1	obsah
saponinů	saponin	k1gInPc2	saponin
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2	[number]	k4	2
maxima	maximum	k1gNnSc2	maximum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc6	první
před	před	k7c7	před
květem	květ	k1gInSc7	květ
nebo	nebo	k8xC	nebo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
počátkem	počátkem	k7c2	počátkem
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
tak	tak	k6eAd1	tak
zjevnou	zjevný	k2eAgFnSc4d1	zjevná
zákonitost	zákonitost	k1gFnSc4	zákonitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolísání	kolísání	k1gNnSc1	kolísání
probíhá	probíhat	k5eAaImIp3nS	probíhat
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Dafert	Dafert	k1gMnSc1	Dafert
a	a	k8xC	a
Mauber	Mauber	k1gMnSc1	Mauber
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
vlivu	vliv	k1gInSc2	vliv
hnojení	hnojení	k1gNnSc2	hnojení
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnojení	hnojení	k1gNnSc1	hnojení
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
obsah	obsah	k1gInSc1	obsah
saponinů	saponin	k1gInPc2	saponin
a	a	k8xC	a
ke	k	k7c3	k
stejným	stejný	k2eAgInPc3d1	stejný
závěrům	závěr	k1gInPc3	závěr
došel	dojít	k5eAaPmAgInS	dojít
Jaretzky	Jaretzek	k1gInPc7	Jaretzek
a	a	k8xC	a
Seyffarth	Seyffartha	k1gFnPc2	Seyffartha
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
kolísání	kolísání	k1gNnSc1	kolísání
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rutin	rutina	k1gFnPc2	rutina
<g/>
.	.	kIx.	.
</s>
<s>
Denním	denní	k2eAgInSc7d1	denní
rytmem	rytmus	k1gInSc7	rytmus
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
vlivy	vliv	k1gInPc4	vliv
na	na	k7c4	na
obsahové	obsahový	k2eAgFnPc4d1	obsahová
látky	látka	k1gFnPc4	látka
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
praví	pravit	k5eAaBmIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecně	obecně	k6eAd1	obecně
se	s	k7c7	s
světelnou	světelný	k2eAgFnSc7d1	světelná
intenzitou	intenzita	k1gFnSc7	intenzita
hemolytický	hemolytický	k2eAgInSc1d1	hemolytický
index	index	k1gInSc1	index
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
a	a	k8xC	a
pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
tehdy	tehdy	k6eAd1	tehdy
klíče	klíč	k1gInPc1	klíč
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
pro	pro	k7c4	pro
květy	květ	k1gInPc4	květ
a	a	k8xC	a
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
pojídány	pojídat	k5eAaImNgInP	pojídat
v	v	k7c6	v
salátech	salát	k1gInPc6	salát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
květů	květ	k1gInPc2	květ
se	se	k3xPyFc4	se
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
prvosenkové	prvosenkový	k2eAgNnSc1d1	prvosenkový
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
medovina	medovina	k1gFnSc1	medovina
a	a	k8xC	a
ocet	ocet	k1gInSc1	ocet
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
se	se	k3xPyFc4	se
jedly	jíst	k5eAaImAgInP	jíst
také	také	k9	také
samotné	samotný	k2eAgInPc1d1	samotný
<g/>
,	,	kIx,	,
pocukrované	pocukrovaný	k2eAgInPc1d1	pocukrovaný
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgNnSc1d1	jarní
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
sedativum	sedativum	k1gNnSc1	sedativum
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
využíván	využíván	k2eAgMnSc1d1	využíván
proti	proti	k7c3	proti
bolestem	bolest	k1gFnPc3	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
nespavosti	nespavost	k1gFnSc3	nespavost
a	a	k8xC	a
neurotickým	neurotický	k2eAgInPc3d1	neurotický
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyléčit	vyléčit	k5eAaPmF	vyléčit
obrnu	obrna	k1gFnSc4	obrna
<g/>
.	.	kIx.	.
</s>
<s>
Výtažek	výtažek	k1gInSc1	výtažek
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc4d1	jarní
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
kosmetických	kosmetický	k2eAgInPc6d1	kosmetický
přípravcích	přípravek	k1gInPc6	přípravek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
vrásky	vráska	k1gFnPc4	vráska
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
:	:	kIx,	:
motiv	motiv	k1gInSc4	motiv
prvosenky	prvosenka	k1gFnSc2	prvosenka
zdobí	zdobit	k5eAaImIp3nS	zdobit
mnoho	mnoho	k4c1	mnoho
předmětů	předmět	k1gInPc2	předmět
spojených	spojený	k2eAgInPc2d1	spojený
se	s	k7c7	s
svátky	svátek	k1gInPc7	svátek
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
velikonoční	velikonoční	k2eAgFnPc1d1	velikonoční
pohlednice	pohlednice	k1gFnPc1	pohlednice
a	a	k8xC	a
přání	přání	k1gNnPc1	přání
a	a	k8xC	a
vily	vila	k1gFnPc1	vila
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
také	také	k9	také
věnce	věnec	k1gInPc4	věnec
a	a	k8xC	a
máje	máj	k1gFnPc4	máj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květomluvě	květomluva	k1gFnSc6	květomluva
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
pánem	pán	k1gMnSc7	pán
tvého	tvůj	k1gMnSc2	tvůj
srdce	srdce	k1gNnSc2	srdce
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
chtěl	chtít	k5eAaImAgInS	chtít
bych	by	kYmCp1nS	by
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jsem	být	k5eAaImIp1nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
bohyni	bohyně	k1gFnSc4	bohyně
lásky	láska	k1gFnSc2	láska
Freye	Frey	k1gFnSc2	Frey
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
prý	prý	k9	prý
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
návštěvníka	návštěvník	k1gMnSc4	návštěvník
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
sálu	sál	k1gInSc3	sál
<g/>
,	,	kIx,	,
Sessrúmniru	Sessrúmniro	k1gNnSc3	Sessrúmniro
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
čtyř	čtyři	k4xCgFnPc2	čtyři
hrabství	hrabství	k1gNnPc2	hrabství
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Essexu	Essex	k1gInSc2	Essex
<g/>
,	,	kIx,	,
Northamptonshire	Northamptonshir	k1gMnSc5	Northamptonshir
<g/>
,	,	kIx,	,
Surrey	Surre	k1gMnPc7	Surre
a	a	k8xC	a
Worcestershire	Worcestershir	k1gMnSc5	Worcestershir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
pověstí	pověst	k1gFnPc2	pověst
představuje	představovat	k5eAaImIp3nS	představovat
prvosenka	prvosenka	k1gFnSc1	prvosenka
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
otevírá	otevírat	k5eAaImIp3nS	otevírat
jaro	jaro	k1gNnSc4	jaro
(	(	kIx(	(
<g/>
petrklíč	petrklíč	k1gInSc1	petrklíč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
pohanského	pohanský	k2eAgInSc2d1	pohanský
Peruna	Perun	k1gMnSc4	Perun
a	a	k8xC	a
klíč	klíč	k1gInSc4	klíč
symbolem	symbol	k1gInSc7	symbol
blesku	blesk	k1gInSc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
i	i	k8xC	i
německých	německý	k2eAgFnPc2d1	německá
pověstí	pověst	k1gFnPc2	pověst
petrklíč	petrklíč	k1gInSc4	petrklíč
otevírá	otevírat	k5eAaImIp3nS	otevírat
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
skrytým	skrytý	k2eAgInPc3d1	skrytý
pokladům	poklad	k1gInPc3	poklad
a	a	k8xC	a
zlatu	zlato	k1gNnSc3	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
bylo	být	k5eAaImAgNnS	být
děvče	děvče	k1gNnSc1	děvče
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
nalezlo	nalézt	k5eAaBmAgNnS	nalézt
petrklíč	petrklíč	k1gInSc4	petrklíč
<g/>
,	,	kIx,	,
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nevěstu	nevěsta	k1gFnSc4	nevěsta
(	(	kIx(	(
<g/>
heiratsschlüssel	heiratsschlüssel	k1gInSc4	heiratsschlüssel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
lékařství	lékařství	k1gNnSc2	lékařství
uváděl	uvádět	k5eAaImAgInS	uvádět
prvosenky	prvosenka	k1gFnSc2	prvosenka
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
svými	svůj	k3xOyFgMnPc7	svůj
pracemi	práce	k1gFnPc7	práce
Wasicky	Wasicko	k1gNnPc7	Wasicko
a	a	k8xC	a
Joachimowitz	Joachimowitz	k1gInSc4	Joachimowitz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
celý	celý	k2eAgInSc4d1	celý
květ	květ	k1gInSc4	květ
s	s	k7c7	s
kalichem	kalich	k1gInSc7	kalich
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
květní	květní	k2eAgFnSc2d1	květní
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
Flos	Flos	k1gInSc1	Flos
primulae	primulaat	k5eAaPmIp3nS	primulaat
<g/>
)	)	kIx)	)
a	a	k8xC	a
oddenek	oddenek	k1gInSc1	oddenek
s	s	k7c7	s
kořeny	kořen	k1gInPc7	kořen
(	(	kIx(	(
<g/>
Radix	radix	k1gInSc1	radix
primulae	primula	k1gInSc2	primula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
list	list	k1gInSc1	list
(	(	kIx(	(
<g/>
Folium	Folium	k1gNnSc1	Folium
primulae	primula	k1gFnSc2	primula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
nahrazovala	nahrazovat	k5eAaImAgFnS	nahrazovat
americkou	americký	k2eAgFnSc4d1	americká
drogu	droga	k1gFnSc4	droga
Radix	radix	k1gInSc4	radix
senegae	senega	k1gFnSc2	senega
rostliny	rostlina	k1gFnPc1	rostlina
vítod	vítod	k1gInSc4	vítod
senega	seneg	k1gMnSc4	seneg
(	(	kIx(	(
<g/>
Polygala	Polygal	k1gMnSc4	Polygal
senega	seneg	k1gMnSc4	seneg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
účincích	účinek	k1gInPc6	účinek
rovnocenná	rovnocenný	k2eAgFnSc1d1	rovnocenná
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
P.	P.	kA	P.
veris	veris	k1gFnSc2	veris
lze	lze	k6eAd1	lze
užít	užít	k5eAaPmF	užít
prvosenku	prvosenka	k1gFnSc4	prvosenka
vyšší	vysoký	k2eAgFnSc4d2	vyšší
(	(	kIx(	(
<g/>
Primula	Primula	k?	Primula
elatior	elatior	k1gInSc1	elatior
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Hill	Hill	k1gMnSc1	Hill
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
se	se	k3xPyFc4	se
sbírá	sbírat	k5eAaImIp3nS	sbírat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
oddenek	oddenek	k1gInSc1	oddenek
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
<g/>
)	)	kIx)	)
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
vegetace	vegetace	k1gFnSc2	vegetace
nebo	nebo	k8xC	nebo
až	až	k9	až
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
je	být	k5eAaImIp3nS	být
chráněná	chráněný	k2eAgFnSc1d1	chráněná
bylina	bylina	k1gFnSc1	bylina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
sběr	sběr	k1gInSc4	sběr
těchto	tento	k3xDgFnPc2	tento
léčivek	léčivka	k1gFnPc2	léčivka
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
povolen	povolen	k2eAgInSc1d1	povolen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
na	na	k7c6	na
zahrádkách	zahrádka	k1gFnPc6	zahrádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgFnPc1	dva
rostlinné	rostlinný	k2eAgFnPc1d1	rostlinná
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nP	sušit
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Sušení	sušení	k1gNnSc1	sušení
musí	muset	k5eAaImIp3nS	muset
probíhat	probíhat	k5eAaImF	probíhat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1	umělá
teplota	teplota	k1gFnSc1	teplota
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Musí	muset	k5eAaImIp3nP	muset
se	se	k3xPyFc4	se
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
rychlé	rychlý	k2eAgNnSc4d1	rychlé
odvádění	odvádění	k1gNnSc4	odvádění
vlhkého	vlhký	k2eAgInSc2d1	vlhký
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
sušená	sušený	k2eAgFnSc1d1	sušená
květní	květní	k2eAgFnSc1d1	květní
droga	droga	k1gFnSc1	droga
má	mít	k5eAaImIp3nS	mít
zachovánu	zachován	k2eAgFnSc4d1	zachována
barvu	barva	k1gFnSc4	barva
čerstvých	čerstvý	k2eAgMnPc2d1	čerstvý
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
ztráty	ztráta	k1gFnSc2	ztráta
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
květů	květ	k1gInPc2	květ
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
u	u	k7c2	u
čistých	čistý	k2eAgFnPc2d1	čistá
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgFnSc1d1	květní
droga	droga	k1gFnSc1	droga
má	mít	k5eAaImIp3nS	mít
slabě	slabě	k6eAd1	slabě
medový	medový	k2eAgInSc4d1	medový
pach	pach	k1gInSc4	pach
a	a	k8xC	a
nasládlou	nasládlý	k2eAgFnSc4d1	nasládlá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nesprávném	správný	k2eNgNnSc6d1	nesprávné
sušení	sušení	k1gNnSc6	sušení
mění	měnit	k5eAaImIp3nS	měnit
droga	droga	k1gFnSc1	droga
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
a	a	k8xC	a
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
na	na	k7c6	na
účinnosti	účinnost	k1gFnSc6	účinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddenek	oddenek	k1gInSc1	oddenek
má	mít	k5eAaImIp3nS	mít
anýzovou	anýzový	k2eAgFnSc4d1	anýzová
vůni	vůně	k1gFnSc4	vůně
a	a	k8xC	a
odporně	odporně	k6eAd1	odporně
škrablavou	škrablavý	k2eAgFnSc4d1	škrablavá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sušením	sušení	k1gNnSc7	sušení
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
oddenky	oddenek	k1gInPc4	oddenek
důkladně	důkladně	k6eAd1	důkladně
proprat	proprat	k5eAaPmF	proprat
v	v	k7c6	v
tekoucí	tekoucí	k2eAgFnSc6d1	tekoucí
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
zbavit	zbavit	k5eAaPmF	zbavit
je	být	k5eAaImIp3nS	být
nadzemních	nadzemní	k2eAgFnPc2d1	nadzemní
částí	část	k1gFnPc2	část
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvé	čerstvý	k2eAgInPc1d1	čerstvý
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
nabývají	nabývat	k5eAaImIp3nP	nabývat
anýzové	anýzový	k2eAgFnPc4d1	anýzová
vůně	vůně	k1gFnPc4	vůně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Endrise	Endrise	k1gFnSc2	Endrise
Korbeláře	korbelář	k1gMnSc2	korbelář
obsah	obsah	k1gInSc1	obsah
saponinů	saponin	k1gInPc2	saponin
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
<g/>
,	,	kIx,	,
maxima	maxima	k1gFnSc1	maxima
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
:	:	kIx,	:
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
skladování	skladování	k1gNnSc6	skladování
procento	procento	k1gNnSc1	procento
saponinů	saponin	k1gInPc2	saponin
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
<g/>
Části	část	k1gFnSc3	část
rostlin	rostlina	k1gFnPc2	rostlina
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jako	jako	k8xS	jako
expektorancia	expektorans	k1gNnPc1	expektorans
(	(	kIx(	(
<g/>
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
prsních	prsní	k2eAgInPc2d1	prsní
čajů	čaj	k1gInPc2	čaj
i	i	k9	i
k	k	k7c3	k
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
výrobě	výroba	k1gFnSc3	výroba
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
sekretolytika	sekretolytika	k1gFnSc1	sekretolytika
v	v	k7c6	v
odvaru	odvar	k1gInSc6	odvar
(	(	kIx(	(
<g/>
10	[number]	k4	10
g	g	kA	g
květů	květ	k1gInPc2	květ
nebo	nebo	k8xC	nebo
1	[number]	k4	1
polévková	polévkový	k2eAgFnSc1d1	polévková
lžíce	lžíce	k1gFnSc1	lžíce
řezaných	řezaný	k2eAgInPc2d1	řezaný
kořenů	kořen	k1gInPc2	kořen
na	na	k7c4	na
2	[number]	k4	2
šálky	šálka	k1gFnSc2	šálka
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
při	při	k7c6	při
onemocnění	onemocnění	k1gNnSc6	onemocnění
a	a	k8xC	a
zánětech	zánět	k1gInPc6	zánět
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
rozedmě	rozedma	k1gFnSc6	rozedma
plicní	plicní	k2eAgFnSc2d1	plicní
a	a	k8xC	a
při	při	k7c6	při
akutní	akutní	k2eAgFnSc6d1	akutní
a	a	k8xC	a
chronické	chronický	k2eAgFnSc6d1	chronická
bronchitidě	bronchitida	k1gFnSc6	bronchitida
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
extrakt	extrakt	k1gInSc1	extrakt
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
jako	jako	k8xS	jako
antirevmatikum	antirevmatikum	k1gNnSc1	antirevmatikum
<g/>
.	.	kIx.	.
</s>
<s>
Drogy	droga	k1gFnPc1	droga
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
také	také	k9	také
při	při	k7c6	při
bolestech	bolest	k1gFnPc6	bolest
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
při	při	k7c6	při
onemocnění	onemocnění	k1gNnSc6	onemocnění
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
,	,	kIx,	,
močových	močový	k2eAgInPc6d1	močový
kamenech	kámen	k1gInPc6	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
extrakty	extrakt	k1gInPc4	extrakt
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
také	také	k9	také
proti	proti	k7c3	proti
epilepsii	epilepsie	k1gFnSc3	epilepsie
a	a	k8xC	a
křečím	křeč	k1gFnPc3	křeč
<g/>
.	.	kIx.	.
<g/>
Drogu	droga	k1gFnSc4	droga
<g/>
,	,	kIx,	,
přípravky	přípravek	k1gInPc4	přípravek
z	z	k7c2	z
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
prvosenky	prvosenka	k1gFnSc2	prvosenka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
léčiva	léčivo	k1gNnSc2	léčivo
Solutan	Solutan	k1gInSc1	Solutan
<g/>
,	,	kIx,	,
Tussilen	Tussilen	k1gInSc1	Tussilen
a	a	k8xC	a
Species	species	k1gFnSc1	species
pectorales	pectoralesa	k1gFnPc2	pectoralesa
Planta	planta	k1gFnSc1	planta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
g	g	kA	g
sušeného	sušený	k2eAgInSc2d1	sušený
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
podráždění	podráždění	k1gNnSc4	podráždění
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc1d2	vyšší
dávky	dávka	k1gFnPc1	dávka
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
způsobit	způsobit	k5eAaPmF	způsobit
dávení	dávení	k1gNnSc3	dávení
a	a	k8xC	a
průjmy	průjem	k1gInPc7	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
alergickou	alergický	k2eAgFnSc4d1	alergická
odezvu	odezva	k1gFnSc4	odezva
u	u	k7c2	u
zvlášť	zvlášť	k6eAd1	zvlášť
citlivých	citlivý	k2eAgMnPc2d1	citlivý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
a	a	k8xC	a
době	doba	k1gFnSc6	doba
kojení	kojení	k1gNnSc2	kojení
<g/>
.	.	kIx.	.
</s>
<s>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
používat	používat	k5eAaImF	používat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zahrady	zahrada	k1gFnPc4	zahrada
nemá	mít	k5eNaImIp3nS	mít
původní	původní	k2eAgInSc1d1	původní
druh	druh	k1gInSc1	druh
jako	jako	k8xC	jako
trvalka	trvalka	k1gFnSc1	trvalka
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
k	k	k7c3	k
chatám	chata	k1gFnPc3	chata
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
oblíbenější	oblíbený	k2eAgFnPc1d2	oblíbenější
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgFnPc1d1	barevná
odrůdy	odrůda	k1gFnPc1	odrůda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
většinou	většinou	k6eAd1	většinou
mají	mít	k5eAaImIp3nP	mít
teplé	teplý	k2eAgInPc4d1	teplý
medové	medový	k2eAgInPc4d1	medový
a	a	k8xC	a
červené	červený	k2eAgInPc4d1	červený
odstíny	odstín	k1gInPc4	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
ale	ale	k9	ale
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
na	na	k7c6	na
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
,	,	kIx,	,
v	v	k7c6	v
záhonech	záhon	k1gInPc6	záhon
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
hodí	hodit	k5eAaImIp3nS	hodit
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
květy	květ	k1gInPc7	květ
sbírají	sbírat	k5eAaImIp3nP	sbírat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
senosečí	senoseč	k1gFnSc7	senoseč
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
polostinné	polostinný	k2eAgNnSc1d1	polostinné
až	až	k9	až
stinné	stinný	k2eAgFnPc4d1	stinná
polohy	poloha	k1gFnPc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
vysévají	vysévat	k5eAaImIp3nP	vysévat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
než	než	k8xS	než
výsev	výsev	k1gInSc1	výsev
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
pozemek	pozemek	k1gInSc4	pozemek
je	být	k5eAaImIp3nS	být
předpěstování	předpěstování	k1gNnSc1	předpěstování
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
pařeništi	pařeniště	k1gNnSc6	pařeniště
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poruše	porucha	k1gFnSc6	porucha
klíčivosti	klíčivost	k1gFnSc2	klíčivost
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
máčet	máčet	k5eAaImF	máčet
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
po	po	k7c4	po
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
HTS	HTS	kA	HTS
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
g	g	kA	g
(	(	kIx(	(
<g/>
1	[number]	k4	1
g	g	kA	g
semen	semeno	k1gNnPc2	semeno
=	=	kIx~	=
1000	[number]	k4	1000
<g/>
−	−	k?	−
<g/>
1320	[number]	k4	1320
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vegetativně	vegetativně	k6eAd1	vegetativně
lze	lze	k6eAd1	lze
množit	množit	k5eAaImF	množit
prvosenku	prvosenka	k1gFnSc4	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
dělením	dělení	k1gNnSc7	dělení
oddenků	oddenek	k1gInPc2	oddenek
<g/>
.	.	kIx.	.
<g/>
Výsev	výsev	k1gInSc1	výsev
je	být	k5eAaImIp3nS	být
doporučován	doporučovat	k5eAaImNgInS	doporučovat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
červnu	červen	k1gInSc6	červen
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Výsadba	výsadba	k1gFnSc1	výsadba
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
až	až	k8xS	až
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
sázeny	sázet	k5eAaImNgInP	sázet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
20	[number]	k4	20
cm	cm	kA	cm
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
.	.	kIx.	.
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
džemech	džem	k1gInPc6	džem
<g/>
,	,	kIx,	,
čajových	čajový	k2eAgFnPc6d1	čajová
směsích	směs	k1gFnPc6	směs
<g/>
,	,	kIx,	,
sirupech	sirup	k1gInPc6	sirup
<g/>
,	,	kIx,	,
salátech	salát	k1gInPc6	salát
<g/>
,	,	kIx,	,
víně	víno	k1gNnSc6	víno
a	a	k8xC	a
v	v	k7c6	v
nálevech	nálev	k1gInPc6	nálev
pro	pro	k7c4	pro
kyselé	kyselý	k2eAgInPc4d1	kyselý
okurky	okurek	k1gInPc4	okurek
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
do	do	k7c2	do
jarních	jarní	k2eAgFnPc2d1	jarní
vlasových	vlasový	k2eAgFnPc2d1	vlasová
koupelí	koupel	k1gFnPc2	koupel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zde	zde	k6eAd1	zde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oddenky	oddenek	k1gInPc1	oddenek
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgInPc1d1	jarní
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
využívaly	využívat	k5eAaPmAgFnP	využívat
pro	pro	k7c4	pro
kýchací	kýchací	k2eAgInSc4d1	kýchací
prášek	prášek	k1gInSc4	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
vývarem	vývar	k1gInSc7	vývar
barvila	barvit	k5eAaImAgNnP	barvit
také	také	k9	také
velikonoční	velikonoční	k2eAgNnPc1d1	velikonoční
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
řezaná	řezaný	k2eAgFnSc1d1	řezaná
rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
prvosenka	prvosenka	k1gFnSc1	prvosenka
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
aranžování	aranžování	k1gNnSc4	aranžování
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Ze	z	k7c2	z
říší	říš	k1gFnPc2	říš
přírody	příroda	k1gFnSc2	příroda
<g/>
/	/	kIx~	/
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgNnSc4d1	jarní
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Primula	Primula	k?	Primula
veris	veris	k1gFnSc2	veris
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Wikikuchařka	Wikikuchařka	k1gFnSc1	Wikikuchařka
<g/>
/	/	kIx~	/
<g/>
Byliny	bylina	k1gFnPc1	bylina
<g/>
/	/	kIx~	/
<g/>
Prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
botany	botan	k1gInPc1	botan
<g/>
.	.	kIx.	.
cz	cz	k?	cz
–	–	k?	–
obecné	obecný	k2eAgFnSc2d1	obecná
informace	informace	k1gFnSc2	informace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
botanika	botanika	k1gFnSc1	botanika
<g/>
.	.	kIx.	.
wendys	wendys	k1gInSc1	wendys
<g/>
.	.	kIx.	.
cz	cz	k?	cz
–	–	k?	–
obecné	obecný	k2eAgFnSc2d1	obecná
informace	informace	k1gFnSc2	informace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
naturfoto	naturfota	k1gFnSc5	naturfota
<g/>
.	.	kIx.	.
cz	cz	k?	cz
–	–	k?	–
obecné	obecný	k2eAgFnPc4d1	obecná
informace	informace	k1gFnPc4	informace
+	+	kIx~	+
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
obrázky	obrázek	k1gInPc4	obrázek
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
biolib	biolib	k1gInSc1	biolib
<g/>
.	.	kIx.	.
cz	cz	k?	cz
–	–	k?	–
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
,	,	kIx,	,
synonyma	synonymum	k1gNnPc1	synonymum
+	+	kIx~	+
obrázky	obrázek	k1gInPc1	obrázek
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
avicenna	avicenna	k1gFnSc1	avicenna
<g/>
.	.	kIx.	.
cz	cz	k?	cz
–	–	k?	–
obecné	obecný	k2eAgFnSc2d1	obecná
informace	informace	k1gFnSc2	informace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
vitamina	vitamin	k2eAgFnSc1d1	vitamin
<g/>
.	.	kIx.	.
cz	cz	k?	cz
–	–	k?	–
možnost	možnost	k1gFnSc1	možnost
nákupu	nákup	k1gInSc2	nákup
sušeného	sušený	k2eAgInSc2d1	sušený
čaje	čaj	k1gInSc2	čaj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
botanical	botanicat	k5eAaPmAgInS	botanicat
<g/>
.	.	kIx.	.
com	com	k?	com
–	–	k?	–
popis	popis	k1gInSc1	popis
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
bioimages	bioimages	k1gInSc1	bioimages
<g/>
.	.	kIx.	.
org	org	k?	org
<g/>
.	.	kIx.	.
uk	uk	k?	uk
–	–	k?	–
mezidruhové	mezidruhový	k2eAgInPc4d1	mezidruhový
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
prvosenkou	prvosenka	k1gFnSc7	prvosenka
jarní	jarní	k2eAgFnSc7d1	jarní
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
akvarelová	akvarelový	k2eAgFnSc1d1	akvarelová
malba	malba	k1gFnSc1	malba
kultivaru	kultivar	k1gInSc2	kultivar
prvosenky	prvosenka	k1gFnSc2	prvosenka
jarní	jarní	k2eAgFnSc2d1	jarní
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
heilpflanzen-welt	heilpflanzenelt	k5eAaPmF	heilpflanzen-welt
<g/>
.	.	kIx.	.
de	de	k?	de
–	–	k?	–
látky	látka	k1gFnSc2	látka
obsažené	obsažený	k2eAgFnSc2d1	obsažená
v	v	k7c6	v
prvosence	prvosenka	k1gFnSc6	prvosenka
jarní	jarní	k2eAgNnSc1d1	jarní
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
