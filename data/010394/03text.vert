<p>
<s>
Visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
volně	volně	k6eAd1	volně
visícím	visící	k2eAgNnSc6d1	visící
laně	lano	k1gNnSc6	lano
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
zajištěnými	zajištěný	k2eAgInPc7d1	zajištěný
nehybnými	hybný	k2eNgInPc7d1	nehybný
konci	konec	k1gInPc7	konec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
nosný	nosný	k2eAgInSc1d1	nosný
systém	systém	k1gInSc1	systém
visutých	visutý	k2eAgInPc2d1	visutý
mostů	most	k1gInPc2	most
musí	muset	k5eAaImIp3nS	muset
vždy	vždy	k6eAd1	vždy
tvořit	tvořit	k5eAaImF	tvořit
materiály	materiál	k1gInPc4	materiál
odolávající	odolávající	k2eAgFnSc2d1	odolávající
tahovým	tahový	k2eAgNnSc7d1	tahové
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
mostů	most	k1gInPc2	most
dominantní	dominantní	k2eAgMnPc1d1	dominantní
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
visutých	visutý	k2eAgInPc2d1	visutý
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
mostovka	mostovka	k1gFnSc1	mostovka
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
upevněna	upevněn	k2eAgFnSc1d1	upevněna
k	k	k7c3	k
nosným	nosný	k2eAgNnPc3d1	nosné
lanům	lano	k1gNnPc3	lano
<g/>
)	)	kIx)	)
a	a	k8xC	a
visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
se	s	k7c7	s
zavěšenou	zavěšený	k2eAgFnSc7d1	zavěšená
mostovkou	mostovka	k1gFnSc7	mostovka
(	(	kIx(	(
<g/>
mostovka	mostovka	k1gFnSc1	mostovka
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
zavěšena	zavěsit	k5eAaPmNgFnS	zavěsit
pomocí	pomocí	k7c2	pomocí
svislých	svislý	k2eAgInPc2d1	svislý
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
konstrukčních	konstrukční	k2eAgInPc2d1	konstrukční
typů	typ	k1gInPc2	typ
mostů	most	k1gInPc2	most
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
visuté	visutý	k2eAgInPc1d1	visutý
mosty	most	k1gInPc1	most
překonat	překonat	k5eAaPmF	překonat
největší	veliký	k2eAgFnPc4d3	veliký
rozpětí	rozpětí	k1gNnPc4	rozpětí
<g/>
.	.	kIx.	.
</s>
<s>
Visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
zavěšeného	zavěšený	k2eAgInSc2d1	zavěšený
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
požita	požit	k2eAgFnSc1d1	požita
soustava	soustava	k1gFnSc1	soustava
nosných	nosný	k2eAgNnPc2d1	nosné
lan	lano	k1gNnPc2	lano
upevněných	upevněný	k2eAgMnPc2d1	upevněný
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
pevných	pevný	k2eAgInPc2d1	pevný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
==	==	k?	==
</s>
</p>
<p>
<s>
Nosný	nosný	k2eAgInSc1d1	nosný
systém	systém	k1gInSc1	systém
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
visutého	visutý	k2eAgInSc2d1	visutý
mostu	most	k1gInSc2	most
geometricky	geometricky	k6eAd1	geometricky
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
mostovku	mostovka	k1gFnSc4	mostovka
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
nosný	nosný	k2eAgInSc4d1	nosný
systém	systém	k1gInSc4	systém
přímo	přímo	k6eAd1	přímo
tuto	tento	k3xDgFnSc4	tento
mostovku	mostovka	k1gFnSc4	mostovka
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
přitom	přitom	k6eAd1	přitom
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
oblouku	oblouk	k1gInSc2	oblouk
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
směřujícím	směřující	k2eAgInSc7d1	směřující
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konce	konec	k1gInPc1	konec
nosného	nosný	k2eAgInSc2d1	nosný
systému	systém	k1gInSc2	systém
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
vyvýšených	vyvýšený	k2eAgInPc6d1	vyvýšený
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
polohově	polohově	k6eAd1	polohově
umístěných	umístěný	k2eAgNnPc2d1	umístěné
naproti	naproti	k6eAd1	naproti
sobě	se	k3xPyFc3	se
např.	např.	kA	např.
přes	přes	k7c4	přes
údolí	údolí	k1gNnPc4	údolí
či	či	k8xC	či
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vydutosti	vydutost	k1gFnSc3	vydutost
mostovky	mostovka	k1gFnSc2	mostovka
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
most	most	k1gInSc1	most
špatně	špatně	k6eAd1	špatně
použitelný	použitelný	k2eAgInSc1d1	použitelný
pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
dopravu	doprava	k1gFnSc4	doprava
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
kolovými	kolový	k2eAgNnPc7d1	kolové
vozidly	vozidlo	k1gNnPc7	vozidlo
a	a	k8xC	a
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
pěší	pěší	k2eAgInSc4d1	pěší
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
nosný	nosný	k2eAgInSc4d1	nosný
systém	systém	k1gInSc4	systém
tvořila	tvořit	k5eAaImAgFnS	tvořit
lana	lano	k1gNnSc2	lano
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vhodné	vhodný	k2eAgInPc4d1	vhodný
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
stonky	stonek	k1gInPc4	stonek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hornaté	hornatý	k2eAgFnSc6d1	hornatá
incké	incký	k2eAgFnSc6d1	incká
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nikdy	nikdy	k6eAd1	nikdy
neobjevila	objevit	k5eNaPmAgFnS	objevit
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
užití	užití	k1gNnSc4	užití
kolo	kolo	k1gNnSc1	kolo
a	a	k8xC	a
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
fungovala	fungovat	k5eAaImAgFnS	fungovat
pouze	pouze	k6eAd1	pouze
pěší	pěší	k2eAgFnSc1d1	pěší
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
náklady	náklad	k1gInPc1	náklad
byly	být	k5eAaImAgInP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
lanové	lanový	k2eAgInPc1d1	lanový
visuté	visutý	k2eAgInPc1d1	visutý
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
lana	lano	k1gNnSc2	lano
byla	být	k5eAaImAgFnS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
ze	z	k7c2	z
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
druhu	druh	k1gInSc2	druh
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
visuté	visutý	k2eAgInPc1d1	visutý
mosty	most	k1gInPc1	most
se	se	k3xPyFc4	se
stavějí	stavět	k5eAaImIp3nP	stavět
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
klasické	klasický	k2eAgInPc1d1	klasický
lanové	lanový	k2eAgInPc1d1	lanový
mosty	most	k1gInPc1	most
a	a	k8xC	a
lávky	lávka	k1gFnPc1	lávka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
nosný	nosný	k2eAgInSc4d1	nosný
materiál	materiál	k1gInSc4	materiál
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
ocelové	ocelový	k2eAgNnSc1d1	ocelové
lano	lano	k1gNnSc1	lano
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
moderní	moderní	k2eAgFnSc2d1	moderní
modifikace	modifikace	k1gFnSc2	modifikace
využívající	využívající	k2eAgInSc4d1	využívající
například	například	k6eAd1	například
předpjatý	předpjatý	k2eAgInSc4d1	předpjatý
beton	beton	k1gInSc4	beton
(	(	kIx(	(
<g/>
předpjaté	předpjatý	k2eAgInPc1d1	předpjatý
kabely	kabel	k1gInPc1	kabel
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
subtilních	subtilní	k2eAgInPc6d1	subtilní
betonových	betonový	k2eAgInPc6d1	betonový
pásech	pás	k1gInPc6	pás
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
předpjaté	předpjatý	k2eAgInPc4d1	předpjatý
visuté	visutý	k2eAgInPc4d1	visutý
pásy	pás	k1gInPc4	pás
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc1d1	stavební
postupy	postup	k1gInPc1	postup
a	a	k8xC	a
technologie	technologie	k1gFnPc1	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
se	s	k7c7	s
zavěšenou	zavěšený	k2eAgFnSc7d1	zavěšená
mostovkou	mostovka	k1gFnSc7	mostovka
==	==	k?	==
</s>
</p>
<p>
<s>
Pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
materiálech	materiál	k1gInPc6	materiál
a	a	k8xC	a
navrhování	navrhování	k1gNnSc2	navrhování
vedl	vést	k5eAaImAgInS	vést
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
nového	nový	k2eAgInSc2d1	nový
konstrukčního	konstrukční	k2eAgInSc2d1	konstrukční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hlavní	hlavní	k2eAgNnPc4d1	hlavní
nosná	nosný	k2eAgNnPc4d1	nosné
lana	lano	k1gNnPc4	lano
visí	viset	k5eAaImIp3nS	viset
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
pylony	pylon	k1gInPc7	pylon
(	(	kIx(	(
<g/>
věžemi	věž	k1gFnPc7	věž
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
jsou	být	k5eAaImIp3nP	být
zavěšena	zavěšen	k2eAgNnPc4d1	zavěšeno
vertikální	vertikální	k2eAgNnPc4d1	vertikální
lana	lano	k1gNnPc4	lano
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nesou	nést	k5eAaImIp3nP	nést
mostovku	mostovka	k1gFnSc4	mostovka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
mostovka	mostovka	k1gFnSc1	mostovka
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mírně	mírně	k6eAd1	mírně
vypouklá	vypouklý	k2eAgFnSc1d1	vypouklá
(	(	kIx(	(
<g/>
nadvýšená	nadvýšený	k2eAgFnSc1d1	nadvýšený
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
světlé	světlý	k2eAgFnSc2d1	světlá
výšky	výška	k1gFnSc2	výška
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
uspořádání	uspořádání	k1gNnSc1	uspořádání
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadné	snadný	k2eAgNnSc1d1	snadné
převedení	převedení	k1gNnSc1	převedení
kolové	kolový	k2eAgFnSc2d1	kolová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
lehkých	lehký	k2eAgInPc2d1	lehký
vlakových	vlakový	k2eAgInPc2d1	vlakový
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lehkého	lehký	k2eAgNnSc2d1	lehké
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
železnici	železnice	k1gFnSc4	železnice
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
mostů	most	k1gInPc2	most
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
téměř	téměř	k6eAd1	téměř
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejdelších	dlouhý	k2eAgInPc2d3	nejdelší
visutých	visutý	k2eAgInPc2d1	visutý
mostů	most	k1gInPc2	most
</s>
</p>
<p>
<s>
Lanový	lanový	k2eAgInSc1d1	lanový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Suspension	Suspension	k1gInSc1	Suspension
bridge	bridg	k1gFnSc2	bridg
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
