<s>
Visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
volně	volně	k6eAd1	volně
visícím	visící	k2eAgNnSc6d1	visící
laně	lano	k1gNnSc6	lano
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
zajištěnými	zajištěný	k2eAgInPc7d1	zajištěný
nehybnými	hybný	k2eNgInPc7d1	nehybný
konci	konec	k1gInPc7	konec
<g/>
.	.	kIx.	.
</s>
