<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Barák	Barák	k1gMnSc1	Barák
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
svou	svůj	k3xOyFgFnSc4	svůj
básnickou	básnický	k2eAgFnSc4d1	básnická
prvotinu	prvotina	k1gFnSc4	prvotina
<g/>
,	,	kIx,	,
poznávala	poznávat	k5eAaImAgFnS	poznávat
česká	český	k2eAgFnSc1d1	Česká
kulturní	kulturní	k2eAgFnSc1d1	kulturní
veřejnost	veřejnost	k1gFnSc1	veřejnost
Josefa	Josef	k1gMnSc2	Josef
Baráka	Barák	k1gMnSc2	Barák
jako	jako	k8xC	jako
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Náležel	náležet	k5eAaImAgInS	náležet
k	k	k7c3	k
mladé	mladý	k2eAgFnSc3d1	mladá
literární	literární	k2eAgFnSc3d1	literární
generaci	generace	k1gFnSc3	generace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1858	[number]	k4	1858
představila	představit	k5eAaPmAgFnS	představit
almanachem	almanach	k1gInSc7	almanach
Máj	Mája	k1gFnPc2	Mája
<g/>
.	.	kIx.	.
</s>
<s>
Barák	Barák	k1gMnSc1	Barák
byl	být	k5eAaImAgMnS	být
redaktorem	redaktor	k1gMnSc7	redaktor
almanachu	almanach	k1gInSc2	almanach
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gMnPc3	jeho
hlavním	hlavní	k2eAgMnPc3d1	hlavní
inspirátorům	inspirátor	k1gMnPc3	inspirátor
Nerudovi	Neruda	k1gMnSc3	Neruda
a	a	k8xC	a
Hálkovi	Hálek	k1gMnSc3	Hálek
nebylo	být	k5eNaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
potřebných	potřebný	k2eAgInPc2d1	potřebný
čtyřiadvacet	čtyřiadvacet	k4xCc4	čtyřiadvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
mrtvých	mrtvý	k2eAgNnPc6d1	mrtvé
létech	léto	k1gNnPc6	léto
Bachova	Bachův	k2eAgInSc2d1	Bachův
absolutismu	absolutismus	k1gInSc2	absolutismus
začínal	začínat	k5eAaImAgInS	začínat
znovu	znovu	k6eAd1	znovu
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
český	český	k2eAgInSc4d1	český
národní	národní	k2eAgInSc4d1	národní
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
Barák	barák	k1gInSc1	barák
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
předním	přední	k2eAgMnPc3d1	přední
organizátorům	organizátor	k1gMnPc3	organizátor
<g/>
.	.	kIx.	.
</s>
<s>
Pořádal	pořádat	k5eAaImAgMnS	pořádat
české	český	k2eAgFnSc2d1	Česká
besedy	beseda	k1gFnSc2	beseda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Akademického	akademický	k2eAgInSc2d1	akademický
čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
zakladatele	zakladatel	k1gMnSc4	zakladatel
Sokola	Sokol	k1gMnSc4	Sokol
<g/>
,	,	kIx,	,
Umělecké	umělecký	k2eAgMnPc4d1	umělecký
besedy	beseda	k1gFnSc2	beseda
i	i	k8xC	i
četných	četný	k2eAgFnPc2d1	četná
studentských	studentská	k1gFnPc2	studentská
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zaměstnáním	zaměstnání	k1gNnSc7	zaměstnání
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
oborem	obor	k1gInSc7	obor
Barákovy	Barákův	k2eAgFnSc2d1	Barákova
činnosti	činnost	k1gFnSc2	činnost
bylo	být	k5eAaImAgNnS	být
novinářství	novinářství	k1gNnSc1	novinářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
Prager	Pragra	k1gFnPc2	Pragra
Morgenpost	Morgenpost	k1gInSc1	Morgenpost
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vznikaly	vznikat	k5eAaImAgInP	vznikat
časopisy	časopis	k1gInPc1	časopis
české	český	k2eAgInPc1d1	český
<g/>
,	,	kIx,	,
redaktorem	redaktor	k1gMnSc7	redaktor
Času	čas	k1gInSc2	čas
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1867	[number]	k4	1867
až	až	k9	až
1873	[number]	k4	1873
vydával	vydávat	k5eAaImAgInS	vydávat
a	a	k8xC	a
řídil	řídit	k5eAaImAgInS	řídit
časopis	časopis	k1gInSc1	časopis
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodícího	rodící	k2eAgMnSc2d1	rodící
se	se	k3xPyFc4	se
mladočeského	mladočeský	k2eAgInSc2d1	mladočeský
hnutí	hnutí	k1gNnSc3	hnutí
představoval	představovat	k5eAaImAgInS	představovat
Barák	barák	k1gInSc1	barák
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nerudou	neruda	k1gFnSc7	neruda
jakýsi	jakýsi	k3yIgInSc4	jakýsi
"	"	kIx"	"
<g/>
plebejský	plebejský	k2eAgInSc4d1	plebejský
demokratický	demokratický	k2eAgInSc4d1	demokratický
radikalismus	radikalismus	k1gInSc4	radikalismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgFnSc7d1	navazující
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
radikálních	radikální	k2eAgMnPc2d1	radikální
demokratů	demokrat	k1gMnPc2	demokrat
z	z	k7c2	z
let	léto	k1gNnPc2	léto
revoluce	revoluce	k1gFnSc1	revoluce
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důsledný	důsledný	k2eAgMnSc1d1	důsledný
demokrat	demokrat	k1gMnSc1	demokrat
si	se	k3xPyFc3	se
Barák	barák	k1gInSc1	barák
všímal	všímat	k5eAaImAgInS	všímat
dělnických	dělnický	k2eAgInPc2d1	dělnický
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
akcí	akce	k1gFnSc7	akce
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgMnPc4	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1871	[number]	k4	1871
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
redaktorem	redaktor	k1gMnSc7	redaktor
prvního	první	k4xOgInSc2	první
dělnického	dělnický	k2eAgInSc2d1	dělnický
časopisu	časopis	k1gInSc2	časopis
Dělnických	dělnický	k2eAgInPc2d1	dělnický
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
nacionalismem	nacionalismus	k1gInSc7	nacionalismus
se	se	k3xPyFc4	se
však	však	k9	však
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
rozporu	rozpor	k1gInSc2	rozpor
se	s	k7c7	s
smýšlením	smýšlení	k1gNnSc7	smýšlení
některých	některý	k3yIgMnPc2	některý
socialistů	socialist	k1gMnPc2	socialist
ve	v	k7c6	v
vydavatelském	vydavatelský	k2eAgInSc6d1	vydavatelský
sboru	sbor	k1gInSc6	sbor
časopisu	časopis	k1gInSc2	časopis
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1872	[number]	k4	1872
redakce	redakce	k1gFnSc2	redakce
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1874	[number]	k4	1874
byl	být	k5eAaImAgMnS	být
Barák	Barák	k1gMnSc1	Barák
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
redaktorem	redaktor	k1gMnSc7	redaktor
mladočeských	mladočeský	k2eAgInPc2d1	mladočeský
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
českých	český	k2eAgFnPc2d1	Česká
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
<g/>
Již	již	k9	již
od	od	k7c2	od
studentských	studentský	k2eAgFnPc2d1	studentská
demonstrací	demonstrace	k1gFnPc2	demonstrace
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1860	[number]	k4	1860
se	se	k3xPyFc4	se
Barák	Barák	k1gMnSc1	Barák
těšil	těšit	k5eAaImAgMnS	těšit
neobyčejné	obyčejný	k2eNgFnSc2d1	neobyčejná
pozornosti	pozornost	k1gFnSc2	pozornost
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
stíhán	stíhat	k5eAaImNgMnS	stíhat
soudně	soudně	k6eAd1	soudně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc7d1	populární
a	a	k8xC	a
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
jméno	jméno	k1gNnSc4	jméno
známé	známý	k2eAgFnSc2d1	známá
jen	jen	k6eAd1	jen
užšímu	úzký	k2eAgInSc3d2	užší
kruhu	kruh	k1gInSc3	kruh
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
české	český	k2eAgFnPc4d1	Česká
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Neprávem	neprávo	k1gNnSc7	neprávo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
napsal	napsat	k5eAaBmAgInS	napsat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
spisovatel	spisovatel	k1gMnSc1	spisovatel
Antal	Antal	k1gMnSc1	Antal
Stašek	Stašek	k1gMnSc1	Stašek
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
svými	svůj	k3xOyFgMnPc7	svůj
náhledy	náhled	k1gInPc1	náhled
předbíhal	předbíhat	k5eAaImAgMnS	předbíhat
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
vždy	vždy	k6eAd1	vždy
známkou	známka	k1gFnSc7	známka
duchů	duch	k1gMnPc2	duch
nad	nad	k7c4	nad
obecnou	obecný	k2eAgFnSc4d1	obecná
úroveň	úroveň	k1gFnSc4	úroveň
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Barák	Barák	k1gMnSc1	Barák
zemřel	zemřít	k5eAaPmAgMnS	zemřít
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1883	[number]	k4	1883
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A-	A-	k?	A-
<g/>
G.	G.	kA	G.
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
900	[number]	k4	900
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josef	k1gMnSc1	Josef
Barák	barák	k1gInSc4	barák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Barák	Barák	k1gMnSc1	Barák
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Barák	Barák	k1gMnSc1	Barák
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
'	'	kIx"	'
<g/>
Josef	Josef	k1gMnSc1	Josef
Barák	Barák	k1gMnSc1	Barák
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
LIBRI	LIBRI	kA	LIBRI
(	(	kIx(	(
<g/>
KDO	kdo	k3yRnSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
'	'	kIx"	'
<g/>
Josef	Josef	k1gMnSc1	Josef
Barák	Barák	k1gMnSc1	Barák
<g/>
'	'	kIx"	'
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Databazeknih	Databazekniha	k1gFnPc2	Databazekniha
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Soupis	soupis	k1gInSc1	soupis
pražských	pražský	k2eAgMnPc2d1	pražský
domovských	domovský	k2eAgMnPc2d1	domovský
příslušníků	příslušník	k1gMnPc2	příslušník
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Barák	Barák	k1gMnSc1	Barák
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Josefa	Josef	k1gMnSc2	Josef
Baráka	Barák	k1gMnSc2	Barák
ml.	ml.	kA	ml.
<g/>
)	)	kIx)	)
</s>
</p>
