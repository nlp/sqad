<p>
<s>
Kukla	Kukla	k1gMnSc1	Kukla
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgNnSc4d1	životní
stadium	stadium	k1gNnSc4	stadium
hmyzu	hmyz	k1gInSc2	hmyz
s	s	k7c7	s
proměnou	proměna	k1gFnSc7	proměna
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
larvy	larva	k1gFnSc2	larva
v	v	k7c4	v
dospělce	dospělec	k1gMnSc4	dospělec
(	(	kIx(	(
<g/>
imago	imago	k1gMnSc1	imago
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stadium	stadium	k1gNnSc4	stadium
nepohyblivé	pohyblivý	k2eNgFnSc2d1	nepohyblivá
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
kryté	krytý	k2eAgNnSc1d1	kryté
pevným	pevný	k2eAgInSc7d1	pevný
obalem	obal	k1gInSc7	obal
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
přestavbě	přestavba	k1gFnSc3	přestavba
(	(	kIx(	(
<g/>
metamorfóze	metamorfóza	k1gFnSc3	metamorfóza
<g/>
)	)	kIx)	)
celého	celý	k2eAgInSc2d1	celý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
proměny	proměna	k1gFnSc2	proměna
jedinec	jedinec	k1gMnSc1	jedinec
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
ze	z	k7c2	z
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
před	před	k7c7	před
zakuklením	zakuklení	k1gNnSc7	zakuklení
nashromáždila	nashromáždit	k5eAaPmAgFnS	nashromáždit
larva	larva	k1gFnSc1	larva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
kukel	kukla	k1gFnPc2	kukla
==	==	k?	==
</s>
</p>
<p>
<s>
Kukla	Kukla	k1gMnSc1	Kukla
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojího	dvojí	k4xRgInSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
krytá	krytý	k2eAgFnSc1d1	krytá
a	a	k8xC	a
nekrytá	krytý	k2eNgFnSc1d1	nekrytá
<g/>
.	.	kIx.	.
</s>
<s>
Krytou	krytý	k2eAgFnSc4d1	krytá
kuklu	kukla	k1gFnSc4	kukla
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
brouci	brouk	k1gMnPc1	brouk
<g/>
,	,	kIx,	,
nekrytou	krytý	k2eNgFnSc4d1	nekrytá
kuklu	kukla	k1gFnSc4	kukla
například	například	k6eAd1	například
motýli	motýl	k1gMnPc1	motýl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kokony	kokon	k1gInPc4	kokon
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
nespokojí	spokojit	k5eNaPmIp3nS	spokojit
s	s	k7c7	s
tvořením	tvoření	k1gNnSc7	tvoření
kukly	kukla	k1gFnSc2	kukla
a	a	k8xC	a
larva	larva	k1gFnSc1	larva
si	se	k3xPyFc3	se
před	před	k7c7	před
zakuklením	zakuklení	k1gNnSc7	zakuklení
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
tzv.	tzv.	kA	tzv.
zámotek	zámotek	k1gInSc4	zámotek
neboli	neboli	k8xC	neboli
kokon	kokon	k1gInSc4	kokon
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
druhu	druh	k1gInSc2	druh
vytvářejícího	vytvářející	k2eAgInSc2d1	vytvářející
kokony	kokon	k1gInPc4	kokon
je	být	k5eAaImIp3nS	být
bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
(	(	kIx(	(
<g/>
Bombyx	Bombyx	k1gInSc1	Bombyx
mori	mor	k1gFnSc2	mor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
housenky	housenka	k1gFnPc1	housenka
produkují	produkovat	k5eAaImIp3nP	produkovat
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejjemnější	jemný	k2eAgNnSc4d3	nejjemnější
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kukla	kuknout	k5eAaPmAgFnS	kuknout
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kukla	kukla	k1gFnSc1	kukla
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
