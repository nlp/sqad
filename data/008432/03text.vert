<p>
<s>
Kalevala	Kalevat	k5eAaImAgFnS	Kalevat
je	být	k5eAaImIp3nS	být
karelo-finský	kareloinský	k2eAgInSc1d1	karelo-finský
národní	národní	k2eAgInSc1d1	národní
epos	epos	k1gInSc1	epos
sestavený	sestavený	k2eAgInSc1d1	sestavený
Eliasem	Elias	k1gMnSc7	Elias
Lönnrotem	Lönnrot	k1gMnSc7	Lönnrot
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
slovesnosti	slovesnost	k1gFnSc2	slovesnost
Finů	Fin	k1gMnPc2	Fin
a	a	k8xC	a
Karelů	Karel	k1gMnPc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
obsahující	obsahující	k2eAgFnSc4d1	obsahující
22	[number]	k4	22
795	[number]	k4	795
veršů	verš	k1gInPc2	verš
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
padesáti	padesát	k4xCc2	padesát
"	"	kIx"	"
<g/>
run	runa	k1gFnPc2	runa
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Epos	epos	k1gInSc1	epos
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
v	v	k7c6	v
sérii	série	k1gFnSc6	série
příběhů	příběh	k1gInPc2	příběh
tří	tři	k4xCgMnPc2	tři
bohatýrů	bohatýr	k1gMnPc2	bohatýr
<g/>
,	,	kIx,	,
Lemminkäinena	Lemminkäinen	k1gMnSc2	Lemminkäinen
<g/>
,	,	kIx,	,
Väinämöinena	Väinämöinen	k1gMnSc2	Väinämöinen
a	a	k8xC	a
Ilmarinena	Ilmarinen	k1gMnSc2	Ilmarinen
<g/>
,	,	kIx,	,
potomků	potomek	k1gMnPc2	potomek
mýtického	mýtický	k2eAgInSc2d1	mýtický
Kalevy	Kaleva	k1gFnPc4	Kaleva
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc4d1	celá
finskou	finský	k2eAgFnSc4d1	finská
mytologii	mytologie	k1gFnSc4	mytologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Lönnrot	Lönnrot	k1gMnSc1	Lönnrot
<g/>
,	,	kIx,	,
vesnický	vesnický	k2eAgMnSc1d1	vesnický
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
,	,	kIx,	,
podnikl	podniknout	k5eAaPmAgMnS	podniknout
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
několik	několik	k4yIc1	několik
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
sbíral	sbírat	k5eAaImAgInS	sbírat
finskou	finský	k2eAgFnSc4d1	finská
lidovou	lidový	k2eAgFnSc4d1	lidová
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
tradovanou	tradovaný	k2eAgFnSc4d1	tradovaná
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
run	run	k1gInSc1	run
takzvanými	takzvaný	k2eAgMnPc7d1	takzvaný
runopěvci	runopěvce	k1gMnPc7	runopěvce
<g/>
,	,	kIx,	,
lidovými	lidový	k2eAgMnPc7d1	lidový
zpěváky	zpěvák	k1gMnPc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
vydáním	vydání	k1gNnSc7	vydání
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
"	"	kIx"	"
<g/>
Pra-Kalevaly	Pra-Kaleval	k1gInPc4	Pra-Kaleval
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
několika	několik	k4yIc2	několik
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
samotné	samotný	k2eAgInPc4d1	samotný
Kalevaly	Kaleval	k1gInPc4	Kaleval
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
samostatné	samostatný	k2eAgInPc4d1	samostatný
eposy	epos	k1gInPc4	epos
Lemminkäinen	Lemminkäinna	k1gFnPc2	Lemminkäinna
a	a	k8xC	a
Väinämöinen	Väinämöinna	k1gFnPc2	Väinämöinna
a	a	k8xC	a
svatební	svatební	k2eAgFnSc2d1	svatební
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
vyšla	vyjít	k5eAaPmAgFnS	vyjít
tzv.	tzv.	kA	tzv.
Stará	Stará	k1gFnSc1	Stará
Kalevala	Kalevala	k1gFnSc1	Kalevala
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
jen	jen	k9	jen
32	[number]	k4	32
run	runa	k1gFnPc2	runa
a	a	k8xC	a
12078	[number]	k4	12078
veršů	verš	k1gInPc2	verš
a	a	k8xC	a
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Nová	nový	k2eAgNnPc1d1	nové
Kalevala	Kalevala	k1gMnSc2	Kalevala
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc3	rok
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lönnrot	Lönnrot	k1gMnSc1	Lönnrot
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
tradované	tradovaný	k2eAgFnPc4d1	tradovaná
runopěvci	runopěvce	k1gMnPc7	runopěvce
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
kdysi	kdysi	k6eAd1	kdysi
dávno	dávno	k6eAd1	dávno
jediným	jediný	k2eAgInSc7d1	jediný
monumentálním	monumentální	k2eAgInSc7d1	monumentální
eposem	epos	k1gInSc7	epos
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
roztříštil	roztříštit	k5eAaPmAgMnS	roztříštit
v	v	k7c4	v
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
celky	celek	k1gInPc4	celek
(	(	kIx(	(
<g/>
stejný	stejný	k2eAgInSc1d1	stejný
názor	názor	k1gInSc1	názor
zastával	zastávat	k5eAaImAgMnS	zastávat
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výsledná	výsledný	k2eAgFnSc1d1	výsledná
Kalevala	Kalevala	k1gFnSc1	Kalevala
je	být	k5eAaImIp3nS	být
pokusem	pokus	k1gInSc7	pokus
spojit	spojit	k5eAaPmF	spojit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
runy	run	k1gInPc4	run
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
dávný	dávný	k2eAgInSc4d1	dávný
epos	epos	k1gInSc4	epos
tak	tak	k6eAd1	tak
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Finští	finský	k2eAgMnPc1d1	finský
folkloristé	folklorista	k1gMnPc1	folklorista
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
dohromady	dohromady	k6eAd1	dohromady
asi	asi	k9	asi
dva	dva	k4xCgInPc1	dva
miliony	milion	k4xCgInPc1	milion
veršů	verš	k1gInPc2	verš
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
variant	varianta	k1gFnPc2	varianta
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
podílel	podílet	k5eAaImAgMnS	podílet
proslulý	proslulý	k2eAgMnSc1d1	proslulý
runopěvec	runopěvec	k1gMnSc1	runopěvec
Arhippa	Arhipp	k1gMnSc2	Arhipp
Perttunen	Perttunna	k1gFnPc2	Perttunna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
Lönnrotova	Lönnrotův	k2eAgInSc2d1	Lönnrotův
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
výsledném	výsledný	k2eAgNnSc6d1	výsledné
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
třetina	třetina	k1gFnSc1	třetina
kalevalských	kalevalský	k2eAgInPc2d1	kalevalský
veršů	verš	k1gInPc2	verš
jsou	být	k5eAaImIp3nP	být
doslovné	doslovný	k2eAgInPc1d1	doslovný
záznamy	záznam	k1gInPc1	záznam
skutečné	skutečný	k2eAgFnSc2d1	skutečná
lidové	lidový	k2eAgFnSc2d1	lidová
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
materiálu	materiál	k1gInSc2	materiál
Lönnrot	Lönnrot	k1gInSc4	Lönnrot
mírně	mírně	k6eAd1	mírně
upravil	upravit	k5eAaPmAgMnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
čtrnáct	čtrnáct	k4xCc4	čtrnáct
procent	procento	k1gNnPc2	procento
veršů	verš	k1gInPc2	verš
měl	mít	k5eAaImAgInS	mít
"	"	kIx"	"
<g/>
včlenit	včlenit	k5eAaPmF	včlenit
<g/>
"	"	kIx"	"
do	do	k7c2	do
výsledných	výsledný	k2eAgInPc2d1	výsledný
textů	text	k1gInPc2	text
na	na	k7c6	na
základě	základ	k1gInSc6	základ
variant	varianta	k1gFnPc2	varianta
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
ve	v	k7c6	v
"	"	kIx"	"
<g/>
staré	starý	k2eAgFnSc6d1	stará
Kalevale	Kalevala	k1gFnSc6	Kalevala
<g/>
"	"	kIx"	"
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
varianty	varianta	k1gFnPc1	varianta
<g/>
,	,	kIx,	,
úhrnem	úhrnem	k6eAd1	úhrnem
asi	asi	k9	asi
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
připojuje	připojovat	k5eAaImIp3nS	připojovat
jako	jako	k9	jako
dodatek	dodatek	k1gInSc1	dodatek
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
tři	tři	k4xCgNnPc1	tři
procenta	procento	k1gNnPc1	procento
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc4	jeho
vlastní	vlastní	k2eAgFnPc4d1	vlastní
invence	invence	k1gFnPc4	invence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohlasy	ohlas	k1gInPc1	ohlas
==	==	k?	==
</s>
</p>
<p>
<s>
Kalevala	Kalevat	k5eAaPmAgFnS	Kalevat
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
finské	finský	k2eAgFnSc2d1	finská
národní	národní	k2eAgFnSc2d1	národní
hrdosti	hrdost	k1gFnSc2	hrdost
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jejích	její	k3xOp3gFnPc2	její
pasáží	pasáž	k1gFnPc2	pasáž
bylo	být	k5eAaImAgNnS	být
zhudebněno	zhudebnit	k5eAaPmNgNnS	zhudebnit
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
klasickými	klasický	k2eAgMnPc7d1	klasický
skladateli	skladatel	k1gMnPc7	skladatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jean	Jean	k1gMnSc1	Jean
Sibelius	Sibelius	k1gMnSc1	Sibelius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
moderní	moderní	k2eAgFnSc7d1	moderní
hudbou	hudba	k1gFnSc7	hudba
(	(	kIx(	(
<g/>
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
Amorphis	Amorphis	k1gFnSc2	Amorphis
<g/>
)	)	kIx)	)
a	a	k8xC	a
ohlasy	ohlas	k1gInPc1	ohlas
Kalevaly	Kaleval	k1gInPc1	Kaleval
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
slaven	slaven	k2eAgMnSc1d1	slaven
jako	jako	k8xC	jako
svátek	svátek	k1gInSc1	svátek
tzv.	tzv.	kA	tzv.
Den	den	k1gInSc1	den
Kalevaly	Kaleval	k1gInPc1	Kaleval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlivy	vliv	k1gInPc1	vliv
Kalevaly	Kaleval	k1gInPc1	Kaleval
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
kultuře	kultura	k1gFnSc6	kultura
<g/>
;	;	kIx,	;
Kalevalou	Kalevala	k1gFnSc7	Kalevala
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
například	například	k6eAd1	například
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
Henry	Henry	k1gMnSc1	Henry
Wadsworth	Wadsworth	k1gMnSc1	Wadsworth
Longfellow	Longfellow	k1gMnSc1	Longfellow
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
proslulé	proslulý	k2eAgFnSc6d1	proslulá
Písni	píseň	k1gFnSc6	píseň
o	o	k7c6	o
Hiawatě	Hiawata	k1gFnSc6	Hiawata
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
použil	použít	k5eAaPmAgInS	použít
shodnou	shodný	k2eAgFnSc4d1	shodná
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kalevalské	kalevalský	k2eAgNnSc1d1	kalevalský
metrum	metrum	k1gNnSc1	metrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česky	česky	k6eAd1	česky
byla	být	k5eAaImAgFnS	být
Kalevala	Kalevala	k1gFnSc1	Kalevala
vydána	vydat	k5eAaPmNgFnS	vydat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Josefa	Josef	k1gMnSc2	Josef
Holečka	Holeček	k1gMnSc2	Holeček
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
překlad	překlad	k1gInSc1	překlad
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
ruském	ruský	k2eAgInSc6d1	ruský
překladu	překlad	k1gInSc6	překlad
druhým	druhý	k4xOgInSc7	druhý
překladem	překlad	k1gInSc7	překlad
do	do	k7c2	do
slovanského	slovanský	k2eAgInSc2d1	slovanský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vůbec	vůbec	k9	vůbec
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
překlady	překlad	k1gInPc4	překlad
tohoto	tento	k3xDgNnSc2	tento
významného	významný	k2eAgNnSc2d1	významné
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Holeček	Holeček	k1gMnSc1	Holeček
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
finsky	finsky	k6eAd1	finsky
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
překladu	překlad	k1gInSc3	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přeložil	přeložit	k5eAaPmAgMnS	přeložit
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
díla	dílo	k1gNnPc4	dílo
finské	finský	k2eAgFnSc2d1	finská
lidové	lidový	k2eAgFnSc2d1	lidová
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Paavo	Paavo	k1gNnSc1	Paavo
Haavikko	Haavikko	k1gNnSc1	Haavikko
napsal	napsat	k5eAaBmAgInS	napsat
scénář	scénář	k1gInSc4	scénář
k	k	k7c3	k
seriálu	seriál	k1gInSc6	seriál
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
Kalevale	Kalevala	k1gFnSc6	Kalevala
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
zfilmován	zfilmován	k2eAgMnSc1d1	zfilmován
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Rauta-aika	Rautaikum	k1gNnSc2	Rauta-aikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonské	estonský	k2eAgNnSc1d1	Estonské
národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c6	na
Kalevalu	Kaleval	k1gInSc6	Kaleval
svým	svůj	k3xOyFgInSc7	svůj
národním	národní	k2eAgInSc7d1	národní
eposem	epos	k1gInSc7	epos
Kalevalův	Kalevalův	k2eAgMnSc1d1	Kalevalův
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Kalevala	Kalevat	k5eAaBmAgFnS	Kalevat
líčí	líčit	k5eAaImIp3nS	líčit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
fiktivního	fiktivní	k2eAgNnSc2d1	fiktivní
setkání	setkání	k1gNnSc2	setkání
dvou	dva	k4xCgMnPc2	dva
nepojmenovaných	pojmenovaný	k2eNgMnPc2d1	nepojmenovaný
runopěvců	runopěvce	k1gMnPc2	runopěvce
vlastně	vlastně	k9	vlastně
všechny	všechen	k3xTgInPc4	všechen
podstatnější	podstatný	k2eAgInPc4d2	podstatnější
příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
finské	finský	k2eAgFnSc2d1	finská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
především	především	k9	především
příběhy	příběh	k1gInPc1	příběh
moudrého	moudrý	k2eAgMnSc2d1	moudrý
věštce	věštec	k1gMnSc2	věštec
a	a	k8xC	a
proslulého	proslulý	k2eAgMnSc2d1	proslulý
runopěvce	runopěvce	k1gMnSc2	runopěvce
Väinämöinena	Väinämöinen	k1gMnSc2	Väinämöinen
<g/>
,	,	kIx,	,
čarodějného	čarodějný	k2eAgMnSc2d1	čarodějný
kováře	kovář	k1gMnSc2	kovář
Ilmarinena	Ilmarinen	k1gMnSc2	Ilmarinen
záletného	záletný	k2eAgMnSc2d1	záletný
válečníka	válečník	k1gMnSc2	válečník
Lemminkäinena	Lemminkäinen	k1gMnSc2	Lemminkäinen
a	a	k8xC	a
nešťastného	šťastný	k2eNgMnSc2d1	nešťastný
mstitele	mstitel	k1gMnSc2	mstitel
Kullerva	Kullerv	k1gMnSc2	Kullerv
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
zarámovány	zarámován	k2eAgInPc1d1	zarámován
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
potomky	potomek	k1gMnPc7	potomek
mýtického	mýtický	k2eAgInSc2d1	mýtický
Kalevy	Kaleva	k1gFnPc4	Kaleva
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
jsou	být	k5eAaImIp3nP	být
Väinämöinen	Väinämöinna	k1gFnPc2	Väinämöinna
<g/>
,	,	kIx,	,
Lemminkäinen	Lemminkäinna	k1gFnPc2	Lemminkäinna
i	i	k8xC	i
Ilmarinen	Ilmarinno	k1gNnPc2	Ilmarinno
a	a	k8xC	a
obyvateli	obyvatel	k1gMnPc7	obyvatel
temné	temný	k2eAgFnSc2d1	temná
říše	říš	k1gFnSc2	říš
Pohjola	Pohjola	k1gFnSc1	Pohjola
na	na	k7c6	na
dalekém	daleký	k2eAgInSc6d1	daleký
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
mýtický	mýtický	k2eAgInSc1d1	mýtický
obraz	obraz	k1gInSc1	obraz
Laponska	Laponsko	k1gNnSc2	Laponsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kalevala	Kalevat	k5eAaImAgFnS	Kalevat
začíná	začínat	k5eAaImIp3nS	začínat
mýtem	mýto	k1gNnSc7	mýto
o	o	k7c4	o
stvoření	stvoření	k1gNnSc4	stvoření
a	a	k8xC	a
oživení	oživení	k1gNnSc4	oživení
světa	svět	k1gInSc2	svět
a	a	k8xC	a
narozením	narození	k1gNnSc7	narození
hrdinů	hrdina	k1gMnPc2	hrdina
Väinämöinena	Väinämöineno	k1gNnSc2	Väinämöineno
a	a	k8xC	a
Ilmarinena	Ilmarineno	k1gNnSc2	Ilmarineno
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
námluvách	námluva	k1gFnPc6	námluva
pěvce	pěvec	k1gMnSc2	pěvec
Väinämöinena	Väinämöinen	k1gMnSc2	Väinämöinen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
zprvu	zprvu	k6eAd1	zprvu
vzít	vzít	k5eAaPmF	vzít
dívku	dívka	k1gFnSc4	dívka
Aino	Aino	k1gNnSc4	Aino
<g/>
,	,	kIx,	,
sestru	sestra	k1gFnSc4	sestra
svého	svůj	k3xOyFgMnSc2	svůj
nepřítele	nepřítel	k1gMnSc2	nepřítel
Joukahainena	Joukahainen	k1gMnSc2	Joukahainen
<g/>
,	,	kIx,	,
Aino	Aino	k6eAd1	Aino
ale	ale	k8xC	ale
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Väinämöinen	Väinämöinen	k1gInSc1	Väinämöinen
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
Louhi	Louh	k1gFnSc2	Louh
z	z	k7c2	z
temné	temný	k2eAgFnSc2d1	temná
severní	severní	k2eAgFnSc2d1	severní
země	zem	k1gFnSc2	zem
Pohjoly	Pohjola	k1gFnSc2	Pohjola
<g/>
.	.	kIx.	.
</s>
<s>
Dívku	dívka	k1gFnSc4	dívka
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
získá	získat	k5eAaPmIp3nS	získat
pěvcův	pěvcův	k2eAgMnSc1d1	pěvcův
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
Ilmarinen	Ilmarinna	k1gFnPc2	Ilmarinna
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
tajemný	tajemný	k2eAgInSc4d1	tajemný
mlýnek	mlýnek	k1gInSc4	mlýnek
sampo	sampa	k1gFnSc5	sampa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
svému	svůj	k3xOyFgMnSc3	svůj
majiteli	majitel	k1gMnSc3	majitel
naděluje	nadělovat	k5eAaImIp3nS	nadělovat
mouku	mouka	k1gFnSc4	mouka
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc4	sůl
a	a	k8xC	a
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějčina	čarodějčin	k2eAgFnSc1d1	čarodějčina
dcera	dcera	k1gFnSc1	dcera
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
panna	panna	k1gFnSc1	panna
Pohjolanka	Pohjolanka	k1gFnSc1	Pohjolanka
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
však	však	k9	však
krutá	krutý	k2eAgFnSc1d1	krutá
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
ji	on	k3xPp3gFnSc4	on
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
pastýř	pastýř	k1gMnSc1	pastýř
Kullervo	Kullervo	k1gNnSc4	Kullervo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
týrala	týrat	k5eAaImAgFnS	týrat
a	a	k8xC	a
jemuž	jenž	k3xRgMnSc3	jenž
zotročila	zotročit	k5eAaPmAgFnS	zotročit
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Kullervo	Kullervo	k6eAd1	Kullervo
poté	poté	k6eAd1	poté
omylem	omylem	k6eAd1	omylem
svede	svést	k5eAaPmIp3nS	svést
vlastní	vlastní	k2eAgFnSc4d1	vlastní
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
když	když	k8xS	když
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
krvesmilstva	krvesmilstvo	k1gNnSc2	krvesmilstvo
<g/>
,	,	kIx,	,
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
část	část	k1gFnSc4	část
Kalevaly	Kaleval	k1gInPc1	Kaleval
líčí	líčit	k5eAaImIp3nP	líčit
příběhy	příběh	k1gInPc1	příběh
Lemminkäinena	Lemminkäinen	k1gMnSc2	Lemminkäinen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
Louhi	Louh	k1gFnSc2	Louh
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
laponským	laponský	k2eAgMnSc7d1	laponský
čarodějem	čaroděj	k1gMnSc7	čaroděj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
matka	matka	k1gFnSc1	matka
-	-	kIx~	-
kouzelnice	kouzelnice	k1gFnSc1	kouzelnice
jej	on	k3xPp3gMnSc4	on
magickými	magický	k2eAgFnPc7d1	magická
hráběmi	hrábě	k1gFnPc7	hrábě
vylovila	vylovit	k5eAaPmAgFnS	vylovit
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
oživila	oživit	k5eAaPmAgFnS	oživit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Lemminkäinen	Lemminkäinen	k1gInSc1	Lemminkäinen
navštívil	navštívit	k5eAaPmAgInS	navštívit
svatbu	svatba	k1gFnSc4	svatba
Ilmarinena	Ilmarineno	k1gNnSc2	Ilmarineno
a	a	k8xC	a
Louhiny	Louhin	k2eAgFnSc2d1	Louhin
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
způsobil	způsobit	k5eAaPmAgInS	způsobit
poprask	poprask	k1gInSc1	poprask
a	a	k8xC	a
pozdvižení	pozdvižení	k1gNnSc1	pozdvižení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
Ilmarinen	Ilmarinen	k1gInSc1	Ilmarinen
a	a	k8xC	a
Väinämöinen	Väinämöinen	k1gInSc1	Väinämöinen
vypraví	vypravit	k5eAaPmIp3nS	vypravit
do	do	k7c2	do
Pohjoly	Pohjola	k1gFnSc2	Pohjola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
sampo	sampa	k1gFnSc5	sampa
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
jim	on	k3xPp3gMnPc3	on
Lemminkäinen	Lemminkäinen	k2eAgInSc4d1	Lemminkäinen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
pomstít	pomstít	k5eAaPmF	pomstít
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
Louhi	Louh	k1gFnSc2	Louh
za	za	k7c4	za
urážky	urážka	k1gFnPc4	urážka
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinové	Hrdinové	k2eAgFnSc5d1	Hrdinové
sampo	sampa	k1gFnSc5	sampa
uloupí	uloupit	k5eAaPmIp3nS	uloupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
bojovníky	bojovník	k1gMnPc7	bojovník
je	on	k3xPp3gMnPc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
Louhi	Louhi	k1gNnSc1	Louhi
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
orla	orel	k1gMnSc4	orel
a	a	k8xC	a
při	při	k7c6	při
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Väinämöinenem	Väinämöinen	k1gInSc7	Väinämöinen
sampo	sampa	k1gFnSc5	sampa
rozbije	rozbít	k5eAaPmIp3nS	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
Zůstane	zůstat	k5eAaPmIp3nS	zůstat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
jen	jen	k6eAd1	jen
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
slané	slaný	k2eAgFnSc2d1	slaná
<g/>
.	.	kIx.	.
</s>
<s>
Kalevala	Kalevat	k5eAaPmAgFnS	Kalevat
končí	končit	k5eAaImIp3nS	končit
jakousi	jakýsi	k3yIgFnSc7	jakýsi
finskou	finský	k2eAgFnSc7d1	finská
verzí	verze	k1gFnSc7	verze
mýtu	mýtus	k1gInSc2	mýtus
o	o	k7c6	o
narození	narození	k1gNnSc6	narození
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
soumrakem	soumrak	k1gInSc7	soumrak
pohanského	pohanský	k2eAgInSc2d1	pohanský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Forma	forma	k1gFnSc1	forma
==	==	k?	==
</s>
</p>
<p>
<s>
Kalevala	Kalevat	k5eAaPmAgFnS	Kalevat
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
tzv.	tzv.	kA	tzv.
kalevalským	kalevalský	k2eAgInSc7d1	kalevalský
metrem	metr	k1gInSc7	metr
<g/>
,	,	kIx,	,
osmislabičným	osmislabičný	k2eAgInSc7d1	osmislabičný
trochejem	trochej	k1gInSc7	trochej
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
nerýmovaným	rýmovaný	k2eNgInSc7d1	nerýmovaný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
<g/>
)	)	kIx)	)
zrýmován	zrýmovat	k5eAaPmNgInS	zrýmovat
či	či	k8xC	či
spojen	spojit	k5eAaPmNgInS	spojit
asonancí	asonance	k1gFnSc7	asonance
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Kalevale	Kalevala	k1gFnSc6	Kalevala
také	také	k9	také
aliterace	aliterace	k1gFnSc1	aliterace
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kalevalův	Kalevalův	k2eAgMnSc1d1	Kalevalův
syn	syn	k1gMnSc1	syn
-	-	kIx~	-
epos	epos	k1gInSc1	epos
estonského	estonský	k2eAgNnSc2d1	Estonské
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
</s>
</p>
<p>
<s>
Kanteletar	Kanteletar	k1gMnSc1	Kanteletar
</s>
</p>
<p>
<s>
Pohjola	Pohjola	k1gFnSc1	Pohjola
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kalevala	Kalevala	k1gFnSc2	Kalevala
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kalevala	Kalevala	k1gFnSc2	Kalevala
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Kalevaly	Kaleval	k1gInPc4	Kaleval
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Holečka	Holeček	k1gMnSc2	Holeček
-	-	kIx~	-
odkaz	odkaz	k1gInSc1	odkaz
do	do	k7c2	do
digitální	digitální	k2eAgFnSc2d1	digitální
knihovny	knihovna	k1gFnSc2	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Kalevala	Kalevala	k1gFnSc1	Kalevala
česky	česky	k6eAd1	česky
v	v	k7c6	v
pdf	pdf	k?	pdf
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Kalevala	Kalevala	k1gFnSc1	Kalevala
finsky	finsky	k6eAd1	finsky
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Kalevala	Kalevala	k1gFnSc1	Kalevala
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Kalevala	Kalevala	k1gFnSc1	Kalevala
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
metru	metr	k1gInSc6	metr
Kalevaly	Kaleval	k1gInPc1	Kaleval
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výtah	výtah	k1gInSc1	výtah
z	z	k7c2	z
Kalevaly	Kaleval	k1gInPc1	Kaleval
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
