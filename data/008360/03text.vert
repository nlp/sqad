<p>
<s>
Jerome	Jerom	k1gInSc5	Jerom
David	David	k1gMnSc1	David
Salinger	Salinger	k1gInSc1	Salinger
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
New	New	k1gFnPc2	New
York	York	k1gInSc4	York
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
publikující	publikující	k2eAgMnSc1d1	publikující
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
J.	J.	kA	J.
D.	D.	kA	D.
Salinger	Salinger	k1gInSc1	Salinger
a	a	k8xC	a
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
románem	román	k1gInSc7	román
Kdo	kdo	k3yQnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
vydaným	vydaný	k2eAgInSc7d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
nevydal	vydat	k5eNaPmAgMnS	vydat
žádné	žádný	k3yNgNnSc4	žádný
nové	nový	k2eAgNnSc4d1	nové
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
neposkytl	poskytnout	k5eNaPmAgInS	poskytnout
žádný	žádný	k3yNgInSc4	žádný
rozhovor	rozhovor	k1gInSc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Cornishi	Cornish	k1gFnSc6	Cornish
v	v	k7c6	v
New	New	k1gFnSc6	New
Hampshiru	Hampshir	k1gInSc2	Hampshir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
a	a	k8xC	a
již	již	k6eAd1	již
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
krátké	krátký	k2eAgFnPc4d1	krátká
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
službou	služba	k1gFnSc7	služba
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
také	také	k9	také
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
The	The	k1gFnSc2	The
New	New	k1gFnPc2	New
Yorker	Yorker	k1gMnSc1	Yorker
kritiky	kritika	k1gFnSc2	kritika
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
povídku	povídka	k1gFnSc4	povídka
Den	den	k1gInSc1	den
jako	jako	k8xS	jako
stvořený	stvořený	k2eAgInSc1d1	stvořený
pro	pro	k7c4	pro
banánové	banánový	k2eAgFnPc4d1	banánová
rybičky	rybička	k1gFnPc4	rybička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
vyšel	vyjít	k5eAaPmAgInS	vyjít
román	román	k1gInSc1	román
Kdo	kdo	k3yInSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ihned	ihned	k6eAd1	ihned
velmi	velmi	k6eAd1	velmi
úspěšným	úspěšný	k2eAgMnPc3d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
popis	popis	k1gInSc1	popis
dospívání	dospívání	k1gNnSc2	dospívání
a	a	k8xC	a
ztráty	ztráta	k1gFnSc2	ztráta
nevinnosti	nevinnost	k1gFnSc2	nevinnost
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
Holdena	Holden	k1gMnSc2	Holden
Caulfielda	Caulfield	k1gMnSc2	Caulfield
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
především	především	k9	především
na	na	k7c4	na
dospívající	dospívající	k2eAgMnPc4d1	dospívající
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
a	a	k8xC	a
prodá	prodat	k5eAaPmIp3nS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
okolo	okolo	k7c2	okolo
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
výtisků	výtisk	k1gInPc2	výtisk
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
románu	román	k1gInSc2	román
Kdo	kdo	k3yQnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
veřejnému	veřejný	k2eAgInSc3d1	veřejný
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Salinger	Salinger	k1gInSc1	Salinger
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
samotářským	samotářský	k2eAgMnSc7d1	samotářský
a	a	k8xC	a
již	již	k6eAd1	již
nepublikoval	publikovat	k5eNaBmAgInS	publikovat
tak	tak	k9	tak
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
následovala	následovat	k5eAaImAgFnS	následovat
sbírka	sbírka	k1gFnSc1	sbírka
krátkých	krátký	k2eAgFnPc2d1	krátká
povídek	povídka	k1gFnPc2	povídka
Devět	devět	k4xCc4	devět
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
příběh	příběh	k1gInSc4	příběh
Franny	Franna	k1gFnSc2	Franna
a	a	k8xC	a
Zooey	Zooea	k1gFnSc2	Zooea
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
dvou	dva	k4xCgInPc2	dva
románů	román	k1gInPc2	román
Vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
tesaři	tesař	k1gMnPc1	tesař
<g/>
,	,	kIx,	,
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
střechu	střecha	k1gFnSc4	střecha
zvedněte	zvednout	k5eAaPmRp2nP	zvednout
<g/>
!	!	kIx.	!
</s>
<s>
/	/	kIx~	/
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
vydaným	vydaný	k2eAgInSc7d1	vydaný
dílem	díl	k1gInSc7	díl
byl	být	k5eAaImAgInS	být
román	román	k1gInSc4	román
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hapworth	Hapworth	k1gInSc4	Hapworth
16	[number]	k4	16
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Salinger	Salinger	k1gMnSc1	Salinger
bojoval	bojovat	k5eAaImAgMnS	bojovat
s	s	k7c7	s
nechtěnou	chtěný	k2eNgFnSc7d1	nechtěná
pozorností	pozornost	k1gFnSc7	pozornost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
právní	právní	k2eAgFnSc2d1	právní
bitvy	bitva	k1gFnSc2	bitva
s	s	k7c7	s
životopiscem	životopisec	k1gMnSc7	životopisec
Ianem	Ianus	k1gMnSc7	Ianus
Hamiltonem	Hamilton	k1gInSc7	Hamilton
a	a	k8xC	a
uvedení	uvedení	k1gNnSc1	uvedení
jeho	jeho	k3xOp3gFnSc2	jeho
osoby	osoba	k1gFnSc2	osoba
v	v	k7c6	v
pamětech	paměť	k1gFnPc6	paměť
napsaných	napsaný	k2eAgFnPc6d1	napsaná
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gInSc3	on
velice	velice	k6eAd1	velice
blízkými	blízký	k2eAgFnPc7d1	blízká
osobami	osoba	k1gFnPc7	osoba
<g/>
:	:	kIx,	:
Joyce	Joyec	k1gInSc2	Joyec
Maynardovou	Maynardův	k2eAgFnSc7d1	Maynardův
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
bývalou	bývalý	k2eAgFnSc7d1	bývalá
milenkou	milenka	k1gFnSc7	milenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
Margaret	Margareta	k1gFnPc2	Margareta
Salingerovou	Salingerový	k2eAgFnSc4d1	Salingerový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
oznámil	oznámit	k5eAaPmAgMnS	oznámit
malý	malý	k2eAgMnSc1d1	malý
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
se	s	k7c7	s
Salingerem	Salinger	k1gInSc7	Salinger
na	na	k7c6	na
knižním	knižní	k2eAgNnSc6d1	knižní
vydání	vydání	k1gNnSc6	vydání
románu	román	k1gInSc2	román
Hapworth	Hapwortha	k1gFnPc2	Hapwortha
16	[number]	k4	16
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
následné	následný	k2eAgFnSc3d1	následná
publicitě	publicita	k1gFnSc3	publicita
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
knihy	kniha	k1gFnSc2	kniha
odloženo	odložen	k2eAgNnSc1d1	odloženo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Salinger	Salinger	k1gMnSc1	Salinger
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Solomon	Solomona	k1gFnPc2	Solomona
byl	být	k5eAaImAgMnS	být
Žid	Žid	k1gMnSc1	Žid
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prodával	prodávat	k5eAaImAgMnS	prodávat
košer	košer	k2eAgInPc4d1	košer
sýry	sýr	k1gInPc4	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
Jillich	Jillicha	k1gFnPc2	Jillicha
byla	být	k5eAaImAgFnS	být
napůl	napůl	k6eAd1	napůl
Skotka	Skotka	k1gFnSc1	Skotka
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
Irka	Irka	k1gFnSc1	Irka
<g/>
.	.	kIx.	.
</s>
<s>
Změnila	změnit	k5eAaPmAgFnS	změnit
si	se	k3xPyFc3	se
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
Miriam	Miriam	k1gFnSc6	Miriam
a	a	k8xC	a
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
na	na	k7c6	na
židovství	židovství	k1gNnSc6	židovství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
nebyla	být	k5eNaImAgFnS	být
Židovka	Židovka	k1gFnSc1	Židovka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
J.	J.	kA	J.
D.	D.	kA	D.
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
až	až	k9	až
po	po	k7c4	po
své	svůj	k3xOyFgNnSc4	svůj
bar	bar	k1gInSc1	bar
micva	micva	k6eAd1	micva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
sestru	sestra	k1gFnSc4	sestra
Doris	Doris	k1gFnSc2	Doris
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Salinger	Salinger	k1gMnSc1	Salinger
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
školu	škola	k1gFnSc4	škola
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Side	Side	k1gInSc4	Side
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
devátý	devátý	k4xOgInSc4	devátý
a	a	k8xC	a
desátý	desátý	k4xOgInSc4	desátý
ročník	ročník	k1gInSc4	ročník
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
škole	škola	k1gFnSc6	škola
McBurney	McBurnea	k1gFnSc2	McBurnea
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
J.	J.	kA	J.
D.	D.	kA	D.
stal	stát	k5eAaPmAgInS	stát
hercem	herc	k1gInSc7	herc
<g/>
,	,	kIx,	,
předvedl	předvést	k5eAaPmAgMnS	předvést
své	svůj	k3xOyFgNnSc4	svůj
nadání	nadání	k1gNnSc4	nadání
pro	pro	k7c4	pro
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
Valley	Vallea	k1gFnSc2	Vallea
Forge	Forg	k1gFnSc2	Forg
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
McBurney	McBurnea	k1gFnSc2	McBurnea
přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
školního	školní	k2eAgInSc2d1	školní
časopisu	časopis	k1gInSc2	časopis
a	a	k8xC	a
ve	v	k7c4	v
Valley	Valle	k2eAgInPc4d1	Valle
forge	forg	k1gInPc4	forg
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
povídky	povídka	k1gFnPc4	povídka
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
přikrývkou	přikrývka	k1gFnSc7	přikrývka
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
baterky	baterka	k1gFnSc2	baterka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
studium	studium	k1gNnSc4	studium
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
ho	on	k3xPp3gNnSc2	on
otec	otec	k1gMnSc1	otec
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
obchodovat	obchodovat	k5eAaImF	obchodovat
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
ho	on	k3xPp3gInSc4	on
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
si	se	k3xPyFc3	se
vylepšit	vylepšit	k5eAaPmF	vylepšit
svou	svůj	k3xOyFgFnSc4	svůj
němčinu	němčina	k1gFnSc4	němčina
a	a	k8xC	a
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc4	Vídeň
opustil	opustit	k5eAaPmAgMnS	opustit
asi	asi	k9	asi
měsíc	měsíc	k1gInSc4	měsíc
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
Rakousko	Rakousko	k1gNnSc1	Rakousko
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1938	[number]	k4	1938
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Adolfu	Adolf	k1gMnSc6	Adolf
Hitlerovi	Hitler	k1gMnSc6	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Ursinus	Ursinus	k1gInSc4	Ursinus
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
vydržel	vydržet	k5eAaPmAgMnS	vydržet
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
semestr	semestr	k1gInSc4	semestr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
večerní	večerní	k2eAgInPc4d1	večerní
kurzy	kurz	k1gInPc4	kurz
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
na	na	k7c6	na
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
učitelem	učitel	k1gMnSc7	učitel
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
editor	editor	k1gMnSc1	editor
časopisu	časopis	k1gInSc2	časopis
Story	story	k1gFnPc2	story
<g/>
,	,	kIx,	,
Whit	Whit	k2eAgInSc1d1	Whit
Burnett	Burnett	k1gInSc1	Burnett
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgMnSc2	druhý
semestru	semestr	k1gInSc2	semestr
kurzů	kurz	k1gInPc2	kurz
si	se	k3xPyFc3	se
Burnett	Burnett	k1gMnSc1	Burnett
všiml	všimnout	k5eAaPmAgMnS	všimnout
jeho	on	k3xPp3gInSc2	on
talentu	talent	k1gInSc2	talent
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dokončil	dokončit	k5eAaPmAgMnS	dokončit
tři	tři	k4xCgFnPc4	tři
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1940	[number]	k4	1940
Salingerovi	Salinger	k1gMnSc3	Salinger
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
Story	story	k1gFnSc6	story
první	první	k4xOgFnSc1	první
povídka	povídka	k1gFnSc1	povídka
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgFnSc1d1	pojednávající
o	o	k7c6	o
několika	několik	k4yIc6	několik
lidech	lid	k1gInPc6	lid
bez	bez	k7c2	bez
životního	životní	k2eAgInSc2d1	životní
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Burnett	Burnett	k1gMnSc1	Burnett
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
mentorem	mentor	k1gMnSc7	mentor
a	a	k8xC	a
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
dopisovali	dopisovat	k5eAaImAgMnP	dopisovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Oonou	Oona	k1gFnSc7	Oona
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neillovou	Neillová	k1gFnSc7	Neillová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
dramatika	dramatik	k1gMnSc2	dramatik
Eugena	Eugeno	k1gNnSc2	Eugeno
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neilla	Neilla	k1gMnSc1	Neilla
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jí	jíst	k5eAaImIp3nS	jíst
telefonoval	telefonovat	k5eAaImAgMnS	telefonovat
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
jí	jíst	k5eAaImIp3nS	jíst
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
dopisy	dopis	k1gInPc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Oona	Oona	k1gFnSc1	Oona
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Charlie	Charlie	k1gMnSc1	Charlie
Chaplinem	Chaplin	k1gInSc7	Chaplin
<g/>
,	,	kIx,	,
za	za	k7c2	za
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
vyhlídkové	vyhlídkový	k2eAgFnSc6d1	vyhlídková
lodi	loď	k1gFnSc6	loď
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
organizační	organizační	k2eAgMnSc1d1	organizační
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
jako	jako	k9	jako
bavič	bavič	k1gMnSc1	bavič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
posílat	posílat	k5eAaImF	posílat
své	svůj	k3xOyFgInPc4	svůj
krátké	krátký	k2eAgInPc4d1	krátký
příběhy	příběh	k1gInPc4	příběh
do	do	k7c2	do
magazínu	magazín	k1gInSc2	magazín
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gFnPc2	jeho
povídek	povídka	k1gFnPc2	povídka
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lunch	lunch	k1gInSc1	lunch
for	forum	k1gNnPc2	forum
Three	Thre	k1gFnSc2	Thre
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Oběd	oběd	k1gInSc4	oběd
pro	pro	k7c4	pro
tři	tři	k4xCgInPc4	tři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Monologue	Monologue	k1gNnSc2	Monologue
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Watery	Watera	k1gFnSc2	Watera
Highball	Highballa	k1gFnPc2	Highballa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Monolog	monolog	k1gInSc1	monolog
pro	pro	k7c4	pro
Wateryho	Watery	k1gMnSc4	Watery
Highballa	Highball	k1gMnSc4	Highball
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k8xC	i
went	went	k5eAaPmF	went
to	ten	k3xDgNnSc4	ten
school	schoolit	k5eAaPmRp2nS	schoolit
with	with	k1gMnSc1	with
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Chodil	chodit	k5eAaImAgMnS	chodit
jsem	být	k5eAaImIp1nS	být
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1941	[number]	k4	1941
přijali	přijmout	k5eAaPmAgMnP	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
povídku	povídka	k1gFnSc4	povídka
"	"	kIx"	"
<g/>
Slight	Slight	k1gMnSc1	Slight
Rebellion	Rebellion	k?	Rebellion
Off	Off	k1gMnSc1	Off
Madison	Madison	k1gMnSc1	Madison
<g/>
"	"	kIx"	"
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
nespokojeném	spokojený	k2eNgMnSc6d1	nespokojený
teenagerovi	teenager	k1gMnSc6	teenager
Holdenu	Holden	k1gMnSc6	Holden
Caulfieldovi	Caulfield	k1gMnSc6	Caulfield
s	s	k7c7	s
"	"	kIx"	"
<g/>
předválečnou	předválečný	k2eAgFnSc7d1	předválečná
nejistotou	nejistota	k1gFnSc7	nejistota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Japonsko	Japonsko	k1gNnSc1	Japonsko
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c4	na
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbor	k1gInSc4	Harbor
byla	být	k5eAaImAgFnS	být
povídka	povídka	k1gFnSc1	povídka
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nepublikovatelná	publikovatelný	k2eNgNnPc4d1	nepublikovatelné
<g/>
"	"	kIx"	"
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
co	co	k9	co
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
J.	J.	kA	J.
D.	D.	kA	D.
naverbován	naverbován	k2eAgInSc4d1	naverbován
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tažení	tažení	k1gNnSc2	tažení
z	z	k7c2	z
Normandie	Normandie	k1gFnSc2	Normandie
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Ernestem	Ernest	k1gMnSc7	Ernest
Hemingwayem	Hemingway	k1gMnSc7	Hemingway
<g/>
,	,	kIx,	,
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
a	a	k8xC	a
který	který	k3yRgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
válečný	válečný	k2eAgMnSc1d1	válečný
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ohromen	ohromit	k5eAaPmNgMnS	ohromit
Hemingwayovou	Hemingwayový	k2eAgFnSc7d1	Hemingwayový
přátelskostí	přátelskost	k1gFnSc7	přátelskost
a	a	k8xC	a
skromností	skromnost	k1gFnSc7	skromnost
<g/>
.	.	kIx.	.
</s>
<s>
Hemingway	Hemingwa	k1gMnPc4	Hemingwa
byl	být	k5eAaImAgMnS	být
zaujat	zaujat	k2eAgMnSc1d1	zaujat
jeho	jeho	k3xOp3gFnSc7	jeho
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
fantastický	fantastický	k2eAgInSc1d1	fantastický
talent	talent	k1gInSc1	talent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
spisovatelé	spisovatel	k1gMnPc1	spisovatel
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
dopisovali	dopisovat	k5eAaImAgMnP	dopisovat
<g/>
.	.	kIx.	.
</s>
<s>
Salinger	Salinger	k1gMnSc1	Salinger
Hemingwayovi	Hemingwaya	k1gMnSc3	Hemingwaya
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1946	[number]	k4	1946
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
setkání	setkání	k1gNnSc1	setkání
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
tou	ten	k3xDgFnSc7	ten
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
o	o	k7c6	o
Holdenu	Holden	k1gMnSc6	Holden
Caulfieldovi	Caulfield	k1gMnSc6	Caulfield
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc6d1	hlavní
postavě	postava	k1gFnSc6	postava
jeho	jeho	k3xOp3gInSc2	jeho
příběhu	příběh	k1gInSc2	příběh
"	"	kIx"	"
<g/>
Slight	Slight	k1gMnSc1	Slight
Rebellion	Rebellion	k?	Rebellion
Off	Off	k1gMnSc1	Off
Madison	Madison	k1gMnSc1	Madison
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
sám	sám	k3xTgMnSc1	sám
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kdo	kdo	k3yInSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
svěřil	svěřit	k5eAaPmAgMnS	svěřit
několika	několik	k4yIc3	několik
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
o	o	k7c6	o
Holdenu	Holden	k1gMnSc6	Holden
Caulfieldovi	Caulfield	k1gMnSc6	Caulfield
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
Kdo	kdo	k3yInSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
sedmnáctiletém	sedmnáctiletý	k2eAgMnSc6d1	sedmnáctiletý
Holdenovi	Holden	k1gMnSc6	Holden
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc2	jeho
vyloučení	vyloučení	k1gNnSc2	vyloučení
a	a	k8xC	a
odchodu	odchod	k1gInSc2	odchod
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
však	však	k9	však
význačná	význačný	k2eAgFnSc1d1	význačná
svým	svůj	k3xOyFgInSc7	svůj
ironickým	ironický	k2eAgInSc7d1	ironický
hlasem	hlas	k1gInSc7	hlas
vypravěče	vypravěč	k1gMnSc2	vypravěč
v	v	k7c6	v
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
Holdena	Holdena	k1gFnSc1	Holdena
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hluboký	hluboký	k2eAgInSc4d1	hluboký
pohled	pohled	k1gInSc4	pohled
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nespolehlivou	spolehlivý	k2eNgFnSc4d1	nespolehlivá
výpověď	výpověď	k1gFnSc4	výpověď
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
důležitost	důležitost	k1gFnSc4	důležitost
loajality	loajalita	k1gFnSc2	loajalita
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
falešnost	falešnost	k1gFnSc1	falešnost
<g/>
"	"	kIx"	"
dospělosti	dospělost	k1gFnSc6	dospělost
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
neupřímnost	neupřímnost	k1gFnSc4	neupřímnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Salinger	Salingero	k1gNnPc2	Salingero
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
školní	školní	k2eAgFnPc4d1	školní
noviny	novina	k1gFnPc4	novina
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
"	"	kIx"	"
<g/>
tak	tak	k9	tak
trochu	trochu	k6eAd1	trochu
<g/>
"	"	kIx"	"
autobiografický	autobiografický	k2eAgMnSc1d1	autobiografický
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Moje	můj	k3xOp1gNnSc1	můj
dětství	dětství	k1gNnSc1	dětství
bylo	být	k5eAaImAgNnS	být
hodně	hodně	k6eAd1	hodně
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xC	jako
toho	ten	k3xDgMnSc4	ten
chlapce	chlapec	k1gMnSc4	chlapec
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
...	...	k?	...
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
úleva	úleva	k1gFnSc1	úleva
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc1	ten
lidem	člověk	k1gMnPc3	člověk
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Prvotní	prvotní	k2eAgFnSc1d1	prvotní
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
knihu	kniha	k1gFnSc4	kniha
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
rozporuplné	rozporuplný	k2eAgFnPc1d1	rozporuplná
<g/>
,	,	kIx,	,
od	od	k7c2	od
hodnocení	hodnocení	k1gNnSc2	hodnocení
"	"	kIx"	"
<g/>
neobvykle	obvykle	k6eNd1	obvykle
brilantní	brilantní	k2eAgInSc1d1	brilantní
debut	debut	k1gInSc1	debut
<g/>
"	"	kIx"	"
od	od	k7c2	od
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
až	až	k9	až
po	po	k7c6	po
degradaci	degradace	k1gFnSc6	degradace
kvůli	kvůli	k7c3	kvůli
monotonnímu	monotonní	k2eAgInSc3d1	monotonní
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
"	"	kIx"	"
<g/>
nesmrtelnosti	nesmrtelnost	k1gFnPc4	nesmrtelnost
a	a	k8xC	a
perverzi	perverze	k1gFnSc4	perverze
<g/>
"	"	kIx"	"
Holdena	Holdena	k1gFnSc1	Holdena
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nadává	nadávat	k5eAaImIp3nS	nadávat
a	a	k8xC	a
často	často	k6eAd1	často
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
příležitostném	příležitostný	k2eAgInSc6d1	příležitostný
sexu	sex	k1gInSc6	sex
a	a	k8xC	a
prostituci	prostituce	k1gFnSc3	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
měl	mít	k5eAaImAgInS	mít
ovšem	ovšem	k9	ovšem
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
osmkrát	osmkrát	k6eAd1	osmkrát
k	k	k7c3	k
dotisku	dotisk	k1gInSc3	dotisk
<g/>
.	.	kIx.	.
</s>
<s>
Třicet	třicet	k4xCc4	třicet
týdnů	týden	k1gInPc2	týden
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
bestsellerů	bestseller	k1gInPc2	bestseller
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
úspěch	úspěch	k1gInSc1	úspěch
byl	být	k5eAaImAgInS	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
poklesem	pokles	k1gInSc7	pokles
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
román	román	k1gInSc1	román
stal	stát	k5eAaPmAgInS	stát
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Iana	Ianus	k1gMnSc2	Ianus
Hamiltona	Hamilton	k1gMnSc2	Hamilton
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
dospívající	dospívající	k2eAgMnPc1d1	dospívající
musí	muset	k5eAaImIp3nP	muset
koupit	koupit	k5eAaPmF	koupit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
příručku	příručka	k1gFnSc4	příručka
jak	jak	k8xS	jak
být	být	k5eAaImF	být
,	,	kIx,	,
<g/>
cool	coonout	k5eAaPmAgMnS	coonout
<g/>
'	'	kIx"	'
a	a	k8xC	a
neafektovaný	afektovaný	k2eNgInSc1d1	neafektovaný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
začaly	začít	k5eAaPmAgFnP	začít
vydávat	vydávat	k5eAaImF	vydávat
články	článek	k1gInPc1	článek
o	o	k7c6	o
"	"	kIx"	"
<g/>
Kultu	kult	k1gInSc6	kult
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
<g/>
"	"	kIx"	"
a	a	k8xC	a
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
amerických	americký	k2eAgFnPc6d1	americká
školách	škola	k1gFnPc6	škola
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
tématu	téma	k1gNnSc3	téma
a	a	k8xC	a
"	"	kIx"	"
<g/>
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
užívání	užívání	k1gNnSc3	užívání
sprostých	sprostý	k2eAgNnPc2d1	sprosté
slov	slovo	k1gNnPc2	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
pečlivý	pečlivý	k2eAgMnSc1d1	pečlivý
rodič	rodič	k1gMnSc1	rodič
napočítal	napočítat	k5eAaPmAgMnS	napočítat
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
verzi	verze	k1gFnSc6	verze
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
237	[number]	k4	237
<g/>
krát	krát	k6eAd1	krát
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
goddam	goddam	k1gInSc1	goddam
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
58	[number]	k4	58
<g/>
krát	krát	k6eAd1	krát
"	"	kIx"	"
<g/>
bastard	bastard	k1gMnSc1	bastard
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
krát	krát	k6eAd1	krát
"	"	kIx"	"
<g/>
christsake	christsakat	k5eAaPmIp3nS	christsakat
<g/>
"	"	kIx"	"
a	a	k8xC	a
6	[number]	k4	6
<g/>
krát	krát	k6eAd1	krát
"	"	kIx"	"
<g/>
fuck	fuck	k6eAd1	fuck
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
amerických	americký	k2eAgMnPc2d1	americký
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zadali	zadat	k5eAaPmAgMnP	zadat
knihu	kniha	k1gFnSc4	kniha
k	k	k7c3	k
četbě	četba	k1gFnSc3	četba
<g/>
,	,	kIx,	,
propuštěno	propuštěn	k2eAgNnSc1d1	propuštěno
anebo	anebo	k8xC	anebo
donuceno	donucen	k2eAgNnSc1d1	donuceno
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
cenzura	cenzura	k1gFnSc1	cenzura
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc1	román
Kdo	kdo	k3yQnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
nepochybně	pochybně	k6eNd1	pochybně
nejčastěji	často	k6eAd3	často
cenzurovanou	cenzurovaný	k2eAgFnSc7d1	cenzurovaná
knihou	kniha	k1gFnSc7	kniha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhým	druhý	k4xOgNnSc7	druhý
nejčastěji	často	k6eAd3	často
vyučovaným	vyučovaný	k2eAgInSc7d1	vyučovaný
románem	román	k1gInSc7	román
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
po	po	k7c6	po
O	o	k7c6	o
myších	myš	k1gFnPc6	myš
a	a	k8xC	a
lidech	lid	k1gInPc6	lid
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Steinbecka	Steinbecko	k1gNnSc2	Steinbecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
čtená	čtený	k2eAgFnSc1d1	čtená
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
celosvětově	celosvětově	k6eAd1	celosvětově
prodáno	prodat	k5eAaPmNgNnS	prodat
přes	přes	k7c4	přes
65	[number]	k4	65
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
románu	román	k1gInSc2	román
Kdo	kdo	k3yInSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
dostal	dostat	k5eAaPmAgInS	dostat
Salinger	Salinger	k1gInSc1	Salinger
několik	několik	k4yIc1	několik
nabídek	nabídka	k1gFnPc2	nabídka
ke	k	k7c3	k
zfilmování	zfilmování	k1gNnSc3	zfilmování
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
od	od	k7c2	od
Samuela	Samuel	k1gMnSc2	Samuel
Goldwyna	Goldwyn	k1gMnSc2	Goldwyn
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
režiséry	režisér	k1gMnPc4	režisér
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
chtěli	chtít	k5eAaImAgMnP	chtít
zajistit	zajistit	k5eAaPmF	zajistit
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
zfilmování	zfilmování	k1gNnSc4	zfilmování
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
tak	tak	k6eAd1	tak
význačná	význačný	k2eAgNnPc4d1	význačné
jména	jméno	k1gNnPc4	jméno
jako	jako	k8xS	jako
Billy	Bill	k1gMnPc4	Bill
Wilder	Wilder	k1gInSc4	Wilder
<g/>
,	,	kIx,	,
Harvey	Harvea	k1gFnPc4	Harvea
Weinstein	Weinsteina	k1gFnPc2	Weinsteina
či	či	k8xC	či
Steven	Stevna	k1gFnPc2	Stevna
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Salinger	Salinger	k1gMnSc1	Salinger
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
Jerry	Jerra	k1gFnPc1	Jerra
Lewis	Lewis	k1gFnPc2	Lewis
již	již	k6eAd1	již
léta	léto	k1gNnPc4	léto
snaží	snažit	k5eAaImIp3nP	snažit
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
roli	role	k1gFnSc3	role
Holdena	Holdeno	k1gNnSc2	Holdeno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
ho	on	k3xPp3gMnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Joyce	Joyce	k1gFnSc1	Joyce
Maynardová	Maynardová	k1gFnSc1	Maynardová
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kdy	kdy	k6eAd1	kdy
mohl	moct	k5eAaImAgMnS	moct
hrát	hrát	k5eAaImF	hrát
Holdena	Holden	k2eAgMnSc4d1	Holden
Caulfielda	Caulfield	k1gMnSc4	Caulfield
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sám	sám	k3xTgInSc1	sám
J.	J.	kA	J.
D.	D.	kA	D.
Salinger	Salinger	k1gMnSc1	Salinger
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Catcher	Catchra	k1gFnPc2	Catchra
in	in	k?	in
the	the	k?	the
Rye	Rye	k1gMnSc1	Rye
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
;	;	kIx,	;
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
<g/>
)	)	kIx)	)
–	–	k?	–
vypravěč	vypravěč	k1gMnSc1	vypravěč
je	být	k5eAaImIp3nS	být
dospívající	dospívající	k2eAgMnSc1d1	dospívající
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
student	student	k1gMnSc1	student
Holden	Holdna	k1gFnPc2	Holdna
Caulfield	Caulfield	k1gMnSc1	Caulfield
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
sympatizuje	sympatizovat	k5eAaImIp3nS	sympatizovat
a	a	k8xC	a
provází	provázet	k5eAaImIp3nS	provázet
čtenáře	čtenář	k1gMnPc4	čtenář
po	po	k7c6	po
jeho	jeho	k3xOp3gInPc6	jeho
pocitech	pocit	k1gInPc6	pocit
<g/>
,	,	kIx,	,
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
zmatcích	zmatek	k1gInPc6	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
je	být	k5eAaImIp3nS	být
nekomunikativní	komunikativní	k2eNgMnSc1d1	nekomunikativní
<g/>
,	,	kIx,	,
složitost	složitost	k1gFnSc1	složitost
navazování	navazování	k1gNnSc2	navazování
kontaktů	kontakt	k1gInPc2	kontakt
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
odlišnosti	odlišnost	k1gFnSc6	odlišnost
jeho	on	k3xPp3gInSc2	on
světa	svět	k1gInSc2	svět
a	a	k8xC	a
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
srovnání	srovnání	k1gNnSc1	srovnání
přirozeného	přirozený	k2eAgInSc2d1	přirozený
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
šaškujícím	šaškující	k2eAgMnSc7d1	šaškující
<g/>
"	"	kIx"	"
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčtenějších	čtený	k2eAgFnPc2d3	Nejčtenější
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Devět	devět	k4xCc1	devět
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
Nine	Nine	k1gFnSc1	Nine
Stories	Stories	k1gMnSc1	Stories
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Franny	Franen	k2eAgFnPc1d1	Franen
a	a	k8xC	a
Zooey	Zooe	k2eAgFnPc1d1	Zooe
(	(	kIx(	(
<g/>
Franny	Franen	k2eAgFnPc1d1	Franen
and	and	k?	and
Zooey	Zooe	k2eAgFnPc1d1	Zooe
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
tesaři	tesař	k1gMnPc1	tesař
<g/>
,	,	kIx,	,
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
střechu	střecha	k1gFnSc4	střecha
zvedněte	zvednout	k5eAaPmRp2nP	zvednout
<g/>
;	;	kIx,	;
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
(	(	kIx(	(
<g/>
Raise	Rais	k1gMnSc2	Rais
High	High	k1gMnSc1	High
The	The	k1gMnSc1	The
Roof	Roof	k1gMnSc1	Roof
Beam	Beam	k1gMnSc1	Beam
<g/>
,	,	kIx,	,
Carpenters	Carpenters	k1gInSc1	Carpenters
<g/>
;	;	kIx,	;
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
Salinger	Salinger	k1gInSc1	Salinger
napsal	napsat	k5eAaPmAgInS	napsat
celkem	celkem	k6eAd1	celkem
35	[number]	k4	35
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
např.	např.	kA	např.
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Smějící	smějící	k2eAgNnSc1d1	smějící
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
the	the	k?	the
Laughing	Laughing	k1gInSc1	Laughing
man	mana	k1gFnPc2	mana
<g/>
)	)	kIx)	)
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
jedné	jeden	k4xCgFnSc6	jeden
postavě	postava	k1gFnSc6	postava
v	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
seriálu	seriál	k1gInSc6	seriál
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
také	také	k9	také
uveden	uveden	k2eAgInSc1d1	uveden
úryvek	úryvek	k1gInSc1	úryvek
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Kdo	kdo	k3yRnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jerome	Jerom	k1gInSc5	Jerom
David	David	k1gMnSc1	David
Salinger	Salingra	k1gFnPc2	Salingra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
J.	J.	kA	J.
D.	D.	kA	D.
Salinger	Salinger	k1gMnSc1	Salinger
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
J.	J.	kA	J.
D.	D.	kA	D.
Salinger	Salinger	k1gMnSc1	Salinger
</s>
</p>
