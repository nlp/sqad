<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Leedsu	Leeds	k1gInSc2	Leeds
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
angličtiny	angličtina	k1gFnSc2	angličtina
tamní	tamní	k2eAgFnSc2d1	tamní
univerzity	univerzita	k1gFnSc2	univerzita
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
titulu	titul	k1gInSc2	titul
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
anglosaštiny	anglosaština	k1gFnSc2	anglosaština
<g/>
.	.	kIx.	.
</s>
