<p>
<s>
StartupJobs	StartupJobs	k1gInSc1	StartupJobs
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
nabídky	nabídka	k1gFnPc4	nabídka
od	od	k7c2	od
startupů	startup	k1gInPc2	startup
<g/>
.	.	kIx.	.
</s>
<s>
Uchazeči	uchazeč	k1gMnPc1	uchazeč
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
najít	najít	k5eAaPmF	najít
aktuální	aktuální	k2eAgFnPc4d1	aktuální
nabídky	nabídka	k1gFnPc4	nabídka
práce	práce	k1gFnSc2	práce
nebo	nebo	k8xC	nebo
znalostní	znalostní	k2eAgInPc4d1	znalostní
testy	test	k1gInPc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Uchazeč	uchazeč	k1gMnSc1	uchazeč
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
životopis	životopis	k1gInSc4	životopis
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
databáze	databáze	k1gFnSc2	databáze
životopisů	životopis	k1gInPc2	životopis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
personalistům	personalista	k1gMnPc3	personalista
inzerujících	inzerující	k2eAgFnPc2d1	inzerující
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c6	o
práci	práce	k1gFnSc6	práce
i	i	k9	i
široká	široký	k2eAgFnSc1d1	široká
veřejnost	veřejnost	k1gFnSc1	veřejnost
mohou	moct	k5eAaImIp3nP	moct
také	také	k6eAd1	také
využít	využít	k5eAaPmF	využít
znalostních	znalostní	k2eAgInPc2d1	znalostní
testů	test	k1gInPc2	test
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
převážně	převážně	k6eAd1	převážně
technologických	technologický	k2eAgInPc2d1	technologický
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
PHP	PHP	kA	PHP
<g/>
,	,	kIx,	,
HTML	HTML	kA	HTML
<g/>
,	,	kIx,	,
CSS	CSS	kA	CSS
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
portálu	portál	k1gInSc2	portál
StartupJobs	StartupJobsa	k1gFnPc2	StartupJobsa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
StartupJobs	StartupJobsa	k1gFnPc2	StartupJobsa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
</p>
<p>
<s>
==	==	k?	==
Vize	vize	k1gFnSc1	vize
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
StartupJobs	StartupJobs	k1gInSc1	StartupJobs
vám	vy	k3xPp2nPc3	vy
pomůže	pomoct	k5eAaPmIp3nS	pomoct
najít	najít	k5eAaPmF	najít
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vás	vy	k3xPp2nPc4	vy
bude	být	k5eAaImBp3nS	být
bavit	bavit	k5eAaImF	bavit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
StartupJobs	StartupJobs	k1gInSc1	StartupJobs
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
2012	[number]	k4	2012
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vytvořit	vytvořit	k5eAaPmF	vytvořit
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgFnPc4d1	pracovní
nabídky	nabídka	k1gFnPc4	nabídka
pro	pro	k7c4	pro
startup	startup	k1gInSc4	startup
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
získává	získávat	k5eAaImIp3nS	získávat
investici	investice	k1gFnSc4	investice
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
MITON	MITON	kA	MITON
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
projekt	projekt	k1gInSc1	projekt
StartupMap	StartupMap	k1gInSc1	StartupMap
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
mapu	mapa	k1gFnSc4	mapa
českých	český	k2eAgInPc2d1	český
startupů	startup	k1gInPc2	startup
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
spouští	spouštět	k5eAaImIp3nS	spouštět
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
cut-e	cut	k6eAd1	cut-e
projekty	projekt	k1gInPc1	projekt
Hiri	Hiri	k1gNnSc1	Hiri
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
Hiri	Hir	k1gFnSc2	Hir
PRO	pro	k7c4	pro
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
znalostní	znalostní	k2eAgNnSc4d1	znalostní
a	a	k8xC	a
psychometrické	psychometrický	k2eAgNnSc4d1	psychometrické
testování	testování	k1gNnSc4	testování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
expanduje	expandovat	k5eAaImIp3nS	expandovat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
se	s	k7c7	s
StartupJobs	StartupJobsa	k1gFnPc2	StartupJobsa
<g/>
.	.	kIx.	.
<g/>
pl.	pl.	k?	pl.
</s>
</p>
<p>
<s>
==	==	k?	==
Portfolio	portfolio	k1gNnSc1	portfolio
==	==	k?	==
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
pracovní	pracovní	k2eAgInSc1d1	pracovní
portál	portál	k1gInSc1	portál
StartupJobs	StartupJobsa	k1gFnPc2	StartupJobsa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
spuštěno	spuštěn	k2eAgNnSc1d1	spuštěno
testování	testování	k1gNnSc1	testování
znalostí	znalost	k1gFnPc2	znalost
na	na	k7c4	na
StartupJobs	StartupJobs	k1gInSc4	StartupJobs
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
mapa	mapa	k1gFnSc1	mapa
startupů	startup	k1gInPc2	startup
StartupMap	StartupMap	k1gMnSc1	StartupMap
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
pro	pro	k7c4	pro
znalostní	znalostní	k2eAgNnSc4d1	znalostní
a	a	k8xC	a
psychometrické	psychometrický	k2eAgNnSc4d1	psychometrické
testování	testování	k1gNnSc4	testování
Hiri	Hir	k1gFnSc2	Hir
PRO	pro	k7c4	pro
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
Hiri	Hir	k1gFnSc2	Hir
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
spuštění	spuštění	k1gNnSc6	spuštění
polské	polský	k2eAgFnSc2d1	polská
verze	verze	k1gFnSc2	verze
StartupJobs	StartupJobsa	k1gFnPc2	StartupJobsa
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
akvizice	akvizice	k1gFnSc1	akvizice
domény	doména	k1gFnSc2	doména
StartupJobs	StartupJobsa	k1gFnPc2	StartupJobsa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
==	==	k?	==
Testování	testování	k1gNnPc1	testování
==	==	k?	==
</s>
</p>
<p>
<s>
StartupJobs	StartupJobs	k1gInSc1	StartupJobs
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
testování	testování	k1gNnSc2	testování
znalostními	znalostní	k2eAgInPc7d1	znalostní
testy	test	k1gInPc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
především	především	k9	především
na	na	k7c4	na
programovací	programovací	k2eAgInPc4d1	programovací
jazyky	jazyk	k1gInPc4	jazyk
(	(	kIx(	(
<g/>
PHP	PHP	kA	PHP
<g/>
,	,	kIx,	,
HTML	HTML	kA	HTML
<g/>
,	,	kIx,	,
CSS	CSS	kA	CSS
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
test	test	k1gInSc1	test
trvá	trvat	k5eAaImIp3nS	trvat
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
20	[number]	k4	20
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
čtyř	čtyři	k4xCgFnPc2	čtyři
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
generátoru	generátor	k1gInSc2	generátor
položek	položka	k1gFnPc2	položka
náhodně	náhodně	k6eAd1	náhodně
generují	generovat	k5eAaImIp3nP	generovat
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
testovaného	testovaný	k2eAgMnSc4d1	testovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	s	k7c7	s
znalostními	znalostní	k2eAgInPc7d1	znalostní
testy	test	k1gInPc7	test
otestovalo	otestovat	k5eAaPmAgNnS	otestovat
17.000	[number]	k4	17.000
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
