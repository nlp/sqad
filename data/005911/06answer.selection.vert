<s>
Mezi	mezi	k7c7	mezi
komunitou	komunita	k1gFnSc7	komunita
Pythonu	Python	k1gMnSc6	Python
je	být	k5eAaImIp3nS	být
Van	van	k1gInSc1	van
Rossum	Rossum	k1gInSc1	Rossum
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Benevolent	Benevolent	k1gInSc4	Benevolent
Dictator	Dictator	k1gInSc4	Dictator
for	forum	k1gNnPc2	forum
Life	Lif	k1gMnSc2	Lif
(	(	kIx(	(
<g/>
BDFL	BDFL	kA	BDFL
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
benevolentní	benevolentní	k2eAgMnPc1d1	benevolentní
doživotní	doživotní	k2eAgMnSc1d1	doživotní
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
)	)	kIx)	)
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
dohledu	dohled	k1gInSc6	dohled
nad	nad	k7c7	nad
procesem	proces	k1gInSc7	proces
vývoje	vývoj	k1gInSc2	vývoj
Pythonu	Python	k1gMnSc3	Python
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
významná	významný	k2eAgNnPc4d1	významné
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
