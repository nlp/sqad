<p>
<s>
Státní	státní	k2eAgFnPc1d1	státní
hranice	hranice	k1gFnPc1	hranice
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
byly	být	k5eAaImAgFnP	být
ustanoveny	ustanovit	k5eAaPmNgFnP	ustanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
po	po	k7c6	po
menších	malý	k2eAgFnPc6d2	menší
změnách	změna	k1gFnPc6	změna
platí	platit	k5eAaImIp3nS	platit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nově	nově	k6eAd1	nově
ustanovené	ustanovený	k2eAgFnPc4d1	ustanovená
hranice	hranice	k1gFnPc4	hranice
kopírující	kopírující	k2eAgFnPc4d1	kopírující
z	z	k7c2	z
části	část	k1gFnSc2	část
hranice	hranice	k1gFnSc2	hranice
Slavonie	Slavonie	k1gFnSc2	Slavonie
a	a	k8xC	a
poté	poté	k6eAd1	poté
SR	SR	kA	SR
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
sousedů	soused	k1gMnPc2	soused
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
(	(	kIx(	(
<g/>
sever	sever	k1gInSc1	sever
<g/>
)	)	kIx)	)
–	–	k?	–
546	[number]	k4	546
km	km	kA	km
</s>
</p>
<p>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
)	)	kIx)	)
–	–	k?	–
329	[number]	k4	329
km	km	kA	km
</s>
</p>
<p>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
)	)	kIx)	)
–	–	k?	–
241	[number]	k4	241
km	km	kA	km
</s>
</p>
<p>
<s>
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
–	–	k?	–
932	[number]	k4	932
km	km	kA	km
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
–	–	k?	–
14	[number]	k4	14
km	km	kA	km
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
spor	spor	k1gInSc1	spor
se	s	k7c7	s
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
o	o	k7c4	o
námořní	námořní	k2eAgFnSc4d1	námořní
a	a	k8xC	a
část	část	k1gFnSc4	část
pozemních	pozemní	k2eAgFnPc2d1	pozemní
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Dragonja	Dragonj	k1gInSc2	Dragonj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vod	voda	k1gFnPc2	voda
kvůli	kvůli	k7c3	kvůli
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
území	území	k1gNnSc4	území
dělá	dělat	k5eAaImIp3nS	dělat
též	též	k9	též
nároky	nárok	k1gInPc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc4	spor
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
využilo	využít	k5eAaPmAgNnS	využít
k	k	k7c3	k
pozastavení	pozastavení	k1gNnSc3	pozastavení
přijímacích	přijímací	k2eAgInPc2d1	přijímací
řízeních	řízení	k1gNnPc6	řízení
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
sporu	spor	k1gInSc3	spor
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
i	i	k9	i
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
Martti	Martti	k1gNnSc2	Martti
Ahtisaari	Ahtisaar	k1gFnSc2	Ahtisaar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Joško	Joško	k6eAd1	Joško
Joras	Joras	k1gInSc1	Joras
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
