<s>
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k8xC	jako
baskytarista	baskytarista	k1gMnSc1	baskytarista
skupiny	skupina	k1gFnSc2	skupina
Patti	Patť	k1gFnSc2	Patť
Smith	Smith	k1gMnSc1	Smith
Group	Group	k1gMnSc1	Group
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
režisérů	režisér	k1gMnPc2	režisér
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
The	The	k1gMnSc1	The
Blank	Blank	k1gMnSc1	Blank
Generation	Generation	k1gInSc4	Generation
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Kabriolet	kabriolet	k1gInSc1	kabriolet
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Pavlem	Pavel	k1gMnSc7	Pavel
a	a	k8xC	a
rodiči	rodič	k1gMnPc7	rodič
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Karel	Karel	k1gMnSc1	Karel
Král	Král	k1gMnSc1	Král
zde	zde	k6eAd1	zde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
překladatel	překladatel	k1gMnSc1	překladatel
pro	pro	k7c4	pro
ČTK	ČTK	kA	ČTK
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nejprve	nejprve	k6eAd1	nejprve
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Apple	Apple	kA	Apple
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
věnoval	věnovat	k5eAaImAgMnS	věnovat
aktivnímu	aktivní	k2eAgNnSc3d1	aktivní
hraní	hraní	k1gNnSc3	hraní
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Luger	Lugra	k1gFnPc2	Lugra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Blondie	Blondie	k1gFnSc2	Blondie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
doprovodné	doprovodný	k2eAgFnSc2d1	doprovodná
skupiny	skupina	k1gFnSc2	skupina
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Patti	Patť	k1gFnSc2	Patť
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
nahrál	nahrát	k5eAaBmAgMnS	nahrát
čtyři	čtyři	k4xCgNnPc4	čtyři
studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
pro	pro	k7c4	pro
ní	on	k3xPp3gFnSc6	on
řadu	řada	k1gFnSc4	řada
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Ask	Ask	k1gFnSc1	Ask
the	the	k?	the
Angels	Angels	k1gInSc1	Angels
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pissing	Pissing	k1gInSc1	Pissing
in	in	k?	in
a	a	k8xC	a
River	Rivero	k1gNnPc2	Rivero
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Dancing	dancing	k1gInSc1	dancing
Barefoot	Barefoot	k1gInSc1	Barefoot
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
a	a	k8xC	a
Kral	Kral	k1gMnSc1	Kral
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
(	(	kIx(	(
<g/>
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
spolupráce	spolupráce	k1gFnSc2	spolupráce
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
jen	jen	k9	jen
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
Even	Evena	k1gFnPc2	Evena
Cowgirls	Cowgirlsa	k1gFnPc2	Cowgirlsa
Get	Get	k1gFnSc2	Get
the	the	k?	the
Blues	blues	k1gNnSc1	blues
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
zpěváka	zpěvák	k1gMnSc2	zpěvák
Iggyho	Iggy	k1gMnSc2	Iggy
Popa	pop	k1gMnSc2	pop
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
nahrál	nahrát	k5eAaPmAgMnS	nahrát
alba	album	k1gNnPc4	album
Soldier	Soldira	k1gFnPc2	Soldira
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
a	a	k8xC	a
Party	party	k1gFnSc1	party
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Kral	Krala	k1gFnPc2	Krala
nepřispěl	přispět	k5eNaPmAgMnS	přispět
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
písní	píseň	k1gFnSc7	píseň
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
je	být	k5eAaImIp3nS	být
však	však	k9	však
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Popem	pop	k1gMnSc7	pop
autorem	autor	k1gMnSc7	autor
osmi	osm	k4xCc2	osm
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
i	i	k8xC	i
hitu	hit	k1gInSc2	hit
"	"	kIx"	"
<g/>
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
později	pozdě	k6eAd2	pozdě
nahrál	nahrát	k5eAaPmAgMnS	nahrát
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Iggyho	Iggy	k1gMnSc4	Iggy
Popa	pop	k1gMnSc4	pop
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Johnem	John	k1gMnSc7	John
Waitem	Wait	k1gMnSc7	Wait
(	(	kIx(	(
<g/>
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
albu	album	k1gNnSc6	album
Ignition	Ignition	k1gInSc4	Ignition
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgMnS	složit
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Subway	Subwaa	k1gFnPc1	Subwaa
Riders	Ridersa	k1gFnPc2	Ridersa
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bistro	bistro	k1gNnSc4	bistro
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
producent	producent	k1gMnSc1	producent
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Band	banda	k1gFnPc2	banda
of	of	k?	of
Outsiders	Outsidersa	k1gFnPc2	Outsidersa
<g/>
,	,	kIx,	,
The	The	k1gFnPc2	The
Vipers	Vipersa	k1gFnPc2	Vipersa
a	a	k8xC	a
Joy	Joy	k1gFnPc2	Joy
Rider	Ridra	k1gFnPc2	Ridra
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Eastern	Easterno	k1gNnPc2	Easterno
Bloc	Bloc	k1gFnSc4	Bloc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vydal	vydat	k5eAaPmAgInS	vydat
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vracet	vracet	k5eAaImF	vracet
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
producenta	producent	k1gMnSc2	producent
(	(	kIx(	(
<g/>
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
nebo	nebo	k8xC	nebo
Garage	Garage	k1gInSc1	Garage
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
Nostalgia	Nostalgius	k1gMnSc4	Nostalgius
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
nahrál	nahrát	k5eAaBmAgInS	nahrát
album	album	k1gNnSc4	album
Alias	alias	k9	alias
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
Davidem	David	k1gMnSc7	David
Kollerem	Koller	k1gMnSc7	Koller
a	a	k8xC	a
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Karlem	Karel	k1gMnSc7	Karel
Šůchou	Šůcha	k1gMnSc7	Šůcha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Suchý	Suchý	k1gMnSc1	Suchý
natočil	natočit	k5eAaBmAgMnS	natočit
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
Dancing	dancing	k1gInSc4	dancing
Barefoot	Barefoota	k1gFnPc2	Barefoota
<g/>
.	.	kIx.	.
</s>
<s>
Nostalgia	Nostalgia	k1gFnSc1	Nostalgia
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Looking	Looking	k1gInSc1	Looking
Back	Back	k1gInSc1	Back
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Native	Natiev	k1gFnSc2	Natiev
<g/>
:	:	kIx,	:
His	his	k1gNnSc1	his
Native	Natiev	k1gFnSc2	Natiev
Complete	Comple	k1gNnSc2	Comple
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Alias	alias	k9	alias
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
Davidem	David	k1gMnSc7	David
Kollerem	Koller	k1gMnSc7	Koller
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Šůchou	Šůcha	k1gMnSc7	Šůcha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modré	modré	k1gNnSc4	modré
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
Soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Prohlédnutí	prohlédnutí	k1gNnSc1	prohlédnutí
/	/	kIx~	/
Clear	Clear	k1gInSc1	Clear
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
...	...	k?	...
"	"	kIx"	"
<g/>
dancing	dancing	k1gInSc1	dancing
barefoot	barefoot	k1gInSc1	barefoot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Dancing	dancing	k1gInSc1	dancing
Reboot	Reboot	k1gMnSc1	Reboot
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
Remixed	Remixed	k1gMnSc1	Remixed
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Živě	živě	k6eAd1	živě
<g/>
,	,	kIx,	,
dětským	dětský	k2eAgInPc3d1	dětský
domovům	domov	k1gInPc3	domov
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Cabriolet	Cabriolet	k1gInSc1	Cabriolet
(	(	kIx(	(
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
;	;	kIx,	;
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Photoalbum	Photoalbum	k1gInSc1	Photoalbum
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Erotická	erotický	k2eAgFnSc1d1	erotická
revue	revue	k1gFnSc1	revue
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Renatou	Renata	k1gFnSc7	Renata
Rychlou	Rychlá	k1gFnSc7	Rychlá
<g/>
)	)	kIx)	)
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Always	Alwaysa	k1gFnPc2	Alwaysa
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Horses	Horses	k1gInSc1	Horses
(	(	kIx(	(
<g/>
Patti	Patt	k2eAgMnPc1d1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Radio	radio	k1gNnSc1	radio
Ethiopia	Ethiopius	k1gMnSc2	Ethiopius
(	(	kIx(	(
<g/>
Patti	Patti	k1gNnSc7	Patti
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Easter	Easter	k1gInSc1	Easter
(	(	kIx(	(
<g/>
Patti	Patt	k2eAgMnPc1d1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Wawe	Wawe	k1gFnSc1	Wawe
(	(	kIx(	(
<g/>
Patti	Patt	k2eAgMnPc1d1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc1	violoncello
Soldier	Soldira	k1gFnPc2	Soldira
(	(	kIx(	(
<g/>
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
Party	parta	k1gFnSc2	parta
(	(	kIx(	(
<g/>
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
Ignition	Ignition	k1gInSc1	Ignition
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Waite	Wait	k1gInSc5	Wait
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
Insomnia	Insomnium	k1gNnSc2	Insomnium
in	in	k?	in
Zambia	Zambia	k1gFnSc1	Zambia
(	(	kIx(	(
<g/>
Joy	Joy	k1gMnSc1	Joy
Rider	Rider	k1gMnSc1	Rider
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Outta	Outta	k1gMnSc1	Outta
the	the	k?	the
Nest	Nest	k1gMnSc1	Nest
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Vipers	Vipersa	k1gFnPc2	Vipersa
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
produkce	produkce	k1gFnSc1	produkce
Everything	Everything	k1gInSc1	Everything
Takes	Takes	k1gMnSc1	Takes
Forever	Forever	k1gMnSc1	Forever
(	(	kIx(	(
<g/>
Band	band	k1gInSc1	band
of	of	k?	of
Outsiders	Outsiders	k1gInSc1	Outsiders
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Up	Up	k1gFnSc2	Up
the	the	k?	the
River	River	k1gInSc1	River
(	(	kIx(	(
<g/>
Band	band	k1gInSc1	band
of	of	k?	of
Outsiders	Outsiders	k1gInSc1	Outsiders
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Even	Even	k1gInSc4	Even
Cowgirls	Cowgirls	k1gInSc1	Cowgirls
Get	Get	k1gFnSc1	Get
the	the	k?	the
Blues	blues	k1gNnSc1	blues
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Eastern	Eastern	k1gInSc1	Eastern
Bloc	Bloc	k1gFnSc1	Bloc
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Eastern	Eastern	k1gInSc1	Eastern
Bloc	Bloc	k1gInSc1	Bloc
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Playtime	Playtim	k1gInSc5	Playtim
(	(	kIx(	(
<g/>
Marc	Marc	k1gInSc4	Marc
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
Exit	exit	k1gInSc1	exit
at	at	k?	at
the	the	k?	the
Axis	Axis	k1gInSc1	Axis
(	(	kIx(	(
<g/>
Sky	Sky	k1gFnSc1	Sky
Cries	Cries	k1gInSc1	Cries
Mary	Mary	k1gFnSc1	Mary
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Native	Natiev	k1gFnSc2	Natiev
(	(	kIx(	(
<g/>
Native	Natiev	k1gFnSc2	Natiev
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Satisfied	Satisfied	k1gMnSc1	Satisfied
Mind	Mind	k1gMnSc1	Mind
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Walkabouts	Walkaboutsa	k1gFnPc2	Walkaboutsa
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
syntezátor	syntezátor	k1gInSc1	syntezátor
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
filmu	film	k1gInSc2	film
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Dvořák	Dvořák	k1gMnSc1	Dvořák
&	&	k?	&
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Černý	Černý	k1gMnSc1	Černý
kočky	kočka	k1gFnSc2	kočka
mokrý	mokrý	k2eAgMnSc1d1	mokrý
žáby	žába	k1gFnPc4	žába
(	(	kIx(	(
<g/>
Lucie	Lucie	k1gFnPc4	Lucie
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
Garage	Garage	k1gFnSc1	Garage
(	(	kIx(	(
<g/>
Garage	Garage	k1gNnSc1	Garage
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Ryzí	ryzí	k2eAgNnSc1d1	ryzí
zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
Alice	Alice	k1gFnSc2	Alice
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Live	Live	k1gFnSc1	Live
from	from	k1gInSc4	from
Bunkr	bunkr	k1gInSc1	bunkr
-	-	kIx~	-
Prague	Prague	k1gInSc1	Prague
(	(	kIx(	(
<g/>
Noel	Noel	k1gInSc1	Noel
Redding	Redding	k1gInSc1	Redding
and	and	k?	and
Friends	Friends	k1gInSc1	Friends
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Happyend	happyend	k1gInSc1	happyend
(	(	kIx(	(
<g/>
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
Pohyby	pohyb	k1gInPc1	pohyb
(	(	kIx(	(
<g/>
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Znám	znát	k5eAaImIp1nS	znát
tolik	tolik	k4yIc4	tolik
písní	píseň	k1gFnPc2	píseň
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Walk	Walk	k1gInSc1	Walk
Choc	Choc	k1gFnSc1	Choc
Ice	Ice	k1gMnSc1	Ice
(	(	kIx(	(
<g/>
Walk	Walk	k1gMnSc1	Walk
Choc	Choc	k1gFnSc1	Choc
Ice	Ice	k1gFnSc1	Ice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc2	produkce
Gypsy	gyps	k1gInPc1	gyps
Streams	Streamsa	k1gFnPc2	Streamsa
(	(	kIx(	(
<g/>
Triny	Trina	k1gFnPc4	Trina
Vocal	Vocal	k1gInSc4	Vocal
Trio	trio	k1gNnSc1	trio
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
Spousta	spousta	k1gFnSc1	spousta
andělů	anděl	k1gMnPc2	anděl
(	(	kIx(	(
<g/>
Aneta	Aneta	k1gFnSc1	Aneta
Langerová	Langerová	k1gFnSc1	Langerová
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
produkce	produkce	k1gFnSc1	produkce
Peace	Peace	k1gMnSc1	Peace
Breaker	Breaker	k1gMnSc1	Breaker
(	(	kIx(	(
<g/>
Skew	Skew	k1gMnSc1	Skew
Siskin	Siskin	k1gMnSc1	Siskin
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Come	Com	k1gInSc2	Com
Hell	Hell	k1gMnSc1	Hell
or	or	k?	or
High	High	k1gMnSc1	High
Water	Water	k1gMnSc1	Water
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Flowers	Flowersa	k1gFnPc2	Flowersa
of	of	k?	of
Hell	Hella	k1gFnPc2	Hella
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Logic	Logic	k1gMnSc1	Logic
&	&	k?	&
Will	Will	k1gInSc1	Will
(	(	kIx(	(
<g/>
Debbi	Debbi	k1gNnSc1	Debbi
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
