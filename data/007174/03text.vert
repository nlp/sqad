<s>
Kupole	kupole	k1gFnSc1	kupole
nebo	nebo	k8xC	nebo
kopule	kopule	k1gFnSc1	kopule
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinsky	latinsky	k6eAd1	latinsky
copa	copa	k6eAd1	copa
<g/>
,	,	kIx,	,
vypouklina	vypouklina	k1gFnSc1	vypouklina
<g/>
,	,	kIx,	,
sud	sud	k1gInSc1	sud
<g/>
,	,	kIx,	,
a	a	k8xC	a
italsky	italsky	k6eAd1	italsky
cupola	cupola	k1gFnSc1	cupola
<g/>
,	,	kIx,	,
kupole	kupole	k1gFnSc1	kupole
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
sférická	sférický	k2eAgFnSc1d1	sférická
klenba	klenba	k1gFnSc1	klenba
tvořená	tvořený	k2eAgFnSc1d1	tvořená
částí	část	k1gFnSc7	část
kulové	kulový	k2eAgFnPc1d1	kulová
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
rotační	rotační	k2eAgFnPc1d1	rotační
plochy	plocha	k1gFnPc1	plocha
nad	nad	k7c7	nad
kruhovým	kruhový	k2eAgInSc7d1	kruhový
<g/>
,	,	kIx,	,
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
či	či	k8xC	či
polygonálním	polygonální	k2eAgInSc7d1	polygonální
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Půdorysem	půdorys	k1gInSc7	půdorys
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kupole	kupole	k1gFnSc2	kupole
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
elipsa	elipsa	k1gFnSc1	elipsa
nebo	nebo	k8xC	nebo
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
mnohoúhelník	mnohoúhelník	k1gInSc1	mnohoúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Používána	používán	k2eAgFnSc1d1	používána
byla	být	k5eAaImAgFnS	být
především	především	k9	především
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
antické	antický	k2eAgFnSc6d1	antická
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
byzantské	byzantský	k2eAgFnSc6d1	byzantská
<g/>
,	,	kIx,	,
islámské	islámský	k2eAgFnSc6d1	islámská
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc6d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc6d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
Výchozím	výchozí	k2eAgInSc7d1	výchozí
tvarem	tvar	k1gInSc7	tvar
kupole	kupole	k1gFnSc2	kupole
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
kulové	kulový	k2eAgFnSc2d1	kulová
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vzepětí	vzepětí	k1gNnSc4	vzepětí
kupole	kupole	k1gFnSc2	kupole
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
ji	on	k3xPp3gFnSc4	on
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
hrotitá	hrotitý	k2eAgFnSc1d1	hrotitá
<g/>
,	,	kIx,	,
či	či	k8xC	či
elipsovitá	elipsovitý	k2eAgFnSc1d1	elipsovitá
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
stlačená	stlačený	k2eAgFnSc1d1	stlačená
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
vrcholu	vrchol	k1gInSc6	vrchol
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
okulus	okulus	k1gInSc4	okulus
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
na	na	k7c4	na
okulus	okulus	k1gInSc4	okulus
nasedá	nasedat	k5eAaImIp3nS	nasedat
lucerna	lucerna	k1gFnSc1	lucerna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kupoli	kupole	k1gFnSc4	kupole
prosvětluje	prosvětlovat	k5eAaImIp3nS	prosvětlovat
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prolomena	prolomen	k2eAgFnSc1d1	prolomena
a	a	k8xC	a
osvětlována	osvětlován	k2eAgFnSc1d1	osvětlována
dalšími	další	k2eAgInPc7d1	další
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
oválnými	oválný	k2eAgInPc7d1	oválný
či	či	k8xC	či
kruhovými	kruhový	k2eAgNnPc7d1	kruhové
okénky	okénko	k1gNnPc7	okénko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prostoupena	prostoupen	k2eAgFnSc1d1	prostoupena
výsečemi	výseč	k1gFnPc7	výseč
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
může	moct	k5eAaImIp3nS	moct
nasedat	nasedat	k5eAaImF	nasedat
na	na	k7c4	na
obvodové	obvodový	k2eAgNnSc4d1	obvodové
zdivo	zdivo	k1gNnSc4	zdivo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nesena	nesen	k2eAgFnSc1d1	nesena
sloupy	sloup	k1gInPc7	sloup
a	a	k8xC	a
pasy	pas	k1gInPc7	pas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kupole	kupole	k1gFnSc1	kupole
umístěna	umístit	k5eAaPmNgFnS	umístit
nad	nad	k7c7	nad
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
či	či	k8xC	či
obdélným	obdélný	k2eAgInSc7d1	obdélný
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
převedení	převedení	k1gNnSc3	převedení
tvaru	tvar	k1gInSc2	tvar
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
klenební	klenební	k2eAgInPc1d1	klenební
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pendentivy	pendentiv	k1gInPc7	pendentiv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pendentivy	pendentiv	k1gInPc7	pendentiv
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc7d1	vlastní
kupolí	kupole	k1gFnSc7	kupole
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěn	umístit	k5eAaPmNgInS	umístit
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
tambur	tambur	k1gInSc1	tambur
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
členěný	členěný	k2eAgInSc1d1	členěný
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
klenby	klenba	k1gFnSc2	klenba
odvozené	odvozený	k2eAgFnSc2d1	odvozená
z	z	k7c2	z
kupole	kupole	k1gFnSc2	kupole
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
placková	plackový	k2eAgFnSc1d1	Placková
klenba	klenba	k1gFnSc1	klenba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tvořena	tvořit	k5eAaImNgFnS	tvořit
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
či	či	k8xC	či
obdélným	obdélný	k2eAgInSc7d1	obdélný
výsekem	výsek	k1gInSc7	výsek
kupole	kupole	k1gFnSc2	kupole
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zděná	zděný	k2eAgFnSc1d1	zděná
<g/>
,	,	kIx,	,
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
nebo	nebo	k8xC	nebo
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
litá	litý	k2eAgFnSc1d1	litá
z	z	k7c2	z
malty	malta	k1gFnSc2	malta
nebo	nebo	k8xC	nebo
betonu	beton	k1gInSc2	beton
a	a	k8xC	a
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
i	i	k9	i
ocelová	ocelový	k2eAgFnSc1d1	ocelová
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
prosklená	prosklený	k2eAgFnSc1d1	prosklená
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
zpevněna	zpevnit	k5eAaPmNgFnS	zpevnit
pasy	pas	k1gInPc7	pas
<g/>
,	,	kIx,	,
umístěnými	umístěný	k2eAgInPc7d1	umístěný
buď	buď	k8xC	buď
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
nebo	nebo	k8xC	nebo
na	na	k7c6	na
líci	líce	k1gNnSc6	líce
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
velké	velký	k2eAgFnPc1d1	velká
kupole	kupole	k1gFnPc1	kupole
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
odolávat	odolávat	k5eAaImF	odolávat
velkým	velký	k2eAgFnPc3d1	velká
silám	síla	k1gFnPc3	síla
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konstruovaly	konstruovat	k5eAaImAgFnP	konstruovat
jako	jako	k9	jako
dvojité	dvojitý	k2eAgFnPc1d1	dvojitá
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
provázané	provázaný	k2eAgNnSc1d1	provázané
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
dóm	dóm	k1gInSc1	dóm
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejjednodušší	jednoduchý	k2eAgFnSc6d3	nejjednodušší
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kupole	kupole	k1gFnSc2	kupole
setkáváme	setkávat	k5eAaImIp1nP	setkávat
u	u	k7c2	u
tradičního	tradiční	k2eAgNnSc2d1	tradiční
inuitského	inuitský	k2eAgNnSc2d1	inuitský
obydlí	obydlí	k1gNnSc2	obydlí
–	–	k?	–
iglú	iglú	k1gNnSc2	iglú
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
kamenná	kamenný	k2eAgFnSc1d1	kamenná
kupole	kupole	k1gFnSc1	kupole
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
z	z	k7c2	z
pravěkého	pravěký	k2eAgNnSc2d1	pravěké
sídliště	sídliště	k1gNnSc2	sídliště
v	v	k7c6	v
Kirokhitti	Kirokhitť	k1gFnSc6	Kirokhitť
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
6000	[number]	k4	6000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kupoli	kupole	k1gFnSc4	kupole
používala	používat	k5eAaImAgFnS	používat
též	též	k9	též
římská	římský	k2eAgFnSc1d1	římská
antika	antika	k1gFnSc1	antika
–	–	k?	–
kupole	kupole	k1gFnSc1	kupole
nad	nad	k7c7	nad
Pantheonem	Pantheon	k1gInSc7	Pantheon
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
do	do	k7c2	do
vybudování	vybudování	k1gNnSc2	vybudování
kupole	kupole	k1gFnSc2	kupole
florentského	florentský	k2eAgInSc2d1	florentský
dómu	dóm	k1gInSc2	dóm
největší	veliký	k2eAgFnSc7d3	veliký
kupolí	kupole	k1gFnSc7	kupole
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
kupole	kupole	k1gFnPc1	kupole
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
byzantské	byzantský	k2eAgFnSc2d1	byzantská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
problém	problém	k1gInSc4	problém
jejich	jejich	k3xOp3gFnSc2	jejich
stavby	stavba	k1gFnSc2	stavba
nad	nad	k7c7	nad
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
půdorysem	půdorys	k1gInSc7	půdorys
pomocí	pomoc	k1gFnPc2	pomoc
pendentivů	pendentiv	k1gInPc2	pendentiv
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnPc1	kupole
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
užívány	užívat	k5eAaImNgInP	užívat
v	v	k7c6	v
islámské	islámský	k2eAgFnSc6d1	islámská
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Hojné	hojný	k2eAgNnSc1d1	hojné
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
a	a	k8xC	a
především	především	k6eAd1	především
barokní	barokní	k2eAgFnSc6d1	barokní
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
stavebních	stavební	k2eAgInPc6d1	stavební
slozích	sloh	k1gInPc6	sloh
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
