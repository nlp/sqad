<s>
Fotosyntéza	fotosyntéza	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
chloroplastech	chloroplast	k1gInPc6
zelených	zelený	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
mnohých	mnohý	k2eAgInPc2d1
dalších	další	k2eAgInPc2d1
eukaryotických	eukaryotický	k2eAgInPc2d1
organizmů	organizmus	k1gInPc2
(	(	kIx(
<g/>
různé	různý	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
v	v	k7c6
buňkách	buňka	k1gFnPc6
sinic	sinice	k1gFnPc2
a	a	k8xC
některých	některý	k3yIgFnPc2
dalších	další	k2eAgFnPc2d1
bakterií	bakterie	k1gFnPc2
<g/>
.	.	kIx.
</s>