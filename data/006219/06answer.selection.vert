<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
na	na	k7c6	na
rumunském	rumunský	k2eAgNnSc6d1	rumunské
území	území	k1gNnSc6	území
tvoří	tvořit	k5eAaImIp3nP	tvořit
deltu	delta	k1gFnSc4	delta
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
deltu	delta	k1gFnSc4	delta
a	a	k8xC	a
biosférickou	biosférický	k2eAgFnSc4d1	biosférická
rezervaci	rezervace	k1gFnSc4	rezervace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
seznamu	seznam	k1gInSc2	seznam
Světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
13	[number]	k4	13
rumunských	rumunský	k2eAgInPc2d1	rumunský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
