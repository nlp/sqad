<s>
Fotografický	fotografický	k2eAgInSc1d1	fotografický
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
plastový	plastový	k2eAgInSc1d1	plastový
pás	pás	k1gInSc1	pás
z	z	k7c2	z
polyesteru	polyester	k1gInSc2	polyester
<g/>
,	,	kIx,	,
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
nebo	nebo	k8xC	nebo
acetátu	acetát	k1gInSc2	acetát
celulosy	celulosa	k1gFnSc2	celulosa
<g/>
,	,	kIx,	,
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
emulze	emulze	k1gFnSc2	emulze
obsahující	obsahující	k2eAgFnSc2d1	obsahující
světlocitlivé	světlocitlivý	k2eAgFnSc2d1	světlocitlivá
halogenidy	halogenida	k1gFnSc2	halogenida
stříbra	stříbro	k1gNnSc2	stříbro
vázané	vázaný	k2eAgFnPc1d1	vázaná
v	v	k7c6	v
želatině	želatina	k1gFnSc6	želatina
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
velikostí	velikost	k1gFnSc7	velikost
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
určující	určující	k2eAgFnSc4d1	určující
citlivost	citlivost	k1gFnSc4	citlivost
a	a	k8xC	a
zrnitost	zrnitost	k1gFnSc4	zrnitost
(	(	kIx(	(
<g/>
rozlišení	rozlišení	k1gNnSc4	rozlišení
<g/>
)	)	kIx)	)
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
emulze	emulze	k1gFnSc1	emulze
vystavena	vystaven	k2eAgFnSc1d1	vystavena
působení	působení	k1gNnSc4	působení
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
jako	jako	k8xC	jako
např.	např.	kA	např.
rentgen	rentgen	k1gInSc4	rentgen
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
latentní	latentní	k2eAgInSc1d1	latentní
(	(	kIx(	(
<g/>
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
<g/>
)	)	kIx)	)
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Chemickými	chemický	k2eAgInPc7d1	chemický
procesy	proces	k1gInPc7	proces
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
na	na	k7c6	na
filmu	film	k1gInSc6	film
může	moct	k5eAaImIp3nS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
obraz	obraz	k1gInSc1	obraz
viditelný	viditelný	k2eAgInSc1d1	viditelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
černobílé	černobílý	k2eAgFnSc6d1	černobílá
fotografii	fotografia	k1gFnSc6	fotografia
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
filmu	film	k1gInSc6	film
obvykle	obvykle	k6eAd1	obvykle
jedna	jeden	k4xCgFnSc1	jeden
vrstva	vrstva	k1gFnSc1	vrstva
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
vystavení	vystavení	k1gNnSc6	vystavení
světelnému	světelný	k2eAgNnSc3d1	světelné
záření	záření	k1gNnSc3	záření
se	se	k3xPyFc4	se
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
soli	sůl	k1gFnSc2	sůl
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
kovové	kovový	k2eAgNnSc4d1	kovové
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
tmavé	tmavý	k2eAgFnPc1d1	tmavá
části	část	k1gFnPc1	část
negativního	negativní	k2eAgInSc2d1	negativní
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgInPc1d1	barevný
filmy	film	k1gInPc1	film
mají	mít	k5eAaImIp3nP	mít
vrstvy	vrstva	k1gFnPc4	vrstva
minimálně	minimálně	k6eAd1	minimálně
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
solí	sůl	k1gFnPc2	sůl
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
barviva	barvivo	k1gNnSc2	barvivo
která	který	k3yRgFnSc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
citlivost	citlivost	k1gFnSc4	citlivost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
na	na	k7c4	na
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
barvy	barva	k1gFnPc4	barva
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
navrchu	navrchu	k6eAd1	navrchu
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
zelenou	zelená	k1gFnSc7	zelená
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zpracování	zpracování	k1gNnSc2	zpracování
jsou	být	k5eAaImIp3nP	být
soli	sůl	k1gFnPc1	sůl
stříbra	stříbro	k1gNnSc2	stříbro
přeměněny	přeměnit	k5eAaPmNgFnP	přeměnit
na	na	k7c4	na
kovové	kovový	k2eAgNnSc4d1	kovové
stříbro	stříbro	k1gNnSc4	stříbro
jako	jako	k8xC	jako
v	v	k7c6	v
černo-bílém	černoílý	k2eAgInSc6d1	černo-bílý
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1	kovové
stříbro	stříbro	k1gNnSc1	stříbro
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
bělícím	bělící	k2eAgFnPc3d1	bělící
procesu	proces	k1gInSc6	proces
převedeno	převést	k5eAaPmNgNnS	převést
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
filmu	film	k1gInSc2	film
odstraněny	odstraněn	k2eAgFnPc4d1	odstraněna
při	při	k7c6	při
ustalování	ustalování	k1gNnSc6	ustalování
aby	aby	kYmCp3nS	aby
dále	daleko	k6eAd2	daleko
neovlivňovaly	ovlivňovat	k5eNaImAgFnP	ovlivňovat
výsledný	výsledný	k2eAgInSc4d1	výsledný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Kodacolor	Kodacolor	k1gInSc1	Kodacolor
II	II	kA	II
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
dvanáct	dvanáct	k4xCc4	dvanáct
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
až	až	k6eAd1	až
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
různých	různý	k2eAgFnPc2d1	různá
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
fotografického	fotografický	k2eAgInSc2d1	fotografický
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
fotografická	fotografický	k2eAgFnSc1d1	fotografická
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
nebo	nebo	k8xC	nebo
cínovou	cínový	k2eAgFnSc4d1	cínová
desku	deska	k1gFnSc4	deska
byla	být	k5eAaImAgFnS	být
nanesena	nanesen	k2eAgFnSc1d1	nanesena
světlocitlivá	světlocitlivý	k2eAgFnSc1d1	světlocitlivá
emulze	emulze	k1gFnSc1	emulze
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ustálenou	ustálený	k2eAgFnSc7d1	ustálená
a	a	k8xC	a
světlu	světlo	k1gNnSc3	světlo
odolnou	odolný	k2eAgFnSc4d1	odolná
fotografii	fotografia	k1gFnSc4	fotografia
na	na	k7c6	na
cínové	cínový	k2eAgFnSc6d1	cínová
desce	deska	k1gFnSc6	deska
pořídil	pořídit	k5eAaPmAgMnS	pořídit
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépce	k1gFnSc1	Niépce
pomocí	pomoc	k1gFnSc7	pomoc
naneseného	nanesený	k2eAgInSc2d1	nanesený
citlivého	citlivý	k2eAgInSc2d1	citlivý
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
skleněné	skleněný	k2eAgFnPc4d1	skleněná
desky	deska	k1gFnPc4	deska
publikoval	publikovat	k5eAaBmAgMnS	publikovat
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
Niepce	Niepce	k1gFnSc2	Niepce
de	de	k?	de
St.	st.	kA	st.
Victor	Victor	k1gMnSc1	Victor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
negativ	negativ	k1gInSc1	negativ
na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
desku	deska	k1gFnSc4	deska
pořídil	pořídit	k5eAaPmAgMnS	pořídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
vynálezce	vynálezce	k1gMnSc1	vynálezce
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
použil	použít	k5eAaPmAgMnS	použít
pojmy	pojem	k1gInPc1	pojem
negativ	negativ	k1gInSc1	negativ
a	a	k8xC	a
pozitiv	pozitiv	k1gInSc1	pozitiv
a	a	k8xC	a
snímek	snímek	k1gInSc1	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Desky	deska	k1gFnPc1	deska
byly	být	k5eAaImAgFnP	být
opatřeny	opatřen	k2eAgMnPc4d1	opatřen
kolodiovým	kolodiový	k2eAgInSc7d1	kolodiový
povlakem	povlak	k1gInSc7	povlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
exponovat	exponovat	k5eAaImF	exponovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
probíhaly	probíhat	k5eAaImAgInP	probíhat
pokusy	pokus	k1gInPc1	pokus
objevit	objevit	k5eAaPmF	objevit
suchý	suchý	k2eAgInSc4d1	suchý
fotografický	fotografický	k2eAgInSc4d1	fotografický
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
fotografický	fotografický	k2eAgInSc4d1	fotografický
proces	proces	k1gInSc4	proces
objevem	objev	k1gInSc7	objev
suchých	suchý	k2eAgFnPc2d1	suchá
panchromatických	panchromatický	k2eAgFnPc2d1	panchromatická
desek	deska	k1gFnPc2	deska
A.	A.	kA	A.
Miethem	Mieth	k1gInSc7	Mieth
a	a	k8xC	a
A.	A.	kA	A.
Traubem	Traub	k1gInSc7	Traub
s	s	k7c7	s
kompletní	kompletní	k2eAgFnSc7d1	kompletní
a	a	k8xC	a
korektní	korektní	k2eAgFnSc7d1	korektní
škálou	škála	k1gFnSc7	škála
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
bratři	bratr	k1gMnPc1	bratr
Lumiè	Lumiè	k1gFnPc2	Lumiè
představili	představit	k5eAaPmAgMnP	představit
první	první	k4xOgFnPc4	první
autochromové	autochromový	k2eAgFnPc4d1	autochromový
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
barevným	barevný	k2eAgInSc7d1	barevný
tiskem	tisk	k1gInSc7	tisk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
deskami	deska	k1gFnPc7	deska
začali	začít	k5eAaPmAgMnP	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
například	například	k6eAd1	například
Edward	Edward	k1gMnSc1	Edward
Steichen	Steichen	k2eAgMnSc1d1	Steichen
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Henri	Henr	k1gFnSc2	Henr
Lartigue	Lartigu	k1gFnSc2	Lartigu
nebo	nebo	k8xC	nebo
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kühn	Kühn	k1gMnSc1	Kühn
<g/>
.	.	kIx.	.
</s>
<s>
Papír	papír	k1gInSc1	papír
jako	jako	k9	jako
pružný	pružný	k2eAgMnSc1d1	pružný
a	a	k8xC	a
flexibilní	flexibilní	k2eAgMnSc1d1	flexibilní
"	"	kIx"	"
<g/>
film	film	k1gInSc1	film
<g/>
"	"	kIx"	"
používal	používat	k5eAaImAgInS	používat
již	již	k6eAd1	již
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
fotografických	fotografický	k2eAgInPc2d1	fotografický
experimentů	experiment	k1gInPc2	experiment
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépka	k1gFnSc6	Niépka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
William	William	k1gInSc1	William
Fox	fox	k1gInSc4	fox
Talbot	Talbot	k1gInSc4	Talbot
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
papírových	papírový	k2eAgInPc2d1	papírový
negativů	negativ	k1gInPc2	negativ
–	–	k?	–
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
slaných	slaný	k2eAgInPc2d1	slaný
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
film	film	k1gInSc1	film
z	z	k7c2	z
celulózy	celulóza	k1gFnSc2	celulóza
jako	jako	k8xC	jako
substrátu	substrát	k1gInSc2	substrát
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
John	John	k1gMnSc1	John
Wesley	Weslea	k1gMnSc2	Weslea
Hyatt	Hyatt	k1gMnSc1	Hyatt
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
patentován	patentovat	k5eAaBmNgInS	patentovat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgInSc1d2	lepší
celuloidový	celuloidový	k2eAgInSc1d1	celuloidový
film	film	k1gInSc1	film
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
americký	americký	k2eAgMnSc1d1	americký
reverend	reverend	k1gMnSc1	reverend
Hannibal	Hannibal	k1gInSc4	Hannibal
Goodwin	Goodwin	k2eAgInSc4d1	Goodwin
pro	pro	k7c4	pro
Thomase	Thomas	k1gMnSc4	Thomas
Edisona	Edison	k1gMnSc4	Edison
a	a	k8xC	a
také	také	k9	také
jej	on	k3xPp3gInSc4	on
patentoval	patentovat	k5eAaBmAgInS	patentovat
jako	jako	k9	jako
metodu	metoda	k1gFnSc4	metoda
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
transparentního	transparentní	k2eAgInSc2d1	transparentní
pružného	pružný	k2eAgInSc2d1	pružný
svitkového	svitkový	k2eAgInSc2d1	svitkový
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
principu	princip	k1gInSc6	princip
vrstvy	vrstva	k1gFnSc2	vrstva
želatiny	želatina	k1gFnSc2	želatina
bromidu	bromid	k1gInSc2	bromid
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
na	na	k7c6	na
celuloidu	celuloid	k1gInSc6	celuloid
(	(	kIx(	(
<g/>
nitrocelulóze	nitrocelulóza	k1gFnSc6	nitrocelulóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1887	[number]	k4	1887
reverend	reverend	k1gMnSc1	reverend
Goodwin	Goodwin	k1gMnSc1	Goodwin
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
řadě	řada	k1gFnSc6	řada
pokusů	pokus	k1gInPc2	pokus
a	a	k8xC	a
testů	test	k1gInPc2	test
patent	patent	k1gInSc1	patent
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Patent	patent	k1gInSc1	patent
byl	být	k5eAaImAgInS	být
však	však	k9	však
na	na	k7c4	na
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
zdržen	zdržet	k5eAaPmNgInS	zdržet
kvůli	kvůli	k7c3	kvůli
nejasně	jasně	k6eNd1	jasně
sepsané	sepsaný	k2eAgFnSc3d1	sepsaná
a	a	k8xC	a
podané	podaný	k2eAgFnSc3d1	podaná
přihlášce	přihláška	k1gFnSc3	přihláška
patentu	patent	k1gInSc2	patent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Eastman	Eastman	k1gMnSc1	Eastman
Kodak	Kodak	kA	Kodak
uznán	uznat	k5eAaPmNgInS	uznat
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
označení	označení	k1gNnSc4	označení
Patent	patent	k1gInSc1	patent
číslo	číslo	k1gNnSc1	číslo
<g/>
:	:	kIx,	:
610861	[number]	k4	610861
<g/>
.	.	kIx.	.
</s>
<s>
George	Georgat	k5eAaPmIp3nS	Georgat
Eastman	Eastman	k1gMnSc1	Eastman
existující	existující	k2eAgInPc4d1	existující
patenty	patent	k1gInPc4	patent
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
odměně	odměna	k1gFnSc3	odměna
vyplácené	vyplácený	k2eAgFnPc1d1	vyplácená
Goodwinovi	Goodwinovi	k1gRnPc1	Goodwinovi
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Instantní	instantní	k2eAgInSc4d1	instantní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Instantní	instantní	k2eAgInSc4d1	instantní
(	(	kIx(	(
<g/>
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
<g/>
)	)	kIx)	)
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
představený	představený	k2eAgInSc4d1	představený
společností	společnost	k1gFnSc7	společnost
Polaroid	polaroid	k1gInSc1	polaroid
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
fotografie	fotografie	k1gFnSc1	fotografie
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
nebo	nebo	k8xC	nebo
minut	minuta	k1gFnPc2	minuta
od	od	k7c2	od
pořízení	pořízení	k1gNnSc2	pořízení
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
fotografického	fotografický	k2eAgInSc2d1	fotografický
přístroje	přístroj	k1gInSc2	přístroj
navrženého	navržený	k2eAgInSc2d1	navržený
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
instantním	instantní	k2eAgInSc6d1	instantní
filmu	film	k1gInSc6	film
jsou	být	k5eAaImIp3nP	být
emulze	emulze	k1gFnPc1	emulze
a	a	k8xC	a
zpracovávající	zpracovávající	k2eAgFnPc1d1	zpracovávající
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
spojeny	spojit	k5eAaPmNgFnP	spojit
v	v	k7c6	v
obsažené	obsažený	k2eAgFnSc6d1	obsažená
obálce	obálka	k1gFnSc6	obálka
nebo	nebo	k8xC	nebo
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
fotografickém	fotografický	k2eAgInSc6d1	fotografický
papíru	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
<g/>
,	,	kIx,	,
vyvolání	vyvolání	k1gNnSc1	vyvolání
i	i	k8xC	i
kopie	kopie	k1gFnSc1	kopie
na	na	k7c4	na
speciální	speciální	k2eAgInSc4d1	speciální
papír	papír	k1gInSc4	papír
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Polaroid	polaroid	k1gInSc1	polaroid
<g/>
,	,	kIx,	,
vůdčí	vůdčí	k2eAgMnSc1d1	vůdčí
výrobce	výrobce	k1gMnSc1	výrobce
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
běžnou	běžný	k2eAgFnSc4d1	běžná
emulzi	emulze	k1gFnSc4	emulze
halogenidů	halogenid	k1gMnPc2	halogenid
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
expozici	expozice	k1gFnSc6	expozice
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc6	vytvoření
negativního	negativní	k2eAgInSc2d1	negativní
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
negativ	negativ	k1gInSc1	negativ
stlačen	stlačen	k2eAgInSc1d1	stlačen
s	s	k7c7	s
fotografickým	fotografický	k2eAgInSc7d1	fotografický
papírem	papír	k1gInSc7	papír
a	a	k8xC	a
chemickými	chemický	k2eAgMnPc7d1	chemický
reaktanty	reaktant	k1gMnPc7	reaktant
<g/>
,	,	kIx,	,
závojové	závojový	k2eAgNnSc1d1	závojové
činidlo	činidlo	k1gNnSc1	činidlo
přenese	přenést	k5eAaPmIp3nS	přenést
negativní	negativní	k2eAgInSc4d1	negativní
obraz	obraz	k1gInSc4	obraz
do	do	k7c2	do
fotografického	fotografický	k2eAgInSc2d1	fotografický
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
jeho	jeho	k3xOp3gNnSc1	jeho
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
obtisk	obtisk	k1gInSc1	obtisk
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
35	[number]	k4	35
<g/>
milimetrovém	milimetrový	k2eAgInSc6d1	milimetrový
formátu	formát	k1gInSc6	formát
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
množství	množství	k1gNnSc1	množství
instantních	instantní	k2eAgInPc2d1	instantní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
černobílých	černobílý	k2eAgInPc2d1	černobílý
<g/>
,	,	kIx,	,
tak	tak	k9	tak
barevných	barevný	k2eAgFnPc2d1	barevná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Formát	formát	k1gInSc1	formát
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
135	[number]	k4	135
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Advanced	Advanced	k1gInSc1	Advanced
Photo	Photo	k1gNnSc1	Photo
System	Syst	k1gMnSc7	Syst
(	(	kIx(	(
<g/>
APS	APS	kA	APS
<g/>
)	)	kIx)	)
svitkový	svitkový	k2eAgInSc1d1	svitkový
film	film	k1gInSc1	film
110	[number]	k4	110
126	[number]	k4	126
127	[number]	k4	127
120	[number]	k4	120
<g/>
/	/	kIx~	/
<g/>
220	[number]	k4	220
(	(	kIx(	(
<g/>
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
středoformátové	středoformátový	k2eAgFnSc6d1	středoformátová
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
viz	vidět	k5eAaImRp2nS	vidět
120	[number]	k4	120
film	film	k1gInSc1	film
Deskový	deskový	k2eAgInSc4d1	deskový
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
velkoformátové	velkoformátový	k2eAgFnSc6d1	velkoformátová
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
)	)	kIx)	)
Diskový	diskový	k2eAgInSc1d1	diskový
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
formát	formát	k1gInSc1	formát
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
kotoučových	kotoučový	k2eAgInPc6d1	kotoučový
fotoaparátech	fotoaparát	k1gInPc6	fotoaparát
<g/>
)	)	kIx)	)
Film	film	k1gInSc1	film
pro	pro	k7c4	pro
kamery	kamera	k1gFnPc4	kamera
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
35	[number]	k4	35
mm	mm	kA	mm
a	a	k8xC	a
70	[number]	k4	70
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
Tabulka	tabulka	k1gFnSc1	tabulka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
