<s>
Fotografický	fotografický	k2eAgInSc1d1	fotografický
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
plastový	plastový	k2eAgInSc1d1	plastový
pás	pás	k1gInSc1	pás
z	z	k7c2	z
polyesteru	polyester	k1gInSc2	polyester
<g/>
,	,	kIx,	,
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
nebo	nebo	k8xC	nebo
acetátu	acetát	k1gInSc2	acetát
celulosy	celulosa	k1gFnSc2	celulosa
<g/>
,	,	kIx,	,
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
emulze	emulze	k1gFnSc2	emulze
obsahující	obsahující	k2eAgFnSc2d1	obsahující
světlocitlivé	světlocitlivý	k2eAgFnSc2d1	světlocitlivá
halogenidy	halogenida	k1gFnSc2	halogenida
stříbra	stříbro	k1gNnSc2	stříbro
vázané	vázaný	k2eAgFnPc1d1	vázaná
v	v	k7c6	v
želatině	želatina	k1gFnSc6	želatina
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
velikostí	velikost	k1gFnSc7	velikost
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
určující	určující	k2eAgFnSc4d1	určující
citlivost	citlivost	k1gFnSc4	citlivost
a	a	k8xC	a
zrnitost	zrnitost	k1gFnSc4	zrnitost
(	(	kIx(	(
<g/>
rozlišení	rozlišení	k1gNnSc4	rozlišení
<g/>
)	)	kIx)	)
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
