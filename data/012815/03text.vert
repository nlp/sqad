<p>
<s>
Kirtland	Kirtland	k1gInSc1	Kirtland
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
téměř	téměř	k6eAd1	téměř
7000	[number]	k4	7000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
rozlehlým	rozlehlý	k2eAgNnSc7d1	rozlehlé
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
především	především	k6eAd1	především
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
střediskem	středisko	k1gNnSc7	středisko
mormonského	mormonský	k2eAgNnSc2d1	mormonské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Josepha	Joseph	k1gMnSc2	Joseph
Smithe	Smith	k1gMnSc2	Smith
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
Komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kirtlandský	Kirtlandský	k2eAgInSc1d1	Kirtlandský
chrám	chrám	k1gInSc1	chrám
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kirtland	Kirtlanda	k1gFnPc2	Kirtlanda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Návštěvnické	návštěvnický	k2eAgNnSc1d1	návštěvnické
centrum	centrum	k1gNnSc1	centrum
Kirtland	Kirtlanda	k1gFnPc2	Kirtlanda
</s>
</p>
