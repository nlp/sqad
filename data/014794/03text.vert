<s>
Automatická	automatický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
</s>
<s>
Šestistupňová	šestistupňový	k2eAgFnSc1d1
automatická	automatický	k2eAgFnSc1d1
hydrodynamická	hydrodynamický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
ZF	ZF	kA
6HP26	6HP26	k4
</s>
<s>
Automobilní	automobilní	k2eAgFnSc1d1
automatická	automatický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
je	být	k5eAaImIp3nS
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
při	při	k7c6
jízdě	jízda	k1gFnSc6
samočinně	samočinně	k6eAd1
mění	měnit	k5eAaImIp3nP
převodové	převodový	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
(	(	kIx(
<g/>
převodový	převodový	k2eAgInSc4d1
poměr	poměr	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnPc1
má	mít	k5eAaImIp3nS
v	v	k7c6
automobilu	automobil	k1gInSc6
tři	tři	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Znásobit	znásobit	k5eAaPmF
a	a	k8xC
přenést	přenést	k5eAaPmF
točivý	točivý	k2eAgInSc4d1
moment	moment	k1gInSc4
od	od	k7c2
spalovacího	spalovací	k2eAgInSc2d1
motoru	motor	k1gInSc2
ke	k	k7c3
kolům	kolo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Zajistit	zajistit	k5eAaPmF
rozjezd	rozjezd	k1gInSc4
vozidla	vozidlo	k1gNnSc2
bez	bez	k7c2
ovládání	ovládání	k1gNnSc2
spojky	spojka	k1gFnSc2
<g/>
,	,	kIx,
pouhým	pouhý	k2eAgNnSc7d1
zmáčknutím	zmáčknutí	k1gNnSc7
plynu	plyn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vhodně	vhodně	k6eAd1
zvolit	zvolit	k5eAaPmF
a	a	k8xC
zařadit	zařadit	k5eAaPmF
potřebný	potřebný	k2eAgInSc4d1
převodový	převodový	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
automatických	automatický	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
dále	daleko	k6eAd2
umožňuje	umožňovat	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
vozidlo	vozidlo	k1gNnSc1
proti	proti	k7c3
pohybu	pohyb	k1gInSc2
parkovací	parkovací	k2eAgNnSc4d1
západkou	západka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
aktivována	aktivovat	k5eAaBmNgFnS
zařazením	zařazení	k1gNnSc7
polohy	poloha	k1gFnSc2
P	P	kA
na	na	k7c6
volící	volící	k2eAgFnSc6d1
páce	páka	k1gFnSc6
v	v	k7c6
kabině	kabina	k1gFnSc6
řidiče	řidič	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
automatických	automatický	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
</s>
<s>
Existují	existovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
druhy	druh	k1gInPc1
automatických	automatický	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Hydrodynamická	hydrodynamický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
s	s	k7c7
hydrodynamickým	hydrodynamický	k2eAgInSc7d1
měničem	měnič	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
převodovek	převodovka	k1gFnPc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
standardní	standardní	k2eAgFnSc4d1
<g/>
,	,	kIx,
klasickou	klasický	k2eAgFnSc4d1
automatickou	automatický	k2eAgFnSc4d1
převodovku	převodovka	k1gFnSc4
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInPc1d1
druhy	druh	k1gInPc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
snaží	snažit	k5eAaImIp3nS
svým	svůj	k3xOyFgNnSc7
chováním	chování	k1gNnSc7
přiblížit	přiblížit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Dvojspojková	dvojspojkový	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volkswagen	volkswagen	k1gInSc1
Group	Group	k1gInSc1
ji	on	k3xPp3gFnSc4
prodává	prodávat	k5eAaImIp3nS
pod	pod	k7c7
obchodním	obchodní	k2eAgInSc7d1
názvem	název	k1gInSc7
DSG	DSG	kA
<g/>
.	.	kIx.
</s>
<s>
Automatizovaná	automatizovaný	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
funkce	funkce	k1gFnSc2
normální	normální	k2eAgFnSc1d1
manuální	manuální	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
automatika	automatika	k1gFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
ovládání	ovládání	k1gNnSc4
spojky	spojka	k1gFnSc2
a	a	k8xC
přeřazování	přeřazování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
základní	základní	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
neumožňuje	umožňovat	k5eNaImIp3nS
řazení	řazení	k1gNnSc1
za	za	k7c2
plného	plný	k2eAgInSc2d1
tahu	tah	k1gInSc2
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Variátor	variátor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
převodovek	převodovka	k1gFnPc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
samostatnou	samostatný	k2eAgFnSc7d1
kapitolou	kapitola	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
všechny	všechen	k3xTgInPc4
předešlé	předešlý	k2eAgInPc4d1
druhy	druh	k1gInPc4
mají	mít	k5eAaImIp3nP
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
manuální	manuální	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
několik	několik	k4yIc1
převodových	převodový	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
převodovka	převodovka	k1gFnSc1
samočinně	samočinně	k6eAd1
řadí	řadit	k5eAaImIp3nS
<g/>
,	,	kIx,
variátor	variátor	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
plynulou	plynulý	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
převodového	převodový	k2eAgInSc2d1
poměru	poměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využití	využití	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
zejména	zejména	k9
ve	v	k7c6
skútrech	skútr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Ovládání	ovládání	k1gNnSc1
automatické	automatický	k2eAgFnSc2d1
převodovky	převodovka	k1gFnSc2
</s>
<s>
Volicí	volicí	k2eAgFnSc1d1
páka	páka	k1gFnSc1
automatické	automatický	k2eAgFnSc2d1
převodovky	převodovka	k1gFnSc2
</s>
<s>
Řidič	řidič	k1gMnSc1
při	při	k7c6
rozjíždění	rozjíždění	k1gNnSc6
nepotřebuje	potřebovat	k5eNaImIp3nS
ovládat	ovládat	k5eAaImF
spojku	spojka	k1gFnSc4
(	(	kIx(
<g/>
automobily	automobil	k1gInPc1
s	s	k7c7
automatickou	automatický	k2eAgFnSc7d1
převodovkou	převodovka	k1gFnSc7
nejsou	být	k5eNaImIp3nP
vybaveny	vybavit	k5eAaPmNgFnP
pedálem	pedál	k1gInSc7
spojky	spojka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
automatických	automatický	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
se	se	k3xPyFc4
ovládá	ovládat	k5eAaImIp3nS
volící	volící	k2eAgFnSc7d1
pákou	páka	k1gFnSc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
polohy	poloha	k1gFnSc2
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
D	D	kA
–	–	k?
Drive	drive	k1gInSc1
–	–	k?
jízda	jízda	k1gFnSc1
vpřed	vpřed	k6eAd1
</s>
<s>
R	R	kA
–	–	k?
Reverse	reverse	k1gFnSc1
–	–	k?
jízda	jízda	k1gFnSc1
vzad	vzad	k6eAd1
(	(	kIx(
<g/>
zpátečka	zpátečka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
N	N	kA
–	–	k?
Neutrál	neutrál	k1gInSc1
–	–	k?
při	při	k7c6
normální	normální	k2eAgFnSc6d1
jízdě	jízda	k1gFnSc6
není	být	k5eNaImIp3nS
nutno	nutno	k6eAd1
používat	používat	k5eAaImF
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
vyřazení	vyřazení	k1gNnSc3
převodovky	převodovka	k1gFnSc2
z	z	k7c2
činnosti	činnost	k1gFnSc2
například	například	k6eAd1
při	při	k7c6
stání	stání	k1gNnSc6
v	v	k7c6
koloně	kolona	k1gFnSc6
se	s	k7c7
zapnutým	zapnutý	k2eAgInSc7d1
motorem	motor	k1gInSc7
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
zatažení	zatažení	k1gNnSc1
ruční	ruční	k2eAgFnSc2d1
brzdy	brzda	k1gFnSc2
a	a	k8xC
uvolnění	uvolnění	k1gNnSc4
pedálu	pedál	k1gInSc2
brzdy	brzda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
P	P	kA
–	–	k?
Park	park	k1gInSc1
–	–	k?
parkovací	parkovací	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
<g/>
,	,	kIx,
zařazení	zařazení	k1gNnSc1
parkovací	parkovací	k2eAgFnSc2d1
západky	západka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
manuální	manuální	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
–	–	k?
u	u	k7c2
různých	různý	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
je	být	k5eAaImIp3nS
řešeno	řešit	k5eAaImNgNnS
různě	různě	k6eAd1
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
sekvenční	sekvenční	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
převodových	převodový	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
pro	pro	k7c4
jízdu	jízda	k1gFnSc4
vpřed	vpřed	k6eAd1
podle	podle	k7c2
volby	volba	k1gFnSc2
řidiče	řidič	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
páce	páka	k1gFnSc6
je	být	k5eAaImIp3nS
tlačítko	tlačítko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
zmáčknout	zmáčknout	k5eAaPmF
při	při	k7c6
některých	některý	k3yIgFnPc6
změnách	změna	k1gFnPc6
polohy	poloha	k1gFnSc2
páky	páka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabraňuje	zabraňovat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
neúmyslným	úmyslný	k2eNgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pro	pro	k7c4
převodovku	převodovka	k1gFnSc4
fatální	fatální	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
zařazení	zařazení	k1gNnSc3
zpátečky	zpátečka	k1gFnSc2
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jízda	jízda	k1gFnSc1
s	s	k7c7
automatickou	automatický	k2eAgFnSc7d1
převodovkou	převodovka	k1gFnSc7
</s>
<s>
Některé	některý	k3yIgInPc4
vozy	vůz	k1gInPc4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
nastartovat	nastartovat	k5eAaPmF
<g/>
,	,	kIx,
jen	jen	k9
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
poloha	poloha	k1gFnSc1
volící	volící	k2eAgFnSc1d1
páky	páka	k1gFnSc2
nastavená	nastavený	k2eAgFnSc1d1
na	na	k7c6
P.	P.	kA
</s>
<s>
U	u	k7c2
některých	některý	k3yIgInPc2
vozů	vůz	k1gInPc2
nelze	lze	k6eNd1
po	po	k7c6
nastartování	nastartování	k1gNnSc6
volící	volící	k2eAgFnSc4d1
páku	páka	k1gFnSc4
přesunout	přesunout	k5eAaPmF
z	z	k7c2
polohy	poloha	k1gFnSc2
P	P	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
současně	současně	k6eAd1
zmáčknut	zmáčknut	k2eAgInSc4d1
pedál	pedál	k1gInSc4
brzdy	brzda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
automatických	automatický	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
po	po	k7c6
zařazení	zařazení	k1gNnSc6
jiné	jiný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
než	než	k8xS
P	P	kA
nebo	nebo	k8xC
N	N	kA
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
plazivému	plazivý	k2eAgInSc3d1
pohybu	pohyb	k1gInSc3
vozu	vůz	k1gInSc2
–	–	k?
na	na	k7c6
rovině	rovina	k1gFnSc6
se	se	k3xPyFc4
vůz	vůz	k1gInSc1
sám	sám	k3xTgInSc4
pomalu	pomalu	k6eAd1
rozjede	rozjet	k5eAaPmIp3nS
a	a	k8xC
pohybuje	pohybovat	k5eAaImIp3nS
se	se	k3xPyFc4
vpřed	vpřed	k6eAd1
přibližně	přibližně	k6eAd1
rychlostí	rychlost	k1gFnSc7
chůze	chůze	k1gFnSc2
<g/>
,	,	kIx,
bez	bez	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
řidič	řidič	k1gMnSc1
přidal	přidat	k5eAaPmAgMnS
plyn	plyn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
chceme	chtít	k5eAaImIp1nP
skutečně	skutečně	k6eAd1
stát	stát	k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
zácpě	zácpa	k1gFnSc6
nebo	nebo	k8xC
na	na	k7c6
křižovatce	křižovatka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neustále	neustále	k6eAd1
držet	držet	k5eAaImF
stlačený	stlačený	k2eAgInSc4d1
pedál	pedál	k1gInSc4
brzdy	brzda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kickdown	Kickdown	k1gNnSc1
mód	móda	k1gFnPc2
</s>
<s>
Při	při	k7c6
potřebě	potřeba	k1gFnSc6
prudkého	prudký	k2eAgNnSc2d1
zrychlení	zrychlení	k1gNnSc2
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
při	při	k7c6
předjíždění	předjíždění	k1gNnSc6
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
u	u	k7c2
automatické	automatický	k2eAgFnSc2d1
převodovky	převodovka	k1gFnSc2
ke	k	k7c3
slovu	slovo	k1gNnSc3
tzv.	tzv.	kA
"	"	kIx"
<g/>
Kickdown	Kickdown	k1gInSc1
efekt	efekt	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
vyvolán	vyvolat	k5eAaPmNgInS
stlačením	stlačení	k1gNnSc7
pedálu	pedál	k1gInSc2
plynu	plyn	k1gInSc2
zcela	zcela	k6eAd1
na	na	k7c4
podlahu	podlaha	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
podřazen	podřazen	k2eAgInSc4d1
nižší	nízký	k2eAgInSc4d2
převodový	převodový	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
(	(	kIx(
<g/>
například	například	k6eAd1
ze	z	k7c2
čtvrtého	čtvrtý	k4xOgInSc2
stupně	stupeň	k1gInSc2
na	na	k7c4
třetí	třetí	k4xOgNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozidlo	vozidlo	k1gNnSc1
pak	pak	k6eAd1
prudce	prudko	k6eAd1
zrychluje	zrychlovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnSc1d1
elektronicky	elektronicky	k6eAd1
řízené	řízený	k2eAgFnPc4d1
automatické	automatický	k2eAgFnPc4d1
převodovky	převodovka	k1gFnPc4
umožňují	umožňovat	k5eAaImIp3nP
podřazení	podřazení	k1gNnSc4
i	i	k9
o	o	k7c4
několik	několik	k4yIc4
rychlostních	rychlostní	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1
mechanické	mechanický	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
s	s	k7c7
řadicí	řadicí	k2eAgFnSc7d1
pákou	páka	k1gFnSc7
a	a	k8xC
nutností	nutnost	k1gFnSc7
citlivě	citlivě	k6eAd1
ovládat	ovládat	k5eAaImF
spojkový	spojkový	k2eAgInSc4d1
pedál	pedál	k1gInSc4
u	u	k7c2
mnoha	mnoho	k4c2
vozidel	vozidlo	k1gNnPc2
nevyhovovalo	vyhovovat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
žádána	žádán	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
bude	být	k5eAaImBp3nS
fungovat	fungovat	k5eAaImF
samočinně	samočinně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
krok	krok	k1gInSc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
Hermann	Hermann	k1gMnSc1
Föttinger	Föttinger	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vynalezl	vynaleznout	k5eAaPmAgMnS
hydrodynamický	hydrodynamický	k2eAgInSc4d1
měnič	měnič	k1gInSc4
<g/>
,	,	kIx,
základ	základ	k1gInSc4
hydrodynamické	hydrodynamický	k2eAgFnSc2d1
automatické	automatický	k2eAgFnSc2d1
převodovky	převodovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
se	se	k3xPyFc4
v	v	k7c6
osobních	osobní	k2eAgInPc6d1
automobilech	automobil	k1gInPc6
tento	tento	k3xDgMnSc1
druh	druh	k1gInSc4
rozšířil	rozšířit	k5eAaPmAgMnS
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
má	mít	k5eAaImIp3nS
samočinná	samočinný	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
v	v	k7c6
USA	USA	kA
zastoupení	zastoupení	k1gNnSc1
okolo	okolo	k7c2
90	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
mají	mít	k5eAaImIp3nP
osobní	osobní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
převážně	převážně	k6eAd1
manuální	manuální	k2eAgFnPc4d1
převodovky	převodovka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Automatické	automatický	k2eAgFnPc1d1
převodovky	převodovka	k1gFnPc1
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
uplatňují	uplatňovat	k5eAaImIp3nP
u	u	k7c2
městských	městský	k2eAgInPc2d1
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
hasičů	hasič	k1gMnPc2
<g/>
,	,	kIx,
autojeřábů	autojeřáb	k1gInPc2
<g/>
,	,	kIx,
popelářských	popelářský	k2eAgInPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
u	u	k7c2
motorových	motorový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
na	na	k7c6
železnici	železnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
jsou	být	k5eAaImIp3nP
však	však	k9
hydromechanické	hydromechanický	k2eAgFnPc4d1
převodovky	převodovka	k1gFnPc4
stále	stále	k6eAd1
častěji	často	k6eAd2
nahrazovány	nahrazován	k2eAgInPc1d1
zejména	zejména	k9
dvouspojkovými	dvouspojkový	k2eAgFnPc7d1
převodovkami	převodovka	k1gFnPc7
(	(	kIx(
<g/>
DSG	DSG	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
též	též	k9
automatizovanými	automatizovaný	k2eAgFnPc7d1
převodovkami	převodovka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
principy	princip	k1gInPc1
těchto	tento	k3xDgFnPc2
převodovek	převodovka	k1gFnPc2
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
velmi	velmi	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
konstrukčně	konstrukčně	k6eAd1
jsou	být	k5eAaImIp3nP
jednodušší	jednoduchý	k2eAgInPc1d2
a	a	k8xC
výrobně	výrobně	k6eAd1
lacinější	laciný	k2eAgInPc1d2
a	a	k8xC
s	s	k7c7
nižším	nízký	k2eAgInSc7d2
počtem	počet	k1gInSc7
převodových	převodový	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
umožňují	umožňovat	k5eAaImIp3nP
dosahovat	dosahovat	k5eAaImF
vyšší	vysoký	k2eAgFnPc4d2
účinnosti	účinnost	k1gFnPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
i	i	k9
menší	malý	k2eAgFnSc2d2
spotřeby	spotřeba	k1gFnSc2
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
hydromechanické	hydromechanický	k2eAgFnSc2d1
převodovky	převodovka	k1gFnSc2
vyžadují	vyžadovat	k5eAaImIp3nP
náročnější	náročný	k2eAgNnSc4d2
elektronické	elektronický	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
jejich	jejich	k3xOp3gNnSc4
rozšíření	rozšíření	k1gNnSc4
do	do	k7c2
osobních	osobní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
je	být	k5eAaImIp3nS
umožněno	umožnit	k5eAaPmNgNnS
až	až	k9
s	s	k7c7
příchodem	příchod	k1gInSc7
elektronických	elektronický	k2eAgFnPc2d1
řídících	řídící	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
počítačů	počítač	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nejčastější	častý	k2eAgFnPc1d3
příčiny	příčina	k1gFnPc1
poruch	porucha	k1gFnPc2
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1
údržba	údržba	k1gFnSc1
automobilu	automobil	k1gInSc2
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
nesprávný	správný	k2eNgInSc4d1
chod	chod	k1gInSc4
převodovky	převodovka	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc4
poruchu	porucha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejčastější	častý	k2eAgFnPc4d3
příčiny	příčina	k1gFnPc4
patří	patřit	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
nedodržování	nedodržování	k1gNnSc1
intervalu	interval	k1gInSc2
výměny	výměna	k1gFnSc2
oleje	olej	k1gInSc2
</s>
<s>
nekvalitní	kvalitní	k2eNgNnSc1d1
chlazení	chlazení	k1gNnSc1
oleje	olej	k1gInSc2
<g/>
,	,	kIx,
způsobuje	způsobovat	k5eAaImIp3nS
ztrátu	ztráta	k1gFnSc4
jeho	jeho	k3xOp3gFnPc2
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgNnPc6,k3yRgNnPc6,k3yIgNnPc6
je	být	k5eAaImIp3nS
správný	správný	k2eAgInSc1d1
chod	chod	k1gInSc1
převodovek	převodovka	k1gFnPc2
založen	založit	k5eAaPmNgInS
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.foxnews.com	www.foxnews.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Diferenciál	diferenciál	k1gInSc1
(	(	kIx(
<g/>
mechanika	mechanika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
přenos	přenos	k1gInSc1
výkonu	výkon	k1gInSc2
</s>
<s>
Nápravová	nápravový	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
</s>
<s>
Planetová	planetový	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
</s>
<s>
Reduktor	reduktor	k1gInSc1
</s>
<s>
Rozvodovka	rozvodovka	k1gFnSc1
</s>
<s>
Řadicí	řadicí	k2eAgFnSc1d1
páka	páka	k1gFnSc1
</s>
<s>
Synchronizovaná	synchronizovaný	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
automatická	automatický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Proč	proč	k6eAd1
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
zvolit	zvolit	k5eAaPmF
automatickou	automatický	k2eAgFnSc4d1
převodovku	převodovka	k1gFnSc4
</s>
<s>
Proč	proč	k6eAd1
stále	stále	k6eAd1
odmítáme	odmítat	k5eAaImIp1nP
automatickou	automatický	k2eAgFnSc4d1
převodovku	převodovka	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
poslední	poslední	k2eAgInSc1d1
vzdor	vzdor	k1gInSc1
vůči	vůči	k7c3
moderní	moderní	k2eAgFnSc3d1
civilizaci	civilizace	k1gFnSc3
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4153580-7	4153580-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85010407	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85010407	#num#	k4
</s>
