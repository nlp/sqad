<s>
Automobilní	automobilní	k2eAgFnSc1d1
automatická	automatický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
je	být	k5eAaImIp3nS
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
při	při	k7c6
jízdě	jízda	k1gFnSc6
samočinně	samočinně	k6eAd1
mění	měnit	k5eAaImIp3nP
převodové	převodový	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
(	(	kIx(
<g/>
převodový	převodový	k2eAgInSc4d1
poměr	poměr	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>