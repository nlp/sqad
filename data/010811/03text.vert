<p>
<s>
Nathan	Nathan	k1gMnSc1	Nathan
Oystrick	Oystrick	k1gMnSc1	Oystrick
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Regina	Regina	k1gFnSc1	Regina
<g/>
,	,	kIx,	,
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
lední	lední	k2eAgMnSc1d1	lední
hokejista	hokejista	k1gMnSc1	hokejista
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Elmira	Elmiro	k1gNnSc2	Elmiro
Jackals	Jackalsa	k1gFnPc2	Jackalsa
v	v	k7c6	v
ECHL	ECHL	kA	ECHL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
ho	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
198	[number]	k4	198
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
draftoval	draftovat	k5eAaImAgMnS	draftovat
tým	tým	k1gInSc4	tým
Atlanta	Atlant	k1gMnSc2	Atlant
Thrashers	Thrashersa	k1gFnPc2	Thrashersa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
podepsal	podepsat	k5eAaPmAgMnS	podepsat
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
přeřazen	přeřadit	k5eAaPmNgInS	přeřadit
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třetí	třetí	k4xOgFnSc6	třetí
sezóně	sezóna	k1gFnSc6	sezóna
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
AHL	AHL	kA	AHL
Chicago	Chicago	k1gNnSc4	Chicago
Wolves	Wolves	k1gInSc1	Wolves
slavil	slavit	k5eAaImAgInS	slavit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
výhru	výhra	k1gFnSc4	výhra
Calder	Calder	k1gInSc4	Calder
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
sezoně	sezona	k1gFnSc6	sezona
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
týmu	tým	k1gInSc2	tým
Atlanty	Atlanta	k1gFnSc2	Atlanta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
odehrál	odehrát	k5eAaPmAgMnS	odehrát
53	[number]	k4	53
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc2	který
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
4	[number]	k4	4
góly	gól	k1gInPc4	gól
a	a	k8xC	a
8	[number]	k4	8
asistencí	asistence	k1gFnPc2	asistence
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
v	v	k7c6	v
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
Wolves	Wolvesa	k1gFnPc2	Wolvesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
za	za	k7c4	za
Jevgenije	Jevgenije	k1gFnSc4	Jevgenije
Arťuchina	Arťuchino	k1gNnSc2	Arťuchino
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Anaheim	Anaheima	k1gFnPc2	Anaheima
Ducks	Ducksa	k1gFnPc2	Ducksa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
odehrál	odehrát	k5eAaPmAgMnS	odehrát
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jednoletou	jednoletý	k2eAgFnSc4d1	jednoletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
Blues	blues	k1gNnSc1	blues
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
ale	ale	k9	ale
natrvalo	natrvalo	k6eAd1	natrvalo
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
a	a	k8xC	a
stihl	stihnout	k5eAaPmAgMnS	stihnout
odehrát	odehrát	k5eAaPmF	odehrát
pouze	pouze	k6eAd1	pouze
devět	devět	k4xCc4	devět
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
jedné	jeden	k4xCgFnSc2	jeden
branky	branka	k1gFnSc2	branka
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
asistencí	asistence	k1gFnPc2	asistence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
opět	opět	k6eAd1	opět
měnil	měnit	k5eAaImAgInS	měnit
působiště	působiště	k1gNnPc4	působiště
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
upsal	upsat	k5eAaPmAgMnS	upsat
Phoenixu	Phoenix	k1gInSc6	Phoenix
Coyotes	Coyotes	k1gInSc1	Coyotes
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
strávil	strávit	k5eAaPmAgInS	strávit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
jedenáctkrát	jedenáctkrát	k6eAd1	jedenáctkrát
skórovat	skórovat	k5eAaBmF	skórovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
ročníkem	ročník	k1gInSc7	ročník
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
se	se	k3xPyFc4	se
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
posílil	posílit	k5eAaPmAgInS	posílit
nový	nový	k2eAgInSc1d1	nový
tým	tým	k1gInSc1	tým
Kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
důležitým	důležitý	k2eAgMnSc7d1	důležitý
hráčem	hráč	k1gMnSc7	hráč
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
odehrál	odehrát	k5eAaPmAgMnS	odehrát
43	[number]	k4	43
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
devět	devět	k4xCc4	devět
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
tři	tři	k4xCgFnPc4	tři
branky	branka	k1gFnPc4	branka
a	a	k8xC	a
šest	šest	k4xCc4	šest
asistencí	asistence	k1gFnPc2	asistence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oystrick	Oystrick	k1gMnSc1	Oystrick
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
Lvu	lev	k1gInSc6	lev
a	a	k8xC	a
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
asistentem	asistent	k1gMnSc7	asistent
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubové	klubový	k2eAgFnPc1d1	klubová
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Nathan	Nathan	k1gMnSc1	Nathan
Oystrick	Oystrick	k1gMnSc1	Oystrick
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nathan	Nathan	k1gMnSc1	Nathan
Oystrick	Oystrick	k1gMnSc1	Oystrick
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Levpraha	Levprah	k1gMnSc4	Levprah
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
