<s>
Ústava	ústava	k1gFnSc1	ústava
pak	pak	k6eAd1	pak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
ratifikaci	ratifikace	k1gFnSc6	ratifikace
zakládajícími	zakládající	k2eAgFnPc7d1	zakládající
dvaceti	dvacet	k4xCc7	dvacet
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
