<s>
Špagety	špagety	k1gFnPc4	špagety
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
doma	doma	k6eAd1	doma
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
smícháním	smíchání	k1gNnSc7	smíchání
jednoho	jeden	k4xCgInSc2	jeden
žloutku	žloutek	k1gInSc2	žloutek
s	s	k7c7	s
cca	cca	kA	cca
90	[number]	k4	90
g	g	kA	g
hrubé	hrubý	k2eAgFnSc2d1	hrubá
mouky	mouka	k1gFnSc2	mouka
a	a	k8xC	a
špetkou	špetka	k1gFnSc7	špetka
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
vyválet	vyválet	k5eAaPmF	vyválet
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
mm	mm	kA	mm
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
uschnout	uschnout	k5eAaPmF	uschnout
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
nakrájet	nakrájet	k5eAaPmF	nakrájet
nožem	nůž	k1gInSc7	nůž
nebo	nebo	k8xC	nebo
strojkem	strojek	k1gInSc7	strojek
<g/>
.	.	kIx.	.
</s>
