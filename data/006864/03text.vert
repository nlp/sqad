<s>
Anna	Anna	k1gFnSc1	Anna
Nechybová	chybový	k2eNgFnSc1d1	Nechybová
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1909	[number]	k4	1909
–	–	k?	–
???	???	k?	???
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
poúnorová	poúnorový	k2eAgFnSc1d1	poúnorová
bezpartijní	bezpartijní	k2eAgFnSc1d1	bezpartijní
poslankyně	poslankyně	k1gFnSc1	poslankyně
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
obvodu	obvod	k1gInSc6	obvod
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
jako	jako	k8xC	jako
bezpartijní	bezpartijní	k2eAgFnSc1d1	bezpartijní
poslankyně	poslankyně	k1gFnSc1	poslankyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
setrvala	setrvat	k5eAaPmAgFnS	setrvat
do	do	k7c2	do
března	březen	k1gInSc2	březen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Svatoslava	Svatoslava	k1gFnSc1	Svatoslava
Kernerová	Kernerová	k1gFnSc1	Kernerová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
profesně	profesně	k6eAd1	profesně
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xS	jako
dělnice	dělnice	k1gFnPc1	dělnice
v	v	k7c6	v
Severočeských	severočeský	k2eAgInPc6d1	severočeský
tukových	tukový	k2eAgInPc6d1	tukový
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
