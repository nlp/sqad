<s>
Britové	Brit	k1gMnPc1	Brit
jsou	být	k5eAaImIp3nP	být
občané	občan	k1gMnPc1	občan
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc2	jeho
zámořských	zámořský	k2eAgNnPc2d1	zámořské
území	území	k1gNnPc2	území
a	a	k8xC	a
korunních	korunní	k2eAgNnPc2d1	korunní
závislých	závislý	k2eAgNnPc2d1	závislé
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
spojením	spojení	k1gNnSc7	spojení
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byly	být	k5eAaImAgFnP	být
položeny	položit	k5eAaPmNgInP	položit
základy	základ	k1gInPc1	základ
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
společné	společný	k2eAgFnSc2d1	společná
britské	britský	k2eAgFnSc2d1	britská
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
bylo	být	k5eAaImAgNnS	být
ke	k	k7c3	k
království	království	k1gNnSc3	království
připojeno	připojit	k5eAaPmNgNnS	připojit
i	i	k9	i
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
oddělila	oddělit	k5eAaPmAgFnS	oddělit
v	v	k7c4	v
Irskou	irský	k2eAgFnSc4d1	irská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
identita	identita	k1gFnSc1	identita
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
je	být	k5eAaImIp3nS	být
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
kvůli	kvůli	k7c3	kvůli
etnicko-sektářským	etnickoektářský	k2eAgFnPc3d1	etnicko-sektářský
třenicím	třenice	k1gFnPc3	třenice
-	-	kIx~	-
protestanté	protestant	k1gMnPc1	protestant
(	(	kIx(	(
<g/>
unionisté	unionista	k1gMnPc1	unionista
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
za	za	k7c4	za
Brity	Brit	k1gMnPc4	Brit
považují	považovat	k5eAaImIp3nP	považovat
<g/>
,	,	kIx,	,
katolíci	katolík	k1gMnPc1	katolík
většinou	většinou	k6eAd1	většinou
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
postihuje	postihovat	k5eAaImIp3nS	postihovat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
multietnické	multietnický	k2eAgFnSc2d1	multietnická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
za	za	k7c4	za
Brity	Brit	k1gMnPc4	Brit
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
jen	jen	k6eAd1	jen
původní	původní	k2eAgNnPc1d1	původní
etnika	etnikum	k1gNnPc1	etnikum
-	-	kIx~	-
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Velšané	Velšan	k1gMnPc1	Velšan
<g/>
,	,	kIx,	,
Skoti	Skot	k1gMnPc1	Skot
a	a	k8xC	a
Irové	Ir	k1gMnPc1	Ir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
množství	množství	k1gNnSc3	množství
jiných	jiný	k2eAgFnPc2d1	jiná
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
místo	místo	k1gNnSc4	místo
označení	označený	k2eAgMnPc1d1	označený
Britové	Brit	k1gMnPc1	Brit
chybně	chybně	k6eAd1	chybně
používáno	používán	k2eAgNnSc4d1	používáno
označení	označení	k1gNnSc4	označení
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
nebo	nebo	k8xC	nebo
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
bývá	bývat	k5eAaImIp3nS	bývat
chybně	chybně	k6eAd1	chybně
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
o	o	k7c4	o
podobnou	podobný	k2eAgFnSc4d1	podobná
chybu	chyba	k1gFnSc4	chyba
jako	jako	k8xC	jako
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
názvu	název	k1gInSc2	název
Holandsko	Holandsko	k1gNnSc4	Holandsko
namísto	namísto	k7c2	namísto
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Holanďané	Holanďan	k1gMnPc1	Holanďan
namísto	namísto	k7c2	namísto
Nizozemci	Nizozemec	k1gMnSc6	Nizozemec
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
při	při	k7c6	při
zaměňování	zaměňování	k1gNnSc6	zaměňování
názvů	název	k1gInPc2	název
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Česko	Česko	k1gNnSc1	Česko
<g/>
/	/	kIx~	/
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Čechy	Čechy	k1gFnPc4	Čechy
versus	versus	k7c1	versus
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Britové	Brit	k1gMnPc1	Brit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Brit	Brit	k1gMnSc1	Brit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
