<s>
Vztah	vztah	k1gInSc1
mezi	mezi	k7c7
délkami	délka	k1gFnPc7
stran	strana	k1gFnPc2
jakých	jaký	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
trojúhelniků	trojúhelnik	k1gInPc2
popisuje	popisovat	k5eAaImIp3nS
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
v	v	k7c6
euklidovské	euklidovský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
?	?	kIx.
</s>