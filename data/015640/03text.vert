<s>
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
formátování	formátování	k1gNnSc1
</s>
<s>
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
popisuje	popisovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
platí	platit	k5eAaImIp3nS
mezi	mezi	k7c7
délkami	délka	k1gFnPc7
stran	stran	k7c2
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
v	v	k7c6
euklidovské	euklidovský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
dopočítat	dopočítat	k5eAaPmF
délku	délka	k1gFnSc4
třetí	třetí	k4xOgFnSc2
strany	strana	k1gFnSc2
takového	takový	k3xDgInSc2
trojúhelníka	trojúhelník	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
délky	délka	k1gFnPc1
dvou	dva	k4xCgFnPc2
zbývajících	zbývající	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Věta	věta	k1gFnSc1
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
Obsah	obsah	k1gInSc1
čtverce	čtverec	k1gInSc2
sestrojeného	sestrojený	k2eAgMnSc4d1
nad	nad	k7c7
přeponou	přepona	k1gFnSc7
libovolného	libovolný	k2eAgInSc2d1
pravoúhlého	pravoúhlý	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
je	být	k5eAaImIp3nS
roven	roven	k2eAgInSc1d1
součtu	součet	k1gInSc3
obsahů	obsah	k1gInPc2
čtverců	čtverec	k1gInPc2
nad	nad	k7c7
oběma	dva	k4xCgMnPc7
jeho	jeho	k3xOp3gFnPc7
odvěsnami	odvěsna	k1gFnPc7
(	(	kIx(
<g/>
dvěma	dva	k4xCgFnPc7
kratšími	krátký	k2eAgFnPc7d2
stranami	strana	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Formálně	formálně	k6eAd1
Pythagorovu	Pythagorův	k2eAgFnSc4d1
větu	věta	k1gFnSc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
rovnice	rovnice	k1gFnSc1
</s>
<s>
c²	c²	k?
=	=	kIx~
a²	a²	k?
+	+	kIx~
b²	b²	k?
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
označuje	označovat	k5eAaImIp3nS
délku	délka	k1gFnSc4
přepony	přepona	k1gFnSc2
pravoúhlého	pravoúhlý	k2eAgInSc2d1
trojúhelníka	trojúhelník	k1gInSc2
a	a	k8xC
délky	délka	k1gFnSc2
odvěsen	odvěsna	k1gFnPc2
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
:	:	kIx,
Součet	součet	k1gInSc1
obsahů	obsah	k1gInPc2
čtverců	čtverec	k1gInPc2
nad	nad	k7c7
odvěsnami	odvěsna	k1gFnPc7
(	(	kIx(
<g/>
modrá	modrat	k5eAaImIp3nS
plus	plus	k6eAd1
červená	červený	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
obsahu	obsah	k1gInSc3
čtverce	čtverec	k1gInSc2
nad	nad	k7c7
přeponou	přepona	k1gFnSc7
pravoúhlého	pravoúhlý	k2eAgInSc2d1
rovinného	rovinný	k2eAgInSc2d1
trojúhelníka	trojúhelník	k1gInSc2
(	(	kIx(
<g/>
fialová	fialový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Věta	věta	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
podle	podle	k7c2
řeckého	řecký	k2eAgMnSc2d1
filosofa	filosof	k1gMnSc2
a	a	k8xC
matematika	matematik	k1gMnSc2
Pythagora	Pythagoras	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
ji	on	k3xPp3gFnSc4
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
objevil	objevit	k5eAaPmAgMnS
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
resp.	resp.	kA
starověkou	starověký	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
známa	znám	k2eAgFnSc1d1
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
starověkých	starověký	k2eAgFnPc6d1
civilizacích	civilizace	k1gFnPc6
dávno	dávno	k6eAd1
předtím	předtím	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
Babylonii	Babylonie	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
Obdélníkové	obdélníkový	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
má	mít	k5eAaImIp3nS
délky	délka	k1gFnPc4
stran	stran	k7c2
30	#num#	k4
a	a	k8xC
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolik	kolika	k1gFnPc2
metrů	metr	k1gInPc2
bude	být	k5eAaImBp3nS
měřit	měřit	k5eAaImF
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
po	po	k7c6
úhlopříčce	úhlopříčka	k1gFnSc6
náměstí	náměstí	k1gNnSc2
rovně	rovně	k6eAd1
z	z	k7c2
jednoho	jeden	k4xCgInSc2
rohu	roh	k1gInSc2
do	do	k7c2
druhého	druhý	k4xOgNnSc2
<g/>
?	?	kIx.
</s>
<s>
Řešení	řešení	k1gNnSc1
<g/>
:	:	kIx,
Představme	představit	k5eAaPmRp1nP
si	se	k3xPyFc3
jeden	jeden	k4xCgInSc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
trojúhelníků	trojúhelník	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgMnPc4
cesta	cesta	k1gFnSc1
náměstí	náměstí	k1gNnSc4
rozdělí	rozdělit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Součet	součet	k1gInSc1
čtverců	čtverec	k1gInPc2
délek	délka	k1gFnPc2
jeho	jeho	k3xOp3gFnPc2
odvěsen	odvěsna	k1gFnPc2
(	(	kIx(
<g/>
stran	strana	k1gFnPc2
náměstí	náměstí	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
30	#num#	k4
<g/>
²	²	k?
m²	m²	k?
+	+	kIx~
40	#num#	k4
<g/>
²	²	k?
m²	m²	k?
=	=	kIx~
900	#num#	k4
m²	m²	k?
+	+	kIx~
1600	#num#	k4
m²	m²	k?
=	=	kIx~
2500	#num#	k4
m²	m²	k?
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
se	se	k3xPyFc4
podle	podle	k7c2
Pythagorovy	Pythagorův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
zároveň	zároveň	k6eAd1
rovná	rovnat	k5eAaImIp3nS
čtverci	čtverec	k1gInSc3
přepony	přepona	k1gFnSc2
trojúhelníka	trojúhelník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
je	on	k3xPp3gMnPc4
tedy	tedy	k9
odmocnit	odmocnit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
dostaneme	dostat	k5eAaPmIp1nP
délku	délka	k1gFnSc4
přepony	přepona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmocnina	odmocnina	k1gFnSc1
z	z	k7c2
2500	#num#	k4
m²	m²	k?
je	být	k5eAaImIp3nS
50	#num#	k4
m	m	kA
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
hledaná	hledaný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
úhlopříčné	úhlopříčný	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zobecnění	zobecnění	k1gNnSc1
Pythagorovy	Pythagorův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
</s>
<s>
Nahrazení	nahrazení	k1gNnSc4
čtverců	čtverec	k1gInPc2
jinými	jiný	k2eAgInPc7d1
plošnými	plošný	k2eAgInPc7d1
obrazci	obrazec	k1gInPc7
</s>
<s>
Součet	součet	k1gInSc1
obsahů	obsah	k1gInPc2
podobných	podobný	k2eAgInPc2d1
obrazců	obrazec	k1gInPc2
nad	nad	k7c7
odvěsnami	odvěsna	k1gFnPc7
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
obsahu	obsah	k1gInSc2
jim	on	k3xPp3gMnPc3
podobného	podobný	k2eAgInSc2d1
obrazce	obrazec	k1gInSc2
nad	nad	k7c7
přeponou	přepona	k1gFnSc7
pravoúhlého	pravoúhlý	k2eAgInSc2d1
rovinného	rovinný	k2eAgInSc2d1
trojúhelníka	trojúhelník	k1gInSc2
(	(	kIx(
<g/>
zde	zde	k6eAd1
pravidelné	pravidelný	k2eAgInPc1d1
pětiúhelníky	pětiúhelník	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čtverce	čtverec	k1gInPc1
lze	lze	k6eAd1
ve	v	k7c6
formulaci	formulace	k1gFnSc6
věty	věta	k1gFnSc2
zaměnit	zaměnit	k5eAaPmF
jakýmikoliv	jakýkoliv	k3yIgInPc7
jinými	jiný	k2eAgInPc7d1
obrazci	obrazec	k1gInPc7
(	(	kIx(
<g/>
kružnicí	kružnice	k1gFnSc7
<g/>
,	,	kIx,
obdélníkem	obdélník	k1gInSc7
<g/>
,	,	kIx,
trojúhelníkem	trojúhelník	k1gInSc7
<g/>
,	,	kIx,
pětiúhelníkem	pětiúhelník	k1gInSc7
<g/>
)	)	kIx)
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
navzájem	navzájem	k6eAd1
podobné	podobný	k2eAgFnPc1d1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
šířka	šířka	k1gFnSc1
je	být	k5eAaImIp3nS
úměrná	úměrný	k2eAgFnSc1d1
délce	délka	k1gFnSc3
příslušné	příslušný	k2eAgFnSc2d1
strany	strana	k1gFnSc2
trojúhelníku	trojúhelník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součet	součet	k1gInSc4
obsahů	obsah	k1gInPc2
těchto	tento	k3xDgInPc2
obrazců	obrazec	k1gInPc2
nad	nad	k7c7
odvěsnami	odvěsna	k1gFnPc7
bude	být	k5eAaImBp3nS
opět	opět	k6eAd1
shodný	shodný	k2eAgInSc1d1
s	s	k7c7
obsahem	obsah	k1gInSc7
obrazce	obrazec	k1gInSc2
nad	nad	k7c7
přeponou	přepona	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Že	že	k9
to	ten	k3xDgNnSc1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
formulace	formulace	k1gFnSc2
původní	původní	k2eAgFnSc2d1
věty	věta	k1gFnSc2
se	s	k7c7
čtverci	čtverec	k1gInPc7
nad	nad	k7c7
stranami	strana	k1gFnPc7
trojúhelníka	trojúhelník	k1gInSc2
<g/>
,	,	kIx,
si	se	k3xPyFc3
uvědomíme	uvědomit	k5eAaPmIp1nP
<g/>
,	,	kIx,
když	když	k8xS
uvážíme	uvážet	k5eAaImIp1nP,k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
obsah	obsah	k1gInSc1
každého	každý	k3xTgInSc2
z	z	k7c2
obrazců	obrazec	k1gInPc2
je	být	k5eAaImIp3nS
díky	díky	k7c3
platnosti	platnost	k1gFnSc3
předpokladů	předpoklad	k1gInPc2
úměrný	úměrný	k2eAgInSc1d1
obsahu	obsah	k1gInSc3
čtverce	čtverec	k1gInSc2
nad	nad	k7c7
danou	daný	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
a	a	k8xC
konstanta	konstanta	k1gFnSc1
úměrnosti	úměrnost	k1gFnSc2
k	k	k7c3
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
táž	týž	k3xTgFnSc1
díky	díky	k7c3
vzájemné	vzájemný	k2eAgFnSc3d1
podobnosti	podobnost	k1gFnSc3
obrazců	obrazec	k1gInPc2
i	i	k8xC
čtverců	čtverec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zobecnění	zobecnění	k1gNnSc1
na	na	k7c4
tři	tři	k4xCgInPc4
obecné	obecný	k2eAgInPc4d1
vektory	vektor	k1gInPc4
v	v	k7c6
Hilbertově	Hilbertův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
</s>
<s>
Pythagorovu	Pythagorův	k2eAgFnSc4d1
větu	věta	k1gFnSc4
lze	lze	k6eAd1
zobecnit	zobecnit	k5eAaPmF
na	na	k7c4
jakýkoliv	jakýkoliv	k3yIgInSc4
vektorový	vektorový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
se	s	k7c7
skalárním	skalární	k2eAgInSc7d1
součinem	součin	k1gInSc7
(	(	kIx(
<g/>
Hilbertův	Hilbertův	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojúhelníkem	trojúhelník	k1gInSc7
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
myslíme	myslet	k5eAaImIp1nP
tři	tři	k4xCgInPc1
vektory	vektor	k1gInPc1
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
<g/>
,	,	kIx,
c	c	k0
takové	takový	k3xDgFnPc4
<g/>
,	,	kIx,
že	že	k8xS
c	c	k0
=	=	kIx~
b	b	k?
-	-	kIx~
a	a	k8xC
a	a	k8xC
že	že	k8xS
a	a	k8xC
a	a	k8xC
b	b	k?
jsou	být	k5eAaImIp3nP
na	na	k7c4
sebe	sebe	k3xPyFc4
kolmé	kolmý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
platí	platit	k5eAaImIp3nS
podobný	podobný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
normami	norma	k1gFnPc7
těchto	tento	k3xDgInPc2
vektorů	vektor	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
rovinného	rovinný	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
‖	‖	k?
</s>
<s>
a	a	k8xC
</s>
<s>
‖	‖	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
‖	‖	k?
</s>
<s>
b	b	k?
</s>
<s>
‖	‖	k?
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
‖	‖	k?
</s>
<s>
c	c	k0
</s>
<s>
‖	‖	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
\	\	kIx~
<g/>
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
\	\	kIx~
<g/>
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
‖	‖	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
‖	‖	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
\	\	kIx~
<g/>
|	|	kIx~
<g/>
}	}	kIx)
</s>
<s>
značí	značit	k5eAaImIp3nS
normu	norma	k1gFnSc4
na	na	k7c6
daném	daný	k2eAgInSc6d1
vektorovém	vektorový	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
této	tento	k3xDgFnSc2
obecnější	obecní	k2eAgFnSc2d2
formulace	formulace	k1gFnSc2
lze	lze	k6eAd1
odvodit	odvodit	k5eAaPmF
i	i	k9
původní	původní	k2eAgFnSc4d1
rovinnou	rovinný	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
věty	věta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
rovinu	rovina	k1gFnSc4
chápeme	chápat	k5eAaImIp1nP
jako	jako	k9
2	#num#	k4
<g/>
-rozměrný	-rozměrný	k2eAgInSc4d1
Euklidův	Euklidův	k2eAgInSc4d1
prostor	prostor	k1gInSc4
s	s	k7c7
obyčejným	obyčejný	k2eAgInSc7d1
skalárním	skalární	k2eAgInSc7d1
součinem	součin	k1gInSc7
a	a	k8xC
v	v	k7c6
trojúhelníku	trojúhelník	k1gInSc6
ABC	ABC	kA
s	s	k7c7
pravým	pravý	k2eAgInSc7d1
úhlem	úhel	k1gInSc7
u	u	k7c2
vrcholu	vrchol	k1gInSc2
C	C	kA
označíme	označit	k5eAaPmIp1nP
a	a	k8xC
=	=	kIx~
B	B	kA
-	-	kIx~
C	C	kA
<g/>
,	,	kIx,
b	b	k?
=	=	kIx~
A	A	kA
-	-	kIx~
C	C	kA
a	a	k8xC
c	c	k0
=	=	kIx~
A	A	kA
-	-	kIx~
B	B	kA
(	(	kIx(
<g/>
=	=	kIx~
b	b	k?
-	-	kIx~
a	a	k8xC
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plyne	plynout	k5eAaImIp3nS
původní	původní	k2eAgFnSc1d1
Pythagorova	Pythagorův	k2eAgFnSc1d1
formulace	formulace	k1gFnSc1
ze	z	k7c2
vztahu	vztah	k1gInSc2
norem	norma	k1gFnPc2
vektorů	vektor	k1gInPc2
<g/>
,	,	kIx,
uvědomíme	uvědomit	k5eAaPmIp1nP
<g/>
-li	-li	k?
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
norma	norma	k1gFnSc1
vektoru	vektor	k1gInSc2
pouze	pouze	k6eAd1
délka	délka	k1gFnSc1
odpovídající	odpovídající	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zobecnění	zobecnění	k1gNnSc4
na	na	k7c4
více	hodně	k6eAd2
dimenzí	dimenze	k1gFnPc2
</s>
<s>
Větu	věta	k1gFnSc4
lze	lze	k6eAd1
zobecnit	zobecnit	k5eAaPmF
i	i	k9
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
dvě	dva	k4xCgFnPc4
dimenze	dimenze	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
pokud	pokud	k8xS
umocníme	umocnit	k5eAaPmIp1nP
délku	délka	k1gFnSc4
tělesové	tělesový	k2eAgFnSc2d1
úhlopříčky	úhlopříčka	k1gFnSc2
kvádru	kvádr	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
cihly	cihla	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c4
druhou	druhý	k4xOgFnSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
rovnat	rovnat	k5eAaImF
součtu	součet	k1gInSc6
čtverců	čtverec	k1gInPc2
délek	délka	k1gFnPc2
všech	všecek	k3xTgInPc2
tří	tři	k4xCgInPc2
rozměrů	rozměr	k1gInPc2
kvádru	kvádr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogické	analogický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
platí	platit	k5eAaImIp3nP
i	i	k9
v	v	k7c6
euklidovských	euklidovský	k2eAgInPc6d1
prostorech	prostor	k1gInPc6
vyšších	vysoký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Matematicky	matematicky	k6eAd1
řečeno	říct	k5eAaPmNgNnS
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
čtverec	čtverec	k1gInSc1
délky	délka	k1gFnSc2
(	(	kIx(
<g/>
normy	norma	k1gFnSc2
<g/>
)	)	kIx)
vektoru	vektor	k1gInSc2
roven	roven	k2eAgInSc1d1
součtu	součet	k1gInSc3
čtverců	čtverec	k1gInPc2
jeho	jeho	k3xOp3gFnPc2
souřadnic	souřadnice	k1gFnPc2
v	v	k7c6
libovolné	libovolný	k2eAgFnSc6d1
ortonormální	ortonormální	k2eAgFnSc6d1
bázi	báze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
představu	představa	k1gFnSc4
lze	lze	k6eAd1
zobecnit	zobecnit	k5eAaPmF
i	i	k9
na	na	k7c4
prostory	prostora	k1gFnPc4
nekonečné	konečný	k2eNgFnSc2d1
dimenze	dimenze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kosinová	kosinový	k2eAgFnSc1d1
věta	věta	k1gFnSc1
–	–	k?
zobecnění	zobecnění	k1gNnSc1
na	na	k7c4
jiné	jiný	k2eAgNnSc4d1
než	než	k8xS
pravé	pravý	k2eAgNnSc4d1
úhly	úhel	k1gInPc7
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
úhel	úhel	k1gInSc1
mezi	mezi	k7c7
stranami	strana	k1gFnPc7
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
pravý	pravý	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
jeho	jeho	k3xOp3gFnSc1
velikost	velikost	k1gFnSc1
γ	γ	k?
zavést	zavést	k5eAaPmF
do	do	k7c2
vztahu	vztah	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
dalšího	další	k2eAgInSc2d1
sčítance	sčítanec	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
γ	γ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
ab	ab	k?
<g/>
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
gamma	gamm	k1gMnSc2
\	\	kIx~
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
formulace	formulace	k1gFnSc1
kosinové	kosinový	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důkaz	důkaz	k1gInSc4
kosinové	kosinový	k2eAgFnSc2d1
věty	věta	k1gFnSc2
lze	lze	k6eAd1
podat	podat	k5eAaPmF
rozdělením	rozdělení	k1gNnSc7
trojúhelníka	trojúhelník	k1gInSc2
na	na	k7c4
dva	dva	k4xCgInPc4
pravoúhlé	pravoúhlý	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
Důkazy	důkaz	k1gInPc1
Pythagorovy	Pythagorův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
</s>
<s>
Důkazů	důkaz	k1gInPc2
Pythagorovy	Pythagorův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
existuje	existovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
mnoho	mnoho	k4c1
(	(	kIx(
<g/>
uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
až	až	k9
300	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
několik	několik	k4yIc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Důkaz	důkaz	k1gInSc1
č.	č.	k?
1	#num#	k4
</s>
<s>
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
grafický	grafický	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtverec	čtverec	k1gInSc1
o	o	k7c6
straně	strana	k1gFnSc6
a	a	k8xC
+	+	kIx~
b	b	k?
lze	lze	k6eAd1
složit	složit	k5eAaPmF
dvěma	dva	k4xCgInPc7
způsoby	způsob	k1gInPc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
obrázek	obrázek	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
ze	z	k7c2
4	#num#	k4
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
a	a	k8xC
dvou	dva	k4xCgInPc2
čtverců	čtverec	k1gInPc2
délkách	délka	k1gFnPc6
stran	strana	k1gFnPc2
a	a	k8xC
a	a	k8xC
b	b	k?
</s>
<s>
ze	z	k7c2
4	#num#	k4
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
a	a	k8xC
jednoho	jeden	k4xCgInSc2
čtverce	čtverec	k1gInSc2
o	o	k7c6
straně	strana	k1gFnSc6
c	c	k0
</s>
<s>
Z	z	k7c2
rovnosti	rovnost	k1gFnSc2
obsahu	obsah	k1gInSc2
čtverce	čtverec	k1gInSc2
při	při	k7c6
obou	dva	k4xCgInPc6
způsobech	způsob	k1gInPc6
složení	složení	k1gNnSc2
pak	pak	k8xC
plyne	plynout	k5eAaImIp3nS
i	i	k9
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Důkaz	důkaz	k1gInSc1
č.	č.	k?
2	#num#	k4
</s>
<s>
Jde	jít	k5eAaImIp3nS
jen	jen	k9
o	o	k7c4
zápis	zápis	k1gInSc4
Důkazu	důkaz	k1gInSc2
č.	č.	k?
1	#num#	k4
pomocí	pomocí	k7c2
rovnic	rovnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
celého	celý	k2eAgInSc2d1
čtverce	čtverec	k1gInSc2
lze	lze	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
dvěma	dva	k4xCgInPc7
způsoby	způsob	k1gInPc7
takto	takto	k6eAd1
(	(	kIx(
<g/>
jen	jen	k9
pravý	pravý	k2eAgInSc4d1
obrázek	obrázek	k1gInSc4
z	z	k7c2
pohledu	pohled	k1gInSc2
čtenáře	čtenář	k1gMnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Strana	strana	k1gFnSc1
čtverce	čtverec	k1gInSc2
je	být	k5eAaImIp3nS
složena	složit	k5eAaPmNgFnS
ze	z	k7c2
stran	strana	k1gFnPc2
trojúhelníku	trojúhelník	k1gInSc2
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
Pro	pro	k7c4
obsah	obsah	k1gInSc4
tedy	tedy	k9
platí	platit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
ab	ab	k?
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Čtverec	čtverec	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
4	#num#	k4
barevnými	barevný	k2eAgInPc7d1
pravoúhlými	pravoúhlý	k2eAgInPc7d1
trojúhelníky	trojúhelník	k1gInPc7
a	a	k8xC
bílým	bílý	k2eAgInSc7d1
čtvercem	čtverec	k1gInSc7
se	se	k3xPyFc4
stranou	stranou	k6eAd1
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
uprostřed	uprostřed	k7c2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
celého	celý	k2eAgInSc2d1
čtverce	čtverec	k1gInSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
součtem	součet	k1gInSc7
obsahu	obsah	k1gInSc2
4	#num#	k4
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
4	#num#	k4
<g/>
ab	ab	k?
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
ab	ab	k?
<g/>
}	}	kIx)
</s>
<s>
)	)	kIx)
a	a	k8xC
bílého	bílý	k2eAgInSc2d1
čtverce	čtverec	k1gInSc2
uprostřed	uprostřed	k7c2
se	se	k3xPyFc4
stranou	stranou	k6eAd1
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
c	c	k0
</s>
<s>
⋅	⋅	k?
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
c	c	k0
<g/>
=	=	kIx~
<g/>
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
+	+	kIx~
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
S	s	k7c7
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
ab	ab	k?
<g/>
+	+	kIx~
<g/>
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Protože	protože	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
vždy	vždy	k6eAd1
o	o	k7c4
tentýž	týž	k3xTgInSc4
velký	velký	k2eAgInSc4d1
čtverec	čtverec	k1gInSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
obsah	obsah	k1gInSc4
spočtený	spočtený	k2eAgInSc4d1
oběma	dva	k4xCgFnPc7
způsoby	způsob	k1gInPc7
rovnat	rovnat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
+	+	kIx~
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
ab	ab	k?
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
ab	ab	k?
<g/>
+	+	kIx~
<g/>
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
dostáváme	dostávat	k5eAaImIp1nP
tvrzení	tvrzení	k1gNnSc4
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Důkaz	důkaz	k1gInSc1
č.	č.	k?
3	#num#	k4
</s>
<s>
Podobnost	podobnost	k1gFnSc1
trojúhelníků	trojúhelník	k1gInPc2
</s>
<s>
Lze	lze	k6eAd1
se	se	k3xPyFc4
snadno	snadno	k6eAd1
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
zeleně	zeleně	k6eAd1
vyznačené	vyznačený	k2eAgInPc1d1
úhly	úhel	k1gInPc1
v	v	k7c6
obrázku	obrázek	k1gInSc6
dole	dole	k6eAd1
(	(	kIx(
<g/>
DCB	DCB	kA
a	a	k8xC
DAC	DAC	kA
-	-	kIx~
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
BAC	bac	k0
<g/>
)	)	kIx)
shodné	shodný	k2eAgFnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
trojúhelníky	trojúhelník	k1gInPc1
ABC	ABC	kA
<g/>
,	,	kIx,
CBD	CBD	kA
a	a	k8xC
ACD	ACD	kA
navzájem	navzájem	k6eAd1
podobné	podobný	k2eAgFnPc1d1
(	(	kIx(
<g/>
velikosti	velikost	k1gFnPc1
jejich	jejich	k3xOp3gFnPc2
stran	strana	k1gFnPc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
úhly	úhel	k1gInPc1
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
velké	velký	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Grafická	grafický	k2eAgFnSc1d1
ilustrace	ilustrace	k1gFnSc1
rovnosti	rovnost	k1gFnSc2
úhlů	úhel	k1gInPc2
</s>
<s>
→	→	k?
</s>
<s>
Dílčí	dílčí	k2eAgInPc1d1
trojúhelníky	trojúhelník	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vzniky	vznik	k1gInPc1
<g/>
,	,	kIx,
když	když	k8xS
výška	výška	k1gFnSc1
rozdělila	rozdělit	k5eAaPmAgFnS
pravoúhlý	pravoúhlý	k2eAgInSc4d1
trojúhelník	trojúhelník	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
přeskupit	přeskupit	k5eAaPmF
a	a	k8xC
položit	položit	k5eAaPmF
přes	přes	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
všechny	všechen	k3xTgInPc1
tři	tři	k4xCgInPc1
trojúhelníky	trojúhelník	k1gInPc1
mají	mít	k5eAaImIp3nP
stejné	stejný	k2eAgInPc1d1
úhly	úhel	k1gInPc1
a	a	k8xC
liší	lišit	k5eAaImIp3nP
se	se	k3xPyFc4
jen	jen	k9
velikostí	velikost	k1gFnSc7
<g/>
,	,	kIx,
označujeme	označovat	k5eAaImIp1nP
je	on	k3xPp3gNnSc4
za	za	k7c4
sobě	se	k3xPyFc3
podobné	podobný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
podobnosti	podobnost	k1gFnSc2
dále	daleko	k6eAd2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
poměry	poměr	k1gInPc1
jejich	jejich	k3xOp3gFnPc2
stran	strana	k1gFnPc2
musejí	muset	k5eAaImIp3nP
být	být	k5eAaImF
shodné	shodný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
faktu	fakt	k1gInSc2
vyšel	vyjít	k5eAaPmAgInS
i	i	k9
Pythagoras	Pythagoras	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpočet	výpočet	k1gInSc1
na	na	k7c6
sousedním	sousední	k2eAgInSc6d1
obrázku	obrázek	k1gInSc6
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jednoduchou	jednoduchý	k2eAgFnSc7d1
úpravou	úprava	k1gFnSc7
dostáváme	dostávat	k5eAaImIp1nP
Pythagorovu	Pythagorův	k2eAgFnSc4d1
větu	věta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pythagorovu	Pythagorův	k2eAgFnSc4d1
větu	věta	k1gFnSc4
lze	lze	k6eAd1
také	také	k9
dokázat	dokázat	k5eAaPmF
důkazem	důkaz	k1gInSc7
kosinové	kosinový	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
jejím	její	k3xOp3gInSc7
speciálním	speciální	k2eAgInSc7d1
případem	případ	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
tedy	tedy	k9
platí	platit	k5eAaImIp3nS
veta	veta	k6eAd1
kosinová	kosinový	k2eAgFnSc1d1
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
i	i	k9
Pythagorova	Pythagorův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Algebraický	algebraický	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
podobnosti	podobnost	k1gFnSc2
trojúhelníků	trojúhelník	k1gInPc2
</s>
<s>
Součet	součet	k1gInSc1
úhlů	úhel	k1gInPc2
trojúhelníka	trojúhelník	k1gInSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
180	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgInSc6
z	z	k7c2
těchto	tento	k3xDgInPc2
tří	tři	k4xCgInPc2
trojúhelníků	trojúhelník	k1gInPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
úhel	úhel	k1gInSc1
pravý	pravý	k2eAgInSc1d1
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
90	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
součet	součet	k1gInSc1
zbývajících	zbývající	k2eAgInPc2d1
dvou	dva	k4xCgInPc2
úhlů	úhel	k1gInPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
90	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dílčích	dílčí	k2eAgInPc6d1
trojúhelnících	trojúhelník	k1gInPc6
známe	znát	k5eAaImIp1nP
vždy	vždy	k6eAd1
dva	dva	k4xCgInPc1
úhly	úhel	k1gInPc1
<g/>
,	,	kIx,
proto	proto	k8xC
dopočítat	dopočítat	k5eAaPmF
třetí	třetí	k4xOgNnSc4
není	být	k5eNaImIp3nS
problém	problém	k1gInSc1
</s>
<s>
Platí	platit	k5eAaImIp3nS
tedy	tedy	k9
<g/>
:	:	kIx,
</s>
<s>
BAC	bacit	k5eAaPmRp2nS
+	+	kIx~
ACB	ACB	kA
<g/>
(	(	kIx(
<g/>
90	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
+	+	kIx~
CBA	CBA	kA
<g/>
(	(	kIx(
<g/>
CBD	CBD	kA
<g/>
)	)	kIx)
=	=	kIx~
180	#num#	k4
<g/>
°	°	k?
</s>
<s>
CBD	CBD	kA
+	+	kIx~
BDC	BDC	kA
<g/>
(	(	kIx(
<g/>
90	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
+	+	kIx~
DCB	DCB	kA
=	=	kIx~
180	#num#	k4
<g/>
°	°	k?
</s>
<s>
DAC	DAC	kA
+	+	kIx~
ACD	ACD	kA
+	+	kIx~
CDA	CDA	kA
<g/>
(	(	kIx(
<g/>
90	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
=	=	kIx~
180	#num#	k4
<g/>
°	°	k?
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
BAC	bacit	k5eAaPmRp2nS
<g/>
(	(	kIx(
<g/>
DAC	DAC	kA
<g/>
)	)	kIx)
+	+	kIx~
CBA	CBA	kA
<g/>
(	(	kIx(
<g/>
CBD	CBD	kA
<g/>
)	)	kIx)
=	=	kIx~
90	#num#	k4
<g/>
°	°	k?
</s>
<s>
CBD	CBD	kA
+	+	kIx~
DCB	DCB	kA
=	=	kIx~
90	#num#	k4
<g/>
°	°	k?
</s>
<s>
DAC	DAC	kA
<g/>
(	(	kIx(
<g/>
DAC	DAC	kA
<g/>
)	)	kIx)
+	+	kIx~
ACD	ACD	kA
=	=	kIx~
90	#num#	k4
<g/>
°	°	k?
</s>
<s>
Z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
rovnice	rovnice	k1gFnSc1
vyplývá	vyplývat	k5eAaImIp3nS
(	(	kIx(
<g/>
BAC	bacit	k5eAaPmRp2nS
a	a	k8xC
DAC	DAC	kA
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
rovny	roven	k2eAgFnPc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
CBA	CBA	kA
<g/>
(	(	kIx(
<g/>
CBD	CBD	kA
<g/>
)	)	kIx)
=	=	kIx~
ACD	ACD	kA
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
CBA	CBA	kA
(	(	kIx(
<g/>
stejný	stejný	k2eAgInSc4d1
jako	jako	k8xC,k8xS
CBD	CBD	kA
<g/>
)	)	kIx)
dosadíme	dosadit	k5eAaPmIp1nP
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
rovnice	rovnice	k1gFnSc2
místo	místo	k7c2
ACD	ACD	kA
<g/>
,	,	kIx,
ze	z	k7c2
srovnání	srovnání	k1gNnSc2
s	s	k7c7
2	#num#	k4
<g/>
.	.	kIx.
rovnicí	rovnice	k1gFnSc7
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CBD	CBD	kA
+	+	kIx~
DCB	DCB	kA
=	=	kIx~
90	#num#	k4
<g/>
°	°	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DAC	DAC	kA
+	+	kIx~
CBA	CBA	kA
(	(	kIx(
<g/>
ACD	ACD	kA
<g/>
,	,	kIx,
CBD	CBD	kA
<g/>
)	)	kIx)
=	=	kIx~
90	#num#	k4
<g/>
°	°	k?
</s>
<s>
pak	pak	k6eAd1
jasně	jasně	k6eAd1
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
DCB	DCB	kA
=	=	kIx~
DAC	DAC	kA
</s>
<s>
Trojúhelníky	trojúhelník	k1gInPc1
si	se	k3xPyFc3
jsou	být	k5eAaImIp3nP
podobné	podobný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
QED	QED	kA
</s>
<s>
Pythagorejská	pythagorejský	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Generátor	generátor	k1gInSc1
pythagorejských	pythagorejský	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pythagorejská	pythagorejský	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
tvoří	tvořit	k5eAaImIp3nS
trojice	trojice	k1gFnSc1
přirozených	přirozený	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
,	,	kIx,
<g/>
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
takových	takový	k3xDgInPc2
<g/>
,	,	kIx,
že	že	k8xS
platí	platit	k5eAaImIp3nS
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
tedy	tedy	k9
přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
vyhovující	vyhovující	k2eAgFnSc1d1
Pythagorově	Pythagorův	k2eAgFnSc3d1
větě	věta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pythagorejská	pythagorejský	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
jsou	být	k5eAaImIp3nP
např.	např.	kA
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pythagorejská	pythagorejský	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
podle	podle	k7c2
následující	následující	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Čísla	číslo	k1gNnPc1
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
,	,	kIx,
<g/>
\	\	kIx~
b	b	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
vyjádřená	vyjádřený	k2eAgFnSc1d1
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
x	x	k?
</s>
<s>
y	y	k?
</s>
<s>
,	,	kIx,
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
y	y	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
=	=	kIx~
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
-y	-y	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
\	\	kIx~
b	b	k?
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
xy	xy	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
c	c	k0
<g/>
=	=	kIx~
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
y	y	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
pro	pro	k7c4
nějaká	nějaký	k3yIgNnPc4
přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
s	s	k7c7
vlastností	vlastnost	k1gFnSc7
</s>
<s>
x	x	k?
</s>
<s>
>	>	kIx)
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
>	>	kIx)
<g/>
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
pythagorejská	pythagorejský	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
y	y	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
dostaneme	dostat	k5eAaPmIp1nP
trojici	trojice	k1gFnSc4
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
b	b	k?
<g/>
=	=	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
c	c	k0
<g/>
=	=	kIx~
<g/>
5	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Pro	pro	k7c4
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
y	y	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
dostaneme	dostat	k5eAaPmIp1nP
trojici	trojice	k1gFnSc4
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
8	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
=	=	kIx~
</s>
<s>
6	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
10	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
=	=	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
b	b	k?
<g/>
=	=	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
c	c	k0
<g/>
=	=	kIx~
<g/>
10	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
zřejmé	zřejmý	k2eAgInPc4d1
z	z	k7c2
výsledků	výsledek	k1gInPc2
pro	pro	k7c4
první	první	k4xOgInSc4
a	a	k8xC
druhý	druhý	k4xOgInSc4
příklad	příklad	k1gInSc4
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
vícenásobek	vícenásobek	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
uvedeném	uvedený	k2eAgInSc6d1
případě	případ	k1gInSc6
dvojnásobek	dvojnásobek	k1gInSc1
<g/>
)	)	kIx)
vypočtených	vypočtený	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
tudíž	tudíž	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
podobné	podobný	k2eAgInPc4d1
trojúhelníky	trojúhelník	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
uvedený	uvedený	k2eAgInSc1d1
způsob	způsob	k1gInSc1
generování	generování	k1gNnSc2
pythagorejských	pythagorejský	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
není	být	k5eNaImIp3nS
dokonalý	dokonalý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
některé	některý	k3yIgFnPc1
existující	existující	k2eAgFnPc1d1
kombinace	kombinace	k1gFnPc1
nejdou	jít	k5eNaImIp3nP
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
vůbec	vůbec	k9
vygenerovat	vygenerovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://phys.org/news/2016-04-year-journey-classroom.html	http://phys.org/news/2016-04-year-journey-classroom.html	k1gInSc1
-	-	kIx~
A	a	k9
3,800	3,800	k4
<g/>
-year	-yeara	k1gFnPc2
journey	journea	k1gFnSc2
from	from	k6eAd1
classroom	classroom	k1gInSc4
to	ten	k3xDgNnSc1
classroom	classroom	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Martin	Martin	k1gMnSc1
Vinkler	Vinkler	k1gMnSc1
<g/>
:	:	kIx,
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
,	,	kIx,
Eukleidovy	Eukleidův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
online	onlinout	k5eAaPmIp3nS
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4176546-1	4176546-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85109374	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85109374	#num#	k4
</s>
