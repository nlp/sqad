<s>
Pythagorova	Pythagorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
popisuje	popisovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
platí	platit	k5eAaImIp3nS
mezi	mezi	k7c7
délkami	délka	k1gFnPc7
stran	stran	k7c2
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
v	v	k7c6
euklidovské	euklidovský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>