<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dravý	dravý	k2eAgInSc1d1	dravý
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
převážně	převážně	k6eAd1	převážně
malými	malý	k2eAgFnPc7d1	malá
pelagickými	pelagický	k2eAgFnPc7d1	pelagická
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
sardinky	sardinka	k1gFnPc4	sardinka
a	a	k8xC	a
smáčci	smáček	k1gMnPc1	smáček
<g/>
.	.	kIx.	.
</s>
