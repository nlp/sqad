<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k1gMnSc1*xF
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
českém	český	k2eAgMnSc6d1
básníkovi	básník	k1gMnSc6
<g/>
,	,	kIx,
prozaikovi	prozaik	k1gMnSc3
a	a	k8xC
publicistovi	publicista	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
českém	český	k2eAgMnSc6d1
zpěvákovi	zpěvák	k1gMnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
(	(	kIx(
<g/>
zpěvák	zpěvák	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Revolučního	revoluční	k2eAgNnSc2d1
nár	nár	k?
<g/>
.	.	kIx.
shromáždění	shromáždění	k1gNnSc1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1918	#num#	k4
–	–	k?
1920	#num#	k4
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ČSR	ČSR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1920	#num#	k4
–	–	k?
1925	#num#	k4
</s>
<s>
Senátor	senátor	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ČSR	ČSR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1925	#num#	k4
–	–	k?
1931	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Státoprávní	státoprávní	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
<g/>
Čs	čs	kA
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1877	#num#	k4
Pšovka	Pšovka	k1gMnSc1
u	u	k7c2
MělníkaRakousko-Uhersko	MělníkaRakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
53	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Lopud	Lopud	k1gInSc1
Jugoslávie	Jugoslávie	k1gFnSc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Olšanské	olšanský	k2eAgInPc1d1
hřbitovy	hřbitov	k1gInPc1
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Choť	choť	k1gFnSc1
</s>
<s>
Zdenka	Zdenka	k1gFnSc1
Hásková	Hásková	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
bratr	bratr	k1gMnSc1
<g/>
:	:	kIx,
Ludvík	Ludvík	k1gMnSc1
Dyk	Dyk	k?
synovec	synovec	k1gMnSc1
<g/>
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Kripner	Kripner	k1gMnSc1
babička	babička	k1gFnSc1
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Patrovská	Patrovský	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
:	:	kIx,
Hedvika	Hedvika	k1gFnSc1
Dicková	Dicková	k1gFnSc1
otec	otec	k1gMnSc1
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Dick	Dick	k1gMnSc1
dědeček	dědeček	k1gMnSc1
<g/>
:	:	kIx,
František	František	k1gMnSc1
Patrovský	Patrovský	k2eAgMnSc1d1
sestra	sestra	k1gFnSc1
<g/>
:	:	kIx,
Hedvika	Hedvika	k1gFnSc1
Dyková	Dyková	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
šachista	šachista	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
a	a	k8xC
novinář	novinář	k1gMnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1877	#num#	k4
Pšovka	Pšovka	k1gMnSc1
u	u	k7c2
Mělníka	Mělník	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
Lopud	Lopuda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
prozaik	prozaik	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
publicista	publicista	k1gMnSc1
a	a	k8xC
nacionalistický	nacionalistický	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
mládí	mládí	k1gNnSc6
jeden	jeden	k4xCgMnSc1
z	z	k7c2
představitelů	představitel	k1gMnPc2
tzv.	tzv.	kA
generace	generace	k1gFnSc1
anarchistických	anarchistický	k2eAgMnPc2d1
buřičů	buřič	k1gMnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
nacionalisticky	nacionalisticky	k6eAd1
orientovaný	orientovaný	k2eAgMnSc1d1
autor	autor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
domácího	domácí	k2eAgInSc2d1
odboje	odboj	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgInS
také	také	k9
signatářem	signatář	k1gMnSc7
Manifestu	manifest	k1gInSc2
českých	český	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gNnPc4
nejznámější	známý	k2eAgNnPc4d3
literární	literární	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
novela	novela	k1gFnSc1
Krysař	krysař	k1gMnSc1
<g/>
,	,	kIx,
drama	drama	k1gFnSc1
Zmoudření	zmoudření	k1gNnSc2
Dona	dona	k1gFnSc1
Quijota	Quijota	k1gFnSc1
či	či	k8xC
básnická	básnický	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
Devátá	devátý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
mládí	mládí	k1gNnSc1
a	a	k8xC
studium	studium	k1gNnSc1
</s>
<s>
Rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
na	na	k7c6
rodném	rodný	k2eAgInSc6d1
domě	dům	k1gInSc6
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Pšovce	Pšovka	k1gFnSc6
u	u	k7c2
Mělníka	Mělník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
druhorozeným	druhorozený	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Václava	Václav	k1gMnSc2
a	a	k8xC
Hedviky	Hedvika	k1gFnSc2
Dykových	Dyková	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorův	Viktorův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Dyk	Dyk	k?
(	(	kIx(
<g/>
v	v	k7c6
úředních	úřední	k2eAgInPc6d1
záznamech	záznam	k1gInPc6
psáno	psán	k2eAgNnSc1d1
Dick	Dick	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
správcem	správce	k1gMnSc7
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
až	až	k6eAd1
centrálním	centrální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
mělnického	mělnický	k2eAgNnSc2d1
panství	panství	k1gNnSc2
knížete	kníže	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Dykův	Dykův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
velice	velice	k6eAd1
vzdělaný	vzdělaný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
odráželo	odrážet	k5eAaImAgNnS
také	také	k9
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
obsahově	obsahově	k6eAd1
hodnotné	hodnotný	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Viktor	Viktor	k1gMnSc1
rád	rád	k2eAgMnSc1d1
při	při	k7c6
studiu	studio	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
vzpomínal	vzpomínat	k5eAaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
mimo	mimo	k6eAd1
ročníků	ročník	k1gInPc2
Lumíra	Lumír	k1gInSc2
<g/>
,	,	kIx,
Světozoru	světozor	k1gInSc2
a	a	k8xC
Zlaté	zlatý	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
obsahovala	obsahovat	k5eAaImAgFnS
i	i	k8xC
svazky	svazek	k1gInPc4
Kobrovy	Kobrův	k2eAgFnSc2d1
Národní	národní	k2eAgFnSc2d1
bibliotéky	bibliotéka	k1gFnSc2
či	či	k8xC
Palackého	Palackého	k2eAgFnPc2d1
Dějin	dějiny	k1gFnPc2
národa	národ	k1gInSc2
českého	český	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
literaturou	literatura	k1gFnSc7
měl	mít	k5eAaImAgInS
tedy	tedy	k9
kontakt	kontakt	k1gInSc4
již	již	k6eAd1
od	od	k7c2
raného	raný	k2eAgNnSc2d1
dětství	dětství	k1gNnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
brzy	brzy	k6eAd1
vášnivým	vášnivý	k2eAgMnSc7d1
čtenářem	čtenář	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgInS
o	o	k7c4
českou	český	k2eAgFnSc4d1
historii	historie	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
umocňovalo	umocňovat	k5eAaImAgNnS
i	i	k9
jeho	jeho	k3xOp3gNnSc1
nacionální	nacionální	k2eAgNnSc1d1
smýšlení	smýšlení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
v	v	k7c6
dětství	dětství	k1gNnSc6
údajně	údajně	k6eAd1
sepsal	sepsat	k5eAaPmAgMnS
několik	několik	k4yIc4
her	hra	k1gFnPc2
pro	pro	k7c4
loutkové	loutkový	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
hrál	hrát	k5eAaImAgMnS
svému	svůj	k3xOyFgMnSc3
staršímu	starší	k1gMnSc3
bratru	bratr	k1gMnSc3
Ludvíkovi	Ludvík	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
složit	složit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
báseň	báseň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1888	#num#	k4
začal	začít	k5eAaPmAgMnS
chodit	chodit	k5eAaImF
na	na	k7c4
pražské	pražský	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Žitné	žitný	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gInSc4
mj.	mj.	kA
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
Alois	Alois	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
byl	být	k5eAaImAgMnS
Viktor	Viktor	k1gMnSc1
přímo	přímo	k6eAd1
uchvácen	uchvácen	k2eAgMnSc1d1
(	(	kIx(
<g/>
dokonce	dokonce	k9
uvažoval	uvažovat	k5eAaImAgMnS
o	o	k7c6
studiu	studio	k1gNnSc6
historie	historie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
Jiráska	Jirásek	k1gMnSc4
mladý	mladý	k2eAgMnSc1d1
Viktor	Viktor	k1gMnSc1
zaujal	zaujmout	k5eAaPmAgMnS
a	a	k8xC
až	až	k6eAd1
do	do	k7c2
smrti	smrt	k1gFnSc2
zůstávali	zůstávat	k5eAaImAgMnP
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
bratrem	bratr	k1gMnSc7
Ludvíkem	Ludvík	k1gMnSc7
<g/>
,	,	kIx,
rovněž	rovněž	k9
studentem	student	k1gMnSc7
gymnázia	gymnázium	k1gNnSc2
v	v	k7c6
Žitné	žitný	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
bydleli	bydlet	k5eAaImAgMnP
u	u	k7c2
své	svůj	k3xOyFgFnSc2
ovdovělé	ovdovělý	k2eAgFnSc2d1
babičky	babička	k1gFnSc2
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
Marie	Maria	k1gFnSc2
Patrovské	Patrovský	k2eAgFnSc2d1
<g/>
,	,	kIx,
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bratr	bratr	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
horlivý	horlivý	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
pokrokářského	pokrokářský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
(	(	kIx(
<g/>
kolem	kolem	k7c2
Časopisu	časopis	k1gInSc2
pokrokového	pokrokový	k2eAgNnSc2d1
studentstva	studentstvo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
zapaloval	zapalovat	k5eAaImAgInS
svými	svůj	k3xOyFgInPc7
politickými	politický	k2eAgInPc7d1
a	a	k8xC
čtenářskými	čtenářský	k2eAgInPc7d1
zájmy	zájem	k1gInPc7
i	i	k8xC
mladšího	mladý	k2eAgMnSc2d2
Viktora	Viktor	k1gMnSc2
a	a	k8xC
později	pozdě	k6eAd2
ho	on	k3xPp3gMnSc4
také	také	k9
uvedl	uvést	k5eAaPmAgMnS
do	do	k7c2
spolkového	spolkový	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1893	#num#	k4
byl	být	k5eAaImAgInS
nad	nad	k7c7
Prahou	Praha	k1gFnSc7
vyhlášen	vyhlášen	k2eAgInSc1d1
výjimečný	výjimečný	k2eAgInSc1d1
stav	stav	k1gInSc1
a	a	k8xC
byli	být	k5eAaImAgMnP
zatýkáni	zatýkán	k2eAgMnPc1d1
omladináři	omladinář	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
vznikají	vznikat	k5eAaImIp3nP
také	také	k6eAd1
jeho	jeho	k3xOp3gInPc1
první	první	k4xOgInPc1
básnické	básnický	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
dochované	dochovaný	k2eAgInPc1d1
v	v	k7c6
rukopisných	rukopisný	k2eAgInPc6d1
sešitech	sešit	k1gInPc6
<g/>
:	:	kIx,
Proudy	proud	k1gInPc1
a	a	k8xC
protiproudy	protiproud	k1gInPc1
<g/>
,	,	kIx,
Dušičková	dušičkový	k2eAgFnSc1d1
elegie	elegie	k1gFnSc1
<g/>
,	,	kIx,
Z	z	k7c2
básní	báseň	k1gFnPc2
zemřelého	zemřelý	k1gMnSc2
<g/>
,	,	kIx,
Lyrika	lyrik	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatýkání	zatýkání	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
<g/>
–	–	k?
<g/>
94	#num#	k4
se	se	k3xPyFc4
dotklo	dotknout	k5eAaPmAgNnS
i	i	k8xC
gymnázií	gymnázium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
toto	tento	k3xDgNnSc4
dění	dění	k1gNnSc4
pozoroval	pozorovat	k5eAaImAgMnS
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
zaujetím	zaujetí	k1gNnSc7
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
již	již	k6eAd1
tehdy	tehdy	k6eAd1
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
radikálním	radikální	k2eAgMnPc3d1
studentům	student	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
chtěli	chtít	k5eAaImAgMnP
skoncovat	skoncovat	k5eAaPmF
s	s	k7c7
monarchií	monarchie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
toto	tento	k3xDgNnSc4
dění	dění	k1gNnSc4
vznikaly	vznikat	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
další	další	k2eAgInPc1d1
rukopisné	rukopisný	k2eAgInPc1d1
verše	verš	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
Intimní	intimní	k2eAgNnSc4d1
volné	volný	k2eAgNnSc4d1
jeviště	jeviště	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
hrálo	hrát	k5eAaImAgNnS
jeho	jeho	k3xOp3gFnSc4
jednoaktovku	jednoaktovka	k1gFnSc4
Pomsta	pomsta	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
sepsal	sepsat	k5eAaPmAgMnS
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
R.	R.	kA
Vilde	Vild	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
začal	začít	k5eAaPmAgInS
publikovat	publikovat	k5eAaBmF
také	také	k9
ve	v	k7c6
Vesně	Vesna	k1gFnSc6
<g/>
,	,	kIx,
Nivě	niva	k1gFnSc6
<g/>
,	,	kIx,
Moderní	moderní	k2eAgFnSc6d1
revui	revue	k1gFnSc6
<g/>
,	,	kIx,
Volných	volný	k2eAgInPc6d1
směrech	směr	k1gInPc6
<g/>
,	,	kIx,
Rozhledech	rozhled	k1gInPc6
<g/>
,	,	kIx,
Studentském	studentský	k2eAgInSc6d1
sborníku	sborník	k1gInSc6
a	a	k8xC
měl	mít	k5eAaImAgMnS
báseň	báseň	k1gFnSc4
v	v	k7c6
Almanachu	almanach	k1gInSc6
secese	secese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
maturitě	maturita	k1gFnSc6
začal	začít	k5eAaPmAgInS
vydávat	vydávat	k5eAaPmF,k5eAaImF
již	již	k6eAd1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
vlastním	vlastní	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
18	#num#	k4
<g/>
letý	letý	k2eAgInSc4d1
zažil	zažít	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
debut	debut	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
ve	v	k7c6
Světozoru	světozor	k1gInSc2
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Viktor	Viktor	k1gMnSc1
Souček	Souček	k1gMnSc1
vyšly	vyjít	k5eAaPmAgInP
první	první	k4xOgFnPc1
tři	tři	k4xCgFnPc1
básně	báseň	k1gFnPc1
–	–	k?
Moderní	moderní	k2eAgFnPc1d1
věřící	věřící	k1gFnPc1
<g/>
,	,	kIx,
Není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
nic	nic	k3yNnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
a	a	k8xC
Otázky	otázka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšně	úspěšně	k6eAd1
složené	složený	k2eAgFnSc6d1
maturitě	maturita	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1896	#num#	k4
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgInS
na	na	k7c4
právnickou	právnický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
c.	c.	k?
k.	k.	k?
české	český	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
Karlo-Ferdinandské	Karlo-Ferdinandský	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mj.	mj.	kA
poslouchal	poslouchat	k5eAaImAgInS
přednášky	přednáška	k1gFnSc2
prof.	prof.	kA
Masaryka	Masaryk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstoupil	vstoupit	k5eAaPmAgMnS
zde	zde	k6eAd1
také	také	k9
do	do	k7c2
Literárního	literární	k2eAgInSc2d1
a	a	k8xC
řečnického	řečnický	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Slavia	Slavia	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
později	pozdě	k6eAd2
vykonával	vykonávat	k5eAaImAgInS
funkci	funkce	k1gFnSc4
místopředsedy	místopředseda	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
dokončil	dokončit	k5eAaPmAgMnS
Právnickou	právnický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
však	však	k9
působil	působit	k5eAaImAgMnS
jako	jako	k9
novinář	novinář	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
nevěnovat	věnovat	k5eNaImF
se	se	k3xPyFc4
právu	právo	k1gNnSc6
došel	dojít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
své	svůj	k3xOyFgFnSc2
justiční	justiční	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Politik	politik	k1gMnSc1
a	a	k8xC
odbojář	odbojář	k1gMnSc1
</s>
<s>
Formování	formování	k1gNnSc1
ideových	ideový	k2eAgInPc2d1
a	a	k8xC
politických	politický	k2eAgInPc2d1
názorů	názor	k1gInPc2
</s>
<s>
Dyk	Dyk	k?
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
<g/>
Ještě	ještě	k6eAd1
ve	v	k7c6
spolku	spolek	k1gInSc6
Slavia	Slavius	k1gMnSc2
zastával	zastávat	k5eAaImAgMnS
Viktor	Viktor	k1gMnSc1
názory	názor	k1gInPc4
umírněné	umírněný	k2eAgNnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ho	on	k3xPp3gMnSc4
stavělo	stavět	k5eAaImAgNnS
do	do	k7c2
opozice	opozice	k1gFnSc2
k	k	k7c3
jeho	jeho	k3xOp3gMnSc3
bratrovi	bratr	k1gMnSc3
Ludvíkovi	Ludvík	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
mnohem	mnohem	k6eAd1
radikálnější	radikální	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rozpor	rozpor	k1gInSc1
se	se	k3xPyFc4
promítal	promítat	k5eAaImAgInS
mj.	mj.	kA
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
Němcům	Němec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
byl	být	k5eAaImAgMnS
od	od	k7c2
samého	samý	k3xTgInSc2
počátku	počátek	k1gInSc2
vyhraněný	vyhraněný	k2eAgMnSc1d1
nacionalista	nacionalista	k1gMnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Viktor	Viktor	k1gMnSc1
zastával	zastávat	k5eAaImAgMnS
krajně	krajně	k6eAd1
levicové	levicový	k2eAgInPc4d1
názory	názor	k1gInPc4
<g/>
,	,	kIx,
dokonce	dokonce	k9
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
zúčastnil	zúčastnit	k5eAaPmAgMnS
sociálně	sociálně	k6eAd1
demokratického	demokratický	k2eAgInSc2d1
prvního	první	k4xOgMnSc2
máje	máj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Slavie	slavie	k1gFnSc2
si	se	k3xPyFc3
postupně	postupně	k6eAd1
prošel	projít	k5eAaPmAgMnS
dvěma	dva	k4xCgInPc7
obdobími	období	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
ovlivněn	ovlivnit	k5eAaPmNgInS
jak	jak	k8xC,k8xS
sociální	sociální	k2eAgInSc1d1
demokracií	demokracie	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
realismem	realismus	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
vůdčí	vůdčí	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
byl	být	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dyk	Dyk	k?
byl	být	k5eAaImAgInS
nejprve	nejprve	k6eAd1
Masarykem	Masaryk	k1gMnSc7
velmi	velmi	k6eAd1
osloven	osloven	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jakmile	jakmile	k8xS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
osobně	osobně	k6eAd1
setkal	setkat	k5eAaPmAgMnS
na	na	k7c6
jedné	jeden	k4xCgFnSc6
přednášce	přednáška	k1gFnSc6
<g/>
,	,	kIx,
nadšení	nadšení	k1gNnSc1
upadlo	upadnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykův	Masarykův	k2eAgInSc4d1
realismus	realismus	k1gInSc4
byl	být	k5eAaImAgMnS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
příliš	příliš	k6eAd1
racionální	racionální	k2eAgFnSc1d1
a	a	k8xC
bez	bez	k7c2
emocí	emoce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
vytýkal	vytýkat	k5eAaImAgMnS
Masarykovi	Masaryk	k1gMnSc3
především	především	k6eAd1
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
odpor	odpor	k1gInSc4
vůči	vůči	k7c3
radikalismu	radikalismus	k1gInSc3
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
jedině	jedině	k6eAd1
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
„	„	k?
<g/>
kladení	kladení	k1gNnSc1
dobra	dobro	k1gNnSc2
lidského	lidský	k2eAgNnSc2d1
nad	nad	k7c4
dobro	dobro	k1gNnSc4
národní	národní	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
pověrečnou	pověrečný	k2eAgFnSc4d1
bázeň	bázeň	k1gFnSc4
před	před	k7c7
šovinismem	šovinismus	k1gInSc7
<g/>
“	“	k?
i	i	k9
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
šlo	jít	k5eAaImAgNnS
pouze	pouze	k6eAd1
o	o	k7c4
nacionalismus	nacionalismus	k1gInSc4
<g/>
,	,	kIx,
běžný	běžný	k2eAgInSc4d1
u	u	k7c2
všech	všecek	k3xTgInPc2
evropských	evropský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
„	„	k?
<g/>
utopii	utopie	k1gFnSc4
možného	možný	k2eAgInSc2d1
smíru	smír	k1gInSc2
s	s	k7c7
Němci	Němec	k1gMnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dyk	Dyk	k?
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
<g/>
,	,	kIx,
ani	ani	k8xC
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
„	„	k?
<g/>
realistickém	realistický	k2eAgNnSc6d1
<g/>
“	“	k?
mezidobí	mezidobí	k1gNnSc1
nestal	stát	k5eNaPmAgInS
Masarykovým	Masarykův	k2eAgMnSc7d1
stoupencem	stoupenec	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dyk	Dyk	k?
původně	původně	k6eAd1
hledal	hledat	k5eAaImAgInS
východisko	východisko	k1gNnSc4
v	v	k7c6
individualismu	individualismus	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
chápal	chápat	k5eAaImAgInS
jako	jako	k9
jedinou	jediný	k2eAgFnSc4d1
možnou	možný	k2eAgFnSc4d1
„	„	k?
<g/>
střední	střední	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
mezi	mezi	k7c7
nacionalismem	nacionalismus	k1gInSc7
a	a	k8xC
socialismem	socialismus	k1gInSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
debutoval	debutovat	k5eAaBmAgMnS
i	i	k9
knižně	knižně	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
básnickou	básnický	k2eAgFnSc4d1
sbírkou	sbírka	k1gFnSc7
A	a	k8xC
porta	porta	k1gFnSc1
inferi	infer	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Dykovi	Dykův	k2eAgMnPc1d1
se	se	k3xPyFc4
zdál	zdát	k5eAaImAgInS
socialismus	socialismus	k1gInSc1
příliš	příliš	k6eAd1
kolektivistický	kolektivistický	k2eAgInSc1d1
a	a	k8xC
materialistický	materialistický	k2eAgInSc1d1
<g/>
,	,	kIx,
nacionalismus	nacionalismus	k1gInSc1
příliš	příliš	k6eAd1
šovinistický	šovinistický	k2eAgInSc1d1
a	a	k8xC
realismus	realismus	k1gInSc1
příliš	příliš	k6eAd1
nábožensky	nábožensky	k6eAd1
moralistní	moralistní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
však	však	k9
pochopil	pochopit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
lidský	lidský	k2eAgInSc4d1
život	život	k1gInSc4
mít	mít	k5eAaImF
nějaký	nějaký	k3yIgInSc4
význam	význam	k1gInSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
mít	mít	k5eAaImF
nějakou	nějaký	k3yIgFnSc4
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
podřízen	podřídit	k5eAaPmNgInS
něčemu	něco	k3yInSc3
vyššímu	vysoký	k2eAgNnSc3d2
než	než	k8xS
je	být	k5eAaImIp3nS
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
tak	tak	k9
pevně	pevně	k6eAd1
cestu	cesta	k1gFnSc4
k	k	k7c3
nacionalismu	nacionalismus	k1gInSc3
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
viděl	vidět	k5eAaImAgMnS
nesobecké	sobecký	k2eNgNnSc1d1
podřízení	podřízení	k1gNnSc1
se	se	k3xPyFc4
člověka	člověk	k1gMnSc2
vyššímu	vysoký	k2eAgNnSc3d2
národnímu	národní	k2eAgNnSc3d1
společenství	společenství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
Dykovu	Dykův	k2eAgFnSc4d1
celoživotní	celoživotní	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
ideovou	ideový	k2eAgFnSc4d1
a	a	k8xC
politickou	politický	k2eAgFnSc4d1
i	i	k9
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
inspiraci	inspirace	k1gFnSc6
literární	literární	k2eAgMnSc1d1
mají	mít	k5eAaImIp3nP
významné	významný	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
události	událost	k1gFnSc3
roku	rok	k1gInSc2
1897	#num#	k4
–	–	k?
volby	volba	k1gFnSc2
do	do	k7c2
páté	pátý	k4xOgFnSc2
kurie	kurie	k1gFnSc2
<g/>
,	,	kIx,
následná	následný	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
českých	český	k2eAgMnPc2d1
sociálně	sociálně	k6eAd1
demokratických	demokratický	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
proti	proti	k7c3
státoprávnímu	státoprávní	k2eAgNnSc3d1
ohrožení	ohrožení	k1gNnSc3
podanému	podaný	k2eAgInSc3d1
mladočechy	mladočech	k1gMnPc4
<g/>
,	,	kIx,
pád	pád	k1gInSc4
Badeniho	Badeni	k1gMnSc2
vlády	vláda	k1gFnSc2
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gNnSc7
spjaté	spjatý	k2eAgNnSc1d1
omezení	omezení	k1gNnSc1
tzv.	tzv.	kA
jazykového	jazykový	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
„	„	k?
<g/>
prosincové	prosincový	k2eAgFnSc2d1
národnostní	národnostní	k2eAgFnSc2d1
bouře	bouř	k1gFnSc2
<g/>
“	“	k?
na	na	k7c4
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
reagovaly	reagovat	k5eAaBmAgInP
příslušné	příslušný	k2eAgInPc1d1
politické	politický	k2eAgInPc1d1
orgány	orgán	k1gInPc1
vyhlášením	vyhlášení	k1gNnSc7
stanného	stanný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
v	v	k7c6
prosinci	prosinec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
německo-český	německo-český	k2eAgInSc1d1
prosinec	prosinec	k1gInSc1
hluboce	hluboko	k6eAd1
zasáhl	zasáhnout	k5eAaPmAgInS
i	i	k9
Viktora	Viktora	k1gMnSc1
Dyka	Dyka	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
I	i	k9
on	on	k3xPp3gMnSc1
vyšel	vyjít	k5eAaPmAgMnS
toho	ten	k3xDgInSc2
měsíce	měsíc	k1gInSc2
s	s	k7c7
trikolorou	trikolora	k1gFnSc7
do	do	k7c2
ulic	ulice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
přístup	přístup	k1gInSc4
k	k	k7c3
politice	politika	k1gFnSc3
i	i	k8xC
životu	život	k1gInSc3
Dyk	Dyk	k?
charakterizoval	charakterizovat	k5eAaBmAgInS
ve	v	k7c6
Vzpomínkách	vzpomínka	k1gFnPc6
a	a	k8xC
komentářích	komentář	k1gInPc6
takto	takto	k6eAd1
"	"	kIx"
<g/>
Kde	kde	k6eAd1
najdete	najít	k5eAaPmIp2nP
planý	planý	k2eAgInSc4d1
radikalismus	radikalismus	k1gInSc4
<g/>
,	,	kIx,
potírejte	potírat	k5eAaImRp2nP
jej	on	k3xPp3gInSc4
"	"	kIx"
Planý	planý	k2eAgInSc4d1
radikalismus	radikalismus	k1gInSc4
je	být	k5eAaImIp3nS
k	k	k7c3
opravdovému	opravdový	k2eAgInSc3d1
v	v	k7c6
podobném	podobný	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
klerikalismus	klerikalismus	k1gInSc1
k	k	k7c3
náboženství	náboženství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
však	však	k9
můžeme	moct	k5eAaImIp1nP
od	od	k7c2
lidí	člověk	k1gMnPc2
vzdělaných	vzdělaný	k2eAgMnPc2d1
žádat	žádat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dovedli	dovést	k5eAaPmAgMnP
odlišovat	odlišovat	k5eAaImF
radikalismus	radikalismus	k1gInSc4
povrchní	povrchnět	k5eAaImIp3nS
a	a	k8xC
heslový	heslový	k2eAgMnSc1d1
od	od	k7c2
onoho	onen	k3xDgNnSc2
<g/>
,	,	kIx,
ku	k	k7c3
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
chceme	chtít	k5eAaImIp1nP
vychovávat	vychovávat	k5eAaImF
<g/>
,	,	kIx,
od	od	k7c2
radikalismu	radikalismus	k1gInSc2
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
opírá	opírat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
základ	základ	k1gInSc4
politického	politický	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
a	a	k8xC
povahovou	povahový	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
,	,	kIx,
předpokládá	předpokládat	k5eAaImIp3nS
mravní	mravní	k2eAgFnSc4d1
sebevýchovu	sebevýchova	k1gFnSc4
a	a	k8xC
kázeň	kázeň	k1gFnSc4
<g/>
,	,	kIx,
pevnost	pevnost	k1gFnSc4
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
zásadách	zásada	k1gFnPc6
i	i	k9
obětavou	obětavý	k2eAgFnSc4d1
ochotu	ochota	k1gFnSc4
a	a	k8xC
kázeň	kázeň	k1gFnSc4
<g/>
,	,	kIx,
pevnost	pevnost	k1gFnSc4
v	v	k7c6
zásadách	zásada	k1gFnPc6
i	i	k8xC
proti	proti	k7c3
daným	daný	k2eAgInPc3d1
poměrům	poměr	k1gInPc3
<g/>
…	…	k?
Radikalismus	radikalismus	k1gInSc1
není	být	k5eNaImIp3nS
plané	planý	k2eAgNnSc4d1
rozčilování	rozčilování	k1gNnSc4
a	a	k8xC
bouření	bouření	k1gNnSc4
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
záchvaty	záchvat	k1gInPc1
rychle	rychle	k6eAd1
prchající	prchající	k2eAgInPc1d1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
netrpělivost	netrpělivost	k1gFnSc1
a	a	k8xC
těkavost	těkavost	k1gFnSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
víra	víra	k1gFnSc1
v	v	k7c4
zázraky	zázrak	k1gInPc4
…	…	k?
radikalismus	radikalismus	k1gInSc1
je	být	k5eAaImIp3nS
houževnaté	houževnatý	k2eAgNnSc4d1
sledování	sledování	k1gNnSc4
vytčeného	vytčený	k2eAgInSc2d1
cíle	cíl	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
důslednost	důslednost	k1gFnSc1
a	a	k8xC
zásadovost	zásadovost	k1gFnSc1
v	v	k7c6
svědomité	svědomitý	k2eAgFnSc6d1
politické	politický	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Otázka	otázka	k1gFnSc1
vztahu	vztah	k1gInSc2
k	k	k7c3
Němcům	Němec	k1gMnPc3
a	a	k8xC
vliv	vliv	k1gInSc4
babičky	babička	k1gFnSc2
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
na	na	k7c6
domě	dům	k1gInSc6
v	v	k7c6
dn	dn	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dykově	Dykův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
v	v	k7c6
letech	let	k1gInPc6
1904	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
žil	žíla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
mladší	mladý	k2eAgMnSc1d2
z	z	k7c2
obou	dva	k4xCgMnPc2
bratrů	bratr	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
více	hodně	k6eAd2
vázal	vázat	k5eAaImAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
babičku	babička	k1gFnSc4
Marii	Maria	k1gFnSc4
Patrovskou	Patrovský	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
mu	on	k3xPp3gMnSc3
velmi	velmi	k6eAd1
ráda	rád	k2eAgFnSc1d1
naslouchala	naslouchat	k5eAaImAgFnS
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
také	také	k9
soukromou	soukromý	k2eAgFnSc7d1
recenzentkou	recenzentka	k1gFnSc7
jeho	jeho	k3xOp3gInPc2
prvních	první	k4xOgInPc2
literárních	literární	k2eAgInPc2d1
počinů	počin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgMnPc1d1
historik	historik	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Med	med	k1gInSc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Marie	Marie	k1gFnSc1
Patrovská	Patrovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hildebrandová	Hildebrandový	k2eAgFnSc1d1
<g/>
,	,	kIx,
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
německé	německý	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
z	z	k7c2
Varnsdorfu	Varnsdorf	k1gInSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byla	být	k5eAaImAgFnS
Němkou	Němka	k1gFnSc7
<g/>
,	,	kIx,
podporovala	podporovat	k5eAaImAgFnS
češství	češství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Med	med	k1gInSc1
se	se	k3xPyFc4
tedy	tedy	k9
přiklání	přiklánět	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
tyto	tento	k3xDgFnPc1
okolnosti	okolnost	k1gFnPc1
mohly	moct	k5eAaImAgFnP
vést	vést	k5eAaImF
k	k	k7c3
prvotní	prvotní	k2eAgFnSc3d1
Viktorově	Viktorův	k2eAgFnSc3d1
národnostní	národnostní	k2eAgFnSc3d1
toleranci	tolerance	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
nebyla	být	k5eNaImAgFnS
zrovna	zrovna	k6eAd1
typická	typický	k2eAgFnSc1d1
u	u	k7c2
všech	všecek	k3xTgMnPc2
jeho	jeho	k3xOp3gMnPc2
spolkových	spolkový	k2eAgMnPc2d1
kolegů	kolega	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Tuto	tento	k3xDgFnSc4
problematiku	problematika	k1gFnSc4
ale	ale	k8xC
blíže	blízce	k6eAd2
rozkryla	rozkrýt	k5eAaPmAgFnS
historička	historička	k1gFnSc1
Jaroslava	Jaroslava	k1gFnSc1
Honcová-Libická	Honcová-Libická	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Patrovská	Patrovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
smíšeného	smíšený	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
otec	otec	k1gMnSc1
Hynek	Hynek	k1gMnSc1
Hildebrand	Hildebrand	k1gInSc1
<g/>
,	,	kIx,
sládek	sládek	k1gMnSc1
v	v	k7c6
Měcholupech	Měcholup	k1gInPc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Čech	Čech	k1gMnSc1
a	a	k8xC
matka	matka	k1gFnSc1
Němka	Němka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
Hildenbrandových	Hildenbrandový	k2eAgInPc2d1
se	se	k3xPyFc4
vždy	vždy	k6eAd1
hlásila	hlásit	k5eAaImAgFnS
(	(	kIx(
<g/>
při	při	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
<g/>
)	)	kIx)
k	k	k7c3
české	český	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
a	a	k8xC
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
přestože	přestože	k8xS
vyrůstala	vyrůstat	k5eAaImAgFnS
ve	v	k7c6
zněmčeném	zněmčený	k2eAgNnSc6d1
pohraničí	pohraničí	k1gNnSc6
(	(	kIx(
<g/>
Varnsdorf	Varnsdorf	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Měcholupy	Měcholupa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
horlivou	horlivý	k2eAgFnSc7d1
vlastenkou	vlastenka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštěvovala	navštěvovat	k5eAaImAgFnS
sice	sice	k8xC
německé	německý	k2eAgFnSc2d1
školy	škola	k1gFnSc2
a	a	k8xC
němčinu	němčina	k1gFnSc4
tedy	tedy	k8xC
ovládala	ovládat	k5eAaImAgFnS
lépe	dobře	k6eAd2
(	(	kIx(
<g/>
a	a	k8xC
nezbavila	zbavit	k5eNaPmAgFnS
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
ani	ani	k8xC
německé	německý	k2eAgFnPc1d1
výslovnosti	výslovnost	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
češství	češství	k1gNnSc1
po	po	k7c6
otci	otec	k1gMnSc6
si	se	k3xPyFc3
upevnila	upevnit	k5eAaPmAgFnS
sňatkem	sňatek	k1gInSc7
s	s	k7c7
Čechem	Čech	k1gMnSc7
Richardem	Richard	k1gMnSc7
Petrovským	petrovský	k2eAgMnSc7d1
<g/>
,	,	kIx,
hospodářským	hospodářský	k2eAgMnSc7d1
úředníkem	úředník	k1gMnSc7
v	v	k7c6
Českém	český	k2eAgInSc6d1
Dubu	dub	k1gInSc6
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
držitelem	držitel	k1gMnSc7
vlastního	vlastní	k2eAgInSc2d1
statku	statek	k1gInSc2
ve	v	k7c6
Středicích	středicí	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Dykovy	Dykův	k2eAgFnSc2d1
pozůstalosti	pozůstalost	k1gFnSc2
dále	daleko	k6eAd2
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
(	(	kIx(
<g/>
dosti	dosti	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1
<g/>
)	)	kIx)
vzájemná	vzájemný	k2eAgFnSc1d1
korespondence	korespondence	k1gFnSc1
byla	být	k5eAaImAgFnS
vedena	vést	k5eAaImNgFnS
pouze	pouze	k6eAd1
německy	německy	k6eAd1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
zřejmě	zřejmě	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Patrovská	Patrovský	k2eAgFnSc1d1
vyvarovala	vyvarovat	k5eAaPmAgFnS
stylistických	stylistický	k2eAgFnPc2d1
a	a	k8xC
pravopisných	pravopisný	k2eAgFnPc2d1
chyb	chyba	k1gFnPc2
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
Viktor	Viktor	k1gMnSc1
i	i	k9
Ludvík	Ludvík	k1gMnSc1
často	často	k6eAd1
snažili	snažit	k5eAaImAgMnP
naučit	naučit	k5eAaPmF
správnou	správný	k2eAgFnSc4d1
výslovnost	výslovnost	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
marně	marně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vliv	vliv	k1gInSc1
na	na	k7c4
Viktora	Viktor	k1gMnSc4
a	a	k8xC
na	na	k7c4
celou	celý	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
měla	mít	k5eAaImAgFnS
Patrovská	Patrovský	k2eAgFnSc1d1
ale	ale	k8xC
značný	značný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
vzpomínek	vzpomínka	k1gFnPc2
JUDr.	JUDr.	kA
Františka	František	k1gMnSc2
Havrdy	Havrda	k1gMnSc2
<g/>
,	,	kIx,
pamětníka	pamětník	k1gMnSc2
a	a	k8xC
vzdáleného	vzdálený	k2eAgMnSc2d1
příbuzného	příbuzný	k1gMnSc2
rodiny	rodina	k1gFnSc2
Dykových	Dyková	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dozvídáme	dozvídat	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
babička	babička	k1gFnSc1
byla	být	k5eAaImAgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
rodinu	rodina	k1gFnSc4
stmelovalo	stmelovat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Marie	Marie	k1gFnSc1
Patrovská	Patrovský	k2eAgFnSc1d1
stála	stát	k5eAaImAgFnS
u	u	k7c2
Viktorovy	Viktorův	k2eAgFnSc2d1
juvenilní	juvenilní	k2eAgFnSc2d1
básnické	básnický	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
také	také	k6eAd1
oporou	opora	k1gFnSc7
v	v	k7c6
dobách	doba	k1gFnPc6
perzekucí	perzekuce	k1gFnPc2
za	za	k7c2
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
jmenování	jmenování	k1gNnSc6
Václava	Václav	k1gMnSc2
Dyka	Dyk	k1gInSc2
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
panství	panství	k1gNnSc2
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
i	i	k9
s	s	k7c7
babičkou	babička	k1gFnSc7
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Lobkovického	lobkovický	k2eAgInSc2d1
paláce	palác	k1gInSc2
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
často	často	k6eAd1
scházeli	scházet	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spolkovými	spolkový	k2eAgMnPc7d1
kolegy	kolega	k1gMnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zde	zde	k6eAd1
vedli	vést	k5eAaImAgMnP
revoluční	revoluční	k2eAgFnSc3d1
protihabsburské	protihabsburský	k2eAgFnSc3d1
řeči	řeč	k1gFnSc3
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
Václava	Václav	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
ale	ale	k8xC
musela	muset	k5eAaImAgFnS
rodina	rodina	k1gFnSc1
vyklidit	vyklidit	k5eAaPmF
služební	služební	k2eAgInSc4d1
byt	byt	k1gInSc4
v	v	k7c6
paláci	palác	k1gInSc6
a	a	k8xC
přestěhovat	přestěhovat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
nového	nový	k2eAgInSc2d1
činžovního	činžovní	k2eAgInSc2d1
domu	dům	k1gInSc2
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
v	v	k7c6
Letohradské	letohradský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
(	(	kIx(
<g/>
dn	dn	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dykova	Dykův	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
zde	zde	k6eAd1
bydlel	bydlet	k5eAaImAgMnS
Viktor	Viktor	k1gMnSc1
s	s	k7c7
rodinou	rodina	k1gFnSc7
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Dyk	Dyk	k?
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Václav	Václav	k1gMnSc1
Dick	Dick	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
–	–	k?
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hedvika	Hedvika	k1gFnSc1
Dicková	Dicková	k1gFnSc1
<g/>
,	,	kIx,
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrovská	Patrovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
(	(	kIx(
<g/>
1853	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Patrovský	Patrovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
dědeček	dědeček	k1gMnSc1
(	(	kIx(
<g/>
1812	#num#	k4
<g/>
–	–	k?
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Patrovská	Patrovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hildebrandová	Hildebrandový	k2eAgFnSc1d1
<g/>
,	,	kIx,
babička	babička	k1gFnSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odbojová	odbojový	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
s	s	k7c7
Jaroslavem	Jaroslav	k1gMnSc7
Vlčkem	Vlček	k1gMnSc7
(	(	kIx(
<g/>
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
s	s	k7c7
Jaroslavem	Jaroslav	k1gMnSc7
Kamperem	Kamper	k1gMnSc7
<g/>
)	)	kIx)
na	na	k7c6
redigování	redigování	k1gNnSc6
časopisu	časopis	k1gInSc2
Lumír	Lumír	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
od	od	k7c2
J.	J.	kA
V.	V.	kA
Sládka	Sládek	k1gMnSc2
zakoupil	zakoupit	k5eAaPmAgMnS
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
nakladatel	nakladatel	k1gMnSc1
Jan	Jan	k1gMnSc1
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
redigoval	redigovat	k5eAaImAgInS
také	také	k9
časopis	časopis	k1gInSc1
Samostatnost	samostatnost	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
publikoval	publikovat	k5eAaBmAgMnS
své	svůj	k3xOyFgInPc4
četné	četný	k2eAgInPc4d1
články	článek	k1gInPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
kritizoval	kritizovat	k5eAaImAgInS
vládní	vládní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protože	protože	k8xS
válečná	válečný	k2eAgFnSc1d1
cenzura	cenzura	k1gFnSc1
byla	být	k5eAaImAgFnS
mnohem	mnohem	k6eAd1
přísnější	přísný	k2eAgFnSc1d2
než	než	k8xS
tiskové	tiskový	k2eAgFnPc1d1
restrikce	restrikce	k1gFnPc1
před	před	k7c7
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
odpovědný	odpovědný	k2eAgMnSc1d1
redaktor	redaktor	k1gMnSc1
dostával	dostávat	k5eAaImAgMnS
jednu	jeden	k4xCgFnSc4
výstrahu	výstraha	k1gFnSc4
za	za	k7c7
druhou	druhý	k4xOgFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
úplného	úplný	k2eAgNnSc2d1
zastavení	zastavení	k1gNnSc2
listu	list	k1gInSc2
v	v	k7c6
září	září	k1gNnSc6
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžitě	okamžitě	k6eAd1
po	po	k7c6
zastavení	zastavení	k1gNnSc6
Samostatnosti	samostatnost	k1gFnSc2
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
časopisu	časopis	k1gInSc2
Lumír	Lumír	k1gInSc1
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yQgMnSc2,k3yRgMnSc2,k3yIgMnSc2
vytvořil	vytvořit	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
baštu	bašta	k1gFnSc4
odboje	odboj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obava	obava	k1gFnSc1
z	z	k7c2
jeho	jeho	k3xOp3gNnPc2
uvěznění	uvěznění	k1gNnPc2
dokonce	dokonce	k9
přiměla	přimět	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
Masaryka	Masaryk	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
již	již	k6eAd1
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
zahraničním	zahraniční	k2eAgInSc6d1
exilu	exil	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
aby	aby	kYmCp3nP
prostřednictvím	prostřednictvím	k7c2
Beneše	Beneš	k1gMnSc2
přemluvil	přemluvit	k5eAaPmAgMnS
Dyka	Dykus	k1gMnSc4
k	k	k7c3
odchodu	odchod	k1gInSc3
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
počáteční	počáteční	k2eAgFnPc4d1
výhrady	výhrada	k1gFnPc4
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
falešný	falešný	k2eAgInSc4d1
bulharský	bulharský	k2eAgInSc4d1
pas	pas	k1gInSc4
odcestuje	odcestovat	k5eAaPmIp3nS
do	do	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémy	problém	k1gInPc4
s	s	k7c7
doručením	doručení	k1gNnSc7
dokladu	doklad	k1gInSc2
ale	ale	k8xC
nakonec	nakonec	k6eAd1
donutily	donutit	k5eAaPmAgFnP
Dyka	Dyk	k2eAgMnSc4d1
zůstat	zůstat	k5eAaPmF
doma	doma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
června	červen	k1gInSc2
1915	#num#	k4
začal	začít	k5eAaPmAgInS
na	na	k7c6
stránkách	stránka	k1gFnPc6
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
zveřejňovat	zveřejňovat	k5eAaImF
na	na	k7c6
pokračování	pokračování	k1gNnSc6
svůj	svůj	k3xOyFgInSc4
román	román	k1gInSc4
Tajemná	tajemný	k2eAgNnPc4d1
dobrodružství	dobrodružství	k1gNnSc4
Alexeje	Alexej	k1gMnSc2
Iványče	Iványč	k1gInSc2
Kozulinova	Kozulinův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Líčením	líčení	k1gNnSc7
poměrů	poměr	k1gInPc2
v	v	k7c6
carském	carský	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
chtěl	chtít	k5eAaImAgInS
čtenáři	čtenář	k1gMnSc3
zprostředkovat	zprostředkovat	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
náhled	náhled	k1gInSc4
na	na	k7c4
nesvobodný	svobodný	k2eNgInSc4d1
život	život	k1gInSc4
v	v	k7c6
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byly	být	k5eAaImAgFnP
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
na	na	k7c4
čtrnáct	čtrnáct	k4xCc4
dní	den	k1gInPc2
zastaveny	zastavit	k5eAaPmNgFnP
a	a	k8xC
šéfredaktor	šéfredaktor	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
byl	být	k5eAaImAgMnS
spolu	spolu	k6eAd1
s	s	k7c7
Dykem	Dyk	k1gInSc7
postaven	postavit	k5eAaPmNgInS
před	před	k7c4
soud	soud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dyk	Dyk	k?
ve	v	k7c6
psaní	psaní	k1gNnSc6
protidynastických	protidynastický	k2eAgInPc2d1
článků	článek	k1gInPc2
ani	ani	k8xC
poté	poté	k6eAd1
neustal	ustat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
činnost	činnost	k1gFnSc1
a	a	k8xC
také	také	k9
vykonstruované	vykonstruovaný	k2eAgNnSc4d1
obvinění	obvinění	k1gNnSc4
z	z	k7c2
vlastizrady	vlastizrada	k1gFnSc2
ho	on	k3xPp3gNnSc4
přivedly	přivést	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
do	do	k7c2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
žaláři	žalář	k1gInSc6
se	se	k3xPyFc4
pod	pod	k7c7
dojmem	dojem	k1gInSc7
vývoje	vývoj	k1gInSc2
událostí	událost	k1gFnPc2
zrodila	zrodit	k5eAaPmAgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
1917	#num#	k4
pravděpodobně	pravděpodobně	k6eAd1
nejznámější	známý	k2eAgFnSc1d3
Dykova	Dykův	k2eAgFnSc1d1
báseň	báseň	k1gFnSc1
Země	zem	k1gFnSc2
mluví	mluvit	k5eAaImIp3nS
<g/>
,	,	kIx,
nepřímo	přímo	k6eNd1
vyzývající	vyzývající	k2eAgMnPc4d1
české	český	k2eAgMnPc4d1
poslance	poslanec	k1gMnPc4
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
svolané	svolaný	k2eAgFnPc1d1
poprvé	poprvé	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
historicky	historicky	k6eAd1
osudových	osudový	k2eAgInPc6d1
okamžicích	okamžik	k1gInPc6
nezapomněli	zapomnět	k5eNaImAgMnP,k5eNaPmAgMnP
na	na	k7c6
svoji	svůj	k3xOyFgFnSc4
odpovědnost	odpovědnost	k1gFnSc4
vůči	vůči	k7c3
národu	národ	k1gInSc3
a	a	k8xC
vlasti	vlast	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
samovazbě	samovazba	k1gFnSc6
překládal	překládat	k5eAaImAgInS
francouzské	francouzský	k2eAgMnPc4d1
básníky	básník	k1gMnPc4
<g/>
,	,	kIx,
především	především	k9
Baudelaira	Baudelaira	k1gMnSc1
a	a	k8xC
Verlaina	Verlaina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korespondenčně	korespondenčně	k6eAd1
také	také	k9
připojil	připojit	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
podpis	podpis	k1gInSc4
k	k	k7c3
manifestu	manifest	k1gInSc2
spisovatelů	spisovatel	k1gMnPc2
z	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1917	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
byl	být	k5eAaImAgMnS
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1917	#num#	k4
propuštěn	propustit	k5eAaPmNgMnS
na	na	k7c4
svobodu	svoboda	k1gFnSc4
<g/>
,	,	kIx,
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
dohnat	dohnat	k5eAaPmF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k9
rokem	rok	k1gInSc7
stráveným	strávený	k2eAgInSc7d1
ve	v	k7c6
vězení	vězení	k1gNnSc6
zameškal	zameškat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
mnohem	mnohem	k6eAd1
radikálnější	radikální	k2eAgMnSc1d2
než	než	k8xS
před	před	k7c7
zatčením	zatčení	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
hroutící	hroutící	k2eAgMnSc1d1
se	se	k3xPyFc4
rakouský	rakouský	k2eAgInSc1d1
kolos	kolos	k1gInSc1
už	už	k9
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
nedokázal	dokázat	k5eNaPmAgMnS
nijak	nijak	k6eAd1
zakročit	zakročit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
po	po	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
politická	politický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
započala	započnout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Státoprávně	státoprávně	k6eAd1
pokrokové	pokrokový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
neúspěšně	úspěšně	k6eNd1
za	za	k7c4
tuto	tento	k3xDgFnSc4
stranu	strana	k1gFnSc4
kandidoval	kandidovat	k5eAaImAgMnS
ve	v	k7c6
vinohradském	vinohradský	k2eAgInSc6d1
volebním	volební	k2eAgInSc6d1
okrese	okres	k1gInSc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1918	#num#	k4
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c6
založení	založení	k1gNnSc6
Československé	československý	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
získal	získat	k5eAaPmAgInS
za	za	k7c4
národní	národní	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
poslanecké	poslanecký	k2eAgNnSc1d1
křeslo	křeslo	k1gNnSc1
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
za	za	k7c4
tuto	tento	k3xDgFnSc4
stranu	strana	k1gFnSc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
senátu	senát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Mandát	mandát	k1gInSc1
obhájil	obhájit	k5eAaPmAgInS
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
senátu	senát	k1gInSc6
setrval	setrvat	k5eAaPmAgInS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
ho	on	k3xPp3gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Kapras	Kapras	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
orientován	orientovat	k5eAaBmNgInS
pravicově	pravicově	k6eAd1
a	a	k8xC
nacionalisticky	nacionalisticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
mezi	mezi	k7c4
nejvýraznější	výrazný	k2eAgMnPc4d3
prvorepublikové	prvorepublikový	k2eAgMnPc4d1
odpůrce	odpůrce	k1gMnPc4
tzv.	tzv.	kA
hradní	hradní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
především	především	k9
jejích	její	k3xOp3gMnPc2
tvůrců	tvůrce	k1gMnPc2
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
a	a	k8xC
Edvarda	Edvard	k1gMnSc2
Beneše	Beneš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příležitostně	příležitostně	k6eAd1
psal	psát	k5eAaImAgInS
do	do	k7c2
časopisu	časopis	k1gInSc2
Vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
vydávalo	vydávat	k5eAaPmAgNnS,k5eAaImAgNnS
hnutí	hnutí	k1gNnSc1
Vlajka	vlajka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
politickou	politický	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
šla	jít	k5eAaImAgFnS
ruku	ruka	k1gFnSc4
v	v	k7c6
ruce	ruka	k1gFnSc6
i	i	k8xC
jeho	jeho	k3xOp3gFnSc1
tvorba	tvorba	k1gFnSc1
literární	literární	k2eAgFnSc1d1
<g/>
,	,	kIx,
nejmarkantnější	markantní	k2eAgInSc1d3
příklad	příklad	k1gInSc1
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
v	v	k7c6
knize	kniha	k1gFnSc6
Prohrané	prohraný	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
shrnuje	shrnovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
prozaickou	prozaický	k2eAgFnSc4d1
a	a	k8xC
lyrickou	lyrický	k2eAgFnSc4d1
předvolební	předvolební	k2eAgFnSc4d1
a	a	k8xC
povolební	povolební	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
v	v	k7c6
období	období	k1gNnSc6
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
lyrika	lyrika	k1gFnSc1
zaměřovala	zaměřovat	k5eAaImAgFnS
především	především	k9
na	na	k7c4
útoky	útok	k1gInPc4
proti	proti	k7c3
politickým	politický	k2eAgMnPc3d1
oponentům	oponent	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obětování	obětování	k1gNnSc1
literární	literární	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
politickému	politický	k2eAgInSc3d1
pamfletismu	pamfletismus	k1gInSc3
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
například	například	k6eAd1
z	z	k7c2
básně	báseň	k1gFnSc2
Svatá	svatý	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
kritizoval	kritizovat	k5eAaImAgMnS
účelové	účelový	k2eAgNnSc1d1
vytvoření	vytvoření	k1gNnSc1
předvolebního	předvolební	k2eAgInSc2d1
kartelu	kartel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zneužívání	zneužívání	k1gNnSc1
veršů	verš	k1gInPc2
k	k	k7c3
potírání	potírání	k1gNnSc3
oponentů	oponent	k1gMnPc2
Dykovi	Dyka	k1gMnSc3
vytýkal	vytýkat	k5eAaImAgMnS
například	například	k6eAd1
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gInSc2
Dyk	Dyk	k?
utvořil	utvořit	k5eAaPmAgMnS
z	z	k7c2
poezie	poezie	k1gFnSc2
služku	služka	k1gFnSc4
politiky	politika	k1gFnSc2
a	a	k8xC
divil	divit	k5eAaImAgMnS
se	se	k3xPyFc4
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k8xC,k8xS
nacionalista	nacionalista	k1gMnSc1
tělem	tělo	k1gNnSc7
a	a	k8xC
duší	duše	k1gFnPc2
nedokázal	dokázat	k5eNaPmAgMnS
svou	svůj	k3xOyFgFnSc4
básnickou	básnický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
adresovat	adresovat	k5eAaBmF
především	především	k9
národu	národ	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
jí	on	k3xPp3gFnSc3
především	především	k6eAd1
vracel	vracet	k5eAaImAgMnS
rány	rána	k1gFnPc4
politickým	politický	k2eAgMnPc3d1
protivníkům	protivník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
napadali	napadat	k5eAaBmAgMnP,k5eAaPmAgMnP,k5eAaImAgMnP
národně	národně	k6eAd1
demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
byl	být	k5eAaImAgMnS
členem	člen	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
vedle	vedle	k7c2
politiky	politika	k1gFnSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
mu	on	k3xPp3gNnSc3
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
zemřela	zemřít	k5eAaPmAgFnS
babička	babička	k1gFnSc1
Marie	Maria	k1gFnSc2
Patrovská	Patrovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
opatrovatelka	opatrovatelka	k1gFnSc1
ve	v	k7c6
studentských	studentský	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
50	#num#	k4
let	léto	k1gNnPc2
bratr	bratr	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
(	(	kIx(
<g/>
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
majitel	majitel	k1gMnSc1
tiskárny	tiskárna	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
redaktor	redaktor	k1gMnSc1
týdeníku	týdeník	k1gInSc2
Demokrat	demokrat	k1gMnSc1
a	a	k8xC
tajemník	tajemník	k1gMnSc1
strany	strana	k1gFnSc2
národně	národně	k6eAd1
demokratické	demokratický	k2eAgFnPc4d1
v	v	k7c6
Praze-Vinohradech	Praze-Vinohrad	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
začal	začít	k5eAaPmAgInS
pravidelně	pravidelně	k6eAd1
trávit	trávit	k5eAaImF
léto	léto	k1gNnSc4
v	v	k7c6
Harrachově	Harrachov	k1gInSc6
s	s	k7c7
rodinami	rodina	k1gFnPc7
Hanuše	Hanuš	k1gMnSc2
Jelínka	Jelínek	k1gMnSc2
a	a	k8xC
Rudolfa	Rudolf	k1gMnSc2
Medka	Medek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
navštívil	navštívit	k5eAaPmAgMnS
také	také	k9
Polsko	Polsko	k1gNnSc4
<g/>
,	,	kIx,
Litvu	Litva	k1gFnSc4
a	a	k8xC
již	již	k6eAd1
podruhé	podruhé	k6eAd1
Francii	Francie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
řádným	řádný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
České	český	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
sedmadvacetileté	sedmadvacetiletý	k2eAgFnSc6d1
známosti	známost	k1gFnSc6
oženil	oženit	k5eAaPmAgMnS
se	s	k7c7
spisovatelkou	spisovatelka	k1gFnSc7
<g/>
,	,	kIx,
překladatelkou	překladatelka	k1gFnSc7
ze	z	k7c2
slovinštiny	slovinština	k1gFnSc2
a	a	k8xC
novinářkou	novinářka	k1gFnSc7
Zdenkou	Zdenka	k1gFnSc7
Háskovou	Hásková	k1gFnSc7
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
spolu	spolu	k6eAd1
podnikli	podniknout	k5eAaPmAgMnP
cestu	cesta	k1gFnSc4
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc7
synovcem	synovec	k1gMnSc7
byl	být	k5eAaImAgMnS
předválečný	předválečný	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
Viktor	Viktor	k1gMnSc1
Kripner	Kripner	k1gMnSc1
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
autor	autor	k1gMnSc1
několika	několik	k4yIc2
básnických	básnický	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
byl	být	k5eAaImAgMnS
šachistou	šachista	k1gMnSc7
<g/>
,	,	kIx,
členem	člen	k1gMnSc7
Českého	český	k2eAgInSc2d1
spolku	spolek	k1gInSc2
šachovního	šachovní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přátelil	přátelit	k5eAaImAgMnS
se	se	k3xPyFc4
s	s	k7c7
prvním	první	k4xOgMnSc7
českým	český	k2eAgMnSc7d1
velmistrem	velmistr	k1gMnSc7
Oldřichem	Oldřich	k1gMnSc7
Durasem	Duras	k1gMnSc7
a	a	k8xC
také	také	k9
mu	on	k3xPp3gMnSc3
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
několik	několik	k4yIc4
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
jedné	jeden	k4xCgFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
v	v	k7c6
němčině	němčina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Několik	několik	k4yIc4
záznamů	záznam	k1gInPc2
Dykových	Dykův	k2eAgFnPc2d1
partií	partie	k1gFnPc2
(	(	kIx(
<g/>
mj.	mj.	kA
právě	právě	k9
s	s	k7c7
Durasem	Duras	k1gInSc7
a	a	k8xC
s	s	k7c7
Richardem	Richard	k1gMnSc7
Rétim	Rétim	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
dochovalo	dochovat	k5eAaPmAgNnS
a	a	k8xC
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k9
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náhrobní	náhrobní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
manželů	manžel	k1gMnPc2
Patrovských	Patrovský	k2eAgInPc2d1
<g/>
,	,	kIx,
dědečka	dědeček	k1gMnSc2
a	a	k8xC
babičky	babička	k1gFnSc2
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Rožďalovicích	Rožďalovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Manželé	manžel	k1gMnPc1
Dykovi	Dykův	k2eAgMnPc1d1
s	s	k7c7
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
s	s	k7c7
manželkou	manželka	k1gFnSc7
Zdeňkou	Zdeňka	k1gFnSc7
Háskovou-Dykovou	Háskovou-Dyková	k1gFnSc7
na	na	k7c6
lázeňské	lázeňský	k2eAgFnSc6d1
kolonádě	kolonáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Středy	střed	k1gInPc1
u	u	k7c2
Dyků	Dyk	k1gInPc2
<g/>
:	:	kIx,
zprava	zprava	k6eAd1
Zdeňka	Zdeňka	k1gFnSc1
Dyková	Dyková	k1gFnSc1
<g/>
,	,	kIx,
správce	správce	k1gMnSc1
Zafouk	Zafouk	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Opolský	opolský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kobliha	kobliha	k1gFnSc1
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zdeňka	Zdeňka	k1gFnSc1
Hásková-Dyková	Hásková-Dyková	k1gFnSc1
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
a	a	k8xC
pohřeb	pohřeb	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
se	se	k3xPyFc4
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
ubytoval	ubytovat	k5eAaPmAgMnS
v	v	k7c6
penziónu	penzión	k1gInSc6
Glavoviće	Glavović	k1gFnSc2
na	na	k7c6
ostrůvku	ostrůvek	k1gInSc6
Lopud	Lopuda	k1gFnPc2
blízko	blízko	k7c2
Dubrovníka	Dubrovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
byl	být	k5eAaImAgInS
raněn	ranit	k5eAaPmNgInS
srdeční	srdeční	k2eAgFnSc7d1
mrtvicí	mrtvice	k1gFnSc7
při	při	k7c6
koupání	koupání	k1gNnSc6
v	v	k7c6
moři	moře	k1gNnSc6
v	v	k7c6
zátoce	zátoka	k1gFnSc6
Šunj	Šunj	k1gInSc4
v	v	k7c6
Jaderském	jaderský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
lopudské	lopudský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
byl	být	k5eAaImAgMnS
dalšího	další	k2eAgInSc2d1
dne	den	k1gInSc2
vypraven	vypraven	k2eAgInSc4d1
pohřeb	pohřeb	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
mrtvé	mrtvý	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
bylo	být	k5eAaImAgNnS
přepraveno	přepravit	k5eAaPmNgNnS
vlakem	vlak	k1gInSc7
přes	přes	k7c4
Záhřeb	Záhřeb	k1gInSc4
do	do	k7c2
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Prahy	Praha	k1gFnSc2
přijel	přijet	k5eAaPmAgMnS
vlak	vlak	k1gInSc4
se	s	k7c7
zesnulým	zesnulý	k1gMnSc7
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
po	po	k7c6
poledni	poledne	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
všemi	všecek	k3xTgFnPc7
poctami	pocta	k1gFnPc7
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
rakev	rakev	k1gFnSc1
vystavena	vystavit	k5eAaPmNgFnS
v	v	k7c6
Pantheonu	Pantheon	k1gInSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
Dykem	Dyk	k1gInSc7
slavností	slavnost	k1gFnPc2
řečí	řeč	k1gFnPc2
rozloučil	rozloučit	k5eAaPmAgInS
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
voják	voják	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Medek	Medek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřben	pohřben	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
na	na	k7c6
pražských	pražský	k2eAgInPc6d1
Olšanských	olšanský	k2eAgInPc6d1
hřbitovech	hřbitov	k1gInPc6
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
rodinné	rodinný	k2eAgFnSc6d1
hrobce	hrobka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smuteční	smuteční	k2eAgInSc1d1
projev	projev	k1gInSc1
zde	zde	k6eAd1
měl	mít	k5eAaImAgInS
výtvarný	výtvarný	k2eAgInSc1d1
a	a	k8xC
literární	literární	k2eAgMnSc1d1
historik	historik	k1gMnSc1
a	a	k8xC
kritik	kritik	k1gMnSc1
prof.	prof.	kA
Arne	Arne	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
Dykově	Dykův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
legenda	legenda	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
že	že	k8xS
svou	svůj	k3xOyFgFnSc4
smrt	smrt	k1gFnSc4
předpověděl	předpovědět	k5eAaPmAgMnS
v	v	k7c6
básni	báseň	k1gFnSc6
Soumrak	soumrak	k1gInSc4
moře	moře	k1gNnSc2
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
ze	z	k7c2
sbírky	sbírka	k1gFnSc2
Devátá	devátý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Památník	památník	k1gInSc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Lopud	Lopuda	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
utopil	utopit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snímek	snímek	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
před	před	k7c7
renovací	renovace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
v	v	k7c6
Mělníku	Mělník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Hrob	hrob	k1gInSc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
,	,	kIx,
Olšanské	olšanský	k2eAgInPc1d1
hřbitovy	hřbitov	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Pohřební	pohřební	k2eAgInSc1d1
průvod	průvod	k1gInSc1
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
před	před	k7c7
Vinohradským	vinohradský	k2eAgNnSc7d1
divadlem	divadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Kresba	kresba	k1gFnSc1
Františka	František	k1gMnSc2
Gellnera	Gellner	k1gMnSc2
<g/>
:	:	kIx,
Arnošt	Arnošt	k1gMnSc1
Procházka	Procházka	k1gMnSc1
a	a	k8xC
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
.	.	kIx.
</s>
<s>
Citáty	citát	k1gInPc1
</s>
<s>
„	„	k?
</s>
<s>
Pan	Pan	k1gMnSc1
Dyk	Dyk	k?
je	být	k5eAaImIp3nS
z	z	k7c2
nejmladší	mladý	k2eAgFnSc2d3
naší	náš	k3xOp1gFnSc2
literatury	literatura	k1gFnSc2
člověk	člověk	k1gMnSc1
snad	snad	k9
nejoriginálnější	originální	k2eAgFnSc4d3
<g/>
,	,	kIx,
nejpoctivější	poctivý	k2eAgFnSc4d3
a	a	k8xC
snad	snad	k9
i	i	k9
nejsilnější	silný	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každým	každý	k3xTgInSc7
způsobem	způsob	k1gInSc7
člověk	člověk	k1gMnSc1
bolestně	bolestně	k6eAd1
a	a	k8xC
zhluboka	zhluboka	k6eAd1
pracující	pracující	k2eAgMnPc1d1
<g/>
,	,	kIx,
temně	temně	k6eAd1
a	a	k8xC
skřípavě	skřípavě	k6eAd1
smílající	smílající	k2eAgMnPc1d1
si	se	k3xPyFc3
na	na	k7c6
srdci	srdce	k1gNnSc6
všecko	všecek	k3xTgNnSc1
kamení	kamenit	k5eAaImIp3nS
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
nasbíral	nasbírat	k5eAaPmAgMnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
české	český	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
někde	někde	k6eAd1
zatrhliny	zatrhlina	k1gFnSc2
a	a	k8xC
uzle	uzel	k1gInSc5
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
verších	verš	k1gInPc6
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
jich	on	k3xPp3gMnPc2
abruptnost	abruptnost	k1gFnSc4
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
skřipot	skřipot	k1gInSc1
a	a	k8xC
baroknost	baroknost	k1gFnSc1
–	–	k?
ale	ale	k8xC
také	také	k9
často	často	k6eAd1
linie	linie	k1gFnPc1
tak	tak	k6eAd1
slavně	slavně	k6eAd1
a	a	k8xC
ryze	ryze	k6eAd1
veliká	veliký	k2eAgFnSc1d1
<g/>
,	,	kIx,
obrys	obrys	k1gInSc1
tak	tak	k9
královsky	královsky	k6eAd1
tragický	tragický	k2eAgMnSc1d1
a	a	k8xC
vroucně	vroucně	k6eAd1
dechnutý	dechnutý	k2eAgInSc1d1
<g/>
,	,	kIx,
auroela	auroet	k5eAaPmAgFnS,k5eAaBmAgFnS,k5eAaImAgFnS
tak	tak	k6eAd1
bledá	bledý	k2eAgFnSc1d1
a	a	k8xC
vroucně	vroucně	k6eAd1
mlčící	mlčící	k2eAgFnSc1d1
<g/>
,	,	kIx,
jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
zavěsit	zavěsit	k5eAaPmF
jen	jen	k9
na	na	k7c4
hlavy	hlava	k1gFnPc4
zmučené	zmučený	k2eAgFnPc1d1
horečkami	horečka	k1gFnPc7
vlastního	vlastní	k2eAgNnSc2d1
bytí	bytí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všude	všude	k6eAd1
<g/>
,	,	kIx,
kdekoli	kdekoli	k6eAd1
nitro	nitro	k1gNnSc1
je	být	k5eAaImIp3nS
rytířsky	rytířsky	k6eAd1
a	a	k8xC
věrně	věrně	k6eAd1
dáno	dát	k5eAaPmNgNnS
do	do	k7c2
služeb	služba	k1gFnPc2
poctivosti	poctivost	k1gFnSc2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
si	se	k3xPyFc3
toto	tento	k3xDgNnSc4
nalézt	nalézt	k5eAaPmF,k5eAaBmF
komplementární	komplementární	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
,	,	kIx,
dikci	dikce	k1gFnSc3
a	a	k8xC
stylu	styl	k1gInSc3
a	a	k8xC
tou	ten	k3xDgFnSc7
je	být	k5eAaImIp3nS
symbolická	symbolický	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
a	a	k8xC
vykvašená	vykvašený	k2eAgFnSc1d1
linie	linie	k1gFnSc1
tragiky	tragika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Náhle	náhle	k6eAd1
slyšíme	slyšet	k5eAaImIp1nP
hlas	hlas	k1gInSc4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Dyk	Dyk	k?
se	se	k3xPyFc4
utopil	utopit	k5eAaPmAgMnS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
A	a	k9
vzápětí	vzápětí	k6eAd1
sto	sto	k4xCgNnSc1
hlasů	hlas	k1gInPc2
–	–	k?
či	či	k8xC
jich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
tisíc	tisíc	k4xCgInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
propuká	propukat	k5eAaImIp3nS
v	v	k7c4
bouřlivé	bouřlivý	k2eAgNnSc4d1
volání	volání	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
jako	jako	k9
by	by	kYmCp3nS
se	se	k3xPyFc4
mísilo	mísit	k5eAaImAgNnS
s	s	k7c7
ozvěnou	ozvěna	k1gFnSc7
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Dyk	Dyk	k?
se	se	k3xPyFc4
utopil	utopit	k5eAaPmAgMnS
<g/>
!	!	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
je	být	k5eAaImIp3nS
mrtev	mrtev	k2eAgMnSc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
Je	být	k5eAaImIp3nS
snad	snad	k9
zbytečné	zbytečný	k2eAgNnSc1d1
líčit	líčit	k5eAaImF
naše	náš	k3xOp1gNnSc4
ohromení	ohromení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyli	být	k5eNaImAgMnP
jsme	být	k5eAaImIp1nP
schopni	schopen	k2eAgMnPc1d1
slova	slovo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dav	Dav	k1gInSc1
utichl	utichnout	k5eAaPmAgInS
a	a	k8xC
mlčky	mlčky	k6eAd1
nás	my	k3xPp1nPc4
vedl	vést	k5eAaImAgInS
do	do	k7c2
malé	malý	k2eAgFnSc2d1
<g/>
,	,	kIx,
nízké	nízký	k2eAgFnSc2d1
místnosti	místnost	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
ležel	ležet	k5eAaImAgMnS
na	na	k7c6
stole	stol	k1gInSc6
Dyk	Dyk	k?
<g/>
,	,	kIx,
přikryt	přikryt	k2eAgMnSc1d1
prostěradlem	prostěradlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdosi	kdosi	k3yInSc1
zvedl	zvednout	k5eAaPmAgMnS
lucernu	lucerna	k1gFnSc4
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
na	na	k7c4
něj	on	k3xPp3gMnSc4
lépe	dobře	k6eAd2
viděli	vidět	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrt	smrt	k1gFnSc1
už	už	k6eAd1
odešla	odejít	k5eAaPmAgFnS
<g/>
,	,	kIx,
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
tváře	tvář	k1gFnSc2
zmizela	zmizet	k5eAaPmAgFnS
hrůza	hrůza	k1gFnSc1
a	a	k8xC
utrpení	utrpení	k1gNnSc4
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
jen	jen	k9
ticho	ticho	k1gNnSc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
těžké	těžký	k2eAgNnSc1d1
<g/>
,	,	kIx,
mramorové	mramorový	k2eAgNnSc1d1
ticho	ticho	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
padá	padat	k5eAaImIp3nS
na	na	k7c4
srdce	srdce	k1gNnSc4
živých	živý	k1gMnPc2
<g/>
...	...	k?
<g/>
Takový	takový	k3xDgMnSc1
byl	být	k5eAaImAgMnS
konec	konec	k1gInSc4
básníka	básník	k1gMnSc2
Deváté	devátý	k4xOgFnSc2
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Zdeněk	Zdeněk	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Dyk	Dyk	k?
často	často	k6eAd1
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
aforistickou	aforistický	k2eAgFnSc4d1
úsečnost	úsečnost	k1gFnSc4
<g/>
,	,	kIx,
satiru	satira	k1gFnSc4
a	a	k8xC
pravidelný	pravidelný	k2eAgInSc4d1
rytmický	rytmický	k2eAgInSc4d1
verš	verš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc1
díla	dílo	k1gNnPc1
zpravidla	zpravidla	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
jasnou	jasný	k2eAgFnSc4d1
pointu	pointa	k1gFnSc4
<g/>
,	,	kIx,
využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
paradoxy	paradox	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
literární	literární	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
značnou	značný	k2eAgFnSc7d1
skepsí	skepse	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pramení	pramenit	k5eAaImIp3nS
z	z	k7c2
potlačení	potlačení	k1gNnSc2
omladinářských	omladinářský	k2eAgFnPc2d1
bouří	bouř	k1gFnPc2
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbírky	sbírka	k1gFnPc1
jsou	být	k5eAaImIp3nP
spojovány	spojovat	k5eAaImNgFnP
s	s	k7c7
tvorbou	tvorba	k1gFnSc7
skupiny	skupina	k1gFnSc2
soustředěné	soustředěný	k2eAgFnSc2d1
kolem	kolem	k7c2
Moderní	moderní	k2eAgFnSc2d1
revue	revue	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poezie	poezie	k1gFnSc1
</s>
<s>
Od	od	k7c2
brány	brána	k1gFnSc2
pekelné	pekelný	k2eAgFnSc2d1
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
A	a	k9
porta	porta	k1gFnSc1
inferi	inferi	k1gNnSc2
<g/>
,	,	kIx,
raná	raný	k2eAgFnSc1d1
subjektivní	subjektivní	k2eAgFnSc1d1
lyrika	lyrika	k1gFnSc1
<g/>
,	,	kIx,
název	název	k1gInSc1
podle	podle	k7c2
latinského	latinský	k2eAgInSc2d1
žalmu	žalm	k1gInSc2
zpívaného	zpívaný	k2eAgInSc2d1
při	při	k7c6
pohřbu	pohřeb	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Síla	síla	k1gFnSc1
života	život	k1gInSc2
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marnosti	marnost	k1gFnPc1
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Satiry	satira	k1gFnPc1
a	a	k8xC
sarkasmy	sarkasmus	k1gInPc1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
,	,	kIx,
politická	politický	k2eAgFnSc1d1
satirická	satirický	k2eAgFnSc1d1
poezie	poezie	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
prvním	první	k4xOgInSc6
oddíle	oddíl	k1gInSc6
této	tento	k3xDgFnSc2
sbírky	sbírka	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
řada	řada	k1gFnSc1
epitafů	epitaf	k1gInPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
básník	básník	k1gMnSc1
charakterizoval	charakterizovat	k5eAaBmAgMnS
své	svůj	k3xOyFgMnPc4
literární	literární	k2eAgMnPc4d1
přátele	přítel	k1gMnPc4
<g/>
,	,	kIx,
nepřátele	nepřítel	k1gMnPc4
i	i	k8xC
sebe	sebe	k3xPyFc4
<g/>
:	:	kIx,
<g/>
"	"	kIx"
<g/>
Viz	vidět	k5eAaImRp2nS
tady	tady	k6eAd1
kosti	kost	k1gFnPc1
Dyka	Dykus	k1gMnSc2
Viktora	Viktor	k1gMnSc2
<g/>
.	.	kIx.
/	/	kIx~
Za	za	k7c4
živa	živ	k2eAgMnSc4d1
býval	bývat	k5eAaImAgInS
velká	velký	k2eAgFnSc1d1
potvora	potvora	k1gFnSc1
<g/>
.	.	kIx.
/	/	kIx~
I	i	k9
on	on	k3xPp3gMnSc1
pil	pít	k5eAaImAgMnS
hodně	hodně	k6eAd1
z	z	k7c2
poezie	poezie	k1gFnSc2
studny	studna	k1gFnSc2
<g/>
.	.	kIx.
/	/	kIx~
Pěstoval	pěstovat	k5eAaImAgMnS
žánry	žánr	k1gInPc4
všecky	všecek	k3xTgInPc1
<g/>
:	:	kIx,
zvláště	zvláště	k6eAd1
ten	ten	k3xDgInSc1
nudný	nudný	k2eAgInSc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
/	/	kIx~
Duch	duch	k1gMnSc1
neklidný	klidný	k2eNgMnSc1d1
<g/>
…	…	k?
<g/>
či	či	k8xC
rýpal	rýpal	k1gMnSc1
<g/>
,	,	kIx,
jek	jek	k1gInSc1
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
/	/	kIx~
ryl	rýt	k5eAaImAgMnS
do	do	k7c2
Machara	Machar	k1gMnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
do	do	k7c2
Hladíka	Hladík	k1gMnSc2
<g/>
.	.	kIx.
/	/	kIx~
I	i	k9
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
hrobě	hrob	k1gInSc6
pokoušel	pokoušet	k5eAaImAgMnS
se	se	k3xPyFc4
rýti	rýt	k5eAaImF
<g/>
.	.	kIx.
/	/	kIx~
Světlo	světlo	k1gNnSc1
věčné	věčný	k2eAgNnSc1d1
ať	ať	k8xC,k8xS
mu	on	k3xPp3gMnSc3
svítí	svítit	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
–	–	k?
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Milá	milá	k1gFnSc1
sedmi	sedm	k4xCc2
loupežníků	loupežník	k1gMnPc2
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
,	,	kIx,
lyrickoepická	lyrickoepický	k2eAgFnSc1d1
poema	poema	k1gFnSc1
(	(	kIx(
<g/>
balada	balada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
formou	forma	k1gFnSc7
dialogu	dialog	k1gInSc2
<g/>
,	,	kIx,
kult	kult	k1gInSc1
síly	síla	k1gFnSc2
a	a	k8xC
vášně	vášeň	k1gFnSc2
<g/>
,	,	kIx,
vliv	vliv	k1gInSc4
romantismu	romantismus	k1gInSc2
a	a	k8xC
anarchismu	anarchismus	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Pohádky	pohádka	k1gFnPc1
z	z	k7c2
naší	náš	k3xOp1gFnSc2
vesnice	vesnice	k1gFnSc2
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
,	,	kIx,
politická	politický	k2eAgFnSc1d1
satirická	satirický	k2eAgFnSc1d1
lyrika	lyrika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS
Moro	mora	k1gFnSc5
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zápas	zápas	k1gInSc1
Jiřího	Jiří	k1gMnSc2
Macků	Macků	k1gMnSc2
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Noci	noc	k1gFnPc1
chiméry	chiméra	k1gFnSc2
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
,	,	kIx,
subjektivní	subjektivní	k2eAgFnSc1d1
lyrika	lyrika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Buřiči	buřič	k1gMnPc1
a	a	k8xC
smíření	smířený	k2eAgMnPc1d1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
oddíly	oddíl	k1gInPc4
Buřiči	Buřič	k1gMnPc7
<g/>
,	,	kIx,
Milá	milá	k1gFnSc1
sedmi	sedm	k4xCc2
loupežníků	loupežník	k1gMnPc2
<g/>
,	,	kIx,
Giuseppe	Giusepp	k1gInSc5
Moro	mora	k1gFnSc5
</s>
<s>
Devátá	devátý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
,	,	kIx,
melancholie	melancholie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Následující	následující	k2eAgFnPc1d1
čtyři	čtyři	k4xCgFnPc1
sbírky	sbírka	k1gFnPc1
patří	patřit	k5eAaImIp3nP
do	do	k7c2
tzv.	tzv.	kA
válečné	válečný	k2eAgFnSc2d1
tetralogie	tetralogie	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
hlavní	hlavní	k2eAgFnSc1d1
myšlenkou	myšlenka	k1gFnSc7
je	být	k5eAaImIp3nS
národní	národní	k2eAgFnSc4d1
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc4d1
<g/>
)	)	kIx)
samostatnost	samostatnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dyk	Dyk	k?
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgInS
inspirovat	inspirovat	k5eAaBmF
1	#num#	k4
<g/>
.	.	kIx.
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzývá	vyzývat	k5eAaImIp3nS
k	k	k7c3
odvaze	odvaha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjadřuje	vyjadřovat	k5eAaImIp3nS
také	také	k9
obavy	obava	k1gFnPc4
o	o	k7c4
osud	osud	k1gInSc4
národa	národ	k1gInSc2
a	a	k8xC
varuje	varovat	k5eAaImIp3nS
před	před	k7c7
zradou	zrada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Lehké	Lehké	k2eAgInPc1d1
a	a	k8xC
těžké	těžký	k2eAgInPc1d1
kroky	krok	k1gInPc1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Anebo	anebo	k8xC
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Okno	okno	k1gNnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Poslední	poslední	k2eAgInSc1d1
rok	rok	k1gInSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Próza	próza	k1gFnSc1
</s>
<s>
Stud	stud	k1gInSc1
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hučí	hučet	k5eAaImIp3nS
jez	jez	k1gInSc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
prózy	próza	k1gFnPc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Konec	konec	k1gInSc1
Hackenschmidův	Hackenschmidův	k2eAgInSc1d1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
,	,	kIx,
román	román	k1gInSc4
z	z	k7c2
cyklu	cyklus	k1gInSc2
"	"	kIx"
<g/>
Akta	akta	k1gNnPc4
působnosti	působnost	k1gFnSc2
Čertova	čertův	k2eAgNnSc2d1
kopyta	kopyto	k1gNnSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
Prosinec	prosinec	k1gInSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
,	,	kIx,
román	román	k1gInSc4
z	z	k7c2
cyklu	cyklus	k1gInSc2
"	"	kIx"
<g/>
Akta	akta	k1gNnPc4
působnosti	působnost	k1gFnSc2
Čertova	čertův	k2eAgNnSc2d1
kopyta	kopyto	k1gNnSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
Prsty	prst	k1gInPc1
Habakukovy	Habakukův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
,	,	kIx,
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Píseň	píseň	k1gFnSc1
o	o	k7c6
vrbě	vrba	k1gFnSc6
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
povídek	povídka	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Příhody	příhoda	k1gFnPc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
povídek	povídka	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Krysař	krysař	k1gMnSc1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
,	,	kIx,
novela	novela	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
čerpá	čerpat	k5eAaImIp3nS
ze	z	k7c2
staroněmecké	staroněmecký	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
shodě	shoda	k1gFnSc6
s	s	k7c7
romantismem	romantismus	k1gInSc7
ji	on	k3xPp3gFnSc4
Dyk	Dyk	k?
obměňuje	obměňovat	k5eAaImIp3nS
motivem	motiv	k1gInSc7
milostné	milostný	k2eAgFnSc2d1
deziluze	deziluze	k1gFnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
online	onlin	k1gInSc5
</s>
<s>
Tajemná	tajemný	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
Alexeje	Alexej	k1gMnSc2
Iványče	Iványč	k1gInSc2
Kozulinova	Kozulinův	k2eAgInSc2d1
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
neúplně	úplně	k6eNd1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
1915	#num#	k4
<g/>
,	,	kIx,
dopsáno	dopsán	k2eAgNnSc4d1
1922	#num#	k4
<g/>
;	;	kIx,
dvě	dva	k4xCgFnPc1
původní	původní	k2eAgFnPc1d1
zkonfiskované	zkonfiskovaný	k2eAgFnPc1d1
kapitoly	kapitola	k1gFnPc1
<g/>
,	,	kIx,
po	po	k7c4
1915	#num#	k4
ztracené	ztracený	k2eAgFnSc2d1
a	a	k8xC
autorem	autor	k1gMnSc7
pro	pro	k7c4
knižní	knižní	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
nepoužité	použitý	k2eNgNnSc4d1
<g/>
,	,	kIx,
až	až	k9
posmrtně	posmrtně	k6eAd1
1931	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tichý	tichý	k2eAgInSc1d1
dům	dům	k1gInSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zlý	zlý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
povídek	povídka	k1gFnPc2
<g/>
,	,	kIx,
psáno	psán	k2eAgNnSc1d1
1905	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prsty	prst	k1gInPc1
Habakukovy	Habakukův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
,	,	kIx,
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Můj	můj	k3xOp1gMnSc1
přítel	přítel	k1gMnSc1
Čehona	Čehona	k1gMnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dědivadelní	Dědivadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
pro	pro	k7c4
ml.	ml.	kA
<g/>
)	)	kIx)
</s>
<s>
Holoubek	Holoubek	k1gMnSc1
Kuzma	Kuzm	k1gMnSc2
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Soykovy	Soykův	k2eAgFnPc1d1
děti	dítě	k1gFnPc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Politická	politický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
Ad	ad	k7c4
usum	usum	k1gInSc4
pana	pan	k1gMnSc2
presidenta	president	k1gMnSc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1929	#num#	k4
–	–	k?
kniha	kniha	k1gFnSc1
kritizující	kritizující	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
Beneše	Beneš	k1gMnSc2
<g/>
,	,	kIx,
Masaryka	Masaryk	k1gMnSc2
a	a	k8xC
celé	celý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
okolo	okolo	k7c2
„	„	k?
<g/>
Hradu	hrad	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c4
národní	národní	k2eAgInSc4d1
stát	stát	k1gInSc4
(	(	kIx(
<g/>
vydáno	vydat	k5eAaPmNgNnS
posmrtně	posmrtně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1932	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
svazků	svazek	k1gInPc2
<g/>
,	,	kIx,
soubor	soubor	k1gInSc1
Dykovy	Dykův	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
publicistiky	publicistika	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1917	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Drama	drama	k1gNnSc1
</s>
<s>
Epizoda	epizoda	k1gFnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
,	,	kIx,
i	i	k9
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Posel	posel	k1gMnSc1
(	(	kIx(
<g/>
1907	#num#	k4
i	i	k8xC
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
1922	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
zamýšlené	zamýšlený	k2eAgFnSc2d1
a	a	k8xC
neuskutečněné	uskutečněný	k2eNgFnSc2d1
dramatické	dramatický	k2eAgFnSc2d1
trilogie	trilogie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Zmoudření	zmoudření	k1gNnSc1
Dona	Don	k1gMnSc2
Quijota	Quijot	k1gMnSc2
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
,	,	kIx,
prem	prem	k1gInSc1
<g/>
.	.	kIx.
1914	#num#	k4
<g/>
,	,	kIx,
výrazný	výrazný	k2eAgInSc1d1
vliv	vliv	k1gInSc1
symbolismu	symbolismus	k1gInSc2
<g/>
,	,	kIx,
zmoudření	zmoudření	k1gNnSc1
=	=	kIx~
ztráta	ztráta	k1gFnSc1
iluzí	iluze	k1gFnPc2
<g/>
,	,	kIx,
smrt	smrt	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1
mág	mág	k1gMnSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
,	,	kIx,
prem	prem	k1gInSc1
<g/>
.	.	kIx.
1915	#num#	k4
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zvěrstva	zvěrstvo	k1gNnPc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
,	,	kIx,
i	i	k9
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
a	a	k8xC
drak	drak	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
,	,	kIx,
prem	prem	k1gInSc1
<g/>
.	.	kIx.
1920	#num#	k4
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
zpracováno	zpracovat	k5eAaPmNgNnS
jako	jako	k9
rozhlasová	rozhlasový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
rozhlasová	rozhlasový	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
<g/>
:	:	kIx,
Lída	Lída	k1gFnSc1
Naarová	Naarová	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Tomeš	Tomeš	k1gMnSc1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1
trilogie	trilogie	k1gFnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
divadelních	divadelní	k2eAgNnPc2d1
heromán	heromán	k2eAgInSc1d1
<g/>
;	;	kIx,
Ranní	ranní	k2eAgFnSc1d1
ropucha	ropucha	k1gFnSc1
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
1908	#num#	k4
<g/>
,	,	kIx,
Figaro	Figara	k1gFnSc5
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
1917	#num#	k4
<g/>
,	,	kIx,
Poražení	poražený	k2eAgMnPc1d1
prem	prema	k1gFnPc2
<g/>
.	.	kIx.
1911	#num#	k4
<g/>
,	,	kIx,
prem	prem	k1gInSc1
<g/>
.	.	kIx.
celku	celek	k1gInSc2
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Napravený	napravený	k2eAgMnSc1d1
plukovník	plukovník	k1gMnSc1
Švec	Švec	k1gMnSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
;	;	kIx,
Zastává	zastávat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
Rudolfa	Rudolf	k1gMnSc2
Medka	Medek	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Paměti	paměť	k1gFnPc1
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1
a	a	k8xC
komentáře	komentář	k1gInPc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Libreta	libreto	k1gNnPc1
</s>
<s>
Libreto	libreto	k1gNnSc4
k	k	k7c3
opeře	opera	k1gFnSc3
Výlety	výlet	k1gInPc1
pana	pan	k1gMnSc2
Broučka	Brouček	k1gMnSc2
od	od	k7c2
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
(	(	kIx(
<g/>
podle	podle	k7c2
románu	román	k1gInSc2
Svatopluka	Svatopluk	k1gMnSc2
Čecha	Čech	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1
–	–	k?
Opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc2
<g/>
…	…	k?
v	v	k7c6
propagandě	propaganda	k1gFnSc6
</s>
<s>
Josef	Josef	k1gMnSc1
Mařatka	Mařatka	k1gFnSc1
–	–	k?
Praha	Praha	k1gFnSc1
svým	svůj	k3xOyFgMnPc3
vítězným	vítězný	k2eAgMnPc3d1
synům	syn	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
básni	báseň	k1gFnSc6
Země	zem	k1gFnSc2
mluví	mluvit	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
(	(	kIx(
<g/>
sbírka	sbírka	k1gFnSc1
Okno	okno	k1gNnSc4
<g/>
)	)	kIx)
personifikoval	personifikovat	k5eAaBmAgInS
Dyk	Dyk	k?
vlast	vlast	k1gFnSc4
jako	jako	k8xS,k8xC
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
svému	svůj	k3xOyFgMnSc3
synovi	syn	k1gMnSc3
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc2
<g/>
,	,	kIx,
nezahynu	zahynout	k5eNaPmIp1nS
<g/>
.	.	kIx.
/	/	kIx~
Opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc2
<g/>
,	,	kIx,
zahyneš	zahynout	k5eAaPmIp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Tento	tento	k3xDgInSc4
text	text	k1gInSc4
byl	být	k5eAaImAgInS
často	často	k6eAd1
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
komunistickou	komunistický	k2eAgFnSc7d1
propagandou	propaganda	k1gFnSc7
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
československým	československý	k2eAgMnPc3d1
emigrantům	emigrant	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Román	román	k1gInSc1
Zdeňka	Zdeněk	k1gMnSc2
Pluhaře	Pluhař	k1gMnSc2
Opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc4
z	z	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
cituje	citovat	k5eAaBmIp3nS
báseň	báseň	k1gFnSc4
v	v	k7c6
názvu	název	k1gInSc6
a	a	k8xC
připomíná	připomínat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
i	i	k9
v	v	k7c6
ději	děj	k1gInSc6
<g/>
,	,	kIx,
popisuje	popisovat	k5eAaImIp3nS
osudy	osud	k1gInPc4
tří	tři	k4xCgInPc2
mladých	mladý	k2eAgMnPc2d1
emigrantů	emigrant	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
opustili	opustit	k5eAaPmAgMnP
vlast	vlast	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
Též	též	k9
jako	jako	k9
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Citace	citace	k1gFnSc1
byla	být	k5eAaImAgFnS
často	často	k6eAd1
používána	používat	k5eAaImNgFnS
i	i	k9
v	v	k7c6
denním	denní	k2eAgInSc6d1
tisku	tisk	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
článcích	článek	k1gInPc6
o	o	k7c6
emigraci	emigrace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Text	text	k1gInSc1
této	tento	k3xDgFnSc2
básně	báseň	k1gFnSc2
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
i	i	k9
na	na	k7c6
čelní	čelní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
skupinového	skupinový	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
na	na	k7c4
paměť	paměť	k1gFnSc4
padlým	padlý	k1gMnSc7
Pražanům	Pražan	k1gMnPc3
v	v	k7c4
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
od	od	k7c2
Josefa	Josef	k1gMnSc2
Mařatky	Mařatka	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
svým	svůj	k3xOyFgMnPc3
vítězným	vítězný	k2eAgMnPc3d1
synům	syn	k1gMnPc3
<g/>
,	,	kIx,
vybudovaný	vybudovaný	k2eAgMnSc1d1
mezi	mezi	k7c7
lety	let	k1gInPc7
1927	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Báseň	báseň	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
upravené	upravený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
nahrána	nahrát	k5eAaPmNgFnS,k5eAaBmNgFnS
coby	coby	k?
pocta	pocta	k1gFnSc1
Viktoru	Viktor	k1gMnSc3
Dykovi	Dyka	k1gMnSc3
jako	jako	k8xS,k8xC
píseň	píseň	k1gFnSc4
nacionalistickou	nacionalistický	k2eAgFnSc7d1
hudební	hudební	k2eAgFnSc7d1
punkovou	punkový	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
Celková	celkový	k2eAgFnSc1d1
Impotence	impotence	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
<g/>
↑	↑	k?
jh	jh	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
mrtev	mrtev	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
1931	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archiv	archiv	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Matrika	matrika	k1gFnSc1
zemřelých	zemřelý	k1gMnPc2
u	u	k7c2
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1
srdce	srdce	k1gNnSc2
Páně	páně	k2eAgNnSc2d1
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPVIN	SPVIN	kA
Z	z	k7c2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
371	#num#	k4
<g/>
↑	↑	k?
Parlamentní	parlamentní	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
Čsl	čsl	kA
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
v	v	k7c6
III	III	kA
<g/>
.	.	kIx.
voleném	volený	k2eAgNnSc6d1
Národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
<g/>
,	,	kIx,
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1929	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1.1	1.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
OPELÍK	OPELÍK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ZÁVADA	závada	k1gFnSc1
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
Opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc2
<g/>
....	....	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
KVAČEK	KVAČEK	k?
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
;	;	kIx,
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
Osmdesát	osmdesát	k4xCc4
let	léto	k1gNnPc2
od	od	k7c2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc4
textů	text	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87806	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
..	..	k?
↑	↑	k?
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HONCOVÁ	HONCOVÁ	kA
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rodině	rodina	k1gFnSc6
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acta	Acta	k1gMnSc1
genealogica	genealogica	k1gMnSc1
ac	ac	k?
heraldica	heraldica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genealogická	genealogický	k2eAgFnSc1d1
a	a	k8xC
heraldická	heraldický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PRECLÍK	preclík	k1gInSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
legie	legie	k1gFnSc1
<g/>
,	,	kIx,
váz	váza	k1gFnPc2
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
219	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Paris	Paris	k1gMnSc1
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Žižkova	Žižkův	k2eAgFnSc1d1
2379	#num#	k4
(	(	kIx(
<g/>
734	#num#	k4
01	#num#	k4
Karviná	Karviná	k1gFnSc1
<g/>
)	)	kIx)
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Masarykovým	Masarykův	k2eAgNnSc7d1
demokratickým	demokratický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87173	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
151	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
157	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
169	#num#	k4
<g/>
↑	↑	k?
jmenný	jmenný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
–	–	k?
Člověk	člověk	k1gMnSc1
politický	politický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fakulta	fakulta	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
↑	↑	k?
PROKOP	prokop	k1gInSc1
<g/>
,	,	kIx,
F.	F.	kA
J.	J.	kA
<g/>
:	:	kIx,
Duras	Duras	k1gMnSc1
vítězí	vítězit	k5eAaImIp3nS
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
↑	↑	k?
The	The	k1gFnSc2
chess	chessa	k1gFnPc2
games	games	k1gMnSc1
of	of	k?
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
↑	↑	k?
Devátá	devátý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
Viktora	Viktor	k1gMnSc4
Dyka	Dykus	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heartcore	Heartcor	k1gMnSc5
<g/>
/	/	kIx~
<g/>
Srdcaři	srdcař	k1gMnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Soumrak	soumrak	k1gInSc1
moře	moře	k1gNnSc2
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
<g/>
↑	↑	k?
Devátá	devátý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
<g/>
↑	↑	k?
ŠTĚPÁNEK	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herec	herec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
187	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
↑	↑	k?
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
Ondřej	Ondřej	k1gMnSc1
a	a	k8xC
drak	drak	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
<g/>
↑	↑	k?
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
Země	zem	k1gFnSc2
mluví	mluvit	k5eAaImIp3nS
<g/>
↑	↑	k?
Slovník	slovník	k1gInSc4
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Pluhař	Pluhař	k1gMnSc1
<g/>
:	:	kIx,
Opustíš	opustit	k5eAaPmIp2nS
<g/>
–	–	k?
<g/>
li	li	k8xS
mne	já	k3xPp1nSc4
<g/>
↑	↑	k?
Např.	např.	kA
Nářek	nářek	k1gInSc1
utečenců	utečenec	k1gMnPc2
<g/>
,	,	kIx,
Na	na	k7c6
prahu	práh	k1gInSc6
nového	nový	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1958	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Např.	např.	kA
Touha	touha	k1gFnSc1
po	po	k7c6
domově	domov	k1gInSc6
převládá	převládat	k5eAaImIp3nS
(	(	kIx(
<g/>
posled	posled	k?
<g/>
.	.	kIx.
odst	odst	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1972	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KRIŠKOVÁ	KRIŠKOVÁ	kA
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
svým	svůj	k3xOyFgMnPc3
vítězným	vítězný	k2eAgMnPc3d1
synům	syn	k1gMnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
socharstvi	socharstvit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Země	země	k1gFnSc1
mluví	mluvit	k5eAaImIp3nS
(	(	kIx(
<g/>
písňová	písňový	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Čeští	český	k2eAgMnPc1d1
spisovatelé	spisovatel	k1gMnPc1
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
56	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literatura	literatura	k1gFnSc1
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
/	/	kIx~
hlavní	hlavní	k2eAgMnSc1d1
redaktor	redaktor	k1gMnSc1
Jan	Jan	k1gMnSc1
Mukařovský	Mukařovský	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Victoria	Victorium	k1gNnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
714	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85865	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
615	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FORST	FORST	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gNnSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
:	:	kIx,
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
instituce	instituce	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A-	A-	k1gFnSc1
<g/>
G.	G.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
797	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
643	#num#	k4
<g/>
–	–	k?
<g/>
646	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GÖTZ	GÖTZ	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
,	,	kIx,
TETAUER	TETAUER	kA
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
umění	umění	k1gNnSc2
dramatické	dramatický	k2eAgFnPc1d1
<g/>
,	,	kIx,
Část	část	k1gFnSc1
I.	I.	kA
–	–	k?
činohra	činohra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Šolc	Šolc	k1gMnSc1
a	a	k8xC
Šimáček	Šimáček	k1gMnSc1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
213	#num#	k4
<g/>
–	–	k?
<g/>
224	#num#	k4
</s>
<s>
KAUTMAN	KAUTMAN	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naděje	naděje	k1gFnPc1
a	a	k8xC
úskalí	úskalí	k1gNnSc1
českého	český	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
v	v	k7c6
českém	český	k2eAgInSc6d1
politickém	politický	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
101	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85281	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOSATÍK	KOSATÍK	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
:	:	kIx,
50	#num#	k4
nejvýznamnějších	významný	k2eAgFnPc2d3
osobností	osobnost	k1gFnPc2
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
280	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
2307	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
:	:	kIx,
monografie	monografie	k1gFnPc1
s	s	k7c7
ukázkami	ukázka	k1gFnPc7
z	z	k7c2
tvorby	tvorba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
414	#num#	k4
s.	s.	k?
</s>
<s>
PUTNA	putna	k1gFnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
C.	C.	kA
Česká	český	k2eAgFnSc1d1
katolická	katolický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
v	v	k7c6
kontextech	kontext	k1gInPc6
:	:	kIx,
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Torst	Torst	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
1390	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
721	#num#	k4
<g/>
-	-	kIx~
<g/>
5391	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
:	:	kIx,
I.	I.	kA
díl	díl	k1gInSc1
:	:	kIx,
A	a	k9
<g/>
–	–	k?
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
;	;	kIx,
Petr	Petr	k1gMnSc1
Meissner	Meissner	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
634	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
281	#num#	k4
<g/>
–	–	k?
<g/>
282	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
a	a	k8xC
T.G.	T.G.	k1gMnSc1
Masaryk	Masaryk	k1gMnSc1
:	:	kIx,
dvojí	dvojí	k4xRgFnSc1
reflexe	reflexe	k1gFnSc1
češství	češství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
203	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
309	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
:	:	kIx,
15	#num#	k4
<g/>
.	.	kIx.
sešit	sešit	k1gInSc1
:	:	kIx,
Dvořák	Dvořák	k1gMnSc1
<g/>
–	–	k?
<g/>
Enz	Enz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
467	#num#	k4
<g/>
–	–	k?
<g/>
610	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
504	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
502	#num#	k4
<g/>
–	–	k?
<g/>
503	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
414	#num#	k4
s	s	k7c7
</s>
<s>
OPELÍK	OPELÍK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
ZÁVADA	závada	k1gFnSc1
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc2
<g/>
…	…	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
,	,	kIx,
122	#num#	k4
s.	s.	k?
</s>
<s>
KVAČEK	KVAČEK	k?
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
,	,	kIx,
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
Osmdesát	osmdesát	k4xCc4
let	léto	k1gNnPc2
od	od	k7c2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc4
textů	text	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87806	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HONCOVÁ	HONCOVÁ	kA
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rodině	rodina	k1gFnSc6
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acta	Acta	k1gMnSc1
genealogica	genealogica	k1gMnSc1
ac	ac	k?
heraldica	heraldica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genealogická	genealogický	k2eAgFnSc1d1
a	a	k8xC
heraldická	heraldický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Anarchističtí	anarchistický	k2eAgMnPc1d1
buřiči	buřič	k1gMnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1900	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
Masarykův	Masarykův	k2eAgInSc4d1
realismus	realismus	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autor	autor	k1gMnSc1
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
</s>
<s>
Osoba	osoba	k1gFnSc1
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Veřejně	veřejně	k6eAd1
dostupná	dostupný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
Viktora	Viktor	k1gMnSc2
Dyka	Dykus	k1gMnSc2
v	v	k7c6
Digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
MZK	MZK	kA
</s>
<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
<g/>
:	:	kIx,
Básník	básník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nenáviděl	návidět	k5eNaImAgMnS,k5eAaImAgMnS
českou	český	k2eAgFnSc4d1
prostřednost	prostřednost	k1gFnSc4
</s>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1
adaptace	adaptace	k1gFnPc1
vybraných	vybraný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
k	k	k7c3
bezplatnému	bezplatný	k2eAgNnSc3d1
stáhnutí	stáhnutí	k1gNnSc3
ve	v	k7c6
formátu	formát	k1gInSc6
mp	mp	k?
<g/>
3	#num#	k4
na	na	k7c6
webu	web	k1gInSc6
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
</s>
<s>
A	a	k8xC
monument	monument	k1gInSc1
to	ten	k3xDgNnSc4
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
by	by	kYmCp3nS
Serbian	Serbian	k1gMnSc1
architect	architect	k1gMnSc1
Nikola	Nikola	k1gMnSc1
Dobrović	Dobrović	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
island	island	k1gInSc1
of	of	k?
Lopud	Lopud	k1gInSc1
</s>
<s>
Ukázky	ukázka	k1gFnPc1
z	z	k7c2
díla	dílo	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1030247	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
11888896X	11888896X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1051	#num#	k4
4417	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80139644	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
32074913	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80139644	#num#	k4
</s>
