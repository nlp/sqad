<s>
January	Januara	k1gFnPc1
Jones	Jonesa	k1gFnPc2
</s>
<s>
January	Januar	k1gInPc1
Jones	Jonesa	k1gFnPc2
Jones	Jonesa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
Rodné	rodný	k2eAgInPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
January	Januar	k1gInPc1
Kristen	Kristen	k2eAgInSc1d1
Jones	Jones	k1gInSc4
Narození	narození	k1gNnPc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
(	(	kIx(
<g/>
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Sioux	Sioux	k1gInSc1
Falls	Falls	k1gInSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnPc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1
High	High	k1gMnSc1
School	School	k1gInSc4
Aktivní	aktivní	k2eAgInSc4d1
roky	rok	k1gInPc7
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Jason	Jason	k1gInSc1
Sudeikis	Sudeikis	k1gFnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Will	Will	k1gInSc1
Forte	forte	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
2015	#num#	k4
<g/>
)	)	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jina	Jina	k1gMnSc1
Jones	Jones	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
January	January	k1gFnSc1
Kristen	Kristen	k1gFnSc1
Jones	Jones	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
Sioux	Sioux	k1gInSc1
Falls	Falls	k1gInPc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
nejznámější	známý	k2eAgFnSc7d3
rolí	role	k1gFnSc7
je	být	k5eAaImIp3nS
Betty	Betty	k1gFnSc1
Draper	Drapra	k1gFnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Šílenci	šílenec	k1gMnPc1
z	z	k7c2
Manhattanu	Manhattan	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
získala	získat	k5eAaPmAgFnS
nominaci	nominace	k1gFnSc4
na	na	k7c4
dvě	dva	k4xCgFnPc4
ceny	cena	k1gFnPc4
Zlatý	zlatý	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
a	a	k8xC
Cenu	cena	k1gFnSc4
Emmy	Emma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgNnP
se	se	k3xPyFc4
a	a	k8xC
byla	být	k5eAaImAgFnS
vychovávána	vychovávat	k5eAaImNgFnS
v	v	k7c6
městě	město	k1gNnSc6
Sioux	Sioux	k1gInSc1
Falls	Falls	k1gInSc1
jako	jako	k8xC,k8xS
dcera	dcera	k1gFnSc1
Karen	Karna	k1gFnPc2
<g/>
,	,	kIx,
prodavačky	prodavačka	k1gFnPc1
v	v	k7c6
obchodě	obchod	k1gInSc6
ze	z	k7c2
sportovním	sportovní	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
a	a	k8xC
Marvina	Marvina	k1gFnSc1
Jonesových	Jonesový	k2eAgNnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
je	být	k5eAaImIp3nS
trenérem	trenér	k1gMnSc7
<g/>
,	,	kIx,
učitelem	učitel	k1gMnSc7
tělocviku	tělocvik	k1gInSc2
a	a	k8xC
fitness	fitness	k6eAd1
trenérem	trenér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pojmenována	pojmenován	k2eAgFnSc1d1
po	po	k7c4
January	Januar	k1gMnPc4
Wayne	Wayn	k1gInSc5
<g/>
,	,	kIx,
postavě	postava	k1gFnSc6
z	z	k7c2
knihy	kniha	k1gFnSc2
od	od	k7c2
Jacqueline	Jacquelin	k1gInSc5
Susann	Susann	k1gNnSc1
<g/>
,	,	kIx,
Once	Once	k1gNnSc1
Is	Is	k1gFnSc2
Not	nota	k1gFnPc2
Enough	Enough	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
sestry	sestra	k1gFnPc4
<g/>
,	,	kIx,
Jacey	Jacea	k1gFnPc4
a	a	k8xC
Jinu	Jina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Hrála	hrát	k5eAaImAgFnS
vedlejší	vedlejší	k2eAgFnPc4d1
role	role	k1gFnPc4
ve	v	k7c6
filmech	film	k1gInPc6
Kurs	kurs	k1gInSc1
sebeovládání	sebeovládání	k1gNnPc2
<g/>
,	,	kIx,
Láska	láska	k1gFnSc1
nebeská	nebeský	k2eAgFnSc1d1
a	a	k8xC
Hříšný	hříšný	k2eAgInSc1d1
tanec	tanec	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
roli	role	k1gFnSc6
frustrované	frustrovaný	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
amerického	americký	k2eAgMnSc2d1
pohraničního	pohraniční	k2eAgMnSc2d1
strážce	strážce	k1gMnSc2
ve	v	k7c6
filmu	film	k1gInSc6
Tři	tři	k4xCgInPc4
pohřby	pohřeb	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
režíroval	režírovat	k5eAaImAgMnS
a	a	k8xC
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
zároveň	zároveň	k6eAd1
i	i	k9
hrál	hrát	k5eAaImAgMnS
Tommy	Tomm	k1gInPc4
Lee	Lea	k1gFnSc3
Jones	Jones	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
Návrat	návrat	k1gInSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
ztvárnila	ztvárnit	k5eAaPmAgFnS
roli	role	k1gFnSc4
Carol	Carola	k1gFnPc2
Dawson	Dawsona	k1gFnPc2
<g/>
,	,	kIx,
ženy	žena	k1gFnPc1
fotbalového	fotbalový	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
Williama	William	k1gMnSc2
"	"	kIx"
<g/>
Red	Red	k1gMnSc2
<g/>
"	"	kIx"
Dawsona	Dawson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Měla	mít	k5eAaImAgFnS
hlavní	hlavní	k2eAgFnSc4d1
ženskou	ženský	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
filmu	film	k1gInSc6
Slib	slib	k1gInSc1
věčné	věčný	k2eAgInPc1d1
lásky	láska	k1gFnPc4
jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgNnSc4d3
dítě	dítě	k1gNnSc4
průkopnické	průkopnický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
postava	postava	k1gFnSc1
se	se	k3xPyFc4
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
tajemného	tajemný	k2eAgMnSc2d1
muže	muž	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zachránil	zachránit	k5eAaPmAgMnS
život	život	k1gInSc4
jejímu	její	k3xOp3gMnSc3
otci	otec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
dramatickém	dramatický	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Šílenci	šílenec	k1gMnPc1
z	z	k7c2
Manhattanu	Manhattan	k1gInSc2
<g/>
,	,	kIx,
odehrávajícím	odehrávající	k2eAgMnSc7d1
se	se	k3xPyFc4
v	v	k7c6
Americe	Amerika	k1gFnSc6
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
mladá	mladý	k2eAgFnSc1d1
příměstská	příměstský	k2eAgFnSc1d1
žena	žena	k1gFnSc1
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
a	a	k8xC
matka	matka	k1gFnSc1
Betty	Betty	k1gFnSc2
Draper	Drapra	k1gFnPc2
Francis	Francis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
též	též	k9
známá	známý	k2eAgFnSc1d1
za	za	k7c4
roli	role	k1gFnSc4
Cadence	Cadence	k1gFnSc2
Flaherty	Flahert	k1gInPc4
<g/>
,	,	kIx,
lásku	láska	k1gFnSc4
Steva	Steve	k1gMnSc2
Stiflera	Stifler	k1gMnSc2
a	a	k8xC
Paula	Paul	k1gMnSc2
Finche	Finch	k1gMnSc2
v	v	k7c6
komedii	komedie	k1gFnSc6
Prci	Prc	k1gFnSc2
<g/>
,	,	kIx,
prci	prc	k1gFnSc2
<g/>
,	,	kIx,
prcičky	prcička	k1gFnSc2
3	#num#	k4
<g/>
:	:	kIx,
Svatba	svatba	k1gFnSc1
<g/>
,	,	kIx,
třetím	třetí	k4xOgMnSc6
dílu	dílo	k1gNnSc3
filmové	filmový	k2eAgFnSc2d1
série	série	k1gFnSc2
Prci	Prc	k1gFnSc2
<g/>
,	,	kIx,
prci	prc	k1gFnSc2
<g/>
,	,	kIx,
prcičky	prcička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
osmnácté	osmnáctý	k4xOgFnSc6
řadě	řada	k1gFnSc6
seriálu	seriál	k1gInSc2
Právo	právo	k1gNnSc1
a	a	k8xC
pořádek	pořádek	k1gInSc4
v	v	k7c6
epizodě	epizoda	k1gFnSc6
"	"	kIx"
<g/>
Quit	Quit	k2eAgInSc1d1
Claim	Claim	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrála	hrát	k5eAaImAgFnS
podvodnici	podvodnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
britském	britský	k2eAgInSc6d1
snímku	snímek	k1gInSc6
Piráti	pirát	k1gMnPc1
na	na	k7c6
vlnách	vlna	k1gFnPc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
o	o	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
pirátské	pirátský	k2eAgFnSc6d1
rozhlasové	rozhlasový	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
82	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
žebříčku	žebříček	k1gInSc6
"	"	kIx"
<g/>
100	#num#	k4
nejpřitažlivějších	přitažlivý	k2eAgFnPc2d3
žen	žena	k1gFnPc2
<g/>
"	"	kIx"
časopisu	časopis	k1gInSc2
Maxim	maxim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
obálce	obálka	k1gFnSc6
britského	britský	k2eAgNnSc2d1
vydání	vydání	k1gNnSc2
časopisu	časopis	k1gInSc2
GQ	GQ	kA
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
uváděla	uvádět	k5eAaImAgFnS
epizodu	epizoda	k1gFnSc4
Saturday	Saturdaa	k1gFnSc2
Night	Night	k1gInSc4
Live	Liv	k1gInSc2
obsahující	obsahující	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
od	od	k7c2
hostující	hostující	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
The	The	k1gMnSc1
Black	Black	k1gMnSc1
Eyed	Eyed	k1gMnSc1
Peas	Peas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podala	podat	k5eAaPmAgFnS
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
negativními	negativní	k2eAgFnPc7d1
kritikami	kritika	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Liama	Liam	k1gMnSc2
Neesona	Neeson	k1gMnSc2
a	a	k8xC
Diane	Dian	k1gInSc5
Kruger	Krugero	k1gNnPc2
v	v	k7c6
thrilleru	thriller	k1gInSc6
Neznámý	známý	k2eNgMnSc1d1
a	a	k8xC
ztvárnila	ztvárnit	k5eAaPmAgFnS
Emmu	Emma	k1gFnSc4
Frost	Frost	k1gFnSc1
ve	v	k7c6
snímku	snímek	k1gInSc6
X-Men	X-Mna	k1gFnPc2
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
třída	třída	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
V	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
časopis	časopis	k1gInSc4
Vanity	Vanita	k1gFnSc2
Fair	fair	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přidala	přidat	k5eAaPmAgFnS
k	k	k7c3
neziskové	ziskový	k2eNgFnSc3d1
organizaci	organizace	k1gFnSc3
Oceana	Oceana	k1gFnSc1
jako	jako	k8xS,k8xC
jejich	jejich	k3xOp3gFnPc2
celebrita-mluvčí	celebrita-mluvčí	k1gFnSc1
a	a	k8xC
bude	být	k5eAaImBp3nS
pracovat	pracovat	k5eAaImF
na	na	k7c6
záchraně	záchrana	k1gFnSc6
ohrožených	ohrožený	k2eAgMnPc2d1
žraloků	žralok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2003	#num#	k4
až	až	k9
2006	#num#	k4
chodila	chodit	k5eAaImAgFnS
se	s	k7c7
zpěvákem	zpěvák	k1gMnSc7
a	a	k8xC
skladatelem	skladatel	k1gMnSc7
Joshem	Josh	k1gInSc7
Grobanem	Groban	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
narodil	narodit	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
první	první	k4xOgMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
pojmenovala	pojmenovat	k5eAaPmAgFnS
Xander	Xander	k1gInSc1
Dane	Dan	k1gMnSc5
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1999	#num#	k4
</s>
<s>
Zuřivost	zuřivost	k1gFnSc1
</s>
<s>
Janice	Janice	k1gFnSc1
Taylor	Taylora	k1gFnPc2
</s>
<s>
2001	#num#	k4
</s>
<s>
Skleněný	skleněný	k2eAgInSc1d1
dům	dům	k1gInSc1
</s>
<s>
Dívka	dívka	k1gFnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
Banditi	Banditi	k?
</s>
<s>
Claire	Clair	k1gMnSc5
<g/>
/	/	kIx~
<g/>
Pink	pink	k2eAgInSc1d1
Boots	Boots	k1gInSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
Kletba	kletba	k1gFnSc1
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
Hollywood	Hollywood	k1gInSc1
<g/>
,	,	kIx,
Hollywood	Hollywood	k1gInSc1
</s>
<s>
Tracy	Traca	k1gFnPc1
</s>
<s>
2003	#num#	k4
</s>
<s>
Kurs	kurs	k1gInSc1
sebeovládání	sebeovládání	k1gNnSc2
</s>
<s>
Gina	Gina	k6eAd1
</s>
<s>
2003	#num#	k4
</s>
<s>
Prci	Prc	k1gFnPc1
<g/>
,	,	kIx,
prci	prc	k1gFnPc1
<g/>
,	,	kIx,
prcičky	prcička	k1gFnPc1
3	#num#	k4
<g/>
:	:	kIx,
Svatba	svatba	k1gFnSc1
</s>
<s>
Cadence	Cadence	k1gFnSc1
Flaherty	Flahert	k1gInPc1
</s>
<s>
2003	#num#	k4
</s>
<s>
Láska	láska	k1gFnSc1
nebeská	nebeský	k2eAgFnSc1d1
</s>
<s>
Jeannie	Jeannie	k1gFnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Hříšný	hříšný	k2eAgInSc1d1
tanec	tanec	k1gInSc1
2	#num#	k4
</s>
<s>
Eve	Eve	k?
</s>
<s>
2005	#num#	k4
</s>
<s>
Tři	tři	k4xCgInPc1
pohřby	pohřeb	k1gInPc1
</s>
<s>
Lou	Lou	k?
Ann	Ann	k1gFnSc1
Norton	Norton	k1gInSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Swedish	Swedish	k1gInSc1
Auto	auto	k1gNnSc1
</s>
<s>
Darla	Darla	k6eAd1
</s>
<s>
2006	#num#	k4
</s>
<s>
Návrat	návrat	k1gInSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
</s>
<s>
Carole	Carole	k1gFnSc1
Dawson	Dawsona	k1gFnPc2
</s>
<s>
2009	#num#	k4
</s>
<s>
Piráti	pirát	k1gMnPc1
na	na	k7c6
vlnách	vlna	k1gFnPc6
</s>
<s>
Elenore	Elenor	k1gMnSc5
</s>
<s>
2011	#num#	k4
</s>
<s>
Neznámý	známý	k2eNgMnSc1d1
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Harris	Harris	k1gFnSc2
</s>
<s>
2011	#num#	k4
</s>
<s>
X-Men	X-Men	k1gInSc1
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
třída	třída	k1gFnSc1
</s>
<s>
Emma	Emma	k1gFnSc1
Frost	Frost	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Vendeta	vendeta	k1gFnSc1
</s>
<s>
Laura	Laura	k1gFnSc1
Gerard	Gerarda	k1gFnPc2
</s>
<s>
2012	#num#	k4
</s>
<s>
Prci	Prc	k1gFnPc1
<g/>
,	,	kIx,
prci	prc	k1gFnPc1
<g/>
,	,	kIx,
prcičky	prcička	k1gFnPc1
<g/>
:	:	kIx,
Školní	školní	k2eAgInSc1d1
sraz	sraz	k1gInSc1
</s>
<s>
Cadence	Cadence	k1gFnSc1
Flaherty	Flahert	k1gInPc1
</s>
<s>
Cameo	Cameo	k6eAd1
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
titulkách	titulkách	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c6
fotce	fotka	k1gFnSc6
z	z	k7c2
Prci	Prci	k1gNnSc2
<g/>
,	,	kIx,
prci	prci	k1gNnSc2
<g/>
,	,	kIx,
prcičky	prcička	k1gFnSc2
3	#num#	k4
<g/>
:	:	kIx,
Svatba	svatba	k1gFnSc1
během	během	k7c2
závěrečných	závěrečný	k2eAgInPc2d1
titulků	titulek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
</s>
<s>
Sweetwater	Sweetwater	k1gMnSc1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Ramírez	Ramíreza	k1gFnPc2
</s>
<s>
2014	#num#	k4
</s>
<s>
Good	Good	k1gMnSc1
Kill	Kill	k1gMnSc1
</s>
<s>
Molly	Molla	k1gFnPc1
Egan	Egana	k1gFnPc2
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1999	#num#	k4
</s>
<s>
Sorority	Sororita	k1gFnPc1
</s>
<s>
Number	Number	k1gMnSc1
One	One	k1gMnSc1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
Get	Get	k?
Real	Real	k1gInSc1
</s>
<s>
Jane	Jan	k1gMnSc5
Cohen	Cohen	k2eAgInSc4d1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Pilot	pilot	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2002	#num#	k4
</s>
<s>
In	In	k?
My	my	k3xPp1nPc1
Life	Life	k1gInSc4
</s>
<s>
Diane	Dianout	k5eAaImIp3nS,k5eAaPmIp3nS
St.	st.	kA
Croix	Croix	k1gInSc1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Slib	slib	k1gInSc1
věčné	věčný	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
</s>
<s>
Missie	Missie	k1gFnSc1
Davis	Davis	k1gFnSc2
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Huff	Huff	k1gMnSc1
</s>
<s>
Marisa	Marisa	k1gFnSc1
Wells	Wellsa	k1gFnPc2
</s>
<s>
2	#num#	k4
díly	dílo	k1gNnPc7
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
Šílenci	šílenec	k1gMnPc1
z	z	k7c2
Manhattanu	Manhattan	k1gInSc2
</s>
<s>
Betty	Betty	k1gFnSc1
Draper	Drapra	k1gFnPc2
Francis	Francis	k1gFnSc2
</s>
<s>
hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
<g/>
,	,	kIx,
92	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
2008	#num#	k4
</s>
<s>
Zákon	zákon	k1gInSc1
a	a	k8xC
pořádek	pořádek	k1gInSc1
</s>
<s>
Kim	Kim	k?
Brody	Brod	k1gInPc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Quit	Quit	k1gMnSc1
Claim	Claim	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2010	#num#	k4
</s>
<s>
Project	Project	k2eAgInSc1d1
Runway	runway	k1gInSc1
</s>
<s>
Samu	Samos	k1gInSc2
sebe	sebe	k3xPyFc4
<g/>
/	/	kIx~
<g/>
porotkyně	porotkyně	k1gFnPc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
A	a	k8xC
Rough	Rough	k1gMnSc1
Day	Day	k1gMnSc1
On	on	k3xPp3gMnSc1
The	The	k1gFnSc4
Runway	runway	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1
chlap	chlap	k1gMnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Melissa	Melissa	k1gFnSc1
Chartres	Chartresa	k1gFnPc2
</s>
<s>
hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
<g/>
,	,	kIx,
63	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
2017	#num#	k4
</s>
<s>
Animals	Animals	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Diana	Diana	k1gFnSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Roaches	Roaches	k1gInSc1
<g/>
“	“	k?
</s>
<s>
Return	Return	k1gInSc1
of	of	k?
the	the	k?
Mac	Mac	kA
</s>
<s>
Samu	Sam	k1gMnSc3
sebe	sebe	k3xPyFc4
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
New	New	k1gFnSc3
Kid	Kid	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Talk	Talk	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2019	#num#	k4
</s>
<s>
Politik	politik	k1gMnSc1
</s>
<s>
Lizbeth	Lizbeth	k1gMnSc1
Sloan	Sloan	k1gMnSc1
</s>
<s>
3	#num#	k4
díly	dílo	k1gNnPc7
</s>
<s>
2020	#num#	k4
</s>
<s>
Na	na	k7c6
tenkém	tenký	k2eAgInSc6d1
ledě	led	k1gInSc6
</s>
<s>
Carol	Carol	k1gInSc1
Baker	Baker	k1gMnSc1
</s>
<s>
hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
nominace	nominace	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Práce	práce	k1gFnSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
CAMIE	CAMIE	kA
Awards	Awards	k1gInSc1
</s>
<s>
CAMIE	CAMIE	kA
Awards	Awards	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Slib	slib	k1gInSc1
věčné	věčný	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
National	Nationat	k5eAaImAgMnS,k5eAaPmAgMnS
Cowboy	Cowboa	k1gFnSc2
&	&	k?
Western	Western	kA
Heritage	Heritage	k1gInSc1
Museum	museum	k1gNnSc1
</s>
<s>
Bronze	bronz	k1gInSc5
Wrangler	Wrangler	k1gInSc4
</s>
<s>
Tři	tři	k4xCgInPc1
pohřby	pohřeb	k1gInPc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Screen	Screen	k2eAgInSc1d1
Actors	Actors	k1gInSc1
Guild	Guild	k1gMnSc1
Award	Award	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3
obsazení	obsazení	k1gNnSc1
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
–	–	k?
drama	drama	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Šílenci	šílenec	k1gMnPc1
z	z	k7c2
Manhattanu	Manhattan	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Screen	Screen	k2eAgInSc1d1
Actors	Actors	k1gInSc1
Guild	Guild	k1gMnSc1
Award	Award	k1gMnSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
drama	drama	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Screen	Screen	k2eAgInSc1d1
Actors	Actors	k1gInSc1
Guild	Guild	k1gMnSc1
Award	Award	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3
obsazení	obsazení	k1gNnSc1
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
–	–	k?
drama	drama	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Screen	Screen	k2eAgInSc1d1
Actors	Actors	k1gInSc1
Guild	Guild	k1gMnSc1
Award	Award	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Ceny	cena	k1gFnPc1
Emmy	Emma	k1gFnSc2
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
dramatickém	dramatický	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Satellite	Satellit	k1gInSc5
Award	Awardo	k1gNnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
dramatickém	dramatický	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
January	Januara	k1gFnSc2
Jones	Jonesa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
January	Januara	k1gFnSc2
Jones	Jones	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
January	Januar	k1gInPc1
Jones	Jonesa	k1gFnPc2
na	na	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
January	Januara	k1gFnPc1
Jones	Jonesa	k1gFnPc2
na	na	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
January	Januara	k1gFnPc1
Jones	Jonesa	k1gFnPc2
na	na	k7c4
Allmovie	Allmovius	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
January	Januara	k1gFnPc1
Jones	Jonesa	k1gFnPc2
na	na	k7c4
Yahoo	Yahoo	k1gNnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Movies	Movies	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
141470437	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1483	#num#	k4
2455	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006102134	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
163733929	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006102134	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Televize	televize	k1gFnSc1
</s>
