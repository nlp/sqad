<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
je	být	k5eAaImIp3nS	být
ustálený	ustálený	k2eAgInSc4d1	ustálený
způsob	způsob	k1gInSc4	způsob
zkrácení	zkrácení	k1gNnSc4	zkrácení
slova	slovo	k1gNnSc2	slovo
nebo	nebo	k8xC	nebo
sousloví	sousloví	k1gNnSc2	sousloví
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
zkratky	zkratka	k1gFnPc1	zkratka
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
často	často	k6eAd1	často
užívaných	užívaný	k2eAgNnPc2d1	užívané
slov	slovo	k1gNnPc2	slovo
či	či	k8xC	či
ustálených	ustálený	k2eAgNnPc2d1	ustálené
spojení	spojení	k1gNnPc2	spojení
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
p.	p.	k?	p.
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
nám.	nám.	k?	nám.
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
zkrácení	zkrácení	k1gNnSc4	zkrácení
zápisu	zápis	k1gInSc2	zápis
víceslovných	víceslovný	k2eAgInPc2d1	víceslovný
názvů	název	k1gInPc2	název
institucí	instituce	k1gFnPc2	instituce
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
spolky	spolek	k1gInPc1	spolek
a	a	k8xC	a
podniky	podnik	k1gInPc1	podnik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokumentů	dokument	k1gInPc2	dokument
(	(	kIx(	(
<g/>
deklarací	deklarace	k1gFnPc2	deklarace
<g/>
,	,	kIx,	,
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborných	odborný	k2eAgInPc2d1	odborný
termínů	termín	k1gInPc2	termín
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
MHD	MHD	kA	MHD
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zkratky	zkratka	k1gFnPc4	zkratka
používali	používat	k5eAaImAgMnP	používat
už	už	k6eAd1	už
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
např	např	kA	např
SPQR	SPQR	kA	SPQR
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
lidu	lid	k1gInSc2	lid
římského	římský	k2eAgMnSc2d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
nápisů	nápis	k1gInPc2	nápis
a	a	k8xC	a
zkratkami	zkratka	k1gFnPc7	zkratka
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
používaných	používaný	k2eAgInPc6d1	používaný
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
epigrafika	epigrafika	k1gFnSc1	epigrafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověkých	středověký	k2eAgInPc6d1	středověký
rukopisných	rukopisný	k2eAgInPc6d1	rukopisný
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
úspornosti	úspornost	k1gFnSc3	úspornost
používalo	používat	k5eAaImAgNnS	používat
více	hodně	k6eAd2	hodně
druhů	druh	k1gMnPc2	druh
zkratek	zkratka	k1gFnPc2	zkratka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kontrakce	kontrakce	k1gFnSc2	kontrakce
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
DNS	DNS	kA	DNS
=	=	kIx~	=
dominus	dominus	k1gMnSc1	dominus
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nahrazení	nahrazení	k1gNnSc4	nahrazení
častých	častý	k2eAgFnPc2d1	častá
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
značkou	značka	k1gFnSc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Rukopisnými	rukopisný	k2eAgFnPc7d1	rukopisná
zkratkami	zkratka	k1gFnPc7	zkratka
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
paleografie	paleografie	k1gFnSc1	paleografie
<g/>
,	,	kIx,	,
pomocná	pomocný	k2eAgFnSc1d1	pomocná
věda	věda	k1gFnSc1	věda
historická	historický	k2eAgFnSc1d1	historická
studující	studující	k1gFnSc7	studující
historická	historický	k2eAgNnPc1d1	historické
písma	písmo	k1gNnPc1	písmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Miloslava	Miloslav	k1gMnSc2	Miloslav
Sedláčka	Sedláček	k1gMnSc2	Sedláček
se	se	k3xPyFc4	se
užívání	užívání	k1gNnSc1	užívání
zkratek	zkratka	k1gFnPc2	zkratka
v	v	k7c6	v
jazykových	jazykový	k2eAgInPc6d1	jazykový
projevech	projev	k1gInPc6	projev
podstatně	podstatně	k6eAd1	podstatně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nejvíce	hodně	k6eAd3	hodně
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sedláčka	Sedláček	k1gMnSc2	Sedláček
nejvíce	nejvíce	k6eAd1	nejvíce
zkratek	zkratka	k1gFnPc2	zkratka
(	(	kIx(	(
<g/>
různého	různý	k2eAgInSc2d1	různý
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
administrativní	administrativní	k2eAgFnSc6d1	administrativní
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgFnSc3d1	výrobní
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Používání	používání	k1gNnSc1	používání
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
vzniku	vznik	k1gInSc2	vznik
zkratek	zkratka	k1gFnPc2	zkratka
bývá	bývat	k5eAaImIp3nS	bývat
úspornost	úspornost	k1gFnSc4	úspornost
vyjadřování	vyjadřování	k1gNnSc3	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zcela	zcela	k6eAd1	zcela
odporují	odporovat	k5eAaImIp3nP	odporovat
slovotvorným	slovotvorný	k2eAgMnPc3d1	slovotvorný
<g/>
,	,	kIx,	,
fonologickým	fonologický	k2eAgFnPc3d1	fonologická
i	i	k8xC	i
gramatickým	gramatický	k2eAgFnPc3d1	gramatická
zvyklostem	zvyklost	k1gFnPc3	zvyklost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
útvary	útvar	k1gInPc4	útvar
neadaptabilní	adaptabilní	k2eNgInPc4d1	adaptabilní
<g/>
.	.	kIx.	.
</s>
<s>
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nutné	nutný	k2eAgNnSc4d1	nutné
vyslovit	vyslovit	k5eAaPmF	vyslovit
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
zkratek	zkratka	k1gFnPc2	zkratka
používali	používat	k5eAaImAgMnP	používat
racionálně	racionálně	k6eAd1	racionálně
a	a	k8xC	a
vykázali	vykázat	k5eAaPmAgMnP	vykázat
jim	on	k3xPp3gMnPc3	on
v	v	k7c4	v
komunikaci	komunikace	k1gFnSc4	komunikace
takové	takový	k3xDgNnSc1	takový
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
hodnoceny	hodnotit	k5eAaImNgFnP	hodnotit
jako	jako	k8xC	jako
přínos	přínos	k1gInSc1	přínos
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jako	jako	k9	jako
ztěžování	ztěžování	k1gNnSc1	ztěžování
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
zkratky	zkratka	k1gFnPc1	zkratka
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgFnPc1d1	specifická
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
mezinárodně	mezinárodně	k6eAd1	mezinárodně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
některé	některý	k3yIgInPc1	některý
evropské	evropský	k2eAgInPc1d1	evropský
jazyky	jazyk	k1gInPc1	jazyk
přebíraly	přebírat	k5eAaImAgInP	přebírat
latinské	latinský	k2eAgFnPc4d1	Latinská
zkratky	zkratka	k1gFnSc2	zkratka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
etc	etc	k?	etc
<g/>
.	.	kIx.	.
=	=	kIx~	=
et	et	k?	et
cetera	cetera	k1gFnSc1	cetera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zčásti	zčásti	k6eAd1	zčásti
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
například	například	k6eAd1	například
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
povahu	povaha	k1gFnSc4	povaha
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
kódy	kód	k1gInPc1	kód
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
PL	PL	kA	PL
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
UK	UK	kA	UK
<g/>
,	,	kIx,	,
UA	UA	kA	UA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkratky	zkratka	k1gFnSc2	zkratka
měn	měna	k1gFnPc2	měna
(	(	kIx(	(
<g/>
ČZSK	ČZSK	kA	ČZSK
<g/>
,	,	kIx,	,
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
například	například	k6eAd1	například
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
používají	používat	k5eAaImIp3nP	používat
zpravidla	zpravidla	k6eAd1	zpravidla
nepřeložené	přeložený	k2eNgInPc1d1	nepřeložený
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
UITP	UITP	kA	UITP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
neformální	formální	k2eNgFnSc6d1	neformální
komunikaci	komunikace	k1gFnSc6	komunikace
(	(	kIx(	(
<g/>
internet	internet	k1gInSc1	internet
<g/>
,	,	kIx,	,
SMS	SMS	kA	SMS
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
specifické	specifický	k2eAgInPc4d1	specifický
prvky	prvek	k1gInPc4	prvek
jazykové	jazykový	k2eAgFnSc2d1	jazyková
ekonomie	ekonomie	k1gFnSc2	ekonomie
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
angličtiny	angličtina	k1gFnSc2	angličtina
i	i	k8xC	i
domácích	domácí	k2eAgInPc2d1	domácí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
anglické	anglický	k2eAgFnPc1d1	anglická
zkratky	zkratka	k1gFnPc1	zkratka
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
mezinárodně	mezinárodně	k6eAd1	mezinárodně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
ustálené	ustálený	k2eAgFnPc1d1	ustálená
(	(	kIx(	(
<g/>
btw	btw	k?	btw
<g/>
,	,	kIx,	,
lol	lol	k?	lol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
i	i	k9	i
specifické	specifický	k2eAgFnPc1d1	specifická
zkratky	zkratka	k1gFnPc1	zkratka
českých	český	k2eAgNnPc2d1	české
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
(	(	kIx(	(
<g/>
AOTJ	AOTJ	kA	AOTJ
=	=	kIx~	=
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
woe	woe	k?	woe
=	=	kIx~	=
vole	vole	k1gNnSc1	vole
<g/>
,	,	kIx,	,
ČKD	ČKD	kA	ČKD
=	=	kIx~	=
často	často	k6eAd1	často
kladené	kladený	k2eAgInPc4d1	kladený
dotazy	dotaz	k1gInPc4	dotaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkratky	zkratka	k1gFnPc1	zkratka
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
tvořeny	tvořit	k5eAaImNgInP	tvořit
s	s	k7c7	s
důvtipem	důvtip	k1gInSc7	důvtip
či	či	k8xC	či
jazykovým	jazykový	k2eAgInSc7d1	jazykový
humorem	humor	k1gInSc7	humor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
FCI	FCI	kA	FCI
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
frikulín	frikulín	k1gInSc1	frikulín
–	–	k?	–
free	free	k1gInSc1	free
<g/>
,	,	kIx,	,
cool	cool	k1gInSc1	cool
a	a	k8xC	a
in	in	k?	in
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
anglické	anglický	k2eAgFnPc4d1	anglická
zkratky	zkratka	k1gFnPc4	zkratka
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
využití	využití	k1gNnSc1	využití
podobnosti	podobnost	k1gFnSc2	podobnost
výslovnosti	výslovnost	k1gFnSc2	výslovnost
anglických	anglický	k2eAgFnPc2d1	anglická
číslovek	číslovka	k1gFnPc2	číslovka
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
a	a	k8xC	a
8	[number]	k4	8
s	s	k7c7	s
některými	některý	k3yIgNnPc7	některý
základními	základní	k2eAgNnPc7d1	základní
slovy	slovo	k1gNnPc7	slovo
či	či	k8xC	či
částmi	část	k1gFnPc7	část
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
U	U	kA	U
=	=	kIx~	=
too	too	k?	too
hard	hardo	k1gNnPc2	hardo
for	forum	k1gNnPc2	forum
you	you	k?	you
<g/>
,	,	kIx,	,
GR	GR	kA	GR
<g/>
8	[number]	k4	8
<g/>
=	=	kIx~	=
great	great	k1gInSc1	great
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
<g />
.	.	kIx.	.
</s>
<s>
má	můj	k3xOp1gFnSc1	můj
analogie	analogie	k1gFnSc1	analogie
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
=	=	kIx~	=
opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
S3H	S3H	k1gFnSc1	S3H
=	=	kIx~	=
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
3NEC	[number]	k4	3NEC
=	=	kIx~	=
Třinec	Třinec	k1gInSc1	Třinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zkratky	zkratka	k1gFnPc1	zkratka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
homonymní	homonymní	k2eAgFnPc4d1	homonymní
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
i	i	k8xC	i
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UK	UK	kA	UK
(	(	kIx(	(
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
i	i	k9	i
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
nebo	nebo	k8xC	nebo
dopravka	dopravka	k1gFnSc1	dopravka
(	(	kIx(	(
<g/>
dopravní	dopravní	k2eAgFnSc1d1	dopravní
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
výchova	výchova	k1gFnSc1	výchova
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třídění	třídění	k1gNnPc1	třídění
zkratek	zkratka	k1gFnPc2	zkratka
==	==	k?	==
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
pojal	pojmout	k5eAaPmAgInS	pojmout
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
"	"	kIx"	"
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgMnSc4	jenž
pojal	pojmout	k5eAaPmAgMnS	pojmout
všechny	všechen	k3xTgInPc4	všechen
morfologické	morfologický	k2eAgInPc4d1	morfologický
(	(	kIx(	(
<g/>
slovotvorné	slovotvorný	k2eAgInPc4d1	slovotvorný
<g/>
)	)	kIx)	)
abreviaturní	abreviaturní	k2eAgInPc4d1	abreviaturní
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
zkratková	zkratkový	k2eAgNnPc1d1	zkratkové
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nezahrnul	zahrnout	k5eNaPmAgInS	zahrnout
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
zkratky	zkratka	k1gFnPc1	zkratka
syntaktické	syntaktický	k2eAgFnPc1d1	syntaktická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
řadil	řadit	k5eAaImAgMnS	řadit
například	například	k6eAd1	například
univerbizaci	univerbizace	k1gFnSc4	univerbizace
a	a	k8xC	a
elipsu	elipsa	k1gFnSc4	elipsa
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
termínu	termín	k1gInSc2	termín
zkratka	zkratka	k1gFnSc1	zkratka
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
termín	termín	k1gInSc1	termín
značka	značka	k1gFnSc1	značka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
značky	značka	k1gFnPc1	značka
sice	sice	k8xC	sice
mají	mít	k5eAaImIp3nP	mít
zároveň	zároveň	k6eAd1	zároveň
povahu	povaha	k1gFnSc4	povaha
zkratek	zkratka	k1gFnPc2	zkratka
(	(	kIx(	(
<g/>
g	g	kA	g
pro	pro	k7c4	pro
gram	gram	k1gInSc4	gram
<g/>
,	,	kIx,	,
l	l	kA	l
pro	pro	k7c4	pro
litr	litr	k1gInSc4	litr
<g/>
,	,	kIx,	,
Kč	Kč	kA	Kč
pro	pro	k7c4	pro
koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
F	F	kA	F
pro	pro	k7c4	pro
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
G	G	kA	G
pro	pro	k7c4	pro
tíhu	tíha	k1gFnSc4	tíha
<g/>
,	,	kIx,	,
v	v	k7c4	v
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
značkami	značka	k1gFnPc7	značka
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
i	i	k9	i
mnohé	mnohý	k2eAgInPc4d1	mnohý
symboly	symbol	k1gInPc4	symbol
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nepísmennou	písmenný	k2eNgFnSc4d1	písmenný
podobu	podoba	k1gFnSc4	podoba
(	(	kIx(	(
<g/>
matematické	matematický	k2eAgInPc1d1	matematický
operátory	operátor	k1gInPc1	operátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písmena	písmeno	k1gNnPc1	písmeno
jiných	jiný	k2eAgFnPc2d1	jiná
abeced	abeceda	k1gFnPc2	abeceda
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
řecké	řecký	k2eAgNnSc1d1	řecké
<g/>
)	)	kIx)	)
či	či	k8xC	či
písmenná	písmenný	k2eAgNnPc4d1	písmenné
nebo	nebo	k8xC	nebo
číselná	číselný	k2eAgNnPc4d1	číselné
označení	označení	k1gNnPc4	označení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
zkrácením	zkrácení	k1gNnSc7	zkrácení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
jako	jako	k9	jako
pořadové	pořadový	k2eAgNnSc4d1	pořadové
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
body	bod	k1gInPc4	bod
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
mezi	mezi	k7c7	mezi
značkami	značka	k1gFnPc7	značka
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tzv.	tzv.	kA	tzv.
značky	značka	k1gFnSc2	značka
jazykové	jazykový	k2eAgFnSc2d1	jazyková
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
většině	většina	k1gFnSc6	většina
zkratky	zkratka	k1gFnSc2	zkratka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
povahu	povaha	k1gFnSc4	povaha
samostatných	samostatný	k2eAgInPc2d1	samostatný
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nemají	mít	k5eNaImIp3nP	mít
funkci	funkce	k1gFnSc3	funkce
jen	jen	k9	jen
zkracovat	zkracovat	k5eAaImF	zkracovat
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
krátkou	krátký	k2eAgFnSc7d1	krátká
výraznou	výrazný	k2eAgFnSc7d1	výrazná
formou	forma	k1gFnSc7	forma
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pojem	pojem	k1gInSc4	pojem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zatímco	zatímco	k8xS	zatímco
Pravidla	pravidlo	k1gNnPc4	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
značky	značka	k1gFnPc4	značka
i	i	k8xC	i
iniciálové	iniciálový	k2eAgFnPc1d1	iniciálová
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
<g/>
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
zkratek	zkratka	k1gFnPc2	zkratka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
grafické	grafický	k2eAgNnSc1d1	grafické
a	a	k8xC	a
graficko-fonické	grafickoonický	k2eAgNnSc1d1	graficko-fonický
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgFnPc1d1	grafická
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čtou	číst	k5eAaImIp3nP	číst
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
nezkrácená	zkrácený	k2eNgNnPc1d1	nezkrácené
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
graficko-fonické	grafickoonický	k2eAgFnPc1d1	graficko-fonický
se	se	k3xPyFc4	se
realizují	realizovat	k5eAaBmIp3nP	realizovat
graficky	graficky	k6eAd1	graficky
i	i	k9	i
zvukově	zvukově	k6eAd1	zvukově
<g/>
.	.	kIx.	.
<g/>
Ustálené	ustálený	k2eAgFnSc2d1	ustálená
grafické	grafický	k2eAgFnSc2d1	grafická
zkratky	zkratka	k1gFnSc2	zkratka
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
iniciálové	iniciálový	k2eAgInPc4d1	iniciálový
(	(	kIx(	(
<g/>
č.	č.	k?	č.
–	–	k?	–
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rámcové	rámcový	k2eAgNnSc4d1	rámcové
(	(	kIx(	(
<g/>
pí	pí	k1gNnSc4	pí
–	–	k?	–
paní	paní	k1gFnSc1	paní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lineární	lineární	k2eAgFnSc1d1	lineární
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
–	–	k?	–
profesor	profesor	k1gMnSc1	profesor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
skeletové	skeletový	k2eAgInPc1d1	skeletový
(	(	kIx(	(
<g/>
plk.	plk.	kA	plk.
–	–	k?	–
plukovník	plukovník	k1gMnSc1	plukovník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
(	(	kIx(	(
<g/>
pplk.	pplk.	kA	pplk.
–	–	k?	–
podplukovník	podplukovník	k1gMnSc1	podplukovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Graficko-fonické	grafickoonický	k2eAgFnSc2d1	graficko-fonický
zkratky	zkratka	k1gFnSc2	zkratka
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
dělil	dělit	k5eAaImAgMnS	dělit
na	na	k7c4	na
</s>
</p>
<p>
<s>
nelexikální	lexikální	k2eNgFnPc1d1	lexikální
(	(	kIx(	(
<g/>
iniciálové	iniciálový	k2eAgFnPc1d1	iniciálová
zkratky	zkratka	k1gFnPc1	zkratka
jako	jako	k8xC	jako
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přechodné	přechodný	k2eAgInPc4d1	přechodný
(	(	kIx(	(
<g/>
iniciálová	iniciálový	k2eAgNnPc1d1	iniciálový
zkratková	zkratkový	k2eAgNnPc1d1	zkratkové
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xC	jako
ÚRO	Úro	k1gNnSc1	Úro
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lexikální	lexikální	k2eAgFnSc1d1	lexikální
(	(	kIx(	(
<g/>
zkratková	zkratkový	k2eAgNnPc1d1	zkratkové
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xS	jako
alobal	alobal	k1gInSc1	alobal
<g/>
,	,	kIx,	,
zkrácená	zkrácený	k2eAgNnPc1d1	zkrácené
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xC	jako
limo	limo	k1gNnSc1	limo
<g/>
,	,	kIx,	,
derivačně	derivačně	k6eAd1	derivačně
zkratková	zkratkový	k2eAgNnPc1d1	zkratkové
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xS	jako
Lachema	lachema	k1gFnSc1	lachema
<g/>
,	,	kIx,	,
derivačně	derivačně	k6eAd1	derivačně
zkrácená	zkrácený	k2eAgNnPc1d1	zkrácené
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xS	jako
profka	profka	k1gFnSc1	profka
<g/>
)	)	kIx)	)
<g/>
Sedláček	Sedláček	k1gMnSc1	Sedláček
si	se	k3xPyFc3	se
však	však	k9	však
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
neostrosti	neostrost	k1gFnPc4	neostrost
Hrbáčkova	Hrbáčkův	k2eAgNnSc2d1	Hrbáčkův
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
,	,	kIx,	,
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
například	například	k6eAd1	například
slovo	slovo	k1gNnSc1	slovo
úča	úča	k?	úča
řadí	řadit	k5eAaImIp3nS	řadit
ke	k	k7c3	k
slovům	slovo	k1gNnPc3	slovo
zkráceným	zkrácený	k2eAgNnPc3d1	zkrácené
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
podobně	podobně	k6eAd1	podobně
utvořená	utvořený	k2eAgNnPc4d1	utvořené
slovo	slovo	k1gNnSc4	slovo
sváča	sváč	k1gInSc2	sváč
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
řadí	řadit	k5eAaImIp3nS	řadit
ke	k	k7c3	k
slovům	slovo	k1gNnPc3	slovo
derivačně	derivačně	k6eAd1	derivačně
zkráceným	zkrácený	k2eAgMnPc3d1	zkrácený
<g/>
.	.	kIx.	.
</s>
<s>
Sedláček	Sedláček	k1gMnSc1	Sedláček
rovněž	rovněž	k9	rovněž
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
kategorie	kategorie	k1gFnSc2	kategorie
kombinovaných	kombinovaný	k2eAgFnPc2d1	kombinovaná
zkratek	zkratka	k1gFnPc2	zkratka
pro	pro	k7c4	pro
zkratky	zkratka	k1gFnPc4	zkratka
typu	typ	k1gInSc2	typ
pplk.	pplk.	kA	pplk.
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
postačilo	postačit	k5eAaPmAgNnS	postačit
řadit	řadit	k5eAaImF	řadit
mezi	mezi	k7c4	mezi
skeletové	skeletový	k2eAgNnSc4d1	skeletové
<g/>
.	.	kIx.	.
</s>
<s>
Sedláček	Sedláček	k1gMnSc1	Sedláček
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
i	i	k9	i
odlišné	odlišný	k2eAgInPc4d1	odlišný
ustálené	ustálený	k2eAgInPc4d1	ustálený
způsoby	způsob	k1gInPc4	způsob
výslovnosti	výslovnost	k1gFnSc2	výslovnost
téže	tenže	k3xDgFnSc2	tenže
zkratky	zkratka	k1gFnSc2	zkratka
(	(	kIx(	(
<g/>
KRB	krb	k1gInSc1	krb
jako	jako	k8xS	jako
ká-er-bé	kárý	k2eAgFnPc1d1	ká-er-bý
i	i	k9	i
jako	jako	k9	jako
krp	krp	k?	krp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
v	v	k7c6	v
Hrbáčkově	Hrbáčkův	k2eAgFnSc6d1	Hrbáčkova
terminologii	terminologie	k1gFnSc6	terminologie
považuje	považovat	k5eAaImIp3nS	považovat
Sedláček	Sedláček	k1gMnSc1	Sedláček
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
iniciálová	iniciálový	k2eAgFnSc1d1	iniciálová
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
použit	použit	k2eAgInSc1d1	použit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
významech	význam	k1gInPc6	význam
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pro	pro	k7c4	pro
typ	typ	k1gInSc4	typ
grafických	grafický	k2eAgFnPc2d1	grafická
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
sl.	sl.	k?	sl.
<g/>
,	,	kIx,	,
stč	stč	k?	stč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
typ	typ	k1gInSc4	typ
graficko-fonických	grafickoonický	k2eAgFnPc2d1	graficko-fonický
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sedláček	Sedláček	k1gMnSc1	Sedláček
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
nazývat	nazývat	k5eAaImF	nazývat
první	první	k4xOgFnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
iniciální	iniciální	k2eAgFnSc2d1	iniciální
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
iniciálové	iniciálový	k2eAgFnSc2d1	iniciálová
nebo	nebo	k8xC	nebo
verzálkové	verzálek	k1gMnPc5	verzálek
<g/>
.	.	kIx.	.
<g/>
Zkracování	zkracování	k1gNnSc4	zkracování
(	(	kIx(	(
<g/>
abreviaci	abreviace	k1gFnSc4	abreviace
<g/>
)	)	kIx)	)
považuje	považovat	k5eAaImIp3nS	považovat
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
vedle	vedle	k7c2	vedle
derivace	derivace	k1gFnSc2	derivace
a	a	k8xC	a
kompozice	kompozice	k1gFnSc2	kompozice
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
základních	základní	k2eAgInPc2d1	základní
slovotvorných	slovotvorný	k2eAgInPc2d1	slovotvorný
způsobů	způsob	k1gInPc2	způsob
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
slovotvorné	slovotvorný	k2eAgNnSc4d1	slovotvorné
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
jen	jen	k9	jen
zkráceniny	zkrácenina	k1gFnPc4	zkrácenina
tvořené	tvořený	k2eAgFnPc4d1	tvořená
ze	z	k7c2	z
seskupení	seskupení	k1gNnSc2	seskupení
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k9	už
toto	tento	k3xDgNnSc1	tento
seskupení	seskupení	k1gNnSc1	seskupení
je	být	k5eAaImIp3nS	být
ustáleným	ustálený	k2eAgNnSc7d1	ustálené
souslovím	sousloví	k1gNnSc7	sousloví
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
de	de	k?	de
facto	facto	k1gNnSc1	facto
kompoziční	kompoziční	k2eAgFnSc4d1	kompoziční
abreviaci	abreviace	k1gFnSc4	abreviace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
prosté	prostý	k2eAgNnSc1d1	prosté
zkracování	zkracování	k1gNnSc1	zkracování
(	(	kIx(	(
<g/>
auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
limo	limo	k1gNnSc1	limo
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
iniciálové	iniciálový	k2eAgFnPc1d1	iniciálová
zkratky	zkratka	k1gFnPc1	zkratka
neřadí	řadit	k5eNaImIp3nP	řadit
do	do	k7c2	do
tématu	téma	k1gNnSc2	téma
tvoření	tvoření	k1gNnSc2	tvoření
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
tématu	téma	k1gNnSc2	téma
tvoření	tvoření	k1gNnSc2	tvoření
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
.	.	kIx.	.
<g/>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
AV	AV	kA	AV
ČR	ČR	kA	ČR
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
publikacích	publikace	k1gFnPc6	publikace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
češtiny	čeština	k1gFnSc2	čeština
obvykle	obvykle	k6eAd1	obvykle
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
zkratky	zkratka	k1gFnPc1	zkratka
na	na	k7c6	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
čistě	čistě	k6eAd1	čistě
grafické	grafický	k2eAgInPc1d1	grafický
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
čtou	číst	k5eAaImIp3nP	číst
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
celé	celý	k2eAgNnSc1d1	celé
nezkrácené	zkrácený	k2eNgNnSc1d1	nezkrácené
slovo	slovo	k1gNnSc1	slovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
iniciální	iniciální	k2eAgInPc1d1	iniciální
<g/>
,	,	kIx,	,
iniciálové	iniciálový	k2eAgInPc1d1	iniciálový
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
=	=	kIx~	=
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
=	=	kIx~	=
sbírky	sbírka	k1gFnPc4	sbírka
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
nebo	nebo	k8xC	nebo
složené	složený	k2eAgNnSc1d1	složené
či	či	k8xC	či
sloučené	sloučený	k2eAgNnSc1d1	sloučené
(	(	kIx(	(
<g/>
čs	čs	kA	čs
<g/>
.	.	kIx.	.
=	=	kIx~	=
československý	československý	k2eAgInSc4d1	československý
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
či	či	k8xC	či
č.	č.	k?	č.
p.	p.	k?	p.
=	=	kIx~	=
číslo	číslo	k1gNnSc1	číslo
popisné	popisný	k2eAgNnSc1d1	popisné
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kontrakční	kontrakční	k2eAgFnPc4d1	kontrakční
(	(	kIx(	(
<g/>
stažené	stažený	k2eAgFnPc4d1	stažená
<g/>
)	)	kIx)	)
z	z	k7c2	z
iniciály	iniciála	k1gFnSc2	iniciála
a	a	k8xC	a
konce	konec	k1gInSc2	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
pí	pí	k1gNnSc1	pí
=	=	kIx~	=
paní	paní	k1gFnSc1	paní
<g/>
,	,	kIx,	,
fa	fa	k1gNnSc1	fa
=	=	kIx~	=
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
faktura	faktura	k1gFnSc1	faktura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
</s>
</p>
<p>
<s>
skeletové	skeletový	k2eAgFnPc1d1	skeletová
<g/>
,	,	kIx,	,
vypouštějící	vypouštějící	k2eAgFnPc1d1	vypouštějící
samohlásky	samohláska	k1gFnPc1	samohláska
(	(	kIx(	(
<g/>
kpt.	kpt.	k?	kpt.
=	=	kIx~	=
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
graficko-fonické	grafickoonický	k2eAgFnPc1d1	graficko-fonický
(	(	kIx(	(
<g/>
iniciálové	iniciálový	k2eAgFnPc1d1	iniciálová
–	–	k?	–
tvořené	tvořený	k2eAgFnPc1d1	tvořená
jen	jen	k6eAd1	jen
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
skupinové	skupinový	k2eAgNnSc1d1	skupinové
–	–	k?	–
tvořené	tvořený	k2eAgNnSc1d1	tvořené
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
slabik	slabika	k1gFnPc2	slabika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hláskované	hláskovaný	k2eAgInPc1d1	hláskovaný
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ČMFS	ČMFS	kA	ČMFS
<g/>
,	,	kIx,	,
SNB	SNB	kA	SNB
<g/>
,	,	kIx,	,
čtou	číst	k5eAaImIp3nP	číst
se	se	k3xPyFc4	se
ú-es-á	ús-é	k1gNnPc1	ú-es-é
<g/>
,	,	kIx,	,
esenbé	esenbé	k?	esenbé
<g/>
)	)	kIx)	)
a	a	k8xC	a
nesklonné	sklonný	k2eNgFnPc1d1	nesklonná
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
jako	jako	k9	jako
iniciálové	iniciálový	k2eAgFnPc1d1	iniciálová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
zkratky	zkratka	k1gFnPc1	zkratka
vlastní	vlastní	k2eAgFnPc1d1	vlastní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
akronymní	akronymnět	k5eAaPmIp3nS	akronymnět
(	(	kIx(	(
<g/>
AMU	AMU	kA	AMU
<g/>
,	,	kIx,	,
ROPID	ROPID	kA	ROPID
<g/>
)	)	kIx)	)
čtou	číst	k5eAaImIp3nP	číst
se	se	k3xPyFc4	se
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
amu	amu	k?	amu
<g/>
,	,	kIx,	,
ropid	ropid	k1gInSc1	ropid
<g/>
)	)	kIx)	)
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
i	i	k9	i
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
čedoku	čedok	k1gInSc6	čedok
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
zkratek	zkratka	k1gFnPc2	zkratka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
oba	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
(	(	kIx(	(
<g/>
ODA	ODA	kA	ODA
=	=	kIx~	=
oda	oda	k?	oda
i	i	k9	i
ó-dé-á	óét	k5eAaPmIp3nS	ó-dé-at
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
je	on	k3xPp3gFnPc4	on
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c2	za
zkratky	zkratka	k1gFnSc2	zkratka
fonické	fonický	k2eAgFnSc2d1	fonická
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
akronymní	akronymní	k2eAgFnPc4d1	akronymní
zkratky	zkratka	k1gFnPc4	zkratka
patří	patřit	k5eAaImIp3nP	patřit
většinou	většinou	k6eAd1	většinou
zkratky	zkratka	k1gFnPc1	zkratka
tvořené	tvořený	k2eAgFnPc1d1	tvořená
jako	jako	k8xC	jako
skupinové	skupinový	k2eAgFnPc1d1	skupinová
(	(	kIx(	(
<g/>
Čedok	Čedok	k1gInSc1	Čedok
<g/>
,	,	kIx,	,
Spofa	Spof	k1gMnSc4	Spof
<g/>
,	,	kIx,	,
Chemopetrol	chemopetrol	k1gInSc1	chemopetrol
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
i	i	k9	i
zkratky	zkratka	k1gFnPc1	zkratka
iniciálové	iniciálový	k2eAgFnPc1d1	iniciálová
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
složení	složení	k1gNnSc4	složení
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
i	i	k9	i
jiné	jiný	k2eAgNnSc4d1	jiné
čtení	čtení	k1gNnSc4	čtení
než	než	k8xS	než
hláskování	hláskování	k1gNnSc4	hláskování
<g/>
;	;	kIx,	;
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
své	svůj	k3xOyFgFnSc2	svůj
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
příznačný	příznačný	k2eAgInSc1d1	příznačný
pro	pro	k7c4	pro
hovorový	hovorový	k2eAgInSc4d1	hovorový
styl	styl	k1gInSc4	styl
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
v	v	k7c6	v
Unesku	Unesco	k1gNnSc6	Unesco
<g/>
,	,	kIx,	,
dikové	dik	k1gMnPc1	dik
=	=	kIx~	=
držitelé	držitel	k1gMnPc1	držitel
investičních	investiční	k2eAgInPc2d1	investiční
kupónů	kupón	k1gInPc2	kupón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
fonické	fonický	k2eAgNnSc1d1	fonické
(	(	kIx(	(
<g/>
zkratková	zkratkový	k2eAgNnPc1d1	zkratkové
slova	slovo	k1gNnPc1	slovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zkráceniny	zkrácenina	k1gFnPc1	zkrácenina
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
domácké	domácký	k2eAgFnPc4d1	domácká
podoby	podoba	k1gFnPc4	podoba
osobních	osobní	k2eAgNnPc2d1	osobní
jmen	jméno	k1gNnPc2	jméno
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
krácením	krácení	k1gNnSc7	krácení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Jindra	Jindra	k1gFnSc1	Jindra
místo	místo	k7c2	místo
Jindřiška	Jindřiška	k1gFnSc1	Jindřiška
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
jako	jako	k8xC	jako
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
od	od	k7c2	od
Helenka	Helenka	k1gFnSc1	Helenka
či	či	k8xC	či
Majdalenka	Majdalenka	k1gFnSc1	Majdalenka
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
podoba	podoba	k1gFnSc1	podoba
kodifikována	kodifikován	k2eAgFnSc1d1	kodifikována
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc1d1	základní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
staženiny	staženina	k1gFnPc1	staženina
(	(	kIx(	(
<g/>
auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
kilo	kilo	k1gNnSc1	kilo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zkratkové	zkratkový	k2eAgFnPc1d1	zkratková
složeniny	složenina	k1gFnPc1	složenina
(	(	kIx(	(
<g/>
karma	karma	k1gFnSc1	karma
<g/>
,	,	kIx,	,
Čedok	Čedok	k1gInSc1	Čedok
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zkratkové	zkratkový	k2eAgFnPc1d1	zkratková
odvozeniny	odvozenina	k1gFnPc1	odvozenina
(	(	kIx(	(
<g/>
esenbák	esenbák	k?	esenbák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
připodobeniny	připodobenina	k1gFnPc1	připodobenina
(	(	kIx(	(
<g/>
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
Liduška	Liduška	k1gFnSc1	Liduška
<g/>
)	)	kIx)	)
<g/>
Běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
fonického	fonický	k2eAgNnSc2d1	fonické
zkracování	zkracování	k1gNnSc2	zkracování
je	být	k5eAaImIp3nS	být
univerbizace	univerbizace	k1gFnSc1	univerbizace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nahrazování	nahrazování	k1gNnSc1	nahrazování
víceslovných	víceslovný	k2eAgInPc2d1	víceslovný
pojmů	pojem	k1gInPc2	pojem
jediným	jediný	k2eAgNnSc7d1	jediné
sloučeným	sloučený	k2eAgNnSc7d1	sloučené
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
adjektivum	adjektivum	k1gNnSc1	adjektivum
je	být	k5eAaImIp3nS	být
zpodstatněno	zpodstatnit	k5eAaPmNgNnS	zpodstatnit
rodovou	rodový	k2eAgFnSc7d1	rodová
příponou	přípona	k1gFnSc7	přípona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
vypuštěné	vypuštěný	k2eAgNnSc4d1	vypuštěné
substantivum	substantivum	k1gNnSc4	substantivum
(	(	kIx(	(
<g/>
silná	silný	k2eAgFnSc1d1	silná
cesta	cesta	k1gFnSc1	cesta
=	=	kIx~	=
silnice	silnice	k1gFnSc1	silnice
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
=	=	kIx~	=
minerálka	minerálka	k1gFnSc1	minerálka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
typem	typ	k1gInSc7	typ
univerbizace	univerbizace	k1gFnSc2	univerbizace
je	být	k5eAaImIp3nS	být
elyptická	elyptický	k2eAgFnSc1d1	elyptický
univerbizace	univerbizace	k1gFnSc1	univerbizace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
vynechána	vynechat	k5eAaPmNgFnS	vynechat
část	část	k1gFnSc1	část
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
vynecháno	vynechán	k2eAgNnSc1d1	vynecháno
substantivum	substantivum	k1gNnSc1	substantivum
a	a	k8xC	a
použit	použit	k2eAgInSc1d1	použit
samostný	samostný	k2eAgInSc1d1	samostný
přívlastek	přívlastek	k1gInSc1	přívlastek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vysoká	vysoký	k2eAgFnSc1d1	vysoká
(	(	kIx(	(
<g/>
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
knížka	knížka	k1gFnSc1	knížka
–	–	k?	–
s	s	k7c7	s
významem	význam	k1gInSc7	význam
průkaz	průkaz	k1gInSc4	průkaz
nevojáka	nevoják	k1gMnSc2	nevoják
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc4	osvobození
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
zkratek	zkratka	k1gFnPc2	zkratka
lze	lze	k6eAd1	lze
též	též	k9	též
posuzovat	posuzovat	k5eAaImF	posuzovat
stupeň	stupeň	k1gInSc4	stupeň
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
plném	plný	k2eAgNnSc6d1	plné
pojmenování	pojmenování	k1gNnSc6	pojmenování
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgInPc1d1	plynoucí
důsledky	důsledek	k1gInPc1	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Zkratky	zkratka	k1gFnPc1	zkratka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
nezávislosti	nezávislost	k1gFnSc2	nezávislost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
značky	značka	k1gFnPc1	značka
<g/>
.	.	kIx.	.
</s>
<s>
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
zkratek	zkratka	k1gFnPc2	zkratka
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
značnému	značný	k2eAgNnSc3d1	značné
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
původní	původní	k2eAgNnSc1d1	původní
znění	znění	k1gNnSc1	znění
(	(	kIx(	(
<g/>
CDU	CDU	kA	CDU
<g/>
,	,	kIx,	,
WC	WC	kA	WC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hrbáčka	Hrbáček	k1gMnSc2	Hrbáček
jako	jako	k8xS	jako
zkratky	zkratka	k1gFnPc1	zkratka
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
povědomí	povědomí	k1gNnSc6	povědomí
uživatelů	uživatel	k1gMnPc2	uživatel
jazyka	jazyk	k1gInSc2	jazyk
lépe	dobře	k6eAd2	dobře
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
vlastních	vlastní	k2eAgInPc2d1	vlastní
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
apelativní	apelativní	k2eAgFnSc1d1	apelativní
funkce	funkce	k1gFnSc1	funkce
povědomí	povědomí	k1gNnSc2	povědomí
zkratkovosti	zkratkovost	k1gFnSc2	zkratkovost
zastírá	zastírat	k5eAaImIp3nS	zastírat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
apelativní	apelativní	k2eAgFnPc1d1	apelativní
lexikální	lexikální	k2eAgFnPc1d1	lexikální
zkratky	zkratka	k1gFnPc1	zkratka
domácí	domácí	k2eAgFnPc1d1	domácí
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
cizí	cizí	k2eAgFnSc3d1	cizí
(	(	kIx(	(
<g/>
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
,	,	kIx,	,
radar	radar	k1gInSc1	radar
<g/>
,	,	kIx,	,
expo	expo	k1gNnSc1	expo
<g/>
,	,	kIx,	,
kolchoz	kolchoz	k1gInSc1	kolchoz
<g/>
,	,	kIx,	,
hobra	hobra	k1gFnSc1	hobra
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
podobu	podoba	k1gFnSc4	podoba
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
zkratkami	zkratka	k1gFnPc7	zkratka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jen	jen	k6eAd1	jen
etymologicky	etymologicky	k6eAd1	etymologicky
<g/>
.	.	kIx.	.
</s>
<s>
Kdosi	kdosi	k3yInSc1	kdosi
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
zkratky	zkratka	k1gFnSc2	zkratka
strukturní	strukturní	k2eAgFnSc2d1	strukturní
(	(	kIx(	(
<g/>
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
)	)	kIx)	)
a	a	k8xC	a
etymologické	etymologický	k2eAgFnPc1d1	etymologická
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
etymologického	etymologický	k2eAgInSc2d1	etymologický
slovníku	slovník	k1gInSc2	slovník
nepoznáme	poznat	k5eNaPmIp1nP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Sedláček	Sedláček	k1gMnSc1	Sedláček
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
zkratky	zkratka	k1gFnPc1	zkratka
spisovné	spisovný	k2eAgFnPc1d1	spisovná
a	a	k8xC	a
nespisovné	spisovný	k2eNgFnPc1d1	nespisovná
<g/>
.	.	kIx.	.
</s>
<s>
Naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
normativní	normativní	k2eAgInSc4d1	normativní
přístup	přístup	k1gInSc4	přístup
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jazykové	jazykový	k2eAgFnSc2d1	jazyková
kultury	kultura	k1gFnSc2	kultura
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
soustředěn	soustředěn	k2eAgInSc4d1	soustředěn
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
používání	používání	k1gNnSc6	používání
zkratek	zkratka	k1gFnPc2	zkratka
ve	v	k7c6	v
spisovném	spisovný	k2eAgInSc6d1	spisovný
projevu	projev	k1gInSc6	projev
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nespisovné	spisovný	k2eNgFnPc1d1	nespisovná
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
leckdy	leckdy	k6eAd1	leckdy
spontánního	spontánní	k2eAgInSc2d1	spontánní
či	či	k8xC	či
expresivního	expresivní	k2eAgInSc2d1	expresivní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
jen	jen	k9	jen
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teoreticky	teoreticky	k6eAd1	teoreticky
lingvistického	lingvistický	k2eAgMnSc2d1	lingvistický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sylabické	sylabický	k2eAgFnPc1d1	sylabická
zkratky	zkratka	k1gFnPc1	zkratka
===	===	k?	===
</s>
</p>
<p>
<s>
Sylabická	sylabický	k2eAgFnSc1d1	sylabická
zkratka	zkratka	k1gFnSc1	zkratka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tvořena	tvořit	k5eAaImNgFnS	tvořit
počátečními	počáteční	k2eAgFnPc7d1	počáteční
slabikami	slabika	k1gFnPc7	slabika
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
internet	internet	k1gInSc1	internet
=	=	kIx~	=
International	International	k1gFnSc1	International
+	+	kIx~	+
net	net	k?	net
(	(	kIx(	(
<g/>
mezinárodí	mezinárodit	k5eAaPmIp3nS	mezinárodit
síť	síť	k1gFnSc1	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
=	=	kIx~	=
International	International	k1gFnSc1	International
+	+	kIx~	+
police	police	k1gFnSc1	police
(	(	kIx(	(
<g/>
mezinárodí	mezinárodí	k1gNnSc1	mezinárodí
policie	policie	k1gFnSc2	policie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sylabické	sylabický	k2eAgFnPc1d1	sylabická
zkratky	zkratka	k1gFnPc1	zkratka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
píší	psát	k5eAaImIp3nP	psát
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
počátečním	počáteční	k2eAgNnSc7d1	počáteční
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
samostatná	samostatný	k2eAgNnPc4d1	samostatné
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jako	jako	k8xC	jako
hláskované	hláskovaný	k2eAgFnPc1d1	hláskovaná
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Užití	užití	k1gNnSc2	užití
====	====	k?	====
</s>
</p>
<p>
<s>
Sylabické	sylabický	k2eAgFnPc1d1	sylabická
zkratky	zkratka	k1gFnPc1	zkratka
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
příliš	příliš	k6eAd1	příliš
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
či	či	k8xC	či
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
někdejším	někdejší	k2eAgInSc6d1	někdejší
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používaly	používat	k5eAaImAgFnP	používat
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
státní	státní	k2eAgInSc4d1	státní
(	(	kIx(	(
<g/>
Kolchoz	kolchoz	k1gInSc4	kolchoz
<g/>
,	,	kIx,	,
SovTransportAvto	SovTransportAvto	k1gNnSc4	SovTransportAvto
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
socialistické	socialistický	k2eAgFnSc2d1	socialistická
organizace	organizace	k1gFnSc2	organizace
jako	jako	k8xC	jako
např.	např.	kA	např.
Kominterna	Kominterna	k1gFnSc1	Kominterna
(	(	kIx(	(
<g/>
komunistická	komunistický	k2eAgFnSc1d1	komunistická
internacionála	internacionála	k1gFnSc1	internacionála
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Komsomol	Komsomol	k1gInSc1	Komsomol
(	(	kIx(	(
<g/>
Kommunističeskij	Kommunističeskij	k1gFnSc1	Kommunističeskij
Sojuz	Sojuz	k1gInSc1	Sojuz
Moloďoži	Moloďož	k1gFnSc3	Moloďož
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
komunistický	komunistický	k2eAgInSc1d1	komunistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
Rusku	Rusko	k1gNnSc6	Rusko
používá	používat	k5eAaImIp3nS	používat
sylabických	sylabický	k2eAgFnPc2d1	sylabická
zkratek	zkratka	k1gFnPc2	zkratka
pro	pro	k7c4	pro
státní	státní	k2eAgFnPc4d1	státní
organizace	organizace	k1gFnPc4	organizace
Minoborony	Minoboron	k1gInPc1	Minoboron
(	(	kIx(	(
<g/>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
oborony	oboron	k1gInPc7	oboron
–	–	k?	–
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
)	)	kIx)	)
a	a	k8xC	a
Minobrnauki	Minobrnauki	k1gNnSc1	Minobrnauki
(	(	kIx(	(
<g/>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrazovanija	obrazovanij	k1gInSc2	obrazovanij
i	i	k8xC	i
nauki	nauk	k1gFnSc2	nauk
–	–	k?	–
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
vědu	věda	k1gFnSc4	věda
<g/>
)	)	kIx)	)
i	i	k9	i
nestátní	státní	k2eNgInPc1d1	nestátní
podniky	podnik	k1gInPc1	podnik
(	(	kIx(	(
<g/>
Rosněfť	Rosněfť	k1gFnSc1	Rosněfť
<g/>
,	,	kIx,	,
Roskosmos	Roskosmos	k1gInSc1	Roskosmos
<g/>
,	,	kIx,	,
Roskomnadzor	Roskomnadzor	k1gInSc1	Roskomnadzor
<g/>
)	)	kIx)	)
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
zkratky	zkratka	k1gFnPc1	zkratka
se	se	k3xPyFc4	se
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
prostředí	prostředí	k1gNnSc6	prostředí
Kripo	kripo	k1gMnSc2	kripo
pro	pro	k7c4	pro
Kriminalpolizei	Kriminalpolizei	k1gNnSc4	Kriminalpolizei
(	(	kIx(	(
<g/>
kriminální	kriminální	k2eAgFnSc2d1	kriminální
policie	policie	k1gFnSc2	policie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sylabickou	sylabický	k2eAgFnSc7d1	sylabická
zkratkou	zkratka	k1gFnSc7	zkratka
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
také	také	k9	také
Gestapo	gestapo	k1gNnSc4	gestapo
pro	pro	k7c4	pro
Geheime	Geheim	k1gMnSc5	Geheim
Staats-Polizei	Staats-Polize	k1gMnSc5	Staats-Polize
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
tajná	tajný	k2eAgFnSc1d1	tajná
státní	státní	k2eAgFnSc1d1	státní
policie	policie	k1gFnSc1	policie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
zkratky	zkratka	k1gFnPc1	zkratka
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
také	také	k9	také
v	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
napš	napš	k1gInSc1	napš
<g/>
.	.	kIx.	.
</s>
<s>
Stasi	stase	k1gFnSc4	stase
pro	pro	k7c4	pro
Staatssicherheit	Staatssicherheit	k1gInSc4	Staatssicherheit
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
státní	státní	k2eAgFnSc4d1	státní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Vopo	Vopo	k1gMnSc1	Vopo
Volkspolizist	Volkspolizist	k1gMnSc1	Volkspolizist
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
příslušník	příslušník	k1gMnSc1	příslušník
policie	policie	k1gFnSc2	policie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sylabické	sylabický	k2eAgFnPc1d1	sylabická
zkratky	zkratka	k1gFnPc1	zkratka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
mimo	mimo	k7c4	mimo
politické	politický	k2eAgNnSc4d1	politické
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
názvy	název	k1gInPc1	název
firem	firma	k1gFnPc2	firma
Aldi	Aldi	k1gNnSc2	Aldi
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
,	,	kIx,	,
Theo	Thea	k1gFnSc5	Thea
Albrechta	Albrecht	k1gMnSc2	Albrecht
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
výrazu	výraz	k1gInSc2	výraz
Diskont	diskont	k1gInSc1	diskont
(	(	kIx(	(
<g/>
sleva	sleva	k1gFnSc1	sleva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Haribo	Hariba	k1gFnSc5	Hariba
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc4	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
Hans	Hans	k1gMnSc1	Hans
Riegl	Riegl	k1gMnSc1	Riegl
a	a	k8xC	a
města	město	k1gNnSc2	město
Bonn	Bonn	k1gInSc1	Bonn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Či	či	k8xC	či
podobně	podobně	k6eAd1	podobně
název	název	k1gInSc1	název
firmy	firma	k1gFnSc2	firma
Adidas	Adidasa	k1gFnPc2	Adidasa
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
počátečních	počáteční	k2eAgFnPc2d1	počáteční
slabik	slabika	k1gFnPc2	slabika
jména	jméno	k1gNnSc2	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
Adolfa	Adolf	k1gMnSc2	Adolf
"	"	kIx"	"
<g/>
Adiho	Adi	k1gMnSc2	Adi
<g/>
"	"	kIx"	"
Dasslera	Dassler	k1gMnSc2	Dassler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sylabické	sylabický	k2eAgFnPc1d1	sylabická
zkratky	zkratka	k1gFnPc1	zkratka
se	se	k3xPyFc4	se
také	také	k9	také
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
jako	jako	k9	jako
názvy	název	k1gInPc1	název
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xC	jako
<g/>
,	,	kIx,	,
např.	např.	kA	např.
PEMEX	PEMEX	kA	PEMEX
(	(	kIx(	(
<g/>
Petróleos	Petróleos	k1gMnSc1	Petróleos
Mexicanos	Mexicanos	k1gMnSc1	Mexicanos
-	-	kIx~	-
"	"	kIx"	"
<g/>
Mexické	mexický	k2eAgInPc1d1	mexický
ropné	ropný	k2eAgInPc1d1	ropný
závody	závod	k1gInPc1	závod
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Fonafifo	Fonafifo	k1gNnSc1	Fonafifo
(	(	kIx(	(
<g/>
Fondo	Fondo	k1gNnSc1	Fondo
Nacional	Nacional	k1gFnPc2	Nacional
de	de	k?	de
Financimiento	Financimiento	k1gNnSc4	Financimiento
Forestal	Forestal	k1gFnSc2	Forestal
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
fond	fond	k1gInSc1	fond
financování	financování	k1gNnSc2	financování
lesnictví	lesnictví	k1gNnSc2	lesnictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
psaní	psaní	k1gNnSc2	psaní
==	==	k?	==
</s>
</p>
<p>
<s>
Takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
čistě	čistě	k6eAd1	čistě
grafické	grafický	k2eAgFnPc1d1	grafická
zkratky	zkratka	k1gFnPc1	zkratka
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
obecných	obecný	k2eAgNnPc2d1	obecné
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
sousloví	sousloví	k1gNnPc2	sousloví
a	a	k8xC	a
píší	psát	k5eAaImIp3nP	psát
se	se	k3xPyFc4	se
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgNnPc1d1	velké
písmena	písmeno	k1gNnPc1	písmeno
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
věty	věta	k1gFnSc2	věta
či	či	k8xC	či
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
zachována	zachovat	k5eAaPmNgFnS	zachovat
podle	podle	k7c2	podle
zkracovaných	zkracovaný	k2eAgNnPc2d1	zkracované
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
sousloví	sousloví	k1gNnPc2	sousloví
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kontrakčních	kontrakční	k2eAgFnPc2d1	kontrakční
zkratek	zkratka	k1gFnPc2	zkratka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zakončují	zakončovat	k5eAaImIp3nP	zakončovat
tečkou	tečka	k1gFnSc7	tečka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zkratka	zkratka	k1gFnSc1	zkratka
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pro	pro	k7c4	pro
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
jednotky	jednotka	k1gFnPc4	jednotka
a	a	k8xC	a
veličiny	veličina	k1gFnPc4	veličina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
tečky	tečka	k1gFnSc2	tečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složenou	složený	k2eAgFnSc7d1	složená
či	či	k8xC	či
sloučenou	sloučený	k2eAgFnSc7d1	sloučená
zkratkou	zkratka	k1gFnSc7	zkratka
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
počáteční	počáteční	k2eAgNnPc1d1	počáteční
písmena	písmeno	k1gNnPc1	písmeno
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnPc1	skupina
písmen	písmeno	k1gNnPc2	písmeno
obou	dva	k4xCgFnPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
utvořena	utvořen	k2eAgFnSc1d1	utvořena
<g/>
,	,	kIx,	,
spojují	spojovat	k5eAaImIp3nP	spojovat
a	a	k8xC	a
píší	psát	k5eAaImIp3nP	psát
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zkratku	zkratka	k1gFnSc4	zkratka
slova	slovo	k1gNnSc2	slovo
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
sloučení	sloučení	k1gNnSc6	sloučení
více	hodně	k6eAd2	hodně
zkratek	zkratka	k1gFnPc2	zkratka
téhož	týž	k3xTgNnSc2	týž
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
čs	čs	kA	čs
<g/>
.	.	kIx.	.
=	=	kIx~	=
československý	československý	k2eAgInSc4d1	československý
<g/>
,	,	kIx,	,
stč	stč	k?	stč
<g/>
.	.	kIx.	.
=	=	kIx~	=
staročeský	staročeský	k2eAgInSc1d1	staročeský
<g/>
,	,	kIx,	,
sevvých	sevvý	k2eAgFnPc2d1	sevvý
<g/>
.	.	kIx.	.
=	=	kIx~	=
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
<g/>
)	)	kIx)	)
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
sloučení	sloučení	k1gNnSc6	sloučení
počátečních	počáteční	k2eAgNnPc2d1	počáteční
písmen	písmeno	k1gNnPc2	písmeno
či	či	k8xC	či
skupin	skupina	k1gFnPc2	skupina
písmen	písmeno	k1gNnPc2	písmeno
z	z	k7c2	z
více	hodně	k6eAd2	hodně
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujících	následující	k2eAgNnPc2d1	následující
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
zkratky	zkratka	k1gFnPc1	zkratka
jsou	být	k5eAaImIp3nP	být
ustáleny	ustálit	k5eAaPmNgFnP	ustálit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
<g/>
)	)	kIx)	)
a	a	k8xC	a
oddělené	oddělený	k2eAgNnSc1d1	oddělené
psaní	psaní	k1gNnSc1	psaní
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
zkratek	zkratka	k1gFnPc2	zkratka
jsou	být	k5eAaImIp3nP	být
vžité	vžitý	k2eAgFnPc1d1	vžitá
sloučené	sloučený	k2eAgFnPc1d1	sloučená
i	i	k8xC	i
nesloučené	sloučený	k2eNgFnPc1d1	sloučený
verze	verze	k1gFnPc1	verze
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
správné	správný	k2eAgInPc4d1	správný
(	(	kIx(	(
<g/>
čj.	čj.	k?	čj.
i	i	k8xC	i
č.	č.	k?	č.
j.	j.	k?	j.
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
i	i	k8xC	i
č.	č.	k?	č.
p.	p.	k?	p.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
vícenásobná	vícenásobný	k2eAgFnSc1d1	vícenásobná
zkratka	zkratka	k1gFnSc1	zkratka
píše	psát	k5eAaImIp3nS	psát
ve	v	k7c6	v
stažené	stažený	k2eAgFnSc6d1	stažená
podobě	podoba	k1gFnSc6	podoba
bez	bez	k7c2	bez
mezer	mezera	k1gFnPc2	mezera
za	za	k7c7	za
tečkami	tečka	k1gFnPc7	tečka
(	(	kIx(	(
<g/>
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravopisné	pravopisný	k2eAgFnPc1d1	pravopisná
a	a	k8xC	a
typografické	typografický	k2eAgFnPc1d1	typografická
příručky	příručka	k1gFnPc1	příručka
obvykle	obvykle	k6eAd1	obvykle
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
chybný	chybný	k2eAgInSc1d1	chybný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
zabývají	zabývat	k5eAaImIp3nP	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
Obchodní	obchodní	k2eAgInSc1d1	obchodní
rejstřík	rejstřík	k1gInSc1	rejstřík
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
právní	právní	k2eAgFnSc2d1	právní
formy	forma	k1gFnSc2	forma
společnosti	společnost	k1gFnSc2	společnost
jako	jako	k9	jako
součásti	součást	k1gFnPc1	součást
jejího	její	k3xOp3gInSc2	její
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
a	a	k8xC	a
pisatelé	pisatel	k1gMnPc1	pisatel
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nP	muset
volit	volit	k5eAaImF	volit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dají	dát	k5eAaPmIp3nP	dát
přednost	přednost	k1gFnSc4	přednost
zachování	zachování	k1gNnSc2	zachování
přesnosti	přesnost	k1gFnSc2	přesnost
kodifikovaného	kodifikovaný	k2eAgInSc2d1	kodifikovaný
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obecnému	obecný	k2eAgNnSc3d1	obecné
pravopisně-typografickému	pravopisněypografický	k2eAgNnSc3d1	pravopisně-typografický
pravidlu	pravidlo	k1gNnSc3	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
zkratky	zkratka	k1gFnPc1	zkratka
mají	mít	k5eAaImIp3nP	mít
povahu	povaha	k1gFnSc4	povaha
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
bez	bez	k7c2	bez
tečky	tečka	k1gFnSc2	tečka
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
kg	kg	kA	kg
<g/>
,	,	kIx,	,
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
označení	označení	k1gNnPc2	označení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
povaha	povaha	k1gFnSc1	povaha
sporná	sporný	k2eAgFnSc1d1	sporná
či	či	k8xC	či
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
(	(	kIx(	(
<g/>
h	h	k?	h
jako	jako	k8xC	jako
značka	značka	k1gFnSc1	značka
a	a	k8xC	a
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
,	,	kIx,	,
ot	ot	k1gMnSc1	ot
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
zkratka	zkratka	k1gFnSc1	zkratka
či	či	k8xC	či
ot	ot	k1gMnSc1	ot
jako	jako	k8xS	jako
značka	značka	k1gFnSc1	značka
počtu	počet	k1gInSc2	počet
otáček	otáčka	k1gFnPc2	otáčka
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Richter	Richter	k1gMnSc1	Richter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
v	v	k7c6	v
Naší	náš	k3xOp1gFnSc6	náš
řeči	řeč	k1gFnSc6	řeč
připouštěl	připouštět	k5eAaImAgInS	připouštět
ve	v	k7c6	v
zlomkových	zlomkový	k2eAgInPc6d1	zlomkový
výrazech	výraz	k1gInPc6	výraz
dva	dva	k4xCgInPc1	dva
výklady	výklad	k1gInPc1	výklad
použití	použití	k1gNnSc2	použití
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
zachovávat	zachovávat	k5eAaImF	zachovávat
rozlišení	rozlišení	k1gNnSc4	rozlišení
zkratek	zkratka	k1gFnPc2	zkratka
od	od	k7c2	od
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
ot	ot	k1gMnSc1	ot
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
psát	psát	k5eAaImF	psát
i	i	k9	i
zkratky	zkratka	k1gFnPc1	zkratka
bez	bez	k7c2	bez
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
zlomek	zlomek	k1gInSc4	zlomek
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
složenou	složený	k2eAgFnSc4d1	složená
<g />
.	.	kIx.	.
</s>
<s>
značku	značka	k1gFnSc4	značka
(	(	kIx(	(
<g/>
ot	ot	k1gMnSc1	ot
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Starší	starý	k2eAgInSc1d2	starší
způsob	způsob	k1gInSc1	způsob
psaní	psaní	k1gNnSc3	psaní
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
psaní	psaní	k1gNnSc4	psaní
složených	složený	k2eAgInPc2d1	složený
zkratkových	zkratkový	k2eAgInPc2d1	zkratkový
přívlastků	přívlastek	k1gInPc2	přívlastek
s	s	k7c7	s
lomítkem	lomítko	k1gNnSc7	lomítko
a	a	k8xC	a
bez	bez	k7c2	bez
teček	tečka	k1gFnPc2	tečka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Ústí	ústí	k1gNnSc1	ústí
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
L	L	kA	L
nebo	nebo	k8xC	nebo
Žďár	Žďár	k1gInSc1	Žďár
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
S	s	k7c7	s
místo	místo	k7c2	místo
Ústí	ústí	k1gNnSc2	ústí
n.	n.	k?	n.
L.	L.	kA	L.
nebo	nebo	k8xC	nebo
Žďár	Žďár	k1gInSc1	Žďár
n.	n.	k?	n.
S.	S.	kA	S.
<g/>
Takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
graficko-fonické	grafickoonický	k2eAgFnSc2d1	graficko-fonický
zkratky	zkratka	k1gFnSc2	zkratka
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obvykle	obvykle	k6eAd1	obvykle
píší	psát	k5eAaImIp3nP	psát
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
a	a	k8xC	a
bez	bez	k7c2	bez
teček	tečka	k1gFnPc2	tečka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
velkými	velký	k2eAgFnPc7d1	velká
písmeny	písmeno	k1gNnPc7	písmeno
jsou	být	k5eAaImIp3nP	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
i	i	k8xC	i
ta	ten	k3xDgNnPc4	ten
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
víceslovném	víceslovný	k2eAgNnSc6d1	víceslovné
vlastním	vlastní	k2eAgNnSc6d1	vlastní
jménu	jméno	k1gNnSc6	jméno
píšou	psát	k5eAaImIp3nP	psát
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
zkratky	zkratka	k1gFnPc1	zkratka
s	s	k7c7	s
tečkami	tečka	k1gFnPc7	tečka
bez	bez	k7c2	bez
mez	mez	k1gFnSc4	mez
mezer	mezera	k1gFnPc2	mezera
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
U.I.T.	U.I.T.	k1gMnSc1	U.I.T.
<g/>
P.	P.	kA	P.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skloňování	skloňování	k1gNnSc2	skloňování
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
zkratek	zkratka	k1gFnPc2	zkratka
(	(	kIx(	(
<g/>
rámcové	rámcový	k2eAgFnPc1d1	rámcová
neboli	neboli	k8xC	neboli
kontrakční	kontrakční	k2eAgFnPc1d1	kontrakční
<g/>
,	,	kIx,	,
lexikální	lexikální	k2eAgFnSc1d1	lexikální
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
jako	jako	k9	jako
běžná	běžný	k2eAgNnPc1d1	běžné
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
akronymních	akronymní	k2eAgFnPc2d1	akronymní
zkratek	zkratka	k1gFnPc2	zkratka
praxe	praxe	k1gFnSc2	praxe
značně	značně	k6eAd1	značně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jazykové	jazykový	k2eAgFnSc6d1	jazyková
přizpůsobivosti	přizpůsobivost	k1gFnSc6	přizpůsobivost
dané	daný	k2eAgFnSc2d1	daná
zkratky	zkratka	k1gFnSc2	zkratka
<g/>
,	,	kIx,	,
zvyklostech	zvyklost	k1gFnPc6	zvyklost
i	i	k8xC	i
preferencích	preference	k1gFnPc6	preference
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
tematických	tematický	k2eAgFnPc2d1	tematická
příruček	příručka	k1gFnPc2	příručka
či	či	k8xC	či
monografií	monografie	k1gFnPc2	monografie
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
doporučeních	doporučení	k1gNnPc6	doporučení
zcela	zcela	k6eAd1	zcela
konzistentní	konzistentní	k2eAgFnSc1d1	konzistentní
<g/>
,	,	kIx,	,
Sedláček	Sedláček	k1gMnSc1	Sedláček
například	například	k6eAd1	například
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
že	že	k8xS	že
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
by	by	kYmCp3nS	by
od	od	k7c2	od
zkratky	zkratka	k1gFnSc2	zkratka
BROLN	BROLN	kA	BROLN
psal	psát	k5eAaImAgInS	psát
7	[number]	k4	7
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
BROLNem	BROLNem	k1gInSc1	BROLNem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
UNESCO	Unesco	k1gNnSc1	Unesco
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
psát	psát	k5eAaImF	psát
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
pádu	pád	k1gInSc2	pád
Uneskem	Unesco	k1gNnSc7	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
ani	ani	k9	ani
akronym	akronym	k1gInSc1	akronym
obvykle	obvykle	k6eAd1	obvykle
neskloňuje	skloňovat	k5eNaImIp3nS	skloňovat
(	(	kIx(	(
<g/>
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
do	do	k7c2	do
ody	ody	k?	ody
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
UNRA	Unra	k1gFnSc1	Unra
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
unře	unře	k1gFnSc6	unře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
problatické	problatický	k2eAgNnSc4d1	problatický
je	být	k5eAaImIp3nS	být
skloňování	skloňování	k1gNnSc4	skloňování
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
celá	celý	k2eAgFnSc1d1	celá
zkratka	zkratka	k1gFnSc1	zkratka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roli	role	k1gFnSc6	role
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
nedochází	docházet	k5eNaImIp3nS	docházet
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
k	k	k7c3	k
hláskovým	hláskový	k2eAgFnPc3d1	hlásková
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
problematické	problematický	k2eAgNnSc1d1	problematické
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zacházet	zacházet	k5eAaImF	zacházet
s	s	k7c7	s
verzálkovými	verzálkův	k2eAgFnPc7d1	verzálkův
zkratkami	zkratka	k1gFnPc7	zkratka
(	(	kIx(	(
<g/>
ROPID	ROPID	kA	ROPID
bez	bez	k7c2	bez
ROPIDU	ROPIDU	kA	ROPIDU
<g/>
,	,	kIx,	,
ROPIDu	ROPIDa	k1gFnSc4	ROPIDa
či	či	k8xC	či
Ropidu	Ropida	k1gFnSc4	Ropida
<g/>
,	,	kIx,	,
ČEDOK	Čedok	k1gInSc4	Čedok
bez	bez	k7c2	bez
Čedoku	Čedok	k1gInSc2	Čedok
<g/>
,	,	kIx,	,
s	s	k7c7	s
ČEDOKem	Čedok	k1gInSc7	Čedok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dotaz	dotaz	k1gInSc1	dotaz
"	"	kIx"	"
<g/>
Pojedete	jet	k5eAaImIp2nP	jet
MHD	MHD	kA	MHD
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
buď	buď	k8xC	buď
celými	celý	k2eAgNnPc7d1	celé
slovy	slovo	k1gNnPc7	slovo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pojetete	Pojetete	k1gFnSc7	Pojetete
městskou	městský	k2eAgFnSc7d1	městská
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dopravou	doprava	k1gFnSc7	doprava
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hláskováním	hláskování	k1gNnSc7	hláskování
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pojetete	Pojetete	k1gFnPc4	Pojetete
emhádé	emhádý	k2eAgFnPc4d1	emhádý
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
i	i	k9	i
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
akronymizaci	akronymizace	k1gFnSc6	akronymizace
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pojetete	Pojetete	k1gFnSc7	Pojetete
emhádou	emháda	k1gFnSc7	emháda
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžnější	běžný	k2eAgMnSc1d2	běžnější
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Pojedete	jet	k5eAaImIp2nP	jet
emhádéčkem	emhádéček	k1gInSc7	emhádéček
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Nesklonné	sklonný	k2eNgFnPc1d1	nesklonná
graficko-fonické	grafickoonický	k2eAgFnPc1d1	graficko-fonický
zkratky	zkratka	k1gFnPc1	zkratka
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
substantivní	substantivní	k2eAgFnSc4d1	substantivní
povahu	povaha	k1gFnSc4	povaha
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
nesklonná	sklonný	k2eNgNnPc1d1	nesklonné
zkratková	zkratkový	k2eAgNnPc1d1	zkratkové
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
kvůli	kvůli	k7c3	kvůli
mluvnické	mluvnický	k2eAgFnSc3d1	mluvnická
shodě	shoda	k1gFnSc3	shoda
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
přiřazení	přiřazení	k1gNnPc1	přiřazení
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
rodu	rod	k1gInSc2	rod
však	však	k9	však
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
si	se	k3xPyFc3	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
rod	rod	k1gInSc4	rod
původního	původní	k2eAgNnSc2d1	původní
označení	označení	k1gNnSc2	označení
(	(	kIx(	(
<g/>
emhádé	emhádý	k2eAgFnPc1d1	emhádý
jezdila	jezdit	k5eAaImAgFnS	jezdit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
získávají	získávat	k5eAaImIp3nP	získávat
vlastní	vlastní	k2eAgInSc4d1	vlastní
rod	rod	k1gInSc4	rod
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
vlastního	vlastní	k2eAgNnSc2d1	vlastní
zakončení	zakončení	k1gNnSc2	zakončení
(	(	kIx(	(
<g/>
emhádé	emhádý	k2eAgNnSc1d1	emhádé
jezdilo	jezdit	k5eAaImAgNnS	jezdit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Akronym	akronym	k1gInSc1	akronym
</s>
</p>
<p>
<s>
Legislativní	legislativní	k2eAgFnSc1d1	legislativní
zkratka	zkratka	k1gFnSc1	zkratka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
často	často	k6eAd1	často
používaných	používaný	k2eAgFnPc2d1	používaná
zkratek	zkratka	k1gFnPc2	zkratka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
v	v	k7c6	v
online	onlinout	k5eAaPmIp3nS	onlinout
diskusích	diskuse	k1gFnPc6	diskuse
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
NGO	NGO	kA	NGO
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
na	na	k7c6	na
lékařských	lékařský	k2eAgInPc6d1	lékařský
předpisech	předpis	k1gInPc6	předpis
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
církevních	církevní	k2eAgInPc2d1	církevní
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
kongregací	kongregace	k1gFnPc2	kongregace
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
států	stát	k1gInPc2	stát
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
okresů	okres	k1gInPc2	okres
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hudebních	hudební	k2eAgFnPc2d1	hudební
zkratek	zkratka	k1gFnPc2	zkratka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
leteckých	letecký	k2eAgFnPc2d1	letecká
zkratek	zkratka	k1gFnPc2	zkratka
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
(	(	kIx(	(
<g/>
seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zkratka	zkratka	k1gFnSc1	zkratka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zkratka	zkratka	k1gFnSc1	zkratka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Zkratky	zkratka	k1gFnSc2	zkratka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Hrbáček	Hrbáček	k1gMnSc1	Hrbáček
<g/>
:	:	kIx,	:
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
zkratky	zkratka	k1gFnPc1	zkratka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
126	[number]	k4	126
s.	s.	k?	s.
</s>
</p>
