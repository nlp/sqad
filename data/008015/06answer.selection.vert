<s>
Kréta	Kréta	k1gFnSc1	Kréta
(	(	kIx(	(
<g/>
též	též	k9	též
Candia	Candium	k1gNnPc1	Candium
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Crete	Cret	k1gMnSc5	Cret
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
řecký	řecký	k2eAgInSc4d1	řecký
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
pátý	pátý	k4xOgInSc1	pátý
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
