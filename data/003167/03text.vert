<s>
Buben	buben	k1gInSc1	buben
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
membranofon	membranofon	k1gInSc1	membranofon
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
např.	např.	kA	např.
blány	blána	k1gFnSc2	blána
či	či	k8xC	či
kůže	kůže	k1gFnSc2	kůže
natažené	natažený	k2eAgFnSc2d1	natažená
na	na	k7c6	na
rezonátoru	rezonátor	k1gInSc6	rezonátor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
idiofonů	idiofon	k1gInPc2	idiofon
<g/>
.	.	kIx.	.
</s>
<s>
Bubny	Bubny	k1gInPc1	Bubny
jsou	být	k5eAaImIp3nP	být
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
z	z	k7c2	z
materiálů	materiál	k1gInPc2	materiál
podléhajících	podléhající	k2eAgFnPc2d1	podléhající
zkáze	zkáza	k1gFnSc3	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
zmínky	zmínka	k1gFnPc1	zmínka
bubnů	buben	k1gInPc2	buben
a	a	k8xC	a
hraní	hraní	k1gNnSc2	hraní
na	na	k7c4	na
bubny	buben	k1gInPc4	buben
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
mezopotámském	mezopotámský	k2eAgNnSc6d1	mezopotámský
vyobrazení	vyobrazení	k1gNnSc6	vyobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
socha	socha	k1gFnSc1	socha
z	z	k7c2	z
období	období	k1gNnSc2	období
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Babylónu	Babylón	k1gInSc6	Babylón
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tvorbu	tvorba	k1gFnSc4	tvorba
hudby	hudba	k1gFnSc2	hudba
byly	být	k5eAaImAgInP	být
bubny	buben	k1gInPc1	buben
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
bicí	bicí	k2eAgInPc1d1	bicí
nástroje	nástroj	k1gInPc1	nástroj
používaný	používaný	k2eAgInSc1d1	používaný
i	i	k9	i
při	při	k7c6	při
náboženských	náboženský	k2eAgInPc6d1	náboženský
rituálech	rituál	k1gInPc6	rituál
nebo	nebo	k8xC	nebo
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
se	se	k3xPyFc4	se
používali	používat	k5eAaImAgMnP	používat
bubeníci	bubeník	k1gMnPc1	bubeník
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xS	jako
tamboři	tambor	k1gMnPc1	tambor
<g/>
)	)	kIx)	)
s	s	k7c7	s
vířivým	vířivý	k2eAgInSc7d1	vířivý
bubnem	buben	k1gInSc7	buben
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
dodržování	dodržování	k1gNnSc2	dodržování
rytmu	rytmus	k1gInSc3	rytmus
pochodu	pochod	k1gInSc2	pochod
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
se	se	k3xPyFc4	se
rozezvučí	rozezvučet	k5eAaPmIp3nS	rozezvučet
pomocí	pomocí	k7c2	pomocí
úderů	úder	k1gInPc2	úder
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
paliček	palička	k1gFnPc2	palička
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
nástrojů	nástroj	k1gInPc2	nástroj
do	do	k7c2	do
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Vířivý	vířivý	k2eAgInSc1d1	vířivý
buben	buben	k1gInSc1	buben
slang	slang	k1gInSc1	slang
<g/>
.	.	kIx.	.
virbl	virbl	k1gInSc1	virbl
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgInSc1d1	vojenský
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
slang	slang	k1gInSc1	slang
<g/>
.	.	kIx.	.
dupák	dupák	k1gInSc1	dupák
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc1d1	malý
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
tympány	tympán	k1gInPc1	tympán
<g/>
,	,	kIx,	,
darbuka	darbuk	k1gMnSc4	darbuk
<g/>
,	,	kIx,	,
tabla	tablo	k1gNnPc1	tablo
<g/>
,	,	kIx,	,
šamanský	šamanský	k2eAgInSc1d1	šamanský
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
djembe	djembat	k5eAaPmIp3nS	djembat
<g/>
.	.	kIx.	.
</s>
<s>
Bicí	bicí	k2eAgInSc1d1	bicí
Drum	Drum	k1gInSc1	Drum
and	and	k?	and
bass	bass	k6eAd1	bass
Sachs-Hornbostelova	Sachs-Hornbostelův	k2eAgFnSc1d1	Sachs-Hornbostelův
klasifikace	klasifikace	k1gFnSc1	klasifikace
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Buben	Bubny	k1gInPc2	Bubny
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Sada	sada	k1gFnSc1	sada
bubnů	buben	k1gInPc2	buben
(	(	kIx(	(
<g/>
drumbenů	drumben	k1gInPc2	drumben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
použít	použít	k5eAaPmF	použít
i	i	k9	i
jako	jako	k9	jako
sedák	sedák	k1gInSc4	sedák
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
graficky	graficky	k6eAd1	graficky
libovolně	libovolně	k6eAd1	libovolně
upravit	upravit	k5eAaPmF	upravit
Stránka	stránka	k1gFnSc1	stránka
s	s	k7c7	s
notami	nota	k1gFnPc7	nota
bicích	bicí	k2eAgFnPc2d1	bicí
dle	dle	k7c2	dle
stylů	styl	k1gInPc2	styl
</s>
