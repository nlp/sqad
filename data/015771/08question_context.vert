<s hack="1">
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
schválila	schválit	k5eAaPmAgFnS
odebrání	odebrání	k1gNnSc4
homosexuality	homosexualita	k1gFnSc2
z	z	k7c2
diagnostického	diagnostický	k2eAgInSc2d1
a	a	k8xC
statistického	statistický	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
(	(	kIx(
<g/>
DSM-II	DSM-II	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
postavila	postavit	k5eAaPmAgFnS
se	se	k3xPyFc4
proti	proti	k7c3
veřejné	veřejný	k2eAgFnSc3d1
i	i	k8xC
soukromé	soukromý	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
homosexuálů	homosexuál	k1gMnPc2
a	a	k8xC
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
zákonů	zákon	k1gInPc2
podporujících	podporující	k2eAgInPc2d1
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1980	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
DSM-III	DSM-III	k1gFnSc6
mezi	mezi	k7c7
poruchy	poruch	k1gInPc7
výslovně	výslovně	k6eAd1
řazena	řadit	k5eAaImNgFnS
egodystonní	egodystonní	k2eAgFnSc1d1
homosexualita	homosexualita	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
DSM-IV	DSM-IV	k1gFnSc2
vypuštěna	vypustit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>