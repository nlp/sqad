<s>
Homosexualita	homosexualita	k1gFnSc1
</s>
<s>
Symboly	symbol	k1gInPc1
pro	pro	k7c4
ženskou	ženská	k1gFnSc4
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
mužskou	mužský	k2eAgFnSc4d1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
homosexualitu	homosexualita	k1gFnSc4
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
homosexuálního	homosexuální	k2eAgNnSc2d1
(	(	kIx(
<g/>
též	též	k9
LGBT	LGBT	kA
<g/>
)	)	kIx)
hnutí	hnutí	k1gNnPc1
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
homós	homós	k6eAd1
stejný	stejný	k2eAgInSc4d1
a	a	k8xC
latinského	latinský	k2eAgInSc2d1
sexus	sexus	k1gInSc4
<g/>
,	,	kIx,
pohlaví	pohlaví	k1gNnSc4
<g/>
)	)	kIx)
v	v	k7c6
nejširším	široký	k2eAgNnSc6d3
pojetí	pojetí	k1gNnSc6
označuje	označovat	k5eAaImIp3nS
princip	princip	k1gInSc1
vazby	vazba	k1gFnSc2
osob	osoba	k1gFnPc2
<g/>
/	/	kIx~
<g/>
tvorů	tvor	k1gMnPc2
<g/>
/	/	kIx~
<g/>
prvků	prvek	k1gInPc2
vykazujících	vykazující	k2eAgFnPc2d1
totožnou	totožný	k2eAgFnSc4d1
charakteristiku	charakteristika	k1gFnSc4
pohlavnosti	pohlavnost	k1gFnSc2
(	(	kIx(
<g/>
stejné	stejný	k2eAgNnSc4d1
pohlaví	pohlaví	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
užším	úzký	k2eAgNnSc6d2
a	a	k8xC
v	v	k7c6
odborném	odborný	k2eAgInSc6d1
diskurzu	diskurz	k1gInSc6
obvyklém	obvyklý	k2eAgInSc6d1
pojetí	pojetí	k1gNnSc4
jde	jít	k5eAaImIp3nS
o	o	k7c4
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
především	především	k6eAd1
nebo	nebo	k8xC
výhradně	výhradně	k6eAd1
na	na	k7c4
osoby	osoba	k1gFnPc4
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
také	také	k9
označení	označení	k1gNnSc4
homosexuální	homosexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
,	,	kIx,
homosexuální	homosexuální	k2eAgFnPc4d1
preference	preference	k1gFnPc4
<g/>
,	,	kIx,
homosexuální	homosexuální	k2eAgNnSc4d1
zaměření	zaměření	k1gNnSc4
<g/>
,	,	kIx,
homosexuální	homosexuální	k2eAgNnSc4d1
založení	založení	k1gNnSc4
a	a	k8xC
další	další	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nositel	nositel	k1gMnSc1
mužské	mužský	k2eAgFnSc2d1
homosexuality	homosexualita	k1gFnSc2
(	(	kIx(
<g/>
náklonnosti	náklonnost	k1gFnSc2
muže	muž	k1gMnSc2
k	k	k7c3
muži	muž	k1gMnSc3
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označován	označován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
gay	gay	k1gMnSc1
<g/>
,	,	kIx,
nositelka	nositelka	k1gFnSc1
ženské	ženský	k2eAgFnSc2d1
homosexuality	homosexualita	k1gFnSc2
(	(	kIx(
<g/>
náklonnosti	náklonnost	k1gFnSc2
ženy	žena	k1gFnSc2
k	k	k7c3
ženě	žena	k1gFnSc3
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k9
lesba	lesba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
se	se	k3xPyFc4
ustálil	ustálit	k5eAaPmAgInS
termín	termín	k1gInSc1
homosexualita	homosexualita	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
označována	označovat	k5eAaImNgFnS
též	též	k9
například	například	k6eAd1
jako	jako	k8xS,k8xC
uranismus	uranismus	k1gInSc1
(	(	kIx(
<g/>
Ulrichs	Ulrichs	k1gInSc1
<g/>
,	,	kIx,
1864	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
psychický	psychický	k2eAgInSc1d1
hermafroditismus	hermafroditismus	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
pojetí	pojetí	k1gNnSc6
Ellise	Ellise	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
inverze	inverze	k1gFnSc1
(	(	kIx(
<g/>
u	u	k7c2
Freuda	Freud	k1gMnSc2
nebo	nebo	k8xC
Chevaliera	chevalier	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
psalo	psát	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
kontrárně	kontrárně	k6eAd1
sexuálním	sexuální	k2eAgNnSc6d1
cítění	cítění	k1gNnSc6
či	či	k8xC
osobách	osoba	k1gFnPc6
(	(	kIx(
<g/>
u	u	k7c2
Krafft-Ebinga	Krafft-Ebing	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slovo	slovo	k1gNnSc4
homoerotika	homoerotik	k1gMnSc2
prosazoval	prosazovat	k5eAaImAgMnS
místo	místo	k7c2
homosexuality	homosexualita	k1gFnSc2
Ferenczi	Ferencze	k1gFnSc4
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
apod.	apod.	kA
</s>
<s>
Zhruba	zhruba	k6eAd1
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
čtvrtiny	čtvrtina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
čtvrtiny	čtvrtina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
homosexualita	homosexualita	k1gFnSc1
uváděna	uvádět	k5eAaImNgFnS
v	v	k7c6
odborných	odborný	k2eAgFnPc6d1
monografiích	monografie	k1gFnPc6
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
systematických	systematický	k2eAgFnPc6d1
klasifikacích	klasifikace	k1gFnPc6
jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
duševních	duševní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
rokem	rok	k1gInSc7
1973	#num#	k4
však	však	k8xC
nejvýznamnější	významný	k2eAgFnPc1d3
americké	americký	k2eAgFnPc1d1
a	a	k8xC
následně	následně	k6eAd1
světové	světový	k2eAgFnPc1d1
<g/>
,	,	kIx,
evropské	evropský	k2eAgFnPc1d1
i	i	k8xC
asijské	asijský	k2eAgFnPc1d1
psychiatrické	psychiatrický	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
na	na	k7c6
základě	základ	k1gInSc6
změny	změna	k1gFnSc2
společenské	společenský	k2eAgFnSc2d1
situace	situace	k1gFnSc2
a	a	k8xC
vědeckých	vědecký	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
tohoto	tento	k3xDgNnSc2
pojetí	pojetí	k1gNnSc2
postupně	postupně	k6eAd1
ustoupily	ustoupit	k5eAaPmAgFnP
a	a	k8xC
homosexualita	homosexualita	k1gFnSc1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
uváděna	uvádět	k5eAaImNgFnS
jako	jako	k9
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
standardních	standardní	k2eAgFnPc2d1
podob	podoba	k1gFnPc2
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
vzniku	vznik	k1gInSc2
pojmu	pojem	k1gInSc2
existovala	existovat	k5eAaImAgFnS
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
různá	různý	k2eAgNnPc1d1
pojetí	pojetí	k1gNnPc1
příčin	příčina	k1gFnPc2
vzniku	vznik	k1gInSc2
homosexuality	homosexualita	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
hypotéz	hypotéza	k1gFnPc2
předpokládajících	předpokládající	k2eAgFnPc2d1
plnou	plný	k2eAgFnSc4d1
genetickou	genetický	k2eAgFnSc4d1
předurčenost	předurčenost	k1gFnSc4
přes	přes	k7c4
pojetí	pojetí	k1gNnSc4
přisuzující	přisuzující	k2eAgInSc1d1
rozhodující	rozhodující	k2eAgInSc1d1
vliv	vliv	k1gInSc1
nitroděložnímu	nitroděložní	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
nebo	nebo	k8xC
rodinným	rodinný	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
a	a	k8xC
výchovným	výchovný	k2eAgInPc3d1
vlivům	vliv	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
jeví	jevit	k5eAaImIp3nS
jako	jako	k9
biologická	biologický	k2eAgFnSc1d1
<g/>
,	,	kIx,
předurčená	předurčený	k2eAgFnSc1d1
komplexní	komplexní	k2eAgFnSc7d1
souhrou	souhra	k1gFnSc7
genetických	genetický	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
a	a	k8xC
raného	raný	k2eAgNnSc2d1
děložního	děložní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
není	být	k5eNaImIp3nS
osobní	osobní	k2eAgFnSc7d1
volbou	volba	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
přesvědčivý	přesvědčivý	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
tvrzení	tvrzení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
rané	raný	k2eAgFnPc1d1
dětské	dětský	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
<g/>
,	,	kIx,
výchova	výchova	k1gFnSc1
<g/>
,	,	kIx,
sexuální	sexuální	k2eAgNnSc4d1
zneužití	zneužití	k1gNnSc4
<g/>
,	,	kIx,
trauma	trauma	k1gNnSc4
nebo	nebo	k8xC
jiné	jiný	k2eAgFnPc4d1
závažné	závažný	k2eAgFnPc4d1
životní	životní	k2eAgFnPc4d1
události	událost	k1gFnPc4
a	a	k8xC
jakékoliv	jakýkoliv	k3yIgNnSc4
sociální	sociální	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
po	po	k7c6
narození	narození	k1gNnSc6
ovlivňují	ovlivňovat	k5eAaImIp3nP
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Údajně	údajně	k6eAd1
takové	takový	k3xDgInPc4
názory	názor	k1gInPc4
pramení	pramenit	k5eAaImIp3nS
z	z	k7c2
nesprávných	správný	k2eNgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
předsudků	předsudek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existovaly	existovat	k5eAaImAgInP
i	i	k9
pokusy	pokus	k1gInPc1
a	a	k8xC
konverzní	konverzní	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
usilující	usilující	k2eAgFnSc2d1
o	o	k7c4
změnu	změna	k1gFnSc4
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgFnPc4d1
studie	studie	k1gFnPc4
dokládají	dokládat	k5eAaImIp3nP
rizikovost	rizikovost	k1gFnSc1
a	a	k8xC
nefunkčnost	nefunkčnost	k1gFnSc1
těchto	tento	k3xDgFnPc2
metod	metoda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slovem	slovem	k6eAd1
homosexualita	homosexualita	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
přeneseném	přenesený	k2eAgInSc6d1
významu	význam	k1gInSc6
označováno	označován	k2eAgNnSc1d1
i	i	k8xC
takzvané	takzvaný	k2eAgNnSc1d1
homosexuální	homosexuální	k2eAgNnSc1d1
chování	chování	k1gNnSc1
ve	v	k7c6
smyslu	smysl	k1gInSc6
projevu	projev	k1gInSc2
vztahů	vztah	k1gInPc2
(	(	kIx(
<g/>
namlouvání	namlouvání	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
soužití	soužití	k1gNnPc1
mezi	mezi	k7c7
osobami	osoba	k1gFnPc7
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
užšího	úzký	k2eAgNnSc2d2
pojetí	pojetí	k1gNnSc2
jako	jako	k8xC,k8xS
pohlavního	pohlavní	k2eAgInSc2d1
styku	styk	k1gInSc2
těchto	tento	k3xDgFnPc2
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
homosexuální	homosexuální	k2eAgFnSc1d1
identita	identita	k1gFnSc1
jako	jako	k8xS,k8xC
označení	označení	k1gNnSc1
nebo	nebo	k8xC
sebepojetí	sebepojetí	k1gNnSc1
nějakého	nějaký	k3yIgMnSc2
člověka	člověk	k1gMnSc2
za	za	k7c2
homosexuálního	homosexuální	k2eAgNnSc2d1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
ve	v	k7c6
smyslu	smysl	k1gInSc6
nositele	nositel	k1gMnSc4
homosexuální	homosexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obecně	obecně	k6eAd1
k	k	k7c3
pojmu	pojem	k1gInSc3
</s>
<s>
Projevy	projev	k1gInPc1
a	a	k8xC
typy	typ	k1gInPc1
homosexuality	homosexualita	k1gFnSc2
</s>
<s>
Mužský	mužský	k2eAgMnSc1d1
pár	pár	k4xCyI
</s>
<s>
Homosexuální	homosexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
celoživotně	celoživotně	k6eAd1
v	v	k7c6
citové	citový	k2eAgFnSc6d1
a	a	k8xC
často	často	k6eAd1
i	i	k9
vztahové	vztahový	k2eAgFnSc3d1
rovině	rovina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
spojeno	spojit	k5eAaPmNgNnS
se	s	k7c7
schopností	schopnost	k1gFnSc7
vytvářet	vytvářet	k5eAaImF
plnohodnotný	plnohodnotný	k2eAgInSc4d1
citový	citový	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
osobou	osoba	k1gFnSc7
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgInPc6
základních	základní	k2eAgInPc6d1
psychologických	psychologický	k2eAgInPc6d1
aspektech	aspekt	k1gInPc6
ekvivalentní	ekvivalentní	k2eAgInSc1d1
heterosexuálnímu	heterosexuální	k2eAgInSc3d1
vztahu	vztah	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
některých	některý	k3yIgFnPc2
koncepcí	koncepce	k1gFnPc2
je	být	k5eAaImIp3nS
homosexuální	homosexuální	k2eAgFnSc1d1
reaktivita	reaktivita	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
různé	různý	k2eAgFnSc6d1
menší	malý	k2eAgFnSc3d2
či	či	k8xC
větší	veliký	k2eAgFnSc3d2
míře	míra	k1gFnSc3
vlastní	vlastní	k2eAgFnSc6d1
většině	většina	k1gFnSc6
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
u	u	k7c2
většiny	většina	k1gFnSc2
jako	jako	k8xC,k8xS
nedominantní	dominantní	k2eNgFnSc1d1
vlastnost	vlastnost	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
projevovat	projevovat	k5eAaImF
například	například	k6eAd1
citovou	citový	k2eAgFnSc7d1
hloubkou	hloubka	k1gFnSc7
v	v	k7c6
běžných	běžný	k2eAgInPc6d1
a	a	k8xC
akceptovaných	akceptovaný	k2eAgInPc6d1
sociálních	sociální	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
(	(	kIx(
<g/>
přátelství	přátelství	k1gNnSc4
<g/>
,	,	kIx,
idea	idea	k1gFnSc1
mužských	mužský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
citovou	citový	k2eAgFnSc7d1
náklonností	náklonnost	k1gFnSc7
<g/>
,	,	kIx,
intenzivnějším	intenzivní	k2eAgNnSc7d2
prožíváním	prožívání	k1gNnSc7
nebo	nebo	k8xC
potřebou	potřeba	k1gFnSc7
citově	citově	k6eAd1
významných	významný	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
k	k	k7c3
osobám	osoba	k1gFnPc3
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
schopností	schopnost	k1gFnSc7
se	se	k3xPyFc4
do	do	k7c2
nich	on	k3xPp3gMnPc2
zamilovat	zamilovat	k5eAaPmF
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
i	i	k8xC
přiznanou	přiznaný	k2eAgFnSc7d1
nebo	nebo	k8xC
nepřiznanou	přiznaný	k2eNgFnSc7d1
touhou	touha	k1gFnSc7
po	po	k7c6
fyzické	fyzický	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
orgasmických	orgasmický	k2eAgFnPc6d1
aktivitách	aktivita	k1gFnPc6
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
nim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
falometrickém	falometrický	k2eAgNnSc6d1
(	(	kIx(
<g/>
PPG	PPG	kA
<g/>
)	)	kIx)
testování	testování	k1gNnSc1
sexuality	sexualita	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
předpokládána	předpokládat	k5eAaImNgFnS
specificky	specificky	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
genitální	genitální	k2eAgFnSc1d1
reaktivita	reaktivita	k1gFnSc1
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
podnětům	podnět	k1gInPc3
odpovídajícím	odpovídající	k2eAgInPc3d1
sexuální	sexuální	k2eAgFnSc2d1
orientaci	orientace	k1gFnSc4
probanda	probando	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
ačkoliv	ačkoliv	k8xS
falografické	falografický	k2eAgFnPc1d1
diagnostické	diagnostický	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
nejsou	být	k5eNaImIp3nP
zcela	zcela	k6eAd1
spolehlivé	spolehlivý	k2eAgInPc1d1
a	a	k8xC
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
závislé	závislý	k2eAgInPc1d1
na	na	k7c4
interpretaci	interpretace	k1gFnSc4
měření	měření	k1gNnSc2
<g/>
,	,	kIx,
výzkumná	výzkumný	k2eAgFnSc1d1
a	a	k8xC
diagnostická	diagnostický	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
korelaci	korelace	k1gFnSc3
mezi	mezi	k7c7
sexuální	sexuální	k2eAgFnSc7d1
orientací	orientace	k1gFnSc7
a	a	k8xC
naměřenou	naměřený	k2eAgFnSc7d1
specifickou	specifický	k2eAgFnSc7d1
reaktivitou	reaktivita	k1gFnSc7
potvrzuje	potvrzovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
obvyklé	obvyklý	k2eAgFnSc2d1
definice	definice	k1gFnSc2
homosexuality	homosexualita	k1gFnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
homosexuální	homosexuální	k2eAgFnSc1d1
reaktivita	reaktivita	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
nedominantní	dominantní	k2eNgFnSc7d1
vlastností	vlastnost	k1gFnSc7
vedle	vedle	k7c2
silnější	silný	k2eAgFnSc2d2
heterosexuální	heterosexuální	k2eAgFnSc2d1
reaktivity	reaktivita	k1gFnSc2
<g/>
,	,	kIx,
pod	pod	k7c4
pojem	pojem	k1gInSc4
homosexuality	homosexualita	k1gFnSc2
nespadá	spadat	k5eNaPmIp3nS,k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Nejběžnějšími	běžný	k2eAgInPc7d3
typy	typ	k1gInPc7
homosexuality	homosexualita	k1gFnSc2
jsou	být	k5eAaImIp3nP
mužská	mužský	k2eAgFnSc1d1
androfilie	androfilie	k1gFnSc1
a	a	k8xC
ženská	ženský	k2eAgFnSc1d1
gynekofilie	gynekofilie	k1gFnSc1
(	(	kIx(
<g/>
gynofilie	gynofilie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
teoretickém	teoretický	k2eAgInSc6d1
modelu	model	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
rozlišuje	rozlišovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc1
sexuální	sexuální	k2eAgFnPc1d1
orientace	orientace	k1gFnPc1
<g/>
,	,	kIx,
bývají	bývat	k5eAaImIp3nP
pod	pod	k7c4
pojem	pojem	k1gInSc4
homosexuality	homosexualita	k1gFnSc2
zařazována	zařazován	k2eAgNnPc1d1
i	i	k8xC
pedofilní	pedofilní	k2eAgNnPc1d1
erotická	erotický	k2eAgNnPc1d1
zaměření	zaměření	k1gNnPc1
na	na	k7c4
osoby	osoba	k1gFnPc4
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Homosexuální	homosexuální	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
stejnopohlavní	stejnopohlavní	k2eAgInSc1d1
sexuální	sexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Slovo	slovo	k1gNnSc1
homosexualita	homosexualita	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
používáno	používat	k5eAaImNgNnS
ve	v	k7c6
významu	význam	k1gInSc6
<g/>
,	,	kIx,
pro	pro	k7c4
tzv.	tzv.	kA
homosexuální	homosexuální	k2eAgNnSc1d1
chování	chování	k1gNnSc1
–	–	k?
například	například	k6eAd1
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
řeč	řeč	k1gFnSc4
o	o	k7c4
„	„	k?
<g/>
trestnosti	trestnost	k1gFnPc4
homosexuality	homosexualita	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
homosexuální	homosexuální	k2eAgNnSc4d1
chování	chování	k1gNnSc4
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
označuje	označovat	k5eAaImIp3nS
orgasmická	orgasmický	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
mezi	mezi	k7c7
příslušníky	příslušník	k1gMnPc7
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zpravidla	zpravidla	k6eAd1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
projevem	projev	k1gInSc7
homosexuality	homosexualita	k1gFnSc2
jako	jako	k8xC,k8xS
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flagrantním	flagrantní	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
zástupná	zástupný	k2eAgFnSc1d1
homosexualita	homosexualita	k1gFnSc1
<g/>
,	,	kIx,
vyskytující	vyskytující	k2eAgFnSc1d1
se	se	k3xPyFc4
příležitostně	příležitostně	k6eAd1
ve	v	k7c6
věznicích	věznice	k1gFnPc6
či	či	k8xC
jiných	jiný	k2eAgFnPc6d1
stejnopohlavních	stejnopohlavní	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
navážou	navázat	k5eAaPmIp3nP
takovýto	takovýto	k3xDgInSc4
„	„	k?
<g/>
zástupný	zástupný	k2eAgInSc1d1
<g/>
“	“	k?
homoerotický	homoerotický	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
při	při	k7c6
první	první	k4xOgFnSc6
příležitosti	příležitost	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
při	při	k7c6
propuštění	propuštění	k1gNnSc6
na	na	k7c4
svobodu	svoboda	k1gFnSc4
<g/>
)	)	kIx)
vracejí	vracet	k5eAaImIp3nP
zpět	zpět	k6eAd1
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
biologicky	biologicky	k6eAd1
determinované	determinovaný	k2eAgFnSc3d1
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Dalším	další	k2eAgInSc7d1
motivem	motiv	k1gInSc7
pro	pro	k7c4
takové	takový	k3xDgNnSc4
jednání	jednání	k1gNnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
finanční	finanční	k2eAgInSc4d1
prospěch	prospěch	k1gInSc4
(	(	kIx(
<g/>
např.	např.	kA
účinkování	účinkování	k1gNnSc4
v	v	k7c6
pornografickém	pornografický	k2eAgInSc6d1
filmu	film	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Homosexuální	homosexuální	k2eAgFnSc1d1
identita	identita	k1gFnSc1
</s>
<s>
Líbající	líbající	k2eAgFnPc1d1
se	se	k3xPyFc4
dívky	dívka	k1gFnPc1
</s>
<s>
Pojem	pojem	k1gInSc1
homosexualita	homosexualita	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
vnímán	vnímat	k5eAaImNgInS
též	též	k9
jako	jako	k8xC,k8xS
homosexuální	homosexuální	k2eAgFnSc1d1
identita	identita	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
dotčená	dotčený	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
se	se	k3xPyFc4
k	k	k7c3
tomuto	tento	k3xDgInSc3
pojmu	pojem	k1gInSc3
(	(	kIx(
<g/>
konstruktu	konstrukt	k1gInSc2
<g/>
)	)	kIx)
buď	buď	k8xC
sama	sám	k3xTgMnSc4
přihlásí	přihlásit	k5eAaPmIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	on	k3xPp3gMnPc4
jím	jíst	k5eAaImIp1nS
ocejchována	ocejchován	k2eAgFnSc1d1
jakožto	jakožto	k8xS
odlišná	odlišný	k2eAgFnSc1d1
od	od	k7c2
většiny	většina	k1gFnSc2
či	či	k8xC
od	od	k7c2
normy	norma	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
jí	on	k3xPp3gFnSc3
tím	ten	k3xDgNnSc7
přiřazeny	přiřazen	k2eAgInPc4d1
prezentační	prezentační	k2eAgInPc4d1
stereotypy	stereotyp	k1gInPc4
(	(	kIx(
<g/>
konotace	konotace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
pojem	pojem	k1gInSc4
navázány	navázán	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
homosexualita	homosexualita	k1gFnSc1
není	být	k5eNaImIp3nS
vrozená	vrozený	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
historicky	historicky	k6eAd1
a	a	k8xC
kulturně	kulturně	k6eAd1
podmíněná	podmíněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jiných	jiný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
jsou	být	k5eAaImIp3nP
tytéž	týž	k3xTgFnPc1
psychické	psychický	k2eAgFnPc1d1
dispozice	dispozice	k1gFnPc1
interpretovány	interpretován	k2eAgFnPc1d1
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
pojmech	pojem	k1gInPc6
a	a	k8xC
kontextech	kontext	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
jiným	jiný	k2eAgNnSc7d1
chováním	chování	k1gNnSc7
a	a	k8xC
jinými	jiný	k2eAgFnPc7d1
společenskými	společenský	k2eAgFnPc7d1
interakcemi	interakce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
užívání	užívání	k1gNnSc1
označení	označení	k1gNnSc2
pro	pro	k7c4
homosexualitu	homosexualita	k1gFnSc4
</s>
<s>
První	první	k4xOgNnSc1
použití	použití	k1gNnSc1
slova	slovo	k1gNnPc1
homosexuál	homosexuál	k1gMnSc1
(	(	kIx(
<g/>
resp.	resp.	kA
homosexualisté	homosexualista	k1gMnPc1
<g/>
,	,	kIx,
tedy	tedy	k9
„	„	k?
<g/>
die	die	k?
Homosexualisten	Homosexualisten	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
doloženo	doložit	k5eAaPmNgNnS
v	v	k7c6
dopise	dopis	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
psal	psát	k5eAaImAgInS
rakousko-uherský	rakousko-uherský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
žurnalista	žurnalista	k1gMnSc1
a	a	k8xC
aktivista	aktivista	k1gMnSc1
maďarské	maďarský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
Karl	Karl	k1gMnSc1
Maria	Mario	k1gMnSc2
Kertbeny	Kertben	k2eAgFnPc1d1
(	(	kIx(
<g/>
psán	psán	k2eAgInSc1d1
též	též	k9
Károly	Károla	k1gFnPc1
Mária	Márium	k1gNnSc2
Kertbeny	Kertben	k2eAgFnPc1d1
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
1847	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Karl	Karla	k1gFnPc2
Maria	Maria	k1gFnSc1
Benkert	Benkert	k1gInSc1
<g/>
)	)	kIx)
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1868	#num#	k4
frískému	fríský	k2eAgMnSc3d1
publicistovi	publicista	k1gMnSc3
<g/>
,	,	kIx,
absolventu	absolvent	k1gMnSc3
studií	studio	k1gNnPc2
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
práva	právo	k1gNnSc2
a	a	k8xC
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
Karlu	Karel	k1gMnSc3
Heinrichu	Heinrich	k1gMnSc3
Ulrichsovi	Ulrichs	k1gMnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
stíhán	stíhat	k5eAaImNgMnS
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgInSc7
„	„	k?
<g/>
protipřirozeným	protipřirozený	k2eAgInPc3d1
<g/>
“	“	k?
kontaktům	kontakt	k1gInPc3
a	a	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
pojmenoval	pojmenovat	k5eAaPmAgMnS
homosexualitu	homosexualita	k1gFnSc4
jako	jako	k8xC,k8xS
uranismus	uranismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejně	veřejně	k6eAd1
použil	použít	k5eAaPmAgInS
termín	termín	k1gInSc1
poprvé	poprvé	k6eAd1
Kertbeny	Kertben	k2eAgInPc4d1
v	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
ve	v	k7c6
dvojici	dvojice	k1gFnSc6
v	v	k7c6
Lipsku	Lipsko	k1gNnSc6
anonymně	anonymně	k6eAd1
zveřejněných	zveřejněný	k2eAgInPc2d1
traktátů	traktát	k1gInPc2
proti	proti	k7c3
soudobé	soudobý	k2eAgFnSc3d1
pruské	pruský	k2eAgFnSc3d1
legislativě	legislativa	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Kertbeny	Kertben	k2eAgInPc1d1
však	však	k9
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
textech	text	k1gInPc6
střídal	střídat	k5eAaImAgMnS
různé	různý	k2eAgInPc4d1
tvary	tvar	k1gInPc4
<g/>
,	,	kIx,
zejména	zejména	k9
adjektivní	adjektivní	k2eAgInSc4d1
tvar	tvar	k1gInSc4
„	„	k?
<g/>
homosexual	homosexual	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
,	,	kIx,
výraz	výraz	k1gInSc1
homosexualismus	homosexualismus	k1gInSc1
bez	bez	k7c2
významového	významový	k2eAgNnSc2d1
rozlišení	rozlišení	k1gNnSc2
střídal	střídat	k5eAaImAgMnS
s	s	k7c7
termínem	termín	k1gInSc7
Homosexualität	Homosexualität	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Výraz	výraz	k1gInSc1
homosexualita	homosexualita	k1gFnSc1
převzali	převzít	k5eAaPmAgMnP
Gustav	Gustav	k1gMnSc1
Jäger	Jäger	k1gMnSc1
(	(	kIx(
<g/>
Die	Die	k1gMnSc1
Entdeckung	Entdeckung	k1gMnSc1
der	drát	k5eAaImRp2nS
Seele	Seele	k1gFnSc2
<g/>
,	,	kIx,
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
von	von	k1gInSc1
Krafft-Ebing	Krafft-Ebing	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
Psychopathia	Psychopathium	k1gNnSc2
sexualis	sexualis	k1gFnSc2
<g/>
,	,	kIx,
1887	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Magnus	Magnus	k1gMnSc1
Hirschfeld	Hirschfeld	k1gMnSc1
(	(	kIx(
<g/>
např.	např.	kA
Jahrbuch	Jahrbuch	k1gInSc1
für	für	k?
sexuelle	sexuelle	k1gInSc1
Zwischenstufen	Zwischenstufna	k1gFnPc2
<g/>
,	,	kIx,
1900	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlivem	vliv	k1gInSc7
nejednoznačnosti	nejednoznačnost	k1gFnSc2
pojmu	pojmout	k5eAaPmIp1nS
sexualita	sexualita	k1gFnSc1
slovo	slovo	k1gNnSc1
homosexuální	homosexuální	k2eAgMnSc1d1
vyvolává	vyvolávat	k5eAaImIp3nS
představu	představa	k1gFnSc4
genitálních	genitální	k2eAgFnPc2d1
a	a	k8xC
orgasmických	orgasmický	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
ho	on	k3xPp3gInSc4
někteří	některý	k3yIgMnPc1
zdráhají	zdráhat	k5eAaImIp3nP
užít	užít	k5eAaPmF
v	v	k7c6
některých	některý	k3yIgInPc6
významech	význam	k1gInPc6
<g/>
,	,	kIx,
mimoto	mimoto	k6eAd1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
pociťováno	pociťovat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
příliš	příliš	k6eAd1
lékařské	lékařský	k2eAgFnPc1d1
a	a	k8xC
odlidšťující	odlidšťující	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různí	různý	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
používají	používat	k5eAaImIp3nP
též	též	k9
pojmy	pojem	k1gInPc1
jako	jako	k8xC,k8xS
homofilie	homofilie	k1gFnSc1
<g/>
,	,	kIx,
homosocialita	homosocialita	k1gFnSc1
<g/>
,	,	kIx,
homoerotika	homoerotika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gMnSc1
používal	používat	k5eAaImAgMnS
kromě	kromě	k7c2
termínu	termín	k1gInSc2
homosexualita	homosexualita	k1gFnSc1
též	též	k9
pojem	pojem	k1gInSc1
inverse	inverse	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
homosexuály	homosexuál	k1gMnPc7
nazýval	nazývat	k5eAaImAgInS
invertovanými	invertovaný	k2eAgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pojmu	pojem	k1gInSc6
inverze	inverze	k1gFnSc1
homosexualita	homosexualita	k1gFnSc1
částečně	částečně	k6eAd1
splývala	splývat	k5eAaImAgFnS
s	s	k7c7
transsexualitou	transsexualita	k1gFnSc7
či	či	k8xC
hermafroditismem	hermafroditismus	k1gInSc7
<g/>
,	,	kIx,
vedle	vedle	k7c2
toho	ten	k3xDgNnSc2
Freud	Freud	k1gInSc1
používal	používat	k5eAaImAgInS
termín	termín	k1gInSc4
perverze	perverze	k1gFnSc2
pro	pro	k7c4
označení	označení	k1gNnSc4
jiných	jiný	k2eAgFnPc2d1
modifikací	modifikace	k1gFnPc2
sexuality	sexualita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
angličtině	angličtina	k1gFnSc6
se	se	k3xPyFc4
vžilo	vžít	k5eAaPmAgNnS
slovo	slovo	k1gNnSc1
gay	gay	k1gMnSc1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ɡ	ɡ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
původní	původní	k2eAgInSc1d1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
veselý	veselý	k2eAgMnSc1d1
<g/>
,	,	kIx,
bezstarostný	bezstarostný	k2eAgMnSc1d1
<g/>
,	,	kIx,
jásavý	jásavý	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slovo	slovo	k1gNnSc1
gay	gay	k1gMnSc1
obvykle	obvykle	k6eAd1
označuje	označovat	k5eAaImIp3nS
jen	jen	k9
homosexuální	homosexuální	k2eAgMnPc4d1
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
slovo	slovo	k1gNnSc1
homosexuál	homosexuál	k1gMnSc1
<g/>
/	/	kIx~
<g/>
homosexuální	homosexuální	k2eAgInSc1d1
sice	sice	k8xC
může	moct	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
obojí	oboj	k1gFnSc7
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
častěji	často	k6eAd2
se	se	k3xPyFc4
však	však	k9
rovněž	rovněž	k9
užívá	užívat	k5eAaImIp3nS
jen	jen	k9
pro	pro	k7c4
muže	muž	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
označení	označení	k1gNnSc4
žen	žena	k1gFnPc2
homosexuální	homosexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
se	se	k3xPyFc4
běžně	běžně	k6eAd1
užívá	užívat	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
lesba	lesba	k1gFnSc1
či	či	k8xC
zdrobněle	zdrobněle	k6eAd1
lesbička	lesbička	k1gFnSc1
a	a	k8xC
přídavné	přídavný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
lesbický	lesbický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
souvisí	souviset	k5eAaImIp3nS
se	s	k7c7
starověkou	starověký	k2eAgFnSc7d1
řeckou	řecký	k2eAgFnSc7d1
básnířkou	básnířka	k1gFnSc7
Sapfó	Sapfó	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
žila	žít	k5eAaImAgFnS
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
ostrově	ostrov	k1gInSc6
Lesbos	Lesbos	k1gInSc1
<g/>
;	;	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
jejích	její	k3xOp3gFnPc6
básních	báseň	k1gFnPc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
opěvování	opěvování	k1gNnSc1
ženského	ženský	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
a	a	k8xC
přátelství	přátelství	k1gNnSc2
mezi	mezi	k7c7
ženami	žena	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sapfó	Sapfó	k1gFnSc1
byla	být	k5eAaImAgFnS
učitelkou	učitelka	k1gFnSc7
vychovávající	vychovávající	k2eAgFnSc1d1
především	především	k6eAd1
šlechtické	šlechtický	k2eAgFnPc4d1
dívky	dívka	k1gFnPc4
<g/>
,	,	kIx,
zda	zda	k8xS
byla	být	k5eAaImAgFnS
sama	sám	k3xTgMnSc4
homosexuálního	homosexuální	k2eAgMnSc4d1
zaměření	zaměření	k1gNnSc3
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
jisté	jistý	k2eAgNnSc1d1
(	(	kIx(
<g/>
ukončila	ukončit	k5eAaPmAgFnS
život	život	k1gInSc4
z	z	k7c2
nešťastné	šťastný	k2eNgFnSc2d1
lásky	láska	k1gFnSc2
k	k	k7c3
muži	muž	k1gMnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
předmoderních	předmoderní	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
středověku	středověk	k1gInSc6
a	a	k8xC
raném	raný	k2eAgInSc6d1
novověku	novověk	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
pro	pro	k7c4
označení	označení	k1gNnSc4
homosexuálně	homosexuálně	k6eAd1
se	se	k3xPyFc4
projevujících	projevující	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
užívalo	užívat	k5eAaImAgNnS
slovo	slovo	k1gNnSc1
sodomita	sodomit	k1gMnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
mezi	mezi	k7c7
muži	muž	k1gMnPc7
sodomie	sodomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
však	však	k9
často	často	k6eAd1
označoval	označovat	k5eAaImAgInS
i	i	k9
jiné	jiný	k2eAgFnPc4d1
zakázané	zakázaný	k2eAgFnPc4d1
pohlavní	pohlavní	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
soulož	soulož	k1gFnSc1
se	s	k7c7
zvířetem	zvíře	k1gNnSc7
nebo	nebo	k8xC
nedovolené	dovolený	k2eNgFnPc1d1
formy	forma	k1gFnPc1
heterosexuálního	heterosexuální	k2eAgInSc2d1
pohlavního	pohlavní	k2eAgInSc2d1
styku	styk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čeština	čeština	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
několik	několik	k4yIc4
vulgarismů	vulgarismus	k1gInPc2
označujících	označující	k2eAgInPc2d1
homosexuály	homosexuál	k1gMnPc7
<g/>
,	,	kIx,
mj.	mj.	kA
teplouš	teplouš	k1gMnSc1
<g/>
,	,	kIx,
buzerant	buzerant	k1gMnSc1
<g/>
,	,	kIx,
bukvice	bukvice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
těchto	tento	k3xDgInPc2
termínů	termín	k1gInPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použit	k2eAgFnPc1d1
(	(	kIx(
<g/>
například	například	k6eAd1
uvnitř	uvnitř	k7c2
homosexuální	homosexuální	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
)	)	kIx)
i	i	k9
bez	bez	k7c2
pejorativního	pejorativní	k2eAgNnSc2d1
vyznění	vyznění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
slovo	slovo	k1gNnSc1
buzerant	buzerant	k1gMnSc1
se	se	k3xPyFc4
do	do	k7c2
češtiny	čeština	k1gFnSc2
dostalo	dostat	k5eAaPmAgNnS
přes	přes	k7c4
němčinu	němčina	k1gFnSc4
ze	z	k7c2
severoitalského	severoitalský	k2eAgNnSc2d1
buzarada	buzarada	k1gFnSc1
(	(	kIx(
<g/>
sodomita	sodomita	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
pozdnělatinského	pozdnělatinský	k2eAgInSc2d1
bugeru	buger	k1gInSc2
(	(	kIx(
<g/>
pův	pův	k?
<g/>
.	.	kIx.
bulgaru	bulgar	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
označovalo	označovat	k5eAaImAgNnS
příslušníka	příslušník	k1gMnSc4
hnutí	hnutí	k1gNnPc1
bogomilů	bogomil	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
(	(	kIx(
<g/>
na	na	k7c6
Západě	západ	k1gInSc6
z	z	k7c2
něj	on	k3xPp3gNnSc2
vycházeli	vycházet	k5eAaImAgMnP
albigenští	albigenští	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenesení	přenesení	k1gNnSc1
významu	význam	k1gInSc2
vycházelo	vycházet	k5eAaImAgNnS
z	z	k7c2
dobových	dobový	k2eAgFnPc2d1
představ	představa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
kacířství	kacířství	k1gNnSc4
spojovaly	spojovat	k5eAaImAgInP
s	s	k7c7
různými	různý	k2eAgFnPc7d1
sexuálními	sexuální	k2eAgFnPc7d1
nevázanostmi	nevázanost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloveso	sloveso	k1gNnSc1
buzerovat	buzerovat	k5eAaImF
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
posunut	posunout	k5eAaPmNgInS
opět	opět	k6eAd1
do	do	k7c2
nesexuálního	sexuální	k2eNgInSc2d1
významu	význam	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
novější	nový	k2eAgNnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
němčině	němčina	k1gFnSc6
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
kromě	kromě	k7c2
slangového	slangový	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
der	drát	k5eAaImRp2nS
Gay	gay	k1gMnSc1
<g/>
“	“	k?
často	často	k6eAd1
neutrální	neutrální	k2eAgNnSc1d1
označení	označení	k1gNnSc1
„	„	k?
<g/>
der	drát	k5eAaImRp2nS
Schwule	Schwule	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
vzniklé	vzniklý	k2eAgInPc1d1
z	z	k7c2
hanlivého	hanlivý	k2eAgNnSc2d1
„	„	k?
<g/>
die	die	k?
Schwuchtel	Schwuchtel	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regionálně	regionálně	k6eAd1
užívané	užívaný	k2eAgNnSc1d1
sloveso	sloveso	k1gNnSc1
„	„	k?
<g/>
schwuchteln	schwuchteln	k1gMnSc1
<g/>
“	“	k?
znamenalo	znamenat	k5eAaImAgNnS
původně	původně	k6eAd1
„	„	k?
<g/>
poskakovat	poskakovat	k5eAaImF
<g/>
,	,	kIx,
tančit	tančit	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spíše	spíše	k9
hanlivým	hanlivý	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
„	„	k?
<g/>
die	die	k?
Tunte	Tunt	k1gInSc5
<g/>
“	“	k?
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
zženštilí	zženštilý	k2eAgMnPc1d1
homosexuálové	homosexuál	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Biologie	biologie	k1gFnSc1
a	a	k8xC
psychologie	psychologie	k1gFnSc1
homosexuality	homosexualita	k1gFnSc2
</s>
<s>
Původ	původ	k1gInSc1
lidské	lidský	k2eAgFnSc2d1
homosexuality	homosexualita	k1gFnSc2
</s>
<s>
Na	na	k7c4
původ	původ	k1gInSc4
(	(	kIx(
<g/>
podstatu	podstata	k1gFnSc4
<g/>
)	)	kIx)
homosexuality	homosexualita	k1gFnSc2
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
rozdílných	rozdílný	k2eAgInPc2d1
pohledů	pohled	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
hormonální	hormonální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
je	být	k5eAaImIp3nS
budoucí	budoucí	k2eAgFnSc1d1
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
jedince	jedinko	k6eAd1
dána	dát	k5eAaPmNgFnS
působením	působení	k1gNnSc7
různě	různě	k6eAd1
vysoké	vysoký	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
pohlavních	pohlavní	k2eAgInPc2d1
hormonů	hormon	k1gInPc2
na	na	k7c4
zárodek	zárodek	k1gInSc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
hormony	hormon	k1gInPc1
určují	určovat	k5eAaImIp3nP
pohlaví	pohlaví	k1gNnSc4
vznikajícího	vznikající	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kterým	který	k3yRgInPc3,k3yQgInPc3,k3yIgInPc3
hormonům	hormon	k1gInPc3
účinek	účinek	k1gInSc1
připisují	připisovat	k5eAaImIp3nP
a	a	k8xC
zda	zda	k8xS
za	za	k7c4
příčinu	příčina	k1gFnSc4
hormonálního	hormonální	k2eAgInSc2d1
stavu	stav	k1gInSc2
považují	považovat	k5eAaImIp3nP
genetickou	genetický	k2eAgFnSc4d1
dispozici	dispozice	k1gFnSc4
plodu	plod	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
některé	některý	k3yIgInPc1
vnější	vnější	k2eAgInPc1d1
faktory	faktor	k1gInPc1
<g/>
,	,	kIx,
například	například	k6eAd1
hormonální	hormonální	k2eAgInSc4d1
a	a	k8xC
psychický	psychický	k2eAgInSc4d1
stav	stav	k1gInSc4
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgMnSc1d1
endokrinolog	endokrinolog	k1gMnSc1
Günter	Günter	k1gMnSc1
Dörner	Dörner	k1gMnSc1
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
výzkumech	výzkum	k1gInPc6
ze	z	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
podání	podání	k1gNnSc6
samčích	samčí	k2eAgInPc2d1
hormonů	hormon	k1gInPc2
kryse	krysa	k1gFnSc3
v	v	k7c6
určitém	určitý	k2eAgNnSc6d1
období	období	k1gNnSc6
březosti	březost	k1gFnSc2
se	se	k3xPyFc4
část	část	k1gFnSc1
narozených	narozený	k2eAgFnPc2d1
samiček	samička	k1gFnPc2
chovala	chovat	k5eAaImAgFnS
homosexuálně	homosexuálně	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
podání	podání	k1gNnSc6
samičích	samičí	k2eAgInPc2d1
hormonů	hormon	k1gInPc2
ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
období	období	k1gNnSc6
se	se	k3xPyFc4
homosexuálně	homosexuálně	k6eAd1
chovala	chovat	k5eAaImAgFnS
část	část	k1gFnSc1
narozených	narozený	k2eAgMnPc2d1
samečků	sameček	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dörnerovi	Dörner	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
objevil	objevit	k5eAaPmAgMnS
příčiny	příčina	k1gFnPc4
vzniku	vznik	k1gInSc2
lidské	lidský	k2eAgFnSc2d1
homosexuality	homosexualita	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
namítalo	namítat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
nelze	lze	k6eNd1
mechanicky	mechanicky	k6eAd1
přenášet	přenášet	k5eAaImF
zjištění	zjištění	k1gNnSc3
o	o	k7c6
zvířatech	zvíře	k1gNnPc6
na	na	k7c4
lidi	člověk	k1gMnPc4
a	a	k8xC
také	také	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInPc1
výzkumy	výzkum	k1gInPc1
nevypovídají	vypovídat	k5eNaPmIp3nP
nic	nic	k3yNnSc4
o	o	k7c4
prožívání	prožívání	k1gNnSc4
sexuality	sexualita	k1gFnSc2
a	a	k8xC
že	že	k8xS
nevylučují	vylučovat	k5eNaImIp3nP
genetický	genetický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
hormonální	hormonální	k2eAgInPc4d1
pochody	pochod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc4
hormonů	hormon	k1gInPc2
také	také	k6eAd1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
pozadí	pozadí	k1gNnSc6
rozšířené	rozšířený	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
(	(	kIx(
<g/>
hypotéza	hypotéza	k1gFnSc1
mateřské	mateřský	k2eAgFnSc2d1
imunosenzitivity	imunosenzitivita	k1gFnSc2
<g/>
,	,	kIx,
autorem	autor	k1gMnSc7
Ray	Ray	k1gMnSc2
Blanchard	Blanchard	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
s	s	k7c7
každým	každý	k3xTgNnSc7
dalším	další	k2eAgNnSc7d1
dítětem	dítě	k1gNnSc7
mužského	mužský	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
vzrůstá	vzrůstat	k5eAaImIp3nS
pravděpodobnost	pravděpodobnost	k1gFnSc1
porození	porození	k1gNnSc2
homosexuálního	homosexuální	k2eAgMnSc4d1
potomka	potomek	k1gMnSc4
a	a	k8xC
statisticky	statisticky	k6eAd1
nejpravděpodobnější	pravděpodobný	k2eAgNnSc1d3
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
homosexuální	homosexuální	k2eAgMnSc1d1
bude	být	k5eAaImBp3nS
nejmladší	mladý	k2eAgMnSc1d3
z	z	k7c2
bratrů	bratr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
rozvojem	rozvoj	k1gInSc7
genetiky	genetika	k1gFnSc2
po	po	k7c6
objevu	objev	k1gInSc6
DNA	DNA	kA
se	se	k3xPyFc4
vynořila	vynořit	k5eAaPmAgFnS
i	i	k9
genetická	genetický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
teorii	teorie	k1gFnSc3
nasvědčuje	nasvědčovat	k5eAaImIp3nS
výzkum	výzkum	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
amerického	americký	k2eAgMnSc4d1
molekulárního	molekulární	k2eAgMnSc4d1
genetika	genetik	k1gMnSc4
Deana	Dean	k1gMnSc4
Hamera	Hamer	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
popsal	popsat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
oblast	oblast	k1gFnSc1
q	q	k?
<g/>
28	#num#	k4
na	na	k7c6
chromozomu	chromozom	k1gInSc6
X	X	kA
s	s	k7c7
homosexualitou	homosexualita	k1gFnSc7
silně	silně	k6eAd1
souvisí	souviset	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistickou	statistický	k2eAgFnSc7d1
souvislostí	souvislost	k1gFnSc7
výskytu	výskyt	k1gInSc2
homosexuality	homosexualita	k1gFnSc2
s	s	k7c7
genetickou	genetický	k2eAgFnSc7d1
příbuzností	příbuznost	k1gFnSc7
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
výzkum	výzkum	k1gInSc1
amerického	americký	k2eAgMnSc2d1
psychologa	psycholog	k1gMnSc2
Michaela	Michael	k1gMnSc2
Baileyho	Bailey	k1gMnSc2
a	a	k8xC
psychiatra	psychiatr	k1gMnSc2
Richarda	Richard	k1gMnSc2
Pillarda	Pillard	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
publikovali	publikovat	k5eAaBmAgMnP
v	v	k7c6
prosinci	prosinec	k1gInSc6
1991	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Genetická	genetický	k2eAgFnSc1d1
studie	studie	k1gFnSc1
mužské	mužský	k2eAgFnSc2d1
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
výskytem	výskyt	k1gInSc7
homosexuality	homosexualita	k1gFnPc4
u	u	k7c2
dvojčat	dvojče	k1gNnPc2
a	a	k8xC
adoptivních	adoptivní	k2eAgMnPc2d1
sourozenců	sourozenec	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
alespoň	alespoň	k9
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
sourozenců	sourozenec	k1gMnPc2
byl	být	k5eAaImAgMnS
homosexuál	homosexuál	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
výzkumném	výzkumný	k2eAgInSc6d1
vzorku	vzorek	k1gInSc6
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
jednovaječných	jednovaječný	k2eAgNnPc2d1
dvojčat	dvojče	k1gNnPc2
bylo	být	k5eAaImAgNnS
homosexuální	homosexuální	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
pravděpodobností	pravděpodobnost	k1gFnSc7
52	#num#	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
druhé	druhý	k4xOgNnSc1
rovněž	rovněž	k9
homosexuální	homosexuální	k2eAgFnSc1d1
(	(	kIx(
<g/>
29	#num#	k4
z	z	k7c2
56	#num#	k4
případů	případ	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
dvojvaječných	dvojvaječný	k2eAgNnPc2d1
dvojčat	dvojče	k1gNnPc2
byla	být	k5eAaImAgFnS
shoda	shoda	k1gFnSc1
ve	v	k7c6
22	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
12	#num#	k4
z	z	k7c2
54	#num#	k4
případů	případ	k1gInPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
biologických	biologický	k2eAgMnPc2d1
sourozenců	sourozenec	k1gMnPc2
(	(	kIx(
<g/>
ne	ne	k9
dvojčat	dvojče	k1gNnPc2
<g/>
)	)	kIx)
9	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
13	#num#	k4
ze	z	k7c2
142	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
adoptivních	adoptivní	k2eAgMnPc2d1
bratrů	bratr	k1gMnPc2
11	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
6	#num#	k4
z	z	k7c2
57	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
že	že	k8xS
bratři	bratr	k1gMnPc1
matek	matka	k1gFnPc2
homosexuálních	homosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
byli	být	k5eAaImAgMnP
třikrát	třikrát	k6eAd1
častěji	často	k6eAd2
homosexuální	homosexuální	k2eAgInSc1d1
než	než	k8xS
bratři	bratr	k1gMnPc1
jejich	jejich	k3xOp3gMnPc2
otců	otec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autoři	autor	k1gMnPc1
z	z	k7c2
této	tento	k3xDgFnSc2
statistiky	statistika	k1gFnSc2
vyvodili	vyvodit	k5eAaPmAgMnP,k5eAaBmAgMnP
závěr	závěr	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
rozdíly	rozdíl	k1gInPc1
nasvědčují	nasvědčovat	k5eAaImIp3nP
genetické	genetický	k2eAgFnSc3d1
příčině	příčina	k1gFnSc3
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
podobným	podobný	k2eAgNnPc3d1
číslům	číslo	k1gNnPc3
(	(	kIx(
<g/>
48	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
16	#num#	k4
%	%	kIx~
a	a	k8xC
6	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
došli	dojít	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
i	i	k8xC
při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
ženského	ženský	k2eAgInSc2d1
vzorku	vzorek	k1gInSc2
populace	populace	k1gFnSc2
stejnou	stejný	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
David	David	k1gMnSc1
Gelman	Gelman	k1gMnSc1
založil	založit	k5eAaPmAgMnS
kritiku	kritika	k1gFnSc4
Bailey-Pillardovy	Bailey-Pillardův	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jednovaječná	jednovaječný	k2eAgNnPc1d1
dvojčata	dvojče	k1gNnPc1
mají	mít	k5eAaImIp3nP
identickou	identický	k2eAgFnSc4d1
genetickou	genetický	k2eAgFnSc4d1
výbavu	výbava	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
evidentní	evidentní	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
jiný	jiný	k2eAgInSc4d1
než	než	k8xS
genetický	genetický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
Cohen	Cohen	k1gInSc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
metodický	metodický	k2eAgInSc4d1
nedostatek	nedostatek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
zkoumaný	zkoumaný	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
vzorek	vzorek	k1gInSc1
byl	být	k5eAaImAgInS
složen	složit	k5eAaPmNgInS
z	z	k7c2
dobrovolníků	dobrovolník	k1gMnPc2
nalezených	nalezený	k2eAgMnPc2d1
prostřednictvím	prostřednictvím	k7c2
inzerátu	inzerát	k1gInSc2
v	v	k7c6
homosexuálních	homosexuální	k2eAgInPc6d1
časopisech	časopis	k1gInPc6
a	a	k8xC
nikoliv	nikoliv	k9
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
tisku	tisk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Výzkum	výzkum	k1gInSc1
Niklase	Niklas	k1gInSc5
Lå	Lå	k1gNnSc4
provedený	provedený	k2eAgMnSc1d1
na	na	k7c4
7	#num#	k4
600	#num#	k4
jednovaječných	jednovaječný	k2eAgNnPc6d1
dvojčatech	dvojče	k1gNnPc6
spíše	spíše	k9
potvrdil	potvrdit	k5eAaPmAgMnS
genetické	genetický	k2eAgFnPc4d1
dispozice	dispozice	k1gFnPc4
vzniku	vznik	k1gInSc2
homosexuality	homosexualita	k1gFnSc2
u	u	k7c2
18	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
%	%	kIx~
a	a	k8xC
vliv	vliv	k1gInSc4
„	„	k?
<g/>
specifického	specifický	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
“	“	k?
u	u	k7c2
61	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specifickým	specifický	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
okolnosti	okolnost	k1gFnPc1
vývoje	vývoj	k1gInSc2
a	a	k8xC
porodu	porod	k1gInSc2
<g/>
,	,	kIx,
traumata	trauma	k1gNnPc1
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
kachen	kachna	k1gFnPc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
ovlivnit	ovlivnit	k5eAaPmF
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
vtištěním	vtištění	k1gNnSc7
a	a	k8xC
vychovat	vychovat	k5eAaPmF
homosexuální	homosexuální	k2eAgMnPc4d1
kačery	kačer	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
lidí	člověk	k1gMnPc2
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
podobnému	podobný	k2eAgInSc3d1
jevu	jev	k1gInSc3
ve	v	k7c6
věku	věk	k1gInSc6
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
dítě	dítě	k1gNnSc1
rodič	rodič	k1gMnSc1
opačného	opačný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
odmítá	odmítat	k5eAaImIp3nS
nebo	nebo	k8xC
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
identifikován	identifikován	k2eAgMnSc1d1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
pohlavím	pohlaví	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
jeví	jevit	k5eAaImIp3nS
jako	jako	k9
biologická	biologický	k2eAgFnSc1d1
<g/>
,	,	kIx,
předurčená	předurčený	k2eAgFnSc1d1
komplexní	komplexní	k2eAgFnSc7d1
souhrou	souhra	k1gFnSc7
genetických	genetický	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
a	a	k8xC
raného	raný	k2eAgNnSc2d1
děložního	děložní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
není	být	k5eNaImIp3nS
osobní	osobní	k2eAgFnSc7d1
volbou	volba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
přesvědčivý	přesvědčivý	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
tvrzení	tvrzení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
rané	raný	k2eAgFnPc1d1
dětské	dětský	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
<g/>
,	,	kIx,
výchova	výchova	k1gFnSc1
<g/>
,	,	kIx,
sexuální	sexuální	k2eAgNnSc1d1
zneužití	zneužití	k1gNnSc1
nebo	nebo	k8xC
jiné	jiný	k2eAgFnPc1d1
závažné	závažný	k2eAgFnPc1d1
životní	životní	k2eAgFnPc1d1
události	událost	k1gFnPc1
ovlivňují	ovlivňovat	k5eAaImIp3nP
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
neexistuje	existovat	k5eNaImIp3nS
rovněž	rovněž	k9
žádný	žádný	k3yNgInSc4
přesvědčivý	přesvědčivý	k2eAgInSc4d1
vědecký	vědecký	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
vrozenosti	vrozenost	k1gFnSc2
a	a	k8xC
neurobiologické	urobiologický	k2eNgFnSc2d1
podstaty	podstata	k1gFnSc2
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecké	vědecký	k2eAgInPc1d1
důkazy	důkaz	k1gInPc1
původu	původ	k1gInSc2
homosexuality	homosexualita	k1gFnSc2
se	se	k3xPyFc4
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
považují	považovat	k5eAaImIp3nP
za	za	k7c4
relevantní	relevantní	k2eAgNnSc4d1
pro	pro	k7c4
teologickou	teologický	k2eAgFnSc4d1
a	a	k8xC
sociální	sociální	k2eAgFnSc4d1
diskusi	diskuse	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
podkopávají	podkopávat	k5eAaImIp3nP
tvrzení	tvrzení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
je	být	k5eAaImIp3nS
volbou	volba	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Endokrionologové	Endokrionolog	k1gMnPc1
Garcia-Falgueras	Garcia-Falgueras	k1gMnSc1
a	a	k8xC
Swaab	Swaab	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
studii	studie	k1gFnSc6
publikované	publikovaný	k2eAgFnSc6d1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
mozek	mozek	k1gInSc1
plodu	plod	k1gInSc2
se	se	k3xPyFc4
vyvíjí	vyvíjet	k5eAaImIp3nS
během	během	k7c2
intrauterinového	intrauterinový	k2eAgNnSc2d1
období	období	k1gNnSc2
v	v	k7c6
případě	případ	k1gInSc6
muže	muž	k1gMnSc2
skrze	skrze	k?
přímým	přímý	k2eAgNnSc7d1
působením	působení	k1gNnSc7
testosteronu	testosteron	k1gInSc2
na	na	k7c4
vyvíjené	vyvíjený	k2eAgFnPc4d1
nervové	nervový	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
nebo	nebo	k8xC
v	v	k7c6
případě	případ	k1gInSc6
ženy	žena	k1gFnSc2
skrze	skrze	k?
absence	absence	k1gFnSc1
nárůstu	nárůst	k1gInSc2
tohoto	tento	k3xDgInSc2
hormonu	hormon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
jsou	být	k5eAaImIp3nP
naše	náš	k3xOp1gFnSc1
genderová	genderový	k2eAgFnSc1d1
identita	identita	k1gFnSc1
(	(	kIx(
<g/>
přesvědčení	přesvědčení	k1gNnSc1
náležitosti	náležitost	k1gFnSc2
k	k	k7c3
mužskému	mužský	k2eAgNnSc3d1
nebo	nebo	k8xC
ženskému	ženský	k2eAgNnSc3d1
pohlaví	pohlaví	k1gNnSc3
<g/>
)	)	kIx)
a	a	k8xC
sexuální	sexuální	k2eAgFnPc1d1
orientace	orientace	k1gFnPc1
naprogramovány	naprogramován	k2eAgFnPc1d1
či	či	k8xC
organizovány	organizován	k2eAgFnPc1d1
do	do	k7c2
struktur	struktura	k1gFnPc2
našeho	náš	k3xOp1gInSc2
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
jsme	být	k5eAaImIp1nP
ještě	ještě	k6eAd1
v	v	k7c6
děloze	děloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
náznak	náznak	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
genderovou	genderový	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
nebo	nebo	k8xC
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
ovlivňovalo	ovlivňovat	k5eAaImAgNnS
sociální	sociální	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
po	po	k7c6
narození	narození	k1gNnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
Epstein	Epstein	k1gMnSc1
v	v	k7c6
internetové	internetový	k2eAgFnSc6d1
studii	studie	k1gFnSc6
publikované	publikovaný	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
shrnul	shrnout	k5eAaPmAgMnS
údaje	údaj	k1gInPc4
od	od	k7c2
více	hodně	k6eAd2
než	než	k8xS
18000	#num#	k4
lidí	člověk	k1gMnPc2
údaje	údaj	k1gInSc2
ohledně	ohledně	k7c2
jejich	jejich	k3xOp3gFnSc2
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
a	a	k8xC
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
během	během	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
dle	dle	k7c2
svého	svůj	k3xOyFgNnSc2
vyjádření	vyjádření	k1gNnSc2
změnili	změnit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1
fertilita	fertilita	k1gFnSc1
žen	žena	k1gFnPc2
s	s	k7c7
homosexuálním	homosexuální	k2eAgNnSc7d1
příbuzenstvem	příbuzenstvo	k1gNnSc7
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
zjistil	zjistit	k5eAaPmAgMnS
Camperio	Camperio	k1gMnSc1
Ciani	Ciaň	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
pokrevně	pokrevně	k6eAd1
spřízněny	spříznit	k5eAaPmNgFnP
s	s	k7c7
homosexuálními	homosexuální	k2eAgMnPc7d1
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
více	hodně	k6eAd2
potomků	potomek	k1gMnPc2
<g/>
,	,	kIx,
než	než	k8xS
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
homosexuální	homosexuální	k2eAgMnPc4d1
muže	muž	k1gMnPc4
v	v	k7c6
příbuzenstvu	příbuzenstvo	k1gNnSc6
nemají	mít	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
v	v	k7c6
plodnosti	plodnost	k1gFnSc6
byl	být	k5eAaImAgInS
nezanedbatelný	zanedbatelný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matky	matka	k1gFnPc1
homosexuálních	homosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
porodily	porodit	k5eAaPmAgFnP
za	za	k7c4
život	život	k1gInSc4
v	v	k7c6
průměru	průměr	k1gInSc6
2,7	2,7	k4
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matky	matka	k1gFnPc1
heterosexuálních	heterosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
přivedly	přivést	k5eAaPmAgFnP
na	na	k7c4
svět	svět	k1gInSc4
v	v	k7c6
průměru	průměr	k1gInSc6
jen	jen	k9
2,3	2,3	k4
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
vysvětlují	vysvětlovat	k5eAaImIp3nP
italští	italský	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
tentýž	týž	k3xTgInSc1
gen	gen	k1gInSc1
zřejmě	zřejmě	k6eAd1
vyvolává	vyvolávat	k5eAaImIp3nS
u	u	k7c2
mužů	muž	k1gMnPc2
i	i	k8xC
žen	žena	k1gFnPc2
identický	identický	k2eAgInSc4d1
efekt	efekt	k1gInSc4
<g/>
:	:	kIx,
zvýšenou	zvýšený	k2eAgFnSc4d1
sexuální	sexuální	k2eAgFnSc4d1
přitažlivost	přitažlivost	k1gFnSc4
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fyziologické	fyziologický	k2eAgInPc1d1
znaky	znak	k1gInPc1
korelující	korelující	k2eAgInPc1d1
s	s	k7c7
homosexualitou	homosexualita	k1gFnSc7
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
se	se	k3xPyFc4
berlínský	berlínský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
Arthur	Arthur	k1gMnSc1
Weil	Weil	k1gMnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
psychické	psychický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
se	se	k3xPyFc4
odrážejí	odrážet	k5eAaImIp3nP
na	na	k7c6
vnějším	vnější	k2eAgInSc6d1
vzhledu	vzhled	k1gInSc6
<g/>
,	,	kIx,
pokusil	pokusit	k5eAaPmAgMnS
najít	najít	k5eAaPmF
tělesné	tělesný	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
homosexuálů	homosexuál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
homosexuálních	homosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
jsou	být	k5eAaImIp3nP
boky	boka	k1gFnPc1
v	v	k7c6
poměru	poměr	k1gInSc6
k	k	k7c3
ramenům	rameno	k1gNnPc3
širší	široký	k2eAgInSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
závěry	závěr	k1gInPc4
byly	být	k5eAaImAgInP
dalšími	další	k2eAgMnPc7d1
výzkumy	výzkum	k1gInPc1
potvrzeny	potvrdit	k5eAaPmNgInP
<g/>
,	,	kIx,
jinými	jiný	k2eAgNnPc7d1
naopak	naopak	k6eAd1
vyvráceny	vyvrátit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
zkoumal	zkoumat	k5eAaImAgInS
stejnou	stejný	k2eAgFnSc4d1
otázku	otázka	k1gFnSc4
český	český	k2eAgMnSc1d1
sexuolog	sexuolog	k1gMnSc1
Kurt	Kurt	k1gMnSc1
Freund	Freund	k1gMnSc1
na	na	k7c6
skupině	skupina	k1gFnSc6
homosexuálních	homosexuální	k2eAgMnPc2d1
a	a	k8xC
heterosexuálních	heterosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
a	a	k8xC
nezjistil	zjistit	k5eNaPmAgInS
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
podstatné	podstatný	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
většího	veliký	k2eAgInSc2d2
penisu	penis	k1gInSc2
u	u	k7c2
homosexuálů	homosexuál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nález	nález	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
shledán	shledat	k5eAaPmNgInS
jako	jako	k8xS,k8xC
metodická	metodický	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
trojice	trojice	k1gFnSc1
kanadských	kanadský	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
prokázala	prokázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
gayů	gay	k1gMnPc2
a	a	k8xC
lesbiček	lesbička	k1gFnPc2
se	se	k3xPyFc4
častěji	často	k6eAd2
vyskytuje	vyskytovat	k5eAaImIp3nS
levorukost	levorukost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
témuž	týž	k3xTgInSc3
závěru	závěr	k1gInSc3
došli	dojít	k5eAaPmAgMnP
psychologové	psycholog	k1gMnPc1
Richard	Richarda	k1gFnPc2
A.	A.	kA
Lippa	Lippa	k1gFnSc1
a	a	k8xC
Ray	Ray	k1gMnSc1
Blanchard	Blanchard	k1gMnSc1
při	pře	k1gFnSc4
online	onlinout	k5eAaPmIp3nS
testu	testa	k1gFnSc4
stanice	stanice	k1gFnSc2
BBC	BBC	kA
(	(	kIx(
<g/>
13	#num#	k4
<g/>
%	%	kIx~
proti	proti	k7c3
11	#num#	k4
<g/>
%	%	kIx~
v	v	k7c6
heterosexuální	heterosexuální	k2eAgFnSc6d1
populaci	populace	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Americký	americký	k2eAgMnSc1d1
neurolog	neurolog	k1gMnSc1
Simon	Simon	k1gMnSc1
LeVay	LeVaa	k1gFnSc2
prováděl	provádět	k5eAaImAgMnS
průzkum	průzkum	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
neodhalil	odhalit	k5eNaPmAgMnS
tělesné	tělesný	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
heterosexuály	heterosexuál	k1gMnPc7
a	a	k8xC
homosexuály	homosexuál	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americký	americký	k2eAgMnSc1d1
biolog	biolog	k1gMnSc1
Terrance	Terrance	k1gFnSc2
Williams	Williams	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
homosexuální	homosexuální	k2eAgMnPc1d1
muži	muž	k1gMnPc1
mají	mít	k5eAaImIp3nP
delší	dlouhý	k2eAgInSc4d2
prsteníček	prsteníček	k1gInSc4
oproti	oproti	k7c3
ukazováčku	ukazováček	k1gInSc3
než	než	k8xS
heterosexuální	heterosexuální	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
tým	tým	k1gInSc1
ze	z	k7c2
Stanfordovy	Stanfordův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
na	na	k7c6
bázi	báze	k1gFnSc6
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
na	na	k7c6
základě	základ	k1gInSc6
fotografií	fotografia	k1gFnPc2
tváří	tvářet	k5eAaImIp3nS
rozpoznal	rozpoznat	k5eAaPmAgMnS
homosexuální	homosexuální	k2eAgMnPc4d1
muže	muž	k1gMnPc4
s	s	k7c7
úspěšností	úspěšnost	k1gFnSc7
až	až	k9
91	#num#	k4
%	%	kIx~
a	a	k8xC
homosexuální	homosexuální	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
s	s	k7c7
úspěšností	úspěšnost	k1gFnSc7
až	až	k9
84	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
ovlivnitelných	ovlivnitelný	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
např.	např.	kA
účes	účes	k1gInSc1
<g/>
,	,	kIx,
program	program	k1gInSc1
bral	brát	k5eAaImAgInS
do	do	k7c2
úvahy	úvaha	k1gFnSc2
i	i	k9
velikost	velikost	k1gFnSc4
brady	brada	k1gFnSc2
<g/>
,	,	kIx,
délku	délka	k1gFnSc4
nosu	nos	k1gInSc2
nebo	nebo	k8xC
výšku	výška	k1gFnSc4
čela	čelo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
populaci	populace	k1gFnSc6
</s>
<s>
Metodologické	metodologický	k2eAgNnSc1d1
podložení	podložení	k1gNnSc1
výzkumu	výzkum	k1gInSc2
o	o	k7c4
zastoupení	zastoupení	k1gNnSc4
homosexuality	homosexualita	k1gFnSc2
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
je	být	k5eAaImIp3nS
problematické	problematický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumy	výzkum	k1gInPc7
obvykle	obvykle	k6eAd1
mohou	moct	k5eAaImIp3nP
podchytit	podchytit	k5eAaPmF
výskyt	výskyt	k1gInSc4
určitých	určitý	k2eAgInPc2d1
druhů	druh	k1gInPc2
dílčích	dílčí	k2eAgFnPc2d1
fyziologických	fyziologický	k2eAgFnPc2d1
reakcí	reakce	k1gFnPc2
<g/>
,	,	kIx,
chování	chování	k1gNnSc2
nebo	nebo	k8xC
sebepojetí	sebepojetí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
bývají	bývat	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
příznačné	příznačný	k2eAgNnSc4d1
pro	pro	k7c4
homosexualitu	homosexualita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
podstata	podstata	k1gFnSc1
homosexuality	homosexualita	k1gFnPc4
běžné	běžný	k2eAgFnSc3d1
kvantitativní	kvantitativní	k2eAgFnSc3d1
vědě	věda	k1gFnSc3
dostupná	dostupný	k2eAgFnSc1d1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
nejprve	nejprve	k6eAd1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
šetření	šetření	k1gNnSc2
vymezit	vymezit	k5eAaPmF
homosexualitu	homosexualita	k1gFnSc4
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
zpravidla	zpravidla	k6eAd1
bývá	bývat	k5eAaImIp3nS
omezena	omezit	k5eAaPmNgFnS
srovnatelnost	srovnatelnost	k1gFnSc1
s	s	k7c7
výzkumy	výzkum	k1gInPc7
vycházejícími	vycházející	k2eAgInPc7d1
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
definice	definice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
také	také	k9
vzít	vzít	k5eAaPmF
v	v	k7c4
potaz	potaz	k1gInSc4
nejednoznačný	jednoznačný	k2eNgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
orientací	orientace	k1gFnSc7
a	a	k8xC
chováním	chování	k1gNnSc7
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
výzkumy	výzkum	k1gInPc1
mohou	moct	k5eAaImIp3nP
zachycovat	zachycovat	k5eAaImF
jen	jen	k9
vybrané	vybraný	k2eAgNnSc4d1
chování	chování	k1gNnSc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
výzkumy	výzkum	k1gInPc7
provádějí	provádět	k5eAaImIp3nP
dotazováním	dotazování	k1gNnPc3
náhodného	náhodný	k2eAgInSc2d1
vzorku	vzorek	k1gInSc2
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
roli	role	k1gFnSc4
hraje	hrát	k5eAaImIp3nS
i	i	k9
ožehavost	ožehavost	k1gFnSc1
tématu	téma	k1gNnSc2
pro	pro	k7c4
dotázané	dotázaný	k2eAgNnSc4d1
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
také	také	k9
jejich	jejich	k3xOp3gNnSc1
sebeuvědomění	sebeuvědomění	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
také	také	k9
hraje	hrát	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
dané	daný	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
přijímána	přijímán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
všech	všecek	k3xTgFnPc2
těchto	tento	k3xDgFnPc2
příčin	příčina	k1gFnPc2
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
výsledky	výsledek	k1gInPc1
průzkumů	průzkum	k1gInPc2
jsou	být	k5eAaImIp3nP
nejednoznačné	jednoznačný	k2eNgFnPc1d1
<g/>
,	,	kIx,
kolísají	kolísat	k5eAaImIp3nP
ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
1	#num#	k4
%	%	kIx~
do	do	k7c2
10	#num#	k4
%	%	kIx~
i	i	k9
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgFnSc1d2
čísla	číslo	k1gNnPc1
obvykle	obvykle	k6eAd1
preferují	preferovat	k5eAaImIp3nP
skupiny	skupina	k1gFnPc1
a	a	k8xC
média	médium	k1gNnPc1
blízké	blízký	k2eAgFnPc1d1
gay	gay	k1gMnSc1
ideologii	ideologie	k1gFnSc4
<g/>
,	,	kIx,
nižší	nízký	k2eAgNnPc1d2
čísla	číslo	k1gNnPc1
skupiny	skupina	k1gFnSc2
s	s	k7c7
konzervativními	konzervativní	k2eAgInPc7d1
názory	názor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
bývají	bývat	k5eAaImIp3nP
uváděna	uváděn	k2eAgFnSc1d1
4	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
ikonizovaný	ikonizovaný	k2eAgInSc4d1
údaj	údaj	k1gInSc4
z	z	k7c2
Kinseyových	Kinseyův	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
kulturním	kulturní	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
přesnější	přesný	k2eAgInPc1d2
výzkumy	výzkum	k1gInPc1
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
Evropu	Evropa	k1gFnSc4
a	a	k8xC
Severní	severní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Průzkumy	průzkum	k1gInPc1
sexuální	sexuální	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
a	a	k8xC
identity	identita	k1gFnSc2
</s>
<s>
První	první	k4xOgInSc1
podrobnější	podrobný	k2eAgInSc1d2
výzkum	výzkum	k1gInSc1
provedl	provést	k5eAaPmAgInS
v	v	k7c6
USA	USA	kA
mezi	mezi	k7c7
bělošskou	bělošský	k2eAgFnSc7d1
populací	populace	k1gFnSc7
Alfred	Alfred	k1gMnSc1
Kinsey	Kinsey	k1gInPc4
<g/>
,	,	kIx,
výsledky	výsledek	k1gInPc4
zveřejnil	zveřejnit	k5eAaPmAgMnS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
zprávách	zpráva	k1gFnPc6
–	–	k?
Sexuální	sexuální	k2eAgNnSc1d1
chování	chování	k1gNnSc1
muže	muž	k1gMnSc2
(	(	kIx(
<g/>
z	z	k7c2
r.	r.	kA
1948	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Sexuální	sexuální	k2eAgNnSc1d1
chování	chování	k1gNnSc1
ženy	žena	k1gFnSc2
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
těchto	tento	k3xDgFnPc2
zpráv	zpráva	k1gFnPc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
po	po	k7c6
dosažení	dosažení	k1gNnSc6
dospělosti	dospělost	k1gFnSc2
37	#num#	k4
%	%	kIx~
mužů	muž	k1gMnPc2
orgasmu	orgasmus	k1gInSc2
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
mužem	muž	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kinseyho	Kinseyha	k1gFnSc5
výzkumy	výzkum	k1gInPc1
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
staly	stát	k5eAaPmAgFnP
předmětem	předmět	k1gInSc7
kritiky	kritika	k1gFnSc2
jak	jak	k8xC,k8xS
z	z	k7c2
ideologických	ideologický	k2eAgFnPc2d1
<g/>
,	,	kIx,
tak	tak	k9
z	z	k7c2
vědeckých	vědecký	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
provedlo	provést	k5eAaPmAgNnS
v	v	k7c4
USA	USA	kA
Národní	národní	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
mínění	mínění	k1gNnSc2
výzkum	výzkum	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgNnSc2
4,9	4,9	k4
%	%	kIx~
amerických	americký	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
mělo	mít	k5eAaImAgNnS
po	po	k7c6
18	#num#	k4
<g/>
.	.	kIx.
roce	rok	k1gInSc6
věku	věk	k1gInSc2
mužského	mužský	k2eAgMnSc2d1
sexuálního	sexuální	k2eAgMnSc2d1
partnera	partner	k1gMnSc2
<g/>
,	,	kIx,
méně	málo	k6eAd2
než	než	k8xS
1	#num#	k4
%	%	kIx~
však	však	k9
mělo	mít	k5eAaImAgNnS
výhradně	výhradně	k6eAd1
homosexuální	homosexuální	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Počítačový	počítačový	k2eAgInSc1d1
dotazník	dotazník	k1gInSc1
mezi	mezi	k7c7
dospívajícími	dospívající	k2eAgMnPc7d1
muži	muž	k1gMnPc7
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
USA	USA	kA
ukázal	ukázat	k5eAaPmAgInS
5,5	5,5	k4
%	%	kIx~
sexuální	sexuální	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
s	s	k7c7
partnerem	partner	k1gMnSc7
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
(	(	kIx(
<g/>
4,9	4,9	k4
%	%	kIx~
<g/>
)	)	kIx)
vykázal	vykázat	k5eAaPmAgInS
průzkum	průzkum	k1gInSc1
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
však	však	k9
připouštěla	připouštět	k5eAaImAgNnP
volnější	volný	k2eAgNnPc1d2
hlediska	hledisko	k1gNnPc1
(	(	kIx(
<g/>
např.	např.	kA
kontakt	kontakt	k1gInSc4
rukama	ruka	k1gFnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
vzrostlo	vzrůst	k5eAaPmAgNnS
na	na	k7c4
13,4	13,4	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
výzkum	výzkum	k1gInSc1
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
(	(	kIx(
<g/>
Christopher	Christophra	k1gFnPc2
Bagley	Baglea	k1gFnSc2
a	a	k8xC
Pierre	Pierr	k1gInSc5
Tremblay	Trembla	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
za	za	k7c4
homosexuála	homosexuál	k1gMnSc4
v	v	k7c6
nějakém	nějaký	k3yIgInSc6
stupni	stupeň	k1gInSc6
označilo	označit	k5eAaPmAgNnS
15,3	15,3	k4
%	%	kIx~
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
za	za	k7c4
výhradně	výhradně	k6eAd1
homosexuální	homosexuální	k2eAgInPc4d1
5,9	5,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
zdraví	zdraví	k1gNnSc2
a	a	k8xC
sociálního	sociální	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
r.	r.	kA
1992	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
výskyt	výskyt	k1gInSc1
mužské	mužský	k2eAgFnSc2d1
homosexuality	homosexualita	k1gFnSc2
v	v	k7c6
populaci	populace	k1gFnSc6
nad	nad	k7c4
18	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
USA	USA	kA
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
4,9	4,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
Institut	institut	k1gInSc1
Alana	Alan	k1gMnSc2
Guttmachera	Guttmacher	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
výzkumu	výzkum	k1gInSc6
udalo	udat	k5eAaPmAgNnS
od	od	k7c2
1,8	1,8	k4
do	do	k7c2
2,8	2,8	k4
%	%	kIx~
mužů	muž	k1gMnPc2
sexuální	sexuální	k2eAgFnSc2d1
kontakt	kontakt	k1gInSc4
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
mužem	muž	k1gMnSc7
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
10	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
homosexuality	homosexualita	k1gFnSc2
ke	k	k7c3
zneužívání	zneužívání	k1gNnSc3
dětí	dítě	k1gFnPc2
</s>
<s>
Sexuální	sexuální	k2eAgInPc4d1
skandály	skandál	k1gInPc4
katolických	katolický	k2eAgMnPc2d1
duchovních	duchovní	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
činy	čin	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
převážné	převážný	k2eAgFnSc6d1
většině	většina	k1gFnSc6
medializovaných	medializovaný	k2eAgInPc2d1
případů	případ	k1gInPc2
týkaly	týkat	k5eAaImAgInP
dospívajících	dospívající	k2eAgMnPc2d1
chlapců	chlapec	k1gMnPc2
<g/>
,	,	kIx,
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
diskusím	diskuse	k1gFnPc3
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
homosexuálové	homosexuál	k1gMnPc1
náchylnější	náchylný	k2eAgMnPc1d2
ke	k	k7c3
zneužívání	zneužívání	k1gNnSc3
dětí	dítě	k1gFnPc2
či	či	k8xC
mladistvých	mladistvý	k1gMnPc2
než	než	k8xS
heterosexuálové	heterosexuál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Empirický	empirický	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
dokumentuje	dokumentovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
osob	osoba	k1gFnPc2
neovlivňuje	ovlivňovat	k5eNaImIp3nS
pravděpodobnost	pravděpodobnost	k1gFnSc1
zneužívání	zneužívání	k1gNnSc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
u	u	k7c2
zvířat	zvíře	k1gNnPc2
</s>
<s>
Dva	dva	k4xCgMnPc1
kačeři	kačer	k1gMnPc1
prokazující	prokazující	k2eAgMnPc1d1
si	se	k3xPyFc3
náklonnost	náklonnost	k1gFnSc4
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
u	u	k7c2
zvířat	zvíře	k1gNnPc2
zahrnují	zahrnovat	k5eAaImIp3nP
například	například	k6eAd1
sexuální	sexuální	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
námluvy	námluva	k1gFnPc4
<g/>
,	,	kIx,
náklonnost	náklonnost	k1gFnSc4
<g/>
,	,	kIx,
soužití	soužití	k1gNnSc4
v	v	k7c6
párech	pár	k1gInPc6
a	a	k8xC
rodičovství	rodičovství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
chování	chování	k1gNnSc1
je	být	k5eAaImIp3nS
ojediněle	ojediněle	k6eAd1
doloženo	doložen	k2eAgNnSc1d1
již	již	k6eAd1
z	z	k7c2
konce	konec	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumy	výzkum	k1gInPc1
podrobnější	podrobný	k2eAgInPc1d2
a	a	k8xC
oproštěné	oproštěný	k2eAgInPc1d1
od	od	k7c2
hodnotících	hodnotící	k2eAgInPc2d1
soudů	soud	k1gInPc2
se	se	k3xPyFc4
uskutečňovaly	uskutečňovat	k5eAaImAgFnP
až	až	k9
ke	k	k7c3
konci	konec	k1gInSc3
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
novějších	nový	k2eAgInPc2d2
výzkumů	výzkum	k1gInPc2
shrnul	shrnout	k5eAaPmAgMnS
v	v	k7c6
knize	kniha	k1gFnSc6
Biological	Biological	k1gMnSc2
Exuberance	Exuberanec	k1gMnSc2
(	(	kIx(
<g/>
Biologická	biologický	k2eAgFnSc1d1
nevázanost	nevázanost	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
kanadský	kanadský	k2eAgMnSc1d1
biolog	biolog	k1gMnSc1
Bruce	Bruce	k1gMnSc1
Bagemihl	Bagemihl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gMnSc2
byl	být	k5eAaImAgInS
sexuální	sexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
mezi	mezi	k7c7
jedinci	jedinec	k1gMnPc7
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
zjištěn	zjistit	k5eAaPmNgInS
u	u	k7c2
471	#num#	k4
živočišných	živočišný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
u	u	k7c2
savců	savec	k1gMnPc2
(	(	kIx(
<g/>
167	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ptáků	pták	k1gMnPc2
(	(	kIx(
<g/>
132	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
překvapivě	překvapivě	k6eAd1
hodně	hodně	k6eAd1
u	u	k7c2
bezobratlých	bezobratlý	k2eAgMnPc2d1
(	(	kIx(
<g/>
125	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zpočátku	zpočátku	k6eAd1
panovalo	panovat	k5eAaImAgNnS
přesvědčení	přesvědčení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
homosexuální	homosexuální	k2eAgInPc1d1
styky	styk	k1gInPc1
u	u	k7c2
zvířat	zvíře	k1gNnPc2
jsou	být	k5eAaImIp3nP
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
někdy	někdy	k6eAd1
u	u	k7c2
lidí	člověk	k1gMnPc2
<g/>
)	)	kIx)
náhražkové	náhražkový	k2eAgFnSc2d1
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nemají	mít	k5eNaImIp3nP
možnost	možnost	k1gFnSc4
se	se	k3xPyFc4
pářit	pářit	k5eAaImF
s	s	k7c7
příslušníkem	příslušník	k1gMnSc7
opačného	opačný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
(	(	kIx(
<g/>
nejprve	nejprve	k6eAd1
totiž	totiž	k9
byly	být	k5eAaImAgFnP
pozorovány	pozorovat	k5eAaImNgFnP
u	u	k7c2
zvířat	zvíře	k1gNnPc2
v	v	k7c6
zajetí	zajetí	k1gNnSc6
–	–	k?
například	například	k6eAd1
v	v	k7c6
některých	některý	k3yIgFnPc6
zoologických	zoologický	k2eAgFnPc6d1
zahradách	zahrada	k1gFnPc6
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Japonsku	Japonsko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
USA	USA	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
některých	některý	k3yIgFnPc6
oceanáriích	oceanárie	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
ale	ale	k8xC
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
není	být	k5eNaImIp3nS
–	–	k?
například	například	k6eAd1
v	v	k7c6
hejnech	hejno	k1gNnPc6
hus	husa	k1gFnPc2
divokých	divoký	k2eAgFnPc2d1
se	se	k3xPyFc4
vytvářejí	vytvářet	k5eAaImIp3nP
stejnopohlavní	stejnopohlavní	k2eAgInPc1d1
páry	pár	k1gInPc1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
je	být	k5eAaImIp3nS
dostatek	dostatek	k1gInSc4
volných	volný	k2eAgMnPc2d1
partnerů	partner	k1gMnPc2
opačného	opačný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
se	se	k3xPyFc4
vytvářejí	vytvářet	k5eAaImIp3nP
stabilní	stabilní	k2eAgInPc1d1
páry	pár	k1gInPc1
z	z	k7c2
jedinců	jedinec	k1gMnPc2
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
–	–	k?
třeba	třeba	k6eAd1
u	u	k7c2
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
racků	racek	k1gMnPc2
se	se	k3xPyFc4
samice	samice	k1gFnSc1
spáří	spářit	k5eAaPmIp3nS,k5eAaImIp3nS
se	s	k7c7
samcem	samec	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
ho	on	k3xPp3gMnSc4
opustí	opustit	k5eAaPmIp3nP
a	a	k8xC
o	o	k7c4
mláďata	mládě	k1gNnPc4
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
samicí	samice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
samčí	samčí	k2eAgInPc1d1
páry	pár	k1gInPc1
labutí	labuť	k1gFnPc2
černých	černý	k2eAgFnPc2d1
se	se	k3xPyFc4
spáří	spářet	k5eAaImIp3nP,k5eAaPmIp3nP
se	s	k7c7
samicemi	samice	k1gFnPc7
a	a	k8xC
vracejí	vracet	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
čtvrtý	čtvrtý	k4xOgMnSc1
tučňák	tučňák	k1gMnSc1
projevuje	projevovat	k5eAaImIp3nS
homosexuální	homosexuální	k2eAgInPc4d1
sklony	sklon	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
vědci	vědec	k1gMnPc1
(	(	kIx(
<g/>
například	například	k6eAd1
Janet	Janet	k1gInSc1
Mannová	Mannová	k1gFnSc1
<g/>
)	)	kIx)
takové	takový	k3xDgNnSc4
chování	chování	k1gNnSc4
označují	označovat	k5eAaImIp3nP
za	za	k7c4
evoluční	evoluční	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
zmírňuje	zmírňovat	k5eAaImIp3nS
vnitrodruhovou	vnitrodruhový	k2eAgFnSc4d1
agresi	agrese	k1gFnSc4
v	v	k7c6
době	doba	k1gFnSc6
rozmnožování	rozmnožování	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
výchově	výchova	k1gFnSc6
mláďat	mládě	k1gNnPc2
nebo	nebo	k8xC
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
citlivých	citlivý	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
</s>
<s>
Bůh	bůh	k1gMnSc1
Sutech	Sutech	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
pokusil	pokusit	k5eAaPmAgMnS
znásilnit	znásilnit	k5eAaPmF
Hora	Hora	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Starověk	starověk	k1gInSc1
</s>
<s>
V	v	k7c6
Babylonské	babylonský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
byla	být	k5eAaImAgFnS
homosexualita	homosexualita	k1gFnSc1
odsuzována	odsuzovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chamurapiho	Chamurapi	k1gMnSc4
zákoník	zákoník	k1gInSc1
stanovoval	stanovovat	k5eAaImAgInS
jako	jako	k9
trest	trest	k1gInSc1
za	za	k7c4
homosexuální	homosexuální	k2eAgInSc4d1
styk	styk	k1gInSc4
kastraci	kastrace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
starověkém	starověký	k2eAgInSc6d1
Egyptě	Egypt	k1gInSc6
nebyla	být	k5eNaImAgFnS
homosexualita	homosexualita	k1gFnSc1
pokládána	pokládat	k5eAaImNgFnS
za	za	k7c4
společensky	společensky	k6eAd1
pozitivní	pozitivní	k2eAgInSc4d1
jev	jev	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
byla	být	k5eAaImAgFnS
tolerována	tolerovat	k5eAaImNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
předmětem	předmět	k1gInSc7
přímého	přímý	k2eAgNnSc2d1
odsouzení	odsouzení	k1gNnSc2
nebyla	být	k5eNaImAgFnS
homosexualita	homosexualita	k1gFnSc1
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
<g/>
-li	-li	k?
násilná	násilný	k2eAgFnSc1d1
–	–	k?
homosexuální	homosexuální	k2eAgNnSc1d1
znásilnění	znásilnění	k1gNnSc1
muže	muž	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
mužem	muž	k1gMnSc7
<g/>
,	,	kIx,
o	o	k7c4
něž	jenž	k3xRgMnPc4
se	se	k3xPyFc4
podle	podle	k7c2
nepřímých	přímý	k2eNgInPc2d1
náznaků	náznak	k1gInPc2
pokusil	pokusit	k5eAaPmAgMnS
v	v	k7c6
mytologii	mytologie	k1gFnSc6
bůh	bůh	k1gMnSc1
Sutech	Sutech	k1gInSc4
na	na	k7c6
Horovi	Hora	k1gMnSc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
pokládána	pokládán	k2eAgFnSc1d1
za	za	k7c4
projev	projev	k1gInSc4
moci	moct	k5eAaImF
<g/>
,	,	kIx,
neboť	neboť	k8xC
Sutech	Sutech	k1gInSc1
tak	tak	k6eAd1
zdůvodňuje	zdůvodňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
proč	proč	k6eAd1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
královská	královský	k2eAgFnSc1d1
moc	moc	k1gFnSc1
svěřena	svěřit	k5eAaPmNgFnS
spíše	spíše	k9
jemu	on	k3xPp3gMnSc3
než	než	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
Horovi	Horův	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
bohové	bůh	k1gMnPc1
„	„	k?
<g/>
zvracejí	zvracet	k5eAaImIp3nP
hnusem	hnus	k1gInSc7
a	a	k8xC
plivají	plivat	k5eAaImIp3nP
na	na	k7c4
Hora	Hora	k1gMnSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
ten	ten	k3xDgInSc1
se	se	k3xPyFc4
však	však	k9
toho	ten	k3xDgMnSc4
může	moct	k5eAaImIp3nS
zprostit	zprostit	k5eAaPmF
poukazem	poukaz	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jej	on	k3xPp3gInSc4
Sutech	Sutech	k1gInSc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nezmocnil	zmocnit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
tomu	ten	k3xDgMnSc3
je	být	k5eAaImIp3nS
s	s	k7c7
homosexualitou	homosexualita	k1gFnSc7
pedofilní	pedofilní	k2eAgFnSc1d1
–	–	k?
se	s	k7c7
stykem	styk	k1gInSc7
muže	muž	k1gMnSc2
s	s	k7c7
nedospělým	nedospělý	k1gMnSc7
chlapcem	chlapec	k1gMnSc7
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
ten	ten	k3xDgInSc1
výslovně	výslovně	k6eAd1
pokládá	pokládat	k5eAaImIp3nS
za	za	k7c4
zapovězený	zapovězený	k2eAgMnSc1d1
Ptahhotepovo	Ptahhotepův	k2eAgNnSc4d1
naučení	naučení	k1gNnSc4
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
125	#num#	k4
<g/>
.	.	kIx.
kapitola	kapitola	k1gFnSc1
Knihy	kniha	k1gFnSc2
mrtvých	mrtvý	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
negativní	negativní	k2eAgFnPc4d1
zpovědi	zpověď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
případech	případ	k1gInPc6
měli	mít	k5eAaImAgMnP
starověcí	starověký	k2eAgMnPc1d1
Egypťané	Egypťan	k1gMnPc1
k	k	k7c3
homosexualitě	homosexualita	k1gFnSc3
vztah	vztah	k1gInSc1
spíše	spíše	k9
lhostejný	lhostejný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k2eAgInSc1d1
literární	literární	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c4
panovníkovi	panovník	k1gMnSc3
Pepim	Pepi	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gMnSc3
generálu	generál	k1gMnSc3
Sisenetovi	Sisenet	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
přímo	přímo	k6eAd1
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
jejich	jejich	k3xOp3gInSc6
homosexuálním	homosexuální	k2eAgInSc6d1
vztahu	vztah	k1gInSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
královo	králův	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
chlípné	chlípný	k2eAgNnSc4d1
a	a	k8xC
nevhodné	vhodný	k2eNgNnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Nelze	lze	k6eNd1
ale	ale	k9
vyloučit	vyloučit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
odsuzující	odsuzující	k2eAgInSc1d1
postoj	postoj	k1gInSc1
má	mít	k5eAaImIp3nS
spíše	spíše	k9
než	než	k8xS
morální	morální	k2eAgInSc4d1
především	především	k6eAd1
politický	politický	k2eAgInSc4d1
podtext	podtext	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Řecku	Řecko	k1gNnSc6
byla	být	k5eAaImAgFnS
homosexualita	homosexualita	k1gFnSc1
široce	široko	k6eAd1
akceptována	akceptován	k2eAgFnSc1d1
a	a	k8xC
mnohdy	mnohdy	k6eAd1
dokonce	dokonce	k9
státně	státně	k6eAd1
podporována	podporovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mít	mít	k5eAaImF
mladého	mladý	k2eAgMnSc4d1
chlapce	chlapec	k1gMnSc4
patřilo	patřit	k5eAaImAgNnS
k	k	k7c3
dobrému	dobré	k1gNnSc3
tónu	tón	k1gInSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
znak	znak	k1gInSc4
mužnosti	mužnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecká	řecký	k2eAgFnSc1d1
pederastie	pederastie	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
vztah	vztah	k1gInSc4
s	s	k7c7
chlapcem	chlapec	k1gMnSc7
ve	v	k7c6
věku	věk	k1gInSc6
asi	asi	k9
14	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
chlapce	chlapec	k1gMnSc4
nebylo	být	k5eNaImAgNnS
hanbou	hanba	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
ctí	ctít	k5eAaImIp3nS
mít	mít	k5eAaImF
takového	takový	k3xDgMnSc4
staršího	starý	k2eAgMnSc4d2
přítele	přítel	k1gMnSc4
<g/>
,	,	kIx,
patrona	patron	k1gMnSc4
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
jednalo	jednat	k5eAaImAgNnS
<g/>
-li	-li	k?
se	se	k3xPyFc4
o	o	k7c4
osobu	osoba	k1gFnSc4
společensky	společensky	k6eAd1
významnou	významný	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
též	též	k6eAd1
<g/>
:	:	kIx,
Platón	Platón	k1gMnSc1
<g/>
:	:	kIx,
Faidros	Faidrosa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Thébská	thébský	k2eAgFnSc1d1
svatá	svatý	k2eAgFnSc1d1
družina	družina	k1gFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
výlučně	výlučně	k6eAd1
z	z	k7c2
homosexuálních	homosexuální	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideou	idea	k1gFnSc7
jejího	její	k3xOp3gInSc2
vzniku	vznik	k1gInSc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bojovníci	bojovník	k1gMnPc1
spojení	spojení	k1gNnSc4
navzájem	navzájem	k6eAd1
milostnými	milostný	k2eAgNnPc7d1
pouty	pouto	k1gNnPc7
budou	být	k5eAaImBp3nP
bojovat	bojovat	k5eAaImF
srdnatěji	srdnatě	k6eAd2
<g/>
,	,	kIx,
protože	protože	k8xS
nebojují	bojovat	k5eNaImIp3nP
jen	jen	k9
za	za	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
za	za	k7c4
své	svůj	k3xOyFgMnPc4
milence	milenec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
33	#num#	k4
letech	léto	k1gNnPc6
existence	existence	k1gFnSc2
byl	být	k5eAaImAgInS
Svatá	svatý	k2eAgFnSc1d1
družina	družina	k1gFnSc1
zničena	zničit	k5eAaPmNgFnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Chaeroneie	Chaeroneie	k1gFnSc2
armádou	armáda	k1gFnSc7
Makedonců	Makedonec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
žena	žena	k1gFnSc1
pozoruje	pozorovat	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
homosexuální	homosexuální	k2eAgMnPc4d1
milence	milenec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Homosexuální	homosexuální	k2eAgNnSc1d1
chování	chování	k1gNnSc1
bylo	být	k5eAaImAgNnS
nejrozšířenější	rozšířený	k2eAgNnSc1d3
v	v	k7c6
dórských	dórský	k2eAgFnPc6d1
obcích	obec	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
je	být	k5eAaImIp3nS
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
součástí	součást	k1gFnSc7
svého	svůj	k3xOyFgInSc2
vzdělávacího	vzdělávací	k2eAgInSc2d1
a	a	k8xC
výcvikového	výcvikový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
Aristotelés	Aristotelés	k1gInSc1
podezříval	podezřívat	k5eAaImAgInS
Kréťany	Kréťan	k1gMnPc4
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
podporují	podporovat	k5eAaImIp3nP
k	k	k7c3
omezení	omezení	k1gNnSc3
porodnosti	porodnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Římě	Řím	k1gInSc6
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
obdobná	obdobný	k2eAgFnSc1d1
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
Nero	Nero	k1gMnSc1
dokonce	dokonce	k9
uzavřel	uzavřít	k5eAaPmAgMnS
s	s	k7c7
mužem	muž	k1gMnSc7
sňatek	sňatek	k1gInSc4
<g/>
,	,	kIx,
láska	láska	k1gFnSc1
císaře	císař	k1gMnSc4
Hadriána	Hadrián	k1gMnSc4
k	k	k7c3
řeckému	řecký	k2eAgMnSc3d1
mladíkovi	mladík	k1gMnSc3
jménem	jméno	k1gNnSc7
Antinoos	Antinoosa	k1gFnPc2
byla	být	k5eAaImAgFnS
dokonce	dokonce	k9
proměněna	proměnit	k5eAaPmNgFnS
ve	v	k7c4
státní	státní	k2eAgInSc4d1
kult	kult	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
totiž	totiž	k9
Antinoos	Antinoos	k1gMnSc1
při	při	k7c6
plavbě	plavba	k1gFnSc6
na	na	k7c6
Nilu	Nil	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
130	#num#	k4
n.	n.	k?
l.	l.	k?
utopil	utopit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Hadrián	Hadrián	k1gMnSc1
mu	on	k3xPp3gMnSc3
přikázal	přikázat	k5eAaPmAgMnS
vztyčit	vztyčit	k5eAaPmF
nespočet	nespočet	k1gInSc4
soch	socha	k1gFnPc2
a	a	k8xC
jako	jako	k9
bohu	bůh	k1gMnSc3
vycházejícího	vycházející	k2eAgInSc2d1
Měsíce	měsíc	k1gInSc2
mu	on	k3xPp3gMnSc3
přikázal	přikázat	k5eAaPmAgMnS
sloužit	sloužit	k5eAaImF
obřady	obřad	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
přejmenovat	přejmenovat	k5eAaPmF
(	(	kIx(
<g/>
spíše	spíše	k9
než	než	k8xS
postavit	postavit	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
také	také	k9
uvádí	uvádět	k5eAaImIp3nS
<g/>
)	)	kIx)
ve	v	k7c6
Středním	střední	k2eAgInSc6d1
Egyptě	Egypt	k1gInSc6
město	město	k1gNnSc1
Antinoupolis	Antinoupolis	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
s	s	k7c7
nástupem	nástup	k1gInSc7
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Číně	Čína	k1gFnSc6
byla	být	k5eAaImAgFnS
mravně	mravně	k6eAd1
indiferentní	indiferentní	k2eAgFnSc1d1
až	až	k9
do	do	k7c2
pádu	pád	k1gInSc2
dynastie	dynastie	k1gFnSc2
Chan	Chana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
poté	poté	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
tolerována	tolerován	k2eAgFnSc1d1
umělcům	umělec	k1gMnPc3
a	a	k8xC
aristokracii	aristokracie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
národy	národ	k1gInPc1
</s>
<s>
Vztah	vztah	k1gInSc1
přírodních	přírodní	k2eAgInPc2d1
národů	národ	k1gInPc2
k	k	k7c3
homosexualitě	homosexualita	k1gFnSc3
zahrnuje	zahrnovat	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
možných	možný	k2eAgInPc2d1
postojů	postoj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
národy	národ	k1gInPc1
pěstovaly	pěstovat	k5eAaImAgInP
iniciační	iniciační	k2eAgFnSc4d1
homosexualitu	homosexualita	k1gFnSc4
<g/>
,	,	kIx,
jiné	jiný	k2eAgNnSc4d1
ji	on	k3xPp3gFnSc4
trestaly	trestat	k5eAaImAgInP
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
podporovaly	podporovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prérijní	prérijní	k2eAgMnPc1d1
Indiáni	Indián	k1gMnPc1
například	například	k6eAd1
pěstovali	pěstovat	k5eAaImAgMnP
homosexualitu	homosexualita	k1gFnSc4
při	při	k7c6
„	„	k?
<g/>
pláňovém	pláňový	k2eAgNnSc6d1
tanci	tanec	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgMnS
zajistit	zajistit	k5eAaPmF
úspěšný	úspěšný	k2eAgInSc4d1
lov	lov	k1gInSc4
bizonů	bizon	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indiáni	Indián	k1gMnPc1
znali	znát	k5eAaImAgMnP
a	a	k8xC
respektovali	respektovat	k5eAaImAgMnP
rovněž	rovněž	k9
transsexualitu	transsexualita	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
nazývali	nazývat	k5eAaImAgMnP
„	„	k?
<g/>
Dvojím	dvojit	k5eAaImIp1nS
Duchem	duch	k1gMnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozdní	pozdní	k2eAgInSc1d1
středověk	středověk	k1gInSc1
a	a	k8xC
renesance	renesance	k1gFnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1327	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
anglický	anglický	k2eAgMnSc1d1
homosexuální	homosexuální	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
pozdního	pozdní	k2eAgInSc2d1
středověku	středověk	k1gInSc2
a	a	k8xC
renesance	renesance	k1gFnSc2
byla	být	k5eAaImAgFnS
homosexualita	homosexualita	k1gFnSc1
(	(	kIx(
<g/>
sodomie	sodomie	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
zvrácená	zvrácený	k2eAgFnSc1d1
láska	láska	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Evropě	Evropa	k1gFnSc6
společensky	společensky	k6eAd1
odsuzována	odsuzován	k2eAgFnSc1d1
a	a	k8xC
trestná	trestný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
nebránilo	bránit	k5eNaImAgNnS
především	především	k9
aristokracii	aristokracie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
věnovala	věnovat	k5eAaImAgNnP,k5eAaPmAgNnP
<g/>
.	.	kIx.
</s>
<s>
Dantova	Dantův	k2eAgFnSc1d1
Božská	božský	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
resp.	resp.	kA
její	její	k3xOp3gFnSc4
část	část	k1gFnSc4
Peklo	péct	k5eAaImAgNnS
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
jmen	jméno	k1gNnPc2
Dantových	Dantův	k2eAgMnPc2d1
současníků	současník	k1gMnPc2
<g/>
,	,	kIx,
sodomitů	sodomit	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Homosexuálové	homosexuál	k1gMnPc1
možná	možná	k9
byli	být	k5eAaImAgMnP
i	i	k9
významní	významný	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Leonardo	Leonardo	k1gMnSc1
da	da	k?
Vinci	Vinca	k1gMnPc7
<g/>
,	,	kIx,
Michelangelo	Michelangela	k1gFnSc5
Buonarotti	Buonarott	k2eAgMnPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Caravaggio	Caravaggio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leonardo	Leonardo	k1gMnSc1
byl	být	k5eAaImAgMnS
sice	sice	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1476	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
anonymního	anonymní	k2eAgNnSc2d1
udání	udání	k1gNnSc2
vyšetřován	vyšetřován	k2eAgInSc1d1
pro	pro	k7c4
poměr	poměr	k1gInSc4
se	s	k7c7
sedmnáctiletým	sedmnáctiletý	k2eAgInSc7d1
modelem	model	k1gInSc7
Jacopo	Jacopa	k1gFnSc5
Saltarellim	Saltarelli	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
vyšetřování	vyšetřování	k1gNnSc1
bylo	být	k5eAaImAgNnS
ukončeno	ukončit	k5eAaPmNgNnS
po	po	k7c6
dvou	dva	k4xCgInPc6
měsících	měsíc	k1gInPc6
bez	bez	k7c2
dalších	další	k2eAgInPc2d1
následků	následek	k1gInPc2
pro	pro	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
život	život	k1gInSc4
a	a	k8xC
kariéru	kariéra	k1gFnSc4
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
byl	být	k5eAaImAgInS
po	po	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
hledáčku	hledáček	k1gInSc6
Ufficiali	Ufficiali	k1gFnSc2
di	di	k?
notte	notte	k5eAaPmIp2nP
e	e	k0
conservatori	conservatori	k1gNnPc2
dell	dellum	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
'	'	kIx"
<g/>
onestà	onestà	k?
dei	dei	k?
monasteri	monaster	k1gFnSc2
<g/>
,	,	kIx,
jakési	jakýsi	k3yIgFnSc2
florentské	florentský	k2eAgFnSc2d1
mravnostní	mravnostní	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
Caravaggiově	Caravaggiův	k2eAgFnSc6d1
lásce	láska	k1gFnSc6
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
modelu	model	k1gInSc6
Ranucciovi	Ranuccius	k1gMnSc3
Thomassonimu	Thomassonim	k1gMnSc3
vypráví	vyprávět	k5eAaImIp3nS
například	například	k6eAd1
oblíbený	oblíbený	k2eAgInSc1d1
film	film	k1gInSc1
Dereka	Dereek	k1gMnSc2
Jarmana	Jarman	k1gMnSc2
Caravaggio	Caravaggio	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Caravaggio	Caravaggio	k6eAd1
svého	svůj	k3xOyFgMnSc4
milence	milenec	k1gMnSc4
(	(	kIx(
<g/>
snad	snad	k9
neúmyslně	úmyslně	k6eNd1
<g/>
)	)	kIx)
zabil	zabít	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1606	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michelangelo	Michelangela	k1gFnSc5
dedikoval	dedikovat	k5eAaBmAgMnS
Thomassovi	Thomass	k1gMnSc3
dei	dei	k?
Cavalierimu	Cavalierim	k1gInSc2
mj.	mj.	kA
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
milostných	milostný	k2eAgFnPc2d1
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
kterých	který	k3yRgInPc2,k3yIgInPc2,k3yQgInPc2
se	se	k3xPyFc4
usuzuje	usuzovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
homosexuál	homosexuál	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
skutečný	skutečný	k2eAgInSc4d1
rozruch	rozruch	k1gInSc4
se	se	k3xPyFc4
postaral	postarat	k5eAaPmAgMnS
papež	papež	k1gMnSc1
Julius	Julius	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1550	#num#	k4
<g/>
–	–	k?
<g/>
1555	#num#	k4
<g/>
)	)	kIx)
vztahem	vztah	k1gInSc7
se	s	k7c7
svým	své	k1gNnSc7
adoptivním	adoptivní	k2eAgNnSc7d1
„	„	k?
<g/>
synovcem	synovec	k1gMnSc7
<g/>
“	“	k?
Innocenzo	Innocenza	k1gFnSc5
Ciocchi	Ciocch	k1gMnSc5
Del	Del	k1gMnSc5
Monte	Mont	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgInPc1d2
papež	papež	k1gMnSc1
sebral	sebrat	k5eAaPmAgMnS
tohoto	tento	k3xDgMnSc4
negramotného	gramotný	k2eNgMnSc4d1
hocha	hoch	k1gMnSc4
z	z	k7c2
ulic	ulice	k1gFnPc2
Parmy	Parma	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
třináct	třináct	k4xCc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
čtrnáct	čtrnáct	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
sedmnáctiletého	sedmnáctiletý	k2eAgInSc2d1
jej	on	k3xPp3gMnSc4
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
do	do	k7c2
úřadu	úřad	k1gInSc2
cardinalis	cardinalis	k1gFnSc2
nepos	neposa	k1gFnPc2
mezi	mezi	k7c4
nejmocnější	mocný	k2eAgMnPc4d3
muže	muž	k1gMnPc4
tehdejšího	tehdejší	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobové	dobový	k2eAgFnPc1d1
satiry	satira	k1gFnPc1
nazývaly	nazývat	k5eAaImAgFnP
sedmnáctiletého	sedmnáctiletý	k2eAgNnSc2d1
kardinála	kardinál	k1gMnSc2
Ganymédés	Ganymédés	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
běžné	běžný	k2eAgNnSc1d1
renesanční	renesanční	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
homosexuála	homosexuál	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
benátský	benátský	k2eAgMnSc1d1
vyslanec	vyslanec	k1gMnSc1
hlásil	hlásit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Innocenzo	Innocenza	k1gFnSc5
sdílí	sdílet	k5eAaImIp3nS
s	s	k7c7
papežem	papež	k1gMnSc7
ložnici	ložnice	k1gFnSc4
i	i	k8xC
lože	lože	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protestanté	protestant	k1gMnPc1
využili	využít	k5eAaPmAgMnP
do	do	k7c2
očí	oko	k1gNnPc2
bijícího	bijící	k2eAgInSc2d1
skandálu	skandál	k1gInSc2
k	k	k7c3
rozsáhlému	rozsáhlý	k2eAgInSc3d1
útoku	útok	k1gInSc3
na	na	k7c4
instituci	instituce	k1gFnSc4
papežství	papežství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
Aztéky	Azték	k1gMnPc4
byla	být	k5eAaImAgFnS
homosexualita	homosexualita	k1gFnSc1
vnímána	vnímat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
porušení	porušení	k1gNnSc1
normy	norma	k1gFnSc2
a	a	k8xC
tvrdě	tvrdě	k6eAd1
trestána	trestat	k5eAaImNgFnS
napíchnutím	napíchnutí	k1gNnSc7
na	na	k7c4
kůl	kůl	k1gInSc4
či	či	k8xC
vytržením	vytržení	k1gNnSc7
vnitřností	vnitřnost	k1gFnSc7
přes	přes	k7c4
anální	anální	k2eAgInSc4d1
otvor	otvor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
represe	represe	k1gFnSc2
</s>
<s>
Růžový	růžový	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
<g/>
,	,	kIx,
označení	označení	k1gNnSc1
homosexuálních	homosexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
v	v	k7c6
nacistických	nacistický	k2eAgInPc6d1
koncentračních	koncentrační	k2eAgInPc6d1
táborech	tábor	k1gInPc6
</s>
<s>
Rostoucí	rostoucí	k2eAgInSc1d1
vliv	vliv	k1gInSc1
křesťanství	křesťanství	k1gNnSc2
přinesl	přinést	k5eAaPmAgInS
dramatickou	dramatický	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
postoje	postoj	k1gInSc2
už	už	k6eAd1
ve	v	k7c6
starověku	starověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Theodosiův	Theodosiův	k2eAgInSc1d1
kodex	kodex	k1gInSc1
(	(	kIx(
<g/>
kolem	kolem	k7c2
429	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
postavil	postavit	k5eAaPmAgMnS
mimo	mimo	k7c4
zákon	zákon	k1gInSc4
stejnopohlavní	stejnopohlavní	k2eAgInPc1d1
svazky	svazek	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
homosexuální	homosexuální	k2eAgInSc4d1
sex	sex	k1gInSc4
za	za	k7c4
nezákonný	zákonný	k2eNgInSc4d1
a	a	k8xC
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
jím	on	k3xPp3gNnSc7
provinili	provinit	k5eAaPmAgMnP
<g/>
,	,	kIx,
odsoudil	odsoudit	k5eAaPmAgMnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
upálením	upálení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Křesťanský	křesťanský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Justinián	Justinián	k1gMnSc1
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
homosexuály	homosexuál	k1gMnPc4
zodpovědnými	zodpovědný	k2eAgInPc7d1
za	za	k7c4
hladomory	hladomor	k1gMnPc4
<g/>
,	,	kIx,
zemětřesení	zemětřesení	k1gNnSc4
a	a	k8xC
mor	mor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Britský	britský	k2eAgMnSc1d1
Buggery	Buggera	k1gFnSc2
Act	Act	k1gFnPc1
z	z	k7c2
roku	rok	k1gInSc2
1533	#num#	k4
stanovil	stanovit	k5eAaPmAgMnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
obcují	obcovat	k5eAaImIp3nP
se	s	k7c7
Židy	Žid	k1gMnPc7
nebo	nebo	k8xC
Židovkami	Židovka	k1gFnPc7
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
souloží	souložit	k5eAaImIp3nP
se	s	k7c7
zvířaty	zvíře	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
sodomité	sodomit	k1gMnPc1
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
zaživa	zaživa	k6eAd1
upáleni	upálit	k5eAaPmNgMnP
po	po	k7c6
řádném	řádný	k2eAgNnSc6d1
přezkoumání	přezkoumání	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
přistiženi	přistihnout	k5eAaPmNgMnP
při	při	k7c6
činu	čin	k1gInSc6
a	a	k8xC
veřejně	veřejně	k6eAd1
usvědčeni	usvědčit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Až	až	k9
konec	konec	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
především	především	k9
první	první	k4xOgFnSc1
polovina	polovina	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přináší	přinášet	k5eAaImIp3nS
skutečně	skutečně	k6eAd1
systematické	systematický	k2eAgFnPc4d1
represe	represe	k1gFnPc4
spojené	spojený	k2eAgFnPc1d1
především	především	k6eAd1
s	s	k7c7
diktaturami	diktatura	k1gFnPc7
Franca	Franca	k?
<g/>
,	,	kIx,
Hitlera	Hitler	k1gMnSc2
a	a	k8xC
Mussoliniho	Mussolini	k1gMnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
spíše	spíše	k9
ojedinělé	ojedinělý	k2eAgInPc1d1
případy	případ	k1gInPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgInP
i	i	k9
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
a	a	k8xC
USA	USA	kA
ještě	ještě	k9
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
Thomas	Thomas	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paralelně	paralelně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
s	s	k7c7
poklesem	pokles	k1gInSc7
vlivu	vliv	k1gInSc2
křesťanství	křesťanství	k1gNnSc2
<g/>
,	,	kIx,
rozvojem	rozvoj	k1gInSc7
sexuologie	sexuologie	k1gFnSc2
jako	jako	k8xC,k8xS
zcela	zcela	k6eAd1
nové	nový	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
a	a	k8xC
růstem	růst	k1gInSc7
životní	životní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
Západu	západ	k1gInSc2
však	však	k9
sílí	sílet	k5eAaImIp3nS
emancipační	emancipační	k2eAgNnPc4d1
homosexuální	homosexuální	k2eAgNnPc4d1
hnutí	hnutí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Medicinalizace	Medicinalizace	k1gFnSc1
homosexuality	homosexualita	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Léčení	léčení	k1gNnSc2
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přinejmenším	přinejmenším	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1883	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
Emil	Emil	k1gMnSc1
Kraepelin	Kraepelina	k1gFnPc2
uvedl	uvést	k5eAaPmAgMnS
v	v	k7c6
klasifikaci	klasifikace	k1gFnSc6
duševních	duševní	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
mezi	mezi	k7c4
„	„	k?
<g/>
stavy	stav	k1gInPc4
psychické	psychický	k2eAgFnSc2d1
oslabenosti	oslabenost	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
homosexualita	homosexualita	k1gFnSc1
objevovala	objevovat	k5eAaImAgFnS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
odborných	odborný	k2eAgFnPc6d1
klasifikacích	klasifikace	k1gFnPc6
jako	jako	k8xC,k8xS
nemoc	nemoc	k1gFnSc4
či	či	k8xC
porucha	porucha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
psychiatrická	psychiatrický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
zařadila	zařadit	k5eAaPmAgFnS
homosexualitu	homosexualita	k1gFnSc4
v	v	k7c6
prvním	první	k4xOgInSc6
vydání	vydání	k1gNnSc6
Diagnostické	diagnostický	k2eAgFnSc2d1
a	a	k8xC
statistického	statistický	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
duševních	duševní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
(	(	kIx(
<g/>
DSM	DSM	kA
<g/>
)	)	kIx)
mezi	mezi	k7c4
sociopatické	sociopatický	k2eAgFnPc4d1
poruchy	porucha	k1gFnPc4
a	a	k8xC
v	v	k7c6
druhém	druhý	k4xOgNnSc6
vydání	vydání	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
mezi	mezi	k7c4
sexuální	sexuální	k2eAgFnSc4d1
deviace	deviace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následný	následný	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
neposkytl	poskytnout	k5eNaPmAgInS
žádný	žádný	k3yNgInSc4
empirický	empirický	k2eAgInSc4d1
nebo	nebo	k8xC
vědecký	vědecký	k2eAgInSc4d1
základ	základ	k1gInSc4
pro	pro	k7c4
považování	považování	k1gNnSc4
homosexuality	homosexualita	k1gFnSc2
za	za	k7c4
poruchu	porucha	k1gFnSc4
nebo	nebo	k8xC
abnormalitu	abnormalita	k1gFnSc4
namísto	namísto	k7c2
normální	normální	k2eAgFnSc2d1
a	a	k8xC
zdravé	zdravý	k2eAgFnSc2d1
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nahromadění	nahromadění	k1gNnSc6
výsledků	výsledek	k1gInPc2
takových	takový	k3xDgInPc2
výzkumů	výzkum	k1gInPc2
odborníci	odborník	k1gMnPc1
na	na	k7c4
lékařství	lékařství	k1gNnSc4
<g/>
,	,	kIx,
duševní	duševní	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
a	a	k8xC
behaviorální	behaviorální	k2eAgFnPc4d1
a	a	k8xC
sociální	sociální	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
dosáhli	dosáhnout	k5eAaPmAgMnP
shody	shoda	k1gFnSc2
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
chybné	chybný	k2eAgNnSc1d1
klasifikovat	klasifikovat	k5eAaImF
homosexualitu	homosexualita	k1gFnSc4
jako	jako	k8xC,k8xS
duševní	duševní	k2eAgFnSc4d1
poruchu	porucha	k1gFnSc4
a	a	k8xC
že	že	k8xS
klasifikace	klasifikace	k1gFnSc1
DSM	DSM	kA
reflektovala	reflektovat	k5eAaImAgFnS
nevyzkoušené	vyzkoušený	k2eNgFnPc4d1
domněnky	domněnka	k1gFnPc4
založené	založený	k2eAgFnPc4d1
na	na	k7c6
kdysi	kdysi	k6eAd1
převažujících	převažující	k2eAgFnPc6d1
sociálních	sociální	k2eAgFnPc6d1
normách	norma	k1gFnPc6
a	a	k8xC
dojmech	dojem	k1gInPc6
z	z	k7c2
klinické	klinický	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
od	od	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
nereprezentativních	reprezentativní	k2eNgInPc2d1
vzorků	vzorek	k1gInPc2
složených	složený	k2eAgInPc2d1
z	z	k7c2
pacientů	pacient	k1gMnPc2
hledajících	hledající	k2eAgMnPc2d1
terapii	terapie	k1gFnSc4
a	a	k8xC
jednotlivců	jednotlivec	k1gMnPc2
ve	v	k7c6
vězení	vězení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
Americká	americký	k2eAgFnSc1d1
psychiatrická	psychiatrický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
empirických	empirický	k2eAgInPc2d1
důkazů	důkaz	k1gInPc2
vědeckých	vědecký	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
schválila	schválit	k5eAaPmAgFnS
odebrání	odebrání	k1gNnSc4
homosexuality	homosexualita	k1gFnSc2
z	z	k7c2
diagnostického	diagnostický	k2eAgInSc2d1
a	a	k8xC
statistického	statistický	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
(	(	kIx(
<g/>
DSM-II	DSM-II	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
postavila	postavit	k5eAaPmAgFnS
se	se	k3xPyFc4
proti	proti	k7c3
veřejné	veřejný	k2eAgFnSc3d1
i	i	k8xC
soukromé	soukromý	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
homosexuálů	homosexuál	k1gMnPc2
a	a	k8xC
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
zákonů	zákon	k1gInPc2
podporujících	podporující	k2eAgInPc2d1
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1980	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
DSM-III	DSM-III	k1gFnSc6
mezi	mezi	k7c7
poruchy	poruch	k1gInPc7
výslovně	výslovně	k6eAd1
řazena	řadit	k5eAaImNgFnS
egodystonní	egodystonní	k2eAgFnSc1d1
homosexualita	homosexualita	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
DSM-IV	DSM-IV	k1gFnSc2
vypuštěna	vypustit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
nezařadila	zařadit	k5eNaPmAgFnS
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
homosexualitu	homosexualita	k1gFnSc4
do	do	k7c2
nové	nový	k2eAgFnSc2d1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
revize	revize	k1gFnPc4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
nemocí	nemoc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
posun	posun	k1gInSc1
postoje	postoj	k1gInSc2
provedly	provést	k5eAaPmAgFnP
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
psychiatrické	psychiatrický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
či	či	k8xC
odborné	odborný	k2eAgInPc4d1
státní	státní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
některých	některý	k3yIgInPc2
významných	významný	k2eAgInPc2d1
evropských	evropský	k2eAgInPc2d1
a	a	k8xC
asijských	asijský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
Japonská	japonský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
psychiatrů	psychiatr	k1gMnPc2
a	a	k8xC
neurologů	neurolog	k1gMnPc2
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
Čínská	čínský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
psychiatrů	psychiatr	k1gMnPc2
na	na	k7c6
základě	základ	k1gInSc6
svého	svůj	k3xOyFgInSc2
pětiletého	pětiletý	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americká	americký	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
psychologů	psycholog	k1gMnPc2
upozorňuje	upozorňovat	k5eAaImIp3nS
na	na	k7c4
dlouhodobou	dlouhodobý	k2eAgFnSc4d1
shodu	shoda	k1gFnSc4
behaviorálních	behaviorální	k2eAgFnPc2d1
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
i	i	k8xC
odborných	odborný	k2eAgFnPc2d1
profesí	profes	k1gFnPc2
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
homosexualita	homosexualita	k1gFnSc1
je	být	k5eAaImIp3nS
normální	normální	k2eAgFnSc1d1
a	a	k8xC
pozitivní	pozitivní	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
,	,	kIx,
staví	stavit	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
proti	proti	k7c3
dezinterpretaci	dezinterpretace	k1gFnSc3
vědeckých	vědecký	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
prezentaci	prezentace	k1gFnSc4
přesných	přesný	k2eAgFnPc2d1
vědeckých	vědecký	k2eAgFnPc2d1
a	a	k8xC
odborných	odborný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c4
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
pro	pro	k7c4
čelení	čelení	k1gNnSc4
předsudkům	předsudek	k1gInPc3
vycházejícím	vycházející	k2eAgInSc6d1
z	z	k7c2
nedostatečné	dostatečný	k2eNgFnSc2d1
znalosti	znalost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odepření	odepření	k1gNnSc1
civilního	civilní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
včetně	včetně	k7c2
vytvoření	vytvoření	k1gNnSc2
právních	právní	k2eAgInPc2d1
statusů	status	k1gInPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
registrované	registrovaný	k2eAgNnSc4d1
partnerství	partnerství	k1gNnSc4
<g/>
,	,	kIx,
stigmatizuje	stigmatizovat	k5eAaBmIp3nS
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
<g/>
,	,	kIx,
udržuje	udržovat	k5eAaImIp3nS
stigma	stigma	k1gNnSc1
historicky	historicky	k6eAd1
spjaté	spjatý	k2eAgFnSc2d1
s	s	k7c7
homosexualitou	homosexualita	k1gFnSc7
a	a	k8xC
posiluje	posilovat	k5eAaImIp3nS
předsudky	předsudek	k1gInPc4
proti	proti	k7c3
lesbám	lesba	k1gFnPc3
<g/>
,	,	kIx,
gayům	gay	k1gMnPc3
a	a	k8xC
bisexuálům	bisexuál	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homofobie	Homofobie	k1gFnPc4
a	a	k8xC
předsudky	předsudek	k1gInPc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Předsudky	předsudek	k1gInPc4
vůči	vůči	k7c3
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
a	a	k8xC
Homofobie	Homofobie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Protest	protest	k1gInSc1
proti	proti	k7c3
homosexualitě	homosexualita	k1gFnSc3
v	v	k7c6
tradičně	tradičně	k6eAd1
katolickém	katolický	k2eAgNnSc6d1
Polsku	Polsko	k1gNnSc6
</s>
<s>
Podle	podle	k7c2
prof.	prof.	kA
Hereka	Hereka	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
termín	termín	k1gInSc4
„	„	k?
<g/>
předsudky	předsudek	k1gInPc4
vůči	vůči	k7c3
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
<g/>
“	“	k?
negativní	negativní	k2eAgInPc4d1
postoje	postoj	k1gInPc4
a	a	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pojmu	pojem	k1gInSc2
homofobie	homofobie	k1gFnSc2
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgInPc4
předpoklady	předpoklad	k1gInPc4
o	o	k7c6
základních	základní	k2eAgInPc6d1
motivech	motiv	k1gInPc6
těchto	tento	k3xDgInPc2
postojů	postoj	k1gInPc2
<g/>
,	,	kIx,
vytyčuje	vytyčovat	k5eAaImIp3nS
studium	studium	k1gNnSc1
postojů	postoj	k1gInPc2
vůči	vůči	k7c3
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
širšího	široký	k2eAgInSc2d2
kontextu	kontext	k1gInSc2
sociálního	sociální	k2eAgInSc2d1
psychologického	psychologický	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
předsudků	předsudek	k1gInPc2
a	a	k8xC
vyhýbá	vyhýbat	k5eAaImIp3nS
se	se	k3xPyFc4
hodnotovým	hodnotový	k2eAgMnPc3d1
soudům	soud	k1gInPc3
o	o	k7c6
takových	takový	k3xDgInPc6
postojích	postoj	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homofobie	Homofobie	k1gFnSc1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
strach	strach	k1gInSc1
z	z	k7c2
osob	osoba	k1gFnPc2
s	s	k7c7
homosexuální	homosexuální	k2eAgFnSc7d1
orientaci	orientace	k1gFnSc4
<g/>
,	,	kIx,
odpor	odpor	k1gInSc4
nebo	nebo	k8xC
apriorní	apriorní	k2eAgFnSc1d1
nedůvěra	nedůvěra	k1gFnSc1
k	k	k7c3
nim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnSc1
diskriminace	diskriminace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1
homosexuální	homosexuální	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
v	v	k7c6
datech	datum	k1gNnPc6
</s>
<s>
1791	#num#	k4
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc1
země	země	k1gFnSc1
moderního	moderní	k2eAgInSc2d1
západního	západní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
dekriminalizuje	dekriminalizovat	k5eAaPmIp3nS
homosexuální	homosexuální	k2eAgInSc4d1
pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
1897	#num#	k4
<g/>
:	:	kIx,
Magnus	Magnus	k1gMnSc1
Hirschfeld	Hirschfeld	k1gMnSc1
zakládá	zakládat	k5eAaImIp3nS
v	v	k7c6
Německu	Německo	k1gNnSc6
Scientific-Humanitarian	Scientific-Humanitariany	k1gInPc2
Committee	Committee	k1gFnSc3
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
organizaci	organizace	k1gFnSc3
bojující	bojující	k2eAgMnSc1d1
za	za	k7c4
práva	právo	k1gNnPc4
homosexuálů	homosexuál	k1gMnPc2
(	(	kIx(
<g/>
pod	pod	k7c7
manifestem	manifest	k1gInSc7
se	se	k3xPyFc4
podepisují	podepisovat	k5eAaImIp3nP
mj.	mj.	kA
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
<g/>
,	,	kIx,
Hermann	Hermann	k1gMnSc1
Hesse	Hesse	k1gFnSc2
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
,	,	kIx,
Rainer	Rainer	k1gMnSc1
Maria	Mario	k1gMnSc2
Rilke	Rilke	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
Lev	Lev	k1gMnSc1
Tolstoj	Tolstoj	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1910	#num#	k4
<g/>
:	:	kIx,
Anarchistka	anarchistka	k1gFnSc1
Emma	Emma	k1gFnSc1
Goldman	Goldman	k1gMnSc1
veřejně	veřejně	k6eAd1
vyslovuje	vyslovovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
homosexuálům	homosexuál	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
<g/>
:	:	kIx,
Vzniká	vznikat	k5eAaImIp3nS
první	první	k4xOgFnSc1
americká	americký	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
bojující	bojující	k2eAgFnSc1d1
za	za	k7c2
práva	právo	k1gNnSc2
gayů	gay	k1gMnPc2
a	a	k8xC
lesbických	lesbický	k2eAgFnPc2d1
žen	žena	k1gFnPc2
–	–	k?
The	The	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Human	Human	k1gMnSc1
Rights	Rights	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
dekriminalizuje	dekriminalizovat	k5eAaPmIp3nS
pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
mezi	mezi	k7c7
osobami	osoba	k1gFnPc7
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
:	:	kIx,
Pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
mezi	mezi	k7c7
osobami	osoba	k1gFnPc7
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
je	být	k5eAaImIp3nS
dekriminalizován	dekriminalizován	k2eAgMnSc1d1
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
:	:	kIx,
Stonewallské	Stonewallský	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
v	v	k7c6
USA	USA	kA
jsou	být	k5eAaImIp3nP
někdy	někdy	k6eAd1
považovány	považován	k2eAgInPc1d1
za	za	k7c4
počátek	počátek	k1gInSc4
moderního	moderní	k2eAgNnSc2d1
LGBT	LGBT	kA
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
:	:	kIx,
Příznivci	příznivec	k1gMnPc1
homosexuálního	homosexuální	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
v	v	k7c6
USA	USA	kA
vedou	vést	k5eAaImIp3nP
kampaň	kampaň	k1gFnSc4
proti	proti	k7c3
označování	označování	k1gNnSc3
homosexuality	homosexualita	k1gFnSc2
za	za	k7c4
nemoc	nemoc	k1gFnSc4
a	a	k8xC
jejímu	její	k3xOp3gNnSc3
léčení	léčení	k1gNnSc3
a	a	k8xC
za	za	k7c4
její	její	k3xOp3gNnSc4
vyškrtnutí	vyškrtnutí	k1gNnSc4
z	z	k7c2
diagnostického	diagnostický	k2eAgInSc2d1
a	a	k8xC
klasifikačního	klasifikační	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sňatek	sňatek	k1gInSc1
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
</s>
<s>
1973	#num#	k4
<g/>
:	:	kIx,
Dozorčí	dozorčí	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Americké	americký	k2eAgFnSc2d1
psychiatrické	psychiatrický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
tlaku	tlak	k1gInSc2
ze	z	k7c2
strany	strana	k1gFnSc2
části	část	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
s	s	k7c7
odvoláním	odvolání	k1gNnSc7
na	na	k7c4
závěry	závěra	k1gFnPc4
vědeckých	vědecký	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
mimo	mimo	k7c4
běžnou	běžný	k2eAgFnSc4d1
revizi	revize	k1gFnSc4
schvaluje	schvalovat	k5eAaImIp3nS
odebrání	odebrání	k1gNnSc4
homosexuality	homosexualita	k1gFnSc2
z	z	k7c2
diagnostického	diagnostický	k2eAgInSc2d1
a	a	k8xC
statistického	statistický	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
(	(	kIx(
<g/>
DSM-II	DSM-II	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
proti	proti	k7c3
veřejné	veřejný	k2eAgFnSc3d1
i	i	k8xC
soukromé	soukromý	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
homosexuálů	homosexuál	k1gMnPc2
a	a	k8xC
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
zákonů	zákon	k1gInPc2
podporujících	podporující	k2eAgInPc2d1
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1974	#num#	k4
<g/>
:	:	kIx,
Členové	člen	k1gMnPc1
Americké	americký	k2eAgFnSc2d1
psychiatrické	psychiatrický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
v	v	k7c6
referendu	referendum	k1gNnSc6
schvalují	schvalovat	k5eAaImIp3nP
rozhodnutí	rozhodnutí	k1gNnSc4
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
odebírající	odebírající	k2eAgFnSc4d1
homosexualitu	homosexualita	k1gFnSc4
z	z	k7c2
diagnostického	diagnostický	k2eAgInSc2d1
a	a	k8xC
statistického	statistický	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
(	(	kIx(
<g/>
DSM-II	DSM-II	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
:	:	kIx,
Americká	americký	k2eAgFnSc1d1
psychologická	psychologický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
rezoluci	rezoluce	k1gFnSc6
vyjadřuje	vyjadřovat	k5eAaImIp3nS
souhlas	souhlas	k1gInSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
Americké	americký	k2eAgFnSc2d1
psychiatrické	psychiatrický	k2eAgFnSc2d1
asociaci	asociace	k1gFnSc3
a	a	k8xC
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
všichni	všechen	k3xTgMnPc1
odborníci	odborník	k1gMnPc1
na	na	k7c4
duševní	duševní	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
pomohli	pomoct	k5eAaPmAgMnP
vymýtit	vymýtit	k5eAaPmF
stigma	stigma	k1gNnSc4
duševní	duševní	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
spjaté	spjatý	k2eAgFnPc4d1
s	s	k7c7
homosexuální	homosexuální	k2eAgFnSc7d1
orientací	orientace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1991	#num#	k4
<g/>
:	:	kIx,
Americká	americký	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
psychoanalytiků	psychoanalytik	k1gMnPc2
se	se	k3xPyFc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
rezoluci	rezoluce	k1gFnSc6
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
proti	proti	k7c3
veřejné	veřejný	k2eAgFnSc3d1
i	i	k8xC
soukromé	soukromý	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
homosexuálů	homosexuál	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1993	#num#	k4
<g/>
:	:	kIx,
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
odebírá	odebírat	k5eAaImIp3nS
homosexualitu	homosexualita	k1gFnSc4
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
seznamu	seznam	k1gInSc2
poruch	porucha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
:	:	kIx,
Japonská	japonský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
psychiatrů	psychiatr	k1gMnPc2
a	a	k8xC
neurologů	neurolog	k1gMnPc2
odebírá	odebírat	k5eAaImIp3nS
homosexualitu	homosexualita	k1gFnSc4
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
seznamu	seznam	k1gInSc2
poruch	porucha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Čínská	čínský	k2eAgFnSc1d1
psychiatrická	psychiatrický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
odebírá	odebírat	k5eAaImIp3nS
homosexualitu	homosexualita	k1gFnSc4
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
seznamu	seznam	k1gInSc2
poruch	porucha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Výchova	výchova	k1gFnSc1
dětí	dítě	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Homoparentalita	Homoparentalita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lesbický	lesbický	k2eAgInSc1d1
pár	pár	k1gInSc1
s	s	k7c7
dětmi	dítě	k1gFnPc7
</s>
<s>
Mnoho	mnoho	k4c1
dětí	dítě	k1gFnPc2
vychovávají	vychovávat	k5eAaImIp3nP
lesby	lesba	k1gFnPc1
nebo	nebo	k8xC
gayové	gay	k1gMnPc1
jak	jak	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
stejnopohlavního	stejnopohlavní	k2eAgInSc2d1
páru	pár	k1gInSc2
tak	tak	k6eAd1
jako	jako	k8xC,k8xS
svobodní	svobodný	k2eAgMnPc1d1
rodiče	rodič	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc1
adopce	adopce	k1gFnSc2
dětí	dítě	k1gFnPc2
homosexuálními	homosexuální	k2eAgInPc7d1
páry	pár	k1gInPc7
nebo	nebo	k8xC
jejich	jejich	k3xOp3gMnPc7
členy	člen	k1gMnPc7
a	a	k8xC
výchova	výchova	k1gFnSc1
dětí	dítě	k1gFnPc2
stejnopohlavními	stejnopohlavní	k2eAgInPc7d1
páry	pár	k1gInPc7
(	(	kIx(
<g/>
homoparentalita	homoparentalita	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
společenských	společenský	k2eAgFnPc2d1
i	i	k8xC
politických	politický	k2eAgFnPc2d1
diskusí	diskuse	k1gFnPc2
a	a	k8xC
odborných	odborný	k2eAgNnPc2d1
studií	studio	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
někdy	někdy	k6eAd1
v	v	k7c6
politických	politický	k2eAgFnPc6d1
debatách	debata	k1gFnPc6
a	a	k8xC
laickém	laický	k2eAgInSc6d1
diskurzu	diskurz	k1gInSc6
objevují	objevovat	k5eAaImIp3nP
tvrzení	tvrzení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
heterosexuální	heterosexuální	k2eAgInPc1d1
páry	pár	k1gInPc1
jsou	být	k5eAaImIp3nP
přirozeně	přirozeně	k6eAd1
lepšími	dobrý	k2eAgMnPc7d2
rodiči	rodič	k1gMnPc7
než	než	k8xS
stejnopohlavní	stejnopohlavní	k2eAgInPc4d1
páry	pár	k1gInPc4
nebo	nebo	k8xC
že	že	k8xS
děti	dítě	k1gFnPc4
leseb	lesba	k1gFnPc2
či	či	k8xC
gayů	gay	k1gMnPc2
si	se	k3xPyFc3
vedou	vést	k5eAaImIp3nP
hůře	zle	k6eAd2
než	než	k8xS
děti	dítě	k1gFnPc4
vychovávané	vychovávaný	k2eAgFnPc4d1
heterosexuálními	heterosexuální	k2eAgMnPc7d1
rodiči	rodič	k1gMnPc7
<g/>
,	,	kIx,
tato	tento	k3xDgNnPc1
tvrzení	tvrzení	k1gNnPc1
nemají	mít	k5eNaImIp3nP
žádnou	žádný	k3yNgFnSc4
oporu	opora	k1gFnSc4
ve	v	k7c6
vědecké	vědecký	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
a	a	k8xC
náboženství	náboženství	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Homosexualita	homosexualita	k1gFnSc1
a	a	k8xC
náboženství	náboženství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
náboženství	náboženství	k1gNnSc2
k	k	k7c3
homosexuálnímu	homosexuální	k2eAgNnSc3d1
jednání	jednání	k1gNnSc3
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nS
a	a	k8xC
jednota	jednota	k1gFnSc1
nepanuje	panovat	k5eNaImIp3nS
ani	ani	k8xC
uvnitř	uvnitř	k7c2
velkých	velký	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
náboženství	náboženství	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
v	v	k7c6
křesťanství	křesťanství	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc1d1
denominace	denominace	k1gFnPc1
zaujímají	zaujímat	k5eAaImIp3nP
různé	různý	k2eAgInPc4d1
postoje	postoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
příkře	příkro	k6eAd1
odmítavých	odmítavý	k2eAgFnPc2d1
Apoštolská	apoštolský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
Jednota	jednota	k1gFnSc1
bratrská	bratrský	k2eAgFnSc1d1
nebo	nebo	k8xC
Římsko-katolická	římsko-katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
tolerantní	tolerantní	k2eAgNnSc4d1
<g/>
,	,	kIx,
gay	gay	k1gMnSc1
friendly	friendnout	k5eAaPmAgInP
postoje	postoj	k1gInPc1
<g/>
,	,	kIx,
například	například	k6eAd1
Starokatolická	starokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
švédští	švédský	k2eAgMnPc1d1
luteráni	luterán	k1gMnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
metodistů	metodista	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Českobratrská	českobratrský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
evangelická	evangelický	k2eAgFnSc1d1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
farářka	farářka	k1gFnSc1
Simona	Simona	k1gFnSc1
Tesařová	Tesařová	k1gFnSc1
udělila	udělit	k5eAaPmAgFnS
homosexuálnímu	homosexuální	k2eAgInSc3d1
páru	pár	k1gInSc3
jako	jako	k8xC,k8xS
první	první	k4xOgNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
církevní	církevní	k2eAgNnSc1d1
požehnání	požehnání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
též	též	k6eAd1
<g/>
:	:	kIx,
Logos	logos	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgNnPc1d1
abrahámovská	abrahámovský	k2eAgNnPc1d1
náboženství	náboženství	k1gNnPc1
(	(	kIx(
<g/>
islám	islám	k1gInSc1
a	a	k8xC
judaismus	judaismus	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vesměs	vesměs	k6eAd1
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
homosexualitě	homosexualita	k1gFnSc3
silně	silně	k6eAd1
odmítavě	odmítavě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
katolický	katolický	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
rižský	rižský	k2eAgMnSc1d1
kardinál	kardinál	k1gMnSc1
Janis	Janis	k1gFnSc2
Pujats	Pujats	k1gInSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Homosexualita	homosexualita	k1gFnSc1
není	být	k5eNaImIp3nS
sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
sexuální	sexuální	k2eAgFnSc1d1
perverze	perverze	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
za	za	k7c4
homosexuální	homosexuální	k2eAgInSc4d1
styk	styk	k1gInSc4
hrozí	hrozit	k5eAaImIp3nS
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
<g/>
,	,	kIx,
Kataru	katar	k1gInSc6
<g/>
,	,	kIx,
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
Afghánistánu	Afghánistán	k1gInSc6
<g/>
,	,	kIx,
Jemenu	Jemen	k1gInSc6
<g/>
,	,	kIx,
Mauritánii	Mauritánie	k1gFnSc6
<g/>
,	,	kIx,
Somálsku	Somálsko	k1gNnSc6
<g/>
,	,	kIx,
Súdánu	Súdán	k1gInSc6
<g/>
,	,	kIx,
Nigérii	Nigérie	k1gFnSc6
a	a	k8xC
v	v	k7c6
SAE	SAE	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Hrozí	hrozit	k5eAaImIp3nS
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
</s>
<s>
V	v	k7c6
mnoha	mnoho	k4c6
muslimských	muslimský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
homosexuální	homosexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
postihován	postihován	k2eAgInSc1d1
trestem	trest	k1gInSc7
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
a	a	k8xC
nemoci	nemoc	k1gFnSc3
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Muži	muž	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
sex	sex	k1gInSc4
s	s	k7c7
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Americké	americký	k2eAgNnSc1d1
Středisko	středisko	k1gNnSc1
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
nemocí	nemoc	k1gFnPc2
a	a	k8xC
prevenci	prevence	k1gFnSc3
uvedlo	uvést	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mezi	mezi	k7c7
mužskými	mužský	k2eAgMnPc7d1
homosexuály	homosexuál	k1gMnPc7
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
častěji	často	k6eAd2
syfilis	syfilis	k1gFnSc1
<g/>
,	,	kIx,
kapavka	kapavka	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
hepatitida	hepatitida	k1gFnSc1
B	B	kA
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
nežli	nežli	k8xS
v	v	k7c4
běžně	běžně	k6eAd1
populaci	populace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Machala	Machala	k1gMnSc1
z	z	k7c2
AIDS	AIDS	kA
centra	centrum	k1gNnSc2
Nemocnice	nemocnice	k1gFnSc1
na	na	k7c6
Bulovce	Bulovka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
kritizoval	kritizovat	k5eAaImAgMnS
nedostatečnou	dostatečný	k2eNgFnSc4d1
osvětu	osvěta	k1gFnSc4
proti	proti	k7c3
šíření	šíření	k1gNnSc3
AIDS	aids	k1gInSc4
a	a	k8xC
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nejrizikovější	rizikový	k2eAgFnSc7d3
skupinou	skupina	k1gFnSc7
nových	nový	k2eAgMnPc2d1
pacientů	pacient	k1gMnPc2
s	s	k7c7
nákazou	nákaza	k1gFnSc7
virem	vir	k1gInSc7
HIV	HIV	kA
a	a	k8xC
současně	současně	k6eAd1
kapavkou	kapavka	k1gFnSc7
nebo	nebo	k8xC
syfilitidou	syfilitida	k1gFnSc7
jsou	být	k5eAaImIp3nP
mladí	mladý	k2eAgMnPc1d1
gayové	gay	k1gMnPc1
s	s	k7c7
promiskuitním	promiskuitní	k2eAgNnSc7d1
chováním	chování	k1gNnSc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
stali	stát	k5eAaPmAgMnP
prototypem	prototyp	k1gInSc7
pacienta	pacient	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
2009	#num#	k4
a	a	k8xC
2010	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
homosexuálové	homosexuál	k1gMnPc1
mají	mít	k5eAaImIp3nP
chatrnější	chatrný	k2eAgNnSc4d2
duševní	duševní	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
a	a	k8xC
horší	zlý	k2eAgFnPc4d2
zkušenosti	zkušenost	k1gFnPc4
s	s	k7c7
lékařskou	lékařský	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
než	než	k8xS
heterosexuálové	heterosexuál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhodobý	dlouhodobý	k2eAgInSc4d1
psychologický	psychologický	k2eAgInSc4d1
nebo	nebo	k8xC
emocionální	emocionální	k2eAgInSc4d1
problém	problém	k1gInSc4
signalizovalo	signalizovat	k5eAaImAgNnS
5,2	5,2	k4
<g/>
%	%	kIx~
heterosexuálů	heterosexuál	k1gMnPc2
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
10,9	10,9	k4
<g/>
%	%	kIx~
u	u	k7c2
gayů	gay	k1gMnPc2
a	a	k8xC
15,0	15,0	k4
<g/>
%	%	kIx~
u	u	k7c2
bisexuálních	bisexuální	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
a	a	k8xC
6,0	6,0	k4
<g/>
%	%	kIx~
u	u	k7c2
heterosexuálů	heterosexuál	k1gMnPc2
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
12,3	12,3	k4
<g/>
%	%	kIx~
u	u	k7c2
leseb	lesba	k1gFnPc2
a	a	k8xC
18,8	18,8	k4
<g/>
%	%	kIx~
u	u	k7c2
bisexuálních	bisexuální	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
umění	umění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Homosexuální	homosexuální	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
byly	být	k5eAaImAgInP
inspirací	inspirace	k1gFnSc7
pro	pro	k7c4
vznik	vznik	k1gInSc4
významných	významný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
literárních	literární	k2eAgInPc2d1
<g/>
,	,	kIx,
filosofických	filosofický	k2eAgInPc2d1
i	i	k8xC
hudebních	hudební	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antická	antický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
nespočet	nespočet	k1gInSc1
příběhů	příběh	k1gInPc2
o	o	k7c6
lásce	láska	k1gFnSc6
mezi	mezi	k7c7
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k9
v	v	k7c6
písních	píseň	k1gFnPc6
<g/>
,	,	kIx,
milostné	milostný	k2eAgFnSc3d1
poezii	poezie	k1gFnSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
satiře	satira	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgInPc1d3
příběhy	příběh	k1gInPc1
najdeme	najít	k5eAaPmIp1nP
shrnuté	shrnutý	k2eAgMnPc4d1
v	v	k7c6
Ovidiových	Ovidiův	k2eAgFnPc6d1
Proměnách	proměna	k1gFnPc6
<g/>
,	,	kIx,
legendární	legendární	k2eAgInSc1d1
groteskní	groteskní	k2eAgInSc1d1
román	román	k1gInSc1
Satyricon	Satyricona	k1gFnPc2
je	být	k5eAaImIp3nS
příběhem	příběh	k1gInSc7
dvojice	dvojice	k1gFnSc2
bývalých	bývalý	k2eAgMnPc2d1
studentů	student	k1gMnPc2
zápasících	zápasící	k2eAgMnPc2d1
o	o	k7c4
přízeň	přízeň	k1gFnSc4
milce	milec	k1gMnSc4
Gitona	Giton	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
svod	svod	k1gInSc1
řecké	řecký	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
Anthologia	Anthologius	k1gMnSc2
Palatina	palatin	k1gMnSc2
obsahuje	obsahovat	k5eAaImIp3nS
kromě	kromě	k7c2
jiných	jiný	k2eAgMnPc2d1
knihu	kniha	k1gFnSc4
(	(	kIx(
<g/>
Músa	Músa	k1gFnSc1
Paidiké	Paidiká	k1gFnSc2
<g/>
)	)	kIx)
„	„	k?
<g/>
chlapcomilné	chlapcomilný	k2eAgFnSc2d1
<g/>
“	“	k?
poezie	poezie	k1gFnSc2
s	s	k7c7
258	#num#	k4
epigramy	epigram	k1gInPc7
a	a	k8xC
knihu	kniha	k1gFnSc4
(	(	kIx(
<g/>
Erótika	Erótikum	k1gNnSc2
<g/>
)	)	kIx)
milostné	milostný	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
s	s	k7c7
309	#num#	k4
epigramy	epigram	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sklonek	sklonek	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přináší	přinášet	k5eAaImIp3nS
renesanci	renesance	k1gFnSc4
homoerotického	homoerotický	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužskou	mužský	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
glorifikoval	glorifikovat	k5eAaBmAgMnS
Walt	Walt	k2eAgMnSc1d1
Whitman	Whitman	k1gMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
Stéblech	stéblo	k1gNnPc6
trávy	tráva	k1gFnSc2
<g/>
,	,	kIx,
homosexualita	homosexualita	k1gFnSc1
je	být	k5eAaImIp3nS
leitmotivem	leitmotiv	k1gInSc7
díla	dílo	k1gNnSc2
Thomase	Thomas	k1gMnSc2
Manna	Mann	k1gMnSc2
<g/>
,	,	kIx,
Allena	Allen	k1gMnSc2
Ginsberga	Ginsberg	k1gMnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ikonickými	ikonický	k2eAgFnPc7d1
postavami	postava	k1gFnPc7
gay	gay	k1gMnSc1
pride	pride	k6eAd1
hnutí	hnutí	k1gNnSc2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
například	například	k6eAd1
italský	italský	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Pier	pier	k1gInSc4
Paolo	Paolo	k1gNnSc4
Pasolini	Pasolin	k2eAgMnPc1d1
<g/>
,	,	kIx,
výtvarník	výtvarník	k1gMnSc1
Andy	Anda	k1gFnSc2
Warhol	Warhol	k1gInSc1
<g/>
,	,	kIx,
hudebníci	hudebník	k1gMnPc1
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
<g/>
,	,	kIx,
Freddie	Freddie	k1gFnSc1
Mercury	Mercura	k1gFnSc2
nebo	nebo	k8xC
George	Georg	k1gInSc2
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světovou	světový	k2eAgFnSc7d1
proslulost	proslulost	k1gFnSc4
si	se	k3xPyFc3
vydobyl	vydobýt	k5eAaPmAgMnS
slovenský	slovenský	k2eAgMnSc1d1
homosexuální	homosexuální	k2eAgMnSc1d1
fotograf	fotograf	k1gMnSc1
Robert	Robert	k1gMnSc1
Vano	vana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Právní	právní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
</s>
<s>
Nápis	nápis	k1gInSc1
v	v	k7c6
penzionu	penzion	k1gInSc6
v	v	k7c6
Keni	Keňa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužský	mužský	k2eAgInSc1d1
homosexuální	homosexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Keni	Keňa	k1gFnSc6
trestným	trestný	k2eAgInSc7d1
činem	čin	k1gInSc7
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
hrozí	hrozit	k5eAaImIp3nS
až	až	k6eAd1
14	#num#	k4
let	léto	k1gNnPc2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehledná	přehledný	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
po	po	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
spolu	spolu	k6eAd1
s	s	k7c7
mapou	mapa	k1gFnSc7
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
článku	článek	k1gInSc6
Zákony	zákon	k1gInPc1
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
homosexuality	homosexualita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
byla	být	k5eAaImAgFnS
i	i	k9
v	v	k7c6
zemích	zem	k1gFnPc6
s	s	k7c7
moderním	moderní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
trestným	trestný	k2eAgInSc7d1
činem	čin	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Ačkoliv	ačkoliv	k8xS
samotná	samotný	k2eAgFnSc1d1
homosexuální	homosexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
bez	bez	k7c2
homosexuálního	homosexuální	k2eAgNnSc2d1
chování	chování	k1gNnSc2
nebývá	bývat	k5eNaImIp3nS
předmětem	předmět	k1gInSc7
stíhání	stíhání	k1gNnSc2
v	v	k7c6
moderních	moderní	k2eAgInPc6d1
právních	právní	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
,	,	kIx,
i	i	k9
některé	některý	k3yIgFnPc1
vyspělé	vyspělý	k2eAgFnPc1d1
země	zem	k1gFnPc1
umožňovaly	umožňovat	k5eAaImAgFnP
nebo	nebo	k8xC
umožňují	umožňovat	k5eAaImIp3nP
znatelnou	znatelný	k2eAgFnSc4d1
diskriminaci	diskriminace	k1gFnSc4
homosexuálně	homosexuálně	k6eAd1
orientovaných	orientovaný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
je	být	k5eAaImIp3nS
nelegální	legální	k2eNgInSc1d1
v	v	k7c6
75	#num#	k4
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
zemí	zem	k1gFnPc2
jako	jako	k8xC,k8xS
Indie	Indie	k1gFnSc1
a	a	k8xC
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Homosexuální	homosexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
</s>
<s>
Omezováno	omezován	k2eAgNnSc1d1
bývá	bývat	k5eAaImIp3nS
například	například	k6eAd1
právo	právo	k1gNnSc1
na	na	k7c6
zaměstnání	zaměstnání	k1gNnSc6
nebo	nebo	k8xC
činnosti	činnost	k1gFnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
výchovy	výchova	k1gFnSc2
a	a	k8xC
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
armády	armáda	k1gFnSc2
a	a	k8xC
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc2
atd.	atd.	kA
V	v	k7c6
Číně	Čína	k1gFnSc6
údajně	údajně	k6eAd1
homosexuálně	homosexuálně	k6eAd1
orientované	orientovaný	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
nemají	mít	k5eNaImIp3nP
právo	právo	k1gNnSc4
uzavřít	uzavřít	k5eAaPmF
sňatek	sňatek	k1gInSc4
s	s	k7c7
osobou	osoba	k1gFnSc7
opačného	opačný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
římskokatolické	římskokatolický	k2eAgNnSc1d1
církevní	církevní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
anulaci	anulace	k1gFnSc3
(	(	kIx(
<g/>
zneplatnění	zneplatnění	k1gNnSc3
<g/>
)	)	kIx)
sňatku	sňatek	k1gInSc2
muže	muž	k1gMnSc2
se	s	k7c7
ženou	žena	k1gFnSc7
pro	pro	k7c4
relativní	relativní	k2eAgFnSc4d1
impotenci	impotence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
řada	řada	k1gFnSc1
zemí	zem	k1gFnPc2
zejména	zejména	k9
euroatlantického	euroatlantický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
postupně	postupně	k6eAd1
přijímá	přijímat	k5eAaImIp3nS
legislativu	legislativa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
postihuje	postihovat	k5eAaImIp3nS
homofobní	homofobní	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
z	z	k7c2
nenávisti	nenávist	k1gFnSc2
a	a	k8xC
verbální	verbální	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
postihuje	postihovat	k5eAaImIp3nS
„	„	k?
<g/>
podněcování	podněcování	k1gNnSc4
k	k	k7c3
nenávisti	nenávist	k1gFnSc3
<g/>
“	“	k?
<g/>
,	,	kIx,
byť	byť	k8xS
v	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
explicitně	explicitně	k6eAd1
nezmiňuje	zmiňovat	k5eNaImIp3nS
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
však	však	k9
aplikovat	aplikovat	k5eAaBmF
kategorii	kategorie	k1gFnSc4
„	„	k?
<g/>
jiná	jiný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijat	k2eAgInSc1d1
tzv.	tzv.	kA
antidiskriminační	antidiskriminační	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zaručuje	zaručovat	k5eAaImIp3nS
právo	právo	k1gNnSc4
na	na	k7c4
rovné	rovný	k2eAgNnSc4d1
zacházení	zacházení	k1gNnSc4
ve	v	k7c6
vymezených	vymezený	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
zaměstnání	zaměstnání	k1gNnSc4
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
,	,	kIx,
sociální	sociální	k2eAgNnSc4d1
zabezpečení	zabezpečení	k1gNnSc4
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc4
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s>
Sexuální	sexuální	k2eAgInSc4d1
styk	styk	k1gInSc4
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
stejnopohlavní	stejnopohlavní	k2eAgInSc1d1
sexuální	sexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
a	a	k8xC
společnostech	společnost	k1gFnPc6
je	být	k5eAaImIp3nS
sexuální	sexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
mezi	mezi	k7c7
osobami	osoba	k1gFnPc7
stejného	stejný	k2eAgInSc2d1
pohlaví	pohlaví	k1gNnSc6
trestný	trestný	k2eAgInSc4d1
<g/>
,	,	kIx,
často	často	k6eAd1
pod	pod	k7c7
sankcí	sankce	k1gFnSc7
nejvyšších	vysoký	k2eAgInPc2d3
trestů	trest	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
až	až	k9
trest	trest	k1gInSc1
smrti	smrt	k1gFnSc2
v	v	k7c6
některých	některý	k3yIgInPc6
muslimských	muslimský	k2eAgInPc6d1
státech	stát	k1gInPc6
jako	jako	k8xC,k8xS
Íránu	Írán	k1gInSc6
nebo	nebo	k8xC
Nigérii	Nigérie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mnoha	mnoho	k4c6
(	(	kIx(
<g/>
všech	všecek	k3xTgMnPc2
evropských	evropský	k2eAgMnPc2d1
a	a	k8xC
většině	většina	k1gFnSc6
asijských	asijský	k2eAgInPc2d1
a	a	k8xC
amerických	americký	k2eAgInPc2d1
<g/>
)	)	kIx)
státech	stát	k1gInPc6
na	na	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
nekladou	klást	k5eNaImIp3nP
překážky	překážka	k1gFnPc1
pro	pro	k7c4
dobrovolný	dobrovolný	k2eAgInSc4d1
sex	sex	k1gInSc4
mezi	mezi	k7c7
nepříbuznými	příbuzný	k2eNgFnPc7d1
osobami	osoba	k1gFnPc7
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
již	již	k6eAd1
dosáhli	dosáhnout	k5eAaPmAgMnP
věku	věk	k1gInSc2
ze	z	k7c2
zákona	zákon	k1gInSc2
daného	daný	k2eAgInSc2d1
státu	stát	k1gInSc2
dostatečného	dostatečný	k2eAgMnSc4d1
pro	pro	k7c4
provádění	provádění	k1gNnSc4
sexuálního	sexuální	k2eAgInSc2d1
styku	styk	k1gInSc2
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
byla	být	k5eAaImAgFnS
sodomie	sodomie	k1gFnSc1
vypuštěna	vypustit	k5eAaPmNgFnS
z	z	k7c2
trestního	trestní	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
započala	započnout	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
odtrestňování	odtrestňování	k1gNnSc2
po	po	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Svazek	svazek	k1gInSc1
nebo	nebo	k8xC
soužití	soužití	k1gNnSc4
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1
soužití	soužití	k1gNnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
</s>
<s>
Registrované	registrovaný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
</s>
<s>
Neregistrované	registrovaný	k2eNgNnSc4d1
soužití	soužití	k1gNnSc4
</s>
<s>
Neuznáváno	uznáván	k2eNgNnSc1d1
či	či	k8xC
neznámo	znám	k2eNgNnSc1d1
</s>
<s>
Manželství	manželství	k1gNnSc1
striktně	striktně	k6eAd1
vymezeno	vymezit	k5eAaPmNgNnS
jako	jako	k8xC,k8xS
svazek	svazek	k1gInSc1
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
</s>
<s>
V	v	k7c6
zemích	zem	k1gFnPc6
západní	západní	k2eAgFnSc2d1
(	(	kIx(
<g/>
euroamerické	euroamerický	k2eAgFnSc2d1
<g/>
)	)	kIx)
právní	právní	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
svazky	svazek	k1gInPc4
a	a	k8xC
soužití	soužití	k1gNnSc4
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
pod	pod	k7c4
obecná	obecný	k2eAgNnPc4d1
základní	základní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
a	a	k8xC
nejsou	být	k5eNaImIp3nP
státní	státní	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
ani	ani	k8xC
omezována	omezován	k2eAgFnSc1d1
nebo	nebo	k8xC
regulována	regulován	k2eAgFnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
podporována	podporován	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Zákony	zákon	k1gInPc1
některých	některý	k3yIgFnPc2
zemí	zem	k1gFnPc2
uznávají	uznávat	k5eAaImIp3nP
pro	pro	k7c4
páry	pár	k1gInPc4
osob	osoba	k1gFnPc2
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
obdobnou	obdobný	k2eAgFnSc4d1
nebo	nebo	k8xC
stejnou	stejný	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
i	i	k8xC
omezení	omezení	k1gNnSc4
jako	jako	k9
pro	pro	k7c4
manželství	manželství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
ve	v	k7c6
státě	stát	k1gInSc6
Iowa	Iow	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgMnS
o	o	k7c6
anulaci	anulace	k1gFnSc6
části	část	k1gFnSc6
zákona	zákon	k1gInSc2
595	#num#	k4
o	o	k7c6
manželství	manželství	k1gNnSc6
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
manželství	manželství	k1gNnSc1
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc4
je	být	k5eAaImIp3nS
platné	platný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
státu	stát	k1gInSc2
Iowa	Iow	k1gInSc2
toto	tento	k3xDgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
potvrdil	potvrdit	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společenské	společenský	k2eAgNnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
neoficiální	neoficiální	k2eAgInPc1d1,k2eNgInPc1d1
postoje	postoj	k1gInPc1
se	se	k3xPyFc4
však	však	k9
mnohdy	mnohdy	k6eAd1
liší	lišit	k5eAaImIp3nP
od	od	k7c2
postoje	postoj	k1gInSc2
deklarovaného	deklarovaný	k2eAgInSc2d1
nebo	nebo	k8xC
uplatňovaného	uplatňovaný	k2eAgInSc2d1
státní	státní	k2eAgInSc4d1
mocí	moc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Výchova	výchova	k1gFnSc1
dětí	dítě	k1gFnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
Zákon	zákon	k1gInSc1
o	o	k7c6
rodině	rodina	k1gFnSc6
umožňuje	umožňovat	k5eAaImIp3nS
osvojení	osvojení	k1gNnSc1
dítěte	dítě	k1gNnSc2
nebo	nebo	k8xC
pěstounskou	pěstounský	k2eAgFnSc4d1
péči	péče	k1gFnSc4
o	o	k7c4
ně	on	k3xPp3gMnPc4
jak	jak	k6eAd1
manželským	manželský	k2eAgInPc3d1
párům	pár	k1gInPc3
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
jednotlivcům	jednotlivec	k1gMnPc3
za	za	k7c4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
poskytují	poskytovat	k5eAaImIp3nP
vhodné	vhodný	k2eAgNnSc4d1
výchovné	výchovný	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodnost	vhodnost	k1gFnSc1
prostředí	prostředí	k1gNnSc2
je	být	k5eAaImIp3nS
posuzována	posuzovat	k5eAaImNgFnS
individuálně	individuálně	k6eAd1
správním	správní	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
a	a	k8xC
znaleckými	znalecký	k2eAgInPc7d1
posudky	posudek	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
projevit	projevit	k5eAaPmF
společenské	společenský	k2eAgInPc4d1
názory	názor	k1gInPc4
i	i	k8xC
lidské	lidský	k2eAgFnPc4d1
kvality	kvalita	k1gFnPc4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
rozhodují	rozhodovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
přijatý	přijatý	k2eAgInSc1d1
v	v	k7c6
ČR	ČR	kA
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc4
právo	právo	k1gNnSc4
výrazně	výrazně	k6eAd1
omezil	omezit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
osvojení	osvojení	k1gNnSc1
dítěte	dítě	k1gNnSc2
registrovaným	registrovaný	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
zcela	zcela	k6eAd1
vyloučil	vyloučit	k5eAaPmAgMnS
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
další	další	k2eAgFnPc4d1
okolnosti	okolnost	k1gFnPc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
jsou	být	k5eAaImIp3nP
vyloučeny	vyloučen	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
úředně	úředně	k6eAd1
zbavené	zbavený	k2eAgFnPc4d1
svéprávnosti	svéprávnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
diskriminační	diskriminační	k2eAgNnSc1d1
ustanovení	ustanovení	k1gNnSc1
zrušil	zrušit	k5eAaPmAgInS
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
Ústavní	ústavní	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Case	Case	k1gFnPc2
No	no	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
S147999	S147999	k1gFnSc1
in	in	k?
the	the	k?
Supreme	Suprem	k1gInSc5
Court	Courta	k1gFnPc2
of	of	k?
the	the	k?
State	status	k1gInSc5
of	of	k?
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
In	In	k1gMnSc1
re	re	k9
Marriage	Marriage	k1gInSc4
Cases	Cases	k1gInSc1
Judicial	Judicial	k1gInSc1
Council	Council	k1gInSc1
Coordination	Coordination	k1gInSc1
Proceeding	Proceeding	k1gInSc4
No	no	k9
<g/>
.	.	kIx.
4365	#num#	k4
<g/>
(	(	kIx(
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Lynne	Lynn	k1gInSc5
Lamberg	Lamberg	k1gMnSc1
<g/>
,	,	kIx,
JAMA	JAMA	kA
(	(	kIx(
<g/>
Journal	Journal	k1gMnSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Medical	Medical	k1gMnSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
contributor	contributor	k1gInSc1
JAMA	JAMA	kA
<g/>
:	:	kIx,
Gay	gay	k1gMnSc1
Is	Is	k1gMnSc1
Okay	Okaa	k1gFnSc2
With	With	k1gMnSc1
APA	APA	kA
(	(	kIx(
<g/>
American	American	k1gMnSc1
Psychiatric	Psychiatric	k1gMnSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
24	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
Royal	Royal	k1gMnSc1
College	Colleg	k1gInSc2
of	of	k?
Psychiatrists	Psychiatrists	k1gInSc1
<g/>
:	:	kIx,
Submission	Submission	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Church	Church	k1gInSc1
of	of	k?
England	England	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Listening	Listening	k1gInSc1
Exercise	Exercise	k1gFnSc2
on	on	k3xPp3gMnSc1
Human	Human	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Sexuality	sexualita	k1gFnSc2
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Pediatrics	Pediatricsa	k1gFnPc2
<g/>
:	:	kIx,
Sexual	Sexual	k1gInSc1
Orientation	Orientation	k1gInSc1
and	and	k?
Adolescents	Adolescents	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
American	American	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Pediatrics	Pediatrics	k1gInSc1
Clinical	Clinical	k1gMnSc1
Report	report	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Report	report	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Psychological	Psychological	k1gMnPc2
Association	Association	k1gInSc1
Task	Taska	k1gFnPc2
Force	force	k1gFnSc2
on	on	k3xPp3gMnSc1
Appropriate	Appropriat	k1gInSc5
Therapeutic	Therapeutice	k1gFnPc2
Responses	Responses	k1gMnSc1
to	ten	k3xDgNnSc1
Sexual	Sexual	k1gInSc1
Orientation	Orientation	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Pediatric	Pediatric	k1gMnSc1
Neuroendocrinology	Neuroendocrinolog	k1gMnPc4
<g/>
:	:	kIx,
Sexual	Sexual	k1gMnSc1
Hormones	Hormones	k1gMnSc1
and	and	k?
the	the	k?
Brain	Brain	k1gMnSc1
<g/>
:	:	kIx,
An	An	k1gMnSc1
Essential	Essential	k1gMnSc1
Alliance	Alliance	k1gFnSc2
for	forum	k1gNnPc2
Sexual	Sexual	k1gMnSc1
Identity	identita	k1gFnSc2
and	and	k?
Sexual	Sexual	k1gInSc1
Orientation	Orientation	k1gInSc1
<g/>
↑	↑	k?
American	American	k1gInSc1
Psychiatric	Psychiatrice	k1gFnPc2
Association	Association	k1gInSc1
Sexual	Sexual	k1gMnSc1
Orientation	Orientation	k1gInSc1
<g/>
↑	↑	k?
American	American	k1gInSc1
Psychological	Psychological	k1gMnSc1
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Resolution	Resolution	k1gInSc1
on	on	k3xPp3gMnSc1
Appropriate	Appropriat	k1gInSc5
Affirmative	Affirmativ	k1gInSc5
Responses	Responses	k1gMnSc1
to	ten	k3xDgNnSc1
Sexual	Sexual	k1gInSc4
Orientation	Orientation	k1gInSc1
Distress	Distress	k1gInSc1
and	and	k?
Change	change	k1gFnSc1
Efforts	Efforts	k1gInSc1
<g/>
↑	↑	k?
Terapie	terapie	k1gFnSc2
homosexuality	homosexualita	k1gFnSc2
<g/>
:	:	kIx,
sbírka	sbírka	k1gFnSc1
praktických	praktický	k2eAgFnPc2d1
rad	rada	k1gFnPc2
<g/>
,	,	kIx,
kterak	kterak	k8xS
(	(	kIx(
<g/>
se	s	k7c7
<g/>
)	)	kIx)
bránit	bránit	k5eAaImF
jinakosti	jinakost	k1gFnSc2
<g/>
↑	↑	k?
DUBAJ	DUBAJ	kA
<g/>
,	,	kIx,
Štefan	Štefan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
postoji	postoj	k1gInSc6
bratislavskej	bratislavskej	k?
verejnosti	verejnost	k1gFnSc2
k	k	k7c3
problematike	problematike	k1gNnSc3
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
M.	M.	kA
<g/>
A.	A.	kA
Thesis	Thesis	k?
<g/>
,	,	kIx,
Universita	universita	k1gFnSc1
Komenského	Komenský	k1gMnSc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Alfréd	Alfréd	k1gMnSc1
Kinsey	Kinsea	k1gFnSc2
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
zprávách	zpráva	k1gFnPc6
používal	používat	k5eAaImAgInS
tzv.	tzv.	kA
Kinseyovu	Kinseyův	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
jen	jen	k8xS
malý	malý	k2eAgInSc1d1
podíl	podíl	k1gInSc1
osob	osoba	k1gFnPc2
byl	být	k5eAaImAgInS
zařaditelný	zařaditelný	k2eAgInSc1d1
do	do	k7c2
krajních	krajní	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
↑	↑	k?
např.	např.	kA
Lois	Lois	k1gInSc1
J.	J.	kA
McDermott	McDermott	k1gInSc1
<g/>
,	,	kIx,
Sexual	Sexual	k1gInSc1
Orientation	Orientation	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
Department	department	k1gInSc1
of	of	k?
Psychology	psycholog	k1gMnPc4
<g/>
↑	↑	k?
Hans	Hans	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Blüher	Blühra	k1gFnPc2
<g/>
:	:	kIx,
Die	Die	k1gFnSc1
Rolle	Rolle	k1gFnSc2
der	drát	k5eAaImRp2nS
Erotik	erotik	k1gMnSc1
in	in	k?
der	drát	k5eAaImRp2nS
männlichen	männlichna	k1gFnPc2
Gesselschaft	Gesselschaft	k1gInSc1
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
↑	↑	k?
Ivo	Ivo	k1gMnSc1
Pondělníček	Pondělníček	k1gMnSc1
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Pondělníčková-Mašlová	Pondělníčková-Mašlová	k1gFnSc1
<g/>
:	:	kIx,
Lidská	lidský	k2eAgFnSc1d1
sexualita	sexualita	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1971	#num#	k4
<g/>
↑	↑	k?
Kertbeny	Kertben	k2eAgInPc1d1
<g/>
,	,	kIx,
Károly	Károla	k1gFnPc1
Mária	Márium	k1gNnSc2
(	(	kIx(
<g/>
1824	#num#	k4
<g/>
–	–	k?
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Archivováno	archivován	k2eAgNnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
encyklopedie	encyklopedie	k1gFnPc1
GLBTQ	GLBTQ	kA
<g/>
↑	↑	k?
Feray	Feraa	k1gFnSc2
<g/>
,	,	kIx,
Jean-Claude	Jean-Claud	k1gMnSc5
<g/>
;	;	kIx,
Herzer	Herzer	k1gMnSc1
<g/>
,	,	kIx,
Manfred	Manfred	k1gMnSc1
<g/>
:	:	kIx,
Homosexual	Homosexual	k1gMnSc1
Studies	Studies	k1gMnSc1
and	and	k?
Politics	Politics	k1gInSc1
in	in	k?
the	the	k?
19	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
<g/>
:	:	kIx,
Karl	Karl	k1gMnSc1
Maria	Mario	k1gMnSc2
Kertbeny	Kertben	k2eAgMnPc4d1
<g/>
,	,	kIx,
Journal	Journal	k1gFnSc4
of	of	k?
Homosexuality	homosexualita	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
↑	↑	k?
Biography	Biographa	k1gFnSc2
<g/>
:	:	kIx,
Karl	Karl	k1gMnSc1
Maria	Mario	k1gMnSc2
Kertbeny	Kertben	k2eAgMnPc4d1
<g/>
,	,	kIx,
GayHistory	GayHistor	k1gInPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
web	web	k1gInSc1
navštíven	navštíven	k2eAgInSc1d1
7	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Welcome	Welcom	k1gInSc5
to	ten	k3xDgNnSc4
Dialogue	Dialogue	k1gFnSc1
<g/>
:	:	kIx,
Historical	Historical	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Booklet	Booklet	k1gInSc1
#	#	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homosexual	Homosexual	k1gInSc1
<g/>
,	,	kIx,
Homosexuality	homosexualita	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Welcome	Welcom	k1gInSc5
Committee	Committee	k1gNnSc3
–	–	k?
Mennonites	Mennonites	k1gInSc1
Working	Working	k1gInSc1
to	ten	k3xDgNnSc1
Increase	Increasa	k1gFnSc3
Dialogue	Dialogu	k1gFnSc2
on	on	k3xPp3gMnSc1
Gay	gay	k1gMnSc1
and	and	k?
Lesbian	Lesbian	k1gInSc1
Inclusion	Inclusion	k1gInSc1
<g/>
↑	↑	k?
Homostudies	Homostudies	k1gInSc1
<g/>
:	:	kIx,
Paradigm	Paradigm	k1gMnSc1
Two	Two	k1gMnSc1
<g/>
:	:	kIx,
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
Homolexis	Homolexis	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
autor	autor	k1gMnSc1
<g/>
:	:	kIx,
Dyneslines	Dyneslines	k1gMnSc1
<g/>
↑	↑	k?
Manfred	Manfred	k1gMnSc1
Herzer	Herzer	k1gMnSc1
<g/>
:	:	kIx,
Kertbeny	Kertben	k2eAgInPc1d1
and	and	k?
the	the	k?
Nameless	Nameless	k1gInSc1
Love	lov	k1gInSc5
<g/>
,	,	kIx,
Journal	Journal	k1gFnSc4
of	of	k?
Homosexuality	homosexualita	k1gFnPc1
<g/>
,	,	kIx,
ročník	ročník	k1gInSc1
12	#num#	k4
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
1	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
↑	↑	k?
Klára	Klára	k1gFnSc1
Dokulilová	Dokulilová	k1gFnSc1
<g/>
:	:	kIx,
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
diskurzu	diskurz	k1gInSc6
sexuologie	sexuologie	k1gFnSc2
<g/>
,	,	kIx,
psychologie	psychologie	k1gFnSc2
a	a	k8xC
sociologie	sociologie	k1gFnSc2
<g/>
↑	↑	k?
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Tři	tři	k4xCgNnPc4
pojednání	pojednání	k1gNnPc4
k	k	k7c3
teorii	teorie	k1gFnSc3
sexuality	sexualita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
,	,	kIx,
česky	česky	k6eAd1
in	in	k?
<g/>
:	:	kIx,
Sebrané	sebraný	k2eAgInPc4d1
spisy	spis	k1gInPc4
Sigmunda	Sigmund	k1gMnSc2
Freuda	Freud	k1gMnSc2
<g/>
,	,	kIx,
Spisy	spis	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1904	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
,	,	kIx,
Psychoanalytické	psychoanalytický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
J.	J.	kA
Kocourek	Kocourek	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
33	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
↑	↑	k?
gay	gay	k1gMnSc1
definition	definition	k1gInSc1
-	-	kIx~
Dictionary	Dictionara	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Rejzek	Rejzek	k1gInSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
etymologický	etymologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
,	,	kIx,
LEDA	leda	k6eAd1
<g/>
,	,	kIx,
Voznice	voznice	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85927	#num#	k4
<g/>
-	-	kIx~
<g/>
85	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
buzerant	buzerant	k1gMnSc1
<g/>
↑	↑	k?
Günter	Günter	k1gMnSc1
Dörner	Dörner	k1gMnSc1
<g/>
:	:	kIx,
197	#num#	k4
<g/>
x	x	k?
<g/>
,	,	kIx,
citační	citační	k2eAgInPc4d1
údaje	údaj	k1gInPc4
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
↑	↑	k?
HAMER	Hamry	k1gInPc2
<g/>
,	,	kIx,
Dean	Dean	k1gNnSc1
H.	H.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
linkage	linkagat	k5eAaPmIp3nS
between	between	k2eAgInSc1d1
DNA	DNA	kA
markers	markers	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
X	X	kA
chromosome	chromosom	k1gInSc5
and	and	k?
male	male	k6eAd1
sexual	sexuat	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
orientation	orientation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
261	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5119	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
321	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
8075	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Bailey	Bailea	k1gMnSc2
JM	JM	kA
<g/>
,	,	kIx,
Pillard	Pillard	k1gMnSc1
RC	RC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
A	a	k8xC
genetic	genetice	k1gFnPc2
study	stud	k1gInPc7
of	of	k?
male	male	k6eAd1
sexual	sexuat	k5eAaImAgInS,k5eAaPmAgInS,k5eAaBmAgInS
orientation	orientation	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archiv	archiv	k1gInSc1
General	General	k1gFnSc2
Psychiatry	psychiatr	k1gMnPc4
48	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1089	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
1845227	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bailey	Bailea	k1gFnSc2
JM	JM	kA
<g/>
,	,	kIx,
Benishay	Benishay	k1gInPc7
DS	DS	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Familial	Familial	k1gInSc1
aggregation	aggregation	k1gInSc1
of	of	k?
female	female	k6eAd1
sexual	sexuat	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
orientation	orientation	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americký	americký	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Psychiatry	psychiatr	k1gMnPc4
150	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
272	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
8422079	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
David	David	k1gMnSc1
Gelman	Gelman	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Born	Born	k1gMnSc1
or	or	k?
Bred	Bred	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Newsweek	Newsweek	k1gMnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
46	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Richard	Richard	k1gMnSc1
Cohen	Cohen	k2eAgMnSc1d1
-	-	kIx~
„	„	k?
<g/>
Coming	Coming	k1gInSc1
Out	Out	k1gMnSc1
Straight	Straight	k1gMnSc1
<g/>
:	:	kIx,
Understanding	Understanding	k1gInSc1
and	and	k?
Healing	Healing	k1gInSc1
Homosexuality	homosexualita	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Oakhill	Oakhill	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
-	-	kIx~
http://www.gaytostraight.org/cos-chapter2.htm	http://www.gaytostraight.org/cos-chapter2.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Vliv	vliv	k1gInSc4
společnosti	společnost	k1gFnSc2
na	na	k7c4
výběr	výběr	k1gInSc4
partnera	partner	k1gMnSc2
<g/>
↑	↑	k?
FRANCK	FRANCK	kA
<g/>
,	,	kIx,
Dierk	Dierk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verhaltensbiologie	Verhaltensbiologie	k1gFnSc1
<g/>
,	,	kIx,
Einfuehrung	Einfuehrung	k1gInSc1
in	in	k?
die	die	k?
Ethologie	Ethologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stuttgart	Stuttgart	k1gInSc1
<g/>
:	:	kIx,
Thieme	Thiem	k1gInSc5
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3135676036	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Church	Church	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
much	moucha	k1gFnPc2
is	is	k?
known	known	k1gMnSc1
about	about	k1gMnSc1
the	the	k?
origins	origins	k1gInSc1
of	of	k?
homosexuality	homosexualita	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Garcia-Falgueras	Garcia-Falgueras	k1gMnSc1
<g/>
,	,	kIx,
A	A	kA
<g/>
,	,	kIx,
&	&	k?
Swaab	Swaab	k1gInSc1
<g/>
,	,	kIx,
DF	DF	kA
<g/>
,	,	kIx,
New	New	k1gFnSc1
Evidence	evidence	k1gFnSc1
of	of	k?
Genetic	Genetice	k1gFnPc2
Factors	Factorsa	k1gFnPc2
Influencing	Influencing	k1gInSc1
Sexual	Sexual	k1gMnSc1
Orientation	Orientation	k1gInSc1
in	in	k?
Men	Men	k1gFnSc2
<g/>
:	:	kIx,
Female	Femala	k1gFnSc3
Fecundity	Fecundita	k1gFnSc2
Increase	Increasa	k1gFnSc3
in	in	k?
the	the	k?
Maternal	Maternal	k1gFnSc1
Line	linout	k5eAaImIp3nS
<g/>
,	,	kIx,
in	in	k?
Endocrine	Endocrin	k1gInSc5
Development	Development	k1gInSc4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
(	(	kIx(
<g/>
abstract	abstract	k1gInSc1
<g/>
)	)	kIx)
dostupné	dostupný	k2eAgNnSc1d1
také	také	k9
na	na	k7c4
PubMed	PubMed	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Do	do	k7c2
Gays	Gaysa	k1gFnPc2
Have	Hav	k1gFnSc2
a	a	k8xC
Choice	Choice	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Scientific	Scientific	k1gMnSc1
American	American	k1gMnSc1
<g/>
,	,	kIx,
March	March	k1gMnSc1
2006	#num#	k4
<g/>
↑	↑	k?
Geny	gen	k1gInPc1
pro	pro	k7c4
homosexualitu	homosexualita	k1gFnSc4
mužů	muž	k1gMnPc2
svědčí	svědčit	k5eAaImIp3nP
ženám	žena	k1gFnPc3
<g/>
,	,	kIx,
Osel	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Je	být	k5eAaImIp3nS
homosexuální	homosexuální	k2eAgNnSc1d1
cítění	cítění	k1gNnSc1
vrozené	vrozený	k2eAgNnSc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
University	universita	k1gFnSc2
of	of	k?
Toronto	Toronto	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canadian	Canadian	k1gInSc1
Scientists	Scientists	k1gInSc4
Find	Find	k?
More	mor	k1gInSc5
Homosexuals	Homosexuals	k1gInSc4
Left-Handed	Left-Handed	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ScienceDaily	ScienceDail	k1gMnPc7
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Handedness	Handedness	k1gInSc1
<g/>
,	,	kIx,
birth	birth	k1gMnSc1
order	order	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
sexual	sexual	k1gInSc1
orientation	orientation	k1gInSc1
<g/>
↑	↑	k?
Finger-length	Finger-length	k1gInSc1
ratios	ratios	k1gMnSc1
and	and	k?
sexual	sexuat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
orientation	orientation	k1gInSc4
<g/>
.	.	kIx.
www.unl.edu	www.unl.edu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
28	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
kar	kar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
pozná	poznat	k5eAaPmIp3nS
homosexuály	homosexuál	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
jí	on	k3xPp3gFnSc3
k	k	k7c3
tomu	ten	k3xDgNnSc3
fotka	fotka	k1gFnSc1
a	a	k8xC
pár	pár	k4xCyI
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2017-09-08	2017-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Yilun	Yilun	k1gMnSc1
Wang	Wang	k1gMnSc1
<g/>
;	;	kIx,
KOSINSKI	KOSINSKI	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deep	Deep	k1gInSc1
neural	urat	k5eNaImAgInS,k5eNaPmAgInS
networks	tworks	k6eNd1
are	ar	k1gInSc5
more	mor	k1gInSc5
accurate	accurat	k1gInSc5
than	thano	k1gNnPc2
humans	humans	k1gInSc1
at	at	k?
detecting	detecting	k1gInSc4
sexual	sexuat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
orientation	orientation	k1gInSc4
from	from	k1gMnSc1
facial	facial	k1gMnSc1
images	images	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanford	Stanford	k1gInSc1
<g/>
:	:	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
American	American	k1gInSc1
Psychological	Psychological	k1gMnSc1
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Lesbian	Lesbian	k1gMnSc1
&	&	k?
Gay	gay	k1gMnSc1
Parenting	Parenting	k1gInSc1
<g/>
↑	↑	k?
Affidavit	Affidavit	k1gFnSc2
of	of	k?
Michael	Michael	k1gMnSc1
Lamb	Lamb	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
United	United	k1gInSc1
States	States	k1gInSc1
District	District	k1gInSc1
Court	Courta	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
district	district	k1gMnSc1
of	of	k?
Massachusetts	Massachusetts	k1gNnPc2
<g/>
,	,	kIx,
civil	civil	k1gMnSc1
action	action	k1gInSc1
no	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-cv-	-cv-	k?
<g/>
10309	#num#	k4
<g/>
↑	↑	k?
Facts	Factsa	k1gFnPc2
About	About	k1gMnSc1
Homosexuality	homosexualita	k1gFnSc2
and	and	k?
Child	Child	k1gInSc1
Molestation	Molestation	k1gInSc1
<g/>
.	.	kIx.
psychology	psycholog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
ucdavis	ucdavis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
19	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stanowisko	Stanowisko	k1gNnSc4
polskiego	polskiego	k6eAd1
Towarzystwa	Towarzystw	k1gInSc2
seksuologicznego	seksuologicznego	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
↑	↑	k?
BAGEMIHL	BAGEMIHL	kA
<g/>
,	,	kIx,
BRUCE	BRUCE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biological	Biological	k1gFnSc1
exuberance	exuberance	k1gFnSc1
:	:	kIx,
animal	animal	k1gMnSc1
homosexuality	homosexualita	k1gFnSc2
and	and	k?
natural	natural	k?
diversity	diversit	k1gInPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
st	st	kA
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Pressa	k1gFnPc2
xiii	xiii	k1gNnSc1
<g/>
,	,	kIx,
751	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
312	#num#	k4
<g/>
-	-	kIx~
<g/>
19239	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
312	#num#	k4
<g/>
-	-	kIx~
<g/>
19239	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
39281997	#num#	k4
↑	↑	k?
Berlin	berlina	k1gFnPc2
gay	gay	k1gMnSc1
penguins	penguinsa	k1gFnPc2
adopt	adopt	k1gMnSc1
abandoned	abandoned	k1gMnSc1
egg	egg	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
,	,	kIx,
<g/>
↑	↑	k?
CNN	CNN	kA
<g/>
,	,	kIx,
By	by	k9
Maggie	Maggie	k1gFnSc1
Hiufu	Hiuf	k1gInSc2
Wong	Wonga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Incest	incest	k1gInSc1
and	and	k?
affairs	affairs	k1gInSc1
of	of	k?
Japan	japan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
scandalous	scandalous	k1gInSc1
penguins	penguins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LITE	lit	k1gInSc5
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gay	gay	k1gMnSc1
ducks	ducks	k6eAd1
derail	derait	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
repopulation	repopulation	k1gInSc4
plan	plan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientific	k1gMnSc1
American	American	k1gMnSc1
Blog	Blog	k1gMnSc1
Network	network	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PINCEMY	PINCEMY	kA
<g/>
,	,	kIx,
Gwénaëlle	Gwénaëlle	k1gFnSc1
<g/>
;	;	kIx,
DOBSON	DOBSON	kA
<g/>
,	,	kIx,
F.	F.	kA
Stephen	Stephen	k1gInSc1
<g/>
;	;	kIx,
JOUVENTIN	JOUVENTIN	kA
<g/>
,	,	kIx,
Pierre	Pierr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homosexual	Homosexual	k1gInSc1
Mating	Mating	k1gInSc4
Displays	Displays	k1gInSc1
in	in	k?
Penguins	Penguins	k1gInSc1
<g/>
:	:	kIx,
Homosexual	Homosexual	k1gInSc1
Mating	Mating	k1gInSc1
Displays	Displays	k1gInSc1
in	in	k?
Penguins	Penguins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ethology	Etholog	k1gMnPc7
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
116	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1210	#num#	k4
<g/>
–	–	k?
<g/>
1216	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1439	.1439	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
310.201	310.201	k4
<g/>
0.01835	0.01835	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MANN	Mann	k1gMnSc1
<g/>
,	,	kIx,
Janet	Janet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Establishing	Establishing	k1gInSc1
trust	trust	k1gInSc4
<g/>
:	:	kIx,
Socio-sexual	Socio-sexual	k1gMnSc1
behaviour	behaviour	k1gMnSc1
and	and	k?
the	the	k?
development	development	k1gInSc1
of	of	k?
male-male	male-male	k6eAd1
bonds	bonds	k6eAd1
among	among	k1gInSc1
Indian	Indiana	k1gFnPc2
Ocean	Oceana	k1gFnPc2
bottlenose	bottlenose	k6eAd1
dolphins	dolphins	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FEKEOVÁ	FEKEOVÁ	kA
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problematika	problematika	k1gFnSc1
homosexuality	homosexualita	k1gFnSc2
ve	v	k7c6
výuce	výuka	k1gFnSc6
na	na	k7c6
českých	český	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
PedF	PedF	k1gMnPc1
MU	MU	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
VERNER	Verner	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
BAREŠ	Bareš	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
;	;	kIx,
VACHALA	VACHALA	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
starověkého	starověký	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
172	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
LANDGRÁFOVÁ	LANDGRÁFOVÁ	kA
<g/>
,	,	kIx,
Renata	Renata	k1gFnSc1
<g/>
;	;	kIx,
KREJČÍ	Krejčí	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
NAVRÁTILOVÁ	Navrátilová	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písně	píseň	k1gFnPc4
Zlaté	zlatý	k2eAgFnSc2d1
bohyně	bohyně	k1gFnSc2
<g/>
:	:	kIx,
staroegyptská	staroegyptský	k2eAgFnSc1d1
milostná	milostný	k2eAgFnSc1d1
poezie	poezie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Set	set	k1gInSc1
Out	Out	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
199	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86277	#num#	k4
<g/>
-	-	kIx~
<g/>
54	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VACHALA	VACHALA	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověsti	pověst	k1gFnSc2
a	a	k8xC
legendy	legenda	k1gFnSc2
faraónského	faraónský	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
podnikatelský	podnikatelský	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
100	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85267	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
63	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VACHALA	VACHALA	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moudrost	moudrost	k1gFnSc1
starého	starý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
podnikatelský	podnikatelský	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
175	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85267	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
24	#num#	k4
.	.	kIx.
↑	↑	k?
WILKINSON	WILKINSON	kA
<g/>
,	,	kIx,
Toby	Tob	k2eAgInPc4d1
H.	H.	kA
A.	A.	kA
Lidé	člověk	k1gMnPc1
starého	starý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Hana	Hana	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
a	a	k8xC
Renata	Renata	k1gFnSc1
Landgráfová	Landgráfový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
336	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1819	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
79	#num#	k4
<g/>
n.	n.	k?
1	#num#	k4
2	#num#	k4
Paul	Paul	k1gMnSc1
Halsall	Halsall	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Homosexual	Homosexual	k1gMnSc1
Eros	Eros	k1gMnSc1
in	in	k?
Early	earl	k1gMnPc4
Greece	Greece	k1gFnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Knowledgrush	Knowledgrush	k1gInSc1
<g/>
:	:	kIx,
Sacred	Sacred	k1gInSc1
Band	band	k1gInSc1
<g/>
.	.	kIx.
www.knowledgerush.com	www.knowledgerush.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
31	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Tatianus	Tatianus	k1gInSc1
<g/>
:	:	kIx,
Řekům	Řek	k1gMnPc3
<g/>
↑	↑	k?
Encyklopaedia	Encyklopaedium	k1gNnSc2
Britannica	Britannica	k1gFnSc1
<g/>
:	:	kIx,
Antinous	Antinous	k1gInSc1
<g/>
.	.	kIx.
www.1911encyclopedia.org	www.1911encyclopedia.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GLBTQ	GLBTQ	kA
<g/>
:	:	kIx,
Hadrian	Hadrian	k1gMnSc1
<g/>
.	.	kIx.
www.glbtq.com	www.glbtq.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Caravaggio	Caravaggio	k1gNnSc1
<g/>
:	:	kIx,
film	film	k1gInSc1
o	o	k7c6
malířově	malířův	k2eAgFnSc6d1
životní	životní	k2eAgFnSc6d1
lásce	láska	k1gFnSc6
<g/>
..	..	k?
www.moviemail-online.co.uk	www.moviemail-online.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Michelangelo	Michelangela	k1gFnSc5
<g/>
"	"	kIx"
<g/>
,	,	kIx,
The	The	k1gMnSc1
New	New	k1gMnSc2
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
<g/>
,	,	kIx,
Macropaedia	Macropaedium	k1gNnSc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
24	#num#	k4
<g/>
,	,	kIx,
page	page	k1gFnSc1
58	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Profesor	profesor	k1gMnSc1
Louis	Louis	k1gMnSc1
Crompton	Crompton	k1gInSc1
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
III	III	kA
<g/>
..	..	k?
www.glbtq.com	www.glbtq.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BRENIŠÍNOVÁ	BRENIŠÍNOVÁ	kA
<g/>
,	,	kIx,
Monika	Monika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Láska	láska	k1gFnSc1
<g/>
,	,	kIx,
zlomené	zlomený	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
i	i	k8xC
stahování	stahování	k1gNnSc1
z	z	k7c2
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sexuální	sexuální	k2eAgInSc4d1
život	život	k1gInSc4
Aztéků	Azték	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
a	a	k8xC
současnost	současnost	k1gFnSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XLII	XLII	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
418	#num#	k4
<g/>
-	-	kIx~
<g/>
5129	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Theodosiův	Theodosiův	k2eAgInSc4d1
kodex	kodex	k1gInSc4
9.8	9.8	k4
<g/>
.3	.3	k4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
When	When	k1gMnSc1
a	a	k8xC
man	man	k1gMnSc1
marries	marries	k1gMnSc1
and	and	k?
is	is	k?
about	about	k1gInSc1
to	ten	k3xDgNnSc4
offer	offer	k1gMnSc1
himself	himself	k1gMnSc1
to	ten	k3xDgNnSc4
men	men	k?
in	in	k?
womanly	womanl	k1gInPc1
fashion	fashion	k1gInSc1
(	(	kIx(
<g/>
quum	quum	k1gInSc1
vir	vir	k1gInSc1
nubit	nubit	k2eAgInSc1d1
in	in	k?
feminam	feminam	k1gInSc1
viris	viris	k1gInSc1
porrecturam	porrecturam	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
what	what	k2eAgInSc1d1
does	does	k1gInSc1
he	he	k0
wish	wish	k1gMnSc1
<g/>
,	,	kIx,
when	when	k1gMnSc1
sex	sex	k1gInSc4
has	hasit	k5eAaImRp2nS
lost	lost	k1gInSc1
all	all	k?
its	its	k?
significance	significance	k1gFnSc1
<g/>
;	;	kIx,
when	when	k1gInSc1
the	the	k?
crime	crimat	k5eAaPmIp3nS
is	is	k?
one	one	k?
which	which	k1gInSc1
it	it	k?
is	is	k?
not	nota	k1gFnPc2
profitable	profitable	k6eAd1
to	ten	k3xDgNnSc4
know	know	k?
<g/>
;	;	kIx,
when	when	k1gMnSc1
Venus	Venus	k1gMnSc1
is	is	k?
changed	changed	k1gMnSc1
to	ten	k3xDgNnSc1
another	anothra	k1gFnPc2
form	form	k1gInSc1
<g/>
;	;	kIx,
when	when	k1gInSc1
love	lov	k1gInSc5
is	is	k?
sought	sought	k2eAgInSc4d1
and	and	k?
not	nota	k1gFnPc2
found	found	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
We	We	k1gMnSc1
order	order	k1gMnSc1
the	the	k?
statutes	statutes	k1gMnSc1
to	ten	k3xDgNnSc4
arise	arise	k6eAd1
<g/>
,	,	kIx,
the	the	k?
laws	laws	k1gInSc1
to	ten	k3xDgNnSc1
be	be	k?
armed	armed	k1gInSc1
with	with	k1gInSc1
an	an	k?
avenging	avenging	k1gInSc1
sword	sword	k1gInSc1
<g/>
,	,	kIx,
that	that	k1gMnSc1
those	those	k1gFnSc2
infamous	infamous	k1gMnSc1
persons	personsa	k1gFnPc2
who	who	k?
are	ar	k1gInSc5
now	now	k?
<g/>
,	,	kIx,
or	or	k?
who	who	k?
hereafter	hereafter	k1gInSc1
may	may	k?
be	be	k?
<g/>
,	,	kIx,
guilty	guilt	k1gInPc1
may	may	k?
be	be	k?
subjected	subjected	k1gInSc1
to	ten	k3xDgNnSc1
exquisite	exquisit	k1gInSc5
punishment	punishment	k1gInSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Theodosiův	Theodosiův	k2eAgInSc4d1
kodex	kodex	k1gInSc4
9.7	9.7	k4
<g/>
.6	.6	k4
<g/>
:	:	kIx,
Omnes	Omnes	k1gInSc4
<g/>
,	,	kIx,
quibus	quibus	k1gInSc4
flagitii	flagitie	k1gFnSc3
usus	usus	k1gInSc1
est	est	k?
<g/>
,	,	kIx,
virile	virile	k6eAd1
corpus	corpus	k1gInSc1
muliebriter	muliebritra	k1gFnPc2
constitutum	constitutum	k1gNnSc1
alieni	aliet	k5eAaPmNgMnP,k5eAaBmNgMnP,k5eAaImNgMnP
sexus	sexus	k1gInSc1
damnare	damnar	k1gMnSc5
patientia	patientium	k1gNnPc1
(	(	kIx(
<g/>
nihil	nihit	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
enim	enim	k1gInSc1
discretum	discretum	k1gNnSc1
videntur	videntura	k1gFnPc2
habere	habrat	k5eAaPmIp3nS
cum	cum	k?
feminis	feminis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
huius	huius	k1gMnSc1
modi	mod	k1gFnSc2
scelus	scelus	k1gMnSc1
spectante	spectant	k1gMnSc5
populo	popula	k1gFnSc5
flammis	flammis	k1gFnSc2
vindicibus	vindicibus	k1gMnSc1
expiabunt	expiabunt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pp	Pp	k1gFnSc1
<g/>
.	.	kIx.
in	in	k?
foro	foro	k1gMnSc1
Traiani	Traiaň	k1gFnSc3
VIII	VIII	kA
<g/>
.	.	kIx.
id	idy	k1gFnPc2
<g/>
.	.	kIx.
aug	aug	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Valentiniano	Valentiniana	k1gFnSc5
a.	a.	k?
IV	IV	kA
<g/>
.	.	kIx.
et	et	k?
Neoterio	Neoterio	k6eAd1
coss	coss	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haec	Haec	k1gFnSc1
lex	lex	k?
interpretatione	interpretation	k1gInSc5
non	non	k?
indiget	indiget	k1gInSc1
<g/>
..	..	k?
<g/>
↑	↑	k?
Justinian	Justinian	k1gInSc1
Novels	Novels	k1gInSc1
77	#num#	k4
<g/>
,	,	kIx,
144	#num#	k4
<g/>
↑	↑	k?
The	The	k1gMnSc1
Law	Law	k1gMnSc1
in	in	k?
England	England	k1gInSc1
<g/>
,	,	kIx,
1290	#num#	k4
<g/>
–	–	k?
<g/>
18851	#num#	k4
2	#num#	k4
Sexual	Sexual	k1gInSc1
Orientation	Orientation	k1gInSc4
and	and	k?
Mental	Mental	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
:	:	kIx,
Examining	Examining	k1gInSc1
Identity	identita	k1gFnSc2
and	and	k?
Development	Development	k1gMnSc1
in	in	k?
Lesbian	Lesbian	k1gMnSc1
<g/>
,	,	kIx,
Gay	gay	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Bisexual	Bisexual	k1gMnSc1
People	People	k1gMnSc1
by	by	k9
Allen	Allen	k1gMnSc1
M.	M.	kA
Omoto	Omoto	k1gNnSc1
and	and	k?
Howard	Howard	k1gMnSc1
S.	S.	kA
Kurtzman	Kurtzman	k1gMnSc1
(	(	kIx(
<g/>
Eds	Eds	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Gonsiorek	Gonsiorka	k1gFnPc2
<g/>
,	,	kIx,
J.	J.	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
empirical	empiricat	k5eAaPmAgMnS
basis	basis	k?
for	forum	k1gNnPc2
the	the	k?
demise	demise	k1gFnSc2
of	of	k?
the	the	k?
illness	illness	k1gInSc1
model	model	k1gInSc1
of	of	k?
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnPc2
J.	J.	kA
C.	C.	kA
Gonsiorek	Gonsiorek	k1gInSc1
&	&	k?
J.	J.	kA
D.	D.	kA
Weinrich	Weinrich	k1gMnSc1
(	(	kIx(
<g/>
Eds	Eds	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Homosexuality	homosexualita	k1gFnSc2
<g/>
:	:	kIx,
Research	Research	k1gInSc1
implications	implicationsa	k1gFnPc2
for	forum	k1gNnPc2
public	publicum	k1gNnPc2
policy	polic	k2eAgFnPc1d1
(	(	kIx(
<g/>
pp	pp	k?
<g/>
.	.	kIx.
115	#num#	k4
<g/>
-	-	kIx~
<g/>
136	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Newbury	Newbura	k1gFnPc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
CA	ca	kA
<g/>
:	:	kIx,
Sage	Sage	k1gInSc1
<g/>
.1	.1	k4
2	#num#	k4
Facts	Factsa	k1gFnPc2
About	About	k1gMnSc1
Homosexuality	homosexualita	k1gFnSc2
and	and	k?
Mental	Mental	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
1	#num#	k4
2	#num#	k4
American	Americana	k1gFnPc2
Psychiatric	Psychiatrice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Homosexuality	homosexualita	k1gFnPc1
and	and	k?
Civil	civil	k1gMnSc1
Rights	Rightsa	k1gFnPc2
POSITION	POSITION	kA
STATEMENT	STATEMENT	kA
<g/>
↑	↑	k?
Chinese	Chinese	k1gFnSc1
psychiatrists	psychiatrists	k6eAd1
debate	debat	k1gInSc5
meaning	meaning	k1gInSc4
of	of	k?
sex	sex	k1gInSc1
orientation	orientation	k1gInSc1
<g/>
↑	↑	k?
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
Times	Times	k1gInSc4
Chinese	Chinese	k1gFnSc2
Psychiatrists	Psychiatristsa	k1gFnPc2
Decide	Decid	k1gInSc5
Homosexuality	homosexualita	k1gFnPc1
Isn	Isn	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
’	’	k?
<g/>
t	t	k?
Abnormal	Abnormal	k1gInSc1
<g/>
↑	↑	k?
American	American	k1gInSc1
Psychological	Psychological	k1gMnSc1
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Resolution	Resolution	k1gInSc1
on	on	k3xPp3gMnSc1
Appropriate	Appropriat	k1gInSc5
Affirmative	Affirmativ	k1gInSc5
Responses	Responses	k1gMnSc1
to	ten	k3xDgNnSc1
Sexual	Sexual	k1gInSc4
Orientation	Orientation	k1gInSc1
Distress	Distress	k1gInSc1
and	and	k?
Change	change	k1gFnSc2
Efforts	Effortsa	k1gFnPc2
(	(	kIx(
<g/>
Rezoluce	rezoluce	k1gFnSc1
o	o	k7c6
vhodných	vhodný	k2eAgFnPc6d1
afirmativních	afirmativní	k2eAgFnPc6d1
reakcích	reakce	k1gFnPc6
na	na	k7c4
distres	distres	k1gInSc4
u	u	k7c2
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
a	a	k8xC
snahy	snaha	k1gFnSc2
o	o	k7c4
změnu	změna	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5.8	5.8	k4
<g/>
.2009	.2009	k4
<g/>
,	,	kIx,
schválila	schválit	k5eAaPmAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
Rada	rada	k1gFnSc1
reprezentantů	reprezentant	k1gInPc2
APA	APA	kA
na	na	k7c6
výročním	výroční	k2eAgNnSc6d1
zasedání	zasedání	k1gNnSc6
↑	↑	k?
Resolution	Resolution	k1gInSc1
on	on	k3xPp3gInSc1
Marriage	Marriage	k1gFnSc4
Equality	Equalita	k1gFnSc2
for	forum	k1gNnPc2
Same-Sex	Same-Sex	k1gInSc4
Couples	Couples	k1gInSc1
(	(	kIx(
<g/>
Rezoluce	rezoluce	k1gFnSc1
o	o	k7c4
manželské	manželský	k2eAgFnPc4d1
rovnosti	rovnost	k1gFnPc4
pro	pro	k7c4
stejnopohlavní	stejnopohlavní	k2eAgInPc4d1
páry	pár	k1gInPc4
<g/>
;	;	kIx,
jednomyslně	jednomyslně	k6eAd1
schválila	schválit	k5eAaPmAgFnS
157	#num#	k4
hlasy	hlas	k1gInPc1
Rada	Rada	k1gMnSc1
reprezentantů	reprezentant	k1gMnPc2
APA	APA	kA
na	na	k7c6
výročním	výroční	k2eAgNnSc6d1
zasedání	zasedání	k1gNnSc6
3	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
5.8	5.8	k4
<g/>
.2011	.2011	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Current	Current	k1gInSc1
Directions	Directions	k1gInSc1
in	in	k?
Psychological	Psychological	k1gFnSc1
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
19	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herek	herka	k1gFnPc2
<g/>
,	,	kIx,
G.	G.	kA
<g/>
M.	M.	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
psychology	psycholog	k1gMnPc4
of	of	k?
sexual	sexual	k1gInSc1
prejudice	prejudice	k1gFnSc1
<g/>
..	..	k?
<g/>
↑	↑	k?
Marianna	Marianen	k2eAgFnSc1d1
Šípošová	Šípošová	k1gFnSc1
<g/>
:	:	kIx,
Homofóbia	Homofóbia	k1gFnSc1
a	a	k8xC
jej	on	k3xPp3gInSc4
dopad	dopad	k1gInSc4
na	na	k7c4
homosexuálnu	homosexuálna	k1gFnSc4
menšinu	menšina	k1gFnSc4
<g/>
,	,	kIx,
FiF	FiF	k1gFnPc4
UK	UK	kA
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
1997	#num#	k4
<g/>
↑	↑	k?
Jaroslava	Jaroslava	k1gFnSc1
Talandová	Talandový	k2eAgFnSc1d1
<g/>
:	:	kIx,
Sociální	sociální	k2eAgFnSc1d1
postavení	postavení	k1gNnSc4
lesbických	lesbický	k2eAgFnPc2d1
žen	žena	k1gFnPc2
a	a	k8xC
alternativní	alternativní	k2eAgInPc4d1
rodinné	rodinný	k2eAgInPc4d1
modely	model	k1gInPc4
v	v	k7c6
kontextu	kontext	k1gInSc6
heterosexuální	heterosexuální	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
L-klub	L-klub	k1gInSc1
Lambda	lambda	k1gNnSc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Pederasty	pederast	k1gMnPc4
<g/>
"	"	kIx"
Decriminalized	Decriminalized	k1gInSc1
in	in	k?
France	Franc	k1gMnSc2
<g/>
↑	↑	k?
Robert	Robert	k1gMnSc1
Alan	Alan	k1gMnSc1
Brookey	Brookea	k1gFnSc2
<g/>
:	:	kIx,
Reinventing	Reinventing	k1gInSc1
the	the	k?
Male	male	k6eAd1
Homosexual	Homosexual	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Rhetoric	Rhetoric	k1gMnSc1
and	and	k?
Power	Power	k1gMnSc1
of	of	k?
the	the	k?
Gay	gay	k1gMnSc1
Gene	gen	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indiana	Indiana	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomington	Bloomington	k1gInSc1
&	&	k?
Indianapolis	Indianapolis	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
43	#num#	k4
<g/>
↑	↑	k?
Craig	Craig	k1gMnSc1
A.	A.	kA
Rimmerman	Rimmerman	k1gMnSc1
<g/>
:	:	kIx,
From	From	k1gMnSc1
Identity	identita	k1gFnSc2
to	ten	k3xDgNnSc4
Politics	Politics	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Lesbian	Lesbian	k1gMnSc1
and	and	k?
Gay	gay	k1gMnSc1
Movements	Movementsa	k1gFnPc2
in	in	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Temple	templ	k1gInSc5
University	universita	k1gFnPc4
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
85	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Donn	donna	k1gFnPc2
Teal	Teal	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Gay	gay	k1gMnSc1
Militants	Militantsa	k1gFnPc2
–	–	k?
How	How	k1gMnSc1
Gay	gay	k1gMnSc1
Liberation	Liberation	k1gInSc4
Began	Began	k1gMnSc1
in	in	k?
America	America	k1gMnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
272	#num#	k4
<g/>
-	-	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
273	#num#	k4
<g/>
↑	↑	k?
Kay	Kay	k1gMnSc1
Tobin	Tobin	k1gMnSc1
<g/>
,	,	kIx,
Randy	rand	k1gInPc1
Wicker	Wicker	k1gInSc1
<g/>
:	:	kIx,
Gay	gay	k1gMnSc1
Crusaders	Crusadersa	k1gFnPc2
(	(	kIx(
<g/>
Homosexuality	homosexualita	k1gFnPc1
:	:	kIx,
Lesbians	Lesbians	k1gInSc1
and	and	k?
Gay	gay	k1gMnSc1
Men	Men	k1gFnSc2
in	in	k?
Society	societa	k1gFnPc1
<g/>
,	,	kIx,
History	Histor	k1gInPc1
and	and	k?
Literature	Literatur	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Arno	Arno	k6eAd1
Pr	pr	k0
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
1975	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
405	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7374	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
216	#num#	k4
<g/>
↑	↑	k?
Kay	Kay	k1gMnSc1
Tobin	Tobin	k1gMnSc1
<g/>
,	,	kIx,
Randy	rand	k1gInPc1
Wicker	Wicker	k1gInSc1
<g/>
:	:	kIx,
Gay	gay	k1gMnSc1
Crusaders	Crusadersa	k1gFnPc2
(	(	kIx(
<g/>
Homosexuality	homosexualita	k1gFnPc1
:	:	kIx,
Lesbians	Lesbians	k1gInSc1
and	and	k?
Gay	gay	k1gMnSc1
Men	Men	k1gFnSc2
in	in	k?
Society	societa	k1gFnPc1
<g/>
,	,	kIx,
History	Histor	k1gInPc1
and	and	k?
Literature	Literatur	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Arno	Arno	k6eAd1
Pr	pr	k0
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
září	září	k1gNnSc2
1975	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7374	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
130	#num#	k4
<g/>
–	–	k?
<g/>
131	#num#	k4
<g/>
↑	↑	k?
Společná	společný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
přátel	přítel	k1gMnPc2
soudu	soud	k1gInSc2
na	na	k7c4
podporu	podpora	k1gFnSc4
strany	strana	k1gFnSc2
odvolávající	odvolávající	k2eAgFnSc1d1
se	se	k3xPyFc4
k	k	k7c3
Nejvyššímu	vysoký	k2eAgInSc3d3
soudu	soud	k1gInSc3
<g />
.	.	kIx.
</s>
<s hack="1">
státu	stát	k1gInSc2
Maine	Main	k1gInSc5
ve	v	k7c6
věci	věc	k1gFnSc6
adopce	adopce	k1gFnSc2
R.	R.	kA
A.	A.	kA
a	a	k8xC
M.	M.	kA
A.	A.	kA
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
organizací	organizace	k1gFnPc2
American	Americany	k1gInPc2
Psychological	Psychological	k1gFnSc1
Association	Association	k1gInSc1
<g/>
;	;	kIx,
Maine	Main	k1gMnSc5
Psychological	Psychological	k1gMnSc5
Association	Association	k1gInSc1
<g/>
,	,	kIx,
National	National	k1gFnSc1
Association	Association	k1gInSc1
of	of	k?
Social	Social	k1gInSc1
Workers	Workers	k1gInSc1
<g/>
,	,	kIx,
National	National	k1gFnSc1
Association	Association	k1gInSc1
of	of	k?
Psychiatric	Psychiatrice	k1gInPc2
Physicians	Physicians	k1gInSc4
<g/>
,	,	kIx,
Child	Child	k1gInSc4
'	'	kIx"
<g/>
Utelfare	Utelfar	k1gMnSc5
League	Leaguus	k1gMnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
of	of	k?
America	America	k1gMnSc1
<g/>
,	,	kIx,
Maine	Main	k1gInSc5
Childern	Childerno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Alliance	Allianka	k1gFnSc3
<g/>
,	,	kIx,
Maine	Main	k1gMnSc5
Medical	Medical	k1gMnSc5
Association	Association	k1gInSc1
<g/>
,	,	kIx,
American	American	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Pediatrics	Pediatrics	k1gInSc1
<g/>
,	,	kIx,
Maine	Main	k1gInSc5
Chapter	Chapter	k1gMnSc1
<g/>
,	,	kIx,
Evan	Evan	k1gMnSc1
B.	B.	kA
Donaldson	Donaldson	k1gInSc1
Adoption	Adoption	k1gInSc1
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
Kids	Kids	k1gInSc1
First	First	k1gInSc1
<g/>
,	,	kIx,
Community	Communit	k1gInPc1
Counseling	Counseling	k1gInSc1
Center	centrum	k1gNnPc2
online	onlinout	k5eAaPmIp3nS
na	na	k7c6
stránkách	stránka	k1gFnPc6
Americké	americký	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
↑	↑	k?
Charlotte	Charlott	k1gInSc5
J.	J.	kA
Patterson	Patterson	k1gMnSc1
<g/>
,	,	kIx,
PhD	PhD	k1gMnSc1
<g/>
;	;	kIx,
American	American	k1gMnSc1
Psychological	Psychological	k1gFnSc2
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Lesbian	Lesbian	k1gMnSc1
&	&	k?
Gay	gay	k1gMnSc1
Parents	Parents	k1gInSc1
<g/>
↑	↑	k?
Canadian	Canadian	k1gInSc1
Psychological	Psychological	k1gMnSc1
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Brief	Brief	k1gMnSc1
presented	presented	k1gMnSc1
to	ten	k3xDgNnSc4
the	the	k?
Legislative	Legislativ	k1gInSc5
House	house	k1gNnSc4
of	of	k?
Commons	Commons	k1gInSc1
Committee	Committe	k1gMnSc2
on	on	k3xPp3gMnSc1
Bill	Bill	k1gMnSc1
C38	C38	k1gMnSc1
By	by	k9
the	the	k?
Canadian	Canadian	k1gInSc1
Psychological	Psychological	k1gMnSc1
Association	Association	k1gInSc1
June	jun	k1gMnSc5
2	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
13	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Požehnání	požehnání	k1gNnSc2
homosexuálnímu	homosexuální	k2eAgInSc3d1
páru	pár	k1gInSc3
v	v	k7c6
kostele	kostel	k1gInSc6
U	u	k7c2
Jákobova	Jákobův	k2eAgInSc2d1
žebříku	žebřík	k1gInSc2
<g/>
.	.	kIx.
logos	logos	k1gInSc1
<g/>
.	.	kIx.
<g/>
gl	gl	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
29	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sexuelle	Sexuelle	k1gInSc1
Perversionen	Perversionen	k2eAgMnSc1d1
sind	sind	k1gMnSc1
keine	keinout	k5eAaPmIp3nS,k5eAaImIp3nS
Menschenrechte	Menschenrecht	k1gInSc5
Archivováno	archivován	k2eAgNnSc4d1
4	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
rozhovor	rozhovor	k1gInSc4
s	s	k7c7
kardinálem	kardinál	k1gMnSc7
Janisem	Janis	k1gInSc7
Pujatsem	Pujats	k1gInSc7
<g/>
,	,	kIx,
Cardinalrating	Cardinalrating	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
20091	#num#	k4
2	#num#	k4
Here	Here	k1gFnPc2
are	ar	k1gInSc5
the	the	k?
10	#num#	k4
countries	countries	k1gInSc1
where	wher	k1gInSc5
homosexuality	homosexualita	k1gFnSc2
may	may	k?
be	be	k?
punished	punished	k1gInSc1
by	by	kYmCp3nS
death	death	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.eatg.org/eatg/Global-HIV-News/HIV-STIs/Sexually-transmitted-infection-rates-in-England-drop-for-the-first-time-in-a-decade-but-continue-to-rise-in-gay-men%5B%5D	http://www.eatg.org/eatg/Global-HIV-News/HIV-STIs/Sexually-transmitted-infection-rates-in-England-drop-for-the-first-time-in-a-decade-but-continue-to-rise-in-gay-men%5B%5D	k4
<g/>
↑	↑	k?
http://www.cdc.gov/ncidod/diseases/hepatitis/msm/hav_msm_fact.htm	http://www.cdc.gov/ncidod/diseases/hepatitis/msm/hav_msm_fact.htm	k1gMnSc1
<g/>
↑	↑	k?
HIV	HIV	kA
and	and	k?
AIDS	AIDS	kA
among	among	k1gMnSc1
Gay	gay	k1gMnSc1
and	and	k?
Bisexual	Bisexual	k1gMnSc1
Men	Men	k1gMnSc1
<g/>
.	.	kIx.
www.cdc.gov	www.cdc.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Tomáš	Tomáš	k1gMnSc1
Belica	Belica	k1gMnSc1
<g/>
,	,	kIx,
Máte	mít	k5eAaImIp2nP
kapavku	kapavka	k1gFnSc4
<g/>
,	,	kIx,
syfilis	syfilis	k1gFnSc1
<g/>
...	...	k?
a	a	k8xC
HIV	HIV	kA
<g/>
,	,	kIx,
deník	deník	k1gInSc4
Metro	metro	k1gNnSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
10	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
[	[	kIx(
<g/>
Homosexuals	Homosexuals	k1gInSc1
<g/>
,	,	kIx,
Lesbians	Lesbians	k1gInSc1
and	and	k?
Bisexuals	Bisexuals	k1gInSc1
“	“	k?
<g/>
More	mor	k1gInSc5
Likely	Likela	k1gFnPc1
to	ten	k3xDgNnSc1
be	be	k?
Mentally	Mentalla	k1gMnSc2
Ill	Ill	k1gMnSc2
<g/>
”	”	k?
<g/>
—	—	k?
<g/>
Cambridge	Cambridge	k1gFnSc2
University	universita	k1gFnSc2
Study	stud	k1gInPc4
http://newobserveronline.com/homosexuals-lesbians-and-bisexuals-more-likely-to-be-mentally-ill-cambridge-university-study/	http://newobserveronline.com/homosexuals-lesbians-and-bisexuals-more-likely-to-be-mentally-ill-cambridge-university-study/	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
The	The	k1gMnSc1
New	New	k1gMnSc1
Observer	Observer	k1gMnSc1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
↑	↑	k?
Marc	Marc	k1gFnSc4
N.	N.	kA
Elliott	Elliott	k2eAgMnSc1d1
PhD	PhD	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Sexual	Sexual	k1gMnSc1
Minorities	Minorities	k1gMnSc1
in	in	k?
England	England	k1gInSc1
Have	Have	k1gNnSc1
Poorer	Poorer	k1gInSc1
Health	Health	k1gMnSc1
and	and	k?
Worse	Worse	k1gFnSc2
Health	Health	k1gMnSc1
Care	car	k1gMnSc5
Experiences	Experiences	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
National	National	k1gMnSc2
Survey	Survea	k1gMnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
↑	↑	k?
Vedci	Vedce	k1gFnSc6
z	z	k7c2
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Homosexuáli	Homosexuál	k1gMnPc1
<g/>
,	,	kIx,
lesby	lesba	k1gFnPc1
a	a	k8xC
bisexuáli	bisexuál	k1gMnPc1
sú	sú	k?
vo	vo	k?
väčšej	väčšet	k5eAaPmRp2nS,k5eAaImRp2nS
miere	mier	k1gInSc5
mentálne	mentálnout	k5eAaPmIp3nS,k5eAaImIp3nS
chorí	chorum	k1gNnPc2
<g/>
“	“	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Hlavné	Hlavný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
TASR	TASR	kA
<g/>
↑	↑	k?
PhDr.	PhDr.	kA
Josef	Josef	k1gMnSc1
Bartoň	Bartoň	k1gMnSc1
<g/>
,	,	kIx,
ThD	ThD	k1gMnSc1
<g/>
.	.	kIx.
z	z	k7c2
katedry	katedra	k1gFnSc2
biblických	biblický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
KTF	KTF	kA
Praha	Praha	k1gFnSc1
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
erotice	erotika	k1gFnSc6
v	v	k7c6
řecké	řecký	k2eAgFnSc6d1
poezii	poezie	k1gFnSc6
<g/>
.	.	kIx.
www.christnet.cz	www.christnet.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Úsilí	úsilí	k1gNnSc1
o	o	k7c6
odtrestnění	odtrestnění	k1gNnSc6
homosexuality	homosexualita	k1gFnSc2
za	za	k7c2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Seidl	Seidl	k1gMnSc1
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Where	Wher	k1gInSc5
are	ar	k1gInSc5
the	the	k?
most	most	k1gInSc1
dangerous	dangerous	k1gMnSc1
places	places	k1gMnSc1
to	ten	k3xDgNnSc4
be	be	k?
gay	gay	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Decision	Decision	k1gInSc1
from	from	k1gMnSc1
the	the	k?
Iowa	Iowa	k1gMnSc1
District	District	k1gMnSc1
Court	Court	k1gInSc1
<g/>
:	:	kIx,
Varnum	Varnum	k1gInSc1
v.	v.	k?
Brien	Brien	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Iowa	Iowum	k1gNnPc1
Supreme	Suprem	k1gInSc5
Court	Court	k1gInSc1
Rules	Rules	k1gInSc1
in	in	k?
Marriage	Marriage	k1gInSc1
Case	Case	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
News	News	k1gInSc1
Release	Releasa	k1gFnSc3
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
Davis	Davis	k1gFnSc2
<g/>
↑	↑	k?
https://www.usoud.cz/fileadmin/user_upload/Tiskova_mluvci/Publikovane_nalezy/2016/Pl._US_7_15_vcetne_disentu_na_web.pdf	https://www.usoud.cz/fileadmin/user_upload/Tiskova_mluvci/Publikovane_nalezy/2016/Pl._US_7_15_vcetne_disentu_na_web.pdf	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Pavlína	Pavlína	k1gFnSc1
Janošová	Janošová	k1gFnSc1
<g/>
:	:	kIx,
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
názorech	názor	k1gInPc6
současné	současný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
219	#num#	k4
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7184-954-5	80-7184-954-5	k4
</s>
<s>
Michal	Michal	k1gMnSc1
Černoušek	černoušek	k1gMnSc1
<g/>
,	,	kIx,
Šílenství	šílenství	k1gNnSc1
v	v	k7c6
zrcadle	zrcadlo	k1gNnSc6
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1994	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7169-086-4	80-7169-086-4	k4
</s>
<s>
HERGEMÖLLER	HERGEMÖLLER	kA
<g/>
,	,	kIx,
Bernd-Ulrich	Bernd-Ulrich	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Einführung	Einführung	k1gMnSc1
in	in	k?
die	die	k?
Historiographie	Historiographie	k1gFnSc2
der	drát	k5eAaImRp2nS
Homosexualitäten	Homosexualitäten	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tübingen	Tübingen	k1gInSc1
<g/>
:	:	kIx,
Ed	Ed	k1gMnSc1
<g/>
.	.	kIx.
discord	discord	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Historische	Historische	k1gInSc1
Einführungen	Einführungen	k1gInSc1
<g/>
;	;	kIx,
sv.	sv.	kA
5	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
89295	#num#	k4
<g/>
-	-	kIx~
<g/>
678	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
téma	téma	k1gNnSc1
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
<g/>
,	,	kIx,
Souvislosti	souvislost	k1gFnSc6
–	–	k?
revue	revue	k1gFnSc2
pro	pro	k7c4
literaturu	literatura	k1gFnSc4
a	a	k8xC
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
č.	č.	k?
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
ISSN	ISSN	kA
0	#num#	k4
<g/>
862	#num#	k4
<g/>
-	-	kIx~
<g/>
6928	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Wolfgang	Wolfgang	k1gMnSc1
Popp	Popp	k1gMnSc1
<g/>
:	:	kIx,
Epos	epos	k1gInSc1
o	o	k7c6
Gilgamešovi	Gilgameš	k1gMnSc6
jako	jako	k8xS,k8xC
první	první	k4xOgInSc4
text	text	k1gInSc4
o	o	k7c6
lásce	láska	k1gFnSc6
muže	muž	k1gMnSc2
k	k	k7c3
muži	muž	k1gMnSc3
<g/>
,	,	kIx,
pův	pův	k?
<g/>
.	.	kIx.
text	text	k1gInSc1
1992	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
</s>
<s>
Maximos	Maximos	k1gInSc1
z	z	k7c2
Tyru	Tyrus	k1gInSc2
<g/>
:	:	kIx,
O	o	k7c6
erótice	erótika	k1gFnSc6
<g/>
,	,	kIx,
původní	původní	k2eAgInSc1d1
text	text	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
z	z	k7c2
německé	německý	k2eAgFnSc2d1
verze	verze	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Filip	Filip	k1gMnSc1
Horáček	Horáček	k1gMnSc1
</s>
<s>
Svátost	svátost	k1gFnSc1
sbratření	sbratření	k1gNnSc2
(	(	kIx(
<g/>
Molitva	Molitvo	k1gNnSc2
na	na	k7c6
bratrotvorenie	bratrotvorenie	k1gFnSc1
–	–	k?
Euché	Euchý	k2eAgNnSc4d1
eis	eis	k1gNnSc4
adelfopoiésin	adelfopoiésina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
staroslověnský	staroslověnský	k2eAgInSc1d1
liturgický	liturgický	k2eAgInSc1d1
text	text	k1gInSc1
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
C.	C.	kA
Putna	putna	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Boswell	Boswell	k1gMnSc1
<g/>
:	:	kIx,
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
pův	pův	k?
<g/>
.	.	kIx.
text	text	k1gInSc1
1980	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
C.	C.	kA
Putna	putna	k1gFnSc1
</s>
<s>
Ramón	Ramón	k1gMnSc1
Llull	Llull	k1gMnSc1
(	(	kIx(
<g/>
Raymundus	Raymundus	k1gMnSc1
Lullus	Lullus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Kniha	kniha	k1gFnSc1
o	o	k7c6
Příteli	přítel	k1gMnSc6
a	a	k8xC
Miláčku	miláček	k1gMnSc6
<g/>
,	,	kIx,
starokatalánský	starokatalánský	k2eAgInSc1d1
text	text	k1gInSc1
z	z	k7c2
přelomu	přelom	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
češtiny	čeština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1905	#num#	k4
Sigismund	Sigismund	k1gMnSc1
Bouška	Bouška	k1gMnSc1
</s>
<s>
Roar	Roar	k1gInSc1
Lishaugen	Lishaugen	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Ta	ten	k3xDgFnSc1
pravá	pravý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
naše	náš	k3xOp1gFnSc1
literatura	literatura	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Karásek	Karásek	k1gMnSc1
ze	z	k7c2
Lvovic	Lvovice	k1gFnPc2
jako	jako	k8xS,k8xC
zakladatel	zakladatel	k1gMnSc1
české	český	k2eAgFnSc2d1
homosexuální	homosexuální	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Franz	Franz	k1gMnSc1
Schindler	Schindler	k1gMnSc1
<g/>
:	:	kIx,
František	František	k1gMnSc1
Čeřovský	Čeřovský	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
boje	boj	k1gInSc2
za	za	k7c4
dekriminalizaci	dekriminalizace	k1gFnSc4
homosexuálů	homosexuál	k1gMnPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
<s>
Věra	Věra	k1gFnSc1
Sokolová	Sokolová	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Moje	můj	k3xOp1gNnSc1
tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Apriles	Apriles	k1gMnSc1
2003	#num#	k4
a	a	k8xC
politika	politika	k1gFnSc1
ženského	ženský	k2eAgNnSc2d1
<g/>
/	/	kIx~
<g/>
lesbického	lesbický	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Kolářová	Kolářová	k1gFnSc1
<g/>
:	:	kIx,
Hledání	hledání	k1gNnSc1
lesbického	lesbický	k2eAgInSc2d1
hlasu	hlas	k1gInSc2
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Kolářová	Kolářová	k1gFnSc1
<g/>
:	:	kIx,
Celý	celý	k2eAgInSc1d1
život	život	k1gInSc1
jsem	být	k5eAaImIp1nS
čekala	čekat	k5eAaImAgFnS
na	na	k7c4
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Jeanette	Jeanett	k1gInSc5
Winterson	Winterson	k1gMnSc1
o	o	k7c6
lesbismu	lesbismus	k1gInSc6
<g/>
,	,	kIx,
genderu	gender	k1gInSc6
a	a	k8xC
kyberprostoru	kyberprostor	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Dominique	Dominiquat	k5eAaPmIp3nS
Fernandez	Fernandez	k1gInSc4
<g/>
:	:	kIx,
Obrazy	obraz	k1gInPc4
z	z	k7c2
dějin	dějiny	k1gFnPc2
homosexuality	homosexualita	k1gFnSc2
ve	v	k7c6
filmu	film	k1gInSc6
(	(	kIx(
<g/>
přeložil	přeložit	k5eAaPmAgMnS
M.	M.	kA
C.	C.	kA
Putna	putna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
C.	C.	kA
Putna	putna	k1gFnSc1
<g/>
:	:	kIx,
Veselá	veselý	k2eAgFnSc1d1
věda	věda	k1gFnSc1
aneb	aneb	k?
Alternativy	alternativa	k1gFnSc2
uvnitř	uvnitř	k7c2
alternativy	alternativa	k1gFnSc2
(	(	kIx(
<g/>
kritický	kritický	k2eAgInSc1d1
nástin	nástin	k1gInSc1
soudobé	soudobý	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
rozpravy	rozprava	k1gFnSc2
o	o	k7c6
homosexualitě	homosexualita	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
OMOTO	OMOTO	kA
<g/>
,	,	kIx,
Allen	Allen	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
KURTZMAN	KURTZMAN	kA
<g/>
,	,	kIx,
Howard	Howard	k1gInSc1
S.	S.	kA
Sexual	Sexual	k1gInSc1
Orientation	Orientation	k1gInSc1
and	and	k?
Mental	Mental	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
:	:	kIx,
Examining	Examining	k1gInSc1
Identity	identita	k1gFnSc2
and	and	k?
Development	Development	k1gMnSc1
in	in	k?
Lesbian	Lesbian	k1gMnSc1
<g/>
,	,	kIx,
Gay	gay	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Bisexual	Bisexual	k1gMnSc1
People	People	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
:	:	kIx,
American	American	k1gMnSc1
Psychological	Psychological	k1gFnSc2
Association	Association	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1591472322	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
323	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GOLDBERG	GOLDBERG	kA
<g/>
,	,	kIx,
Abbie	Abbie	k1gFnSc2
E.	E.	kA
Lesbian	Lesbian	k1gMnSc1
and	and	k?
Gay	gay	k1gMnSc1
Parents	Parents	k1gInSc1
and	and	k?
Their	Their	k1gInSc1
Children	Childrna	k1gFnPc2
<g/>
:	:	kIx,
Research	Research	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Family	Famil	k1gMnPc4
Life	Lif	k1gFnSc2
Cycle	Cycle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
:	:	kIx,
American	American	k1gMnSc1
Psychological	Psychological	k1gFnSc2
Association	Association	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1433805363	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
233	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Léčení	léčení	k1gNnSc1
homosexuality	homosexualita	k1gFnSc2
</s>
<s>
Homofobie	Homofobie	k1gFnSc1
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgInSc1d1
sexuální	sexuální	k2eAgInSc1d1
styk	styk	k1gInSc1
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
</s>
<s>
Heterosexualita	Heterosexualita	k1gFnSc1
</s>
<s>
Against	Against	k1gFnSc1
Nature	Natur	k1gInSc5
<g/>
?	?	kIx.
</s>
<s>
Straight	Straight	k1gInSc1
ally	alla	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
homosexualita	homosexualita	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Uranismus	Uranismus	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Téma	téma	k1gNnSc1
homosexualita	homosexualita	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
homosexualita	homosexualita	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1
v	v	k7c6
Sociologické	sociologický	k2eAgFnSc6d1
encyklopedii	encyklopedie	k1gFnSc6
Sociologického	sociologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
web	web	k1gInSc1
o	o	k7c6
homosexualitě	homosexualita	k1gFnSc6
</s>
<s>
Petr	Petr	k1gMnSc1
Weiss	Weiss	k1gMnSc1
<g/>
:	:	kIx,
Homosexualita	homosexualita	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
Společnosti	společnost	k1gFnSc2
pro	pro	k7c4
plánování	plánování	k1gNnSc4
rodiny	rodina	k1gFnSc2
a	a	k8xC
sexuální	sexuální	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
</s>
<s>
Radim	Radim	k1gMnSc1
Uzel	uzel	k1gInSc1
<g/>
:	:	kIx,
Tmářství	tmářství	k1gNnSc1
kolem	kolem	k7c2
homosexuality	homosexualita	k1gFnSc2
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
</s>
<s>
Antonín	Antonín	k1gMnSc1
Brzek	Brzek	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
Procházka	Procházka	k1gMnSc1
<g/>
,	,	kIx,
Dagmar	Dagmar	k1gFnSc1
Křížková	Křížková	k1gFnSc1
<g/>
:	:	kIx,
Medicínský	medicínský	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
homosexualitu	homosexualita	k1gFnSc4
Grano	Grano	k6eAd1
Salis	Salis	k1gInSc1
</s>
<s>
Usnesení	usnesení	k1gNnSc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
o	o	k7c6
homofobii	homofobie	k1gFnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Is	Is	k?
sexual	sexuat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
orientation	orientation	k1gInSc4
determined	determined	k1gMnSc1
at	at	k?
birth	birth	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
srovnání	srovnání	k1gNnSc2
názorů	názor	k1gInPc2
na	na	k7c4
původ	původ	k1gInSc4
homosexuality	homosexualita	k1gFnSc2
na	na	k7c6
webu	web	k1gInSc6
ProCon	ProCona	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
homosexualita	homosexualita	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
stránky	stránka	k1gFnSc2
spjaté	spjatý	k2eAgFnSc2d1
s	s	k7c7
hnutími	hnutí	k1gNnPc7
Exodus	Exodus	k1gInSc1
a	a	k8xC
Pro	pro	k7c4
Life	Life	k1gFnSc4
</s>
<s>
BROUK	brouk	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
<g/>
:	:	kIx,
Psychoanalytická	psychoanalytický	k2eAgFnSc1d1
sexuologie	sexuologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Alois	Alois	k1gMnSc1
Srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
1933	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
in	in	k?
Lidská	lidský	k2eAgFnSc1d1
duše	duše	k1gFnSc1
a	a	k8xC
sex	sex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
)	)	kIx)
–	–	k?
kapitoly	kapitola	k1gFnSc2
Homosexualita	homosexualita	k1gFnSc1
(	(	kIx(
<g/>
Homoerotismus	Homoerotismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Theorie	theorie	k1gFnSc1
homosexuality	homosexualita	k1gFnSc2
<g/>
,	,	kIx,
Psychoanalytická	psychoanalytický	k2eAgFnSc1d1
etiologie	etiologie	k1gFnSc1
a	a	k8xC
Sapphická	Sapphický	k2eAgFnSc1d1
láska	láska	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sexualita	sexualita	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4025798-8	4025798-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13999	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85061780	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85061780	#num#	k4
</s>
