<s>
Moták	moták	k1gMnSc1	moták
pochop	pochop	k1gMnSc1	pochop
(	(	kIx(	(
<g/>
Circus	Circus	k1gMnSc1	Circus
aeruginosus	aeruginosus	k1gMnSc1	aeruginosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
pochop	pochop	k1gMnSc1	pochop
rákosní	rákosní	k2eAgMnSc1d1	rákosní
je	být	k5eAaImIp3nS	být
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
motáka	moták	k1gMnSc2	moták
pochopa	pochop	k1gMnSc2	pochop
<g/>
,	,	kIx,	,
právě	právě	k9	právě
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
motáků	moták	k1gInPc2	moták
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
daný	daný	k2eAgInSc4d1	daný
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
vrchní	vrchní	k2eAgFnSc4d1	vrchní
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
kaštanově	kaštanově	k6eAd1	kaštanově
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
,	,	kIx,	,
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
hruď	hruď	k1gFnSc4	hruď
nažloutlou	nažloutlý	k2eAgFnSc4d1	nažloutlá
<g/>
,	,	kIx,	,
břišní	břišní	k2eAgFnSc4d1	břišní
část	část	k1gFnSc4	část
rudohnědou	rudohnědý	k2eAgFnSc4d1	rudohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedý	šedý	k2eAgInSc1d1	šedý
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
jsou	být	k5eAaImIp3nP	být
šedé	šedý	k2eAgFnPc1d1	šedá
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Špičky	špička	k1gFnPc1	špička
křídel	křídlo	k1gNnPc2	křídlo
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
těžší	těžký	k2eAgFnSc1d2	těžší
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
mladými	mladý	k2eAgMnPc7d1	mladý
ptáky	pták	k1gMnPc7	pták
jednobarevně	jednobarevně	k6eAd1	jednobarevně
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
tmavší	tmavý	k2eAgMnSc1d2	tmavší
než	než	k8xS	než
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vrch	vrch	k1gInSc1	vrch
hlavy	hlava	k1gFnSc2	hlava
má	mít	k5eAaImIp3nS	mít
světlý	světlý	k2eAgMnSc1d1	světlý
<g/>
,	,	kIx,	,
smetanový	smetanový	k2eAgMnSc1d1	smetanový
(	(	kIx(	(
<g/>
připomínající	připomínající	k2eAgFnSc4d1	připomínající
žlutou	žlutý	k2eAgFnSc4d1	žlutá
čepičku	čepička	k1gFnSc4	čepička
<g/>
)	)	kIx)	)
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
kostřec	kostřec	k1gInSc1	kostřec
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
tmavší	tmavý	k2eAgFnPc1d2	tmavší
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
čepičky	čepička	k1gFnSc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Poznáme	poznat	k5eAaPmIp1nP	poznat
ho	on	k3xPp3gNnSc4	on
podle	podle	k7c2	podle
typického	typický	k2eAgInSc2d1	typický
letu	let	k1gInSc2	let
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gInSc4	on
snadno	snadno	k6eAd1	snadno
zaměnit	zaměnit	k5eAaPmF	zaměnit
s	s	k7c7	s
motákem	moták	k1gMnSc7	moták
lužním	lužní	k2eAgMnSc7d1	lužní
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
štíhlejší	štíhlý	k2eAgInSc1d2	štíhlejší
<g/>
,	,	kIx,	,
s	s	k7c7	s
podélnými	podélný	k2eAgInPc7d1	podélný
pruhy	pruh	k1gInPc7	pruh
zespodu	zespodu	k7c2	zespodu
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
toku	tok	k1gInSc6	tok
předvádí	předvádět	k5eAaImIp3nS	předvádět
samec	samec	k1gMnSc1	samec
v	v	k7c6	v
letu	let	k1gInSc6	let
akrobatické	akrobatický	k2eAgInPc4d1	akrobatický
kousky	kousek	k1gInPc4	kousek
<g/>
,	,	kIx,	,
vystoupá	vystoupat	k5eAaPmIp3nS	vystoupat
vysoko	vysoko	k6eAd1	vysoko
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
snáší	snášet	k5eAaImIp3nS	snášet
dolů	dolů	k6eAd1	dolů
za	za	k7c7	za
družkou	družka	k1gFnSc7	družka
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
v	v	k7c6	v
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgNnSc1d1	vysoké
"	"	kIx"	"
<g/>
kví-eh	kvíh	k1gInSc1	kví-eh
<g/>
"	"	kIx"	"
a	a	k8xC	a
pískavé	pískavý	k2eAgNnSc1d1	pískavé
"	"	kIx"	"
<g/>
fíh	fíh	k?	fíh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnSc2d1	velká
káně	káně	k1gFnSc2	káně
lesní	lesní	k2eAgFnSc1d1	lesní
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
útlejším	útlý	k2eAgNnSc7d2	útlejší
vzezřením	vzezření	k1gNnSc7	vzezření
<g/>
,	,	kIx,	,
delším	dlouhý	k2eAgInSc7d2	delší
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
při	při	k7c6	při
klouzavém	klouzavý	k2eAgInSc6d1	klouzavý
letu	let	k1gInSc6	let
staví	stavit	k5eAaPmIp3nS	stavit
obě	dva	k4xCgNnPc4	dva
křídla	křídlo	k1gNnPc4	křídlo
do	do	k7c2	do
polohy	poloha	k1gFnSc2	poloha
širokého	široký	k2eAgNnSc2d1	široké
V.	V.	kA	V.
Pochopové	Pochop	k1gMnPc1	Pochop
preferují	preferovat	k5eAaImIp3nP	preferovat
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
hnízdění	hnízdění	k1gNnSc4	hnízdění
si	se	k3xPyFc3	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
rákosové	rákosový	k2eAgInPc4d1	rákosový
porosty	porost	k1gInPc4	porost
nebo	nebo	k8xC	nebo
polní	polní	k2eAgFnSc2d1	polní
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
ozim	ozim	k1gFnSc1	ozim
<g/>
,	,	kIx,	,
řepka	řepka	k1gFnSc1	řepka
<g/>
,	,	kIx,	,
vojtěška	vojtěška	k1gFnSc1	vojtěška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaBmIp3nS	stavit
si	se	k3xPyFc3	se
hnízdo	hnízdo	k1gNnSc4	hnízdo
ze	z	k7c2	z
stébel	stéblo	k1gNnPc2	stéblo
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
rákosu	rákos	k1gInSc6	rákos
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
května	květen	k1gInSc2	květen
snáší	snášet	k5eAaImIp3nS	snášet
samice	samice	k1gFnSc1	samice
v	v	k7c6	v
dvoudenním	dvoudenní	k2eAgInSc6d1	dvoudenní
intervalu	interval	k1gInSc6	interval
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
bělavých	bělavý	k2eAgNnPc2d1	bělavé
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
asi	asi	k9	asi
jednoho	jeden	k4xCgInSc2	jeden
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
samici	samice	k1gFnSc4	samice
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
nestřídá	střídat	k5eNaImIp3nS	střídat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přináší	přinášet	k5eAaImIp3nS	přinášet
jí	on	k3xPp3gFnSc3	on
potravu	potrava	k1gFnSc4	potrava
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
sezení	sezení	k1gNnSc3	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
i	i	k8xC	i
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
–	–	k?	–
samice	samice	k1gFnSc2	samice
přinesenou	přinesený	k2eAgFnSc4d1	přinesená
kořist	kořist	k1gFnSc4	kořist
porcuje	porcovat	k5eAaImIp3nS	porcovat
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
krmí	krmě	k1gFnPc2	krmě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
mláďata	mládě	k1gNnPc4	mládě
stráví	strávit	k5eAaPmIp3nS	strávit
asi	asi	k9	asi
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
se	se	k3xPyFc4	se
rozlézají	rozlézat	k5eAaImIp3nP	rozlézat
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
někdy	někdy	k6eAd1	někdy
vrací	vracet	k5eAaImIp3nS	vracet
i	i	k9	i
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
vzletnosti	vzletnost	k1gFnSc2	vzletnost
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
lovištěm	loviště	k1gNnSc7	loviště
jsou	být	k5eAaImIp3nP	být
břehy	břeh	k1gInPc1	břeh
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
slepých	slepý	k2eAgInPc2d1	slepý
a	a	k8xC	a
hustě	hustě	k6eAd1	hustě
zarostlých	zarostlý	k2eAgNnPc2d1	zarostlé
říčních	říční	k2eAgNnPc2d1	říční
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
rákosiny	rákosina	k1gFnPc4	rákosina
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc4	louka
a	a	k8xC	a
zamokřená	zamokřený	k2eAgNnPc4d1	zamokřené
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
takovou	takový	k3xDgFnSc7	takový
krajinou	krajina	k1gFnSc7	krajina
létá	létat	k5eAaImIp3nS	létat
pochop	pochop	k1gMnSc1	pochop
nízkým	nízký	k2eAgFnPc3d1	nízká
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
neurovnaným	urovnaný	k2eNgMnSc7d1	neurovnaný
(	(	kIx(	(
<g/>
jakoby	jakoby	k8xS	jakoby
potácivým	potácivý	k2eAgInSc7d1	potácivý
<g/>
)	)	kIx)	)
letem	let	k1gInSc7	let
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
potravu	potrava	k1gFnSc4	potrava
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
neloví	lovit	k5eNaImIp3nP	lovit
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
.	.	kIx.	.
</s>
<s>
Moták	moták	k1gMnSc1	moták
pochop	pochop	k1gMnSc1	pochop
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgInSc4d1	tažný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
zimoviště	zimoviště	k1gNnSc4	zimoviště
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
odlétá	odlétat	k5eAaPmIp3nS	odlétat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
až	až	k8xS	až
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
ze	z	k7c2	z
zimovišť	zimoviště	k1gNnPc2	zimoviště
přilétá	přilétat	k5eAaImIp3nS	přilétat
během	během	k7c2	během
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
odhadem	odhad	k1gInSc7	odhad
max	max	kA	max
<g/>
.	.	kIx.	.
1700	[number]	k4	1700
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
četnost	četnost	k1gFnSc1	četnost
výskytu	výskyt	k1gInSc2	výskyt
motáka	moták	k1gMnSc2	moták
pochopa	pochop	k1gMnSc2	pochop
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moták	moták	k1gMnSc1	moták
pochop	pochop	k1gMnSc1	pochop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Moták	moták	k1gInSc1	moták
pochop	pochop	k1gMnSc1	pochop
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
"	"	kIx"	"
<g/>
Hlas	hlas	k1gInSc1	hlas
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
</s>
