<s>
Ermesinda	Ermesinda	k1gFnSc1	Ermesinda
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Ermesinde	Ermesind	k1gInSc5	Ermesind
de	de	k?	de
Luxembourg	Luxembourg	k1gMnSc1	Luxembourg
<g/>
,	,	kIx,	,
1186	[number]	k4	1186
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1247	[number]	k4	1247
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1196	[number]	k4	1196
lucemburskou	lucemburský	k2eAgFnSc7d1	Lucemburská
hraběnkou	hraběnka	k1gFnSc7	hraběnka
<g/>
,	,	kIx,	,
pramáti	pramáti	k1gFnSc1	pramáti
lucemburského	lucemburský	k2eAgInSc2d1	lucemburský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
