<s>
Ermesinda	Ermesinda	k1gFnSc1	Ermesinda
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Ermesinde	Ermesind	k1gInSc5	Ermesind
de	de	k?	de
Luxembourg	Luxembourg	k1gMnSc1	Luxembourg
<g/>
,	,	kIx,	,
1186	[number]	k4	1186
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1247	[number]	k4	1247
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1196	[number]	k4	1196
lucemburskou	lucemburský	k2eAgFnSc7d1	Lucemburská
hraběnkou	hraběnka	k1gFnSc7	hraběnka
<g/>
,	,	kIx,	,
pramáti	pramáti	k1gFnSc1	pramáti
lucemburského	lucemburský	k2eAgInSc2d1	lucemburský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
dcerou	dcera	k1gFnSc7	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Slepého	slepý	k2eAgMnSc2d1	slepý
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
z	z	k7c2	z
Namuru	Namur	k1gInSc2	Namur
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
třetí	třetí	k4xOgFnPc1	třetí
manželky	manželka	k1gFnPc1	manželka
Anežky	Anežka	k1gFnSc2	Anežka
z	z	k7c2	z
Geldernu	Gelderna	k1gFnSc4	Gelderna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
narozením	narození	k1gNnSc7	narození
byl	být	k5eAaImAgMnS	být
dědicem	dědic	k1gMnSc7	dědic
lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
hraběte	hrabě	k1gMnSc2	hrabě
synovec	synovec	k1gMnSc1	synovec
Balduin	Balduin	k1gMnSc1	Balduin
V.	V.	kA	V.
Henegavský	Henegavský	k2eAgInSc1d1	Henegavský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
ji	on	k3xPp3gFnSc4	on
otec	otec	k1gMnSc1	otec
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Champagne	Champagn	k1gMnSc5	Champagn
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnSc1d1	původní
dědic	dědic	k1gMnSc1	dědic
Balduin	Balduin	k1gMnSc1	Balduin
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nevzdával	vzdávat	k5eNaImAgMnS	vzdávat
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
a	a	k8xC	a
zakročit	zakročit	k5eAaPmF	zakročit
musel	muset	k5eAaImAgMnS	muset
až	až	k9	až
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Balduin	Balduin	k1gMnSc1	Balduin
nakonec	nakonec	k6eAd1	nakonec
získal	získat	k5eAaPmAgMnS	získat
Namurské	Namurský	k2eAgNnSc4d1	Namurský
hrabství	hrabství	k1gNnSc4	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Ermesinda	Ermesinda	k1gFnSc1	Ermesinda
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
staršího	starý	k2eAgMnSc4d2	starší
Theobalda	Theobald	k1gMnSc4	Theobald
I.	I.	kA	I.
z	z	k7c2	z
Baru	bar	k1gInSc2	bar
(	(	kIx(	(
<g/>
1158	[number]	k4	1158
<g/>
-	-	kIx~	-
<g/>
1214	[number]	k4	1214
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
získat	získat	k5eAaPmF	získat
Namur	Namur	k1gMnSc1	Namur
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
čtyři	čtyři	k4xCgFnPc1	čtyři
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Theobald	Theobald	k1gMnSc1	Theobald
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1214	[number]	k4	1214
a	a	k8xC	a
osmadvacetiletá	osmadvacetiletý	k2eAgFnSc1d1	osmadvacetiletá
vdova	vdova	k1gFnSc1	vdova
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
provdala	provdat	k5eAaPmAgFnS	provdat
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
za	za	k7c2	za
hraběte	hrabě	k1gMnSc2	hrabě
Walrama	Walramum	k1gNnSc2	Walramum
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Limburského	limburský	k2eAgInSc2d1	limburský
<g/>
.	.	kIx.	.
</s>
<s>
Druhému	druhý	k4xOgMnSc3	druhý
manželovi	manžel	k1gMnSc3	manžel
porodila	porodit	k5eAaPmAgFnS	porodit
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
a	a	k8xC	a
přežila	přežít	k5eAaPmAgFnS	přežít
ho	on	k3xPp3gMnSc4	on
o	o	k7c6	o
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ermesindu	Ermesind	k1gInSc6	Ermesind
se	se	k3xPyFc4	se
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
jako	jako	k9	jako
na	na	k7c4	na
dobrou	dobrý	k2eAgFnSc4d1	dobrá
panovnici	panovnice	k1gFnSc4	panovnice
a	a	k8xC	a
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
kláštera	klášter	k1gInSc2	klášter
Clairefontaine	Clairefontain	k1gMnSc5	Clairefontain
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
také	také	k9	také
uložena	uložit	k5eAaPmNgFnS	uložit
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
.	.	kIx.	.
</s>
