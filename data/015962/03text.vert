<s>
Místní	místní	k2eAgNnSc1d1
referendum	referendum	k1gNnSc1
o	o	k7c6
zřízení	zřízení	k1gNnSc6
ženské	ženský	k2eAgFnSc2d1
věznice	věznice	k1gFnSc2
v	v	k7c6
Králíkách	Králíkách	k?
</s>
<s>
Místní	místní	k2eAgNnSc1d1
referendum	referendum	k1gNnSc1
o	o	k7c6
zřízení	zřízení	k1gNnSc6
ženské	ženský	k2eAgFnSc2d1
věznice	věznice	k1gFnSc2
v	v	k7c6
Králíkách	Králíky	k1gInPc6
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
zřízení	zřízení	k1gNnSc3
hlasovalo	hlasovat	k5eAaImAgNnS
86,02	86,02	k4
%	%	kIx~
zúčastněných	zúčastněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
a	a	k8xC
výsledek	výsledek	k1gInSc1
referenda	referendum	k1gNnSc2
byl	být	k5eAaImAgInS
tedy	tedy	k9
platný	platný	k2eAgInSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
účast	účast	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
44,7	44,7	k4
%	%	kIx~
a	a	k8xC
překročila	překročit	k5eAaPmAgFnS
tak	tak	k6eAd1
požadovanou	požadovaný	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
35	#num#	k4
%	%	kIx~
účasti	účast	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyvolání	vyvolání	k1gNnSc1
referenda	referendum	k1gNnSc2
</s>
<s>
Dětský	dětský	k2eAgInSc1d1
výchovný	výchovný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Králíkách	Králíkách	k?
ukončil	ukončit	k5eAaPmAgMnS
provoz	provoz	k1gInSc4
koncem	koncem	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původním	původní	k2eAgInSc7d1
plánem	plán	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
bylo	být	k5eAaImAgNnS
využít	využít	k5eAaPmF
tuto	tento	k3xDgFnSc4
budovu	budova	k1gFnSc4
jako	jako	k8xC,k8xS
ubytovnu	ubytovna	k1gFnSc4
pro	pro	k7c4
žadatele	žadatel	k1gMnPc4
o	o	k7c4
azyl	azyl	k1gInSc4
<g/>
,	,	kIx,
proti	proti	k7c3
ovšem	ovšem	k9
byla	být	k5eAaImAgFnS
významná	významný	k2eAgFnSc1d1
část	část	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
argumentovali	argumentovat	k5eAaImAgMnP
možným	možný	k2eAgNnSc7d1
poškozením	poškození	k1gNnSc7
turistického	turistický	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
v	v	k7c6
regionu	region	k1gInSc6
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
zvažovalo	zvažovat	k5eAaImAgNnS
konání	konání	k1gNnSc3
referenda	referendum	k1gNnSc2
<g/>
,	,	kIx,
ministerstvo	ministerstvo	k1gNnSc1
ovšem	ovšem	k9
od	od	k7c2
svého	svůj	k3xOyFgInSc2
plánu	plán	k1gInSc2
předčasně	předčasně	k6eAd1
ustoupilo	ustoupit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalším	další	k2eAgInSc7d1
návrhem	návrh	k1gInSc7
využití	využití	k1gNnSc1
budovy	budova	k1gFnSc2
bylo	být	k5eAaImAgNnS
zřízení	zřízení	k1gNnSc1
ženské	ženský	k2eAgFnSc2d1
věznice	věznice	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
podle	podle	k7c2
Ministerstva	ministerstvo	k1gNnSc2
spravedlnosti	spravedlnost	k1gFnSc2
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
umístěno	umístit	k5eAaPmNgNnS
až	až	k9
200	#num#	k4
vězeňkyň	vězeňkyně	k1gFnPc2
s	s	k7c7
nejmírnějším	mírný	k2eAgInSc7d3
stupněm	stupeň	k1gInSc7
ochrany	ochrana	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
by	by	kYmCp3nS
zde	zde	k6eAd1
podle	podle	k7c2
Vězeňské	vězeňský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
našlo	najít	k5eAaPmAgNnS
práci	práce	k1gFnSc4
asi	asi	k9
120	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
odsouzené	odsouzený	k1gMnPc4
bylo	být	k5eAaImAgNnS
také	také	k6eAd1
plánováno	plánovat	k5eAaImNgNnS
zřízení	zřízení	k1gNnSc4
učiliště	učiliště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Referendum	referendum	k1gNnSc1
vyhlásilo	vyhlásit	k5eAaPmAgNnS
Zastupitelstvo	zastupitelstvo	k1gNnSc4
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
zasedání	zasedání	k1gNnSc6
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Referendum	referendum	k1gNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c4
pátek	pátek	k1gInSc4
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
otázka	otázka	k1gFnSc1
zněla	znět	k5eAaImAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Souhlasíte	souhlasit	k5eAaImIp2nP
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
areálu	areál	k1gInSc6
bývalého	bývalý	k2eAgInSc2d1
Dětského	dětský	k2eAgInSc2d1
výchovného	výchovný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
v	v	k7c6
Králíkách	Králíkách	k?
zřídilo	zřídit	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
ČR	ČR	kA
ženskou	ženský	k2eAgFnSc4d1
věznici	věznice	k1gFnSc4
s	s	k7c7
dohledem	dohled	k1gInSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
zamítnut	zamítnout	k5eAaPmNgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
pro	pro	k7c4
zřízení	zřízení	k1gNnSc4
věznice	věznice	k1gFnSc2
vyjádřilo	vyjádřit	k5eAaPmAgNnS
pouze	pouze	k6eAd1
14	#num#	k4
<g/>
%	%	kIx~
zúčastněných	zúčastněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
hlasování	hlasování	k1gNnSc2
je	být	k5eAaImIp3nS
platný	platný	k2eAgMnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
překročil	překročit	k5eAaPmAgInS
požadovanou	požadovaný	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
35	#num#	k4
%	%	kIx~
účasti	účast	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
k	k	k7c3
urnám	urna	k1gFnPc3
dorazilo	dorazit	k5eAaPmAgNnS
44,7	44,7	k4
<g/>
%	%	kIx~
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
Robert	Robert	k1gMnSc1
Pelikán	Pelikán	k1gMnSc1
přislíbil	přislíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
výsledku	výsledek	k1gInSc3
referenda	referendum	k1gNnSc2
bude	být	k5eAaImBp3nS
přihlédnuto	přihlédnut	k2eAgNnSc1d1
<g/>
,	,	kIx,
přestože	přestože	k8xS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
není	být	k5eNaImIp3nS
právně	právně	k6eAd1
závazný	závazný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Lidé	člověk	k1gMnPc1
z	z	k7c2
Králík	Králík	k1gMnSc1
v	v	k7c6
referendu	referendum	k1gNnSc6
odmítli	odmítnout	k5eAaPmAgMnP
záměr	záměr	k1gInSc4
zřízení	zřízení	k1gNnSc2
věznice	věznice	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
|	|	kIx~
ČeskéNoviny	ČeskéNovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.ceskenoviny.cz	www.ceskenoviny.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Město	město	k1gNnSc4
Králíky	Králík	k1gMnPc4
-	-	kIx~
Zápis	zápis	k1gInSc1
komise	komise	k1gFnSc2
o	o	k7c6
výsledku	výsledek	k1gInSc6
hlasování	hlasování	k1gNnSc2
v	v	k7c6
místním	místní	k2eAgNnSc6d1
referendu	referendum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlin	k1gInSc5
<g/>
↑	↑	k?
Králíky	Králík	k1gMnPc7
vyhlásí	vyhlásit	k5eAaPmIp3nP
referendum	referendum	k1gNnSc1
o	o	k7c6
zřízení	zřízení	k1gNnSc6
věznice	věznice	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Zápis	zápis	k1gInSc1
ze	z	k7c2
7	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
zastupitelstva	zastupitelstvo	k1gNnSc2
Města	město	k1gNnSc2
Králíky	Králík	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
online	onlin	k1gMnSc5
</s>
