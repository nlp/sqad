<s>
Obecné	obecný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
/	/	kIx~
Nařízení	nařízení	k1gNnSc1
GDPR	GDPR	kA
</s>
<s>
NAŘÍZENÍ	nařízení	k1gNnSc1
EVROPSKÉHO	evropský	k2eAgInSc2d1
PARLAMENTU	parlament	k1gInSc2
A	a	k8xC
RADY	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
679	#num#	k4
o	o	k7c6
ochraně	ochrana	k1gFnSc6
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
o	o	k7c6
volném	volný	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
těchto	tento	k3xDgInPc2
údajů	údaj	k1gInPc2
a	a	k8xC
o	o	k7c6
zrušení	zrušení	k1gNnSc6
směrnice	směrnice	k1gFnSc2
95	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
Předpis	předpis	k1gInSc4
státu	stát	k1gInSc2
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Druh	druh	k1gInSc1
předpisu	předpis	k1gInSc2
</s>
<s>
Nařízení	nařízení	k1gNnSc4
Číslo	číslo	k1gNnSc1
předpisu	předpis	k1gInSc2
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
679	#num#	k4
<g/>
/	/	kIx~
<g/>
EU	EU	kA
Obvyklá	obvyklý	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
GDPR	GDPR	kA
Údaje	údaj	k1gInPc1
Autor	autor	k1gMnSc1
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
Evropská	evropský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Schváleno	schválit	k5eAaPmNgNnS
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
Účinnost	účinnost	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
Související	související	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c4
zpracování	zpracování	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
Oblast	oblast	k1gFnSc4
úpravy	úprava	k1gFnSc2
</s>
<s>
správní	správní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
ONOOÚ	ONOOÚ	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
GDPR	GDPR	kA
<g/>
,	,	kIx,
General	General	k1gMnSc1
Data	datum	k1gNnSc2
Protection	Protection	k1gInSc1
Regulation	Regulation	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Nařízení	nařízení	k1gNnSc2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
č.	č.	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
679	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
o	o	k7c6
ochraně	ochrana	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
o	o	k7c6
volném	volný	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
těchto	tento	k3xDgInPc2
údajů	údaj	k1gInPc2
a	a	k8xC
o	o	k7c6
zrušení	zrušení	k1gNnSc6
směrnice	směrnice	k1gFnSc2
95	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
(	(	kIx(
<g/>
obecné	obecný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nařízení	nařízení	k1gNnSc1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
výrazné	výrazný	k2eAgNnSc4d1
zvýšení	zvýšení	k1gNnSc4
ochrany	ochrana	k1gFnSc2
osobních	osobní	k2eAgNnPc2d1
dat	datum	k1gNnPc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Úředním	úřední	k2eAgInSc6d1
věstníku	věstník	k1gInSc6
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předmět	předmět	k1gInSc1
a	a	k8xC
cíle	cíl	k1gInPc1
</s>
<s>
Stanovit	stanovit	k5eAaPmF
pravidla	pravidlo	k1gNnPc4
ochrany	ochrana	k1gFnSc2
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
jejich	jejich	k3xOp3gInPc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
pohyb	pohyb	k1gInSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nařízení	nařízení	k1gNnSc1
chrání	chránit	k5eAaImIp3nS
základní	základní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
právo	právo	k1gNnSc4
ochrany	ochrana	k1gFnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volný	volný	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
není	být	k5eNaImIp3nS
z	z	k7c2
důvodu	důvod	k1gInSc2
ochrany	ochrana	k1gFnSc2
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
omezen	omezit	k5eAaPmNgInS
ani	ani	k8xC
zakázán	zakázat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Věcná	věcný	k2eAgFnSc1d1
působnost	působnost	k1gFnSc1
</s>
<s>
Nařízení	nařízení	k1gNnSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
na	na	k7c4
automatizované	automatizovaný	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
i	i	k9
na	na	k7c4
neautomatizované	automatizovaný	k2eNgNnSc4d1
zpracování	zpracování	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
obsažených	obsažený	k2eAgInPc2d1
v	v	k7c6
evidenci	evidence	k1gFnSc6
nebo	nebo	k8xC
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
zařazeny	zařadit	k5eAaPmNgInP
do	do	k7c2
evidence	evidence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nařízení	nařízení	k1gNnSc1
se	se	k3xPyFc4
nevztahuje	vztahovat	k5eNaImIp3nS
na	na	k7c4
zpracování	zpracování	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
prováděné	prováděný	k2eAgFnPc1d1
<g/>
:	:	kIx,
</s>
<s>
při	při	k7c6
výkonu	výkon	k1gInSc6
činností	činnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
nespadají	spadat	k5eNaImIp3nP,k5eNaPmIp3nP
do	do	k7c2
oblasti	oblast	k1gFnSc2
působnosti	působnost	k1gFnSc2
práva	právo	k1gNnSc2
Unie	unie	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
při	při	k7c6
výkonu	výkon	k1gInSc6
činností	činnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
do	do	k7c2
oblasti	oblast	k1gFnSc2
působnosti	působnost	k1gFnSc2
hlavy	hlava	k1gFnSc2
V	v	k7c4
kapitoly	kapitola	k1gFnPc4
2	#num#	k4
Smlouvy	smlouva	k1gFnSc2
o	o	k7c4
EU	EU	kA
<g/>
;	;	kIx,
</s>
<s>
fyzickou	fyzický	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
v	v	k7c6
průběhu	průběh	k1gInSc6
výlučně	výlučně	k6eAd1
osobních	osobní	k2eAgFnPc2d1
či	či	k8xC
domácích	domácí	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
příslušnými	příslušný	k2eAgInPc7d1
orgány	orgán	k1gInPc7
za	za	k7c7
účelem	účel	k1gInSc7
prevence	prevence	k1gFnSc2
<g/>
,	,	kIx,
vyšetřování	vyšetřování	k1gNnSc2
<g/>
,	,	kIx,
odhalování	odhalování	k1gNnSc2
či	či	k8xC
stíhání	stíhání	k1gNnSc2
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
nebo	nebo	k8xC
výkonu	výkon	k1gInSc2
trestů	trest	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
ochrany	ochrana	k1gFnSc2
před	před	k7c7
hrozbami	hrozba	k1gFnPc7
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gNnPc2
předcházení	předcházení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Zpracování	zpracování	k1gNnSc1
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
orgány	orgán	k1gInPc7
<g/>
,	,	kIx,
institucemi	instituce	k1gFnPc7
a	a	k8xC
jinými	jiný	k2eAgInPc7d1
subjekty	subjekt	k1gInPc7
Unie	unie	k1gFnSc2
je	být	k5eAaImIp3nS
upraveno	upravit	k5eAaPmNgNnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
nařízením	nařízení	k1gNnSc7
(	(	kIx(
<g/>
ES	es	k1gNnPc2
<g/>
)	)	kIx)
č.	č.	k?
45	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nařízení	nařízení	k1gNnSc1
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
45	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
a	a	k8xC
další	další	k2eAgInPc4d1
právní	právní	k2eAgInPc4d1
akty	akt	k1gInPc4
Unie	unie	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
takového	takový	k3xDgNnSc2
zpracování	zpracování	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
jsou	být	k5eAaImIp3nP
uzpůsobeny	uzpůsobit	k5eAaPmNgInP
zásadám	zásada	k1gFnPc3
a	a	k8xC
pravidlům	pravidlo	k1gNnPc3
tohoto	tento	k3xDgNnSc2
nařízení	nařízení	k1gNnSc2
podle	podle	k7c2
článku	článek	k1gInSc2
98	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nařízením	nařízení	k1gNnSc7
není	být	k5eNaImIp3nS
dotčeno	dotčen	k2eAgNnSc1d1
uplatňování	uplatňování	k1gNnSc1
směrnice	směrnice	k1gFnSc2
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
</s>
<s>
Nařízení	nařízení	k1gNnSc1
zpřesňuje	zpřesňovat	k5eAaImIp3nS
a	a	k8xC
rozšiřuje	rozšiřovat	k5eAaImIp3nS
okruh	okruh	k1gInSc4
a	a	k8xC
definici	definice	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobní	osobní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
jsou	být	k5eAaImIp3nP
jakékoliv	jakýkoliv	k3yIgFnPc4
informace	informace	k1gFnPc4
o	o	k7c6
identifikované	identifikovaný	k2eAgFnSc6d1
nebo	nebo	k8xC
identifikovatelné	identifikovatelný	k2eAgFnSc3d1
fyzické	fyzický	k2eAgFnSc3d1
osobě	osoba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobním	osobnit	k5eAaImIp1nS
údajem	údaj	k1gInSc7
proto	proto	k8xC
nejsou	být	k5eNaImIp3nP
např.	např.	kA
údaje	údaj	k1gInSc2
o	o	k7c6
právnické	právnický	k2eAgFnSc6d1
osobě	osoba	k1gFnSc6
(	(	kIx(
<g/>
o	o	k7c6
jejích	její	k3xOp3gMnPc6
zaměstnancích	zaměstnanec	k1gMnPc6
už	už	k9
ale	ale	k8xC
ano	ano	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
údaje	údaj	k1gInPc1
o	o	k7c6
osobách	osoba	k1gFnPc6
zemřelých	zemřelý	k2eAgMnPc2d1
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
to	ten	k3xDgNnSc1
údaje	údaj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
konkrétní	konkrétní	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
neztotožňují	ztotožňovat	k5eNaImIp3nP
(	(	kIx(
<g/>
např.	např.	kA
pouhé	pouhý	k2eAgNnSc4d1
běžné	běžný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
a	a	k8xC
příjmení	příjmení	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
mezi	mezi	k7c4
osobní	osobní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g />
.	.	kIx.
</s>
<s hack="1">
nepatří	patřit	k5eNaImIp3nS
údaje	údaj	k1gInPc4
anonymizované	anonymizovaný	k2eAgInPc4d1
<g/>
,	,	kIx,
tedy	tedy	k9
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
původně	původně	k6eAd1
možnost	možnost	k1gFnSc1
identifikace	identifikace	k1gFnSc2
osoby	osoba	k1gFnSc2
obsahovaly	obsahovat	k5eAaImAgInP
<g/>
,	,	kIx,
ale	ale	k8xC
takový	takový	k3xDgInSc1
identifikátor	identifikátor	k1gInSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byl	být	k5eAaImAgInS
odstraněn	odstranit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Už	už	k6eAd1
směrnice	směrnice	k1gFnSc1
(	(	kIx(
<g/>
č.	č.	k?
95	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
<g/>
,	,	kIx,
předcházející	předcházející	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
<g/>
)	)	kIx)
naopak	naopak	k6eAd1
mezi	mezi	k7c4
osobní	osobní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
zařadila	zařadit	k5eAaPmAgFnS
i	i	k8xC
dynamické	dynamický	k2eAgFnSc2d1
IP	IP	kA
adresy	adresa	k1gFnSc2
či	či	k8xC
jiné	jiný	k2eAgInPc4d1
virtuální	virtuální	k2eAgInPc4d1
identifikátory	identifikátor	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Osobním	osobní	k2eAgInSc7d1
údajem	údaj	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
např.	např.	kA
i	i	k8xC
způsob	způsob	k1gInSc1
vystupování	vystupování	k1gNnSc2
advokáta	advokát	k1gMnSc2
v	v	k7c6
soudním	soudní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Závaznost	závaznost	k1gFnSc1
</s>
<s>
Nařízení	nařízení	k1gNnSc1
je	být	k5eAaImIp3nS
závazné	závazný	k2eAgNnSc1d1
v	v	k7c6
celém	celý	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
a	a	k8xC
přímo	přímo	k6eAd1
použitelné	použitelný	k2eAgFnPc1d1
ve	v	k7c6
všech	všecek	k3xTgInPc6
členských	členský	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facebook	Facebook	k1gInSc4
a	a	k8xC
Google	Googla	k1gFnSc3
jsou	být	k5eAaImIp3nP
první	první	k4xOgInPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
čelí	čelit	k5eAaImIp3nS
žalobě	žaloba	k1gFnSc3
z	z	k7c2
porušování	porušování	k1gNnSc2
nařízení	nařízení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Studie	studie	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dané	daný	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
nadále	nadále	k6eAd1
omezují	omezovat	k5eAaImIp3nP
práva	právo	k1gNnPc4
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
obsahu	obsah	k1gInSc2
</s>
<s>
Nařízení	nařízení	k1gNnSc1
definuje	definovat	k5eAaBmIp3nS
zásady	zásada	k1gFnPc1
zpracování	zpracování	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
podmínky	podmínka	k1gFnPc4
zákonnosti	zákonnost	k1gFnSc2
jejich	jejich	k3xOp3gNnSc2
zpracování	zpracování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upravuje	upravovat	k5eAaImIp3nS
také	také	k9
podmínky	podmínka	k1gFnPc1
vyjádření	vyjádření	k1gNnSc2
poskytnutého	poskytnutý	k2eAgInSc2d1
souhlasu	souhlas	k1gInSc2
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
údajů	údaj	k1gInPc2
a	a	k8xC
poskytování	poskytování	k1gNnSc4
informací	informace	k1gFnPc2
a	a	k8xC
přístupu	přístup	k1gInSc2
k	k	k7c3
osobním	osobní	k2eAgInPc3d1
údajům	údaj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Vybraná	vybraná	k1gFnSc1
práva	právo	k1gNnSc2
subjektu	subjekt	k1gInSc2
údajů	údaj	k1gInPc2
</s>
<s>
Mimo	mimo	k6eAd1
dalších	další	k2eAgNnPc2d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
subjekt	subjekt	k1gInSc1
údajů	údaj	k1gInPc2
následující	následující	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
na	na	k7c4
opravu	oprava	k1gFnSc4
</s>
<s>
na	na	k7c4
výmaz	výmaz	k1gInSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
právo	práv	k2eAgNnSc1d1
být	být	k5eAaImF
zapomenut	zapomenut	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
na	na	k7c4
omezení	omezení	k1gNnSc4
zpracování	zpracování	k1gNnSc2
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1
povinnosti	povinnost	k1gFnPc1
správce	správce	k1gMnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
</s>
<s>
Ohlašovací	ohlašovací	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
správce	správce	k1gMnSc2
vůči	vůči	k7c3
dozorovému	dozorový	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
(	(	kIx(
<g/>
čl	čl	kA
<g/>
.	.	kIx.
33	#num#	k4
GDPR	GDPR	kA
<g/>
)	)	kIx)
</s>
<s>
Správce	správce	k1gMnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
čl	čl	kA
<g/>
.	.	kIx.
33	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
GDPR	GDPR	kA
povinnost	povinnost	k1gFnSc4
ohlásit	ohlásit	k5eAaPmF
jakékoliv	jakýkoliv	k3yIgNnSc4
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
dozorovému	dozorový	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
(	(	kIx(
<g/>
v	v	k7c6
ČR	ČR	kA
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bez	bez	k7c2
zbytečného	zbytečný	k2eAgInSc2d1
odkladu	odklad	k1gInSc2
a	a	k8xC
pokud	pokud	k8xS
možno	možno	k6eAd1
do	do	k7c2
72	#num#	k4
hodin	hodina	k1gFnPc2
od	od	k7c2
okamžiku	okamžik	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gMnSc6
dozvěděl	dozvědět	k5eAaPmAgMnS
(	(	kIx(
<g/>
Pokud	pokud	k8xS
není	být	k5eNaImIp3nS
ohlášení	ohlášení	k1gNnSc1
učiněno	učinit	k5eAaImNgNnS,k5eAaPmNgNnS
do	do	k7c2
72	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
později	pozdě	k6eAd2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
současně	současně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
uvedeny	uveden	k2eAgInPc4d1
důvody	důvod	k1gInPc4
zpoždění	zpoždění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správce	správce	k1gMnSc1
tak	tak	k9
nemusí	muset	k5eNaImIp3nS
učinit	učinit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
jen	jen	k9
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
toto	tento	k3xDgNnSc1
porušení	porušení	k1gNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
riziko	riziko	k1gNnSc4
pro	pro	k7c4
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
případě	případ	k1gInSc6
používání	používání	k1gNnSc2
pseudonymizace	pseudonymizace	k1gFnSc2
či	či	k8xC
šifrování	šifrování	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
mohou	moct	k5eAaImIp3nP
riziko	riziko	k1gNnSc4
pro	pro	k7c4
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
zcela	zcela	k6eAd1
eliminovat	eliminovat	k5eAaBmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ohlášení	ohlášení	k1gNnSc1
porušení	porušení	k1gNnSc2
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
musí	muset	k5eAaImIp3nP
podle	podle	k7c2
čl	čl	kA
<g/>
.	.	kIx.
33	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
GDPR	GDPR	kA
přinejmenším	přinejmenším	k6eAd1
obsahovat	obsahovat	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
popis	popis	k1gInSc1
povahy	povaha	k1gFnSc2
daného	daný	k2eAgInSc2d1
případu	případ	k1gInSc2
porušení	porušení	k1gNnSc2
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
včetně	včetně	k7c2
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kategorií	kategorie	k1gFnPc2
a	a	k8xC
přibližného	přibližný	k2eAgInSc2d1
počtu	počet	k1gInSc2
dotčených	dotčený	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
údajů	údaj	k1gInPc2
a	a	k8xC
kategorií	kategorie	k1gFnPc2
a	a	k8xC
přibližného	přibližný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
dotčených	dotčený	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
jméno	jméno	k1gNnSc1
a	a	k8xC
kontaktní	kontaktní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
pověřence	pověřenec	k1gMnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
nebo	nebo	k8xC
jiného	jiný	k2eAgNnSc2d1
kontaktního	kontaktní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
může	moct	k5eAaImIp3nS
poskytnout	poskytnout	k5eAaPmF
bližší	blízký	k2eAgFnPc4d2
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
popis	popis	k1gInSc1
pravděpodobných	pravděpodobný	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
popis	popis	k1gInSc1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
správce	správce	k1gMnPc4
přijal	přijmout	k5eAaPmAgMnS
nebo	nebo	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
k	k	k7c3
přijetí	přijetí	k1gNnSc3
s	s	k7c7
cílem	cíl	k1gInSc7
vyřešit	vyřešit	k5eAaPmF
dané	daný	k2eAgNnSc4d1
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
případných	případný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
ke	k	k7c3
zmírnění	zmírnění	k1gNnSc3
možných	možný	k2eAgInPc2d1
nepříznivých	příznivý	k2eNgInPc2d1
dopadů	dopad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
zveřejnil	zveřejnit	k5eAaPmAgInS
formulář	formulář	k1gInSc1
Archivováno	archivován	k2eAgNnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
správci	správce	k1gMnPc1
mohou	moct	k5eAaImIp3nP
použít	použít	k5eAaPmF
při	při	k7c6
oznamování	oznamování	k1gNnSc6
porušení	porušení	k1gNnSc2
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
subjektu	subjekt	k1gInSc2
údajů	údaj	k1gInPc2
dozorovému	dozorový	k2eAgInSc3d1
orgánu	orgán	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oznamovací	oznamovací	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
správce	správka	k1gFnSc3
vůči	vůči	k7c3
subjektům	subjekt	k1gInPc3
údajů	údaj	k1gInPc2
(	(	kIx(
<g/>
čl	čl	kA
<g/>
.	.	kIx.
34	#num#	k4
GDPR	GDPR	kA
<g/>
)	)	kIx)
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
konkrétní	konkrétní	k2eAgInSc4d1
případ	případ	k1gInSc4
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
za	za	k7c4
následek	následek	k1gInSc4
vysoké	vysoký	k2eAgNnSc4d1
riziko	riziko	k1gNnSc4
pro	pro	k7c4
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
správce	správce	k1gMnSc1
musí	muset	k5eAaImIp3nS
podle	podle	k7c2
čl	čl	kA
<g/>
.	.	kIx.
34	#num#	k4
GDPR	GDPR	kA
toto	tento	k3xDgNnSc4
porušení	porušení	k1gNnSc4
bez	bez	k7c2
zbytečného	zbytečný	k2eAgInSc2d1
odkladu	odklad	k1gInSc2
oznámit	oznámit	k5eAaPmF
i	i	k9
přímo	přímo	k6eAd1
subjektům	subjekt	k1gInPc3
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oznámení	oznámení	k1gNnSc6
určeném	určený	k2eAgNnSc6d1
subjektu	subjekt	k1gInSc2
údajů	údaj	k1gInPc2
musí	muset	k5eAaImIp3nS
správce	správce	k1gMnSc4
podle	podle	k7c2
čl	čl	kA
<g/>
.	.	kIx.
34	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
GDPR	GDPR	kA
popsat	popsat	k5eAaPmF
povahu	povaha	k1gFnSc4
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
uvést	uvést	k5eAaPmF
v	v	k7c6
něm	on	k3xPp3gInSc6
přinejmenším	přinejmenším	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
jméno	jméno	k1gNnSc1
a	a	k8xC
kontaktní	kontaktní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
pověřence	pověřenec	k1gMnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
nebo	nebo	k8xC
jiného	jiný	k2eAgNnSc2d1
kontaktního	kontaktní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
může	moct	k5eAaImIp3nS
poskytnout	poskytnout	k5eAaPmF
bližší	blízký	k2eAgFnPc4d2
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
popis	popis	k1gInSc1
pravděpodobných	pravděpodobný	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
popis	popis	k1gInSc1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
správce	správce	k1gMnPc4
přijal	přijmout	k5eAaPmAgMnS
nebo	nebo	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
k	k	k7c3
přijetí	přijetí	k1gNnSc3
s	s	k7c7
cílem	cíl	k1gInSc7
vyřešit	vyřešit	k5eAaPmF
dané	daný	k2eAgNnSc4d1
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
případných	případný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
ke	k	k7c3
zmírnění	zmírnění	k1gNnSc3
možných	možný	k2eAgInPc2d1
nepříznivých	příznivý	k2eNgInPc2d1
dopadů	dopad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
výše	výše	k1gFnPc1,k1gFnPc1wB
uvedené	uvedený	k2eAgFnSc2d1
informace	informace	k1gFnSc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
vysvětlit	vysvětlit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
pro	pro	k7c4
subjekt	subjekt	k1gInSc4
údajů	údaj	k1gInPc2
pochopitelné	pochopitelný	k2eAgFnPc1d1
(	(	kIx(
<g/>
tedy	tedy	k8xC
za	za	k7c4
použití	použití	k1gNnSc4
jasných	jasný	k2eAgInPc2d1
a	a	k8xC
jednoduchých	jednoduchý	k2eAgInPc2d1
jazykových	jazykový	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodné	vhodný	k2eAgFnPc4d1
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
subjekty	subjekt	k1gInPc4
údajů	údaj	k1gInPc2
poučit	poučit	k5eAaPmF
i	i	k9
o	o	k7c6
způsobu	způsob	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
mohou	moct	k5eAaImIp3nP
samy	sám	k3xTgInPc1
následky	následek	k1gInPc1
porušení	porušení	k1gNnSc2
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
minimalizovat	minimalizovat	k5eAaBmF
(	(	kIx(
<g/>
např.	např.	kA
pokud	pokud	k8xS
unikla	uniknout	k5eAaPmAgFnS
databáze	databáze	k1gFnSc1
přihlašovacích	přihlašovací	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
a	a	k8xC
hesel	heslo	k1gNnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
žádoucí	žádoucí	k2eAgInPc4d1
subjekty	subjekt	k1gInPc4
údajů	údaj	k1gInPc2
poučit	poučit	k5eAaPmF
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
si	se	k3xPyFc3
měly	mít	k5eAaImAgFnP
heslo	heslo	k1gNnSc4
co	co	k8xS
nejrychleji	rychle	k6eAd3
změnit	změnit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Správce	správce	k1gMnSc1
nemusí	muset	k5eNaImIp3nS
podle	podle	k7c2
čl	čl	kA
<g/>
.	.	kIx.
34	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
GDPR	GDPR	kA
oznámit	oznámit	k5eAaPmF
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
subjektům	subjekt	k1gInPc3
údajů	údaj	k1gInPc2
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
splněna	splněn	k2eAgFnSc1d1
kterákoli	kterýkoli	k3yIgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
podmínek	podmínka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
správce	správce	k1gMnPc4
zavedl	zavést	k5eAaPmAgInS
náležitá	náležitý	k2eAgNnPc4d1
technická	technický	k2eAgNnPc4d1
a	a	k8xC
organizační	organizační	k2eAgNnPc4d1
ochranná	ochranný	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
a	a	k8xC
tato	tento	k3xDgNnPc1
opatření	opatření	k1gNnPc1
byla	být	k5eAaImAgNnP
použita	použít	k5eAaPmNgNnP
u	u	k7c2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
dotčených	dotčený	k2eAgInPc2d1
porušením	porušení	k1gNnSc7
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
činí	činit	k5eAaImIp3nS
tyto	tento	k3xDgInPc4
údaje	údaj	k1gInPc4
nesrozumitelnými	srozumitelný	k2eNgFnPc7d1
pro	pro	k7c4
kohokoli	kdokoli	k3yInSc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
není	být	k5eNaImIp3nS
oprávněn	oprávněn	k2eAgMnSc1d1
k	k	k7c3
nim	on	k3xPp3gMnPc3
mít	mít	k5eAaImF
přístup	přístup	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
šifrování	šifrování	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
správce	správce	k1gMnSc1
přijal	přijmout	k5eAaPmAgMnS
následná	následný	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zajistí	zajistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vysoké	vysoký	k2eAgNnSc4d1
riziko	riziko	k1gNnSc4
pro	pro	k7c4
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
subjektů	subjekt	k1gInPc2
údajů	údaj	k1gInPc2
se	se	k3xPyFc4
již	již	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
neprojeví	projevit	k5eNaPmIp3nS
<g/>
,	,	kIx,
</s>
<s>
vyžadovalo	vyžadovat	k5eAaImAgNnS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
nepřiměřené	přiměřený	k2eNgNnSc1d1
úsilí	úsilí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
však	však	k9
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
subjekty	subjekt	k1gInPc1
údajů	údaj	k1gInPc2
informovány	informován	k2eAgInPc1d1
stejně	stejně	k6eAd1
účinným	účinný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
pomocí	pomocí	k7c2
veřejného	veřejný	k2eAgNnSc2d1
oznámení	oznámení	k1gNnSc2
nebo	nebo	k8xC
podobného	podobný	k2eAgNnSc2d1
opatření	opatření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
GDPR	GDPR	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Podle	podle	k7c2
zjištění	zjištění	k1gNnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
nebyla	být	k5eNaImAgFnS
ještě	ještě	k6eAd1
koncem	koncem	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
připravenost	připravenost	k1gFnSc1
podnikatelů	podnikatel	k1gMnPc2
na	na	k7c6
GDPR	GDPR	kA
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
někteří	některý	k3yIgMnPc1
o	o	k7c6
nové	nový	k2eAgFnSc6d1
povinnosti	povinnost	k1gFnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
ochrany	ochrana	k1gFnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
nejspíše	nejspíše	k9
ani	ani	k9
nevěděli	vědět	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Zavedení	zavedení	k1gNnSc1
GDPR	GDPR	kA
by	by	kYmCp3nP
u	u	k7c2
živnostníků	živnostník	k1gMnPc2
nemělo	mít	k5eNaImAgNnS
trvat	trvat	k5eAaImF
déle	dlouho	k6eAd2
než	než	k8xS
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
školy	škola	k1gFnPc4
zavedení	zavedení	k1gNnSc4
GDPR	GDPR	kA
znamená	znamenat	k5eAaImIp3nS
mít	mít	k5eAaImF
dobře	dobře	k6eAd1
udělený	udělený	k2eAgInSc4d1
souhlas	souhlas	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
pokud	pokud	k8xS
někteří	některý	k3yIgMnPc1
rodiče	rodič	k1gMnPc1
souhlas	souhlas	k1gInSc4
nedají	dát	k5eNaPmIp3nP
<g/>
,	,	kIx,
omezí	omezit	k5eAaPmIp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
možnost	možnost	k1gFnSc1
prezentace	prezentace	k1gFnSc2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
nutné	nutný	k2eAgNnSc1d1
používat	používat	k5eAaImF
začerňování	začerňování	k1gNnSc4
a	a	k8xC
některé	některý	k3yIgFnPc4
třídy	třída	k1gFnPc4
nebudou	být	k5eNaImBp3nP
moci	moct	k5eAaImF
být	být	k5eAaImF
prezentovány	prezentovat	k5eAaBmNgFnP
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Souhlas	souhlas	k1gInSc1
rodičů	rodič	k1gMnPc2
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
<g />
.	.	kIx.
</s>
<s hack="1">
být	být	k5eAaImF
nutné	nutný	k2eAgFnPc1d1
vyžadovat	vyžadovat	k5eAaImF
například	například	k6eAd1
v	v	k7c6
situacích	situace	k1gFnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dítě	dítě	k1gNnSc1
vyhraje	vyhrát	k5eAaPmIp3nS
matematickou	matematický	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
a	a	k8xC
nechá	nechat	k5eAaPmIp3nS
se	se	k3xPyFc4
fotografovat	fotografovat	k5eAaImF
<g/>
,	,	kIx,
souhlas	souhlas	k1gInSc1
zákonného	zákonný	k2eAgMnSc2d1
zástupce	zástupce	k1gMnSc2
se	s	k7c7
zveřejněním	zveřejnění	k1gNnSc7
výsledků	výsledek	k1gInPc2
soutěže	soutěž	k1gFnSc2
a	a	k8xC
fotografií	fotografia	k1gFnPc2
výherce	výherce	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Ochrana	ochrana	k1gFnSc1
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
ve	v	k7c6
školách	škola	k1gFnPc6
se	se	k3xPyFc4
netýká	týkat	k5eNaImIp3nS
pouze	pouze	k6eAd1
jmen	jméno	k1gNnPc2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
týká	týkat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
jmen	jméno	k1gNnPc2
jejich	jejich	k3xOp3gMnPc2
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Pokuta	pokuta	k1gFnSc1
za	za	k7c4
porušení	porušení	k1gNnSc4
GDPR	GDPR	kA
zohledňuje	zohledňovat	k5eAaImIp3nS
instituci	instituce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
jej	on	k3xPp3gMnSc4
porušila	porušit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c4
zpracování	zpracování	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
umožňuje	umožňovat	k5eAaImIp3nS
udělit	udělit	k5eAaPmF
souhlas	souhlas	k1gInSc4
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
údajů	údaj	k1gInPc2
bez	bez	k7c2
souhlasů	souhlas	k1gInPc2
rodičů	rodič	k1gMnPc2
dětem	dítě	k1gFnPc3
od	od	k7c2
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Původní	původní	k2eAgFnSc1d1
předloha	předloha	k1gFnSc1
počítala	počítat	k5eAaImAgFnS
s	s	k7c7
hranicí	hranice	k1gFnSc7
13	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
byla	být	k5eAaImAgFnS
zvýšena	zvýšit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Odbory	odbor	k1gInPc1
a	a	k8xC
zaměstnavatelské	zaměstnavatelský	k2eAgInPc1d1
svazy	svaz	k1gInPc1
požadovaly	požadovat	k5eAaImAgInP
hranici	hranice	k1gFnSc4
16	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
v	v	k7c6
nemocnicích	nemocnice	k1gFnPc6
<g/>
,	,	kIx,
školách	škola	k1gFnPc6
a	a	k8xC
obcích	obec	k1gFnPc6
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
hlídat	hlídat	k5eAaImF
speciální	speciální	k2eAgMnSc1d1
pověřenec	pověřenec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
koncem	koncem	k7c2
září	září	k1gNnSc2
2017	#num#	k4
obcím	obec	k1gFnPc3
<g/>
,	,	kIx,
úřadům	úřad	k1gInPc3
a	a	k8xC
firmám	firma	k1gFnPc3
nebylo	být	k5eNaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bude	být	k5eAaImBp3nS
práce	práce	k1gFnSc1
pověřence	pověřenec	k1gMnSc2
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
jestli	jestli	k8xS
jich	on	k3xPp3gMnPc2
bude	být	k5eAaImBp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dostatek	dostatek	k1gInSc1
<g/>
,	,	kIx,
kolik	kolik	k9
budou	být	k5eAaImBp3nP
stát	stát	k5eAaImF,k5eAaPmF
a	a	k8xC
kde	kde	k6eAd1
na	na	k7c4
ně	on	k3xPp3gNnSc4
vezmou	vzít	k5eAaPmIp3nP
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konzervativní	konzervativní	k2eAgInSc1d1
odhad	odhad	k1gInSc1
nákladů	náklad	k1gInPc2
na	na	k7c4
provoz	provoz	k1gInSc4
pověřenců	pověřenec	k1gMnPc2
je	být	k5eAaImIp3nS
600	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
náklady	náklad	k1gInPc4
bude	být	k5eAaImBp3nS
potřeba	potřeba	k1gFnSc1
vynaložit	vynaložit	k5eAaPmF
na	na	k7c4
vstupní	vstupní	k2eAgFnPc4d1
analýzy	analýza	k1gFnPc4
<g/>
,	,	kIx,
nové	nový	k2eAgInPc4d1
počítače	počítač	k1gInPc4
pro	pro	k7c4
pověřence	pověřenec	k1gMnPc4
a	a	k8xC
programové	programový	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
hrozí	hrozit	k5eAaImIp3nS
nedostatek	nedostatek	k1gInSc1
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
jeden	jeden	k4xCgMnSc1
specialista	specialista	k1gMnSc1
může	moct	k5eAaImIp3nS
pracovat	pracovat	k5eAaImF
pro	pro	k7c4
více	hodně	k6eAd2
radnic	radnice	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
větší	veliký	k2eAgFnSc2d2
obce	obec	k1gFnSc2
si	se	k3xPyFc3
s	s	k7c7
jedním	jeden	k4xCgMnSc7
pověřencem	pověřenec	k1gMnSc7
nevystačí	vystačit	k5eNaBmIp3nS
a	a	k8xC
budou	být	k5eAaImBp3nP
jich	on	k3xPp3gInPc2
potřebovat	potřebovat	k5eAaImF
více	hodně	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
doporučilo	doporučit	k5eAaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jeden	jeden	k4xCgMnSc1
pověřenec	pověřenec	k1gMnSc1
pracoval	pracovat	k5eAaImAgMnS
nejvýše	vysoce	k6eAd3,k6eAd1
pro	pro	k7c4
deset	deset	k4xCc4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OVM	OVM	kA
pokuty	pokuta	k1gFnPc1
neplatí	platit	k5eNaImIp3nP
</s>
<s>
Orgánu	orgán	k1gMnSc3
veřejné	veřejný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
veřejnému	veřejný	k2eAgInSc3d1
subjektu	subjekt	k1gInSc3
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
uložen	uložit	k5eAaPmNgInS
správní	správní	k2eAgInSc1d1
trest	trest	k1gInSc1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
porušení	porušení	k1gNnSc3
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dáno	dán	k2eAgNnSc1d1
§	§	k?
62	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
110	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Úřad	úřad	k1gInSc1
upustí	upustit	k5eAaPmIp3nS
od	od	k7c2
uložení	uložení	k1gNnSc2
správního	správní	k2eAgInSc2d1
trestu	trest	k1gInSc2
také	také	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
<g/>
-li	-li	k?
o	o	k7c4
správce	správce	k1gMnSc4
a	a	k8xC
zpracovatele	zpracovatel	k1gMnSc4
uvedené	uvedený	k2eAgFnPc4d1
v	v	k7c6
čl	čl	kA
<g/>
.	.	kIx.
83	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
7	#num#	k4
nařízení	nařízení	k1gNnPc4
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
679	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
83	#num#	k4
odst	odstum	k1gNnPc2
<g/>
.	.	kIx.
7	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
...	...	k?
<g/>
každý	každý	k3xTgInSc1
členský	členský	k2eAgInSc1d1
stát	stát	k1gInSc1
může	moct	k5eAaImIp3nS
stanovit	stanovit	k5eAaPmF
pravidla	pravidlo	k1gNnPc4
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
a	a	k8xC
do	do	k7c2
jaké	jaký	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
míry	míra	k1gFnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
ukládat	ukládat	k5eAaImF
správní	správní	k2eAgFnPc4d1
pokuty	pokuta	k1gFnPc4
orgánům	orgán	k1gInPc3
veřejné	veřejný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
veřejným	veřejný	k2eAgInPc3d1
subjektům	subjekt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Takto	takto	k6eAd1
například	například	k6eAd1
ÚOOÚ	ÚOOÚ	kA
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
nemohl	moct	k5eNaImAgMnS
udělit	udělit	k5eAaPmF
pokutu	pokuta	k1gFnSc4
Ministerstvu	ministerstvo	k1gNnSc3
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
to	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
celkem	celkem	k6eAd1
88	#num#	k4
000	#num#	k4
neoprávněných	oprávněný	k2eNgInPc2d1
přístupů	přístup	k1gInPc2
k	k	k7c3
údajům	údaj	k1gInPc3
v	v	k7c6
registru	registr	k1gInSc6
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NAŘÍZENÍ	nařízení	k1gNnSc1
EVROPSKÉHO	evropský	k2eAgInSc2d1
PARLAMENTU	parlament	k1gInSc2
A	a	k8xC
RADY	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
679	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
pdf	pdf	k?
<g/>
,	,	kIx,
html	html	k1gInSc1
<g/>
↑	↑	k?
ŠALAMON	ŠALAMON	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravte	připravit	k5eAaPmRp2nP
se	se	k3xPyFc4
na	na	k7c4
GDPR	GDPR	kA
-	-	kIx~
Osobní	osobní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
jsou	být	k5eAaImIp3nP
všude	všude	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Incomaker	Incomaker	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NEŠPŮREK	NEŠPŮREK	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc1
Breyer	Breyra	k1gFnPc2
a	a	k8xC
dynamická	dynamický	k2eAgFnSc1d1
IP	IP	kA
adresa	adresa	k1gFnSc1
jako	jako	k8xC,k8xS
osobní	osobní	k2eAgInSc1d1
údaj	údaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právní	právní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LOBOTKA	LOBOTKA	kA
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Způsob	způsob	k1gInSc1
vystupování	vystupování	k1gNnSc2
advokáta	advokát	k1gMnSc2
v	v	k7c6
soudním	soudní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
je	být	k5eAaImIp3nS
osobním	osobní	k2eAgInSc7d1
údajem	údaj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
SMART	SMART	kA
LAW	LAW	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-08	2019-02-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rozsudek	rozsudek	k1gInSc4
Krajského	krajský	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
ze	z	k7c2
dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
j.	j.	k?
31	#num#	k4
A	a	k8xC
68	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
177	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://echo24.cz/a/S9u4h/prvni-den-gdpr-facebook-a-google-celi-zalobe-za-200-miliard	https://echo24.cz/a/S9u4h/prvni-den-gdpr-facebook-a-google-celi-zalobe-za-200-miliard	k1gInSc1
-	-	kIx~
První	první	k4xOgInSc1
den	den	k1gInSc1
GDPR	GDPR	kA
<g/>
:	:	kIx,
Facebook	Facebook	k1gInSc1
a	a	k8xC
Google	Google	k1gFnPc1
čelí	čelit	k5eAaImIp3nP
žalobě	žaloba	k1gFnSc3
za	za	k7c4
200	#num#	k4
miliard	miliarda	k4xCgFnPc2
<g/>
↑	↑	k?
https://phys.org/news/2018-06-facebook-google-users-eu-law.html	https://phys.org/news/2018-06-facebook-google-users-eu-law.htmla	k1gFnPc2
-	-	kIx~
Facebook	Facebook	k1gInSc1
<g/>
,	,	kIx,
Google	Google	k1gNnSc1
'	'	kIx"
<g/>
manipulate	manipulat	k1gMnSc5
<g/>
'	'	kIx"
users	users	k6eAd1
to	ten	k3xDgNnSc1
share	shar	k1gInSc5
data	datum	k1gNnPc4
despite	despit	k1gInSc5
EU	EU	kA
law	law	k?
<g/>
:	:	kIx,
study	stud	k1gInPc1
<g/>
↑	↑	k?
LOBOTKA	LOBOTKA	kA
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohlašování	ohlašování	k1gNnSc4
porušení	porušení	k1gNnSc2
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SMART	SMART	kA
LAW	LAW	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-17	2020-02-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Formulář	formulář	k1gInSc1
ohlášení	ohlášení	k1gNnSc2
porušení	porušení	k1gNnSc2
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
dle	dle	k7c2
GDPR	GDPR	kA
<g/>
..	..	k?
www.uoou.cz	www.uoou.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LOBOTKA	LOBOTKA	kA
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinnost	povinnost	k1gFnSc1
oznámit	oznámit	k5eAaPmF
porušení	porušení	k1gNnSc4
zabezpečení	zabezpečení	k1gNnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
subjektům	subjekt	k1gInPc3
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SMART	SMART	kA
LAW	LAW	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-24	2020-02-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
GDPR	GDPR	kA
se	se	k3xPyFc4
dotkne	dotknout	k5eAaPmIp3nS
i	i	k9
živnostníků	živnostník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
neví	vědět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24.11	24.11	k4
<g/>
.2017	.2017	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
-jan-	-jan-	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Školy	škola	k1gFnPc1
se	se	k3xPyFc4
s	s	k7c7
obavami	obava	k1gFnPc7
připravují	připravovat	k5eAaImIp3nP
na	na	k7c4
novou	nový	k2eAgFnSc4d1
směrnici	směrnice	k1gFnSc4
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21.03	21.03	k4
<g/>
.2018	.2018	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GDPR	GDPR	kA
stručně	stručně	k6eAd1
<g/>
:	:	kIx,
Úřad	úřad	k1gInSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
www.uoou.cz	www.uoou.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Souhlas	souhlas	k1gInSc1
se	s	k7c7
zpracováním	zpracování	k1gNnSc7
údajů	údaj	k1gInPc2
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
děti	dítě	k1gFnPc1
smět	smět	k5eAaImF
dát	dát	k5eAaPmF
od	od	k7c2
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21.03	21.03	k4
<g/>
.2018	.2018	k4
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
CECHL	CECHL	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
BAROCH	BAROCH	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
příkaz	příkaz	k1gInSc1
z	z	k7c2
Bruselu	Brusel	k1gInSc2
<g/>
:	:	kIx,
strážce	strážce	k1gMnSc1
dat	datum	k1gNnPc2
v	v	k7c6
každé	každý	k3xTgFnSc6
vsi	ves	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22.09	22.09	k4
<g/>
.2017	.2017	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
o	o	k7c4
zpracování	zpracování	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
↑	↑	k?
ÚOOÚ	ÚOOÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ÚOOÚ	ÚOOÚ	kA
nemohl	moct	k5eNaImAgMnS
udělit	udělit	k5eAaPmF
pokutu	pokuta	k1gFnSc4
ministerstvu	ministerstvo	k1gNnSc3
<g/>
,	,	kIx,
neumožňuje	umožňovat	k5eNaImIp3nS
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
zákon	zákon	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
9.8	9.8	k4
<g/>
.2019	.2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
gdprinfo	gdprinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
<g/>
,	,	kIx,
Obecné	obecný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
</s>
<s>
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
o	o	k7c6
GDPR	GDPR	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
EU	EU	kA
</s>
<s>
GDPR	GDPR	kA
návod	návod	k1gInSc1
pro	pro	k7c4
malé	malý	k2eAgMnPc4d1
podnikatele	podnikatel	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
GDPR	GDPR	kA
a	a	k8xC
kybernetický	kybernetický	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
:	:	kIx,
Strašák	strašák	k1gMnSc1
nebo	nebo	k8xC
pomocník	pomocník	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
46	#num#	k4
otázek	otázka	k1gFnPc2
a	a	k8xC
odpovědí	odpověď	k1gFnPc2
ke	k	k7c3
GDPR	GDPR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
GDPR	GDPR	kA
certifikace	certifikace	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
zásady	zásada	k1gFnPc1
GDPR	GDPR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Právo	právo	k1gNnSc1
|	|	kIx~
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1105568555	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2017007011	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
24146824948007631105	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2017007011	#num#	k4
</s>
