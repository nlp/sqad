<s>
Obecné	obecný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
ONOOÚ	ONOOÚ	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
GDPR	GDPR	kA
<g/>
,	,	kIx,
General	General	k1gMnSc1
Data	datum	k1gNnSc2
Protection	Protection	k1gInSc1
Regulation	Regulation	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Nařízení	nařízení	k1gNnSc2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
č.	č.	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
679	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
o	o	k7c6
ochraně	ochrana	k1gFnSc6
<g />
.	.	kIx.
</s>