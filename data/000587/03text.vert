<s>
Mělník	Mělník	k1gInSc1	Mělník
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
30	[number]	k4	30
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
naproti	naproti	k7c3	naproti
ústí	ústí	k1gNnSc3	ústí
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
;	;	kIx,	;
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
vyvýšenině	vyvýšenina	k1gFnSc6	vyvýšenina
obtékané	obtékaný	k2eAgFnSc6d1	obtékaná
potokem	potok	k1gInSc7	potok
Pšovkou	Pšovka	k1gFnSc7	Pšovka
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Mělník	Mělník	k1gInSc1	Mělník
je	být	k5eAaImIp3nS	být
osídleno	osídlen	k2eAgNnSc1d1	osídleno
už	už	k6eAd1	už
od	od	k7c2	od
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mělnicka	Mělnicko	k1gNnSc2	Mělnicko
se	se	k3xPyFc4	se
také	také	k9	také
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
některé	některý	k3yIgInPc1	některý
historické	historický	k2eAgInPc1d1	historický
romány	román	k1gInPc1	román
Eduarda	Eduard	k1gMnSc2	Eduard
Štorcha	Štorch	k1gMnSc2	Štorch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starších	starý	k2eAgInPc2d2	starší
historiografických	historiografický	k2eAgInPc2d1	historiografický
názorů	názor	k1gInPc2	názor
byl	být	k5eAaImAgInS	být
Mělník	Mělník	k1gInSc1	Mělník
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
centrem	centrum	k1gNnSc7	centrum
slovanského	slovanský	k2eAgInSc2d1	slovanský
kmene	kmen	k1gInSc2	kmen
Pšovanů	Pšovan	k1gMnPc2	Pšovan
-	-	kIx~	-
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
hradiště	hradiště	k1gNnSc1	hradiště
Pšov	Pšov	k1gInSc1	Pšov
<g/>
.	.	kIx.	.
</s>
<s>
Mělník	Mělník	k1gInSc1	Mělník
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
slovanské	slovanský	k2eAgNnSc1d1	slovanské
pojmenování	pojmenování	k1gNnSc1	pojmenování
kopce	kopec	k1gInSc2	kopec
z	z	k7c2	z
mělnících	mělnící	k2eAgFnPc2d1	mělnící
se	se	k3xPyFc4	se
hornin	hornina	k1gFnPc2	hornina
bočního	boční	k2eAgInSc2d1	boční
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
osídlení	osídlení	k1gNnSc2	osídlení
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kosmy	Kosma	k1gMnSc2	Kosma
byl	být	k5eAaImAgInS	být
zdejší	zdejší	k2eAgInSc1d1	zdejší
kmen	kmen	k1gInSc1	kmen
Pšovanů	Pšovan	k1gMnPc2	Pšovan
připojen	připojit	k5eAaPmNgInS	připojit
ke	k	k7c3	k
knížectví	knížectví	k1gNnSc3	knížectví
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
sňatkem	sňatek	k1gInSc7	sňatek
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
s	s	k7c7	s
Ludmilou	Ludmila	k1gFnSc7	Ludmila
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
posledního	poslední	k2eAgNnSc2d1	poslední
pšovského	pšovský	k2eAgNnSc2d1	pšovský
knížete	kníže	k1gNnSc2wR	kníže
Slavibora	Slavibora	k1gFnSc1	Slavibora
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Pšov	Pšov	k1gInSc1	Pšov
nahradil	nahradit	k5eAaPmAgInS	nahradit
koncem	koncem	k7c2	koncem
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nový	nový	k2eAgInSc4d1	nový
kamenný	kamenný	k2eAgInSc4d1	kamenný
hrad	hrad	k1gInSc4	hrad
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Emma	Emma	k1gFnSc1	Emma
zde	zde	k6eAd1	zde
razila	razit	k5eAaImAgFnS	razit
denáry	denár	k1gInPc4	denár
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Emma	Emma	k1gFnSc1	Emma
regina	regina	k1gMnSc1	regina
-	-	kIx~	-
civitas	civitas	k1gInSc1	civitas
Melnic	Melnice	k1gFnPc2	Melnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
Mělníku	Mělník	k1gInSc6	Mělník
pěstovat	pěstovat	k5eAaImF	pěstovat
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
se	se	k3xPyFc4	se
na	na	k7c6	na
Mělníku	Mělník	k1gInSc6	Mělník
tradoval	tradovat	k5eAaImAgInS	tradovat
zvyk	zvyk	k1gInSc1	zvyk
dávat	dávat	k5eAaImF	dávat
hrad	hrad	k1gInSc4	hrad
kněžnám	kněžna	k1gFnPc3	kněžna
a	a	k8xC	a
královnám	královna	k1gFnPc3	královna
věnem	věno	k1gNnSc7	věno
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
město	město	k1gNnSc4	město
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
(	(	kIx(	(
<g/>
královské	královský	k2eAgNnSc1d1	královské
věnné	věnný	k2eAgNnSc1d1	věnné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Mělník	Mělník	k1gInSc1	Mělník
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c4	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
podhradí	podhradí	k1gNnSc2	podhradí
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
trhové	trhový	k2eAgFnSc2d1	trhová
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Zakládací	zakládací	k2eAgFnSc4d1	zakládací
listinu	listina	k1gFnSc4	listina
město	město	k1gNnSc4	město
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
první	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Mělník	Mělník	k1gInSc1	Mělník
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1274	[number]	k4	1274
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byl	být	k5eAaImAgMnS	být
Mělnickým	mělnický	k2eAgMnSc7d1	mělnický
darován	darován	k2eAgInSc4d1	darován
k	k	k7c3	k
dobudování	dobudování	k1gNnSc3	dobudování
obce	obec	k1gFnSc2	obec
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
výnosu	výnos	k1gInSc6	výnos
labského	labský	k2eAgInSc2d1	labský
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
Mělník	Mělník	k1gInSc1	Mělník
založeno	založen	k2eAgNnSc4d1	založeno
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
způsobu	způsob	k1gInSc6	způsob
jinozemských	jinozemský	k2eAgNnPc2d1	jinozemský
měst	město	k1gNnPc2	město
za	za	k7c2	za
krále	král	k1gMnSc4	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
r.	r.	kA	r.
1274	[number]	k4	1274
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohot	tohota	k1gFnPc2	tohota
času	čas	k1gInSc2	čas
nyzýváno	nyzýván	k2eAgNnSc1d1	nyzýván
bylo	být	k5eAaImAgNnS	být
městem	město	k1gNnSc7	město
královským	královský	k2eAgNnSc7d1	královské
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Přemysla	Přemysl	k1gMnSc2	Přemysl
II	II	kA	II
<g/>
.	.	kIx.	.
náleželo	náležet	k5eAaImAgNnS	náležet
královně	královna	k1gFnSc3	královna
Kunhutě	Kunhuta	k1gFnSc3	Kunhuta
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ho	on	k3xPp3gNnSc4	on
odtud	odtud	k6eAd1	odtud
od	od	k7c2	od
královen	královna	k1gFnPc2	královna
českých	český	k2eAgFnPc2d1	Česká
napořád	napořád	k6eAd1	napořád
jako	jako	k9	jako
věna	věno	k1gNnSc2	věno
užíváno	užíván	k2eAgNnSc1d1	užíváno
<g/>
.	.	kIx.	.
</s>
<s>
Mělník	Mělník	k1gInSc1	Mělník
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
natrvalo	natrvalo	k6eAd1	natrvalo
za	za	k7c4	za
královské	královský	k2eAgNnSc4d1	královské
věnné	věnný	k2eAgNnSc4d1	věnné
město	město	k1gNnSc4	město
až	až	k9	až
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sem	sem	k6eAd1	sem
dal	dát	k5eAaPmAgMnS	dát
přivézt	přivézt	k5eAaPmF	přivézt
z	z	k7c2	z
Burgundska	Burgundsko	k1gNnSc2	Burgundsko
a	a	k8xC	a
z	z	k7c2	z
Champagne	Champagn	k1gMnSc5	Champagn
vinnou	vinný	k2eAgFnSc4d1	vinná
révu	réva	k1gFnSc4	réva
a	a	k8xC	a
zvelebil	zvelebit	k5eAaPmAgInS	zvelebit
tak	tak	k6eAd1	tak
mělnické	mělnický	k2eAgNnSc4d1	Mělnické
vinařství	vinařství	k1gNnSc4	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
zůstával	zůstávat	k5eAaImAgInS	zůstávat
Mělník	Mělník	k1gInSc1	Mělník
ve	v	k7c6	v
spolku	spolek	k1gInSc6	spolek
s	s	k7c7	s
Pražany	Pražan	k1gMnPc7	Pražan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
město	město	k1gNnSc1	město
hospodářsky	hospodářsky	k6eAd1	hospodářsky
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
se	se	k3xPyFc4	se
Mělník	Mělník	k1gInSc1	Mělník
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
královny	královna	k1gFnSc2	královna
vdovy	vdova	k1gFnSc2	vdova
Johanky	Johanka	k1gFnSc2	Johanka
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
darovala	darovat	k5eAaPmAgFnS	darovat
zdejší	zdejší	k2eAgInSc4d1	zdejší
kapitule	kapitula	k1gFnSc6	kapitula
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
vskutku	vskutku	k9	vskutku
královský	královský	k2eAgInSc4d1	královský
dar	dar	k1gInSc4	dar
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
1,5	[number]	k4	1,5
tuny	tuna	k1gFnSc2	tuna
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
přála	přát	k5eAaImAgFnS	přát
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
pochována	pochován	k2eAgFnSc1d1	pochována
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1475	[number]	k4	1475
na	na	k7c6	na
Mělníce	Mělník	k1gInSc6	Mělník
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
probíhat	probíhat	k5eAaImF	probíhat
velké	velký	k2eAgFnPc4d1	velká
přestavby	přestavba	k1gFnPc4	přestavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
převezl	převézt	k5eAaPmAgMnS	převézt
ostatky	ostatek	k1gInPc4	ostatek
královny	královna	k1gFnSc2	královna
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc4	město
stagnovalo	stagnovat	k5eAaImAgNnS	stagnovat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
upadalo	upadat	k5eAaImAgNnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
zejména	zejména	k9	zejména
po	po	k7c6	po
potlačeném	potlačený	k2eAgInSc6d1	potlačený
protihabsburském	protihabsburský	k2eAgInSc6d1	protihabsburský
odboji	odboj	k1gInSc6	odboj
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
a	a	k8xC	a
části	část	k1gFnSc2	část
šlechty	šlechta	k1gFnSc2	šlechta
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc1d1	habsburský
zabavil	zabavit	k5eAaPmAgInS	zabavit
městu	město	k1gNnSc3	město
všechen	všechen	k3xTgInSc4	všechen
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
postihl	postihnout	k5eAaPmAgInS	postihnout
ho	on	k3xPp3gMnSc4	on
citelnou	citelný	k2eAgFnSc7d1	citelná
finanční	finanční	k2eAgFnSc7d1	finanční
pokutou	pokuta	k1gFnSc7	pokuta
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
cechy	cech	k1gInPc4	cech
<g/>
,	,	kIx,	,
dosadil	dosadit	k5eAaPmAgMnS	dosadit
do	do	k7c2	do
města	město	k1gNnSc2	město
císařského	císařský	k2eAgMnSc2d1	císařský
rychtáře	rychtář	k1gMnSc2	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
od	od	k7c2	od
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zlatým	zlatý	k2eAgInSc7d1	zlatý
věkem	věk	k1gInSc7	věk
pro	pro	k7c4	pro
mělnické	mělnický	k2eAgMnPc4d1	mělnický
vinaře	vinař	k1gMnPc4	vinař
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřovaly	rozšiřovat	k5eAaImAgFnP	rozšiřovat
se	se	k3xPyFc4	se
vinice	vinice	k1gFnPc1	vinice
<g/>
,	,	kIx,	,
bohatí	bohatý	k2eAgMnPc1d1	bohatý
měšťané	měšťan	k1gMnPc1	měšťan
financovali	financovat	k5eAaBmAgMnP	financovat
stavbu	stavba	k1gFnSc4	stavba
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
a	a	k8xC	a
na	na	k7c6	na
Chloumku	chloumek	k1gInSc6	chloumek
kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
i	i	k9	i
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc4d1	častý
pobyty	pobyt	k1gInPc4	pobyt
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
švédská	švédský	k2eAgFnSc1d1	švédská
okupace	okupace	k1gFnSc1	okupace
<g/>
,	,	kIx,	,
drancování	drancování	k1gNnPc1	drancování
<g/>
,	,	kIx,	,
požáry	požár	k1gInPc1	požár
<g/>
,	,	kIx,	,
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Odchodem	odchod	k1gInSc7	odchod
mnoha	mnoho	k4c2	mnoho
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
vinařů	vinař	k1gMnPc2	vinař
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
téměř	téměř	k6eAd1	téměř
vylidnilo	vylidnit	k5eAaPmAgNnS	vylidnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Mělník	Mělník	k1gInSc4	Mělník
ničily	ničit	k5eAaImAgInP	ničit
požáry	požár	k1gInPc1	požár
<g/>
,	,	kIx,	,
morové	morový	k2eAgFnPc1d1	morová
epidemie	epidemie	k1gFnPc1	epidemie
<g/>
,	,	kIx,	,
selská	selský	k2eAgNnPc1d1	selské
povstání	povstání	k1gNnPc1	povstání
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
neúrodných	úrodný	k2eNgNnPc2d1	neúrodné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Mělník	Mělník	k1gInSc1	Mělník
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
zemědělského	zemědělský	k2eAgNnSc2d1	zemědělské
zázemí	zázemí	k1gNnSc2	zázemí
pro	pro	k7c4	pro
pražskou	pražský	k2eAgFnSc4d1	Pražská
aglomeraci	aglomerace	k1gFnSc4	aglomerace
a	a	k8xC	a
také	také	k9	také
průchozí	průchozí	k2eAgFnSc7d1	průchozí
stanicí	stanice	k1gFnSc7	stanice
dálkového	dálkový	k2eAgInSc2d1	dálkový
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k6eAd1	málo
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
dotkla	dotknout	k5eAaPmAgFnS	dotknout
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgNnSc1d1	někdejší
královské	královský	k2eAgNnSc1d1	královské
věnné	věnný	k2eAgNnSc1d1	věnné
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
provinciální	provinciální	k2eAgNnSc4d1	provinciální
městečko	městečko	k1gNnSc4	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Továrny	továrna	k1gFnPc1	továrna
zde	zde	k6eAd1	zde
vznikaly	vznikat	k5eAaImAgFnP	vznikat
jen	jen	k9	jen
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
byly	být	k5eAaImAgInP	být
trhy	trh	k1gInPc1	trh
<g/>
,	,	kIx,	,
jarmarky	jarmarky	k?	jarmarky
<g/>
,	,	kIx,	,
živnosti	živnost	k1gFnPc4	živnost
<g/>
,	,	kIx,	,
řemesla	řemeslo	k1gNnPc4	řemeslo
a	a	k8xC	a
tradiční	tradiční	k2eAgNnPc4d1	tradiční
vinařství	vinařství	k1gNnPc4	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Bachova	Bachův	k2eAgInSc2d1	Bachův
absolutismu	absolutismus	k1gInSc2	absolutismus
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
aktivita	aktivita	k1gFnSc1	aktivita
různých	různý	k2eAgFnPc2d1	různá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
hasičský	hasičský	k2eAgInSc1d1	hasičský
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
Sokol	Sokol	k1gInSc1	Sokol
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
veslařů	veslař	k1gMnPc2	veslař
mělnických	mělnické	k1gNnPc2	mělnické
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
velocipedistů	velocipedista	k1gMnPc2	velocipedista
<g/>
,	,	kIx,	,
Zpěvácký	zpěvácký	k2eAgInSc1d1	zpěvácký
spolek	spolek	k1gInSc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
první	první	k4xOgInSc4	první
místní	místní	k2eAgInSc4d1	místní
týdeník	týdeník	k1gInSc4	týdeník
<g/>
.	.	kIx.	.
</s>
<s>
Výhodná	výhodný	k2eAgFnSc1d1	výhodná
poloha	poloha	k1gFnSc1	poloha
Mělníka	Mělník	k1gInSc2	Mělník
přinesla	přinést	k5eAaPmAgFnS	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
rozvoj	rozvoj	k1gInSc4	rozvoj
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
komunikací	komunikace	k1gFnPc2	komunikace
-	-	kIx~	-
první	první	k4xOgInSc1	první
parník	parník	k1gInSc1	parník
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
most	most	k1gInSc4	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přístavní	přístavní	k2eAgNnSc1d1	přístavní
překladiště	překladiště	k1gNnSc1	překladiště
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vltavský	vltavský	k2eAgInSc1d1	vltavský
kanál	kanál	k1gInSc1	kanál
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
používal	používat	k5eAaImAgInS	používat
telefon	telefon	k1gInSc4	telefon
a	a	k8xC	a
telegraf	telegraf	k1gInSc4	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
podnikání	podnikání	k1gNnPc2	podnikání
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgInS	vynutit
i	i	k9	i
vznik	vznik	k1gInSc1	vznik
peněžních	peněžní	k2eAgInPc2d1	peněžní
ústavů	ústav	k1gInPc2	ústav
-	-	kIx~	-
Občanské	občanský	k2eAgFnPc1d1	občanská
záložny	záložna	k1gFnPc1	záložna
<g/>
,	,	kIx,	,
spořitelny	spořitelna	k1gFnPc1	spořitelna
<g/>
,	,	kIx,	,
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
záložny	záložna	k1gFnPc1	záložna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
Mělníku	Mělník	k1gInSc6	Mělník
stále	stále	k6eAd1	stále
vedlo	vést	k5eAaImAgNnS	vést
zemědělství	zemědělství	k1gNnSc4	zemědělství
-	-	kIx~	-
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
se	se	k3xPyFc4	se
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
sladovnický	sladovnický	k2eAgInSc1d1	sladovnický
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
výrobky	výrobek	k1gInPc7	výrobek
se	se	k3xPyFc4	se
čile	čile	k6eAd1	čile
obchodovalo	obchodovat	k5eAaImAgNnS	obchodovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
postaven	postavit	k5eAaPmNgInS	postavit
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
Mělníku	Mělník	k1gInSc6	Mělník
řada	řada	k1gFnSc1	řada
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
-	-	kIx~	-
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
speciální	speciální	k2eAgFnSc1d1	speciální
vinařská	vinařský	k2eAgFnSc1d1	vinařská
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
škola	škola	k1gFnSc1	škola
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
košíkářská	košíkářský	k2eAgFnSc1d1	košíkářská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc4	vývoj
oblasti	oblast	k1gFnSc2	oblast
zastavila	zastavit	k5eAaPmAgFnS	zastavit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
první	první	k4xOgFnPc4	první
republiky	republika	k1gFnPc4	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
-	-	kIx~	-
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
hodně	hodně	k6eAd1	hodně
stavělo	stavět	k5eAaImAgNnS	stavět
-	-	kIx~	-
budovy	budova	k1gFnPc4	budova
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
kulturního	kulturní	k2eAgInSc2d1	kulturní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
stavba	stavba	k1gFnSc1	stavba
velkého	velký	k2eAgInSc2d1	velký
obchodního	obchodní	k2eAgInSc2d1	obchodní
přístavu	přístav	k1gInSc2	přístav
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
<g/>
,	,	kIx,	,
budovaly	budovat	k5eAaImAgFnP	budovat
se	se	k3xPyFc4	se
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
prováděla	provádět	k5eAaImAgFnS	provádět
se	se	k3xPyFc4	se
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
a	a	k8xC	a
1933	[number]	k4	1933
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
slavnosti	slavnost	k1gFnPc4	slavnost
vinobraní	vinobraní	k1gNnSc2	vinobraní
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ze	z	k7c2	z
slavnosti	slavnost	k1gFnSc2	slavnost
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
po	po	k7c6	po
záboru	zábor	k1gInSc6	zábor
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
mělnického	mělnický	k2eAgInSc2d1	mělnický
okresu	okres	k1gInSc2	okres
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
bezprostředního	bezprostřední	k2eAgNnSc2d1	bezprostřední
sousedství	sousedství	k1gNnSc2	sousedství
"	"	kIx"	"
<g/>
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc4	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
Mělníku	Mělník	k1gInSc6	Mělník
oslavili	oslavit	k5eAaPmAgMnP	oslavit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
společně	společně	k6eAd1	společně
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
sovětské	sovětský	k2eAgFnSc2d1	sovětská
a	a	k8xC	a
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
pomník	pomník	k1gInSc4	pomník
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
GEN.	gen.	kA	gen.
KAROL	KAROL	kA	KAROL
ŚWIERCZEWSKI	ŚWIERCZEWSKI	kA	ŚWIERCZEWSKI
<g/>
"	"	kIx"	"
v	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
poblíž	poblíž	k7c2	poblíž
pošty	pošta	k1gFnSc2	pošta
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
třeba	třeba	k6eAd1	třeba
organizace	organizace	k1gFnSc1	organizace
Cech	cech	k1gInSc1	cech
českých	český	k2eAgMnPc2d1	český
vinařů	vinař	k1gMnPc2	vinař
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Mělník	Mělník	k1gInSc1	Mělník
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
Borek	borek	k1gInSc1	borek
<g/>
,	,	kIx,	,
Chloumek	chloumek	k1gInSc1	chloumek
<g/>
,	,	kIx,	,
Mlazice	Mlazice	k1gFnSc1	Mlazice
<g/>
,	,	kIx,	,
Pšovka	Pšovka	k1gFnSc1	Pšovka
<g/>
,	,	kIx,	,
Rousovice	Rousovice	k1gFnSc1	Rousovice
<g/>
,	,	kIx,	,
11549	[number]	k4	11549
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
Instituce	instituce	k1gFnSc1	instituce
<g/>
:	:	kIx,	:
okresní	okresní	k2eAgInSc1d1	okresní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
berní	berní	k2eAgFnSc1d1	berní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
berní	berní	k2eAgInSc1d1	berní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
celní	celní	k2eAgInSc1d1	celní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgInSc1d1	katastrální
měřičský	měřičský	k2eAgInSc1d1	měřičský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
důchodkový	důchodkový	k2eAgInSc1d1	důchodkový
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
cejchovní	cejchovní	k2eAgInSc1d1	cejchovní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgNnSc1d1	okresní
četnické	četnický	k2eAgNnSc1d1	četnické
velitelství	velitelství	k1gNnSc1	velitelství
<g/>
,	,	kIx,	,
3	[number]	k4	3
katolické	katolický	k2eAgInPc1d1	katolický
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
československé	československý	k2eAgFnSc2d1	Československá
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc1d1	státní
reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
vinařsko-ovocnicko-zahradnická	vinařskovocnickoahradnický	k2eAgFnSc1d1	vinařsko-ovocnicko-zahradnický
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
košíkářská	košíkářský	k2eAgFnSc1d1	košíkářská
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc1	klášter
kapucínů	kapucín	k1gMnPc2	kapucín
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc4	klášter
milosrdných	milosrdný	k2eAgFnPc2d1	milosrdná
sester	sestra	k1gFnPc2	sestra
<g/>
,	,	kIx,	,
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
veřejná	veřejný	k2eAgFnSc1d1	veřejná
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
chudobinec	chudobinec	k1gInSc1	chudobinec
<g/>
,	,	kIx,	,
sirotčinec	sirotčinec	k1gInSc1	sirotčinec
Živnosti	živnost	k1gFnSc2	živnost
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
obchodní	obchodní	k2eAgNnSc1d1	obchodní
grémium	grémium	k1gNnSc1	grémium
<g/>
,	,	kIx,	,
společenstvo	společenstvo	k1gNnSc1	společenstvo
cukrářů	cukrář	k1gMnPc2	cukrář
<g/>
,	,	kIx,	,
holičů	holič	k1gMnPc2	holič
<g/>
,	,	kIx,	,
hostinských	hostinský	k1gMnPc2	hostinský
a	a	k8xC	a
výčepníků	výčepník	k1gMnPc2	výčepník
<g/>
,	,	kIx,	,
klempířů	klempíř	k1gMnPc2	klempíř
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
košíkářů	košíkář	k1gMnPc2	košíkář
<g/>
,	,	kIx,	,
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
obuvníků	obuvník	k1gMnPc2	obuvník
<g/>
,	,	kIx,	,
pekařů	pekař	k1gMnPc2	pekař
<g/>
,	,	kIx,	,
potravní	potravní	k2eAgInPc4d1	potravní
a	a	k8xC	a
dopravní	dopravní	k2eAgInPc4d1	dopravní
<g/>
,	,	kIx,	,
řezníků	řezník	k1gMnPc2	řezník
<g/>
,	,	kIx,	,
zámečníků	zámečník	k1gMnPc2	zámečník
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc4	cukrovar
České	český	k2eAgFnSc2d1	Česká
akc	akc	k?	akc
<g/>
.	.	kIx.	.
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
cukerní	cukerný	k2eAgMnPc1d1	cukerný
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
továrny	továrna	k1gFnPc1	továrna
na	na	k7c4	na
kaučukové	kaučukový	k2eAgNnSc4d1	kaučukové
zboží	zboží	k1gNnSc4	zboží
Haro	Haro	k6eAd1	Haro
<g/>
,	,	kIx,	,
konzervy	konzerva	k1gFnPc4	konzerva
<g />
.	.	kIx.	.
</s>
<s>
zeleninové	zeleninový	k2eAgInPc1d1	zeleninový
<g/>
,	,	kIx,	,
5	[number]	k4	5
továren	továrna	k1gFnPc2	továrna
na	na	k7c4	na
košíkářské	košíkářský	k2eAgNnSc4d1	košíkářské
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
kůže	kůže	k1gFnSc2	kůže
Kratina	Kratina	k1gMnSc1	Kratina
<g/>
,	,	kIx,	,
květináče	květináč	k1gInSc2	květináč
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
likéry	likér	k1gInPc1	likér
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
3	[number]	k4	3
továrny	továrna	k1gFnPc4	továrna
na	na	k7c4	na
nábytek	nábytek	k1gInSc4	nábytek
proutěný	proutěný	k2eAgInSc4d1	proutěný
<g/>
,	,	kIx,	,
olejna	olejna	k1gFnSc1	olejna
<g/>
,	,	kIx,	,
2	[number]	k4	2
pivovary	pivovar	k1gInPc7	pivovar
(	(	kIx(	(
<g/>
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
Právovarečné	Právovarečný	k2eAgNnSc1d1	Právovarečný
měšťanstvo	měšťanstvo	k1gNnSc1	měšťanstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
sekt	sekt	k1gInSc4	sekt
<g />
.	.	kIx.	.
</s>
<s>
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
vinice	vinice	k1gFnSc1	vinice
(	(	kIx(	(
<g/>
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
Viktorin	Viktorin	k1gInSc1	Viktorin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
cihelny	cihelna	k1gFnPc4	cihelna
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
3	[number]	k4	3
pily	pila	k1gFnSc2	pila
<g/>
,	,	kIx,	,
velkostatek	velkostatek	k1gInSc4	velkostatek
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
Služby	služba	k1gFnSc2	služba
<g/>
:	:	kIx,	:
8	[number]	k4	8
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
2	[number]	k4	2
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
3	[number]	k4	3
zvěrolékaři	zvěrolékař	k1gMnPc7	zvěrolékař
<g/>
,	,	kIx,	,
9	[number]	k4	9
advokátů	advokát	k1gMnPc2	advokát
<g/>
,	,	kIx,	,
2	[number]	k4	2
notáři	notář	k1gMnSc6	notář
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
2	[number]	k4	2
biografy	biograf	k1gInPc1	biograf
<g/>
,	,	kIx,	,
3	[number]	k4	3
fotoateliéry	fotoateliér	k1gInPc1	fotoateliér
<g/>
,	,	kIx,	,
geometr	geometr	k1gInSc1	geometr
<g/>
,	,	kIx,	,
45	[number]	k4	45
hostinců	hostinec	k1gInPc2	hostinec
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc1	hotel
Stádník	Stádník	k1gInSc1	Stádník
<g/>
,	,	kIx,	,
Vykysal	vykysat	k5eAaPmAgMnS	vykysat
<g/>
,	,	kIx,	,
U	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
,	,	kIx,	,
2	[number]	k4	2
lékárny	lékárna	k1gFnPc1	lékárna
<g/>
,	,	kIx,	,
Lidová	lidový	k2eAgFnSc1d1	lidová
záložna	záložna	k1gFnSc1	záložna
na	na	k7c6	na
Mělníce	Mělník	k1gInSc6	Mělník
<g/>
,	,	kIx,	,
Mělnická	mělnický	k2eAgFnSc1d1	Mělnická
spořitelna	spořitelna	k1gFnSc1	spořitelna
<g/>
,	,	kIx,	,
Mělnická	mělnický	k2eAgFnSc1d1	Mělnická
<g />
.	.	kIx.	.
</s>
<s>
záložna	záložna	k1gFnSc1	záložna
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
živnosti	živnost	k1gFnPc4	živnost
<g/>
,	,	kIx,	,
Občanská	občanský	k2eAgFnSc1d1	občanská
záložna	záložna	k1gFnSc1	záložna
na	na	k7c6	na
Mělníku	Mělník	k1gInSc6	Mělník
<g/>
,	,	kIx,	,
Občanská	občanský	k2eAgFnSc1d1	občanská
záložna	záložna	k1gFnSc1	záložna
v	v	k7c6	v
Pšovce	Pšovka	k1gFnSc6	Pšovka
<g/>
,	,	kIx,	,
Okresní	okresní	k2eAgFnSc1d1	okresní
záložna	záložna	k1gFnSc1	záložna
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
na	na	k7c6	na
Mělníce	Mělník	k1gInSc6	Mělník
<g/>
,	,	kIx,	,
Spořitelní	spořitelní	k2eAgInSc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
Blata	Blata	k1gNnPc4	Blata
<g/>
,	,	kIx,	,
Spořitelní	spořitelní	k2eAgInSc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
Mlazice	Mlazice	k1gFnPc4	Mlazice
<g/>
,	,	kIx,	,
Úvěrní	úvěrní	k2eAgNnSc4d1	úvěrní
a	a	k8xC	a
rolnické	rolnický	k2eAgNnSc4d1	rolnické
družstvo	družstvo	k1gNnSc4	družstvo
<g/>
,	,	kIx,	,
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
záložna	záložna	k1gFnSc1	záložna
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
Mělníce	Mělník	k1gInSc6	Mělník
<g/>
,	,	kIx,	,
Živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
záložna	záložna	k1gFnSc1	záložna
na	na	k7c6	na
Mělníce	Mělník	k1gInSc6	Mělník
<g/>
,	,	kIx,	,
3	[number]	k4	3
vinárny	vinárna	k1gFnSc2	vinárna
<g/>
,	,	kIx,	,
3	[number]	k4	3
zubní	zubní	k2eAgInPc1d1	zubní
ateliéry	ateliér	k1gInPc1	ateliér
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Vehlovice	Vehlovice	k1gFnSc2	Vehlovice
(	(	kIx(	(
<g/>
560	[number]	k4	560
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
dlaždič	dlaždič	k1gMnSc1	dlaždič
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kapelník	kapelník	k1gMnSc1	kapelník
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
pekař	pekař	k1gMnSc1	pekař
<g/>
,	,	kIx,	,
2	[number]	k4	2
sadaři	sadař	k1gMnPc7	sadař
<g/>
,	,	kIx,	,
2	[number]	k4	2
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
1850	[number]	k4	1850
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1855	[number]	k4	1855
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1868	[number]	k4	1868
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1939	[number]	k4	1939
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1942	[number]	k4	1942
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1945	[number]	k4	1945
země	zem	k1gFnSc2	zem
česká	český	k2eAgNnPc1d1	české
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc1d1	správní
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Mělník	Mělník	k1gInSc1	Mělník
Dle	dle	k7c2	dle
Palackého	Palackého	k2eAgNnSc2d1	Palackého
díla	dílo	k1gNnSc2	dílo
Popis	popis	k1gInSc4	popis
království	království	k1gNnSc2	království
Českého	český	k2eAgInSc2d1	český
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
Mělník	Mělník	k1gInSc1	Mělník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
199	[number]	k4	199
domů	dům	k1gInPc2	dům
a	a	k8xC	a
1581	[number]	k4	1581
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
662	[number]	k4	662
domech	dům	k1gInPc6	dům
5	[number]	k4	5
245	[number]	k4	245
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
767	[number]	k4	767
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
5	[number]	k4	5
170	[number]	k4	170
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
31	[number]	k4	31
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
9	[number]	k4	9
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
3	[number]	k4	3
643	[number]	k4	643
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
618	[number]	k4	618
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
270	[number]	k4	270
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
113	[number]	k4	113
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
893	[number]	k4	893
domech	dům	k1gInPc6	dům
5	[number]	k4	5
813	[number]	k4	813
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
5	[number]	k4	5
614	[number]	k4	614
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
65	[number]	k4	65
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
3	[number]	k4	3
558	[number]	k4	558
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
958	[number]	k4	958
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
535	[number]	k4	535
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
84	[number]	k4	84
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
vývoj	vývoj	k1gInSc4	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
ve	v	k7c6	v
Strategickém	strategický	k2eAgInSc6d1	strategický
plánu	plán	k1gInSc6	plán
města	město	k1gNnSc2	město
Mělník	Mělník	k1gInSc1	Mělník
z	z	k7c2	z
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
dat	datum	k1gNnPc2	datum
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Věkové	věkový	k2eAgNnSc4d1	věkové
složení	složení	k1gNnSc4	složení
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
údajích	údaj	k1gInPc6	údaj
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
2014	[number]	k4	2014
na	na	k7c6	na
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
do	do	k7c2	do
21	[number]	k4	21
<g/>
členného	členný	k2eAgNnSc2d1	členné
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
hnutí	hnutí	k1gNnSc4	hnutí
Mé	můj	k3xOp1gNnSc4	můj
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
subjekt	subjekt	k1gInSc1	subjekt
založený	založený	k2eAgInSc1d1	založený
starostou	starosta	k1gMnSc7	starosta
Ctiradem	Ctirad	k1gMnSc7	Ctirad
Mikešem	Mikeš	k1gMnSc7	Mikeš
<g/>
)	)	kIx)	)
s	s	k7c7	s
devíti	devět	k4xCc7	devět
mandáty	mandát	k1gInPc7	mandát
(	(	kIx(	(
<g/>
32,8	[number]	k4	32,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
před	před	k7c7	před
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
(	(	kIx(	(
<g/>
19,63	[number]	k4	19,63
%	%	kIx~	%
<g/>
,	,	kIx,	,
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
9,74	[number]	k4	9,74
%	%	kIx~	%
<g/>
,	,	kIx,	,
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
mandáty	mandát	k1gInPc4	mandát
také	také	k6eAd1	také
získala	získat	k5eAaPmAgFnS	získat
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
8,5	[number]	k4	8,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
mandátu	mandát	k1gInSc6	mandát
obdržely	obdržet	k5eAaPmAgFnP	obdržet
ANO	ano	k9	ano
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
6,82	[number]	k4	6,82
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
6,46	[number]	k4	6,46
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mělničané	Mělničan	k1gMnPc1	Mělničan
(	(	kIx(	(
<g/>
6,41	[number]	k4	6,41
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
2010	[number]	k4	2010
na	na	k7c6	na
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
do	do	k7c2	do
21	[number]	k4	21
<g/>
členného	členný	k2eAgNnSc2d1	členné
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
s	s	k7c7	s
pěti	pět	k4xCc7	pět
mandáty	mandát	k1gInPc7	mandát
(	(	kIx(	(
<g/>
21,00	[number]	k4	21,00
%	%	kIx~	%
<g/>
)	)	kIx)	)
před	před	k7c7	před
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
20,45	[number]	k4	20,45
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
20,26	[number]	k4	20,26
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
mandáty	mandát	k1gInPc7	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
mandátech	mandát	k1gInPc6	mandát
získaly	získat	k5eAaPmAgInP	získat
subjekty	subjekt	k1gInPc1	subjekt
<g/>
:	:	kIx,	:
Koalice	koalice	k1gFnPc1	koalice
sdružení	sdružení	k1gNnSc2	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
Pro	pro	k7c4	pro
Mělník	Mělník	k1gInSc4	Mělník
a	a	k8xC	a
Strany	strana	k1gFnPc1	strana
zelených	zelená	k1gFnPc2	zelená
(	(	kIx(	(
<g/>
9,89	[number]	k4	9,89
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mělničané	Mělničan	k1gMnPc1	Mělničan
(	(	kIx(	(
<g/>
9,50	[number]	k4	9,50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RADNICE	radnice	k1gFnSc1	radnice
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
9,46	[number]	k4	9,46
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
9,11	[number]	k4	9,11
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
období	období	k1gNnSc4	období
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koaliční	koaliční	k2eAgFnSc1d1	koaliční
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
sedmi	sedm	k4xCc2	sedm
křesel	křeslo	k1gNnPc2	křeslo
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
Mé	můj	k3xOp1gNnSc1	můj
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mělničané	Mělničan	k1gMnPc1	Mělničan
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tak	tak	k6eAd1	tak
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
disponují	disponovat	k5eAaBmIp3nP	disponovat
15	[number]	k4	15
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
21	[number]	k4	21
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
je	být	k5eAaImIp3nS	být
Ctirad	Ctirad	k1gMnSc1	Ctirad
Mikeš	Mikeš	k1gMnSc1	Mikeš
(	(	kIx(	(
<g/>
Mé	můj	k3xOp1gNnSc1	můj
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
místostarostou	místostarosta	k1gMnSc7	místostarosta
Milan	Milan	k1gMnSc1	Milan
Schweigstill	Schweigstill	k1gMnSc1	Schweigstill
(	(	kIx(	(
<g/>
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
místostarostou	místostarosta	k1gMnSc7	místostarosta
pak	pak	k6eAd1	pak
Petr	Petr	k1gMnSc1	Petr
Volf	Volf	k1gMnSc1	Volf
(	(	kIx(	(
<g/>
Mělničané	Mělničan	k1gMnPc1	Mělničan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
existovala	existovat	k5eAaImAgFnS	existovat
čtyřkoaliční	čtyřkoaliční	k2eAgFnSc1d1	čtyřkoaliční
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Sedmičlennou	sedmičlenný	k2eAgFnSc4d1	sedmičlenná
radu	rada	k1gFnSc4	rada
tvořili	tvořit	k5eAaImAgMnP	tvořit
zástupci	zástupce	k1gMnPc1	zástupce
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RADNICE	radnice	k1gFnSc1	radnice
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mělničané	Mělničan	k1gMnPc1	Mělničan
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
12	[number]	k4	12
mandátů	mandát	k1gInPc2	mandát
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
senátního	senátní	k2eAgInSc2d1	senátní
obvodu	obvod	k1gInSc2	obvod
č.	č.	k?	č.
28	[number]	k4	28
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
senátorkou	senátorka	k1gFnSc7	senátorka
Veronika	Veronika	k1gFnSc1	Veronika
Vrecionová	Vrecionový	k2eAgFnSc1d1	Vrecionová
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
mandát	mandát	k1gInSc4	mandát
vykonávat	vykonávat	k5eAaImF	vykonávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Mělnický	mělnický	k2eAgInSc1d1	mělnický
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc6d1	stojící
na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgNnSc6d1	vyvýšené
místě	místo	k1gNnSc6	místo
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
středověký	středověký	k2eAgInSc4d1	středověký
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
klenutá	klenutý	k2eAgFnSc1d1	klenutá
síň	síň	k1gFnSc1	síň
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
a	a	k8xC	a
brána	brána	k1gFnSc1	brána
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
ovdovělých	ovdovělý	k2eAgFnPc2d1	ovdovělá
českých	český	k2eAgFnPc2d1	Česká
královen	královna	k1gFnPc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1550	[number]	k4	1550
bylo	být	k5eAaImAgNnS	být
přistavěno	přistavěn	k2eAgNnSc1d1	přistavěno
renesanční	renesanční	k2eAgNnSc1d1	renesanční
severní	severní	k2eAgNnSc1d1	severní
křídlo	křídlo	k1gNnSc1	křídlo
s	s	k7c7	s
loggiemi	loggie	k1gFnPc7	loggie
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
přistavěno	přistavět	k5eAaPmNgNnS	přistavět
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
upravena	upraven	k2eAgFnSc1d1	upravena
vinárna	vinárna	k1gFnSc1	vinárna
s	s	k7c7	s
terasou	terasa	k1gFnSc7	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
rychta	rychta	k1gFnSc1	rychta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1449	[number]	k4	1449
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
a	a	k8xC	a
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
trojlodní	trojlodní	k2eAgInSc1d1	trojlodní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
románský	románský	k2eAgMnSc1d1	románský
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Přestavěn	přestavět	k5eAaPmNgMnS	přestavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1480-1488	[number]	k4	1480-1488
(	(	kIx(	(
<g/>
klenba	klenba	k1gFnSc1	klenba
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
presbytář	presbytář	k1gInSc1	presbytář
<g/>
,	,	kIx,	,
klenba	klenba	k1gFnSc1	klenba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
krypta	krypta	k1gFnSc1	krypta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kněžišti	kněžiště	k1gNnSc6	kněžiště
je	být	k5eAaImIp3nS	být
sanktuář	sanktuář	k1gInSc1	sanktuář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
a	a	k8xC	a
renesanční	renesanční	k2eAgFnSc1d1	renesanční
lavice	lavice	k1gFnSc1	lavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sakristii	sakristie	k1gFnSc6	sakristie
je	být	k5eAaImIp3nS	být
náhrobek	náhrobek	k1gInSc1	náhrobek
probošta	probošt	k1gMnSc2	probošt
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Landštejna	Landštejn	k1gInSc2	Landštejn
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1389	[number]	k4	1389
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1585	[number]	k4	1585
<g/>
,	,	kIx,	,
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
<s>
Kapucínský	kapucínský	k2eAgInSc1d1	kapucínský
kostel	kostel	k1gInSc1	kostel
Čtrnácti	čtrnáct	k4xCc2	čtrnáct
svatých	svatý	k2eAgMnPc2d1	svatý
pomocníků	pomocník	k1gMnPc2	pomocník
s	s	k7c7	s
někdejším	někdejší	k2eAgInSc7d1	někdejší
klášterem	klášter	k1gInSc7	klášter
Renesanční	renesanční	k2eAgFnSc2d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgInPc1d1	barokní
domy	dům	k1gInPc1	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
v	v	k7c6	v
Palackého	Palackého	k2eAgFnSc6d1	Palackého
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytky	zbytek	k1gInPc4	zbytek
hradeb	hradba	k1gFnPc2	hradba
s	s	k7c7	s
baštou	bašta	k1gFnSc7	bašta
a	a	k8xC	a
vodárenskou	vodárenský	k2eAgFnSc7d1	vodárenská
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyhlídce	vyhlídka	k1gFnSc6	vyhlídka
za	za	k7c7	za
severním	severní	k2eAgNnSc7d1	severní
křídlem	křídlo	k1gNnSc7	křídlo
zámku	zámek	k1gInSc2	zámek
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Maxe	Max	k1gMnSc2	Max
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
М	М	k?	М
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
v	v	k7c6	v
Blagoevgradské	Blagoevgradský	k2eAgFnSc6d1	Blagoevgradský
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
vinohradnickou	vinohradnický	k2eAgFnSc4d1	vinohradnická
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
vína	víno	k1gNnSc2	víno
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
koupit	koupit	k5eAaPmF	koupit
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
nesou	nést	k5eAaImIp3nP	nést
označení	označení	k1gNnSc4	označení
Melnik	Melnik	k1gInSc1	Melnik
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
přednášel	přednášet	k5eAaImAgMnS	přednášet
poslanec	poslanec	k1gMnSc1	poslanec
profesor	profesor	k1gMnSc1	profesor
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
přednášky	přednáška	k1gFnPc1	přednáška
vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k9	i
knižně	knižně	k6eAd1	knižně
např.	např.	kA	např.
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1908	[number]	k4	1908
"	"	kIx"	"
<g/>
Potřeba	potřeba	k1gFnSc1	potřeba
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
politiky	politika	k1gFnSc2	politika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc1	Mělník
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
Nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
vinařství	vinařství	k1gNnSc3	vinařství
je	být	k5eAaImIp3nS	být
mělnický	mělnický	k2eAgInSc4d1	mělnický
kopec	kopec	k1gInSc4	kopec
protkán	protkán	k2eAgInSc4d1	protkán
sítí	síť	k1gFnSc7	síť
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
pozvolna	pozvolna	k6eAd1	pozvolna
zpřístupňována	zpřístupňován	k2eAgFnSc1d1	zpřístupňována
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
nejširší	široký	k2eAgFnSc1d3	nejširší
studna	studna	k1gFnSc1	studna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc4	osoba
zvýrazněné	zvýrazněný	k2eAgFnPc4d1	zvýrazněná
tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
mělnickými	mělnický	k2eAgMnPc7d1	mělnický
rodáky	rodák	k1gMnPc7	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Budinský-Krička	Budinský-Krička	k1gMnSc1	Budinský-Krička
Václav	Václav	k1gMnSc1	Václav
Drobný	drobný	k2eAgMnSc1d1	drobný
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
Emma	Emma	k1gFnSc1	Emma
(	(	kIx(	(
<g/>
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
Feix	Feix	k1gInSc1	Feix
Adolf	Adolf	k1gMnSc1	Adolf
Fišer	Fišer	k1gMnSc1	Fišer
Karel	Karel	k1gMnSc1	Karel
Fořt	Fořt	k1gMnSc1	Fořt
(	(	kIx(	(
<g/>
ovocnář	ovocnář	k1gMnSc1	ovocnář
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Fridrich	Fridrich	k1gMnSc1	Fridrich
(	(	kIx(	(
<g/>
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Gemerich	Gemerich	k1gMnSc1	Gemerich
rytíř	rytíř	k1gMnSc1	rytíř
z	z	k7c2	z
Neubergu	Neuberg	k1gInSc2	Neuberg
Kamil	Kamil	k1gMnSc1	Kamil
Hilbert	Hilbert	k1gMnSc1	Hilbert
Josef	Josef	k1gMnSc1	Josef
Hořejší	Hořejší	k1gMnSc1	Hořejší
Jakub	Jakub	k1gMnSc1	Jakub
Hořčický	Hořčický	k2eAgMnSc1d1	Hořčický
z	z	k7c2	z
Tepence	Tepence	k1gFnSc2	Tepence
Jan	Jan	k1gMnSc1	Jan
Eduard	Eduard	k1gMnSc1	Eduard
rytíř	rytíř	k1gMnSc1	rytíř
z	z	k7c2	z
Neubergu	Neuberg	k1gInSc2	Neuberg
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
Otakar	Otakar	k1gMnSc1	Otakar
Jaroš	Jaroš	k1gMnSc1	Jaroš
Johana	Johana	k1gFnSc1	Johana
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Natalena	Natalen	k2eAgFnSc1d1	Natalen
Koroleva	Koroleva	k1gFnSc1	Koroleva
Vilém	Vilém	k1gMnSc1	Vilém
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Krombholc	Krombholc	k1gInSc1	Krombholc
(	(	kIx(	(
<g/>
pohřben	pohřben	k2eAgMnSc1d1	pohřben
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
pěvkyní	pěvkyně	k1gFnSc7	pěvkyně
Marií	Maria	k1gFnSc7	Maria
Tauberovou	Tauberová	k1gFnSc7	Tauberová
-	-	kIx~	-
Krombholcovou	Krombholcův	k2eAgFnSc7d1	Krombholcova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Kubíček	Kubíček	k1gMnSc1	Kubíček
Václav	Václav	k1gMnSc1	Václav
Levý	levý	k2eAgMnSc1d1	levý
Svatá	svatat	k5eAaImIp3nS	svatat
Ludmila	Ludmila	k1gFnSc1	Ludmila
Vincenc	Vincenc	k1gMnSc1	Vincenc
Makovský	Makovský	k1gMnSc1	Makovský
Jiří	Jiří	k1gMnSc1	Jiří
Malý	Malý	k1gMnSc1	Malý
František	František	k1gMnSc1	František
Markus	Markus	k1gMnSc1	Markus
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
Martinovský	Martinovský	k2eAgMnSc1d1	Martinovský
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matiegka	Matiegko	k1gNnSc2	Matiegko
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Myslbek	Myslbek	k1gMnSc1	Myslbek
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
Arnošt	Arnošt	k1gMnSc1	Arnošt
Peths	Pethsa	k1gFnPc2	Pethsa
Bořík	Bořík	k1gMnSc1	Bořík
Procházka	Procházka	k1gMnSc1	Procházka
Bohumil	Bohumil	k1gMnSc1	Bohumil
Rameš	Rameš	k1gMnSc1	Rameš
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
cyklista	cyklista	k1gMnSc1	cyklista
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sígl	Sígl	k?	Sígl
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Smolák	smolák	k1gMnSc1	smolák
Josef	Josef	k1gMnSc1	Josef
Švagrovský	švagrovský	k2eAgMnSc1d1	švagrovský
Oldřich	Oldřich	k1gMnSc1	Oldřich
Wenzl	Wenzl	k1gFnSc2	Wenzl
<g />
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Zahajský	Zahajský	k2eAgMnSc1d1	Zahajský
František	František	k1gMnSc1	František
Žilka	Žilka	k1gMnSc1	Žilka
Jitka	Jitka	k1gFnSc1	Jitka
Čvančarová	Čvančarová	k1gFnSc1	Čvančarová
Marie	Maria	k1gFnSc2	Maria
Dolistová	Dolistový	k2eAgFnSc1d1	Dolistový
Kateřina	Kateřina	k1gFnSc1	Kateřina
Jacques	Jacques	k1gMnSc1	Jacques
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kraj	kraj	k1gInSc1	kraj
Vilém	Vilém	k1gMnSc1	Vilém
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
Václav	Václav	k1gMnSc1	Václav
Krejčí	Krejčí	k1gMnSc1	Krejčí
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
Jan	Jan	k1gMnSc1	Jan
Morava	Morava	k1gFnSc1	Morava
Jiří	Jiří	k1gMnSc1	Jiří
Nedoma	Nedoma	k1gMnSc1	Nedoma
Alexandr	Alexandr	k1gMnSc1	Alexandr
Oniščenko	Oniščenka	k1gFnSc5	Oniščenka
Lenka	lenka	k1gFnSc3	lenka
Šmídová	Šmídová	k1gFnSc1	Šmídová
Pavel	Pavel	k1gMnSc1	Pavel
Verbíř	verbíř	k1gMnSc1	verbíř
Olga	Olga	k1gFnSc1	Olga
Vychodilová	Vychodilová	k1gFnSc1	Vychodilová
Kateřina	Kateřina	k1gFnSc1	Kateřina
Winterová	Winterová	k1gFnSc1	Winterová
Pavel	Pavel	k1gMnSc1	Pavel
Zářecký	zářecký	k2eAgMnSc1d1	zářecký
Jan	Jan	k1gMnSc1	Jan
Živný	živný	k2eAgMnSc1d1	živný
Z	z	k7c2	z
mělnické	mělnický	k2eAgFnSc2d1	Mělnická
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vidět	vidět	k5eAaImF	vidět
památná	památný	k2eAgFnSc1d1	památná
hora	hora	k1gFnSc1	hora
Říp	Říp	k1gInSc1	Říp
a	a	k8xC	a
za	za	k7c2	za
dobré	dobrý	k2eAgFnSc2d1	dobrá
viditelnosti	viditelnost	k1gFnSc2	viditelnost
České	český	k2eAgNnSc1d1	české
středohoří	středohoří	k1gNnSc1	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velmi	velmi	k6eAd1	velmi
vyhledávaným	vyhledávaný	k2eAgFnPc3d1	vyhledávaná
oblastem	oblast	k1gFnPc3	oblast
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
Mělníka	Mělník	k1gInSc2	Mělník
patří	patřit	k5eAaImIp3nS	patřit
Kokořínsko	Kokořínsko	k1gNnSc4	Kokořínsko
s	s	k7c7	s
pískovcovými	pískovcový	k2eAgFnPc7d1	pískovcová
skalami	skála	k1gFnPc7	skála
a	a	k8xC	a
gotickým	gotický	k2eAgInSc7d1	gotický
hradem	hrad	k1gInSc7	hrad
Kokořín	Kokořína	k1gFnPc2	Kokořína
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Správa	správa	k1gFnSc1	správa
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Kokořínsko	Kokořínsko	k1gNnSc1	Kokořínsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
přibližně	přibližně	k6eAd1	přibližně
naproti	naproti	k6eAd1	naproti
Liběchovu	Liběchův	k2eAgFnSc4d1	Liběchova
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Horní	horní	k2eAgFnSc2d1	horní
Počaply	Počaply	k1gFnSc2	Počaply
(	(	kIx(	(
<g/>
asi	asi	k9	asi
9	[number]	k4	9
km	km	kA	km
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
Labe	Labe	k1gNnSc2	Labe
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
tepelná	tepelný	k2eAgFnSc1d1	tepelná
Elektrárna	elektrárna	k1gFnSc1	elektrárna
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
pohled	pohled	k1gInSc1	pohled
je	být	k5eAaImIp3nS	být
i	i	k9	i
z	z	k7c2	z
Vrázovy	Vrázův	k2eAgFnSc2d1	Vrázova
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
<g/>
.	.	kIx.	.
</s>
<s>
Mělník	Mělník	k1gInSc1	Mělník
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
Mělník	Mělník	k1gInSc1	Mělník
a	a	k8xC	a
Vehlovice	Vehlovice	k1gFnSc1	Vehlovice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nemá	mít	k5eNaImIp3nS	mít
definovány	definován	k2eAgMnPc4d1	definován
žádné	žádný	k1gMnPc4	žádný
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
městu	město	k1gNnSc3	město
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
připojovány	připojován	k2eAgFnPc4d1	připojována
okolní	okolní	k2eAgFnPc4d1	okolní
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
později	pozdě	k6eAd2	pozdě
stavebně	stavebně	k6eAd1	stavebně
splynuly	splynout	k5eAaPmAgFnP	splynout
s	s	k7c7	s
městem	město	k1gNnSc7	město
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
evidovány	evidován	k2eAgFnPc4d1	evidována
jako	jako	k8xS	jako
základní	základní	k2eAgFnPc4d1	základní
sídelní	sídelní	k2eAgFnPc4d1	sídelní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Blata	Blata	k1gNnPc1	Blata
Chloumek	chloumek	k1gInSc4	chloumek
Mlazice	Mlazic	k1gMnSc2	Mlazic
Podolí	Podolí	k1gNnSc2	Podolí
Pšovka	Pšovka	k1gMnSc1	Pšovka
I	i	k8xC	i
a	a	k8xC	a
II	II	kA	II
Rousovice	Rousovice	k1gFnSc1	Rousovice
Rybáře	Rybář	k1gMnSc2	Rybář
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
a	a	k8xC	a
Obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Mělník	Mělník	k1gInSc1	Mělník
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
69	[number]	k4	69
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
39	[number]	k4	39
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Lučenec	Lučenec	k1gInSc1	Lučenec
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
Oranienburg	Oranienburg	k1gInSc1	Oranienburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
Przeworsk	Przeworsk	k1gInSc1	Przeworsk
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Wetzikon	Wetzikon	k1gNnSc4	Wetzikon
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
Pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
Městem	město	k1gNnSc7	město
procházejí	procházet	k5eAaImIp3nP	procházet
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
Zdiby	Zdiba	k1gFnPc1	Zdiba
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
Bor	bor	k1gInSc1	bor
-	-	kIx~	-
Rumburk	Rumburk	k1gInSc1	Rumburk
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
Řevničov	Řevničov	k1gInSc1	Řevničov
-	-	kIx~	-
Slaný	Slaný	k1gInSc1	Slaný
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
-	-	kIx~	-
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
246	[number]	k4	246
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Roudnice	Roudnice	k1gFnSc1	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Libochovice	Libochovice	k1gFnPc1	Libochovice
-	-	kIx~	-
Louny	Louny	k1gInPc1	Louny
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
273	[number]	k4	273
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Mšeno	Mšeno	k1gNnSc1	Mšeno
-	-	kIx~	-
Doksy	Doksy	k1gInPc1	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
072	[number]	k4	072
vedoucí	vedoucí	k1gFnSc1	vedoucí
podél	podél	k7c2	podél
Labe	Labe	k1gNnSc2	Labe
z	z	k7c2	z
Lysé	Lysá	k1gFnSc2	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
železniční	železniční	k2eAgFnSc4d1	železniční
Trať	trať	k1gFnSc4	trať
076	[number]	k4	076
do	do	k7c2	do
Mladé	mladá	k1gFnSc2	mladá
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
odbočná	odbočný	k2eAgFnSc1d1	odbočná
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
072	[number]	k4	072
i	i	k8xC	i
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Mělník-Mlazice	Mělník-Mlazice	k1gFnSc2	Mělník-Mlazice
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
072	[number]	k4	072
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
-	-	kIx~	-
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
západ	západ	k1gInSc1	západ
je	být	k5eAaImIp3nS	být
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
elektrizovaná	elektrizovaný	k2eAgFnSc1d1	elektrizovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
Trať	trať	k1gFnSc1	trať
076	[number]	k4	076
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
-	-	kIx~	-
Mšeno	Mšeno	k1gNnSc1	Mšeno
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
regionální	regionální	k2eAgFnSc1d1	regionální
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
do	do	k7c2	do
Mšena	Mšen	k1gInSc2	Mšen
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
2012	[number]	k4	2012
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
Městem	město	k1gNnSc7	město
projížděly	projíždět	k5eAaImAgFnP	projíždět
dálkové	dálkový	k2eAgFnPc1d1	dálková
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
Praha-Jablonné	Praha-Jablonný	k2eAgFnPc1d1	Praha-Jablonný
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
<g/>
,	,	kIx,	,
Praha-Česká	Praha-Český	k2eAgFnSc1d1	Praha-Česká
Lípa-Nový	Lípa-Nový	k2eAgInSc4d1	Lípa-Nový
Bor-Varnsdorf-Rumburk-Šluknov	Bor-Varnsdorf-Rumburk-Šluknov	k1gInSc4	Bor-Varnsdorf-Rumburk-Šluknov
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc4d1	nový
Bor-Česká	Bor-Český	k2eAgFnSc1d1	Bor-Český
Lípa-Praha	Lípa-Praha	k1gFnSc1	Lípa-Praha
a	a	k8xC	a
Jablonné	Jablonná	k1gFnSc2	Jablonná
v	v	k7c6	v
Podj	Podj	k1gMnSc1	Podj
<g/>
.	.	kIx.	.
<g/>
-Doksy-Dubá-Mělník-Praha	-Doksy-Dubá-Mělník-Praha	k1gMnSc1	-Doksy-Dubá-Mělník-Praha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vedly	vést	k5eAaImAgFnP	vést
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
<g/>
:	:	kIx,	:
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c4	nad
Labem-Stará	Labem-Starý	k2eAgNnPc4d1	Labem-Starý
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Kokořín	Kokořín	k1gInSc1	Kokořín
<g/>
,	,	kIx,	,
Kralupy	Kralupy	k1gInPc1	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Mšeno	Mšeno	k1gNnSc1	Mšeno
<g/>
,	,	kIx,	,
Neratovice	Neratovice	k1gFnSc1	Neratovice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Roudnice	Roudnice	k1gFnSc1	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Štětí	štětit	k5eAaImIp3nS	štětit
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
072	[number]	k4	072
jezdilo	jezdit	k5eAaImAgNnS	jezdit
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
8	[number]	k4	8
párů	pár	k1gInPc2	pár
rychlíků	rychlík	k1gInPc2	rychlík
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
Všetat	Všetat	k1gFnSc2	Všetat
do	do	k7c2	do
Mělníka	Mělník	k1gInSc2	Mělník
14	[number]	k4	14
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
Mělníka	Mělník	k1gInSc2	Mělník
do	do	k7c2	do
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
10	[number]	k4	10
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
7	[number]	k4	7
párů	pár	k1gInPc2	pár
rychlíků	rychlík	k1gInPc2	rychlík
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
Všetat	Všetat	k1gFnSc2	Všetat
do	do	k7c2	do
Mělníka	Mělník	k1gInSc2	Mělník
9	[number]	k4	9
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
Mělníka	Mělník	k1gInSc2	Mělník
do	do	k7c2	do
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
7	[number]	k4	7
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
076	[number]	k4	076
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
mezi	mezi	k7c7	mezi
Mělníkem	Mělník	k1gInSc7	Mělník
a	a	k8xC	a
Mšenem	Mšen	k1gInSc7	Mšen
denně	denně	k6eAd1	denně
jezdilo	jezdit	k5eAaImAgNnS	jezdit
6	[number]	k4	6
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Cyklistika	cyklistika	k1gFnSc1	cyklistika
Městem	město	k1gNnSc7	město
vedou	vést	k5eAaImIp3nP	vést
cyklotrasy	cyklotrasa	k1gFnPc4	cyklotrasa
č.	č.	k?	č.
2	[number]	k4	2
Kralupy	Kralupy	k1gInPc4	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
-	-	kIx~	-
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
č.	č.	k?	č.
24	[number]	k4	24
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
-	-	kIx~	-
Neratovice	Neratovice	k1gFnSc1	Neratovice
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
č.	č.	k?	č.
203	[number]	k4	203
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Lhotka	Lhotka	k1gFnSc1	Lhotka
<g/>
,	,	kIx,	,
č.	č.	k?	č.
8171	[number]	k4	8171
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Malý	malý	k2eAgInSc1d1	malý
Újezd	Újezd	k1gInSc1	Újezd
-	-	kIx~	-
Jelenice	jelenice	k1gFnSc1	jelenice
<g/>
.	.	kIx.	.
</s>
<s>
Pěší	pěší	k2eAgFnSc1d1	pěší
turistika	turistika	k1gFnSc1	turistika
Územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
vedou	vést	k5eAaImIp3nP	vést
turistické	turistický	k2eAgFnPc1d1	turistická
trasy	trasa	k1gFnPc1	trasa
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Lhotka	Lhotka	k1gFnSc1	Lhotka
-	-	kIx~	-
Kokořínský	Kokořínský	k2eAgInSc1d1	Kokořínský
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Záboří	Záboří	k1gNnSc1	Záboří
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Liběchov	Liběchov	k1gInSc1	Liběchov
-	-	kIx~	-
Tupadly	Tupadlo	k1gNnPc7	Tupadlo
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc4	Mělník
-	-	kIx~	-
Čepička	čepička	k1gFnSc1	čepička
-	-	kIx~	-
Lhotka	Lhotka	k1gFnSc1	Lhotka
<g/>
.	.	kIx.	.
</s>
