<s>
Chetitština	chetitština	k1gFnSc1
</s>
<s>
Chetitština	chetitština	k1gFnSc1
(	(	kIx(
<g/>
𒉈	𒉈	k?
<g/>
,	,	kIx,
nešili	šít	k5eNaImAgMnP
<g/>
)	)	kIx)
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Anatolie	Anatolie	k1gFnSc1
Počet	počet	k1gInSc1
mluvčích	mluvčí	k1gMnPc2
</s>
<s>
vymřelý	vymřelý	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
Klasifikace	klasifikace	k1gFnSc2
</s>
<s>
Indoevropské	indoevropský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Anatolské	anatolský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
Písmo	písmo	k1gNnSc4
</s>
<s>
Chetitský	chetitský	k2eAgInSc1d1
klínopis	klínopis	k1gInSc1
Postavení	postavení	k1gNnSc1
Regulátor	regulátor	k1gInSc1
</s>
<s>
není	být	k5eNaImIp3nS
stanoven	stanovit	k5eAaPmNgInS
Úřední	úřední	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
není	být	k5eNaImIp3nS
úředním	úřední	k2eAgNnSc7d1
Kódy	kód	k1gInPc1
ISO	ISO	kA
639-1	639-1	k4
</s>
<s>
není	být	k5eNaImIp3nS
ISO	ISO	kA
639-2	639-2	k4
</s>
<s>
hit	hit	k1gInSc1
(	(	kIx(
<g/>
B	B	kA
<g/>
)	)	kIx)
hit	hit	k1gInSc1
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
ISO	ISO	kA
639-3	639-3	k4
</s>
<s>
sux	sux	k?
Ethnologue	Ethnologue	k1gInSc1
</s>
<s>
sux	sux	k?
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
není	být	k5eNaImIp3nS
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chetitština	chetitština	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
chetitštině	chetitština	k1gFnSc6
𒉈	𒉈	k?
<g/>
,	,	kIx,
nešili	šít	k5eNaImAgMnP
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vymřelý	vymřelý	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
z	z	k7c2
anatolské	anatolský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
indoevropských	indoevropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
nejstarší	starý	k2eAgInSc1d3
zaznamenaný	zaznamenaný	k2eAgInSc1d1
indoevropský	indoevropský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
používán	používat	k5eAaImNgInS
přibližně	přibližně	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
území	území	k1gNnSc6
severní	severní	k2eAgFnSc2d1
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
(	(	kIx(
<g/>
Anatolie	Anatolie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
vůbec	vůbec	k9
nejstarší	starý	k2eAgInSc4d3
dochovaný	dochovaný	k2eAgInSc4d1
chetitský	chetitský	k2eAgInSc4d1
text	text	k1gInSc4
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
tzv.	tzv.	kA
Anitta	Anitta	k1gFnSc1
text	text	k1gInSc1
<g/>
,	,	kIx,
datovaný	datovaný	k2eAgInSc4d1
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
K	k	k7c3
zápisu	zápis	k1gInSc3
chetitštiny	chetitština	k1gFnSc2
obyčejně	obyčejně	k6eAd1
sloužil	sloužit	k5eAaImAgInS
klínopis	klínopis	k1gInSc1
<g/>
,	,	kIx,
méně	málo	k6eAd2
často	často	k6eAd1
je	být	k5eAaImIp3nS
zachováno	zachovat	k5eAaPmNgNnS
též	též	k6eAd1
specifické	specifický	k2eAgNnSc1d1
hieroglyfické	hieroglyfický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texty	text	k1gInPc1
byly	být	k5eAaImAgInP
psány	psát	k5eAaImNgInP
profesionálními	profesionální	k2eAgInPc7d1
písaři	písař	k1gMnPc1
na	na	k7c4
hliněné	hliněný	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
jedné	jeden	k4xCgFnSc2
bronzové	bronzový	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
většinou	většinou	k6eAd1
následně	následně	k6eAd1
vypáleny	vypálen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
historického	historický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
chetitština	chetitština	k1gFnSc1
<g/>
“	“	k?
termín	termín	k1gInSc4
nepřesný	přesný	k2eNgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sami	sám	k3xTgMnPc1
Chetité	Chetita	k1gMnPc1
nazývali	nazývat	k5eAaImAgMnP
„	„	k?
<g/>
chetitštinou	chetitština	k1gFnSc7
<g/>
“	“	k?
neindoevropský	indoevropský	k2eNgInSc4d1
jazyk	jazyk	k1gInSc4
svých	svůj	k3xOyFgMnPc2
předchůdců	předchůdce	k1gMnPc2
a	a	k8xC
chetitštinu	chetitština	k1gFnSc4
samu	sám	k3xTgFnSc4
„	„	k?
<g/>
nešili	šít	k5eNaImAgMnP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc4
nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
již	již	k6eAd1
zaveden	zavést	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1916	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
byl	být	k5eAaImAgMnS
tento	tento	k3xDgMnSc1
jazyk	jazyk	k1gMnSc1
rozluštěn	rozluštit	k5eAaPmNgMnS
českým	český	k2eAgMnSc7d1
badatelem	badatel	k1gMnSc7
Bedřichem	Bedřich	k1gMnSc7
Hrozným	hrozný	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Chetitské	chetitský	k2eAgInPc1d1
texty	text	k1gInPc1
se	se	k3xPyFc4
rozdělují	rozdělovat	k5eAaImIp3nP
do	do	k7c2
tří	tři	k4xCgInPc2
vývojových	vývojový	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
stará	starý	k2eAgFnSc1d1
chetitština	chetitština	k1gFnSc1
(	(	kIx(
<g/>
1750	#num#	k4
<g/>
–	–	k?
<g/>
1450	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
střední	střední	k2eAgFnSc1d1
chetitština	chetitština	k1gFnSc1
(	(	kIx(
<g/>
1450	#num#	k4
<g/>
–	–	k?
<g/>
1380	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
nová	nový	k2eAgFnSc1d1
chetitština	chetitština	k1gFnSc1
(	(	kIx(
<g/>
1380	#num#	k4
<g/>
–	–	k?
<g/>
1220	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Fonologie	fonologie	k1gFnSc1
</s>
<s>
Souhlásky	souhláska	k1gFnPc1
</s>
<s>
Labiála	labiála	k1gFnSc1
</s>
<s>
Alveolára	alveolára	k1gFnSc1
</s>
<s>
Palatála	palatála	k1gFnSc1
</s>
<s>
Velára	velára	k1gFnSc1
</s>
<s>
Labio-velára	Labio-velár	k1gMnSc4
</s>
<s>
Uvulára	Uvulár	k1gMnSc4
</s>
<s>
Ploziva	Ploziva	k1gFnSc1
</s>
<s>
Neznělé	znělý	k2eNgNnSc1d1
</s>
<s>
p	p	k?
</s>
<s>
t	t	k?
</s>
<s>
k	k	k7c3
</s>
<s>
kʷ	kʷ	k?
</s>
<s>
Znělé	znělý	k2eAgNnSc1d1
</s>
<s>
b	b	k?
</s>
<s>
d	d	k?
</s>
<s>
g	g	kA
</s>
<s>
gʷ	gʷ	k?
</s>
<s>
Frikativa	frikativa	k1gFnSc1
</s>
<s>
Neznělé	znělý	k2eNgNnSc1d1
</s>
<s>
s	s	k7c7
</s>
<s>
X	X	kA
<g/>
,	,	kIx,
Xʷ	Xʷ	k1gFnSc1
</s>
<s>
Znělé	znělý	k2eAgNnSc1d1
</s>
<s>
ʁ	ʁ	k?
<g/>
,	,	kIx,
ʁ	ʁ	k?
</s>
<s>
Nazála	nazála	k1gFnSc1
</s>
<s>
m	m	kA
</s>
<s>
n	n	k0
</s>
<s>
Vibranta	vibranta	k1gFnSc1
</s>
<s>
r	r	kA
</s>
<s>
Afrikáta	afrikáta	k1gFnSc1
</s>
<s>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
</s>
<s>
Approximanta	Approximanta	k1gFnSc1
</s>
<s>
l	l	kA
</s>
<s>
j	j	k?
</s>
<s>
w	w	k?
</s>
<s>
Samohlásky	samohláska	k1gFnPc1
</s>
<s>
Chetitština	chetitština	k1gFnSc1
rozlišovala	rozlišovat	k5eAaImAgFnS
čtyři	čtyři	k4xCgInPc4
vokály	vokál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
samohlásky	samohláska	k1gFnPc1
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
krátké	krátký	k2eAgFnPc1d1
i	i	k8xC
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
přední	přední	k2eAgMnSc1d1
</s>
<s>
střední	střední	k2eAgFnSc1d1
</s>
<s>
zadní	zadní	k2eAgInSc1d1
</s>
<s>
vysoké	vysoký	k2eAgFnPc1d1
</s>
<s>
i	i	k9
iː	iː	k?
</s>
<s>
u	u	k7c2
uː	uː	k?
</s>
<s>
střední	střední	k2eAgFnSc1d1
</s>
<s>
e	e	k0
eː	eː	k?
</s>
<s>
nízké	nízký	k2eAgNnSc1d1
</s>
<s>
a	a	k8xC
aː	aː	k?
</s>
<s>
Gramatika	gramatika	k1gFnSc1
</s>
<s>
Morfologie	morfologie	k1gFnSc1
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
</s>
<s>
Podstatná	podstatný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
rozlišovala	rozlišovat	k5eAaImAgFnS
9	#num#	k4
pádů	pád	k1gInPc2
(	(	kIx(
<g/>
nominativ	nominativ	k1gInSc1
<g/>
,	,	kIx,
genitiv	genitiv	k1gInSc1
<g/>
,	,	kIx,
dativ-lokál	dativ-lokál	k1gInSc1
<g/>
,	,	kIx,
akusativ	akusativ	k1gInSc1
<g/>
,	,	kIx,
vocativ	vocativ	k1gInSc1
<g/>
,	,	kIx,
ablativ	ablativ	k1gInSc1
<g/>
,	,	kIx,
ergativ	ergativ	k1gInSc1
<g/>
,	,	kIx,
allativ	allativ	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
instrumentál	instrumentál	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dvě	dva	k4xCgNnPc1
čísla	číslo	k1gNnPc1
(	(	kIx(
<g/>
singulár	singulár	k1gInSc1
a	a	k8xC
plurál	plurál	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
namísto	namísto	k7c2
v	v	k7c6
jazycích	jazyk	k1gInPc6
častějšího	častý	k2eAgInSc2d2
rodu	rod	k1gInSc2
mužského	mužský	k2eAgInSc2d1
a	a	k8xC
ženského	ženský	k2eAgInSc2d1
rozlišuje	rozlišovat	k5eAaImIp3nS
životný	životný	k2eAgInSc1d1
(	(	kIx(
<g/>
například	například	k6eAd1
slovo	slovo	k1gNnSc1
muž	muž	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
neživotný	životný	k2eNgMnSc1d1
(	(	kIx(
<g/>
například	například	k6eAd1
slovo	slovo	k1gNnSc1
stůl	stůl	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stejně	stejně	k6eAd1
se	se	k3xPyFc4
rozlišovala	rozlišovat	k5eAaImAgNnP
i	i	k9
přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
a	a	k8xC
zájmena	zájmeno	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
skloňování	skloňování	k1gNnSc2
substantiv	substantivum	k1gNnPc2
„	„	k?
<g/>
pišna	pišn	k1gMnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
muž	muž	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
pē	pē	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
místo	místo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Životný	životný	k2eAgInSc1d1
</s>
<s>
Neživotný	životný	k2eNgInSc1d1
</s>
<s>
SingulárPlurálSingulárPlurál	SingulárPlurálSingulárPlurál	k1gMnSc1
</s>
<s>
Nominativ	nominativ	k1gInSc1
</s>
<s>
pišnašpišnē	pišnašpišnē	k1gFnSc1
</s>
<s>
Vocativ	Vocativ	k1gInSc1
</s>
<s>
pišnepišne	pišnepišnout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
–	–	k?
</s>
<s>
Akusativ	akusativ	k1gInSc1
</s>
<s>
pišnanpišnušpē	pišnanpišnušpē	k1gFnSc1
</s>
<s>
Genitiv	genitiv	k1gInSc1
</s>
<s>
pišnašpišnašpē	pišnašpišnašpē	k5eAaPmIp2nS
</s>
<s>
Dativ	dativ	k1gInSc1
<g/>
/	/	kIx~
<g/>
Lokál	lokál	k1gInSc1
</s>
<s>
pišnipišnašpē	pišnipišnašpē	k5eAaPmIp2nS
</s>
<s>
Ablativ	ablativ	k1gInSc1
</s>
<s>
pišnazpišnazpē	pišnazpišnazpē	k1gInSc1
</s>
<s>
Ergativ	Ergativ	k1gInSc1
</s>
<s>
pišnanzapišnantē	pišnanzapišnantē	k5eAaPmIp2nS
</s>
<s>
Allativ	Allativ	k1gInSc1
</s>
<s>
pišna	pišen	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
pē	pē	k1gFnSc1
<g/>
–	–	k?
</s>
<s>
Instrumentál	instrumentál	k1gInSc1
</s>
<s>
pišnitpišnitpē	pišnitpišnitpē	k5eAaImF,k5eAaPmF
</s>
<s>
Slovesa	sloveso	k1gNnPc1
</s>
<s>
Slovesa	sloveso	k1gNnPc1
byla	být	k5eAaImAgNnP
oproti	oproti	k7c3
dalším	další	k2eAgMnPc3d1
raným	raný	k2eAgMnPc3d1
indoevropským	indoevropský	k2eAgMnPc3d1
jazykům	jazyk	k1gMnPc3
méně	málo	k6eAd2
komplikovaná	komplikovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišovala	rozlišovat	k5eAaImAgFnS
dva	dva	k4xCgInPc4
rody	rod	k1gInPc4
(	(	kIx(
<g/>
činný	činný	k2eAgMnSc1d1
a	a	k8xC
medium	medium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
časy	čas	k1gInPc1
(	(	kIx(
<g/>
přítomný	přítomný	k2eAgInSc1d1
a	a	k8xC
minulý	minulý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
způsoby	způsob	k1gInPc4
(	(	kIx(
<g/>
oznamovací	oznamovací	k2eAgFnSc7d1
a	a	k8xC
rozkazovací	rozkazovací	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovesa	sloveso	k1gNnPc1
měla	mít	k5eAaImAgNnP
kromě	kromě	k7c2
infinitivu	infinitiv	k1gInSc2
ještě	ještě	k9
supinum	supinum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tabulce	tabulka	k1gFnSc6
vidíte	vidět	k5eAaImIp2nP
příklad	příklad	k1gInSc4
skloňování	skloňování	k1gNnSc2
slovesa	sloveso	k1gNnSc2
„	„	k?
<g/>
být	být	k5eAaImF
<g/>
“	“	k?
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
sloveso	sloveso	k1gNnSc1
je	být	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
jen	jen	k9
v	v	k7c6
činném	činný	k2eAgInSc6d1
rodě	rod	k1gInSc6
a	a	k8xC
medium	medium	k1gNnSc1
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infinitiv	infinitiv	k1gInSc1
tohoto	tento	k3xDgNnSc2
slovesa	sloveso	k1gNnSc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
ašanna	ašanna	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
supinum	supinum	k1gNnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
ē	ē	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
skloňování	skloňování	k1gNnSc2
slovesa	sloveso	k1gNnSc2
„	„	k?
<g/>
být	být	k5eAaImF
<g/>
“	“	k?
</s>
<s>
Oznamovací	oznamovací	k2eAgFnSc1d1
</s>
<s>
Imperativ	imperativ	k1gInSc1
</s>
<s>
Přítomný	přítomný	k1gMnSc1
</s>
<s>
Minulý	minulý	k2eAgInSc4d1
</s>
<s>
Činný	činný	k2eAgInSc1d1
rod	rod	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
sg	sg	k?
</s>
<s>
ē	ē	k1gFnPc7
</s>
<s>
ē	ē	k1gMnSc1
</s>
<s>
ašallu	ašallat	k5eAaPmIp1nS
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
sg	sg	k?
</s>
<s>
ē	ē	k5eAaPmIp1nSwK
</s>
<s>
ē	ē	k1gMnSc1
</s>
<s>
ē	ē	k5eAaPmIp2nS
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
sg	sg	k?
</s>
<s>
ē	ē	k1gMnPc1
</s>
<s>
ē	ē	k1gMnSc1
</s>
<s>
ē	ē	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
pl	pl	k?
</s>
<s>
ašueni	ašuen	k2eAgMnPc1d1
</s>
<s>
ē	ē	k1gInSc1
</s>
<s>
ašueni	ašuen	k2eAgMnPc1d1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
pl	pl	k?
</s>
<s>
ašteni	ašten	k2eAgMnPc1d1
</s>
<s>
ē	ē	k2eAgInSc1d1
</s>
<s>
ē	ē	k2eAgInSc1d1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
pl	pl	k?
</s>
<s>
ašanzi	ašanh	k1gMnPc1
</s>
<s>
ē	ē	k1gMnSc1
</s>
<s>
ašandu	ašanda	k1gFnSc4
</s>
<s>
Syntax	syntax	k1gFnSc1
</s>
<s>
Chetitština	chetitština	k1gFnSc1
byla	být	k5eAaImAgFnS
syntetický	syntetický	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovosled	slovosled	k1gInSc1
byl	být	k5eAaImAgInS
podle	podle	k7c2
stylu	styl	k1gInSc2
„	„	k?
<g/>
podmět-předmět-sloveso	podmět-předmět-slovesa	k1gFnSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Hittite	Hittit	k1gInSc5
Grammar	Grammar	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Dita	Dita	k1gFnSc1
Frantíková	Frantíková	k1gFnSc1
<g/>
:	:	kIx,
Chetitská	chetitský	k2eAgFnSc1d1
čítanka	čítanka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7308-644-2	978-80-7308-644-2	k4
</s>
<s>
Jörg	Jörg	k1gMnSc1
Klinger	Klinger	k1gMnSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Hethiter	Hethiter	k1gMnSc1
<g/>
,	,	kIx,
<g/>
C.H.	C.H.	k1gMnSc1
Beck	Beck	k1gMnSc1
oHG	oHG	k?
<g/>
,	,	kIx,
München	München	k1gInSc1
2007	#num#	k4
</s>
<s>
Alwin	Alwin	k2eAgInSc1d1
Kloekhorst	Kloekhorst	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Etymological	Etymological	k1gFnSc1
Dictionary	Dictionara	k1gFnSc2
of	of	k?
the	the	k?
Hittite	Hittit	k1gInSc5
Inherited	Inherited	k1gMnSc1
Lexicon	Lexicon	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Brill	Brillum	k1gNnPc2
<g/>
,	,	kIx,
Leiden	Leidna	k1gFnPc2
2008	#num#	k4
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
:	:	kIx,
Za	za	k7c7
tajemstvím	tajemství	k1gNnSc7
říše	říš	k1gFnSc2
Chetitů	Chetit	k1gMnPc2
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1961	#num#	k4
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
W.	W.	kA
Fortson	Fortson	k1gMnSc1
<g/>
:	:	kIx,
Indo-European	Indo-European	k1gMnSc1
Language	language	k1gFnSc2
and	and	k?
Culture	Cultur	k1gMnSc5
<g/>
:	:	kIx,
An	An	k1gMnSc6
Introduction	Introduction	k1gInSc4
<g/>
,	,	kIx,
Blackwell	Blackwella	k1gFnPc2
<g/>
,	,	kIx,
Malden	Maldna	k1gFnPc2
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
pag	pag	k?
<g/>
.	.	kIx.
158	#num#	k4
<g/>
–	–	k?
<g/>
167	#num#	k4
</s>
<s>
Calvert	Calvert	k1gInSc1
Watkins	Watkins	k1gInSc1
<g/>
:	:	kIx,
Hittite	Hittit	k1gInSc5
<g/>
,	,	kIx,
V	V	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
ancient	ancient	k1gMnSc1
languages	languages	k1gMnSc1
of	of	k?
Asia	Asia	k1gMnSc1
Minor	minor	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ed	Ed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
D.	D.	kA
Woodard	Woodard	k1gMnSc1
<g/>
,	,	kIx,
<g/>
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
pag	pag	k?
<g/>
.	.	kIx.
6	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
</s>
<s>
Marek	Marek	k1gMnSc1
Rychtařík	Rychtařík	k1gMnSc1
<g/>
:	:	kIx,
Chetitština	chetitština	k1gFnSc1
<g/>
,	,	kIx,
V	V	kA
<g/>
:	:	kIx,
Jazyky	jazyk	k1gInPc1
starého	starý	k2eAgInSc2d1
Orientu	Orient	k1gInSc2
<g/>
,	,	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
pag	pag	k?
<g/>
.	.	kIx.
61	#num#	k4
<g/>
—	—	k?
<g/>
65	#num#	k4
<g/>
,	,	kIx,
139	#num#	k4
<g/>
—	—	k?
<g/>
142	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Anatolské	anatolský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Indoevropské	indoevropský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Chetité	Chetita	k1gMnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chetitština	chetitština	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
chetitština	chetitština	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4120195-4	4120195-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85061276	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85061276	#num#	k4
</s>
