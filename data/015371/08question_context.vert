<s>
Chetitština	chetitština	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
chetitštině	chetitština	k1gFnSc6
𒉈	𒉈	k?
<g/>
,	,	kIx,
nešili	šít	k5eNaImAgMnP
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vymřelý	vymřelý	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
z	z	k7c2
anatolské	anatolský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
indoevropských	indoevropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
nejstarší	starý	k2eAgInSc1d3
zaznamenaný	zaznamenaný	k2eAgInSc1d1
indoevropský	indoevropský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>