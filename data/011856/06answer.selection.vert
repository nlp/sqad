<s>
Mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
drobnohled	drobnohled	k1gInSc1	drobnohled
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
optický	optický	k2eAgInSc4d1	optický
přístroj	přístroj	k1gInSc4	přístroj
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
malého	malý	k2eAgInSc2d1	malý
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
objektu	objekt	k1gInSc2	objekt
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
zvětšení	zvětšení	k1gNnSc6	zvětšení
<g/>
.	.	kIx.	.
</s>
