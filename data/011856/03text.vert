<p>
<s>
Mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
drobnohled	drobnohled	k1gInSc1	drobnohled
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
optický	optický	k2eAgInSc4d1	optický
přístroj	přístroj	k1gInSc4	přístroj
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
malého	malý	k2eAgInSc2d1	malý
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
objektu	objekt	k1gInSc2	objekt
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
zvětšení	zvětšení	k1gNnSc6	zvětšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
mikroskop	mikroskop	k1gInSc1	mikroskop
je	on	k3xPp3gMnPc4	on
obvykle	obvykle	k6eAd1	obvykle
myšlen	myšlen	k2eAgInSc1d1	myšlen
optický	optický	k2eAgInSc1d1	optický
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
využívá	využívat	k5eAaImIp3nS	využívat
světelných	světelný	k2eAgInPc2d1	světelný
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
mikroskopy	mikroskop	k1gInPc1	mikroskop
<g/>
,	,	kIx,	,
např.	např.	kA	např.
elektronový	elektronový	k2eAgInSc1d1	elektronový
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
první	první	k4xOgInSc4	první
drobnohled	drobnohled	k1gInSc4	drobnohled
sestavil	sestavit	k5eAaPmAgMnS	sestavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1590	[number]	k4	1590
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
Zacharias	Zachariasa	k1gFnPc2	Zachariasa
Janssen	Janssno	k1gNnPc2	Janssno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Jansenovy	Jansenův	k2eAgFnSc2d1	Jansenův
konstrukce	konstrukce	k1gFnSc2	konstrukce
mikroskopií	mikroskopie	k1gFnSc7	mikroskopie
zabýval	zabývat	k5eAaImAgMnS	zabývat
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnSc1	Galilei
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
mikroskopů	mikroskop	k1gInPc2	mikroskop
sestavil	sestavit	k5eAaPmAgMnS	sestavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
holandský	holandský	k2eAgMnSc1d1	holandský
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
Anton	Anton	k1gMnSc1	Anton
van	vana	k1gFnPc2	vana
Leeuwenhoek	Leeuwenhoky	k1gFnPc2	Leeuwenhoky
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
práce	práce	k1gFnSc2	práce
patřily	patřit	k5eAaImAgInP	patřit
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
mikroskopického	mikroskopický	k2eAgNnSc2d1	mikroskopické
pozorování	pozorování	k1gNnSc2	pozorování
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
přelomem	přelom	k1gInSc7	přelom
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
britského	britský	k2eAgMnSc2d1	britský
geologa	geolog	k1gMnSc2	geolog
Roberta	Robert	k1gMnSc2	Robert
Hooka	Hooka	k1gFnSc1	Hooka
Micrographia	Micrographia	k1gFnSc1	Micrographia
vydané	vydaný	k2eAgNnSc4d1	vydané
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1665	[number]	k4	1665
konstrukci	konstrukce	k1gFnSc4	konstrukce
mikroskopu	mikroskop	k1gInSc2	mikroskop
s	s	k7c7	s
odděleným	oddělený	k2eAgInSc7d1	oddělený
objektivem	objektiv	k1gInSc7	objektiv
<g/>
,	,	kIx,	,
okulárem	okulár	k1gInSc7	okulár
a	a	k8xC	a
osvětlovacím	osvětlovací	k2eAgNnSc7d1	osvětlovací
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
mnoho	mnoho	k4c1	mnoho
zobrazení	zobrazení	k1gNnPc2	zobrazení
získaných	získaný	k2eAgFnPc2d1	získaná
pomocí	pomoc	k1gFnPc2	pomoc
mikroskopů	mikroskop	k1gInPc2	mikroskop
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
doloženy	doložen	k2eAgFnPc1d1	doložena
možnosti	možnost	k1gFnPc1	možnost
přístroje	přístroj	k1gInSc2	přístroj
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
zahájila	zahájit	k5eAaPmAgFnS	zahájit
výrobu	výroba	k1gFnSc4	výroba
mikroskopů	mikroskop	k1gInPc2	mikroskop
firma	firma	k1gFnSc1	firma
Carl	Carl	k1gInSc1	Carl
Zeiss	Zeiss	k1gInSc1	Zeiss
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
mikroskopu	mikroskop	k1gInSc2	mikroskop
==	==	k?	==
</s>
</p>
<p>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
mikroskop	mikroskop	k1gInSc1	mikroskop
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
optické	optický	k2eAgFnSc2d1	optická
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
promítání	promítání	k1gNnSc4	promítání
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
mechanické	mechanický	k2eAgFnSc3d1	mechanická
části	část	k1gFnSc3	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
optickou	optický	k2eAgFnSc4d1	optická
část	část	k1gFnSc4	část
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tělo	tělo	k1gNnSc4	tělo
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mechanická	mechanický	k2eAgFnSc1d1	mechanická
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
stativu	stativ	k1gInSc2	stativ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
drží	držet	k5eAaImIp3nS	držet
tělo	tělo	k1gNnSc4	tělo
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
,	,	kIx,	,
a	a	k8xC	a
stolku	stolek	k1gInSc2	stolek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
stativu	stativ	k1gInSc2	stativ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
makro-	makro-	k?	makro-
a	a	k8xC	a
mikrošroub	mikrošroub	k1gInSc4	mikrošroub
sloužící	sloužící	k2eAgInSc4d1	sloužící
k	k	k7c3	k
ostření	ostření	k1gNnSc3	ostření
na	na	k7c4	na
preparát	preparát	k1gInSc4	preparát
<g/>
.	.	kIx.	.
</s>
<s>
Stolek	stolek	k1gInSc1	stolek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
křížový	křížový	k2eAgInSc4d1	křížový
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pohybujeme	pohybovat	k5eAaImIp1nP	pohybovat
pomocí	pomocí	k7c2	pomocí
šroubů	šroub	k1gInPc2	šroub
preparátem	preparát	k1gInSc7	preparát
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kulatý	kulatý	k2eAgInSc4d1	kulatý
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
posazení	posazení	k1gNnSc4	posazení
Petriho	Petri	k1gMnSc4	Petri
misek	miska	k1gFnPc2	miska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stolcích	stolec	k1gInPc6	stolec
bývají	bývat	k5eAaImIp3nP	bývat
zvýrazněné	zvýrazněný	k2eAgFnPc1d1	zvýrazněná
souřadnice	souřadnice	k1gFnPc1	souřadnice
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
preparátem	preparát	k1gInSc7	preparát
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stolkem	stolek	k1gInSc7	stolek
pak	pak	k6eAd1	pak
u	u	k7c2	u
lepších	dobrý	k2eAgInPc2d2	lepší
mikroskopů	mikroskop	k1gInPc2	mikroskop
bývá	bývat	k5eAaImIp3nS	bývat
umístěn	umístěn	k2eAgInSc1d1	umístěn
nosič	nosič	k1gInSc1	nosič
kondenzoru	kondenzor	k1gInSc2	kondenzor
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
hlavice	hlavice	k1gFnSc1	hlavice
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mikroskopu	mikroskop	k1gInSc2	mikroskop
možnost	možnost	k1gFnSc4	možnost
měnit	měnit	k5eAaImF	měnit
objektivy	objektiv	k1gInPc4	objektiv
různých	různý	k2eAgFnPc2d1	různá
zvětšení	zvětšení	k1gNnPc2	zvětšení
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
umístěný	umístěný	k2eAgInSc1d1	umístěný
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
revolverový	revolverový	k2eAgInSc4d1	revolverový
nosič	nosič	k1gInSc4	nosič
objektivů	objektiv	k1gInPc2	objektiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optická	optický	k2eAgFnSc1d1	optická
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
okulárů	okulár	k1gInPc2	okulár
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
díváme	dívat	k5eAaImIp1nP	dívat
na	na	k7c4	na
preparát	preparát	k1gInSc4	preparát
<g/>
,	,	kIx,	,
objektivů	objektiv	k1gInPc2	objektiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
obraz	obraz	k1gInSc4	obraz
preparátu	preparát	k1gInSc2	preparát
<g/>
,	,	kIx,	,
a	a	k8xC	a
osvětlení	osvětlení	k1gNnSc1	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
mikroskopu	mikroskop	k1gInSc2	mikroskop
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
optické	optický	k2eAgFnSc2d1	optická
části	část	k1gFnSc2	část
i	i	k8xC	i
polní	polní	k2eAgFnSc1d1	polní
clona	clona	k1gFnSc1	clona
<g/>
,	,	kIx,	,
kondenzor	kondenzor	k1gInSc1	kondenzor
<g/>
,	,	kIx,	,
sběrná	sběrný	k2eAgFnSc1d1	sběrná
čočka	čočka	k1gFnSc1	čočka
a	a	k8xC	a
přídavné	přídavný	k2eAgInPc1d1	přídavný
filtry	filtr	k1gInPc1	filtr
(	(	kIx(	(
<g/>
polarizační	polarizační	k2eAgMnSc1d1	polarizační
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Optická	optický	k2eAgFnSc1d1	optická
soustava	soustava	k1gFnSc1	soustava
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
mikroskopu	mikroskop	k1gInSc2	mikroskop
jsou	být	k5eAaImIp3nP	být
čočky	čočka	k1gFnPc1	čočka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
objektiv	objektiv	k1gInSc4	objektiv
a	a	k8xC	a
okulár	okulár	k1gInSc4	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Okuláry	okulár	k1gInPc1	okulár
a	a	k8xC	a
objektivy	objektiv	k1gInPc1	objektiv
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
výměnné	výměnný	k2eAgInPc1d1	výměnný
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
zvětšením	zvětšení	k1gNnSc7	zvětšení
–	–	k?	–
výhodnější	výhodný	k2eAgFnPc4d2	výhodnější
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnPc4	kombinace
většího	veliký	k2eAgNnSc2d2	veliký
zvětšení	zvětšení	k1gNnSc2	zvětšení
u	u	k7c2	u
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
a	a	k8xC	a
menšího	malý	k2eAgInSc2d2	menší
u	u	k7c2	u
okuláru	okulár	k1gInSc2	okulár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
okulárové	okulárový	k2eAgFnSc2d1	okulárová
clony	clona	k1gFnSc2	clona
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
průhledný	průhledný	k2eAgInSc1d1	průhledný
skleněný	skleněný	k2eAgInSc1d1	skleněný
disk	disk	k1gInSc1	disk
například	například	k6eAd1	například
s	s	k7c7	s
měřítkem	měřítko	k1gNnSc7	měřítko
nebo	nebo	k8xC	nebo
čtvercovou	čtvercový	k2eAgFnSc7d1	čtvercová
sítí	síť	k1gFnSc7	síť
určený	určený	k2eAgInSc4d1	určený
například	například	k6eAd1	například
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
počítání	počítání	k1gNnSc3	počítání
sledovaných	sledovaný	k2eAgInPc2d1	sledovaný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
mikroskop	mikroskop	k1gInSc1	mikroskop
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
spojných	spojný	k2eAgFnPc2d1	spojná
soustav	soustava	k1gFnPc2	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
společnou	společný	k2eAgFnSc4d1	společná
optickou	optický	k2eAgFnSc4d1	optická
osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nazývána	nazýván	k2eAgFnSc1d1	nazývána
objektiv	objektiv	k1gInSc1	objektiv
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
malou	malý	k2eAgFnSc4d1	malá
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
předmět	předmět	k1gInSc1	předmět
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
blízko	blízko	k6eAd1	blízko
před	před	k7c4	před
předmětové	předmětový	k2eAgNnSc4d1	předmětové
ohnisko	ohnisko	k1gNnSc4	ohnisko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vzniká	vznikat	k5eAaImIp3nS	vznikat
skutečný	skutečný	k2eAgInSc4d1	skutečný
<g/>
,	,	kIx,	,
zvětšený	zvětšený	k2eAgInSc4d1	zvětšený
a	a	k8xC	a
převrácený	převrácený	k2eAgInSc4d1	převrácený
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
vzniká	vznikat	k5eAaImIp3nS	vznikat
mezi	mezi	k7c7	mezi
druhou	druhý	k4xOgFnSc7	druhý
částí	část	k1gFnSc7	část
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
okulárem	okulár	k1gInSc7	okulár
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
předmětovým	předmětový	k2eAgNnSc7d1	předmětové
ohniskem	ohnisko	k1gNnSc7	ohnisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
obraz	obraz	k1gInSc4	obraz
pak	pak	k6eAd1	pak
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
okulárem	okulár	k1gInSc7	okulár
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
lupou	lupat	k5eAaImIp3nP	lupat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získáváme	získávat	k5eAaImIp1nP	získávat
další	další	k2eAgNnSc4d1	další
zvětšení	zvětšení	k1gNnSc4	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
okuláru	okulár	k1gInSc2	okulár
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Obrazové	obrazový	k2eAgNnSc1d1	obrazové
ohnisko	ohnisko	k1gNnSc1	ohnisko
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
předmětové	předmětový	k2eAgNnSc4d1	předmětové
ohnisko	ohnisko	k1gNnSc4	ohnisko
okuláru	okulár	k1gInSc2	okulár
nesplývají	splývat	k5eNaImIp3nP	splývat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
o	o	k7c4	o
hodnotu	hodnota	k1gFnSc4	hodnota
optického	optický	k2eAgInSc2d1	optický
intervalu	interval	k1gInSc2	interval
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
u	u	k7c2	u
mikroskopu	mikroskop	k1gInSc2	mikroskop
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
15	[number]	k4	15
cm	cm	kA	cm
a	a	k8xC	a
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
mikroskopů	mikroskop	k1gInPc2	mikroskop
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
mikroskopů	mikroskop	k1gInPc2	mikroskop
využívajících	využívající	k2eAgInPc2d1	využívající
odlišné	odlišný	k2eAgInPc4d1	odlišný
zdroje	zdroj	k1gInPc4	zdroj
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasické	klasický	k2eAgInPc1d1	klasický
mikroskopy	mikroskop	k1gInPc1	mikroskop
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
a	a	k8xC	a
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
jsou	být	k5eAaImIp3nP	být
klasické	klasický	k2eAgInPc1d1	klasický
světelné	světelný	k2eAgInPc1d1	světelný
mikroskopy	mikroskop	k1gInPc1	mikroskop
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
obvykle	obvykle	k6eAd1	obvykle
bílé	bílý	k2eAgNnSc4d1	bílé
světlo	světlo	k1gNnSc4	světlo
ze	z	k7c2	z
zdroje	zdroj	k1gInSc2	zdroj
(	(	kIx(	(
<g/>
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dělíme	dělit	k5eAaImIp1nP	dělit
je	on	k3xPp3gFnPc4	on
na	na	k7c4	na
mikroskopy	mikroskop	k1gInPc4	mikroskop
monokulární	monokulární	k2eAgInSc1d1	monokulární
<g/>
,	,	kIx,	,
binokulární	binokulární	k2eAgInPc1d1	binokulární
(	(	kIx(	(
<g/>
např.	např.	kA	např.
binokulární	binokulární	k2eAgInPc1d1	binokulární
lupy	lup	k1gInPc1	lup
<g/>
)	)	kIx)	)
a	a	k8xC	a
trinokulární	trinokulární	k2eAgInPc4d1	trinokulární
mikroskopy	mikroskop	k1gInPc4	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Monokulární	Monokulární	k2eAgInPc1d1	Monokulární
mikroskopy	mikroskop	k1gInPc1	mikroskop
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgInSc4	jeden
okulár	okulár	k1gInSc4	okulár
<g/>
.	.	kIx.	.
</s>
<s>
Binokulární	binokulární	k2eAgInPc1d1	binokulární
mikroskopy	mikroskop	k1gInPc1	mikroskop
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
okuláry	okulár	k1gInPc4	okulár
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
tzv.	tzv.	kA	tzv.
binokulární	binokulární	k2eAgFnSc7d1	binokulární
hlavicí	hlavice	k1gFnSc7	hlavice
–	–	k?	–
tj.	tj.	kA	tj.
hlavicí	hlavice	k1gFnSc7	hlavice
mající	mající	k2eAgInSc4d1	mající
dva	dva	k4xCgInPc4	dva
okulárové	okulárový	k2eAgInPc4d1	okulárový
tubusy	tubus	k1gInPc4	tubus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
každého	každý	k3xTgInSc2	každý
okuláru	okulár	k1gInSc2	okulár
pak	pak	k6eAd1	pak
odchází	odcházet	k5eAaImIp3nS	odcházet
jeden	jeden	k4xCgInSc1	jeden
svazek	svazek	k1gInSc1	svazek
paprsků	paprsek	k1gInPc2	paprsek
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
pohodlně	pohodlně	k6eAd1	pohodlně
dívat	dívat	k5eAaImF	dívat
oběma	dva	k4xCgInPc7	dva
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
trinokulární	trinokulární	k2eAgInSc1d1	trinokulární
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dva	dva	k4xCgInPc1	dva
tubusy	tubus	k1gInPc1	tubus
jsou	být	k5eAaImIp3nP	být
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnPc4	pozorování
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
nebo	nebo	k8xC	nebo
kamery	kamera	k1gFnSc2	kamera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stereomikroskopy	Stereomikroskop	k1gInPc4	Stereomikroskop
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
binokulárním	binokulární	k2eAgInSc7d1	binokulární
mikroskopem	mikroskop	k1gInSc7	mikroskop
je	být	k5eAaImIp3nS	být
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
stereomikroskop	stereomikroskop	k1gInSc1	stereomikroskop
neboli	neboli	k8xC	neboli
binokulární	binokulární	k2eAgFnSc1d1	binokulární
lupa	lupa	k1gFnSc1	lupa
či	či	k8xC	či
binolupa	binolupa	k1gFnSc1	binolupa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
dvěma	dva	k4xCgInPc7	dva
okuláry	okulár	k1gInPc7	okulár
<g/>
,	,	kIx,	,
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
tak	tak	k9	tak
velkého	velký	k2eAgNnSc2d1	velké
zvětšení	zvětšení	k1gNnSc2	zvětšení
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
100	[number]	k4	100
<g/>
×	×	k?	×
zvětšení	zvětšení	k1gNnSc1	zvětšení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
velký	velký	k2eAgInSc4d1	velký
pracovní	pracovní	k2eAgInSc4d1	pracovní
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
nazýván	nazývat	k5eAaImNgInS	nazývat
preparační	preparační	k2eAgInSc1d1	preparační
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
přímou	přímý	k2eAgFnSc4d1	přímá
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
preparátem	preparát	k1gInSc7	preparát
pod	pod	k7c7	pod
objektivem	objektiv	k1gInSc7	objektiv
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
vybaven	vybavit	k5eAaPmNgInS	vybavit
hranolem	hranol	k1gInSc7	hranol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převrací	převracet	k5eAaImIp3nS	převracet
obraz	obraz	k1gInSc4	obraz
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
mikroskopu	mikroskop	k1gInSc2	mikroskop
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
že	že	k8xS	že
výsledný	výsledný	k2eAgInSc1d1	výsledný
obraz	obraz	k1gInSc1	obraz
nevidíme	vidět	k5eNaImIp1nP	vidět
převráceně	převráceně	k6eAd1	převráceně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Binolupa	Binolupa	k1gFnSc1	Binolupa
nasvěcuje	nasvěcovat	k5eAaImIp3nS	nasvěcovat
preparát	preparát	k1gInSc4	preparát
seshora	seshora	k6eAd1	seshora
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pozorovat	pozorovat	k5eAaImF	pozorovat
neprůhledné	průhledný	k2eNgInPc4d1	neprůhledný
objekty	objekt	k1gInPc4	objekt
(	(	kIx(	(
<g/>
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
v	v	k7c6	v
entomologii	entomologie	k1gFnSc6	entomologie
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
oborech	obor	k1gInPc6	obor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polarizační	polarizační	k2eAgInPc1d1	polarizační
mikroskopy	mikroskop	k1gInPc1	mikroskop
===	===	k?	===
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnSc7d1	speciální
verzí	verze	k1gFnSc7	verze
světelného	světelný	k2eAgInSc2d1	světelný
mikroskopu	mikroskop	k1gInSc2	mikroskop
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
polarizační	polarizační	k2eAgInSc1d1	polarizační
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
polarizování	polarizování	k1gNnSc3	polarizování
procházejícího	procházející	k2eAgNnSc2d1	procházející
světla	světlo	k1gNnSc2	světlo
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sledování	sledování	k1gNnSc2	sledování
dvojlomných	dvojlomný	k2eAgFnPc2d1	dvojlomná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
je	být	k5eAaImIp3nS	být
také	také	k9	také
fluorescenční	fluorescenční	k2eAgInSc1d1	fluorescenční
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
opět	opět	k6eAd1	opět
pomocí	pomocí	k7c2	pomocí
soustavy	soustava	k1gFnSc2	soustava
filtrů	filtr	k1gInPc2	filtr
propouští	propouštět	k5eAaImIp3nS	propouštět
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
světla	světlo	k1gNnSc2	světlo
nutnou	nutný	k2eAgFnSc4d1	nutná
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
fluorescence	fluorescence	k1gFnSc2	fluorescence
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
například	například	k6eAd1	například
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konfokální	konfokální	k2eAgInPc1d1	konfokální
mikroskopy	mikroskop	k1gInPc1	mikroskop
===	===	k?	===
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
konfokální	konfokální	k2eAgInPc4d1	konfokální
mikroskopy	mikroskop	k1gInPc4	mikroskop
schopné	schopný	k2eAgNnSc1d1	schopné
zaostřit	zaostřit	k5eAaPmF	zaostřit
na	na	k7c4	na
úzkou	úzký	k2eAgFnSc4d1	úzká
optickou	optický	k2eAgFnSc4d1	optická
rovinu	rovina	k1gFnSc4	rovina
používají	používat	k5eAaImIp3nP	používat
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
světla	světlo	k1gNnSc2	světlo
lasery	laser	k1gInPc1	laser
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
typem	typ	k1gInSc7	typ
mikroskopu	mikroskop	k1gInSc2	mikroskop
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
mikroskop	mikroskop	k1gInSc1	mikroskop
elektronový	elektronový	k2eAgInSc1d1	elektronový
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
místo	místo	k7c2	místo
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
proudu	proud	k1gInSc2	proud
fotonů	foton	k1gInPc2	foton
<g/>
)	)	kIx)	)
využívá	využívat	k5eAaPmIp3nS	využívat
proud	proud	k1gInSc1	proud
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Papírové	papírový	k2eAgInPc1d1	papírový
mikroskopy	mikroskop	k1gInPc1	mikroskop
===	===	k?	===
</s>
</p>
<p>
<s>
Americký	americký	k2eAgMnSc1d1	americký
biofyzik	biofyzik	k1gMnSc1	biofyzik
Manu	manout	k5eAaImIp1nS	manout
Prakash	Prakash	k1gInSc4	Prakash
na	na	k7c4	na
Standford	Standford	k1gInSc4	Standford
University	universita	k1gFnSc2	universita
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
studenty	student	k1gMnPc7	student
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
extrémně	extrémně	k6eAd1	extrémně
levný	levný	k2eAgInSc4d1	levný
mikroskop	mikroskop	k1gInSc4	mikroskop
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
několika	několik	k4yIc2	několik
optických	optický	k2eAgMnPc2d1	optický
a	a	k8xC	a
elektronických	elektronický	k2eAgInPc2d1	elektronický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyrobit	vyrobit	k5eAaPmF	vyrobit
z	z	k7c2	z
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mikroskopy	mikroskop	k1gInPc1	mikroskop
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
specializované	specializovaný	k2eAgInPc1d1	specializovaný
na	na	k7c4	na
indikaci	indikace	k1gFnSc4	indikace
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
mikrobů	mikrob	k1gInPc2	mikrob
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
velmi	velmi	k6eAd1	velmi
skladné	skladný	k2eAgNnSc1d1	skladné
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
výrobní	výrobní	k2eAgFnSc1d1	výrobní
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
50	[number]	k4	50
centů	cent	k1gInPc2	cent
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10	[number]	k4	10
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakash	Prakash	k1gInSc1	Prakash
(	(	kIx(	(
<g/>
v	v	k7c6	v
koordinaci	koordinace	k1gFnSc6	koordinace
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
<g/>
)	)	kIx)	)
jimi	on	k3xPp3gInPc7	on
hodlá	hodlat	k5eAaImIp3nS	hodlat
vybavit	vybavit	k5eAaPmF	vybavit
laboratoře	laboratoř	k1gFnPc4	laboratoř
a	a	k8xC	a
ordinace	ordinace	k1gFnPc4	ordinace
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zvětšení	zvětšení	k1gNnSc2	zvětšení
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
úhlové	úhlový	k2eAgNnSc4d1	úhlové
zvětšení	zvětšení	k1gNnSc4	zvětšení
mikroskopu	mikroskop	k1gInSc2	mikroskop
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc1	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
γ	γ	k?	γ
</s>
</p>
<p>
</p>
<p>
<s>
γ	γ	k?	γ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
γ	γ	k?	γ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnSc2	gammum
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
γ	γ	k?	γ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnPc4	gammum
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
označuje	označovat	k5eAaImIp3nS	označovat
zvětšení	zvětšení	k1gNnSc1	zvětšení
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
okuláru	okulár	k1gInSc2	okulár
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
obrazová	obrazový	k2eAgFnSc1d1	obrazová
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
předmětová	předmětový	k2eAgFnSc1d1	předmětová
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
okuláru	okulár	k1gInSc2	okulár
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
optický	optický	k2eAgInSc4d1	optický
interval	interval	k1gInSc4	interval
mikroskopu	mikroskop	k1gInSc2	mikroskop
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konvenční	konvenční	k2eAgFnSc1d1	konvenční
zraková	zrakový	k2eAgFnSc1d1	zraková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optickým	optický	k2eAgInSc7d1	optický
mikroskopem	mikroskop	k1gInSc7	mikroskop
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zvětšení	zvětšení	k1gNnSc1	zvětšení
50	[number]	k4	50
<g/>
×	×	k?	×
až	až	k9	až
1000	[number]	k4	1000
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgNnSc1d1	maximální
teoretické	teoretický	k2eAgNnSc1d1	teoretické
zvětšení	zvětšení	k1gNnSc1	zvětšení
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2000	[number]	k4	2000
<g/>
×	×	k?	×
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
bariéry	bariéra	k1gFnPc4	bariéra
kvůli	kvůli	k7c3	kvůli
omezení	omezení	k1gNnSc3	omezení
délky	délka	k1gFnSc2	délka
světelných	světelný	k2eAgFnPc2d1	světelná
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
==	==	k?	==
</s>
</p>
<p>
<s>
FRET	fretum	k1gNnPc2	fretum
(	(	kIx(	(
<g/>
fluorescence	fluorescence	k1gFnSc1	fluorescence
resonance	resonance	k1gFnSc2	resonance
energy	energ	k1gInPc1	energ
transfer	transfer	k1gInSc1	transfer
<g/>
)	)	kIx)	)
mikroskopie	mikroskopie	k1gFnSc1	mikroskopie
</s>
</p>
<p>
<s>
TIRF	TIRF	kA	TIRF
(	(	kIx(	(
<g/>
total	totat	k5eAaImAgInS	totat
internal	internat	k5eAaPmAgInS	internat
reflection	reflection	k1gInSc1	reflection
fluorescence	fluorescence	k1gFnSc2	fluorescence
<g/>
)	)	kIx)	)
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
</s>
</p>
<p>
<s>
AFM	AFM	kA	AFM
(	(	kIx(	(
<g/>
atomic	atomic	k1gMnSc1	atomic
force	force	k1gFnSc2	force
microscopy	microscopa	k1gFnSc2	microscopa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SICM	SICM	kA	SICM
(	(	kIx(	(
<g/>
scanning	scanning	k1gInSc1	scanning
ion	ion	k1gInSc1	ion
conductance	conductance	k1gFnSc2	conductance
microscopy	microscopa	k1gFnSc2	microscopa
<g/>
)	)	kIx)	)
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
</s>
</p>
<p>
<s>
STED	STED	kA	STED
(	(	kIx(	(
<g/>
stimulated	stimulated	k1gInSc1	stimulated
emission	emission	k1gInSc1	emission
depletion	depletion	k1gInSc1	depletion
<g/>
)	)	kIx)	)
mikroskopie	mikroskopie	k1gFnSc1	mikroskopie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Elektronový	elektronový	k2eAgInSc1d1	elektronový
mikroskop	mikroskop	k1gInSc1	mikroskop
</s>
</p>
<p>
<s>
Fluorescenční	fluorescenční	k2eAgInSc1d1	fluorescenční
mikroskop	mikroskop	k1gInSc1	mikroskop
</s>
</p>
<p>
<s>
Konfokální	konfokální	k2eAgInSc1d1	konfokální
mikroskop	mikroskop	k1gInSc1	mikroskop
</s>
</p>
<p>
<s>
Polarizační	polarizační	k2eAgInSc1d1	polarizační
mikroskop	mikroskop	k1gInSc1	mikroskop
</s>
</p>
<p>
<s>
AFM	AFM	kA	AFM
neboli	neboli	k8xC	neboli
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
atomárních	atomární	k2eAgFnPc2d1	atomární
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
<s>
Mikrotom	mikrotom	k1gInSc1	mikrotom
</s>
</p>
<p>
<s>
Lupa	lupa	k1gFnSc1	lupa
</s>
</p>
<p>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
</s>
</p>
<p>
<s>
Mikrofotografie	mikrofotografie	k1gFnSc1	mikrofotografie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mikroskop	mikroskop	k1gInSc1	mikroskop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mikroskop	mikroskop	k1gInSc1	mikroskop
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
