<s>
Paraguay	Paraguay	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
státě	stát	k1gInSc6
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
řece	řeka	k1gFnSc6
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Paraguay	Paraguay	k1gFnSc1
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Paraguayská	paraguayský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
República	Repúblic	k2eAgFnSc1d1
del	del	k?
ParaguayTetã	ParaguayTetã	k1gFnSc1
Paraguái	Paraguá	k1gFnSc2
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaParaguayos	HymnaParaguayos	k1gMnSc1
<g/>
,	,	kIx,
República	República	k1gMnSc1
o	o	k7c6
Muerte	Muert	k1gMnSc5
</s>
<s>
MottoMír	MottoMír	k1gInSc1
a	a	k8xC
spravedlnost	spravedlnost	k1gFnSc1
Geografie	geografie	k1gFnSc2
</s>
<s>
Poloha	poloha	k1gFnSc1
Paraguaye	Paraguay	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Asunción	Asunción	k1gInSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
406	#num#	k4
750	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
60	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Cerro	Cerro	k6eAd1
Peró	Peró	k?
(	(	kIx(
<g/>
Cerro	Cerro	k1gNnSc1
Tres	tresa	k1gFnPc2
Kandú	Kandú	k1gFnPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
842	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
-4	-4	k4
Poloha	poloha	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
58	#num#	k4
<g/>
°	°	k?
z.	z.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
7	#num#	k4
152	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
99	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
16	#num#	k4
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
196	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▲	▲	k?
0,752	0,752	k4
(	(	kIx(
<g/>
střední	střední	k2eAgFnSc2d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
98	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
španělština	španělština	k1gFnSc1
(	(	kIx(
<g/>
úřední	úřední	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
guaraní	guaranit	k5eAaPmIp3nP
(	(	kIx(
<g/>
úřední	úřední	k2eAgNnPc1d1
<g/>
)	)	kIx)
Náboženství	náboženství	k1gNnPc1
</s>
<s>
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
90	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
menonité	menonitý	k2eAgFnPc1d1
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
protestanti	protestant	k1gMnPc1
10	#num#	k4
<g/>
%	%	kIx~
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
prezidentská	prezidentský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1811	#num#	k4
(	(	kIx(
<g/>
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Mario	Mario	k1gMnSc1
Abdo	Abdo	k1gMnSc1
Benítez	Benítez	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Viceprezident	viceprezident	k1gMnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Afara	Afara	k1gMnSc1
Měna	měna	k1gFnSc1
</s>
<s>
paraguayský	paraguayský	k2eAgMnSc1d1
guaraní	guaranit	k5eAaPmIp3nS
(	(	kIx(
<g/>
PYG	PYG	kA
<g/>
)	)	kIx)
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
198	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
USD	USD	kA
(	(	kIx(
<g/>
106	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
600	#num#	k4
PRY	PRY	kA
PY	PY	kA
MPZ	MPZ	kA
</s>
<s>
PY	PY	kA
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+595	+595	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
py	py	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Paraguayská	paraguayský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vnitrozemský	vnitrozemský	k2eAgInSc1d1
stát	stát	k1gInSc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
obou	dva	k4xCgInPc6
březích	břeh	k1gInPc6
řeky	řeka	k1gFnSc2
Paraguay	Paraguay	k1gFnSc4
<g/>
,	,	kIx,
hraničí	hraničit	k5eAaImIp3nP
s	s	k7c7
Argentinou	Argentina	k1gFnSc7
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
jihozápadě	jihozápad	k1gInSc6
<g/>
,	,	kIx,
Brazílií	Brazílie	k1gFnSc7
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
a	a	k8xC
Bolívií	Bolívie	k1gFnSc7
na	na	k7c6
severozápadě	severozápad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
výkladů	výklad	k1gInPc2
názvu	název	k1gInSc2
Paraguay	Paraguay	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vznikl	vzniknout	k5eAaPmAgInS
složením	složení	k1gNnSc7
guaraníjských	guaraníjský	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
používaného	používaný	k2eAgInSc2d1
indiány	indián	k1gMnPc4
Guaraní	Guaraný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
para	para	k1gFnSc1
(	(	kIx(
<g/>
pestrý	pestrý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gua	gua	k?
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
guey	guey	k1gInPc1
(	(	kIx(
<g/>
země	zem	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dle	dle	k7c2
Benjamina	Benjamin	k1gMnSc2
Kurase	Kurasa	k1gFnSc6
česky	česky	k6eAd1
Pestrozemí	Pestrozemý	k2eAgMnPc1d1
či	či	k8xC
Pestrořící	Pestrořící	k2eAgMnPc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pará	pará	k1gFnSc1
(	(	kIx(
<g/>
oceán	oceán	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gua	gua	k?
(	(	kIx(
<g/>
k	k	k7c3
<g/>
/	/	kIx~
<g/>
od	od	k7c2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
y	y	k?
(	(	kIx(
<g/>
voda	voda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
znamenenající	znamenenající	k2eAgFnSc1d1
„	„	k?
<g/>
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jde	jít	k5eAaImIp3nS
až	až	k9
k	k	k7c3
oceánu	oceán	k1gInSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
výraz	výraz	k1gInSc1
původně	původně	k6eAd1
označoval	označovat	k5eAaImAgInS
pouze	pouze	k6eAd1
oblast	oblast	k1gFnSc4
Asunciónu	Asunción	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
ve	v	k7c6
španělštině	španělština	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
celou	celý	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Původ	původ	k1gInSc1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgInPc2
vodorovných	vodorovný	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchní	vrchní	k2eAgInSc1d1
pruh	pruh	k1gInSc1
je	být	k5eAaImIp3nS
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
střední	střední	k2eAgInSc1d1
pruh	pruh	k1gInSc1
bílý	bílý	k2eAgInSc1d1
a	a	k8xC
spodní	spodní	k2eAgInSc1d1
pruh	pruh	k1gInSc1
je	být	k5eAaImIp3nS
sytě	sytě	k6eAd1
modrý	modrý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červená	Červená	k1gFnSc1
(	(	kIx(
<g/>
červený	červený	k2eAgInSc1d1
pruh	pruh	k1gInSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
symbolizovat	symbolizovat	k5eAaImF
statečnost	statečnost	k1gFnSc4
<g/>
,	,	kIx,
hrdinství	hrdinství	k1gNnSc4
a	a	k8xC
spravedlnost	spravedlnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílá	bílé	k1gNnPc1
(	(	kIx(
<g/>
bílý	bílý	k2eAgInSc1d1
pruh	pruh	k1gInSc1
<g/>
)	)	kIx)
symbolizuje	symbolizovat	k5eAaImIp3nS
čistotu	čistota	k1gFnSc4
<g/>
,	,	kIx,
mír	mír	k1gInSc4
a	a	k8xC
jednotu	jednota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modrá	modrat	k5eAaImIp3nS
(	(	kIx(
<g/>
modrý	modrý	k2eAgInSc1d1
pruh	pruh	k1gInSc1
<g/>
)	)	kIx)
symbolizuje	symbolizovat	k5eAaImIp3nS
dobrotu	dobrota	k1gFnSc4
a	a	k8xC
mírnost	mírnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
bílého	bílý	k2eAgInSc2d1
pruhu	pruh	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
kruhu	kruh	k1gInSc6
věnec	věnec	k1gInSc4
z	z	k7c2
olivové	olivový	k2eAgFnSc2d1
ratolesti	ratolest	k1gFnSc2
a	a	k8xC
ve	v	k7c6
věnci	věnec	k1gInSc6
modrý	modrý	k2eAgInSc1d1
kruh	kruh	k1gInSc1
se	s	k7c7
známou	známý	k2eAgFnSc7d1
jihoamerickou	jihoamerický	k2eAgFnSc7d1
pěticípou	pěticípý	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
ještě	ještě	k6eAd1
zdobí	zdobit	k5eAaImIp3nS
nápis	nápis	k1gInSc1
REPUBLICA	REPUBLICA	kA
DEL	DEL	kA
PARAGUAY	Paraguay	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
Paraguayská	paraguayský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
známé	známý	k2eAgNnSc1d1
foto	foto	k1gNnSc1
Francisca	Francisca	k1gFnSc1
Solana	Solana	k1gFnSc1
Lopeze	Lopeze	k1gFnSc1
před	před	k7c7
jeho	jeho	k3xOp3gFnSc7
smrtí	smrt	k1gFnSc7
</s>
<s>
O	o	k7c6
období	období	k1gNnSc6
před	před	k7c7
příchodem	příchod	k1gInSc7
Španělů	Španěl	k1gMnPc2
toho	ten	k3xDgNnSc2
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
jisté	jistý	k2eAgNnSc1d1
že	že	k8xS
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
dnešní	dnešní	k2eAgFnSc2d1
Paraguaye	Paraguay	k1gFnSc2
obývali	obývat	k5eAaImAgMnP
Indiáni	Indián	k1gMnPc1
označovaní	označovaný	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
Guaraní	Guaraný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
Paraguaye	Paraguay	k1gFnSc2
působili	působit	k5eAaImAgMnP
v	v	k7c6
koloniálním	koloniální	k2eAgNnSc6d1
období	období	k1gNnSc6
jezuité	jezuita	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
zde	zde	k6eAd1
zakládali	zakládat	k5eAaImAgMnP
své	svůj	k3xOyFgFnPc4
redukce	redukce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezuitské	jezuitský	k2eAgFnPc4d1
misie	misie	k1gFnPc4
La	la	k1gNnSc2
Santísima	Santísima	k1gNnSc1
Trinidad	Trinidad	k1gInSc1
del	del	k?
Paraná	Paraná	k1gFnSc1
a	a	k8xC
Jesús	Jesús	k1gInSc1
de	de	k?
Tavarangué	Tavaranguá	k1gFnSc2
v	v	k7c6
současnosti	současnost	k1gFnSc6
figurují	figurovat	k5eAaImIp3nP
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
organizace	organizace	k1gFnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nezávislou	závislý	k2eNgFnSc4d1
v	v	k7c6
květnu	květen	k1gInSc6
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1814	#num#	k4
a	a	k8xC
1840	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
země	zem	k1gFnSc2
diktátor	diktátor	k1gMnSc1
José	Josá	k1gFnSc2
Gaspar	Gaspar	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
de	de	k?
Francia	francium	k1gNnSc2
(	(	kIx(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1
též	též	k9
jako	jako	k9
El	Ela	k1gFnPc2
Supremo	Suprema	k1gFnSc5
–	–	k?
Nejvyšší	vysoký	k2eAgFnPc1d3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
postav	postava	k1gFnPc2
paraguayské	paraguayský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
panoval	panovat	k5eAaImAgInS
v	v	k7c6
zemi	zem	k1gFnSc6
chaos	chaos	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
nezanechal	zanechat	k5eNaPmAgInS
žádného	žádný	k3yNgMnSc4
přímého	přímý	k2eAgMnSc4d1
potomka	potomek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1841	#num#	k4
zvolil	zvolit	k5eAaPmAgInS
kongres	kongres	k1gInSc1
jako	jako	k8xC,k8xS
vrchního	vrchní	k2eAgMnSc2d1
konzula	konzul	k1gMnSc2
Carlose	Carlosa	k1gFnSc3
Antonia	Antonio	k1gMnSc2
Lópeze	Lópeze	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1844	#num#	k4
prezidentem	prezident	k1gMnSc7
a	a	k8xC
vládl	vládnout	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1862	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
druhého	druhý	k4xOgMnSc4
paraguayského	paraguayský	k2eAgMnSc4d1
diktátora	diktátor	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
doby	doba	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
takřka	takřka	k6eAd1
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS
z	z	k7c2
220	#num#	k4
tisíc	tisíc	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
1840	#num#	k4
na	na	k7c4
400	#num#	k4
tisíc	tisíc	k4xCgInSc4
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1862	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
smrti	smrt	k1gFnSc6
C.A.	C.A.	k1gMnSc1
Lópeze	Lópeze	k1gFnSc2
chopil	chopit	k5eAaPmAgMnS
vlády	vláda	k1gFnSc2
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Francisko	Franciska	k1gFnSc5
Solano	Solana	k1gFnSc5
Lopez	Lopez	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c6
Brazílii	Brazílie	k1gFnSc6
a	a	k8xC
rozpoutal	rozpoutat	k5eAaPmAgMnS
tak	tak	k6eAd1
Paraguayskou	paraguayský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
(	(	kIx(
<g/>
válka	válka	k1gFnSc1
proti	proti	k7c3
Trojspolku	trojspolek	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Paraguay	Paraguay	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1864	#num#	k4
až	až	k9
1870	#num#	k4
bojovala	bojovat	k5eAaImAgFnS
proti	proti	k7c3
Brazílii	Brazílie	k1gFnSc3
<g/>
,	,	kIx,
Argentině	Argentina	k1gFnSc6
a	a	k8xC
Uruguayi	Uruguay	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c6
území	území	k1gNnSc6
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
počátečních	počáteční	k2eAgInPc6d1
úspěších	úspěch	k1gInPc6
se	se	k3xPyFc4
ale	ale	k9
situace	situace	k1gFnSc1
otočila	otočit	k5eAaPmAgFnS
a	a	k8xC
země	země	k1gFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
bránit	bránit	k5eAaImF
invazi	invaze	k1gFnSc3
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vojáci	voják	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
neuvěřitelně	uvěřitelně	k6eNd1
statečně	statečně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojenci	spojenec	k1gMnPc7
<g/>
,	,	kIx,
hlavně	hlavně	k9
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
velkou	velký	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguayci	Paraguayec	k1gMnPc1
museli	muset	k5eAaImAgMnP
bojovat	bojovat	k5eAaImF
dále	daleko	k6eAd2
<g/>
,	,	kIx,
protože	protože	k8xS
spojenci	spojenec	k1gMnPc1
uvažovali	uvažovat	k5eAaImAgMnP
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
a	a	k8xC
zániku	zánik	k1gInSc2
jejich	jejich	k3xOp3gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asunción	Asunción	k1gInSc1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
v	v	k7c6
lednu	leden	k1gInSc6
1869	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1870	#num#	k4
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
v	v	k7c6
boji	boj	k1gInSc6
prezident	prezident	k1gMnSc1
Lopez	Lopez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguay	Paraguay	k1gFnSc1
byla	být	k5eAaImAgFnS
válkou	válka	k1gFnSc7
totálně	totálně	k6eAd1
zničená	zničený	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
zemi	zem	k1gFnSc6
nezbyli	zbýt	k5eNaPmAgMnP
takřka	takřka	k6eAd1
žádní	žádný	k3yNgMnPc1
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
musela	muset	k5eAaImAgFnS
Argentině	Argentina	k1gFnSc3
a	a	k8xC
Brazílii	Brazílie	k1gFnSc3
postoupit	postoupit	k5eAaPmF
část	část	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tuyutí	Tuyutí	k1gNnSc4
<g/>
,	,	kIx,
květen	květen	k1gInSc4
1866	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
paraguayské	paraguayský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
před	před	k7c7
válkou	válka	k1gFnSc7
čítala	čítat	k5eAaImAgFnS
450	#num#	k4
až	až	k9
900	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
válku	válka	k1gFnSc4
přežilo	přežít	k5eAaPmAgNnS
pouze	pouze	k6eAd1
220	#num#	k4
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
pouze	pouze	k6eAd1
28	#num#	k4
tisíc	tisíc	k4xCgInPc2
dospělých	dospělý	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
válce	válka	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rekonstrukci	rekonstrukce	k1gFnSc3
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
strany	strana	k1gFnSc2
Colorado	Colorado	k1gNnSc1
a	a	k8xC
Liberální	liberální	k2eAgFnPc1d1
strany	strana	k1gFnPc1
</s>
<s>
Okupace	okupace	k1gFnSc1
Paraguaye	Paraguay	k1gFnSc2
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
sousední	sousední	k2eAgFnPc1d1
velmoci	velmoc	k1gFnPc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
a	a	k8xC
Argentina	Argentina	k1gFnSc1
podporovaly	podporovat	k5eAaImAgFnP
některé	některý	k3yIgFnPc1
strany	strana	k1gFnPc1
soupeřící	soupeřící	k2eAgFnPc1d1
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
o	o	k7c4
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brazílie	Brazílie	k1gFnSc1
podporovala	podporovat	k5eAaImAgFnS
stranu	strana	k1gFnSc4
označovanou	označovaný	k2eAgFnSc4d1
jako	jako	k8xC,k8xS
Colorado	Colorado	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vládla	vládnout	k5eAaImAgFnS
zemi	zem	k1gFnSc4
v	v	k7c6
letech	léto	k1gNnPc6
1880	#num#	k4
až	až	k9
1904	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rostoucímu	rostoucí	k2eAgNnSc3d1
napětí	napětí	k1gNnSc3
mezi	mezi	k7c7
stranou	strana	k1gFnSc7
Colorado	Colorado	k1gNnSc1
a	a	k8xC
Liberální	liberální	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
třecí	třecí	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
byla	být	k5eAaImAgFnS
státní	státní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
rozdělování	rozdělování	k1gNnSc2
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
podporována	podporovat	k5eAaImNgFnS
zejména	zejména	k9
rolníky	rolník	k1gMnPc4
a	a	k8xC
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
Argentinou	Argentina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
liberální	liberální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
však	však	k9
nepřinesla	přinést	k5eNaPmAgFnS
výraznější	výrazný	k2eAgFnSc4d2
změnu	změna	k1gFnSc4
a	a	k8xC
neuspokojila	uspokojit	k5eNaPmAgFnS
požadavky	požadavek	k1gInPc4
rolníků	rolník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1870	#num#	k4
a	a	k8xC
1954	#num#	k4
vládlo	vládnout	k5eAaImAgNnS
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
44	#num#	k4
prezidentů	prezident	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
bylo	být	k5eAaImAgNnS
24	#num#	k4
svrženo	svrhnout	k5eAaPmNgNnS
vojenským	vojenský	k2eAgInSc7d1
převratem	převrat	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
Chaco	Chaco	k6eAd1
a	a	k8xC
Únorová	únorový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
válce	válka	k1gFnSc3
s	s	k7c7
Bolívií	Bolívie	k1gFnSc7
o	o	k7c6
území	území	k1gNnSc6
Gran	Grana	k1gFnPc2
Chaca	Chaca	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
trvala	trvat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguay	Paraguay	k1gFnSc1
válku	válek	k1gInSc2
vyhrála	vyhrát	k5eAaPmAgFnS
a	a	k8xC
území	území	k1gNnPc1
anektovala	anektovat	k5eAaBmAgNnP
i	i	k9
přesto	přesto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bolivijské	bolivijský	k2eAgFnPc1d1
složky	složka	k1gFnPc1
zdály	zdát	k5eAaImAgFnP
být	být	k5eAaImF
na	na	k7c6
počátku	počátek	k1gInSc6
konfliktu	konflikt	k1gInSc2
silnější	silný	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
síla	síla	k1gFnSc1
Bolívie	Bolívie	k1gFnSc1
byla	být	k5eAaImAgFnS
dána	dát	k5eAaPmNgFnS
jak	jak	k6eAd1
větším	veliký	k2eAgNnSc7d2
bohatstvím	bohatství	k1gNnSc7
této	tento	k3xDgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
větší	veliký	k2eAgFnSc7d2
populací	populace	k1gFnSc7
a	a	k8xC
tedy	tedy	k9
i	i	k9
početnější	početní	k2eAgFnSc7d2
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vyzbrojení	vyzbrojení	k1gNnSc6
Bolivijské	bolivijský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
významnou	významný	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
podílela	podílet	k5eAaImAgFnS
Československá	československý	k2eAgFnSc1d1
zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
a	a	k8xC
na	na	k7c6
výcviku	výcvik	k1gInSc6
vojska	vojsko	k1gNnSc2
pak	pak	k6eAd1
speciální	speciální	k2eAgFnSc1d1
československá	československý	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
mise	mise	k1gFnSc1
<g/>
,	,	kIx,
vypravená	vypravený	k2eAgFnSc1d1
do	do	k7c2
Bolívie	Bolívie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
o	o	k7c4
Gran	Gran	k1gInSc4
Chaco	Chaco	k6eAd1
skončila	skončit	k5eAaPmAgFnS
uzavřením	uzavření	k1gNnSc7
příměří	příměří	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Diktatura	diktatura	k1gFnSc1
Alfreda	Alfred	k1gMnSc2
Stroessnera	Stroessner	k1gMnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
země	zem	k1gFnSc2
Alfredo	Alfredo	k1gNnSc1
Stroessner	Stroessner	k1gInSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vláda	vláda	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
strana	strana	k1gFnSc1
Colorado	Colorado	k1gNnSc4
pravidelně	pravidelně	k6eAd1
vyhrávala	vyhrávat	k5eAaImAgFnS
každých	každý	k3xTgNnPc2
5	#num#	k4
let	léto	k1gNnPc2
volby	volba	k1gFnSc2
nad	nad	k7c7
dalšími	další	k2eAgFnPc7d1
dvěma	dva	k4xCgFnPc7
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc1
režim	režim	k1gInSc1
nesmiřitelný	smiřitelný	k2eNgInSc1d1
vůči	vůči	k7c3
komunistům	komunista	k1gMnPc3
(	(	kIx(
<g/>
roku	rok	k1gInSc2
1979	#num#	k4
hostil	hostit	k5eAaImAgMnS
XII	XII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongres	kongres	k1gInSc1
Světové	světový	k2eAgFnSc2d1
Antikomunistické	antikomunistický	k2eAgFnSc2d1
Ligy	liga	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přinesl	přinést	k5eAaPmAgInS
zemi	zem	k1gFnSc3
stabilitu	stabilita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stroessner	Stroessner	k1gMnSc1
se	se	k3xPyFc4
držel	držet	k5eAaImAgMnS
úsporných	úsporný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
navrhována	navrhovat	k5eAaImNgFnS
Mezinárodním	mezinárodní	k2eAgInSc7d1
měnovým	měnový	k2eAgInSc7d1
fondem	fond	k1gInSc7
a	a	k8xC
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
z	z	k7c2
Paraguaye	Paraguay	k1gFnSc2
zemi	zem	k1gFnSc4
bezpečnou	bezpečný	k2eAgFnSc4d1
jak	jak	k8xC,k8xS
pro	pro	k7c4
domácí	domácí	k1gMnPc4
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
investory	investor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inicioval	iniciovat	k5eAaBmAgMnS
také	také	k9
některé	některý	k3yIgInPc1
velké	velký	k2eAgInPc1d1
projekty	projekt	k1gInPc1
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
výstavbu	výstavba	k1gFnSc4
velké	velký	k2eAgFnSc2d1
přehradní	přehradní	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
Itaipú	Itaipú	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dala	dát	k5eAaPmAgFnS
práci	práce	k1gFnSc4
mnoha	mnoho	k4c3
chudým	chudý	k2eAgMnPc3d1
Paraguaycům	Paraguayec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názory	názor	k1gInPc1
Paraguayců	Paraguayec	k1gMnPc2
na	na	k7c4
Stroessnera	Stroessner	k1gMnSc2
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nP
<g/>
;	;	kIx,
mnozí	mnohý	k2eAgMnPc1d1
ho	on	k3xPp3gMnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
zkorumpovaného	zkorumpovaný	k2eAgMnSc4d1
diktátora	diktátor	k1gMnSc4
tvrdě	tvrdě	k6eAd1
porušujícího	porušující	k2eAgMnSc4d1
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
naopak	naopak	k6eAd1
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
politickou	politický	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
a	a	k8xC
ekonomický	ekonomický	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
přetrvával	přetrvávat	k5eAaImAgInS
během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
</s>
<s>
Po	po	k7c6
vojenském	vojenský	k2eAgInSc6d1
převratu	převrat	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
za	za	k7c2
podpory	podpora	k1gFnSc2
USA	USA	kA
a	a	k8xC
Katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
zorganizoval	zorganizovat	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Andrés	Andrésa	k1gFnPc2
Rodríguez	Rodríguez	k1gMnSc1
<g/>
,	,	kIx,
Stroessner	Stroessner	k1gMnSc1
uprchl	uprchnout	k5eAaPmAgMnS
do	do	k7c2
Brazílie	Brazílie	k1gFnSc2
a	a	k8xC
Rodríguez	Rodríguez	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
demokratizovat	demokratizovat	k5eAaBmF
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
omezila	omezit	k5eAaPmAgFnS
možnost	možnost	k1gFnSc4
znovuzvolení	znovuzvolení	k1gNnSc2
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
svobodné	svobodný	k2eAgFnPc1d1
volby	volba	k1gFnPc1
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yQgFnPc6,k3yRgFnPc6
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
zvolen	zvolit	k5eAaPmNgMnS
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Wasmosy	Wasmosa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
roztržce	roztržka	k1gFnSc3
mezi	mezi	k7c7
prezidentem	prezident	k1gMnSc7
Wasmosym	Wasmosym	k1gInSc4
a	a	k8xC
generálem	generál	k1gMnSc7
Oviedem	Ovied	k1gMnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hlavním	hlavní	k2eAgInSc7d1
bodem	bod	k1gInSc7
sváru	svár	k1gInSc2
byl	být	k5eAaImAgInS
prezidentův	prezidentův	k2eAgInSc1d1
plán	plán	k1gInSc1
na	na	k7c4
restrukturalizaci	restrukturalizace	k1gFnSc4
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lino	Lina	k1gFnSc5
Oviedo	Oviedo	k1gNnSc4
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
převrat	převrat	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
však	však	k9
obyvatelé	obyvatel	k1gMnPc1
země	zem	k1gFnSc2
odmítli	odmítnout	k5eAaPmAgMnP
a	a	k8xC
potlačili	potlačit	k5eAaPmAgMnP
(	(	kIx(
<g/>
za	za	k7c2
podpory	podpora	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Organizace	organizace	k1gFnSc1
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
regionu	region	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
zvolen	zvolit	k5eAaPmNgMnS
Nicanor	Nicanor	k1gInSc1
Duarte	Duart	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
pak	pak	k6eAd1
Fernando	Fernanda	k1gFnSc5
Lugo	Lugo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
však	však	k9
parlament	parlament	k1gInSc1
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
odvolal	odvolat	k5eAaPmAgInS
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
nastoupil	nastoupit	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgMnSc1d1
viceprezident	viceprezident	k1gMnSc1
Federico	Federico	k1gMnSc1
Franco	Franco	k1gMnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
srpna	srpen	k1gInSc2
2013	#num#	k4
je	být	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
Horacio	Horacio	k1gMnSc1
Cartes	Cartes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Geografie	geografie	k1gFnSc2
Paraguaye	Paraguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kopce	kopec	k1gInPc1
okolo	okolo	k7c2
Paraguarí	Paraguarí	k1gFnSc2
</s>
<s>
Řeka	řeka	k1gFnSc1
Paraguay	Paraguay	k1gFnSc1
rozděluje	rozdělovat	k5eAaImIp3nS
zemi	zem	k1gFnSc4
na	na	k7c4
dvě	dva	k4xCgFnPc4
velmi	velmi	k6eAd1
rozdílné	rozdílný	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Paraguaye	Paraguay	k1gFnSc2
(	(	kIx(
<g/>
Paraguay	Paraguay	k1gFnSc1
Oriental	Oriental	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
také	také	k9
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
označením	označení	k1gNnSc7
region	region	k1gInSc1
Paraneñ	Paraneñ	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
travnatými	travnatý	k2eAgFnPc7d1
pláněmi	pláň	k1gFnPc7
a	a	k8xC
zvlněným	zvlněný	k2eAgInSc7d1
terénem	terén	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
země	zem	k1gFnSc2
(	(	kIx(
<g/>
Paraguay	Paraguay	k1gFnSc1
Occidental	Occidental	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
rovněž	rovněž	k9
Chaco	Chaco	k6eAd1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
typická	typický	k2eAgFnSc1d1
svým	svůj	k3xOyFgMnSc7
nízkým	nízký	k2eAgInSc7d1
reliéfem	reliéf	k1gInSc7
a	a	k8xC
bažinatou	bažinatý	k2eAgFnSc7d1
rovinou	rovina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úrodná	úrodný	k2eAgFnSc1d1
půda	půda	k1gFnSc1
východně	východně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Paraguay	Paraguay	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
chovat	chovat	k5eAaImF
skot	skot	k1gInSc4
a	a	k8xC
pěstovat	pěstovat	k5eAaImF
rozličné	rozličný	k2eAgFnPc4d1
zemědělské	zemědělský	k2eAgFnPc4d1
plodiny	plodina	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
bavlník	bavlník	k1gInSc4
<g/>
,	,	kIx,
obiloviny	obilovina	k1gFnPc4
<g/>
,	,	kIx,
cukrovou	cukrový	k2eAgFnSc4d1
třtinu	třtina	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roviny	rovina	k1gFnPc1
a	a	k8xC
močály	močál	k1gInPc1
na	na	k7c6
západě	západ	k1gInSc6
země	zem	k1gFnSc2
jsou	být	k5eAaImIp3nP
řídce	řídce	k6eAd1
obydlené	obydlený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguay	Paraguay	k1gFnSc1
získává	získávat	k5eAaImIp3nS
téměř	téměř	k6eAd1
všechnu	všechen	k3xTgFnSc4
elektrickou	elektrický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
z	z	k7c2
vodních	vodní	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Paraneñ	Paraneñ	k6eAd1
</s>
<s>
Region	region	k1gInSc1
Paraneñ	Paraneñ	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
též	též	k9
Východní	východní	k2eAgInSc1d1
region	region	k1gInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
od	od	k7c2
řeky	řeka	k1gFnSc2
Paraguay	Paraguay	k1gFnSc1
na	na	k7c6
východě	východ	k1gInSc6
po	po	k7c4
řeku	řeka	k1gFnSc4
Paraná	Paraná	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tvoří	tvořit	k5eAaImIp3nS
hranici	hranice	k1gFnSc4
s	s	k7c7
Brazílií	Brazílie	k1gFnSc7
a	a	k8xC
Argentinou	Argentina	k1gFnSc7
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
80	#num#	k4
%	%	kIx~
regionu	region	k1gInSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
menší	malý	k2eAgFnSc4d2
než	než	k8xS
300	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmenší	malý	k2eAgFnSc1d3
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
55	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
v	v	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
slévají	slévat	k5eAaImIp3nP
řeky	řeka	k1gFnPc1
Paraguay	Paraguay	k1gFnSc1
a	a	k8xC
Paraná	Paraná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Gran	Gran	k1gMnSc1
Chaco	Chaco	k1gMnSc1
</s>
<s>
Region	region	k1gInSc1
Gran	Gran	k1gMnSc1
Chaco	Chaco	k1gMnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
západě	západ	k1gInSc6
Paraguaye	Paraguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
plochy	plocha	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
polopouštní	polopouštní	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gran	Gran	k1gNnSc1
Chaco	Chaco	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
charakteristické	charakteristický	k2eAgNnSc1d1
svými	svůj	k3xOyFgFnPc7
bažinami	bažina	k1gFnPc7
<g/>
,	,	kIx,
savanami	savana	k1gFnPc7
a	a	k8xC
řídkými	řídký	k2eAgInPc7d1
lesy	les	k1gInPc7
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadmořské	nadmořský	k2eAgFnPc4d1
výšky	výška	k1gFnPc4
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
nepřesahují	přesahovat	k5eNaImIp3nP
300	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k1gMnSc1
je	být	k5eAaImIp3nS
mokřad	mokřad	k1gInSc1
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Estero	Ester	k1gFnSc5
Patiñ	Patiñ	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
1	#num#	k4
500	#num#	k4
km	km	kA
největším	veliký	k2eAgMnSc7d3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Klima	klima	k1gNnSc1
</s>
<s>
Klima	klima	k1gNnSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
dle	dle	k7c2
regionů	region	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
subtropického	subtropický	k2eAgInSc2d1
až	až	k9
po	po	k7c4
klima	klima	k1gNnSc4
mírného	mírný	k2eAgInSc2d1
podnebného	podnebný	k2eAgInSc2d1
pásu	pás	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
letního	letní	k2eAgNnSc2d1
období	období	k1gNnSc2
je	být	k5eAaImIp3nS
dominantním	dominantní	k2eAgInSc7d1
klimatickým	klimatický	k2eAgInSc7d1
jevem	jev	k1gInSc7
teplý	teplý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
Scirocco	scirocco	k1gNnSc1
vanoucí	vanoucí	k2eAgFnSc1d1
ze	z	k7c2
severovýchodu	severovýchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zimy	zima	k1gFnSc2
jsou	být	k5eAaImIp3nP
nejčastější	častý	k2eAgMnPc1d3
tzv.	tzv.	kA
Pamperos	Pamperosa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
způsobují	způsobovat	k5eAaImIp3nP
prudké	prudký	k2eAgNnSc4d1
ochlazení	ochlazení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
země	zem	k1gFnSc2
má	mít	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
jen	jen	k9
dvě	dva	k4xCgNnPc1
roční	roční	k2eAgNnPc1d1
období	období	k1gNnPc1
<g/>
:	:	kIx,
léto	léto	k1gNnSc4
od	od	k7c2
října	říjen	k1gInSc2
do	do	k7c2
března	březen	k1gInSc2
a	a	k8xC
zimu	zima	k1gFnSc4
od	od	k7c2
května	květen	k1gInSc2
do	do	k7c2
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc4
a	a	k8xC
září	září	k1gNnSc4
jsou	být	k5eAaImIp3nP
přechodovými	přechodový	k2eAgInPc7d1
měsíci	měsíc	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaro	jaro	k1gNnSc1
a	a	k8xC
podzim	podzim	k1gInSc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
neexistují	existovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
departementy	departement	k1gInPc1
Paraguaye	Paraguay	k1gFnSc2
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
je	být	k5eAaImIp3nS
zastupitelská	zastupitelský	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
státu	stát	k1gInSc2
stojí	stát	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
volený	volený	k2eAgInSc4d1
na	na	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
je	být	k5eAaImIp3nS
také	také	k9
nejvyšším	vysoký	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
výkonné	výkonný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonodárná	zákonodárný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rukou	ruka	k1gFnPc6
dvoukomorového	dvoukomorový	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
(	(	kIx(
<g/>
80	#num#	k4
členů	člen	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
senátu	senát	k1gInSc2
(	(	kIx(
<g/>
45	#num#	k4
členů	člen	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1
žen	žena	k1gFnPc2
v	v	k7c6
kongresu	kongres	k1gInSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nízké	nízký	k2eAgNnSc1d1
(	(	kIx(
<g/>
10	#num#	k4
%	%	kIx~
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Správní	správní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Departementy	departement	k1gInPc4
Paraguaye	Paraguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
17	#num#	k4
oblastí	oblast	k1gFnPc2
zvaných	zvaný	k2eAgFnPc2d1
departamentos	departamentosa	k1gFnPc2
(	(	kIx(
<g/>
jednotné	jednotný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
departamento	departamento	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
okrsku	okrsek	k1gInSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
(	(	kIx(
<g/>
distrito	distrita	k1gFnSc5
capital	capital	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
departmentů	department	k1gInPc2
</s>
<s>
číslo	číslo	k1gNnSc1
namapě	namapě	k6eAd1
</s>
<s>
Department	department	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Plocha	plocha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Hustota	hustota	k1gFnSc1
(	(	kIx(
<g/>
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
Alto	Alta	k1gMnSc5
ParaguayFuerte	ParaguayFuert	k1gMnSc5
Olimpo	Olimpa	k1gFnSc5
</s>
<s>
82	#num#	k4
349	#num#	k4
</s>
<s>
15	#num#	k4
008	#num#	k4
</s>
<s>
0,2	0,2	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Alto	Alto	k6eAd1
ParanáCiudad	ParanáCiudad	k1gInSc1
del	del	k?
Este	Est	k1gMnSc5
</s>
<s>
14	#num#	k4
895	#num#	k4
</s>
<s>
703	#num#	k4
507	#num#	k4
</s>
<s>
47,2	47,2	k4
</s>
<s>
3	#num#	k4
</s>
<s>
AmambayPedro	AmambayPedro	k6eAd1
Juan	Juan	k1gMnSc1
Caballero	caballero	k1gMnSc1
</s>
<s>
12	#num#	k4
933	#num#	k4
</s>
<s>
123	#num#	k4
861	#num#	k4
</s>
<s>
9,6	9,6	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Distrito	Distrita	k1gFnSc5
CapitalAsunción	CapitalAsunción	k1gInSc1
</s>
<s>
117	#num#	k4
</s>
<s>
518	#num#	k4
792	#num#	k4
</s>
<s>
4434,2	4434,2	k4
</s>
<s>
5	#num#	k4
</s>
<s>
BoquerónFiladelfia	BoquerónFiladelfia	k1gFnSc1
</s>
<s>
91	#num#	k4
669	#num#	k4
</s>
<s>
45	#num#	k4
617	#num#	k4
</s>
<s>
0,5	0,5	k4
</s>
<s>
6	#num#	k4
</s>
<s>
CaaguazúCoronel	CaaguazúCoronel	k1gInSc1
Oviedo	Oviedo	k1gNnSc1
</s>
<s>
11	#num#	k4
474	#num#	k4
</s>
<s>
448	#num#	k4
983	#num#	k4
</s>
<s>
39,1	39,1	k4
</s>
<s>
7	#num#	k4
</s>
<s>
CaazapáCaazapá	CaazapáCaazapat	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
9	#num#	k4
496	#num#	k4
</s>
<s>
139	#num#	k4
241	#num#	k4
</s>
<s>
14,7	14,7	k4
</s>
<s>
8	#num#	k4
</s>
<s>
CanindeyúSalto	CanindeyúSalto	k1gNnSc1
del	del	k?
Guairá	Guairý	k2eAgNnPc4d1
</s>
<s>
14	#num#	k4
667	#num#	k4
</s>
<s>
140	#num#	k4
551	#num#	k4
</s>
<s>
9,6	9,6	k4
</s>
<s>
9	#num#	k4
</s>
<s>
CentralAreguá	CentralAreguat	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
2	#num#	k4
465	#num#	k4
</s>
<s>
1	#num#	k4
929	#num#	k4
834	#num#	k4
</s>
<s>
782,9	782,9	k4
</s>
<s>
10	#num#	k4
</s>
<s>
ConcepciónConcepción	ConcepciónConcepción	k1gMnSc1
</s>
<s>
18	#num#	k4
051	#num#	k4
</s>
<s>
180	#num#	k4
277	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
CordilleraCaacupé	CordilleraCaacupý	k2eAgNnSc1d1
</s>
<s>
4	#num#	k4
948	#num#	k4
</s>
<s>
234	#num#	k4
805	#num#	k4
</s>
<s>
47,5	47,5	k4
</s>
<s>
12	#num#	k4
</s>
<s>
GuairáVillarrica	GuairáVillarrica	k6eAd1
</s>
<s>
3	#num#	k4
846	#num#	k4
</s>
<s>
190	#num#	k4
035	#num#	k4
</s>
<s>
49,4	49,4	k4
</s>
<s>
13	#num#	k4
</s>
<s>
ItapúaEncarnación	ItapúaEncarnación	k1gMnSc1
</s>
<s>
16	#num#	k4
525	#num#	k4
</s>
<s>
463	#num#	k4
410	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
MisionesSan	MisionesSan	k1gMnSc1
Juan	Juan	k1gMnSc1
Bautista	Bautista	k1gMnSc1
</s>
<s>
9	#num#	k4
556	#num#	k4
</s>
<s>
113	#num#	k4
644	#num#	k4
</s>
<s>
11,9	11,9	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Ñ	Ñ	k1gMnSc1
</s>
<s>
12	#num#	k4
147	#num#	k4
</s>
<s>
82	#num#	k4
846	#num#	k4
</s>
<s>
6,8	6,8	k4
</s>
<s>
16	#num#	k4
</s>
<s>
ParaguaríParaguarí	ParaguaríParaguarí	k6eAd1
</s>
<s>
8	#num#	k4
705	#num#	k4
</s>
<s>
221	#num#	k4
932	#num#	k4
</s>
<s>
25,5	25,5	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Presidente	president	k1gMnSc5
HayesVilla	HayesVilla	k1gMnSc1
Hayes	Hayesa	k1gFnPc2
</s>
<s>
72	#num#	k4
907	#num#	k4
</s>
<s>
81	#num#	k4
876	#num#	k4
</s>
<s>
1,1	1,1	k4
</s>
<s>
18	#num#	k4
</s>
<s>
San	San	k?
PedroSan	PedroSan	k1gInSc1
Pedro	Pedro	k1gNnSc1
</s>
<s>
20	#num#	k4
002	#num#	k4
</s>
<s>
318	#num#	k4
787	#num#	k4
</s>
<s>
15,9	15,9	k4
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Praguayský	Praguayský	k2eAgInSc1d1
vývoz	vývoz	k1gInSc1
produktů	produkt	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
</s>
<s>
Deforestace	Deforestace	k1gFnSc1
v	v	k7c6
praguayském	praguayské	k1gNnSc6
Gran	Gran	k1gMnSc1
Chaco	Chaco	k1gMnSc1
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1
Itaipú	Itaipú	k1gFnSc2
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
údajů	údaj	k1gInPc2
OSN	OSN	kA
(	(	kIx(
<g/>
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Index	index	k1gInSc1
<g/>
)	)	kIx)
druhým	druhý	k4xOgInSc7
nejchudším	chudý	k2eAgInSc7d3
státem	stát	k1gInSc7
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
(	(	kIx(
<g/>
po	po	k7c6
Surinamu	Surinam	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Světové	světový	k2eAgFnSc2d1
banky	banka	k1gFnSc2
žije	žít	k5eAaImIp3nS
14	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
pod	pod	k7c7
hranicí	hranice	k1gFnSc7
chudoby	chudoba	k1gFnSc2
<g/>
,	,	kIx,
stanovenou	stanovený	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
2	#num#	k4
americké	americký	k2eAgInPc4d1
dolary	dolar	k1gInPc4
na	na	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
ekonomickou	ekonomický	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
Paraguaye	Paraguay	k1gFnSc2
je	být	k5eAaImIp3nS
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
agrobusiness	agrobusiness	k1gInSc1
a	a	k8xC
chov	chov	k1gInSc1
dobytka	dobytek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnějšími	významný	k2eAgInPc7d3
exportními	exportní	k2eAgInPc7d1
artikly	artikl	k1gInPc7
Paraguaye	Paraguay	k1gFnSc2
jsou	být	k5eAaImIp3nP
elektřina	elektřina	k1gFnSc1
a	a	k8xC
zemědělské	zemědělský	k2eAgInPc1d1
produkty	produkt	k1gInPc1
(	(	kIx(
<g/>
sója	sója	k1gFnSc1
a	a	k8xC
sójový	sójový	k2eAgInSc1d1
olej	olej	k1gInSc1
<g/>
,	,	kIx,
kukuřice	kukuřice	k1gFnSc1
<g/>
,	,	kIx,
obilí	obilí	k1gNnSc1
<g/>
,	,	kIx,
hovězí	hovězí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
největší	veliký	k2eAgFnSc4d3
slabinu	slabina	k1gFnSc4
Paraguaye	Paraguay	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
dlouhodobém	dlouhodobý	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
považován	považován	k2eAgInSc1d1
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
země	země	k1gFnSc1
nemá	mít	k5eNaImIp3nS
přístup	přístup	k1gInSc4
k	k	k7c3
moři	moře	k1gNnSc3
a	a	k8xC
její	její	k3xOp3gFnPc4
možnosti	možnost	k1gFnPc4
vyvážet	vyvážet	k5eAaImF
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
značně	značně	k6eAd1
omezeny	omezit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchod	obchod	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
především	především	k9
se	s	k7c7
sousedními	sousední	k2eAgInPc7d1
státy	stát	k1gInPc7
–	–	k?
Argentinou	Argentina	k1gFnSc7
a	a	k8xC
Brazílií	Brazílie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ekonomice	ekonomika	k1gFnSc6
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
velkých	velká	k1gFnPc2
sousedů	soused	k1gMnPc2
je	být	k5eAaImIp3nS
ekonomika	ekonomika	k1gFnSc1
země	země	k1gFnSc1
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
závislá	závislý	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
především	především	k9
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
(	(	kIx(
<g/>
devalvace	devalvace	k1gFnSc1
brazilské	brazilský	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
(	(	kIx(
<g/>
významné	významný	k2eAgInPc4d1
problémy	problém	k1gInPc4
argentinské	argentinský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
země	země	k1gFnSc1
propadla	propadnout	k5eAaPmAgFnS
do	do	k7c2
ekonomické	ekonomický	k2eAgFnSc2d1
recese	recese	k1gFnSc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
mezi	mezi	k7c7
lety	léto	k1gNnPc7
2003	#num#	k4
a	a	k8xC
2008	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
stabilnímu	stabilní	k2eAgInSc3d1
ekonomickému	ekonomický	k2eAgInSc3d1
růstu	růst	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
způsobem	způsob	k1gInSc7
globální	globální	k2eAgFnSc7d1
poptávkou	poptávka	k1gFnSc7
po	po	k7c6
produktech	produkt	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
Paraguay	Paraguay	k1gFnSc1
exportuje	exportovat	k5eAaBmIp3nS
a	a	k8xC
také	také	k9
stabilním	stabilní	k2eAgNnSc7d1
klimatem	klima	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
přispíval	přispívat	k5eAaImAgInS
k	k	k7c3
velké	velký	k2eAgFnSc3d1
produkci	produkce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
ekonomika	ekonomika	k1gFnSc1
propadla	propadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
na	na	k7c4
což	což	k3yRnSc4,k3yQnSc4
vláda	vláda	k1gFnSc1
reagovala	reagovat	k5eAaBmAgFnS
úspornými	úsporný	k2eAgNnPc7d1
opatřeními	opatření	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1
síla	síla	k1gFnSc1
</s>
<s>
V	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
bylo	být	k5eAaImAgNnS
podle	podle	k7c2
odhadů	odhad	k1gInPc2
CIA	CIA	kA
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
zhruba	zhruba	k6eAd1
3	#num#	k4
miliony	milion	k4xCgInPc1
osob	osoba	k1gFnPc2
na	na	k7c6
trhu	trh	k1gInSc6
práce	práce	k1gFnSc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
(	(	kIx(
<g/>
45	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezaměstnanost	nezaměstnanost	k1gFnSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
15	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
zemi	zem	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
značná	značný	k2eAgFnSc1d1
šedá	šedý	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stávky	stávka	k1gFnPc1
jsou	být	k5eAaImIp3nP
legální	legální	k2eAgFnPc1d1
a	a	k8xC
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
časté	častý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Energetika	energetika	k1gFnSc1
</s>
<s>
Energetika	energetika	k1gFnSc1
Paraguaye	Paraguay	k1gFnSc2
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
výhradně	výhradně	k6eAd1
na	na	k7c6
dvou	dva	k4xCgFnPc6
obrovských	obrovský	k2eAgFnPc6d1
vodních	vodní	k2eAgFnPc6d1
elektrárnách	elektrárna	k1gFnPc6
na	na	k7c6
řece	řeka	k1gFnSc6
Paraná	Paraná	k1gFnSc1
(	(	kIx(
<g/>
Itaipú	Itaipú	k1gFnSc1
<g/>
,	,	kIx,
Yacyretá	Yacyretý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
zajišťují	zajišťovat	k5eAaImIp3nP
100	#num#	k4
%	%	kIx~
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
navíc	navíc	k6eAd1
elektřinu	elektřina	k1gFnSc4
vyváží	vyvážit	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
do	do	k7c2
Argentiny	Argentina	k1gFnSc2
a	a	k8xC
Brazílie	Brazílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Odhady	odhad	k1gInPc1
o	o	k7c6
délce	délka	k1gFnSc6
silniční	silniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
se	se	k3xPyFc4
značně	značně	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
cestu	cesta	k1gFnSc4
a	a	k8xC
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
agentury	agentura	k1gFnSc2
CzechTrade	CzechTrad	k1gInSc5
je	on	k3xPp3gInPc4
v	v	k7c6
zemi	zem	k1gFnSc6
zhruba	zhruba	k6eAd1
25	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
silnic	silnice	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
však	však	k9
pouze	pouze	k6eAd1
1	#num#	k4
000	#num#	k4
km	km	kA
zpevněných	zpevněný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguay	Paraguay	k1gFnSc1
dále	daleko	k6eAd2
disponuje	disponovat	k5eAaBmIp3nS
971	#num#	k4
kilometry	kilometr	k1gInPc4
železnice	železnice	k1gFnSc2
o	o	k7c6
různých	různý	k2eAgInPc6d1
rozchodech	rozchod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgInSc4d3
letiště	letiště	k1gNnSc1
je	být	k5eAaImIp3nS
lokalizováno	lokalizovat	k5eAaBmNgNnS
v	v	k7c6
Asunciónu	Asunción	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Luque	Luqu	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
Největším	veliký	k2eAgInSc7d3
problémem	problém	k1gInSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
je	být	k5eAaImIp3nS
proces	proces	k1gInSc1
odlesňování	odlesňování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
dekádě	dekáda	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
ubývalo	ubývat	k5eAaImAgNnS
každoročně	každoročně	k6eAd1
zhruba	zhruba	k6eAd1
0,5	0,5	k4
%	%	kIx~
lesního	lesní	k2eAgInSc2d1
porostu	porost	k1gInSc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostoucím	rostoucí	k2eAgInSc7d1
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
také	také	k9
znečištění	znečištění	k1gNnSc1
a	a	k8xC
to	ten	k3xDgNnSc4
zejména	zejména	k9
řek	řeka	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgMnPc2,k3yRgMnPc2,k3yIgMnPc2
jsou	být	k5eAaImIp3nP
vypouštěny	vypouštěn	k2eAgFnPc1d1
škodlivé	škodlivý	k2eAgFnPc1d1
látky	látka	k1gFnPc1
(	(	kIx(
<g/>
měď	měď	k1gFnSc1
<g/>
,	,	kIx,
chrom	chrom	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
Gran	Gran	k1gMnSc1
Chaco	Chaco	k1gMnSc1
je	být	k5eAaImIp3nS
problémem	problém	k1gInSc7
zasolování	zasolování	k1gNnSc2
již	již	k6eAd1
tak	tak	k6eAd1
aridních	aridní	k2eAgFnPc2d1
půd	půda	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znesnadňuje	znesnadňovat	k5eAaImIp3nS
práci	práce	k1gFnSc4
farmářů	farmář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc3
Paraguaye	Paraguay	k1gFnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
44	#num#	k4
%	%	kIx~
pokryto	pokrýt	k5eAaPmNgNnS
lesními	lesní	k2eAgInPc7d1
porosty	porost	k1gInPc7
<g/>
,	,	kIx,
zemědělská	zemědělský	k2eAgFnSc1d1
půda	půda	k1gFnSc1
zaujímá	zaujímat	k5eAaImIp3nS
16	#num#	k4
%	%	kIx~
rozlohy	rozloha	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Hustota	hustota	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
</s>
<s>
Populace	populace	k1gFnSc1
Paraguaye	Paraguay	k1gFnSc2
je	být	k5eAaImIp3nS
rozložena	rozložen	k2eAgFnSc1d1
značně	značně	k6eAd1
nerovnoměrně	rovnoměrně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
populace	populace	k1gFnSc2
žije	žít	k5eAaImIp3nS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Grand	grand	k1gMnSc1
Chaco	Chaco	k1gMnSc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
pouze	pouze	k6eAd1
zhruba	zhruba	k6eAd1
pro	pro	k7c4
2	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguay	Paraguay	k1gFnSc1
má	mít	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejvíce	hodně	k6eAd3,k6eAd1
homogenních	homogenní	k2eAgFnPc2d1
populací	populace	k1gFnPc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
95	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
má	mít	k5eAaImIp3nS
smíšený	smíšený	k2eAgInSc4d1
španělsko-guaranský	španělsko-guaranský	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
silná	silný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
japonská	japonský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
pozůstatek	pozůstatek	k1gInSc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
německá	německý	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
výsledků	výsledek	k1gInPc2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
zemi	zem	k1gFnSc6
něco	něco	k3yInSc1
málo	málo	k6eAd1
přes	přes	k7c4
3	#num#	k4
miliony	milion	k4xCgInPc1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
došlo	dojít	k5eAaPmAgNnS
díky	díky	k7c3
vysoké	vysoký	k2eAgFnSc3d1
míře	míra	k1gFnSc3
porodnosti	porodnost	k1gFnSc2
k	k	k7c3
velkému	velký	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
počet	počet	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
konci	konec	k1gInSc6
této	tento	k3xDgFnSc2
dekády	dekáda	k1gFnSc2
odhadován	odhadovat	k5eAaImNgInS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
4	#num#	k4
až	až	k9
4,4	4,4	k4
milionu	milion	k4xCgInSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
však	však	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
významnému	významný	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
úhrnné	úhrnný	k2eAgFnSc2d1
plodnosti	plodnost	k1gFnSc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
tento	tento	k3xDgInSc1
pokles	pokles	k1gInSc1
je	být	k5eAaImIp3nS
menší	malý	k2eAgInSc1d2
než	než	k8xS
u	u	k7c2
sousedních	sousední	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
hodnota	hodnota	k1gFnSc1
tohoto	tento	k3xDgInSc2
ukazatele	ukazatel	k1gInSc2
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
2,5	2,5	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
žije	žít	k5eAaImIp3nS
v	v	k7c6
zemi	zem	k1gFnSc6
zhruba	zhruba	k6eAd1
6,7	6,7	k4
milionu	milion	k4xCgInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
bude	být	k5eAaImBp3nS
dále	daleko	k6eAd2
růst	růst	k5eAaImF
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
ne	ne	k9
již	již	k6eAd1
tak	tak	k6eAd1
velkým	velký	k2eAgNnSc7d1
tempem	tempo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Population	Population	k1gInSc1
Reference	reference	k1gFnSc2
Bureau	Bureaus	k1gInSc2
očekává	očekávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2025	#num#	k4
bude	být	k5eAaImBp3nS
v	v	k7c6
zemi	zem	k1gFnSc6
žít	žít	k5eAaImF
8	#num#	k4
miliónů	milión	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2050	#num#	k4
pak	pak	k6eAd1
9,9	9,9	k4
milionu	milion	k4xCgInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
velkému	velký	k2eAgInSc3d1
populačnímu	populační	k2eAgInSc3d1
přírůstku	přírůstek	k1gInSc3
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
je	být	k5eAaImIp3nS
populace	populace	k1gFnSc1
Paraguaye	Paraguay	k1gFnSc2
velmi	velmi	k6eAd1
mladá	mladý	k2eAgFnSc1d1
<g/>
,	,	kIx,
36	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
má	mít	k5eAaImIp3nS
méně	málo	k6eAd2
než	než	k8xS
15	#num#	k4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
městech	město	k1gNnPc6
žije	žít	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
zhruba	zhruba	k6eAd1
58	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgMnPc7d3
městy	město	k1gNnPc7
jsou	být	k5eAaImIp3nP
Asunción	Asunción	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
jehož	jehož	k3xOyRp3gFnSc6
aglomeraci	aglomerace	k1gFnSc6
žije	žít	k5eAaImIp3nS
až	až	k9
1,9	1,9	k4
milionu	milion	k4xCgInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ciudad	Ciudad	k1gInSc1
del	del	k?
Este	Est	k1gMnSc5
a	a	k8xC
San	San	k1gMnSc5
Lorenzo	Lorenza	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Zdraví	zdraví	k1gNnSc1
</s>
<s>
Zdravotní	zdravotní	k2eAgFnSc1d1
péče	péče	k1gFnSc1
se	se	k3xPyFc4
postupem	postup	k1gInSc7
času	čas	k1gInSc2
zlepšovala	zlepšovat	k5eAaImAgFnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
do	do	k7c2
jejího	její	k3xOp3gNnSc2
zlepšení	zlepšení	k1gNnSc2
vláda	vláda	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
osmdesátých	osmdesátý	k4xOgNnPc2
a	a	k8xC
devadesátých	devadesátý	k4xOgNnPc2
letech	léto	k1gNnPc6
investovala	investovat	k5eAaBmAgFnS
stále	stále	k6eAd1
větší	veliký	k2eAgInPc4d2
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
dekádě	dekáda	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
tento	tento	k3xDgInSc1
pozitivní	pozitivní	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
poněkud	poněkud	k6eAd1
zbrzdil	zbrzdit	k5eAaPmAgInS
a	a	k8xC
Paraguay	Paraguay	k1gFnSc4
má	mít	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
menší	malý	k2eAgInPc4d2
výdaje	výdaj	k1gInPc4
na	na	k7c4
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
než	než	k8xS
většina	většina	k1gFnSc1
ostatních	ostatní	k2eAgFnPc2d1
jihoamerických	jihoamerický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průzkum	průzkum	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
27	#num#	k4
procent	procento	k1gNnPc2
populace	populace	k1gFnSc2
stále	stále	k6eAd1
nemá	mít	k5eNaImIp3nS
přístup	přístup	k1gInSc4
ke	k	k7c3
zdravotní	zdravotní	k2eAgFnSc3d1
péči	péče	k1gFnSc3
(	(	kIx(
<g/>
státní	státní	k2eAgInSc4d1
<g/>
,	,	kIx,
či	či	k8xC
soukromé	soukromý	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prevalence	prevalence	k1gFnSc1
viru	vir	k1gInSc2
HIV	HIV	kA
je	být	k5eAaImIp3nS
v	v	k7c6
zemi	zem	k1gFnSc6
relativně	relativně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
2007-2008	2007-2008	k4
se	se	k3xPyFc4
počet	počet	k1gInSc1
nakažených	nakažený	k2eAgMnPc2d1
tímto	tento	k3xDgInSc7
virem	vir	k1gInSc7
odhadoval	odhadovat	k5eAaImAgMnS
na	na	k7c4
0,6	0,6	k4
%	%	kIx~
populace	populace	k1gFnSc2
ve	v	k7c6
věku	věk	k1gInSc6
15-49	15-49	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
paraguayské	paraguayský	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
je	být	k5eAaImIp3nS
zaručena	zaručen	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
vyznání	vyznání	k1gNnSc2
a	a	k8xC
neexistuje	existovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
státní	státní	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantním	dominantní	k2eAgNnSc6d1
je	být	k5eAaImIp3nS
římskokatolické	římskokatolický	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
90	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
5	#num#	k4
%	%	kIx~
populace	populace	k1gFnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
evangelicismu	evangelicismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
žije	žít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
40	#num#	k4
tisíc	tisíc	k4xCgInPc2
Mennonitů	Mennonit	k1gInPc2
německo-nizozemského	německo-nizozemský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
najít	najít	k5eAaPmF
také	také	k9
vyznavače	vyznavač	k1gMnPc4
Buddhismu	buddhismus	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
přistěhovalce	přistěhovalec	k1gMnPc4
z	z	k7c2
Korey	Korea	k1gFnSc2
a	a	k8xC
vyznavače	vyznavač	k1gMnPc4
Islámu	islám	k1gInSc2
<g/>
,	,	kIx,
především	především	k9
imigranty	imigrant	k1gMnPc4
ze	z	k7c2
Sýrie	Sýrie	k1gFnSc2
a	a	k8xC
Libanonu	Libanon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významný	významný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
počet	počet	k1gInSc1
Židů	Žid	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
do	do	k7c2
země	zem	k1gFnSc2
přicházeli	přicházet	k5eAaImAgMnP
zejména	zejména	k9
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
a	a	k8xC
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
z	z	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Asunción	Asunción	k1gInSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Paraguaye	Paraguay	k1gFnSc2
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Ovecha	Ovecha	k1gFnSc1
Ragué	Raguá	k1gFnSc2
</s>
<s>
Paraguayská	paraguayský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
je	být	k5eAaImIp3nS
mixem	mix	k1gInSc7
mezi	mezi	k7c7
kulturou	kultura	k1gFnSc7
evropskou	evropský	k2eAgFnSc7d1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
přinesli	přinést	k5eAaPmAgMnP
španělští	španělský	k2eAgMnPc1d1
dobyvatelé	dobyvatel	k1gMnPc1
a	a	k8xC
kulturou	kultura	k1gFnSc7
Guaraní	Guaraní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguayská	paraguayský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
je	být	k5eAaImIp3nS
proto	proto	k8xC
dnes	dnes	k6eAd1
stále	stále	k6eAd1
bilingvální	bilingválnit	k5eAaPmIp3nS
a	a	k8xC
asi	asi	k9
80	#num#	k4
%	%	kIx~
hovoří	hovořit	k5eAaImIp3nS
jak	jak	k6eAd1
španělsky	španělsky	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
guaraní	guaranit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšířený	rozšířený	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
jazyk	jazyk	k1gInSc1
Jopará	Joparý	k2eAgFnSc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
spojuje	spojovat	k5eAaImIp3nS
prvky	prvek	k1gInPc4
španělštiny	španělština	k1gFnSc2
a	a	k8xC
guaraní	guaraní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Padesátá	padesátý	k4xOgNnPc4
a	a	k8xC
šedesátá	šedesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
znamenala	znamenat	k5eAaImAgFnS
velký	velký	k2eAgInSc4d1
rozkvět	rozkvět	k1gInSc4
paraguayské	paraguayský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
ukázala	ukázat	k5eAaPmAgFnS
nové	nový	k2eAgMnPc4d1
autory	autor	k1gMnPc4
jako	jako	k8xC,k8xS
José	Josý	k2eAgNnSc4d1
Ricardo	Ricardo	k1gNnSc4
Mazó	Mazó	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Roque	Roque	k1gFnSc1
Vallejos	Vallejosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgMnPc4d3
paraguayské	paraguayský	k2eAgMnPc4d1
spisovatele	spisovatel	k1gMnPc4
patří	patřit	k5eAaImIp3nS
Augusto	Augusta	k1gMnSc5
Roa	Roa	k1gMnPc4
Bastos	Bastos	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
Cervantesovu	Cervantesův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
udělována	udělován	k2eAgFnSc1d1
španělsky	španělsky	k6eAd1
píšícím	píšící	k2eAgMnPc3d1
autorům	autor	k1gMnPc3
za	za	k7c4
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Zásadním	zásadní	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
hudební	hudební	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
je	být	k5eAaImIp3nS
Agustín	Agustín	k1gMnSc1
Barrios	Barrios	k1gMnSc1
kytarista	kytarista	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc4
moderní	moderní	k2eAgInPc1d1
hudební	hudební	k2eAgInPc1d1
proudy	proud	k1gInPc1
jako	jako	k8xC,k8xS
například	například	k6eAd1
rock	rock	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
poměrně	poměrně	k6eAd1
novým	nový	k2eAgInSc7d1
fenoménem	fenomén	k1gInSc7
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
kvůli	kvůli	k7c3
izolaci	izolace	k1gFnSc3
způsobené	způsobený	k2eAgFnSc2d1
diktátorem	diktátor	k1gMnSc7
Stroessnerem	Stroessner	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
desetiletí	desetiletí	k1gNnSc6
však	však	k9
vzniklo	vzniknout	k5eAaPmAgNnS
i	i	k9
několik	několik	k4yIc1
rockových	rockový	k2eAgInPc2d1
festivalů	festival	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejznámější	známý	k2eAgMnPc1d3
jsou	být	k5eAaImIp3nP
Pilsen	Pilsen	k2eAgInSc4d1
Rock	rock	k1gInSc4
a	a	k8xC
Quilmes	Quilmes	k1gInSc4
Rock	rock	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
těchto	tento	k3xDgInPc6
festivalech	festival	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
pořádány	pořádán	k2eAgInPc1d1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Asunciónu	Asunción	k1gInSc2
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
schází	scházet	k5eAaImIp3nS
okolo	okolo	k7c2
60	#num#	k4
tisíc	tisíc	k4xCgInPc2
příznivců	příznivec	k1gMnPc2
tohoto	tento	k3xDgInSc2
hudebního	hudební	k2eAgInSc2d1
žánru	žánr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kinematografie	kinematografie	k1gFnSc1
</s>
<s>
Produkce	produkce	k1gFnSc1
filmů	film	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
výrazně	výrazně	k6eAd1
menší	malý	k2eAgFnSc1d2
než	než	k8xS
v	v	k7c6
sousední	sousední	k2eAgFnSc6d1
Argentině	Argentina	k1gFnSc6
a	a	k8xC
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
byla	být	k5eAaImAgFnS
paraguayská	paraguayský	k2eAgFnSc1d1
kinematografie	kinematografie	k1gFnSc1
sužována	sužovat	k5eAaImNgFnS
především	především	k9
nedostatkem	nedostatek	k1gInSc7
finančních	finanční	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
1990	#num#	k4
byl	být	k5eAaImAgInS
zásadním	zásadní	k2eAgInSc7d1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
paraguayského	paraguayský	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
pro	pro	k7c4
Paraguay	Paraguay	k1gFnSc4
jako	jako	k9
takovou	takový	k3xDgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Paraguayský	paraguayský	k2eAgInSc1d1
fond	fond	k1gInSc1
pro	pro	k7c4
kinematografii	kinematografie	k1gFnSc4
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
festival	festival	k1gInSc1
v	v	k7c6
Asunciónu	Asunción	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
dekádě	dekáda	k1gFnSc6
uspělo	uspět	k5eAaPmAgNnS
několik	několik	k4yIc1
paraguayských	paraguayský	k2eAgInPc2d1
filmů	film	k1gInPc2
na	na	k7c6
prestižních	prestižní	k2eAgInPc6d1
filmových	filmový	k2eAgInPc6d1
festivalech	festival	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
snímek	snímek	k1gInSc4
O	o	k7c4
Amigo	Amigo	k1gNnSc4
Dunor	Dunora	k1gFnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
Hamaca	Hamac	k2eAgFnSc1d1
Paraguaya	Paraguaya	k1gFnSc1
(	(	kIx(
<g/>
Paraguayská	paraguayský	k2eAgFnSc1d1
houpací	houpací	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Média	médium	k1gNnPc1
</s>
<s>
Noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
rozhlas	rozhlas	k1gInSc1
i	i	k8xC
televize	televize	k1gFnSc1
reprezentují	reprezentovat	k5eAaImIp3nP
celé	celý	k2eAgFnPc4d1
politické	politický	k2eAgFnPc4d1
a	a	k8xC
názorové	názorový	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
žurnalisté	žurnalist	k1gMnPc1
však	však	k9
podle	podle	k7c2
BBC	BBC	kA
čelí	čelit	k5eAaImIp3nS
zastrašování	zastrašování	k1gNnSc4
pokud	pokud	k8xS
se	se	k3xPyFc4
hlouběji	hluboko	k6eAd2
zajímají	zajímat	k5eAaImIp3nP
o	o	k7c4
některé	některý	k3yIgInPc4
případy	případ	k1gInPc4
korupce	korupce	k1gFnSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrozšířenější	rozšířený	k2eAgFnPc4d3
noviny	novina	k1gFnPc4
se	se	k3xPyFc4
jmenují	jmenovat	k5eAaImIp3nP,k5eAaBmIp3nP
Diario	Diario	k6eAd1
ABC	ABC	kA
Color	Colora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3
sportem	sport	k1gInSc7
je	být	k5eAaImIp3nS
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
fotbal	fotbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguayský	paraguayský	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
tým	tým	k1gInSc4
dosáhl	dosáhnout	k5eAaPmAgMnS
největšího	veliký	k2eAgInSc2d3
úspěchu	úspěch	k1gInSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
podlehl	podlehnout	k5eAaPmAgMnS
až	až	k9
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
budoucím	budoucí	k2eAgMnPc3d1
šampiónům	šampión	k1gMnPc3
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
tým	tým	k1gInSc1
hraje	hrát	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
domácí	domácí	k1gMnPc4
zápasy	zápas	k1gInPc4
na	na	k7c4
Estadio	Estadio	k1gNnSc4
Defensores	Defensores	k1gMnSc1
del	del	k?
Chaco	Chaco	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
pojmenován	pojmenovat	k5eAaPmNgMnS
na	na	k7c4
počest	počest	k1gFnSc4
obránců	obránce	k1gMnPc2
regionu	region	k1gInSc2
Gran	Gran	k1gMnSc1
Chaco	Chaco	k1gMnSc1
ve	v	k7c6
válce	válka	k1gFnSc6
s	s	k7c7
Bolívií	Bolívie	k1gFnSc7
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velkému	velký	k2eAgInSc3d1
zájmu	zájem	k1gInSc3
se	se	k3xPyFc4
těší	těšit	k5eAaImIp3nS
domácí	domácí	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
ní	on	k3xPp3gFnSc6
hraje	hrát	k5eAaImIp3nS
12	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
je	být	k5eAaImIp3nS
Club	club	k1gInSc4
Olimpia	Olimpium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
diktatury	diktatura	k1gFnSc2
Alfreda	Alfred	k1gMnSc2
Stroessnera	Stroessner	k1gMnSc2
poněkud	poněkud	k6eAd1
podcenila	podcenit	k5eAaPmAgFnS
význam	význam	k1gInSc4
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
demokratizace	demokratizace	k1gFnSc2
přinesl	přinést	k5eAaPmAgInS
zlepšení	zlepšení	k1gNnSc4
vzdělávacího	vzdělávací	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
činily	činit	k5eAaImAgInP
výdaje	výdaj	k1gInPc1
na	na	k7c4
vzdělávání	vzdělávání	k1gNnSc4
4,7	4,7	k4
%	%	kIx~
HDP	HDP	kA
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
pouze	pouze	k6eAd1
1,7	1,7	k4
%	%	kIx~
HDP	HDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
do	do	k7c2
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
dochází	docházet	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
94	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rovněž	rovněž	k9
systém	systém	k1gInSc1
terciérního	terciérní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
není	být	k5eNaImIp3nS
dostatečný	dostatečný	k2eAgInSc1d1
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
bylo	být	k5eAaImAgNnS
zajišťováno	zajišťovat	k5eAaImNgNnS
pouze	pouze	k6eAd1
jednou	jednou	k6eAd1
státní	státní	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
a	a	k8xC
jednou	jeden	k4xCgFnSc7
katolickou	katolický	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
sice	sice	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
otevření	otevření	k1gNnSc3
dalších	další	k2eAgFnPc2d1
deseti	deset	k4xCc2
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
vysokoškolské	vysokoškolský	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
stále	stále	k6eAd1
nemá	mít	k5eNaImIp3nS
příliš	příliš	k6eAd1
velkou	velký	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
a	a	k8xC
nejprestižnější	prestižní	k2eAgFnSc1d3
univerzitou	univerzita	k1gFnSc7
je	být	k5eAaImIp3nS
Universidad	Universidad	k1gInSc1
Nacional	Nacional	k1gFnSc2
de	de	k?
Asunción	Asunción	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
dvanácti	dvanáct	k4xCc6
fakultách	fakulta	k1gFnPc6
studuje	studovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
36	#num#	k4
tisíc	tisíc	k4xCgInPc2
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byla	být	k5eAaImAgFnS
míra	míra	k1gFnSc1
gramotnosti	gramotnost	k1gFnSc2
94	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
muži	muž	k1gMnPc7
a	a	k8xC
ženami	žena	k1gFnPc7
byly	být	k5eAaImAgFnP
minimální	minimální	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negramotnost	negramotnost	k1gFnSc1
je	být	k5eAaImIp3nS
problémem	problém	k1gInSc7
především	především	k9
v	v	k7c6
chudých	chudý	k2eAgFnPc6d1
venkovských	venkovský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Při	při	k7c6
přípravě	příprava	k1gFnSc6
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
byly	být	k5eAaImAgFnP
čerpány	čerpán	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
cizojazyčných	cizojazyčný	k2eAgFnPc2d1
Wikipedií	Wikipedie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Paraguaye	Paraguay	k1gFnSc2
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
konzervativec	konzervativec	k1gMnSc1
Mario	Mario	k1gMnSc1
Abdo	Abdo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-23	2018-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GDP	GDP	kA
per	pero	k1gNnPc2
capita	capitum	k1gNnSc2
<g/>
,	,	kIx,
PPP	PPP	kA
(	(	kIx(
<g/>
current	current	k1gMnSc1
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
$	$	kIx~
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KURAS	KURAS	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bláhové	Bláhové	k2eAgNnSc2d1
snění	snění	k1gNnSc2
o	o	k7c6
světě	svět	k1gInSc6
bez	bez	k7c2
padouchů	padouch	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eminent	Eminent	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
213	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7281	#num#	k4
<g/>
-	-	kIx~
<g/>
473	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Prales	prales	k1gInSc1
padouchů	padouch	k1gMnPc2
prostý	prostý	k2eAgMnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RUBINSEIN	RUBINSEIN	kA
<g/>
,	,	kIx,
W.	W.	kA
D.	D.	kA
Genocide	Genocid	k1gInSc5
<g/>
:	:	kIx,
a	a	k8xC
history	histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Pearson	Pearson	k1gInSc1
Education	Education	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
582	#num#	k4
<g/>
-	-	kIx~
<g/>
50601	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
94	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Paraguay	Paraguay	k1gFnSc1
<g/>
:	:	kIx,
Život	život	k1gInSc1
v	v	k7c6
Srdci	srdce	k1gNnSc6
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
www.stoplusjednicka.cz	www.stoplusjednicka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://zpravy.idnes.cz/senat-odvolal-paraguayskeho-prezidenta-f05-/zahranicni.aspx?c=A120623_022338_zahranicni_klm	http://zpravy.idnes.cz/senat-odvolal-paraguayskeho-prezidenta-f05-/zahranicni.aspx?c=A120623_022338_zahranicni_klm	k1gInSc1
<g/>
↑	↑	k?
http://hn.ihned.cz/c1-20523670	http://hn.ihned.cz/c1-20523670	k4
<g/>
↑	↑	k?
Anuario	Anuario	k1gMnSc1
Estadístico	Estadístico	k1gMnSc1
de	de	k?
América	América	k1gFnSc1
Latina	latina	k1gFnSc1
y	y	k?
el	ela	k1gFnPc2
Caribe	Carib	k1gInSc5
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
2.2.2.29	2.2.2.29	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
Latinskou	latinský	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
a	a	k8xC
Karibik	Karibik	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.businessinfo.cz	www.businessinfo.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.cia.gov/library/publications/the-world-factbook/geos/pa.html	https://www.cia.gov/library/publications/the-world-factbook/geos/pa.html	k1gMnSc1
<g/>
↑	↑	k?
Anuario	Anuario	k1gMnSc1
Estadístico	Estadístico	k1gMnSc1
de	de	k?
América	América	k1gFnSc1
Latina	latina	k1gFnSc1
y	y	k?
el	ela	k1gFnPc2
Caribe	Carib	k1gInSc5
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
3.4	3.4	k4
TIERRAS	TIERRAS	kA
Y	Y	kA
SUELOS	SUELOS	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
Latinskou	latinský	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
a	a	k8xC
Karibik	Karibik	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://countrystudies.us/paraguay/26.htm	http://countrystudies.us/paraguay/26.htm	k1gInSc1
<g/>
↑	↑	k?
http://www.prb.org/Articles/2010/paraguaytfrdecline.aspx?p=11	http://www.prb.org/Articles/2010/paraguaytfrdecline.aspx?p=11	k4
2	#num#	k4
http://www.prb.org/Datafinder/Geography/Summary.aspx?region=110&	http://www.prb.org/Datafinder/Geography/Summary.aspx?region=110&	k?
<g/>
↑	↑	k?
http://news.bbc.co.uk/2/hi/americas/country_profiles/1222081.stm#media	http://news.bbc.co.uk/2/hi/americas/country_profiles/1222081.stm#medium	k1gNnSc2
<g/>
↑	↑	k?
http://lcweb2.loc.gov/frd/cs/profiles/Paraguay.pdf	http://lcweb2.loc.gov/frd/cs/profiles/Paraguay.pdf	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hispanoamerika	Hispanoamerika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Paraguay	Paraguay	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Paraguay	Paraguay	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Paraguay	Paraguay	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Galerie	galerie	k1gFnSc1
Paraguay	Paraguay	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
LatinskaAmerikaDnes	LatinskaAmerikaDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Paraguay	Paraguay	k1gFnSc1
(	(	kIx(
<g/>
informace	informace	k1gFnSc1
<g/>
,	,	kIx,
reportáž	reportáž	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
webu	web	k1gInSc6
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Dnes	dnes	k6eAd1
</s>
<s>
www.paraguay.cz	www.paraguay.cz	k1gInSc1
–	–	k?
Honorární	honorární	k2eAgInSc1d1
konzulát	konzulát	k1gInSc1
Republiky	republika	k1gFnSc2
Paraguay	Paraguay	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
-	-	kIx~
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc1
Report	report	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BTI	BTI	kA
2010	#num#	k4
—	—	k?
Paraguay	Paraguay	k1gFnSc4
Country	country	k2eAgInSc4d1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gütersloh	Gütersloh	k1gMnSc1
<g/>
:	:	kIx,
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bureau	Bureau	k6eAd1
of	of	k?
Western	western	k1gInSc1
Hemisphere	Hemispher	k1gInSc5
Affairs	Affairs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Background	Background	k1gInSc1
Note	Not	k1gInSc2
<g/>
:	:	kIx,
Paraguay	Paraguay	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
2011-07-06	2011-07-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Paraguay	Paraguay	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-07-05	2011-07-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Country	country	k2eAgInSc1d1
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Paraguay	Paraguay	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-10-24	2005-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
ČR	ČR	kA
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Airesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrnná	souhrnný	k2eAgFnSc1d1
teritoriální	teritoriální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Businessinfo	Businessinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-05-01	2011-05-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BUTLAND	BUTLAND	kA
<g/>
,	,	kIx,
Gilbert	Gilbert	k1gMnSc1
James	James	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paraguay	Paraguay	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Argentina	Argentina	k1gFnSc1
•	•	k?
Bolívie	Bolívie	k1gFnSc2
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
Paraguay	Paraguay	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Surinam	Surinam	k1gInSc1
•	•	k?
Uruguay	Uruguay	k1gFnSc1
•	•	k?
Venezuela	Venezuela	k1gFnSc1
Součásti	součást	k1gFnSc2
suverénních	suverénní	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Francouzská	francouzský	k2eAgFnSc1d1
Guyana	Guyana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nizozemské	nizozemský	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
Bonaire	Bonair	k1gMnSc5
•	•	k?
Saba	Sab	k1gInSc2
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Eustach	Eustach	k1gMnSc1
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Falklandy	Falkland	k1gInPc1
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Georgie	Georgie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgInPc1d1
Sandwichovy	Sandwichův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Nizozemské	nizozemský	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
Aruba	Aruba	k1gFnSc1
•	•	k?
Curaçao	curaçao	k1gNnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
130368	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4044601-3	4044601-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2308	#num#	k4
8111	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79066597	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
139099883	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79066597	#num#	k4
</s>
