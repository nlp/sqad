<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
plně	plně	k6eAd1	plně
elektricky	elektricky	k6eAd1	elektricky
osvětleným	osvětlený	k2eAgNnSc7d1	osvětlené
divadlem	divadlo	k1gNnSc7	divadlo
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
divadla	divadlo	k1gNnSc2	divadlo
dokonce	dokonce	k9	dokonce
postavena	postaven	k2eAgFnSc1d1	postavena
malá	malý	k2eAgFnSc1d1	malá
parní	parní	k2eAgFnSc1d1	parní
elektrárna	elektrárna	k1gFnSc1	elektrárna
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
přijel	přijet	k5eAaPmAgMnS	přijet
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
svůj	svůj	k3xOyFgInSc4	svůj
výtvor	výtvor	k1gInSc4	výtvor
<g/>
.	.	kIx.	.
</s>
