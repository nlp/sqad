<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
Ferdinandu	Ferdinand	k1gMnSc6
I.	I.	kA
Dobrotivém	dobrotivý	k2eAgInSc6d1
(	(	kIx(
<g/>
1793	#num#	k4
<g/>
–	–	k?
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
císaři	císař	k1gMnSc3
rakouském	rakouský	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
<g/>
.	.	kIx.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
V.	V.	kA
<g/>
)	)	kIx)
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
a	a	k8xC
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivýna	Dobrotivýna	k1gFnSc1
obraze	obraz	k1gInSc5
F.	F.	kA
Hayeze	Hayeze	k1gFnPc1
z	z	k7c2
roku	rok	k1gInSc2
1840	#num#	k4
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
(	(	kIx(
<g/>
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
)	)	kIx)
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
(	(	kIx(
<g/>
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
chorvatský	chorvatský	k2eAgMnSc1d1
a	a	k8xC
slavonský	slavonský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1836	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
(	(	kIx(
<g/>
českým	český	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1793	#num#	k4
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1875	#num#	k4
(	(	kIx(
<g/>
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1
hrobka	hrobka	k1gFnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Savojská	savojský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
uherský	uherský	k2eAgMnSc1d1
a	a	k8xC
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1793	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1875	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
habsbursko-lotrinské	habsbursko-lotrinský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
,	,	kIx,
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
poslední	poslední	k2eAgMnSc1d1
korunovaný	korunovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
atd.	atd.	kA
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
ze	z	k7c2
švédské	švédský	k2eAgFnSc2d1
historické	historický	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
Illustrerad	Illustrerada	k1gFnPc2
verldshistoria	verldshistorium	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
František	František	k1gMnSc1
Marcelin	Marcelin	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
syn	syn	k1gMnSc1
rakouského	rakouský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
a	a	k8xC
uherského	uherský	k2eAgMnSc2d1
a	a	k8xC
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
Františka	František	k1gMnSc2
I.	I.	kA
a	a	k8xC
Marie	Marie	k1gFnSc1
Terezy	Tereza	k1gFnSc2
Neapolsko-Sicilské	neapolsko-sicilský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dětství	dětství	k1gNnSc2
trpěl	trpět	k5eAaImAgMnS
epileptickými	epileptický	k2eAgInPc7d1
záchvaty	záchvat	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
způsobeny	způsobit	k5eAaPmNgInP
porodním	porodní	k2eAgNnSc7d1
traumatem	trauma	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
rachitidou	rachitida	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
devíti	devět	k4xCc2
let	léto	k1gNnPc2
nezískal	získat	k5eNaPmAgInS
téměř	téměř	k6eAd1
žádné	žádný	k3yNgNnSc4
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
tehdejší	tehdejší	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
fyzická	fyzický	k2eAgFnSc1d1
a	a	k8xC
duševní	duševní	k2eAgFnSc1d1
námaha	námaha	k1gFnSc1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
zhoršení	zhoršení	k1gNnSc3
stavu	stav	k1gInSc2
epileptických	epileptický	k2eAgMnPc2d1
pacientů	pacient	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
pozdější	pozdní	k2eAgMnSc1d2
Ferdinandovy	Ferdinandův	k2eAgInPc4d1
pokroky	pokrok	k1gInPc4
postupovaly	postupovat	k5eAaImAgFnP
pomalu	pomalu	k6eAd1
<g/>
,	,	kIx,
panovaly	panovat	k5eAaImAgFnP
určité	určitý	k2eAgFnPc4d1
obavy	obava	k1gFnPc4
o	o	k7c6
následnictví	následnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Osobnost	osobnost	k1gFnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
měl	mít	k5eAaImAgMnS
hudební	hudební	k2eAgNnSc4d1
nadání	nadání	k1gNnSc4
<g/>
,	,	kIx,
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
klavír	klavír	k1gInSc4
a	a	k8xC
trubku	trubka	k1gFnSc4
<g/>
,	,	kIx,
nadšeně	nadšeně	k6eAd1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
botanikou	botanika	k1gFnSc7
a	a	k8xC
zajímal	zajímat	k5eAaImAgMnS
se	se	k3xPyFc4
o	o	k7c4
vývoj	vývoj	k1gInSc4
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodně	rozhodně	k6eAd1
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
označit	označit	k5eAaPmF
za	za	k7c4
slabomyslného	slabomyslný	k2eAgMnSc4d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
hovořil	hovořit	k5eAaImAgMnS
pěti	pět	k4xCc2
jazyky	jazyk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
byl	být	k5eAaImAgMnS
poněkud	poněkud	k6eAd1
flegmatický	flegmatický	k2eAgMnSc1d1
a	a	k8xC
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
zdravotních	zdravotní	k2eAgInPc2d1
problémů	problém	k1gInPc2
vyplývala	vyplývat	k5eAaImAgFnS
některá	některý	k3yIgNnPc4
další	další	k2eAgNnPc4d1
omezení	omezení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Ferdinandovi	Ferdinandův	k2eAgMnPc1d1
rodiče	rodič	k1gMnPc1
byli	být	k5eAaImAgMnP
bratranci	bratranec	k1gMnPc1
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Ferdinandovi	Ferdinandův	k2eAgMnPc1d1
prarodiče	prarodič	k1gMnPc1
byli	být	k5eAaImAgMnP
vlastními	vlastní	k2eAgMnPc7d1
sourozenci	sourozenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferdinandův	Ferdinandův	k2eAgMnSc1d1
děd	děd	k1gMnSc1
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgInS
totiž	totiž	k9
bratrem	bratr	k1gMnSc7
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1
babičky	babička	k1gFnSc2
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Marie	Maria	k1gFnSc2
Karolíny	Karolína	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
děd	děd	k1gMnSc1
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
byl	být	k5eAaImAgMnS
bratrem	bratr	k1gMnSc7
jeho	jeho	k3xOp3gFnSc2
babičky	babička	k1gFnSc2
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Marie	Maria	k1gFnSc2
Ludoviky	Ludovika	k1gFnSc2
Španělské	španělský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Ferdinand	Ferdinand	k1gMnSc1
měl	mít	k5eAaImAgMnS
pouze	pouze	k6eAd1
čtyři	čtyři	k4xCgMnPc4
praprarodiče	praprarodič	k1gMnPc4
a	a	k8xC
nikoliv	nikoliv	k9
osm	osm	k4xCc1
jak	jak	k6eAd1
je	být	k5eAaImIp3nS
běžné	běžný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
blízké	blízký	k2eAgNnSc1d1
příbuzenství	příbuzenství	k1gNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
z	z	k7c2
Františkových	Františkův	k2eAgFnPc2d1
a	a	k8xC
Mariiných	Mariin	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
potýkat	potýkat	k5eAaImF
s	s	k7c7
genetickou	genetický	k2eAgFnSc7d1
degenerací	degenerace	k1gFnSc7
a	a	k8xC
jen	jen	k9
část	část	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
samostatného	samostatný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Císař	Císař	k1gMnSc1
a	a	k8xC
král	král	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
na	na	k7c6
majestátním	majestátní	k2eAgInSc6d1
portrétu	portrét	k1gInSc6
L.	L.	kA
Kupelwiesera	Kupelwieser	k1gMnSc4
z	z	k7c2
roku	rok	k1gInSc2
1847	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
,	,	kIx,
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
pravé	pravý	k2eAgFnSc6d1
ruce	ruka	k1gFnSc6
zobrazeny	zobrazit	k5eAaPmNgFnP
svatoštěpánská	svatoštěpánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
rakouská	rakouský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
a	a	k8xC
svatováclavská	svatováclavský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Korunovace	korunovace	k1gFnSc1
císaře	císař	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
I.	I.	kA
českým	český	k2eAgMnSc7d1
králem	král	k1gMnSc7
v	v	k7c6
chrámu	chrám	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Víta	Vít	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
1836	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
byl	být	k5eAaImAgMnS
Ferdinand	Ferdinand	k1gMnSc1
v	v	k7c6
Prešpurku	Prešpurku	k?
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc6d1
Bratislavě	Bratislava	k1gFnSc6
<g/>
)	)	kIx)
korunován	korunován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
a	a	k8xC
nadále	nadále	k6eAd1
mu	on	k3xPp3gMnSc3
náležel	náležet	k5eAaImAgMnS
titul	titul	k1gInSc4
„	„	k?
<g/>
mladší	mladý	k2eAgMnSc1d2
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
a	a	k8xC
arcivévoda	arcivévoda	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
korunovačním	korunovační	k2eAgInSc6d1
obřadu	obřad	k1gInSc6
pronesl	pronést	k5eAaPmAgMnS
řeč	řeč	k1gFnSc4
v	v	k7c6
maďarštině	maďarština	k1gFnSc6
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
po	po	k7c6
Marii	Maria	k1gFnSc6
Terezii	Terezie	k1gFnSc6
druhým	druhý	k4xOgNnSc7
a	a	k8xC
posledním	poslední	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
při	při	k7c6
tomto	tento	k3xDgInSc6
obřadu	obřad	k1gInSc6
hovořil	hovořit	k5eAaImAgMnS
v	v	k7c6
mateřštině	mateřština	k1gFnSc6
svých	svůj	k3xOyFgMnPc2
poddaných	poddaný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1835	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
smrti	smrt	k1gFnSc6
Františka	František	k1gMnSc2
I.	I.	kA
stal	stát	k5eAaPmAgInS
rakouským	rakouský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1836	#num#	k4
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
korunovat	korunovat	k5eAaBmF
na	na	k7c4
českého	český	k2eAgMnSc4d1
krále	král	k1gMnSc4
jako	jako	k9
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejoblíbenějších	oblíbený	k2eAgMnPc2d3
panovníků	panovník	k1gMnPc2
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
sám	sám	k3xTgInSc4
však	však	k9
prakticky	prakticky	k6eAd1
nevládl	vládnout	k5eNaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
před	před	k7c7
smrtí	smrt	k1gFnSc7
fakticky	fakticky	k6eAd1
složil	složit	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
do	do	k7c2
rukou	ruka	k1gFnPc2
všemocného	všemocný	k2eAgMnSc2d1
kancléře	kancléř	k1gMnSc2
Metternicha	Metternich	k1gMnSc2
a	a	k8xC
státní	státní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
předsedal	předsedat	k5eAaImAgMnS
císařův	císařův	k2eAgMnSc1d1
strýc	strýc	k1gMnSc1
arcivévoda	arcivévoda	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
a	a	k8xC
jejímž	jejíž	k3xOyRp3gInSc7
členem	člen	k1gInSc7
byl	být	k5eAaImAgInS
jak	jak	k8xS,k8xC
Metternich	Metternich	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
český	český	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Kolovrat	kolovrat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
naposledy	naposledy	k6eAd1
jmenovaní	jmenovaný	k1gMnPc1
zaujímali	zaujímat	k5eAaImAgMnP
rozdílná	rozdílný	k2eAgNnPc4d1
politická	politický	k2eAgNnPc4d1
stanoviska	stanovisko	k1gNnPc4
a	a	k8xC
navzájem	navzájem	k6eAd1
si	se	k3xPyFc3
blokovali	blokovat	k5eAaImAgMnP
jakákoli	jakýkoli	k3yIgNnPc4
politická	politický	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
nakonec	nakonec	k6eAd1
vedlo	vést	k5eAaImAgNnS
až	až	k9
k	k	k7c3
revoluci	revoluce	k1gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1848	#num#	k4
byl	být	k5eAaImAgMnS
proti	proti	k7c3
své	svůj	k3xOyFgFnSc3
vůli	vůle	k1gFnSc3
odstaven	odstavit	k5eAaPmNgInS
od	od	k7c2
trůnu	trůn	k1gInSc2
a	a	k8xC
v	v	k7c6
Arcibiskupském	arcibiskupský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
abdikoval	abdikovat	k5eAaBmAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
synovce	synovec	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
</s>
<s>
Po	po	k7c6
abdikaci	abdikace	k1gFnSc6
</s>
<s>
Zbytek	zbytek	k1gInSc4
života	život	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
Ferdinand	Ferdinand	k1gMnSc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Marií	Maria	k1gFnSc7
Annou	Anna	k1gFnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
mu	on	k3xPp3gMnSc3
kdysi	kdysi	k6eAd1
lékaři	lékař	k1gMnPc1
předpovídali	předpovídat	k5eAaImAgMnP
krátký	krátký	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
vydržel	vydržet	k5eAaPmAgMnS
zde	zde	k6eAd1
ještě	ještě	k9
27	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
vykázáno	vykázán	k2eAgNnSc4d1
jedno	jeden	k4xCgNnSc4
křídlo	křídlo	k1gNnSc4
na	na	k7c6
druhém	druhý	k4xOgInSc6
nádvoří	nádvoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
zakoupeno	zakoupit	k5eAaPmNgNnS
bývalé	bývalý	k2eAgNnSc1d1
Toskánské	toskánský	k2eAgNnSc1d1
panství	panství	k1gNnSc1
se	s	k7c7
zámky	zámek	k1gInPc7
Ploskovice	Ploskovice	k1gFnSc2
a	a	k8xC
Zákupy	zákup	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc2
pobýval	pobývat	k5eAaImAgMnS
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
a	a	k8xC
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
byly	být	k5eAaImAgInP
nákladně	nákladně	k6eAd1
upraveny	upravit	k5eAaPmNgInP
a	a	k8xC
přestavěny	přestavět	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Zákupský	Zákupský	k2eAgInSc4d1
zámek	zámek	k1gInSc4
mu	on	k3xPp3gMnSc3
patřil	patřit	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
27	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdíval	jezdívat	k5eAaImAgMnS
na	na	k7c4
něj	on	k3xPp3gMnSc4
kočárem	kočár	k1gInSc7
z	z	k7c2
Ploskovic	Ploskovice	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
Prahy	Praha	k1gFnSc2
buď	buď	k8xC
kočárem	kočár	k1gInSc7
<g/>
,	,	kIx,
či	či	k8xC
později	pozdě	k6eAd2
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
1867	#num#	k4
<g/>
)	)	kIx)
vlakem	vlak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
samostatně	samostatně	k6eAd1
občas	občas	k6eAd1
i	i	k9
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
<g/>
,	,	kIx,
sardinská	sardinský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Marie	Maria	k1gFnSc2
Anna	Anna	k1gFnSc1
Karolína	Karolína	k1gFnSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
ta	ten	k3xDgFnSc1
trávila	trávit	k5eAaImAgFnS
daleko	daleko	k6eAd1
více	hodně	k6eAd2
času	čas	k1gInSc2
v	v	k7c6
slunné	slunný	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Zákupech	zákup	k1gInPc6
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
novém	nový	k2eAgNnSc6d1
sídle	sídlo	k1gNnSc6
pobýval	pobývat	k5eAaImAgMnS
excísař	excísař	k1gMnSc1
poprvé	poprvé	k6eAd1
v	v	k7c6
květnu	květen	k1gInSc6
1851	#num#	k4
a	a	k8xC
mnohokrát	mnohokrát	k6eAd1
za	za	k7c7
ním	on	k3xPp3gNnSc7
přijeli	přijet	k5eAaPmAgMnP
i	i	k8xC
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
rozsáhlého	rozsáhlý	k2eAgNnSc2d1
příbuzenstva	příbuzenstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
zámku	zámek	k1gInSc6
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pražané	Pražan	k1gMnPc1
ho	on	k3xPp3gMnSc4
měli	mít	k5eAaImAgMnP
rádi	rád	k2eAgMnPc1d1
<g/>
,	,	kIx,
vídávali	vídávat	k5eAaImAgMnP
ho	on	k3xPp3gMnSc4
na	na	k7c6
dnešní	dnešní	k2eAgFnSc6d1
Národní	národní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
každodenních	každodenní	k2eAgFnPc6d1
procházkách	procházka	k1gFnPc6
dával	dávat	k5eAaImAgInS
dětem	dítě	k1gFnPc3
bonbony	bonbon	k1gInPc4
a	a	k8xC
chudým	chudý	k2eAgInSc7d1
almužnu	almužna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praze	Praha	k1gFnSc6
postupně	postupně	k6eAd1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c4
450	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
věku	věk	k1gInSc6
82	#num#	k4
let	léto	k1gNnPc2
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ferdinandův	Ferdinandův	k2eAgInSc1d1
život	život	k1gInSc1
v	v	k7c6
datech	datum	k1gNnPc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
jakožto	jakožto	k8xS
lombardsko-benátský	lombardsko-benátský	k2eAgMnSc1d1
král	král	k1gMnSc1
na	na	k7c6
portrétu	portrét	k1gInSc6
A.	A.	kA
Weißenböcka	Weißenböcka	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1838	#num#	k4
</s>
<s>
1793	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
–	–	k?
narození	narození	k1gNnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
1802	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
–	–	k?
byl	být	k5eAaImAgInS
povolán	povolat	k5eAaPmNgMnS
princův	princův	k2eAgMnSc1d1
první	první	k4xOgMnSc1
vychovatel	vychovatel	k1gMnSc1
František	František	k1gMnSc1
Maria	Maria	k1gFnSc1
von	von	k1gInSc1
Steffaneo-Carnea	Steffaneo-Carnea	k1gFnSc1
</s>
<s>
1805	#num#	k4
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
–	–	k?
evakuace	evakuace	k1gFnSc2
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
z	z	k7c2
Vídně	Vídeň	k1gFnSc2
před	před	k7c7
blížícími	blížící	k2eAgFnPc7d1
se	se	k3xPyFc4
vojsky	vojsky	k6eAd1
francouzského	francouzský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Napoleona	Napoleon	k1gMnSc2
</s>
<s>
1807	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
–	–	k?
zemřela	zemřít	k5eAaPmAgFnS
Ferdinandova	Ferdinandův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Marie	Maria	k1gFnSc2
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
1807	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
–	–	k?
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
služby	služba	k1gFnSc2
byl	být	k5eAaImAgMnS
propuštěn	propuštěn	k2eAgMnSc1d1
princův	princův	k2eAgMnSc1d1
první	první	k4xOgMnSc1
vychovatel	vychovatel	k1gMnSc1
Steffaneo-Carnea	Steffaneo-Carnea	k1gMnSc1
</s>
<s>
1808	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
–	–	k?
František	František	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Marii	Mario	k1gMnPc7
Ludovikou	Ludovika	k1gFnSc7
z	z	k7c2
Modeny	Modena	k1gFnSc2
a	a	k8xC
Ferdinand	Ferdinand	k1gMnSc1
tak	tak	k6eAd1
dostal	dostat	k5eAaPmAgMnS
nevlastní	vlastní	k2eNgFnSc4d1
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
zasloužila	zasloužit	k5eAaPmAgFnS
o	o	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
a	a	k8xC
vzdělání	vzdělání	k1gNnSc4
</s>
<s>
1809	#num#	k4
–	–	k?
princi	princa	k1gFnSc2
byl	být	k5eAaImAgMnS
přidělen	přidělen	k2eAgMnSc1d1
nový	nový	k2eAgMnSc1d1
vychovatel	vychovatel	k1gMnSc1
–	–	k?
svobodný	svobodný	k2eAgMnSc1d1
pán	pán	k1gMnSc1
Josef	Josef	k1gMnSc1
Erberg	Erberg	k1gMnSc1
</s>
<s>
1815	#num#	k4
–	–	k?
se	se	k3xPyFc4
s	s	k7c7
otcem	otec	k1gMnSc7
zúčastnil	zúčastnit	k5eAaPmAgMnS
tažení	tažení	k1gNnSc4
proti	proti	k7c3
Napoleonovi	Napoleon	k1gMnSc3
</s>
<s>
1816	#num#	k4
<g/>
,	,	kIx,
duben	duben	k1gInSc1
–	–	k?
zemřela	zemřít	k5eAaPmAgFnS
Ferdinandova	Ferdinandův	k2eAgFnSc1d1
nevlastní	vlastní	k2eNgFnSc1d1
matka	matka	k1gFnSc1
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
</s>
<s>
1824	#num#	k4
–	–	k?
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
lékařské	lékařský	k2eAgNnSc1d1
dobrozdání	dobrozdání	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Ferdinand	Ferdinand	k1gMnSc1
není	být	k5eNaImIp3nS
schopen	schopen	k2eAgMnSc1d1
vlády	vláda	k1gFnPc4
</s>
<s>
1825	#num#	k4
–	–	k?
vedl	vést	k5eAaImAgMnS
rakouskou	rakouský	k2eAgFnSc4d1
delegaci	delegace	k1gFnSc4
pozvanou	pozvaný	k2eAgFnSc4d1
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
ke	k	k7c3
korunovaci	korunovace	k1gFnSc3
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
I.	I.	kA
</s>
<s>
1830	#num#	k4
–	–	k?
vydáno	vydat	k5eAaPmNgNnS
lékařské	lékařský	k2eAgNnSc1d1
dobrozdání	dobrozdání	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
potvrzovalo	potvrzovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
vlády	vláda	k1gFnPc4
a	a	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
oženit	oženit	k5eAaPmF
</s>
<s>
1830	#num#	k4
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
korunovace	korunovace	k1gFnSc1
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
v	v	k7c6
Prešpurku	Prešpurku	k?
</s>
<s>
1832	#num#	k4
<g/>
,	,	kIx,
únor	únor	k1gInSc1
–	–	k?
sňatek	sňatek	k1gInSc1
s	s	k7c7
Marií	Maria	k1gFnSc7
Annou	Anna	k1gFnSc7
Karolínou	Karolína	k1gFnSc7
Piou	Pio	k2eAgFnSc4d1
Savojskou	savojský	k2eAgFnSc4d1
</s>
<s>
1832	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
–	–	k?
neúspěšný	úspěšný	k2eNgInSc1d1
atentát	atentát	k1gInSc1
v	v	k7c6
Bádenu	Báden	k1gInSc6
</s>
<s>
1836	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
korunovace	korunovace	k1gFnSc1
českým	český	k2eAgMnSc7d1
králem	král	k1gMnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
1838	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
korunovace	korunovace	k1gFnSc1
v	v	k7c6
Miláně	Milán	k1gInSc6
na	na	k7c4
lombardského	lombardský	k2eAgMnSc4d1
krále	král	k1gMnSc4
</s>
<s>
1848	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
–	–	k?
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
zveřejněn	zveřejněn	k2eAgInSc1d1
manifest	manifest	k1gInSc1
oznamující	oznamující	k2eAgFnSc4d1
Ferdinandovu	Ferdinandův	k2eAgFnSc4d1
abdikaci	abdikace	k1gFnSc4
</s>
<s>
1848	#num#	k4
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
–	–	k?
Ferdinand	Ferdinand	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
se	se	k3xPyFc4
odebrali	odebrat	k5eAaPmAgMnP
do	do	k7c2
Prahy	Praha	k1gFnSc2
</s>
<s>
1856	#num#	k4
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
–	–	k?
oslava	oslava	k1gFnSc1
stříbrné	stříbrný	k2eAgFnSc2d1
svatby	svatba	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
1852	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
květen	květena	k1gFnPc2
–	–	k?
setkání	setkání	k1gNnSc1
s	s	k7c7
carem	car	k1gMnSc7
Mikulášem	Mikuláš	k1gMnSc7
I.	I.	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
1858	#num#	k4
–	–	k?
sepsal	sepsat	k5eAaPmAgMnS
závěť	závěť	k1gFnSc4
</s>
<s>
1875	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
–	–	k?
smrt	smrt	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Tituly	titul	k1gInPc1
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1
Císařské	císařský	k2eAgNnSc1d1
a	a	k8xC
Královské	královský	k2eAgNnSc1d1
Apoštolské	apoštolský	k2eAgNnSc1d1
Veličenstvo	veličenstvo	k1gNnSc1
Ferdinand	Ferdinand	k1gMnSc1
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
Z	z	k7c2
milosti	milost	k1gFnSc2
Boží	boží	k2eAgFnSc2d1
</s>
<s>
Císař	Císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
<g/>
,	,	kIx,
pátý	pátý	k4xOgInSc1
toho	ten	k3xDgNnSc2
jména	jméno	k1gNnSc2
<g/>
,	,	kIx,
král	král	k1gMnSc1
lombardsko-benátský	lombardsko-benátský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
Dalmacie	Dalmacie	k1gFnSc2
<g/>
,	,	kIx,
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
,	,	kIx,
Slavonie	Slavonie	k1gFnSc2
<g/>
,	,	kIx,
král	král	k1gMnSc1
Haliče	Halič	k1gFnSc2
a	a	k8xC
Lodomerie	Lodomerie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Ilyrie	Ilyrie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Král	Král	k1gMnSc1
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Arcivévoda	arcivévoda	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Velkovévoda	velkovévoda	k1gMnSc1
toskánský	toskánský	k2eAgMnSc1d1
a	a	k8xC
krakovský	krakovský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vévoda	vévoda	k1gMnSc1
lotrinský	lotrinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
salzburský	salzburský	k2eAgInSc1d1
<g/>
,	,	kIx,
štýrský	štýrský	k2eAgInSc1d1
<g/>
,	,	kIx,
korutanský	korutanský	k2eAgInSc1d1
<g/>
,	,	kIx,
kraňský	kraňský	k2eAgInSc1d1
<g/>
,	,	kIx,
slezský	slezský	k2eAgInSc1d1
<g/>
,	,	kIx,
modenský	modenský	k2eAgInSc1d1
<g/>
,	,	kIx,
parmský	parmský	k2eAgInSc1d1
<g/>
,	,	kIx,
Piacenzy	Piacenza	k1gFnPc4
a	a	k8xC
Guastally	Guastall	k1gInPc4
<g/>
,	,	kIx,
osvětimský	osvětimský	k2eAgInSc1d1
a	a	k8xC
zátorský	zátorský	k2eAgInSc1d1
<g/>
,	,	kIx,
těšínský	těšínský	k2eAgInSc1d1
<g/>
,	,	kIx,
<g/>
fruilský	fruilský	k2eAgInSc1d1
<g/>
,	,	kIx,
Dubrovníku	Dubrovník	k1gInSc2
a	a	k8xC
Zadaru	Zadar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Velkokníže	velkokníže	k1gMnSc1
sedmihradský	sedmihradský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1
habsburský	habsburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
kyburský	kyburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
tyrolský	tyrolský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Gorice	Gorice	k1gFnSc1
a	a	k8xC
Gradiška	Gradiška	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Princ	princ	k1gMnSc1
tridentský	tridentský	k2eAgMnSc1d1
a	a	k8xC
brixenský	brixenský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Markrabě	markrabě	k1gMnSc1
Horní	horní	k2eAgFnSc2d1
Lužice	Lužice	k1gFnSc2
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnSc1d1
Lužice	Lužice	k1gFnSc1
a	a	k8xC
istrijský	istrijský	k2eAgMnSc1d1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
Hohenemsu	Hohenems	k1gInSc2
<g/>
,	,	kIx,
Feldkirchu	Feldkirch	k1gInSc2
<g/>
,	,	kIx,
Bregenzu	Bregenz	k1gInSc2
<g/>
,	,	kIx,
Sonnenburgu	Sonnenburg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pán	pán	k1gMnSc1
Terstu	Terst	k1gInSc2
<g/>
,	,	kIx,
Kotoru	Kotor	k1gInSc2
a	a	k8xC
vindické	vindický	k2eAgFnSc2d1
marky	marka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Charlotta	Charlotta	k1gFnSc1
Orléanská	Orléanský	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
V.	V.	kA
Španělský	španělský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
</s>
<s>
Isabela	Isabela	k1gFnSc1
Farnese	Farnese	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovikum	k1gNnSc2
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
August	August	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
</s>
<s>
'	'	kIx"
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
<g/>
'	'	kIx"
</s>
<s>
Filip	Filip	k1gMnSc1
V.	V.	kA
Španělský	španělský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Parmská	parmský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgMnSc5d1
</s>
<s>
August	August	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Charlotta	Charlotta	k1gFnSc1
Orléanská	Orléanský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ASCHENBRENNER	ASCHENBRENNER	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezděz	Bezděz	k1gInSc1
,	,	kIx,
vlastivědný	vlastivědný	k2eAgInSc1d1
sborník	sborník	k1gInSc1
Českolipska	Českolipsko	k1gNnSc2
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
9172	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
:	:	kIx,
16	#num#	k4
<g/>
.	.	kIx.
sešit	sešit	k1gInSc1
:	:	kIx,
Ep	Ep	k1gMnSc1
<g/>
–	–	k?
<g/>
Fe	Fe	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
;	;	kIx,
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
136	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978-80-200-2292-9	978-80-200-2292-9	k4
(	(	kIx(
<g/>
Academia	academia	k1gFnSc1
<g/>
)	)	kIx)
;	;	kIx,
ISBN	ISBN	kA
978-80-7286-215-3	978-80-7286-215-3	k4
(	(	kIx(
<g/>
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
125	#num#	k4
<g/>
–	–	k?
<g/>
126	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
<g/>
;	;	kIx,
RAK	rak	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
VLNAS	VLNAS	kA
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stínu	stín	k1gInSc6
tvých	tvůj	k3xOp2gNnPc2
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85785	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA
<g/>
,	,	kIx,
Brigitte	Brigitte	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisná	životopisný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
;	;	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85946	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
HOLLER	HOLLER	kA
Gerd	Gerd	k1gMnSc1
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
I	i	k9
:	:	kIx,
Poslední	poslední	k2eAgInSc1d1
Habsburk	Habsburk	k1gInSc1
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
<g/>
:	:	kIx,
Spravedlnost	spravedlnost	k1gFnSc1
pro	pro	k7c4
císaře	císař	k1gMnSc4
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnPc1
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1998	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85946-90-4	80-85946-90-4	k4
</s>
<s>
KLEISNER	KLEISNER	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medaile	medaile	k1gFnSc1
císaře	císař	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
Dobrotivého	dobrotivý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7036	#num#	k4
<g/>
-	-	kIx~
<g/>
396	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SÉGUR-CABANAC	SÉGUR-CABANAC	k?
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
Graf	graf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaiser	Kaiser	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
V.	V.	kA
<g/>
)	)	kIx)
der	drát	k5eAaImRp2nS
Gütige	Gütige	k1gInSc1
in	in	k?
Prag	Prag	k1gInSc1
:	:	kIx,
Die	Die	k1gFnSc1
Zeit	Zeit	k1gMnSc1
nach	nach	k1gInSc1
dem	dem	k?
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
März	März	k1gInSc1
1848	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brünn	Brünn	k1gMnSc1
<g/>
:	:	kIx,
Irrgang	Irrgang	k1gMnSc1
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
215	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SEKYRKOVÁ	sekyrkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Milada	Milada	k1gFnSc1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1836	#num#	k4
<g/>
:	:	kIx,
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
Poslední	poslední	k2eAgFnSc1d1
pražská	pražský	k2eAgFnSc1d1
korunovace	korunovace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Havran	Havran	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
189	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86515	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SEKYRKOVÁ	sekyrkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Milada	Milada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
In	In	k1gMnSc1
<g/>
:	:	kIx,
RYANTOVÁ	RYANTOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
VOREL	Vorel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
940	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
519	#num#	k4
<g/>
-	-	kIx~
<g/>
529	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SCHIMMER	SCHIMMER	kA
C.	C.	kA
A.	A.	kA
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Vídeň	Vídeň	k1gFnSc1
1849	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
představitelů	představitel	k1gMnPc2
českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
rakouských	rakouský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
uherských	uherský	k2eAgMnPc2d1
králů	král	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
ve	v	k7c4
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
1830	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Chorvatsko-slavonský	chorvatsko-slavonský	k2eAgMnSc1d1
král	král	k1gMnSc1
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Dalmátský	Dalmátský	k2eAgMnSc1d1
král	král	k1gMnSc1
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Lombardsko-benátský	lombardsko-benátský	k2eAgMnSc1d1
král	král	k1gMnSc1
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
zánik	zánik	k1gInSc1
státu	stát	k1gInSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Haličsko-vladiměřský	Haličsko-vladiměřský	k2eAgMnSc1d1
král	král	k1gMnSc1
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Hlava	hlava	k1gFnSc1
dynastie	dynastie	k1gFnSc2
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnPc1
mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Krok	krok	k1gInSc1
•	•	k?
Libuše	Libuše	k1gFnSc1
Mytičtí	mytický	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Oráč	oráč	k1gMnSc1
•	•	k?
Nezamysl	Nezamysl	k1gMnSc1
•	•	k?
Mnata	Mnata	k1gFnSc1
•	•	k?
Vojen	vojna	k1gFnPc2
•	•	k?
Vnislav	Vnislav	k1gMnSc1
•	•	k?
Křesomysl	Křesomysl	k1gMnSc1
•	•	k?
Neklan	Neklan	k1gMnSc1
•	•	k?
Hostivít	Hostivít	k1gMnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
872	#num#	k4
<g/>
–	–	k?
<g/>
889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
I.	I.	kA
(	(	kIx(
<g/>
894	#num#	k4
<g/>
–	–	k?
<g/>
915	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
915	#num#	k4
<g/>
–	–	k?
<g/>
921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
svatý	svatý	k1gMnSc1
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
921	#num#	k4
<g/>
–	–	k?
<g/>
935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
935	#num#	k4
<g/>
–	–	k?
<g/>
972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
972	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
999	#num#	k4
<g/>
–	–	k?
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
Chrabrý¹	Chrabrý¹	k1gMnSc1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
–	–	k?
<g/>
1004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladivoj	Vladivoj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1004	#num#	k4
<g/>
–	–	k?
<g/>
1012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
–	–	k?
<g/>
1033	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1033	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
II	II	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
(	(	kIx(
<g/>
1100	#num#	k4
<g/>
–	–	k?
<g/>
1107	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svatopluk	Svatopluk	k1gMnSc1
Olomoucký	olomoucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1109	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1109	#num#	k4
<g/>
–	–	k?
<g/>
1117	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1117	#num#	k4
<g/>
–	–	k?
<g/>
1020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
I.	I.	kA
(	(	kIx(
<g/>
1120	#num#	k4
<g/>
–	–	k?
<g/>
1025	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
–	–	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1140	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1173	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1173	#num#	k4
<g/>
–	–	k?
<g/>
1178	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1178	#num#	k4
<g/>
–	–	k?
<g/>
1189	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
(	(	kIx(
<g/>
1193	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vladislav	Vladislav	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1198	#num#	k4
<g/>
–	–	k?
<g/>
1230	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1230	#num#	k4
<g/>
–	–	k?
<g/>
1253	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1253	#num#	k4
<g/>
–	–	k?
<g/>
1278	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1278	#num#	k4
<g/>
–	–	k?
<g/>
1305	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1305	#num#	k4
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1307	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
Lucemburkové	Lucemburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1346	#num#	k4
<g/>
–	–	k?
<g/>
1378	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1378	#num#	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1419	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1438	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
•	•	k?
interregnum	interregnum	k1gNnSc4
(	(	kIx(
<g/>
1439	#num#	k4
<g/>
–	–	k?
<g/>
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
(	(	kIx(
<g/>
1453	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1457	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Korvín²	Korvín²	k1gMnSc1
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
<g/>
)	)	kIx)
Jagellonci	Jagellonec	k1gInSc6
<g/>
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1516	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1516	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1564	#num#	k4
<g/>
–	–	k?
<g/>
1576	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1576	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
–	–	k?
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Falcký³	Falcký³	k1gMnSc1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1620	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
–	–	k?
<g/>
1657	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgInSc1d1
•	•	k?
Leopold	Leopolda	k1gFnPc2
I.	I.	kA
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský³	Bavorský³	k1gFnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
Habsburko-Lotrinkové	Habsburko-Lotrinkový	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
¹	¹	k?
Piastovec	Piastovec	k1gInSc1
<g/>
,	,	kIx,
²	²	k?
vládce	vládce	k1gMnSc1
vedlejších	vedlejší	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
³	³	k?
vzdorokrál	vzdorokrál	k1gMnSc1
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Markrabata	markrabě	k1gNnPc4
Malý	malý	k2eAgInSc4d1
znak	znak	k1gInSc4
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babenberkové	Babenberkové	k2eAgInSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Jindřich	Jindřich	k1gMnSc1
I.	I.	kA
•	•	k?
Adalbert	Adalbert	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
do	do	k7c2
1156	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vévodové	vévoda	k1gMnPc1
Babenberkové	Babenberková	k1gFnSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1156	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemyslovci	Přemyslovec	k1gMnPc5
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zähringové	Zähringový	k2eAgFnPc4d1
</s>
<s>
Heřman	Heřman	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bádenský	bádenský	k2eAgMnSc1d1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Bádenský	bádenský	k2eAgInSc5d1
Habsburkové	Habsburk	k1gMnPc5
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
I.	I.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
I.	I.	kA
•	•	k?
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ota	Ota	k1gMnSc1
•	•	k?
Rudolf	Rudolf	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Vilém	Vilém	k1gMnSc1
•	•	k?
Albrecht	Albrecht	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
V.	V.	kA
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Arnošt	Arnošt	k1gMnSc1
Železný	Železný	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
do	do	k7c2
1453	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Arcivévodové	arcivévoda	k1gMnPc1
Habsburkové	Habsburk	k1gMnPc1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Rudolf	Rudolf	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
do	do	k7c2
1804	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Císařové	Císařové	k2eAgFnSc1d1
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
od	od	k7c2
1804	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
rakouští	rakouský	k2eAgMnPc1d1
princové	princ	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
titulární	titulární	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
uvedeni	uvést	k5eAaPmNgMnP
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
šabloně	šablona	k1gFnSc6
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
Znak	znak	k1gInSc4
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
I.	I.	kA
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
V.	V.	kA
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
<g/>
*	*	kIx~
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štýrský	štýrský	k2eAgInSc4d1
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Karel	Karel	k1gMnSc1
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Jan	Jan	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
Vilém	Vilém	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
František	František	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
František	František	k1gMnSc1
<g/>
**	**	k?
12	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ambrož	Ambrož	k1gMnSc1
<g/>
***	***	k?
13	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
•	•	k?
František	františek	k1gInSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
•	•	k?
Štěpán	Štěpán	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Rainer	Rainer	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
V.	V.	kA
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
***	***	k?
14	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Mexický	mexický	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Rakousko-Těšínský	rakousko-těšínský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
•	•	k?
Evžen	Evžen	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
August	August	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Filip	Filip	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Ota	Ota	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Petr	Petr	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopold	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Leo	Leo	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Luitpold	Luitpold	k1gMnSc1
16	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Evžen	Evžen	k1gMnSc1
•	•	k?
Gottfried	Gottfried	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Pius	Pius	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Hubert	Hubert	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Theodor	Theodor	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Klement	Klement	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
17	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Habsburg	Habsburg	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Felix	Felix	k1gMnSc1
Rakouský	rakouský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
*	*	kIx~
také	také	k9
španělský	španělský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
(	(	kIx(
<g/>
infant	infant	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
**	**	k?
také	také	k9
toskánský	toskánský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
***	***	k?
také	také	k9
modenský	modenský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
kurzívou	kurzíva	k1gFnSc7
jsou	být	k5eAaImIp3nP
rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700516	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118686747	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2125	#num#	k4
5254	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
93011939	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500354224	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27865600	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
93011939	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc1
</s>
