<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
uherský	uherský	k2eAgMnSc1d1
a	a	k8xC
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
V.	V.	kA
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1793	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1875	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
habsbursko-lotrinské	habsbursko-lotrinský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
,	,	kIx,
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
poslední	poslední	k2eAgMnSc1d1
korunovaný	korunovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
atd.	atd.	kA
</s>