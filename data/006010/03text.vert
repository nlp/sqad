<s>
Otaku	Otaku	k6eAd1	Otaku
(	(	kIx(	(
<g/>
お	お	k?	お
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgInSc1d1	japonský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
počítačového	počítačový	k2eAgMnSc4d1	počítačový
nadšence	nadšenec	k1gMnSc4	nadšenec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
geek	geek	k1gInSc1	geek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
označující	označující	k2eAgFnSc4d1	označující
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
posedlá	posedlý	k2eAgFnSc1d1	posedlá
zálibami	záliba	k1gFnPc7	záliba
jako	jako	k8xC	jako
je	on	k3xPp3gNnSc4	on
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
manga	mango	k1gNnSc2	mango
či	či	k8xC	či
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
otaku	otak	k1gInSc2	otak
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
z	z	k7c2	z
japonského	japonský	k2eAgInSc2d1	japonský
termínu	termín	k1gInSc2	termín
pro	pro	k7c4	pro
cizí	cizí	k2eAgInSc4d1	cizí
dům	dům	k1gInSc4	dům
nebo	nebo	k8xC	nebo
rodinu	rodina	k1gFnSc4	rodina
(	(	kIx(	(
<g/>
お	お	k?	お
<g/>
,	,	kIx,	,
otaku	otaku	k6eAd1	otaku
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
též	též	k9	též
užíváno	užíván	k2eAgNnSc4d1	užíváno
jako	jako	k8xS	jako
uctivé	uctivý	k2eAgNnSc4d1	uctivé
osobní	osobní	k2eAgNnSc4d1	osobní
zájmeno	zájmeno	k1gNnSc4	zájmeno
"	"	kIx"	"
<g/>
vy	vy	k3xPp2nPc1	vy
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
vzdáleně	vzdáleně	k6eAd1	vzdáleně
přirovnatelné	přirovnatelný	k2eAgNnSc1d1	přirovnatelné
ke	k	k7c3	k
španělskému	španělský	k2eAgInSc3d1	španělský
výrazu	výraz	k1gInSc3	výraz
usted	usted	k1gInSc4	usted
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
slang	slang	k1gInSc1	slang
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
starého	starý	k2eAgNnSc2d1	staré
užití	užití	k1gNnSc2	užití
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
hiraganě	hiragana	k1gFnSc6	hiragana
(	(	kIx(	(
<g/>
お	お	k?	お
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
katakaně	katakaně	k6eAd1	katakaně
(	(	kIx(	(
<g/>
オ	オ	k?	オ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
použití	použití	k1gNnSc1	použití
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
použil	použít	k5eAaPmAgMnS	použít
humorista	humorista	k1gMnSc1	humorista
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
Akio	Akio	k1gMnSc1	Akio
Nakamori	Nakamor	k1gFnSc2	Nakamor
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Otaku	Otak	k1gInSc2	Otak
(	(	kIx(	(
<g/>
お	お	k?	お
Otaku	Otak	k1gInSc2	Otak
no	no	k9	no
Kenkjú	Kenkjú	k1gFnSc1	Kenkjú
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydávané	vydávaný	k2eAgFnSc2d1	vydávaná
v	v	k7c4	v
lolicon	lolicon	k1gNnSc4	lolicon
magazínu	magazín	k1gInSc2	magazín
Manga	mango	k1gNnSc2	mango
Burikko	Burikko	k1gNnSc1	Burikko
<g/>
.	.	kIx.	.
</s>
<s>
Akio	Akio	k1gMnSc1	Akio
Nakamori	Nakamore	k1gFnSc4	Nakamore
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
oslovení	oslovení	k1gNnSc1	oslovení
běžně	běžně	k6eAd1	běžně
používáno	používat	k5eAaImNgNnS	používat
mezi	mezi	k7c7	mezi
počítačovými	počítačový	k2eAgMnPc7d1	počítačový
nadšenci	nadšenec	k1gMnPc7	nadšenec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
geek	geek	k1gInSc1	geek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
popisovalo	popisovat	k5eAaImAgNnS	popisovat
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
se	s	k7c7	s
sobě	se	k3xPyFc3	se
rovnými	rovný	k2eAgFnPc7d1	rovná
pomocí	pomocí	k7c2	pomocí
odtažitých	odtažitý	k2eAgInPc2d1	odtažitý
<g/>
,	,	kIx,	,
formálních	formální	k2eAgNnPc2d1	formální
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
tráví	trávit	k5eAaImIp3nP	trávit
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
do	do	k7c2	do
běžné	běžný	k2eAgFnSc2d1	běžná
řeči	řeč	k1gFnSc2	řeč
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
termín	termín	k1gInSc1	termín
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
známosti	známost	k1gFnSc2	známost
díky	dík	k1gInPc1	dík
Nakamoriho	Nakamori	k1gMnSc2	Nakamori
publikaci	publikace	k1gFnSc6	publikace
Období	období	k1gNnSc1	období
M	M	kA	M
(	(	kIx(	(
<g/>
Mの	Mの	k1gMnSc1	Mの
M	M	kA	M
no	no	k9	no
Džidai	Džidai	k1gNnSc2	Džidai
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
<g/>
)	)	kIx)	)
nedávno	nedávno	k6eAd1	nedávno
zatčeného	zatčený	k2eAgMnSc2d1	zatčený
samotářského	samotářský	k2eAgMnSc2d1	samotářský
sériového	sériový	k2eAgMnSc2d1	sériový
vraha	vrah	k1gMnSc2	vrah
Cutomu	Cutom	k1gInSc2	Cutom
Mijazakiho	Mijazaki	k1gMnSc2	Mijazaki
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
posedlý	posedlý	k2eAgMnSc1d1	posedlý
pornografickými	pornografický	k2eAgInPc7d1	pornografický
anime	anim	k1gInSc5	anim
<g/>
/	/	kIx~	/
<g/>
manga	mango	k1gNnPc4	mango
a	a	k8xC	a
trpěl	trpět	k5eAaImAgMnS	trpět
představami	představa	k1gFnPc7	představa
o	o	k7c6	o
znásilňování	znásilňování	k1gNnSc6	znásilňování
mladých	mladý	k2eAgFnPc2d1	mladá
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
z	z	k7c2	z
kdysi	kdysi	k6eAd1	kdysi
neškodného	škodný	k2eNgNnSc2d1	neškodné
slova	slovo	k1gNnSc2	slovo
stalo	stát	k5eAaPmAgNnS	stát
velké	velký	k2eAgNnSc1d1	velké
tabu	tabu	k1gNnSc1	tabu
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
interpretace	interpretace	k1gFnSc1	interpretace
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
původu	původ	k1gInSc2	původ
slova	slovo	k1gNnSc2	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
květnového	květnový	k2eAgNnSc2d1	květnové
vydání	vydání	k1gNnSc2	vydání
časopisu	časopis	k1gInSc2	časopis
EX	ex	k6eAd1	ex
Taishuu	Taishua	k1gFnSc4	Taishua
Magazine	Magazin	k1gInSc5	Magazin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
televizní	televizní	k2eAgFnSc2d1	televizní
série	série	k1gFnSc2	série
Super	super	k2eAgInSc1d1	super
Dimension	Dimension	k1gInSc1	Dimension
Fortress	Fortressa	k1gFnPc2	Fortressa
Macross	Macrossa	k1gFnPc2	Macrossa
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
–	–	k?	–
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
pořadu	pořad	k1gInSc2	pořad
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
oslovovat	oslovovat	k5eAaImF	oslovovat
ostatní	ostatní	k2eAgFnPc4d1	ostatní
postavy	postava	k1gFnPc4	postava
"	"	kIx"	"
<g/>
otaku	otaku	k6eAd1	otaku
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
fanoušci	fanoušek	k1gMnPc1	fanoušek
poté	poté	k6eAd1	poté
slovo	slovo	k1gNnSc4	slovo
přejali	přejmout	k5eAaPmAgMnP	přejmout
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
původcem	původce	k1gMnSc7	původce
"	"	kIx"	"
<g/>
otaku	otako	k1gNnSc3	otako
<g/>
"	"	kIx"	"
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
díla	dílo	k1gNnPc4	dílo
sci-fi	scii	k1gFnSc2	sci-fi
autorky	autorka	k1gFnSc2	autorka
Motoko	Motoko	k1gNnSc1	Motoko
Arai	Arai	k1gNnSc2	Arai
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Wrong	Wrong	k1gInSc1	Wrong
About	About	k1gMnSc1	About
Japan	japan	k1gInSc1	japan
<g/>
"	"	kIx"	"
autor	autor	k1gMnSc1	autor
Peter	Peter	k1gMnSc1	Peter
Carey	Carea	k1gFnSc2	Carea
dělá	dělat	k5eAaImIp3nS	dělat
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
novelistkou	novelistka	k1gFnSc7	novelistka
<g/>
,	,	kIx,	,
umělkyní	umělkyně	k1gFnSc7	umělkyně
a	a	k8xC	a
Gundam	Gundam	k1gInSc1	Gundam
kronikářkou	kronikářka	k1gFnSc7	kronikářka
Juka	juka	k1gFnSc1	juka
Minakawaou	Minakawaa	k1gFnSc7	Minakawaa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prozradila	prozradit	k5eAaPmAgFnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
Otaku	Otak	k1gInSc2	Otak
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
k	k	k7c3	k
oslovování	oslovování	k1gNnSc3	oslovování
čtenářů	čtenář	k1gMnPc2	čtenář
této	tento	k3xDgFnSc2	tento
autorky	autorka	k1gFnSc2	autorka
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáři	čtenář	k1gMnPc1	čtenář
měli	mít	k5eAaImAgMnP	mít
její	její	k3xOp3gFnSc4	její
práci	práce	k1gFnSc4	práce
natolik	natolik	k6eAd1	natolik
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
přijali	přijmout	k5eAaPmAgMnP	přijmout
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
japonském	japonský	k2eAgInSc6d1	japonský
slangu	slang	k1gInSc6	slang
otaku	otaku	k6eAd1	otaku
popisuje	popisovat	k5eAaImIp3nS	popisovat
fanouška	fanoušek	k1gMnSc4	fanoušek
posedlého	posedlý	k2eAgInSc2d1	posedlý
specifickým	specifický	k2eAgNnSc7d1	specifické
tématem	téma	k1gNnSc7	téma
nebo	nebo	k8xC	nebo
koníčkem	koníček	k1gInSc7	koníček
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
anime	animat	k5eAaPmIp3nS	animat
otaku	otaku	k6eAd1	otaku
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
tráví	trávit	k5eAaImIp3nS	trávit
v	v	k7c4	v
kuse	kuse	k6eAd1	kuse
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
bez	bez	k7c2	bez
odpočinku	odpočinek	k1gInSc2	odpočinek
sledováním	sledování	k1gNnSc7	sledování
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
a	a	k8xC	a
manga	mango	k1gNnSc2	mango
otaku	otak	k1gInSc2	otak
(	(	kIx(	(
<g/>
nadšenec	nadšenec	k1gMnSc1	nadšenec
japonských	japonský	k2eAgFnPc2d1	japonská
kreslených	kreslený	k2eAgFnPc2d1	kreslená
novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pasokon	pasokon	k1gNnSc1	pasokon
otaku	otak	k1gInSc2	otak
(	(	kIx(	(
<g/>
PC	PC	kA	PC
nadšenci	nadšenec	k1gMnPc1	nadšenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gému	géma	k1gMnSc4	géma
otaku	otaka	k1gMnSc4	otaka
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
a	a	k8xC	a
otaku	otaku	k6eAd1	otaku
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
radikální	radikální	k2eAgMnPc1d1	radikální
fanoušci	fanoušek	k1gMnPc1	fanoušek
japonských	japonský	k2eAgInPc2d1	japonský
idolů	idol	k1gInPc2	idol
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
mladých	mladý	k2eAgFnPc2d1	mladá
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existují	existovat	k5eAaImIp3nP	existovat
tecudó	tecudó	k?	tecudó
otaku	otaka	k1gFnSc4	otaka
(	(	kIx(	(
<g/>
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
koníčkem	koníček	k1gInSc7	koníček
je	být	k5eAaImIp3nS	být
studování	studování	k1gNnSc1	studování
metra	metro	k1gNnSc2	metro
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
gundži	gundzat	k5eAaPmIp1nS	gundzat
otaku	otaku	k6eAd1	otaku
(	(	kIx(	(
<g/>
vojenští	vojenský	k2eAgMnPc1d1	vojenský
nadšenci	nadšenec	k1gMnPc1	nadšenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
pojem	pojem	k1gInSc1	pojem
Otaku	Otak	k1gInSc2	Otak
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
zavedených	zavedený	k2eAgNnPc6d1	zavedené
slovních	slovní	k2eAgNnPc6d1	slovní
spojeních	spojení	k1gNnPc6	spojení
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c4	na
cokoliv	cokoliv	k3yInSc4	cokoliv
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgFnSc4d1	hudební
otaku	otaka	k1gFnSc4	otaka
<g/>
,	,	kIx,	,
otaku	otaka	k1gFnSc4	otaka
do	do	k7c2	do
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
kulinářský	kulinářský	k2eAgMnSc1d1	kulinářský
otaku	otaku	k6eAd1	otaku
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převzaté	převzatý	k2eAgNnSc1d1	převzaté
slovo	slovo	k1gNnSc1	slovo
maniakku	maniakk	k1gInSc2	maniakk
či	či	k8xC	či
mania	manium	k1gNnSc2	manium
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
"	"	kIx"	"
<g/>
maniac	maniac	k1gFnSc1	maniac
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
dáváno	dáván	k2eAgNnSc1d1	dáváno
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
se	s	k7c7	s
specialistou	specialista	k1gMnSc7	specialista
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
zájem	zájem	k1gInSc4	zájem
či	či	k8xC	či
koníček	koníček	k1gInSc4	koníček
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
jevit	jevit	k5eAaImF	jevit
známky	známka	k1gFnPc1	známka
otaku	otak	k1gInSc2	otak
sklonů	sklon	k1gInPc2	sklon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Gundam	Gundam	k1gInSc1	Gundam
Mania	Manium	k1gNnSc2	Manium
by	by	kYmCp3nS	by
popisovalo	popisovat	k5eAaImAgNnS	popisovat
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c6	o
anime	animat	k5eAaPmIp3nS	animat
sérii	série	k1gFnSc4	série
Gundam	Gundam	k1gInSc1	Gundam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maniakku	Maniakku	k6eAd1	Maniakku
je	být	k5eAaImIp3nS	být
mírnější	mírný	k2eAgFnSc1d2	mírnější
a	a	k8xC	a
méně	málo	k6eAd2	málo
urážející	urážející	k2eAgInSc4d1	urážející
výraz	výraz	k1gInSc4	výraz
než	než	k8xS	než
otaku	otaka	k1gFnSc4	otaka
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
japonští	japonský	k2eAgMnPc1d1	japonský
otaku	otaku	k6eAd1	otaku
takto	takto	k6eAd1	takto
humorně	humorně	k6eAd1	humorně
označují	označovat	k5eAaImIp3nP	označovat
sebe	sebe	k3xPyFc4	sebe
nebo	nebo	k8xC	nebo
své	svůj	k3xOyFgNnSc4	svůj
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
přijímajíc	přijímat	k5eAaImSgNnS	přijímat
tak	tak	k6eAd1	tak
postavení	postavení	k1gNnSc1	postavení
vášnivého	vášnivý	k2eAgMnSc2d1	vášnivý
fanouška	fanoušek	k1gMnSc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
užívají	užívat	k5eAaImIp3nP	užívat
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
hrdě	hrdě	k6eAd1	hrdě
<g/>
,	,	kIx,	,
snažíc	snažit	k5eAaImSgFnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
zbavení	zbavení	k1gNnSc4	zbavení
negativního	negativní	k2eAgInSc2d1	negativní
podtónu	podtón	k1gInSc2	podtón
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
řeči	řeč	k1gFnSc6	řeč
většina	většina	k1gFnSc1	většina
Japonců	Japonec	k1gMnPc2	Japonec
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nepřijatelné	přijatelný	k2eNgNnSc4d1	nepřijatelné
být	být	k5eAaImF	být
vážnou	vážný	k2eAgFnSc7d1	vážná
cestou	cesta	k1gFnSc7	cesta
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
otaku	otaku	k6eAd1	otaku
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
otaku	otaku	k6eAd1	otaku
stereotypně	stereotypně	k6eAd1	stereotypně
spojováno	spojován	k2eAgNnSc1d1	spojováno
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
ženských	ženská	k1gFnPc2	ženská
Otaku	Otak	k1gInSc2	Otak
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
ulička	ulička	k1gFnSc1	ulička
v	v	k7c6	v
Tokijském	tokijský	k2eAgInSc6d1	tokijský
Higaši	Higaš	k1gInSc6	Higaš
Ikebukuro	Ikebukura	k1gFnSc5	Ikebukura
distriktu	distrikt	k1gInSc3	distrikt
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Otome	Otom	k1gInSc5	Otom
Road	Road	k1gInSc4	Road
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
panen	panna	k1gFnPc2	panna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
panen	panna	k1gFnPc2	panna
je	být	k5eAaImIp3nS	být
průřezem	průřez	k1gInSc7	průřez
japonského	japonský	k2eAgNnSc2d1	Japonské
ženství	ženství	k1gNnSc2	ženství
<g/>
,	,	kIx,	,
s	s	k7c7	s
věkovým	věkový	k2eAgNnSc7d1	věkové
rozpětím	rozpětí	k1gNnSc7	rozpětí
od	od	k7c2	od
středoškolaček	středoškolačka	k1gFnPc2	středoškolačka
po	po	k7c6	po
klasické	klasický	k2eAgFnSc6d1	klasická
40	[number]	k4	40
<g/>
leté	letý	k2eAgFnPc1d1	letá
hospodyňky	hospodyňka	k1gFnPc1	hospodyňka
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
zasvěcených	zasvěcený	k2eAgMnPc2d1	zasvěcený
komiksům	komiks	k1gInPc3	komiks
a	a	k8xC	a
knihám	kniha	k1gFnPc3	kniha
s	s	k7c7	s
Shounen-ai	Shouneni	k1gNnSc7	Shounen-ai
tematikou	tematika	k1gFnSc7	tematika
(	(	kIx(	(
<g/>
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
homosexuálních	homosexuální	k2eAgMnPc6d1	homosexuální
mužích	muž	k1gMnPc6	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poličkách	polička	k1gFnPc6	polička
v	v	k7c6	v
Otome	Otom	k1gInSc5	Otom
Road	Road	k1gMnSc1	Road
dominuje	dominovat	k5eAaImIp3nS	dominovat
dódžinši	dódžinše	k1gFnSc4	dódžinše
<g/>
,	,	kIx,	,
manga	mango	k1gNnPc4	mango
vytvářená	vytvářený	k2eAgFnSc1d1	vytvářená
amatérskými	amatérský	k2eAgMnPc7d1	amatérský
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
slavnými	slavný	k2eAgFnPc7d1	slavná
mangami	manga	k1gFnPc7	manga
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc7	jejich
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc4	možnost
podívat	podívat	k5eAaImF	podívat
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
na	na	k7c4	na
otaku	otaka	k1gFnSc4	otaka
kulturu	kultura	k1gFnSc4	kultura
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
údajně	údajně	k6eAd1	údajně
pravdivý	pravdivý	k2eAgInSc4d1	pravdivý
příběh	příběh	k1gInSc4	příběh
z	z	k7c2	z
internetové	internetový	k2eAgFnSc2d1	internetová
nástěnky	nástěnka	k1gFnSc2	nástěnka
http://2ch.net	[url]	k1gMnSc1	http://2ch.net
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Denša	Denš	k1gInSc2	Denš
Otoko	Otoko	k1gNnSc1	Otoko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
romantický	romantický	k2eAgInSc1d1	romantický
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
geekovi	geeek	k1gMnSc6	geeek
a	a	k8xC	a
pohledné	pohledný	k2eAgFnSc6d1	pohledná
ženě	žena	k1gFnSc6	žena
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
potkají	potkat	k5eAaPmIp3nP	potkat
na	na	k7c6	na
vlakovém	vlakový	k2eAgNnSc6d1	vlakové
nástupišti	nástupiště	k1gNnSc6	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
několika	několik	k4yIc2	několik
mang	mango	k1gNnPc2	mango
<g/>
,	,	kIx,	,
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
vydaného	vydaný	k2eAgInSc2d1	vydaný
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
vysílaného	vysílaný	k2eAgMnSc2d1	vysílaný
Fuji	Fuji	kA	Fuji
TV	TV	kA	TV
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
stalo	stát	k5eAaPmAgNnS	stát
dalším	další	k2eAgNnSc7d1	další
horkým	horký	k2eAgNnSc7d1	horké
tématem	téma	k1gNnSc7	téma
<g/>
,	,	kIx,	,
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
zredukovat	zredukovat	k5eAaPmF	zredukovat
negativní	negativní	k2eAgInPc4d1	negativní
stereotypy	stereotyp	k1gInPc4	stereotyp
o	o	k7c6	o
otaku	otak	k1gInSc6	otak
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
přijatelnost	přijatelnost	k1gFnSc4	přijatelnost
některých	některý	k3yIgNnPc2	některý
otaku	otaku	k6eAd1	otaku
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
právě	právě	k9	právě
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
smršti	smršť	k1gFnSc3	smršť
se	se	k3xPyFc4	se
pár	pár	k4xCyI	pár
japonských	japonský	k2eAgFnPc2d1	japonská
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
herců	herc	k1gInPc2	herc
a	a	k8xC	a
modelů	model	k1gInPc2	model
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
otaku	otaku	k6eAd1	otaku
koníčkům	koníček	k1gInPc3	koníček
<g/>
.	.	kIx.	.
</s>
<s>
Podskupinou	podskupina	k1gFnSc7	podskupina
otaku	otaku	k6eAd1	otaku
jsou	být	k5eAaImIp3nP	být
Akiba-kei	Akibae	k1gMnPc1	Akiba-ke
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
trávící	trávící	k2eAgFnSc2d1	trávící
mnoho	mnoho	k4c1	mnoho
času	čas	k1gInSc2	čas
v	v	k7c6	v
Akihabaře	Akihabara	k1gFnSc6	Akihabara
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
koníčky	koníček	k1gInPc1	koníček
jsou	být	k5eAaImIp3nP	být
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
hry	hra	k1gFnPc1	hra
a	a	k8xC	a
japonské	japonský	k2eAgFnPc1d1	japonská
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
idoly	idol	k1gInPc1	idol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Subkultura	subkultura	k1gFnSc1	subkultura
otaku	otak	k1gInSc2	otak
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
proud	proud	k1gInSc4	proud
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
anime	animat	k5eAaPmIp3nS	animat
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
otaku	otaku	k6eAd1	otaku
nejvíce	nejvíce	k6eAd1	nejvíce
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dódžinši	dódžinše	k1gFnSc4	dódžinše
(	(	kIx(	(
<g/>
dódžinši	dódžinše	k1gFnSc4	dódžinše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
publikovaná	publikovaný	k2eAgNnPc1d1	publikované
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
místní	místní	k2eAgFnSc6d1	místní
otaku	otaku	k6eAd1	otaku
subkulturou	subkultura	k1gFnSc7	subkultura
než	než	k8xS	než
manga	mango	k1gNnPc4	mango
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
čtoucí	čtoucí	k2eAgNnSc1d1	čtoucí
mangu	mango	k1gNnSc3	mango
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
většinou	většina	k1gFnSc7	většina
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
otaku	otaka	k1gFnSc4	otaka
subkulturu	subkultura	k1gFnSc4	subkultura
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
čtení	čtení	k1gNnSc1	čtení
mangy	mango	k1gNnPc7	mango
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
více	hodně	k6eAd2	hodně
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
převzatý	převzatý	k2eAgInSc1d1	převzatý
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
kontextu	kontext	k1gInSc6	kontext
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
konkrétně	konkrétně	k6eAd1	konkrétně
s	s	k7c7	s
fanouškem	fanoušek	k1gMnSc7	fanoušek
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
má	mít	k5eAaImIp3nS	mít
obdobný	obdobný	k2eAgInSc4d1	obdobný
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Trekkie	Trekkie	k1gFnSc1	Trekkie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
používání	používání	k1gNnSc1	používání
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
sváru	svár	k1gInSc2	svár
mezi	mezi	k7c7	mezi
staršími	starší	k1gMnPc7	starší
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
umírněnými	umírněný	k2eAgFnPc7d1	umírněná
anime	aniit	k5eAaImRp1nP	aniit
nadšenci	nadšenec	k1gMnPc7	nadšenec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
slovům	slovo	k1gNnPc3	slovo
s	s	k7c7	s
negativním	negativní	k2eAgNnSc7d1	negativní
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
preferují	preferovat	k5eAaImIp3nP	preferovat
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
otaku	otaku	k6eAd1	otaku
<g/>
"	"	kIx"	"
slovo	slovo	k1gNnSc1	slovo
wotaku	wotak	k1gInSc2	wotak
(	(	kIx(	(
<g/>
ヲ	ヲ	k?	ヲ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
definice	definice	k1gFnSc1	definice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
japonských	japonský	k2eAgInPc6d1	japonský
fórech	fór	k1gInPc6	fór
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
2	[number]	k4	2
<g/>
channel	channela	k1gFnPc2	channela
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
výrazy	výraz	k1gInPc7	výraz
otaku	otaku	k6eAd1	otaku
(	(	kIx(	(
<g/>
オ	オ	k?	オ
<g/>
)	)	kIx)	)
a	a	k8xC	a
wotaku	wotak	k1gInSc2	wotak
(	(	kIx(	(
<g/>
ヲ	ヲ	k?	ヲ
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
náladě	nálada	k1gFnSc6	nálada
nebo	nebo	k8xC	nebo
osobním	osobní	k2eAgInSc6d1	osobní
postoji	postoj	k1gInSc6	postoj
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
nadšenci	nadšenec	k1gMnPc7	nadšenec
pro	pro	k7c4	pro
anime	anim	k1gInSc5	anim
a	a	k8xC	a
manga	mango	k1gNnPc4	mango
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
známý	známý	k2eAgInSc1d1	známý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepříliš	příliš	k6eNd1	příliš
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
otaku	otaku	k6eAd1	otaku
tvoří	tvořit	k5eAaImIp3nP	tvořit
úctyhodnou	úctyhodný	k2eAgFnSc4d1	úctyhodná
část	část	k1gFnSc4	část
tvůrčích	tvůrčí	k2eAgFnPc2d1	tvůrčí
sil	síla	k1gFnPc2	síla
stojících	stojící	k2eAgFnPc2d1	stojící
za	za	k7c4	za
anime	aniit	k5eAaImRp1nP	aniit
a	a	k8xC	a
mangou	manga	k1gFnSc7	manga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
i	i	k9	i
několik	několik	k4yIc1	několik
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
tematicky	tematicky	k6eAd1	tematicky
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
samotných	samotný	k2eAgInPc2d1	samotný
otaku	otaku	k6eAd1	otaku
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
často	často	k6eAd1	často
o	o	k7c4	o
nadlehčené	nadlehčený	k2eAgFnPc4d1	nadlehčená
napodobeniny	napodobenina	k1gFnPc4	napodobenina
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Otaku	Otaka	k1gMnSc4	Otaka
no	no	k9	no
Video	video	k1gNnSc1	video
(	(	kIx(	(
<g/>
お	お	k?	お
Otaku	Otak	k1gInSc2	Otak
no	no	k9	no
Bideo	Bideo	k6eAd1	Bideo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dvojice	dvojice	k1gFnSc1	dvojice
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
seznamování	seznamování	k1gNnSc6	seznamování
vysokoškolského	vysokoškolský	k2eAgMnSc2d1	vysokoškolský
studenta	student	k1gMnSc2	student
se	s	k7c7	s
světem	svět	k1gInSc7	svět
otaku	otaku	k6eAd1	otaku
díky	díky	k7c3	díky
středoškolskému	středoškolský	k2eAgMnSc3d1	středoškolský
kamarádovi	kamarád	k1gMnSc3	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
největším	veliký	k2eAgInPc3d3	veliký
otaku	otak	k1gInSc2	otak
<g/>
.	.	kIx.	.
</s>
<s>
Otaku	Otaku	k6eAd1	Otaku
no	no	k9	no
Video	video	k1gNnSc1	video
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
japonském	japonský	k2eAgNnSc6d1	Japonské
televizním	televizní	k2eAgNnSc6d1	televizní
studiu	studio	k1gNnSc6	studio
Gainax	Gainax	k1gInSc1	Gainax
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
naživo	naživo	k1gNnSc4	naživo
natočených	natočený	k2eAgInPc2d1	natočený
parodujících	parodující	k2eAgInPc2d1	parodující
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
otaku	otak	k1gInSc2	otak
<g/>
.	.	kIx.	.
</s>
<s>
Comic	Comic	k1gMnSc1	Comic
Party	parta	k1gFnSc2	parta
(	(	kIx(	(
<g/>
こ	こ	k?	こ
Komikku	Komikk	k1gInSc2	Komikk
Pátí	pátý	k4xOgMnPc1	pátý
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Původně	původně	k6eAd1	původně
série	série	k1gFnSc1	série
herních	herní	k2eAgFnPc2d1	herní
simulací	simulace	k1gFnPc2	simulace
schůzek	schůzka	k1gFnPc2	schůzka
byla	být	k5eAaImAgFnS	být
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
manga	mango	k1gNnSc2	mango
sérií	série	k1gFnPc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Comic	Comic	k1gMnSc1	Comic
Party	parta	k1gFnSc2	parta
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
odmítnutém	odmítnutý	k2eAgMnSc6d1	odmítnutý
studentovi	student	k1gMnSc6	student
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nadšeně	nadšeně	k6eAd1	nadšeně
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
na	na	k7c4	na
dódžinši	dódžinše	k1gFnSc4	dódžinše
scénu	scéna	k1gFnSc4	scéna
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bláznivým	bláznivý	k2eAgMnSc7d1	bláznivý
kamarádem	kamarád	k1gMnSc7	kamarád
-	-	kIx~	-
otaku	otak	k1gInSc2	otak
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
několik	několik	k4yIc1	několik
vlastních	vlastní	k2eAgMnPc2d1	vlastní
dódžinši	dódžinsat	k5eAaPmIp1nSwK	dódžinsat
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
s	s	k7c7	s
pracemi	práce	k1gFnPc7	práce
ostatních	ostatní	k2eAgMnPc2d1	ostatní
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
a	a	k8xC	a
vypořádá	vypořádat	k5eAaPmIp3nS	vypořádat
se	se	k3xPyFc4	se
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
méně	málo	k6eAd2	málo
než	než	k8xS	než
nadšená	nadšený	k2eAgFnSc1d1	nadšená
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnPc1d1	objevená
vášně	vášeň	k1gFnPc1	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Genshiken	Genshiken	k1gInSc1	Genshiken
(	(	kIx(	(
<g/>
げ	げ	k?	げ
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
otaku	otak	k1gInSc2	otak
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
osudů	osud	k1gInPc2	osud
dvou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
-	-	kIx~	-
nováčka	nováček	k1gMnSc2	nováček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
otaku	otaku	k6eAd1	otaku
a	a	k8xC	a
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
jiného	jiný	k2eAgMnSc2d1	jiný
člena	člen	k1gMnSc2	člen
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neschvaluje	schvalovat	k5eNaImIp3nS	schvalovat
vášeň	vášeň	k1gFnSc4	vášeň
jejího	její	k3xOp3gNnSc2	její
pohledného	pohledný	k2eAgNnSc2d1	pohledné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hloupého	hloupý	k2eAgNnSc2d1	hloupé
<g/>
,	,	kIx,	,
otaku	otaku	k6eAd1	otaku
přítele	přítel	k1gMnSc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Densha	Densha	k1gFnSc1	Densha
Otoko	Otoko	k1gNnSc4	Otoko
(	(	kIx(	(
<g/>
電	電	k?	電
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
Vlakový	vlakový	k2eAgMnSc1d1	vlakový
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Denša	Denša	k1gMnSc1	Denša
Otoko	Otoko	k1gNnSc4	Otoko
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
natočeno	natočit	k5eAaBmNgNnS	natočit
podle	podle	k7c2	podle
skutečného	skutečný	k2eAgInSc2d1	skutečný
příběhu	příběh	k1gInSc2	příběh
japonského	japonský	k2eAgMnSc2d1	japonský
geeka	geeek	k1gMnSc2	geeek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zachrání	zachránit	k5eAaPmIp3nS	zachránit
pohlednou	pohledný	k2eAgFnSc4d1	pohledná
ženu	žena	k1gFnSc4	žena
(	(	kIx(	(
<g/>
OL	Ola	k1gFnPc2	Ola
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
office	office	k1gFnSc1	office
lady	lady	k1gFnSc2	lady
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hermè	Hermè	k1gMnSc1	Hermè
<g/>
,	,	kIx,	,
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
opilce	opilec	k1gMnSc2	opilec
na	na	k7c6	na
vlakové	vlakový	k2eAgFnSc6d1	vlaková
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
ho	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
pozve	pozvat	k5eAaPmIp3nS	pozvat
na	na	k7c4	na
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
NHK	NHK	kA	NHK
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
・	・	k?	・
<g/>
H	H	kA	H
<g/>
・	・	k?	・
<g/>
Kに	Kに	k1gMnSc1	Kに
<g/>
!	!	kIx.	!
</s>
<s desamb="1">
NHK	NHK	kA	NHK
ni	on	k3xPp3gFnSc4	on
Jókoso	Jókosa	k1gFnSc5	Jókosa
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
později	pozdě	k6eAd2	pozdě
vycházela	vycházet	k5eAaImAgFnS	vycházet
i	i	k8xC	i
jako	jako	k9	jako
manga	mango	k1gNnSc2	mango
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
hikikomori	hikikomor	k1gFnSc6	hikikomor
<g/>
,	,	kIx,	,
dívce	dívka	k1gFnSc6	dívka
toužící	toužící	k2eAgFnSc2d1	toužící
mu	on	k3xPp3gMnSc3	on
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
sousedovi-otaku	sousedovitak	k1gInSc2	sousedovi-otak
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gMnSc7	jeho
bývalým	bývalý	k2eAgMnSc7d1	bývalý
spolužákem	spolužák	k1gMnSc7	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
mnoho	mnoho	k6eAd1	mnoho
otaku	otaka	k1gFnSc4	otaka
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
která	který	k3yRgNnPc4	který
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
lolicon	lolicon	k1gInSc4	lolicon
<g/>
,	,	kIx,	,
moé	moé	k?	moé
a	a	k8xC	a
dódžin	dódžin	k2eAgInSc1d1	dódžin
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Metal	metat	k5eAaImAgMnS	metat
Gear	Gear	k1gMnSc1	Gear
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Otaku	Otak	k1gInSc2	Otak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Konata	Konata	k1gFnSc1	Konata
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
český	český	k2eAgInSc1d1	český
novinkový	novinkový	k2eAgInSc1d1	novinkový
portál	portál	k1gInSc1	portál
o	o	k7c6	o
otaku	otak	k1gInSc6	otak
yukata	yukata	k1gFnSc1	yukata
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
novinkový	novinkový	k2eAgInSc1d1	novinkový
portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
otaku	otaka	k1gFnSc4	otaka
chill	chilla	k1gFnPc2	chilla
<g/>
.	.	kIx.	.
<g/>
sekai	sekai	k1gNnSc2	sekai
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
recenze	recenze	k1gFnPc4	recenze
<g/>
,	,	kIx,	,
občasné	občasný	k2eAgInPc4d1	občasný
překlady	překlad	k1gInPc4	překlad
anime	animat	k5eAaPmIp3nS	animat
&	&	k?	&
mangy	mango	k1gNnPc7	mango
shirai	shirai	k1gNnSc1	shirai
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
otaku	otaku	k6eAd1	otaku
portál	portál	k1gInSc1	portál
a	a	k8xC	a
server	server	k1gInSc1	server
s	s	k7c7	s
Anime	Anim	k1gInSc5	Anim
online	onlinout	k5eAaPmIp3nS	onlinout
však	však	k9	však
bez	bez	k7c2	bez
Mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Akihabara	Akihabara	k1gFnSc1	Akihabara
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
databáze	databáze	k1gFnSc1	databáze
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
mangy	mango	k1gNnPc7	mango
a	a	k8xC	a
doramy	doram	k1gInPc1	doram
s	s	k7c7	s
titulky	titulek	k1gInPc7	titulek
<g/>
,	,	kIx,	,
komentáři	komentář	k1gInPc7	komentář
a	a	k8xC	a
odkazy	odkaz	k1gInPc7	odkaz
na	na	k7c4	na
zdroje	zdroj	k1gInPc4	zdroj
HNS	HNS	kA	HNS
<g/>
.	.	kIx.	.
<g/>
moe	moe	k?	moe
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
anime	animat	k5eAaPmIp3nS	animat
portál	portál	k1gInSc1	portál
se	s	k7c7	s
streamem	stream	k1gInSc7	stream
nejrůznějších	různý	k2eAgMnPc2d3	nejrůznější
anime	animat	k5eAaPmIp3nS	animat
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
alone	alonout	k5eAaImIp3nS	alonout
<g/>
,	,	kIx,	,
but	but	k?	but
not	nota	k1gFnPc2	nota
lonely	lonela	k1gFnSc2	lonela
-	-	kIx~	-
anglický	anglický	k2eAgInSc1d1	anglický
článek	článek	k1gInSc1	článek
o	o	k7c6	o
japonských	japonský	k2eAgFnPc6d1	japonská
otaku	otaku	k6eAd1	otaku
dnes	dnes	k6eAd1	dnes
Politika	politika	k1gFnSc1	politika
otaku	otak	k1gInSc2	otak
Urbandictionary	Urbandictionara	k1gFnSc2	Urbandictionara
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
definice	definice	k1gFnSc1	definice
otaku	otaku	k6eAd1	otaku
na	na	k7c6	na
Urban	Urban	k1gMnSc1	Urban
Dictionary	Dictionara	k1gFnSc2	Dictionara
Akihabara	Akihabara	k1gFnSc1	Akihabara
-	-	kIx~	-
Anglická	anglický	k2eAgFnSc1d1	anglická
komunita	komunita	k1gFnSc1	komunita
otaku	otaku	k6eAd1	otaku
Amatérská	amatérský	k2eAgNnPc4d1	amatérské
manga	mango	k1gNnPc4	mango
subkultura	subkultura	k1gFnSc1	subkultura
Anglický	anglický	k2eAgInSc1d1	anglický
seznam	seznam	k1gInSc1	seznam
termínů	termín	k1gInPc2	termín
užívaných	užívaný	k2eAgInPc2d1	užívaný
v	v	k7c6	v
otaku	otak	k1gInSc6	otak
hantýrce	hantýrka	k1gFnSc3	hantýrka
</s>
