<s desamb="1">
Pléd	pléd	k1gInSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
typický	typický	k2eAgInSc1d1
pro	pro	k7c4
Skotsko	Skotsko	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
barevnými	barevný	k2eAgInPc7d1
čtvercovými	čtvercový	k2eAgInPc7d1
vzory	vzor	k1gInPc7
(	(	kIx(
<g/>
Tartan	tartan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozšířený	rozšířený	k2eAgInSc1d1
se	se	k3xPyFc4
ale	ale	k9
stal	stát	k5eAaPmAgInS
i	i	k9
pléd	pléd	k1gInSc1
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
místo	místo	k7c2
ovčí	ovčí	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
zpracovává	zpracovávat	k5eAaImIp3nS
vlna	vlna	k1gFnSc1
z	z	k7c2
alpak	alpaka	k1gFnPc2
<g/>
.	.	kIx.
</s>