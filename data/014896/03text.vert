<s>
Pléd	pléd	k1gInSc1
</s>
<s>
Pléd	pléd	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
Textilie	textilie	k1gFnSc1
</s>
<s>
Tkanina	tkanina	k1gFnSc1
z	z	k7c2
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tkají	tkát	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
sukně	sukně	k1gFnPc4
<g/>
,	,	kIx,
košile	košile	k1gFnPc4
<g/>
,	,	kIx,
kalhoty	kalhoty	k1gFnPc4
<g/>
,	,	kIx,
šátky	šátek	k1gInPc4
a	a	k8xC
další	další	k2eAgNnSc4d1
oblečení	oblečení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pléd	pléd	k1gInSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
typický	typický	k2eAgInSc1d1
pro	pro	k7c4
Skotsko	Skotsko	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
barevnými	barevný	k2eAgInPc7d1
čtvercovými	čtvercový	k2eAgInPc7d1
vzory	vzor	k1gInPc7
(	(	kIx(
<g/>
Tartan	tartan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozšířený	rozšířený	k2eAgInSc1d1
se	se	k3xPyFc4
ale	ale	k9
stal	stát	k5eAaPmAgInS
i	i	k9
pléd	pléd	k1gInSc1
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
místo	místo	k7c2
ovčí	ovčí	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
zpracovává	zpracovávat	k5eAaImIp3nS
vlna	vlna	k1gFnSc1
z	z	k7c2
alpak	alpaka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Módní	módní	k2eAgInSc1d1
doplněk	doplněk	k1gInSc1
</s>
<s>
Látkový	látkový	k2eAgInSc1d1
přehoz	přehoz	k1gInSc1
<g/>
,	,	kIx,
nošený	nošený	k2eAgInSc1d1
ženami	žena	k1gFnPc7
jako	jako	k8xC,k8xS
módní	módní	k2eAgInSc1d1
doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
praktickou	praktický	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buďto	buďto	k8xC
patřil	patřit	k5eAaImAgInS
jako	jako	k9
součást	součást	k1gFnSc4
ke	k	k7c3
kabátům	kabát	k1gInPc3
nebo	nebo	k8xC
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
kombinovat	kombinovat	k5eAaImF
jako	jako	k9
samostatný	samostatný	k2eAgInSc4d1
přehoz	přehoz	k1gInSc4
(	(	kIx(
<g/>
u	u	k7c2
ženského	ženský	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Látkový	látkový	k2eAgInSc4d1
nebo	nebo	k8xC
pletený	pletený	k2eAgInSc4d1
<g/>
,	,	kIx,
nošený	nošený	k2eAgInSc4d1
namísto	namísto	k7c2
šály	šála	k1gFnSc2
nebo	nebo	k8xC
šátku	šátek	k1gInSc2
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgInS
mít	mít	k5eAaImF
i	i	k9
knoflíky	knoflík	k1gInPc4
pro	pro	k7c4
zapnutí	zapnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Plný	plný	k2eAgInSc1d1
pléd	pléd	k1gInSc1
</s>
<s>
Plný	plný	k2eAgInSc1d1
pléd	pléd	k1gInSc1
(	(	kIx(
<g/>
full	full	k1gInSc1
plaid	plaid	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součást	součást	k1gFnSc1
skotského	skotský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
kroje	kroj	k1gInSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
připomínající	připomínající	k2eAgFnSc4d1
rozměrnější	rozměrný	k2eAgFnSc4d2
šálu	šála	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Žena	žena	k1gFnSc1
s	s	k7c7
látkovým	látkový	k2eAgInSc7d1
plédem	pléd	k1gInSc7
-	-	kIx~
přehozem	přehoz	k1gInSc7
</s>
<s>
Pléd	pléd	k1gInSc1
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
pláště	plášť	k1gInSc2
v	v	k7c6
módním	módní	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1897	#num#	k4
</s>
<s>
Skotský	skotský	k2eAgInSc1d1
dudák	dudák	k1gInSc1
s	s	k7c7
tartanovým	tartanový	k2eAgInSc7d1
plédem	pléd	k1gInSc7
</s>
<s>
Sir	sir	k1gMnSc1
John	John	k1gMnSc1
Sinclair	Sinclair	k1gMnSc1
s	s	k7c7
plédem	pléd	k1gInSc7
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
v	v	k7c6
kroji	kroj	k1gInSc6
z	z	k7c2
plédu	pléd	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Přikrývka	přikrývka	k1gFnSc1
nebo	nebo	k8xC
doplněk	doplněk	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Objevte	objevit	k5eAaPmRp2nP
kouzlo	kouzlo	k1gNnSc4
plédů	pléd	k1gInPc2
právě	právě	k9
teď	teď	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
