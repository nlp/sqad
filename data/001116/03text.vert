<s>
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Sympatizoval	sympatizovat	k5eAaImAgMnS	sympatizovat
s	s	k7c7	s
májovci	májovec	k1gMnPc7	májovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepatřil	patřit	k5eNaImAgMnS	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tvořil	tvořit	k5eAaImAgInS	tvořit
za	za	k7c2	za
jiných	jiný	k2eAgFnPc2d1	jiná
okolností	okolnost	k1gFnPc2	okolnost
než	než	k8xS	než
oni	onen	k3xDgMnPc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
ševcovského	ševcovský	k2eAgMnSc2d1	ševcovský
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Arbese	Arbes	k1gMnSc2	Arbes
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
a	a	k8xC	a
manželky	manželka	k1gFnPc1	manželka
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Winklerové	Winklerová	k1gFnSc2	Winklerová
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
-	-	kIx~	-
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třetí	třetí	k4xOgFnSc1	třetí
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgMnS	být
součástí	součást	k1gFnSc7	součást
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
strávil	strávit	k5eAaPmAgMnS	strávit
celé	celý	k2eAgNnSc4d1	celé
mládí	mládí	k1gNnSc4	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1851	[number]	k4	1851
až	až	k9	až
1854	[number]	k4	1854
do	do	k7c2	do
farní	farní	k2eAgFnSc2d1	farní
maltézské	maltézský	k2eAgFnSc2d1	Maltézská
školy	škola	k1gFnSc2	škola
u	u	k7c2	u
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
učit	učit	k5eAaImF	učit
obuvnictví	obuvnictví	k1gNnSc4	obuvnictví
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
však	však	k9	však
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc4d1	dobrá
známky	známka	k1gFnPc4	známka
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
rodiče	rodič	k1gMnPc1	rodič
poslali	poslat	k5eAaPmAgMnP	poslat
na	na	k7c4	na
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
na	na	k7c6	na
nižší	nízký	k2eAgFnSc6d2	nižší
reálce	reálka	k1gFnSc6	reálka
u	u	k7c2	u
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Juliem	Julius	k1gMnSc7	Julius
Zeyerem	Zeyer	k1gMnSc7	Zeyer
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
novoměstské	novoměstský	k2eAgFnSc6d1	Novoměstská
vyšší	vysoký	k2eAgFnSc6d2	vyšší
německé	německý	k2eAgFnSc6d1	německá
reálce	reálka	k1gFnSc6	reálka
v	v	k7c6	v
Mikulandské	Mikulandský	k2eAgFnSc6d1	Mikulandská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poznal	poznat	k5eAaPmAgMnS	poznat
Jana	Jan	k1gMnSc4	Jan
Nerudu	Neruda	k1gMnSc4	Neruda
jako	jako	k8xS	jako
učitele	učitel	k1gMnSc4	učitel
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
sice	sice	k8xC	sice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
polytechniku	polytechnika	k1gFnSc4	polytechnika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jiným	jiný	k2eAgInPc3d1	jiný
oborům	obor	k1gInPc3	obor
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
žurnalistou	žurnalista	k1gMnSc7	žurnalista
a	a	k8xC	a
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1867	[number]	k4	1867
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Josefinou	Josefin	k2eAgFnSc7d1	Josefina
(	(	kIx(	(
<g/>
Josefou	Josefa	k1gFnSc7	Josefa
<g/>
)	)	kIx)	)
Rabochovou	Rabochův	k2eAgFnSc7d1	Rabochův
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
soustružníka	soustružník	k1gMnSc2	soustružník
Jana	Jan	k1gMnSc2	Jan
Rabocha	Raboch	k1gMnSc2	Raboch
<g/>
,	,	kIx,	,
S	s	k7c7	s
Josefinou	Josefin	k2eAgFnSc7d1	Josefina
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
dceru	dcera	k1gFnSc4	dcera
(	(	kIx(	(
<g/>
narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
týden	týden	k1gInSc1	týden
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
dcera	dcera	k1gFnSc1	dcera
však	však	k9	však
brzy	brzy	k6eAd1	brzy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Olga	Olga	k1gFnSc1	Olga
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
o	o	k7c4	o
Jakuba	Jakub	k1gMnSc4	Jakub
Arbese	Arbes	k1gMnSc4	Arbes
pečovala	pečovat	k5eAaImAgFnS	pečovat
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
literární	literární	k2eAgFnSc4d1	literární
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Arbes	Arbes	k1gMnSc1	Arbes
ztratil	ztratit	k5eAaPmAgMnS	ztratit
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
diktoval	diktovat	k5eAaImAgInS	diktovat
dceři	dcera	k1gFnSc3	dcera
Olze	Olga	k1gFnSc3	Olga
své	svůj	k3xOyFgInPc4	svůj
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
dcera	dcera	k1gFnSc1	dcera
Polyxena	Polyxena	k1gFnSc1	Polyxena
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
??	??	k?	??
<g/>
)	)	kIx)	)
žila	žít	k5eAaImAgFnS	žít
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Edgar	Edgar	k1gMnSc1	Edgar
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jedenáctiletý	jedenáctiletý	k2eAgMnSc1d1	jedenáctiletý
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
časopisu	časopis	k1gInSc2	časopis
Hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
jiného	jiný	k2eAgInSc2d1	jiný
časopisu	časopis	k1gInSc2	časopis
Vesna	Vesna	k1gFnSc1	Vesna
kutnohorská	kutnohorský	k2eAgFnSc1d1	Kutnohorská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
redaktorem	redaktor	k1gMnSc7	redaktor
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
do	do	k7c2	do
r.	r.	kA	r.
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
titul	titul	k1gInSc1	titul
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Naše	naši	k1gMnPc4	naši
listy	lista	k1gFnSc2	lista
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
psal	psát	k5eAaImAgMnS	psát
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
jiných	jiný	k2eAgInPc2d1	jiný
listů	list	k1gInPc2	list
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
články	článek	k1gInPc4	článek
a	a	k8xC	a
vedení	vedení	k1gNnSc4	vedení
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
byl	být	k5eAaImAgInS	být
německou	německý	k2eAgFnSc7d1	německá
porotou	porota	k1gFnSc7	porota
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
15	[number]	k4	15
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
strávil	strávit	k5eAaPmAgInS	strávit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Lípě	lípa	k1gFnSc6	lípa
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
až	až	k9	až
do	do	k7c2	do
výpovědi	výpověď	k1gFnSc2	výpověď
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
jako	jako	k9	jako
značnou	značný	k2eAgFnSc4d1	značná
osobní	osobní	k2eAgFnSc4d1	osobní
křivdu	křivda	k1gFnSc4	křivda
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vydavatele	vydavatel	k1gMnSc2	vydavatel
Julia	Julius	k1gMnSc2	Julius
Grégra	Grégr	k1gMnSc2	Grégr
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
pobyl	pobýt	k5eAaPmAgMnS	pobýt
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
staročeského	staročeský	k2eAgInSc2d1	staročeský
časopisu	časopis	k1gInSc2	časopis
Politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
i	i	k9	i
odtud	odtud	k6eAd1	odtud
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1879	[number]	k4	1879
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
dramaturg	dramaturg	k1gMnSc1	dramaturg
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
ztratil	ztratit	k5eAaPmAgMnS	ztratit
a	a	k8xC	a
už	už	k6eAd1	už
natrvalo	natrvalo	k6eAd1	natrvalo
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
svobodný	svobodný	k2eAgMnSc1d1	svobodný
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
okruhu	okruh	k1gInSc3	okruh
satirických	satirický	k2eAgMnPc2d1	satirický
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mikolášem	Mikoláš	k1gMnSc7	Mikoláš
Alšem	Aleš	k1gMnSc7	Aleš
vydával	vydávat	k5eAaImAgInS	vydávat
satirický	satirický	k2eAgInSc1d1	satirický
časopis	časopis	k1gInSc1	časopis
Šotek	šotek	k1gMnSc1	šotek
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobných	obdobný	k2eAgInPc2d1	obdobný
časopisů	časopis	k1gInPc2	časopis
založil	založit	k5eAaPmAgMnS	založit
několik	několik	k4yIc4	několik
<g/>
:	:	kIx,	:
Volné	volný	k2eAgNnSc1d1	volné
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc1	hlas
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
,	,	kIx,	,
Věstník	věstník	k1gMnSc1	věstník
Jednoty	jednota	k1gFnSc2	jednota
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
Slovenský	slovenský	k2eAgInSc1d1	slovenský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
psát	psát	k5eAaImF	psát
veřejně	veřejně	k6eAd1	veřejně
nesměl	smět	k5eNaImAgMnS	smět
a	a	k8xC	a
nacházel	nacházet	k5eAaImAgMnS	nacházet
útočiště	útočiště	k1gNnSc4	útočiště
ve	v	k7c6	v
stolní	stolní	k2eAgFnSc6d1	stolní
společnosti	společnost	k1gFnSc6	společnost
Mahábhárata	Mahábhárata	k1gFnSc1	Mahábhárata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
literární	literární	k2eAgFnSc2d1	literární
činnosti	činnost	k1gFnSc2	činnost
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
velice	velice	k6eAd1	velice
oddaný	oddaný	k2eAgInSc4d1	oddaný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Nerudovi	Neruda	k1gMnSc3	Neruda
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
vzorem	vzor	k1gInSc7	vzor
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
choval	chovat	k5eAaImAgMnS	chovat
obrovský	obrovský	k2eAgInSc4d1	obrovský
obdiv	obdiv	k1gInSc4	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jej	on	k3xPp3gNnSc4	on
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
bohaté	bohatý	k2eAgFnSc6d1	bohatá
literární	literární	k2eAgFnSc6d1	literární
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozluštil	rozluštit	k5eAaPmAgInS	rozluštit
deník	deník	k1gInSc1	deník
K.	K.	kA	K.
H.	H.	kA	H.
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
popisoval	popisovat	k5eAaImAgMnS	popisovat
jeho	jeho	k3xOp3gFnSc4	jeho
sexuální	sexuální	k2eAgFnPc1d1	sexuální
hrátky	hrátky	k1gFnPc1	hrátky
s	s	k7c7	s
milenkou	milenka	k1gFnSc7	milenka
Lory	Lora	k1gFnSc2	Lora
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
jeho	jeho	k3xOp3gInSc1	jeho
sexuální	sexuální	k2eAgInSc1d1	sexuální
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Volil	volit	k5eAaImAgInS	volit
náměty	námět	k1gInPc7	námět
ze	z	k7c2	z
současného	současný	k2eAgInSc2d1	současný
pražského	pražský	k2eAgInSc2d1	pražský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
svérázný	svérázný	k2eAgMnSc1d1	svérázný
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
vypadají	vypadat	k5eAaPmIp3nP	vypadat
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
jeho	jeho	k3xOp3gNnPc4	jeho
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
-	-	kIx~	-
romaneto	romaneto	k1gNnSc1	romaneto
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obsáhlejší	obsáhlý	k2eAgFnSc1d2	obsáhlejší
povídka	povídka	k1gFnSc1	povídka
s	s	k7c7	s
dobrodružným	dobrodružný	k2eAgInSc7d1	dobrodružný
<g/>
,	,	kIx,	,
napínavým	napínavý	k2eAgInSc7d1	napínavý
dějem	děj	k1gInSc7	děj
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dramatické	dramatický	k2eAgInPc1d1	dramatický
zápletky	zápletek	k1gInPc1	zápletek
s	s	k7c7	s
překvapivým	překvapivý	k2eAgNnSc7d1	překvapivé
zakončením	zakončení	k1gNnSc7	zakončení
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
jsou	být	k5eAaImIp3nP	být
neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
tajemnost	tajemnost	k1gFnSc1	tajemnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
najde	najít	k5eAaPmIp3nS	najít
racionální	racionální	k2eAgNnSc4d1	racionální
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rozporu	rozpor	k1gInSc2	rozpor
mezi	mezi	k7c7	mezi
dokázanými	dokázaný	k2eAgFnPc7d1	dokázaná
věcmi	věc	k1gFnPc7	věc
a	a	k8xC	a
věcmi	věc	k1gFnPc7	věc
mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
vědci	vědec	k1gMnPc1	vědec
nebo	nebo	k8xC	nebo
studenti	student	k1gMnPc1	student
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
<g/>
.	.	kIx.	.
</s>
<s>
Publicista	publicista	k1gMnSc1	publicista
David	David	k1gMnSc1	David
Gross	Gross	k1gMnSc1	Gross
ho	on	k3xPp3gInSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc4d3	veliký
český	český	k2eAgInSc4d1	český
román	román	k1gInSc4	román
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prvky	prvek	k1gInPc4	prvek
fantastiky	fantastika	k1gFnSc2	fantastika
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
Poslední	poslední	k2eAgMnPc1d1	poslední
dnové	den	k1gMnPc1	den
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Arbes	Arbes	k1gMnSc1	Arbes
řazen	řadit	k5eAaImNgMnS	řadit
i	i	k9	i
mezi	mezi	k7c4	mezi
autory	autor	k1gMnPc4	autor
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zásluhy	zásluha	k1gFnPc1	zásluha
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
byly	být	k5eAaImAgInP	být
oceněny	ocenit	k5eAaPmNgInP	ocenit
členstvím	členství	k1gNnSc7	členství
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
dopisujícím	dopisující	k2eAgInSc7d1	dopisující
členem	člen	k1gInSc7	člen
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
řádným	řádný	k2eAgMnSc7d1	řádný
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
zemřel	zemřít	k5eAaPmAgMnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1914	[number]	k4	1914
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
součástí	součást	k1gFnSc7	součást
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Malvazinkách	malvazinka	k1gFnPc6	malvazinka
<g/>
.	.	kIx.	.
</s>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
na	na	k7c6	na
skřipci	skřipec	k1gInSc6	skřipec
Elegie	elegie	k1gFnSc2	elegie
o	o	k7c6	o
černých	černý	k2eAgNnPc6d1	černé
očích	oko	k1gNnPc6	oko
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
-	-	kIx~	-
Autor	autor	k1gMnSc1	autor
(	(	kIx(	(
<g/>
Arbes	Arbes	k1gMnSc1	Arbes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
vypravěčem	vypravěč	k1gMnSc7	vypravěč
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Omylem	omylem	k6eAd1	omylem
je	být	k5eAaImIp3nS	být
zavřen	zavřen	k2eAgInSc1d1	zavřen
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
mladíkem	mladík	k1gInSc7	mladík
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
Xaverius	Xaverius	k1gMnSc1	Xaverius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
též	též	k9	též
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
rozluštit	rozluštit	k5eAaPmF	rozluštit
tajemství	tajemství	k1gNnSc1	tajemství
obrazu	obraz	k1gInSc2	obraz
sv.	sv.	kA	sv.
Xaveria	Xaverius	k1gMnSc2	Xaverius
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
sestavit	sestavit	k5eAaPmF	sestavit
mapku	mapka	k1gFnSc4	mapka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
hledat	hledat	k5eAaImF	hledat
poklad	poklad	k1gInSc4	poklad
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
Smíchov	Smíchov	k1gInSc4	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Xaverius	Xaverius	k1gMnSc1	Xaverius
ale	ale	k8xC	ale
ve	v	k7c6	v
zmatku	zmatek	k1gInSc6	zmatek
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
hledání	hledání	k1gNnSc2	hledání
pokladu	poklad	k1gInSc2	poklad
utíká	utíkat	k5eAaImIp3nS	utíkat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Xaverius	Xaverius	k1gMnSc1	Xaverius
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Sivooký	sivooký	k2eAgMnSc1d1	sivooký
démon	démon	k1gMnSc1	démon
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
Zázračná	zázračný	k2eAgFnSc1d1	zázračná
madona	madona	k1gFnSc1	madona
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
Ukřižovaná	ukřižovaný	k2eAgFnSc1d1	ukřižovaná
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kamarád	kamarád	k1gMnSc1	kamarád
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
války	válka	k1gFnSc2	válka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
má	mít	k5eAaImIp3nS	mít
potom	potom	k6eAd1	potom
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
že	že	k8xS	že
kamarád	kamarád	k1gMnSc1	kamarád
dostal	dostat	k5eAaPmAgMnS	dostat
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
stroj	stroj	k1gInSc4	stroj
času	čas	k1gInSc2	čas
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dějiny	dějiny	k1gFnPc1	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
jsou	být	k5eAaImIp3nP	být
dějinami	dějiny	k1gFnPc7	dějiny
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Protiválečné	protiválečný	k2eAgNnSc1d1	protiválečné
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Akrobati	akrobat	k1gMnPc1	akrobat
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
-	-	kIx~	-
Líčí	líčit	k5eAaImIp3nS	líčit
příběh	příběh	k1gInSc1	příběh
setkání	setkání	k1gNnSc1	setkání
autora	autor	k1gMnSc2	autor
s	s	k7c7	s
umělci	umělec	k1gMnPc7	umělec
-	-	kIx~	-
akrobaty	akrobat	k1gMnPc7	akrobat
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
vášní	vášeň	k1gFnSc7	vášeň
propadli	propadnout	k5eAaPmAgMnP	propadnout
svému	svůj	k3xOyFgInSc3	svůj
druhu	druh	k1gInSc3	druh
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
staví	stavit	k5eAaPmIp3nS	stavit
jej	on	k3xPp3gMnSc4	on
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
cokoli	cokoli	k3yInSc4	cokoli
jiného	jiný	k2eAgMnSc4d1	jiný
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
toto	tento	k3xDgNnSc1	tento
umění	umění	k1gNnSc1	umění
provozovat	provozovat	k5eAaImF	provozovat
ochotni	ochoten	k2eAgMnPc1d1	ochoten
obětovat	obětovat	k5eAaBmF	obětovat
mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
lilie	lilie	k1gFnSc1	lilie
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
-	-	kIx~	-
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
fantaskní	fantaskní	k2eAgInPc4d1	fantaskní
prvky	prvek	k1gInPc4	prvek
Šílený	šílený	k2eAgMnSc1d1	šílený
Job	Job	k1gMnSc1	Job
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
Můj	můj	k3xOp1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
vrah	vrah	k1gMnSc1	vrah
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
Advokát	advokát	k1gMnSc1	advokát
chuďasů	chuďas	k1gMnPc2	chuďas
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
Divotvorná	divotvorný	k2eAgFnSc1d1	divotvorná
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
Aspoň	aspoň	k9	aspoň
se	se	k3xPyFc4	se
pousměj	pousmát	k5eAaPmRp2nS	pousmát
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
Dva	dva	k4xCgMnPc1	dva
barikádníci	barikádník	k1gMnPc1	barikádník
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
Zborcené	zborcený	k2eAgFnSc2d1	zborcená
harfy	harfa	k1gFnSc2	harfa
tón	tón	k1gInSc1	tón
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc4	první
noc	noc	k1gFnSc4	noc
u	u	k7c2	u
mrtvoly	mrtvola	k1gFnSc2	mrtvola
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
Il	Il	k1gFnSc1	Il
divino	divin	k2eAgNnSc4d1	Divino
bohemo	bohemo	k6eAd1	bohemo
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
Lotr	lotr	k1gMnSc1	lotr
Gólo	Gólo	k1gMnSc1	Gólo
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
a	a	k8xC	a
složitý	složitý	k2eAgInSc4d1	složitý
umělecký	umělecký	k2eAgInSc4d1	umělecký
vývoj	vývoj	k1gInSc4	vývoj
nadaného	nadaný	k2eAgMnSc2d1	nadaný
kočovného	kočovný	k2eAgMnSc2d1	kočovný
herce	herec	k1gMnSc2	herec
Sunkovského	Sunkovský	k2eAgMnSc2d1	Sunkovský
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
ubíjí	ubíjet	k5eAaImIp3nS	ubíjet
nezájem	nezájem	k1gInSc1	nezájem
a	a	k8xC	a
nepochopení	nepochopení	k1gNnSc1	nepochopení
publika	publikum	k1gNnSc2	publikum
i	i	k8xC	i
malá	malý	k2eAgFnSc1d1	malá
víra	víra	k1gFnSc1	víra
ve	v	k7c4	v
vlastní	vlastní	k2eAgFnPc4d1	vlastní
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Medea	Medea	k6eAd1	Medea
s	s	k7c7	s
bezvýrazným	bezvýrazný	k2eAgNnSc7d1	bezvýrazné
okem	oke	k1gNnSc7	oke
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
Duhový	duhový	k2eAgInSc1d1	duhový
bod	bod	k1gInSc1	bod
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
Lilie	lilie	k1gFnSc2	lilie
v	v	k7c6	v
úpalu	úpal	k1gInSc6	úpal
slunečním	sluneční	k2eAgInSc6d1	sluneční
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Duhokřídlá	duhokřídlý	k2eAgFnSc1d1	duhokřídlý
Psýché	Psýché	k1gFnSc1	Psýché
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
V	v	k7c6	v
růžovém	růžový	k2eAgInSc6d1	růžový
rozmaru	rozmar	k1gInSc6	rozmar
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgMnPc1d1	poslední
dnové	den	k1gMnPc1	den
lidstva	lidstvo	k1gNnSc2	lidstvo
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
Vymírající	vymírající	k2eAgInSc1d1	vymírající
hřbitov	hřbitov	k1gInSc1	hřbitov
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
Kandidáti	kandidát	k1gMnPc1	kandidát
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
-	-	kIx~	-
utopie	utopie	k1gFnSc1	utopie
o	o	k7c6	o
socialismu	socialismus	k1gInSc6	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Parta	parta	k1gFnSc1	parta
mladých	mladý	k2eAgMnPc2d1	mladý
mužů	muž	k1gMnPc2	muž
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
sociálně	sociálně	k6eAd1	sociálně
lepším	dobrý	k2eAgInSc6d2	lepší
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zdědí	zdědit	k5eAaPmIp3nP	zdědit
fabriku	fabrika	k1gFnSc4	fabrika
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
kapitalista	kapitalista	k1gMnSc1	kapitalista
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
<g/>
.	.	kIx.	.
</s>
<s>
Adamité	adamita	k1gMnPc1	adamita
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgMnPc1d1	moderní
upíři	upír	k1gMnPc1	upír
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
-	-	kIx~	-
pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
života	život	k1gInSc2	život
pražských	pražský	k2eAgMnPc2d1	pražský
podnikatelů	podnikatel	k1gMnPc2	podnikatel
a	a	k8xC	a
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
Štrajchpudlíci	štrajchpudlík	k1gMnPc1	štrajchpudlík
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Epikurejci	epikurejec	k1gMnPc1	epikurejec
<g/>
,	,	kIx,	,
nedokončeno	dokončen	k2eNgNnSc4d1	nedokončeno
-	-	kIx~	-
román	román	k1gInSc4	román
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
smíchovských	smíchovský	k2eAgMnPc2d1	smíchovský
tiskařských	tiskařský	k2eAgMnPc2d1	tiskařský
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
<g />
.	.	kIx.	.
</s>
<s>
zde	zde	k6eAd1	zde
jejich	jejich	k3xOp3gInPc4	jejich
existenční	existenční	k2eAgInPc4d1	existenční
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
nelidské	lidský	k2eNgInPc4d1	nelidský
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
továrnách	továrna	k1gFnPc6	továrna
Mesiáš	Mesiáš	k1gMnSc1	Mesiáš
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
"	"	kIx"	"
<g/>
ztracené	ztracený	k2eAgFnSc2d1	ztracená
existence	existence	k1gFnSc2	existence
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
revolucionář	revolucionář	k1gMnSc1	revolucionář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
mnohaletého	mnohaletý	k2eAgNnSc2d1	mnohaleté
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
domů	dům	k1gInPc2	dům
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
národní	národní	k2eAgFnSc1d1	národní
společnost	společnost	k1gFnSc1	společnost
vzdálila	vzdálit	k5eAaPmAgFnS	vzdálit
od	od	k7c2	od
jeho	jeho	k3xOp3gInPc2	jeho
ideálů	ideál	k1gInPc2	ideál
<g/>
)	)	kIx)	)
Anděl	Anděla	k1gFnPc2	Anděla
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
-	-	kIx~	-
rozklad	rozklad	k1gInSc1	rozklad
hodnot	hodnota	k1gFnPc2	hodnota
způsobený	způsobený	k2eAgMnSc1d1	způsobený
pronikáním	pronikání	k1gNnSc7	pronikání
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
tzv.	tzv.	kA	tzv.
Mravokárné	mravokárný	k2eAgInPc1d1	mravokárný
románky	románek	k1gInPc1	románek
<g/>
:	:	kIx,	:
Panna	Panna	k1gFnSc1	Panna
Stázička	Stázička	k1gFnSc1	Stázička
neboli	neboli	k8xC	neboli
Buď	buď	k8xC	buď
ctnostná	ctnostný	k2eAgFnSc1d1	ctnostná
i	i	k8xC	i
cudná	cudný	k2eAgFnSc1d1	cudná
<g/>
!	!	kIx.	!
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
domácíček	domácíček	k1gInSc1	domácíček
aneb	aneb	k?	aneb
Buď	buď	k8xC	buď
spořivý	spořivý	k2eAgInSc1d1	spořivý
Maloměstský	maloměstský	k2eAgInSc1d1	maloměstský
Salomo	Saloma	k1gFnSc5	Saloma
čili	čili	k8xC	čili
Buď	buď	k8xC	buď
lidumil	lidumil	k1gMnSc1	lidumil
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
dobráček	dobráček	k1gMnSc1	dobráček
čili	čili	k8xC	čili
Buď	buď	k8xC	buď
dobrý	dobrý	k2eAgMnSc1d1	dobrý
a	a	k8xC	a
úslužný	úslužný	k2eAgMnSc1d1	úslužný
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
Rek	rek	k1gMnSc1	rek
<g/>
,	,	kIx,	,
jakých	jaký	k3yIgNnPc2	jaký
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Pracuj	pracovat	k5eAaImRp2nS	pracovat
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
Epizody	epizoda	k1gFnSc2	epizoda
Okolí	okolí	k1gNnSc1	okolí
Prahy	Praha	k1gFnSc2	Praha
Pláč	pláč	k1gInSc1	pláč
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
neboli	neboli	k8xC	neboli
Nová	nový	k2eAgFnSc1d1	nová
persekuce	persekuce	k1gFnSc1	persekuce
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
Paganini	Paganin	k2eAgMnPc1d1	Paganin
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
-	-	kIx~	-
o	o	k7c6	o
českém	český	k2eAgMnSc6d1	český
houslistovi	houslista	k1gMnSc6	houslista
Josefu	Josef	k1gMnSc6	Josef
Slavíkovi	Slavík	k1gMnSc6	Slavík
K.	K.	kA	K.
H.	H.	kA	H.
Mácha	Mácha	k1gMnSc1	Mácha
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
-	-	kIx~	-
Rozluštěn	rozluštěn	k2eAgInSc1d1	rozluštěn
deník	deník	k1gInSc1	deník
K.	K.	kA	K.
H.	H.	kA	H.
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
psal	psát	k5eAaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
popisuje	popisovat	k5eAaImIp3nS	popisovat
milostný	milostný	k2eAgInSc1d1	milostný
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
snoubenkou	snoubenka	k1gFnSc7	snoubenka
Lory	Lory	k1gFnPc2	Lory
<g/>
.	.	kIx.	.
</s>
<s>
Persekuce	persekuce	k1gFnSc1	persekuce
lidu	lid	k1gInSc2	lid
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
J.	J.	kA	J.
Ex	ex	k6eAd1	ex
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
František	František	k1gMnSc1	František
Thun	Thun	k1gMnSc1	Thun
z	z	k7c2	z
Hohenšteina	Hohenšteino	k1gNnSc2	Hohenšteino
<g/>
,	,	kIx,	,
c.	c.	k?	c.
k.	k.	k?	k.
místodržící	místodržící	k1gMnSc1	místodržící
v	v	k7c6	v
království	království	k1gNnSc6	království
českém	český	k2eAgNnSc6d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
úřadní	úřadní	k2eAgFnSc2d1	úřadní
činnosti	činnost	k1gFnSc2	činnost
Jeho	jeho	k3xOp3gFnSc1	jeho
Excelence	Excelence	k1gFnSc1	Excelence
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
Siluety	silueta	k1gFnSc2	silueta
divadelní	divadelní	k2eAgFnSc2d1	divadelní
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Arabesky	arabeska	k1gFnSc2	arabeska
literární	literární	k2eAgMnSc1d1	literární
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
Nesmrtelní	smrtelní	k2eNgMnPc1d1	nesmrtelní
pijáci	piják	k1gMnPc1	piják
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Záhadné	záhadný	k2eAgFnSc2d1	záhadná
povahy	povaha	k1gFnSc2	povaha
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Z	z	k7c2	z
duševní	duševní	k2eAgFnSc2d1	duševní
dílny	dílna	k1gFnSc2	dílna
básníků	básník	k1gMnPc2	básník
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
-	-	kIx~	-
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
psychologii	psychologie	k1gFnSc4	psychologie
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvorby	tvorba	k1gFnSc2	tvorba
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Čechách-Praha	Čechách-Praha	k1gMnSc1	Čechách-Praha
1946	[number]	k4	1946
<g/>
-Melantrich	-Melantrich	k1gInSc1	-Melantrich
(	(	kIx(	(
<g/>
Citát	citát	k1gInSc1	citát
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
:	:	kIx,	:
Na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
vždy	vždy	k6eAd1	vždy
jenom	jenom	k9	jenom
hosté	host	k1gMnPc1	host
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
povídky	povídka	k1gFnPc4	povídka
uveřejňoval	uveřejňovat	k5eAaImAgMnS	uveřejňovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
novinových	novinový	k2eAgInPc2d1	novinový
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
především	především	k9	především
významných	významný	k2eAgFnPc2d1	významná
světových	světový	k2eAgFnPc2d1	světová
osobností	osobnost	k1gFnPc2	osobnost
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	s	k7c7	s
mj.	mj.	kA	mj.
K.	K.	kA	K.
H.	H.	kA	H.
Máchou	Mácha	k1gMnSc7	Mácha
a	a	k8xC	a
K.	K.	kA	K.
Sabinou	Sabina	k1gFnSc7	Sabina
<g/>
.	.	kIx.	.
</s>
