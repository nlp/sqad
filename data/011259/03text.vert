<p>
<s>
Brakická	brakický	k2eAgFnSc1d1	brakická
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
koncentraci	koncentrace	k1gFnSc4	koncentrace
solí	sůl	k1gFnPc2	sůl
mezi	mezi	k7c7	mezi
mořskou	mořský	k2eAgFnSc7d1	mořská
a	a	k8xC	a
sladkou	sladký	k2eAgFnSc7d1	sladká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
slanější	slaný	k2eAgFnSc1d2	slanější
než	než	k8xS	než
sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
slaná	slaný	k2eAgFnSc1d1	slaná
jako	jako	k8xC	jako
voda	voda	k1gFnSc1	voda
mořská	mořský	k2eAgFnSc1d1	mořská
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
slaná	slaný	k2eAgFnSc1d1	slaná
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
míchá	míchat	k5eAaImIp3nS	míchat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
sladkou	sladký	k2eAgFnSc7d1	sladká
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
ústích	ústí	k1gNnPc6	ústí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
brakická	brakický	k2eAgFnSc1d1	brakická
voda	voda	k1gFnSc1	voda
tvoří	tvořit	k5eAaImIp3nS	tvořit
celá	celý	k2eAgNnPc4d1	celé
moře	moře	k1gNnPc4	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brakická	brakický	k2eAgFnSc1d1	brakická
voda	voda	k1gFnSc1	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
od	od	k7c2	od
0,5	[number]	k4	0,5
do	do	k7c2	do
30	[number]	k4	30
g	g	kA	g
solí	solit	k5eAaImIp3nS	solit
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
brakických	brakický	k2eAgFnPc2d1	brakická
vod	voda	k1gFnPc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
plocha	plocha	k1gFnSc1	plocha
brakické	brakický	k2eAgFnSc2d1	brakická
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Azovské	azovský	k2eAgNnSc1d1	Azovské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
ústí	ústí	k1gNnSc1	ústí
Temže	Temže	k1gFnSc2	Temže
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Londýně	Londýn	k1gInSc6	Londýn
</s>
</p>
<p>
<s>
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
slanost	slanost	k1gFnSc1	slanost
okolních	okolní	k2eAgInPc2d1	okolní
oceánů	oceán	k1gInPc2	oceán
stovky	stovka	k1gFnSc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
</s>
</p>
<p>
<s>
Río	Río	k?	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
</s>
</p>
<p>
<s>
Pangong	Pangong	k1gMnSc1	Pangong
Tso	Tso	k1gMnSc1	Tso
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Himaláje	Himaláje	k1gFnPc4	Himaláje
–	–	k?	–
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
severoindického	severoindický	k2eAgInSc2d1	severoindický
Ladakhu	Ladakh	k1gInSc2	Ladakh
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
slané	slaný	k2eAgNnSc1d1	slané
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitékající	přitékající	k2eAgFnSc1d1	přitékající
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
tavných	tavný	k2eAgInPc2d1	tavný
ledovců	ledovec	k1gInPc2	ledovec
postupně	postupně	k6eAd1	postupně
snížila	snížit	k5eAaPmAgFnS	snížit
jeho	jeho	k3xOp3gFnSc4	jeho
salinitu	salinita	k1gFnSc4	salinita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
brakická	brakický	k2eAgFnSc1d1	brakická
voda	voda	k1gFnSc1	voda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
