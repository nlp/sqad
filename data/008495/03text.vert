<p>
<s>
75	[number]	k4	75
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Bratislava	Bratislava	k1gFnSc1	Bratislava
a	a	k8xC	a
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Slovenska	Slovensko	k1gNnSc2	Slovensko
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
hostitelství	hostitelství	k1gNnSc1	hostitelství
prvotní	prvotní	k2eAgNnSc1d1	prvotní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
odehrály	odehrát	k5eAaPmAgFnP	odehrát
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
některé	některý	k3yIgInPc4	některý
zápasy	zápas	k1gInPc4	zápas
šampionátů	šampionát	k1gInPc2	šampionát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
a	a	k8xC	a
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
IIHF	IIHF	kA	IIHF
stanovila	stanovit	k5eAaPmAgFnS	stanovit
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
zimního	zimní	k2eAgInSc2d1	zimní
stadionu	stadion	k1gInSc2	stadion
jako	jako	k8xC	jako
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
přiřčení	přiřčení	k1gNnSc4	přiřčení
pořadatelství	pořadatelství	k1gNnSc2	pořadatelství
a	a	k8xC	a
organizování	organizování	k1gNnSc2	organizování
turnaje	turnaj	k1gInSc2	turnaj
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
peněz	peníze	k1gInPc2	peníze
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
stávajícího	stávající	k2eAgInSc2d1	stávající
bratislavského	bratislavský	k2eAgInSc2d1	bratislavský
stadionu	stadion	k1gInSc2	stadion
Ondreje	Ondrej	k1gMnSc2	Ondrej
Nepely	Nepela	k1gFnSc2	Nepela
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
mistrovství	mistrovství	k1gNnSc2	mistrovství
nesl	nést	k5eAaImAgInS	nést
sponzorský	sponzorský	k2eAgInSc1d1	sponzorský
název	název	k1gInSc1	název
Orange	Orange	k1gNnSc2	Orange
aréna	aréna	k1gFnSc1	aréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
z	z	k7c2	z
I.	I.	kA	I.
divize	divize	k1gFnSc2	divize
MS	MS	kA	MS
2010	[number]	k4	2010
postoupily	postoupit	k5eAaPmAgFnP	postoupit
2	[number]	k4	2
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
při	při	k7c6	při
MS	MS	kA	MS
2006	[number]	k4	2006
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
IIHF	IIHF	kA	IIHF
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
skupin	skupina	k1gFnPc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Stadiony	stadion	k1gInPc4	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
(	(	kIx(	(
<g/>
Košice	Košice	k1gInPc1	Košice
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
C	C	kA	C
(	(	kIx(	(
<g/>
Košice	Košice	k1gInPc1	Košice
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
D	D	kA	D
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Osmifinále	osmifinále	k1gNnPc3	osmifinále
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
E	E	kA	E
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
F	F	kA	F
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Skupina	skupina	k1gFnSc1	skupina
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Zápasy	zápas	k1gInPc1	zápas
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Play-off	Playff	k1gInSc4	Play-off
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
hráčů	hráč	k1gMnPc2	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
podle	podle	k7c2	podle
direktoriátu	direktoriát	k1gInSc2	direktoriát
IIHF	IIHF	kA	IIHF
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
All	All	k1gFnPc2	All
Stars	Stars	k1gInSc1	Stars
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Pořadí	pořadí	k1gNnSc1	pořadí
podle	podle	k7c2	podle
bodů	bod	k1gInPc2	bod
===	===	k?	===
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc1	pořadí
hráčů	hráč	k1gMnPc2	hráč
podle	podle	k7c2	podle
dosažených	dosažený	k2eAgInPc2d1	dosažený
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
vstřelený	vstřelený	k2eAgInSc4d1	vstřelený
gól	gól	k1gInSc4	gól
nebo	nebo	k8xC	nebo
přihrávku	přihrávka	k1gFnSc4	přihrávka
na	na	k7c4	na
gól	gól	k1gInSc4	gól
hráč	hráč	k1gMnSc1	hráč
získává	získávat	k5eAaImIp3nS	získávat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záp	Záp	k?	Záp
<g/>
.	.	kIx.	.
=	=	kIx~	=
Odehrané	odehraný	k2eAgInPc1d1	odehraný
zápasy	zápas	k1gInPc1	zápas
<g/>
;	;	kIx,	;
G	G	kA	G
=	=	kIx~	=
Góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
P	P	kA	P
=	=	kIx~	=
Přihrávky	přihrávka	k1gFnPc4	přihrávka
<g/>
;	;	kIx,	;
Body	bod	k1gInPc4	bod
=	=	kIx~	=
Body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
−	−	k?	−
=	=	kIx~	=
Plus	plus	k1gInSc1	plus
<g/>
/	/	kIx~	/
<g/>
Minus	minus	k1gInSc1	minus
<g/>
;	;	kIx,	;
PTM	PTM	kA	PTM
=	=	kIx~	=
Počet	počet	k1gInSc1	počet
trestných	trestný	k2eAgFnPc2d1	trestná
minut	minuta	k1gFnPc2	minuta
<g/>
;	;	kIx,	;
Poz	Poz	k1gFnSc1	Poz
<g/>
.	.	kIx.	.
=	=	kIx~	=
Pozice	pozice	k1gFnSc1	pozice
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
brankářů	brankář	k1gMnPc2	brankář
===	===	k?	===
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc4	pořadí
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
pěti	pět	k4xCc2	pět
brankářů	brankář	k1gMnPc2	brankář
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
podle	podle	k7c2	podle
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
zásahů	zásah	k1gInPc2	zásah
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
,	,	kIx,	,
brankář	brankář	k1gMnSc1	brankář
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
odehráno	odehrát	k5eAaPmNgNnS	odehrát
minimálně	minimálně	k6eAd1	minimálně
40	[number]	k4	40
%	%	kIx~	%
hrací	hrací	k2eAgFnSc2d1	hrací
doby	doba	k1gFnSc2	doba
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
=	=	kIx~	=
Čas	čas	k1gInSc1	čas
na	na	k7c6	na
ledě	led	k1gInSc6	led
(	(	kIx(	(
<g/>
minuty	minuta	k1gFnSc2	minuta
<g/>
:	:	kIx,	:
<g/>
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
SNB	SNB	kA	SNB
=	=	kIx~	=
Střely	střel	k1gInPc1	střel
na	na	k7c4	na
branku	branka	k1gFnSc4	branka
<g/>
;	;	kIx,	;
OG	OG	kA	OG
=	=	kIx~	=
Obdržené	obdržený	k2eAgInPc1d1	obdržený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
PGZ	PGZ	kA	PGZ
=	=	kIx~	=
Průměr	průměr	k1gInSc1	průměr
gólů	gól	k1gInPc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
;	;	kIx,	;
Úsp	Úsp	k1gFnSc4	Úsp
<g/>
%	%	kIx~	%
=	=	kIx~	=
Procento	procento	k1gNnSc1	procento
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
zásahů	zásah	k1gInPc2	zásah
<g/>
;	;	kIx,	;
Prod	Prod	k1gMnSc1	Prod
<g/>
.	.	kIx.	.
=	=	kIx~	=
Prodloužení	prodloužení	k1gNnSc1	prodloužení
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
==	==	k?	==
</s>
</p>
<p>
<s>
IIHF	IIHF	kA	IIHF
vybrala	vybrat	k5eAaPmAgFnS	vybrat
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
turnaj	turnaj	k1gInSc4	turnaj
16	[number]	k4	16
hlavních	hlavní	k2eAgInPc2d1	hlavní
a	a	k8xC	a
16	[number]	k4	16
čárových	čárový	k2eAgMnPc2d1	čárový
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
sudí	sudí	k1gMnPc1	sudí
===	===	k?	===
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Baluška	Baluška	k1gMnSc1	Baluška
</s>
</p>
<p>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Bulanov	Bulanov	k1gInSc4	Bulanov
</s>
</p>
<p>
<s>
Darcy	Darca	k1gFnPc1	Darca
Burchell	Burchella	k1gFnPc2	Burchella
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
</s>
</p>
<p>
<s>
Danny	Danna	k1gFnPc1	Danna
Kurmann	Kurmanna	k1gFnPc2	Kurmanna
</s>
</p>
<p>
<s>
Christer	Christer	k1gInSc1	Christer
Lärking	Lärking	k1gInSc1	Lärking
</s>
</p>
<p>
<s>
Eduards	Eduards	k1gInSc1	Eduards
Odiņ	Odiņ	k1gFnSc2	Odiņ
</s>
</p>
<p>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Olenin	Olenina	k1gFnPc2	Olenina
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Ország	Ország	k1gMnSc1	Ország
</s>
</p>
<p>
<s>
Sami	sám	k3xTgMnPc1	sám
Partanen	Partanen	k1gInSc1	Partanen
</s>
</p>
<p>
<s>
Sören	Sören	k2eAgInSc1d1	Sören
Persson	Persson	k1gInSc1	Persson
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Piechaczek	Piechaczka	k1gFnPc2	Piechaczka
</s>
</p>
<p>
<s>
Brent	Brent	k?	Brent
Rety	ret	k1gInPc1	ret
Reiber	Reibero	k1gNnPc2	Reibero
</s>
</p>
<p>
<s>
Jyri	Jyri	k6eAd1	Jyri
Rönn	Rönn	k1gInSc1	Rönn
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šindler	Šindler	k1gMnSc1	Šindler
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Sterns	Sternsa	k1gFnPc2	Sternsa
</s>
</p>
<p>
<s>
===	===	k?	===
Čároví	čárový	k2eAgMnPc1d1	čárový
sudí	sudí	k1gMnPc1	sudí
===	===	k?	===
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Arm	Arm	k1gMnSc1	Arm
</s>
</p>
<p>
<s>
Chris	Chris	k1gFnSc1	Chris
Carlson	Carlsona	k1gFnPc2	Carlsona
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Carnathan	Carnathan	k1gMnSc1	Carnathan
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Deďulja	Deďulja	k1gMnSc1	Deďulja
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Gebauer	Gebauer	k1gMnSc1	Gebauer
</s>
</p>
<p>
<s>
Manuel	Manuel	k1gMnSc1	Manuel
Hollenstein	Hollenstein	k1gMnSc1	Hollenstein
</s>
</p>
<p>
<s>
Matjaž	Matjaž	k1gFnSc1	Matjaž
Hribar	Hribara	k1gFnPc2	Hribara
</s>
</p>
<p>
<s>
Kiel	Kiel	k1gInSc1	Kiel
Murchison	Murchisona	k1gFnPc2	Murchisona
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Novák	Novák	k1gMnSc1	Novák
</s>
</p>
<p>
<s>
Andre	Andr	k1gMnSc5	Andr
Schrader	Schrader	k1gMnSc1	Schrader
</s>
</p>
<p>
<s>
Sirko	sirka	k1gFnSc5	sirka
Schulz	Schulz	k1gMnSc1	Schulz
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Semionov	Semionov	k1gInSc4	Semionov
</s>
</p>
<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Šeljanin	Šeljanina	k1gFnPc2	Šeljanina
</s>
</p>
<p>
<s>
Jussi	Juss	k1gMnSc5	Juss
Terho	Terha	k1gMnSc5	Terha
</s>
</p>
<p>
<s>
Christian	Christian	k1gMnSc1	Christian
Tillerkvist	Tillerkvist	k1gMnSc1	Tillerkvist
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Valach	Valach	k1gMnSc1	Valach
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Soupisky	soupiska	k1gFnPc1	soupiska
hokejových	hokejový	k2eAgFnPc2d1	hokejová
reprezentací	reprezentace	k1gFnPc2	reprezentace
na	na	k7c4	na
MS	MS	kA	MS
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
I	I	kA	I
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
II	II	kA	II
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
III	III	kA	III
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2011	[number]	k4	2011
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
sponzora	sponzor	k1gMnSc2	sponzor
MS	MS	kA	MS
v	v	k7c6	v
Hokeji	hokej	k1gInSc6	hokej
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Tým	tým	k1gInSc1	tým
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Deset	deset	k4xCc1	deset
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
MS.	MS.	k1gFnSc4	MS.
</s>
</p>
<p>
<s>
Hokejportal	Hokejportat	k5eAaPmAgInS	Hokejportat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Kompletní	kompletní	k2eAgNnSc4d1	kompletní
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
z	z	k7c2	z
MS	MS	kA	MS
2011	[number]	k4	2011
</s>
</p>
