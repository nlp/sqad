<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Moravský	moravský	k2eAgInSc1d1
Beroun	Beroun	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Místo	místo	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
47	#num#	k4
<g/>
′	′	k?
<g/>
38,92	38,92	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
24,99	24,99	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Lidická	lidický	k2eAgFnSc1d1
Kód	kód	k1gInSc1
památky	památka	k1gFnPc4
</s>
<s>
46236	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
135	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Moravském	moravský	k2eAgInSc6d1
Berouně	Beroun	k1gInSc6
je	být	k5eAaImIp3nS
trojlodní	trojlodní	k2eAgFnSc1d1
halová	halový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
přechodného	přechodný	k2eAgInSc2d1
slohu	sloh	k1gInSc2
s	s	k7c7
prodlouženým	prodloužený	k2eAgNnSc7d1
kněžištěm	kněžiště	k1gNnSc7
a	a	k8xC
závěrem	závěr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
koutě	kout	k1gInSc6
mezi	mezi	k7c7
severní	severní	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
a	a	k8xC
kněžištěm	kněžiště	k1gNnSc7
stojí	stát	k5eAaImIp3nS
hranolová	hranolový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
s	s	k7c7
vnějším	vnější	k2eAgNnSc7d1
válcovým	válcový	k2eAgNnSc7d1
schodištěm	schodiště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
opěráky	opěrák	k1gInPc4
kněžiště	kněžiště	k1gNnSc2
v	v	k7c6
ose	osa	k1gFnSc6
kostela	kostel	k1gInSc2
je	být	k5eAaImIp3nS
grotta	grotta	k1gFnSc1
s	s	k7c7
Kristem	Kristus	k1gMnSc7
v	v	k7c6
Getsemanské	getsemanský	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1750	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgInSc1d1
zařízení	zařízení	k1gNnSc4
kostela	kostel	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kamenná	kamenný	k2eAgFnSc1d1
křtitelnice	křtitelnice	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
konce	konec	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
zvon	zvon	k1gInSc1
na	na	k7c6
věži	věž	k1gFnSc6
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1584	#num#	k4
a	a	k8xC
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
zaniklé	zaniklý	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
Jestřebí	Jestřebí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
dal	dát	k5eAaPmAgInS
postavit	postavit	k5eAaPmF
v	v	k7c6
letech	léto	k1gNnPc6
1607-1610	1607-1610	k4
kníže	kníže	k1gMnSc1
Karel	Karel	k1gMnSc1
z	z	k7c2
Münsterbergu	Münsterberg	k1gInSc2
jako	jako	k8xS,k8xC
evangelický	evangelický	k2eAgInSc4d1
luterský	luterský	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgFnP
původní	původní	k2eAgFnPc1d1
empory	empora	k1gFnPc1
<g/>
,	,	kIx,
tedy	tedy	k9
tribunové	tribunový	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
časté	častý	k2eAgInPc1d1
u	u	k7c2
protestantských	protestantský	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pravděpodobně	pravděpodobně	k6eAd1
při	při	k7c6
opravě	oprava	k1gFnSc6
kostela	kostel	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
rekatolizaci	rekatolizace	k1gFnSc4
roku	rok	k1gInSc2
1672	#num#	k4
bylo	být	k5eAaImAgNnS
zasvěcení	zasvěcení	k1gNnSc1
změněno	změnit	k5eAaPmNgNnS
na	na	k7c4
současné	současný	k2eAgNnSc4d1
Nanebevzetí	nanebevzetí	k1gNnSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
opravován	opravovat	k5eAaImNgInS
po	po	k7c6
požárech	požár	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
1744	#num#	k4
a	a	k8xC
1799	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
opravě	oprava	k1gFnSc6
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1960	#num#	k4
kostelní	kostelní	k2eAgFnSc1d1
báně	báně	k1gFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
makovice	makovice	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
bylo	být	k5eAaImAgNnS
vyjmuto	vyjmout	k5eAaPmNgNnS
velké	velký	k2eAgNnSc1d1
válcové	válcový	k2eAgNnSc1d1
pouzdro	pouzdro	k1gNnSc1
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgNnSc1d1
pět	pět	k4xCc1
rukopisných	rukopisný	k2eAgInPc2d1
celků	celek	k1gInPc2
z	z	k7c2
let	léto	k1gNnPc2
1610	#num#	k4
<g/>
,	,	kIx,
1672	#num#	k4
<g/>
,	,	kIx,
1751,1848	1751,1848	k4
a	a	k8xC
1922	#num#	k4
<g/>
,	,	kIx,
postihujících	postihující	k2eAgInPc2d1
časové	časový	k2eAgNnSc4d1
období	období	k1gNnSc4
přes	přes	k7c4
350	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
dokumenty	dokument	k1gInPc4
byl	být	k5eAaImAgInS
přehled	přehled	k1gInSc1
správců	správce	k1gMnPc2
moravskoberounské	moravskoberounský	k2eAgFnSc2d1
fary	fara	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1571	#num#	k4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zde	zde	k6eAd1
potravinové	potravinový	k2eAgInPc1d1
lístky	lístek	k1gInPc1
z	z	k7c2
přelomu	přelom	k1gInSc2
let	léto	k1gNnPc2
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
1920	#num#	k4
<g/>
,	,	kIx,
náboj	náboj	k1gInSc1
do	do	k7c2
pušky	puška	k1gFnSc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc4
mince	mince	k1gFnPc4
z	z	k7c2
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
propagační	propagační	k2eAgInSc1d1
plakát	plakát	k1gInSc1
vyzývající	vyzývající	k2eAgInSc1d1
k	k	k7c3
účasti	účast	k1gFnSc3
na	na	k7c6
oslavách	oslava	k1gFnPc6
svěcení	svěcení	k1gNnPc1
zvonů	zvon	k1gInPc2
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpna	srpen	k1gInSc2
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
opravář	opravář	k1gMnSc1
Karel	Karel	k1gMnSc1
Zimmer	Zimmer	k1gMnSc1
pak	pak	k6eAd1
vložil	vložit	k5eAaPmAgMnS
do	do	k7c2
pouzdra	pouzdro	k1gNnSc2
krabičku	krabička	k1gFnSc4
s	s	k7c7
jedinou	jediný	k2eAgFnSc7d1
cigaretou	cigareta	k1gFnSc7
s	s	k7c7
přáním	přání	k1gNnSc7
„	„	k?
<g/>
pro	pro	k7c4
toho	ten	k3xDgMnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
další	další	k2eAgInPc1d1
válec	válec	k1gInSc1
otevře	otevřít	k5eAaPmIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnici	účastnice	k1gFnSc4
pamětihodné	pamětihodný	k2eAgFnSc2d1
události	událost	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Pracovníci	pracovník	k1gMnPc1
komunálních	komunální	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
Miroslav	Miroslav	k1gMnSc1
Pálka	Pálka	k1gMnSc1
<g/>
,	,	kIx,
Štefan	Štefan	k1gMnSc1
Csőgör	Csőgör	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
MNV	MNV	kA
Lubomír	Lubomír	k1gMnSc1
Drápal	Drápal	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
Alois	Alois	k1gMnSc1
Mazánek	Mazánek	k1gMnSc1
<g/>
,	,	kIx,
pracovník	pracovník	k1gMnSc1
Vlastivědného	vlastivědný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
v	v	k7c6
Bruntále	Bruntál	k1gInSc6
<g/>
,	,	kIx,
redaktorka	redaktorka	k1gFnSc1
Květa	Květa	k1gFnSc1
Javorská	Javorská	k1gFnSc1
<g/>
,	,	kIx,
kronikář	kronikář	k1gMnSc1
Josef	Josef	k1gMnSc1
Kalva	kalva	k1gFnSc1
a	a	k8xC
kulturní	kulturní	k2eAgMnSc1d1
referent	referent	k1gMnSc1
ONV	ONV	kA
Bruntál	Bruntál	k1gInSc1
J.	J.	kA
Piňos	Piňos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
B.	B.	kA
Samek	Samek	k1gMnSc1
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnPc1d1
památky	památka	k1gFnPc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
199	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
579-580	579-580	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
B.	B.	kA
Samek	Samek	k1gMnSc1
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnPc1d1
památky	památka	k1gFnPc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
199	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
580	#num#	k4
<g/>
↑	↑	k?
BARTUŠEK	Bartušek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
POJSL	POJSL	kA
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
evangelické	evangelický	k2eAgInPc1d1
kostely	kostel	k1gInPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velehrad	Velehrad	k1gInSc1
<g/>
:	:	kIx,
Historická	historický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Starý	Starý	k1gMnSc1
Velehrad	Velehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
32	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86157	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
B.	B.	kA
Samek	Samek	k1gMnSc1
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnPc1d1
památky	památka	k1gFnPc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
199	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
579	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
Beroun	Beroun	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kostel	kostel	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
města	město	k1gNnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
