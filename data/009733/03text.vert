<p>
<s>
Halogeny	halogen	k1gInPc1	halogen
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
halové	halový	k2eAgInPc4d1	halový
prvky	prvek	k1gInPc4	prvek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gInPc4	člen
17	[number]	k4	17
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
halogeny	halogen	k1gInPc7	halogen
patří	patřit	k5eAaImIp3nS	patřit
fluor	fluor	k1gInSc1	fluor
<g/>
,	,	kIx,	,
chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
brom	brom	k1gInSc1	brom
a	a	k8xC	a
jod	jod	k1gInSc1	jod
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
připojují	připojovat	k5eAaImIp3nP	připojovat
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
astat	astat	k1gInSc1	astat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
vazebných	vazebný	k2eAgFnPc2d1	vazebná
a	a	k8xC	a
ionizačních	ionizační	k2eAgFnPc2d1	ionizační
vlastností	vlastnost	k1gFnPc2	vlastnost
řadil	řadit	k5eAaImAgInS	řadit
spíše	spíše	k9	spíše
k	k	k7c3	k
polokovům	polokov	k1gInPc3	polokov
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
tennessin	tennessin	k1gInSc1	tennessin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
halogen	halogen	k1gInSc1	halogen
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
solitvorný	solitvorný	k2eAgMnSc1d1	solitvorný
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
halogeny	halogen	k1gInPc1	halogen
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
valenční	valenční	k2eAgFnSc6d1	valenční
elektronové	elektronový	k2eAgFnSc6d1	elektronová
vrstvě	vrstva	k1gFnSc6	vrstva
sedm	sedm	k4xCc4	sedm
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Halogeny	halogen	k1gInPc1	halogen
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
vázané	vázaný	k2eAgFnPc1d1	vázaná
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
halogenem	halogen	k1gInSc7	halogen
je	být	k5eAaImIp3nS	být
chlor	chlor	k1gInSc1	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Fluor	fluor	k1gInSc1	fluor
a	a	k8xC	a
chlor	chlor	k1gInSc1	chlor
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
brom	brom	k1gInSc1	brom
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
a	a	k8xC	a
jód	jód	k1gInSc1	jód
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
dvouprvkové	dvouprvkový	k2eAgFnPc1d1	dvouprvková
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
elektropozitivnějšími	elektropozitivný	k2eAgInPc7d2	elektropozitivný
prvky	prvek	k1gInPc7	prvek
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
skupenských	skupenský	k2eAgInPc6d1	skupenský
stavech	stav	k1gInPc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
halogenovodíků	halogenovodík	k1gInPc2	halogenovodík
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nazývat	nazývat	k5eAaImF	nazývat
halogenidy	halogenida	k1gFnPc4	halogenida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fluor	fluor	k1gInSc1	fluor
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
prvků	prvek	k1gInPc2	prvek
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnotu	hodnota	k1gFnSc4	hodnota
elektronegativity	elektronegativita	k1gFnSc2	elektronegativita
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4,0	[number]	k4	4,0
<g/>
-	-	kIx~	-
<g/>
4,1	[number]	k4	4,1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnSc2	sloučenina
halogenů	halogen	k1gInPc2	halogen
s	s	k7c7	s
elektropozitivními	elektropozitivní	k2eAgInPc7d1	elektropozitivní
kovy	kov	k1gInPc7	kov
jsou	být	k5eAaImIp3nP	být
iontové	iontový	k2eAgInPc1d1	iontový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
etymologie	etymologie	k1gFnSc1	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
halogen	halogen	k1gInSc4	halogen
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
jako	jako	k9	jako
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc4d1	objevený
prvek	prvek	k1gInSc4	prvek
chlór	chlór	k1gInSc1	chlór
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
baron	baron	k1gMnSc1	baron
Jöns	Jönsa	k1gFnPc2	Jönsa
Jacob	Jacoba	k1gFnPc2	Jacoba
Berzelius	Berzelius	k1gInSc1	Berzelius
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
termín	termín	k1gInSc1	termín
halogen	halogen	k1gInSc1	halogen
–	–	k?	–
ἅ	ἅ	k?	ἅ
(	(	kIx(	(
<g/>
háls	háls	k1gInSc1	háls
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
γ	γ	k?	γ
(	(	kIx(	(
<g/>
gen-	gen-	k?	gen-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
γ	γ	k?	γ
(	(	kIx(	(
<g/>
gígnomai	gígnoma	k1gFnSc2	gígnoma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
stávat	stávat	k5eAaImF	stávat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
–	–	k?	–
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgInPc4	čtyři
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
fluor	fluor	k1gInSc1	fluor
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
jód	jód	k1gInSc1	jód
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
společně	společně	k6eAd1	společně
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vazebné	vazebný	k2eAgFnPc4d1	vazebná
možnosti	možnost	k1gFnPc4	možnost
halogenů	halogen	k1gInPc2	halogen
==	==	k?	==
</s>
</p>
<p>
<s>
Konfigurace	konfigurace	k1gFnSc1	konfigurace
ns	ns	k?	ns
<g/>
2	[number]	k4	2
<g/>
np	np	k?	np
<g/>
5	[number]	k4	5
valenční	valenční	k2eAgFnSc2d1	valenční
sféry	sféra	k1gFnSc2	sféra
atomu	atom	k1gInSc2	atom
halogenů	halogen	k1gInPc2	halogen
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
stabilizace	stabilizace	k1gFnSc2	stabilizace
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
vazeb	vazba	k1gFnPc2	vazba
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
atomy	atom	k1gInPc7	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
vazby	vazba	k1gFnSc2	vazba
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
atomu	atom	k1gInSc2	atom
halogenu	halogen	k1gInSc2	halogen
přijímán	přijímat	k5eAaImNgInS	přijímat
další	další	k2eAgInSc1d1	další
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
elektronová	elektronový	k2eAgFnSc1d1	elektronová
hustota	hustota	k1gFnSc1	hustota
na	na	k7c6	na
atomu	atom	k1gInSc6	atom
limituje	limitovat	k5eAaBmIp3nS	limitovat
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
konfigurace	konfigurace	k1gFnSc1	konfigurace
ns	ns	k?	ns
<g/>
2	[number]	k4	2
<g/>
np	np	k?	np
<g/>
6	[number]	k4	6
a	a	k8xC	a
atom	atom	k1gInSc1	atom
halogenu	halogen	k1gInSc2	halogen
získává	získávat	k5eAaImIp3nS	získávat
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
-	-	kIx~	-
<g/>
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
způsobu	způsob	k1gInSc6	způsob
stabilizace	stabilizace	k1gFnSc2	stabilizace
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
elektronová	elektronový	k2eAgFnSc1d1	elektronová
hustota	hustota	k1gFnSc1	hustota
atomu	atom	k1gInSc2	atom
halogenu	halogen	k1gInSc2	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Vazebnou	vazebný	k2eAgFnSc4d1	vazebná
situaci	situace	k1gFnSc4	situace
pak	pak	k6eAd1	pak
popisujeme	popisovat	k5eAaImIp1nP	popisovat
pomocí	pomocí	k7c2	pomocí
přisouzení	přisouzení	k1gNnSc2	přisouzení
dosažení	dosažení	k1gNnSc2	dosažení
kladného	kladný	k2eAgInSc2d1	kladný
oxidačního	oxidační	k2eAgInSc2d1	oxidační
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vazebné	vazebný	k2eAgInPc4d1	vazebný
trendy	trend	k1gInPc4	trend
==	==	k?	==
</s>
</p>
<p>
<s>
Halogeny	halogen	k1gInPc1	halogen
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
řadu	řada	k1gFnSc4	řada
trendů	trend	k1gInPc2	trend
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
dolů	dolů	k6eAd1	dolů
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
klesající	klesající	k2eAgFnSc4d1	klesající
elektronegativitu	elektronegativita	k1gFnSc4	elektronegativita
a	a	k8xC	a
reaktivitu	reaktivita	k1gFnSc4	reaktivita
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
bodu	bod	k1gInSc2	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
bodu	bod	k1gInSc2	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
halogenů	halogen	k1gInPc2	halogen
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Fluor	fluor	k1gInSc1	fluor
<g/>
:	:	kIx,	:
fluorit	fluorit	k1gInSc1	fluorit
(	(	kIx(	(
<g/>
kazivec	kazivec	k1gInSc1	kazivec
-	-	kIx~	-
CaF	CaF	k1gFnSc1	CaF
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
apatity	apatit	k1gInPc1	apatit
<g/>
,	,	kIx,	,
Kryolit	kryolit	k1gInSc1	kryolit
(	(	kIx(	(
<g/>
Hexafluorohlinitan	Hexafluorohlinitan	k1gInSc1	Hexafluorohlinitan
sodný	sodný	k2eAgInSc1d1	sodný
-	-	kIx~	-
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chlor	chlor	k1gInSc1	chlor
<g/>
:	:	kIx,	:
halit	halit	k1gInSc1	halit
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gMnSc1	NaCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sylvín	sylvín	k1gInSc1	sylvín
(	(	kIx(	(
<g/>
KCl	KCl	k1gFnSc1	KCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karnalit	karnalit	k1gInSc1	karnalit
-	-	kIx~	-
hexahydrát	hexahydrát	k1gInSc1	hexahydrát
chloridu	chlorid	k1gInSc2	chlorid
draselno-hořečnatého	draselnoořečnatý	k2eAgInSc2d1	draselno-hořečnatý
-	-	kIx~	-
(	(	kIx(	(
<g/>
KCl	KCl	k1gFnSc1	KCl
·	·	k?	·
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2	[number]	k4	2
·	·	k?	·
<g/>
6	[number]	k4	6
H	H	kA	H
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brom	brom	k1gInSc1	brom
<g/>
:	:	kIx,	:
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
chaluhy	chaluha	k1gFnPc1	chaluha
<g/>
,	,	kIx,	,
slaná	slaný	k2eAgNnPc1d1	slané
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
sloučeniny	sloučenina	k1gFnPc1	sloučenina
chlóru	chlór	k1gInSc2	chlór
<g/>
,	,	kIx,	,
bromkarnalit	bromkarnalit	k1gInSc1	bromkarnalit
(	(	kIx(	(
<g/>
KBr	KBr	k1gFnSc1	KBr
·	·	k?	·
MgBr	MgBr	k1gInSc1	MgBr
<g/>
2	[number]	k4	2
·	·	k?	·
<g/>
6	[number]	k4	6
H	H	kA	H
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jód	jód	k1gInSc1	jód
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
NaIO	NaIO	k1gFnSc1	NaIO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
chilský	chilský	k2eAgMnSc1d1	chilský
ledek-	ledek-	k?	ledek-
NaNO	NaNO	k1gMnSc1	NaNO
<g/>
3	[number]	k4	3
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
mořské	mořský	k2eAgInPc1d1	mořský
nerosty	nerost	k1gInPc1	nerost
<g/>
,	,	kIx,	,
štítná	štítný	k2eAgFnSc1d1	štítná
žláza	žláza	k1gFnSc1	žláza
-	-	kIx~	-
součást	součást	k1gFnSc1	součást
hormonů-	hormonů-	k?	hormonů-
kromě	kromě	k7c2	kromě
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
se	se	k3xPyFc4	se
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Cl	Cl	k1gFnPc2	Cl
<g/>
,	,	kIx,	,
Br	br	k0	br
<g/>
,	,	kIx,	,
I	i	k9	i
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
rozpuštěné	rozpuštěný	k2eAgInPc1d1	rozpuštěný
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
aniontů	anion	k1gInPc2	anion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cl-	Cl-	k?	Cl-
<g/>
:	:	kIx,	:
Br-	Br-	k1gMnSc1	Br-
<g/>
:	:	kIx,	:
I	I	kA	I
=	=	kIx~	=
200	[number]	k4	200
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
:	:	kIx,	:
0,1	[number]	k4	0,1
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc4	sloučenina
halogenů	halogen	k1gInPc2	halogen
==	==	k?	==
</s>
</p>
<p>
<s>
Nejmenší	malý	k2eAgFnPc1d3	nejmenší
možnosti	možnost	k1gFnPc1	možnost
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
sloučenin	sloučenina	k1gFnPc2	sloučenina
má	mít	k5eAaImIp3nS	mít
fluor	fluor	k1gInSc1	fluor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
-	-	kIx~	-
<g/>
I.	I.	kA	I.
To	to	k9	to
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
fluorovodík	fluorovodík	k1gInSc4	fluorovodík
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
soli	sůl	k1gFnSc2	sůl
fluoridy	fluorid	k1gInPc4	fluorid
a	a	k8xC	a
také	také	k9	také
kyselinu	kyselina	k1gFnSc4	kyselina
fluornou	fluorný	k2eAgFnSc4d1	fluorný
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dichlordifluormethan	dichlordifluormethan	k1gInSc1	dichlordifluormethan
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
tři	tři	k4xCgInPc1	tři
halové	halový	k2eAgInPc1d1	halový
prvky	prvek	k1gInPc1	prvek
mají	mít	k5eAaImIp3nP	mít
možnosti	možnost	k1gFnPc4	možnost
oproti	oproti	k7c3	oproti
fluoru	fluor	k1gInSc3	fluor
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgFnSc3d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Chlór	chlór	k1gInSc4	chlór
<g/>
,	,	kIx,	,
brom	brom	k1gInSc4	brom
i	i	k8xC	i
jód	jód	k1gInSc4	jód
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
oxidačních	oxidační	k2eAgNnPc6d1	oxidační
číslech	číslo	k1gNnPc6	číslo
<g/>
:	:	kIx,	:
I-	I-	k1gFnSc4	I-
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
+	+	kIx~	+
a	a	k8xC	a
VII	VII	kA	VII
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
a	a	k8xC	a
brom	brom	k1gInSc1	brom
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
IV	IV	kA	IV
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
VI	VI	kA	VI
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chloridech	chlorid	k1gInPc6	chlorid
<g/>
,	,	kIx,	,
bromidech	bromid	k1gInPc6	bromid
a	a	k8xC	a
jodidech	jodid	k1gInPc6	jodid
<g/>
,	,	kIx,	,
chlorovodíku	chlorovodík	k1gInSc6	chlorovodík
<g/>
,	,	kIx,	,
bromovodíku	bromovodík	k1gInSc6	bromovodík
a	a	k8xC	a
jodovodíku	jodovodík	k1gInSc6	jodovodík
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
vodných	vodný	k2eAgInPc6d1	vodný
roztocích	roztok	k1gInPc6	roztok
mají	mít	k5eAaImIp3nP	mít
oxidační	oxidační	k2eAgNnSc4d1	oxidační
číslo	číslo	k1gNnSc4	číslo
I-	I-	k1gFnSc2	I-
<g/>
.	.	kIx.	.
</s>
<s>
Kladné	kladný	k2eAgFnPc1d1	kladná
hodnoty	hodnota	k1gFnPc1	hodnota
oxidačního	oxidační	k2eAgNnSc2d1	oxidační
čísla	číslo	k1gNnSc2	číslo
halogenů	halogen	k1gInPc2	halogen
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
oxidech	oxid	k1gInPc6	oxid
<g/>
:	:	kIx,	:
oxid	oxid	k1gInSc4	oxid
chlorný	chlorný	k2eAgInSc4d1	chlorný
Cl	Cl	k1gFnSc7	Cl
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
chloristý	chloristý	k2eAgInSc4d1	chloristý
Cl	Cl	k1gFnSc7	Cl
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
bromitý	bromitý	k2eAgInSc1d1	bromitý
Br	br	k0	br
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
oxid	oxid	k1gInSc1	oxid
jodičný	jodičný	k2eAgInSc1d1	jodičný
I	i	k9	i
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
oxidy	oxid	k1gInPc1	oxid
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
halogenů	halogen	k1gInPc2	halogen
jsou	být	k5eAaImIp3nP	být
kyselinotvorné	kyselinotvorný	k2eAgFnPc1d1	kyselinotvorná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vznikají	vznikat	k5eAaImIp3nP	vznikat
příslušné	příslušný	k2eAgFnPc1d1	příslušná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kyselina	kyselina	k1gFnSc1	kyselina
jodičná	jodičný	k2eAgFnSc1d1	jodičný
či	či	k8xC	či
bromná	bromný	k2eAgFnSc1d1	bromný
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
kyseliny	kyselina	k1gFnPc1	kyselina
jsou	být	k5eAaImIp3nP	být
jednosytné	jednosytný	k2eAgFnPc1d1	jednosytná
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednu	jeden	k4xCgFnSc4	jeden
řadu	řada	k1gFnSc4	řada
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kyselina	kyselina	k1gFnSc1	kyselina
chlorná	chlorný	k2eAgFnSc1d1	chlorná
tvoří	tvořit	k5eAaImIp3nS	tvořit
chlornany	chlornan	k1gInPc1	chlornan
<g/>
,	,	kIx,	,
bromitá	bromitat	k5eAaPmIp3nS	bromitat
bromitany	bromitan	k1gInPc1	bromitan
a	a	k8xC	a
jodičná	jodičný	k2eAgFnSc1d1	jodičný
jodičnany	jodičnana	k1gFnPc1	jodičnana
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
halové	halový	k2eAgInPc1d1	halový
prvky	prvek	k1gInPc1	prvek
vázány	vázán	k2eAgInPc1d1	vázán
také	také	k9	také
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
např.	např.	kA	např.
<g/>
:	:	kIx,	:
vinylchlorid	vinylchlorid	k1gInSc1	vinylchlorid
<g/>
,	,	kIx,	,
brommethan	brommethan	k1gInSc1	brommethan
<g/>
,	,	kIx,	,
trichlorethylen	trichlorethylen	k1gInSc1	trichlorethylen
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mnemotechnické	mnemotechnický	k2eAgFnPc1d1	mnemotechnická
pomůcky	pomůcka	k1gFnPc1	pomůcka
–	–	k?	–
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
