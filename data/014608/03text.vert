<s>
Nandi	Nand	k1gMnPc1
</s>
<s>
Socha	socha	k1gFnSc1
Nandiho	Nandi	k1gMnSc2
v	v	k7c6
Maisúru	Maisúr	k1gInSc6
</s>
<s>
Nandi	Nand	k1gMnPc1
(	(	kIx(
<g/>
v	v	k7c6
sanskrtu	sanskrt	k1gInSc6
न	न	k?
<g/>
ं	ं	k?
<g/>
द	द	k?
<g/>
ी	ी	k?
-	-	kIx~
"	"	kIx"
<g/>
Ten	ten	k3xDgInSc1
Radostný	radostný	k2eAgInSc1d1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
býk	býk	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
podle	podle	k7c2
hinduistické	hinduistický	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
jezdí	jezdit	k5eAaImIp3nS
bůh	bůh	k1gMnSc1
Šiva	Šiva	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Socha	socha	k1gFnSc1
Nandiho	Nandi	k1gMnSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
naproti	naproti	k7c3
vchodu	vchod	k1gInSc3
do	do	k7c2
vimány	vimána	k1gFnSc2
hinduistických	hinduistický	k2eAgInPc2d1
chrámů	chrám	k1gInPc2
zasvěcených	zasvěcený	k2eAgInPc2d1
Šivovi	Šiva	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
však	však	k9
chrámy	chrám	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
zasvěceny	zasvětit	k5eAaPmNgInP
pouze	pouze	k6eAd1
Nandimu	Nandimo	k1gNnSc3
samotnému	samotný	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nandi	Nand	k1gMnPc1
je	být	k5eAaImIp3nS
uctíván	uctívat	k5eAaImNgMnS
ženami	žena	k1gFnPc7
jako	jako	k9
symbol	symbol	k1gInSc4
plodnosti	plodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
byl	být	k5eAaImAgInS
Nandi	Nand	k1gMnPc1
antropomorfní	antropomorfní	k2eAgFnSc4d1
strážce	strážce	k1gMnSc1
posvátné	posvátný	k2eAgFnSc2d1
transhimálajské	transhimálajský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Kailáš	Kailáš	k1gFnSc2
<g/>
,	,	kIx,
zasvěcené	zasvěcený	k2eAgFnPc1d1
Šivovi	Šivův	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
coby	coby	k?
Mahárši	Mahárše	k1gFnSc4
Nandi	Nand	k1gMnPc1
Nátha	Náth	k1gMnSc4
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Velký	velký	k2eAgMnSc1d1
svatý	svatý	k1gMnSc1
Nandi	Nand	k1gMnPc1
Nátha	Nátha	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
zakladatel	zakladatel	k1gMnSc1
hinduistické	hinduistický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
Nandinátha	Nandinátha	k1gMnSc1
Sampradája	Sampradája	k1gMnSc1
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
až	až	k9
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Jako	jako	k8xS,k8xC
takový	takový	k3xDgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
šivaismu	šivaismus	k1gInSc6
uznáván	uznáván	k2eAgInSc1d1
též	též	k9
jako	jako	k8xS,k8xC
duchovní	duchovní	k1gMnSc1
vůdce	vůdce	k1gMnSc1
(	(	kIx(
<g/>
guru	guru	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gFnSc4
jméno	jméno	k1gNnSc1
v	v	k7c6
hinduismu	hinduismus	k1gInSc6
aplikováno	aplikován	k2eAgNnSc1d1
na	na	k7c4
posvátného	posvátný	k2eAgMnSc4d1
"	"	kIx"
<g/>
býka	býk	k1gMnSc4
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vršabha	vršabha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uctívaného	uctívaný	k2eAgInSc2d1
již	již	k6eAd1
v	v	k7c6
pradávné	pradávný	k2eAgFnSc6d1
civilizaci	civilizace	k1gFnSc6
poříčí	poříčí	k1gNnSc2
Indu	Indus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
Nandiho	Nandi	k1gMnSc2
stal	stát	k5eAaPmAgInS
také	také	k9
dopravní	dopravní	k2eAgInSc1d1
prostředek	prostředek	k1gInSc1
Šivy	Šiva	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
hlavní	hlavní	k2eAgMnSc1d1
následovník	následovník	k1gMnSc1
a	a	k8xC
vůdce	vůdce	k1gMnPc4
jeho	jeho	k3xOp3gNnPc2
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nandi	Nand	k1gMnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Socha	socha	k1gFnSc1
Nandiho	Nandi	k1gMnSc2
v	v	k7c6
Čennaí	Čennaí	k1gFnSc6
(	(	kIx(
<g/>
Madrásu	Madrás	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hinduismus	hinduismus	k1gInSc1
</s>
