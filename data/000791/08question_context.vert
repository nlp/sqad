<s>
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Australské	australský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
)	)	kIx)
-	-	kIx~
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
též	též	k9
Australský	australský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
-	-	kIx~
je	být	k5eAaImIp3nS
federativní	federativní	k2eAgNnSc1d1
stát	stát	k5eAaImF
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
nacházející	nacházející	k2eAgFnSc6d1
se	se	k3xPyFc4
na	na	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>