<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Australské	australský	k2eAgNnSc1d1	Australské
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Commonwealth	Commonwealth	k1gInSc1	Commonwealth
of	of	k?	of
Australia	Australia	k1gFnSc1	Australia
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
též	též	k9	též
Australský	australský	k2eAgInSc1d1	australský
svaz	svaz	k1gInSc1	svaz
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgNnSc1d1	federativní
stát	stát	k5eAaImF	stát
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
nacházející	nacházející	k2eAgFnSc6d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
ho	on	k3xPp3gMnSc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
velký	velký	k2eAgInSc4d1	velký
ostrov	ostrov	k1gInSc4	ostrov
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Jižním	jižní	k2eAgNnSc6d1	jižní
<g/>
,	,	kIx,	,
Indickém	indický	k2eAgInSc6d1	indický
a	a	k8xC	a
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Indonésií	Indonésie	k1gFnSc7	Indonésie
<g/>
,	,	kIx,	,
Východním	východní	k2eAgInSc7d1	východní
Timorem	Timor	k1gInSc7	Timor
a	a	k8xC	a
Papuou	Papuý	k2eAgFnSc7d1	Papuý
Novou	nový	k2eAgFnSc7d1	nová
Guineou	Guinea	k1gFnSc7	Guinea
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
se	s	k7c7	s
Šalamounovými	Šalamounův	k2eAgInPc7d1	Šalamounův
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
Vanuatu	Vanuat	k1gMnSc3	Vanuat
a	a	k8xC	a
Novou	nový	k2eAgFnSc4d1	nová
Kaledonií	Kaledonie	k1gFnSc7	Kaledonie
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
Zélandem	Zéland	k1gInSc7	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Canberra	Canberra	k1gFnSc1	Canberra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
-	-	kIx~	-
24	[number]	k4	24
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
pobřežních	pobřežní	k2eAgNnPc6d1	pobřežní
městech	město	k1gNnPc6	město
jako	jako	k8xS	jako
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
,	,	kIx,	,
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
Perth	Pertha	k1gFnPc2	Pertha
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gInSc5	Adelaid
a	a	k8xC	a
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
pevnina	pevnina	k1gFnSc1	pevnina
je	být	k5eAaImIp3nS	být
obydlena	obydlet	k5eAaPmNgFnS	obydlet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřicet	čtyřicet	k4xCc4	čtyřicet
dva	dva	k4xCgInPc4	dva
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
domorodými	domorodý	k2eAgMnPc7d1	domorodý
obyvateli	obyvatel	k1gMnPc7	obyvatel
-	-	kIx~	-
Austrálci	Austrálec	k1gMnPc7	Austrálec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ojedinělých	ojedinělý	k2eAgFnPc6d1	ojedinělá
návštěvách	návštěva	k1gFnPc6	návštěva
rybářů	rybář	k1gMnPc2	rybář
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
evropských	evropský	k2eAgMnPc2d1	evropský
objevitelů	objevitel	k1gMnPc2	objevitel
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
v	v	k7c6	v
sedmnáctém	sedmnáctý	k4xOgNnSc6	sedmnáctý
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
východní	východní	k2eAgFnSc1d1	východní
polovina	polovina	k1gFnSc1	polovina
pevniny	pevnina	k1gFnSc2	pevnina
zabrána	zabrán	k2eAgFnSc1d1	zabrána
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc1	pobřeží
osídleno	osídlen	k2eAgNnSc1d1	osídleno
transporty	transport	k1gInPc1	transport
trestanců	trestanec	k1gMnPc2	trestanec
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1788	[number]	k4	1788
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k9	jako
kolonie	kolonie	k1gFnSc2	kolonie
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
populace	populace	k1gFnPc1	populace
byly	být	k5eAaImAgFnP	být
objevovány	objevovat	k5eAaImNgFnP	objevovat
nové	nový	k2eAgFnPc1d1	nová
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
pět	pět	k4xCc1	pět
dalších	další	k2eAgFnPc2d1	další
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
britských	britský	k2eAgFnPc2d1	britská
zámořských	zámořský	k2eAgFnPc2d1	zámořská
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
se	se	k3xPyFc4	se
šest	šest	k4xCc1	šest
kolonií	kolonie	k1gFnPc2	kolonie
stalo	stát	k5eAaPmAgNnS	stát
federací	federace	k1gFnSc7	federace
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
základ	základ	k1gInSc1	základ
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
funguje	fungovat	k5eAaImIp3nS	fungovat
stabilní	stabilní	k2eAgMnSc1d1	stabilní
liberálně	liberálně	k6eAd1	liberálně
demokratický	demokratický	k2eAgInSc1d1	demokratický
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
podobný	podobný	k2eAgInSc1d1	podobný
politickému	politický	k2eAgInSc3d1	politický
systému	systém	k1gInSc3	systém
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
adjektiva	adjektivum	k1gNnSc2	adjektivum
australis	australis	k1gFnSc2	australis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jižní	jižní	k2eAgNnSc1d1	jižní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
"	"	kIx"	"
<g/>
neznámé	známý	k2eNgFnSc6d1	neznámá
jižní	jižní	k2eAgFnSc6d1	jižní
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
terra	terra	k1gFnSc1	terra
australis	australis	k1gFnSc1	australis
incognita	incognita	k1gFnSc1	incognita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
datována	datován	k2eAgFnSc1d1	datována
až	až	k9	až
do	do	k7c2	do
římských	římský	k2eAgMnPc2d1	římský
časů	čas	k1gInPc2	čas
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
běžná	běžný	k2eAgFnSc1d1	běžná
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
geografii	geografie	k1gFnSc6	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
vázána	vázán	k2eAgFnSc1d1	vázána
na	na	k7c6	na
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
znalosti	znalost	k1gFnPc1	znalost
umístění	umístění	k1gNnSc2	umístění
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
použití	použití	k1gNnSc4	použití
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Australia	Australia	k1gFnSc1	Australia
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1625	[number]	k4	1625
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
"	"	kIx"	"
<g/>
A	a	k9	a
note	not	k1gMnSc2	not
of	of	k?	of
Australia	Australius	k1gMnSc2	Australius
del	del	k?	del
Espiritu	Espirit	k1gInSc2	Espirit
Santo	Sant	k2eAgNnSc1d1	Santo
<g/>
,	,	kIx,	,
written	written	k2eAgMnSc1d1	written
by	by	k9	by
Master	master	k1gMnSc1	master
Hakluyt	Hakluyt	k1gInSc1	Hakluyt
<g/>
"	"	kIx"	"
uveřejněné	uveřejněný	k2eAgFnSc2d1	uveřejněná
Samuelem	Samuel	k1gMnSc7	Samuel
Purchasem	Purchas	k1gMnSc7	Purchas
v	v	k7c4	v
Hakluytus	Hakluytus	k1gInSc4	Hakluytus
Posthumus	Posthumus	k1gMnSc1	Posthumus
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemské	nizozemský	k2eAgNnSc1d1	Nizozemské
přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
Australische	Australisch	k1gFnSc2	Australisch
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
nizozemskými	nizozemský	k2eAgInPc7d1	nizozemský
úřady	úřad	k1gInPc7	úřad
v	v	k7c6	v
Jakartě	Jakarta	k1gFnSc6	Jakarta
roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
nově	nově	k6eAd1	nově
objevenou	objevený	k2eAgFnSc4d1	objevená
pevninu	pevnina	k1gFnSc4	pevnina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Australia	Australia	k1gFnSc1	Australia
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
dále	daleko	k6eAd2	daleko
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1693	[number]	k4	1693
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
francouzského	francouzský	k2eAgInSc2d1	francouzský
románu	román	k1gInSc2	román
Gabriela	Gabriel	k1gMnSc4	Gabriel
de	de	k?	de
Foignyho	Foigny	k1gMnSc4	Foigny
Les	les	k1gInSc1	les
Aventures	Aventures	k1gMnSc1	Aventures
de	de	k?	de
Jacques	Jacques	k1gMnSc1	Jacques
Sadeur	Sadeur	k1gMnSc1	Sadeur
dans	dans	k1gInSc4	dans
la	la	k1gNnSc2	la
Découverte	Découvert	k1gInSc5	Découvert
et	et	k?	et
le	le	k?	le
Voyage	Voyage	k1gNnSc2	Voyage
de	de	k?	de
la	la	k1gNnSc2	la
Terre	Terr	k1gMnSc5	Terr
Australe	Austral	k1gMnSc5	Austral
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
použil	použít	k5eAaPmAgMnS	použít
Alexander	Alexandra	k1gFnPc2	Alexandra
Dalrymple	Dalrymple	k1gFnSc2	Dalrymple
v	v	k7c6	v
An	An	k1gFnSc6	An
Historical	Historical	k1gFnSc2	Historical
Collection	Collection	k1gInSc1	Collection
of	of	k?	of
Voyages	Voyages	k1gInSc1	Voyages
and	and	k?	and
Discoveries	Discoveries	k1gInSc1	Discoveries
in	in	k?	in
the	the	k?	the
South	South	k1gMnSc1	South
Pacific	Pacific	k1gMnSc1	Pacific
Ocean	Ocean	k1gMnSc1	Ocean
(	(	kIx(	(
<g/>
1771	[number]	k4	1771
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jím	on	k3xPp3gNnSc7	on
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
jihopacifickou	jihopacifický	k2eAgFnSc4d1	jihopacifický
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
uveřejnili	uveřejnit	k5eAaPmAgMnP	uveřejnit
George	Georg	k1gMnSc4	Georg
Shaw	Shaw	k1gMnSc4	Shaw
a	a	k8xC	a
Sir	sir	k1gMnSc1	sir
James	James	k1gMnSc1	James
Smith	Smith	k1gMnSc1	Smith
publikaci	publikace	k1gFnSc4	publikace
Zoology	zoolog	k1gMnPc4	zoolog
and	and	k?	and
Botany	Botan	k1gInPc4	Botan
of	of	k?	of
New	New	k1gFnSc2	New
Holland	Hollanda	k1gFnPc2	Hollanda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
psali	psát	k5eAaImAgMnP	psát
o	o	k7c4	o
"	"	kIx"	"
<g/>
velkém	velký	k2eAgInSc6d1	velký
ostrově	ostrov	k1gInSc6	ostrov
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
kontinentu	kontinent	k1gInSc2	kontinent
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
,	,	kIx,	,
Australásii	Australásie	k1gFnSc6	Australásie
nebo	nebo	k8xC	nebo
Novém	nový	k2eAgNnSc6d1	nové
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
zpopularizováno	zpopularizovat	k5eAaPmNgNnS	zpopularizovat
až	až	k9	až
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
dílem	dílem	k6eAd1	dílem
A	a	k9	a
Voyage	Voyage	k1gFnSc1	Voyage
to	ten	k3xDgNnSc4	ten
Terra	Terra	k1gMnSc1	Terra
Australis	Australis	k1gFnSc2	Australis
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
Matthewa	Matthewus	k1gMnSc2	Matthewus
Flinderse	Flinderse	k1gFnSc2	Flinderse
-	-	kIx~	-
prvního	první	k4xOgMnSc2	první
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Austrálii	Austrálie	k1gFnSc4	Austrálie
obeplul	obeplout	k5eAaPmAgMnS	obeplout
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
titulu	titul	k1gInSc3	titul
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
odrážel	odrážet	k5eAaImAgMnS	odrážet
pohled	pohled	k1gInSc4	pohled
britské	britský	k2eAgFnSc2d1	britská
admirality	admiralita	k1gFnSc2	admiralita
<g/>
,	,	kIx,	,
Flinders	Flinders	k1gInSc4	Flinders
adjektivum	adjektivum	k1gNnSc4	adjektivum
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
čtena	čten	k2eAgFnSc1d1	čtena
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
jménu	jméno	k1gNnSc6	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
Lachlan	Lachlan	k1gMnSc1	Lachlan
Macquarie	Macquarie	k1gFnSc1	Macquarie
následně	následně	k6eAd1	následně
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zprávách	zpráva	k1gFnPc6	zpráva
do	do	k7c2	do
mateřské	mateřský	k2eAgFnSc2d1	mateřská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
pak	pak	k6eAd1	pak
doporučil	doporučit	k5eAaPmAgMnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
admiralita	admiralita	k1gFnSc1	admiralita
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
a	a	k8xC	a
kontinent	kontinent	k1gInSc1	kontinent
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
oficiálně	oficiálně	k6eAd1	oficiálně
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
prvního	první	k4xOgNnSc2	první
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
50	[number]	k4	50
000	[number]	k4	000
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Australané	Australan	k1gMnPc1	Australan
přišli	přijít	k5eAaPmAgMnP	přijít
přes	přes	k7c4	přes
pevninský	pevninský	k2eAgInSc4d1	pevninský
most	most	k1gInSc4	most
a	a	k8xC	a
úžiny	úžina	k1gFnPc4	úžina
z	z	k7c2	z
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgMnPc4d1	obří
vačnatce	vačnatec	k1gMnPc4	vačnatec
vyhubili	vyhubit	k5eAaPmAgMnP	vyhubit
lovem	lov	k1gInSc7	lov
a	a	k8xC	a
krajinu	krajina	k1gFnSc4	krajina
změnili	změnit	k5eAaPmAgMnP	změnit
žďářením	žďáření	k1gNnSc7	žďáření
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
sběrači	sběrač	k1gMnPc1	sběrač
se	s	k7c7	s
složitou	složitý	k2eAgFnSc7d1	složitá
ústní	ústní	k2eAgFnSc7d1	ústní
tradicí	tradice	k1gFnSc7	tradice
a	a	k8xC	a
systémem	systém	k1gInSc7	systém
duchovních	duchovní	k2eAgFnPc2d1	duchovní
hodnot	hodnota	k1gFnPc2	hodnota
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
úctě	úcta	k1gFnSc6	úcta
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
víru	víra	k1gFnSc4	víra
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Čas	čas	k1gInSc1	čas
snů	sen	k1gInPc2	sen
<g/>
"	"	kIx"	"
-	-	kIx~	-
Dreamtime	Dreamtim	k1gMnSc5	Dreamtim
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Torresovy	Torresův	k2eAgFnSc2d1	Torresova
úžiny	úžina	k1gFnSc2	úžina
a	a	k8xC	a
části	část	k1gFnSc2	část
severního	severní	k2eAgInSc2d1	severní
Queenslandu	Queensland	k1gInSc2	Queensland
dále	daleko	k6eAd2	daleko
obývali	obývat	k5eAaImAgMnP	obývat
ostrované	ostrovan	k1gMnPc1	ostrovan
z	z	k7c2	z
Torresovy	Torresův	k2eAgFnSc2d1	Torresova
úžiny	úžina	k1gFnSc2	úžina
<g/>
,	,	kIx,	,
etničtí	etnický	k2eAgMnPc1d1	etnický
Melanézané	Melanézaný	k2eAgNnSc4d1	Melanézaný
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kultura	kultura	k1gFnSc1	kultura
však	však	k9	však
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
dnešních	dnešní	k2eAgMnPc2d1	dnešní
Austrálců	Austrálec	k1gMnPc2	Austrálec
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
nesporným	sporný	k2eNgInSc7d1	nesporný
evropským	evropský	k2eAgNnSc7d1	Evropské
pozorováním	pozorování	k1gNnSc7	pozorování
australské	australský	k2eAgFnSc2d1	australská
pevniny	pevnina	k1gFnSc2	pevnina
je	být	k5eAaImIp3nS	být
pozorování	pozorování	k1gNnSc4	pozorování
holandského	holandský	k2eAgMnSc2d1	holandský
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
Dirka	Direk	k1gMnSc2	Direk
Hartóga	Hartóg	k1gMnSc2	Hartóg
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1616	[number]	k4	1616
doplul	doplout	k5eAaPmAgInS	doplout
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
lodí	loď	k1gFnSc7	loď
Eendracht	Eendrachta	k1gFnPc2	Eendrachta
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Žraločí	žraločí	k2eAgFnSc2d1	žraločí
zátoky	zátoka	k1gFnSc2	zátoka
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
zmapovali	zmapovat	k5eAaPmAgMnP	zmapovat
většinu	většina	k1gFnSc4	většina
západního	západní	k2eAgMnSc2d1	západní
a	a	k8xC	a
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nazývali	nazývat	k5eAaImAgMnP	nazývat
Nové	Nové	k2eAgNnSc4d1	Nové
Holandsko	Holandsko	k1gNnSc4	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoušeli	pokoušet	k5eNaImAgMnP	pokoušet
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
osídlit	osídlit	k5eAaPmF	osídlit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
evropským	evropský	k2eAgInSc7d1	evropský
nálezem	nález	k1gInSc7	nález
je	být	k5eAaImIp3nS	být
holandský	holandský	k2eAgInSc4d1	holandský
talíř	talíř	k1gInSc4	talíř
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
1616	[number]	k4	1616
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
se	se	k3xPyFc4	se
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
plavil	plavit	k5eAaImAgMnS	plavit
podél	podél	k6eAd1	podél
a	a	k8xC	a
mapoval	mapovat	k5eAaImAgMnS	mapovat
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
a	a	k8xC	a
zabral	zabrat	k5eAaPmAgMnS	zabrat
pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
přistál	přistát	k5eAaImAgInS	přistát
v	v	k7c4	v
Botany	Botan	k1gMnPc4	Botan
Bay	Bay	k1gFnSc2	Bay
<g/>
,	,	kIx,	,
objevy	objev	k1gInPc4	objev
jeho	jeho	k3xOp3gFnSc2	jeho
expedice	expedice	k1gFnSc2	expedice
pak	pak	k6eAd1	pak
daly	dát	k5eAaPmAgInP	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
trestanecké	trestanecký	k2eAgFnSc2d1	trestanecká
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgNnSc4d1	Britské
zámořské	zámořský	k2eAgNnSc4d1	zámořské
teritorium	teritorium	k1gNnSc4	teritorium
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
osídlením	osídlení	k1gNnSc7	osídlení
Port	porta	k1gFnPc2	porta
Jacksonu	Jackson	k1gInSc2	Jackson
kapitánem	kapitán	k1gMnSc7	kapitán
Arthurem	Arthur	k1gMnSc7	Arthur
Philippem	Philipp	k1gMnSc7	Philipp
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
flotilou	flotila	k1gFnSc7	flotila
First	First	k1gMnSc1	First
Fleet	Fleet	k1gMnSc1	Fleet
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
australským	australský	k2eAgMnSc7d1	australský
národním	národní	k2eAgInSc7d1	národní
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
Dnem	den	k1gInSc7	den
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
Van	vana	k1gFnPc2	vana
Diemena	Diemen	k2eAgFnSc1d1	Diemena
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
<g/>
,	,	kIx,	,
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kolonií	kolonie	k1gFnSc7	kolonie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
částí	část	k1gFnPc2	část
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
další	další	k2eAgFnPc1d1	další
samostatné	samostatný	k2eAgFnPc1d1	samostatná
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Austrálie	Austrálie	k1gFnSc1	Austrálie
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Victoria	Victorium	k1gNnSc2	Victorium
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
a	a	k8xC	a
Queensland	Queensland	k1gInSc1	Queensland
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc1d1	severní
teritorium	teritorium	k1gNnSc1	teritorium
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
vynětím	vynětí	k1gNnSc7	vynětí
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Austrálie	Austrálie	k1gFnSc1	Austrálie
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
svobodná	svobodný	k2eAgFnSc1d1	svobodná
provincie	provincie	k1gFnSc1	provincie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
free	freat	k5eAaPmIp3nS	freat
province	province	k1gFnSc1	province
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
tedy	tedy	k9	tedy
nikdy	nikdy	k6eAd1	nikdy
trestaneckou	trestanecký	k2eAgFnSc7d1	trestanecká
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
Victoria	Victorium	k1gNnSc2	Victorium
a	a	k8xC	a
Západní	západní	k2eAgFnPc1d1	západní
Austrálie	Austrálie	k1gFnPc1	Austrálie
byly	být	k5eAaImAgFnP	být
také	také	k9	také
založeny	založit	k5eAaPmNgFnP	založit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
svobodné	svobodný	k2eAgFnSc6d1	svobodná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
přijaly	přijmout	k5eAaPmAgInP	přijmout
transporty	transport	k1gInPc1	transport
trestanců	trestanec	k1gMnPc2	trestanec
<g/>
.	.	kIx.	.
</s>
<s>
Transporty	transporta	k1gFnPc1	transporta
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
obyvatel	obyvatel	k1gMnPc2	obyvatel
zastaveny	zastaven	k2eAgInPc1d1	zastaven
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zahájení	zahájení	k1gNnSc2	zahájení
evropského	evropský	k2eAgNnSc2d1	Evropské
osídlování	osídlování	k1gNnSc2	osídlování
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
350	[number]	k4	350
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgNnPc2d1	následující
150	[number]	k4	150
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
vlivem	vlivem	k7c2	vlivem
rozšíření	rozšíření	k1gNnSc2	rozšíření
infekčních	infekční	k2eAgFnPc2d1	infekční
nemocí	nemoc	k1gFnPc2	nemoc
přinesených	přinesený	k2eAgFnPc2d1	přinesená
Evropany	Evropan	k1gMnPc7	Evropan
a	a	k8xC	a
násilného	násilný	k2eAgNnSc2d1	násilné
přesídlování	přesídlování	k1gNnSc2	přesídlování
spojeného	spojený	k2eAgNnSc2d1	spojené
s	s	k7c7	s
kulturním	kulturní	k2eAgInSc7d1	kulturní
rozkladem	rozklad	k1gInSc7	rozklad
prudce	prudko	k6eAd1	prudko
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Odebírání	odebírání	k1gNnSc1	odebírání
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
i	i	k8xC	i
samotných	samotný	k2eAgMnPc2d1	samotný
Aboriginců	Aboriginec	k1gMnPc2	Aboriginec
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
dle	dle	k7c2	dle
jistých	jistý	k2eAgFnPc2d1	jistá
definic	definice	k1gFnPc2	definice
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
genocidu	genocida	k1gFnSc4	genocida
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
podání	podání	k1gNnPc1	podání
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
nebo	nebo	k8xC	nebo
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
politickými	politický	k2eAgInPc7d1	politický
či	či	k8xC	či
ideologickými	ideologický	k2eAgInPc7d1	ideologický
cíli	cíl	k1gInPc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
spory	spor	k1gInPc1	spor
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Války	válka	k1gFnSc2	válka
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
History	Histor	k1gInPc1	Histor
Wars	Warsa	k1gFnPc2	Warsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
následných	následný	k2eAgFnPc6d1	následná
změnách	změna	k1gFnPc6	změna
ústavy	ústava	k1gFnSc2	ústava
získala	získat	k5eAaPmAgFnS	získat
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
pravomoc	pravomoc	k1gFnSc4	pravomoc
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
moc	moc	k6eAd1	moc
i	i	k9	i
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jim	on	k3xPp3gFnPc3	on
bylo	být	k5eAaImAgNnS	být
přiznáno	přiznat	k5eAaPmNgNnS	přiznat
postavení	postavení	k1gNnSc1	postavení
rovnoprávných	rovnoprávný	k2eAgMnPc2d1	rovnoprávný
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
země	zem	k1gFnSc2	zem
nebylo	být	k5eNaImAgNnS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Australský	australský	k2eAgInSc1d1	australský
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
případu	případ	k1gInSc6	případ
Mabo	Mabo	k1gMnSc1	Mabo
v.	v.	k?	v.
Queensland	Queensland	k1gInSc1	Queensland
(	(	kIx(	(
<g/>
No	no	k9	no
2	[number]	k4	2
<g/>
)	)	kIx)	)
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
představu	představa	k1gFnSc4	představa
Austrálie	Austrálie	k1gFnSc2	Austrálie
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
prázdné	prázdný	k2eAgFnPc4d1	prázdná
země	zem	k1gFnPc4	zem
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
terra	terra	k1gMnSc1	terra
nullius	nullius	k1gMnSc1	nullius
<g/>
)	)	kIx)	)
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejího	její	k3xOp3gMnSc4	její
zabrání	zabránit	k5eAaPmIp3nS	zabránit
Evropany	Evropan	k1gMnPc4	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
Bathurstu	Bathurst	k1gInSc6	Bathurst
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Eureka	Eureka	k1gMnSc1	Eureka
Stockade	Stockad	k1gInSc5	Stockad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vzpoura	vzpoura	k1gFnSc1	vzpoura
proti	proti	k7c3	proti
zavedení	zavedení	k1gNnSc3	zavedení
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
těžební	těžební	k2eAgFnPc4d1	těžební
licence	licence	k1gFnPc4	licence
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
prvním	první	k4xOgMnSc6	první
vyjádřením	vyjádření	k1gNnSc7	vyjádření
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1855	[number]	k4	1855
a	a	k8xC	a
1890	[number]	k4	1890
postupně	postupně	k6eAd1	postupně
všech	všecek	k3xTgFnPc2	všecek
pět	pět	k4xCc1	pět
kolonií	kolonie	k1gFnPc2	kolonie
získalo	získat	k5eAaPmAgNnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
samosprávnou	samosprávný	k2eAgFnSc4d1	samosprávná
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
stále	stále	k6eAd1	stále
zůstávaly	zůstávat	k5eAaImAgFnP	zůstávat
součástí	součást	k1gFnSc7	součást
Britské	britský	k2eAgFnPc1d1	britská
říše	říš	k1gFnPc1	říš
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlastní	vlastní	k2eAgFnSc7d1	vlastní
kontrolou	kontrola	k1gFnSc7	kontrola
londýnské	londýnský	k2eAgNnSc4d1	Londýnské
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
pro	pro	k7c4	pro
kolonie	kolonie	k1gFnPc4	kolonie
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
především	především	k9	především
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
,	,	kIx,	,
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
přepravu	přeprava	k1gFnSc4	přeprava
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
pak	pak	k6eAd1	pak
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
plánování	plánování	k1gNnSc2	plánování
<g/>
,	,	kIx,	,
porad	porada	k1gFnPc2	porada
a	a	k8xC	a
hlasování	hlasování	k1gNnSc2	hlasování
vzniká	vznikat	k5eAaImIp3nS	vznikat
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dominium	dominium	k1gNnSc1	dominium
Britské	britský	k2eAgFnSc2d1	britská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
rodí	rodit	k5eAaImIp3nS	rodit
Australský	australský	k2eAgInSc1d1	australský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
z	z	k7c2	z
části	část	k1gFnSc2	část
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
vzniká	vznikat	k5eAaImIp3nS	vznikat
Teritorium	teritorium	k1gNnSc4	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
ACT	ACT	kA	ACT
<g/>
,	,	kIx,	,
Australian	Australian	k1gMnSc1	Australian
Capital	Capital	k1gMnSc1	Capital
Territory	Territor	k1gInPc7	Territor
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
území	území	k1gNnSc4	území
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgNnSc2d1	nové
federálního	federální	k2eAgNnSc2d1	federální
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Canberry	Canberra	k1gFnSc2	Canberra
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1901	[number]	k4	1901
a	a	k8xC	a
1927	[number]	k4	1927
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
městem	město	k1gNnSc7	město
Melbourne	Melbourne	k1gNnSc1	Melbourne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc4d1	severní
teritorium	teritorium	k1gNnSc4	teritorium
je	být	k5eAaImIp3nS	být
pod	pod	k7c4	pod
pravomoci	pravomoc	k1gFnPc4	pravomoc
jihoaustralské	jihoaustralský	k2eAgFnSc2d1	jihoaustralský
vlády	vláda	k1gFnSc2	vláda
pod	pod	k7c4	pod
pravomoc	pravomoc	k1gFnSc4	pravomoc
vlády	vláda	k1gFnSc2	vláda
federální	federální	k2eAgFnSc2d1	federální
převedeno	převést	k5eAaPmNgNnS	převést
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Gallipoli	Gallipoli	k1gNnSc6	Gallipoli
mnoho	mnoho	k4c1	mnoho
Australanů	Australan	k1gMnPc2	Australan
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
národa	národ	k1gInSc2	národ
-	-	kIx~	-
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akci	akce	k1gFnSc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
i	i	k9	i
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
Kokoda	Kokoda	k1gMnSc1	Kokoda
Track	Track	k1gMnSc1	Track
Campaign	Campaign	k1gMnSc1	Campaign
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
přeložitelné	přeložitelný	k2eAgInPc1d1	přeložitelný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Tažení	tažení	k1gNnSc1	tažení
po	po	k7c6	po
Kokodské	Kokodský	k2eAgFnSc6d1	Kokodský
stezce	stezka	k1gFnSc6	stezka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gInSc4	on
Austrálie	Austrálie	k1gFnSc1	Austrálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
ukončil	ukončit	k5eAaPmAgInS	ukončit
tzv.	tzv.	kA	tzv.
Statute	statut	k1gInSc5	statut
of	of	k?	of
Westminster	Westminstra	k1gFnPc2	Westminstra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
většinu	většina	k1gFnSc4	většina
ústavních	ústavní	k2eAgFnPc2d1	ústavní
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Otřes	otřes	k1gInSc1	otřes
způsobený	způsobený	k2eAgInSc1d1	způsobený
britskou	britský	k2eAgFnSc7d1	britská
porážkou	porážka	k1gFnSc7	porážka
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
a	a	k8xC	a
hrozby	hrozba	k1gFnPc1	hrozba
japonské	japonský	k2eAgFnPc1d1	japonská
invaze	invaze	k1gFnPc1	invaze
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
obrátila	obrátit	k5eAaPmAgFnS	obrátit
jako	jako	k9	jako
na	na	k7c4	na
nového	nový	k2eAgMnSc4d1	nový
spojence	spojenec	k1gMnSc4	spojenec
a	a	k8xC	a
ochránce	ochránce	k1gMnSc4	ochránce
na	na	k7c4	na
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oficiálně	oficiálně	k6eAd1	oficiálně
stala	stát	k5eAaPmAgFnS	stát
americkým	americký	k2eAgMnSc7d1	americký
vojenským	vojenský	k2eAgMnSc7d1	vojenský
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zahájila	zahájit	k5eAaPmAgFnS	zahájit
podporu	podpora	k1gFnSc4	podpora
hromadné	hromadný	k2eAgFnSc2d1	hromadná
imigrace	imigrace	k1gFnSc2	imigrace
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
politiky	politika	k1gFnSc2	politika
bílé	bílý	k2eAgFnSc2d1	bílá
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
"	"	kIx"	"
i	i	k9	i
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
radikální	radikální	k2eAgFnSc4d1	radikální
přeměnu	přeměna	k1gFnSc4	přeměna
australského	australský	k2eAgInSc2d1	australský
demografického	demografický	k2eAgInSc2d1	demografický
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
kultury	kultura	k1gFnSc2	kultura
i	i	k8xC	i
vnímání	vnímání	k1gNnSc2	vnímání
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
ústavní	ústavní	k2eAgFnPc1d1	ústavní
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
přijetím	přijetí	k1gNnSc7	přijetí
tzv.	tzv.	kA	tzv.
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
Austrálii	Austrálie	k1gFnSc6	Austrálie
odstraňujícího	odstraňující	k2eAgInSc2d1	odstraňující
všechny	všechen	k3xTgInPc1	všechen
britské	britský	k2eAgInPc1d1	britský
vlivy	vliv	k1gInPc1	vliv
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
australských	australský	k2eAgInPc6d1	australský
státech	stát	k1gInPc6	stát
a	a	k8xC	a
rušícího	rušící	k2eAgInSc2d1	rušící
soudní	soudní	k2eAgFnSc4d1	soudní
odvolatelnost	odvolatelnost	k1gFnSc4	odvolatelnost
k	k	k7c3	k
orgánům	orgán	k1gInPc3	orgán
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Australští	australský	k2eAgMnPc1d1	australský
voliči	volič	k1gMnPc1	volič
však	však	k9	však
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
republikánskému	republikánský	k2eAgNnSc3d1	republikánské
zřízení	zřízení	k1gNnSc3	zřízení
55	[number]	k4	55
<g/>
%	%	kIx~	%
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
Gougha	Gough	k1gMnSc2	Gough
Whitlama	Whitlam	k1gMnSc2	Whitlam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
věnována	věnován	k2eAgFnSc1d1	věnována
budoucnosti	budoucnost	k1gFnSc2	budoucnost
Austrálie	Austrálie	k1gFnSc2	Austrálie
jako	jako	k8xS	jako
součásti	součást	k1gFnPc4	součást
regionu	region	k1gInSc2	region
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
7,617,930	[number]	k4	7,617,930
km2	km2	k4	km2
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Austrálie	Austrálie	k1gFnSc2	Austrálie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Indoaustralské	Indoaustralský	k2eAgFnSc6d1	Indoaustralský
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Obklopena	obklopen	k2eAgFnSc1d1	obklopena
je	být	k5eAaImIp3nS	být
Indickým	indický	k2eAgMnSc7d1	indický
<g/>
,	,	kIx,	,
Jižním	jižní	k2eAgMnSc7d1	jižní
a	a	k8xC	a
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Asie	Asie	k1gFnSc2	Asie
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
Arafurským	Arafurský	k2eAgNnSc7d1	Arafurské
a	a	k8xC	a
Timorským	Timorský	k2eAgNnSc7d1	Timorské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
25	[number]	k4	25
760	[number]	k4	760
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
bariérový	bariérový	k2eAgInSc1d1	bariérový
útes	útes	k1gInSc1	útes
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
korálovým	korálový	k2eAgInSc7d1	korálový
útesem	útes	k1gInSc7	útes
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
severovýchodnímu	severovýchodní	k2eAgNnSc3d1	severovýchodní
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
světovým	světový	k2eAgInSc7d1	světový
monolitem	monolit	k1gInSc7	monolit
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gMnSc1	Mount
Augustus	Augustus	k1gMnSc1	Augustus
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
je	být	k5eAaImIp3nS	být
Uluru	Ulura	k1gFnSc4	Ulura
známý	známý	k1gMnSc1	známý
též	též	k9	též
jako	jako	k9	jako
Ayerova	Ayerův	k2eAgFnSc1d1	Ayerova
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
2	[number]	k4	2
228	[number]	k4	228
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
Mount	Mount	k1gInSc4	Mount
Kosciuszko	Kosciuszka	k1gFnSc5	Kosciuszka
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
předělovém	předělový	k2eAgNnSc6d1	předělové
pohoří	pohoří	k1gNnSc6	pohoří
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Austrálie	Austrálie	k1gFnSc2	Austrálie
jako	jako	k8xC	jako
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
státu	stát	k1gInSc3	stát
<g/>
:	:	kIx,	:
Mawson	Mawson	k1gMnSc1	Mawson
Peak	Peak	k1gMnSc1	Peak
na	na	k7c6	na
Heardově	Heardův	k2eAgInSc6d1	Heardův
ostrově	ostrov	k1gInSc6	ostrov
jej	on	k3xPp3gMnSc4	on
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
o	o	k7c4	o
517	[number]	k4	517
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
plochu	plocha	k1gFnSc4	plocha
zabírají	zabírat	k5eAaImIp3nP	zabírat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
pouště	poušť	k1gFnSc2	poušť
a	a	k8xC	a
polopouště	polopoušť	k1gFnSc2	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
nejsušším	suchý	k2eAgInSc7d3	nejsušší
a	a	k8xC	a
nejplošším	plochý	k2eAgInSc7d3	nejplošší
obydleným	obydlený	k2eAgInSc7d1	obydlený
kontinentem	kontinent	k1gInSc7	kontinent
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgNnSc4d3	nejmenší
množství	množství	k1gNnSc4	množství
úrodné	úrodný	k2eAgFnSc2d1	úrodná
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgNnSc4d1	mírné
klima	klima	k1gNnSc4	klima
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc4	klima
tropické	tropický	k2eAgFnSc2d1	tropická
<g/>
,	,	kIx,	,
vegetace	vegetace	k1gFnSc2	vegetace
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
deštných	deštný	k2eAgInPc2d1	deštný
i	i	k8xC	i
obyčejných	obyčejný	k2eAgInPc2d1	obyčejný
lesů	les	k1gInPc2	les
a	a	k8xC	a
mangrovových	mangrovový	k2eAgFnPc2d1	mangrovová
bažin	bažina	k1gFnPc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
celek	celek	k1gInSc4	celek
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
velmi	velmi	k6eAd1	velmi
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
oceánem	oceán	k1gInSc7	oceán
včetně	včetně	k7c2	včetně
jevu	jev	k1gInSc2	jev
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gFnSc4	Niñ
způsobujícího	způsobující	k2eAgInSc2d1	způsobující
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
sezónními	sezónní	k2eAgInPc7d1	sezónní
systémy	systém	k1gInPc7	systém
tlakových	tlakový	k2eAgFnPc2d1	tlaková
níží	níže	k1gFnPc2	níže
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
cyklóny	cyklóna	k1gFnPc1	cyklóna
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
má	mít	k5eAaImIp3nS	mít
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
australského	australský	k2eAgInSc2d1	australský
povrchu	povrch	k1gInSc2	povrch
pouštní	pouštní	k2eAgMnSc1d1	pouštní
nebo	nebo	k8xC	nebo
polopouštní	polopouštní	k2eAgInSc1d1	polopouštní
charakter	charakter	k1gInSc1	charakter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
rozvinuto	rozvinout	k5eAaPmNgNnS	rozvinout
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
prostředí	prostředí	k1gNnSc4	prostředí
od	od	k7c2	od
alpinských	alpinský	k2eAgNnPc2d1	alpinské
vřesovišť	vřesoviště	k1gNnPc2	vřesoviště
po	po	k7c4	po
tropické	tropický	k2eAgInPc4d1	tropický
deštné	deštný	k2eAgInPc4d1	deštný
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
geografické	geografický	k2eAgNnSc1d1	geografické
odloučení	odloučení	k1gNnSc1	odloučení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
australských	australský	k2eAgInPc2d1	australský
organismů	organismus	k1gInPc2	organismus
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
unikátních	unikátní	k2eAgInPc2d1	unikátní
<g/>
.	.	kIx.	.
85	[number]	k4	85
%	%	kIx~	%
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
84	[number]	k4	84
%	%	kIx~	%
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
45	[number]	k4	45
%	%	kIx~	%
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
89	[number]	k4	89
%	%	kIx~	%
procent	procento	k1gNnPc2	procento
příbřežních	příbřežní	k2eAgFnPc2d1	příbřežní
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
endemických	endemický	k2eAgFnPc2d1	endemická
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
australských	australský	k2eAgInPc2d1	australský
ekoregionů	ekoregion	k1gInPc2	ekoregion
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
lidskou	lidský	k2eAgFnSc7d1	lidská
aktivitou	aktivita	k1gFnSc7	aktivita
a	a	k8xC	a
člověkem	člověk	k1gMnSc7	člověk
vysazenými	vysazený	k2eAgNnPc7d1	vysazené
zvířaty	zvíře	k1gNnPc7	zvíře
či	či	k8xC	či
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ramsarské	Ramsarský	k2eAgFnSc2d1	Ramsarská
úmluvy	úmluva	k1gFnSc2	úmluva
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
64	[number]	k4	64
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
,	,	kIx,	,
16	[number]	k4	16
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
jako	jako	k8xS	jako
Světové	světový	k2eAgNnSc1d1	světové
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
australských	australský	k2eAgFnPc2d1	australská
dřevin	dřevina	k1gFnPc2	dřevina
je	být	k5eAaImIp3nS	být
vždyzelených	vždyzelený	k2eAgFnPc2d1	vždyzelená
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS	přizpůsobit
vzdorování	vzdorování	k1gNnSc3	vzdorování
ohni	oheň	k1gInSc3	oheň
nebo	nebo	k8xC	nebo
suchu	sucho	k1gNnSc3	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
rostliny	rostlina	k1gFnPc4	rostlina
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
eukalypty	eukalypt	k1gInPc1	eukalypt
nebo	nebo	k8xC	nebo
akácie	akácie	k1gFnPc1	akácie
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
endemické	endemický	k2eAgInPc4d1	endemický
druhy	druh	k1gInPc4	druh
luštěnin	luštěnina	k1gFnPc2	luštěnina
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
symbióze	symbióza	k1gFnSc3	symbióza
s	s	k7c7	s
bakteriemi	bakterie	k1gFnPc7	bakterie
rhizobia	rhizobium	k1gNnSc2	rhizobium
a	a	k8xC	a
mykorhizujícími	mykorhizující	k2eAgFnPc7d1	mykorhizující
houbami	houba	k1gFnPc7	houba
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
na	na	k7c4	na
živiny	živina	k1gFnPc1	živina
chudých	chudý	k2eAgFnPc6d1	chudá
zeminách	zemina	k1gFnPc6	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
australská	australský	k2eAgFnSc1d1	australská
fauna	fauna	k1gFnSc1	fauna
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ptakořitné	ptakořitný	k2eAgNnSc1d1	ptakořitný
(	(	kIx(	(
<g/>
ptakopyska	ptakopysk	k1gMnSc4	ptakopysk
a	a	k8xC	a
ježury	ježura	k1gFnPc4	ježura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vačnatců	vačnatec	k1gMnPc2	vačnatec
včetně	včetně	k7c2	včetně
klokanů	klokan	k1gMnPc2	klokan
<g/>
,	,	kIx,	,
koaly	koala	k1gFnSc2	koala
a	a	k8xC	a
wombata	wombat	k1gMnSc2	wombat
nebo	nebo	k8xC	nebo
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
emu	emu	k1gMnPc1	emu
<g/>
,	,	kIx,	,
ledňáci	ledňák	k1gMnPc1	ledňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dingo	dingo	k1gMnSc1	dingo
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
zavlečen	zavlečen	k2eAgInSc4d1	zavlečen
austronésany	austronésan	k1gInPc7	austronésan
obchodujícími	obchodující	k2eAgInPc7d1	obchodující
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mnoho	mnoho	k4c1	mnoho
rostlin	rostlina	k1gFnPc2	rostlina
i	i	k8xC	i
živočichů	živočich	k1gMnPc2	živočich
vyhynulo	vyhynout	k5eAaPmAgNnS	vyhynout
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
lidského	lidský	k2eAgNnSc2d1	lidské
osídlování	osídlování	k1gNnSc2	osídlování
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
vyhynulo	vyhynout	k5eAaPmAgNnS	vyhynout
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vakovlk	vakovlk	k6eAd1	vakovlk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Australský	australský	k2eAgInSc1d1	australský
svaz	svaz	k1gInSc1	svaz
je	být	k5eAaImIp3nS	být
federální	federální	k2eAgFnSc7d1	federální
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
s	s	k7c7	s
parlamentním	parlamentní	k2eAgInSc7d1	parlamentní
systémem	systém	k1gInSc7	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Královnou	královna	k1gFnSc7	královna
Austrálie	Austrálie	k1gFnSc2	Austrálie
je	být	k5eAaImIp3nS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
role	role	k1gFnSc1	role
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
její	její	k3xOp3gFnSc4	její
roli	role	k1gFnSc4	role
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zastupovaná	zastupovaný	k2eAgFnSc1d1	zastupovaná
je	být	k5eAaImIp3nS	být
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
guvernéry	guvernér	k1gMnPc4	guvernér
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
státní	státní	k2eAgFnSc6d1	státní
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
dává	dávat	k5eAaImIp3nS	dávat
ústava	ústava	k1gFnSc1	ústava
generálnímu	generální	k2eAgMnSc3d1	generální
guvernérovi	guvernér	k1gMnSc3	guvernér
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
okolností	okolnost	k1gFnPc2	okolnost
ji	on	k3xPp3gFnSc4	on
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
uplatněním	uplatnění	k1gNnSc7	uplatnění
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
bylo	být	k5eAaImAgNnS	být
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Whitlamovy	Whitlamův	k2eAgFnSc2d1	Whitlamův
vlády	vláda	k1gFnSc2	vláda
během	během	k7c2	během
ústavní	ústavní	k2eAgFnSc2d1	ústavní
krize	krize	k1gFnSc2	krize
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
moc	moc	k6eAd1	moc
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
-	-	kIx~	-
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
ji	on	k3xPp3gFnSc4	on
Australský	australský	k2eAgInSc1d1	australský
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
(	(	kIx(	(
<g/>
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
souhlas	souhlas	k1gInSc4	souhlas
či	či	k8xC	či
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
schvalovanými	schvalovaný	k2eAgInPc7d1	schvalovaný
zákony	zákon	k1gInPc7	zákon
moc	moc	k6eAd1	moc
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
-	-	kIx~	-
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
<g />
.	.	kIx.	.
</s>
<s>
ji	on	k3xPp3gFnSc4	on
Federální	federální	k2eAgFnSc1d1	federální
výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Federal	Federal	k1gFnSc1	Federal
Executive	Executiv	k1gInSc5	Executiv
Council	Councila	k1gFnPc2	Councila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
a	a	k8xC	a
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
především	především	k9	především
jen	jen	k9	jen
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgMnPc1d1	soudní
-	-	kIx~	-
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
ji	on	k3xPp3gFnSc4	on
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
níže	nízce	k6eAd2	nízce
postavené	postavený	k2eAgInPc1d1	postavený
soudy	soud	k1gInPc1	soud
<g/>
;	;	kIx,	;
na	na	k7c6	na
orgánech	orgán	k1gInPc6	orgán
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
jsou	být	k5eAaImIp3nP	být
formálně	formálně	k6eAd1	formálně
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
Dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
svazový	svazový	k2eAgInSc1d1	svazový
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
Senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnPc1d1	horní
komory	komora	k1gFnPc1	komora
<g/>
)	)	kIx)	)
se	s	k7c7	s
76	[number]	k4	76
senátory	senátor	k1gMnPc7	senátor
<g/>
,	,	kIx,	,
Sněmovny	sněmovna	k1gFnPc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
<g/>
)	)	kIx)	)
se	s	k7c7	s
150	[number]	k4	150
členy	člen	k1gMnPc7	člen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
komoře	komora	k1gFnSc6	komora
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc4	každý
stát	stát	k1gInSc4	stát
dvanáct	dvanáct	k4xCc1	dvanáct
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
teritoria	teritorium	k1gNnPc1	teritorium
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc4	volba
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každé	každý	k3xTgInPc1	každý
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
polovina	polovina	k1gFnSc1	polovina
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
senátora	senátor	k1gMnSc2	senátor
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
podporou	podpora	k1gFnSc7	podpora
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vládu	vláda	k1gFnSc4	vláda
vedenou	vedený	k2eAgFnSc4d1	vedená
premiérem	premiér	k1gMnSc7	premiér
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc7	první
ministrem	ministr	k1gMnSc7	ministr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
jsou	být	k5eAaImIp3nP	být
povinné	povinný	k2eAgFnPc1d1	povinná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
občany	občan	k1gMnPc4	občan
starší	starý	k2eAgMnPc1d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nP	působit
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
:	:	kIx,	:
Australská	australský	k2eAgFnSc1d1	australská
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Australská	australský	k2eAgFnSc1d1	australská
národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
komoře	komora	k1gFnSc6	komora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
i	i	k9	i
několika	několik	k4yIc7	několik
nezávislými	závislý	k2eNgInPc7d1	nezávislý
a	a	k8xC	a
menším	malý	k2eAgFnPc3d2	menší
stranám	strana	k1gFnPc3	strana
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
nebo	nebo	k8xC	nebo
Australští	australský	k2eAgMnPc1d1	australský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vládla	vládnout	k5eAaImAgFnS	vládnout
koalice	koalice	k1gFnSc1	koalice
národní	národní	k2eAgFnSc2d1	národní
a	a	k8xC	a
liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Howardem	Howard	k1gMnSc7	Howard
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
koalice	koalice	k1gFnSc1	koalice
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
i	i	k9	i
senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgNnSc7	první
uskupením	uskupení	k1gNnSc7	uskupení
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
během	během	k7c2	během
mandátu	mandát	k1gInSc2	mandát
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
však	však	k9	však
ovládala	ovládat	k5eAaImAgFnS	ovládat
státy	stát	k1gInPc4	stát
a	a	k8xC	a
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
federálních	federální	k2eAgFnPc6d1	federální
volbách	volba	k1gFnPc6	volba
Australská	australský	k2eAgFnSc1d1	australská
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lídr	lídr	k1gMnSc1	lídr
strany	strana	k1gFnSc2	strana
Kevin	Kevin	k1gMnSc1	Kevin
Rudd	Rudd	k1gMnSc1	Rudd
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
však	však	k9	však
na	na	k7c6	na
vnitrostranické	vnitrostranický	k2eAgFnSc6d1	vnitrostranická
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
strany	strana	k1gFnSc2	strana
vystřídán	vystřídat	k5eAaPmNgMnS	vystřídat
Julií	Julie	k1gFnSc7	Julie
Gillard	Gillarda	k1gFnPc2	Gillarda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
současně	současně	k6eAd1	současně
nahradila	nahradit	k5eAaPmAgFnS	nahradit
i	i	k9	i
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
první	první	k4xOgFnSc7	první
premiérkou	premiérka	k1gFnSc7	premiérka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kevin	Kevin	k2eAgInSc1d1	Kevin
Rudd	Rudd	k1gInSc1	Rudd
se	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
nakrátko	nakrátko	k6eAd1	nakrátko
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgMnS	stát
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Tony	Tony	k1gMnSc1	Tony
Abbott	Abbott	k1gMnSc1	Abbott
<g/>
,	,	kIx,	,
když	když	k8xS	když
Liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
Austrálie	Austrálie	k1gFnSc2	Austrálie
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
zářijových	zářijový	k2eAgFnPc6d1	zářijová
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
jej	on	k3xPp3gNnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
vnitrostranický	vnitrostranický	k2eAgMnSc1d1	vnitrostranický
rival	rival	k1gMnSc1	rival
Malcolm	Malcolm	k1gMnSc1	Malcolm
Turnbull	Turnbull	k1gMnSc1	Turnbull
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Australské	australský	k2eAgInPc4d1	australský
státy	stát	k1gInPc4	stát
a	a	k8xC	a
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
šesti	šest	k4xCc2	šest
států	stát	k1gInPc2	stát
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
pevninských	pevninský	k2eAgInPc2d1	pevninský
a	a	k8xC	a
několika	několik	k4yIc2	několik
malých	malý	k2eAgNnPc2d1	malé
ostrovních	ostrovní	k2eAgNnPc2d1	ostrovní
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
Queensland	Queensland	k1gInSc1	Queensland
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
<g/>
,	,	kIx,	,
Victoria	Victorium	k1gNnSc2	Victorium
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Dvěma	dva	k4xCgInPc7	dva
velkými	velký	k2eAgInPc7d1	velký
pevninskými	pevninský	k2eAgInPc7d1	pevninský
teritorii	teritorium	k1gNnPc7	teritorium
pak	pak	k6eAd1	pak
Severní	severní	k2eAgNnSc4d1	severní
teritorium	teritorium	k1gNnSc4	teritorium
a	a	k8xC	a
Teritorium	teritorium	k1gNnSc4	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Teritoria	teritorium	k1gNnPc1	teritorium
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
fungují	fungovat	k5eAaImIp3nP	fungovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
svazový	svazový	k2eAgInSc1d1	svazový
parlament	parlament	k1gInSc1	parlament
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
veškerou	veškerý	k3xTgFnSc4	veškerý
jejich	jejich	k3xOp3gFnSc4	jejich
legislativu	legislativa	k1gFnSc4	legislativa
upravit	upravit	k5eAaPmF	upravit
<g/>
,	,	kIx,	,
změnit	změnit	k5eAaPmF	změnit
nebo	nebo	k8xC	nebo
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
federální	federální	k2eAgFnSc1d1	federální
legislativa	legislativa	k1gFnSc1	legislativa
nadřazena	nadřazen	k2eAgFnSc1d1	nadřazena
kromě	kromě	k7c2	kromě
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
vyjmenovaných	vyjmenovaný	k2eAgFnPc2d1	vyjmenovaná
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
i	i	k8xC	i
legislativám	legislativa	k1gFnPc3	legislativa
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
legislativní	legislativní	k2eAgFnSc7d1	legislativní
pravomocí	pravomoc	k1gFnSc7	pravomoc
státních	státní	k2eAgInPc2d1	státní
parlamentů	parlament	k1gInPc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
správu	správa	k1gFnSc4	správa
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
,	,	kIx,	,
přepravy	přeprava	k1gFnSc2	přeprava
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
i	i	k8xC	i
teritorium	teritorium	k1gNnSc1	teritorium
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
legislativní	legislativní	k2eAgInSc4d1	legislativní
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Severního	severní	k2eAgNnSc2d1	severní
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
Teritoria	teritorium	k1gNnSc2	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
Queenslandu	Queensland	k1gInSc2	Queensland
<g/>
,	,	kIx,	,
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1	dolní
komory	komora	k1gFnPc1	komora
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
Zákonodárná	zákonodárný	k2eAgNnPc1d1	zákonodárné
shromáždění	shromáždění	k1gNnPc1	shromáždění
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnPc1d1	horní
komory	komora	k1gFnPc1	komora
pak	pak	k6eAd1	pak
jako	jako	k9	jako
Zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Hlavami	hlava	k1gFnPc7	hlava
států	stát	k1gInPc2	stát
i	i	k8xC	i
teritorií	teritorium	k1gNnPc2	teritorium
jsou	být	k5eAaImIp3nP	být
premiéři	premiér	k1gMnPc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státě	stát	k1gInSc6	stát
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
,	,	kIx,	,
v	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
Severního	severní	k2eAgNnSc2d1	severní
teritoria	teritorium	k1gNnSc2	teritorium
administrátorem	administrátor	k1gMnSc7	administrátor
a	a	k8xC	a
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
v	v	k7c6	v
Teritoriu	teritorium	k1gNnSc6	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
několik	několik	k4yIc4	několik
malých	malý	k2eAgNnPc2d1	malé
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
spravuje	spravovat	k5eAaImIp3nS	spravovat
Teritorium	teritorium	k1gNnSc1	teritorium
Jervis	Jervis	k1gFnSc2	Jervis
Bay	Bay	k1gFnSc2	Bay
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
námořní	námořní	k2eAgFnSc1d1	námořní
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
tzv.	tzv.	kA	tzv.
vnější	vnější	k2eAgNnPc1d1	vnější
obydlená	obydlený	k2eAgNnPc1d1	obydlené
teritoria	teritorium	k1gNnPc1	teritorium
<g/>
:	:	kIx,	:
Norfolk	Norfolk	k1gInSc1	Norfolk
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgInSc1d1	vánoční
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Kokosové	kokosový	k2eAgInPc1d1	kokosový
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
několik	několik	k4yIc1	několik
neobydlených	obydlený	k2eNgFnPc2d1	neobydlená
<g/>
:	:	kIx,	:
Ashmorův	Ashmorův	k2eAgInSc1d1	Ashmorův
a	a	k8xC	a
Cartierův	Cartierův	k2eAgInSc1d1	Cartierův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Ostrovy	ostrov	k1gInPc1	ostrov
Korálového	korálový	k2eAgNnSc2d1	korálové
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Heardův	Heardův	k2eAgMnSc1d1	Heardův
a	a	k8xC	a
MacDonaldovy	Macdonaldův	k2eAgInPc1d1	Macdonaldův
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Australské	australský	k2eAgNnSc4d1	Australské
antarktické	antarktický	k2eAgNnSc4d1	antarktické
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
vyspělý	vyspělý	k2eAgInSc1d1	vyspělý
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
a	a	k8xC	a
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
s	s	k7c7	s
bohatými	bohatý	k2eAgFnPc7d1	bohatá
zásobami	zásoba	k1gFnPc7	zásoba
a	a	k8xC	a
významnou	významný	k2eAgFnSc7d1	významná
těžbou	těžba	k1gFnSc7	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
černé	černé	k1gNnSc1	černé
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
Queenslandu	Queensland	k1gInSc6	Queensland
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
ve	v	k7c6	v
Victorii	Victorie	k1gFnSc6	Victorie
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bauxit	bauxit	k1gInSc1	bauxit
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
10,1	[number]	k4	10,1
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc1	palladium
<g/>
,	,	kIx,	,
zirkon	zirkon	k1gInSc1	zirkon
<g/>
,	,	kIx,	,
rutil	rutil	k1gInSc1	rutil
<g/>
,	,	kIx,	,
ilmenit	ilmenit	k1gInSc1	ilmenit
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
a	a	k8xC	a
opály	opál	k1gInPc1	opál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
patří	patřit	k5eAaImIp3nP	patřit
hutnictví	hutnictví	k1gNnPc4	hutnictví
(	(	kIx(	(
<g/>
surové	surový	k2eAgNnSc4d1	surové
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
ocel	ocel	k1gFnSc4	ocel
<g/>
,	,	kIx,	,
barevné	barevný	k2eAgInPc4d1	barevný
kovy	kov	k1gInPc4	kov
a	a	k8xC	a
hliník	hliník	k1gInSc1	hliník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
petrochemie	petrochemie	k1gFnSc1	petrochemie
(	(	kIx(	(
<g/>
rafinérie	rafinérie	k1gFnSc1	rafinérie
ropy	ropa	k1gFnSc2	ropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc1	chemie
(	(	kIx(	(
<g/>
plastické	plastický	k2eAgFnSc2d1	plastická
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
superfosfáty	superfosfát	k1gInPc1	superfosfát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
aut	auto	k1gNnPc2	auto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
centra	centrum	k1gNnPc1	centrum
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
aglomeracích	aglomerace	k1gFnPc6	aglomerace
Sydney	Sydney	k1gNnSc2	Sydney
a	a	k8xC	a
Melbourne	Melbourne	k1gNnSc2	Melbourne
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
Perth	Pertha	k1gFnPc2	Pertha
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gMnSc5	Adelaid
<g/>
,	,	kIx,	,
Newcastle	Newcastle	k1gFnPc2	Newcastle
a	a	k8xC	a
Geelong	Geelonga	k1gFnPc2	Geelonga
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
jen	jen	k9	jen
6,5	[number]	k4	6,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
54	[number]	k4	54
%	%	kIx~	%
a	a	k8xC	a
lesy	les	k1gInPc1	les
19	[number]	k4	19
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sklízí	sklízet	k5eAaImIp3nS	sklízet
se	se	k3xPyFc4	se
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
sorgo	sorgo	k1gNnSc1	sorgo
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
řepka	řepka	k1gFnSc1	řepka
<g/>
,	,	kIx,	,
sója	sója	k1gFnSc1	sója
<g/>
,	,	kIx,	,
vinné	vinný	k2eAgInPc4d1	vinný
hrozny	hrozen	k1gInPc4	hrozen
<g/>
,	,	kIx,	,
chmel	chmel	k1gInSc4	chmel
a	a	k8xC	a
tabák	tabák	k1gInSc4	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
pro	pro	k7c4	pro
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc4	chov
ovcí	ovce	k1gFnPc2	ovce
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
000	[number]	k4	000
000	[number]	k4	000
což	což	k3yQnSc4	což
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
x	x	k?	x
více	hodně	k6eAd2	hodně
než	než	k8xS	než
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
produkce	produkce	k1gFnSc2	produkce
surové	surový	k2eAgFnSc2d1	surová
a	a	k8xC	a
prané	praný	k2eAgFnSc2d1	praná
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
ovčích	ovčí	k2eAgFnPc2d1	ovčí
kůží	kůže	k1gFnPc2	kůže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rozlehlosti	rozlehlost	k1gFnSc2	rozlehlost
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
zejména	zejména	k9	zejména
letecká	letecký	k2eAgFnSc1d1	letecká
a	a	k8xC	a
silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
také	také	k9	také
doprava	doprava	k1gFnSc1	doprava
železniční	železniční	k2eAgFnSc1d1	železniční
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
osídlenou	osídlený	k2eAgFnSc7d1	osídlená
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
o	o	k7c6	o
dvojnásobné	dvojnásobný	k2eAgFnSc6d1	dvojnásobná
rozloze	rozloha	k1gFnSc6	rozloha
Indie	Indie	k1gFnSc2	Indie
obývá	obývat	k5eAaImIp3nS	obývat
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
stoupnout	stoupnout	k5eAaPmF	stoupnout
až	až	k9	až
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
42	[number]	k4	42
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2050	[number]	k4	2050
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
současný	současný	k2eAgInSc1d1	současný
migrační	migrační	k2eAgInSc1d1	migrační
trend	trend	k1gInSc1	trend
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
plánuje	plánovat	k5eAaImIp3nS	plánovat
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
2,8	[number]	k4	2,8
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
85	[number]	k4	85
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
evropskému	evropský	k2eAgInSc3d1	evropský
původu	původ	k1gInSc3	původ
a	a	k8xC	a
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
skupin	skupina	k1gFnPc2	skupina
převažují	převažovat	k5eAaImIp3nP	převažovat
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
517	[number]	k4	517
000	[number]	k4	000
obývají	obývat	k5eAaImIp3nP	obývat
především	především	k9	především
Severní	severní	k2eAgNnSc4d1	severní
teritorium	teritorium	k1gNnSc4	teritorium
a	a	k8xC	a
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Torresově	Torresův	k2eAgInSc6d1	Torresův
průlivu	průliv	k1gInSc6	průliv
<g/>
.	.	kIx.	.
89	[number]	k4	89
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Austrálie	Austrálie	k1gFnSc1	Austrálie
žádný	žádný	k3yNgInSc4	žádný
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pevně	pevně	k6eAd1	pevně
zakotvena	zakotven	k2eAgFnSc1d1	zakotvena
angličtina	angličtina	k1gFnSc1	angličtina
de	de	k?	de
facto	facto	k1gNnSc1	facto
jako	jako	k8xC	jako
národní	národní	k2eAgInSc1d1	národní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posledním	poslední	k2eAgNnSc6d1	poslední
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	s	k7c7	s
64	[number]	k4	64
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
za	za	k7c4	za
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
katolíků	katolík	k1gMnPc2	katolík
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
chaldejští	chaldejský	k2eAgMnPc1d1	chaldejský
katolíci	katolík	k1gMnPc1	katolík
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc1	eparchie
sv.	sv.	kA	sv.
<g/>
Tomáše	Tomáš	k1gMnSc2	Tomáš
Apoštola	apoštol	k1gMnSc2	apoštol
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
-	-	kIx~	-
32	[number]	k4	32
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
<g />
.	.	kIx.	.
</s>
<s>
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
:	:	kIx,	:
2014	[number]	k4	2014
-	-	kIx~	-
39	[number]	k4	39
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
35	[number]	k4	35
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
*	*	kIx~	*
2	[number]	k4	2
<g/>
)	)	kIx)	)
maronité	maronitý	k2eAgFnSc2d1	maronitý
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
sv.	sv.	kA	sv.
Marona	Maron	k1gMnSc2	Maron
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
-	-	kIx~	-
125	[number]	k4	125
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
160	[number]	k4	160
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
-	-	kIx~	-
150	[number]	k4	150
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
154	[number]	k4	154
500	[number]	k4	500
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
150	[number]	k4	150
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
*	*	kIx~	*
3	[number]	k4	3
<g/>
)	)	kIx)	)
syromalabarští	syromalabarský	k2eAgMnPc1d1	syromalabarský
katolíci	katolík	k1gMnPc1	katolík
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
50	[number]	k4	50
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
*	*	kIx~	*
4	[number]	k4	4
<g/>
)	)	kIx)	)
řeckokatolíci-melchité	řeckokatolícielchitý	k2eAgFnSc2d1	řeckokatolíci-melchitý
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
sv.	sv.	kA	sv.
Michaela	Michael	k1gMnSc2	Michael
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
-	-	kIx~	-
40	[number]	k4	40
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
-	-	kIx~	-
45	[number]	k4	45
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
52	[number]	k4	52
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
52	[number]	k4	52
900	[number]	k4	900
<g/>
,	,	kIx,	,
*	*	kIx~	*
5	[number]	k4	5
<g/>
)	)	kIx)	)
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc1	eparchie
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
Pavla	Pavla	k1gFnSc1	Pavla
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
-	-	kIx~	-
25	[number]	k4	25
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
-	-	kIx~	-
35	[number]	k4	35
400	[number]	k4	400
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
i	i	k8xC	i
2014	[number]	k4	2014
-	-	kIx~	-
32	[number]	k4	32
500	[number]	k4	500
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
33	[number]	k4	33
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
personální	personální	k2eAgInSc1d1	personální
ordinariát	ordinariát	k1gInSc1	ordinariát
pro	pro	k7c4	pro
římskokatolíky	římskokatolík	k1gInPc4	římskokatolík
anglikán	anglikán	k1gMnSc1	anglikán
<g/>
.	.	kIx.	.
ritu	rit	k1gInSc2	rit
(	(	kIx(	(
<g/>
bývalé	bývalý	k2eAgMnPc4d1	bývalý
anglikány	anglikán	k1gMnPc4	anglikán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Person	persona	k1gFnPc2	persona
<g/>
.	.	kIx.	.
ordinariát	ordinariát	k1gInSc1	ordinariát
Our	Our	k1gFnSc2	Our
Lady	Lada	k1gFnSc2	Lada
(	(	kIx(	(
<g/>
Naší	náš	k3xOp1gFnSc7	náš
Paní	paní	k1gFnSc7	paní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
Jižního	jižní	k2eAgInSc2d1	jižní
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
měl	mít	k5eAaImAgInS	mít
300	[number]	k4	300
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
7	[number]	k4	7
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
4	[number]	k4	4
farnostech	farnost	k1gFnPc6	farnost
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
:	:	kIx,	:
2013	[number]	k4	2013
-	-	kIx~	-
1	[number]	k4	1
000	[number]	k4	000
věřících	věřící	k2eAgMnPc2d1	věřící
<g/>
,	,	kIx,	,
8	[number]	k4	8
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
4	[number]	k4	4
farnostech	farnost	k1gFnPc6	farnost
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
už	už	k6eAd1	už
2	[number]	k4	2
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
14	[number]	k4	14
kněží	kněz	k1gMnPc2	kněz
v	v	k7c6	v
11	[number]	k4	11
farnostech	farnost	k1gFnPc6	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Ordinářem	ordinář	k1gMnSc7	ordinář
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Harry	Harra	k1gFnSc2	Harra
Entwistle	Entwistle	k1gFnSc2	Entwistle
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.31	.31	k4	.31
<g/>
.5	.5	k4	.5
<g/>
.1940	.1940	k4	.1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
anglikánské	anglikánský	k2eAgFnSc3d1	anglikánská
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
17	[number]	k4	17
%	%	kIx~	%
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
asi	asi	k9	asi
19	[number]	k4	19
%	%	kIx~	%
protestanti	protestant	k1gMnPc1	protestant
celkem	celkem	k6eAd1	celkem
<g/>
,	,	kIx,	,
<g/>
vč.	vč.	k?	vč.
mormonů	mormon	k1gMnPc2	mormon
a	a	k8xC	a
jehovistů	jehovista	k1gMnPc2	jehovista
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
3	[number]	k4	3
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
např.	např.	kA	např.
syrských	syrský	k2eAgMnPc2d1	syrský
jakobitů	jakobita	k1gMnPc2	jakobita
(	(	kIx(	(
<g/>
miafyzit	miafyzit	k1gInSc1	miafyzit
<g/>
.	.	kIx.	.
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
)	)	kIx)	)
-	-	kIx~	-
patriarchální	patriarchální	k2eAgInSc1d1	patriarchální
vikariát	vikariát	k1gInSc1	vikariát
-	-	kIx~	-
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
farností	farnost	k1gFnPc2	farnost
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
farnost	farnost	k1gFnSc4	farnost
na	na	k7c6	na
N.	N.	kA	N.
Zélandě	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nekřesťanským	křesťanský	k2eNgNnPc3d1	nekřesťanské
náboženstvím	náboženství	k1gNnPc3	náboženství
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
7	[number]	k4	7
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
k	k	k7c3	k
buddhismu	buddhismus	k1gInSc2	buddhismus
a	a	k8xC	a
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
22	[number]	k4	22
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k9	jako
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
vyznání	vyznání	k1gNnSc6	vyznání
odpovědět	odpovědět	k5eAaPmF	odpovědět
(	(	kIx(	(
<g/>
vyznání	vyznání	k1gNnSc4	vyznání
nezjištěno	zjištěn	k2eNgNnSc4d1	nezjištěno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedělní	nedělní	k2eAgFnPc1d1	nedělní
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
(	(	kIx(	(
<g/>
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
<g/>
)	)	kIx)	)
návštěvu	návštěva	k1gFnSc4	návštěva
okolo	okolo	k7c2	okolo
7,5	[number]	k4	7,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
katolické	katolický	k2eAgFnPc4d1	katolická
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
převládali	převládat	k5eAaImAgMnP	převládat
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
australského	australský	k2eAgInSc2d1	australský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
mnoho	mnoho	k4c1	mnoho
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
Italů	Ital	k1gMnPc2	Ital
a	a	k8xC	a
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
vláda	vláda	k1gFnSc1	vláda
zrušila	zrušit	k5eAaPmAgFnS	zrušit
politiku	politika	k1gFnSc4	politika
"	"	kIx"	"
<g/>
bílé	bílý	k2eAgFnPc1d1	bílá
Austrálie	Austrálie	k1gFnPc1	Austrálie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
preferovala	preferovat	k5eAaImAgFnS	preferovat
imigranty	imigrant	k1gMnPc4	imigrant
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
12	[number]	k4	12
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
australská	australský	k2eAgFnSc1d1	australská
vláda	vláda	k1gFnSc1	vláda
přijímá	přijímat	k5eAaImIp3nS	přijímat
ročně	ročně	k6eAd1	ročně
okolo	okolo	k7c2	okolo
190	[number]	k4	190
000	[number]	k4	000
imigrantů	imigrant	k1gMnPc2	imigrant
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
spoustu	spousta	k1gFnSc4	spousta
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
staveb	stavba	k1gFnPc2	stavba
jako	jako	k9	jako
např.	např.	kA	např.
Opera	opera	k1gFnSc1	opera
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
vynikají	vynikat	k5eAaImIp3nP	vynikat
australské	australský	k2eAgFnPc1d1	australská
reprezentace	reprezentace	k1gFnPc1	reprezentace
v	v	k7c6	v
kriketu	kriket	k1gInSc6	kriket
<g/>
,	,	kIx,	,
pozemním	pozemní	k2eAgInSc6d1	pozemní
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
netballu	netball	k1gInSc6	netball
<g/>
,	,	kIx,	,
rugby	rugby	k1gNnSc1	rugby
league	league	k1gNnSc1	league
a	a	k8xC	a
rugby	rugby	k1gNnSc1	rugby
union	union	k1gInSc1	union
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
australský	australský	k2eAgInSc1d1	australský
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
také	také	k9	také
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
surfing	surfing	k1gInSc1	surfing
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
BLAINEY	BLAINEY	kA	BLAINEY
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc7	Geoffrey
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
334	[number]	k4	334
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Austrálie	Austrálie	k1gFnSc2	Austrálie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Austrálie	Austrálie	k1gFnSc2	Austrálie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc2	kategorie
Austrálie	Austrálie	k1gFnSc2	Austrálie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
http://www.zemesveta.cz/archiv/rocnik-2002/priroda-australie-5-2002	[url]	k4	http://www.zemesveta.cz/archiv/rocnik-2002/priroda-australie-5-2002
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Novem	novum	k1gNnSc7	novum
Zélandu	Zéland	k1gInSc2	Zéland
(	(	kIx(	(
<g/>
nefunkční	funkční	k2eNgMnSc1d1	nefunkční
<g/>
)	)	kIx)	)
Fotogalerie	Fotogalerie	k1gFnPc1	Fotogalerie
<g/>
,	,	kIx,	,
rady	rada	k1gFnPc1	rada
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
zájezdy	zájezd	k1gInPc4	zájezd
a	a	k8xC	a
dovolená	dovolená	k1gFnSc1	dovolená
Austrálie	Austrálie	k1gFnSc2	Austrálie
Austrálie	Austrálie	k1gFnSc2	Austrálie
průvodce	průvodce	k1gMnSc1	průvodce
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
Austrálie	Austrálie	k1gFnSc2	Austrálie
cestování	cestování	k1gNnSc2	cestování
|	|	kIx~	|
Austrálie	Austrálie	k1gFnSc1	Austrálie
mapy	mapa	k1gFnSc2	mapa
Australia	Australia	k1gFnSc1	Australia
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Australia	Australia	k1gFnSc1	Australia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Austrálie	Austrálie	k1gFnSc1	Austrálie
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
East	East	k1gMnSc1	East
Asian	Asian	k1gMnSc1	Asian
and	and	k?	and
Pacific	Pacific	k1gMnSc1	Pacific
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Australia	Australia	k1gFnSc1	Australia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Australia	Australia	k1gFnSc1	Australia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Australia	Australium	k1gNnSc2	Australium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
LANGE	LANGE	kA	LANGE
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Terence	Terence	k1gFnSc2	Terence
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Australia	Australia	k1gFnSc1	Australia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
