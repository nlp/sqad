<p>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Petrov	Petrov	k1gInSc1	Petrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sídelním	sídelní	k2eAgInSc7d1	sídelní
kostelem	kostel	k1gInSc7	kostel
biskupa	biskup	k1gInSc2	biskup
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Petrov	Petrov	k1gInSc1	Petrov
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
architektonickým	architektonický	k2eAgFnPc3d1	architektonická
památkám	památka	k1gFnPc3	památka
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
také	také	k9	také
mezi	mezi	k7c4	mezi
nejvýraznější	výrazný	k2eAgFnPc4d3	nejvýraznější
brněnské	brněnský	k2eAgFnPc4d1	brněnská
dominanty	dominanta	k1gFnPc4	dominanta
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
desetikoruně	desetikoruna	k1gFnSc6	desetikoruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
jsou	být	k5eAaImIp3nP	být
vysoké	vysoká	k1gFnPc1	vysoká
84	[number]	k4	84
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
interiér	interiér	k1gInSc1	interiér
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
barokní	barokní	k2eAgFnSc1d1	barokní
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Schweigla	Schweigl	k1gMnSc2	Schweigl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
vnější	vnější	k2eAgFnSc1d1	vnější
silueta	silueta	k1gFnSc1	silueta
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Augusta	August	k1gMnSc2	August
Kirsteina	Kirstein	k1gMnSc2	Kirstein
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vzešel	vzejít	k5eAaPmAgInS	vzejít
jako	jako	k9	jako
vítězný	vítězný	k2eAgInSc4d1	vítězný
z	z	k7c2	z
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
novogotiky	novogotika	k1gFnSc2	novogotika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
dnešní	dnešní	k2eAgFnSc2d1	dnešní
katedrály	katedrála	k1gFnSc2	katedrála
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k6eAd1	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
zbudována	zbudován	k2eAgFnSc1d1	zbudována
románská	románský	k2eAgFnSc1d1	románská
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
markraběte	markrabě	k1gMnSc2	markrabě
Konráda	Konrád	k1gMnSc2	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Oty	Ota	k1gMnPc4	Ota
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kostelík	kostelík	k1gInSc1	kostelík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
apsidu	apsida	k1gFnSc4	apsida
i	i	k8xC	i
kryptu	krypta	k1gFnSc4	krypta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
přestavbě	přestavba	k1gFnSc3	přestavba
na	na	k7c4	na
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
románskou	románský	k2eAgFnSc4d1	románská
baziliku	bazilika	k1gFnSc4	bazilika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
během	během	k7c2	během
archeologického	archeologický	k2eAgInSc2d1	archeologický
výzkumu	výzkum	k1gInSc2	výzkum
katedrály	katedrála	k1gFnSc2	katedrála
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
přístupné	přístupný	k2eAgInPc1d1	přístupný
i	i	k8xC	i
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
do	do	k7c2	do
raně	raně	k6eAd1	raně
gotické	gotický	k2eAgFnSc2d1	gotická
podoby	podoba	k1gFnSc2	podoba
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
ke	k	k7c3	k
vzestupu	vzestup	k1gInSc2	vzestup
jeho	jeho	k3xOp3gFnSc2	jeho
důležitosti	důležitost	k1gFnSc2	důležitost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
proboštským	proboštský	k2eAgInSc7d1	proboštský
kostelem	kostel	k1gInSc7	kostel
a	a	k8xC	a
také	také	k9	také
kolegiátní	kolegiátní	k2eAgFnSc7d1	kolegiátní
kapitulou	kapitula	k1gFnSc7	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
vážně	vážně	k6eAd1	vážně
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
době	doba	k1gFnSc6	doba
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
chrámu	chrám	k1gInSc2	chrám
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
poměrně	poměrně	k6eAd1	poměrně
silně	silně	k6eAd1	silně
barokizován	barokizován	k2eAgInSc1d1	barokizován
architektem	architekt	k1gMnSc7	architekt
Mořicem	Mořic	k1gMnSc7	Mořic
Grimmem	Grimm	k1gInSc7	Grimm
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
především	především	k9	především
na	na	k7c6	na
bočních	boční	k2eAgInPc6d1	boční
oltářích	oltář	k1gInPc6	oltář
<g/>
,	,	kIx,	,
na	na	k7c6	na
přestavbě	přestavba	k1gFnSc6	přestavba
hlavního	hlavní	k2eAgNnSc2d1	hlavní
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
gotických	gotický	k2eAgInPc2d1	gotický
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Přestavbu	přestavba	k1gFnSc4	přestavba
katedrály	katedrála	k1gFnSc2	katedrála
provedl	provést	k5eAaPmAgMnS	provést
významný	významný	k2eAgMnSc1d1	významný
brněnský	brněnský	k2eAgMnSc1d1	brněnský
stavitel	stavitel	k1gMnSc1	stavitel
František	František	k1gMnSc1	František
Benedikt	Benedikt	k1gMnSc1	Benedikt
Klíčník	klíčník	k1gMnSc1	klíčník
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
barokizace	barokizace	k1gFnSc2	barokizace
se	se	k3xPyFc4	se
však	však	k9	však
vývoj	vývoj	k1gInSc1	vývoj
katedrály	katedrála	k1gFnSc2	katedrála
nezastavil	zastavit	k5eNaPmAgInS	zastavit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
dále	daleko	k6eAd2	daleko
architektonicky	architektonicky	k6eAd1	architektonicky
upravována	upravován	k2eAgFnSc1d1	upravována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
novo-gotizující	novootizující	k2eAgFnSc3d1	novo-gotizující
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
presbytáře	presbytář	k1gInSc2	presbytář
kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
také	také	k9	také
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
dnešního	dnešní	k2eAgInSc2d1	dnešní
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
původní	původní	k2eAgInSc4d1	původní
<g/>
"	"	kIx"	"
barokní	barokní	k2eAgMnSc1d1	barokní
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
dnešním	dnešní	k2eAgInSc7d1	dnešní
11	[number]	k4	11
m	m	kA	m
vysokým	vysoký	k2eAgInSc7d1	vysoký
pseudo-gotickým	pseudootický	k2eAgInSc7d1	pseudo-gotický
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
oltářem	oltář	k1gInSc7	oltář
od	od	k7c2	od
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
řezbáře	řezbář	k1gMnSc2	řezbář
Josefa	Josef	k1gMnSc2	Josef
Leimera	Leimer	k1gMnSc2	Leimer
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
ukřižování	ukřižování	k1gNnSc4	ukřižování
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
vyobrazeno	vyobrazen	k2eAgNnSc1d1	vyobrazeno
všech	všecek	k3xTgMnPc2	všecek
dvanáct	dvanáct	k4xCc4	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
katedrály	katedrála	k1gFnSc2	katedrála
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
náhrobky	náhrobek	k1gInPc1	náhrobek
brněnských	brněnský	k2eAgMnPc2d1	brněnský
biskupů	biskup	k1gMnPc2	biskup
Václava	Václav	k1gMnSc2	Václav
Urbana	Urban	k1gMnSc2	Urban
Stufflera	Stuffler	k1gMnSc2	Stuffler
a	a	k8xC	a
Vincence	Vincenc	k1gMnSc2	Vincenc
Josefa	Josef	k1gMnSc2	Josef
Schrattenbacha	Schrattenbach	k1gMnSc2	Schrattenbach
a	a	k8xC	a
také	také	k9	také
několika	několik	k4yIc2	několik
patricijských	patricijský	k2eAgFnPc2d1	patricijská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vnější	vnější	k2eAgFnSc1d1	vnější
kazatelna	kazatelna	k1gFnSc1	kazatelna
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Kapistránka	Kapistránek	k1gMnSc2	Kapistránek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
františkánského	františkánský	k2eAgMnSc2d1	františkánský
řádového	řádový	k2eAgMnSc2d1	řádový
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
Kapistrána	Kapistrán	k2eAgFnSc1d1	Kapistrána
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
kázal	kázat	k5eAaImAgInS	kázat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1451	[number]	k4	1451
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kazatelna	kazatelna	k1gFnSc1	kazatelna
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nesloužila	sloužit	k5eNaImAgNnP	sloužit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
jako	jako	k8xC	jako
připomenutí	připomenutí	k1gNnSc4	připomenutí
jeho	jeho	k3xOp3gFnSc2	jeho
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Program	program	k1gInSc1	program
záchrany	záchrana	k1gFnSc2	záchrana
architektonického	architektonický	k2eAgNnSc2d1	architektonické
dědictví	dědictví	k1gNnSc2	dědictví
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Programu	program	k1gInSc2	program
záchrany	záchrana	k1gFnSc2	záchrana
architektonického	architektonický	k2eAgNnSc2d1	architektonické
dědictví	dědictví	k1gNnSc2	dědictví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995-2014	[number]	k4	1995-2014
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
čerpáno	čerpat	k5eAaImNgNnS	čerpat
13	[number]	k4	13
900	[number]	k4	900
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nápis	nápis	k1gInSc1	nápis
nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
==	==	k?	==
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
hlavním	hlavní	k2eAgInSc7d1	hlavní
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
se	se	k3xPyFc4	se
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
průčelí	průčelí	k1gNnSc6	průčelí
nachází	nacházet	k5eAaImIp3nS	nacházet
latinsky	latinsky	k6eAd1	latinsky
psaný	psaný	k2eAgInSc4d1	psaný
citát	citát	k1gInSc4	citát
z	z	k7c2	z
Matoušova	Matoušův	k2eAgNnSc2d1	Matoušovo
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BÍLEK	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Brněnské	brněnský	k2eAgInPc1d1	brněnský
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Šlapanice	Šlapanice	k1gFnSc1	Šlapanice
<g/>
:	:	kIx,	:
Vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUČA	KUČA	k?	KUČA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
vývoj	vývoj	k1gInSc1	vývoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
předměstí	předměstí	k1gNnSc2	předměstí
a	a	k8xC	a
připojených	připojený	k2eAgFnPc2d1	připojená
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Baset	Baset	k1gMnSc1	Baset
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86223	[number]	k4	86223
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAMEK	Samek	k1gMnSc1	Samek
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A-	A-	k?	A-
<g/>
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
651	[number]	k4	651
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
474	[number]	k4	474
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
náboženských	náboženský	k2eAgFnPc2d1	náboženská
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Petrov	Petrov	k1gInSc1	Petrov
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
svatých	svatý	k2eAgFnPc2d1	svatá
bran	brána	k1gFnPc2	brána
milosrdenství	milosrdenství	k1gNnPc2	milosrdenství
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
Diecézní	diecézní	k2eAgNnSc1d1	diecézní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
sakrální	sakrální	k2eAgFnSc1d1	sakrální
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
katedrála	katedrál	k1gMnSc2	katedrál
svatého	svatý	k1gMnSc2	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katedrála	katedrál	k1gMnSc4	katedrál
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
–	–	k?	–
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
2014-09-07	[number]	k4	2014-09-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
