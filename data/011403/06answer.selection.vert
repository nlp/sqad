<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Petrov	Petrov	k1gInSc1	Petrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sídelním	sídelní	k2eAgInSc7d1	sídelní
kostelem	kostel	k1gInSc7	kostel
biskupa	biskup	k1gInSc2	biskup
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
