<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Petrov	Petrov	k1gInSc1	Petrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sídelním	sídelní	k2eAgInSc7d1	sídelní
kostelem	kostel	k1gInSc7	kostel
biskupa	biskup	k1gInSc2	biskup
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Petrov	Petrov	k1gInSc1	Petrov
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>

