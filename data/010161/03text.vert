<p>
<s>
Noel	Noel	k1gMnSc1	Noel
Eduardo	Eduardo	k1gNnSc1	Eduardo
Valladares	Valladares	k1gMnSc1	Valladares
Bonilla	Bonilla	k1gMnSc1	Bonilla
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
honduraský	honduraský	k2eAgMnSc1d1	honduraský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
hrající	hrající	k2eAgFnSc4d1	hrající
za	za	k7c4	za
tamní	tamní	k2eAgInSc4d1	tamní
klub	klub	k1gInSc4	klub
CD	CD	kA	CD
Olimpia	Olimpius	k1gMnSc2	Olimpius
Tegucigalpa	Tegucigalp	k1gMnSc2	Tegucigalp
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
zápasů	zápas	k1gInPc2	zápas
za	za	k7c4	za
honduraskou	honduraský	k2eAgFnSc4d1	honduraská
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
Hondurasu	Honduras	k1gInSc2	Honduras
debutoval	debutovat	k5eAaBmAgInS	debutovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Haiti	Haiti	k1gNnSc3	Haiti
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
honduraskou	honduraský	k2eAgFnSc7d1	honduraská
reprezentací	reprezentace	k1gFnSc7	reprezentace
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
Gold	Gold	k1gMnSc1	Gold
Cupu	cup	k1gInSc2	cup
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Středoamerického	středoamerický	k2eAgInSc2d1	středoamerický
poháru	pohár	k1gInSc2	pohár
2011	[number]	k4	2011
a	a	k8xC	a
2009	[number]	k4	2009
a	a	k8xC	a
turnaje	turnaj	k1gInPc1	turnaj
Copa	Cop	k1gInSc2	Cop
América	Améric	k1gInSc2	Améric
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
transfermarkt	transfermarkt	k1gInSc1	transfermarkt
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
National	National	k1gFnSc6	National
Football	Footballa	k1gFnPc2	Footballa
Teams	Teamsa	k1gFnPc2	Teamsa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
