<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
sedmý	sedmý	k4xOgInSc1
nasazený	nasazený	k2eAgInSc1d1	nasazený
francouzský	francouzský	k2eAgInSc1d1	francouzský
pár	pár	k1gInSc4	pár
složený	složený	k2eAgInSc4d1	složený
ze	z	k7c2
17	#num#	k4
<g/>
letého	letý	k2eAgMnSc4d1	letý
Huga	Hugo	k1gMnSc4	Hugo
Gastona	Gaston	k1gMnSc4	Gaston
a	a	k8xC
18	#num#	k4
<g/>
letého	letý	k2eAgNnSc2d1	leté
Clémenta	Clémento	k1gNnSc2	Clémento
Tabura	Tabura	k1gFnSc1	Tabura
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
ve	v	k7c6
finále	finále	k1gNnSc6	finále
hladce	hladko	k6eAd1
zdolali	zdolat	k5eAaPmAgMnP
německou	německý	k2eAgFnSc4d1	německá
dvojici	dvojice	k1gFnSc4	dvojice
Rudolf	Rudolf	k1gMnSc1	Rudolf
Molleker	Molleker	k1gMnSc1	Molleker
a	a	k8xC
Henri	Henr	k1gMnSc5	Henr
Squire	Squir	k1gMnSc5	Squir
<g/>
,	,	kIx,
startující	startující	k2eAgMnPc1d1	startující
z	z	k7c2
pozice	pozice	k1gFnSc2	pozice
náhradníků	náhradník	k1gMnPc2	náhradník
<g/>
,	,	kIx,
po	po	k7c6
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.
</s>
