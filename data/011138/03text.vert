<p>
<s>
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
juniorů	junior	k1gMnPc2	junior
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
2018	#num#	k4
probíhala	probíhat	k5eAaImAgFnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
deblové	deblový	k2eAgFnSc2d1	deblová
soutěže	soutěž	k1gFnSc2	soutěž
melbournského	melbournský	k2eAgInSc2d1	melbournský
grandslamu	grandslam	k1gInSc2	grandslam
nastoupilo	nastoupit	k5eAaPmAgNnS
třicet	třicet	k4xCc1
dva	dva	k4xCgInPc1
párů	pár	k1gInPc2	pár
tvořených	tvořený	k2eAgFnPc2d1	tvořená
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
splňovali	splňovat	k5eAaImAgMnP
kritéria	kritérion	k1gNnSc2	kritérion
juniorské	juniorský	k2eAgFnSc2d1	juniorská
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
byl	být	k5eAaImAgMnS
tchajwansko-čínský	tchajwansko-čínský	k2eAgMnSc1d1	tchajwansko-čínský
pár	pár	k4xCyI
18	#num#	k4
<g/>
letých	letý	k2eAgMnPc2d1	letý
juniorů	junior	k1gMnPc2	junior
Hsu	Hsu	k1gMnSc7	Hsu
Yu-hsiou	Yusia	k1gMnSc7	Yu-hsia
a	a	k8xC
Čao	čao	k0
Ling-si	Ling	k1gMnSc5	Ling-s
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hsu	Hsu	k1gMnSc1	Hsu
již	již	k9
nebyl	být	k5eNaImAgMnS
věkově	věkově	k6eAd1
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
do	do	k7c2
soutěže	soutěž	k1gFnSc2	soutěž
zasáhnout	zasáhnout	k5eAaPmF
a	a	k8xC
Čao	čao	k0
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
rozhodl	rozhodnout	k5eAaPmAgMnS
nezúčastnit	zúčastnit	k5eNaPmF
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
sedmý	sedmý	k4xOgInSc1
nasazený	nasazený	k2eAgInSc1d1	nasazený
francouzský	francouzský	k2eAgInSc1d1	francouzský
pár	pár	k1gInSc4	pár
složený	složený	k2eAgInSc4d1	složený
ze	z	k7c2
17	#num#	k4
<g/>
letého	letý	k2eAgMnSc4d1	letý
Huga	Hugo	k1gMnSc4	Hugo
Gastona	Gaston	k1gMnSc4	Gaston
a	a	k8xC
18	#num#	k4
<g/>
letého	letý	k2eAgNnSc2d1	leté
Clémenta	Clémento	k1gNnSc2	Clémento
Tabura	Tabura	k1gFnSc1	Tabura
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
ve	v	k7c6
finále	finále	k1gNnSc6	finále
hladce	hladko	k6eAd1
zdolali	zdolat	k5eAaPmAgMnP
německou	německý	k2eAgFnSc4d1	německá
dvojici	dvojice	k1gFnSc4	dvojice
Rudolf	Rudolf	k1gMnSc1	Rudolf
Molleker	Molleker	k1gMnSc1	Molleker
a	a	k8xC
Henri	Henr	k1gMnSc5	Henr
Squire	Squir	k1gMnSc5	Squir
<g/>
,	,	kIx,
startující	startující	k2eAgMnPc1d1	startující
z	z	k7c2
pozice	pozice	k1gFnSc2	pozice
náhradníků	náhradník	k1gMnPc2	náhradník
<g/>
,	,	kIx,
po	po	k7c6
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
z	z	k7c2
vítězů	vítěz	k1gMnPc2	vítěz
získal	získat	k5eAaPmAgMnS
premiérový	premiérový	k2eAgInSc4d1	premiérový
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
kariéry	kariéra	k1gFnSc2	kariéra
a	a	k8xC
do	do	k7c2
juniorského	juniorský	k2eAgInSc2d1	juniorský
kombinovaného	kombinovaný	k2eAgInSc2d1	kombinovaný
žebříčku	žebříček	k1gInSc2	žebříček
ITF	ITF	kA
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgInS
270	#num#	k4
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navázali	navázat	k5eAaPmAgMnP
tak	tak	k9
na	na	k7c4
výhru	výhra	k1gFnSc4	výhra
jediných	jediný	k2eAgMnPc2d1	jediný
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vítězů	vítěz	k1gMnPc2	vítěz
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6	soutěž
jako	jako	k9
páru	pára	k1gFnSc4	pára
<g/>
,	,	kIx,
Jérôma	Jérôm	k1gMnSc4	Jérôm
Haehnela	Haehnel	k1gMnSc4	Haehnel
a	a	k8xC
Juliena	Julien	k1gMnSc4	Julien
Jeanpierreho	Jeanpierre	k1gMnSc4	Jeanpierre
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
z	z	k7c2
Melbourne	Melbourne	k1gNnSc2	Melbourne
Parku	park	k1gInSc2	park
odvezli	odvézt	k5eAaPmAgMnP
triumf	triumf	k1gInSc4	triumf
v	v	k7c6
roce	rok	k1gInSc6	rok
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgMnSc1
Francouz	Francouz	k1gMnSc1	Francouz
Nicolas	Nicolas	k1gMnSc1	Nicolas
Mahut	Mahut	k1gMnSc1	Mahut
pak	pak	k6eAd1
získal	získat	k5eAaPmAgMnS
juniorskou	juniorský	k2eAgFnSc4d1	juniorská
trofej	trofej	k1gFnSc4	trofej
se	s	k7c7
Španělem	Španěl	k1gMnSc7	Španěl
Tommym	Tommymum	k1gNnPc2	Tommymum
Robredem	Robred	k1gInSc7	Robred
v	v	k7c6
sezóně	sezóna	k1gFnSc6	sezóna
2000	#num#	k4
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Nasazení	nasazení	k1gNnSc1	nasazení
párů	pár	k1gInPc2	pár
==	==	k?
</s>
</p>
<p>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sebastián	Sebastián	k1gMnSc1	Sebastián
Báez	Báez	k1gMnSc1	Báez
/	/	kIx~
Thiago	Thiago	k1gMnSc1	Thiago
Seyboth	Seyboth	k1gMnSc1	Seyboth
Wild	Wild	k1gMnSc1	Wild
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sebastian	Sebastian	k1gMnSc1	Sebastian
Korda	Korda	k1gMnSc1	Korda
/	/	kIx~
Nicolás	Nicolás	k1gInSc1	Nicolás
Mejía	Mejí	k1gInSc2	Mejí
(	(	kIx(
<g/>
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naoki	Naoki	k1gNnSc7	Naoki
Tadžima	Tadžimum	k1gNnSc2	Tadžimum
/	/	kIx~
Alexej	Alexej	k1gMnSc1	Alexej
Zacharov	Zacharov	k1gInSc1	Zacharov
(	(	kIx(
<g/>
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,
odstoupili	odstoupit	k5eAaPmAgMnP
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aidan	Aidan	k1gMnSc1	Aidan
McHugh	McHugh	k1gMnSc1	McHugh
/	/	kIx~
Timofej	Timofej	k1gFnSc1	Timofej
Skatov	Skatov	k1gInSc1	Skatov
(	(	kIx(
<g/>
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tristan	Tristan	k1gInSc1	Tristan
Boyer	Boyer	k1gInSc4	Boyer
/	/	kIx~
Juan	Juan	k1gMnSc1	Juan
Manuel	Manuel	k1gMnSc1	Manuel
Cerúndolo	Cerúndola	k1gFnSc5	Cerúndola
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1	Tomáš
Macháč	Macháč	k1gMnSc1	Macháč
/	/	kIx~
Ondřej	Ondřej	k1gMnSc1	Ondřej
Štyler	Štyler	k1gMnSc1	Štyler
(	(	kIx(
<g/>
semifinále	semifinále	k1gNnSc1	semifinále
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hugo	Hugo	k1gMnSc1	Hugo
Gaston	Gaston	k1gInSc1	Gaston
/	/	kIx~
Clément	Clément	k1gMnSc1	Clément
Tabur	Tabur	k1gMnSc1	Tabur
(	(	kIx(
<g/>
vítězové	vítěz	k1gMnPc1	vítěz
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeong	Jeong	k1gInSc1	Jeong
Yeong-seok	Yeongeok	k1gInSc4	Yeong-seok
/	/	kIx~
Park	park	k1gInSc1	park
Ui-sung	Uiung	k1gInSc1	Ui-sung
(	(	kIx(
<g/>
odstoupili	odstoupit	k5eAaPmAgMnP
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
==	==	k?
Pavouk	pavouk	k1gMnSc1	pavouk
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Finálová	finálový	k2eAgFnSc1d1	finálová
fáze	fáze	k1gFnSc1	fáze
===	===	k?
</s>
</p>
<p>
<s>
===	===	k?
Horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
===	===	k?
</s>
</p>
<p>
<s>
===	===	k?
Dolní	dolní	k2eAgFnSc1d1	dolní
polovina	polovina	k1gFnSc1	polovina
===	===	k?
</s>
</p>
<p>
<s>
==	==	k?
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Reference	reference	k1gFnPc1	reference
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2
článku	článek	k1gInSc2	článek
2018	#num#	k4
Australian	Australiana	k1gFnPc2	Australiana
Open	Opena	k1gFnPc2	Opena
–	–	k?
Boys	boy	k1gMnPc2	boy
<g/>
'	'	kIx"
Doubles	Doubles	k1gMnSc1	Doubles
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?
</s>
</p>
<p>
<s>
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
juniorů	junior	k1gMnPc2	junior
na	na	k7c4
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gNnSc4	Open
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1	dostupné
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
</p>
