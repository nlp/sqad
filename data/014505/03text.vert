<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
formátování	formátování	k1gNnSc4
<g/>
,	,	kIx,
encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Michael	Michael	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc4d1
</s>
<s>
Král	Král	k1gMnSc1
popu	pop	k1gInSc2
(	(	kIx(
<g/>
King	King	k1gInSc1
of	of	k?
Pop	pop	k1gInSc1
<g/>
)	)	kIx)
Narození	narození	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1958	#num#	k4
Gary	Gara	k1gFnSc2
<g/>
,	,	kIx,
Indiana	Indiana	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
Původ	původ	k1gInSc1
</s>
<s>
Afroamerický	afroamerický	k2eAgInSc1d1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
50	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
Příčina	příčina	k1gFnSc1
úmrtí	úmrť	k1gFnPc2
</s>
<s>
Srdeční	srdeční	k2eAgFnSc1d1
zástava	zástava	k1gFnSc1
vyvolaná	vyvolaný	k2eAgFnSc1d1
akutní	akutní	k2eAgFnSc7d1
intoxikací	intoxikace	k1gFnSc7
propofolem	propofol	k1gInSc7
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Forest	Forest	k1gMnSc1
Lawn	Lawn	k1gMnSc1
Memorial	Memorial	k1gMnSc1
Park	park	k1gInSc1
(	(	kIx(
<g/>
34	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
118	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
<g/>
)	)	kIx)
Žánry	žánr	k1gInPc1
</s>
<s>
R	R	kA
<g/>
&	&	k?
<g/>
BSoulPopFunkNew	BSoulPopFunkNew	k1gMnSc1
jack	jack	k1gMnSc1
swingRock	swingRock	k1gMnSc1
Povolání	povolání	k1gNnSc4
</s>
<s>
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
tanečník	tanečník	k1gMnSc1
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
textař	textař	k1gMnSc1
<g/>
,	,	kIx,
producent	producent	k1gMnSc1
<g/>
,	,	kIx,
aranžér	aranžér	k1gMnSc1
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
choreograf	choreograf	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
bicí	bicí	k2eAgFnSc1d1
<g/>
,	,	kIx,
piáno	piáno	k?
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
basa	basa	k1gFnSc1
Hlasový	hlasový	k2eAgInSc4d1
obor	obor	k1gInSc4
</s>
<s>
tenor	tenor	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
dětství	dětství	k1gNnSc6
soprán	soprán	k1gInSc4
<g/>
)	)	kIx)
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1964	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
Vydavatelé	vydavatel	k1gMnPc5
</s>
<s>
Motown	Motown	k1gNnSc1
<g/>
,	,	kIx,
Epic	Epic	k1gFnSc1
(	(	kIx(
<g/>
Sony	Sony	kA
Music	Music	k1gMnSc1
Entertainment	Entertainment	k1gMnSc1
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Grammy	Gramma	k1gFnPc1
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Male	male	k6eAd1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
Vocal	Vocal	k1gInSc1
Performance	performance	k1gFnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
Grammy	Gramma	k1gFnSc2
Award	Award	k1gInSc1
for	forum	k1gNnPc2
Best	Besta	k1gFnPc2
Male	male	k6eAd1
Rock	rock	k1gInSc1
Vocal	Vocal	k1gInSc1
Performance	performance	k1gFnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
Cena	cena	k1gFnSc1
Grammy	Gramma	k1gFnSc2
za	za	k7c4
nahrávku	nahrávka	k1gFnSc4
roku	rok	k1gInSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
Grammy	Gramm	k1gInPc1
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Album	album	k1gNnSc4
for	forum	k1gNnPc2
Children	Childrna	k1gFnPc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Lisa	Lisa	k1gFnSc1
Marie	Marie	k1gFnSc1
Presleyová	Presleyová	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
Debbie	Debbie	k1gFnSc1
Rowe	Rowe	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Prince	princ	k1gMnSc2
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
IParis	IParis	k1gInSc4
Katherine	Katherin	k1gInSc5
JacksonPrince	JacksonPrinec	k1gInSc2
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
II	II	kA
Rodiče	rodič	k1gMnSc4
</s>
<s>
Joe	Joe	k?
Jackson	Jackson	k1gMnSc1
a	a	k8xC
Katherine	Katherin	k1gInSc5
Jackson	Jackson	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Rebbie	Rebbie	k1gFnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Janet	Janet	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
Toya	Toya	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Jermaine	Jermain	k1gInSc5
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Marlon	Marlon	k1gInSc1
Jackson	Jackson	k1gInSc1
<g/>
,	,	kIx,
Randy	rand	k1gInPc1
Jackson	Jacksona	k1gFnPc2
<g/>
,	,	kIx,
Jackie	Jackie	k1gFnSc1
Jackson	Jackson	k1gMnSc1
a	a	k8xC
Tito	tento	k3xDgMnPc1
Jackson	Jackson	k1gInSc4
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnSc3
<g/>
)	)	kIx)
<g/>
Austin	Austin	k1gMnSc1
Brown	Brown	k1gMnSc1
(	(	kIx(
<g/>
synovec	synovec	k1gMnSc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Neverland	Neverland	k1gInSc1
Ranch	Ranch	k1gInSc1
Podpis	podpis	k1gInSc4
</s>
<s>
Web	web	k1gInSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Michael	Michael	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1958	#num#	k4
Gary	Gara	k1gFnSc2
<g/>
,	,	kIx,
Indiana	Indiana	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
popový	popový	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
choreograf	choreograf	k1gMnSc1
<g/>
,	,	kIx,
tanečník	tanečník	k1gMnSc1
<g/>
,	,	kIx,
producent	producent	k1gMnSc1
a	a	k8xC
mecenáš	mecenáš	k1gMnSc1
afroamerického	afroamerický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařadil	zařadit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
nejprodávanějších	prodávaný	k2eAgMnPc2d3
interpretů	interpret	k1gMnPc2
po	po	k7c6
skupině	skupina	k1gFnSc6
The	The	k1gFnSc4
Beatles	beatles	k1gMnSc3
a	a	k8xC
zpěvákovi	zpěvák	k1gMnSc3
Elvisi	Elvis	k1gMnSc3
Presleym	Presleym	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
desky	deska	k1gFnSc2
Thriller	thriller	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bad	Bad	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dangerous	Dangerous	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
History	Histor	k1gInPc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgMnPc4d3
v	v	k7c6
oblasti	oblast	k1gFnSc6
pop	pop	k1gMnSc1
music	music	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
Thriller	thriller	k1gInSc1
je	být	k5eAaImIp3nS
s	s	k7c7
výrazným	výrazný	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
nejprodávanějším	prodávaný	k2eAgInSc7d3
albem	album	k1gNnSc7
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
prodalo	prodat	k5eAaPmAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gInSc2
110	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jackson	Jackson	k1gInSc1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c2
Krále	Král	k1gMnSc2
popu	pop	k1gInSc2
(	(	kIx(
<g/>
King	King	k1gInSc1
of	of	k?
Pop	pop	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejslavnějších	slavný	k2eAgFnPc2d3
osob	osoba	k1gFnPc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zástupcem	zástupce	k1gMnSc7
konvenčního	konvenční	k2eAgInSc2d1
popu	pop	k1gInSc2
a	a	k8xC
taneční	taneční	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgFnSc2d1
ze	z	k7c2
soulu	soul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
svého	svůj	k3xOyFgInSc2
charakteristicky	charakteristicky	k6eAd1
vysokého	vysoký	k2eAgInSc2d1
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
rytmického	rytmický	k2eAgNnSc2d1
cítění	cítění	k1gNnSc2
a	a	k8xC
tanečních	taneční	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
byl	být	k5eAaImAgInS
znám	znám	k2eAgInSc1d1
i	i	k9
jako	jako	k9
velký	velký	k2eAgMnSc1d1
perfekcionista	perfekcionista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
získal	získat	k5eAaPmAgInS
celkem	celkem	k6eAd1
13	#num#	k4
cen	cena	k1gFnPc2
Grammy	Gramma	k1gFnSc2
a	a	k8xC
26	#num#	k4
cen	cena	k1gFnPc2
American	American	k1gMnSc1
Music	Music	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
umělecká	umělecký	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
se	se	k3xPyFc4
z	z	k7c2
devíti	devět	k4xCc2
sourozenců	sourozenec	k1gMnPc2
narodil	narodit	k5eAaPmAgMnS
jako	jako	k9
sedmý	sedmý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zahájení	zahájení	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
kariéry	kariéra	k1gFnSc2
se	se	k3xPyFc4
pokládá	pokládat	k5eAaImIp3nS
rok	rok	k1gInSc1
1964	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
bratry	bratr	k1gMnPc7
(	(	kIx(
<g/>
skupina	skupina	k1gFnSc1
Jackson	Jackson	k1gNnSc1
5	#num#	k4
<g/>
)	)	kIx)
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c6
pódiu	pódium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sólovou	sólový	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vydal	vydat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
baladu	balada	k1gFnSc4
Got	Got	k1gFnSc2
To	to	k9
Be	Be	k1gMnSc5
There	Ther	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
píseň	píseň	k1gFnSc4
přidal	přidat	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
1972	#num#	k4
na	na	k7c4
stejnojmenné	stejnojmenný	k2eAgNnSc4d1
album	album	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
Michael	Michael	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
alba	album	k1gNnSc2
Ben	Ben	k1gInSc1
<g/>
,	,	kIx,
Music	Music	k1gMnSc1
&	&	k?
Me	Me	k1gMnSc1
a	a	k8xC
Forever	Forever	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
alba	album	k1gNnPc4
vydal	vydat	k5eAaPmAgInS
pod	pod	k7c7
společností	společnost	k1gFnSc7
Motown	Motowna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pak	pak	k6eAd1
přišel	přijít	k5eAaPmAgInS
zlom	zlom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
i	i	k8xC
skupina	skupina	k1gFnSc1
Jackson	Jackson	k1gNnSc1
5	#num#	k4
už	už	k6eAd1
nechtěli	chtít	k5eNaImAgMnP
vydávat	vydávat	k5eAaImF,k5eAaPmF
alba	album	k1gNnPc4
pod	pod	k7c7
Motownem	Motown	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odešli	odejít	k5eAaPmAgMnP
tedy	tedy	k9
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
z	z	k7c2
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
a	a	k8xC
přidali	přidat	k5eAaPmAgMnP
se	se	k3xPyFc4
k	k	k7c3
nahrávací	nahrávací	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
Epic	Epic	k1gInSc4
Records	Recordsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
také	také	k9
skupina	skupina	k1gFnSc1
Jackson	Jackson	k1gInSc1
5	#num#	k4
změnila	změnit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc4
na	na	k7c4
The	The	k1gFnSc4
Jacksons	Jacksonsa	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
Motown	Motown	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
název	název	k1gInSc1
Jackson	Jackson	k1gNnSc1
5	#num#	k4
je	být	k5eAaImIp3nS
„	„	k?
<g/>
jejich	jejich	k3xOp3gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Michael	Michael	k1gMnSc1
od	od	k7c2
útlého	útlý	k2eAgInSc2d1
věku	věk	k1gInSc2
velice	velice	k6eAd1
trpěl	trpět	k5eAaImAgMnS
výchovou	výchova	k1gFnSc7
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Joe	Joe	k1gMnSc1
používal	používat	k5eAaImAgMnS
staromódní	staromódní	k2eAgFnSc2d1
výchovné	výchovný	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
často	často	k6eAd1
bil	bít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
také	také	k6eAd1
stal	stát	k5eAaPmAgInS
terčem	terč	k1gInSc7
posměchu	posměch	k1gInSc2
kvůli	kvůli	k7c3
svému	své	k1gNnSc3
nosu	nos	k1gInSc2
a	a	k8xC
otec	otec	k1gMnSc1
mu	on	k3xPp3gMnSc3
začal	začít	k5eAaPmAgMnS
říkat	říkat	k5eAaImF
„	„	k?
<g/>
Velký	velký	k2eAgInSc4d1
nos	nos	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
matka	matka	k1gFnSc1
Katherine	Katherin	k1gInSc5
tyto	tento	k3xDgFnPc4
metody	metoda	k1gFnPc4
neschvalovala	schvalovat	k5eNaImAgFnS
a	a	k8xC
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
vždy	vždy	k6eAd1
zahrnovala	zahrnovat	k5eAaImAgFnS
láskou	láska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
se	se	k3xPyFc4
vždy	vždy	k6eAd1
přidával	přidávat	k5eAaImAgMnS
na	na	k7c4
stranu	strana	k1gFnSc4
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
domovních	domovní	k2eAgFnPc6d1
zkouškách	zkouška	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
měl	mít	k5eAaImAgInS
před	před	k7c7
školou	škola	k1gFnSc7
i	i	k9
po	po	k7c6
ní	on	k3xPp3gFnSc6
<g/>
,	,	kIx,
držel	držet	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
v	v	k7c6
ruce	ruka	k1gFnSc6
pásek	páska	k1gFnPc2
a	a	k8xC
když	když	k8xS
někdo	někdo	k3yInSc1
ze	z	k7c2
skupiny	skupina	k1gFnSc2
Jackson	Jackson	k1gInSc1
5	#num#	k4
něco	něco	k3yInSc4
pokazil	pokazit	k5eAaPmAgMnS
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
jím	jíst	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
vyplatilo	vyplatit	k5eAaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
takhle	takhle	k6eAd1
vytvořil	vytvořit	k5eAaPmAgMnS
jedinečného	jedinečný	k2eAgMnSc4d1
Krále	Král	k1gMnSc4
popu	pop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
devíti	devět	k4xCc2
sourozenců	sourozenec	k1gMnPc2
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
hudební	hudební	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
zahájil	zahájit	k5eAaPmAgInS
již	již	k6eAd1
jako	jako	k9
chlapec	chlapec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
jeho	jeho	k3xOp3gFnSc4
starší	starý	k2eAgMnPc1d2
bratři	bratr	k1gMnPc1
vystupovali	vystupovat	k5eAaImAgMnP
v	v	k7c6
sourozenecké	sourozenecký	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
a	a	k8xC
Michael	Michael	k1gMnSc1
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
přidal	přidat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jacksonovým	Jacksonův	k2eAgInSc7d1
vzorem	vzor	k1gInSc7
byl	být	k5eAaImAgInS
již	již	k6eAd1
od	od	k7c2
útlého	útlý	k2eAgInSc2d1
věku	věk	k1gInSc2
černošský	černošský	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
Frankie	Frankie	k1gFnSc2
Lymon	Lymon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
se	se	k3xPyFc4
Michael	Michael	k1gMnSc1
stal	stát	k5eAaPmAgMnS
nejmladším	mladý	k2eAgMnSc7d3
leaderem	leader	k1gMnSc7
skupiny	skupina	k1gFnSc2
The	The	k1gMnSc1
Jackson	Jackson	k1gMnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
prorazila	prorazit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
a	a	k8xC
první	první	k4xOgFnPc4
LP	LP	kA
vydala	vydat	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
bylo	být	k5eAaImAgNnS
Michaelovi	Michaelův	k2eAgMnPc1d1
12	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
už	už	k6eAd1
tehdy	tehdy	k6eAd1
odstartoval	odstartovat	k5eAaPmAgMnS
sólovou	sólový	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
jeho	jeho	k3xOp3gInSc7
hitem	hit	k1gInSc7
byla	být	k5eAaImAgFnS
balada	balada	k1gFnSc1
Got	Got	k1gFnSc1
To	ten	k3xDgNnSc4
Be	Be	k1gMnSc5
There	Ther	k1gMnSc5
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
s	s	k7c7
bratry	bratr	k1gMnPc7
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c4
turné	turné	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
dostává	dostávat	k5eAaImIp3nS
cenu	cena	k1gFnSc4
z	z	k7c2
rukou	ruka	k1gFnPc2
amerického	americký	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Ronalda	Ronald	k1gMnSc2
Reagana	Reagan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílý	bílý	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
s	s	k7c7
americkým	americký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Ronaldem	Ronald	k1gMnSc7
Reaganem	Reagan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílý	bílý	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
autorskou	autorský	k2eAgFnSc4d1
desku	deska	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
Off	Off	k1gMnSc1
the	the	k?
Wall	Wall	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
vzal	vzít	k5eAaPmAgInS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvlivnějších	vlivný	k2eAgMnPc2d3
hudebních	hudební	k2eAgMnPc2d1
producentů	producent	k1gMnPc2
Quincy	Quinca	k1gFnSc2
Jones	Jones	k1gMnSc1
a	a	k8xC
odstartoval	odstartovat	k5eAaPmAgMnS
tak	tak	k6eAd1
Jacksonovu	Jacksonův	k2eAgFnSc4d1
hvězdnou	hvězdný	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
Thriller	thriller	k1gInSc1
–	–	k?
nejprodávanější	prodávaný	k2eAgNnSc1d3
album	album	k1gNnSc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
–	–	k?
vydal	vydat	k5eAaPmAgMnS
tandem	tandem	k1gInSc4
Jackson	Jackson	k1gMnSc1
–	–	k?
Jones	Jones	k1gMnSc1
o	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluční	revoluční	k2eAgInSc4d1
videoklip	videoklip	k1gInSc4
k	k	k7c3
titulní	titulní	k2eAgFnSc3d1
písni	píseň	k1gFnSc3
udal	udat	k5eAaPmAgInS
novou	nový	k2eAgFnSc4d1
normu	norma	k1gFnSc4
videoklipu	videoklip	k1gInSc2
jako	jako	k8xC,k8xS
takového	takový	k3xDgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
předvedl	předvést	k5eAaPmAgMnS
Jackson	Jackson	k1gMnSc1
při	při	k7c6
výroční	výroční	k2eAgFnSc6d1
show	show	k1gFnSc6
společnosti	společnost	k1gFnSc2
Motown	Motown	k1gNnSc4
na	na	k7c4
živo	živo	k6eAd1
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
sólovou	sólový	k2eAgFnSc4d1
taneční	taneční	k2eAgFnSc4d1
sestavu	sestava	k1gFnSc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
legendárního	legendární	k2eAgInSc2d1
Moonwalk	Moonwalka	k1gFnPc2
<g/>
)	)	kIx)
k	k	k7c3
písni	píseň	k1gFnSc3
Billie	Billie	k1gFnSc1
Jean	Jean	k1gMnSc1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
vyprofiloval	vyprofilovat	k5eAaPmAgMnS
jako	jako	k9
nejnadanější	nadaný	k2eAgMnSc1d3
hudební	hudební	k2eAgMnSc1d1
performer	performer	k1gMnSc1
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
Bad	Bad	k1gMnSc4
–	–	k?
druhé	druhý	k4xOgNnSc1
nejprodávanější	prodávaný	k2eAgNnSc1d3
album	album	k1gNnSc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Album	album	k1gNnSc1
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
kratší	krátký	k2eAgFnSc1d2
než	než	k8xS
Thriller	thriller	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
způsobeno	způsobit	k5eAaPmNgNnS
Jacksonovým	Jacksonův	k2eAgInSc7d1
požadavkem	požadavek	k1gInSc7
novějšího	nový	k2eAgInSc2d2
a	a	k8xC
průbojnějšího	průbojný	k2eAgInSc2d2
zvuku	zvuk	k1gInSc2
alba	alba	k1gFnSc1
–	–	k?
mnoho	mnoho	k4c1
písní	píseň	k1gFnPc2
z	z	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zůstalo	zůstat	k5eAaPmAgNnS
dodnes	dodnes	k6eAd1
nezařazených	zařazený	k2eNgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
album	album	k1gNnSc1
Dangerous	Dangerous	k1gInSc1
představuje	představovat	k5eAaImIp3nS
zlom	zlom	k1gInSc4
v	v	k7c6
Jacksonově	Jacksonův	k2eAgFnSc6d1
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpoutal	odpoutat	k5eAaPmAgMnS
se	se	k3xPyFc4
od	od	k7c2
dosavadní	dosavadní	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
Quincy	Quinc	k1gMnPc7
Jonesem	Jones	k1gMnSc7
a	a	k8xC
výrazně	výrazně	k6eAd1
proměnil	proměnit	k5eAaPmAgMnS
„	„	k?
<g/>
sound	sound	k1gMnSc1
<g/>
“	“	k?
svých	svůj	k3xOyFgFnPc2
písní	píseň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
procesu	proces	k1gInSc6
hrál	hrát	k5eAaImAgInS
tzv.	tzv.	kA
beatbox	beatbox	k1gInSc1
–	–	k?
Jacksonovy	Jacksonův	k2eAgFnSc2d1
hlasové	hlasový	k2eAgFnSc2d1
dispozice	dispozice	k1gFnSc2
byly	být	k5eAaImAgInP
natolik	natolik	k6eAd1
pestré	pestrý	k2eAgInPc1d1
<g/>
,	,	kIx,
že	že	k8xS
obsáhly	obsáhnout	k5eAaPmAgFnP
nejen	nejen	k6eAd1
vokální	vokální	k2eAgFnSc4d1
linku	linka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
rovněž	rovněž	k9
většinu	většina	k1gFnSc4
hudebního	hudební	k2eAgInSc2d1
doprovodu	doprovod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
album	album	k1gNnSc1
HIStory	HIStor	k1gMnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
Jackson	Jackson	k1gMnSc1
posílil	posílit	k5eAaPmAgMnS
roli	role	k1gFnSc4
elektroniky	elektronika	k1gFnSc2
a	a	k8xC
výrazně	výrazně	k6eAd1
propracoval	propracovat	k5eAaPmAgInS
zapojení	zapojení	k1gNnSc4
svého	svůj	k3xOyFgInSc2
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
coby	coby	k?
bicího	bicí	k2eAgInSc2d1
nástroje	nástroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samostatnou	samostatný	k2eAgFnSc4d1
kapitolu	kapitola	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
film	film	k1gInSc1
–	–	k?
videoklip	videoklip	k1gInSc4
Ghosts	Ghostsa	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
Jackson	Jackson	k1gMnSc1
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
režisérem	režisér	k1gMnSc7
Stanem	stan	k1gInSc7
Winstonem	Winston	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
dokladem	doklad	k1gInSc7
Jacksonova	Jacksonův	k2eAgInSc2d1
choreografického	choreografický	k2eAgInSc2d1
perfekcionismu	perfekcionismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgFnSc1d1
album	album	k1gNnSc4
Blood	Blood	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Dance	Danka	k1gFnSc3
Floor	Floora	k1gFnPc2
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
popudu	popud	k1gInSc2
Sony	Sony	kA
music	music	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
chtěla	chtít	k5eAaImAgFnS
zpěváka	zpěvák	k1gMnSc4
dostat	dostat	k5eAaPmF
do	do	k7c2
diskotékových	diskotékový	k2eAgFnPc2d1
síní	síň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
album	album	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
pět	pět	k4xCc4
původních	původní	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
remixy	remix	k1gInPc4
starších	starý	k2eAgInPc2d2
hitů	hit	k1gInPc2
a	a	k8xC
Jackson	Jackson	k1gInSc1
se	se	k3xPyFc4
s	s	k7c7
touto	tento	k3xDgFnSc7
koncepcí	koncepce	k1gFnSc7
nikdy	nikdy	k6eAd1
zcela	zcela	k6eAd1
nespokojil	spokojit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
Invincible	Invincible	k1gFnPc2
tak	tak	k9
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
navázáním	navázání	k1gNnSc7
na	na	k7c4
Dangerous	Dangerous	k1gInSc4
a	a	k8xC
History	Histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jackson	Jackson	k1gInSc1
nikdy	nikdy	k6eAd1
nepřestal	přestat	k5eNaPmAgInS
sledovat	sledovat	k5eAaImF
nové	nový	k2eAgInPc4d1
trendy	trend	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
zvukové	zvukový	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
je	být	k5eAaImIp3nS
právě	právě	k6eAd1
Invincible	Invincible	k1gFnSc4
skvělým	skvělý	k2eAgInSc7d1
dokladem	doklad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jacksonova	Jacksonův	k2eAgFnSc1d1
nahrávací	nahrávací	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Sony	Sony	kA
Music	Musice	k1gInPc2
však	však	k9
neměla	mít	k5eNaImAgFnS
zájem	zájem	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
album	album	k1gNnSc1
Invincible	Invincible	k1gFnSc4
mělo	mít	k5eAaImAgNnS
komerční	komerční	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
potřebovala	potřebovat	k5eAaImAgFnS
Jacksona	Jacksona	k1gFnSc1
izolovat	izolovat	k5eAaBmF
od	od	k7c2
finančních	finanční	k2eAgInPc2d1
příjmů	příjem	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byl	být	k5eAaImAgInS
nucen	nutit	k5eAaImNgMnS
jí	on	k3xPp3gFnSc7
odprodat	odprodat	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
podíl	podíl	k1gInSc4
ve	v	k7c6
firemním	firemní	k2eAgInSc6d1
ATV	ATV	kA
katalogu	katalog	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
alba	album	k1gNnSc2
Invincible	Invincible	k1gFnSc2
vyšel	vyjít	k5eAaPmAgInS
jen	jen	k9
jeden	jeden	k4xCgInSc1
singl	singl	k1gInSc1
(	(	kIx(
<g/>
You	You	k1gFnSc1
Rock	rock	k1gInSc1
my	my	k3xPp1nPc1
World	Worldo	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
natočil	natočit	k5eAaBmAgMnS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
videoklip	videoklip	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k9
se	se	k3xPyFc4
alba	album	k1gNnSc2
prodalo	prodat	k5eAaPmAgNnS
úctyhodných	úctyhodný	k2eAgInPc2d1
10	#num#	k4
milionů	milion	k4xCgInPc2
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
spor	spor	k1gInSc4
se	s	k7c7
Sony	Sony	kA
v	v	k7c6
podstatě	podstata	k1gFnSc6
ukončil	ukončit	k5eAaPmAgMnS
Jacksonovu	Jacksonův	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
plánoval	plánovat	k5eAaImAgMnS
comeback	comeback	k1gInSc4
u	u	k7c2
jiné	jiný	k2eAgFnSc2d1
nahrávací	nahrávací	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jackson	Jackson	k1gMnSc1
připravoval	připravovat	k5eAaImAgMnS
koncertní	koncertní	k2eAgNnSc4d1
turné	turné	k1gNnSc4
„	„	k?
<g/>
This	This	k1gInSc1
is	is	k?
it	it	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
začít	začít	k5eAaPmF
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
vyprodaných	vyprodaný	k2eAgNnPc2d1
50	#num#	k4
koncertů	koncert	k1gInPc2
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
O2	O2	k1gFnSc6
aréně	aréna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
už	už	k6eAd1
nedožil	dožít	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
album	album	k1gNnSc4
Thriller	thriller	k1gInSc4
dostal	dostat	k5eAaPmAgMnS
8	#num#	k4
cen	cena	k1gFnPc2
Grammy	Gramma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
jich	on	k3xPp3gMnPc2
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
dostal	dostat	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Music	Music	k1gMnSc1
Awards	Awards	k1gInSc4
mu	on	k3xPp3gMnSc3
udělili	udělit	k5eAaPmAgMnP
titul	titul	k1gInSc4
Umělec	umělec	k1gMnSc1
století	století	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
Thriller	thriller	k1gInSc1
získalo	získat	k5eAaPmAgNnS
zápis	zápis	k1gInSc4
do	do	k7c2
Guinessovy	Guinessův	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
rekordů	rekord	k1gInPc2
jako	jako	k8xS,k8xC
nejprodávanější	prodávaný	k2eAgNnSc1d3
album	album	k1gNnSc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
mála	málo	k1gNnSc2
umělců	umělec	k1gMnPc2
byl	být	k5eAaImAgMnS
Jackson	Jackson	k1gMnSc1
dvakrát	dvakrát	k6eAd1
přijat	přijmout	k5eAaPmNgMnS
do	do	k7c2
rockenrolové	rockenrolový	k2eAgFnSc2d1
síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
také	také	k9
2	#num#	k4
hvězdy	hvězda	k1gFnSc2
na	na	k7c6
hollywoodském	hollywoodský	k2eAgInSc6d1
chodníku	chodník	k1gInSc6
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
album	album	k1gNnSc4
Thriller	thriller	k1gInSc4
přijala	přijmout	k5eAaPmAgFnS
knihovna	knihovna	k1gFnSc1
amerického	americký	k2eAgInSc2d1
Kongresu	kongres	k1gInSc2
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
sbírek	sbírka	k1gFnPc2
pro	pro	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
nezpochybnitelný	zpochybnitelný	k2eNgInSc4d1
kulturní	kulturní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
krále	král	k1gMnSc4
popu	pop	k1gInSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
King	King	k1gMnSc1
of	of	k?
Pop	pop	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
(	(	kIx(
<g/>
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mu	on	k3xPp3gMnSc3
dala	dát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
dlouholetá	dlouholetý	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
Elizabeth	Elizabeth	k1gFnSc2
Taylor	Taylora	k1gFnPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
„	„	k?
<g/>
král	král	k1gMnSc1
popu	pop	k1gInSc2
<g/>
,	,	kIx,
rocku	rock	k1gInSc2
a	a	k8xC
soulu	soul	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
však	však	k9
pro	pro	k7c4
Michaela	Michael	k1gMnSc4
Jacksona	Jackson	k1gMnSc4
omezující	omezující	k2eAgInSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
tvorbě	tvorba	k1gFnSc6
obsáhl	obsáhnout	k5eAaPmAgInS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
žánrů	žánr	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
již	již	k6eAd1
zmíněný	zmíněný	k2eAgInSc4d1
rock	rock	k1gInSc4
a	a	k8xC
soul	soul	k1gInSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
<g/>
,	,	kIx,
funk	funk	k1gInSc1
<g/>
,	,	kIx,
new	new	k?
jack	jack	k1gInSc1
swing	swing	k1gInSc1
a	a	k8xC
také	také	k9
například	například	k6eAd1
prvky	prvek	k1gInPc4
gospelu	gospel	k1gInSc2
či	či	k8xC
rappu	rapp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Neopomenutelný	opomenutelný	k2eNgInSc1d1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
talent	talent	k1gInSc1
pro	pro	k7c4
tanec	tanec	k1gInSc4
a	a	k8xC
pohyb	pohyb	k1gInSc4
všeobecně	všeobecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
neučil	učít	k5eNaPmAgMnS,k5eNaImAgMnS
tančit	tančit	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
přesto	přesto	k8xC
je	být	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
druhým	druhý	k4xOgMnSc7
Fredem	Fred	k1gMnSc7
Astairem	Astair	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
výborně	výborně	k6eAd1
stepoval	stepovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
Pepsi	Pepsi	k1gFnSc7
</s>
<s>
V	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Jackson	Jackson	k1gMnSc1
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
firmou	firma	k1gFnSc7
Pepsi	Pepsi	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
jej	on	k3xPp3gMnSc4
sponzorovala	sponzorovat	k5eAaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
natočil	natočit	k5eAaBmAgMnS
pro	pro	k7c4
ní	on	k3xPp3gFnSc6
několik	několik	k4yIc4
reklamních	reklamní	k2eAgNnPc2d1
videí	video	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
zazněl	zaznít	k5eAaPmAgInS,k5eAaImAgInS
hudební	hudební	k2eAgInSc1d1
motiv	motiv	k1gInSc1
upravené	upravený	k2eAgFnSc2d1
skladby	skladba	k1gFnSc2
Billie	Billie	k1gFnSc1
Jean	Jean	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
pracovalo	pracovat	k5eAaImAgNnS
za	za	k7c2
účasti	účast	k1gFnSc2
úplné	úplný	k2eAgFnSc2d1
sestavy	sestava	k1gFnSc2
skupiny	skupina	k1gFnSc2
Jacksons	Jacksonsa	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1984	#num#	k4
v	v	k7c6
Shrine	Shrin	k1gInSc5
Auditorium	auditorium	k1gNnSc1
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angeles	k1gInSc4
před	před	k7c7
3000	#num#	k4
fanoušky	fanoušek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chybnou	chybný	k2eAgFnSc7d1
časovou	časový	k2eAgFnSc7d1
koordinací	koordinace	k1gFnSc7
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Jacksonově	Jacksonův	k2eAgFnSc6d1
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
k	k	k7c3
předčasnému	předčasný	k2eAgNnSc3d1
odpálení	odpálení	k1gNnSc3
pyrotechniky	pyrotechnika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
speciálních	speciální	k2eAgInPc2d1
efektů	efekt	k1gInPc2
reklamy	reklama	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vznícení	vznícení	k1gNnSc3
Jacksonových	Jacksonův	k2eAgInPc2d1
vlasů	vlas	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
což	což	k3yQnSc4,k3yRnSc4
umělec	umělec	k1gMnSc1
nezaregistroval	zaregistrovat	k5eNaPmAgMnS
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgMnS
v	v	k7c6
produkci	produkce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
sekundách	sekunda	k1gFnPc6
plamen	plamen	k1gInSc4
uhasili	uhasit	k5eAaPmAgMnP
Jacksonovi	Jacksonův	k2eAgMnPc1d1
spoluhráči	spoluhráč	k1gMnPc1
<g/>
,	,	kIx,
přesto	přesto	k8xC
umělec	umělec	k1gMnSc1
utrpěl	utrpět	k5eAaPmAgMnS
popáleniny	popálenina	k1gFnPc4
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
na	na	k7c6
hlavě	hlava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jiné	jiný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
na	na	k7c4
motivy	motiv	k1gInPc4
(	(	kIx(
<g/>
Billie	Billie	k1gFnSc1
Jean	Jean	k1gMnSc1
a	a	k8xC
Bad	Bad	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
již	již	k6eAd1
obešly	obejít	k5eAaPmAgFnP
bez	bez	k7c2
komplikací	komplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
interprety	interpret	k1gMnPc7
</s>
<s>
Jackson	Jackson	k1gMnSc1
nazpíval	nazpívat	k5eAaBmAgMnS,k5eAaPmAgMnS
mnoho	mnoho	k4c4
skladeb	skladba	k1gFnPc2
s	s	k7c7
různými	různý	k2eAgMnPc7d1
interprety	interpret	k1gMnPc7
(	(	kIx(
<g/>
kromě	kromě	k7c2
svých	svůj	k3xOyFgMnPc2
bratrů	bratr	k1gMnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Janet	Janet	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
Scream	Scream	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Diana	Diana	k1gFnSc1
Ross	Rossa	k1gFnPc2
(	(	kIx(
<g/>
Ease	Eas	k1gMnSc2
On	on	k3xPp3gMnSc1
Down	Down	k1gMnSc1
The	The	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
,	,	kIx,
Eaten	Eaten	k2eAgMnSc1d1
alive	alivat	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
McCartney	McCartnea	k1gFnSc2
(	(	kIx(
<g/>
Say	Say	k1gMnSc1
<g/>
,	,	kIx,
Say	Say	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Say	Say	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Girl	girl	k1gFnSc2
is	is	k?
Mine	minout	k5eAaImIp3nS
<g/>
,	,	kIx,
Man	Man	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stevie	Stevie	k1gFnSc1
Wonder	Wonder	k1gMnSc1
(	(	kIx(
<g/>
Just	just	k6eAd1
Good	Good	k1gInSc1
Friends	Friendsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3T	3T	k4
(	(	kIx(
<g/>
Michaelovi	Michaelův	k2eAgMnPc1d1
synovci	synovec	k1gMnPc1
–	–	k?
I	i	k9
Need	Need	k1gMnSc1
You	You	k1gMnSc1
<g/>
,	,	kIx,
Why	Why	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
for	forum	k1gNnPc2
Africa	Africus	k1gMnSc2
–	–	k?
United	United	k1gInSc1
Support	support	k1gInSc1
of	of	k?
Artists	Artists	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
for	forum	k1gNnPc2
Africa	Africa	k1gFnSc1
(	(	kIx(
<g/>
We	We	k1gFnSc1
Are	ar	k1gInSc5
The	The	k1gFnPc6
World	Worldo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mick	Mick	k1gMnSc1
Jagger	Jagger	k1gMnSc1
(	(	kIx(
<g/>
State	status	k1gInSc5
Of	Of	k1gFnPc6
Shock	Shocko	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Freddie	Freddie	k1gFnSc1
Mercury	Mercura	k1gFnSc2
(	(	kIx(
<g/>
State	status	k1gInSc5
Of	Of	k1gMnPc4
Shock	Shock	k1gInSc4
<g/>
,	,	kIx,
There	Ther	k1gInSc5
Must	Must	k1gInSc1
Be	Be	k1gMnSc1
More	mor	k1gInSc5
To	ten	k3xDgNnSc4
Life	Lif	k1gFnPc1
Than	Thana	k1gFnPc2
This	Thisa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Eddie	Eddie	k1gFnSc1
Murphy	Murpha	k1gFnSc2
(	(	kIx(
<g/>
Whatzupwitu	Whatzupwit	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
Siedah	Siedah	k1gMnSc1
Garrett	Garrett	k1gMnSc1
(	(	kIx(
<g/>
I	i	k9
Just	just	k6eAd1
Can	Can	k1gFnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Stop	stop	k2eAgInSc4d1
Loving	Loving	k1gInSc4
You	You	k1gMnSc1
<g/>
,	,	kIx,
Man	Man	k1gMnSc1
In	In	k1gFnSc2
The	The	k1gMnSc1
Mirror	Mirror	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rockwell	Rockwell	k1gInSc1
(	(	kIx(
<g/>
Somebody	Someboda	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Watching	Watching	k1gInSc1
Me	Me	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Britney	Britney	k1gInPc1
Spears	Spears	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Way	Way	k1gMnSc2
You	You	k1gMnSc2
Make	Mak	k1gMnSc2
Me	Me	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Feel	Feel	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
will	will	k1gMnSc1
<g/>
.	.	kIx.
<g/>
i.	i.	k?
<g/>
am	am	k?
(	(	kIx(
<g/>
The	The	k1gFnSc1
Girl	girl	k1gFnSc1
Is	Is	k1gFnSc1
Mine	minout	k5eAaImIp3nS
2008	#num#	k4
<g/>
,	,	kIx,
P.	P.	kA
<g/>
Y.T.	Y.T.	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
Wanna	Wanna	k1gFnSc1
Be	Be	k1gMnSc1
Startin	Startin	k1gMnSc1
<g/>
’	’	k?
Somethin	Somethin	k1gInSc1
<g/>
’	’	k?
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Akon	Akon	k1gNnSc1
(	(	kIx(
<g/>
Wanna	Wanen	k2eAgFnSc1d1
Be	Be	k1gFnSc1
Startin	Startin	k1gInSc1
<g/>
’	’	k?
Somethin	Somethin	k1gInSc1
<g/>
’	’	k?
2008	#num#	k4
<g/>
,	,	kIx,
Hold	hold	k1gInSc1
My	my	k3xPp1nPc1
Hand	Hando	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Fergie	Fergie	k1gFnSc1
(	(	kIx(
<g/>
Beat	beat	k1gInSc1
It	It	k1gFnSc2
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Anka	Anka	k1gFnSc1
(	(	kIx(
<g/>
This	This	k1gInSc1
Is	Is	k1gMnSc2
It	It	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Barry	Barr	k1gInPc1
Gibb	Gibb	k1gInSc1
(	(	kIx(
<g/>
All	All	k1gMnSc1
In	In	k1gFnSc2
Your	Your	k1gMnSc1
Name	Name	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hudba	hudba	k1gFnSc1
pro	pro	k7c4
filmy	film	k1gInPc4
a	a	k8xC
seriály	seriál	k1gInPc4
</s>
<s>
Jackson	Jackson	k1gMnSc1
se	se	k3xPyFc4
spolupodílel	spolupodílet	k5eAaImAgMnS
i	i	k9
na	na	k7c6
hudební	hudební	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
početných	početný	k2eAgInPc2d1
filmů	film	k1gInPc2
a	a	k8xC
seriálů	seriál	k1gInPc2
(	(	kIx(
<g/>
Zachraňte	zachránit	k5eAaPmRp2nP
Willyho	Willy	k1gMnSc4
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
Simpsonovi	Simpsonův	k2eAgMnPc1d1
<g/>
,	,	kIx,
E.T.	E.T.	k1gMnSc1
–	–	k?
Mimozemšťan	mimozemšťan	k1gMnSc1
<g/>
,	,	kIx,
Moonwalker	Moonwalker	k1gMnSc1
<g/>
,	,	kIx,
Kapitán	kapitán	k1gMnSc1
EO	EO	kA
<g/>
,	,	kIx,
Čaroděj	čaroděj	k1gMnSc1
<g/>
,	,	kIx,
Safari	safari	k1gNnSc1
<g/>
,	,	kIx,
Superman	superman	k1gMnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Coververze	Coververze	k1gFnSc1
skladeb	skladba	k1gFnPc2
Michaela	Michaela	k1gFnSc1
<g/>
/	/	kIx~
<g/>
sourozenců	sourozenec	k1gMnPc2
</s>
<s>
Z	z	k7c2
některých	některý	k3yIgFnPc2
skladeb	skladba	k1gFnPc2
Jacksona	Jacksona	k1gFnSc1
<g/>
,	,	kIx,
resp.	resp.	kA
sourozenecké	sourozenecký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgFnP
i	i	k8xC
coververze	coververze	k1gFnPc1
různých	různý	k2eAgFnPc2d1
kvalit	kvalita	k1gFnPc2
–	–	k?
Mariah	Mariah	k1gMnSc1
Carey	Carea	k1gFnSc2
<g/>
:	:	kIx,
„	„	k?
<g/>
I	i	k8xC
<g/>
’	’	k?
<g/>
ll	ll	k?
be	be	k?
there	ther	k1gInSc5
<g/>
“	“	k?
<g/>
;	;	kIx,
Weird	Weird	k1gInSc1
Al	ala	k1gFnPc2
Yankovic	Yankovice	k1gFnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Fat	fatum	k1gNnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Eat	Eat	k1gFnSc1
it	it	k?
<g/>
“	“	k?
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Humanitární	humanitární	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
a	a	k8xC
reflexe	reflexe	k1gFnPc1
společenských	společenský	k2eAgInPc2d1
problémů	problém	k1gInPc2
v	v	k7c6
tvorbě	tvorba	k1gFnSc6
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
(	(	kIx(
<g/>
osobně	osobně	k6eAd1
i	i	k9
finančně	finančně	k6eAd1
<g/>
)	)	kIx)
podílel	podílet	k5eAaImAgInS
na	na	k7c6
mnohých	mnohý	k2eAgFnPc6d1
humanitárních	humanitární	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
a	a	k8xC
projektech	projekt	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1984	#num#	k4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
částku	částka	k1gFnSc4
1,5	1,5	k4
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
získal	získat	k5eAaPmAgMnS
z	z	k7c2
mimosoudního	mimosoudní	k2eAgNnSc2d1
vyrovnání	vyrovnání	k1gNnSc2
s	s	k7c7
Pepsi	Pepsi	k1gFnSc7
commercial	commercial	k1gMnSc1
po	po	k7c6
nehodě	nehoda	k1gFnSc6
z	z	k7c2
natáčení	natáčení	k1gNnSc2
reklamního	reklamní	k2eAgNnSc2d1
videa	video	k1gNnSc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
Pepsi	Pepsi	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nemocnici	nemocnice	k1gFnSc4
Brotman	Brotman	k1gMnSc1
Medical	Medical	k1gFnSc2
Center	centrum	k1gNnPc2
v	v	k7c4
Culver	Culver	k1gInSc4
City	city	k1gNnSc1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
ošetřován	ošetřovat	k5eAaImNgInS
na	na	k7c6
oddělení	oddělení	k1gNnSc6
popálenin	popálenina	k1gFnPc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Burn	Burn	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
Lionelem	Lionel	k1gMnSc7
Richiem	Richius	k1gMnSc7
na	na	k7c6
charitativním	charitativní	k2eAgInSc6d1
singlu	singl	k1gInSc6
We	We	k1gFnSc2
Are	ar	k1gInSc5
the	the	k?
World	World	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejúspěšnějších	úspěšný	k2eAgInPc2d3
v	v	k7c6
historii	historie	k1gFnSc6
(	(	kIx(
<g/>
prodáno	prodat	k5eAaPmNgNnS
téměř	téměř	k6eAd1
20	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
a	a	k8xC
miliony	milion	k4xCgInPc4
dolarů	dolar	k1gInPc2
byly	být	k5eAaImAgFnP
věnovány	věnovat	k5eAaPmNgFnP,k5eAaImNgFnP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
hladomoru	hladomor	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnos	výnos	k1gInSc1
z	z	k7c2
prodeje	prodej	k1gInSc2
nahrávky	nahrávka	k1gFnSc2
písně	píseň	k1gFnSc2
Man	Man	k1gMnSc1
In	In	k1gMnSc1
the	the	k?
Mirror	Mirror	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
realizována	realizován	k2eAgFnSc1d1
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
šel	jít	k5eAaImAgMnS
na	na	k7c4
podporu	podpora	k1gFnSc4
Camp	camp	k1gInSc1
Ronald	Ronald	k1gMnSc1
McDonald	McDonald	k1gMnSc1
for	forum	k1gNnPc2
Good	Good	k1gMnSc1
Times	Times	k1gMnSc1
věnující	věnující	k2eAgMnSc1d1
se	se	k3xPyFc4
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
trpí	trpět	k5eAaImIp3nP
rakovinou	rakovina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
založil	založit	k5eAaPmAgMnS
charitativní	charitativní	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
Heal	Heala	k1gFnPc2
the	the	k?
World	World	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
Uzdravme	uzdravit	k5eAaPmRp1nP
svět	svět	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zpřístupnila	zpřístupnit	k5eAaPmAgFnS
dětem	dítě	k1gFnPc3
Neverland	Neverlanda	k1gFnPc2
a	a	k8xC
posílala	posílat	k5eAaImAgFnS
do	do	k7c2
světa	svět	k1gInSc2
miliony	milion	k4xCgInPc1
dolarů	dolar	k1gInPc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
válkami	válka	k1gFnPc7
a	a	k8xC
nemocemi	nemoc	k1gFnPc7
ohroženým	ohrožený	k2eAgMnSc7d1
lidem	člověk	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdával	vzdávat	k5eAaImAgMnS
se	se	k3xPyFc4
honorářů	honorář	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
svého	svůj	k3xOyFgInSc2
podílu	podíl	k1gInSc2
z	z	k7c2
výnosu	výnos	k1gInSc2
Victory	Victor	k1gMnPc4
Tour	Tour	k1gInSc4
skupiny	skupina	k1gFnPc1
The	The	k1gFnSc2
Jacksons	Jacksonsa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
zisku	zisk	k1gInSc2
z	z	k7c2
Dangerous	Dangerous	k1gInSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
o	o	k7c6
69	#num#	k4
koncertech	koncert	k1gInPc6
v	v	k7c6
letech	let	k1gInPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
atd.	atd.	kA
</s>
<s>
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
na	na	k7c4
charitativní	charitativní	k2eAgInPc4d1
účely	účel	k1gInPc4
údajně	údajně	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Texty	text	k1gInPc1
mnohých	mnohé	k1gNnPc2
svých	svůj	k3xOyFgFnPc2
písní	píseň	k1gFnPc2
Jackson	Jackson	k1gMnSc1
poukazoval	poukazovat	k5eAaImAgMnS
na	na	k7c4
aktuální	aktuální	k2eAgInPc4d1
společenské	společenský	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
mnohdy	mnohdy	k6eAd1
týkaly	týkat	k5eAaImAgFnP
osobně	osobně	k6eAd1
–	–	k?
na	na	k7c4
rasismus	rasismus	k1gInSc4
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
sociální	sociální	k2eAgFnPc4d1
nerovnosti	nerovnost	k1gFnPc4
<g/>
,	,	kIx,
násilí	násilí	k1gNnSc4
<g/>
,	,	kIx,
hladomor	hladomor	k1gInSc1
<g/>
,	,	kIx,
zneužívání	zneužívání	k1gNnSc1
drog	droga	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
téma	téma	k1gNnSc4
stále	stále	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
kontroverzní	kontroverzní	k2eAgFnSc7d1
<g/>
,	,	kIx,
na	na	k7c4
ničení	ničení	k1gNnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
nebo	nebo	k8xC
i	i	k9
na	na	k7c6
mediální	mediální	k2eAgFnSc6d1
manipulaci	manipulace	k1gFnSc6
(	(	kIx(
<g/>
Black	Black	k1gInSc1
or	or	k?
White	Whit	k1gMnSc5
<g/>
,	,	kIx,
Gone	Gon	k1gMnPc4
Too	Too	k1gMnSc1
Soon	Soon	k1gMnSc1
<g/>
,	,	kIx,
Heal	Heal	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
<g/>
,	,	kIx,
Earth	Earth	k1gMnSc1
Song	song	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
For	forum	k1gNnPc2
Africa	Africa	k1gFnSc1
(	(	kIx(
<g/>
United	United	k1gInSc1
Support	support	k1gInSc1
of	of	k?
Artists	Artists	k1gInSc4
for	forum	k1gNnPc2
Africa	Africus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
We	We	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
World	World	k1gMnSc1
<g/>
,	,	kIx,
Will	Will	k1gMnSc1
You	You	k1gMnSc1
Be	Be	k1gMnSc5
There	Ther	k1gMnSc5
<g/>
,	,	kIx,
Tabloid	tabloid	k1gInSc1
Junkie	Junkie	k1gFnSc1
<g/>
,	,	kIx,
Scream	Scream	k1gInSc1
<g/>
,	,	kIx,
Money	Money	k1gInPc1
<g/>
,	,	kIx,
They	Thea	k1gFnPc1
Don	Don	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
Care	car	k1gMnSc5
About	About	k1gMnSc1
Us	Us	k1gFnSc1
<g/>
,	,	kIx,
Little	Little	k1gFnSc1
Susie	Susie	k1gFnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Lost	Losta	k1gFnPc2
Children	Childrna	k1gFnPc2
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
charitativní	charitativní	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
dostal	dostat	k5eAaPmAgMnS
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1984	#num#	k4
cenu	cena	k1gFnSc4
z	z	k7c2
rukou	ruka	k1gFnPc2
amerického	americký	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Ronalda	Ronald	k1gMnSc2
Reagana	Reagan	k1gMnSc2
a	a	k8xC
za	za	k7c4
podporu	podpora	k1gFnSc4
39	#num#	k4
charitativních	charitativní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
připsán	připsat	k5eAaPmNgInS
do	do	k7c2
Guinessovy	Guinessův	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
rekordů	rekord	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgMnS
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
vyznamenán	vyznamenat	k5eAaPmNgInS
cenou	cena	k1gFnSc7
od	od	k7c2
The	The	k1gFnSc2
African	African	k1gInSc1
Ambassadors	Ambassadors	k1gInSc1
<g/>
’	’	k?
Spouses	Spouses	k1gInSc1
Association	Association	k1gInSc1
za	za	k7c4
celosvětové	celosvětový	k2eAgNnSc4d1
humanitární	humanitární	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zahrnovalo	zahrnovat	k5eAaImAgNnS
i	i	k8xC
finanční	finanční	k2eAgNnSc4d1
přispívání	přispívání	k1gNnSc4
charitativním	charitativní	k2eAgFnPc3d1
organizacím	organizace	k1gFnPc3
bojujícím	bojující	k2eAgFnPc3d1
proti	proti	k7c3
hladomoru	hladomor	k1gMnSc3
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Jacksonově	Jacksonův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
navrhla	navrhnout	k5eAaPmAgFnS
demokratická	demokratický	k2eAgFnSc1d1
kongresmanka	kongresmanka	k1gFnSc1
z	z	k7c2
Texasu	Texas	k1gInSc2
Sheila	Sheila	k1gFnSc1
Jackson	Jackson	k1gMnSc1
Lee	Lea	k1gFnSc6
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
reprezentantů	reprezentant	k1gInPc2
projednání	projednání	k1gNnSc4
rezoluce	rezoluce	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
Jacksonovi	Jackson	k1gMnSc3
přiznat	přiznat	k5eAaPmF
(	(	kIx(
<g/>
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
za	za	k7c4
mimořádné	mimořádný	k2eAgFnPc4d1
humanitární	humanitární	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
<g/>
)	)	kIx)
status	status	k1gInSc4
americké	americký	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
eventuálním	eventuální	k2eAgFnPc3d1
kontroverzím	kontroverze	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
rezoluce	rezoluce	k1gFnSc1
způsobit	způsobit	k5eAaPmF
<g/>
,	,	kIx,
však	však	k9
byla	být	k5eAaImAgFnS
zamítnuta	zamítnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Posmrtně	posmrtně	k6eAd1
byl	být	k5eAaImAgMnS
Jackson	Jackson	k1gMnSc1
za	za	k7c2
humanitární	humanitární	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
vyznamenán	vyznamenat	k5eAaPmNgInS
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
cenou	cena	k1gFnSc7
Save	Sav	k1gMnSc2
the	the	k?
World	World	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
převzal	převzít	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Jermaine	Jermain	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
s	s	k7c7
dětmi	dítě	k1gFnPc7
v	v	k7c6
Disneylandu	Disneyland	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
(	(	kIx(
<g/>
Prince	princ	k1gMnSc2
Michael	Michael	k1gMnSc1
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
vpravo	vpravo	k6eAd1
<g/>
,	,	kIx,
Prince	princa	k1gFnSc3
Michael	Michael	k1gMnSc1
II	II	kA
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Jacksonův	Jacksonův	k2eAgInSc4d1
ranč	ranč	k1gInSc4
Neverland	Neverlanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Celosvětové	celosvětový	k2eAgNnSc4d1
turné	turné	k1gNnSc4
HIStory	HIStor	k1gInPc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
<g/>
)	)	kIx)
doprovázela	doprovázet	k5eAaImAgFnS
velkolepá	velkolepý	k2eAgFnSc1d1
Jacksonova	Jacksonův	k2eAgFnSc1d1
socha	socha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stála	stát	k5eAaImAgFnS
i	i	k9
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Letné	Letná	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
umělec	umělec	k1gMnSc1
zahajoval	zahajovat	k5eAaImAgMnS
koncertní	koncertní	k2eAgFnSc4d1
šňůru	šňůra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Hvězda	hvězda	k1gFnSc1
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
na	na	k7c6
Hollywoodském	hollywoodský	k2eAgInSc6d1
chodníku	chodník	k1gInSc6
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podpis	podpis	k1gInSc1
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Barva	barva	k1gFnSc1
kůže	kůže	k1gFnSc2
</s>
<s>
Jackson	Jackson	k1gMnSc1
byl	být	k5eAaImAgMnS
sice	sice	k8xC
afroamerického	afroamerický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
kůže	kůže	k1gFnSc2
ale	ale	k8xC
postupně	postupně	k6eAd1
získávala	získávat	k5eAaImAgFnS
světlejší	světlý	k2eAgInSc4d2
odstín	odstín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
byla	být	k5eAaImAgFnS
vyvolána	vyvolat	k5eAaPmNgFnS
kožním	kožní	k2eAgNnSc7d1
onemocněním	onemocnění	k1gNnSc7
zvaném	zvaný	k2eAgInSc6d1
vitiligo	vitiligo	k1gNnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
relativně	relativně	k6eAd1
neznámé	známý	k2eNgNnSc4d1
kožní	kožní	k2eAgNnSc4d1
onemocnění	onemocnění	k1gNnSc4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
výpadkům	výpadek	k1gInPc3
pigmentu	pigment	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
biografie	biografie	k1gFnSc2
J.	J.	kA
Randyho	Randy	k1gMnSc2
Taraborrelliho	Taraborrelli	k1gMnSc2
bylo	být	k5eAaImAgNnS
Jacksnovi	Jacksnův	k2eAgMnPc1d1
vitiligo	vitiliga	k1gFnSc5
diagnostikováno	diagnostikovat	k5eAaBmNgNnS
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
informace	informace	k1gFnPc4
potvrdil	potvrdit	k5eAaPmAgMnS
i	i	k9
Jacksonův	Jacksonův	k2eAgMnSc1d1
osobní	osobní	k2eAgMnSc1d1
dermatolog	dermatolog	k1gMnSc1
Dr	dr	kA
<g/>
.	.	kIx.
Arnold	Arnold	k1gMnSc1
Klein	Klein	k1gMnSc1
v	v	k7c4
talk	talk	k1gInSc4
show	show	k1gFnSc1
Larryho	Larry	k1gMnSc2
Kinga	King	k1gMnSc2
na	na	k7c6
americké	americký	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
CNN	CNN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jackson	Jackson	k1gMnSc1
kryl	krýt	k5eAaImAgMnS
příznaky	příznak	k1gInPc1
zpočátku	zpočátku	k6eAd1
tmavým	tmavý	k2eAgInSc7d1
make-upem	make-up	k1gInSc7
<g/>
,	,	kIx,
s	s	k7c7
postupným	postupný	k2eAgInSc7d1
rozvojem	rozvoj	k1gInSc7
choroby	choroba	k1gFnSc2
ale	ale	k8xC
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
make-up	make-up	k1gInSc1
světlý	světlý	k2eAgInSc1d1
a	a	k8xC
údajně	údajně	k6eAd1
i	i	k9
speciální	speciální	k2eAgInPc4d1
přípravky	přípravek	k1gInPc4
na	na	k7c4
zestejnění	zestejnění	k1gNnSc4
zbývajících	zbývající	k2eAgFnPc2d1
tmavých	tmavý	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
si	se	k3xPyFc3
vytvořit	vytvořit	k5eAaPmF
tmavé	tmavý	k2eAgNnSc4d1
tetování	tetování	k1gNnSc4
dokreslující	dokreslující	k2eAgNnSc4d1
obočí	obočí	k1gNnSc4
a	a	k8xC
drobné	drobný	k2eAgNnSc4d1
růžové	růžový	k2eAgNnSc4d1
tetování	tetování	k1gNnSc4
u	u	k7c2
rtů	ret	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Veřejnost	veřejnost	k1gFnSc1
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
onemocnění	onemocnění	k1gNnSc6
poprvé	poprvé	k6eAd1
informoval	informovat	k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
s	s	k7c7
Oprah	Opraha	k1gFnPc2
Winfreyovou	Winfreyový	k2eAgFnSc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
mimo	mimo	k6eAd1
jiné	jiná	k1gFnSc2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
rozvoj	rozvoj	k1gInSc4
vitiliga	vitiliga	k1gFnSc1
byl	být	k5eAaImAgInS
geneticky	geneticky	k6eAd1
podmíněn	podmínit	k5eAaPmNgInS
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc4,k3yQnSc4
později	pozdě	k6eAd2
potvrdily	potvrdit	k5eAaPmAgFnP
i	i	k9
Jacksonovy	Jacksonův	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
Janet	Janeta	k1gFnPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
La	la	k1gNnSc1
Toya	Toy	k1gInSc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
2009	#num#	k4
La	la	k1gNnSc2
Toya	Toyum	k1gNnSc2
sdělila	sdělit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
pigmentovou	pigmentový	k2eAgFnSc4d1
poruchu	porucha	k1gFnSc4
zdědil	zdědit	k5eAaPmAgInS
i	i	k9
Jacksonův	Jacksonův	k2eAgMnSc1d1
starší	starý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Prince	princ	k1gMnSc2
Michael	Michael	k1gMnSc1
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
má	mít	k5eAaImIp3nS
skvrny	skvrna	k1gFnSc2
patrné	patrný	k2eAgFnSc2d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
rukou	ruka	k1gFnPc2
a	a	k8xC
krku	krk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Pitevní	pitevní	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgMnS
zpěvák	zpěvák	k1gMnSc1
na	na	k7c6
těle	tělo	k1gNnSc6
<g/>
,	,	kIx,
hlavně	hlavně	k9
pak	pak	k6eAd1
na	na	k7c6
hrudi	hruď	k1gFnSc6
<g/>
,	,	kIx,
břichu	břich	k1gInSc6
<g/>
,	,	kIx,
obličeji	obličej	k1gInSc6
a	a	k8xC
pažích	paže	k1gFnPc6
v	v	k7c6
době	doba	k1gFnSc6
úmrtí	úmrtí	k1gNnSc2
světlé	světlý	k2eAgFnSc2d1
skvrny	skvrna	k1gFnSc2
svědčící	svědčící	k2eAgInSc1d1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
vitiligem	vitiligo	k1gNnSc7
skutečně	skutečně	k6eAd1
trpěl	trpět	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plastické	plastický	k2eAgFnPc1d1
operace	operace	k1gFnPc1
</s>
<s>
První	první	k4xOgFnSc4
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
Jackson	Jacksona	k1gFnPc2
podstoupil	podstoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
zlomil	zlomit	k5eAaPmAgMnS
nos	nos	k1gInSc4
při	při	k7c6
taneční	taneční	k2eAgFnSc6d1
zkoušce	zkouška	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nos	nos	k1gInSc1
si	se	k3xPyFc3
nechal	nechat	k5eAaPmAgInS
opět	opět	k6eAd1
upravit	upravit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
po	po	k7c4
ošetření	ošetření	k1gNnSc4
popálenin	popálenina	k1gFnPc2
z	z	k7c2
pyrotechnické	pyrotechnický	k2eAgFnSc2d1
nehody	nehoda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
přihodila	přihodit	k5eAaPmAgFnS
při	při	k7c6
natáčení	natáčení	k1gNnSc6
reklamního	reklamní	k2eAgInSc2d1
videoklipu	videoklip	k1gInSc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
Pepsi	Pepsi	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dokumentu	dokument	k1gInSc6
britského	britský	k2eAgMnSc2d1
novináře	novinář	k1gMnSc2
Martina	Martin	k1gMnSc2
Bashira	Bashira	k1gFnSc1
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Dokumenty	dokument	k1gInPc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
umělec	umělec	k1gMnSc1
svěřuje	svěřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
mládí	mládí	k1gNnSc6
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vzhled	vzhled	k1gInSc4
<g/>
,	,	kIx,
např.	např.	kA
tvar	tvar	k1gInSc4
nosu	nos	k1gInSc2
<g/>
,	,	kIx,
terčem	terč	k1gInSc7
zvláště	zvláště	k6eAd1
otcových	otcův	k2eAgFnPc2d1
zraňujících	zraňující	k2eAgFnPc2d1
narážek	narážka	k1gFnPc2
nebo	nebo	k8xC
si	se	k3xPyFc3
potrpěl	potrpět	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c4
akné	akné	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
psychologové	psycholog	k1gMnPc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Jackson	Jackson	k1gMnSc1
trpěl	trpět	k5eAaImAgMnS
dysmorfofobií	dysmorfofobie	k1gFnSc7
<g/>
,	,	kIx,
psychickým	psychický	k2eAgInSc7d1
stavem	stav	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
trpící	trpící	k2eAgNnSc4d1
obává	obávat	k5eAaImIp3nS
přijetí	přijetí	k1gNnSc4
svého	svůj	k3xOyFgInSc2
vzhledu	vzhled	k1gInSc2
okolím	okolí	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Přes	přes	k7c4
spekulace	spekulace	k1gFnPc4
médií	médium	k1gNnPc2
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nepotvrdilo	potvrdit	k5eNaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Michael	Michael	k1gMnSc1
podstoupil	podstoupit	k5eAaPmAgMnS
jinou	jiný	k2eAgFnSc4d1
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
operaci	operace	k1gFnSc4
nosu	nos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Jackson	Jackson	k1gMnSc1
v	v	k7c6
dokumentu	dokument	k1gInSc6
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
podstoupil	podstoupit	k5eAaPmAgMnS
pouze	pouze	k6eAd1
plastické	plastický	k2eAgFnPc4d1
operace	operace	k1gFnPc4
nosu	nos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
otázku	otázka	k1gFnSc4
zda	zda	k8xS
nepodstoupil	podstoupit	k5eNaPmAgInS
operaci	operace	k1gFnSc4
lícních	lícní	k2eAgFnPc2d1
kostí	kost	k1gFnPc2
odpověděl	odpovědět	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Tyto	tento	k3xDgFnPc1
lícní	lícní	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ne	Ne	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můj	můj	k1gMnSc1
otec	otec	k1gMnSc1
má	mít	k5eAaImIp3nS
úplně	úplně	k6eAd1
stejné	stejný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Máme	mít	k5eAaImIp1nP
indiánskou	indiánský	k2eAgFnSc4d1
krev	krev	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Zdravotní	zdravotní	k2eAgInPc1d1
problémy	problém	k1gInPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
byla	být	k5eAaImAgFnS
Jacksonovi	Jacksonův	k2eAgMnPc1d1
diagnostikována	diagnostikovat	k5eAaBmNgFnS
dvě	dva	k4xCgNnPc1
kožní	kožní	k2eAgNnPc1d1
onemocnění	onemocnění	k1gNnPc1
<g/>
:	:	kIx,
postupně	postupně	k6eAd1
ustupující	ustupující	k2eAgInSc4d1
lupus	lupus	k1gInSc4
a	a	k8xC
rozšiřující	rozšiřující	k2eAgNnSc4d1
se	se	k3xPyFc4
vitiligo	vitiligo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
způsobuje	způsobovat	k5eAaImIp3nS
ztrátu	ztráta	k1gFnSc4
pigmentu	pigment	k1gInSc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Barva	barva	k1gFnSc1
kůže	kůže	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
Jacksonova	Jacksonův	k2eAgInSc2d1
života	život	k1gInSc2
se	se	k3xPyFc4
množily	množit	k5eAaImAgFnP
bulvární	bulvární	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
údajném	údajný	k2eAgNnSc6d1
chatrném	chatrný	k2eAgNnSc6d1
zdraví	zdraví	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
buď	buď	k8xC
dementovány	dementován	k2eAgFnPc1d1
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
zůstaly	zůstat	k5eAaPmAgFnP
nepotvrzeny	potvrdit	k5eNaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popíraly	popírat	k5eAaImAgInP
je	být	k5eAaImIp3nS
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnSc4
předimenzovanou	předimenzovaný	k2eAgFnSc4d1
prezentaci	prezentace	k1gFnSc4
snižovaly	snižovat	k5eAaImAgInP
i	i	k9
samotné	samotný	k2eAgInPc1d1
Jacksonovy	Jacksonův	k2eAgInPc1d1
veřejné	veřejný	k2eAgInPc1d1
výstupy	výstup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Pitva	pitva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Soudy	soud	k1gInPc1
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1993	#num#	k4
byl	být	k5eAaImAgInS
Jackson	Jackson	k1gInSc1
nařknut	nařknout	k5eAaPmNgInS
zubařem	zubař	k1gMnSc7
Evanem	Evan	k1gMnSc7
Chandlerem	Chandler	k1gMnSc7
z	z	k7c2
údajného	údajný	k2eAgNnSc2d1
zneužívání	zneužívání	k1gNnSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
třináctiletého	třináctiletý	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Jordana	Jordan	k1gMnSc2
Chandlera	Chandler	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požadavek	požadavek	k1gInSc1
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
byl	být	k5eAaImAgInS
stáhnut	stáhnout	k5eAaPmNgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
Jacksonova	Jacksonův	k2eAgFnSc1d1
pojistka	pojistka	k1gFnSc1
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1994	#num#	k4
pokryla	pokrýt	k5eAaPmAgFnS
finanční	finanční	k2eAgFnSc1d1
vyrovnání	vyrovnání	k1gNnSc3
částkou	částka	k1gFnSc7
22	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
strategie	strategie	k1gFnPc4
byla	být	k5eAaImAgFnS
Jacksonovi	Jacksonův	k2eAgMnPc1d1
doporučena	doporučit	k5eAaPmNgFnS
z	z	k7c2
prostého	prostý	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
—	—	k?
datum	datum	k1gNnSc1
civilního	civilní	k2eAgInSc2d1
procesu	proces	k1gInSc2
byl	být	k5eAaImAgInS
nastaven	nastavit	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
předcházel	předcházet	k5eAaImAgInS
kriminálnímu	kriminální	k2eAgInSc3d1
procesu	proces	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k9
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
obžaloba	obžaloba	k1gFnSc1
by	by	kYmCp3nS
během	během	k7c2
civilního	civilní	k2eAgInSc2d1
procesu	proces	k1gInSc2
získala	získat	k5eAaPmAgFnS
poznatky	poznatek	k1gInPc4
o	o	k7c4
strategii	strategie	k1gFnSc4
Jacksonovy	Jacksonův	k2eAgFnSc2d1
obhajoby	obhajoba	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
proti	proti	k7c3
Jacksonovi	Jackson	k1gMnSc3
bylo	být	k5eAaImAgNnS
následně	následně	k6eAd1
použito	použít	k5eAaPmNgNnS
v	v	k7c6
procesu	proces	k1gInSc6
kriminálním	kriminální	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jacksonovi	Jacksonův	k2eAgMnPc1d1
právníci	právník	k1gMnPc1
na	na	k7c4
toto	tento	k3xDgNnSc4
nespravedlivé	spravedlivý	k2eNgNnSc4d1
zacházení	zacházení	k1gNnSc4
před	před	k7c7
zahájením	zahájení	k1gNnSc7
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
několikrát	několikrát	k6eAd1
upozorňovali	upozorňovat	k5eAaImAgMnP
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
jim	on	k3xPp3gMnPc3
avšak	avšak	k8xC
vyhověno	vyhověn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgNnSc7d1
východiskem	východisko	k1gNnSc7
bylo	být	k5eAaImAgNnS
tedy	tedy	k9
přistoupení	přistoupení	k1gNnSc4
na	na	k7c4
vyrovnání	vyrovnání	k1gNnSc4
(	(	kIx(
<g/>
v	v	k7c6
civilním	civilní	k2eAgInSc6d1
procesu	proces	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
nakonec	nakonec	k6eAd1
došlo	dojít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
proces	proces	k1gInSc4
civilní	civilní	k2eAgFnPc1d1
<g/>
,	,	kIx,
vyrovnání	vyrovnání	k1gNnSc1
bylo	být	k5eAaImAgNnS
uhrazeno	uhradit	k5eAaPmNgNnS
z	z	k7c2
Jacksonovy	Jacksonův	k2eAgFnSc2d1
pojistky	pojistka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
ne	ne	k9
z	z	k7c2
„	„	k?
<g/>
jeho	jeho	k3xOp3gFnPc1
kapsy	kapsa	k1gFnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
často	často	k6eAd1
nesprávně	správně	k6eNd1
uváděno	uvádět	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
případ	případ	k1gInSc1
přispěl	přispět	k5eAaPmAgInS
také	také	k9
ke	k	k7c3
změně	změna	k1gFnSc3
zákona	zákon	k1gInSc2
—	—	k?
civilní	civilní	k2eAgInSc1d1
soud	soud	k1gInSc1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nesmí	smět	k5eNaImIp3nS
předcházet	předcházet	k5eAaImF
kriminálnímu	kriminální	k2eAgInSc3d1
soudu	soud	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
rodiny	rodina	k1gFnSc2
Chandlerů	Chandler	k1gMnPc2
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
ukončil	ukončit	k5eAaPmAgMnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
otcem	otec	k1gMnSc7
Evanem	Evano	k1gNnSc7
jakýkoliv	jakýkoliv	k3yIgInSc4
kontakt	kontakt	k1gInSc4
<g/>
;	;	kIx,
traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
z	z	k7c2
důvodu	důvod	k1gInSc2
falešného	falešný	k2eAgNnSc2d1
obvinění	obvinění	k1gNnSc2
Jacksona	Jackson	k1gMnSc2
z	z	k7c2
pedofilie	pedofilie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
pravdivosti	pravdivost	k1gFnSc3
těchto	tento	k3xDgFnPc2
domněnek	domněnka	k1gFnPc2
přispívá	přispívat	k5eAaImIp3nS
i	i	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
jen	jen	k9
pár	pár	k4xCyI
týdnů	týden	k1gInPc2
po	po	k7c6
Jacksonově	Jacksonův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
spáchal	spáchat	k5eAaPmAgMnS
zubař	zubař	k1gMnSc1
Evan	Evan	k1gMnSc1
Chandler	Chandler	k1gMnSc1
sebevraždu	sebevražda	k1gFnSc4
střelbou	střelba	k1gFnSc7
do	do	k7c2
hlavy	hlava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
pobývá	pobývat	k5eAaImIp3nS
pod	pod	k7c7
změněným	změněný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
Long	Long	k1gMnSc1
Island	Island	k1gInSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
a	a	k8xC
odmítá	odmítat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
případu	případ	k1gInSc3
jakkoliv	jakkoliv	k6eAd1
vyjadřovat	vyjadřovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
oprávněný	oprávněný	k2eAgInSc1d1
strach	strach	k1gInSc1
z	z	k7c2
možné	možný	k2eAgFnSc2d1
nucené	nucený	k2eAgFnSc2d1
náhrady	náhrada	k1gFnSc2
nečestně	čestně	k6eNd1
získaných	získaný	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2003	#num#	k4
byl	být	k5eAaImAgMnS
Jackson	Jackson	k1gMnSc1
obviněn	obvinit	k5eAaPmNgMnS
ze	z	k7c2
sexuálního	sexuální	k2eAgNnSc2d1
obtěžování	obtěžování	k1gNnSc2
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
rodinou	rodina	k1gFnSc7
třináctiletého	třináctiletý	k2eAgMnSc4d1
chlapce	chlapec	k1gMnSc4
Gavina	Gavin	k1gMnSc4
Arviza	Arviz	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
veřejnosti	veřejnost	k1gFnSc2
znám	znám	k2eAgInSc1d1
už	už	k9
z	z	k7c2
kontroverzního	kontroverzní	k2eAgInSc2d1
záběru	záběr	k1gInSc2
z	z	k7c2
dříve	dříve	k6eAd2
odvysílaného	odvysílaný	k2eAgMnSc2d1
(	(	kIx(
<g/>
v	v	k7c6
USA	USA	kA
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2003	#num#	k4
<g/>
)	)	kIx)
dokumentu	dokument	k1gInSc2
britského	britský	k2eAgMnSc2d1
novináře	novinář	k1gMnSc2
Martina	Martin	k1gMnSc2
Bashira	Bashira	k1gFnSc1
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
chlapec	chlapec	k1gMnSc1
pokládá	pokládat	k5eAaImIp3nS
Jacksonovi	Jackson	k1gMnSc3
hlavu	hlava	k1gFnSc4
na	na	k7c4
rameno	rameno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jackson	Jackson	k1gNnSc1
v	v	k7c6
Gavinově	Gavinův	k2eAgFnSc6d1
přítomnosti	přítomnost	k1gFnSc6
otevřeně	otevřeně	k6eAd1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
Gavinem	Gavin	k1gInSc7
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
dalšími	další	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
občas	občas	k6eAd1
spává	spávat	k5eAaImIp3nS
ve	v	k7c6
společné	společný	k2eAgFnSc6d1
ložnici	ložnice	k1gFnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
výslovně	výslovně	k6eAd1
přejí	přát	k5eAaImIp3nP
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
s	s	k7c7
tím	ten	k3xDgNnSc7
souhlasí	souhlasit	k5eAaImIp3nP
–	–	k?
sám	sám	k3xTgMnSc1
jim	on	k3xPp3gMnPc3
ale	ale	k8xC
tuto	tento	k3xDgFnSc4
alternativu	alternativa	k1gFnSc4
nenabízí	nabízet	k5eNaImIp3nS
a	a	k8xC
má	mít	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pokoje	pokoj	k1gInSc2
pro	pro	k7c4
hosty	host	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
dokumentu	dokument	k1gInSc2
prohlašuje	prohlašovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
na	na	k7c4
sdílení	sdílení	k1gNnSc4
jedné	jeden	k4xCgFnSc2
postele	postel	k1gFnSc2
s	s	k7c7
dětmi	dítě	k1gFnPc7
neshledává	shledávat	k5eNaImIp3nS
nic	nic	k3yNnSc1
špatného	špatný	k2eAgNnSc2d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
strany	strana	k1gFnSc2
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
nijak	nijak	k6eAd1
sexuálně	sexuálně	k6eAd1
motivované	motivovaný	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodává	dodávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
takto	takto	k6eAd1
byli	být	k5eAaImAgMnP
on	on	k3xPp3gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
sourozenci	sourozenec	k1gMnPc1
vychováni	vychovat	k5eAaPmNgMnP
a	a	k8xC
že	že	k8xS
takto	takto	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
chovat	chovat	k5eAaImF
celý	celý	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
poslední	poslední	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
základní	základní	k2eAgFnSc2d1
rodinné	rodinný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
zásadně	zásadně	k6eAd1
zpřetrhávají	zpřetrhávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudní	soudní	k2eAgInSc1d1
psycholog	psycholog	k1gMnSc1
Dr	dr	kA
<g/>
.	.	kIx.
Stanley	Stanlea	k1gMnSc2
Katz	Katz	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Jacksona	Jackson	k1gMnSc4
nelze	lze	k6eNd1
kvalifikovat	kvalifikovat	k5eAaBmF
jako	jako	k9
pedofila	pedofil	k1gMnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
pouze	pouze	k6eAd1
zažívá	zažívat	k5eAaImIp3nS
návrat	návrat	k1gInSc4
do	do	k7c2
ztraceného	ztracený	k2eAgNnSc2d1
dětství	dětství	k1gNnSc2
<g/>
,	,	kIx,
čemuž	což	k3yQnSc3,k3yRnSc3
víceméně	víceméně	k9
odpovídá	odpovídat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
duševní	duševní	k2eAgNnSc4d1
rozpoložení	rozpoložení	k1gNnSc4
a	a	k8xC
chování	chování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
mezi	mezi	k7c7
obviněním	obvinění	k1gNnSc7
a	a	k8xC
soudem	soud	k1gInSc7
se	se	k3xPyFc4
Jackson	Jackson	k1gMnSc1
údajně	údajně	k6eAd1
stal	stát	k5eAaPmAgMnS
závislý	závislý	k2eAgMnSc1d1
na	na	k7c6
uklidňujícím	uklidňující	k2eAgInSc6d1
léku	lék	k1gInSc6
Demerolu	Demerol	k1gInSc2
a	a	k8xC
výrazně	výrazně	k6eAd1
ztratil	ztratit	k5eAaPmAgMnS
na	na	k7c6
váze	váha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
The	The	k1gMnSc2
People	People	k1gMnSc2
of	of	k?
the	the	k?
State	status	k1gInSc5
of	of	k?
California	Californium	k1gNnSc2
v.	v.	k?
Michael	Michael	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
Jackson	Jackson	k1gMnSc1
čelil	čelit	k5eAaImAgMnS
obvinění	obvinění	k1gNnSc4
Arviza	Arviz	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
sourozenců	sourozenec	k1gMnPc2
ze	z	k7c2
sexuálního	sexuální	k2eAgNnSc2d1
obtěžování	obtěžování	k1gNnSc2
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
a	a	k8xC
Jacksonovi	Jacksonův	k2eAgMnPc1d1
v	v	k7c6
něm	on	k3xPp3gMnSc6
hrozilo	hrozit	k5eAaImAgNnS
až	až	k6eAd1
20	#num#	k4
let	léto	k1gNnPc2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
jej	on	k3xPp3gInSc4
porota	porota	k1gFnSc1
vrchního	vrchní	k2eAgInSc2d1
soudu	soud	k1gInSc2
státu	stát	k1gInSc2
Kalifornie	Kalifornie	k1gFnSc1
shledala	shledat	k5eAaPmAgFnS
nevinným	vinný	k2eNgInSc7d1
ve	v	k7c6
všech	všecek	k3xTgNnPc6
deseti	deset	k4xCc6
bodech	bod	k1gInPc6
obžaloby	obžaloba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřování	vyšetřování	k1gNnSc1
a	a	k8xC
soud	soud	k1gInSc1
trval	trvat	k5eAaImAgInS
574	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
procesu	proces	k1gInSc2
se	se	k3xPyFc4
Jackson	Jackson	k1gMnSc1
uchýlil	uchýlit	k5eAaPmAgMnS
do	do	k7c2
Bahrajnu	Bahrajn	k1gInSc2
<g/>
,	,	kIx,
malého	malý	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Perského	perský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
přestože	přestože	k8xS
se	se	k3xPyFc4
obvinění	obviněný	k1gMnPc1
ze	z	k7c2
sexuálního	sexuální	k2eAgNnSc2d1
zneužívání	zneužívání	k1gNnSc2
dětí	dítě	k1gFnPc2
ukazují	ukazovat	k5eAaImIp3nP
jako	jako	k9
falešná	falešný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jacksonova	Jacksonův	k2eAgFnSc1d1
reputace	reputace	k1gFnSc1
byla	být	k5eAaImAgFnS
kvůli	kvůli	k7c3
neutuchajícím	utuchající	k2eNgFnPc3d1
mediálním	mediální	k2eAgFnPc3d1
spekulacím	spekulace	k1gFnPc3
u	u	k7c2
části	část	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
poškozena	poškodit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
otcovství	otcovství	k1gNnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
byl	být	k5eAaImAgMnS
dvakrát	dvakrát	k6eAd1
ženatý	ženatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
manželkou	manželka	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
zpěvačka	zpěvačka	k1gFnSc1
Lisa	Lisa	k1gFnSc1
Marie	Marie	k1gFnSc1
Presley	Preslea	k1gFnSc2
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
Elvise	Elvis	k1gMnSc2
Presleyho	Presley	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
Jackson	Jackson	k1gInSc1
seznámil	seznámit	k5eAaPmAgInS
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatba	svatba	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
květnu	květen	k1gInSc3
roku	rok	k1gInSc2
1994	#num#	k4
v	v	k7c6
Dominikánské	dominikánský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
ještě	ještě	k6eAd1
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
událost	událost	k1gFnSc1
držena	držen	k2eAgFnSc1d1
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pár	pár	k4xCyI
musel	muset	k5eAaImAgMnS
čelit	čelit	k5eAaImF
mediálním	mediální	k2eAgFnPc3d1
spekulacím	spekulace	k1gFnPc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
oddal	oddat	k5eAaPmAgMnS
pouze	pouze	k6eAd1
za	za	k7c7
účelem	účel	k1gInSc7
napravení	napravení	k1gNnSc2
Jacksonovy	Jacksonův	k2eAgFnSc2d1
reputace	reputace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
poškozena	poškodit	k5eAaPmNgFnS
proběhlým	proběhlý	k2eAgNnSc7d1
soudním	soudní	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Soudy	soud	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
se	se	k3xPyFc4
odloučili	odloučit	k5eAaPmAgMnP
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1995	#num#	k4
a	a	k8xC
rozvedli	rozvést	k5eAaPmAgMnP
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
zůstali	zůstat	k5eAaPmAgMnP
přátelé	přítel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Podruhé	podruhé	k6eAd1
se	se	k3xPyFc4
Jackson	Jackson	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
roku	rok	k1gInSc2
1996	#num#	k4
(	(	kIx(
<g/>
bez	bez	k7c2
příprav	příprava	k1gFnPc2
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
hotelovém	hotelový	k2eAgInSc6d1
pokoji	pokoj	k1gInSc6
<g/>
)	)	kIx)
se	s	k7c7
zdravotní	zdravotní	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
Deborah	Deboraha	k1gFnPc2
Jeanne	Jeann	k1gMnSc5
Rowe	Rowus	k1gMnSc5
(	(	kIx(
<g/>
známou	známý	k2eAgFnSc7d1
jako	jako	k8xC,k8xS
Debbie	Debbie	k1gFnSc1
Rowe	Row	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
mu	on	k3xPp3gMnSc3
porodila	porodit	k5eAaPmAgFnS
2	#num#	k4
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
Prince	princ	k1gMnSc2
Michaela	Michael	k1gMnSc2
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Paris	Paris	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
umělcově	umělcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
tisku	tisk	k1gInSc6
objevily	objevit	k5eAaPmAgFnP
zprávy	zpráva	k1gFnPc1
<g/>
,	,	kIx,
podle	podle	k7c2
nichž	jenž	k3xRgNnPc2
Rowe	Rowe	k1gNnPc2
přiznala	přiznat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Jackson	Jackson	k1gMnSc1
není	být	k5eNaImIp3nS
jejich	jejich	k3xOp3gMnSc7
biologickým	biologický	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
–	–	k?
tato	tento	k3xDgFnSc1
zpráva	zpráva	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
dementována	dementován	k2eAgFnSc1d1
(	(	kIx(
<g/>
rozhovor	rozhovor	k1gInSc1
byl	být	k5eAaImAgInS
falešný	falešný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
žili	žít	k5eAaImAgMnP
odděleně	odděleně	k6eAd1
a	a	k8xC
rozvedli	rozvést	k5eAaPmAgMnP
se	se	k3xPyFc4
po	po	k7c6
necelých	celý	k2eNgNnPc6d1
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Rowe	Rowe	k1gFnSc1
přenechala	přenechat	k5eAaPmAgFnS
Jacksonovi	Jackson	k1gMnSc3
plné	plný	k2eAgNnSc1d1
právo	právo	k1gNnSc1
na	na	k7c4
opatrovnictví	opatrovnictví	k1gNnSc4
obou	dva	k4xCgFnPc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Totožnost	totožnost	k1gFnSc1
náhradní	náhradní	k2eAgFnSc2d1
matky	matka	k1gFnSc2
nejmladšího	mladý	k2eAgNnSc2d3
dítěte	dítě	k1gNnSc2
–	–	k?
Prince	princ	k1gMnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
II	II	kA
<g/>
,	,	kIx,
zvaného	zvaný	k2eAgNnSc2d1
„	„	k?
<g/>
Blanket	blanket	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
)	)	kIx)
–	–	k?
je	být	k5eAaImIp3nS
neznámá	známý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
bulvární	bulvární	k2eAgNnPc1d1
média	médium	k1gNnPc1
dodnes	dodnes	k6eAd1
spekulují	spekulovat	k5eAaImIp3nP
o	o	k7c6
věrohodnosti	věrohodnost	k1gFnSc6
Jacksonova	Jacksonův	k2eAgNnSc2d1
otcovství	otcovství	k1gNnSc2
kvůli	kvůli	k7c3
odstínu	odstín	k1gInSc3
pleti	pleť	k1gFnSc2
potomků	potomek	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
světlejší	světlý	k2eAgFnSc1d2
než	než	k8xS
Jacksonova	Jacksonův	k2eAgFnSc1d1
původní	původní	k2eAgFnSc1d1
barva	barva	k1gFnSc1
kůže	kůže	k1gFnSc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
závěr	závěr	k1gInSc4
sekce	sekce	k1gFnSc1
Barva	barva	k1gFnSc1
kůže	kůže	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Prince	princ	k1gMnSc2
Jackson	Jackson	k1gInSc1
údajně	údajně	k6eAd1
vykazuje	vykazovat	k5eAaImIp3nS
známky	známka	k1gFnSc2
choroby	choroba	k1gFnSc2
vitiliga	vitiliga	k1gFnSc1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
Jacksonova	Jacksonův	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
La	la	k1gNnSc2
Toya	Toy	k1gInSc2
<g/>
,	,	kIx,
vyvracejíc	vyvracet	k5eAaImSgNnS
tak	tak	k6eAd1
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
pochyby	pochyba	k1gFnSc2
o	o	k7c6
Jacksonově	Jacksonův	k2eAgNnSc6d1
otcovství	otcovství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Jackson	Jackson	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
na	na	k7c4
selhání	selhání	k1gNnSc4
srdce	srdce	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Centrála	centrála	k1gFnSc1
rychlé	rychlý	k2eAgFnSc2d1
zdravotní	zdravotní	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
911	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
ze	z	k7c2
zpěvákova	zpěvákův	k2eAgNnSc2d1
pronajatého	pronajatý	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
v	v	k7c6
100	#num#	k4
North	North	k1gInSc1
Carolwood	Carolwood	k1gInSc4
Drive	drive	k1gInSc4
v	v	k7c4
Holmby	Holmba	k1gFnPc4
Hills	Hillsa	k1gFnPc2
telefonát	telefonát	k1gInSc1
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
12.21	12.21	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volající	volající	k2eAgFnSc7d1
žádal	žádat	k5eAaImAgMnS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
pro	pro	k7c4
člověka	člověk	k1gMnSc4
v	v	k7c6
bezvědomí	bezvědomí	k1gNnSc6
<g/>
,	,	kIx,
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
se	se	k3xPyFc4
přítomný	přítomný	k2eAgMnSc1d1
osobní	osobní	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
pokoušel	pokoušet	k5eAaImAgMnS
neúspěšně	úspěšně	k6eNd1
resuscitovat	resuscitovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
do	do	k7c2
domu	dům	k1gInSc2
o	o	k7c4
tři	tři	k4xCgFnPc4
minuty	minuta	k1gFnPc4
později	pozdě	k6eAd2
dorazili	dorazit	k5eAaPmAgMnP
zdravotníci	zdravotník	k1gMnPc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
nedýchal	dýchat	k5eNaImAgMnS
a	a	k8xC
byla	být	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
zjištěna	zjistit	k5eAaPmNgFnS
srdeční	srdeční	k2eAgFnSc1d1
zástava	zástava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Resuscitace	resuscitace	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
během	během	k7c2
převozu	převoz	k1gInSc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
hodinu	hodina	k1gFnSc4
po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Ronald	Ronalda	k1gFnPc2
Reagan	Reagan	k1gMnSc1
UCLA	UCLA	kA
Medical	Medical	k1gFnSc1
Center	centrum	k1gNnPc2
v	v	k7c6
13.14	13.14	k4
(	(	kIx(
<g/>
20.14	20.14	k4
UTC	UTC	kA
<g/>
,	,	kIx,
tj.	tj.	kA
22.14	22.14	k4
SELČ	SELČ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neúspěšných	úspěšný	k2eNgInPc6d1
pokusech	pokus	k1gInPc6
o	o	k7c6
oživení	oživení	k1gNnSc6
byl	být	k5eAaImAgMnS
Jackson	Jackson	k1gMnSc1
prohlášen	prohlásit	k5eAaPmNgMnS
ve	v	k7c4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
(	(	kIx(
<g/>
21.26	21.26	k4
UTC	UTC	kA
<g/>
,	,	kIx,
tj.	tj.	kA
23.26	23.26	k4
SELČ	SELČ	kA
<g/>
)	)	kIx)
za	za	k7c4
mrtvého	mrtvý	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
příčinu	příčina	k1gFnSc4
náhlého	náhlý	k2eAgNnSc2d1
úmrtí	úmrtí	k1gNnSc2
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
akutní	akutní	k2eAgFnSc1d1
otrava	otrava	k1gFnSc1
silným	silný	k2eAgNnSc7d1
sedativem	sedativum	k1gNnSc7
propofolem	propofol	k1gMnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
Jacksonovi	Jacksonův	k2eAgMnPc1d1
od	od	k7c2
května	květen	k1gInSc2
roku	rok	k1gInSc2
2009	#num#	k4
podával	podávat	k5eAaImAgMnS
proti	proti	k7c3
chronické	chronický	k2eAgFnSc3d1
nespavosti	nespavost	k1gFnSc3
osobní	osobní	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
Jacksonovu	Jacksonův	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
době	doba	k1gFnSc6
prohlášena	prohlásit	k5eAaPmNgFnS
koronerem	koroner	k1gMnSc7
za	za	k7c4
vraždu	vražda	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
zodpovědný	zodpovědný	k2eAgMnSc1d1
Jacksonův	Jacksonův	k2eAgMnSc1d1
osobní	osobní	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
–	–	k?
kardiolog	kardiolog	k1gMnSc1
Conrad	Conrad	k1gInSc4
Murray	Murraa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angeles	k1gInSc1
obviněn	obviněn	k2eAgInSc1d1
z	z	k7c2
neúmyslného	úmyslný	k2eNgNnSc2d1
zabití	zabití	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
propuštěn	propuštěn	k2eAgInSc1d1
na	na	k7c4
kauci	kauce	k1gFnSc4
(	(	kIx(
<g/>
75.000	75.000	k4
dolarů	dolar	k1gInPc2
=	=	kIx~
1,85	1,85	k4
milionu	milion	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
s	s	k7c7
omezením	omezení	k1gNnSc7
lékařské	lékařský	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
a	a	k8xC
zabavením	zabavení	k1gNnSc7
pasu	pas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
Conrad	Conrad	k1gInSc1
Murray	Murraa	k1gFnSc2
stanul	stanout	k5eAaPmAgInS
před	před	k7c7
soudem	soud	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
mu	on	k3xPp3gMnSc3
hrozily	hrozit	k5eAaImAgFnP
až	až	k9
čtyři	čtyři	k4xCgInPc1
roky	rok	k1gInPc1
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
procesu	proces	k1gInSc6
Conrad	Conrada	k1gFnPc2
Robert	Robert	k1gMnSc1
Murray	Murraa	k1gFnSc2
vs	vs	k?
<g/>
.	.	kIx.
lid	lid	k1gInSc1
státu	stát	k1gInSc2
Kalifornie	Kalifornie	k1gFnSc2
jednohlasně	jednohlasně	k6eAd1
všemi	všecek	k3xTgMnPc7
členy	člen	k1gMnPc7
poroty	porota	k1gFnSc2
uznán	uznán	k2eAgInSc4d1
vinným	vinný	k1gMnSc7
z	z	k7c2
neúmyslného	úmyslný	k2eNgNnSc2d1
zabití	zabití	k1gNnSc2
svého	svůj	k3xOyFgMnSc2
pacienta	pacient	k1gMnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
a	a	k8xC
následně	následně	k6eAd1
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
ke	k	k7c3
čtyřem	čtyři	k4xCgInPc3
letům	let	k1gInPc3
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vězení	vězení	k1gNnSc2
byl	být	k5eAaImAgMnS
propuštěn	propustit	k5eAaPmNgMnS
po	po	k7c6
necelých	celý	k2eNgNnPc6d1
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pitva	pitva	k1gFnSc1
</s>
<s>
Po	po	k7c6
Jacksonově	Jacksonův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
v	v	k7c6
bulvárních	bulvární	k2eAgNnPc6d1
médiích	médium	k1gNnPc6
zprávy	zpráva	k1gFnSc2
o	o	k7c6
údajných	údajný	k2eAgFnPc6d1
„	„	k?
<g/>
šokujících	šokující	k2eAgInPc6d1
výsledcích	výsledek	k1gInPc6
pitvy	pitva	k1gFnSc2
<g/>
“	“	k?
umělce	umělec	k1gMnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
vzápětí	vzápětí	k6eAd1
popřeny	popřen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
října	říjen	k1gInSc2
podalo	podat	k5eAaPmAgNnS
BBC	BBC	kA
informace	informace	k1gFnSc2
o	o	k7c6
závěru	závěr	k1gInSc6
pitvy	pitva	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
měl	mít	k5eAaImAgMnS
Jackson	Jackson	k1gMnSc1
silné	silný	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
tělesná	tělesný	k2eAgFnSc1d1
váha	váha	k1gFnSc1
odpovídala	odpovídat	k5eAaImAgFnS
výšce	výška	k1gFnSc6
postavy	postava	k1gFnPc4
a	a	k8xC
ačkoli	ačkoli	k8xS
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
zjištěna	zjištěn	k2eAgFnSc1d1
artritida	artritida	k1gFnSc1
a	a	k8xC
chronický	chronický	k2eAgInSc1d1
zánět	zánět	k1gInSc1
plic	plíce	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
celkem	celkem	k6eAd1
zdravý	zdravý	k2eAgMnSc1d1
padesátiletý	padesátiletý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Diskografie	diskografie	k1gFnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
Got	Got	k1gFnSc4
to	ten	k3xDgNnSc1
Be	Be	k1gMnSc2
There	Ther	k1gMnSc5
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
Ben	Ben	k1gInSc1
</s>
<s>
1973	#num#	k4
<g/>
:	:	kIx,
Music	Music	k1gMnSc1
&	&	k?
Me	Me	k1gMnSc1
</s>
<s>
1975	#num#	k4
<g/>
:	:	kIx,
Forever	Forever	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
</s>
<s>
1979	#num#	k4
<g/>
:	:	kIx,
Off	Off	k1gMnSc1
the	the	k?
Wall	Wall	k1gMnSc1
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Thriller	thriller	k1gInSc1
</s>
<s>
1987	#num#	k4
<g/>
:	:	kIx,
Bad	Bad	k1gFnSc1
</s>
<s>
1991	#num#	k4
<g/>
:	:	kIx,
Dangerous	Dangerous	k1gInSc1
</s>
<s>
1995	#num#	k4
<g/>
:	:	kIx,
HIStory	HIStor	k1gInPc1
<g/>
:	:	kIx,
Past	past	k1gFnSc1
<g/>
,	,	kIx,
Present	Present	k1gInSc1
and	and	k?
Future	Futur	k1gMnSc5
<g/>
,	,	kIx,
Book	Booko	k1gNnPc2
I	i	k8xC
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Invincible	Invincible	k1gFnSc1
</s>
<s>
Turné	turné	k1gNnSc1
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
:	:	kIx,
Bad	Bad	k1gMnSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
:	:	kIx,
Dangerous	Dangerous	k1gMnSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
:	:	kIx,
HIStory	HIStor	k1gInPc1
World	Worlda	k1gFnPc2
Tour	Toura	k1gFnPc2
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
This	This	k1gInSc1
Is	Is	k1gMnSc2
It	It	k1gMnSc2
(	(	kIx(
<g/>
neuskutečněno	uskutečnit	k5eNaPmNgNnS
kvůli	kvůli	k7c3
úmrtí	úmrtí	k1gNnSc3
zpěváka	zpěvák	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
1974	#num#	k4
<g/>
:	:	kIx,
Free	Free	k1gFnSc4
to	ten	k3xDgNnSc1
Be	Be	k1gFnPc1
<g/>
…	…	k?
You	You	k1gMnSc1
&	&	k?
Me	Me	k1gMnSc1
</s>
<s>
1976	#num#	k4
<g/>
:	:	kIx,
Bugsy	Bugsa	k1gFnSc2
Malone	Malon	k1gInSc5
</s>
<s>
1978	#num#	k4
<g/>
:	:	kIx,
Čaroděj	čaroděj	k1gMnSc1
(	(	kIx(
<g/>
na	na	k7c4
motivy	motiv	k1gInPc4
knihy	kniha	k1gFnSc2
Čaroděj	čaroděj	k1gMnSc1
ze	z	k7c2
země	zem	k1gFnSc2
Oz	Oz	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1981	#num#	k4
<g/>
:	:	kIx,
Diana	Diana	k1gFnSc1
</s>
<s>
1985	#num#	k4
<g/>
:	:	kIx,
Other	Other	k1gMnSc1
Lover	Lover	k1gMnSc1
</s>
<s>
1986	#num#	k4
<g/>
:	:	kIx,
Kapitán	kapitán	k1gMnSc1
EO	EO	kA
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
Moonwalker	Moonwalker	k1gInSc1
</s>
<s>
2002	#num#	k4
<g/>
:	:	kIx,
Muži	muž	k1gMnPc1
v	v	k7c6
černém	černé	k1gNnSc6
2	#num#	k4
(	(	kIx(
<g/>
cameo	cameo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
Miss	miss	k1gFnSc1
trosečnice	trosečnice	k1gFnSc1
(	(	kIx(
<g/>
cameo	cameo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Krátké	Krátké	k2eAgInPc1d1
hudební	hudební	k2eAgInPc1d1
filmy	film	k1gInPc1
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
Smooth	Smooth	k1gInSc1
Criminal	Criminal	k1gFnSc2
–	–	k?
součást	součást	k1gFnSc1
snímku	snímek	k1gInSc2
Moonwalker	Moonwalkra	k1gFnPc2
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ghosts	Ghosts	k1gInSc1
</s>
<s>
1987	#num#	k4
<g/>
:	:	kIx,
Bad	Bad	k1gFnSc1
</s>
<s>
Dokumenty	dokument	k1gInPc1
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Making	Making	k1gInSc1
of	of	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Thriller	thriller	k1gInSc1
–	–	k?
revoluční	revoluční	k2eAgInSc1d1
VHS	VHS	kA
projekt	projekt	k1gInSc1
s	s	k7c7
krátkým	krátký	k2eAgInSc7d1
hudebním	hudební	k2eAgInSc7d1
filmem	film	k1gInSc7
Thriller	thriller	k1gInSc1
a	a	k8xC
dokument	dokument	k1gInSc4
přibližující	přibližující	k2eAgNnSc4d1
pozadí	pozadí	k1gNnSc4
natáčení	natáčení	k1gNnSc2
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
The	The	k1gMnSc1
Legend	legenda	k1gFnPc2
Continues	Continues	k1gMnSc1
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
American	American	k1gMnSc1
Dream	Dream	k1gInSc1
–	–	k?
dokument	dokument	k1gInSc1
o	o	k7c6
rodině	rodina	k1gFnSc6
Jacksonů	Jackson	k1gInPc2
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
The	The	k1gMnSc1
One	One	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
:	:	kIx,
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
dokument	dokument	k1gInSc1
britského	britský	k2eAgMnSc2d1
novináře	novinář	k1gMnSc2
Martina	Martin	k1gMnSc2
Bashira	Bashir	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
<g/>
:	:	kIx,
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Take	Take	k1gFnSc1
Two	Two	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Footage	Footag	k1gMnSc2
You	You	k1gMnSc2
Were	Wer	k1gMnSc2
Never	Never	k1gMnSc1
Meant	Meant	k1gMnSc1
to	ten	k3xDgNnSc4
See	See	k1gFnSc1
(	(	kIx(
<g/>
reakce	reakce	k1gFnSc1
na	na	k7c4
dokument	dokument	k1gInSc4
Martina	Martin	k1gMnSc2
Bashira	Bashir	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
Jackson	Jackson	k1gMnSc1
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
manipulativně	manipulativně	k6eAd1
zpracovaný	zpracovaný	k2eAgMnSc1d1
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
zahrnut	zahrnut	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Bashir	Bashir	k1gInSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
dokumentu	dokument	k1gInSc6
o	o	k7c6
Jacksonovi	Jackson	k1gMnSc6
vypustil	vypustit	k5eAaPmAgInS
<g/>
;	;	kIx,
uvádí	uvádět	k5eAaImIp3nS
Maury	Maur	k1gMnPc4
Povich	Povicha	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
This	This	k1gInSc1
Is	Is	k1gMnSc3
It	It	k1gMnSc3
(	(	kIx(
<g/>
režie	režie	k1gFnSc2
<g/>
:	:	kIx,
Kenny	Kenna	k1gMnSc2
Ortega	Orteg	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
Life	Life	k1gFnSc1
of	of	k?
an	an	k?
Icon	Icon	k1gInSc1
</s>
<s>
2012	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
BAD	BAD	kA
25	#num#	k4
(	(	kIx(
<g/>
dokument	dokument	k1gInSc4
mapující	mapující	k2eAgFnSc4d1
období	období	k1gNnSc4
vydání	vydání	k1gNnSc2
alba	album	k1gNnSc2
BAD	BAD	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Spike	Spike	k1gInSc1
Lee	Lea	k1gFnSc3
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
<g/>
:	:	kIx,
Lies	Lies	k1gInSc1
of	of	k?
Leaving	Leaving	k1gInSc1
Neverland	Neverland	k1gInSc1
(	(	kIx(
<g/>
režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Mark	Mark	k1gMnSc1
Hostetler	Hostetler	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
Chase	chasa	k1gFnSc3
the	the	k?
Truth	Truth	k1gInSc1
(	(	kIx(
<g/>
režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Jordan	Jordan	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
<g/>
:	:	kIx,
Square	square	k1gInSc1
One	One	k1gFnSc2
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Danny	Danen	k2eAgFnPc1d1
Wu	Wu	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
Moonwalk	Moonwalk	k1gInSc1
(	(	kIx(
<g/>
autobiografie	autobiografie	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
vlastní	vlastní	k2eAgInSc4d1
životopis	životopis	k1gInSc4
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
Tanec	tanec	k1gInSc1
jako	jako	k8xC,k8xS
sen	sen	k1gInSc1
–	–	k?
Dancing	dancing	k1gInSc1
the	the	k?
Dream	Dream	k1gInSc1
<g/>
,	,	kIx,
soubor	soubor	k1gInSc1
vlastní	vlastní	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
<g/>
,	,	kIx,
esejí	esej	k1gFnPc2
a	a	k8xC
fotografií	fotografia	k1gFnPc2
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
Zpátky	zpátky	k6eAd1
v	v	k7c6
čase	čas	k1gInSc6
od	od	k7c2
autora	autor	k1gMnSc2
Daryla	Daryla	k1gMnSc2
Easlea	Easleus	k1gMnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Úmrtí	úmrtí	k1gNnPc1
list	list	k1gInSc1
vystavený	vystavený	k2eAgInSc1d1
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angeles	k1gMnSc1
County	Counta	k1gFnSc2
<g/>
↑	↑	k?
Král	Král	k1gMnSc1
popu	pop	k1gInSc2
je	být	k5eAaImIp3nS
mrtev	mrtev	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemá	mít	k5eNaImIp3nS
následníka	následník	k1gMnSc2
<g/>
,	,	kIx,
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Kariéra	kariéra	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Radiobonton	Radiobonton	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Video	video	k1gNnSc1
of	of	k?
the	the	k?
1984	#num#	k4
Pepsi	Pepsi	k1gFnSc6
commercial	commerciat	k5eAaImAgMnS,k5eAaBmAgMnS,k5eAaPmAgMnS
when	when	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
hair	hair	k1gMnSc1
caught	caught	k1gMnSc1
on	on	k3xPp3gMnSc1
fire	firat	k5eAaPmIp3nS
<g/>
,	,	kIx,
Usmagazine	Usmagazin	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
EARLY	earl	k1gMnPc7
<g/>
,	,	kIx,
Chas	chasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
January	Januara	k1gFnSc2
27	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
hair	hair	k1gMnSc1
catches	catches	k1gMnSc1
fire	firat	k5eAaPmIp3nS
during	during	k1gInSc4
Pepsi	Pepsi	k1gFnSc1
commercial	commercial	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bt	Bt	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2019-01-22	2019-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Congressional	Congressional	k1gMnSc1
Resolution	Resolution	k1gInSc1
<g/>
,	,	kIx,
Opencongress	Opencongress	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Michael	Michaela	k1gFnPc2
Jacksons	Jacksons	k1gInSc4
Greatest	Greatest	k1gFnSc4
Non	Non	k1gMnPc2
Musical	musical	k1gInSc4
Achievements	Achievementsa	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Brainz	Brainz	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
org	org	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Charity	charita	k1gFnSc2
Auction	Auction	k1gInSc1
–	–	k?
Great	Great	k2eAgInSc1d1
Heart	Heart	k1gInSc1
of	of	k?
MICHAEL	Michaela	k1gFnPc2
JACKSON	JACKSON	kA
and	and	k?
JEFF	JEFF	kA
KOONS	KOONS	kA
from	from	k6eAd1
The	The	k1gMnPc1
Krasnals	Krasnalsa	k1gFnPc2
for	forum	k1gNnPc2
children	childrna	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Michaeljackson	Michaeljackson	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Gave	Gave	k1gFnSc1
$	$	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
300	#num#	k4
Million	Million	k1gInSc4
To	to	k9
Charities	Charities	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Blackstarnews	Blackstarnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Seznam	seznam	k1gInSc1
podporovaných	podporovaný	k2eAgFnPc2d1
charitativních	charitativní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
Allmichaeljackson	Allmichaeljacksona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Congressional	Congressional	k1gFnSc1
Resolution	Resolution	k1gInSc1
Would	Would	k1gMnSc1
Honor	Honor	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
As	as	k9
World	World	k1gMnSc1
Humanitarian	Humanitarian	k1gMnSc1
Archivováno	archivován	k2eAgNnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Cnsnews	Cnsnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Jackson	Jackson	k1gInSc1
byl	být	k5eAaImAgInS
posmrtně	posmrtně	k6eAd1
vyznamenán	vyznamenat	k5eAaPmNgInS
za	za	k7c4
humanitární	humanitární	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
<g/>
,	,	kIx,
Lidovky	Lidovky	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
20091	#num#	k4
2	#num#	k4
3	#num#	k4
Jacksonův	Jacksonův	k2eAgInSc4d1
lékař	lékař	k1gMnSc1
propuštěn	propustit	k5eAaPmNgMnS
na	na	k7c4
kauci	kauce	k1gFnSc4
<g/>
,	,	kIx,
další	další	k2eAgNnSc4d1
slyšení	slyšení	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
soudu	soud	k1gInSc2
v	v	k7c6
dubnu	duben	k1gInSc6
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
↑	↑	k?
Interview	interview	k1gNnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
–	–	k?
Oprah	Oprah	k1gMnSc1
Winfrey	Winfrea	k1gMnSc2
:	:	kIx,
Vitiligo	Vitiligo	k6eAd1
<g/>
1	#num#	k4
2	#num#	k4
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Vitiligoguide	Vitiligoguid	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
20091	#num#	k4
2	#num#	k4
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
son	son	k1gInSc1
Prince	princ	k1gMnSc2
has	hasit	k5eAaImRp2nS
inherited	inherited	k1gMnSc1
vitiligo	vitiligo	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
skin	skin	k1gMnSc1
disease	disease	k6eAd1
<g/>
,	,	kIx,
Mirror	Mirror	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
Son	son	k1gInSc1
Prince	princ	k1gMnSc2
has	hasit	k5eAaImRp2nS
inherited	inherited	k1gMnSc1
vitiligo	vitiligo	k1gMnSc1
skin	skin	k1gMnSc1
condition	condition	k1gInSc4
<g/>
,	,	kIx,
says	says	k6eAd1
La	la	k1gNnPc1
Toya	Toyum	k1gNnPc1
<g/>
,	,	kIx,
Telegraph	Telegraph	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Son	son	k1gInSc1
‘	‘	k?
<g/>
Has	hasit	k5eAaImRp2nS
Vitiligo	Vitiligo	k1gMnSc1
<g/>
’	’	k?
Archivováno	archivován	k2eAgNnSc1d1
24	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Showbizspy	Showbizsp	k1gInPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
La	la	k1gNnSc1
Toya	Toya	k1gFnSc1
<g/>
:	:	kIx,
Son	son	k1gInSc1
has	hasit	k5eAaImRp2nS
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
skin	skin	k1gMnSc1
disease	disease	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Bild	Bild	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
2009	#num#	k4
<g/>
↑	↑	k?
Detail	detail	k1gInSc1
ruky	ruka	k1gFnSc2
<g/>
1	#num#	k4
Detail	detail	k1gInSc1
ruky	ruka	k1gFnSc2
<g/>
2	#num#	k4
Archivováno	archivován	k2eAgNnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Detail	detail	k1gInSc1
ruky	ruka	k1gFnSc2
<g/>
3	#num#	k4
Archivováno	archivován	k2eAgNnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Detail	detail	k1gInSc1
krku	krk	k1gInSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Detail	detail	k1gInSc1
paže	paže	k1gFnSc1
<g/>
,	,	kIx,
20101	#num#	k4
2	#num#	k4
Dokument	dokument	k1gInSc1
„	„	k?
<g/>
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
“	“	k?
s	s	k7c7
českými	český	k2eAgInPc7d1
titulky	titulek	k1gInPc7
Archivováno	archivován	k2eAgNnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
sekce	sekce	k1gFnSc1
Dokumenty	dokument	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stream	Stream	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Taraborrelli	Taraborrelle	k1gFnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
434	#num#	k4
<g/>
–	–	k?
<g/>
436	#num#	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
prý	prý	k9
trpí	trpět	k5eAaImIp3nS
řadou	řada	k1gFnSc7
zdravotních	zdravotní	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Father	Fathra	k1gFnPc2
Of	Of	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Accuser	Accuser	k1gMnSc1
Found	Found	k1gMnSc1
Dead	Dead	k1gMnSc1
Of	Of	k1gMnSc1
Apparent	Apparent	k1gMnSc1
Suicide	Suicid	k1gInSc5
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Wpix	Wpix	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
father	fathra	k1gFnPc2
of	of	k?
molestation	molestation	k1gInSc1
accuser	accuser	k1gMnSc1
Jordan	Jordan	k1gMnSc1
Chandler	Chandler	k1gMnSc1
shoots	shootsa	k1gFnPc2
himself	himself	k1gMnSc1
in	in	k?
the	the	k?
head	head	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Musictoob	Musictooba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Dokument	dokument	k1gInSc1
„	„	k?
<g/>
Living	Living	k1gInSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Jackson	Jackson	k1gMnSc1
<g/>
“	“	k?
s	s	k7c7
českými	český	k2eAgInPc7d1
titulky	titulek	k1gInPc7
Archivováno	archivován	k2eAgNnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
Dokumenty	dokument	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stream	Stream	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
The	The	k1gMnSc1
case	cas	k1gFnSc2
against	against	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
23	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Thesmokinggun	Thesmokinggun	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
král	král	k1gMnSc1
popu	pop	k1gInSc2
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
911	#num#	k4
phone	phonout	k5eAaImIp3nS,k5eAaPmIp3nS
call	call	k1gInSc1
–	–	k?
audio	audio	k2eAgInSc1d1
<g/>
,	,	kIx,
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Přepis	přepis	k1gInSc1
telefonátu	telefonát	k1gInSc2
na	na	k7c4
tísňovou	tísňový	k2eAgFnSc4d1
linku	linka	k1gFnSc4
ze	z	k7c2
sídla	sídlo	k1gNnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
,	,	kIx,
Ceskenoviny	Ceskenovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Fans	Fans	k1gInSc1
mourn	mourn	k1gMnSc1
artist	artist	k1gMnSc1
for	forum	k1gNnPc2
whom	whom	k6eAd1
it	it	k?
didn	didn	k1gInSc1
<g/>
’	’	k?
<g/>
t	t	k?
matter	matter	k1gInSc1
if	if	k?
you	you	k?
were	were	k1gNnPc2
black	blacko	k1gNnPc2
or	or	k?
white	white	k5eAaPmIp2nP
<g/>
,	,	kIx,
Timesonline	Timesonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
co	co	k9
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Jacksonovu	Jacksonův	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
způsobila	způsobit	k5eAaPmAgFnS
smrtelná	smrtelný	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
léku	lék	k1gInSc2
propofol	propofola	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
České	český	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
↑	↑	k?
Jackson	Jackson	k1gMnSc1
autopsy	autopsa	k1gFnSc2
details	detailsa	k1gFnPc2
revealed	revealed	k1gInSc1
<g/>
,	,	kIx,
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Král	Král	k1gMnSc1
popu	pop	k1gInSc2
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Got	Got	k?
to	ten	k3xDgNnSc1
Be	Be	k1gMnSc1
There	Ther	k1gInSc5
•	•	k?
Ben	Ben	k1gInSc1
•	•	k?
Music	Music	k1gMnSc1
&	&	k?
Me	Me	k1gMnSc1
•	•	k?
Forever	Forever	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
•	•	k?
Off	Off	k1gMnSc1
the	the	k?
Wall	Wall	k1gMnSc1
•	•	k?
Thriller	thriller	k1gInSc1
•	•	k?
Bad	Bad	k1gFnSc1
•	•	k?
Dangerous	Dangerous	k1gInSc4
•	•	k?
HIStory	HIStor	k1gInPc4
<g/>
:	:	kIx,
Past	past	k1gFnSc1
<g/>
,	,	kIx,
Present	Present	k1gInSc1
and	and	k?
Future	Futur	k1gMnSc5
<g/>
,	,	kIx,
Book	Book	k1gMnSc1
I	i	k8xC
•	•	k?
Invincible	Invincible	k1gFnSc1
Posmrtná	posmrtný	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
•	•	k?
Xscape	Xscap	k1gMnSc5
Soundtracky	soundtrack	k1gInPc1
</s>
<s>
The	The	k?
Wiz	Wiz	k1gMnSc1
•	•	k?
E.T.	E.T.	k1gMnSc1
the	the	k?
Extra-Terrestrial	Extra-Terrestrial	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
This	This	k1gInSc1
Is	Is	k1gMnSc2
It	It	k1gMnSc2
•	•	k?
Immortal	Immortal	k1gFnSc2
Remixová	Remixový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
The	The	k?
Original	Original	k1gFnSc1
Soul	Soul	k1gInSc1
of	of	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
Blood	Blood	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Dance	Danka	k1gFnSc3
Floor	Floor	k1gMnSc1
<g/>
:	:	kIx,
HIStory	HIStor	k1gInPc1
in	in	k?
the	the	k?
Mix	mix	k1gInSc1
•	•	k?
Stripped	Stripped	k1gInSc1
Mixes	Mixes	k1gMnSc1
•	•	k?
The	The	k1gMnSc4
Remix	Remix	k1gInSc1
Suite	Suit	k1gMnSc5
Kompilace	kompilace	k1gFnPc4
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
One	One	k1gMnSc1
Day	Day	k1gMnSc1
in	in	k?
Your	Your	k1gMnSc1
Life	Lif	k1gFnSc2
•	•	k?
The	The	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
Mix	mix	k1gInSc1
•	•	k?
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
Masters	Mastersa	k1gFnPc2
–	–	k?
The	The	k1gFnSc2
Millennium	millennium	k1gNnSc1
Collection	Collection	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
Greatest	Greatest	k1gInSc1
Hits	Hits	k1gInSc1
<g/>
:	:	kIx,
HIStory	HIStor	k1gInPc1
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
I	i	k9
•	•	k?
Number	Number	k1gInSc1
Ones	Ones	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Essential	Essential	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
King	King	k1gMnSc1
of	of	k?
Pop	pop	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Definitive	Definitiv	k1gInSc5
Collection	Collection	k1gInSc4
•	•	k?
Icon	Icon	k1gInSc1
Boxsety	Boxseta	k1gFnSc2
</s>
<s>
Anthology	Antholog	k1gMnPc4
•	•	k?
The	The	k1gFnSc1
Ultimate	Ultimat	k1gInSc5
Collection	Collection	k1gInSc4
•	•	k?
Visionary	Visionara	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Video	video	k1gNnSc1
Singles	Singles	k1gMnSc1
•	•	k?
50	#num#	k4
Best	Best	k2eAgInSc4d1
Songs	Songs	k1gInSc4
–	–	k?
The	The	k1gFnSc2
Motown	Motown	k1gMnSc1
Years	Years	k1gInSc1
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
&	&	k?
The	The	k1gMnSc1
Jackson	Jackson	k1gMnSc1
5	#num#	k4
•	•	k?
The	The	k1gFnSc1
Collection	Collection	k1gInSc1
•	•	k?
Hello	Hello	k1gNnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Motown	Motown	k1gMnSc1
Solo	Solo	k1gMnSc1
Collection	Collection	k1gInSc4
Ostatní	ostatní	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Farewell	Farewell	k1gMnSc1
My	my	k3xPp1nPc1
Summer	Summer	k1gInSc1
Love	lov	k1gInSc5
•	•	k?
Looking	Looking	k1gInSc1
Back	Back	k1gInSc1
to	ten	k3xDgNnSc1
Yesterday	Yesterdaa	k1gFnPc1
•	•	k?
Thriller	thriller	k1gInSc1
25	#num#	k4
•	•	k?
Bad	Bad	k1gFnSc2
25	#num#	k4
Turné	turné	k1gNnSc2
</s>
<s>
Bad	Bad	k?
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Dangerous	Dangerous	k1gMnSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
HIStory	HIStor	k1gInPc1
World	Worldo	k1gNnPc2
Tour	Tour	k1gInSc1
•	•	k?
This	This	k1gInSc1
Is	Is	k1gMnSc2
It	It	k1gMnSc2
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
•	•	k?
Jacksons	Jacksons	k1gInSc1
•	•	k?
Moonwalk	Moonwalk	k1gInSc1
•	•	k?
Tanec	tanec	k1gInSc1
jako	jako	k8xC,k8xS
sen	sen	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000603166	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118711083	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1023	#num#	k4
081X	081X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83133203	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500260234	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27092134	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83133203	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
