<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Chile	Chile	k1gNnSc2	Chile
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
u	u	k7c2	u
žerdi	žerď	k1gFnSc2	žerď
modré	modrý	k2eAgNnSc1d1	modré
karé	karé	k1gNnSc1	karé
(	(	kIx(	(
<g/>
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
<g/>
)	)	kIx)	)
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
pěticípou	pěticípý	k2eAgFnSc7d1	pěticípá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc4	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
sněhem	sníh	k1gInSc7	sníh
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
Andy	Anda	k1gFnPc4	Anda
<g/>
,	,	kIx,	,
modrá	modrat	k5eAaImIp3nS	modrat
oblohu	obloha	k1gFnSc4	obloha
klenoucí	klenoucí	k2eAgFnSc2d1	klenoucí
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
státem	stát	k1gInSc7	stát
a	a	k8xC	a
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
už	už	k6eAd1	už
dávnými	dávný	k2eAgMnPc7d1	dávný
chilskými	chilský	k2eAgMnPc7d1	chilský
Indiány	Indián	k1gMnPc7	Indián
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc2	jejich
pět	pět	k4xCc4	pět
cípů	cíp	k1gInPc2	cíp
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
pěti	pět	k4xCc7	pět
původními	původní	k2eAgFnPc7d1	původní
provinciemi	provincie	k1gFnPc7	provincie
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
Atacama	Atacama	k1gFnSc1	Atacama
<g/>
,	,	kIx,	,
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
<g/>
,	,	kIx,	,
Bío-Bío	Bío-Bía	k1gFnSc5	Bío-Bía
<g/>
,	,	kIx,	,
Coquimbo	Coquimba	k1gFnSc5	Coquimba
a	a	k8xC	a
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Higgins	Higgins	k1gInSc4	Higgins
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
Chile	Chile	k1gNnSc2	Chile
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
zjednodušenou	zjednodušený	k2eAgFnSc7d1	zjednodušená
verzí	verze	k1gFnSc7	verze
vlajky	vlajka	k1gFnSc2	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
barvy	barva	k1gFnPc4	barva
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
osídlili	osídlit	k5eAaPmAgMnP	osídlit
území	území	k1gNnPc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
středního	střední	k2eAgNnSc2d1	střední
Chile	Chile	k1gNnSc2	Chile
Djagitové	Djagitový	k2eAgNnSc1d1	Djagitový
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
Mapuči	Mapuč	k1gFnSc3	Mapuč
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Aurakáni	Aurakán	k2eAgMnPc1d1	Aurakán
nebo	nebo	k8xC	nebo
Araukánci	Araukánek	k1gMnPc1	Araukánek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1460	[number]	k4	1460
<g/>
–	–	k?	–
<g/>
1485	[number]	k4	1485
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Incké	incký	k2eAgFnSc2d1	incká
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1535	[number]	k4	1535
bylo	být	k5eAaImAgNnS	být
chilské	chilský	k2eAgNnSc1d1	Chilské
území	území	k1gNnSc1	území
postupně	postupně	k6eAd1	postupně
kolonizováno	kolonizovat	k5eAaBmNgNnS	kolonizovat
Španěly	Španěl	k1gMnPc4	Španěl
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1568	[number]	k4	1568
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
severní	severní	k2eAgNnSc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgNnSc1d1	střední
Chile	Chile	k1gNnSc1	Chile
součástí	součást	k1gFnSc7	součást
Místokrálovství	Místokrálovství	k1gNnSc2	Místokrálovství
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
vlajkami	vlajka	k1gFnPc7	vlajka
vyvěšovanými	vyvěšovaný	k2eAgInPc7d1	vyvěšovaný
na	na	k7c6	na
chilském	chilský	k2eAgNnSc6d1	Chilské
území	území	k1gNnSc6	území
byly	být	k5eAaImAgFnP	být
španělské	španělský	k2eAgFnPc1d1	španělská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.18	.18	k4	.18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1810	[number]	k4	1810
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
povstáním	povstání	k1gNnSc7	povstání
kreolského	kreolský	k2eAgNnSc2d1	kreolské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
Santiagu	Santiago	k1gNnSc6	Santiago
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
junta	junta	k1gFnSc1	junta
a	a	k8xC	a
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
nových	nový	k2eAgInPc2d1	nový
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
byla	být	k5eAaImAgFnS	být
naplněna	naplněn	k2eAgFnSc1d1	naplněna
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
oslav	oslava	k1gFnPc2	oslava
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
vztyčil	vztyčit	k5eAaPmAgInS	vztyčit
José	José	k1gNnSc7	José
Miguel	Miguela	k1gFnPc2	Miguela
Carrera	Carrer	k1gMnSc2	Carrer
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc2	vůdce
osvobozeneckého	osvobozenecký	k2eAgNnSc2d1	osvobozenecké
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
tvořila	tvořit	k5eAaImAgFnS	tvořit
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
modro-bílo-žlutými	modroílo-žlutý	k2eAgInPc7d1	modro-bílo-žlutý
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
přijata	přijat	k2eAgFnSc1d1	přijata
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
dekret	dekret	k1gInSc4	dekret
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
byla	být	k5eAaImAgFnS	být
vykládána	vykládán	k2eAgFnSc1d1	vykládána
jako	jako	k8xC	jako
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
hodnoty	hodnota	k1gFnPc1	hodnota
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
suverenita	suverenita	k1gFnSc1	suverenita
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
generálem	generál	k1gMnSc7	generál
Marianem	Marian	k1gMnSc7	Marian
Osoriem	Osorium	k1gNnSc7	Osorium
z	z	k7c2	z
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
dekretem	dekret	k1gInSc7	dekret
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
použita	použit	k2eAgFnSc1d1	použita
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Rancagua	Rancagu	k1gInSc2	Rancagu
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
byly	být	k5eAaImAgFnP	být
znovuzavedeny	znovuzaveden	k2eAgFnPc1d1	znovuzavedena
španělské	španělský	k2eAgFnPc1d1	španělská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1817	[number]	k4	1817
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Chacabuca	Chacabuc	k1gInSc2	Chacabuc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vojska	vojsko	k1gNnSc2	vojsko
generálů	generál	k1gMnPc2	generál
San	San	k1gMnSc2	San
Martina	Martin	k1gMnSc2	Martin
a	a	k8xC	a
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Higginse	Higgins	k1gInSc6	Higgins
porazila	porazit	k5eAaPmAgFnS	porazit
Španěly	Španěl	k1gMnPc4	Španěl
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgFnSc1d1	obnovena
chilská	chilský	k2eAgFnSc1d1	chilská
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1817	[number]	k4	1817
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Higgins	Higgins	k1gInSc4	Higgins
<g/>
)	)	kIx)	)
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
bezoblačnou	bezoblačný	k2eAgFnSc4d1	bezoblačná
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
sněhem	sníh	k1gInSc7	sníh
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
Andy	Anda	k1gFnSc2	Anda
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
hrdinů	hrdina	k1gMnPc2	hrdina
prolitá	prolitý	k2eAgFnSc1d1	prolitá
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Již	již	k6eAd1	již
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1817	[number]	k4	1817
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
chilská	chilský	k2eAgFnSc1d1	chilská
vlajka	vlajka	k1gFnSc1	vlajka
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novou	nova	k1gFnSc7	nova
(	(	kIx(	(
<g/>
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
platnou	platný	k2eAgFnSc7d1	platná
<g/>
)	)	kIx)	)
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgMnS	prosadit
ji	on	k3xPp3gFnSc4	on
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
José	Josá	k1gFnSc2	Josá
Ignacio	Ignacio	k1gNnSc1	Ignacio
Zenteno	Zenten	k2eAgNnSc1d1	Zenten
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
voják	voják	k1gMnSc1	voják
Antonio	Antonio	k1gMnSc1	Antonio
Arcos	Arcos	k1gMnSc1	Arcos
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
jméno	jméno	k1gNnSc4	jméno
Gregorio	Gregorio	k1gMnSc1	Gregorio
de	de	k?	de
Andía	Andía	k1gMnSc1	Andía
y	y	k?	y
Varela	Varela	k1gFnSc1	Varela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Chile	Chile	k1gNnSc4	Chile
samostatnost	samostatnost	k1gFnSc1	samostatnost
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1818	[number]	k4	1818
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
uznalo	uznat	k5eAaPmAgNnS	uznat
i	i	k8xC	i
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
chilských	chilský	k2eAgInPc2d1	chilský
regionů	region	k1gInPc2	region
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Chile	Chile	k1gNnSc1	Chile
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
šestnácti	šestnáct	k4xCc2	šestnáct
regionů	region	k1gInPc2	region
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
chilského	chilský	k2eAgInSc2d1	chilský
regionu	region	k1gInSc2	region
Coquimbo	Coquimba	k1gFnSc5	Coquimba
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
vexilologů	vexilolog	k1gMnPc2	vexilolog
příkladem	příklad	k1gInSc7	příklad
zcela	zcela	k6eAd1	zcela
nevexilologické	vexilologický	k2eNgFnSc2d1	vexilologický
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
porušující	porušující	k2eAgFnPc1d1	porušující
snad	snad	k9	snad
všechny	všechen	k3xTgFnPc1	všechen
zásady	zásada	k1gFnPc1	zásada
vytváření	vytváření	k1gNnSc2	vytváření
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnSc2	vlajka
provincie	provincie	k1gFnSc2	provincie
Isla	Islus	k1gMnSc2	Islus
de	de	k?	de
Páscua	Páscuus	k1gMnSc2	Páscuus
a	a	k8xC	a
chilských	chilský	k2eAgNnPc2d1	Chilské
etnik	etnikum	k1gNnPc2	etnikum
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastní	vlastní	k2eAgFnSc4d1	vlastní
(	(	kIx(	(
<g/>
neoficiální	oficiální	k2eNgFnSc4d1	neoficiální
<g/>
)	)	kIx)	)
vlajku	vlajka	k1gFnSc4	vlajka
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
pouze	pouze	k6eAd1	pouze
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
51	[number]	k4	51
chilských	chilský	k2eAgFnPc2d1	chilská
provincií	provincie	k1gFnPc2	provincie
-	-	kIx~	-
Isla	Isl	k2eAgFnSc1d1	Isla
de	de	k?	de
Páscua	Páscua	k1gFnSc1	Páscua
(	(	kIx(	(
<g/>
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
domorodém	domorodý	k2eAgInSc6d1	domorodý
jazyce	jazyk	k1gInSc6	jazyk
Rapa	rap	k1gMnSc2	rap
Nui	Nui	k1gMnSc2	Nui
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
pod	pod	k7c4	pod
region	region	k1gInSc4	region
Valparaíso	Valparaísa	k1gFnSc5	Valparaísa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
červený	červený	k2eAgInSc1d1	červený
Rei-miro	Reiiro	k1gNnSc4	Rei-miro
<g/>
,	,	kIx,	,
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
předmět	předmět	k1gInSc4	předmět
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
zdobily	zdobit	k5eAaImAgFnP	zdobit
ženy	žena	k1gFnPc1	žena
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rei-miro	Reiiro	k6eAd1	Rei-miro
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
polynéskou	polynéský	k2eAgFnSc4d1	polynéská
kánoi	kánoe	k1gFnSc4	kánoe
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
konce	konec	k1gInPc1	konec
ozdoby	ozdoba	k1gFnSc2	ozdoba
jsou	být	k5eAaImIp3nP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
stylizovanými	stylizovaný	k2eAgFnPc7d1	stylizovaná
lidskými	lidský	k2eAgFnPc7d1	lidská
hlavami	hlava	k1gFnPc7	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mapučové	Mapuč	k1gMnPc1	Mapuč
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
také	také	k9	také
Araukáni	Araukán	k2eAgMnPc1d1	Araukán
nebo	nebo	k8xC	nebo
Araukánci	Araukánek	k1gMnPc1	Araukánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
indiánů	indián	k1gMnPc2	indián
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižních	jižní	k2eAgFnPc2d1	jižní
And	Anda	k1gFnPc2	Anda
a	a	k8xC	a
Patagonie	Patagonie	k1gFnSc1	Patagonie
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
vlajky	vlajka	k1gFnPc1	vlajka
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
další	další	k2eAgNnPc1d1	další
etnika	etnikum	k1gNnPc1	etnikum
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Chile	Chile	k1gNnSc2	Chile
</s>
</p>
<p>
<s>
Chilská	chilský	k2eAgFnSc1d1	chilská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Chile	Chile	k1gNnSc2	Chile
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chilská	chilský	k2eAgFnSc1d1	chilská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
