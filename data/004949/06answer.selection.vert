<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
a	a	k8xC	a
katakana	katakana	k1gFnSc1	katakana
jsou	být	k5eAaImIp3nP	být
slabičné	slabičný	k2eAgFnPc4d1	slabičná
abecedy	abeceda	k1gFnPc4	abeceda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kandži	kandzat	k5eAaPmIp1nS	kandzat
je	on	k3xPp3gNnSc4	on
morfemografické	morfemografický	k2eAgNnSc4d1	morfemografický
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
