<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
je	být	k5eAaImIp3nS	být
liturgický	liturgický	k2eAgInSc4d1	liturgický
a	a	k8xC	a
literární	literární	k2eAgInSc4d1	literární
jazyk	jazyk	k1gInSc4	jazyk
používaný	používaný	k2eAgInSc4d1	používaný
slovanskými	slovanský	k2eAgFnPc7d1	Slovanská
pravoslavnými	pravoslavný	k2eAgFnPc7d1	pravoslavná
a	a	k8xC	a
řeckokatolickými	řeckokatolický	k2eAgFnPc7d1	řeckokatolická
církvemi	církev	k1gFnPc7	církev
<g/>
;	;	kIx,	;
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednotný	jednotný	k2eAgInSc4d1	jednotný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
;	;	kIx,	;
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
podobách	podoba	k1gFnPc6	podoba
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
redakcích	redakce	k1gFnPc6	redakce
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
ze	z	k7c2	z
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
(	(	kIx(	(
<g/>
staré	starý	k2eAgFnPc1d1	stará
církevní	církevní	k2eAgFnPc1d1	církevní
slovanštiny	slovanština	k1gFnPc1	slovanština
<g/>
)	)	kIx)	)
úpravou	úprava	k1gFnSc7	úprava
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
výslovnosti	výslovnost	k1gFnSc2	výslovnost
a	a	k8xC	a
náhradou	náhrada	k1gFnSc7	náhrada
části	část	k1gFnSc2	část
lexika	lexikon	k1gNnSc2	lexikon
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
existence	existence	k1gFnSc2	existence
tzv.	tzv.	kA	tzv.
Trnovské	Trnovský	k2eAgFnSc2d1	Trnovský
školy	škola	k1gFnSc2	škola
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
prosazovány	prosazován	k2eAgFnPc1d1	prosazována
četné	četný	k2eAgFnPc1d1	četná
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
znovu	znovu	k6eAd1	znovu
přeložily	přeložit	k5eAaPmAgFnP	přeložit
početné	početný	k2eAgFnPc1d1	početná
náboženské	náboženský	k2eAgFnPc1d1	náboženská
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
nepřesným	přesný	k2eNgInPc3d1	nepřesný
výkladům	výklad	k1gInPc3	výklad
náboženských	náboženský	k2eAgFnPc2d1	náboženská
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
různých	různý	k2eAgFnPc2d1	různá
forem	forma	k1gFnPc2	forma
hereze	hereze	k1gFnSc2	hereze
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
jednotný	jednotný	k2eAgInSc1d1	jednotný
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
rozrůznil	rozrůznit	k5eAaPmAgInS	rozrůznit
vznikem	vznik	k1gInSc7	vznik
různých	různý	k2eAgFnPc2d1	různá
redakcí	redakce	k1gFnPc2	redakce
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
navzájem	navzájem	k6eAd1	navzájem
oddělených	oddělený	k2eAgNnPc6d1	oddělené
centrech	centrum	k1gNnPc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
zajistily	zajistit	k5eAaPmAgInP	zajistit
hlavně	hlavně	k9	hlavně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc1	jazyk
dobře	dobře	k6eAd1	dobře
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
pro	pro	k7c4	pro
mluvčí	mluvčí	k1gMnPc4	mluvčí
daného	daný	k2eAgInSc2d1	daný
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
slovanského	slovanský	k2eAgInSc2d1	slovanský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
území	území	k1gNnSc6	území
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
redakce	redakce	k1gFnPc1	redakce
církevní	církevní	k2eAgFnPc4d1	církevní
slovanštiny	slovanština	k1gFnPc4	slovanština
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgNnSc4d1	srozumitelné
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
odešli	odejít	k5eAaPmAgMnP	odejít
tamní	tamní	k2eAgMnPc1d1	tamní
vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
do	do	k7c2	do
Valašska	Valašsko	k1gNnSc2	Valašsko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svojí	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
bulharské	bulharský	k2eAgFnSc2d1	bulharská
redakce	redakce	k1gFnSc2	redakce
v	v	k7c6	v
pravoslavném	pravoslavný	k2eAgInSc6d1	pravoslavný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
upravovaný	upravovaný	k2eAgInSc1d1	upravovaný
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dobře	dobře	k6eAd1	dobře
ovládali	ovládat	k5eAaImAgMnP	ovládat
bulharštinu	bulharština	k1gFnSc4	bulharština
<g/>
,	,	kIx,	,
nezůstane	zůstat	k5eNaPmIp3nS	zůstat
ve	v	k7c6	v
východoslovanském	východoslovanský	k2eAgNnSc6d1	východoslovanské
prostředí	prostředí	k1gNnSc6	prostředí
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
drobných	drobný	k2eAgFnPc2d1	drobná
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
tendence	tendence	k1gFnPc1	tendence
používat	používat	k5eAaImF	používat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
některé	některý	k3yIgFnSc2	některý
koncovky	koncovka	k1gFnSc2	koncovka
ruských	ruský	k2eAgInPc2d1	ruský
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
např.	např.	kA	např.
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgNnSc1d1	ruské
časování	časování	k1gNnSc1	časování
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc3	osoba
zakončení	zakončení	k1gNnSc1	zakončení
na	na	k7c6	na
tь	tь	k?	tь
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
byly	být	k5eAaImAgFnP	být
nosovky	nosovka	k1gFnPc1	nosovka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
slovanštině	slovanština	k1gFnSc6	slovanština
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
slovanskou	slovanský	k2eAgFnSc7d1	Slovanská
hláskou	hláska	k1gFnSc7	hláska
jať	jať	k1gNnSc2	jať
(	(	kIx(	(
<g/>
zapisovanou	zapisovaný	k2eAgFnSc4d1	zapisovaná
ѣ	ѣ	k?	ѣ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
nakonec	nakonec	k6eAd1	nakonec
zrealizovala	zrealizovat	k5eAaPmAgFnS	zrealizovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
novověku	novověk	k1gInSc2	novověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
spisovné	spisovný	k2eAgFnPc1d1	spisovná
formy	forma	k1gFnPc1	forma
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
literární	literární	k2eAgFnSc2d1	literární
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
východních	východní	k2eAgInPc2d1	východní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
;	;	kIx,	;
zejména	zejména	k9	zejména
ruština	ruština	k1gFnSc1	ruština
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
množství	množství	k1gNnSc2	množství
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
užíván	užíván	k2eAgMnSc1d1	užíván
rovněž	rovněž	k9	rovněž
jazyk	jazyk	k1gMnSc1	jazyk
lidový	lidový	k2eAgMnSc1d1	lidový
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
vznikala	vznikat	k5eAaImAgFnS	vznikat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
přechodných	přechodný	k2eAgFnPc2d1	přechodná
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihoslovanských	jihoslovanský	k2eAgFnPc6d1	Jihoslovanská
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
kultury	kultura	k1gFnSc2	kultura
i	i	k9	i
písemnictví	písemnictví	k1gNnSc4	písemnictví
a	a	k8xC	a
dominovat	dominovat	k5eAaImF	dominovat
začala	začít	k5eAaPmAgFnS	začít
turečtina	turečtina	k1gFnSc1	turečtina
a	a	k8xC	a
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
platila	platit	k5eAaImAgFnS	platit
za	za	k7c4	za
jazyk	jazyk	k1gInSc4	jazyk
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
nástupu	nástup	k1gInSc2	nástup
reforem	reforma	k1gFnPc2	reforma
Vuka	Vuka	k1gFnSc1	Vuka
Karadžiće	Karadžiće	k1gFnSc1	Karadžiće
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
ale	ale	k8xC	ale
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
konstrukce	konstrukce	k1gFnPc4	konstrukce
různých	různý	k2eAgInPc2d1	různý
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
slavenosrbský	slavenosrbský	k2eAgInSc4d1	slavenosrbský
nebo	nebo	k8xC	nebo
ruskoslovanský	ruskoslovanský	k2eAgInSc4d1	ruskoslovanský
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
získala	získat	k5eAaPmAgFnS	získat
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
ruská	ruský	k2eAgFnSc1d1	ruská
redakce	redakce	k1gFnSc1	redakce
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
jihovýchodní	jihovýchodní	k2eAgFnSc7d1	jihovýchodní
Evropou	Evropa	k1gFnSc7	Evropa
byly	být	k5eAaImAgFnP	být
dováženy	dovážit	k5eAaPmNgFnP	dovážit
hlavně	hlavně	k9	hlavně
knihy	kniha	k1gFnPc1	kniha
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
ruský	ruský	k2eAgInSc1d1	ruský
jazyk	jazyk	k1gInSc1	jazyk
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
jazykové	jazykový	k2eAgInPc4d1	jazykový
procesy	proces	k1gInPc4	proces
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
byly	být	k5eAaImAgFnP	být
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
,	,	kIx,	,
či	či	k8xC	či
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Církevněslovanský	církevněslovanský	k2eAgInSc1d1	církevněslovanský
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
užití	užití	k1gNnSc6	užití
vymřelý	vymřelý	k2eAgInSc4d1	vymřelý
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
jej	on	k3xPp3gInSc4	on
používá	používat	k5eAaImIp3nS	používat
řada	řada	k1gFnSc1	řada
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
společný	společný	k2eAgInSc4d1	společný
jazyk	jazyk	k1gInSc4	jazyk
jak	jak	k8xS	jak
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
Bulharů	Bulhar	k1gMnPc2	Bulhar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
překládaly	překládat	k5eAaImAgInP	překládat
mnohé	mnohý	k2eAgInPc1d1	mnohý
náboženské	náboženský	k2eAgInPc1d1	náboženský
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
v	v	k7c6	v
bulharské	bulharský	k2eAgFnSc6d1	bulharská
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
damaskiny	damaskina	k1gFnPc1	damaskina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
sepsány	sepsat	k5eAaPmNgFnP	sepsat
právě	právě	k9	právě
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
slovanštině	slovanština	k1gFnSc6	slovanština
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
řeči	řeč	k1gFnSc2	řeč
prostému	prostý	k2eAgInSc3d1	prostý
lidu	lid	k1gInSc3	lid
však	však	k9	však
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
začala	začít	k5eAaPmAgFnS	začít
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
přirozený	přirozený	k2eAgInSc1d1	přirozený
vývoj	vývoj	k1gInSc1	vývoj
lidových	lidový	k2eAgInPc2d1	lidový
jazyků	jazyk	k1gInPc2	jazyk
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
neustále	neustále	k6eAd1	neustále
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ustrnuta	ustrnut	k2eAgFnSc1d1	ustrnut
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
