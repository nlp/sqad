<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
je	být	k5eAaImIp3nS	být
liturgický	liturgický	k2eAgInSc4d1	liturgický
a	a	k8xC	a
literární	literární	k2eAgInSc4d1	literární
jazyk	jazyk	k1gInSc4	jazyk
používaný	používaný	k2eAgInSc4d1	používaný
slovanskými	slovanský	k2eAgFnPc7d1	Slovanská
pravoslavnými	pravoslavný	k2eAgFnPc7d1	pravoslavná
a	a	k8xC	a
řeckokatolickými	řeckokatolický	k2eAgFnPc7d1	řeckokatolická
církvemi	církev	k1gFnPc7	církev
<g/>
;	;	kIx,	;
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
.	.	kIx.	.
</s>
