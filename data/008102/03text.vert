<s>
Horečka	horečka	k1gFnSc1	horečka
(	(	kIx(	(
<g/>
lat	lat	k1gInSc1	lat
<g/>
:	:	kIx,	:
<g/>
febris	febris	k1gInSc1	febris
<g/>
,	,	kIx,	,
ř	ř	k?	ř
<g/>
:	:	kIx,	:
<g/>
pyretos	pyretos	k1gMnSc1	pyretos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
stavu	stav	k1gInSc2	stav
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
horečku	horečka	k1gFnSc4	horečka
považována	považován	k2eAgFnSc1d1	považována
teplota	teplota	k1gFnSc1	teplota
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
teplotu	teplota	k1gFnSc4	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
pro	pro	k7c4	pro
příliš	příliš	k6eAd1	příliš
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotu	teplota	k1gFnSc4	teplota
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
přehřátí	přehřátí	k1gNnSc1	přehřátí
<g/>
,	,	kIx,	,
hypertermie	hypertermie	k1gFnSc1	hypertermie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
za	za	k7c4	za
horečku	horečka	k1gFnSc4	horečka
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
pyrogeny	pyrogen	k1gInPc7	pyrogen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
centrum	centrum	k1gNnSc4	centrum
termoregulace	termoregulace	k1gFnSc2	termoregulace
v	v	k7c6	v
hypotalamu	hypotalamus	k1gInSc6	hypotalamus
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
snižovat	snižovat	k5eAaImF	snižovat
horečku	horečka	k1gFnSc4	horečka
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
jen	jen	k9	jen
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
jistý	jistý	k2eAgInSc4d1	jistý
práh	práh	k1gInSc4	práh
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
udávaný	udávaný	k2eAgInSc1d1	udávaný
jako	jako	k9	jako
39	[number]	k4	39
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
či	či	k8xC	či
pokud	pokud	k8xS	pokud
trvá	trvat	k5eAaImIp3nS	trvat
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
není	být	k5eNaImIp3nS	být
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouze	pouze	k6eAd1	pouze
projev	projev	k1gInSc1	projev
(	(	kIx(	(
<g/>
příznak	příznak	k1gInSc1	příznak
<g/>
)	)	kIx)	)
nemoci	nemoc	k1gFnSc2	nemoc
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
chorobného	chorobný	k2eAgInSc2d1	chorobný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
léčebným	léčebný	k2eAgInSc7d1	léčebný
procesem	proces	k1gInSc7	proces
<g/>
:	:	kIx,	:
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
imunitní	imunitní	k2eAgFnSc2d1	imunitní
odpovědi	odpověď	k1gFnSc2	odpověď
a	a	k8xC	a
omezuje	omezovat	k5eAaImIp3nS	omezovat
účinnost	účinnost	k1gFnSc4	účinnost
množení	množení	k1gNnSc2	množení
některých	některý	k3yIgInPc2	některý
patogenů	patogen	k1gInPc2	patogen
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tepovou	tepový	k2eAgFnSc4d1	tepová
frekvenci	frekvence	k1gFnSc4	frekvence
a	a	k8xC	a
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
látkovou	látkový	k2eAgFnSc4d1	látková
výměnu	výměna	k1gFnSc4	výměna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
zátěž	zátěž	k1gFnSc4	zátěž
především	především	k9	především
pro	pro	k7c4	pro
starší	starý	k2eAgMnPc4d2	starší
pacienty	pacient	k1gMnPc4	pacient
a	a	k8xC	a
pacienty	pacient	k1gMnPc4	pacient
se	s	k7c7	s
srdeční	srdeční	k2eAgFnSc7d1	srdeční
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysilující	vysilující	k2eAgNnSc1d1	vysilující
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mění	měnit	k5eAaImIp3nS	měnit
rychlost	rychlost	k1gFnSc4	rychlost
některých	některý	k3yIgFnPc2	některý
biochemických	biochemický	k2eAgFnPc2d1	biochemická
reakcí	reakce	k1gFnPc2	reakce
–	–	k?	–
klade	klást	k5eAaImIp3nS	klást
vyšší	vysoký	k2eAgInPc4d2	vyšší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
udržování	udržování	k1gNnSc4	udržování
homeostázy	homeostáza	k1gFnSc2	homeostáza
<g/>
,	,	kIx,	,
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
opotřebovávání	opotřebovávání	k1gNnSc1	opotřebovávání
některých	některý	k3yIgFnPc2	některý
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
atd.	atd.	kA	atd.
Pokud	pokud	k8xS	pokud
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
překročí	překročit	k5eAaPmIp3nS	překročit
určitou	určitý	k2eAgFnSc4d1	určitá
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
mít	mít	k5eAaImF	mít
silně	silně	k6eAd1	silně
destruktivní	destruktivní	k2eAgInPc4d1	destruktivní
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
ještě	ještě	k6eAd1	ještě
přežít	přežít	k5eAaPmF	přežít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
teplota	teplota	k1gFnSc1	teplota
41,9	[number]	k4	41,9
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
37	[number]	k4	37
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
mírné	mírný	k2eAgNnSc4d1	mírné
přehřátí	přehřátí	k1gNnSc4	přehřátí
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vůbec	vůbec	k9	vůbec
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
38	[number]	k4	38
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
°	°	k?	°
<g/>
C	C	kA	C
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
zřetelné	zřetelný	k2eAgNnSc4d1	zřetelné
přehřátí	přehřátí	k1gNnSc4	přehřátí
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
častý	častý	k2eAgInSc4d1	častý
odpočinek	odpočinek	k1gInSc4	odpočinek
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
–	–	k?	–
pacientovi	pacient	k1gMnSc6	pacient
je	být	k5eAaImIp3nS	být
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
41	[number]	k4	41
°	°	k?	°
<g/>
C	C	kA	C
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
začíná	začínat	k5eAaImIp3nS	začínat
mít	mít	k5eAaImF	mít
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
,	,	kIx,	,
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
má	mít	k5eAaImIp3nS	mít
zimnici	zimnice	k1gFnSc4	zimnice
<g/>
;	;	kIx,	;
když	když	k8xS	když
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
však	však	k9	však
přikryje	přikrýt	k5eAaPmIp3nS	přikrýt
dekou	deka	k1gFnSc7	deka
<g/>
,	,	kIx,	,
cítí	cítit	k5eAaImIp3nS	cítit
naopak	naopak	k6eAd1	naopak
silné	silný	k2eAgNnSc1d1	silné
horko	horko	k1gNnSc1	horko
41	[number]	k4	41
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
má	mít	k5eAaImIp3nS	mít
halucinace	halucinace	k1gFnPc4	halucinace
<g/>
,	,	kIx,	,
padá	padat	k5eAaImIp3nS	padat
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
se	se	k3xPyFc4	se
potí	potit	k5eAaImIp3nS	potit
<g/>
;	;	kIx,	;
hrozí	hrozit	k5eAaImIp3nS	hrozit
selhání	selhání	k1gNnSc4	selhání
organizmu	organizmus	k1gInSc2	organizmus
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
více	hodně	k6eAd2	hodně
–	–	k?	–
selhání	selhání	k1gNnSc4	selhání
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
Horečka	horečka	k1gFnSc1	horečka
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
příznak	příznak	k1gInSc1	příznak
je	být	k5eAaImIp3nS	být
snižována	snižován	k2eAgFnSc1d1	snižována
<g/>
,	,	kIx,	,
především	především	k9	především
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
přestává	přestávat	k5eAaImIp3nS	přestávat
plnit	plnit	k5eAaImF	plnit
svoji	svůj	k3xOyFgFnSc4	svůj
fyziologickou	fyziologický	k2eAgFnSc4d1	fyziologická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
a	a	k8xC	a
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
průvodní	průvodní	k2eAgInPc1d1	průvodní
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
chybou	chyba	k1gFnSc7	chyba
ani	ani	k8xC	ani
její	její	k3xOp3gNnSc1	její
snižování	snižování	k1gNnSc1	snižování
paušální	paušální	k2eAgNnSc1d1	paušální
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
horečky	horečka	k1gFnSc2	horečka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dva	dva	k4xCgInPc1	dva
přístupy	přístup	k1gInPc1	přístup
<g/>
:	:	kIx,	:
farmakologický	farmakologický	k2eAgInSc1d1	farmakologický
přístup	přístup	k1gInSc1	přístup
<g/>
:	:	kIx,	:
Horečka	horečka	k1gFnSc1	horečka
je	být	k5eAaImIp3nS	být
snižována	snižovat	k5eAaImNgFnS	snižovat
podáváním	podávání	k1gNnSc7	podávání
léků	lék	k1gInPc2	lék
působících	působící	k2eAgInPc2d1	působící
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zánětu	zánět	k1gInSc2	zánět
a	a	k8xC	a
tlumící	tlumící	k2eAgInSc4d1	tlumící
probíhající	probíhající	k2eAgInSc4d1	probíhající
zánět	zánět	k1gInSc4	zánět
nebo	nebo	k8xC	nebo
centrálně	centrálně	k6eAd1	centrálně
v	v	k7c6	v
termoregulačním	termoregulační	k2eAgNnSc6d1	termoregulační
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
mechanismech	mechanismus	k1gInPc6	mechanismus
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
inhibice	inhibice	k1gFnSc1	inhibice
enzymu	enzym	k1gInSc2	enzym
cyklooxygenáza	cyklooxygenáza	k1gFnSc1	cyklooxygenáza
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
léků	lék	k1gInPc2	lék
však	však	k9	však
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
kyselina	kyselina	k1gFnSc1	kyselina
acetylsalicylová	acetylsalicylový	k2eAgFnSc1d1	acetylsalicylová
tlumí	tlumit	k5eAaImIp3nS	tlumit
i	i	k9	i
probíhající	probíhající	k2eAgInSc4d1	probíhající
zánět	zánět	k1gInSc4	zánět
<g/>
,	,	kIx,	,
paracetamol	paracetamol	k1gInSc4	paracetamol
působí	působit	k5eAaImIp3nP	působit
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
centrálně	centrálně	k6eAd1	centrálně
<g/>
.	.	kIx.	.
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
přístup	přístup	k1gInSc1	přístup
<g/>
:	:	kIx,	:
Pacient	pacient	k1gMnSc1	pacient
je	být	k5eAaImIp3nS	být
fyzicky	fyzicky	k6eAd1	fyzicky
ochlazován	ochlazován	k2eAgMnSc1d1	ochlazován
např.	např.	kA	např.
omýváním	omývání	k1gNnPc3	omývání
vlažnou	vlažný	k2eAgFnSc7d1	vlažná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Extrémním	extrémní	k2eAgInSc7d1	extrémní
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
studený	studený	k2eAgInSc1d1	studený
zábal	zábal	k1gInSc1	zábal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
horečky	horečka	k1gFnSc2	horečka
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k8xC	i
vliv	vliv	k1gInSc1	vliv
některé	některý	k3yIgInPc4	některý
další	další	k2eAgInPc4d1	další
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
terapeutické	terapeutický	k2eAgNnSc1d1	terapeutické
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
však	však	k9	však
omezené	omezený	k2eAgNnSc1d1	omezené
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
medicína	medicína	k1gFnSc1	medicína
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
postupů	postup	k1gInPc2	postup
doporučovaných	doporučovaný	k2eAgInPc2d1	doporučovaný
při	při	k7c6	při
horečce	horečka	k1gFnSc6	horečka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
bezu	bez	k1gInSc2	bez
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klinická	klinický	k2eAgFnSc1d1	klinická
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
je	být	k5eAaImIp3nS	být
však	však	k9	však
značně	značně	k6eAd1	značně
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
"	"	kIx"	"
<g/>
léčení	léčení	k1gNnSc4	léčení
horečky	horečka	k1gFnSc2	horečka
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
přiživit	přiživit	k5eAaPmF	přiživit
i	i	k9	i
prodejci	prodejce	k1gMnPc1	prodejce
potravinových	potravinový	k2eAgInPc2d1	potravinový
doplňků	doplněk	k1gInPc2	doplněk
a	a	k8xC	a
tak	tak	k9	tak
o	o	k7c6	o
řadě	řada	k1gFnSc6	řada
svých	svůj	k3xOyFgInPc2	svůj
produktů	produkt	k1gInPc2	produkt
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
snižování	snižování	k1gNnSc4	snižování
horečky	horečka	k1gFnSc2	horečka
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivost	pravdivost	k1gFnSc1	pravdivost
těchto	tento	k3xDgNnPc2	tento
tvrzení	tvrzení	k1gNnPc2	tvrzení
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
lepších	dobrý	k2eAgInPc6d2	lepší
případech	případ	k1gInPc6	případ
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
v	v	k7c6	v
horších	horší	k1gNnPc6	horší
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nepravdivá	pravdivý	k2eNgNnPc4d1	nepravdivé
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
Antipyretikum	antipyretikum	k1gNnSc4	antipyretikum
kyselina	kyselina	k1gFnSc1	kyselina
acetylsalicylová	acetylsalicylový	k2eAgFnSc1d1	acetylsalicylová
Paracetamol	paracetamol	k1gInSc4	paracetamol
aspirin	aspirin	k1gInSc1	aspirin
<g/>
,	,	kIx,	,
acylpyrin	acylpyrin	k1gInSc1	acylpyrin
Chřipka	chřipka	k1gFnSc1	chřipka
Malárie	malárie	k1gFnSc1	malárie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
horečka	horečka	k1gFnSc1	horečka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
horečka	horečka	k1gFnSc1	horečka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
