<s>
Felix	Felix	k1gMnSc1	Felix
Holzmann	Holzmann	k1gMnSc1	Holzmann
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Chemnitz	Chemnitz	k1gInSc1	Chemnitz
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
komik	komik	k1gMnSc1	komik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
scénkách	scénka	k1gFnPc6	scénka
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
s	s	k7c7	s
kulatými	kulatý	k2eAgFnPc7d1	kulatá
brýlemi	brýle	k1gFnPc7	brýle
a	a	k8xC	a
ve	v	k7c6	v
slaměném	slaměný	k2eAgInSc6d1	slaměný
klobouku	klobouk	k1gInSc6	klobouk
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tralaláčku	tralaláček	k1gInSc2	tralaláček
<g/>
.	.	kIx.	.
</s>
<s>
Scénky	scénka	k1gFnPc1	scénka
měly	mít	k5eAaImAgFnP	mít
vesměs	vesměs	k6eAd1	vesměs
charakter	charakter	k1gInSc4	charakter
dialogu	dialog	k1gInSc2	dialog
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
monologu	monolog	k1gInSc2	monolog
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
typ	typ	k1gInSc1	typ
natvrdlého	natvrdlý	k2eAgMnSc2d1	natvrdlý
chlapíka	chlapík	k1gMnSc2	chlapík
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
všechno	všechen	k3xTgNnSc1	všechen
dochází	docházet	k5eAaImIp3nS	docházet
nezvykle	zvykle	k6eNd1	zvykle
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
tázavým	tázavý	k2eAgNnSc7d1	tázavé
zpěvavým	zpěvavý	k2eAgNnSc7d1	zpěvavé
protahováním	protahování	k1gNnSc7	protahování
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
precizně	precizně	k6eAd1	precizně
vystavěné	vystavěný	k2eAgInPc1d1	vystavěný
<g/>
;	;	kIx,	;
humor	humor	k1gInSc1	humor
často	často	k6eAd1	často
těží	těžet	k5eAaImIp3nS	těžet
z	z	k7c2	z
nečekaného	čekaný	k2eNgNnSc2d1	nečekané
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nepatřičně	patřičně	k6eNd1	patřičně
doslovného	doslovný	k2eAgNnSc2d1	doslovné
<g/>
)	)	kIx)	)
uchopení	uchopení	k1gNnSc2	uchopení
významu	význam	k1gInSc2	význam
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Partnery	partner	k1gMnPc4	partner
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
vystoupeních	vystoupení	k1gNnPc6	vystoupení
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
František	František	k1gMnSc1	František
Budín	Budín	k1gMnSc1	Budín
(	(	kIx(	(
<g/>
v	v	k7c6	v
civilu	civil	k1gMnSc6	civil
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
ČSD	ČSD	kA	ČSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Maurer	Maurer	k1gMnSc1	Maurer
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
,	,	kIx,	,
Milan	Milana	k1gFnPc2	Milana
Neděla	dít	k5eNaBmAgFnS	dít
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Borovička	Borovička	k1gMnSc1	Borovička
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Bruder	Bruder	k1gMnSc1	Bruder
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Dítě	Dítě	k1gMnSc1	Dítě
ale	ale	k8xC	ale
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
nebo	nebo	k8xC	nebo
Iva	Iva	k1gFnSc1	Iva
Janžurová	Janžurový	k2eAgFnSc1d1	Janžurová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
žil	žíla	k1gFnPc2	žíla
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
-	-	kIx~	-
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
a	a	k8xC	a
konferenciérkou	konferenciérka	k1gFnSc7	konferenciérka
Barbarou	Barbara	k1gFnSc7	Barbara
Greif	Greif	k1gInSc4	Greif
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
rockovém	rockový	k2eAgInSc6d1	rockový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
obdržel	obdržet	k5eAaPmAgMnS	obdržet
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Mírového	mírový	k2eAgNnSc2d1	Mírové
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dům	dům	k1gInSc1	dům
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
umístěna	umístit	k5eAaPmNgFnS	umístit
jeho	jeho	k3xOp3gFnSc1	jeho
busta	busta	k1gFnSc1	busta
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
Akvárium	akvárium	k1gNnSc1	akvárium
hovoří	hovořit	k5eAaImIp3nS	hovořit
Holzmannova	Holzmannův	k2eAgFnSc1d1	Holzmannův
postava	postava	k1gFnSc1	postava
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
na	na	k7c6	na
Máchově	Máchův	k2eAgFnSc6d1	Máchova
44	[number]	k4	44
<g/>
,	,	kIx,	,
taková	takový	k3xDgFnSc1	takový
adresa	adresa	k1gFnSc1	adresa
tam	tam	k6eAd1	tam
však	však	k9	však
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
veřejnost	veřejnost	k1gFnSc1	veřejnost
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
armádě	armáda	k1gFnSc6	armáda
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
proto	proto	k8xC	proto
strávil	strávit	k5eAaPmAgMnS	strávit
rok	rok	k1gInSc4	rok
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
tvrzením	tvrzení	k1gNnPc3	tvrzení
chybějí	chybět	k5eAaImIp3nP	chybět
upřesňující	upřesňující	k2eAgInPc1d1	upřesňující
dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
×	×	k?	×
<g />
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
Holzmann	Holzmann	k1gMnSc1	Holzmann
Ještě	ještě	k9	ještě
10	[number]	k4	10
<g/>
×	×	k?	×
Felix	Felix	k1gMnSc1	Felix
Holzmann	Holzmann	k1gInSc4	Holzmann
Ztracený	ztracený	k2eAgInSc4d1	ztracený
a	a	k8xC	a
nalezený	nalezený	k2eAgInSc4d1	nalezený
Huhulák	Huhulák	k1gInSc4	Huhulák
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Felix	Felix	k1gMnSc1	Felix
Holzmann	Holzmann	k1gMnSc1	Holzmann
Profil	profil	k1gInSc4	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Supraphonu	supraphon	k1gInSc2	supraphon
Felix	Felix	k1gMnSc1	Felix
Holzmann	Holzmann	k1gMnSc1	Holzmann
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Článek	článek	k1gInSc4	článek
o	o	k7c6	o
Felixu	Felixu	k?	Felixu
Holzmannovi	Holzmann	k1gMnSc6	Holzmann
v	v	k7c6	v
Blesku	blesk	k1gInSc6	blesk
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Felixe	Felix	k1gMnSc4	Felix
Holzmanna	Holzmann	k1gMnSc4	Holzmann
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
Holzmann	Holzmann	k1gMnSc1	Holzmann
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Příběhy	příběh	k1gInPc4	příběh
slavných	slavný	k2eAgInPc2d1	slavný
</s>
