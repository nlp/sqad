<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
souvislého	souvislý	k2eAgInSc2d1	souvislý
oceánu	oceán	k1gInSc2	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
71	[number]	k4	71
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
