<s>
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devicesa	k1gFnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Logo	logo	k1gNnSc1
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Právní	právní	k2eAgInPc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
Public	publicum	k1gNnPc2
<g/>
)	)	kIx)
Datum	datum	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1969	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
NexGen	NexGen	k2eAgMnSc1d1
Zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
Jerry	Jerra	k1gFnPc1
Sanders	Sandersa	k1gFnPc2
<g/>
,	,	kIx,
Edwin	Edwin	k1gInSc4
Turney	Turnea	k1gFnSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
USA	USA	kA
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
95054	#num#	k4
<g/>
,	,	kIx,
US	US	kA
Souřadnice	souřadnice	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
121	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Počet	počet	k1gInSc1
poboček	pobočka	k1gFnPc2
</s>
<s>
47	#num#	k4
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
Lisa	Lisa	k1gFnSc1
Su	Su	k?
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
Claflin	Claflin	k2eAgMnSc1d1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
představenstva	představenstvo	k1gNnSc2
<g/>
)	)	kIx)
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Rozsah	rozsah	k1gInSc1
působení	působení	k1gNnSc3
</s>
<s>
Celosvětově	celosvětově	k6eAd1
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
počítačový	počítačový	k2eAgInSc1d1
hardware	hardware	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
MikroprocesoryGrafické	MikroprocesoryGrafický	k2eAgInPc1d1
kartyČipsetyDTV	kartyČipsetyDTV	k?
decoder	decoder	k1gInSc1
chipsHandheld	chipsHandhelda	k1gFnPc2
media	medium	k1gNnSc2
chipsets	chipsets	k6eAd1
Obrat	obrat	k1gInSc1
</s>
<s>
9,8	9,8	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
6,5	6,5	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
1,4	1,4	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
631	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
2,5	2,5	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
341	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Celková	celkový	k2eAgNnPc1d1
aktiva	aktivum	k1gNnPc1
</s>
<s>
9	#num#	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
6	#num#	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
5,8	5,8	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
2,8	2,8	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
11	#num#	k4
400	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
SeaMicroATI	SeaMicroATI	k?
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
http://www.amd.com	http://www.amd.com	k1gInSc1
NYSE	NYSE	kA
</s>
<s>
NYSE	NYSE	kA
<g/>
:	:	kIx,
AMD	AMD	kA
LEI	lei	k1gInSc1
</s>
<s>
R2I72C950HOYXII45366	R2I72C950HOYXII45366	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
AMD	AMD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
hardwarová	hardwarový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
působící	působící	k2eAgFnSc1d1
na	na	k7c6
celosvětovém	celosvětový	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1969	#num#	k4
jako	jako	k8xC,k8xS
start-up	start-up	k1gMnSc1
v	v	k7c6
Silicon	Silicon	kA
Valley	Vallea	k1gFnSc2
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc2
s	s	k7c7
aktuálním	aktuální	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustřeďuje	soustřeďovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
vývoj	vývoj	k1gInSc4
procesorů	procesor	k1gInPc2
<g/>
,	,	kIx,
APU	APU	kA
<g/>
,	,	kIx,
GPU	GPU	kA
a	a	k8xC
čipsetů	čipset	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CPU	cpát	k5eAaImIp1nS
hlavně	hlavně	k9
x	x	k?
<g/>
86	#num#	k4
kompatibilní	kompatibilní	k2eAgInSc4d1
<g/>
,	,	kIx,
čipsety	čipset	k2eAgInPc4d1
pro	pro	k7c4
příslušné	příslušný	k2eAgFnPc4d1
CPU	CPU	kA
a	a	k8xC
GPU	GPU	kA
kompatibilní	kompatibilní	k2eAgMnSc1d1
DirectX	DirectX	k1gMnSc1
<g/>
,	,	kIx,
OpenGL	OpenGL	k1gMnSc1
<g/>
,	,	kIx,
OpenGL	OpenGL	k1gMnPc1
ES	es	k1gNnPc2
a	a	k8xC
OpenCL	OpenCL	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc4d1
podíl	podíl	k1gInSc4
firmy	firma	k1gFnSc2
Spansion	Spansion	k1gInSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
výrobce	výrobce	k1gMnSc1
flash	flasha	k1gFnPc2
pamětí	paměť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
rok	rok	k1gInSc4
2020	#num#	k4
vykázala	vykázat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
tržby	tržba	k1gFnSc2
9,76	9,76	k4
miliardy	miliarda	k4xCgFnSc2
amerických	americký	k2eAgMnPc2d1
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
než	než	k8xS
205	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
produktů	produkt	k1gInPc2
probíhá	probíhat	k5eAaImIp3nS
u	u	k7c2
externích	externí	k2eAgMnPc2d1
dodavatelů	dodavatel	k1gMnPc2
<g/>
:	:	kIx,
GPU	GPU	kA
vyrábí	vyrábět	k5eAaImIp3nS
TSMC	TSMC	kA
<g/>
,	,	kIx,
CPU	CPU	kA
vyrábí	vyrábět	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
GlobalFoundries	GlobalFoundriesa	k1gFnPc2
spolu	spolu	k6eAd1
se	s	k7c7
společností	společnost	k1gFnSc7
TSMC	TSMC	kA
a	a	k8xC
čipsety	čipseta	k1gFnSc2
vyrábí	vyrábět	k5eAaImIp3nS
GlobalFoundries	GlobalFoundries	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grafické	grafický	k2eAgFnPc4d1
karty	karta	k1gFnPc4
si	se	k3xPyFc3
vyrábí	vyrábět	k5eAaImIp3nS
každá	každý	k3xTgFnSc1
společnost	společnost	k1gFnSc1
podle	podle	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3
konkurenty	konkurent	k1gMnPc7
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
je	být	k5eAaImIp3nS
zejména	zejména	k9
společnost	společnost	k1gFnSc1
Intel	Intel	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
zejména	zejména	k9
procesory	procesor	k1gInPc4
a	a	k8xC
čipsety	čipset	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
nVidia	nVidium	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
zejména	zejména	k9
GPU	GPU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mobilním	mobilní	k2eAgInSc6d1
segmentu	segment	k1gInSc6
výrobci	výrobce	k1gMnSc3
SoC	soc	kA
čipů	čip	k1gInPc2
postavených	postavený	k2eAgInPc2d1
na	na	k7c6
architektuře	architektura	k1gFnSc6
ARM	ARM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
je	být	k5eAaImIp3nS
AMD	AMD	kA
2	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgFnSc7d3
společností	společnost	k1gFnSc7
zabývající	zabývající	k2eAgMnPc1d1
se	se	k3xPyFc4
vývojem	vývoj	k1gInSc7
procesorů	procesor	k1gInPc2
(	(	kIx(
<g/>
hlavně	hlavně	k9
x	x	k?
<g/>
86	#num#	k4
kompatibilní	kompatibilní	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nově	nově	k6eAd1
též	též	k9
procesory	procesor	k1gInPc1
ARM	ARM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
grafických	grafický	k2eAgInPc2d1
čipů	čip	k1gInPc2
a	a	k8xC
čipsetů	čipset	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Firma	firma	k1gFnSc1
AMD	AMD	kA
je	být	k5eAaImIp3nS
též	též	k9
významným	významný	k2eAgMnSc7d1
výrobcem	výrobce	k1gMnSc7
herních	herní	k2eAgInPc2d1
a	a	k8xC
konzolových	konzolový	k2eAgInPc2d1
APU	APU	kA
a	a	k8xC
GPU	GPU	kA
<g/>
,	,	kIx,
například	například	k6eAd1
pro	pro	k7c4
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
a	a	k8xC
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
a	a	k8xC
nové	nový	k2eAgFnSc3d1
nadcházející	nadcházející	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
a	a	k8xC
Xbox	Xbox	k1gInSc1
Series	Seriesa	k1gFnPc2
X	X	kA
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
,	,	kIx,
s	s	k7c7
novým	nový	k2eAgMnSc7d1
RDNA2	RDNA2	k1gMnSc7
a	a	k8xC
zen	zen	k2eAgInSc1d1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
AMD	AMD	kA
mezi	mezi	k7c7
12	#num#	k4
největšími	veliký	k2eAgInPc7d3
ve	v	k7c6
výrobě	výroba	k1gFnSc6
polovodičových	polovodičový	k2eAgFnPc2d1
součástek	součástka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
si	se	k3xPyFc3
polepšila	polepšit	k5eAaPmAgFnS
na	na	k7c4
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společnost	společnost	k1gFnSc1
AMD	AMD	kA
se	se	k3xPyFc4
dříve	dříve	k6eAd2
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
i	i	k9
vývoji	vývoj	k1gInSc3
zvukových	zvukový	k2eAgInPc2d1
čipů	čip	k1gInPc2
<g/>
,	,	kIx,
různých	různý	k2eAgInPc2d1
IO	IO	kA
čipů	čip	k1gInPc2
a	a	k8xC
vývoji	vývoj	k1gInSc6
vlastních	vlastní	k2eAgInPc2d1
čipsetů	čipset	k1gInPc2
pro	pro	k7c4
první	první	k4xOgInPc4
procesory	procesor	k1gInPc4
Athlon	Athlon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
obnově	obnova	k1gFnSc6
vývoje	vývoj	k1gInSc2
vlastních	vlastní	k2eAgMnPc2d1
čipsetů	čipset	k1gMnPc2
se	se	k3xPyFc4
rozhodlo	rozhodnout	k5eAaPmAgNnS
po	po	k7c4
odkoupení	odkoupení	k1gNnSc4
společnosti	společnost	k1gFnSc2
ATI	ATI	kA
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
zaměstnanců	zaměstnanec	k1gMnPc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
červen	červen	k1gInSc1
2011	#num#	k4
</s>
<s>
leden	leden	k1gInSc1
2010	#num#	k4
</s>
<s>
březen	březen	k1gInSc1
2009	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
Q2	Q2	k4
2007	#num#	k4
</s>
<s>
prosinec	prosinec	k1gInSc1
2006	#num#	k4
</s>
<s>
listopad	listopad	k1gInSc1
2005	#num#	k4
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
9,687	9,687	k4
</s>
<s>
11	#num#	k4
705	#num#	k4
</s>
<s>
11	#num#	k4
100	#num#	k4
</s>
<s>
10	#num#	k4
400	#num#	k4
</s>
<s>
14	#num#	k4
700	#num#	k4
</s>
<s>
15	#num#	k4
653	#num#	k4
</s>
<s>
16	#num#	k4
719	#num#	k4
</s>
<s>
16	#num#	k4
000	#num#	k4
</s>
<s>
18	#num#	k4
100	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
Vedení	vedení	k1gNnSc1
</s>
<s>
Lisa	Lisa	k1gFnSc1
Su	Su	k?
–	–	k?
Výkonná	výkonný	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
a	a	k8xC
CEO	CEO	kA
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devicesa	k1gFnPc2
</s>
<s>
Její	její	k3xOp3gFnSc1
kariéra	kariéra	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
Texas	Texas	kA
Instruments	Instruments	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
polovodičů	polovodič	k1gInPc2
a	a	k8xC
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
nástupem	nástup	k1gInSc7
k	k	k7c3
AMD	AMD	kA
pracovala	pracovat	k5eAaImAgFnS
12	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
IBM	IBM	kA
a	a	k8xC
posléze	posléze	k6eAd1
rv	rv	k?
roce	rok	k1gInSc6
2008	#num#	k4
v	v	k7c6
Freescale	Freescala	k1gFnSc6
Semiconductor	Semiconductor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
nastupuje	nastupovat	k5eAaImIp3nS
v	v	k7c6
AMD	AMD	kA
na	na	k7c6
pozici	pozice	k1gFnSc6
vice	vika	k1gFnSc6
prezidentky	prezidentka	k1gFnSc2
a	a	k8xC
provozní	provozní	k2eAgFnSc2d1
ředitelky	ředitelka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
nahrazuje	nahrazovat	k5eAaImIp3nS
dosavadního	dosavadní	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
Rory	Rora	k1gMnSc2
Reada	Read	k1gMnSc2
na	na	k7c4
pozici	pozice	k1gFnSc4
výkonné	výkonný	k2eAgFnSc2d1
ředitelky	ředitelka	k1gFnSc2
a	a	k8xC
CEO	CEO	kA
</s>
<s>
Chekib	Chekib	k1gMnSc1
Akrout	Akrout	k1gMnSc1
–	–	k?
senior	senior	k1gMnSc1
vicepresident	vicepresident	k1gMnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
divize	divize	k1gFnSc2
technologií	technologie	k1gFnPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
historie	historie	k1gFnSc1
ve	v	k7c4
společnosti	společnost	k1gFnPc4
AMD	AMD	kA
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
hlavním	hlavní	k2eAgMnSc7d1
úkol	úkol	k1gInSc4
je	být	k5eAaImIp3nS
dohlížet	dohlížet	k5eAaImF
na	na	k7c4
vývoj	vývoj	k1gInSc4
inovativních	inovativní	k2eAgFnPc2d1
a	a	k8xC
průlomových	průlomový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
dalších	další	k2eAgFnPc2d1
generací	generace	k1gFnPc2
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
platforem	platforma	k1gFnPc2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součást	součást	k1gFnSc1
úkolu	úkol	k1gInSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
taky	taky	k6eAd1
dohlížel	dohlížet	k5eAaImAgMnS
na	na	k7c4
vytváření	vytváření	k1gNnSc4
„	„	k?
<g/>
roadmap	roadmap	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
delší	dlouhý	k2eAgInPc4d2
časové	časový	k2eAgInPc4d1
úseky	úsek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
je	být	k5eAaImIp3nS
hlavně	hlavně	k9
okolo	okolo	k7c2
procesorů	procesor	k1gInPc2
<g/>
,	,	kIx,
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
IP	IP	kA
<g/>
/	/	kIx~
<g/>
Soc	soc	kA
atd.	atd.	kA
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
profesní	profesní	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
na	na	k7c6
vyšších	vysoký	k2eAgFnPc6d2
pozicích	pozice	k1gFnPc6
začala	začít	k5eAaPmAgFnS
ve	v	k7c4
společnosti	společnost	k1gFnPc4
IBM	IBM	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
pozici	pozice	k1gFnSc4
ředitele	ředitel	k1gMnSc2
divize	divize	k1gFnSc2
IBM	IBM	kA
Microelectronics	Microelectronics	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyvíjela	vyvíjet	k5eAaImAgFnS
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1
a	a	k8xC
širokopásmové	širokopásmový	k2eAgInPc4d1
mikroprocesory	mikroprocesor	k1gInPc4
na	na	k7c4
propojení	propojení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
vedl	vést	k5eAaImAgInS
vývoj	vývoj	k1gInSc4
procesoru	procesor	k1gInSc2
PowerPC	PowerPC	k1gFnSc2
používaného	používaný	k2eAgNnSc2d1
v	v	k7c6
počítačích	počítač	k1gInPc6
Apple	Apple	kA
Macintosh	Macintosh	kA
a	a	k8xC
v	v	k7c4
konzoly	konzola	k1gFnPc4
Nintendo	Nintendo	k6eAd1
GameCube	GameCub	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byl	být	k5eAaImAgInS
na	na	k7c6
pozici	pozice	k1gFnSc6
viceprezidenta	viceprezident	k1gMnSc2
vývoje	vývoj	k1gInSc2
procesorů	procesor	k1gInPc2
pro	pro	k7c4
zábavu	zábava	k1gFnSc4
a	a	k8xC
embedded	embedded	k1gInSc4
zaměření	zaměření	k1gNnSc2
a	a	k8xC
taky	taky	k6eAd1
vedl	vést	k5eAaImAgInS
vývoj	vývoj	k1gInSc1
procesoru	procesor	k1gInSc2
Cell	cello	k1gNnPc2
používaný	používaný	k2eAgInSc4d1
partnery	partner	k1gMnPc7
Sony	Sony	kA
<g/>
,	,	kIx,
Toshiba	Toshiba	kA
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
(	(	kIx(
<g/>
konzole	konzola	k1gFnSc3
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
)	)	kIx)
atd.	atd.	kA
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgInS
bakalářský	bakalářský	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
v	v	k7c6
oboru	obor	k1gInSc6
fyziky	fyzika	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
Pierre	Pierr	k1gInSc5
&	&	k?
Marie	Maria	k1gFnSc2
Curie	curie	k1gNnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
doktorský	doktorský	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
oboru	obor	k1gInSc6
fyziky	fyzika	k1gFnSc2
a	a	k8xC
elektroniky	elektronika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Harry	Harr	k1gInPc1
Wolin	Wolina	k1gFnPc2
–	–	k?
senior	senior	k1gMnSc1
vicepresident	vicepresident	k1gMnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
právní	právní	k2eAgMnSc1d1
poradce	poradce	k1gMnSc1
a	a	k8xC
tajemník	tajemník	k1gMnSc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
historie	historie	k1gFnSc1
ve	v	k7c4
společnosti	společnost	k1gFnPc4
AMD	AMD	kA
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
na	na	k7c6
pozici	pozice	k1gFnSc6
viceprezidenta	viceprezident	k1gMnSc2
divize	divize	k1gFnSc2
řešící	řešící	k2eAgNnPc4d1
intelektuální	intelektuální	k2eAgNnPc4d1
vlastnictví	vlastnictví	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
3	#num#	k4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
na	na	k7c6
nynější	nynější	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
hlavního	hlavní	k2eAgMnSc2d1
právního	právní	k2eAgMnSc2d1
poradce	poradce	k1gMnSc2
a	a	k8xC
tajemníka	tajemník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgInSc1d1
za	za	k7c4
všechny	všechen	k3xTgInPc4
právní	právní	k2eAgInPc4d1
kroky	krok	k1gInPc4
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
vyšetřování	vyšetřování	k1gNnSc1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
a	a	k8xC
veřejného	veřejný	k2eAgNnSc2d1
vystupování	vystupování	k1gNnSc2
<g/>
,	,	kIx,
styku	styk	k1gInSc2
s	s	k7c7
vládou	vláda	k1gFnSc7
a	a	k8xC
komunitou	komunita	k1gFnSc7
a	a	k8xC
vystupování	vystupování	k1gNnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
některých	některý	k3yIgFnPc2
velmi	velmi	k6eAd1
důležitých	důležitý	k2eAgNnPc6d1
rozhodnutích	rozhodnutí	k1gNnPc6
společnosti	společnost	k1gFnSc2
jako	jako	k8xC,k8xS
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
mezi	mezi	k7c7
AMD	AMD	kA
a	a	k8xC
společností	společnost	k1gFnSc7
IBM	IBM	kA
na	na	k7c6
společném	společný	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
výrobních	výrobní	k2eAgInPc2d1
procesů	proces	k1gInPc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
IO	IO	kA
<g/>
,	,	kIx,
dále	daleko	k6eAd2
třeba	třeba	k6eAd1
při	při	k7c6
odkoupení	odkoupení	k1gNnSc6
společnosti	společnost	k1gFnSc2
ATI	ATI	kA
nebo	nebo	k8xC
při	při	k7c6
právním	právní	k2eAgInSc6d1
sporu	spor	k1gInSc6
se	s	k7c7
společností	společnost	k1gFnSc7
Intel	Intel	kA
pro	pro	k7c4
monopolní	monopolní	k2eAgNnSc4d1
chování	chování	k1gNnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
Intelu	Intel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
pracování	pracování	k1gNnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
AMD	AMD	kA
je	být	k5eAaImIp3nS
taky	taky	k9
členem	člen	k1gMnSc7
správní	správní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
LifeWorkds	LifeWorkdsa	k1gFnPc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
neziskovou	ziskový	k2eNgFnSc4d1
organizaci	organizace	k1gFnSc4
v	v	k7c6
Austinu	Austin	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
State	status	k1gInSc5
Bars	Bars	k?
of	of	k?
Arizona	Arizona	k1gFnSc1
and	and	k?
Texas	Texas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
má	mít	k5eAaImIp3nS
registraci	registrace	k1gFnSc4
na	na	k7c4
vykonávání	vykonávání	k1gNnSc4
právníka	právník	k1gMnSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
profesní	profesní	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
na	na	k7c6
vyšších	vysoký	k2eAgFnPc6d2
pozicích	pozice	k1gFnPc6
začala	začít	k5eAaPmAgFnS
ve	v	k7c4
společnosti	společnost	k1gFnPc4
Motorola	Motorola	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
propracoval	propracovat	k5eAaPmAgInS
až	až	k9
na	na	k7c4
pozici	pozice	k1gFnSc4
viceprezidenta	viceprezident	k1gMnSc2
a	a	k8xC
ředitele	ředitel	k1gMnSc2
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
divize	divize	k1gFnSc2
polovodičových	polovodičový	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgInS
bakalářský	bakalářský	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
z	z	k7c2
oboru	obor	k1gInSc2
chemie	chemie	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Arizoně	Arizona	k1gFnSc6
a	a	k8xC
vystudoval	vystudovat	k5eAaPmAgInS
práva	právo	k1gNnPc4
na	na	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
získal	získat	k5eAaPmAgInS
ocenění	ocenění	k1gNnSc4
Magna	Magen	k2eAgFnSc1d1
Stella	Stella	k1gFnSc1
award	award	k6eAd1
za	za	k7c4
inovativní	inovativní	k2eAgNnSc4d1
spravování	spravování	k1gNnSc4
fóra	fórum	k1gNnSc2
Texas	Texas	kA
General	General	k1gMnSc1
Counsel	Counsel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
známé	známý	k2eAgNnSc1d1
bydliště	bydliště	k1gNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Austin	Austina	k1gFnPc2
ve	v	k7c6
státu	stát	k1gInSc6
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
sdílí	sdílet	k5eAaImIp3nS
domácnost	domácnost	k1gFnSc4
s	s	k7c7
manželkou	manželka	k1gFnSc7
Tracy	Traca	k1gFnSc2
a	a	k8xC
svými	svůj	k3xOyFgMnPc7
3	#num#	k4
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Vývojářská	vývojářský	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
</s>
<s>
AMD	AMD	kA
Submicron	Submicron	k1gMnSc1
Development	Development	k1gMnSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
SDC	SDC	kA
<g/>
)	)	kIx)
–	–	k?
centrum	centrum	k1gNnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
nejnovějších	nový	k2eAgFnPc2d3
technologií	technologie	k1gFnPc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Založen	založen	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1
investice	investice	k1gFnPc1
jsou	být	k5eAaImIp3nP
už	už	k6eAd1
přes	přes	k7c4
700	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
</s>
<s>
Hlavně	hlavně	k9
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
nové	nový	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
okolo	okolo	k7c2
částečně	částečně	k6eAd1
dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Spansion	Spansion	k1gInSc1
a	a	k8xC
to	ten	k3xDgNnSc1
technologií	technologie	k1gFnSc7
Flash	Flasha	k1gFnPc2
pamětí	paměť	k1gFnPc2
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
společností	společnost	k1gFnPc2
</s>
<s>
AMD	AMD	kA
Shanghai	Shangha	k1gFnPc1
Research	Research	k1gMnSc1
and	and	k?
Development	Development	k1gInSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
SRDC	SRDC	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pobočky	pobočka	k1gFnPc1
</s>
<s>
Původní	původní	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
firmy	firma	k1gFnSc2
ATI	ATI	kA
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
pobočka	pobočka	k1gFnSc1
firmy	firma	k1gFnSc2
AMD	AMD	kA
</s>
<s>
Kanada	Kanada	k1gFnSc1
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Markham	Markham	k1gInSc1
v	v	k7c4
Ontario	Ontario	k1gNnSc4
–	–	k?
AMD	AMD	kA
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
Commerce	Commerka	k1gFnSc3
Valley	Vallea	k1gFnSc2
Dr	dr	kA
<g/>
.	.	kIx.
East	East	k1gMnSc1
<g/>
,	,	kIx,
Markham	Markham	k1gInSc1
<g/>
,	,	kIx,
ON	on	k3xPp3gMnSc1
L3T	L3T	k1gFnSc7
7	#num#	k4
<g/>
X	X	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
Canada	Canada	k1gFnSc1
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
43.838	43.838	k4
<g/>
899	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-79.379	-79.379	k4
<g/>
713	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Markham	Markham	k1gInSc1
v	v	k7c4
Ontario	Ontario	k1gNnSc4
–	–	k?
AMD	AMD	kA
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
33	#num#	k4
Commerce	Commerka	k1gFnSc3
Valley	Vallea	k1gFnSc2
Dr	dr	kA
<g/>
.	.	kIx.
East	East	k1gMnSc1
<g/>
,	,	kIx,
Markham	Markham	k1gInSc1
<g/>
,	,	kIx,
ON	on	k3xPp3gMnSc1
L3T	L3T	k1gFnSc7
7	#num#	k4
<g/>
N	N	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
Canada	Canada	k1gFnSc1
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
43.839	43.839	k4
<g/>
778	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-79.378	-79.378	k4
<g/>
49	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc1
<g/>
:	:	kIx,
905-882-2600	905-882-2600	k4
a	a	k8xC
905-882-2620	905-882-2620	k4
</s>
<s>
Markham	Markham	k1gInSc1
v	v	k7c4
Ontario	Ontario	k1gNnSc4
–	–	k?
AMD	AMD	kA
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
10	#num#	k4
Commerce	Commerka	k1gFnSc3
Valley	Vallea	k1gFnSc2
Dr	dr	kA
<g/>
.	.	kIx.
East	East	k1gMnSc1
<g/>
,	,	kIx,
Markham	Markham	k1gInSc1
<g/>
,	,	kIx,
Ontario	Ontario	k1gNnSc1
L3T	L3T	k1gFnSc2
7	#num#	k4
<g/>
N	N	kA
<g/>
7	#num#	k4
<g/>
,	,	kIx,
Canada	Canada	k1gFnSc1
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
43.839	43.839	k4
<g/>
973	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-79.380	-79.380	k4
<g/>
351	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc1
<g/>
:	:	kIx,
905-882-2600	905-882-2600	k4
a	a	k8xC
905-882-2620	905-882-2620	k4
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
firmy	firma	k1gFnSc2
AMD	AMD	kA
v	v	k7c6
Sunnyvale	Sunnyvala	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
</s>
<s>
Pobočka	pobočka	k1gFnSc1
firmy	firma	k1gFnSc2
AMD	AMD	kA
LEED-certified	LEED-certified	k1gInSc1
v	v	k7c4
Austin	Austin	k1gInSc4
v	v	k7c6
Texasu	Texas	k1gInSc6
</s>
<s>
USA	USA	kA
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sunnyvale	Sunnyvale	k6eAd1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
One	One	k1gFnSc1
AMD	AMD	kA
Place	plac	k1gInSc6
<g/>
,	,	kIx,
P.	P.	kA
<g/>
O.	O.	kA
Box	box	k1gInSc4
3453	#num#	k4
<g/>
,	,	kIx,
Sunnyvale	Sunnyvala	k1gFnSc3
<g/>
,	,	kIx,
CA	ca	kA
<g/>
,	,	kIx,
94088-3453	94088-3453	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
37.386	37.386	k4
<g/>
266	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-121.999	-121.999	k4
<g/>
829	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
408-749-4000	408-749-4000	k4
</s>
<s>
Fort	Fort	k?
Collins	Collins	k1gInSc1
v	v	k7c4
Colorado	Colorado	k1gNnSc4
–	–	k?
AMD	AMD	kA
–	–	k?
R	R	kA
<g/>
&	&	k?
<g/>
D	D	kA
a	a	k8xC
Vzhled	vzhled	k1gInSc4
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
2950	#num#	k4
East	Eastum	k1gNnPc2
Harmony	Harmona	k1gFnSc2
Road	Roado	k1gNnPc2
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
300	#num#	k4
<g/>
,	,	kIx,
Fort	Fort	k?
Collins	Collins	k1gInSc1
<g/>
,	,	kIx,
CO	co	k6eAd1
<g/>
,	,	kIx,
80528-9558	80528-9558	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
40.523	40.523	k4
<g/>
568	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-105.036	-105.036	k4
<g/>
359	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
970-226-9500	970-226-9500	k4
</s>
<s>
Orlando	Orlando	k6eAd1
na	na	k7c6
Floridě	Florida	k1gFnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
R	R	kA
<g/>
&	&	k?
<g/>
D	D	kA
a	a	k8xC
Vzhled	vzhled	k1gInSc4
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
3501	#num#	k4
Quadrangle	Quadrangle	k1gNnPc2
Blvd	Blvda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
375	#num#	k4
<g/>
,	,	kIx,
Orlando	Orlanda	k1gFnSc5
<g/>
,	,	kIx,
FL	FL	kA
<g/>
,	,	kIx,
32817	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
28.601	28.601	k4
<g/>
146	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-81.214	-81.214	k4
<g/>
668	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc1
<g/>
:	:	kIx,
407-541-6800	407-541-6800	k4
a	a	k8xC
407-541-6801	407-541-6801	k4
</s>
<s>
Boxborough	Boxborough	k1gInSc1
v	v	k7c6
Massachusetts	Massachusetts	k1gNnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
R	R	kA
<g/>
&	&	k?
<g/>
D	D	kA
a	a	k8xC
Vzhled	vzhled	k1gInSc4
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
90	#num#	k4
Central	Central	k1gFnSc7
St	St	kA
<g/>
,	,	kIx,
Boxborough	Boxborough	k1gMnSc1
<g/>
,	,	kIx,
MA	MA	kA
01719	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
42.465	42.465	k4
<g/>
159	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-71.464	-71.464	k4
<g/>
705	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nashua	Nashua	k6eAd1
v	v	k7c6
New	New	k1gFnSc6
Hampshire	Hampshir	k1gInSc5
–	–	k?
AMD	AMD	kA
–	–	k?
R	R	kA
<g/>
&	&	k?
<g/>
D	D	kA
a	a	k8xC
Vzhled	vzhled	k1gInSc4
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
402	#num#	k4
Amhearst	Amhearst	k1gMnSc1
Street	Street	k1gMnSc1
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
304	#num#	k4
<g/>
,	,	kIx,
Nashua	Nashua	k1gFnSc1
<g/>
,	,	kIx,
NH	NH	kA
03063	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
42.707	42.707	k4
<g/>
974	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-71.452	-71.452	k4
<g/>
117	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austin	Austin	k1gInSc1
v	v	k7c6
Texasu	Texas	k1gInSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
Vývoz	vývoz	k1gInSc1
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
FedEx	FedEx	k1gInSc1
<g/>
/	/	kIx~
<g/>
DHL	DHL	kA
<g/>
)	)	kIx)
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
5900	#num#	k4
East	East	k2eAgInSc1d1
Ben	Ben	k1gInSc1
White	Whit	k1gInSc5
Blvd	Blvd	k1gMnSc1
<g/>
,	,	kIx,
Bldg	Bldg	k1gMnSc1
3	#num#	k4
<g/>
-Oltorf	-Oltorf	k1gInSc1
<g/>
,	,	kIx,
Austin	Austin	k1gInSc1
<g/>
,	,	kIx,
TX	TX	kA
78741	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
30.213	30.213	k4
<g/>
49	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-97.717	-97.717	k4
<g/>
193	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
512-602-5204	512-602-5204	k4
</s>
<s>
Austin	Austin	k1gInSc1
v	v	k7c6
Texasu	Texas	k1gInSc6
–	–	k?
AMD	AMD	kA
Austin	Austin	k1gInSc1
<g/>
,	,	kIx,
Lone	Lone	k1gInSc1
Star	Star	kA
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
7171	#num#	k4
Southwest	Southwest	k1gInSc1
Pkwy	Pkwa	k1gFnSc2
<g/>
,	,	kIx,
Austin	Austin	k1gMnSc1
<g/>
,	,	kIx,
TX	TX	kA
78735	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
30.251	30.251	k4
<g/>
436	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-97.863	-97.863	k4
<g/>
403	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
512-602-1000	512-602-1000	k4
</s>
<s>
Houston	Houston	k1gInSc1
v	v	k7c6
Texasu	Texas	k1gInSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
20333	#num#	k4
State	status	k1gInSc5
Highway	Highway	k1gInPc1
249	#num#	k4
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
320	#num#	k4
<g/>
,	,	kIx,
Houston	Houston	k1gInSc1
<g/>
,	,	kIx,
TX	TX	kA
77070	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
29.988	29.988	k4
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-95.573	-95.573	k4
<g/>
131	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bellevue	bellevue	k1gFnSc1
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
–	–	k?
AMD	AMD	kA
–	–	k?
R	R	kA
<g/>
&	&	k?
<g/>
D	D	kA
a	a	k8xC
Vzhled	vzhled	k1gInSc4
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
2002	#num#	k4
–	–	k?
156	#num#	k4
<g/>
th	th	k?
Ave	ave	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NE	Ne	kA
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
300	#num#	k4
<g/>
,	,	kIx,
Bellevue	bellevue	k1gFnSc1
<g/>
,	,	kIx,
WA	WA	kA
98007	#num#	k4
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
47.627	47.627	k4
<g/>
865	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-122.132	-122.132	k4
<g/>
4	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fax	fax	k1gInSc1
<g/>
:	:	kIx,
425-378-9460	425-378-9460	k4
</s>
<s>
Portland	Portland	k1gInSc1
v	v	k7c6
Oregonu	Oregon	k1gInSc6
–	–	k?
AMD	AMD	kA
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
902	#num#	k4
SW	SW	kA
Sixth	Sixth	k1gInSc4
Ave	ave	k1gNnSc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Mezzanine	Mezzanin	k1gInSc5
Floor	Floor	k1gInSc1
<g/>
,	,	kIx,
Portland	Portland	k1gInSc1
<g/>
,	,	kIx,
OR	OR	kA
97204	#num#	k4
</s>
<s>
Evropa	Evropa	k1gFnSc1
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Drážďany	Drážďany	k1gInPc1
v	v	k7c6
Německu	Německo	k1gNnSc6
–	–	k?
R	R	kA
<g/>
&	&	k?
<g/>
D	D	kA
Micro	Micro	k1gNnSc4
Devices	Devices	k1gInSc1
</s>
<s>
Camberley	Camberlea	k1gFnPc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
–	–	k?
AMD	AMD	kA
UK	UK	kA
Ltd	ltd	kA
<g/>
.	.	kIx.
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Coliseum	Coliseum	k1gInSc1
<g/>
,	,	kIx,
Riverside	Riversid	k1gMnSc5
Way	Way	k1gMnSc5
<g/>
,	,	kIx,
Watchmoor	Watchmoor	k1gInSc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
Camberley	Camberley	k1gInPc1
<g/>
,	,	kIx,
Surrey	Surrey	k1gInPc1
<g/>
,	,	kIx,
GU15	GU15	k1gFnSc1
3YL	3YL	k4
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
+	+	kIx~
44	#num#	k4
1276	#num#	k4
803100	#num#	k4
a	a	k8xC
+	+	kIx~
44	#num#	k4
1276	#num#	k4
684127	#num#	k4
</s>
<s>
Miláno	Milán	k2eAgNnSc1d1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
–	–	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
Spa	Spa	k1gMnSc1
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Via	via	k7c4
Montefeltro	Montefeltro	k1gNnSc4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
20156	#num#	k4
Milano	Milana	k1gFnSc5
Italy	Ital	k1gMnPc4
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
+	+	kIx~
39	#num#	k4
02	#num#	k4
3008161	#num#	k4
a	a	k8xC
+	+	kIx~
39	#num#	k4
02	#num#	k4
33497689	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
–	–	k?
AMD	AMD	kA
International	International	k1gMnSc1
Sales	Sales	k1gMnSc1
and	and	k?
Services	Services	k1gMnSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Presnenskiy	Presnenskia	k1gFnSc2
val	val	k1gInSc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
,	,	kIx,
Moscow	Moscow	k1gFnSc1
<g/>
,	,	kIx,
Russia	Russia	k1gFnSc1
123557	#num#	k4
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
+	+	kIx~
7	#num#	k4
495	#num#	k4
726	#num#	k4
55	#num#	k4
05	#num#	k4
a	a	k8xC
+	+	kIx~
7	#num#	k4
495	#num#	k4
726	#num#	k4
55	#num#	k4
04	#num#	k4
</s>
<s>
Mnichov	Mnichov	k1gInSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
–	–	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
GmbH	GmbH	k1gMnSc1
–	–	k?
Obchod	obchod	k1gInSc1
a	a	k8xC
prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Einsteinring	Einsteinring	k1gInSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
,	,	kIx,
85609	#num#	k4
Dornach	Dornacha	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Aschheim	Aschheim	k1gInSc1
<g/>
,	,	kIx,
Munich	Munich	k1gInSc1
<g/>
,	,	kIx,
Germany	German	k1gInPc1
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
+	+	kIx~
49	#num#	k4
89	#num#	k4
450	#num#	k4
530	#num#	k4
a	a	k8xC
+	+	kIx~
49	#num#	k4
89	#num#	k4
406	#num#	k4
490	#num#	k4
</s>
<s>
Paříž	Paříž	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
121	#num#	k4
rue	rue	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Aguesseau	Aguesseaus	k1gInSc2
<g/>
,	,	kIx,
92100	#num#	k4
Boulogne-Billancourt	Boulogne-Billancourta	k1gFnPc2
<g/>
,	,	kIx,
France	Franc	k1gMnPc4
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
+	+	kIx~
33	#num#	k4
1	#num#	k4
41	#num#	k4
03	#num#	k4
15	#num#	k4
51	#num#	k4
</s>
<s>
Varšava	Varšava	k1gFnSc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
ul	ul	kA
<g/>
.	.	kIx.
Grochowska	Grochowska	k1gFnSc1
341	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
03-838	03-838	k4
Warszawa	Warszawum	k1gNnSc2
<g/>
,	,	kIx,
Poland	Poland	k1gInSc1
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
+	+	kIx~
48	#num#	k4
22	#num#	k4
698	#num#	k4
85	#num#	k4
46	#num#	k4
a	a	k8xC
+	+	kIx~
48	#num#	k4
22	#num#	k4
698	#num#	k4
85	#num#	k4
46	#num#	k4
</s>
<s>
Asie	Asie	k1gFnSc1
a	a	k8xC
Tichomoří	Tichomoří	k1gNnSc1
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sydney	Sydney	k1gNnSc4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
–	–	k?
AMD	AMD	kA
Far	fara	k1gFnPc2
East	Easta	k1gFnPc2
Ltd	ltd	kA
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Level	level	k1gInSc1
9	#num#	k4
<g/>
,	,	kIx,
123	#num#	k4
Epping	Epping	k1gInSc1
Road	Road	k1gInSc4
<g/>
,	,	kIx,
North	North	k1gInSc4
Ryde	Ryd	k1gInSc2
NSW	NSW	kA
2113	#num#	k4
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
(	(	kIx(
<g/>
+	+	kIx~
<g/>
61	#num#	k4
<g/>
)	)	kIx)
2	#num#	k4
8877	#num#	k4
7222	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Obchod	obchod	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
19	#num#	k4
<g/>
/	/	kIx~
<g/>
F	F	kA
North	North	k1gInSc1
Building	Building	k1gInSc1
<g/>
,	,	kIx,
Raycom	Raycom	k1gInSc1
Infotech	Infot	k1gInPc6
Park	park	k1gInSc4
Tower	Towra	k1gFnPc2
C	C	kA
<g/>
,	,	kIx,
No	no	k9
<g/>
.	.	kIx.
2	#num#	k4
Science	Science	k1gFnSc1
Institute	institut	k1gInSc5
South	South	k1gInSc4
Rd	Rd	k1gFnPc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Zhong	Zhong	k1gMnSc1
Guan	Guan	k1gMnSc1
Cun	Cun	k1gMnSc1
<g/>
,	,	kIx,
Haidian	Haidian	k1gMnSc1
Dist	Dist	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tel	tel	kA
<g/>
:	:	kIx,
86-10-82861888	86-10-82861888	k4
a	a	k8xC
Fax	fax	k1gInSc1
<g/>
:	:	kIx,
86-10-82861999	86-10-82861999	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
+	+	kIx~
Karibik	Karibik	k1gInSc1
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
v	v	k7c6
Argentině	Argentina	k1gFnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Av	Av	k1gFnSc1
<g/>
.	.	kIx.
del	del	k?
Libertador	Libertador	k1gInSc1
602	#num#	k4
<g/>
,	,	kIx,
piso	piso	k6eAd1
16	#num#	k4
<g/>
,	,	kIx,
C1001ABT	C1001ABT	k1gMnSc1
–	–	k?
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
-34.588	-34.588	k4
<g/>
704	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-58.381	-58.381	k4
<g/>
97	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
(	(	kIx(
<g/>
54	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
4877-6400	4877-6400	k4
a	a	k8xC
(	(	kIx(
<g/>
54	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
4877-6416	4877-6416	k4
</s>
<s>
Sã	Sã	k1gFnSc5
Paulo	Paula	k1gFnSc5
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
–	–	k?
AMD	AMD	kA
South	South	k1gMnSc1
America	America	k1gMnSc1
Ltda	Ltda	k1gMnSc1
<g/>
.	.	kIx.
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Av	Av	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luis	Luisa	k1gFnPc2
Carlos	Carlos	k1gMnSc1
Berrini	Berrin	k1gMnPc1
<g/>
,	,	kIx,
1645	#num#	k4
–	–	k?
10	#num#	k4
<g/>
º	º	k?
andar	andara	k1gFnPc2
–	–	k?
Conjunto	Conjunto	k1gNnSc1
101	#num#	k4
<g/>
,	,	kIx,
Brooklin	Brooklin	k1gInSc1
–	–	k?
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
–	–	k?
SP	SP	kA
<g/>
,	,	kIx,
Cep	cep	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
0	#num#	k4
<g/>
4571	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
11	#num#	k4
<g/>
,	,	kIx,
Brasil	Brasil	k1gFnSc1
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
-23.611	-23.611	k4
<g/>
18	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-46.694	-46.694	k4
<g/>
853	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc4
<g/>
:	:	kIx,
(	(	kIx(
<g/>
55	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
3478-2150	3478-2150	k4
a	a	k8xC
(	(	kIx(
<g/>
55	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
3478-2200	3478-2200	k4
</s>
<s>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
–	–	k?
AMD	AMD	kA
–	–	k?
Prodej	prodej	k1gInSc1
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Blvd	Blvd	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manuel	Manuel	k1gMnSc1
Ávila	Ávila	k1gMnSc1
Camacho	Camacha	k1gFnSc5
No	no	k9
<g/>
.	.	kIx.
40	#num#	k4
<g/>
,	,	kIx,
Torre	torr	k1gInSc5
Esmeralda	Esmeralda	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Piso	Pisa	k1gFnSc5
18	#num#	k4
<g/>
,	,	kIx,
Col	cola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lomas	Lomas	k1gMnSc1
de	de	k?
Chapultepec	Chapultepec	k1gMnSc1
<g/>
,	,	kIx,
México	México	k1gMnSc1
DF	DF	kA
<g/>
,	,	kIx,
CP	CP	kA
11000	#num#	k4
–	–	k?
México	México	k1gMnSc1
</s>
<s>
GPS	GPS	kA
<g/>
:	:	kIx,
19.408	19.408	k4
<g/>
73	#num#	k4
<g/>
°	°	k?
N	N	kA
<g/>
,	,	kIx,
-99.194	-99.194	k4
<g/>
442	#num#	k4
<g/>
°	°	k?
E	E	kA
</s>
<s>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc1
<g/>
:	:	kIx,
52-55-9138-0240	52-55-9138-0240	k4
a	a	k8xC
52-55-5520-9113	52-55-5520-9113	k4
</s>
<s>
Střední	střední	k2eAgInSc1d1
východ	východ	k1gInSc1
a	a	k8xC
Afrika	Afrika	k1gFnSc1
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dubaj	Dubaj	k1gInSc1
v	v	k7c6
UAE	UAE	kA
–	–	k?
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Adresa	adresa	k1gFnSc1
<g/>
:	:	kIx,
Emaar	Emaar	k1gInSc1
Business	business	k1gInSc1
Park	park	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
304	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
rd	rd	k?
Floor	Floora	k1gFnPc2
<g/>
,	,	kIx,
Dubai	Dubai	k1gNnSc2
<g/>
,	,	kIx,
United	United	k1gMnSc1
Arab	Arab	k1gMnSc1
Emirates	Emirates	k1gMnSc1
</s>
<s>
Tel	tel	kA
a	a	k8xC
fax	fax	k1gInSc1
<g/>
:	:	kIx,
+971	+971	k4
4	#num#	k4
427	#num#	k4
2626	#num#	k4
a	a	k8xC
+971	+971	k4
4	#num#	k4
427	#num#	k4
2627	#num#	k4
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2011	#num#	k4
na	na	k7c4
FDS	FDS	kA
(	(	kIx(
<g/>
Fusion	Fusion	k1gInSc1
Developer	developer	k1gMnSc1
Summit	summit	k1gInSc1
<g/>
)	)	kIx)
Jem	Jem	k?
Davies	Davies	k1gInSc1
ohlásil	ohlásit	k5eAaPmAgInS
jménem	jméno	k1gNnSc7
společnosti	společnost	k1gFnSc2
ARM	ARM	kA
spolupráci	spolupráce	k1gFnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
AMD	AMD	kA
na	na	k7c6
podpoře	podpora	k1gFnSc6
a	a	k8xC
rozvoji	rozvoj	k1gInSc3
API	API	kA
OpenCL	OpenCL	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Server	server	k1gInSc1
SemiAccurate	SemiAccurat	k1gInSc5
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
IT	IT	kA
informoval	informovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
AMD	AMD	kA
a	a	k8xC
Amkor	Amkor	k1gMnSc1
spolupracují	spolupracovat	k5eAaImIp3nP
na	na	k7c6
použití	použití	k1gNnSc6
technologií	technologie	k1gFnPc2
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
integraci	integrace	k1gFnSc4
přídavných	přídavný	k2eAgInPc2d1
čipů	čip	k1gInPc2
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
grafický	grafický	k2eAgInSc4d1
čip	čip	k1gInSc4
<g/>
,	,	kIx,
přímo	přímo	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
technologie	technologie	k1gFnPc4
TSV	TSV	kA
(	(	kIx(
<g/>
Through	Through	k1gInSc1
Silicon	Silicon	kA
Via	via	k7c4
<g/>
)	)	kIx)
a	a	k8xC
Silicon	Silicon	kA
interposer	interposer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
umožnit	umožnit	k5eAaPmF
do	do	k7c2
budoucna	budoucno	k1gNnSc2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
integrovat	integrovat	k5eAaBmF
na	na	k7c6
GPU	GPU	kA
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
grafickou	grafický	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
pro	pro	k7c4
snížení	snížení	k1gNnSc4
spotřeby	spotřeba	k1gFnSc2
v	v	k7c6
mobilním	mobilní	k2eAgInSc6d1
a	a	k8xC
konzolovém	konzolový	k2eAgInSc6d1
segmentu	segment	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
firmou	firma	k1gFnSc7
IBM	IBM	kA
<g/>
,	,	kIx,
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
její	její	k3xOp3gNnSc1
vývoj	vývoj	k1gInSc1
výrobních	výrobní	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
technologií	technologie	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
CPU	CPU	kA
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Finance	finance	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Finanční	finanční	k2eAgNnSc1d1
hospodaření	hospodaření	k1gNnSc1
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s>
Finance	finance	k1gFnPc1
za	za	k7c4
rok	rok	k1gInSc4
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Obrat	obrat	k1gInSc1
<g/>
(	(	kIx(
<g/>
Celkové	celkový	k2eAgInPc1d1
příjmy	příjem	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Příjem	příjem	k1gInSc1
–	–	k?
Hrubý	hrubý	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
Příjem	příjem	k1gInSc1
–	–	k?
Čistý	čistý	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
Provozní	provozní	k2eAgInPc1d1
výnosy	výnos	k1gInPc1
<g/>
(	(	kIx(
<g/>
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přímé	přímý	k2eAgInPc1d1
náklady	náklad	k1gInPc1
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c4
vývoj	vývoj	k1gInSc4
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
▲	▲	k?
6,5	6,5	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
471	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
848	#num#	k4
miliónů	milión	k4xCgInPc2
USD	USD	kA
</s>
<s>
▼	▼	k?
5	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
1	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
2010	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
▼	▼	k?
5,4	5,4	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
293	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
<g/>
(	(	kIx(
<g/>
▲	▲	k?
301	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
▲	▲	k?
664	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
9,1	9,1	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
648	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
</s>
<s>
2009	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
▼	▼	k?
5,8	5,8	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
-3,1	-3,1	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
-2	-2	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▼	▼	k?
7,7	7,7	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▼	▼	k?
-82	-82	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
</s>
<s>
2008	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
▲	▲	k?
6	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▼	▼	k?
2,3	2,3	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▼	▼	k?
-3,4	-3,4	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
-2,9	-2,9	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
3,8	3,8	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
1,9	1,9	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
11,6	11,6	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
3	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
2007	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
▼	▼	k?
5,7	5,7	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
</s>
<s>
▲	▲	k?
2,8	2,8	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
</s>
<s>
▲	▲	k?
166	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
<g/>
(	(	kIx(
<g/>
▼	▼	k?
-166	-166	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
▼	▼	k?
2,9	2,9	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
▲	▲	k?
1,2	1,2	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
2006	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
▲	▲	k?
5,9	5,9	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2,4	2,4	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
166	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3,5	3,5	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
1,1	1,1	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
</s>
<s>
2005	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
5	#num#	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
</s>
<s>
2004	#num#	k4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Obrat	obrat	k1gInSc1
<g/>
(	(	kIx(
<g/>
Celkové	celkový	k2eAgInPc1d1
příjmy	příjem	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Příjem	příjem	k1gInSc1
–	–	k?
Hrubý	hrubý	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
Příjem	příjem	k1gInSc1
–	–	k?
Čistý	čistý	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
Provozní	provozní	k2eAgInPc1d1
výnosy	výnos	k1gInPc1
<g/>
(	(	kIx(
<g/>
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přímé	přímý	k2eAgInPc1d1
náklady	náklad	k1gInPc1
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c4
vývoj	vývoj	k1gInSc4
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Podíl	podíl	k1gInSc1
na	na	k7c6
trhu	trh	k1gInSc6
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
dodaných	dodaná	k1gFnPc2
GPU	GPU	kA
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
KvartálRok	KvartálRok	k1gInSc1
</s>
<s>
Q22011	Q22011	k4
</s>
<s>
Q12011	Q12011	k4
</s>
<s>
Q22010	Q22010	k4
</s>
<s>
KvartálRok	KvartálRok	k1gInSc1
</s>
<s>
AMD	AMD	kA
</s>
<s>
28	#num#	k4
950	#num#	k4
</s>
<s>
31	#num#	k4
221	#num#	k4
</s>
<s>
29	#num#	k4
975	#num#	k4
</s>
<s>
AMD	AMD	kA
</s>
<s>
24,1	24,1	k4
%	%	kIx~
</s>
<s>
24	#num#	k4
%	%	kIx~
</s>
<s>
24,5	24,5	k4
%	%	kIx~
</s>
<s>
Intel	Intel	kA
</s>
<s>
66	#num#	k4
450	#num#	k4
</s>
<s>
73	#num#	k4
300	#num#	k4
</s>
<s>
66	#num#	k4
400	#num#	k4
</s>
<s>
Intel	Intel	kA
</s>
<s>
55,4	55,4	k4
%	%	kIx~
</s>
<s>
56,3	56,3	k4
%	%	kIx~
</s>
<s>
54,3	54,3	k4
%	%	kIx~
</s>
<s>
Matrox	Matrox	k1gInSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
Matrox	Matrox	k1gInSc1
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
NVIDIA	NVIDIA	kA
</s>
<s>
23	#num#	k4
845	#num#	k4
</s>
<s>
24	#num#	k4
875	#num#	k4
</s>
<s>
24	#num#	k4
225	#num#	k4
</s>
<s>
NVIDIA	NVIDIA	kA
</s>
<s>
19,9	19,9	k4
%	%	kIx~
</s>
<s>
19,1	19,1	k4
%	%	kIx~
</s>
<s>
19,8	19,8	k4
%	%	kIx~
</s>
<s>
SIS	SIS	kA
<g/>
/	/	kIx~
<g/>
XGI	XGI	kA
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
880	#num#	k4
</s>
<s>
SIS	SIS	kA
<g/>
/	/	kIx~
<g/>
XGI	XGI	kA
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
0,7	0,7	k4
%	%	kIx~
</s>
<s>
VIA	via	k7c4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
3	#num#	k4
</s>
<s>
750	#num#	k4
</s>
<s>
880	#num#	k4
</s>
<s>
840	#num#	k4
</s>
<s>
VIA	via	k7c4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
3	#num#	k4
</s>
<s>
0,5	0,5	k4
%	%	kIx~
</s>
<s>
0,7	0,7	k4
%	%	kIx~
</s>
<s>
0,7	0,7	k4
%	%	kIx~
</s>
<s>
Další	další	k2eAgFnSc1d1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Další	další	k2eAgFnSc1d1
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
120	#num#	k4
020	#num#	k4
</s>
<s>
130	#num#	k4
276	#num#	k4
</s>
<s>
122	#num#	k4
370	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
100	#num#	k4
%	%	kIx~
</s>
<s>
100	#num#	k4
%	%	kIx~
</s>
<s>
100	#num#	k4
%	%	kIx~
</s>
<s>
KvartálRok	KvartálRok	k1gInSc1
</s>
<s>
Q22011	Q22011	k4
</s>
<s>
Q12011	Q12011	k4
</s>
<s>
Q22010	Q22010	k4
</s>
<s>
KvartálRok	KvartálRok	k1gInSc1
</s>
<s>
Produkty	produkt	k1gInPc1
</s>
<s>
Procesory	procesor	k1gInPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Procesory	procesor	k1gInPc1
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s>
Firma	firma	k1gFnSc1
AMD	AMD	kA
vyvíjí	vyvíjet	k5eAaImIp3nS
procesory	procesor	k1gInPc4
<g/>
,	,	kIx,
aktuálně	aktuálně	k6eAd1
řady	řada	k1gFnPc1
Ryzen	ryzna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nahradily	nahradit	k5eAaPmAgFnP
zastaralé	zastaralý	k2eAgNnSc4d1
FX	FX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ryzeny	Ryzen	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
srovnatelné	srovnatelný	k2eAgInPc1d1
s	s	k7c7
konkurenčními	konkurenční	k2eAgInPc7d1
procesory	procesor	k1gInPc7
od	od	k7c2
Intelu	Intel	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
některých	některý	k3yIgInPc6
parametrech	parametr	k1gInPc6
u	u	k7c2
vyšších	vysoký	k2eAgFnPc2d2
řad	řada	k1gFnPc2
jsou	být	k5eAaImIp3nP
i	i	k9
znatelně	znatelně	k6eAd1
výkonnější	výkonný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvíjí	vyvíjet	k5eAaImIp3nS
také	také	k9
nové	nový	k2eAgInPc1d1
APU	APU	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
socketu	socket	k1gInSc6
AM4	AM4	k1gFnSc2
s	s	k7c7
nahradil	nahradit	k5eAaPmAgMnS
tak	tak	k9
starší	starší	k1gMnSc1
FM	FM	kA
<g/>
2	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
AM	AM	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnešní	dnešní	k2eAgInPc1d1
procesory	procesor	k1gInPc1
Ryzen	ryzna	k1gFnPc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnPc1
jsou	být	k5eAaImIp3nP
vyráběny	vyrábět	k5eAaImNgFnP
12	#num#	k4
<g/>
nm	nm	k?
výrobní	výrobní	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc4
předchůdci	předchůdce	k1gMnPc1
byly	být	k5eAaImAgInP
procesory	procesor	k1gInPc1
Ryzen	ryzna	k1gFnPc2
1	#num#	k4
<g/>
.	.	kIx.
<g/>
generace	generace	k1gFnPc1
vyráběny	vyráběn	k2eAgFnPc1d1
14	#num#	k4
<g/>
nm	nm	k?
výrobní	výrobní	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
<g/>
,	,	kIx,
kolem	kolem	k7c2
června	červen	k1gInSc2
<g/>
–	–	k?
<g/>
srpna	srpen	k1gInSc2
2019	#num#	k4
vyjdou	vyjít	k5eAaPmIp3nP
také	také	k9
procesory	procesor	k1gInPc1
Ryzen	ryzna	k1gFnPc2
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnPc1
vyráběny	vyráběn	k2eAgFnPc1d1
7	#num#	k4
<g/>
nm	nm	k?
výrobní	výrobní	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Grafické	grafický	k2eAgInPc1d1
čipy	čip	k1gInPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
AMD	AMD	kA
Radeon	Radeon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
AMD	AMD	kA
vyvíjí	vyvíjet	k5eAaImIp3nS
grafické	grafický	k2eAgInPc4d1
čipy	čip	k1gInPc4
Radeon	Radeona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
jsou	být	k5eAaImIp3nP
osazovány	osazován	k2eAgInPc1d1
na	na	k7c4
grafické	grafický	k2eAgFnPc4d1
karty	karta	k1gFnPc4
řady	řada	k1gFnSc2
Radeon	Radeona	k1gFnPc2
a	a	k8xC
FireGL	FireGL	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radeon	Radeon	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
spotřební	spotřební	k2eAgInSc4d1
segment	segment	k1gInSc4
<g/>
,	,	kIx,
FireGL	FireGL	k1gFnSc1
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
profi	profi	k6eAd1
grafickém	grafický	k2eAgInSc6d1
segmentu	segment	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čipy	čip	k1gInPc7
podporují	podporovat	k5eAaImIp3nP
rozhraní	rozhraní	k1gNnSc2
PCI	PCI	kA
Express	express	k1gInSc1
x	x	k?
<g/>
16	#num#	k4
3.0	3.0	k4
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
operuje	operovat	k5eAaImIp3nS
v	v	k7c6
celém	celý	k2eAgNnSc6d1
portfoliu	portfolio	k1gNnSc6
grafických	grafický	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
nižší	nízký	k2eAgFnSc2d2
třídy	třída	k1gFnSc2
přes	přes	k7c4
Střední	střední	k2eAgFnSc4d1
a	a	k8xC
vyšší	vysoký	k2eAgFnSc4d2
třídu	třída	k1gFnSc4
až	až	k9
po	po	k7c4
Serverový	serverový	k2eAgInSc4d1
a	a	k8xC
firemní	firemní	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc4
celkové	celkový	k2eAgNnSc4d1
obsazení	obsazení	k1gNnSc4
spotřebitelského	spotřebitelský	k2eAgInSc2d1
trhu	trh	k1gInSc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
21	#num#	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
u	u	k7c2
svých	svůj	k3xOyFgFnPc2
grafických	grafický	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
nejmodernější	moderní	k2eAgFnPc4d3
technologie	technologie	k1gFnPc4
od	od	k7c2
pamětí	paměť	k1gFnPc2
DDR3	DDR3	k1gFnSc2
(	(	kIx(
<g/>
nižší	nízký	k2eAgFnSc1d2
třída	třída	k1gFnSc1
<g/>
)	)	kIx)
přes	přes	k7c4
GDDR5	GDDR5	k1gFnSc4
až	až	k9
po	po	k7c6
nové	nový	k2eAgFnSc6d1
HBM	HBM	kA
dnes	dnes	k6eAd1
již	již	k6eAd1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
generaci	generace	k1gFnSc6
(	(	kIx(
<g/>
HBM	HBM	kA
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
GDDR6	GDDR6	k1gFnSc1
u	u	k7c2
GPU	GPU	kA
s	s	k7c7
označením	označení	k1gNnSc7
AMD	AMD	kA
Navi	Nav	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výrobci	výrobce	k1gMnPc1
grafických	grafický	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
</s>
<s>
Asus	Asus	k6eAd1
</s>
<s>
Gainward	Gainward	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
ukončili	ukončit	k5eAaPmAgMnP
spolupráci	spolupráce	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Gigabyte	Gigabyt	k1gInSc5
Technology	technolog	k1gMnPc7
</s>
<s>
Micro-Star	Micro-Star	k1gMnSc1
International	International	k1gMnSc1
(	(	kIx(
<g/>
MSI	MSI	kA
<g/>
)	)	kIx)
</s>
<s>
Powercolor	Powercolor	k1gMnSc1
</s>
<s>
Sapphire	Sapphir	k1gMnSc5
–	–	k?
vždy	vždy	k6eAd1
osazoval	osazovat	k5eAaImAgInS
pouze	pouze	k6eAd1
GPU	GPU	kA
od	od	k7c2
firmy	firma	k1gFnSc2
AMD	AMD	kA
(	(	kIx(
<g/>
srpen	srpen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
XFX	XFX	kA
</s>
<s>
HIS	his	k1gNnSc1
</s>
<s>
ASrock	ASrock	k6eAd1
</s>
<s>
Biostar	Biostar	k1gMnSc1
</s>
<s>
ColorFire	ColorFir	k1gMnSc5
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dataland	Dataland	k1gInSc1
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kurotoshiku	Kurotoshika	k1gFnSc4
</s>
<s>
Maxsun	Maxsun	k1gNnSc1
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
VisionTek	VisionTek	k6eAd1
</s>
<s>
Pradeon	Pradeon	k1gNnSc1
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Yeston	Yeston	k1gInSc1
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
HUANANZHI	HUANANZHI	kA
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čipsety	Čipseta	k1gFnPc1
</s>
<s>
S	s	k7c7
koupením	koupení	k1gNnSc7
ATI	ATI	kA
získala	získat	k5eAaPmAgFnS
i	i	k9
čipsety	čipset	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
nadále	nadále	k6eAd1
vyvíjí	vyvíjet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnovější	nový	k2eAgFnSc1d3
řada	řada	k1gFnSc1
je	být	k5eAaImIp3nS
X570	X570	k1gFnSc1
"	"	kIx"
<g/>
Valhalla	Valhalla	k1gFnSc1
<g/>
"	"	kIx"
série	série	k1gFnSc1
pro	pro	k7c4
patici	patice	k1gFnSc4
AM4	AM4	k1gFnSc2
(	(	kIx(
<g/>
Socket	Socket	k1gInSc1
1331	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
vyvíjela	vyvíjet	k5eAaImAgFnS
čipsety	čipset	k1gMnPc4
i	i	k9
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
vývoj	vývoj	k1gInSc4
ukončila	ukončit	k5eAaPmAgFnS
s	s	k7c7
příchodem	příchod	k1gInSc7
Socket	Socketa	k1gFnPc2
A.	A.	kA
</s>
<s>
Operační	operační	k2eAgFnPc1d1
paměti	paměť	k1gFnPc1
</s>
<s>
Společnost	společnost	k1gFnSc1
si	se	k3xPyFc3
nechala	nechat	k5eAaPmAgFnS
vyrobit	vyrobit	k5eAaPmF
neurčitý	určitý	k2eNgInSc4d1
menší	malý	k2eAgInSc4d2
počet	počet	k1gInSc4
operačních	operační	k2eAgFnPc2d1
pamětí	paměť	k1gFnPc2
s	s	k7c7
paměťovými	paměťový	k2eAgInPc7d1
čipy	čip	k1gInPc7
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
logem	logo	k1gNnSc7
pro	pro	k7c4
OEM	OEM	kA
segment	segment	k1gInSc1
partnerů	partner	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěla	chtít	k5eAaImAgFnS
údajně	údajně	k6eAd1
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
jestli	jestli	k8xS
by	by	kYmCp3nS
o	o	k7c4
ně	on	k3xPp3gMnPc4
byl	být	k5eAaImAgInS
zájem	zájem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
pár	pár	k4xCyI
pamětí	paměť	k1gFnPc2
dostalo	dostat	k5eAaPmAgNnS
na	na	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovala	existovat	k5eAaImAgFnS
i	i	k9
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
stažena	stáhnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvolněny	uvolněn	k2eAgInPc1d1
byly	být	k5eAaImAgInP
moduly	modul	k1gInPc1
o	o	k7c6
velikosti	velikost	k1gFnSc6
2	#num#	k4
GiB	GiB	k1gFnPc2
a	a	k8xC
frekvenci	frekvence	k1gFnSc4
1066	#num#	k4
<g/>
,	,	kIx,
1333	#num#	k4
a	a	k8xC
1600	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
uvést	uvést	k5eAaPmF
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
logem	logo	k1gNnSc7
i	i	k9
volně	volně	k6eAd1
do	do	k7c2
prodeje	prodej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čipy	čip	k1gInPc7
nesou	nést	k5eAaImIp3nP
logo	logo	k1gNnSc4
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
samotná	samotný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
operačních	operační	k2eAgFnPc2d1
pamětí	paměť	k1gFnPc2
probíhá	probíhat	k5eAaImIp3nS
u	u	k7c2
dvou	dva	k4xCgFnPc2
společností	společnost	k1gFnPc2
Patriot	patriot	k1gMnSc1
a	a	k8xC
VisionTek	VisionTek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměti	paměť	k1gFnPc1
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
třech	tři	k4xCgInPc2
výkonnostních	výkonnostní	k2eAgFnPc2d1
řad	řada	k1gFnPc2
<g/>
:	:	kIx,
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
Performance	performance	k1gFnSc1
a	a	k8xC
Radeon	Radeon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
řady	řada	k1gFnPc1
mají	mít	k5eAaImIp3nP
velikost	velikost	k1gFnSc4
2	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
nebo	nebo	k8xC
8	#num#	k4
GB	GB	kA
<g/>
,	,	kIx,
nepodporuji	podporovat	k5eNaImIp1nS
ECC	ECC	kA
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
240	#num#	k4
pinů	pin	k1gInPc2
a	a	k8xC
mají	mít	k5eAaImIp3nP
výšku	výška	k1gFnSc4
3	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
Entertainment	Entertainment	k1gInSc4
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
hlavně	hlavně	k9
pro	pro	k7c4
OEM	OEM	kA
a	a	k8xC
výrobce	výrobce	k1gMnSc1
počítačů	počítač	k1gMnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
zaujmout	zaujmout	k5eAaPmF
nízkou	nízký	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
<g/>
;	;	kIx,
prodejní	prodejní	k2eAgFnSc1d1
efektivní	efektivní	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
je	být	k5eAaImIp3nS
1333	#num#	k4
MHz	Mhz	kA
a	a	k8xC
napětí	napětí	k1gNnSc2
1,5	1,5	k4
V.	V.	kA
Řada	řada	k1gFnSc1
Performance	performance	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
hlavně	hlavně	k9
pro	pro	k7c4
běžné	běžný	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
prodejní	prodejní	k2eAgFnSc1d1
efektivní	efektivní	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
je	být	k5eAaImIp3nS
1333	#num#	k4
a	a	k8xC
1600	#num#	k4
MHz	Mhz	kA
a	a	k8xC
napětí	napětí	k1gNnSc2
1,35	1,35	k4
až	až	k9
1,5	1,5	k4
V.	V.	kA
A	a	k8xC
řada	řada	k1gFnSc1
Radeon	Radeona	k1gFnPc2
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
hlavně	hlavně	k9
pro	pro	k7c4
výkonné	výkonný	k2eAgFnPc4d1
sestavy	sestava	k1gFnPc4
<g/>
,	,	kIx,
prodejní	prodejní	k2eAgFnSc1d1
efektivní	efektivní	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
je	být	k5eAaImIp3nS
1866	#num#	k4
MHz	Mhz	kA
a	a	k8xC
napětí	napětí	k1gNnSc1
je	být	k5eAaImIp3nS
1,5	1,5	k4
až	až	k9
1,65	1,65	k4
V.	V.	kA
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
technologií	technologie	k1gFnPc2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
společnosti	společnost	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
ATI	ATI	kA
<g/>
#	#	kIx~
<g/>
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1
milníky	milník	k1gInPc1
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1969	#num#	k4
založení	založení	k1gNnSc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1969	#num#	k4
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
nového	nový	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
na	na	k7c6
adrese	adresa	k1gFnSc6
901	#num#	k4
Thompson	Thompsona	k1gFnPc2
Place	plac	k1gInSc6
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
prošla	projít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
transformací	transformace	k1gFnSc7
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
(	(	kIx(
<g/>
Limited	limited	k2eAgNnPc2d1
Public	publicum	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
společnost	společnost	k1gFnSc1
otevřela	otevřít	k5eAaPmAgFnS
první	první	k4xOgFnSc4
zámořskou	zámořský	k2eAgFnSc4d1
výrobní	výrobní	k2eAgFnSc4d1
fabriku	fabrika	k1gFnSc4
ve	v	k7c6
městě	město	k1gNnSc6
Penang	Penanga	k1gFnPc2
ve	v	k7c6
státu	stát	k1gInSc6
Malajsie	Malajsie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
produkovat	produkovat	k5eAaImF
RAM	RAM	kA
paměti	paměť	k1gFnSc2
a	a	k8xC
přesněji	přesně	k6eAd2
čip	čip	k1gInSc1
Am	Am	k1gFnSc2
<g/>
9102	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
dohodla	dohodnout	k5eAaPmAgFnS
a	a	k8xC
podepsala	podepsat	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
výměně	výměna	k1gFnSc6
patentů	patent	k1gInPc2
se	s	k7c7
společností	společnost	k1gFnSc7
Intel	Intel	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
založila	založit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
společně	společně	k6eAd1
s	s	k7c7
další	další	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
novou	nový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Computers	Computers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
společnost	společnost	k1gFnSc1
vstoupila	vstoupit	k5eAaPmAgFnS
na	na	k7c4
kapitálový	kapitálový	k2eAgInSc4d1
trh	trh	k1gInSc4
–	–	k?
na	na	k7c4
newyorskou	newyorský	k2eAgFnSc4d1
burzu	burza	k1gFnSc4
NYSE	NYSE	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
redesignu	redesigno	k1gNnSc3
loga	logo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
po	po	k7c4
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
nezměněné	změněný	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
uzavření	uzavření	k1gNnSc3
smluvního	smluvní	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
s	s	k7c7
IBM	IBM	kA
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
AMD	AMD	kA
dodávala	dodávat	k5eAaImAgFnS
klony	klon	k1gInPc4
procesorů	procesor	k1gInPc2
Intel	Intel	kA
8086	#num#	k4
a	a	k8xC
8088	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
vyrábět	vyrábět	k5eAaImF
výrobky	výrobek	k1gInPc4
splňující	splňující	k2eAgInSc4d1
standard	standard	k1gInSc4
INT	INT	kA
<g/>
.	.	kIx.
<g/>
STD	STD	kA
<g/>
.1000	.1000	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
společnost	společnost	k1gFnSc1
Intel	Intel	kA
rozvázal	rozvázat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
AMD	AMD	kA
a	a	k8xC
ta	ten	k3xDgFnSc1
následně	následně	k6eAd1
zažalovala	zažalovat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Intel	Intel	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
centrum	centrum	k1gNnSc1
vývoje	vývoj	k1gInSc2
AMD	AMD	kA
Submicron	Submicron	k1gMnSc1
Development	Development	k1gMnSc1
Center	centrum	k1gNnPc2
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc3
<g/>
,	,	kIx,
stát	stát	k1gInSc4
Kalifornie	Kalifornie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byla	být	k5eAaImAgFnS
spolu	spolu	k6eAd1
se	s	k7c7
společností	společnost	k1gFnSc7
Fujitsu	Fujits	k1gInSc2
založena	založen	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Spansion	Spansion	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
produkcí	produkce	k1gFnSc7
Flash	Flasha	k1gFnPc2
pamětí	paměť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
fúze	fúze	k1gFnSc1
se	s	k7c7
společností	společnost	k1gFnSc7
NexGen	NexGna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
společnost	společnost	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
partnerství	partnerství	k1gNnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
Motorola	Motorola	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byly	být	k5eAaImAgInP
prodány	prodán	k2eAgInPc1d1
návrhy	návrh	k1gInPc1
programovatelný	programovatelný	k2eAgInSc1d1
logický	logický	k2eAgInSc4d1
pamětí	paměť	k1gFnSc7
Vantis	Vantis	k1gFnSc2
společnosti	společnost	k1gFnSc2
Lattice	Lattice	k1gFnSc2
Semiconductor	Semiconductor	k1gInSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
představila	představit	k5eAaPmAgFnS
první	první	k4xOgInSc4
procesor	procesor	k1gInSc4
architektury	architektura	k1gFnSc2
x	x	k?
<g/>
86	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
běžel	běžet	k5eAaImAgMnS
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
vyšší	vysoký	k2eAgFnSc4d2
než	než	k8xS
1	#num#	k4
GHz	GHz	k1gFnPc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
na	na	k7c4
1016	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
společnost	společnost	k1gFnSc1
získala	získat	k5eAaPmAgFnS
důležitou	důležitý	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
Hector	Hector	k1gMnSc1
Ruiz	Ruiz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
fúze	fúze	k1gFnSc1
se	se	k3xPyFc4
společnosti	společnost	k1gFnSc2
Alchemy	Alchema	k1gFnSc2
Semiconductor	Semiconductor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
se	s	k7c7
společností	společnost	k1gFnSc7
IBM	IBM	kA
<g/>
,	,	kIx,
dále	daleko	k6eAd2
byla	být	k5eAaImAgFnS
uzavřená	uzavřený	k2eAgFnSc1d1
strategická	strategický	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
se	s	k7c7
společností	společnost	k1gFnPc2
Sun	Sun	kA
Microsystems	Microsystems	k1gInSc1
a	a	k8xC
odkoupení	odkoupení	k1gNnSc4
společnosti	společnost	k1gFnSc2
National	National	k1gMnSc1
Semiconductor	Semiconductor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
společnost	společnost	k1gFnSc1
AMD	AMD	kA
odkoupila	odkoupit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
ATI	ATI	kA
a	a	k8xC
otevření	otevření	k1gNnSc2
nového	nový	k2eAgNnSc2d1
výzkumného	výzkumný	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
AMD	AMD	kA
Shanghai	Shanghai	k1gNnSc1
Research	Research	k1gMnSc1
and	and	k?
Development	Development	k1gInSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
SRDC	SRDC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
nastupuje	nastupovat	k5eAaImIp3nS
na	na	k7c6
pozici	pozice	k1gFnSc6
ředitele	ředitel	k1gMnSc2
Hectora	Hector	k1gMnSc2
Ruize	Ruize	k1gFnSc2
nový	nový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Dirk	Dirk	k1gMnSc1
Meyer	Meyer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgInS
vyřešen	vyřešit	k5eAaPmNgInS
soudní	soudní	k2eAgInSc1d1
spor	spor	k1gInSc1
mezi	mezi	k7c7
společnostmi	společnost	k1gFnPc7
AMD	AMD	kA
a	a	k8xC
Intel	Intel	kA
mimosoudně	mimosoudně	k6eAd1
ve	v	k7c4
prospěch	prospěch	k1gInSc4
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
společnost	společnost	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
program	program	k1gInSc4
AMD	AMD	kA
Fusion	Fusion	k1gInSc1
Fund	fund	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
nový	nový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
společnosti	společnost	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Rory	Rora	k1gFnSc2
Read	Read	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
společnosti	společnost	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
Lisa	Lisa	k1gFnSc1
Su	Su	k?
</s>
<s>
Éra	éra	k1gFnSc1
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
založení	založení	k1gNnSc2
firmy	firma	k1gFnSc2
</s>
<s>
Společnost	společnost	k1gFnSc1
AMD	AMD	kA
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1969	#num#	k4
se	s	k7c7
vstupním	vstupní	k2eAgInSc7d1
kapitálem	kapitál	k1gInSc7
100	#num#	k4
tisíc	tisíc	k4xCgInPc2
USD	USD	kA
<g/>
,	,	kIx,
bývalými	bývalý	k2eAgMnPc7d1
pracovníky	pracovník	k1gMnPc7
firmy	firma	k1gFnSc2
Fairchild	Fairchild	k1gInSc1
Semiconductor	Semiconductor	k1gInSc1
Jerry	Jerra	k1gFnSc2
Sanders	Sandersa	k1gFnPc2
III	III	kA
<g/>
,	,	kIx,
Ed	Ed	k1gMnSc1
Turney	Turnea	k1gFnSc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Carey	Carea	k1gFnSc2
<g/>
,	,	kIx,
Sven	Sven	k1gNnSc1
Simonsen	Simonsna	k1gFnPc2
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Gifford	Gifford	k1gMnSc1
a	a	k8xC
třemi	tři	k4xCgInPc7
členy	člen	k1gInPc7
Gifford	Gifforda	k1gFnPc2
týmu	tým	k1gInSc2
Frank	Frank	k1gMnSc1
Botte	Bott	k1gInSc5
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Giles	Giles	k1gMnSc1
a	a	k8xC
Larry	Larra	k1gFnPc1
Stenger	Stengra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
pracovní	pracovní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
byl	být	k5eAaImAgInS
podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
obývací	obývací	k2eAgInSc4d1
pokoj	pokoj	k1gInSc4
v	v	k7c6
rodinném	rodinný	k2eAgInSc6d1
domě	dům	k1gInSc6
spoluzakladatele	spoluzakladatel	k1gMnSc2
John	John	k1gMnSc1
Carey	Carea	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
zaměření	zaměření	k1gNnSc1
firmy	firma	k1gFnSc2
bylo	být	k5eAaImAgNnS
na	na	k7c4
logické	logický	k2eAgInPc4d1
čipy	čip	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následně	následně	k6eAd1
se	se	k3xPyFc4
přestěhovali	přestěhovat	k5eAaPmAgMnP
do	do	k7c2
dvou	dva	k4xCgFnPc2
kanceláří	kancelář	k1gFnPc2
ve	v	k7c6
městě	město	k1gNnSc6
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
získání	získání	k1gNnSc6
peněz	peníze	k1gInPc2
na	na	k7c4
výrobní	výrobní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
a	a	k8xC
nové	nový	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
během	během	k7c2
září	září	k1gNnSc2
přestěhovala	přestěhovat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
do	do	k7c2
nového	nový	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
na	na	k7c6
adrese	adresa	k1gFnSc6
901	#num#	k4
Thompson	Thompsona	k1gFnPc2
Place	plac	k1gInSc6
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
v	v	k7c6
továrně	továrna	k1gFnSc6
Fab	Fab	k1gMnSc2
<g/>
1	#num#	k4
byly	být	k5eAaImAgInP
dosaženy	dosažen	k2eAgInPc1d1
první	první	k4xOgInPc1
dobré	dobrý	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
čipu	čip	k1gInSc2
Am	Am	k1gFnSc2
<g/>
9300	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
bitový	bitový	k2eAgInSc1d1
MSI	MSI	kA
shift	shift	k1gInSc1
register	registrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
pár	pár	k4xCyI
následujících	následující	k2eAgNnPc2d1
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
portfolium	portfolium	k1gNnSc1
společnosti	společnost	k1gFnSc2
tvořeno	tvořit	k5eAaImNgNnS
hlavně	hlavně	k6eAd1
nabízením	nabízení	k1gNnSc7
optimalizace	optimalizace	k1gFnSc2
výrobků	výrobek	k1gInPc2
jiných	jiný	k2eAgFnPc2d1
firem	firma	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
prostředky	prostředek	k1gInPc4
alternativních	alternativní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
optimalizace	optimalizace	k1gFnPc1
byly	být	k5eAaImAgFnP
většinou	většina	k1gFnSc7
pro	pro	k7c4
vyšší	vysoký	k2eAgFnSc4d2
rychlost	rychlost	k1gFnSc4
a	a	k8xC
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
používali	používat	k5eAaImAgMnP
i	i	k9
heslo	heslo	k1gNnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Parametry	parametr	k1gInPc1
především	především	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
kvality	kvalita	k1gFnSc2
poskytovaných	poskytovaný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
vysokou	vysoký	k2eAgFnSc4d1
záruku	záruka	k1gFnSc4
kvality	kvalita	k1gFnSc2
všech	všecek	k3xTgInPc2
výrobků	výrobek	k1gInPc2
podle	podle	k7c2
vojenského	vojenský	k2eAgInSc2d1
standardu	standard	k1gInSc2
MIL-STD-	MIL-STD-	k1gFnSc2
<g/>
883	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Éra	éra	k1gFnSc1
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
vstupu	vstup	k1gInSc2
na	na	k7c4
akciový	akciový	k2eAgInSc4d1
trh	trh	k1gInSc4
a	a	k8xC
nových	nový	k2eAgInPc2d1
výrobních	výrobní	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1970	#num#	k4
měla	mít	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
už	už	k9
53	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
a	a	k8xC
18	#num#	k4
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
finanční	finanční	k2eAgInPc1d1
zisky	zisk	k1gInPc1
byly	být	k5eAaImAgInP
nízké	nízký	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
prošla	projít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
transformací	transformace	k1gFnSc7
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
(	(	kIx(
<g/>
Limited	limited	k2eAgNnPc2d1
Public	publicum	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
září	září	k1gNnSc6
vydala	vydat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
525	#num#	k4
tisíc	tisíc	k4xCgInPc2
akcií	akcie	k1gFnPc2
v	v	k7c4
cenně	cenně	k6eAd1
15	#num#	k4
USD	USD	kA
<g/>
/	/	kIx~
<g/>
kus	kus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
v	v	k7c6
listopadu	listopad	k1gInSc6
byla	být	k5eAaImAgFnS
postavená	postavený	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c6
adrese	adresa	k1gFnSc6
902	#num#	k4
Thompson	Thompsona	k1gFnPc2
Place	plac	k1gInSc6
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
společnost	společnost	k1gFnSc1
otevřela	otevřít	k5eAaPmAgFnS
první	první	k4xOgFnSc4
zámořskou	zámořský	k2eAgFnSc4d1
výrobní	výrobní	k2eAgFnSc4d1
fabriku	fabrika	k1gFnSc4
ve	v	k7c6
městě	město	k1gNnSc6
Penang	Penanga	k1gFnPc2
ve	v	k7c6
státu	stát	k1gInSc6
Malajsie	Malajsie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
měla	mít	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
už	už	k9
1500	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
a	a	k8xC
vyráběla	vyrábět	k5eAaImAgFnS
přes	přes	k7c4
200	#num#	k4
různých	různý	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
vlastních	vlastní	k2eAgInPc6d1
tak	tak	k8xS,k8xC
cizích	cizí	k2eAgInPc6d1
návrhu	návrh	k1gInSc6
a	a	k8xC
roční	roční	k2eAgFnPc1d1
tržby	tržba	k1gFnPc1
byly	být	k5eAaImAgFnP
zhruba	zhruba	k6eAd1
26,5	26,5	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
taky	taky	k6eAd1
oslavila	oslavit	k5eAaPmAgFnS
páté	pátá	k1gFnPc4
výročí	výročí	k1gNnSc2
existence	existence	k1gFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
společnost	společnost	k1gFnSc1
investovala	investovat	k5eAaBmAgFnS
do	do	k7c2
zlepšování	zlepšování	k1gNnSc2
a	a	k8xC
rozšíření	rozšíření	k1gNnSc2
vybavení	vybavení	k1gNnSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
výstavba	výstavba	k1gFnSc1
nové	nový	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
na	na	k7c6
adrese	adresa	k1gFnSc6
915	#num#	k4
DeGuigne	DeGuign	k1gInSc5
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc3
<g/>
,	,	kIx,
</s>
<s>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
výrobní	výrobní	k2eAgFnSc7d1
továrnou	továrna	k1gFnSc7
nově	nově	k6eAd1
založené	založený	k2eAgNnSc1d1
společnost	společnost	k1gFnSc4
Spansion	Spansion	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
založila	založit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
AMD	AMD	kA
a	a	k8xC
další	další	k2eAgInPc1d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Fab	Fab	k1gFnSc1
<g/>
25	#num#	k4
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
110	#num#	k4
nm	nm	k?
<g/>
,	,	kIx,
90	#num#	k4
nm	nm	k?
a	a	k8xC
65	#num#	k4
nm	nm	k?
výrobním	výrobní	k2eAgInSc7d1
procesem	proces	k1gInSc7
</s>
<s>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
,	,	kIx,
dále	daleko	k6eAd2
novou	nový	k2eAgFnSc7d1
montážní	montážní	k2eAgFnSc7d1
dílnou	dílna	k1gFnSc7
ve	v	k7c6
městě	město	k1gNnSc6
Manila	Manila	k1gFnSc1
ve	v	k7c6
státu	stát	k1gInSc6
Filipíny	Filipíny	k1gFnPc4
a	a	k8xC
rozšířením	rozšíření	k1gNnSc7
továrny	továrna	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Penang	Penanga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
roku	rok	k1gInSc2
1974	#num#	k4
a	a	k8xC
1975	#num#	k4
zavádí	zavádět	k5eAaImIp3nP
společnost	společnost	k1gFnSc4
44	#num#	k4
hodinový	hodinový	k2eAgInSc4d1
pracovní	pracovní	k2eAgInSc4d1
týden	týden	k1gInSc4
kvůli	kvůli	k7c3
recesi	recese	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následně	následně	k6eAd1
v	v	k7c6
daném	daný	k2eAgInSc6d1
roce	rok	k1gInSc6
1975	#num#	k4
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
produkovat	produkovat	k5eAaImF
RAM	RAM	kA
paměti	paměť	k1gFnSc2
a	a	k8xC
přesněji	přesně	k6eAd2
čip	čip	k1gInSc1
Am	Am	k1gFnSc2
<g/>
9102	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
a	a	k8xC
během	během	k7c2
roku	rok	k1gInSc2
vyslovil	vyslovit	k5eAaPmAgMnS
ředitel	ředitel	k1gMnSc1
Jerry	Jerra	k1gFnSc2
Sanders	Sandersa	k1gFnPc2
rčení	rčení	k1gNnSc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Lidé	člověk	k1gMnPc1
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
výrobky	výrobek	k1gInPc4
a	a	k8xC
zisk	zisk	k1gInSc4
budou	být	k5eAaImBp3nP
následovat	následovat	k5eAaImF
<g/>
“	“	k?
(	(	kIx(
<g/>
people	people	k6eAd1
first	first	k5eAaPmF
<g/>
,	,	kIx,
products	products	k6eAd1
and	and	k?
profit	profit	k1gInSc1
will	will	k1gMnSc1
follow	follow	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dále	daleko	k6eAd2
během	během	k7c2
reverzního	reverzní	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
čipu	čip	k1gInSc2
Intel	Intel	kA
8080	#num#	k4
vytvořila	vytvořit	k5eAaPmAgFnS
bit-slice	bit-slice	k1gFnSc1
procesor	procesor	k1gInSc4
pro	pro	k7c4
návrh	návrh	k1gInSc4
mini	mini	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
dohodla	dohodnout	k5eAaPmAgFnS
a	a	k8xC
podepsala	podepsat	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
výměně	výměna	k1gFnSc6
patentů	patent	k1gInPc2
se	s	k7c7
společností	společnost	k1gFnSc7
Intel	Intel	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
taky	taky	k6eAd1
společnost	společnost	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
hodnoty	hodnota	k1gFnPc4
168	#num#	k4
miliónů	milión	k4xCgInPc2
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vánoce	vánoce	k1gFnPc4
společnost	společnost	k1gFnSc1
uspořádala	uspořádat	k5eAaPmAgFnS
oslavu	oslava	k1gFnSc4
v	v	k7c4
Rickeýs	Rickeýs	k1gInSc4
Hyatt	Hyatt	k1gInSc4
House	house	k1gNnSc1
ve	v	k7c6
městě	město	k1gNnSc6
Palo	Pala	k1gMnSc5
Alto	Alta	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
1977	#num#	k4
založila	založit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
společně	společně	k6eAd1
s	s	k7c7
další	další	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
novou	nový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Computers	Computers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
otevírá	otevírat	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
novou	nový	k2eAgFnSc4d1
továrnu	továrna	k1gFnSc4
ve	v	k7c6
městě	město	k1gNnSc6
Manile	Manila	k1gFnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
společnost	společnost	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
100	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
ročního	roční	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
a	a	k8xC
započala	započnout	k5eAaPmAgFnS
výstavba	výstavba	k1gFnSc1
nové	nový	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Austin	Austina	k1gFnPc2
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1979	#num#	k4
společnost	společnost	k1gFnSc1
vstoupila	vstoupit	k5eAaPmAgFnS
na	na	k7c4
kapitálový	kapitálový	k2eAgInSc4d1
trh	trh	k1gInSc4
–	–	k?
na	na	k7c4
newyorskou	newyorský	k2eAgFnSc4d1
burzu	burza	k1gFnSc4
NYSE	NYSE	kA
<g/>
,	,	kIx,
</s>
<s>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
dále	daleko	k6eAd2
započala	započnout	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
v	v	k7c6
nových	nový	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
továrny	továrna	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Austin	Austina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
společnosti	společnost	k1gFnSc2
Google	Google	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
získává	získávat	k5eAaImIp3nS
data	datum	k1gNnPc4
od	od	k7c2
společnosti	společnost	k1gFnSc2
NYSE	NYSE	kA
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
AMD	AMD	kA
obchodovat	obchodovat	k5eAaImF
s	s	k7c7
akciemi	akcie	k1gFnPc7
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počáteční	počáteční	k2eAgFnSc1d1
cena	cena	k1gFnSc1
akcie	akcie	k1gFnSc2
byla	být	k5eAaImAgFnS
0,75	0,75	k4
USD	USD	kA
a	a	k8xC
počet	počet	k1gInSc1
2	#num#	k4
138	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Éra	éra	k1gFnSc1
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
důležitých	důležitý	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
a	a	k8xC
stavba	stavba	k1gFnSc1
výzkumného	výzkumný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
</s>
<s>
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
1980	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
redesignu	redesigno	k1gNnSc3
loga	logo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
dosáhla	dosáhnout	k5eAaPmAgFnS
2	#num#	k4
<g/>
×	×	k?
vyšších	vysoký	k2eAgInPc2d2
zisků	zisk	k1gInPc2
než	než	k8xS
před	před	k7c7
dvěma	dva	k4xCgInPc7
roky	rok	k1gInPc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
společnost	společnost	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
důležitého	důležitý	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
<g/>
,	,	kIx,
vyvinuté	vyvinutý	k2eAgInPc1d1
čipy	čip	k1gInPc1
společností	společnost	k1gFnPc2
se	se	k3xPyFc4
namontovali	namontovat	k5eAaPmAgMnP
do	do	k7c2
raketoplánu	raketoplán	k1gInSc2
Columbia	Columbia	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dostavěna	dostavěn	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
výrobní	výrobní	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
San	San	k1gMnSc1
Antonio	Antonio	k1gMnSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
se	s	k7c7
společností	společnost	k1gFnSc7
Intel	Intel	kA
obnovuje	obnovovat	k5eAaImIp3nS
a	a	k8xC
rozšiřuje	rozšiřovat	k5eAaImIp3nS
původní	původní	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
výměně	výměna	k1gFnSc6
patentů	patent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
roku	rok	k1gInSc2
zainvestovala	zainvestovat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
do	do	k7c2
rozšíření	rozšíření	k1gNnSc2
výrobních	výrobní	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
továren	továrna	k1gFnPc2
ve	v	k7c6
městech	město	k1gNnPc6
Texas	Texas	kA
<g/>
,	,	kIx,
San	San	k1gFnSc4
Antonii	Antonie	k1gFnSc4
a	a	k8xC
Austion	Austion	k1gInSc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
zlepšení	zlepšení	k1gNnSc3
konkurenceschopnosti	konkurenceschopnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
počátku	počátek	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
začala	začít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
více	hodně	k6eAd2
vyvíjet	vyvíjet	k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
produkty	produkt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
uzavření	uzavření	k1gNnSc3
smluvního	smluvní	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
s	s	k7c7
IBM	IBM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
smlouva	smlouva	k1gFnSc1
zajišťovala	zajišťovat	k5eAaImAgFnS
dodání	dodání	k1gNnSc4
mikroprocesorů	mikroprocesor	k1gInPc2
typu	typ	k1gInSc2
AMD	AMD	kA
8086	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
a	a	k8xC
později	pozdě	k6eAd2
hlavně	hlavně	k9
AMD	AMD	kA
8088	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
do	do	k7c2
osobních	osobní	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
značky	značka	k1gFnSc2
IBM	IBM	kA
<g/>
,	,	kIx,
známé	známý	k2eAgFnSc2d1
pod	pod	k7c7
značkou	značka	k1gFnSc7
IBM	IBM	kA
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
přesné	přesný	k2eAgInPc4d1
klony	klon	k1gInPc4
čipů	čip	k1gInPc2
Intel	Intel	kA
8086	#num#	k4
a	a	k8xC
8088	#num#	k4
navržené	navržený	k2eAgFnSc2d1
společností	společnost	k1gFnSc7
Intel	Intel	kA
<g/>
,	,	kIx,
návrhy	návrh	k1gInPc1
byly	být	k5eAaImAgInP
získány	získán	k2eAgInPc1d1
díky	díky	k7c3
podepsání	podepsání	k1gNnSc3
licenční	licenční	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
mezi	mezi	k7c7
společnostmi	společnost	k1gFnPc7
AMD	AMD	kA
a	a	k8xC
Intel	Intel	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
společnost	společnost	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
první	první	k4xOgFnSc7
výrobní	výrobní	k2eAgFnSc7d1
linku	linka	k1gFnSc4
(	(	kIx(
<g/>
MMP	MMP	kA
<g/>
)	)	kIx)
továrny	továrna	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Austin	Austina	k1gFnPc2
<g/>
,	,	kIx,
obsluhovali	obsluhovat	k5eAaImAgMnP
ji	on	k3xPp3gFnSc4
čtyři	čtyři	k4xCgMnPc1
zaměstnanci	zaměstnanec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
se	s	k7c7
společností	společnost	k1gFnSc7
Intel	Intel	kA
podepisuje	podepisovat	k5eAaImIp3nS
další	další	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
výměně	výměna	k1gFnSc6
patentů	patent	k1gInPc2
ohledně	ohledně	k7c2
mikroprocesorů	mikroprocesor	k1gInPc2
a	a	k8xC
obvodů	obvod	k1gInPc2
řady	řada	k1gFnSc2
iAPX	iAPX	k?
<g/>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
představuje	představovat	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
nový	nový	k2eAgInSc4d1
průmyslový	průmyslový	k2eAgInSc4d1
standard	standard	k1gInSc4
INT	INT	kA
<g/>
.	.	kIx.
<g/>
STD	STD	kA
<g/>
.1000	.1000	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zaručuje	zaručovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
kvalitu	kvalita	k1gFnSc4
v	v	k7c6
oboru	obor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
během	během	k7c2
roku	rok	k1gInSc2
společnost	společnost	k1gFnSc1
založila	založit	k5eAaPmAgFnS
pobočku	pobočka	k1gFnSc4
AMD	AMD	kA
Singapur	Singapur	k1gInSc4
ve	v	k7c6
městě	město	k1gNnSc6
Singapur	Singapur	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
se	se	k3xPyFc4
AMD	AMD	kA
umístila	umístit	k5eAaPmAgFnS
v	v	k7c6
žebříčku	žebříček	k1gInSc6
100	#num#	k4
nejlepších	dobrý	k2eAgMnPc2d3
zaměstnavatelů	zaměstnavatel	k1gMnPc2
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
poprvé	poprvé	k6eAd1
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
žebříčku	žebříček	k1gInSc6
Fortune	Fortun	k1gInSc5
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
byla	být	k5eAaImAgFnS
rozvázána	rozvázán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
mezi	mezi	k7c7
společnostmi	společnost	k1gFnPc7
Intel	Intel	kA
a	a	k8xC
AMD	AMD	kA
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
tak	tak	k9
výroba	výroba	k1gFnSc1
licencovaných	licencovaný	k2eAgInPc2d1
klonů	klon	k1gInPc2
procesorů	procesor	k1gInPc2
společnosti	společnost	k1gFnSc2
Intel	Intel	kA
<g/>
,	,	kIx,
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
se	se	k3xPyFc4
už	už	k6eAd1
nepovedlo	povést	k5eNaPmAgNnS
získat	získat	k5eAaPmF
návrhy	návrh	k1gInPc4
nového	nový	k2eAgInSc2d1
procesoru	procesor	k1gInSc2
Intel	Intel	kA
80386	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozvázání	rozvázání	k1gNnSc1
bylo	být	k5eAaImAgNnS
zpochybněno	zpochybnit	k5eAaPmNgNnS
u	u	k7c2
soudu	soud	k1gInSc2
<g/>
,	,	kIx,
případ	případ	k1gInSc1
se	se	k3xPyFc4
táhl	táhnout	k5eAaImAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
rozhodl	rozhodnout	k5eAaPmAgMnS
v	v	k7c4
prospěch	prospěch	k1gInSc4
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
klon	klon	k1gInSc4
AMD	AMD	kA
Am	Am	k1gFnSc7
<g/>
386	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInSc1d1
spor	spor	k1gInSc1
mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
společnostmi	společnost	k1gFnPc7
byl	být	k5eAaImAgInS
rozpoután	rozpoután	k2eAgInSc1d1
následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
<g/>
,	,	kIx,
Intel	Intel	kA
zažaloval	zažalovat	k5eAaPmAgInS
AMD	AMD	kA
kvůli	kvůli	k7c3
porušení	porušení	k1gNnSc3
ochranné	ochranný	k2eAgFnSc2d1
známky	známka	k1gFnSc2
<g/>
,	,	kIx,
AMD	AMD	kA
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
získalo	získat	k5eAaPmAgNnS
právo	právo	k1gNnSc1
na	na	k7c6
používání	používání	k1gNnSc6
číselného	číselný	k2eAgNnSc2d1
označení	označení	k1gNnSc2
svých	svůj	k3xOyFgInPc2
procesorů	procesor	k1gInPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
odůvodněno	odůvodnit	k5eAaPmNgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
číslo	číslo	k1gNnSc4
nebo	nebo	k8xC
skupinu	skupina	k1gFnSc4
čísel	číslo	k1gNnPc2
si	se	k3xPyFc3
nejde	jít	k5eNaImIp3nS
registrovat	registrovat	k5eAaBmF
jako	jako	k9
ochranou	ochrana	k1gFnSc7
známku	známka	k1gFnSc4
a	a	k8xC
proto	proto	k8xC
Intel	Intel	kA
následně	následně	k6eAd1
pojmenoval	pojmenovat	k5eAaPmAgMnS
další	další	k2eAgInSc4d1
procesor	procesor	k1gInSc4
slovem	slovem	k6eAd1
<g/>
:	:	kIx,
Intel	Intel	kA
Pentium	Pentium	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1987	#num#	k4
společnost	společnost	k1gFnSc1
rovně	roveň	k1gFnSc2
získala	získat	k5eAaPmAgFnS
firmu	firma	k1gFnSc4
Monolithic	Monolithice	k1gInPc2
Memories	Memories	k1gInSc1
a	a	k8xC
vstoupila	vstoupit	k5eAaPmAgFnS
na	na	k7c4
trh	trh	k1gInSc4
programovatelných	programovatelný	k2eAgFnPc2d1
logických	logický	k2eAgFnPc2d1
pamětí	paměť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
centrum	centrum	k1gNnSc1
vývoje	vývoj	k1gInSc2
AMD	AMD	kA
Submicron	Submicron	k1gMnSc1
Development	Development	k1gMnSc1
Center	centrum	k1gNnPc2
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc3
<g/>
,	,	kIx,
stát	stát	k1gInSc4
Kalifornie	Kalifornie	k1gFnSc2
<g/>
,	,	kIx,
centrum	centrum	k1gNnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
nejnovějších	nový	k2eAgFnPc2d3
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Éra	éra	k1gFnSc1
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
důležitých	důležitý	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
a	a	k8xC
odkoupení	odkoupení	k1gNnSc4
společnosti	společnost	k1gFnSc2
NexGen	NexGna	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byla	být	k5eAaImAgFnS
spolu	spolu	k6eAd1
se	s	k7c7
společností	společnost	k1gFnSc7
Fujitsu	Fujits	k1gInSc2
založena	založen	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Spansion	Spansion	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
produkcí	produkce	k1gFnSc7
Flash	Flasha	k1gFnPc2
pamětí	paměť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
se	se	k3xPyFc4
společnosti	společnost	k1gFnSc6
Compaq	Compaq	kA
Computer	computer	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
produkce	produkce	k1gFnSc2
počítačů	počítač	k1gInPc2
založených	založený	k2eAgInPc2d1
na	na	k7c6
čipech	čip	k1gInPc6
Am	Am	k1gFnSc2
<g/>
486	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
fúze	fúze	k1gFnSc1
se	s	k7c7
společností	společnost	k1gFnSc7
NexGen	NexGna	k1gFnPc2
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
vývoji	vývoj	k1gInSc6
mikroprocesorů	mikroprocesor	k1gInPc2
a	a	k8xC
společnost	společnost	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
plán	plán	k1gInSc4
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
továrny	továrna	k1gFnSc2
Fab	Fab	k1gFnSc2
30	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Drážďany	Drážďany	k1gInPc4
ve	v	k7c6
státu	stát	k1gInSc6
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
společnost	společnost	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
partnerství	partnerství	k1gNnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
Motorola	Motorola	kA
<g/>
,	,	kIx,
výsledkem	výsledek	k1gInSc7
spolupráce	spolupráce	k1gFnSc2
byl	být	k5eAaImAgInS
vývoj	vývoj	k1gInSc1
polovodičové	polovodičový	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
na	na	k7c6
bázi	báze	k1gFnSc6
mědi	měď	k1gFnSc2
<g/>
,	,	kIx,
následně	následně	k6eAd1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
použita	použít	k5eAaPmNgFnS
technologie	technologie	k1gFnSc1
v	v	k7c6
nové	nový	k2eAgFnSc6d1
procesorové	procesorový	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
AMD	AMD	kA
K	K	kA
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
1999	#num#	k4
byly	být	k5eAaImAgFnP
prodány	prodat	k5eAaPmNgInP
návrhy	návrh	k1gInPc1
programovatelné	programovatelný	k2eAgFnSc2d1
logické	logický	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
Vantis	Vantis	k1gFnSc2
společnosti	společnost	k1gFnSc2
Lattice	Lattice	k1gFnSc2
Semiconductor	Semiconductor	k1gInSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
AMD	AMD	kA
představila	představit	k5eAaPmAgFnS
první	první	k4xOgInSc4
procesor	procesor	k1gInSc4
architektury	architektura	k1gFnSc2
x	x	k?
<g/>
86	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
běžel	běžet	k5eAaImAgMnS
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
vyšší	vysoký	k2eAgFnSc4d2
než	než	k8xS
1	#num#	k4
GHz	GHz	k1gFnPc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
na	na	k7c4
1016	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Éra	éra	k1gFnSc1
začátku	začátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
IBM	IBM	kA
<g/>
,	,	kIx,
odkoupení	odkoupení	k1gNnSc2
společnosti	společnost	k1gFnSc2
ATI	ATI	kA
a	a	k8xC
rozdělení	rozdělení	k1gNnSc2
společnosti	společnost	k1gFnSc2
2	#num#	k4
subjekty	subjekt	k1gInPc7
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
AMD	AMD	kA
s	s	k7c7
pomocí	pomoc	k1gFnSc7
procesoru	procesor	k1gInSc2
AMD	AMD	kA
Athlon	Athlon	k1gInSc1
stala	stát	k5eAaPmAgFnS
prvním	první	k4xOgMnSc6
výrobcem	výrobce	k1gMnSc7
procesorů	procesor	k1gInPc2
s	s	k7c7
frekvencí	frekvence	k1gFnSc7
vyšší	vysoký	k2eAgFnSc7d2
než	než	k8xS
1	#num#	k4
GHz	GHz	k1gFnPc2
a	a	k8xC
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
výrobu	výroba	k1gFnSc4
v	v	k7c6
továrně	továrna	k1gFnSc6
Fab	Fab	k1gFnSc1
30	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Dráždany	Dráždana	k1gFnSc2
ve	v	k7c6
státu	stát	k1gInSc6
Německo	Německo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
fúze	fúze	k1gFnSc1
se	se	k3xPyFc4
společnosti	společnost	k1gFnSc2
Alchemy	Alchema	k1gFnSc2
Semiconductor	Semiconductor	k1gInSc1
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
produkcí	produkce	k1gFnSc7
nízko	nízko	k6eAd1
odběrových	odběrový	k2eAgInPc2d1
a	a	k8xC
embedded	embedded	k1gInSc1
procesorů	procesor	k1gInPc2
a	a	k8xC
Jerry	Jerr	k1gMnPc7
Sanders	Sandersa	k1gFnPc2
získal	získat	k5eAaPmAgInS
důležitou	důležitý	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
<g/>
,	,	kIx,
prezidenta	prezident	k1gMnSc4
Hector	Hector	k1gMnSc1
Ruiz	Ruiz	k1gMnSc1
společnosti	společnost	k1gFnSc2
Motorola	Motorola	kA
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
následně	následně	k6eAd1
prezidentem	prezident	k1gMnSc7
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
a	a	k8xC
COO	COO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
podepsání	podepsání	k1gNnSc3
smlouvy	smlouva	k1gFnSc2
se	s	k7c7
společností	společnost	k1gFnSc7
IBM	IBM	kA
<g/>
,	,	kIx,
smlouva	smlouva	k1gFnSc1
se	se	k3xPyFc4
týkala	týkat	k5eAaImAgFnS
společného	společný	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
výrobních	výrobní	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
přinést	přinést	k5eAaPmF
pokročilejší	pokročilý	k2eAgInSc4d2
výrobní	výrobní	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc4d2
hustotu	hustota	k1gFnSc4
tranzistorů	tranzistor	k1gInPc2
na	na	k7c6
čipu	čip	k1gInSc6
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgFnPc4d2
frekvence	frekvence	k1gFnPc4
čipu	čip	k1gInSc2
atd	atd	kA
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
využito	využít	k5eAaPmNgNnS
při	při	k7c6
vývoji	vývoj	k1gInSc6
nového	nový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
procesoru	procesor	k1gInSc2
AMD	AMD	kA
K	k	k7c3
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
také	také	k6eAd1
k	k	k7c3
uzavření	uzavření	k1gNnSc3
strategické	strategický	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
se	s	k7c7
společností	společnost	k1gFnSc7
Sun	suna	k1gFnPc2
Microsystems	Microsystemsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
odkoupení	odkoupení	k1gNnSc4
společnosti	společnost	k1gFnSc2
National	National	k1gMnSc1
Semiconductor	Semiconductor	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
výrobky	výrobek	k1gInPc4
na	na	k7c6
trhu	trh	k1gInSc6
architektury	architektura	k1gFnSc2
x	x	k?
<g/>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
stala	stát	k5eAaPmAgFnS
prvním	první	k4xOgMnSc7
producentem	producent	k1gMnSc7
více	hodně	k6eAd2
jádrového	jádrový	k2eAgInSc2d1
procesoru	procesor	k1gInSc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
dvou	dva	k4xCgFnPc2
jádrového	jádrový	k2eAgInSc2d1
procesoru	procesor	k1gInSc2
a	a	k8xC
otevřela	otevřít	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
pobočku	pobočka	k1gFnSc4
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
(	(	kIx(
<g/>
China	China	k1gFnSc1
<g/>
)	)	kIx)
Co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Ltd	ltd	kA
<g/>
.	.	kIx.
ve	v	k7c6
městě	město	k1gNnSc6
Peking	Peking	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
2005	#num#	k4
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
nejvýkonnější	výkonný	k2eAgInSc1d3
procesor	procesor	k1gInSc1
aktuální	aktuální	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
pro	pro	k7c4
servery	server	k1gInPc4
1	#num#	k4
až	až	k8xS
8P	8P	k4
provedení	provedení	k1gNnSc6
i	i	k9
pracovní	pracovní	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
byla	být	k5eAaImAgFnS
tento	tento	k3xDgInSc4
rok	rok	k1gInSc4
podána	podán	k2eAgFnSc1d1
žaloba	žaloba	k1gFnSc1
k	k	k7c3
antimonopolnímu	antimonopolní	k2eAgInSc3d1
soudu	soud	k1gInSc3
<g/>
,	,	kIx,
týkala	týkat	k5eAaImAgFnS
se	se	k3xPyFc4
omezování	omezování	k1gNnSc4
konkurence	konkurence	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
společnosti	společnost	k1gFnSc2
Intel	Intel	kA
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
měl	mít	k5eAaImAgMnS
obsazeno	obsazen	k2eAgNnSc4d1
80	#num#	k4
%	%	kIx~
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intel	Intel	kA
umožňoval	umožňovat	k5eAaImAgInS
dumpingové	dumpingový	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
pro	pro	k7c4
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
integrovali	integrovat	k5eAaBmAgMnP
do	do	k7c2
svých	svůj	k3xOyFgInPc2
produktů	produkt	k1gInPc2
pouze	pouze	k6eAd1
jeho	jeho	k3xOp3gInPc4
procesory	procesor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
také	také	k9
otevřela	otevřít	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
továrnu	továrna	k1gFnSc4
Fab	Fab	k1gFnSc1
36	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Drážďany	Drážďany	k1gInPc4
<g/>
,	,	kIx,
plný	plný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
začal	začít	k5eAaPmAgMnS
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
významnému	významný	k2eAgNnSc3d1
spojení	spojení	k1gNnSc3
s	s	k7c7
producentem	producent	k1gMnSc7
grafických	grafický	k2eAgInPc2d1
čipů	čip	k1gInPc2
<g/>
,	,	kIx,
čipsetů	čipset	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
produktů	produkt	k1gInPc2
společností	společnost	k1gFnPc2
ATI	ATI	kA
<g/>
,	,	kIx,
také	také	k9
uzavřela	uzavřít	k5eAaPmAgFnS
partnerství	partnerství	k1gNnSc4
s	s	k7c7
producentem	producent	k1gMnSc7
osobních	osobní	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
společností	společnost	k1gFnPc2
DELL	Dell	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
do	do	k7c2
počítačů	počítač	k1gMnPc2
montovat	montovat	k5eAaImF
i	i	k9
produkty	produkt	k1gInPc1
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
vývoje	vývoj	k1gInSc2
AMD	AMD	kA
Shanghai	Shangha	k1gFnPc1
Research	Research	k1gMnSc1
and	and	k?
Development	Development	k1gInSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
SRDC	SRDC	kA
<g/>
)	)	kIx)
ve	v	k7c6
městě	město	k1gNnSc6
Šanghaj	Šanghaj	k1gFnSc1
(	(	kIx(
<g/>
vývoj	vývoj	k1gInSc1
pro	pro	k7c4
mobilní	mobilní	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
rovněž	rovněž	k9
zakládajícím	zakládající	k2eAgMnSc7d1
členem	člen	k1gMnSc7
neziskové	ziskový	k2eNgFnSc2d1
organizace	organizace	k1gFnSc2
The	The	k1gMnPc2
Green	Green	k1gInSc1
Grid	Grida	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
snížení	snížení	k1gNnSc4
spotřeby	spotřeba	k1gFnSc2
v	v	k7c6
IT	IT	kA
odvětví	odvětví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
proběhla	proběhnout	k5eAaPmAgFnS
první	první	k4xOgNnSc4
šetření	šetření	k1gNnSc4
pro	pro	k7c4
podezření	podezření	k1gNnSc4
monopolu	monopol	k1gInSc2
a	a	k8xC
smluvních	smluvní	k2eAgFnPc2d1
cen	cena	k1gFnPc2
mezi	mezi	k7c7
společnostmi	společnost	k1gFnPc7
ATI	ATI	kA
a	a	k8xC
NVIDIA	NVIDIA	kA
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
taky	taky	k6eAd1
demonstrovala	demonstrovat	k5eAaBmAgFnS
výkonnou	výkonný	k2eAgFnSc4d1
PC	PC	kA
platformu	platforma	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
výkon	výkon	k1gInSc4
nad	nad	k7c4
1	#num#	k4
teraflop	teraflop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
2007	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
první	první	k4xOgFnSc1
platforma	platforma	k1gFnSc1
Spider	Spidra	k1gFnPc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
obsahovala	obsahovat	k5eAaImAgFnS
procesor	procesor	k1gInSc4
Phenom	Phenom	k1gInSc4
<g/>
,	,	kIx,
grafikou	grafika	k1gFnSc7
kartu	karta	k1gFnSc4
HD	HD	kA
4800	#num#	k4
a	a	k8xC
čipset	čipset	k1gMnSc1
série	série	k1gFnSc2
AMD	AMD	kA
700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
začalo	začít	k5eAaPmAgNnS
vedení	vedení	k1gNnSc1
společnosti	společnost	k1gFnSc2
řešit	řešit	k5eAaImF
rozdělení	rozdělení	k1gNnSc4
společnosti	společnost	k1gFnSc2
na	na	k7c4
vývoj	vývoj	k1gInSc4
a	a	k8xC
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
jednání	jednání	k1gNnSc4
pokračovaly	pokračovat	k5eAaImAgFnP
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
superpočítač	superpočítač	k1gInSc1
TACC	TACC	kA
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
nejvýkonnějším	výkonný	k2eAgMnSc7d3
v	v	k7c6
oblasti	oblast	k1gFnSc6
obecného	obecný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
složen	složit	k5eAaPmNgMnS
společně	společně	k6eAd1
se	s	k7c7
společností	společnost	k1gFnSc7
Sun	Sun	kA
a	a	k8xC
obsahoval	obsahovat	k5eAaImAgInS
procesory	procesor	k1gInPc4
AMD	AMD	kA
Opteron	Opteron	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
neziskový	ziskový	k2eNgInSc4d1
program	program	k1gInSc4
AMD	AMD	kA
Changing	Changing	k1gInSc1
the	the	k?
Game	game	k1gInSc1
pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
vnímaní	vnímaný	k2eAgMnPc1d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
techniky	technika	k1gFnSc2
a	a	k8xC
života	život	k1gInSc2
učením	učení	k1gNnSc7
pomocí	pomocí	k7c2
digitálních	digitální	k2eAgFnPc2d1
her	hra	k1gFnPc2
se	se	k3xPyFc4
sociální	sociální	k2eAgMnPc1d1
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představila	představit	k5eAaPmAgFnS
AMD	AMD	kA
Cinema	Cinema	k1gFnSc1
2.0	2.0	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
lépe	dobře	k6eAd2
rozlišit	rozlišit	k5eAaPmF
filmy	film	k1gInPc4
od	od	k7c2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dirk	Dirk	k1gMnSc1
Meyer	Meyer	k1gMnSc1
nastupuje	nastupovat	k5eAaImIp3nS
na	na	k7c4
místo	místo	k1gNnSc4
výkonného	výkonný	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
místo	místo	k7c2
Hectora	Hector	k1gMnSc2
Ruize	Ruize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgInS
mimosoudním	mimosoudní	k2eAgNnSc7d1
vyrovnáním	vyrovnání	k1gNnSc7
ukončen	ukončen	k2eAgInSc4d1
spor	spor	k1gInSc4
mezi	mezi	k7c7
společnostmi	společnost	k1gFnPc7
AMD	AMD	kA
a	a	k8xC
Intel	Intel	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intel	Intel	kA
zaplatil	zaplatit	k5eAaPmAgInS
1,25	1,25	k4
miliardy	miliarda	k4xCgFnSc2
amerických	americký	k2eAgMnPc2d1
dolarů	dolar	k1gInPc2
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgMnS
se	s	k7c7
společností	společnost	k1gFnSc7
AMD	AMD	kA
novou	nový	k2eAgFnSc4d1
cross-licenční	cross-licenční	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zaručilo	zaručit	k5eAaPmAgNnS
oběma	dva	k4xCgMnPc7
společnostem	společnost	k1gFnPc3
přístup	přístup	k1gInSc4
k	k	k7c3
patentům	patent	k1gInPc3
druhé	druhý	k4xOgFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představila	představit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
program	program	k1gInSc4
Fusion	Fusion	k1gInSc1
Partner	partner	k1gMnSc1
Program	program	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
na	na	k7c4
zlepšení	zlepšení	k1gNnSc4
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedla	uvést	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
značku	značka	k1gFnSc4
VISION	vision	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
měla	mít	k5eAaImAgFnS
ulehčit	ulehčit	k5eAaPmF
nákup	nákup	k1gInSc4
produktů	produkt	k1gInPc2
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
nevyznají	vyznat	k5eNaPmIp3nP
v	v	k7c6
informačních	informační	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
verze	verze	k1gFnSc1
VISION	vision	k1gInSc4
produktů	produkt	k1gInPc2
měla	mít	k5eAaImAgFnS
zvládat	zvládat	k5eAaImF
některé	některý	k3yIgFnPc4
činnosti	činnost	k1gFnPc4
(	(	kIx(
<g/>
multimedia	multimedium	k1gNnSc2
<g/>
,	,	kIx,
multimedia	multimedium	k1gNnSc2
a	a	k8xC
hry	hra	k1gFnSc2
...	...	k?
<g/>
,	,	kIx,
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prodeji	prodej	k1gInSc3
výrobních	výrobní	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
GlobalFoundries	GlobalFoundriesa	k1gFnPc2
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
i	i	k9
nová	nový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Investment	Investment	k1gInSc4
Company	Compana	k1gFnSc2
(	(	kIx(
<g/>
ATIC	ATIC	kA
=	=	kIx~
Advanced	Advanced	k1gMnSc1
Technology	technolog	k1gMnPc7
Investment	Investment	k1gInSc1
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
díky	díky	k7c3
tomuto	tento	k3xDgInSc3
kroku	krok	k1gInSc3
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
AMD	AMD	kA
už	už	k6eAd1
jenom	jenom	k9
plně	plně	k6eAd1
věnovat	věnovat	k5eAaImF,k5eAaPmF
vývoji	vývoj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
AMD	AMD	kA
vyrobila	vyrobit	k5eAaPmAgFnS
pětisetmiliontý	pětisetmiliontý	k2eAgInSc4d1
procesor	procesor	k1gInSc4
architektury	architektura	k1gFnSc2
x	x	k?
<g/>
86	#num#	k4
a	a	k8xC
taky	taky	k6eAd1
padesátimiliontý	padesátimiliontý	k2eAgInSc1d1
grafický	grafický	k2eAgInSc1d1
čip	čip	k1gInSc1
AMD	AMD	kA
Hollywood	Hollywood	k1gInSc4
použitý	použitý	k2eAgInSc4d1
v	v	k7c4
konzoly	konzola	k1gFnPc4
Nintento	Nintento	k1gNnSc4
Wii	Wii	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedla	uvést	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
pro	pro	k7c4
procesory	procesor	k1gInPc4
do	do	k7c2
ultra	ultra	k2eAgInPc2d1
tenkých	tenký	k2eAgInPc2d1
notebooků	notebook	k1gInPc2
pod	pod	k7c7
označením	označení	k1gNnSc7
Yukon	Yukona	k1gFnPc2
(	(	kIx(
<g/>
CPU	CPU	kA
AMD	AMD	kA
Athlon	Athlon	k1gInSc4
Neo	Neo	k1gFnSc2
<g/>
,	,	kIx,
iGPU	iGPU	k?
Radeon	Radeon	k1gMnSc1
X1250	X1250	k1gMnSc1
nebo	nebo	k8xC
Radeon	Radeon	k1gMnSc1
HD	HD	kA
3410	#num#	k4
Mobility	mobilita	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedla	uvést	k5eAaPmAgFnS
i	i	k9
novou	nový	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
Dragon	Dragon	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
kombinuje	kombinovat	k5eAaImIp3nS
procesor	procesor	k1gInSc4
AMD	AMD	kA
Phenom	Phenom	k1gInSc1
II	II	kA
X	X	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
grafickou	grafický	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
Radeon	Radeon	k1gInSc1
HD	HD	kA
4800	#num#	k4
a	a	k8xC
čipset	čipset	k1gMnSc1
série	série	k1gFnSc2
AMD	AMD	kA
700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
získala	získat	k5eAaPmAgFnS
zlaté	zlatý	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
U.	U.	kA
<g/>
S.	S.	kA
Green	Green	k2eAgInSc4d1
Building	Building	k1gInSc4
Council	Council	k1gMnSc1
Leadership	Leadership	k1gMnSc1
in	in	k?
Energy	Energ	k1gInPc1
and	and	k?
Environmental	Environmental	k1gMnSc1
Design	design	k1gInSc1
(	(	kIx(
<g/>
LEED	LEED	kA
<g/>
)	)	kIx)
pro	pro	k7c4
pobočku	pobočka	k1gFnSc4
ve	v	k7c6
městě	město	k1gNnSc6
Austin	Austina	k1gFnPc2
(	(	kIx(
<g/>
nejekologičtější	ekologický	k2eAgFnSc1d3
budova	budova	k1gFnSc1
v	v	k7c6
Texasu	Texas	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
společnost	společnost	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
Randy	rand	k1gInPc1
Allen	Allen	k1gMnSc1
na	na	k7c6
pozici	pozice	k1gFnSc6
senior	senior	k1gMnSc1
více	hodně	k6eAd2
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
společnosti	společnost	k1gFnSc6
AMD	AMD	kA
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Éra	éra	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
získání	získání	k1gNnSc6
produktů	produkt	k1gInPc2
na	na	k7c6
trhu	trh	k1gInSc6
GPU	GPU	kA
a	a	k8xC
zvolení	zvolení	k1gNnSc4
nového	nový	k2eAgInSc2d1
CEO	CEO	kA
</s>
<s>
Na	na	k7c6
veletrhu	veletrh	k1gInSc6
Computex	Computex	k1gInSc4
2010	#num#	k4
předvedla	předvést	k5eAaPmAgFnS
první	první	k4xOgInSc4
procesor	procesor	k1gInSc4
řady	řada	k1gFnSc2
AMD	AMD	kA
Fusion	Fusion	k1gInSc1
(	(	kIx(
<g/>
APU	APU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spustila	spustit	k5eAaPmAgFnS
program	program	k1gInSc4
AMD	AMD	kA
Fusion	Fusion	k1gInSc1
Fund	fund	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
na	na	k7c6
optimalizaci	optimalizace	k1gFnSc6
programů	program	k1gInPc2
pro	pro	k7c4
procesory	procesor	k1gInPc4
AMD	AMD	kA
Fusion	Fusion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizovala	aktualizovat	k5eAaBmAgFnS
platformu	platforma	k1gFnSc4
VISION	vision	k1gInSc1
a	a	k8xC
přidala	přidat	k5eAaPmAgFnS
nové	nový	k2eAgFnPc4d1
značky	značka	k1gFnPc4
VISION	vision	k1gInSc4
Pro	pro	k7c4
(	(	kIx(
<g/>
notebooky	notebook	k1gInPc4
Lenovo	Lenův	k2eAgNnSc1d1
ThinkPad	ThinkPad	k1gInSc4
X	X	kA
<g/>
100	#num#	k4
<g/>
e	e	k0
a	a	k8xC
řada	řada	k1gFnSc1
ThinkPad	ThinkPad	k1gInSc1
Edge	Edge	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
od	od	k7c2
provincie	provincie	k1gFnSc2
Ontario	Ontario	k1gNnSc1
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
pětiletý	pětiletý	k2eAgInSc4d1
grant	grant	k1gInSc4
52,8	52,8	k4
milionu	milion	k4xCgInSc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
56,4	56,4	k4
milionu	milion	k4xCgInSc2
kanadských	kanadský	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
na	na	k7c4
vývoj	vývoj	k1gInSc4
dalších	další	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
řady	řada	k1gFnSc2
AMD	AMD	kA
Fusion	Fusion	k1gInSc4
a	a	k8xC
programovou	programový	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
září	září	k1gNnSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
přestala	přestat	k5eAaPmAgFnS
používat	používat	k5eAaImF
u	u	k7c2
svých	svůj	k3xOyFgInPc2
GPU	GPU	kA
oficiálně	oficiálně	k6eAd1
jmenovku	jmenovka	k1gFnSc4
ATI	ATI	kA
z	z	k7c2
důvodu	důvod	k1gInSc2
vydání	vydání	k1gNnSc2
procesorů	procesor	k1gInPc2
Fusion	Fusion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejným	stejný	k2eAgNnPc3d1
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
Leo	Leo	k1gMnSc1
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInSc1d1
procesor	procesor	k1gInSc1
Phenom	Phenom	k1gInSc1
II	II	kA
X	X	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
grafickou	grafický	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
Radeon	Radeon	k1gInSc1
HD	HD	kA
5000	#num#	k4
a	a	k8xC
čipset	čipset	k1gMnSc1
řady	řada	k1gFnSc2
AMD	AMD	kA
800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
nadále	nadále	k6eAd1
používá	používat	k5eAaImIp3nS
pouze	pouze	k6eAd1
AMD	AMD	kA
Radeon	Radeona	k1gFnPc2
HD	HD	kA
xxxx	xxxx	k1gInSc1
(	(	kIx(
<g/>
srpen	srpen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
byla	být	k5eAaImAgFnS
3	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgMnSc7d3
výrobcem	výrobce	k1gMnSc7
GPU	GPU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
předehnala	předehnat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
NVIDIA	NVIDIA	kA
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
o	o	k7c4
pár	pár	k4xCyI
procent	procento	k1gNnPc2
(	(	kIx(
<g/>
červen	červen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
zase	zase	k9
začala	začít	k5eAaPmAgFnS
ztrácet	ztrácet	k5eAaImF
a	a	k8xC
v	v	k7c6
Q1	Q1	k1gFnSc6
2011	#num#	k4
se	se	k3xPyFc4
zase	zase	k9
propadla	propadnout	k5eAaPmAgFnS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
nový	nový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
prozatímní	prozatímní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
společnosti	společnost	k1gFnSc2
Thomas	Thomas	k1gMnSc1
Seifert	Seifert	k1gMnSc1
<g/>
,	,	kIx,
potom	potom	k6eAd1
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
nečekaně	nečekaně	k6eAd1
rezignoval	rezignovat	k5eAaBmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Dirk	Dirk	k1gMnSc1
Meyer	Meyer	k1gMnSc1
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
pozici	pozice	k1gFnSc6
necelé	celý	k2eNgInPc4d1
2	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2011	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
nový	nový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
společnosti	společnost	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Rory	Rora	k1gFnSc2
Read	Read	k1gMnSc1
<g/>
,	,	kIx,
přešel	přejít	k5eAaPmAgMnS
ze	z	k7c2
společnosti	společnost	k1gFnSc2
Lenovo	Lenovo	k1gNnSc1
<g/>
,	,	kIx,
nahradil	nahradit	k5eAaPmAgMnS
prozatímního	prozatímní	k2eAgMnSc4d1
ředitele	ředitel	k1gMnSc4
společnosti	společnost	k1gFnSc2
Thomas	Thomas	k1gMnSc1
Seifert	Seifert	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
funkci	funkce	k1gFnSc6
přes	přes	k7c4
0,5	0,5	k4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společnost	společnost	k1gFnSc1
Spansion	Spansion	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byla	být	k5eAaImAgFnS
spolu	spolu	k6eAd1
se	s	k7c7
společností	společnost	k1gFnSc7
Fujitsu	Fujits	k1gInSc2
založena	založen	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Spansion	Spansion	k1gInSc4
na	na	k7c4
vývoj	vývoj	k1gInSc4
a	a	k8xC
produkci	produkce	k1gFnSc4
NOR	Nor	k1gMnSc1
Flash	Flasha	k1gFnPc2
pamětí	paměť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
společnost	společnost	k1gFnSc1
Spansion	Spansion	k1gInSc1
získala	získat	k5eAaPmAgFnS
další	další	k2eAgInSc4d1
partnery	partner	k1gMnPc7
AMD	AMD	kA
forms	formsa	k1gFnPc2
FASL	FASL	kA
a	a	k8xC
LLC	LLC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
společnost	společnost	k1gFnSc1
Spansion	Spansion	k1gInSc4
změnila	změnit	k5eAaPmAgFnS
právní	právní	k2eAgFnSc4d1
formu	forma	k1gFnSc4
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
převzala	převzít	k5eAaPmAgFnS
továrnu	továrna	k1gFnSc4
na	na	k7c6
adrese	adresa	k1gFnSc6
915	#num#	k4
DeGuigne	DeGuign	k1gInSc5
ve	v	k7c6
městě	město	k1gNnSc6
Sunnyvale	Sunnyvala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
firmy	firma	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1
a	a	k8xC
vývojová	vývojový	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
skončily	skončit	k5eAaPmAgFnP
jednání	jednání	k1gNnPc1
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
AMD	AMD	kA
na	na	k7c4
výrobní	výrobní	k2eAgFnSc4d1
a	a	k8xC
vývojovou	vývojový	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
souhlas	souhlas	k1gInSc4
s	s	k7c7
rozdělením	rozdělení	k1gNnSc7
AMD	AMD	kA
a	a	k8xC
odprodejem	odprodej	k1gInSc7
66	#num#	k4
<g/>
%	%	kIx~
podílu	podíl	k1gInSc2
vládě	vláda	k1gFnSc3
Abu	Abu	k1gFnSc3
Dhabi	Dhab	k1gFnSc3
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
firma	firma	k1gFnSc1
GlobalFoundries	GlobalFoundries	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
GlobalFoundries	GlobalFoundriesa	k1gFnPc2
bude	být	k5eAaImBp3nS
pro	pro	k7c4
AMD	AMD	kA
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ostatní	ostatní	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
vyrábět	vyrábět	k5eAaImF
procesory	procesor	k1gInPc4
v	v	k7c6
továrnách	továrna	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
dříve	dříve	k6eAd2
vlastnila	vlastnit	k5eAaImAgFnS
firma	firma	k1gFnSc1
AMD	AMD	kA
a	a	k8xC
dalších	další	k2eAgMnPc6d1
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
stavět	stavět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
firmy	firma	k1gFnSc2
na	na	k7c6
oddělení	oddělení	k1gNnSc6
</s>
<s>
(	(	kIx(
<g/>
květen	květen	k1gInSc4
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
GPU	GPU	kA
a	a	k8xC
CPU	CPU	kA
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
Rick	Rick	k1gMnSc1
Bergman	Bergman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Advanced	Advanced	k1gInSc1
Technology	technolog	k1gMnPc4
Group	Group	k1gInSc1
<g/>
“	“	k?
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Nigel	Nigel	k1gInSc4
Dessau	Dessaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
budoucích	budoucí	k2eAgFnPc2d1
technologických	technologický	k2eAgFnPc2d1
inovací	inovace	k1gFnPc2
a	a	k8xC
marketingové	marketingový	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
</s>
<s>
Styk	styk	k1gInSc1
se	s	k7c7
zákazníky	zákazník	k1gMnPc7
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
Emilop	Emilop	k1gInSc1
Ghilardi	Ghilard	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Odprodej	odprodej	k1gInSc1
továren	továrna	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Před	před	k7c7
odprodejem	odprodej	k1gInSc7
továren	továrna	k1gFnPc2
firmě	firma	k1gFnSc3
GlobalFoundries	GlobalFoundries	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
mělo	mít	k5eAaImAgNnS
AMD	AMD	kA
následující	následující	k2eAgFnPc4d1
továrny	továrna	k1gFnPc4
<g/>
:	:	kIx,
Fab	Fab	k1gFnSc1
36	#num#	k4
a	a	k8xC
Fab	Fab	k1gMnSc3
30	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
(	(	kIx(
<g/>
Drážďany	Drážďany	k1gInPc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
později	pozdě	k6eAd2
přejmenována	přejmenován	k2eAgFnSc1d1
na	na	k7c6
Fab	Fab	k1gFnSc6
1	#num#	k4
a	a	k8xC
Fab	Fab	k1gFnSc1
<g/>
4	#num#	k4
<g/>
x	x	k?
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
později	pozdě	k6eAd2
přejmenována	přejmenován	k2eAgFnSc1d1
na	na	k7c6
Fab	Fab	k1gFnSc6
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
vývoje	vývoj	k1gInSc2
CPU	CPU	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Procesory	procesor	k1gInPc1
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
<g/>
#	#	kIx~
<g/>
Historie	historie	k1gFnSc1
vývoje	vývoj	k1gInSc2
CPU	CPU	kA
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
(	(	kIx(
<g/>
dále	daleko	k6eAd2
AMD	AMD	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
založili	založit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
dřívější	dřívější	k2eAgFnSc4d1
výkonní	výkonný	k2eAgMnPc1d1
manažeři	manažer	k1gMnPc1
ze	z	k7c2
společnosti	společnost	k1gFnSc2
Fairchild	Fairchild	k1gMnSc1
Semiconductor	Semiconductor	k1gMnSc1
–	–	k?
byli	být	k5eAaImAgMnP
jimi	on	k3xPp3gMnPc7
Jerry	Jerr	k1gMnPc7
Sanders	Sanders	k1gInSc4
III	III	kA
<g/>
,	,	kIx,
Ed	Ed	k1gMnSc1
Turney	Turnea	k1gFnSc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Carey	Carea	k1gFnSc2
<g/>
,	,	kIx,
Sven	Sven	k1gNnSc1
Simonsen	Simonsna	k1gFnPc2
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Gifford	Gifford	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1
milníky	milník	k1gInPc1
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
první	první	k4xOgFnSc1
vlastní	vlastní	k2eAgFnSc1d1
produktová	produktový	k2eAgFnSc1d1
řady	řada	k1gFnPc1
čipů	čip	k1gInPc2
AMD	AMD	kA
Am	Am	k1gFnSc1
<g/>
2900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
vylepšené	vylepšený	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
AMD	AMD	kA
K	K	kA
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
vylepšené	vylepšený	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
AMD	AMD	kA
K	K	kA
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
AMD	AMD	kA
K7	K7	k1gMnPc2
(	(	kIx(
<g/>
pojmenovány	pojmenován	k2eAgInPc1d1
AMD	AMD	kA
Athlon	Athlon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
levných	levný	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
AMD	AMD	kA
Duron	Duron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
mobilních	mobilní	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
AMD	AMD	kA
Athlon	Athlon	k1gInSc1
M.	M.	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
AMD	AMD	kA
K8	K8	k1gMnPc2
(	(	kIx(
<g/>
procesory	procesor	k1gInPc4
AMD	AMD	kA
Athlon	Athlon	k1gInSc4
64	#num#	k4
<g/>
,	,	kIx,
AMD	AMD	kA
Opteron	Opteron	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
mobilních	mobilní	k2eAgInPc2d1
64	#num#	k4
<g/>
bitových	bitový	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
AMD	AMD	kA
Turion	Turion	k1gInSc4
64	#num#	k4
a	a	k8xC
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dvoujádrových	dvoujádrový	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
AMD	AMD	kA
Athlon	Athlon	k1gInSc4
64	#num#	k4
X	X	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
vylepšené	vylepšený	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
„	„	k?
<g/>
AMD	AMD	kA
K	k	k7c3
<g/>
10	#num#	k4
<g/>
“	“	k?
(	(	kIx(
<g/>
procesory	procesor	k1gInPc4
AMD	AMD	kA
Opteron	Opteron	k1gInSc4
<g/>
,	,	kIx,
AMD	AMD	kA
Phenom	Phenom	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
vylepšené	vylepšený	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
„	„	k?
<g/>
AMD	AMD	kA
K	k	k7c3
<g/>
10.5	10.5	k4
<g/>
“	“	k?
(	(	kIx(
<g/>
procesory	procesor	k1gInPc4
AMD	AMD	kA
Phenom	Phenom	k1gInSc4
II	II	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
šesti	šest	k4xCc2
jádrových	jádrový	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
AMD	AMD	kA
Phenom	Phenom	k1gInSc1
II	II	kA
X6	X6	k1gMnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
AMD	AMD	kA
Bulldozer	Bulldozer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
vylepšené	vylepšený	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
AMD	AMD	kA
Piledriver	Piledriver	k1gMnSc1
(	(	kIx(
<g/>
nástupce	nástupce	k1gMnSc1
jádra	jádro	k1gNnSc2
Bulldozer	Bulldozra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
procesor	procesor	k1gInSc1
APU	APU	kA
Trinity	Trinit	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
ZEN	zen	k2eAgNnSc1d1
na	na	k7c4
trh	trh	k1gInSc4
s	s	k7c7
kódovým	kódový	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Ryzen	ryzna	k1gFnPc2
(	(	kIx(
<g/>
Summit	summit	k1gInSc1
Ridge	Ridg	k1gInSc2
<g/>
,	,	kIx,
K	k	k7c3
<g/>
17	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
vývoje	vývoj	k1gInSc2
GPU	GPU	kA
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
ATI	ATI	kA
<g/>
#	#	kIx~
<g/>
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
novou	nový	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
grafických	grafický	k2eAgInPc2d1
čipů	čip	k1gInPc2
ATI	ATI	kA
Radeon	Radeon	k1gMnSc1
R600	R600	k1gFnSc2
letované	letovaný	k2eAgFnPc4d1
na	na	k7c4
grafické	grafický	k2eAgFnPc4d1
karty	karta	k1gFnPc4
Radeon	Radeona	k1gFnPc2
HD	HD	kA
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
verze	verze	k1gFnPc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
pro	pro	k7c4
stolní	stolní	k2eAgInSc4d1
tak	tak	k8xC,k8xS
mobilní	mobilní	k2eAgInSc4d1
segment	segment	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
uvedla	uvést	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
GPU	GPU	kA
Radeon	Radeona	k1gFnPc2
R	R	kA
<g/>
800	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
podporoval	podporovat	k5eAaImAgInS
API	API	kA
DirectX	DirectX	k1gFnSc2
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
grafické	grafický	k2eAgFnPc4d1
karty	karta	k1gFnPc4
AMD	AMD	kA
FireStream	FireStream	k1gInSc4
9350	#num#	k4
a	a	k8xC
9370	#num#	k4
s	s	k7c7
výkonem	výkon	k1gInSc7
až	až	k9
2,64	2,64	k4
TFlops	TFlopsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkoupení	odkoupení	k1gNnSc1
ATI	ATI	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2006	#num#	k4
AMD	AMD	kA
dokončilo	dokončit	k5eAaPmAgNnS
převzetí	převzetí	k1gNnSc1
společnosti	společnost	k1gFnSc2
ATI	ATI	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyvíjela	vyvíjet	k5eAaImAgFnS
grafické	grafický	k2eAgInPc4d1
čipy	čip	k1gInPc4
<g/>
,	,	kIx,
čipové	čipový	k2eAgFnPc4d1
sady	sada	k1gFnPc4
a	a	k8xC
komponenty	komponent	k1gInPc4
pro	pro	k7c4
spotřební	spotřební	k2eAgFnSc4d1
elektroniku	elektronika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odkoupení	odkoupení	k1gNnSc1
stálo	stát	k5eAaImAgNnS
AMD	AMD	kA
celkově	celkově	k6eAd1
5,4	5,4	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
<g/>
,	,	kIx,
4,3	4,3	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
z	z	k7c2
pokladny	pokladna	k1gFnSc2
a	a	k8xC
58	#num#	k4
miliónů	milión	k4xCgInPc2
svých	svůj	k3xOyFgFnPc2
akcií	akcie	k1gFnPc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
1,1	1,1	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
částka	částka	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
nadsazená	nadsazený	k2eAgFnSc1d1
a	a	k8xC
společnosti	společnost	k1gFnSc6
AMD	AMD	kA
to	ten	k3xDgNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
finanční	finanční	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
odkoupení	odkoupení	k1gNnSc3
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
více	hodně	k6eAd2
konkurence	konkurence	k1gFnSc2
schopné	schopný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
převzetí	převzetí	k1gNnSc3
ATI	ATI	kA
má	mít	k5eAaImIp3nS
dnes	dnes	k6eAd1
celou	celý	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
CPU-čipset-GPU	CPU-čipset-GPU	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
mohla	moct	k5eAaImAgFnS
začít	začít	k5eAaPmF
vyvíjet	vyvíjet	k5eAaImF
SoC	soc	kA
řešení	řešení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odkoupení	odkoupení	k1gNnSc6
prodalo	prodat	k5eAaPmAgNnS
AMD	AMD	kA
část	část	k1gFnSc4
ATI	ATI	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyvíjela	vyvíjet	k5eAaImAgFnS
grafické	grafický	k2eAgInPc4d1
čipy	čip	k1gInPc4
pro	pro	k7c4
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
a	a	k8xC
procesory	procesor	k1gInPc1
ARM	ARM	kA
<g/>
.	.	kIx.
</s>
<s>
Byla	být	k5eAaImAgNnP
také	také	k9
vedena	veden	k2eAgNnPc1d1
jednání	jednání	k1gNnPc1
s	s	k7c7
firmou	firma	k1gFnSc7
NVIDIA	NVIDIA	kA
<g/>
,	,	kIx,
ale	ale	k8xC
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
se	se	k3xPyFc4
nedokázalo	dokázat	k5eNaPmAgNnS
dohodnout	dohodnout	k5eAaPmF
na	na	k7c6
ceně	cena	k1gFnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
věcech	věc	k1gFnPc6
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
letech	léto	k1gNnPc6
vyšlo	vyjít	k5eAaPmAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
NVIDIi	NVIDI	k1gFnSc6
má	mít	k5eAaImIp3nS
už	už	k6eAd1
velmi	velmi	k6eAd1
dlouho	dlouho	k6eAd1
kupní	kupní	k2eAgFnSc4d1
moc	moc	k1gFnSc4
nejdřív	dříve	k6eAd3
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
jeden	jeden	k4xCgInSc4
z	z	k7c2
dalších	další	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
technologií	technologie	k1gFnPc2
</s>
<s>
Éra	éra	k1gFnSc1
začátku	začátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
generace	generace	k1gFnSc2
technologií	technologie	k1gFnPc2
CrossFire	CrossFir	k1gInSc5
<g/>
,	,	kIx,
Cool	Cool	k1gMnSc1
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
<g/>
Quiet	Quieta	k1gFnPc2
a	a	k8xC
EyeFinity	EyeFinita	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
AMD	AMD	kA
PowerNow	PowerNow	k1gMnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
použita	použít	k5eAaPmNgFnS
v	v	k7c4
CPU	CPU	kA
AMD	AMD	kA
K	k	k7c3
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
vyvinutá	vyvinutý	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
AMD	AMD	kA
HyperTransport	HyperTransport	k1gInSc4
je	být	k5eAaImIp3nS
přijata	přijmout	k5eAaPmNgFnS
firmami	firma	k1gFnPc7
Agilent	Agilent	k1gInSc1
<g/>
,	,	kIx,
Apple	Apple	kA
Computer	computer	k1gInSc1
<g/>
,	,	kIx,
Broadcom	Broadcom	k1gInSc1
<g/>
,	,	kIx,
Cisco	Cisco	k1gNnSc1
Systems	Systemsa	k1gFnPc2
<g/>
,	,	kIx,
IBM	IBM	kA
<g/>
,	,	kIx,
nVidia	nVidium	k1gNnSc2
<g/>
,	,	kIx,
Sun	Sun	kA
a	a	k8xC
Texas	Texas	kA
Instruments	Instruments	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2002	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
AMD	AMD	kA
Cool	Coola	k1gFnPc2
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
<g/>
Quiet	Quiet	k1gInSc4
(	(	kIx(
<g/>
použita	použít	k5eAaPmNgFnS
v	v	k7c4
CPU	CPU	kA
Athlon	Athlon	k1gInSc4
XP	XP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
vylepšenou	vylepšený	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
CrossFire	CrossFir	k1gInSc5
a	a	k8xC
novou	nový	k2eAgFnSc7d1
AMD	AMD	kA
Live	Live	k1gFnSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
pro	pro	k7c4
multimediální	multimediální	k2eAgFnSc4d1
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
2	#num#	k4
roky	rok	k1gInPc7
později	pozdě	k6eAd2
uvedla	uvést	k5eAaPmAgFnS
rozšíření	rozšíření	k1gNnSc4
AMD	AMD	kA
LIVE	LIVE	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
Explorer	Explorer	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
umožnil	umožnit	k5eAaPmAgInS
zlepšit	zlepšit	k5eAaPmF
HD	HD	kA
obsah	obsah	k1gInSc4
na	na	k7c6
domácích	domácí	k1gFnPc6
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
taky	taky	k6eAd1
představila	představit	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
AMD	AMD	kA
Eyefinity	Eyefinita	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
zapojit	zapojit	k5eAaPmF
až	až	k9
6	#num#	k4
monitorů	monitor	k1gInPc2
na	na	k7c4
6	#num#	k4
výstupů	výstup	k1gInPc2
z	z	k7c2
jedné	jeden	k4xCgFnSc2
grafické	grafický	k2eAgFnSc2d1
karty	karta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vydáním	vydání	k1gNnSc7
procesorů	procesor	k1gInPc2
architektury	architektura	k1gFnSc2
Zen	zen	k2eAgFnPc1d1
přibyly	přibýt	k5eAaPmAgFnP
také	také	k9
technologie	technologie	k1gFnPc1
Pure	Pur	k1gInSc2
Power	Power	k1gInSc1
<g/>
,	,	kIx,
Precision	Precision	k1gInSc1
Boost	Boost	k1gInSc1
<g/>
,	,	kIx,
Extended	Extended	k1gInSc1
Frequency	Frequenca	k1gFnSc2
Range	Rang	k1gFnSc2
(	(	kIx(
<g/>
XFR	XFR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Neural	Neural	k1gMnSc1
Net	Net	k1gFnSc2
Prediction	Prediction	k1gInSc1
a	a	k8xC
Smart	Smart	k1gInSc1
Prefetch	Prefetcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
Zen	zen	k2eAgMnPc7d1
<g/>
+	+	kIx~
a	a	k8xC
X470	X470	k1gMnSc7
chipestem	chipest	k1gMnSc7
přibylo	přibýt	k5eAaPmAgNnS
StoreMI	StoreMI	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
grafickými	grafický	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
Navi	Nav	k1gFnSc2
přibylo	přibýt	k5eAaPmAgNnS
Fidelity	Fidelita	k1gFnPc4
FX	FX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
jsou	být	k5eAaImIp3nP
na	na	k7c6
trhu	trh	k1gInSc6
také	také	k9
monitory	monitor	k1gInPc1
s	s	k7c7
podporou	podpora	k1gFnSc7
FreeSync	FreeSync	k1gFnSc1
a	a	k8xC
FreeSync	FreeSync	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
de	de	k?
facto	facto	k1gNnSc1
vylepšájí	vylepšájet	k5eAaImIp3nS,k5eAaPmIp3nS
funkci	funkce	k1gFnSc4
vertikální	vertikální	k2eAgFnSc2d1
synchronizace	synchronizace	k1gFnSc2
(	(	kIx(
<g/>
Vsync	Vsync	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
i	i	k9
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zranitelnost	zranitelnost	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
2020	#num#	k4
byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
letitá	letitý	k2eAgFnSc1d1
zranitelnost	zranitelnost	k1gFnSc1
procesorů	procesor	k1gInPc2
AMD	AMD	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
extrahovat	extrahovat	k5eAaBmF
klíče	klíč	k1gInSc2
AES	AES	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Výpočet	výpočet	k1gInSc1
podle	podle	k7c2
kvartálů	kvartál	k1gInPc2
<g/>
1	#num#	k4
2	#num#	k4
Údaje	údaj	k1gInSc2
jsou	být	k5eAaImIp3nP
vzaty	vzít	k5eAaPmNgFnP
z	z	k7c2
textu	text	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
nemají	mít	k5eNaImIp3nP
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodané	dodaný	k2eAgFnPc4d1
citace	citace	k1gFnPc4
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
obecné	obecný	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
měly	mít	k5eAaImAgFnP
by	by	kYmCp3nP
stačit	stačit	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
AMD	AMD	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
Corporate	Corporat	k1gInSc5
Information	Information	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
AMD	AMD	kA
Reports	Reportsa	k1gFnPc2
Fourth	Fourtha	k1gFnPc2
Quarter	quarter	k1gInSc1
and	and	k?
Annual	Annual	k1gInSc4
2020	#num#	k4
Financial	Financial	k1gInSc1
Results	Results	k1gInSc4
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
AMD	AMD	kA
Reports	Reportsa	k1gFnPc2
Fourth	Fourtha	k1gFnPc2
Quarter	quarter	k1gInSc1
and	and	k?
Annual	Annual	k1gInSc4
2019	#num#	k4
Financial	Financial	k1gInSc1
Results	Results	k1gInSc4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
AMD	AMD	kA
Reports	Reports	k1gInSc1
Fourth	Fourth	k1gInSc1
Quarter	quarter	k1gInSc1
and	and	k?
Annual	Annual	k1gInSc4
2019	#num#	k4
Financial	Financial	k1gInSc1
Results	Results	k1gInSc4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
www.bizjournals.com	www.bizjournals.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Spansion	Spansion	k1gInSc1
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
-	-	kIx~
Company	Compan	k1gInPc1
Profile	profil	k1gInSc5
<g/>
,	,	kIx,
Information	Information	k1gInSc1
<g/>
,	,	kIx,
Business	business	k1gInSc1
Description	Description	k1gInSc1
<g/>
,	,	kIx,
History	Histor	k1gInPc1
<g/>
,	,	kIx,
Background	Background	k1gInSc1
Information	Information	k1gInSc1
on	on	k3xPp3gMnSc1
Spansion	Spansion	k1gInSc1
Inc	Inc	k1gFnSc1
<g/>
..	..	k?
www.referenceforbusiness.com	www.referenceforbusiness.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KURZY	Kurz	k1gMnPc7
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
USD	USD	kA
<g/>
,	,	kIx,
americký	americký	k2eAgInSc1d1
dolar	dolar	k1gInSc1
-	-	kIx~
převod	převod	k1gInSc1
měn	měna	k1gFnPc2
na	na	k7c6
CZK	CZK	kA
<g/>
,	,	kIx,
českou	český	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
|	|	kIx~
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.kurzy.cz	www.kurzy.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
(	(	kIx(
<g/>
AMD	AMD	kA
<g/>
)	)	kIx)
|	|	kIx~
History	Histor	k1gInPc1
&	&	k?
Facts	Facts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AMD	AMD	kA
Reports	Reports	k1gInSc1
Fourth	Fourth	k1gInSc1
Quarter	quarter	k1gInSc1
and	and	k?
Full	Full	k1gMnSc1
Year	Year	k1gMnSc1
2020	#num#	k4
Financial	Financial	k1gInSc1
Results	Results	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
–	–	k?
about	about	k1gMnSc1
us	us	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOOD	HOOD	kA
<g/>
,	,	kIx,
Vic	Vic	k1gFnSc1
<g/>
;	;	kIx,
PINO	PINO	kA
<g/>
,	,	kIx,
Nick	Nick	k1gMnSc1
<g/>
;	;	kIx,
MAY	May	k1gMnSc1
2021	#num#	k4
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
Vjestica	Vjestica	k1gMnSc1
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
Series	Series	k1gInSc4
X	X	kA
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechRadar	TechRadar	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LYNN	LYNN	kA
<g/>
,	,	kIx,
Lottie	Lottie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurogamer	Eurogamer	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-09-17	2020-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
IHS	IHS	kA
iSuppli	iSuppnout	k5eAaPmAgMnP
Market	market	k1gInSc4
Intelligence	Intelligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semiconductor	Semiconductor	k1gInSc1
market	market	k1gInSc4
declines	declines	k1gInSc4
less	lessa	k1gFnPc2
than	than	k1gMnSc1
expected	expected	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-11-23	2009-11-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Executive	Executiv	k1gInSc5
Biography	Biograph	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Executive	Executiv	k1gInSc5
Biography	Biograph	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
History	Histor	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
TG	tg	kA
Daily	Daila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Timeline	Timelin	k1gInSc5
<g/>
:	:	kIx,
How	How	k1gMnSc1
AMD	AMD	kA
changed	changed	k1gMnSc1
over	over	k1gMnSc1
the	the	k?
past	past	k1gFnSc4
39	#num#	k4
years	yearsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-10-07	2008-10-07	k4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Submicron	Submicron	k1gMnSc1
Development	Development	k1gMnSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
SDC	SDC	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
EE	EE	kA
Times	Timesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
fabricates	fabricatesa	k1gFnPc2
double-gate	double-gat	k1gInSc5
transistor	transistor	k1gInSc1
for	forum	k1gNnPc2
10	#num#	k4
<g/>
-nm	-nm	k?
designs	designs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-09-09	2002-09-09	k4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
9	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Wired	Wired	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robot	robot	k1gMnSc1
Obstetric	Obstetric	k1gMnSc1
Wards	Wards	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-10-07	2008-10-07	k4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
Locations	Locationsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Maps	Maps	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
z	z	k7c2
odkazu	odkaz	k1gInSc2
na	na	k7c6
AMD	AMD	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odkaz	odkaz	k1gInSc4
na	na	k7c4
Google	Google	k1gNnSc4
mapy	mapa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
podle	podle	k7c2
umístěný	umístěný	k2eAgInSc4d1
značky	značka	k1gFnSc2
adresy	adresa	k1gFnSc2
beru	brát	k5eAaImIp1nS
GPS	GPS	kA
souřadnice	souřadnice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nemusí	muset	k5eNaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
úplně	úplně	k6eAd1
přesné	přesný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deep	Deep	k1gInSc1
in	in	k?
IT.	IT.	k1gFnSc2
ARM	ARM	kA
s	s	k7c7
AMD	AMD	kA
ohlásily	ohlásit	k5eAaPmAgFnP
partnerství	partnerství	k1gNnSc4
pro	pro	k7c4
společnou	společný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
OpenCL	OpenCL	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-06-17	2011-06-17	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
epicenter	epicentrum	k1gNnPc2
of	of	k?
heterogeneous	heterogeneous	k1gMnSc1
computing	computing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SemiAccurate	SemiAccurat	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exclusive	Exclusiev	k1gFnPc1
<g/>
:	:	kIx,
AMD	AMD	kA
far	fara	k1gFnPc2
future	futur	k1gMnSc5
prototype	prototyp	k1gInSc5
GPU	GPU	kA
pictured	pictured	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-10-27	2011-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deep	Deep	k1gInSc1
in	in	k?
IT.	IT.	k1gFnSc2
AMD	AMD	kA
s	s	k7c7
Amkor	Amkor	k1gMnSc1
pracují	pracovat	k5eAaImIp3nP
na	na	k7c6
grafickém	grafický	k2eAgInSc6d1
čipu	čip	k1gInSc6
budoucnosti	budoucnost	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-01	2011-11-01	k4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hi-Fi	Hi-Fi	k1gNnSc4
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodaření	hospodaření	k1gNnSc1
AMD	AMD	kA
za	za	k7c4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Q.	Q.	kA
2008	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-04-19	2008-04-19	k4
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
zkopírovanou	zkopírovaný	k2eAgFnSc4d1
URL	URL	kA
už	už	k6eAd1
neexistujícího	existující	k2eNgInSc2d1
článku	článek	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
velmi	velmi	k6eAd1
mnoho	mnoho	k4c1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
doplněk	doplněk	k1gInSc1
pro	pro	k7c4
ref	ref	k?
<g/>
=	=	kIx~
<g/>
finance_federman_	finance_federman_	k?
<g/>
1	#num#	k4
<g/>
Q	Q	kA
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
proto	proto	k8xC
datum	datum	k1gNnSc1
přístupu	přístup	k1gInSc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
vzniku	vznik	k1gInSc2
samotného	samotný	k2eAgInSc2d1
článku	článek	k1gInSc2
<g/>
..	..	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
YAHOO	YAHOO	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
AMD	AMD	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
pro	pro	k7c4
ref	ref	k?
<g/>
=	=	kIx~
<g/>
finance_federman_	finance_federman_	k?
<g/>
1	#num#	k4
<g/>
Q	Q	kA
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
proto	proto	k8xC
datum	datum	k1gNnSc1
přístupu	přístup	k1gInSc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
vzniku	vznik	k1gInSc2
samotného	samotný	k2eAgInSc2d1
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deep	Deep	k1gInSc1
in	in	k?
IT.	IT.	k1gFnSc4
Výsledky	výsledek	k1gInPc4
hospodaření	hospodaření	k1gNnSc2
firem	firma	k1gFnPc2
AMD	AMD	kA
a	a	k8xC
Intel	Intel	kA
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-01-22	2010-01-22	k4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
Reports	Reports	k1gInSc1
Fourth	Fourth	k1gInSc1
Quarter	quarter	k1gInSc1
Results	Results	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-01-21	2010-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DDworld	DDworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodeje	prodej	k1gInSc2
grafik	grafika	k1gFnPc2
klesají	klesat	k5eAaImIp3nP
–	–	k?
NVIDIA	NVIDIA	kA
uspěla	uspět	k5eAaPmAgFnS
s	s	k7c7
cenovou	cenový	k2eAgFnSc7d1
strategií	strategie	k1gFnSc7
proti	proti	k7c3
AMD	AMD	kA
v	v	k7c6
mobilním	mobilní	k2eAgInSc6d1
segmentu	segment	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-03	2011-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jon	Jon	k1gMnSc1
Peddie	Peddie	k1gFnSc2
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jon	Jon	k1gFnSc1
Peddie	Peddie	k1gFnSc1
Research	Research	k1gInSc4
reports	reports	k1gInSc1
disappointing	disappointing	k1gInSc1
4	#num#	k4
<g/>
th	th	k?
quarter	quarter	k1gInSc1
<g/>
:	:	kIx,
PC	PC	kA
Graphics	Graphics	k1gInSc4
shipments	shipmentsa	k1gFnPc2
down	down	k1gNnSc1
7.8	7.8	k4
<g/>
%	%	kIx~
year	year	k1gMnSc1
over	over	k1gMnSc1
year	year	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-01-31	2011-01-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ColorFire	ColorFir	k1gInSc5
RX	RX	kA
570	#num#	k4
Ustorm	Ustorm	k1gInSc1
Specs	Specs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dataland	Dataland	k1gInSc1
RX	RX	kA
570	#num#	k4
Dual	Dualum	k1gNnPc2
Cool	Coola	k1gFnPc2
Specs	Specsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MAXSUN	MAXSUN	kA
RX	RX	kA
570	#num#	k4
Giant	Gianto	k1gNnPc2
Specs	Specsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Product	Product	k1gInSc1
Centre	centr	k1gInSc5
/	/	kIx~
Game	game	k1gInSc1
graphics	graphicsa	k1gFnPc2
card_Shenzhen	card_Shenzhna	k1gFnPc2
Panlei	Panle	k1gFnSc2
Intelligent	Intelligent	k1gMnSc1
Technology	technolog	k1gMnPc4
Co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Ltd	ltd	kA
<g/>
..	..	k?
www.pradeon.com	www.pradeon.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Yeston	Yeston	k1gInSc1
RX	RX	kA
570	#num#	k4
GAMEACE	GAMEACE	kA
Specs	Specsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ONDA	ONDA	kA
RX	RX	kA
570	#num#	k4
Model	modla	k1gFnPc2
Specs	Specsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deep	Deep	k1gMnSc1
in	in	k?
IT.	IT.	k1gMnSc1
Paměti	paměť	k1gFnSc2
AMD	AMD	kA
Radeon	Radeon	k1gInSc1
v	v	k7c6
obchodech	obchod	k1gInPc6
nehledejte	hledat	k5eNaImRp2nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-15	2011-08-15	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Xbit	Xbit	k1gMnSc1
labotories	labotories	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
<g/>
:	:	kIx,
Own-Brand	Own-Brand	k1gInSc1
Memory	Memora	k1gFnSc2
Modules	Modulesa	k1gFnPc2
-	-	kIx~
Probe	Prob	k1gInSc5
of	of	k?
Opportunity	Opportunit	k2eAgFnPc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-10	2011-08-10	k4
10	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deep	Deep	k1gInSc1
in	in	k?
IT.	IT.	k1gFnSc2
AMD	AMD	kA
začíná	začínat	k5eAaImIp3nS
pod	pod	k7c7
značkou	značka	k1gFnSc7
Radeon	Radeon	k1gNnSc4
prodávat	prodávat	k5eAaImF
DDR3	DDR3	k1gFnSc4
paměti	paměť	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-08	2011-08-08	k4
10	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Impress	Impress	k1gInSc1
Watch	Watch	k1gMnSc1
Corporation	Corporation	k1gInSc1
<g/>
.	.	kIx.
珍	珍	k?
<g/>
？	？	k?
<g/>
AMDブ	AMDブ	k1gFnSc7
<g/>
3	#num#	k4
<g/>
メ	メ	k?
</s>
<s>
チ	チ	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-06	2011-08-06	k4
23	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deep	Deep	k1gInSc1
in	in	k?
IT.	IT.	k1gFnSc2
Paměti	paměť	k1gFnSc2
od	od	k7c2
AMD	AMD	kA
nakonec	nakonec	k6eAd1
v	v	k7c6
prodeji	prodej	k1gInSc6
budou	být	k5eAaImBp3nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-29	2011-11-29	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
Memory	Memora	k1gFnSc2
for	forum	k1gNnPc2
Systems	Systems	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PCTuning	PCTuning	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
začne	začít	k5eAaPmIp3nS
prodávat	prodávat	k5eAaImF
DDR3	DDR3	k1gFnSc4
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
Radeon	Radeon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-29	2011-11-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VR-Zone	VR-Zon	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
Radeon	Radeon	k1gMnSc1
Memory	Memora	k1gFnSc2
Goes	Goes	k1gInSc1
Beyond	Beyond	k1gInSc1
Enthusiasts	Enthusiasts	k1gInSc4
Memory	Memora	k1gFnSc2
<g/>
,	,	kIx,
Brings	Brings	k1gInSc4
Overive	Overiev	k1gFnSc2
Support	support	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-28	2011-11-28	k4
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Datasheet	Datasheet	k1gMnSc1
Archive	archiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
AM9300	AM9300	k1gMnSc1
Datasheet	Datasheet	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Wikia	Wikia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
elektrotechniky	elektrotechnika	k1gFnSc2
a	a	k8xC
řemesel	řemeslo	k1gNnPc2
Rožnov	Rožnov	k1gInSc1
pod	pod	k7c7
Radhoštěm	Radhošť	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
AMD	AMD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Spansion	Spansion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spansion	Spansion	k1gInSc1
<g/>
®	®	k?
Locations	Locations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Spansion	Spansion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
About	About	k1gMnSc1
Us	Us	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CPU	CPU	kA
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Datasheet	Datasheet	k1gMnSc1
Archive	archiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
AM9102	AM9102	k1gMnSc1
Datasheet	Datasheet	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AMD	AMD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japan	japan	k1gInSc1
Disaster	Disaster	k1gInSc4
-	-	kIx~
People	People	k1gMnSc1
First	First	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NYSE	NYSE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Public	publicum	k1gNnPc2
<g/>
,	,	kIx,
NYSE	NYSE	kA
<g/>
:	:	kIx,
<g/>
AMD	AMD	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CPU	CPU	kA
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
8086	#num#	k4
microprocessor	microprocessor	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CPU	CPU	kA
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
8088	#num#	k4
microprocessor	microprocessor	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
Valley	Vallea	k1gFnSc2
Historical	Historical	k1gFnSc2
Association	Association	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
Company	Compana	k1gFnSc2
History	Histor	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
cesky	cesky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cheb	Cheb	k1gInSc1
gymnázium	gymnázium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
AMD	AMD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
cesky	cesky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CPU	CPU	kA
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
80386	#num#	k4
microprocessor	microprocessor	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Business	business	k1gInSc1
Wire	Wire	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ASML	ASML	kA
Delivers	Deliversa	k1gFnPc2
Its	Its	k1gFnSc1
First	First	k1gFnSc1
193	#num#	k4
nm	nm	k?
Step	step	k1gInSc1
&	&	k?
Scan	Scan	k1gInSc1
System	Syst	k1gInSc7
to	ten	k3xDgNnSc4
AMD	AMD	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Submicron	Submicron	k1gInSc1
Development	Development	k1gInSc1
Center	centrum	k1gNnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CPU	CPU	kA
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
80486	#num#	k4
microprocessor	microprocessor	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnPc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Justice	justice	k1gFnSc1
Dept	dept	k1gInSc1wB
<g/>
.	.	kIx.
subpoenas	subpoenas	k1gInSc1
AMD	AMD	kA
<g/>
,	,	kIx,
NVIDIA	NVIDIA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-10-06	2008-10-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ExtraHardware	ExtraHardwar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
má	mít	k5eAaImIp3nS
nového	nový	k2eAgMnSc4d1
ředitele	ředitel	k1gMnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-07-18	2008-07-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DDworld	DDworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodeje	prodej	k1gInSc2
grafik	grafika	k1gFnPc2
klesají	klesat	k5eAaImIp3nP
–	–	k?
NVIDIA	NVIDIA	kA
uspěla	uspět	k5eAaPmAgFnS
s	s	k7c7
cenovou	cenový	k2eAgFnSc7d1
strategií	strategie	k1gFnSc7
proti	proti	k7c3
AMD	AMD	kA
v	v	k7c6
mobilním	mobilní	k2eAgInSc6d1
segmentu	segment	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-03	2011-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EXTRAHARDWARE	EXTRAHARDWARE	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dirk	Dirk	k1gMnSc1
Meyer	Meyer	k1gMnSc1
odstupuje	odstupovat	k5eAaImIp3nS
z	z	k7c2
vedení	vedení	k1gNnSc2
AMD	AMD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-02-11	2011-02-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PCTuning	PCTuning	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
má	mít	k5eAaImIp3nS
konečně	konečně	k6eAd1
po	po	k7c6
osmi	osm	k4xCc6
měsících	měsíc	k1gInPc6
nového	nový	k2eAgMnSc4d1
generálního	generální	k2eAgMnSc4d1
ředitele	ředitel	k1gMnSc4
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-25	2011-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DDworld	DDworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
má	mít	k5eAaImIp3nS
konečně	konečně	k6eAd1
nového	nový	k2eAgMnSc4d1
ředitele	ředitel	k1gMnSc4
–	–	k?
bývalý	bývalý	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Lenovo	Lenovo	k1gNnSc4
a	a	k8xC
zaměstnanec	zaměstnanec	k1gMnSc1
IBM	IBM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrá	dobrý	k2eAgFnSc1d1
volba	volba	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-25	2011-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ExtraHardware	ExtraHardwar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rory	Rora	k1gFnSc2
Read	Reada	k1gFnPc2
<g/>
:	:	kIx,
včera	včera	k6eAd1
prezident	prezident	k1gMnSc1
Lenova	Lenov	k1gInSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
CEO	CEO	kA
AMD	AMD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-25	2011-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DDworld	DDworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
NVIDIA	NVIDIA	kA
<g/>
:	:	kIx,
TEGRA	TEGRA	kA
stála	stát	k5eAaImAgFnS
2	#num#	k4
miliardy	miliarda	k4xCgFnSc2
dolarů	dolar	k1gInPc2
–	–	k?
zatím	zatím	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
nezaplatila	zaplatit	k5eNaPmAgFnS
ale	ale	k8xC
má	mít	k5eAaImIp3nS
potenciál	potenciál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-10-24	2011-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://techxplore.com/news/2020-03-amd-processors-susceptible-vulnerabilities-leaks.html	https://techxplore.com/news/2020-03-amd-processors-susceptible-vulnerabilities-leaks.html	k1gMnSc1
-	-	kIx~
AMD	AMD	kA
processors	processors	k6eAd1
susceptible	susceptible	k6eAd1
to	ten	k3xDgNnSc4
security	securita	k1gFnPc1
vulnerabilities	vulnerabilitiesa	k1gFnPc2
<g/>
,	,	kIx,
data	datum	k1gNnSc2
leaks	leaksa	k1gFnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
ARM	ARM	kA
–	–	k?
konkurenční	konkurenční	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
vývoje	vývoj	k1gInSc2
procesorů	procesor	k1gInPc2
</s>
<s>
ATI	ATI	kA
–	–	k?
odkoupená	odkoupený	k2eAgFnSc1d1
firma	firma	k1gFnSc1
</s>
<s>
Intel	Intel	kA
–	–	k?
konkurenční	konkurenční	k2eAgFnSc1d1
firma	firma	k1gFnSc1
hlavně	hlavně	k9
v	v	k7c6
segmentu	segment	k1gInSc6
procesorů	procesor	k1gInPc2
</s>
<s>
NVIDIA	NVIDIA	kA
–	–	k?
konkurenční	konkurenční	k2eAgFnSc1d1
firma	firma	k1gFnSc1
hlavně	hlavně	k9
v	v	k7c6
segmentu	segment	k1gInSc6
grafických	grafický	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
</s>
<s>
VIA	via	k7c4
–	–	k?
konkurenční	konkurenční	k2eAgFnSc1d1
firma	firma	k1gFnSc1
hlavně	hlavně	k9
v	v	k7c6
segmentu	segment	k1gInSc6
procesorů	procesor	k1gInPc2
a	a	k8xC
grafických	grafický	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
</s>
<s>
AMD	AMD	kA
Radeon	Radeon	k1gMnSc1
–	–	k?
vyvíjené	vyvíjený	k2eAgInPc4d1
grafické	grafický	k2eAgInPc4d1
čipy	čip	k1gInPc4
a	a	k8xC
na	na	k7c6
nich	on	k3xPp3gFnPc6
postavené	postavený	k2eAgFnPc1d1
grafické	grafický	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
GPU	GPU	kA
firmy	firma	k1gFnSc2
AMD	AMD	kA
</s>
<s>
Seznam	seznam	k1gInSc1
mikroprocesorů	mikroprocesor	k1gInPc2
AMD	AMD	kA
</s>
<s>
Seznam	seznam	k1gInSc1
technologií	technologie	k1gFnPc2
firmy	firma	k1gFnSc2
AMD	AMD	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devicesa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
Stránka	stránka	k1gFnSc1
s	s	k7c7
novinkami	novinka	k1gFnPc7
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
Stránka	stránka	k1gFnSc1
zabývající	zabývající	k2eAgFnSc1d1
se	se	k3xPyFc4
technologiemi	technologie	k1gFnPc7
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Blog	Blog	k1gInSc1
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
NYSE	NYSE	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Rozlučte	rozloučit	k5eAaPmRp2nP
se	se	k3xPyFc4
s	s	k7c7
ATI	ATI	kA
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
i	i	k9
rozdělení	rozdělení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Jak	jak	k6eAd1
fungují	fungovat	k5eAaImIp3nP
procesory	procesor	k1gInPc1
AMD	AMD	kA
(	(	kIx(
<g/>
video	video	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
ATI	ATI	kA
Stream	Stream	k1gInSc1
Technology	technolog	k1gMnPc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
AMD	AMD	kA
Accelerated	Accelerated	k1gMnSc1
Parallel	Parallel	k1gMnSc1
Processing	Processing	k1gInSc1
(	(	kIx(
<g/>
APP	APP	kA
<g/>
)	)	kIx)
SDK	SDK	kA
(	(	kIx(
<g/>
formerly	formerla	k1gMnSc2
ATI	ATI	kA
Stream	Stream	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Jak	jak	k6eAd1
funguje	fungovat	k5eAaImIp3nS
Turbo	turba	k1gFnSc5
Core	Cor	k1gMnSc4
2.0	2.0	k4
u	u	k7c2
procesorů	procesor	k1gInPc2
AMD	AMD	kA
FX	FX	kA
</s>
<s>
Facebook	Facebook	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
anglická	anglický	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
španělská	španělský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
italská	italský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
turecká	turecký	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
facebooku	facebook	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Advanced	Advanced	k1gInSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gInSc1
Procesory	procesor	k1gInPc1
</s>
<s>
vlastní	vlastní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
</s>
<s>
Am	Am	k?
<g/>
2900	#num#	k4
no-x	no-x	k1gInSc1
<g/>
86	#num#	k4
</s>
<s>
Am	Am	k?
<g/>
9080	#num#	k4
x	x	k?
<g/>
86	#num#	k4
</s>
<s>
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
</s>
<s>
Am	Am	k?
<g/>
286	#num#	k4
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
32	#num#	k4
</s>
<s>
Am	Am	k?
<g/>
386	#num#	k4
|	|	kIx~
Am	Am	k1gFnSc1
<g/>
486	#num#	k4
|	|	kIx~
Am	Am	k1gFnSc1
<g/>
586	#num#	k4
|	|	kIx~
K5	K5	k1gMnSc1
|	|	kIx~
K6	K6	k1gMnSc1
|	|	kIx~
K6-2	K6-2	k1gMnSc1
|	|	kIx~
K	K	kA
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
+	+	kIx~
|	|	kIx~
<g/>
K	K	kA
<g/>
6	#num#	k4
<g/>
-III	-III	k?
|	|	kIx~
Duron	Duron	k1gMnSc1
|	|	kIx~
Athlon	Athlon	k1gInSc1
|	|	kIx~
Athlon	Athlon	k1gInSc1
M	M	kA
|	|	kIx~
Geode	Geod	k1gInSc5
|	|	kIx~
Sempron	Sempron	k1gInSc1
|	|	kIx~
Opteron	Opteron	k1gInSc1
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
</s>
<s>
Athlon	Athlon	k1gInSc1
64	#num#	k4
|	|	kIx~
Athlon	Athlon	k1gInSc1
64	#num#	k4
X2	X2	k1gFnPc2
|	|	kIx~
Turion	Turion	k1gInSc1
64	#num#	k4
|	|	kIx~
Turion	Turion	k1gInSc1
64	#num#	k4
X2	X2	k1gFnPc2
|	|	kIx~
Athlon	Athlon	k1gInSc1
II	II	kA
|	|	kIx~
Opteron	Opteron	k1gMnSc1
|	|	kIx~
Phenom	Phenom	k1gInSc1
|	|	kIx~
Phenom	Phenom	k1gInSc1
II	II	kA
|	|	kIx~
Fusion	Fusion	k1gInSc1
|	|	kIx~
Bulldozer	Bulldozer	k1gMnSc1
|	|	kIx~
Piledriver	Piledriver	k1gMnSc1
|	|	kIx~
Steamroller	Steamroller	k1gMnSc1
|	|	kIx~
Excavator	Excavator	k1gMnSc1
</s>
<s>
RISC	RISC	kA
</s>
<s>
Am	Am	k?
<g/>
29000	#num#	k4
|	|	kIx~
Alchemy	Alchem	k1gInPc1
Budoucí	budoucí	k2eAgInPc1d1
</s>
<s>
Zen	zen	k2eAgNnPc1d1
Jádra	jádro	k1gNnPc1
CPU	CPU	kA
</s>
<s>
K5	K5	k4
|	|	kIx~
K6	K6	k1gMnSc1
|	|	kIx~
K6-2	K6-2	k1gMnSc1
|	|	kIx~
K6-III	K6-III	k1gMnSc1
|	|	kIx~
K8	K8	k1gMnSc1
|	|	kIx~
K10	K10	k1gMnSc1
|	|	kIx~
K	K	kA
<g/>
10.5	10.5	k4
|	|	kIx~
K11	K11	k1gFnSc1
</s>
<s>
Grafické	grafický	k2eAgInPc1d1
čipy	čip	k1gInPc1
</s>
<s>
2D	2D	k4
zobrazení	zobrazení	k1gNnSc1
</s>
<s>
Mach	Mach	k1gMnSc1
DirectX	DirectX	k1gMnSc1
</s>
<s>
DirectX	DirectX	k?
5	#num#	k4
až	až	k9
9.0	9.0	k4
<g/>
c	c	k0
</s>
<s>
Rage	Rage	k1gFnSc1
|	|	kIx~
R100	R100	k1gMnSc1
|	|	kIx~
R200	R200	k1gMnSc1
|	|	kIx~
R300	R300	k1gMnSc1
|	|	kIx~
R420	R420	k1gMnSc1
|	|	kIx~
R520	R520	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
580	#num#	k4
<g/>
)	)	kIx)
DirectX	DirectX	k1gFnSc1
10	#num#	k4
+	+	kIx~
10.1	10.1	k4
</s>
<s>
R600	R600	k4
|	|	kIx~
R680	R680	k1gFnSc1
|	|	kIx~
R700	R700	k1gFnSc1
DirectX	DirectX	k1gFnSc1
11	#num#	k4
+	+	kIx~
11.1	11.1	k4
</s>
<s>
R800	R800	k4
|	|	kIx~
R900	R900	k1gFnSc1
|	|	kIx~
R1000	R1000	k1gFnSc1
Budoucí	budoucí	k2eAgFnSc1d1
</s>
<s>
R1100	R1100	k4
</s>
<s>
GPU	GPU	kA
pro	pro	k7c4
konzole	konzola	k1gFnSc3
</s>
<s>
Flipper	Flipper	k1gInSc1
(	(	kIx(
<g/>
GameCube	GameCub	k1gInSc5
<g/>
)	)	kIx)
|	|	kIx~
Xenos	Xenos	k1gInSc1
(	(	kIx(
<g/>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
)	)	kIx)
|	|	kIx~
Hollywood	Hollywood	k1gInSc1
(	(	kIx(
<g/>
Wii	Wii	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Grafické	grafický	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Radeon	Radeon	k1gMnSc1
</s>
<s>
Radeon	Radeon	k1gInSc1
7000	#num#	k4
|	|	kIx~
Radeon	Radeon	k1gInSc4
9000	#num#	k4
Řada	řada	k1gFnSc1
X	X	kA
</s>
<s>
Radeon	Radeon	k1gMnSc1
X100	X100	k1gMnSc1
|	|	kIx~
Radeon	Radeon	k1gMnSc1
X1000	X1000	k1gMnSc1
Řada	řada	k1gFnSc1
HD	HD	kA
</s>
<s>
Radeon	Radeon	k1gInSc1
HD	HD	kA
2000	#num#	k4
|	|	kIx~
Radeon	Radeon	k1gInSc1
HD	HD	kA
3000	#num#	k4
|	|	kIx~
Radeon	Radeon	k1gInSc1
HD	HD	kA
4000	#num#	k4
|	|	kIx~
Radeon	Radeon	k1gInSc1
HD	HD	kA
5000	#num#	k4
|	|	kIx~
Radeon	Radeon	k1gInSc1
HD	HD	kA
6000	#num#	k4
|	|	kIx~
Radeon	Radeon	k1gInSc1
HD	HD	kA
7000	#num#	k4
Budoucí	budoucí	k2eAgInPc1d1
</s>
<s>
Radeon	Radeon	k1gInSc1
HD	HD	kA
8000	#num#	k4
</s>
<s>
Čipové	čipový	k2eAgFnPc1d1
sady	sada	k1gFnPc1
</s>
<s>
Platforma	platforma	k1gFnSc1
AMD	AMD	kA
a	a	k8xC
Intel	Intel	kA
</s>
<s>
ATI	ATI	kA
Radeon	Radeon	k1gInSc1
Xpress	Xpress	k1gInSc1
200	#num#	k4
|	|	kIx~
ATI	ATI	kA
Express	express	k1gInSc4
1000	#num#	k4
|	|	kIx~
ATI	ATI	kA
CrossFire	CrossFir	k1gInSc5
Xpress	Xpress	k1gInSc4
3200	#num#	k4
Platforma	platforma	k1gFnSc1
AMD	AMD	kA
</s>
<s>
AMD-640	AMD-640	k4
|	|	kIx~
AMD-750	AMD-750	k1gFnSc1
|	|	kIx~
AMD-	AMD-	k1gFnSc1
<g/>
760	#num#	k4
<g/>
x	x	k?
|	|	kIx~
AMD-8000	AMD-8000	k1gMnSc1
|	|	kIx~
AMD	AMD	kA
480X	480X	k4
CrossFire	CrossFir	k1gInSc5
|	|	kIx~
AMD	AMD	kA
570X	570X	k4
CrossFire	CrossFir	k1gInSc5
|	|	kIx~
AMD	AMD	kA
580X	580X	k4
CrossFire	CrossFir	k1gInSc5
|	|	kIx~
AMD	AMD	kA
600	#num#	k4
|	|	kIx~
AMD	AMD	kA
700	#num#	k4
|	|	kIx~
AMD	AMD	kA
800	#num#	k4
|	|	kIx~
AMD	AMD	kA
900	#num#	k4
|	|	kIx~
A00M	A00M	k1gMnSc1
|	|	kIx~
A05	A05	k1gMnSc1
|	|	kIx~
A55E	A55E	k1gMnSc1
|	|	kIx~
A75	A75	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
můstek	můstek	k1gInSc4
</s>
<s>
AMD-645	AMD-645	k4
|	|	kIx~
AMD-700	AMD-700	k1gMnSc1
|	|	kIx~
AMD-8100	AMD-8100	k1gMnSc1
|	|	kIx~
SB600	SB600	k1gMnSc1
|	|	kIx~
SB700	SB700	k1gMnSc1
|	|	kIx~
SB700S	SB700S	k1gMnSc1
|	|	kIx~
SB800	SB800	k1gMnSc1
|	|	kIx~
Geode	Geod	k1gMnSc5
GXx	GXx	k1gMnSc5
|	|	kIx~
Geode	Geod	k1gInSc5
LX	LX	kA
</s>
<s>
Patice	patice	k1gFnSc1
</s>
<s>
Cizí	cizí	k2eAgInSc1d1
</s>
<s>
Socket	Socket	k1gInSc1
5	#num#	k4
|	|	kIx~
Socket	Socketa	k1gFnPc2
7	#num#	k4
Vlastní	vlastní	k2eAgFnSc1d1
</s>
<s>
Desktop	desktop	k1gInSc1
a	a	k8xC
mobilní	mobilní	k2eAgFnSc1d1
</s>
<s>
Super	super	k2eAgInSc1d1
socket	socket	k1gInSc1
7	#num#	k4
|	|	kIx~
Slot	slot	k1gInSc1
A	A	kA
|	|	kIx~
Socket	Socket	k1gInSc1
462	#num#	k4
|	|	kIx~
Socket	Socket	k1gInSc1
754	#num#	k4
|	|	kIx~
Socket	Socket	k1gInSc1
939	#num#	k4
|	|	kIx~
Socket	Socket	k1gInSc1
563	#num#	k4
|	|	kIx~
Socket	Socket	k1gMnSc1
S1	S1	k1gMnSc1
|	|	kIx~
Socket	Socket	k1gMnSc1
AM2	AM2	k1gMnSc1
|	|	kIx~
Socket	Socket	k1gInSc1
AM	AM	kA
<g/>
2	#num#	k4
<g/>
+	+	kIx~
|	|	kIx~
Socket	Socket	k1gMnSc1
AM3	AM3	k1gMnSc1
|	|	kIx~
Socket	Socket	k1gInSc1
AM	AM	kA
<g/>
3	#num#	k4
<g/>
+	+	kIx~
|	|	kIx~
Socket	Socket	k1gMnSc1
FM1	FM1	k1gMnSc1
|	|	kIx~
Socket	Socket	k1gMnSc1
FM2	FM2	k1gFnSc2
Server	server	k1gInSc1
</s>
<s>
Socket	Socket	k1gMnSc1
F	F	kA
|	|	kIx~
Socket	Socket	k1gMnSc1
G34	G34	k1gMnSc1
|	|	kIx~
Socket	Socket	k1gMnSc1
C32	C32	k1gMnSc1
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
Instrukční	instrukční	k2eAgFnPc1d1
sady	sada	k1gFnPc1
</s>
<s>
x	x	k?
<g/>
86	#num#	k4
|	|	kIx~
AMD64	AMD64	k1gMnSc1
|	|	kIx~
IA-32	IA-32	k1gMnSc1
|	|	kIx~
3	#num#	k4
<g/>
DNow	DNow	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
|	|	kIx~
MMX	MMX	kA
|	|	kIx~
SSE	SSE	kA
|	|	kIx~
SSE2	SSE2	k1gMnSc1
|	|	kIx~
SSE3	SSE3	k1gMnSc1
|	|	kIx~
SSE4	SSE4	k1gMnSc1
Multimédia	multimédium	k1gNnPc1
</s>
<s>
UVD	UVD	kA
|	|	kIx~
Avivo	Avivo	k1gNnSc4
HD	HD	kA
|	|	kIx~
PowerPlay	PowerPlaa	k1gMnSc2
|	|	kIx~
SmoothVision	SmoothVision	k1gInSc1
|	|	kIx~
Video	video	k1gNnSc1
Immersion	Immersion	k1gInSc1
CPU	cpát	k5eAaImIp1nS
</s>
<s>
HyperTransport	HyperTransport	k1gInSc1
|	|	kIx~
Turbo	turba	k1gFnSc5
core	core	k1gNnPc1
|	|	kIx~
Cool	Cool	k1gInSc1
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
<g/>
Quiet	Quiet	k1gMnSc1
|	|	kIx~
PowerNow	PowerNow	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
|	|	kIx~
AMD-V	AMD-V	k1gMnSc1
|	|	kIx~
NX	NX	kA
bit	bit	k1gInSc1
GPU	GPU	kA
</s>
<s>
CrossFireX	CrossFireX	k?
|	|	kIx~
Eyefinity	Eyefinita	k1gFnSc2
|	|	kIx~
HD3D	HD3D	k1gMnSc1
|	|	kIx~
APP	APP	kA
|	|	kIx~
VISION	vision	k1gInSc1
Engine	Engin	k1gInSc5
|	|	kIx~
HyperMemory	HyperMemora	k1gFnSc2
|	|	kIx~
Hybrid	hybrid	k1gInSc1
Graphics	Graphics	k1gInSc1
|	|	kIx~
XGP	XGP	kA
|	|	kIx~
Multi	Mulť	k1gFnSc2
Rendering	Rendering	k1gInSc1
|	|	kIx~
XvBA	XvBA	k1gFnSc1
|	|	kIx~
SmartShader	SmartShader	k1gInSc1
|	|	kIx~
FullStream	FullStream	k1gInSc1
|	|	kIx~
HyperZ	HyperZ	k1gFnSc1
|	|	kIx~
TruForm	TruForm	k1gInSc1
(	(	kIx(
<g/>
N-Patch	N-Patch	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Programy	program	k1gInPc1
</s>
<s>
VISION	vision	k1gInSc1
Engine	Engin	k1gInSc5
Control	Control	k1gInSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
Catalyst	Catalyst	k1gFnSc1
<g/>
)	)	kIx)
|	|	kIx~
Overive	Overiev	k1gFnPc1
|	|	kIx~
fglrx	fglrx	k1gInSc1
(	(	kIx(
<g/>
Linux	Linux	kA
<g/>
)	)	kIx)
|	|	kIx~
HydraVision	HydraVision	k1gInSc1
|	|	kIx~
HLSL2GLSL	HLSL2GLSL	k1gFnSc1
|	|	kIx~
Video	video	k1gNnSc1
converter	convertra	k1gFnPc2
(	(	kIx(
<g/>
AVIVO	AVIVO	kA
<g/>
)	)	kIx)
|	|	kIx~
Fusion	Fusion	k1gInSc1
Utility	utilita	k1gFnSc2
|	|	kIx~
System	Syst	k1gInSc7
Monitor	monitor	k1gInSc1
|	|	kIx~
APP	APP	kA
SDK	SDK	kA
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
slotů	slot	k1gInPc2
a	a	k8xC
socketů	socket	k1gMnPc2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
|	|	kIx~
Seznam	seznam	k1gInSc1
mikroprocesorů	mikroprocesor	k1gInPc2
AMD	AMD	kA
|	|	kIx~
Seznam	seznam	k1gInSc1
GPU	GPU	kA
firmy	firma	k1gFnSc2
AMD	AMD	kA
|	|	kIx~
Seznam	seznam	k1gInSc1
technologií	technologie	k1gFnPc2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
|	|	kIx~
Procesory	procesor	k1gInPc1
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
|	|	kIx~
Chipsety	Chipset	k1gInPc1
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
5036308-6	5036308-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2322	#num#	k4
4566	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
87802759	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
128547700	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
87802759	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Procesory	procesor	k1gInPc1
</s>
