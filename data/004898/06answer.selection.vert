<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zaznamenáno	zaznamenán	k2eAgNnSc4d1	zaznamenáno
8	[number]	k4	8
023	[number]	k4	023
sebevražd	sebevražda	k1gFnPc2	sebevražda
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
7	[number]	k4	7
010	[number]	k4	010
osob	osoba	k1gFnPc2	osoba
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tvoří	tvořit	k5eAaImIp3nS	tvořit
1,48	[number]	k4	1,48
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
23,3	[number]	k4	23,3
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc6	úmrtí
vnějšími	vnější	k2eAgFnPc7d1	vnější
příčinami	příčina	k1gFnPc7	příčina
<g/>
.	.	kIx.	.
</s>
