<s>
David	David	k1gMnSc1	David
Popper	Popper	k1gMnSc1	Popper
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1843	[number]	k4	1843
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1913	[number]	k4	1913
Baden	Badno	k1gNnPc2	Badno
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
violocellista	violocellista	k1gMnSc1	violocellista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Popper	Popper	k1gMnSc1	Popper
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
Ester	Ester	k1gFnSc2	Ester
Kischové	Kischová	k1gFnSc2	Kischová
a	a	k8xC	a
Angeluse	Angeluse	k1gFnSc1	Angeluse
Poppera	Popper	k1gMnSc2	Popper
<g/>
,	,	kIx,	,
pražského	pražský	k2eAgMnSc2d1	pražský
vrchního	vrchní	k2eAgMnSc2d1	vrchní
židovského	židovský	k2eAgMnSc2d1	židovský
kantora	kantor	k1gMnSc2	kantor
Cikánovy	cikánův	k2eAgFnSc2d1	Cikánova
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
Julia	Julius	k1gMnSc2	Julius
Goltermanna	Goltermann	k1gMnSc2	Goltermann
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgInS	zaujmout
již	již	k6eAd1	již
během	během	k7c2	během
studií	studio	k1gNnPc2	studio
svou	svůj	k3xOyFgFnSc7	svůj
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
metod	metoda	k1gFnPc2	metoda
houslové	houslový	k2eAgFnSc2d1	houslová
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vyhledávaným	vyhledávaný	k2eAgMnSc7d1	vyhledávaný
sólistou	sólista	k1gMnSc7	sólista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
vynikající	vynikající	k2eAgFnSc7d1	vynikající
klavíristkou	klavíristka	k1gFnSc7	klavíristka
Sofií	Sofia	k1gFnPc2	Sofia
Menterovou	Menterův	k2eAgFnSc7d1	Menterův
a	a	k8xC	a
společně	společně	k6eAd1	společně
sklízeli	sklízet	k5eAaImAgMnP	sklízet
úspěchy	úspěch	k1gInPc4	úspěch
na	na	k7c6	na
koncertních	koncertní	k2eAgFnPc6d1	koncertní
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
však	však	k9	však
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
sólové	sólový	k2eAgFnSc2d1	sólová
dráhy	dráha	k1gFnSc2	dráha
byl	být	k5eAaImAgMnS	být
Popper	Popper	k1gMnSc1	Popper
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
koncertním	koncertní	k2eAgMnSc7d1	koncertní
mistrem	mistr	k1gMnSc7	mistr
a	a	k8xC	a
sólistou	sólista	k1gMnSc7	sólista
Dvorní	dvorní	k2eAgFnSc2d1	dvorní
opery	opera	k1gFnSc2	opera
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
Hudební	hudební	k2eAgFnSc6d1	hudební
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
mistrovství	mistrovství	k1gNnSc4	mistrovství
byl	být	k5eAaImAgMnS	být
vyznamenán	vyznamenat	k5eAaPmNgMnS	vyznamenat
Řádem	řád	k1gInSc7	řád
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1913	[number]	k4	1913
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Zpopelněn	zpopelněn	k2eAgMnSc1d1	zpopelněn
však	však	k9	však
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
hrál	hrát	k5eAaImAgInS	hrát
také	také	k9	také
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
orchestrech	orchestr	k1gInPc6	orchestr
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
jako	jako	k8xC	jako
pedagog	pedagog	k1gMnSc1	pedagog
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Popper	Popper	k1gMnSc1	Popper
komponoval	komponovat	k5eAaImAgMnS	komponovat
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
složil	složit	k5eAaPmAgMnS	složit
4	[number]	k4	4
koncerty	koncert	k1gInPc4	koncert
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
3	[number]	k4	3
suity	suita	k1gFnSc2	suita
a	a	k8xC	a
řadu	řad	k1gInSc2	řad
drobnějších	drobný	k2eAgFnPc2d2	drobnější
přednesových	přednesový	k2eAgFnPc2d1	přednesová
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
je	být	k5eAaImIp3nS	být
Requiem	Requium	k1gNnSc7	Requium
pro	pro	k7c4	pro
4	[number]	k4	4
violoncella	violoncello	k1gNnSc2	violoncello
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>

