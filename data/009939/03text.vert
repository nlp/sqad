<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
Poltár	Poltár	k1gInSc1	Poltár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
třetím	třetí	k4xOgMnSc7	třetí
prezidentem	prezident	k1gMnSc7	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jej	on	k3xPp3gMnSc4	on
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
vyloučili	vyloučit	k5eAaPmAgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
škole	škola	k1gFnSc6	škola
i	i	k9	i
učil	učít	k5eAaPmAgMnS	učít
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
prorektorem	prorektor	k1gMnSc7	prorektor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odborným	odborný	k2eAgMnSc7d1	odborný
poradcem	poradce	k1gMnSc7	poradce
československého	československý	k2eAgInSc2d1	československý
filmu	film	k1gInSc2	film
Advokátka	advokátka	k1gFnSc1	advokátka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
do	do	k7c2	do
března	březen	k1gInSc2	březen
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
generálním	generální	k2eAgMnSc7d1	generální
prokurátorem	prokurátor	k1gMnSc7	prokurátor
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgMnS	být
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
Hnutie	Hnutie	k1gFnSc2	Hnutie
za	za	k7c4	za
demokraciu	demokracium	k1gNnSc6	demokracium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kandidatury	kandidatura	k1gFnSc2	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
přímých	přímý	k2eAgFnPc2d1	přímá
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
slovenským	slovenský	k2eAgMnSc7d1	slovenský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgInS	porazit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
kandidáta	kandidát	k1gMnSc2	kandidát
SDKÚ	SDKÚ	kA	SDKÚ
Eduarda	Eduard	k1gMnSc2	Eduard
Kukana	Kukan	k1gMnSc2	Kukan
a	a	k8xC	a
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
svého	svůj	k3xOyFgInSc2	svůj
bývalého	bývalý	k2eAgMnSc4d1	bývalý
stranického	stranický	k2eAgMnSc4d1	stranický
kolegu	kolega	k1gMnSc4	kolega
z	z	k7c2	z
HZDS	HZDS	kA	HZDS
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Mečiara	Mečiar	k1gMnSc4	Mečiar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
obhajovat	obhajovat	k5eAaImF	obhajovat
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
nezískal	získat	k5eNaPmAgMnS	získat
žádný	žádný	k1gMnSc1	žádný
kandidát	kandidát	k1gMnSc1	kandidát
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
s	s	k7c7	s
protikandidátkou	protikandidátka	k1gFnSc7	protikandidátka
vládní	vládní	k2eAgFnSc2d1	vládní
opozice	opozice	k1gFnSc2	opozice
Ivetou	Iveta	k1gFnSc7	Iveta
Radičovou	radičův	k2eAgFnSc7d1	radičův
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
s	s	k7c7	s
11	[number]	k4	11
<g/>
%	%	kIx~	%
rozdílem	rozdíl	k1gInSc7	rozdíl
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Onemocnění	onemocnění	k1gNnSc1	onemocnění
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k9	již
takřka	takřka	k6eAd1	takřka
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
trpí	trpět	k5eAaImIp3nS	trpět
rakovinou	rakovina	k1gFnSc7	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Řádem	řád	k1gInSc7	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gInSc4	Gašparovič
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
sme	sme	k?	sme
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Bilance	bilance	k1gFnSc1	bilance
2004-2009	[number]	k4	2004-2009
na	na	k7c4	na
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
