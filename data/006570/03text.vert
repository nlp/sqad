<s>
Cholesterol	cholesterol	k1gInSc1	cholesterol
je	být	k5eAaImIp3nS	být
steroidní	steroidní	k2eAgFnSc1d1	steroidní
látka	látka	k1gFnSc1	látka
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
i	i	k8xC	i
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
korýši	korýš	k1gMnPc1	korýš
či	či	k8xC	či
někteří	některý	k3yIgMnPc1	některý
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjiměčně	výjiměčně	k6eAd1	výjiměčně
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
hub	houba	k1gFnPc2	houba
nebo	nebo	k8xC	nebo
u	u	k7c2	u
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
<g/>
,	,	kIx,	,
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
zastoupení	zastoupení	k1gNnSc6	zastoupení
i	i	k9	i
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cholesterol	cholesterol	k1gInSc1	cholesterol
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
tělu	tělo	k1gNnSc3	tělo
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
hormonů	hormon	k1gInPc2	hormon
a	a	k8xC	a
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
však	však	k9	však
nese	nést	k5eAaImIp3nS	nést
pro	pro	k7c4	pro
organismus	organismus	k1gInSc4	organismus
zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
onemocnění	onemocnění	k1gNnSc1	onemocnění
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Cholesterol	cholesterol	k1gInSc1	cholesterol
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
steroidní	steroidní	k2eAgFnSc2d1	steroidní
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
každé	každý	k3xTgFnSc2	každý
naší	náš	k3xOp1gFnSc2	náš
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
stavební	stavební	k2eAgFnSc7d1	stavební
jednotkou	jednotka	k1gFnSc7	jednotka
nervů	nerv	k1gInPc2	nerv
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
hormonů	hormon	k1gInPc2	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
organismus	organismus	k1gInSc4	organismus
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
pak	pak	k6eAd1	pak
přijímáme	přijímat	k5eAaImIp1nP	přijímat
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
<g/>
.	.	kIx.	.
</s>
<s>
Cholesterol	cholesterol	k1gInSc1	cholesterol
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
organizmus	organizmus	k1gInSc4	organizmus
nepostradatelný	postradatelný	k2eNgInSc4d1	nepostradatelný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ho	on	k3xPp3gInSc2	on
nesmíme	smět	k5eNaImIp1nP	smět
mít	mít	k5eAaImF	mít
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
nadbytek	nadbytek	k1gInSc4	nadbytek
<g/>
.	.	kIx.	.
</s>
<s>
Doporučený	doporučený	k2eAgInSc4d1	doporučený
maximální	maximální	k2eAgInSc4d1	maximální
příjem	příjem	k1gInSc4	příjem
cholesterolu	cholesterol	k1gInSc2	cholesterol
u	u	k7c2	u
nesportující	sportující	k2eNgFnSc2d1	nesportující
populace	populace	k1gFnSc2	populace
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
překračovat	překračovat	k5eAaImF	překračovat
300	[number]	k4	300
mg	mg	kA	mg
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Sportovci	sportovec	k1gMnPc1	sportovec
však	však	k9	však
díky	díky	k7c3	díky
většímu	veliký	k2eAgInSc3d2	veliký
energetickému	energetický	k2eAgInSc3d1	energetický
příjmu	příjem	k1gInSc3	příjem
souvisejícímu	související	k2eAgInSc3d1	související
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
zkonzumované	zkonzumovaný	k2eAgFnSc2d1	zkonzumovaná
potravy	potrava	k1gFnSc2	potrava
nutně	nutně	k6eAd1	nutně
musí	muset	k5eAaImIp3nP	muset
přijímat	přijímat	k5eAaImF	přijímat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
tak	tak	k6eAd1	tak
–	–	k?	–
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
vegetariáni	vegetarián	k1gMnPc1	vegetarián
–	–	k?	–
překročí	překročit	k5eAaPmIp3nP	překročit
uvedený	uvedený	k2eAgInSc4d1	uvedený
limit	limit	k1gInSc4	limit
<g/>
.	.	kIx.	.
</s>
<s>
Endogenní	endogenní	k2eAgFnSc1d1	endogenní
tvorba	tvorba	k1gFnSc1	tvorba
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
denní	denní	k2eAgFnSc1d1	denní
potřeba	potřeba	k1gFnSc1	potřeba
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
individuální	individuální	k2eAgFnSc2d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
a	a	k8xC	a
aktivních	aktivní	k2eAgMnPc2d1	aktivní
jedinců	jedinec	k1gMnPc2	jedinec
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
příjmu	příjem	k1gInSc6	příjem
cholesterolu	cholesterol	k1gInSc2	cholesterol
potravou	potrava	k1gFnSc7	potrava
snižuje	snižovat	k5eAaImIp3nS	snižovat
jeho	jeho	k3xOp3gFnSc4	jeho
produkci	produkce	k1gFnSc4	produkce
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Syntézu	syntéza	k1gFnSc4	syntéza
začíná	začínat	k5eAaImIp3nS	začínat
acetyl-CoA	acetyl-CoA	k?	acetyl-CoA
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
acetoacetyl-CoA	acetoacetyl-CoA	k?	acetoacetyl-CoA
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
díky	díky	k7c3	díky
enzymům	enzym	k1gInPc3	enzym
thioláze	thioláze	k1gFnSc2	thioláze
a	a	k8xC	a
HMG-CoA	HMG-CoA	k1gFnSc2	HMG-CoA
syntáze	syntáze	k1gFnSc2	syntáze
vzniká	vznikat	k5eAaImIp3nS	vznikat
meziprodukt	meziprodukt	k1gInSc4	meziprodukt
hydroxymethylglutaryl-CoA	hydroxymethylglutaryl-CoA	k?	hydroxymethylglutaryl-CoA
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
syntéza	syntéza	k1gFnSc1	syntéza
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
přes	přes	k7c4	přes
mevalonát	mevalonát	k1gInSc4	mevalonát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
fosforylován	fosforylovat	k5eAaBmNgInS	fosforylovat
na	na	k7c4	na
5	[number]	k4	5
<g/>
-difosfomevalonát	ifosfomevalonát	k1gInSc4	-difosfomevalonát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dekarboxylaci	dekarboxylace	k1gFnSc3	dekarboxylace
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
2	[number]	k4	2
produkty	produkt	k1gInPc4	produkt
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
-	-	kIx~	-
isopentenyldifosfát	isopentenyldifosfát	k1gInSc1	isopentenyldifosfát
a	a	k8xC	a
dimethylallylfosfát	dimethylallylfosfát	k1gInSc1	dimethylallylfosfát
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kondenzují	kondenzovat	k5eAaImIp3nP	kondenzovat
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
geranylfosfát	geranylfosfát	k5eAaPmF	geranylfosfát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
syntéza	syntéza	k1gFnSc1	syntéza
prochází	procházet	k5eAaImIp3nS	procházet
přes	přes	k7c4	přes
farnesyldifosfát	farnesyldifosfát	k1gInSc4	farnesyldifosfát
<g/>
,	,	kIx,	,
skvalen	skvalen	k2eAgMnSc1d1	skvalen
<g/>
,	,	kIx,	,
2,3	[number]	k4	2,3
<g/>
-epoxyskvalen	poxyskvalen	k2eAgInSc1d1	-epoxyskvalen
<g/>
,	,	kIx,	,
lanosterol	lanosterol	k1gInSc1	lanosterol
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
prekurzor	prekurzor	k1gInSc4	prekurzor
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
)	)	kIx)	)
a	a	k8xC	a
následnými	následný	k2eAgFnPc7d1	následná
19	[number]	k4	19
kroky	krok	k1gInPc4	krok
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
3	[number]	k4	3
methylových	methylový	k2eAgFnPc2d1	methylová
skupin	skupina	k1gFnPc2	skupina
vzniká	vznikat	k5eAaImIp3nS	vznikat
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Cholesterol	cholesterol	k1gInSc1	cholesterol
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
(	(	kIx(	(
<g/>
apolipoproteiny	apolipoproteina	k1gFnPc4	apolipoproteina
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
lipoproteiny	lipoprotein	k1gInPc7	lipoprotein
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
třídy	třída	k1gFnPc1	třída
lipoproteinů	lipoprotein	k1gInPc2	lipoprotein
podle	podle	k7c2	podle
hustoty	hustota	k1gFnSc2	hustota
<g/>
:	:	kIx,	:
vysokodenzitní	vysokodenzitní	k2eAgInSc1d1	vysokodenzitní
lipoprotein	lipoprotein	k1gInSc1	lipoprotein
(	(	kIx(	(
<g/>
HDL	HDL	kA	HDL
<g/>
)	)	kIx)	)
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gMnSc4	on
shluky	shluk	k1gInPc1	shluk
velikosti	velikost	k1gFnSc3	velikost
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
nm	nm	k?	nm
obsahující	obsahující	k2eAgInPc1d1	obsahující
převážně	převážně	k6eAd1	převážně
apolipoprotein	apolipoprotein	k1gInSc4	apolipoprotein
A1	A1	k1gFnSc2	A1
uvolňující	uvolňující	k2eAgInSc4d1	uvolňující
cholesterol	cholesterol	k1gInSc4	cholesterol
do	do	k7c2	do
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
vázaný	vázaný	k2eAgInSc1d1	vázaný
v	v	k7c6	v
HDL	HDL	kA	HDL
je	být	k5eAaImIp3nS	být
známkou	známka	k1gFnSc7	známka
dobré	dobrý	k2eAgFnSc2d1	dobrá
schopnosti	schopnost	k1gFnSc2	schopnost
vyloučit	vyloučit	k5eAaPmF	vyloučit
nadbytečný	nadbytečný	k2eAgInSc4d1	nadbytečný
cholesterol	cholesterol	k1gInSc4	cholesterol
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
organismu	organismus	k1gInSc2	organismus
nízkodenzitní	nízkodenzitní	k2eAgInSc1d1	nízkodenzitní
lipoprotein	lipoprotein	k1gInSc1	lipoprotein
(	(	kIx(	(
<g/>
LDL	LDL	kA	LDL
<g/>
)	)	kIx)	)
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc4	velikost
okolo	okolo	k7c2	okolo
20	[number]	k4	20
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
apolipoprotein	apolipoprotein	k1gInSc4	apolipoprotein
B	B	kA	B
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c2	za
ukládání	ukládání	k1gNnSc2	ukládání
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
VLDL	VLDL	kA	VLDL
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
štěpení	štěpení	k1gNnSc2	štěpení
jejich	jejich	k3xOp3gInPc2	jejich
triglyceridů	triglycerid	k1gInPc2	triglycerid
<g/>
,	,	kIx,	,
vážou	vázat	k5eAaImIp3nP	vázat
se	se	k3xPyFc4	se
na	na	k7c4	na
membránový	membránový	k2eAgInSc4d1	membránový
receptor	receptor	k1gInSc4	receptor
velmi	velmi	k6eAd1	velmi
nízkodenzitní	nízkodenzitní	k2eAgInSc4d1	nízkodenzitní
lipoprotein	lipoprotein	k1gInSc4	lipoprotein
(	(	kIx(	(
<g/>
VLDL	VLDL	kA	VLDL
<g/>
)	)	kIx)	)
–	–	k?	–
lipoprotein	lipoprotein	k1gInSc1	lipoprotein
(	(	kIx(	(
<g/>
velikosti	velikost	k1gFnSc2	velikost
30	[number]	k4	30
až	až	k9	až
80	[number]	k4	80
nm	nm	k?	nm
<g/>
)	)	kIx)	)
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc6d1	nízká
hustotě	hustota	k1gFnSc6	hustota
<g/>
,	,	kIx,	,
syntetizuje	syntetizovat	k5eAaImIp3nS	syntetizovat
se	se	k3xPyFc4	se
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
část	část	k1gFnSc1	část
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
náklad	náklad	k1gInSc1	náklad
TG	tg	kA	tg
(	(	kIx(	(
<g/>
triglyceridů	triglycerid	k1gInPc2	triglycerid
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejmenší	malý	k2eAgNnSc4d3	nejmenší
množství	množství	k1gNnSc4	množství
apolipoproteinů	apolipoprotein	k1gInPc2	apolipoprotein
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzniku	vznik	k1gInSc2	vznik
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
cholesterol	cholesterol	k1gInSc1	cholesterol
<g/>
:	:	kIx,	:
exogenní	exogenní	k2eAgFnPc1d1	exogenní
–	–	k?	–
vnější	vnější	k2eAgFnPc1d1	vnější
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
)	)	kIx)	)
endogenní	endogenní	k2eAgFnPc1d1	endogenní
–	–	k?	–
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
tělo	tělo	k1gNnSc1	tělo
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
samo	sám	k3xTgNnSc1	sám
<g/>
)	)	kIx)	)
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
hladina	hladina	k1gFnSc1	hladina
celkového	celkový	k2eAgInSc2d1	celkový
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
cholesterolemie	cholesterolemie	k1gFnSc1	cholesterolemie
<g/>
)	)	kIx)	)
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
5,00	[number]	k4	5,00
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
milimolů	milimol	k1gInPc2	milimol
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
od	od	k7c2	od
5,01	[number]	k4	5,01
do	do	k7c2	do
6,5	[number]	k4	6,5
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
za	za	k7c4	za
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
hladinou	hladina	k1gFnSc7	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
měli	mít	k5eAaImAgMnP	mít
více	hodně	k6eAd2	hodně
všímat	všímat	k5eAaImF	všímat
svého	svůj	k3xOyFgInSc2	svůj
jídelníčku	jídelníček	k1gInSc2	jídelníček
a	a	k8xC	a
upravit	upravit	k5eAaPmF	upravit
svůj	svůj	k3xOyFgInSc4	svůj
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
6,5	[number]	k4	6,5
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
riziková	rizikový	k2eAgFnSc1d1	riziková
<g/>
.	.	kIx.	.
</s>
<s>
Lidem	člověk	k1gMnPc3	člověk
s	s	k7c7	s
takto	takto	k6eAd1	takto
vysokým	vysoký	k2eAgInSc7d1	vysoký
cholesterolem	cholesterol	k1gInSc7	cholesterol
hrozí	hrozit	k5eAaImIp3nS	hrozit
větší	veliký	k2eAgNnSc1d2	veliký
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
srdečně-cévních	srdečněévní	k2eAgNnPc2d1	srdečně-cévní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
hodnoty	hodnota	k1gFnSc2	hodnota
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
znát	znát	k5eAaImF	znát
nejen	nejen	k6eAd1	nejen
svůj	svůj	k3xOyFgInSc4	svůj
celkový	celkový	k2eAgInSc4d1	celkový
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hladinu	hladina	k1gFnSc4	hladina
"	"	kIx"	"
<g/>
hodného	hodný	k2eAgNnSc2d1	hodné
<g/>
"	"	kIx"	"
HDL	HDL	kA	HDL
a	a	k8xC	a
"	"	kIx"	"
<g/>
zlého	zlý	k2eAgNnSc2d1	zlé
<g/>
"	"	kIx"	"
LDL	LDL	kA	LDL
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
LDL	LDL	kA	LDL
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
3	[number]	k4	3
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
totiž	totiž	k9	totiž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
usazování	usazování	k1gNnSc4	usazování
nadbytečného	nadbytečný	k2eAgInSc2d1	nadbytečný
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
cévních	cévní	k2eAgFnPc6d1	cévní
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
sklerotické	sklerotický	k2eAgInPc4d1	sklerotický
pláty	plát	k1gInPc4	plát
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
cévy	céva	k1gFnPc4	céva
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
pružnost	pružnost	k1gFnSc1	pružnost
a	a	k8xC	a
zužuje	zužovat	k5eAaImIp3nS	zužovat
se	se	k3xPyFc4	se
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
průtok	průtok	k1gInSc4	průtok
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
HDL	HDL	kA	HDL
cholesterol	cholesterol	k1gInSc1	cholesterol
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
krev	krev	k1gFnSc1	krev
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nadbytečného	nadbytečný	k2eAgInSc2d1	nadbytečný
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
odvádí	odvádět	k5eAaImIp3nS	odvádět
ho	on	k3xPp3gMnSc4	on
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
metabolizován	metabolizován	k2eAgInSc1d1	metabolizován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gInSc1	jeho
přínos	přínos	k1gInSc1	přínos
není	být	k5eNaImIp3nS	být
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
hladině	hladina	k1gFnSc6	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
upravit	upravit	k5eAaPmF	upravit
životosprávu	životospráva	k1gFnSc4	životospráva
a	a	k8xC	a
dodržováním	dodržování	k1gNnSc7	dodržování
zásad	zásada	k1gFnPc2	zásada
zdravého	zdravý	k2eAgInSc2d1	zdravý
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
snížení	snížení	k1gNnSc4	snížení
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
mýtem	mýtus	k1gInSc7	mýtus
je	být	k5eAaImIp3nS	být
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
hlídat	hlídat	k5eAaImF	hlídat
pouze	pouze	k6eAd1	pouze
příjem	příjem	k1gInSc4	příjem
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
bychom	by	kYmCp1nP	by
proto	proto	k6eAd1	proto
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
přijmout	přijmout	k5eAaPmF	přijmout
max	max	kA	max
<g/>
.	.	kIx.	.
300	[number]	k4	300
mg	mg	kA	mg
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
máme	mít	k5eAaImIp1nP	mít
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
pak	pak	k6eAd1	pak
maximálně	maximálně	k6eAd1	maximálně
200	[number]	k4	200
mg	mg	kA	mg
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
důležitější	důležitý	k2eAgMnSc1d2	důležitější
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
zaměřit	zaměřit	k5eAaPmF	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
tuky	tuk	k1gInPc4	tuk
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
hladinu	hladina	k1gFnSc4	hladina
LDL	LDL	kA	LDL
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
cholesterol	cholesterol	k1gInSc1	cholesterol
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
srdečně-cévních	srdečněévní	k2eAgNnPc2d1	srdečně-cévní
onemocnění	onemocnění	k1gNnPc2	onemocnění
(	(	kIx(	(
<g/>
tato	tento	k3xDgNnPc1	tento
onemocnění	onemocnění	k1gNnPc1	onemocnění
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
úmrtí	úmrť	k1gFnPc2	úmrť
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
hladiny	hladina	k1gFnSc2	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
týká	týkat	k5eAaImIp3nS	týkat
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
Studie	studie	k1gFnSc1	studie
Post-Monica	Post-Monica	k1gFnSc1	Post-Monica
<g/>
,	,	kIx,	,
IKEM	IKEM	kA	IKEM
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hyperlipidemie	hyperlipidemie	k1gFnSc1	hyperlipidemie
neboli	neboli	k8xC	neboli
hyperlipoprotenie	hyperlipoprotenie	k1gFnSc1	hyperlipoprotenie
(	(	kIx(	(
<g/>
HLP	HLP	kA	HLP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
život	život	k1gInSc4	život
ohrožujících	ohrožující	k2eAgNnPc2d1	ohrožující
onemocnění	onemocnění	k1gNnPc2	onemocnění
je	být	k5eAaImIp3nS	být
familiární	familiární	k2eAgFnSc1d1	familiární
hypercholesterolemie	hypercholesterolemie	k1gFnSc1	hypercholesterolemie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
částečnou	částečný	k2eAgFnSc7d1	částečná
nebo	nebo	k8xC	nebo
úplnou	úplný	k2eAgFnSc7d1	úplná
absencí	absence	k1gFnSc7	absence
receptorů	receptor	k1gInPc2	receptor
pro	pro	k7c4	pro
LDL	LDL	kA	LDL
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
primární	primární	k2eAgFnSc1d1	primární
(	(	kIx(	(
<g/>
familiární	familiární	k2eAgFnSc1d1	familiární
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
geneticky	geneticky	k6eAd1	geneticky
podmíněná	podmíněný	k2eAgFnSc1d1	podmíněná
<g/>
)	)	kIx)	)
sekundární	sekundární	k2eAgFnSc1d1	sekundární
(	(	kIx(	(
<g/>
získaná	získaný	k2eAgFnSc1d1	získaná
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
tedy	tedy	k9	tedy
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
)	)	kIx)	)
</s>
