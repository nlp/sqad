<s>
Inteligenční	inteligenční	k2eAgInSc1d1	inteligenční
kvocient	kvocient	k1gInSc1	kvocient
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
standardizované	standardizovaný	k2eAgNnSc1d1	standardizované
skóre	skóre	k1gNnSc4	skóre
používané	používaný	k2eAgNnSc4d1	používané
jako	jako	k9	jako
výstup	výstup	k1gInSc4	výstup
standardizovaných	standardizovaný	k2eAgInPc2d1	standardizovaný
inteligenčních	inteligenční	k2eAgInPc2d1	inteligenční
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
výkonových	výkonový	k2eAgInPc2d1	výkonový
<g/>
)	)	kIx)	)
psychologických	psychologický	k2eAgInPc2d1	psychologický
testů	test	k1gInPc2	test
k	k	k7c3	k
vyčíslení	vyčíslení	k1gNnSc3	vyčíslení
inteligence	inteligence	k1gFnSc2	inteligence
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
ostatní	ostatní	k2eAgFnSc3d1	ostatní
populaci	populace	k1gFnSc3	populace
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
skupině	skupina	k1gFnSc3	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
deviační	deviační	k2eAgNnPc4d1	deviační
skóre	skóre	k1gNnPc4	skóre
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
100	[number]	k4	100
a	a	k8xC	a
směrodatnou	směrodatný	k2eAgFnSc7d1	směrodatná
odchylkou	odchylka	k1gFnSc7	odchylka
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
německý	německý	k2eAgMnSc1d1	německý
psycholog	psycholog	k1gMnSc1	psycholog
William	William	k1gInSc1	William
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
když	když	k8xS	když
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
původní	původní	k2eAgNnSc4d1	původní
pojetí	pojetí	k1gNnSc4	pojetí
inteligence	inteligence	k1gFnSc2	inteligence
podle	podle	k7c2	podle
Alfreda	Alfred	k1gMnSc2	Alfred
Bineta	Binet	k1gMnSc2	Binet
a	a	k8xC	a
Théodora	Théodor	k1gMnSc2	Théodor
Simona	Simona	k1gFnSc1	Simona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
mentálním	mentální	k2eAgInSc7d1	mentální
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Definoval	definovat	k5eAaBmAgMnS	definovat
míru	míra	k1gFnSc4	míra
inteligence	inteligence	k1gFnSc2	inteligence
jako	jako	k8xC	jako
poměr	poměr	k1gInSc1	poměr
"	"	kIx"	"
<g/>
mentálního	mentální	k2eAgInSc2d1	mentální
věku	věk	k1gInSc2	věk
<g/>
"	"	kIx"	"
a	a	k8xC	a
věku	věk	k1gInSc2	věk
chronologického	chronologický	k2eAgInSc2d1	chronologický
(	(	kIx(	(
<g/>
skutečného	skutečný	k2eAgInSc2d1	skutečný
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
Q	Q	kA	Q
=	=	kIx~	=
100	[number]	k4	100
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
e	e	k0	e
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
́	́	k?	́
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ı	ı	k?	ı
́	́	k?	́
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
ˇ	ˇ	k?	ˇ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
h	h	k?	h
r	r	kA	r
o	o	k7c4	o
n	n	k0	n
o	o	k7c4	o
l	l	kA	l
o	o	k7c6	o
g	g	kA	g
i	i	k9	i
c	c	k0	c
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
́	́	k?	́
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
ˇ	ˇ	k?	ˇ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
IQ	iq	kA	iq
<g/>
=	=	kIx~	=
<g/>
100	[number]	k4	100
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc4	frac
{	{	kIx(	{
<g/>
ment	menta	k1gFnPc2	menta
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
acute	acute	k5eAaPmIp2nP	acute
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
ln	ln	k?	ln
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
acute	acute	k5eAaPmIp2nP	acute
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
imath	imath	k1gInSc1	imath
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
v	v	k7c6	v
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
check	check	k6eAd1	check
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
chronologick	chronologick	k6eAd1	chronologick
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
acute	acute	k5eAaPmIp2nP	acute
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
v	v	k7c6	v
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
check	check	k6eAd1	check
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
k	k	k7c3	k
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
mělo	mít	k5eAaImAgNnS	mít
desetileté	desetiletý	k2eAgNnSc4d1	desetileté
dítě	dítě	k1gNnSc4	dítě
intelektuální	intelektuální	k2eAgFnSc2d1	intelektuální
schopnosti	schopnost	k1gFnSc2	schopnost
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
dítěte	dítě	k1gNnSc2	dítě
třináctiletého	třináctiletý	k2eAgNnSc2d1	třináctileté
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
IQ	iq	kA	iq
rovno	roven	k2eAgNnSc4d1	rovno
130	[number]	k4	130
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
·	·	k?	·
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
testování	testování	k1gNnSc6	testování
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
úlohy	úloha	k1gFnPc1	úloha
rozčleněné	rozčleněný	k2eAgFnPc1d1	rozčleněná
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
staří	starý	k2eAgMnPc1d1	starý
jedinci	jedinec	k1gMnPc1	jedinec
je	on	k3xPp3gMnPc4	on
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
schopni	schopen	k2eAgMnPc1d1	schopen
zvládnout	zvládnout	k5eAaPmF	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Mentální	mentální	k2eAgInSc1d1	mentální
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
určil	určit	k5eAaPmAgInS	určit
podle	podle	k7c2	podle
nejnáročnějších	náročný	k2eAgFnPc2d3	nejnáročnější
úloh	úloha	k1gFnPc2	úloha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
byl	být	k5eAaImAgInS	být
testovaný	testovaný	k2eAgInSc1d1	testovaný
schopen	schopen	k2eAgInSc1d1	schopen
adekvátně	adekvátně	k6eAd1	adekvátně
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vzorec	vzorec	k1gInSc1	vzorec
však	však	k9	však
lze	lze	k6eAd1	lze
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
biologický	biologický	k2eAgInSc1d1	biologický
věk	věk	k1gInSc1	věk
roste	růst	k5eAaImIp3nS	růst
lineárně	lineárně	k6eAd1	lineárně
a	a	k8xC	a
mentální	mentální	k2eAgMnPc4d1	mentální
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
získávali	získávat	k5eAaImAgMnP	získávat
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
hodnoty	hodnota	k1gFnPc4	hodnota
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
preferuje	preferovat	k5eAaImIp3nS	preferovat
odvozený	odvozený	k2eAgInSc1d1	odvozený
kvocient	kvocient	k1gInSc1	kvocient
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
odchylkové	odchylkové	k2eAgInSc1d1	odchylkové
neboli	neboli	k8xC	neboli
deviační	deviační	k2eAgInSc1d1	deviační
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
úroveň	úroveň	k1gFnSc1	úroveň
rozumových	rozumový	k2eAgFnPc2d1	rozumová
schopností	schopnost	k1gFnPc2	schopnost
jedince	jedinec	k1gMnSc4	jedinec
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
průměru	průměr	k1gInSc3	průměr
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
škála	škála	k1gFnSc1	škála
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
populační	populační	k2eAgFnSc6d1	populační
skupině	skupina	k1gFnSc6	skupina
100	[number]	k4	100
a	a	k8xC	a
směrodatnou	směrodatný	k2eAgFnSc7d1	směrodatná
odchylkou	odchylka	k1gFnSc7	odchylka
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
řídí	řídit	k5eAaImIp3nS	řídit
vzorcem	vzorec	k1gInSc7	vzorec
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
Q	Q	kA	Q
=	=	kIx~	=
100	[number]	k4	100
+	+	kIx~	+
15	[number]	k4	15
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
μ	μ	k?	μ
)	)	kIx)	)
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
IQ	iq	kA	iq
<g/>
=	=	kIx~	=
<g/>
100	[number]	k4	100
<g/>
+	+	kIx~	+
<g/>
15	[number]	k4	15
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
x-	x-	k?	x-
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}}}	}}}	k?	}}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hrubé	hrubý	k2eAgNnSc4d1	hrubé
skóre	skóre	k1gNnSc4	skóre
dosažené	dosažený	k2eAgNnSc4d1	dosažené
v	v	k7c6	v
testu	test	k1gInSc6	test
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
součet	součet	k1gInSc1	součet
všech	všecek	k3xTgFnPc2	všecek
správných	správný	k2eAgFnPc2d1	správná
položek	položka	k1gFnPc2	položka
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
správných	správný	k2eAgFnPc2d1	správná
položek	položka	k1gFnPc2	položka
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
populaci	populace	k1gFnSc6	populace
(	(	kIx(	(
<g/>
členěné	členěný	k2eAgNnSc1d1	členěné
zpravidla	zpravidla	k6eAd1	zpravidla
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
národnosti	národnost	k1gFnSc2	národnost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnPc6	sigma
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
odchylka	odchylka	k1gFnSc1	odchylka
hrubého	hrubý	k2eAgNnSc2d1	hrubé
skóre	skóre	k1gNnSc2	skóre
testu	test	k1gInSc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
inteligence	inteligence	k1gFnSc1	inteligence
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
normální	normální	k2eAgNnSc4d1	normální
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
asi	asi	k9	asi
70	[number]	k4	70
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
IQ	iq	kA	iq
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
115	[number]	k4	115
<g/>
.	.	kIx.	.
</s>
<s>
IQ	iq	kA	iq
115	[number]	k4	115
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
nad	nad	k7c7	nad
130	[number]	k4	130
už	už	k9	už
jen	jen	k6eAd1	jen
necelé	celý	k2eNgInPc4d1	necelý
2	[number]	k4	2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
rozložení	rozložení	k1gNnSc1	rozložení
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
u	u	k7c2	u
nižších	nízký	k2eAgFnPc2d2	nižší
hodnot	hodnota	k1gFnPc2	hodnota
IQ	iq	kA	iq
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
13	[number]	k4	13
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
méně	málo	k6eAd2	málo
než	než	k8xS	než
70	[number]	k4	70
opět	opět	k6eAd1	opět
méně	málo	k6eAd2	málo
než	než	k8xS	než
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
–	–	k?	–
tyto	tento	k3xDgFnPc1	tento
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnPc1d1	orientační
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Skóre	skóre	k1gNnSc1	skóre
pod	pod	k7c7	pod
70	[number]	k4	70
body	bod	k1gInPc7	bod
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
inteligenční	inteligenční	k2eAgInSc1d1	inteligenční
kvocient	kvocient	k1gInSc1	kvocient
ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klasifikace	klasifikace	k1gFnSc2	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
není	být	k5eNaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
kritériem	kritérion	k1gNnSc7	kritérion
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgFnPc1d1	důležitá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
adaptační	adaptační	k2eAgFnPc4d1	adaptační
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Testování	testování	k1gNnSc1	testování
inteligence	inteligence	k1gFnSc2	inteligence
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
studentů	student	k1gMnPc2	student
k	k	k7c3	k
predikci	predikce	k1gFnSc3	predikce
možných	možný	k2eAgInPc2d1	možný
studijních	studijní	k2eAgInPc2d1	studijní
problémů	problém	k1gInPc2	problém
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
studijního	studijní	k2eAgInSc2d1	studijní
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
matematických	matematický	k2eAgFnPc2d1	matematická
schopností	schopnost	k1gFnPc2	schopnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
k	k	k7c3	k
predikci	predikce	k1gFnSc3	predikce
lepší	lepšit	k5eAaImIp3nS	lepšit
než	než	k8xS	než
IQ	iq	kA	iq
testy	test	k1gInPc4	test
či	či	k8xC	či
testy	test	k1gInPc4	test
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Určitá	určitý	k2eAgFnSc1d1	určitá
hodnota	hodnota	k1gFnSc1	hodnota
IQ	iq	kA	iq
nemusí	muset	k5eNaImIp3nS	muset
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
životě	život	k1gInSc6	život
člověka	člověk	k1gMnSc2	člověk
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
a	a	k8xC	a
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
charakteru	charakter	k1gInSc6	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
schopnost	schopnost	k1gFnSc1	schopnost
odolat	odolat	k5eAaPmF	odolat
pokušení	pokušení	k1gNnSc4	pokušení
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
studijní	studijní	k2eAgInPc4d1	studijní
výsledky	výsledek	k1gInPc4	výsledek
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
IQ	iq	kA	iq
<g/>
.	.	kIx.	.
</s>
<s>
Mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
označuje	označovat	k5eAaImIp3nS	označovat
výrazně	výrazně	k6eAd1	výrazně
sníženou	snížený	k2eAgFnSc4d1	snížená
inteligenci	inteligence	k1gFnSc4	inteligence
oproti	oproti	k7c3	oproti
průměru	průměr	k1gInSc3	průměr
populace	populace	k1gFnSc2	populace
daného	daný	k2eAgInSc2d1	daný
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
většinou	většinou	k6eAd1	většinou
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
schopnost	schopnost	k1gFnSc4	schopnost
vést	vést	k5eAaImF	vést
běžný	běžný	k2eAgInSc4d1	běžný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
rozdělení	rozdělení	k1gNnSc1	rozdělení
mentální	mentální	k2eAgFnSc2d1	mentální
retardace	retardace	k1gFnSc2	retardace
v	v	k7c6	v
termínech	termín	k1gInPc6	termín
debil	debil	k1gMnSc1	debil
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
imbecil	imbecil	k1gMnSc1	imbecil
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
a	a	k8xC	a
idiot	idiot	k1gMnSc1	idiot
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
pod	pod	k7c7	pod
35	[number]	k4	35
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
velmi	velmi	k6eAd1	velmi
pejorativního	pejorativní	k2eAgInSc2d1	pejorativní
významu	význam	k1gInSc2	význam
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
lehkou	lehký	k2eAgFnSc4d1	lehká
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těžkou	těžký	k2eAgFnSc7d1	těžká
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
pod	pod	k7c7	pod
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
nejnovější	nový	k2eAgFnSc1d3	nejnovější
klasifikace	klasifikace	k1gFnSc1	klasifikace
Americké	americký	k2eAgFnSc2d1	americká
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
společnosti	společnost	k1gFnSc2	společnost
DSM-V	DSM-V	k1gFnSc2	DSM-V
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
neschopnost	neschopnost	k1gFnSc1	neschopnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
intellectual	intellectual	k1gInSc1	intellectual
disability	disabilita	k1gFnSc2	disabilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
výrazných	výrazný	k2eAgFnPc2d1	výrazná
odchylek	odchylka	k1gFnPc2	odchylka
je	být	k5eAaImIp3nS	být
však	však	k9	však
nemožné	možný	k2eNgNnSc1d1	nemožné
a	a	k8xC	a
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
konkrétní	konkrétní	k2eAgFnSc3d1	konkrétní
osobě	osoba	k1gFnSc3	osoba
neefektivní	efektivní	k2eNgMnSc1d1	neefektivní
stanovovat	stanovovat	k5eAaImF	stanovovat
přesnou	přesný	k2eAgFnSc4d1	přesná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
klinický	klinický	k2eAgInSc4d1	klinický
popis	popis	k1gInSc4	popis
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
daný	daný	k2eAgMnSc1d1	daný
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
hodnoty	hodnota	k1gFnPc1	hodnota
IQ	iq	kA	iq
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
neustále	neustále	k6eAd1	neustále
stoupají	stoupat	k5eAaImIp3nP	stoupat
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
body	bod	k1gInPc4	bod
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fenomén	fenomén	k1gInSc1	fenomén
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Flynnův	Flynnův	k2eAgInSc1d1	Flynnův
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jeho	jeho	k3xOp3gFnPc1	jeho
příčiny	příčina	k1gFnPc1	příčina
leží	ležet	k5eAaImIp3nP	ležet
ve	v	k7c6	v
skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
zvyšující	zvyšující	k2eAgFnSc3d1	zvyšující
inteligenci	inteligence	k1gFnSc3	inteligence
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
již	již	k9	již
díky	díky	k7c3	díky
intenzivnějšímu	intenzivní	k2eAgNnSc3d2	intenzivnější
vzdělávání	vzdělávání	k1gNnSc3	vzdělávání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
díky	díky	k7c3	díky
lepší	dobrý	k2eAgFnSc3d2	lepší
výživě	výživa	k1gFnSc3	výživa
či	či	k8xC	či
dědičnosti	dědičnost	k1gFnSc3	dědičnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
možné	možný	k2eAgInPc1d1	možný
důvody	důvod	k1gInPc1	důvod
tohoto	tento	k3xDgNnSc2	tento
navyšování	navyšování	k1gNnSc2	navyšování
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
metodologické	metodologický	k2eAgFnPc4d1	metodologická
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
testování	testování	k1gNnSc6	testování
IQ	iq	kA	iq
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
obeznámenost	obeznámenost	k1gFnSc1	obeznámenost
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc1	jaký
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
IQ	iq	kA	iq
testech	test	k1gInPc6	test
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jiní	jiný	k2eAgMnPc1d1	jiný
badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgMnPc1d1	vycházející
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
inteligencí	inteligence	k1gFnSc7	inteligence
a	a	k8xC	a
reakční	reakční	k2eAgFnSc7d1	reakční
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
inteligence	inteligence	k1gFnSc1	inteligence
v	v	k7c4	v
posledních	poslední	k2eAgNnPc2d1	poslední
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
letech	léto	k1gNnPc6	léto
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dlouhodobějšího	dlouhodobý	k2eAgNnSc2d2	dlouhodobější
hlediska	hledisko	k1gNnSc2	hledisko
však	však	k9	však
odbornou	odborný	k2eAgFnSc7d1	odborná
komunitou	komunita	k1gFnSc7	komunita
není	být	k5eNaImIp3nS	být
pokles	pokles	k1gInSc1	pokles
přijímán	přijímat	k5eAaImNgInS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
u	u	k7c2	u
žáků	žák	k1gMnPc2	žák
posledních	poslední	k2eAgInPc2d1	poslední
ročníků	ročník	k1gInPc2	ročník
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
přírůstek	přírůstek	k1gInSc1	přírůstek
IQ	iq	kA	iq
mezi	mezi	k7c4	mezi
70	[number]	k4	70
<g/>
.	.	kIx.	.
roky	rok	k1gInPc1	rok
a	a	k8xC	a
současností	současnost	k1gFnSc7	současnost
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
různého	různý	k2eAgInSc2d1	různý
typu	typ	k1gInSc2	typ
inteligenčního	inteligenční	k2eAgInSc2d1	inteligenční
testu	test	k1gInSc2	test
2,3	[number]	k4	2,3
<g/>
–	–	k?	–
<g/>
4,2	[number]	k4	4,2
bodů	bod	k1gInPc2	bod
IQ	iq	kA	iq
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
