<s>
Inteligenční	inteligenční	k2eAgInSc1d1	inteligenční
kvocient	kvocient	k1gInSc1	kvocient
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
standardizované	standardizovaný	k2eAgNnSc1d1	standardizované
skóre	skóre	k1gNnSc4	skóre
používané	používaný	k2eAgNnSc4d1	používané
jako	jako	k9	jako
výstup	výstup	k1gInSc4	výstup
standardizovaných	standardizovaný	k2eAgInPc2d1	standardizovaný
inteligenčních	inteligenční	k2eAgInPc2d1	inteligenční
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
výkonových	výkonový	k2eAgInPc2d1	výkonový
<g/>
)	)	kIx)	)
psychologických	psychologický	k2eAgInPc2d1	psychologický
testů	test	k1gInPc2	test
k	k	k7c3	k
vyčíslení	vyčíslení	k1gNnSc3	vyčíslení
inteligence	inteligence	k1gFnSc2	inteligence
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
ostatní	ostatní	k2eAgFnSc3d1	ostatní
populaci	populace	k1gFnSc3	populace
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
skupině	skupina	k1gFnSc3	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
