<s>
Embryologie	embryologie	k1gFnSc1	embryologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
zárodku	zárodek	k1gInSc6	zárodek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
embryo	embryo	k1gNnSc1	embryo
=	=	kIx~	=
zárodek	zárodek	k1gInSc1	zárodek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
individuálního	individuální	k2eAgInSc2d1	individuální
vývoje	vývoj	k1gInSc2	vývoj
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
prenatálním	prenatální	k2eAgNnSc6d1	prenatální
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
