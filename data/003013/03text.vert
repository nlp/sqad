<s>
Embryologie	embryologie	k1gFnSc1	embryologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
zárodku	zárodek	k1gInSc6	zárodek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
embryo	embryo	k1gNnSc1	embryo
=	=	kIx~	=
zárodek	zárodek	k1gInSc1	zárodek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
individuálního	individuální	k2eAgInSc2d1	individuální
vývoje	vývoj	k1gInSc2	vývoj
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
prenatálním	prenatální	k2eAgNnSc6d1	prenatální
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
oplození	oplození	k1gNnSc2	oplození
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
oplozené	oplozený	k2eAgNnSc1d1	oplozené
vajíčko	vajíčko	k1gNnSc1	vajíčko
=	=	kIx~	=
zygota	zygota	k1gFnSc1	zygota
<g/>
)	)	kIx)	)
po	po	k7c4	po
porod	porod	k1gInSc4	porod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
objektů	objekt	k1gInPc2	objekt
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
embryologii	embryologie	k1gFnSc4	embryologie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Morfologická	morfologický	k2eAgFnSc1d1	morfologická
embryologie	embryologie	k1gFnSc1	embryologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
tvarové	tvarový	k2eAgFnPc4d1	tvarová
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
embryologie	embryologie	k1gFnSc1	embryologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vývoj	vývoj	k1gInSc4	vývoj
funkce	funkce	k1gFnSc2	funkce
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgFnSc2d1	experimentální
embryologie	embryologie	k1gFnSc2	embryologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
příčiny	příčina	k1gFnPc4	příčina
změn	změna	k1gFnPc2	změna
experimentálními	experimentální	k2eAgFnPc7d1	experimentální
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
embryologie	embryologie	k1gFnSc1	embryologie
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
vývoj	vývoj	k1gInSc4	vývoj
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
embryologie	embryologie	k1gFnSc2	embryologie
je	být	k5eAaImIp3nS	být
K.	K.	kA	K.
E.	E.	kA	E.
von	von	k1gInSc1	von
Baer	Baer	k1gInSc1	Baer
<g/>
.	.	kIx.	.
biologie	biologie	k1gFnSc1	biologie
vývojová	vývojový	k2eAgFnSc1d1	vývojová
biologie	biologie	k1gFnSc2	biologie
embryologie	embryologie	k1gFnSc2	embryologie
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Embryonální	embryonální	k2eAgInSc4d1	embryonální
vývoj	vývoj	k1gInSc4	vývoj
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
zárodku	zárodek	k1gInSc2	zárodek
(	(	kIx(	(
<g/>
embryo	embryo	k1gNnSc1	embryo
<g/>
)	)	kIx)	)
=	=	kIx~	=
embryogeneze	embryogeneze	k1gFnSc1	embryogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vývoje	vývoj	k1gInSc2	vývoj
embrya	embryo	k1gNnSc2	embryo
a	a	k8xC	a
fétu	fétus	k1gInSc2	fétus
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
vývoj	vývoj	k1gInSc1	vývoj
jedince	jedinec	k1gMnSc2	jedinec
od	od	k7c2	od
početí	početí	k1gNnSc2	početí
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
<g/>
.	.	kIx.	.
vajíčko	vajíčko	k1gNnSc1	vajíčko
(	(	kIx(	(
<g/>
ovum	ovum	k1gInSc1	ovum
<g/>
)	)	kIx)	)
+	+	kIx~	+
spermie	spermie	k1gFnSc1	spermie
(	(	kIx(	(
<g/>
spermatozoon	spermatozoon	k1gInSc1	spermatozoon
<g/>
)	)	kIx)	)
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
+	+	kIx~	+
<g/>
---------------	---------------	k?	---------------
<g/>
+	+	kIx~	+
|	|	kIx~	|
|	|	kIx~	|
spojení	spojení	k1gNnSc1	spojení
gamet	gameta	k1gFnPc2	gameta
(	(	kIx(	(
<g/>
=	=	kIx~	=
oplození	oplození	k1gNnSc1	oplození
<g/>
)	)	kIx)	)
|	|	kIx~	|
---	---	k?	---
zygota	zygota	k1gFnSc1	zygota
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s>
rýhování	rýhování	k1gNnSc1	rýhování
(	(	kIx(	(
<g/>
mitotické	mitotický	k2eAgNnSc1d1	mitotické
dělení	dělení	k1gNnSc1	dělení
zygoty	zygota	k1gFnSc2	zygota
<g/>
)	)	kIx)	)
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
blastoméry	blastoméra	k1gFnSc2	blastoméra
=	=	kIx~	=
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
buňky	buňka	k1gFnSc2	buňka
embryo	embryo	k1gNnSc1	embryo
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
morula	morula	k1gFnSc1	morula
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nS	tvořit
ji	on	k3xPp3gFnSc4	on
16	[number]	k4	16
a	a	k8xC	a
více	hodně	k6eAd2	hodně
blastomér	blastomér	k1gInSc4	blastomér
<g/>
)	)	kIx)	)
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
blastocysta	blastocysta	k1gFnSc1	blastocysta
(	(	kIx(	(
<g/>
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
ploché	plochý	k2eAgFnPc4d1	plochá
buňky	buňka	k1gFnPc4	buňka
-	-	kIx~	-
trofoblast	trofoblast	k1gInSc1	trofoblast
a	a	k8xC	a
buněčný	buněčný	k2eAgInSc1d1	buněčný
uzel	uzel	k1gInSc1	uzel
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
embryoblast	embryoblast	k1gInSc1	embryoblast
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
implantace	implantace	k1gFnSc1	implantace
=	=	kIx~	=
nidace	nidace	k1gFnSc1	nidace
=	=	kIx~	=
uhnízdění	uhnízdění	k1gNnSc1	uhnízdění
na	na	k7c4	na
děložní	děložní	k2eAgFnSc4d1	děložní
sliznici	sliznice	k1gFnSc4	sliznice
|	|	kIx~	|
|	|	kIx~	|
---	---	k?	---
|	|	kIx~	|
plod	plod	k1gInSc4	plod
(	(	kIx(	(
<g/>
fétus	fétus	k1gInSc4	fétus
<g/>
)	)	kIx)	)
=	=	kIx~	=
jedinec	jedinec	k1gMnSc1	jedinec
ve	v	k7c6	v
fetálním	fetální	k2eAgNnSc6d1	fetální
vývojovém	vývojový	k2eAgNnSc6d1	vývojové
období	období	k1gNnSc6	období
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
lunárního	lunární	k2eAgInSc2d1	lunární
měsíce	měsíc	k1gInSc2	měsíc
do	do	k7c2	do
porodu	porod	k1gInSc2	porod
<g/>
:	:	kIx,	:
vývoj	vývoj	k1gInSc1	vývoj
orgánových	orgánový	k2eAgInPc2d1	orgánový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
růst	růst	k1gInSc4	růst
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
diferenciace	diferenciace	k1gFnSc1	diferenciace
orgánů	orgán	k1gInPc2	orgán
<g />
.	.	kIx.	.
</s>
<s>
Embryo	embryo	k1gNnSc1	embryo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
týdne	týden	k1gInSc2	týden
intrauterinního	intrauterinní	k2eAgInSc2d1	intrauterinní
vývoje	vývoj	k1gInSc2	vývoj
<g/>
:	:	kIx,	:
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
zárodečných	zárodečný	k2eAgInPc2d1	zárodečný
listů	list	k1gInPc2	list
zárodečný	zárodečný	k2eAgInSc4d1	zárodečný
terčík	terčík	k1gInSc4	terčík
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgInPc1d1	hlavní
orgánové	orgánový	k2eAgInPc1d1	orgánový
základy	základ	k1gInPc4	základ
crown-rump	crownump	k1gMnSc1	crown-rump
length	length	k1gMnSc1	length
(	(	kIx(	(
<g/>
CRL	CRL	kA	CRL
<g/>
)	)	kIx)	)
=	=	kIx~	=
temenokostrční	temenokostrční	k2eAgFnSc1d1	temenokostrční
délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
TKD	TKD	kA	TKD
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
pomoci	pomoct	k5eAaPmF	pomoct
sonografie	sonografie	k1gFnPc4	sonografie
pro	pro	k7c4	pro
příklad	příklad	k1gInSc4	příklad
(	(	kIx(	(
<g/>
přibližné	přibližný	k2eAgFnPc1d1	přibližná
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
týden	týden	k1gInSc1	týden
TKD	TKD	kA	TKD
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
75	[number]	k4	75
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
týdnu	týden	k1gInSc6	týden
160	[number]	k4	160
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
28	[number]	k4	28
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
<g />
.	.	kIx.	.
</s>
<s>
270	[number]	k4	270
<g/>
mm	mm	kA	mm
a	a	k8xC	a
v	v	k7c6	v
38	[number]	k4	38
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
360	[number]	k4	360
<g/>
mm	mm	kA	mm
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
.	.	kIx.	.
global	globat	k5eAaImAgMnS	globat
length	length	k1gMnSc1	length
(	(	kIx(	(
<g/>
GL	GL	kA	GL
<g/>
)	)	kIx)	)
=	=	kIx~	=
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
temenopatní	temenopatní	k2eAgFnSc1d1	temenopatní
délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
TPD	TPD	kA	TPD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
embrya	embryo	k1gNnSc2	embryo
(	(	kIx(	(
<g/>
po	po	k7c4	po
fétus	fétus	k1gInSc4	fétus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
CRL	CRL	kA	CRL
a	a	k8xC	a
GL	GL	kA	GL
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc4d1	stejné
<g/>
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
příklad	příklad	k1gInSc4	příklad
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
týdnu	týden	k1gInSc6	týden
TPD	TPD	kA	TPD
30	[number]	k4	30
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
týden	týden	k1gInSc4	týden
90	[number]	k4	90
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
týden	týden	k1gInSc4	týden
160	[number]	k4	160
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
<g/>
týden	týden	k1gInSc4	týden
350	[number]	k4	350
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
38	[number]	k4	38
<g/>
.	.	kIx.	.
týden	týden	k1gInSc4	týden
500	[number]	k4	500
<g/>
mm	mm	kA	mm
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
přibližné	přibližný	k2eAgFnSc2d1	přibližná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
)	)	kIx)	)
sir	sir	k1gMnSc1	sir
Julian	Julian	k1gMnSc1	Julian
Huxley	Huxlea	k1gFnSc2	Huxlea
Hans	Hans	k1gMnSc1	Hans
Spemann	Spemann	k1gMnSc1	Spemann
Karl	Karl	k1gMnSc1	Karl
Ernst	Ernst	k1gMnSc1	Ernst
von	von	k1gInSc4	von
Baer	Baer	k1gMnSc1	Baer
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
embryologie	embryologie	k1gFnSc2	embryologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Animace	animace	k1gFnSc1	animace
z	z	k7c2	z
embryologie	embryologie	k1gFnSc2	embryologie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
