<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
vývoje	vývoj	k1gInSc2	vývoj
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
prenatálním	prenatální	k2eAgNnSc6d1	prenatální
období	období	k1gNnSc6	období
<g/>
?	?	kIx.	?
</s>
