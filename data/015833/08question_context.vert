<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc1
znalostí	znalost	k1gFnPc2
<g/>
,	,	kIx,
teorií	teorie	k1gFnPc2
a	a	k8xC
modelů	model	k1gInPc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jak	jak	k6eAd1
jsou	být	k5eAaImIp3nP
vyučovány	vyučován	k2eAgFnPc1d1
univerzitami	univerzita	k1gFnPc7
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
ekonomy	ekonom	k1gMnPc4
obecně	obecně	k6eAd1
přijímány	přijímán	k2eAgInPc1d1
jako	jako	k8xC,k8xS
základ	základ	k1gInSc1
pro	pro	k7c4
diskusi	diskuse	k1gFnSc4
<g/>
.	.	kIx.
</s>