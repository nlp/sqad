<s desamb="1">
Známá	známý	k2eAgFnSc1d1
také	také	k6eAd1
jako	jako	k9
ortodoxní	ortodoxní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
v	v	k7c6
kontrastu	kontrast	k1gInSc6
s	s	k7c7
heterodoxní	heterodoxní	k2eAgFnSc7d1
ekonomií	ekonomie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
školy	škola	k1gFnPc4
nebo	nebo	k8xC
přístupy	přístup	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
přijímá	přijímat	k5eAaImIp3nS
pouze	pouze	k6eAd1
menšina	menšina	k1gFnSc1
ekonomů	ekonom	k1gMnPc2
<g/>
.	.	kIx.
</s>