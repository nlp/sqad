<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
směry	směr	k1gInPc1
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
(	(	kIx(
<g/>
ortodoxní	ortodoxní	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
</s>
<s>
Neokeynesiánství	Neokeynesiánství	k1gNnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
keynesiánská	keynesiánský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
</s>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
</s>
<s>
Heterodoxní	heterodoxní	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
</s>
<s>
Ekologická	ekologický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Feministická	feministický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Georgismus	Georgismus	k1gInSc1
</s>
<s>
Institucionální	institucionální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Termoekonomie	Termoekonomie	k1gFnSc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
institucionální	institucionální	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
A	a	k9
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
...	...	k?
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc1
znalostí	znalost	k1gFnPc2
<g/>
,	,	kIx,
teorií	teorie	k1gFnPc2
a	a	k8xC
modelů	model	k1gInPc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jak	jak	k6eAd1
jsou	být	k5eAaImIp3nP
vyučovány	vyučován	k2eAgFnPc1d1
univerzitami	univerzita	k1gFnPc7
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
ekonomy	ekonom	k1gMnPc4
obecně	obecně	k6eAd1
přijímány	přijímán	k2eAgInPc1d1
jako	jako	k8xC,k8xS
základ	základ	k1gInSc1
pro	pro	k7c4
diskusi	diskuse	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známá	známý	k2eAgFnSc1d1
také	také	k6eAd1
jako	jako	k9
ortodoxní	ortodoxní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
v	v	k7c6
kontrastu	kontrast	k1gInSc6
s	s	k7c7
heterodoxní	heterodoxní	k2eAgFnSc7d1
ekonomií	ekonomie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
školy	škola	k1gFnPc4
nebo	nebo	k8xC
přístupy	přístup	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
přijímá	přijímat	k5eAaImIp3nS
pouze	pouze	k6eAd1
menšina	menšina	k1gFnSc1
ekonomů	ekonom	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
ekonomie	ekonomie	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
lze	lze	k6eAd1
počítat	počítat	k5eAaImF
neoklasickou	neoklasický	k2eAgFnSc4d1
ekonomii	ekonomie	k1gFnSc4
<g/>
,	,	kIx,
novou	nový	k2eAgFnSc4d1
klasickou	klasický	k2eAgFnSc4d1
makroekonomii	makroekonomie	k1gFnSc4
<g/>
,	,	kIx,
neoklasickou	neoklasický	k2eAgFnSc4d1
syntézu	syntéza	k1gFnSc4
a	a	k8xC
směry	směr	k1gInPc4
soustředěné	soustředěný	k2eAgInPc4d1
kolem	kolem	k7c2
keynesiánství	keynesiánství	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
neokeynesiánství	neokeynesiánství	k1gNnSc4
a	a	k8xC
novou	nový	k2eAgFnSc4d1
keynesiánskou	keynesiánský	k2eAgFnSc4d1
ekonomii	ekonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
a	a	k8xC
rozdíly	rozdíl	k1gInPc1
proti	proti	k7c3
heterodoxní	heterodoxní	k2eAgFnSc3d1
ekonomii	ekonomie	k1gFnSc3
</s>
<s>
Panuje	panovat	k5eAaImIp3nS
shoda	shoda	k1gFnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
druhá	druhý	k4xOgFnSc1
polovina	polovina	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
složila	složit	k5eAaPmAgFnS
„	„	k?
<g/>
labutí	labutí	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
<g/>
“	“	k?
pozitivismu	pozitivismus	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
filozofie	filozofie	k1gFnSc2
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
a	a	k8xC
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
<g/>
,	,	kIx,
dva	dva	k4xCgMnPc1
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
ekonomů	ekonom	k1gMnPc2
a	a	k8xC
současně	současně	k6eAd1
dva	dva	k4xCgMnPc4
z	z	k7c2
nej-vlivnějších	-vlivný	k2eAgMnPc2d3
autorů	autor	k1gMnPc2
k	k	k7c3
metodologii	metodologie	k1gFnSc3
ekonomie	ekonomie	k1gFnSc2
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
tento	tento	k3xDgInSc4
vývoj	vývoj	k1gInSc4
ve	v	k7c6
filosofii	filosofie	k1gFnSc6
vědy	věda	k1gFnSc2
nikdy	nikdy	k6eAd1
nereflektovali	reflektovat	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Například	například	k6eAd1
Neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
i	i	k8xC
rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgInP
na	na	k7c4
teorii	teorie	k1gFnSc4
mezní	mezní	k2eAgFnSc2d1
užitečnosti	užitečnost	k1gFnSc2
a	a	k8xC
subjektivní	subjektivní	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
napomohla	napomoct	k5eAaPmAgFnS
rozšíření	rozšíření	k1gNnSc3
matematické	matematický	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
napříč	napříč	k7c7
různými	různý	k2eAgFnPc7d1
ekonomickými	ekonomický	k2eAgFnPc7d1
školami	škola	k1gFnPc7
a	a	k8xC
naopak	naopak	k6eAd1
rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
největším	veliký	k2eAgMnSc7d3
kritikem	kritik	k1gMnSc7
matematizace	matematizace	k1gFnSc2
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mainstream	Mainstream	k1gInSc1
economics	economics	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
KOVANDA	Kovanda	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složila	složit	k5eAaPmAgFnS
finanční	finanční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
„	„	k?
<g/>
labutí	labutí	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
<g/>
“	“	k?
pozitivistické	pozitivistický	k2eAgFnSc3d1
ekonomii	ekonomie	k1gFnSc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
vědy	věda	k1gFnSc2
/	/	kIx~
Theory	Theora	k1gFnPc1
of	of	k?
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
36	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
421	#num#	k4
<g/>
–	–	k?
<g/>
436	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1804	#num#	k4
<g/>
-	-	kIx~
<g/>
6347	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PFEIFER	PFEIFER	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nereálné	reálný	k2eNgInPc4d1
předpoklady	předpoklad	k1gInPc4
ekonomie	ekonomie	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
jako	jako	k8xC,k8xS
podmínka	podmínka	k1gFnSc1
matematizace	matematizace	k1gFnSc2
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
E-LOGOS	E-LOGOS	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
