<s>
Olmékové	Olmékové	k2eAgFnSc1d1	Olmékové
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
hlavní	hlavní	k2eAgNnSc1d1	hlavní
středisko	středisko	k1gNnSc1	středisko
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Veracruz	Veracruza	k1gFnPc2	Veracruza
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
rostl	růst	k5eAaImAgInS	růst
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Pojménování	Pojménování	k1gNnSc1	Pojménování
Olmékové	Olméková	k1gFnSc2	Olméková
(	(	kIx(	(
<g/>
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
nahuatl	nahuatnout	k5eAaPmAgMnS	nahuatnout
"	"	kIx"	"
<g/>
kaučukoví	kaučukový	k2eAgMnPc1d1	kaučukový
lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
příslušníci	příslušník	k1gMnPc1	příslušník
etnika	etnikum	k1gNnSc2	etnikum
Olméků	Olméek	k1gMnPc2	Olméek
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pobřeží	pobřeží	k1gNnSc2	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Aztéků	Azték	k1gMnPc2	Azték
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
tvůrců	tvůrce	k1gMnPc2	tvůrce
Olmécké	Olmécký	k2eAgFnSc2d1	Olmécká
kultury	kultura	k1gFnSc2	kultura
asi	asi	k9	asi
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
jazykové	jazykový	k2eAgFnSc3d1	jazyková
rodině	rodina	k1gFnSc3	rodina
mixe	mix	k1gInSc5	mix
soque	soque	k1gFnPc3	soque
<g/>
.	.	kIx.	.
</s>
<s>
Posvátné	posvátný	k2eAgFnPc4d1	posvátná
míčové	míčový	k2eAgFnPc4d1	Míčová
hry	hra	k1gFnPc4	hra
hráli	hrát	k5eAaImAgMnP	hrát
již	již	k6eAd1	již
mixe-zoqueové	mixeoqueové	k2eAgNnSc4d1	mixe-zoqueové
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c4	v
Paso	Paso	k1gNnSc4	Paso
de	de	k?	de
la	la	k1gNnSc2	la
Amada	Amad	k1gMnSc2	Amad
<g/>
,	,	kIx,	,
1600	[number]	k4	1600
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
z	z	k7c2	z
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k6eAd1	právě
tento	tento	k3xDgInSc4	tento
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
vlastnost	vlastnost	k1gFnSc4	vlastnost
odrážet	odrážet	k5eAaImF	odrážet
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
překvapila	překvapit	k5eAaPmAgFnS	překvapit
Evropany	Evropan	k1gMnPc4	Evropan
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
těch	ten	k3xDgNnPc6	ten
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
sledovat	sledovat	k5eAaImF	sledovat
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
soustředili	soustředit	k5eAaPmAgMnP	soustředit
jen	jen	k9	jen
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hřiště	hřiště	k1gNnSc1	hřiště
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kulturách	kultura	k1gFnPc6	kultura
a	a	k8xC	a
průběhem	průběh	k1gInSc7	průběh
času	čas	k1gInSc2	čas
jiný	jiný	k2eAgInSc4d1	jiný
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Lišil	lišit	k5eAaImAgMnS	lišit
se	se	k3xPyFc4	se
jak	jak	k8xC	jak
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
-	-	kIx~	-
Mayské	mayský	k2eAgFnSc2d1	mayská
nádoby	nádoba	k1gFnSc2	nádoba
často	často	k6eAd1	často
vyobrazují	vyobrazovat	k5eAaImIp3nP	vyobrazovat
utkání	utkání	k1gNnSc4	utkání
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
vládci	vládce	k1gMnPc7	vládce
nebo	nebo	k8xC	nebo
bohy	bůh	k1gMnPc7	bůh
až	až	k8xS	až
po	po	k7c4	po
velká	velký	k2eAgNnPc4d1	velké
družstva	družstvo	k1gNnPc4	družstvo
-	-	kIx~	-
tak	tak	k6eAd1	tak
i	i	k8xC	i
tvar	tvar	k1gInSc4	tvar
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
nesměli	smět	k5eNaImAgMnP	smět
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
míč	míč	k1gInSc1	míč
spadl	spadnout	k5eAaPmAgInS	spadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
,	,	kIx,	,
nesměli	smět	k5eNaImAgMnP	smět
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
udržet	udržet	k5eAaPmF	udržet
pomocí	pomocí	k7c2	pomocí
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
směli	smět	k5eAaImAgMnP	smět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
ramena	rameno	k1gNnSc2	rameno
<g/>
,	,	kIx,	,
boky	bok	k1gInPc1	bok
<g/>
,	,	kIx,	,
kolena	koleno	k1gNnPc1	koleno
a	a	k8xC	a
lokty	loket	k1gInPc1	loket
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jim	on	k3xPp3gMnPc3	on
chránily	chránit	k5eAaImAgInP	chránit
kožené	kožený	k2eAgInPc1d1	kožený
chrániče	chránič	k1gInPc1	chránič
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ještě	ještě	k9	ještě
složitější	složitý	k2eAgMnSc1d2	složitější
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
míč	míč	k1gInSc1	míč
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
prohodit	prohodit	k5eAaPmF	prohodit
kruhem	kruh	k1gInSc7	kruh
uprostřed	uprostřed	k7c2	uprostřed
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
významnou	významný	k2eAgFnSc7d1	významná
rituální	rituální	k2eAgFnSc7d1	rituální
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poražené	poražený	k2eAgNnSc1d1	poražené
družstvo	družstvo	k1gNnSc1	družstvo
bylo	být	k5eAaImAgNnS	být
popraveno	popraven	k2eAgNnSc1d1	popraveno
a	a	k8xC	a
obětováno	obětován	k2eAgNnSc1d1	obětováno
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
olméckých	olmécký	k2eAgNnPc2d1	olmécký
obřadních	obřadní	k2eAgNnPc2d1	obřadní
center	centrum	k1gNnPc2	centrum
byly	být	k5eAaImAgInP	být
kurty	kurt	k1gInPc1	kurt
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgNnP	hrát
již	již	k6eAd1	již
výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
míčová	míčový	k2eAgFnSc1d1	Míčová
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
dvě	dva	k4xCgFnPc4	dva
takováto	takovýto	k3xDgNnPc1	takovýto
hlavní	hlavní	k2eAgNnPc1d1	hlavní
obřadní	obřadní	k2eAgNnPc1d1	obřadní
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
San	San	k1gFnSc6	San
Lorenzu	Lorenzu	k?	Lorenzu
a	a	k8xC	a
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
roku	rok	k1gInSc2	rok
900	[number]	k4	900
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nové	Nové	k2eAgNnSc1d1	Nové
středisko	středisko	k1gNnSc1	středisko
stálo	stát	k5eAaImAgNnS	stát
v	v	k7c6	v
La	la	k1gNnSc6	la
Ventě	Venť	k1gFnSc2	Venť
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nejvýraznějšími	výrazný	k2eAgInPc7d3	nejvýraznější
prvky	prvek	k1gInPc7	prvek
těchto	tento	k3xDgInPc2	tento
středisek	středisko	k1gNnPc2	středisko
byly	být	k5eAaImAgInP	být
pyramidy	pyramid	k1gInPc1	pyramid
nebo	nebo	k8xC	nebo
mohyly	mohyla	k1gFnSc2	mohyla
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
mohyly	mohyla	k1gFnPc1	mohyla
mívaly	mívat	k5eAaImAgFnP	mívat
často	často	k6eAd1	často
tvar	tvar	k1gInSc4	tvar
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mezoameriky	Mezoamerika	k1gFnSc2	Mezoamerika
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
činných	činný	k2eAgFnPc2d1	činná
sopek	sopka	k1gFnPc2	sopka
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
je	on	k3xPp3gMnPc4	on
uctívali	uctívat	k5eAaImAgMnP	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
posvátnými	posvátný	k2eAgInPc7d1	posvátný
monumenty	monument	k1gInPc7	monument
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
obřadních	obřadní	k2eAgNnPc6d1	obřadní
centrech	centrum	k1gNnPc6	centrum
oltáře	oltář	k1gInSc2	oltář
zdobené	zdobený	k2eAgInPc4d1	zdobený
reliéfy	reliéf	k1gInPc4	reliéf
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Plastiky	plastika	k1gFnPc1	plastika
Olméků	Olméek	k1gMnPc2	Olméek
nám	my	k3xPp1nPc3	my
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
národ	národ	k1gInSc4	národ
daroval	darovat	k5eAaPmAgMnS	darovat
bohům	bůh	k1gMnPc3	bůh
spoustu	spousta	k1gFnSc4	spousta
dospělých	dospělý	k2eAgFnPc2d1	dospělá
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
i	i	k8xC	i
rituální	rituální	k2eAgInSc1d1	rituální
kanibalismus	kanibalismus	k1gInSc1	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
Olmécká	Olmécký	k2eAgFnSc1d1	Olmécká
kultura	kultura	k1gFnSc1	kultura
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
kultur	kultura	k1gFnPc2	kultura
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k9	rovněž
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
stavět	stavět	k5eAaImF	stavět
monumentální	monumentální	k2eAgInPc4d1	monumentální
chrámy	chrám	k1gInPc4	chrám
a	a	k8xC	a
posvátná	posvátný	k2eAgNnPc4d1	posvátné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
se	se	k3xPyFc4	se
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mluvčí	mluvčí	k1gMnPc4	mluvčí
miše-sokeských	mišeokeský	k2eAgInPc2d1	miše-sokeský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
nazývali	nazývat	k5eAaImAgMnP	nazývat
–	–	k?	–
označení	označení	k1gNnSc1	označení
Olmékové	Olméková	k1gFnSc2	Olméková
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
mnohem	mnohem	k6eAd1	mnohem
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
Aztéků	Azték	k1gMnPc2	Azték
'	'	kIx"	'
<g/>
Lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
největšími	veliký	k2eAgMnPc7d3	veliký
středisky	středisko	k1gNnPc7	středisko
byly	být	k5eAaImAgFnP	být
La	la	k1gNnSc4	la
Venta	Vento	k1gNnSc2	Vento
<g/>
,	,	kIx,	,
San	San	k1gFnSc2	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
<g/>
,	,	kIx,	,
Laguna	laguna	k1gFnSc1	laguna
de	de	k?	de
Los	los	k1gInSc4	los
Cerros	Cerrosa	k1gFnPc2	Cerrosa
a	a	k8xC	a
Tres	tresa	k1gFnPc2	tresa
Zapotes	Zapotesa	k1gFnPc2	Zapotesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
největší	veliký	k2eAgFnPc4d3	veliký
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
této	tento	k3xDgFnSc2	tento
bohaté	bohatý	k2eAgFnSc2d1	bohatá
civilizace	civilizace	k1gFnSc2	civilizace
patrně	patrně	k6eAd1	patrně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
stály	stát	k5eAaImAgFnP	stát
příznivé	příznivý	k2eAgFnPc1d1	příznivá
přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
lidem	člověk	k1gMnPc3	člověk
zajistily	zajistit	k5eAaPmAgInP	zajistit
blahobyt	blahobyt	k1gInSc4	blahobyt
a	a	k8xC	a
rozkvět	rozkvět	k1gInSc4	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Olméckou	Olmécký	k2eAgFnSc4d1	Olmécká
kulturu	kultura	k1gFnSc4	kultura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
poprvé	poprvé	k6eAd1	poprvé
identifikoval	identifikovat	k5eAaBmAgInS	identifikovat
a	a	k8xC	a
od	od	k7c2	od
mayské	mayský	k2eAgFnSc2d1	mayská
kultury	kultura	k1gFnSc2	kultura
odlišil	odlišit	k5eAaPmAgInS	odlišit
Marshall	Marshall	k1gInSc1	Marshall
H.	H.	kA	H.
Saville	Saville	k1gInSc1	Saville
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
newyorského	newyorský	k2eAgNnSc2d1	newyorské
Muzea	muzeum	k1gNnSc2	muzeum
amerických	americký	k2eAgMnPc2d1	americký
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
obývali	obývat	k5eAaImAgMnP	obývat
různé	různý	k2eAgInPc4d1	různý
regiony	region	k1gInPc4	region
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mexika	Mexiko	k1gNnSc2	Mexiko
v	v	k7c6	v
období	období	k1gNnSc6	období
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozkvět	rozkvět	k1gInSc1	rozkvět
olmécké	olmécký	k2eAgFnSc2d1	olmécká
civilizace	civilizace	k1gFnSc2	civilizace
nastal	nastat	k5eAaPmAgInS	nastat
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
50	[number]	k4	50
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
střediscích	středisko	k1gNnPc6	středisko
mimo	mimo	k6eAd1	mimo
monumentálních	monumentální	k2eAgFnPc2d1	monumentální
obřadních	obřadní	k2eAgFnPc2d1	obřadní
staveb	stavba	k1gFnPc2	stavba
Olmékové	Olméek	k1gMnPc1	Olméek
tesali	tesat	k5eAaImAgMnP	tesat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
obrovité	obrovitý	k2eAgFnSc2d1	obrovitá
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
poznávacím	poznávací	k2eAgNnSc7d1	poznávací
znamením	znamení	k1gNnSc7	znamení
jejich	jejich	k3xOp3gFnSc2	jejich
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
prvky	prvek	k1gInPc7	prvek
jejich	jejich	k3xOp3gFnSc2	jejich
civilizace	civilizace	k1gFnSc2	civilizace
byly	být	k5eAaImAgInP	být
obrovské	obrovský	k2eAgInPc1d1	obrovský
vytesané	vytesaný	k2eAgInPc1d1	vytesaný
oltáře	oltář	k1gInPc1	oltář
<g/>
,	,	kIx,	,
stély	stéla	k1gFnPc1	stéla
a	a	k8xC	a
tyrkysové	tyrkysový	k2eAgFnPc1d1	tyrkysová
mozaiky	mozaika	k1gFnPc1	mozaika
<g/>
.	.	kIx.	.
</s>
<s>
Olmécká	Olmécký	k2eAgFnSc1d1	Olmécká
kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
po	po	k7c6	po
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
do	do	k7c2	do
severního	severní	k2eAgNnSc2d1	severní
i	i	k8xC	i
jižního	jižní	k2eAgNnSc2d1	jižní
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
pozornosti	pozornost	k1gFnSc2	pozornost
si	se	k3xPyFc3	se
patrně	patrně	k6eAd1	patrně
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
sedmnáct	sedmnáct	k4xCc1	sedmnáct
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
obrovitých	obrovitý	k2eAgFnPc2d1	obrovitá
hlav	hlava	k1gFnPc2	hlava
vytesaných	vytesaný	k2eAgFnPc2d1	vytesaná
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
účel	účel	k1gInSc1	účel
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
hlav	hlava	k1gFnPc2	hlava
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgInPc4d1	jiný
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
společným	společný	k2eAgInSc7d1	společný
prvkem	prvek	k1gInSc7	prvek
jsou	být	k5eAaImIp3nP	být
jakési	jakýsi	k3yIgFnPc1	jakýsi
vytesané	vytesaný	k2eAgFnPc1d1	vytesaná
helmy	helma	k1gFnPc1	helma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jejich	jejich	k3xOp3gInPc1	jejich
rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
–	–	k?	–
od	od	k7c2	od
největší	veliký	k2eAgFnSc2d3	veliký
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
3,4	[number]	k4	3,4
metru	metr	k1gInSc2	metr
po	po	k7c4	po
nejmenší	malý	k2eAgMnPc4d3	nejmenší
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měří	měřit	k5eAaImIp3nS	měřit
1,47	[number]	k4	1,47
m.	m.	k?	m.
Hlavy	hlava	k1gFnPc1	hlava
byly	být	k5eAaImAgFnP	být
vytesány	vytesat	k5eAaPmNgFnP	vytesat
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
bloku	blok	k1gInSc2	blok
sopečného	sopečný	k2eAgInSc2d1	sopečný
čediče	čedič	k1gInSc2	čedič
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Tuxtlas	Tuxtlasa	k1gFnPc2	Tuxtlasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
svého	svůj	k3xOyFgNnSc2	svůj
určení	určení	k1gNnSc2	určení
dopraveny	dopravit	k5eAaPmNgInP	dopravit
do	do	k7c2	do
značných	značný	k2eAgFnPc2d1	značná
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
hlavy	hlava	k1gFnPc1	hlava
byly	být	k5eAaImAgFnP	být
zakopány	zakopat	k5eAaPmNgFnP	zakopat
nebo	nebo	k8xC	nebo
přetesány	přetesat	k5eAaPmNgFnP	přetesat
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
podoby	podoba	k1gFnPc4	podoba
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
patrně	patrně	k6eAd1	patrně
při	při	k7c6	při
dobytí	dobytí	k1gNnSc4	dobytí
novou	nový	k2eAgFnSc7d1	nová
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
uctívali	uctívat	k5eAaImAgMnP	uctívat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
božstev	božstvo	k1gNnPc2	božstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některá	některý	k3yIgFnSc1	některý
uzdravovala	uzdravovat	k5eAaImAgFnS	uzdravovat
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
ovládala	ovládat	k5eAaImAgFnS	ovládat
temné	temný	k2eAgFnPc4d1	temná
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc1	náboženství
bylo	být	k5eAaImAgNnS	být
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
i	i	k8xC	i
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
šamanismus	šamanismus	k1gInSc4	šamanismus
a	a	k8xC	a
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
ducha	duch	k1gMnSc2	duch
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
postavou	postava	k1gFnSc7	postava
olméckých	olmécký	k2eAgNnPc2d1	olmécký
božstev	božstvo	k1gNnPc2	božstvo
byl	být	k5eAaImAgMnS	být
jaguár	jaguár	k1gMnSc1	jaguár
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgInS	uctívat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mnoha	mnoho	k4c2	mnoho
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
uctíváno	uctíván	k2eAgNnSc1d1	uctíváno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
dítě	dítě	k1gNnSc1	dítě
jaguár-člověk	jaguár-člověka	k1gFnPc2	jaguár-člověka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
potomkem	potomek	k1gMnSc7	potomek
jaguářího	jaguáří	k2eAgMnSc2d1	jaguáří
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yIgNnSc1	který
ovládalo	ovládat	k5eAaImAgNnS	ovládat
blesk	blesk	k1gInSc4	blesk
<g/>
,	,	kIx,	,
hrom	hrom	k1gInSc1	hrom
a	a	k8xC	a
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
božstvu	božstvo	k1gNnSc3	božstvo
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
obětována	obětován	k2eAgFnSc1d1	obětována
krev	krev	k1gFnSc1	krev
při	při	k7c6	při
velkých	velký	k2eAgInPc6d1	velký
obřadech	obřad	k1gInPc6	obřad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Obřady	obřad	k1gInPc4	obřad
obvykle	obvykle	k6eAd1	obvykle
vedl	vést	k5eAaImAgMnS	vést
šaman	šaman	k1gMnSc1	šaman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
blíže	blízce	k6eAd2	blízce
božstvům	božstvo	k1gNnPc3	božstvo
užitím	užití	k1gNnSc7	užití
tabákového	tabákový	k2eAgInSc2d1	tabákový
prášku	prášek	k1gInSc2	prášek
nebo	nebo	k8xC	nebo
kouřením	kouření	k1gNnSc7	kouření
směsi	směs	k1gFnSc2	směs
tabáku	tabák	k1gInSc2	tabák
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Kouř	kouř	k1gInSc1	kouř
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnPc2	jejich
náboženství	náboženství	k1gNnPc2	náboženství
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
k	k	k7c3	k
oblakům	oblak	k1gInPc3	oblak
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pak	pak	k6eAd1	pak
padal	padat	k5eAaImAgInS	padat
blahodárný	blahodárný	k2eAgInSc4d1	blahodárný
déšť	déšť	k1gInSc4	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
božstvy	božstvo	k1gNnPc7	božstvo
Olméků	Olmék	k1gInPc2	Olmék
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
první	první	k4xOgMnSc1	první
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
kukuřicový	kukuřicový	k2eAgMnSc1d1	kukuřicový
bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
opeřený	opeřený	k2eAgMnSc1d1	opeřený
had	had	k1gMnSc1	had
a	a	k8xC	a
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
jejich	jejich	k3xOp3gFnSc2	jejich
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
stvořitelem	stvořitel	k1gMnSc7	stvořitel
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Dvojčata	dvojče	k1gNnPc1	dvojče
Hunahpu	Hunahp	k1gInSc2	Hunahp
a	a	k8xC	a
Xbalanque	Xbalanque	k1gFnSc1	Xbalanque
překonala	překonat	k5eAaPmAgFnS	překonat
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
praotci	praotec	k1gMnPc7	praotec
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Kukuřičný	kukuřičný	k2eAgMnSc1d1	kukuřičný
bůh	bůh	k1gMnSc1	bůh
byl	být	k5eAaImAgMnS	být
zosobněním	zosobnění	k1gNnSc7	zosobnění
života	život	k1gInSc2	život
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
opakující	opakující	k2eAgFnPc1d1	opakující
cesty	cesta	k1gFnPc1	cesta
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
podsvětí	podsvětí	k1gNnSc6	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hlava	hlava	k1gFnSc1	hlava
vypadala	vypadat	k5eAaPmAgFnS	vypadat
jako	jako	k8xC	jako
kukuřičný	kukuřičný	k2eAgInSc1d1	kukuřičný
klas	klas	k1gInSc1	klas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
modifikaci	modifikace	k1gFnSc4	modifikace
lebek	lebka	k1gFnPc2	lebka
mayských	mayský	k2eAgMnPc2d1	mayský
novorozenců	novorozenec	k1gMnPc2	novorozenec
z	z	k7c2	z
urozených	urozený	k2eAgFnPc2d1	urozená
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
patrně	patrně	k6eAd1	patrně
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
rituální	rituální	k2eAgNnSc4d1	rituální
pouštění	pouštění	k1gNnSc4	pouštění
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
chrámech	chrám	k1gInPc6	chrám
našlo	najít	k5eAaPmAgNnS	najít
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
keramických	keramický	k2eAgInPc2d1	keramický
trnů	trn	k1gInPc2	trn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
nebyly	být	k5eNaImAgInP	být
doloženy	doložit	k5eAaPmNgInP	doložit
zásadní	zásadní	k2eAgInPc1d1	zásadní
archeologické	archeologický	k2eAgInPc1d1	archeologický
důkazy	důkaz	k1gInPc1	důkaz
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
nalezišti	naleziště	k1gNnSc6	naleziště
El	Ela	k1gFnPc2	Ela
Manatí	Manatý	k2eAgMnPc1d1	Manatý
objeveno	objevit	k5eAaPmNgNnS	objevit
několik	několik	k4yIc1	několik
poškozených	poškozený	k2eAgFnPc2d1	poškozená
lebek	lebka	k1gFnPc2	lebka
<g/>
,	,	kIx,	,
stehenních	stehenní	k2eAgFnPc2d1	stehenní
kostí	kost	k1gFnPc2	kost
i	i	k8xC	i
celých	celý	k2eAgFnPc2d1	celá
koster	kostra	k1gFnPc2	kostra
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
znali	znát	k5eAaImAgMnP	znát
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
století	století	k1gNnSc2	století
koncept	koncept	k1gInSc4	koncept
nuly	nula	k1gFnSc2	nula
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
nulu	nula	k1gFnSc4	nula
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
dobrými	dobrý	k2eAgMnPc7d1	dobrý
matematiky	matematik	k1gMnPc7	matematik
a	a	k8xC	a
astronomy	astronom	k1gMnPc7	astronom
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
znalosti	znalost	k1gFnPc1	znalost
stály	stát	k5eAaImAgFnP	stát
i	i	k9	i
za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
komplexního	komplexní	k2eAgInSc2d1	komplexní
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
olmécké	olmécký	k2eAgFnSc2d1	olmécká
kultury	kultura	k1gFnSc2	kultura
způsobil	způsobit	k5eAaPmAgInS	způsobit
velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
mayských	mayský	k2eAgNnPc2d1	mayské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
narušil	narušit	k5eAaPmAgMnS	narušit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
l	l	kA	l
byla	být	k5eAaImAgFnS	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
La	la	k1gNnSc7	la
Venta	Vento	k1gNnSc2	Vento
a	a	k8xC	a
La	la	k1gNnSc7	la
Libertad	Libertad	k1gInSc1	Libertad
<g/>
.	.	kIx.	.
</s>
<s>
Opuštění	opuštění	k1gNnSc1	opuštění
La	la	k1gNnSc2	la
Libertadu	Libertad	k1gInSc2	Libertad
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zánik	zánik	k1gInSc4	zánik
olmécké	olmécký	k2eAgFnSc2d1	olmécká
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olmékové	Olméková	k1gFnSc2	Olméková
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
