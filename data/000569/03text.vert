<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Philosopher	Philosophra	k1gFnPc2	Philosophra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnSc2d1	Rowlingová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáctiletý	jedenáctiletý	k2eAgInSc4d1	jedenáctiletý
Harry	Harr	k1gInPc4	Harr
Potter	Potter	k1gInSc1	Potter
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
4	[number]	k4	4
v	v	k7c6	v
Zobí	Zobí	k2eAgFnSc6d1	Zobí
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Kvikálkově	Kvikálkův	k2eAgMnSc6d1	Kvikálkův
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
-	-	kIx~	-
pana	pan	k1gMnSc4	pan
a	a	k8xC	a
paní	paní	k1gFnSc4	paní
Dursleyových	Dursleyová	k1gFnPc2	Dursleyová
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
procházka	procházka	k1gFnSc1	procházka
růžovým	růžový	k2eAgInSc7d1	růžový
sadem	sad	k1gInSc7	sad
<g/>
...	...	k?	...
Právě	právě	k6eAd1	právě
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
synáček	synáček	k1gMnSc1	synáček
Dudley	Dudlea	k1gMnSc2	Dudlea
dělá	dělat	k5eAaImIp3nS	dělat
Harrymu	Harrym	k1gInSc2	Harrym
ze	z	k7c2	z
života	život	k1gInSc2	život
pořádné	pořádný	k2eAgNnSc4d1	pořádné
peklo	peklo	k1gNnSc4	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
neznepříjemňují	znepříjemňovat	k5eNaImIp3nP	znepříjemňovat
pouze	pouze	k6eAd1	pouze
předpojatí	předpojatý	k2eAgMnPc1d1	předpojatý
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Harryho	Harry	k1gMnSc4	Harry
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
projevující	projevující	k2eAgFnPc1d1	projevující
kouzelnické	kouzelnický	k2eAgFnPc1d1	kouzelnická
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
před	před	k7c4	před
Harryho	Harry	k1gMnSc4	Harry
jedenáctými	jedenáctý	k4xOgFnPc7	jedenáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
přijde	přijít	k5eAaPmIp3nS	přijít
podivný	podivný	k2eAgInSc1d1	podivný
dopis	dopis	k1gInSc1	dopis
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
obálce	obálka	k1gFnSc6	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
Vernon	Vernon	k1gMnSc1	Vernon
dopis	dopis	k1gInSc4	dopis
pozná	poznat	k5eAaPmIp3nS	poznat
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
nadcházející	nadcházející	k2eAgFnPc4d1	nadcházející
haldy	halda	k1gFnPc4	halda
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
domu	dům	k1gInSc2	dům
dostanou	dostat	k5eAaPmIp3nP	dostat
přes	přes	k7c4	přes
plata	plato	k1gNnSc2	plato
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
komín	komín	k1gInSc4	komín
<g/>
...	...	k?	...
Snaha	snaha	k1gFnSc1	snaha
jeho	jeho	k3xOp3gMnSc4	jeho
i	i	k9	i
tety	teta	k1gFnPc1	teta
Petunie	Petunie	k1gFnPc1	Petunie
zamezit	zamezit	k5eAaPmF	zamezit
Harrymu	Harrym	k1gInSc3	Harrym
styk	styk	k1gInSc4	styk
s	s	k7c7	s
kouzelnickým	kouzelnický	k2eAgInSc7d1	kouzelnický
světem	svět	k1gInSc7	svět
však	však	k9	však
přesto	přesto	k6eAd1	přesto
vyjde	vyjít	k5eAaPmIp3nS	vyjít
vniveč	vniveč	k6eAd1	vniveč
<g/>
,	,	kIx,	,
když	když	k8xS	když
vezme	vzít	k5eAaPmIp3nS	vzít
Vernon	Vernon	k1gNnSc4	Vernon
Dursley	Durslea	k1gFnSc2	Durslea
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
na	na	k7c4	na
opuštěný	opuštěný	k2eAgInSc4d1	opuštěný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
nikdo	nikdo	k3yNnSc1	nikdo
žádnou	žádný	k3yNgFnSc4	žádný
poštu	pošta	k1gFnSc4	pošta
posílat	posílat	k5eAaImF	posílat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
mýlí	mýlit	k5eAaImIp3nP	mýlit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
malý	malý	k1gMnSc1	malý
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
před	před	k7c7	před
jedenácti	jedenáct	k4xCc7	jedenáct
lety	léto	k1gNnPc7	léto
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
vtrhne	vtrhnout	k5eAaPmIp3nS	vtrhnout
do	do	k7c2	do
polorozpadlé	polorozpadlý	k2eAgFnSc2d1	polorozpadlá
chaty	chata	k1gFnSc2	chata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nocují	nocovat	k5eAaImIp3nP	nocovat
<g/>
,	,	kIx,	,
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
</s>
<s>
Představí	představit	k5eAaPmIp3nS	představit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Rubeus	Rubeus	k1gMnSc1	Rubeus
Hagrid	Hagrida	k1gFnPc2	Hagrida
klíčník	klíčník	k1gMnSc1	klíčník
a	a	k8xC	a
šafář	šafář	k1gMnSc1	šafář
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
a	a	k8xC	a
oznámí	oznámit	k5eAaPmIp3nS	oznámit
překvapenému	překvapený	k2eAgNnSc3d1	překvapené
Harrymu	Harrymum	k1gNnSc3	Harrymum
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snažily	snažit	k5eAaImAgFnP	snažit
sdělit	sdělit	k5eAaPmF	sdělit
dopisy	dopis	k1gInPc4	dopis
-	-	kIx~	-
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
září	září	k1gNnSc2	září
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
učit	učit	k5eAaImF	učit
kouzlům	kouzlo	k1gNnPc3	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
také	také	k9	také
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nezemřeli	zemřít	k5eNaPmAgMnP	zemřít
-	-	kIx~	-
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zabil	zabít	k5eAaPmAgMnS	zabít
zlý	zlý	k2eAgMnSc1d1	zlý
čaroděj	čaroděj	k1gMnSc1	čaroděj
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
,	,	kIx,	,
nejobávanější	obávaný	k2eAgMnSc1d3	nejobávanější
a	a	k8xC	a
nejkrutější	krutý	k2eAgMnSc1d3	nejkrutější
čaroděj	čaroděj	k1gMnSc1	čaroděj
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Hagridem	Hagrid	k1gInSc7	Hagrid
navštíví	navštívit	k5eAaPmIp3nS	navštívit
hostinec	hostinec	k1gInSc1	hostinec
Děravý	děravý	k2eAgInSc1d1	děravý
kotel	kotel	k1gInSc4	kotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkají	potkat	k5eAaPmIp3nP	potkat
několik	několik	k4yIc4	několik
Hagridových	Hagridový	k2eAgFnPc2d1	Hagridový
známých	známá	k1gFnPc2	známá
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
nový	nový	k2eAgMnSc1d1	nový
učitel	učitel	k1gMnSc1	učitel
Obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Quirrell	Quirrell	k1gMnSc1	Quirrell
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
také	také	k9	také
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
kouzelníků	kouzelník	k1gMnPc2	kouzelník
doopravdy	doopravdy	k9	doopravdy
slavný	slavný	k2eAgMnSc1d1	slavný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
Hagrid	Hagrid	k1gInSc4	Hagrid
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
díky	díky	k7c3	díky
něčemu	něco	k3yInSc3	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
stěží	stěží	k6eAd1	stěží
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Tom	Tom	k1gMnSc1	Tom
Rolvoj	Rolvoj	k1gInSc4	Rolvoj
Raddle	Raddle	k1gFnSc2	Raddle
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
při	při	k7c6	při
vraždě	vražda	k1gFnSc6	vražda
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
jeho	jeho	k3xOp3gNnSc7	jeho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázal	dokázat	k5eNaPmAgInS	dokázat
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zabít	zabít	k5eAaPmF	zabít
toho	ten	k3xDgMnSc4	ten
malého	malý	k2eAgMnSc4d1	malý
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
Potter	Pottra	k1gFnPc2	Pottra
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
co	co	k9	co
přežil	přežít	k5eAaPmAgMnS	přežít
kletbu	kletba	k1gFnSc4	kletba
Avada	Avada	k1gMnSc1	Avada
Kedavra	Kedavra	k1gMnSc1	Kedavra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kletba	kletba	k1gFnSc1	kletba
smrtící	smrtící	k2eAgFnSc1d1	smrtící
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Příčnou	příčný	k2eAgFnSc4d1	příčná
ulici	ulice	k1gFnSc4	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
Harry	Harra	k1gMnSc2	Harra
vybere	vybrat	k5eAaPmIp3nS	vybrat
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
Gringottově	Gringottův	k2eAgFnSc6d1	Gringottova
bance	banka	k1gFnSc6	banka
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
zděděné	zděděný	k2eAgInPc4d1	zděděný
po	po	k7c6	po
rodičích	rodič	k1gMnPc6	rodič
a	a	k8xC	a
Hagrid	Hagrid	k1gInSc4	Hagrid
zde	zde	k6eAd1	zde
vyzvedne	vyzvednout	k5eAaPmIp3nS	vyzvednout
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
nadmíru	nadmíru	k6eAd1	nadmíru
podivný	podivný	k2eAgInSc4d1	podivný
balíček	balíček	k1gInSc4	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
nakoupí	nakoupit	k5eAaPmIp3nP	nakoupit
školní	školní	k2eAgFnPc4d1	školní
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
jako	jako	k9	jako
dárek	dárek	k1gInSc4	dárek
k	k	k7c3	k
narozeninám	narozeniny	k1gFnPc3	narozeniny
dostane	dostat	k5eAaPmIp3nS	dostat
Harry	Harra	k1gFnPc4	Harra
od	od	k7c2	od
Hagrida	Hagrid	k1gMnSc2	Hagrid
sovu	sova	k1gFnSc4	sova
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pojmenuje	pojmenovat	k5eAaPmIp3nS	pojmenovat
Hedvika	Hedvika	k1gFnSc1	Hedvika
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgNnSc2	první
září	září	k1gNnSc2	září
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
King	Kinga	k1gFnPc2	Kinga
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
projde	projít	k5eAaPmIp3nS	projít
přepážkou	přepážka	k1gFnSc7	přepážka
mezi	mezi	k7c7	mezi
nástupišti	nástupiště	k1gNnSc6	nástupiště
devět	devět	k4xCc1	devět
a	a	k8xC	a
deset	deset	k4xCc1	deset
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
devět	devět	k4xCc1	devět
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nasedne	nasednout	k5eAaPmIp3nS	nasednout
na	na	k7c4	na
Bradavický	Bradavický	k2eAgInSc4d1	Bradavický
expres	expres	k1gInSc4	expres
<g/>
.	.	kIx.	.
</s>
<s>
Seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
se	s	k7c7	s
zrzavým	zrzavý	k2eAgMnSc7d1	zrzavý
chlapcem	chlapec	k1gMnSc7	chlapec
Ronem	Ron	k1gMnSc7	Ron
a	a	k8xC	a
s	s	k7c7	s
nadanou	nadaný	k2eAgFnSc7d1	nadaná
čarodějkou	čarodějka	k1gFnSc7	čarodějka
z	z	k7c2	z
mudlovské	mudlovský	k2eAgFnSc2d1	mudlovská
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
nebyli	být	k5eNaImAgMnP	být
kouzelníky	kouzelník	k1gMnPc4	kouzelník
<g/>
)	)	kIx)	)
Hermionou	Hermiona	k1gFnSc7	Hermiona
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
nekamarádí	kamarádit	k5eNaImIp3nS	kamarádit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
namyšlená	namyšlený	k2eAgFnSc1d1	namyšlená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Ronem	ron	k1gInSc7	ron
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkají	setkat	k5eAaPmIp3nP	setkat
i	i	k9	i
s	s	k7c7	s
Nevillem	Nevill	k1gMnSc7	Nevill
Longbottomem	Longbottom	k1gMnSc7	Longbottom
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
si	se	k3xPyFc3	se
stihnou	stihnout	k5eAaPmIp3nP	stihnout
udělat	udělat	k5eAaPmF	udělat
i	i	k9	i
nějaké	nějaký	k3yIgMnPc4	nějaký
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
jsou	být	k5eAaImIp3nP	být
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nechuti	nechuť	k1gFnSc3	nechuť
i	i	k9	i
Hermiona	Hermiona	k1gFnSc1	Hermiona
<g/>
,	,	kIx,	,
Moudrým	moudrý	k2eAgInSc7d1	moudrý
kloboukem	klobouk	k1gInSc7	klobouk
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc2d1	vyznačující
se	se	k3xPyFc4	se
chrabrostí	chrabrost	k1gFnSc7	chrabrost
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgMnPc2	který
jsou	být	k5eAaImIp3nP	být
studenti	student	k1gMnPc1	student
Bradavické	Bradavický	k2eAgFnSc2d1	Bradavická
školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
zařazováni	zařazovat	k5eAaImNgMnP	zařazovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
zahájení	zahájení	k1gNnSc2	zahájení
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
Harry	Harra	k1gMnSc2	Harra
všimne	všimnout	k5eAaPmIp3nS	všimnout
profesora	profesor	k1gMnSc4	profesor
Snapea	Snapeus	k1gMnSc4	Snapeus
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnSc4	učitel
s	s	k7c7	s
hákovitým	hákovitý	k2eAgInSc7d1	hákovitý
nosem	nos	k1gInSc7	nos
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
dovídá	dovídat	k5eAaImIp3nS	dovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kolejním	kolejní	k2eAgMnSc7d1	kolejní
ředitelem	ředitel	k1gMnSc7	ředitel
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
stane	stanout	k5eAaPmIp3nS	stanout
jeho	on	k3xPp3gMnSc4	on
nejméně	málo	k6eAd3	málo
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
vyučujícím	vyučující	k2eAgMnSc7d1	vyučující
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
Hermionu	Hermion	k1gInSc2	Hermion
Grangerovou	Grangerová	k1gFnSc4	Grangerová
panují	panovat	k5eAaImIp3nP	panovat
neshody	neshoda	k1gFnPc1	neshoda
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
napadne	napadnout	k5eAaPmIp3nS	napadnout
horský	horský	k2eAgMnSc1d1	horský
troll	troll	k1gMnSc1	troll
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
ji	on	k3xPp3gFnSc4	on
zachrání	zachránit	k5eAaPmIp3nP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
nerozlučnou	rozlučný	k2eNgFnSc7d1	nerozlučná
trojicí	trojice	k1gFnSc7	trojice
kamarádů	kamarád	k1gMnPc2	kamarád
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
také	také	k9	také
ukáže	ukázat	k5eAaPmIp3nS	ukázat
být	být	k5eAaImF	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
kouzelnický	kouzelnický	k2eAgInSc1d1	kouzelnický
sport	sport	k1gInSc1	sport
na	na	k7c6	na
košťatech	koště	k1gNnPc6	koště
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
míči	míč	k1gInPc7	míč
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc4	tři
střelce	střelec	k1gMnPc4	střelec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
s	s	k7c7	s
camrálem	camrál	k1gInSc7	camrál
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dva	dva	k4xCgMnPc4	dva
odražeče	odražeč	k1gMnPc4	odražeč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
dva	dva	k4xCgInPc1	dva
potlouky	potlouk	k1gInPc1	potlouk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
posílají	posílat	k5eAaImIp3nP	posílat
proti	proti	k7c3	proti
soupeřům	soupeř	k1gMnPc3	soupeř
<g/>
,	,	kIx,	,
brankáře	brankář	k1gMnSc4	brankář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hlídá	hlídat	k5eAaImIp3nS	hlídat
tři	tři	k4xCgInPc4	tři
obruče	obruč	k1gInPc4	obruč
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
chytače	chytač	k1gMnSc4	chytač
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nebelvírském	belvírský	k2eNgNnSc6d1	nebelvírské
mužstvu	mužstvo	k1gNnSc6	mužstvo
Harry	Harra	k1gFnSc2	Harra
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
chytá	chytat	k5eAaImIp3nS	chytat
zlatonku	zlatonka	k1gFnSc4	zlatonka
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
přinese	přinést	k5eAaPmIp3nS	přinést
svému	svůj	k3xOyFgInSc3	svůj
týmu	tým	k1gInSc3	tým
sto	sto	k4xCgNnSc1	sto
padesát	padesát	k4xCc1	padesát
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ukončí	ukončit	k5eAaPmIp3nS	ukončit
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastního	vlastní	k2eAgNnSc2d1	vlastní
pátrání	pátrání	k1gNnSc2	pátrání
Harry	Harra	k1gFnSc2	Harra
usoudí	usoudit	k5eAaPmIp3nS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
snaží	snažit	k5eAaImIp3nS	snažit
ukrást	ukrást	k5eAaPmF	ukrást
kámen	kámen	k1gInSc4	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
,	,	kIx,	,
zázračný	zázračný	k2eAgInSc4d1	zázračný
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgNnSc4d1	vyrábějící
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
elixír	elixír	k1gInSc4	elixír
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gInSc4	on
Hagrid	Hagrid	k1gInSc4	Hagrid
v	v	k7c6	v
onom	onen	k3xDgNnSc6	onen
nadmíru	nadmíru	k6eAd1	nadmíru
podezřelém	podezřelý	k2eAgInSc6d1	podezřelý
balíčku	balíček	k1gInSc6	balíček
přinesl	přinést	k5eAaPmAgInS	přinést
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
podezřelým	podezřelý	k2eAgMnSc7d1	podezřelý
je	on	k3xPp3gMnPc4	on
profesor	profesor	k1gMnSc1	profesor
Snape	Snap	k1gMnSc5	Snap
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Rona	Ron	k1gMnSc2	Ron
a	a	k8xC	a
Hermiony	Hermion	k1gInPc7	Hermion
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kámen	kámen	k1gInSc1	kámen
uložen	uložit	k5eAaPmNgInS	uložit
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
něco	něco	k3yInSc4	něco
šokujícího	šokující	k2eAgMnSc2d1	šokující
<g/>
.	.	kIx.	.
</s>
<s>
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
nechce	chtít	k5eNaImIp3nS	chtít
ukrást	ukrást	k5eAaPmF	ukrást
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
nenápadný	nápadný	k2eNgMnSc1d1	nenápadný
koktavý	koktavý	k2eAgMnSc1d1	koktavý
profesor	profesor	k1gMnSc1	profesor
Quirrell	Quirrell	k1gMnSc1	Quirrell
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
jej	on	k3xPp3gMnSc4	on
předat	předat	k5eAaPmF	předat
svému	svůj	k3xOyFgMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
vrah	vrah	k1gMnSc1	vrah
Harryho	Harry	k1gMnSc2	Harry
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnPc4d1	snažící
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc2	Harrym
se	se	k3xPyFc4	se
kámen	kámen	k1gInSc1	kámen
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
před	před	k7c7	před
Quirrellem	Quirrello	k1gNnSc7	Quirrello
zachránit	zachránit	k5eAaPmF	zachránit
a	a	k8xC	a
tak	tak	k9	tak
návrat	návrat	k1gInSc4	návrat
Voldemorta	Voldemort	k1gMnSc4	Voldemort
odložit	odložit	k5eAaPmF	odložit
<g/>
.	.	kIx.	.
</s>
