<s>
Stůl	stůl	k1gInSc1	stůl
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
nábytku	nábytek	k1gInSc2	nábytek
tvořený	tvořený	k2eAgInSc4d1	tvořený
deskou	deska	k1gFnSc7	deska
a	a	k8xC	a
nohou	noha	k1gFnSc7	noha
či	či	k8xC	či
nohami	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
desku	deska	k1gFnSc4	deska
podloženou	podložený	k2eAgFnSc4d1	podložená
lubem	lub	k1gInSc7	lub
či	či	k8xC	či
doplněnou	doplněný	k2eAgFnSc7d1	doplněná
zásuvkou	zásuvka	k1gFnSc7	zásuvka
nebo	nebo	k8xC	nebo
zásuvkami	zásuvka	k1gFnPc7	zásuvka
<g/>
.	.	kIx.	.
</s>
