<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
povstání	povstání	k1gNnSc2	povstání
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
protestantských	protestantský	k2eAgFnPc2d1	protestantská
provincií	provincie	k1gFnPc2	provincie
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
nadvládě	nadvláda	k1gFnSc3	nadvláda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
prince	princ	k1gMnSc2	princ
Viléma	Vilém	k1gMnSc2	Vilém
Oranžského	oranžský	k2eAgMnSc2d1	oranžský
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
vlajce	vlajka	k1gFnSc6	vlajka
–	–	k?	–
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
–	–	k?	–
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
mluví	mluvit	k5eAaImIp3nS	mluvit
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
sedmi	sedm	k4xCc2	sedm
severních	severní	k2eAgFnPc2d1	severní
provincií	provincie	k1gFnPc2	provincie
od	od	k7c2	od
španělského	španělský	k2eAgNnSc2d1	španělské
obklíčení	obklíčení	k1gNnSc2	obklíčení
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
Republiky	republika	k1gFnSc2	republika
spojeného	spojený	k2eAgNnSc2d1	spojené
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
(	(	kIx(	(
<g/>
1581	[number]	k4	1581
<g/>
)	)	kIx)	)
se	s	k7c7	s
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
princova	princův	k2eAgFnSc1d1	princova
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
"	"	kIx"	"
v	v	k7c6	v
livrejových	livrejový	k2eAgFnPc6d1	livrejový
barvách	barva	k1gFnPc6	barva
oranžského	oranžský	k2eAgInSc2d1	oranžský
domu	dům	k1gInSc2	dům
používala	používat	k5eAaImAgFnS	používat
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
společně	společně	k6eAd1	společně
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
Svobodného	svobodný	k2eAgNnSc2d1	svobodné
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
byla	být	k5eAaImAgFnS	být
zavedená	zavedený	k2eAgFnSc1d1	zavedená
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Španělsko	Španělsko	k1gNnSc1	Španělsko
uznalo	uznat	k5eAaPmAgNnS	uznat
nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
viditelnost	viditelnost	k1gFnSc4	viditelnost
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
však	však	k9	však
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
oranžový	oranžový	k2eAgInSc1d1	oranžový
pruh	pruh	k1gInSc1	pruh
nahrazený	nahrazený	k2eAgMnSc1d1	nahrazený
červeným	červený	k2eAgInSc7d1	červený
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
bylo	být	k5eAaImAgNnS	být
používání	používání	k1gNnSc1	používání
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
výslovně	výslovně	k6eAd1	výslovně
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vlajka	vlajka	k1gFnSc1	vlajka
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
republikánská	republikánský	k2eAgFnSc1d1	republikánská
trikolóra	trikolóra	k1gFnSc1	trikolóra
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
svobody	svoboda	k1gFnSc2	svoboda
byla	být	k5eAaImAgFnS	být
vzorem	vzor	k1gInSc7	vzor
všech	všecek	k3xTgFnPc2	všecek
dalších	další	k2eAgFnPc2d1	další
trikolór	trikolóra	k1gFnPc2	trikolóra
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
francouzské	francouzský	k2eAgNnSc1d1	francouzské
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
i	i	k8xC	i
ruská	ruský	k2eAgFnSc1d1	ruská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Petr	Petr	k1gMnSc1	Petr
Veliký	veliký	k2eAgMnSc1d1	veliký
vrátil	vrátit	k5eAaPmAgInS	vrátit
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
tady	tady	k6eAd1	tady
nabytých	nabytý	k2eAgFnPc2d1	nabytá
zkušeností	zkušenost	k1gFnPc2	zkušenost
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
vlastního	vlastní	k2eAgNnSc2d1	vlastní
námořního	námořní	k2eAgNnSc2d1	námořní
loďstva	loďstvo	k1gNnSc2	loďstvo
a	a	k8xC	a
podle	podle	k7c2	podle
nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
vzoru	vzor	k1gInSc2	vzor
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
i	i	k9	i
ruskou	ruský	k2eAgFnSc4d1	ruská
obchodní	obchodní	k2eAgFnSc4d1	obchodní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
pouze	pouze	k6eAd1	pouze
pořadí	pořadí	k1gNnSc4	pořadí
barev	barva	k1gFnPc2	barva
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
na	na	k7c4	na
trikolóru	trikolóra	k1gFnSc4	trikolóra
bílo-modro-červenou	bíloodro-červený	k2eAgFnSc4d1	bílo-modro-červená
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
ruskou	ruský	k2eAgFnSc7d1	ruská
státní	státní	k2eAgFnSc7d1	státní
a	a	k8xC	a
národní	národní	k2eAgFnSc7d1	národní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
panslovanské	panslovanský	k2eAgFnSc6d1	panslovanská
éře	éra	k1gFnSc6	éra
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
tvořily	tvořit	k5eAaImAgFnP	tvořit
doposud	doposud	k6eAd1	doposud
nesvobodné	svobodný	k2eNgInPc1d1	nesvobodný
slovanské	slovanský	k2eAgInPc1d1	slovanský
národy	národ	k1gInPc1	národ
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
s	s	k7c7	s
úpravami	úprava	k1gFnPc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
Srbové	Srbová	k1gFnSc2	Srbová
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
následovali	následovat	k5eAaImAgMnP	následovat
Černohorci	Černohorec	k1gMnPc1	Černohorec
<g/>
,	,	kIx,	,
Chorvati	Chorvat	k1gMnPc1	Chorvat
<g/>
,	,	kIx,	,
Slovinci	Slovinec	k1gMnPc1	Slovinec
<g/>
,	,	kIx,	,
Slováci	Slovák	k1gMnPc1	Slovák
a	a	k8xC	a
Bulhaři	Bulhar	k1gMnPc1	Bulhar
(	(	kIx(	(
<g/>
tady	tady	k6eAd1	tady
byl	být	k5eAaImAgInS	být
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
nahrazený	nahrazený	k2eAgMnSc1d1	nahrazený
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vlajka	vlajka	k1gFnSc1	vlajka
nepřímo	přímo	k6eNd1	přímo
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
vlajky	vlajka	k1gFnPc4	vlajka
některých	některý	k3yIgInPc2	některý
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
českou	český	k2eAgFnSc4d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
vlajku	vlajka	k1gFnSc4	vlajka
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
správní	správní	k2eAgInSc4d1	správní
obvod	obvod	k1gInSc4	obvod
Karibské	karibský	k2eAgNnSc1d1	Karibské
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
ostrovy	ostrov	k1gInPc1	ostrov
Bonaire	Bonair	k1gInSc5	Bonair
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Eustach	Eustach	k1gMnSc1	Eustach
a	a	k8xC	a
Saba	Saba	k1gMnSc1	Saba
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
Nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
Antil	Antily	k1gFnPc2	Antily
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
ovlivněné	ovlivněný	k2eAgFnPc4d1	ovlivněná
nizozemskou	nizozemský	k2eAgFnSc7d1	nizozemská
vlajkou	vlajka	k1gFnSc7	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
==	==	k?	==
</s>
</p>
<p>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
je	být	k5eAaImIp3nS	být
členěno	členit	k5eAaImNgNnS	členit
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
užívají	užívat	k5eAaImIp3nP	užívat
své	svůj	k3xOyFgFnPc4	svůj
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Holandska	Holandsko	k1gNnSc2	Holandsko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
</s>
</p>
<p>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
