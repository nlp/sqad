<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
jsou	být	k5eAaImIp3nP	být
netopýři	netopýr	k1gMnPc1	netopýr
(	(	kIx(	(
<g/>
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
podřád	podřád	k1gInSc1	podřád
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
kaloni	kaloň	k1gMnPc1	kaloň
<g/>
)	)	kIx)	)
jedinými	jediný	k2eAgMnPc7d1	jediný
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
létat	létat	k5eAaImF	létat
–	–	k?	–
létající	létající	k2eAgFnPc1d1	létající
veverky	veverka	k1gFnPc1	veverka
<g/>
,	,	kIx,	,
vakoveverky	vakoveverka	k1gFnPc1	vakoveverka
a	a	k8xC	a
plachtící	plachtící	k2eAgMnPc1d1	plachtící
kuskusovití	kuskusovitý	k2eAgMnPc1d1	kuskusovitý
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
klouzat	klouzat	k5eAaImF	klouzat
na	na	k7c4	na
omezenou	omezený	k2eAgFnSc4d1	omezená
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
