<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
brněnských	brněnský	k2eAgFnPc6d1	brněnská
rockových	rockový	k2eAgFnPc6d1	rocková
skupinách	skupina	k1gFnPc6	skupina
Speakers	Speakers	k1gInSc4	Speakers
<g/>
,	,	kIx,	,
Progres	progres	k1gInSc4	progres
2	[number]	k4	2
a	a	k8xC	a
Futurum	futurum	k1gNnSc4	futurum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
začínal	začínat	k5eAaImAgMnS	začínat
svoji	svůj	k3xOyFgFnSc4	svůj
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
ve	v	k7c6	v
vokální	vokální	k2eAgFnSc6d1	vokální
skupině	skupina	k1gFnSc6	skupina
Singings	Singingsa	k1gFnPc2	Singingsa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
lídrem	lídr	k1gMnSc7	lídr
kapely	kapela	k1gFnSc2	kapela
Speakers	Speakersa	k1gFnPc2	Speakersa
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
vydal	vydat	k5eAaPmAgMnS	vydat
dva	dva	k4xCgInPc4	dva
singly	singl	k1gInPc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
ale	ale	k8xC	ale
ze	z	k7c2	z
Speakers	Speakersa	k1gFnPc2	Speakersa
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgInSc1d2	pozdější
název	název	k1gInSc1	název
Junior	junior	k1gMnSc1	junior
<g/>
)	)	kIx)	)
odešel	odejít	k5eAaPmAgInS	odejít
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
Skupiny	skupina	k1gFnSc2	skupina
Jana	Jan	k1gMnSc2	Jan
Sochora	Sochor	k1gMnSc2	Sochor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
folkového	folkový	k2eAgMnSc4d1	folkový
zpěváka	zpěvák	k1gMnSc4	zpěvák
Boba	Bob	k1gMnSc4	Bob
Frídla	Frídla	k1gMnSc4	Frídla
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Pavlem	Pavel	k1gMnSc7	Pavel
Váněm	Váněm	k1gInSc4	Váněm
<g/>
,	,	kIx,	,
klávesistou	klávesista	k1gMnSc7	klávesista
Janem	Jan	k1gMnSc7	Jan
Sochorem	Sochor	k1gMnSc7	Sochor
a	a	k8xC	a
bubeníkem	bubeník	k1gMnSc7	bubeník
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kluka	kluk	k1gMnSc2	kluk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
Pelcem	Pelc	k1gMnSc7	Pelc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
natočili	natočit	k5eAaBmAgMnP	natočit
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
kapely	kapela	k1gFnSc2	kapela
Barnodaj	Barnodaj	k1gInSc1	Barnodaj
album	album	k1gNnSc1	album
Mauglí	Maugle	k1gFnPc2	Maugle
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
facto	facto	k1gNnSc1	facto
tak	tak	k8xC	tak
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc2	The
Progress	Progressa	k1gFnPc2	Progressa
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
změnou	změna	k1gFnSc7	změna
byl	být	k5eAaImAgMnS	být
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
na	na	k7c6	na
postu	post	k1gInSc6	post
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Emanuela	Emanuel	k1gMnSc2	Emanuel
Sideridise	Sideridise	k1gFnSc2	Sideridise
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
<g/>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nahrávání	nahrávání	k1gNnSc2	nahrávání
Mauglího	Mauglí	k2eAgNnSc2d1	Mauglí
ale	ale	k8xC	ale
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
skupina	skupina	k1gFnSc1	skupina
Progres	progres	k1gInSc1	progres
2	[number]	k4	2
(	(	kIx(	(
<g/>
zůstali	zůstat	k5eAaPmAgMnP	zůstat
Váně	Váň	k1gMnSc4	Váň
<g/>
,	,	kIx,	,
Kluka	kluk	k1gMnSc4	kluk
a	a	k8xC	a
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
novými	nový	k2eAgMnPc7d1	nový
hudebníky	hudebník	k1gMnPc7	hudebník
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
klávesista	klávesista	k1gMnSc1	klávesista
Karel	Karel	k1gMnSc1	Karel
Horký	Horký	k1gMnSc1	Horký
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Miloš	Miloš	k1gMnSc1	Miloš
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chystala	chystat	k5eAaImAgFnS	chystat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
audiovizuální	audiovizuální	k2eAgNnSc4d1	audiovizuální
představení	představení	k1gNnSc4	představení
–	–	k?	–
rockovou	rockový	k2eAgFnSc4d1	rocková
operu	opera	k1gFnSc4	opera
Dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
vesmírem	vesmír	k1gInSc7	vesmír
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Klukou	Kluka	k1gMnSc7	Kluka
stal	stát	k5eAaPmAgInS	stát
jádrem	jádro	k1gNnSc7	jádro
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
měnila	měnit	k5eAaImAgFnS	měnit
obsazení	obsazení	k1gNnSc4	obsazení
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
alba	alba	k1gFnSc1	alba
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
koncertní	koncertní	k2eAgInPc1d1	koncertní
tematické	tematický	k2eAgInPc1d1	tematický
programy	program	k1gInPc1	program
(	(	kIx(	(
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnSc7	džungle
<g/>
,	,	kIx,	,
Mozek	mozek	k1gInSc1	mozek
a	a	k8xC	a
Změna	změna	k1gFnSc1	změna
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
desky	deska	k1gFnSc2	deska
Změna	změna	k1gFnSc1	změna
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ale	ale	k9	ale
skupina	skupina	k1gFnSc1	skupina
Progres	progres	k1gInSc1	progres
2	[number]	k4	2
transformovala	transformovat	k5eAaBmAgFnS	transformovat
do	do	k7c2	do
formace	formace	k1gFnSc2	formace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Progres-Pokrok	Progres-Pokrok	k1gInSc1	Progres-Pokrok
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
už	už	k9	už
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
nefiguroval	figurovat	k5eNaImAgMnS	figurovat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
jiné	jiný	k2eAgFnSc2d1	jiná
brněnské	brněnský	k2eAgFnSc2d1	brněnská
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Futurum	futurum	k1gNnSc4	futurum
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
založili	založit	k5eAaPmAgMnP	založit
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
Progres	progres	k1gInSc4	progres
2	[number]	k4	2
Miloš	Miloš	k1gMnSc1	Miloš
Morávek	Morávek	k1gMnSc1	Morávek
a	a	k8xC	a
Roman	Roman	k1gMnSc1	Roman
Dragoun	dragoun	k1gMnSc1	dragoun
<g/>
.	.	kIx.	.
</s>
<s>
Pelc	Pelc	k1gMnSc1	Pelc
zde	zde	k6eAd1	zde
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Lubomíra	Lubomír	k1gMnSc4	Lubomír
Eremiáše	Eremiáš	k1gMnSc4	Eremiáš
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
chystala	chystat	k5eAaImAgFnS	chystat
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
ale	ale	k9	ale
odešli	odejít	k5eAaPmAgMnP	odejít
lídr	lídr	k1gMnSc1	lídr
kapely	kapela	k1gFnSc2	kapela
Dragoun	dragoun	k1gMnSc1	dragoun
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Janem	Jan	k1gMnSc7	Jan
Seidlem	Seidel	k1gMnSc7	Seidel
a	a	k8xC	a
Futurum	futurum	k1gNnSc1	futurum
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
se	se	k3xPyFc4	se
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
podnikání	podnikání	k1gNnSc2	podnikání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozvoji	rozvoj	k1gInSc6	rozvoj
internetu	internet	k1gInSc2	internet
založil	založit	k5eAaPmAgInS	založit
také	také	k9	také
vlastní	vlastní	k2eAgInSc1d1	vlastní
server	server	k1gInSc1	server
s	s	k7c7	s
databází	databáze	k1gFnSc7	databáze
restaurací	restaurace	k1gFnPc2	restaurace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
Progres	progres	k1gInSc1	progres
2	[number]	k4	2
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
,	,	kIx,	,
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
doposud	doposud	k6eAd1	doposud
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nadále	nadále	k6eAd1	nadále
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
postu	post	k1gInSc6	post
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
vydali	vydat	k5eAaPmAgMnP	vydat
Progres	progres	k1gInSc4	progres
2	[number]	k4	2
album	album	k1gNnSc1	album
Tulák	tulák	k1gMnSc1	tulák
po	po	k7c6	po
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
znovuobnovené	znovuobnovený	k2eAgFnSc6d1	znovuobnovená
skupině	skupina	k1gFnSc6	skupina
Bronz	bronz	k1gInSc1	bronz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
hostoval	hostovat	k5eAaImAgMnS	hostovat
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
na	na	k7c6	na
dvojkoncertě	dvojkoncert	k1gInSc6	dvojkoncert
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnPc2	založení
skupiny	skupina	k1gFnSc2	skupina
Futurum	futurum	k1gNnSc1	futurum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Se	s	k7c7	s
Speakers	Speakersa	k1gFnPc2	Speakersa
===	===	k?	===
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
"	"	kIx"	"
<g/>
Mlha	mlha	k1gFnSc1	mlha
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
"	"	kIx"	"
<g/>
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
Progres	progres	k1gInSc1	progres
2	[number]	k4	2
===	===	k?	===
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
"	"	kIx"	"
<g/>
Prám	prám	k1gInSc1	prám
z	z	k7c2	z
trámů	trám	k1gInPc2	trám
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
Mauglí	Mauglí	k1gNnSc1	Mauglí
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
"	"	kIx"	"
<g/>
Roentgen	roentgen	k1gInSc1	roentgen
19	[number]	k4	19
<g/>
'	'	kIx"	'
<g/>
30	[number]	k4	30
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
"	"	kIx"	"
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
jablku	jablko	k1gNnSc6	jablko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
Dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
vesmírem	vesmír	k1gInSc7	vesmír
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
Dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
vesmírem	vesmír	k1gInSc7	vesmír
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
–	–	k?	–
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
stroj	stroj	k1gInSc1	stroj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
"	"	kIx"	"
<g/>
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
odvrácené	odvrácený	k2eAgFnSc3d1	odvrácená
straně	strana	k1gFnSc3	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
Třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
The	The	k1gMnSc1	The
Third	Third	k1gMnSc1	Third
Book	Book	k1gMnSc1	Book
of	of	k?	of
Jungle	Jungle	k1gInSc1	Jungle
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
"	"	kIx"	"
<g/>
Nech	nechat	k5eAaPmRp2nS	nechat
je	on	k3xPp3gNnPc4	on
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
"	"	kIx"	"
<g/>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
"	"	kIx"	"
<g/>
Máš	mít	k5eAaImIp2nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Změna	změna	k1gFnSc1	změna
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
album	album	k1gNnSc4	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
vesmírem	vesmír	k1gInSc7	vesmír
/	/	kIx~	/
<g/>
live	live	k6eAd1	live
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
nahrané	nahraný	k2eAgNnSc1d1	nahrané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Progres	progres	k1gInSc1	progres
Story	story	k1gFnSc1	story
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Progres	progres	k1gInSc1	progres
Story	story	k1gFnSc1	story
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
živé	živá	k1gFnSc6	živá
DVD	DVD	kA	DVD
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Live	Live	k1gNnSc1	Live
(	(	kIx(	(
<g/>
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
a	a	k8xC	a
DVD	DVD	kA	DVD
nahrané	nahraný	k2eAgInPc1d1	nahraný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
–	–	k?	–
Tulák	tulák	k1gMnSc1	tulák
po	po	k7c6	po
hvězdách	hvězda	k1gFnPc6	hvězda
(	(	kIx(	(
<g/>
album	album	k1gNnSc4	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
Futurem	futurum	k1gNnSc7	futurum
===	===	k?	===
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc1	narozeniny
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
(	(	kIx(	(
<g/>
živé	živá	k1gFnPc4	živá
DVD	DVD	kA	DVD
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pavel	Pavel	k1gMnSc1	Pavel
Pelc	Pelc	k1gMnSc1	Pelc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
