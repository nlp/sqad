<s>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
(	(	kIx(	(
<g/>
Latinsky	latinsky	k6eAd1	latinsky
suicidium	suicidium	k1gNnSc1	suicidium
<g/>
,	,	kIx,	,
výraz	výraz	k1gInSc1	výraz
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
sui	sui	k?	sui
caedere	caedrat	k5eAaPmIp3nS	caedrat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zabít	zabít	k5eAaPmF	zabít
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čin	čin	k1gInSc1	čin
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
člověk	člověk	k1gMnSc1	člověk
úmyslně	úmyslně	k6eAd1	úmyslně
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Z	z	k7c2	z
psychologického	psychologický	k2eAgNnSc2d1	psychologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
agresí	agrese	k1gFnPc2	agrese
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
však	však	k9	však
představit	představit	k5eAaPmF	představit
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
psychologické	psychologický	k2eAgInPc4d1	psychologický
mechanismy	mechanismus	k1gInPc4	mechanismus
včetně	včetně	k7c2	včetně
projevu	projev	k1gInSc2	projev
zoufalství	zoufalství	k1gNnSc2	zoufalství
<g/>
,	,	kIx,	,
existenciální	existenciální	k2eAgFnSc2d1	existenciální
tísně	tíseň	k1gFnSc2	tíseň
a	a	k8xC	a
neschopnosti	neschopnost	k1gFnSc2	neschopnost
dál	daleko	k6eAd2	daleko
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
bipolární	bipolární	k2eAgFnSc1d1	bipolární
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
schizofrenie	schizofrenie	k1gFnSc1	schizofrenie
<g/>
,	,	kIx,	,
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
<g/>
,	,	kIx,	,
narkomanie	narkomanie	k1gFnSc1	narkomanie
aj.	aj.	kA	aj.
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
nezřídka	nezřídka	k6eAd1	nezřídka
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
stresové	stresový	k2eAgInPc1d1	stresový
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
finanční	finanční	k2eAgFnPc4d1	finanční
potíže	potíž	k1gFnPc4	potíž
nebo	nebo	k8xC	nebo
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
mezilidskými	mezilidský	k2eAgInPc7d1	mezilidský
vztahy	vztah	k1gInPc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
sebevraždám	sebevražda	k1gFnPc3	sebevražda
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
omezení	omezení	k1gNnSc6	omezení
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
střelným	střelný	k2eAgFnPc3d1	střelná
zbraním	zbraň	k1gFnPc3	zbraň
<g/>
,	,	kIx,	,
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
duševních	duševní	k2eAgFnPc2d1	duševní
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
v	v	k7c6	v
psychoterapii	psychoterapie	k1gFnSc6	psychoterapie
<g/>
,	,	kIx,	,
v	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
při	při	k7c6	při
zneužívání	zneužívání	k1gNnSc6	zneužívání
návykových	návykový	k2eAgFnPc2d1	návyková
látek	látka	k1gFnPc2	látka
i	i	k8xC	i
zlepšování	zlepšování	k1gNnSc4	zlepšování
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgInPc1d3	nejběžnější
způsoby	způsob	k1gInPc1	způsob
sebevraždy	sebevražda	k1gFnSc2	sebevražda
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
liší	lišit	k5eAaImIp3nS	lišit
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
dostupností	dostupnost	k1gFnSc7	dostupnost
prostředků	prostředek	k1gInPc2	prostředek
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
provedení	provedení	k1gNnSc3	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
časté	častý	k2eAgFnPc4d1	častá
formy	forma	k1gFnPc4	forma
patří	patřit	k5eAaImIp3nP	patřit
oběšení	oběšený	k2eAgMnPc1d1	oběšený
<g/>
,	,	kIx,	,
otrava	otrava	k1gMnSc1	otrava
pesticidy	pesticid	k1gInPc4	pesticid
a	a	k8xC	a
použití	použití	k1gNnPc4	použití
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
následky	následek	k1gInPc4	následek
sebevraždy	sebevražda	k1gFnSc2	sebevražda
každoročně	každoročně	k6eAd1	každoročně
zemře	zemřít	k5eAaPmIp3nS	zemřít
přibližně	přibližně	k6eAd1	přibližně
800	[number]	k4	800
000	[number]	k4	000
až	až	k6eAd1	až
jeden	jeden	k4xCgInSc1	jeden
milión	milión	k4xCgInSc1	milión
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
celosvětově	celosvětově	k6eAd1	celosvětově
činí	činit	k5eAaImIp3nS	činit
sebevraždu	sebevražda	k1gFnSc4	sebevražda
desátou	desátá	k1gFnSc4	desátá
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
sebevrahy	sebevrah	k1gMnPc7	sebevrah
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
dojde	dojít	k5eAaPmIp3nS	dojít
přibližně	přibližně	k6eAd1	přibližně
k	k	k7c3	k
10	[number]	k4	10
až	až	k8xS	až
20	[number]	k4	20
miliónům	milión	k4xCgInPc3	milión
neúspěšných	úspěšný	k2eNgMnPc2d1	neúspěšný
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
;	;	kIx,	;
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
u	u	k7c2	u
mladších	mladý	k2eAgFnPc2d2	mladší
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
ovlivňovány	ovlivňovat	k5eAaImNgInP	ovlivňovat
širšími	široký	k2eAgFnPc7d2	širší
existenčními	existenční	k2eAgNnPc7d1	existenční
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Abrahámovská	abrahámovský	k2eAgNnPc1d1	abrahámovské
náboženství	náboženství	k1gNnPc1	náboženství
sebevraždu	sebevražda	k1gFnSc4	sebevražda
tradičně	tradičně	k6eAd1	tradičně
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
urážku	urážka	k1gFnSc4	urážka
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
posvátnost	posvátnost	k1gFnSc4	posvátnost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
samurajské	samurajský	k2eAgFnSc2d1	samurajská
éry	éra	k1gFnSc2	éra
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byl	být	k5eAaImAgInS	být
rituál	rituál	k1gInSc1	rituál
seppuku	seppuk	k1gInSc2	seppuk
uznáván	uznávat	k5eAaImNgInS	uznávat
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc1	prostředek
odčinění	odčinění	k1gNnSc2	odčinění
selhání	selhání	k1gNnSc2	selhání
nebo	nebo	k8xC	nebo
forma	forma	k1gFnSc1	forma
protestu	protest	k1gInSc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Satí	Satí	k6eAd1	Satí
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
nezákonný	zákonný	k2eNgInSc4d1	nezákonný
hinduistický	hinduistický	k2eAgInSc4d1	hinduistický
pohřební	pohřební	k2eAgInSc4d1	pohřební
obřad	obřad	k1gInSc4	obřad
<g/>
,	,	kIx,	,
očekával	očekávat	k5eAaImAgInS	očekávat
od	od	k7c2	od
vdovy	vdova	k1gFnSc2	vdova
sebeupálení	sebeupálení	k1gNnSc2	sebeupálení
na	na	k7c6	na
manželově	manželův	k2eAgFnSc6d1	manželova
pohřební	pohřební	k2eAgFnSc6d1	pohřební
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
dobrovolně	dobrovolně	k6eAd1	dobrovolně
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
i	i	k8xC	i
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
trestně	trestně	k6eAd1	trestně
postižitelné	postižitelný	k2eAgNnSc1d1	postižitelné
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
islámských	islámský	k2eAgFnPc2d1	islámská
zemí	zem	k1gFnPc2	zem
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
odchodu	odchod	k1gInSc2	odchod
ze	z	k7c2	z
života	život	k1gInSc2	život
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
formou	forma	k1gFnSc7	forma
sebeupálení	sebeupálení	k1gNnSc2	sebeupálení
prostředkem	prostředek	k1gInSc7	prostředek
protestu	protest	k1gInSc2	protest
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kamikaze	kamikaze	k1gMnPc1	kamikaze
a	a	k8xC	a
sebevražedné	sebevražedný	k2eAgInPc1d1	sebevražedný
bombové	bombový	k2eAgInPc1d1	bombový
útoky	útok	k1gInPc1	útok
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaPmNgInP	využívat
jako	jako	k8xC	jako
forma	forma	k1gFnSc1	forma
vojenské	vojenský	k2eAgFnSc2d1	vojenská
nebo	nebo	k8xC	nebo
teroristické	teroristický	k2eAgFnSc2d1	teroristická
taktiky	taktika	k1gFnSc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
rovněž	rovněž	k9	rovněž
jako	jako	k8xC	jako
dokonaná	dokonaný	k2eAgFnSc1d1	dokonaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
akt	akt	k1gInSc1	akt
odnětí	odnětí	k1gNnSc1	odnětí
si	se	k3xPyFc3	se
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nebo	nebo	k8xC	nebo
sebevražedné	sebevražedný	k2eAgNnSc4d1	sebevražedné
chování	chování	k1gNnSc4	chování
bez	bez	k7c2	bez
fatálních	fatální	k2eAgInPc2d1	fatální
následků	následek	k1gInPc2	následek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
charakterizovány	charakterizován	k2eAgFnPc1d1	charakterizována
jako	jako	k8xS	jako
sebepoškození	sebepoškození	k1gNnSc1	sebepoškození
motivované	motivovaný	k2eAgFnSc2d1	motivovaná
touhou	touha	k1gFnSc7	touha
ukončit	ukončit	k5eAaPmF	ukončit
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
nepřímo	přímo	k6eNd1	přímo
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
druhému	druhý	k4xOgMnSc3	druhý
člověku	člověk	k1gMnSc3	člověk
přivodit	přivodit	k5eAaPmF	přivodit
si	se	k3xPyFc3	se
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
poskytnutím	poskytnutí	k1gNnSc7	poskytnutí
rady	rada	k1gFnSc2	rada
nebo	nebo	k8xC	nebo
prostředků	prostředek	k1gInPc2	prostředek
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
spáchání	spáchání	k1gNnSc3	spáchání
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
eutanazie	eutanazie	k1gFnSc2	eutanazie
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
druhá	druhý	k4xOgFnSc1	druhý
osoba	osoba	k1gFnSc1	osoba
hraje	hrát	k5eAaImIp3nS	hrát
mnohem	mnohem	k6eAd1	mnohem
aktivnější	aktivní	k2eAgFnSc4d2	aktivnější
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražedné	sebevražedný	k2eAgFnPc1d1	sebevražedná
myšlenky	myšlenka	k1gFnPc1	myšlenka
jsou	být	k5eAaImIp3nP	být
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
spojením	spojení	k1gNnSc7	spojení
zvratného	zvratný	k2eAgNnSc2d1	zvratné
zájmena	zájmeno	k1gNnSc2	zájmeno
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
vrah	vrah	k1gMnSc1	vrah
<g/>
/	/	kIx~	/
<g/>
vražda	vražda	k1gFnSc1	vražda
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
základ	základ	k1gInSc4	základ
v	v	k7c6	v
praslovanském	praslovanský	k2eAgInSc6d1	praslovanský
výrazu	výraz	k1gInSc6	výraz
vorgъ	vorgъ	k?	vorgъ
-	-	kIx~	-
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
;	;	kIx,	;
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
významu	význam	k1gInSc2	význam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
vykonání	vykonání	k1gNnSc4	vykonání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ustálené	ustálený	k2eAgNnSc1d1	ustálené
slovní	slovní	k2eAgNnSc1d1	slovní
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
spáchat	spáchat	k5eAaPmF	spáchat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
pojí	pojit	k5eAaImIp3nS	pojit
s	s	k7c7	s
negativními	negativní	k2eAgInPc7d1	negativní
jevy	jev	k1gInPc7	jev
(	(	kIx(	(
<g/>
spáchat	spáchat	k5eAaPmF	spáchat
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
spáchat	spáchat	k5eAaPmF	spáchat
hřích	hřích	k1gInSc4	hřích
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
souvisí	souviset	k5eAaImIp3nS	souviset
také	také	k9	také
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
nábožeství	nábožeství	k1gNnSc2	nábožeství
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
tento	tento	k3xDgInSc1	tento
skutek	skutek	k1gInSc1	skutek
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
těžký	těžký	k2eAgInSc4d1	těžký
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
sebevrahům	sebevrah	k1gMnPc3	sebevrah
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dokonce	dokonce	k9	dokonce
odepřen	odepřen	k2eAgInSc1d1	odepřen
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
takový	takový	k3xDgInSc4	takový
jev	jev	k1gInSc4	jev
popisují	popisovat	k5eAaImIp3nP	popisovat
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nP	volit
přímá	přímý	k2eAgNnPc4d1	přímé
slova	slovo	k1gNnPc4	slovo
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nalehl	nalehnout	k5eAaPmAgInS	nalehnout
na	na	k7c4	na
meč	meč	k1gInSc4	meč
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
oběsil	oběsit	k5eAaPmAgInS	oběsit
se	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
také	také	k9	také
pojmy	pojem	k1gInPc1	pojem
samovrah	samovrah	k?	samovrah
<g/>
,	,	kIx,	,
samovražda	samovražda	k1gFnSc1	samovražda
a	a	k8xC	a
také	také	k9	také
básnické	básnický	k2eAgInPc1d1	básnický
pojmy	pojem	k1gInPc1	pojem
samobijstvo	samobijstvo	k1gNnSc1	samobijstvo
<g/>
,	,	kIx,	,
samobojstvo	samobojstvo	k1gNnSc1	samobojstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sebevraždu	sebevražda	k1gFnSc4	sebevražda
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
eufemismy	eufemismus	k1gInPc1	eufemismus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dobrovolný	dobrovolný	k2eAgInSc1d1	dobrovolný
odchod	odchod	k1gInSc1	odchod
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Psycholog	psycholog	k1gMnSc1	psycholog
Pavel	Pavel	k1gMnSc1	Pavel
Říčan	Říčany	k1gInPc2	Říčany
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Agresivita	agresivita	k1gFnSc1	agresivita
a	a	k8xC	a
šikana	šikana	k1gFnSc1	šikana
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
používá	používat	k5eAaImIp3nS	používat
pojmenování	pojmenování	k1gNnSc1	pojmenování
suicidium	suicidium	k1gNnSc1	suicidium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
sebezabití	sebezabití	k1gNnSc1	sebezabití
<g/>
.	.	kIx.	.
</s>
<s>
Připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
poznámku	poznámka	k1gFnSc4	poznámka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Záměrně	záměrně	k6eAd1	záměrně
neužívám	užívat	k5eNaImIp1nS	užívat
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
odsuzující	odsuzující	k2eAgInSc1d1	odsuzující
akcent	akcent	k1gInSc1	akcent
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
suicidií	suicidie	k1gFnPc2	suicidie
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
knihy	kniha	k1gFnSc2	kniha
rakouského	rakouský	k2eAgMnSc2d1	rakouský
esejisty	esejista	k1gMnSc2	esejista
a	a	k8xC	a
obhájce	obhájce	k1gMnSc2	obhájce
sebevraždy	sebevražda	k1gFnSc2	sebevražda
Jeana	Jean	k1gMnSc2	Jean
Améryho	Améry	k1gMnSc4	Améry
Vztáhnout	vztáhnout	k5eAaPmF	vztáhnout
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
ruku	ruka	k1gFnSc4	ruka
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
sebevraždu	sebevražda	k1gFnSc4	sebevražda
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
termín	termín	k1gInSc1	termín
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
chystá	chystat	k5eAaImIp3nS	chystat
označení	označení	k1gNnSc1	označení
suicidér	suicidér	k1gMnSc1	suicidér
a	a	k8xC	a
pro	pro	k7c4	pro
oběť	oběť	k1gFnSc4	oběť
dokonané	dokonaný	k2eAgFnSc2d1	dokonaná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
označení	označení	k1gNnSc2	označení
suicidant	suicidanta	k1gFnPc2	suicidanta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
riziko	riziko	k1gNnSc4	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
genetická	genetický	k2eAgFnSc1d1	genetická
dispozice	dispozice	k1gFnSc1	dispozice
<g/>
,	,	kIx,	,
psychiatrické	psychiatrický	k2eAgFnPc1d1	psychiatrická
poruchy	porucha	k1gFnPc1	porucha
<g/>
,	,	kIx,	,
narkomanie	narkomanie	k1gFnPc1	narkomanie
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
psychické	psychický	k2eAgInPc1d1	psychický
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc1d1	kulturní
<g/>
,	,	kIx,	,
rodinné	rodinný	k2eAgFnPc1d1	rodinná
a	a	k8xC	a
společenské	společenský	k2eAgFnPc1d1	společenská
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
genetika	genetika	k1gFnSc1	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Duševní	duševní	k2eAgNnPc1d1	duševní
onemocnění	onemocnění	k1gNnPc1	onemocnění
a	a	k8xC	a
zneužívání	zneužívání	k1gNnSc1	zneužívání
návykových	návykový	k2eAgFnPc2d1	návyková
látek	látka	k1gFnPc2	látka
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
souběžnými	souběžný	k2eAgInPc7d1	souběžný
jevy	jev	k1gInPc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
rizikové	rizikový	k2eAgInPc4d1	rizikový
faktory	faktor	k1gInPc4	faktor
patří	patřit	k5eAaImIp3nP	patřit
předchozí	předchozí	k2eAgInPc4d1	předchozí
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
dostupnost	dostupnost	k1gFnSc1	dostupnost
prostředků	prostředek	k1gInPc2	prostředek
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
spáchání	spáchání	k1gNnSc1	spáchání
<g/>
,	,	kIx,	,
předchozí	předchozí	k2eAgFnSc1d1	předchozí
sebevražda	sebevražda	k1gFnSc1	sebevražda
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
nebo	nebo	k8xC	nebo
traumatické	traumatický	k2eAgNnSc1d1	traumatické
poranění	poranění	k1gNnSc1	poranění
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
se	s	k7c7	s
střelnými	střelný	k2eAgFnPc7d1	střelná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Socioekonomické	socioekonomický	k2eAgInPc1d1	socioekonomický
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
,	,	kIx,	,
bezdomovectví	bezdomovectví	k1gNnSc1	bezdomovectví
a	a	k8xC	a
diskriminace	diskriminace	k1gFnSc1	diskriminace
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgFnPc4d1	další
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
vyvolat	vyvolat	k5eAaPmF	vyvolat
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
dopis	dopis	k1gInSc1	dopis
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
genetika	genetika	k1gFnSc1	genetika
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
%	%	kIx~	%
až	až	k9	až
55	[number]	k4	55
%	%	kIx~	%
případů	případ	k1gInPc2	případ
sebevražedného	sebevražedný	k2eAgNnSc2d1	sebevražedné
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Váleční	váleční	k2eAgMnPc1d1	váleční
veteráni	veterán	k1gMnPc1	veterán
jsou	být	k5eAaImIp3nP	být
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
vyšším	vysoký	k2eAgNnSc7d2	vyšší
rizikem	riziko	k1gNnSc7	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
částečně	částečně	k6eAd1	částečně
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyššího	vysoký	k2eAgInSc2d2	vyšší
počtu	počet	k1gInSc2	počet
případů	případ	k1gInPc2	případ
duševních	duševní	k2eAgNnPc2d1	duševní
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
problémů	problém	k1gInPc2	problém
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
abnormální	abnormální	k2eAgFnSc7d1	abnormální
verzí	verze	k1gFnSc7	verze
genu	gen	k1gInSc2	gen
SKA2	SKA2	k1gFnSc2	SKA2
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
větší	veliký	k2eAgInPc4d2	veliký
problémy	problém	k1gInPc4	problém
zastavit	zastavit	k5eAaPmF	zastavit
příliv	příliv	k1gInSc4	příliv
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
produkuje	produkovat	k5eAaImIp3nS	produkovat
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
stres	stres	k1gInSc4	stres
(	(	kIx(	(
<g/>
KAMINSKI	KAMINSKI	kA	KAMINSKI
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
díky	díky	k7c3	díky
kterým	který	k3yIgMnPc3	který
tělo	tělo	k1gNnSc4	tělo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
negativním	negativní	k2eAgNnSc6d1	negativní
rozpoložení	rozpoložení	k1gNnSc6	rozpoložení
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
připravenosti	připravenost	k1gFnSc6	připravenost
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
experimentů	experiment	k1gInPc2	experiment
až	až	k6eAd1	až
u	u	k7c2	u
80	[number]	k4	80
<g/>
%	%	kIx~	%
sebevrahů	sebevrah	k1gMnPc2	sebevrah
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pomocí	pomocí	k7c2	pomocí
testu	test	k1gInSc2	test
předvídat	předvídat	k5eAaImF	předvídat
hrozící	hrozící	k2eAgInSc4d1	hrozící
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Duševní	duševní	k2eAgFnPc1d1	duševní
poruchy	porucha	k1gFnPc1	porucha
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
sebevraždy	sebevražda	k1gFnSc2	sebevražda
častým	častý	k2eAgInSc7d1	častý
jevem	jev	k1gInSc7	jev
<g/>
;	;	kIx,	;
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
27	[number]	k4	27
%	%	kIx~	%
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
přijatých	přijatý	k2eAgInPc2d1	přijatý
na	na	k7c4	na
psychiatrické	psychiatrický	k2eAgNnSc4d1	psychiatrické
oddělení	oddělení	k1gNnSc4	oddělení
je	být	k5eAaImIp3nS	být
doživotní	doživotní	k2eAgNnSc1d1	doživotní
riziko	riziko	k1gNnSc1	riziko
dokonané	dokonaný	k2eAgFnSc2d1	dokonaná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
přibližně	přibližně	k6eAd1	přibližně
8,6	[number]	k4	8,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
polovina	polovina	k1gFnSc1	polovina
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zemřely	zemřít	k5eAaPmAgFnP	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
trpěla	trpět	k5eAaImAgFnS	trpět
depresí	deprese	k1gFnSc7	deprese
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
nebo	nebo	k8xC	nebo
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
afektivních	afektivní	k2eAgFnPc2d1	afektivní
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bipolární	bipolární	k2eAgFnSc1d1	bipolární
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
dvacetinásobně	dvacetinásobně	k6eAd1	dvacetinásobně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
významné	významný	k2eAgFnPc4d1	významná
příčiny	příčina	k1gFnPc4	příčina
sebevražd	sebevražda	k1gFnPc2	sebevražda
patří	patřit	k5eAaImIp3nP	patřit
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnSc2	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bipolární	bipolární	k2eAgFnSc1d1	bipolární
porucha	porucha	k1gFnSc1	porucha
a	a	k8xC	a
posttraumatická	posttraumatický	k2eAgFnSc1d1	posttraumatická
stresová	stresový	k2eAgFnSc1d1	stresová
porucha	porucha	k1gFnSc1	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
následky	následek	k1gInPc4	následek
sebevraždy	sebevražda	k1gFnSc2	sebevražda
umírá	umírat	k5eAaImIp3nS	umírat
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	poruch	k1gInPc1	poruch
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
jsou	být	k5eAaImIp3nP	být
dalším	další	k2eAgInSc7d1	další
onemocněním	onemocnění	k1gNnPc3	onemocnění
spojeným	spojený	k2eAgInSc7d1	spojený
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rizikem	riziko	k1gNnSc7	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
prediktivním	prediktivní	k2eAgMnSc7d1	prediktivní
ukazatelem	ukazatel	k1gMnSc7	ukazatel
eventuálního	eventuální	k2eAgNnSc2d1	eventuální
dokonání	dokonání	k1gNnSc2	dokonání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
jsou	být	k5eAaImIp3nP	být
předchozí	předchozí	k2eAgInPc4d1	předchozí
sebevražedné	sebevražedný	k2eAgInPc4d1	sebevražedný
pokusy	pokus	k1gInPc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
předchází	předcházet	k5eAaImIp3nS	předcházet
dřívější	dřívější	k2eAgInSc4d1	dřívější
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
pokusí	pokusit	k5eAaPmIp3nP	pokusit
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
roka	rok	k1gInSc2	rok
dokoná	dokonat	k5eAaPmIp3nS	dokonat
1	[number]	k4	1
%	%	kIx~	%
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
pak	pak	k6eAd1	pak
sebevraždu	sebevražda	k1gFnSc4	sebevražda
spáchá	spáchat	k5eAaPmIp3nS	spáchat
po	po	k7c6	po
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
sebepoškozování	sebepoškozování	k1gNnSc1	sebepoškozování
není	být	k5eNaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
sebepoškozující	sebepoškozující	k2eAgNnSc4d1	sebepoškozující
chování	chování	k1gNnSc4	chování
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
rizikem	riziko	k1gNnSc7	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
%	%	kIx~	%
případů	případ	k1gInPc2	případ
dokonaných	dokonaný	k2eAgFnPc2d1	dokonaná
sebevražd	sebevražda	k1gFnPc2	sebevražda
dotyčná	dotyčný	k2eAgFnSc1d1	dotyčná
osoba	osoba	k1gFnSc1	osoba
navštívila	navštívit	k5eAaPmAgFnS	navštívit
lékaře	lékař	k1gMnSc4	lékař
během	během	k7c2	během
roku	rok	k1gInSc2	rok
předcházejícího	předcházející	k2eAgNnSc2d1	předcházející
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
;	;	kIx,	;
ve	v	k7c6	v
45	[number]	k4	45
%	%	kIx~	%
tato	tento	k3xDgFnSc1	tento
návštěva	návštěva	k1gFnSc1	návštěva
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
předcházejícím	předcházející	k2eAgInSc6d1	předcházející
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sebevraždu	sebevražda	k1gFnSc4	sebevražda
dokonaly	dokonat	k5eAaPmAgFnP	dokonat
<g/>
,	,	kIx,	,
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
kontaktovalo	kontaktovat	k5eAaImAgNnS	kontaktovat
služby	služba	k1gFnPc4	služba
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Zneužívání	zneužívání	k1gNnSc1	zneužívání
návykových	návykový	k2eAgFnPc2d1	návyková
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
sebevraždy	sebevražda	k1gFnSc2	sebevražda
po	po	k7c6	po
depresi	deprese	k1gFnSc6	deprese
a	a	k8xC	a
bipolární	bipolární	k2eAgFnSc3d1	bipolární
poruše	porucha	k1gFnSc3	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sebevraždami	sebevražda	k1gFnPc7	sebevražda
souvisí	souviset	k5eAaImIp3nS	souviset
jak	jak	k6eAd1	jak
chronické	chronický	k2eAgNnSc1d1	chronické
zneužívání	zneužívání	k1gNnSc1	zneužívání
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
akutní	akutní	k2eAgFnPc4d1	akutní
intoxikace	intoxikace	k1gFnPc4	intoxikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
osobním	osobní	k2eAgInSc7d1	osobní
zármutkem	zármutek	k1gInSc7	zármutek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úmrtí	úmrtí	k1gNnSc2	úmrtí
blízké	blízký	k2eAgFnSc2d1	blízká
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
riziko	riziko	k1gNnSc1	riziko
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Zneužívání	zneužívání	k1gNnSc1	zneužívání
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
také	také	k9	také
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
duševního	duševní	k2eAgNnSc2d1	duševní
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
spáchání	spáchání	k1gNnSc2	spáchání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
sedativně-hypnotických	sedativněypnotický	k2eAgFnPc2d1	sedativně-hypnotický
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
alkohol	alkohol	k1gInSc4	alkohol
nebo	nebo	k8xC	nebo
benzodiazepiny	benzodiazepin	k1gInPc4	benzodiazepin
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
u	u	k7c2	u
15	[number]	k4	15
%	%	kIx~	%
až	až	k9	až
61	[number]	k4	61
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
spotřebou	spotřeba	k1gFnSc7	spotřeba
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hustotou	hustota	k1gFnSc7	hustota
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
alkohol	alkohol	k1gInSc1	alkohol
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgFnSc1	tento
souvislost	souvislost	k1gFnSc1	souvislost
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
zejména	zejména	k9	zejména
na	na	k7c6	na
konzumaci	konzumace	k1gFnSc6	konzumace
destilovaných	destilovaný	k2eAgInPc2d1	destilovaný
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
na	na	k7c6	na
konzumaci	konzumace	k1gFnSc6	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
2,2	[number]	k4	2,2
<g/>
-	-	kIx~	-
<g/>
3,4	[number]	k4	3,4
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
léčených	léčený	k2eAgFnPc2d1	léčená
z	z	k7c2	z
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
života	život	k1gInSc2	život
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
následky	následek	k1gInPc4	následek
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholici	alkoholik	k1gMnPc1	alkoholik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
muži	muž	k1gMnPc7	muž
staršího	starý	k2eAgInSc2d2	starší
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
pokusili	pokusit	k5eAaPmAgMnP	pokusit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
3	[number]	k4	3
až	až	k8xS	až
35	[number]	k4	35
%	%	kIx~	%
případů	případ	k1gInPc2	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
osob	osoba	k1gFnPc2	osoba
užívajících	užívající	k2eAgFnPc2d1	užívající
heroin	heroina	k1gFnPc2	heroina
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sebevraždy	sebevražda	k1gFnSc2	sebevražda
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
čtrnáctinásobek	čtrnáctinásobek	k1gInSc1	čtrnáctinásobek
počtu	počet	k1gInSc2	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tuto	tento	k3xDgFnSc4	tento
drogu	droga	k1gFnSc4	droga
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
kokainu	kokain	k1gInSc2	kokain
a	a	k8xC	a
metamfetaminů	metamfetamin	k1gInPc2	metamfetamin
se	s	k7c7	s
sebevraždou	sebevražda	k1gFnSc7	sebevražda
velmi	velmi	k6eAd1	velmi
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
užívajících	užívající	k2eAgFnPc2d1	užívající
kokain	kokain	k1gInSc4	kokain
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
během	během	k7c2	během
abstinenční	abstinenční	k2eAgFnSc2d1	abstinenční
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
užívající	užívající	k2eAgMnPc4d1	užívající
inhalanty	inhalant	k1gMnPc4	inhalant
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
vystaveny	vystavit	k5eAaPmNgInP	vystavit
značnému	značný	k2eAgNnSc3d1	značné
riziku	riziko	k1gNnSc3	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
-	-	kIx~	-
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
pokusí	pokusit	k5eAaPmIp3nS	pokusit
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
65	[number]	k4	65
%	%	kIx~	%
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
.	.	kIx.	.
</s>
<s>
Kouření	kouření	k1gNnSc1	kouření
cigaret	cigareta	k1gFnPc2	cigareta
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
rovněž	rovněž	k9	rovněž
spojováno	spojovat	k5eAaImNgNnS	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
souvislost	souvislost	k1gFnSc4	souvislost
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
málo	málo	k4c1	málo
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
však	však	k9	však
vysloveny	vysloven	k2eAgFnPc1d1	vyslovena
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
předpoklad	předpoklad	k1gInSc4	předpoklad
ke	k	k7c3	k
kouření	kouření	k1gNnSc3	kouření
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouření	kouření	k1gNnSc1	kouření
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
lidi	člověk	k1gMnPc4	člověk
následně	následně	k6eAd1	následně
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
myšlenkám	myšlenka	k1gFnPc3	myšlenka
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
kouření	kouření	k1gNnSc1	kouření
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
chemické	chemický	k2eAgInPc4d1	chemický
pochody	pochod	k1gInPc4	pochod
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
sebevražedných	sebevražedný	k2eAgInPc2d1	sebevražedný
sklonů	sklon	k1gInPc2	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Konopí	konopí	k1gNnSc1	konopí
však	však	k9	však
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
nijak	nijak	k6eAd1	nijak
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Gambleři	gambler	k1gMnPc1	gambler
(	(	kIx(	(
<g/>
patologičtí	patologický	k2eAgMnPc1d1	patologický
hráči	hráč	k1gMnPc1	hráč
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
běžnou	běžný	k2eAgFnSc7d1	běžná
populací	populace	k1gFnSc7	populace
spojováni	spojován	k2eAgMnPc1d1	spojován
s	s	k7c7	s
častějšími	častý	k2eAgFnPc7d2	častější
myšlenkami	myšlenka	k1gFnPc7	myšlenka
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
12	[number]	k4	12
až	až	k9	až
24	[number]	k4	24
%	%	kIx~	%
patologických	patologický	k2eAgMnPc2d1	patologický
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
jejich	jejich	k3xOp3gMnPc2	jejich
životních	životní	k2eAgMnPc2d1	životní
partnerů	partner	k1gMnPc2	partner
je	být	k5eAaImIp3nS	být
třikrát	třikrát	k6eAd1	třikrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
běžné	běžný	k2eAgFnSc2d1	běžná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
faktory	faktor	k1gMnPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
u	u	k7c2	u
gamblerů	gambler	k1gMnPc2	gambler
riziko	riziko	k1gNnSc4	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
duševní	duševní	k2eAgFnPc4d1	duševní
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
alkohol	alkohol	k1gInSc1	alkohol
a	a	k8xC	a
užívání	užívání	k1gNnSc1	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
sebevražedností	sebevražednost	k1gFnSc7	sebevražednost
(	(	kIx(	(
<g/>
suicidalitou	suicidalita	k1gFnSc7	suicidalita
<g/>
)	)	kIx)	)
a	a	k8xC	a
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
existuje	existovat	k5eAaImIp3nS	existovat
jistá	jistý	k2eAgFnSc1d1	jistá
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
;	;	kIx,	;
ke	k	k7c3	k
zmíněným	zmíněný	k2eAgFnPc3d1	zmíněná
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
potížím	potíž	k1gFnPc3	potíž
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
patří	patřit	k5eAaImIp3nS	patřit
chronická	chronický	k2eAgFnSc1d1	chronická
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
traumatické	traumatický	k2eAgNnSc1d1	traumatické
poranění	poranění	k1gNnSc1	poranění
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
rakovina	rakovina	k1gFnSc1	rakovina
<g/>
,	,	kIx,	,
hemodialýza	hemodialýza	k1gFnSc1	hemodialýza
<g/>
,	,	kIx,	,
HIV	HIV	kA	HIV
nebo	nebo	k8xC	nebo
systémový	systémový	k2eAgInSc4d1	systémový
lupus	lupus	k1gInSc4	lupus
erythematodes	erythematodesa	k1gFnPc2	erythematodesa
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
rakoviny	rakovina	k1gFnSc2	rakovina
riziko	riziko	k1gNnSc4	riziko
následné	následný	k2eAgFnSc2d1	následná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
přibližně	přibližně	k6eAd1	přibližně
zdvojnásobuje	zdvojnásobovat	k5eAaImIp3nS	zdvojnásobovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
sebevražednost	sebevražednost	k1gFnSc1	sebevražednost
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
po	po	k7c6	po
vypořádání	vypořádání	k1gNnSc6	vypořádání
se	se	k3xPyFc4	se
s	s	k7c7	s
depresivním	depresivní	k2eAgNnSc7d1	depresivní
onemocněním	onemocnění	k1gNnSc7	onemocnění
nebo	nebo	k8xC	nebo
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgNnSc7	jeden
onemocněním	onemocnění	k1gNnSc7	onemocnění
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
obzvláště	obzvláště	k6eAd1	obzvláště
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jsou	být	k5eAaImIp3nP	být
zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
problémy	problém	k1gInPc1	problém
uváděny	uvádět	k5eAaImNgInP	uvádět
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	poruch	k1gInPc1	poruch
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nespavost	nespavost	k1gFnSc1	nespavost
a	a	k8xC	a
spánková	spánkový	k2eAgFnSc1d1	spánková
apnoe	apnoe	k1gFnSc1	apnoe
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
deprese	deprese	k1gFnSc2	deprese
a	a	k8xC	a
sebevraždy	sebevražda	k1gFnSc2	sebevražda
dalšími	další	k2eAgInPc7d1	další
rizikovými	rizikový	k2eAgInPc7d1	rizikový
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
poruchy	porucha	k1gFnPc1	porucha
spánku	spánek	k1gInSc2	spánek
rizikovým	rizikový	k2eAgInSc7d1	rizikový
činitelem	činitel	k1gInSc7	činitel
nezávislým	závislý	k2eNgInSc7d1	nezávislý
na	na	k7c6	na
depresi	deprese	k1gFnSc6	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc7	symptom
podobnými	podobný	k2eAgInPc7d1	podobný
afektivním	afektivní	k2eAgFnPc3d1	afektivní
poruchám	porucha	k1gFnPc3	porucha
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevovat	projevovat	k5eAaImF	projevovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
hypotyreóza	hypotyreóza	k1gFnSc1	hypotyreóza
<g/>
,	,	kIx,	,
Alzheimerova	Alzheimerův	k2eAgFnSc1d1	Alzheimerova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
nádory	nádor	k1gInPc1	nádor
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
systémový	systémový	k2eAgInSc4d1	systémový
lupus	lupus	k1gInSc4	lupus
erythematodes	erythematodesa	k1gFnPc2	erythematodesa
a	a	k8xC	a
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
řady	řada	k1gFnSc2	řada
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
beta	beta	k1gNnSc1	beta
blokátorů	blokátor	k1gInPc2	blokátor
a	a	k8xC	a
steroidů	steroid	k1gInPc2	steroid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
řada	řada	k1gFnSc1	řada
psychosociálních	psychosociální	k2eAgInPc2d1	psychosociální
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
pocit	pocit	k1gInSc4	pocit
beznaděje	beznaděje	k1gFnSc1	beznaděje
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
radosti	radost	k1gFnSc2	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
a	a	k8xC	a
úzkostlivost	úzkostlivost	k1gFnSc1	úzkostlivost
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
rovněž	rovněž	k9	rovněž
snížená	snížený	k2eAgFnSc1d1	snížená
schopnost	schopnost	k1gFnSc1	schopnost
řešit	řešit	k5eAaImF	řešit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
snížená	snížený	k2eAgFnSc1d1	snížená
kontrola	kontrola	k1gFnSc1	kontrola
impulzivního	impulzivní	k2eAgNnSc2d1	impulzivní
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
vnímání	vnímání	k1gNnSc2	vnímání
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
jako	jako	k9	jako
přítěže	přítěž	k1gFnPc1	přítěž
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc4	riziko
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
také	také	k9	také
nedávné	dávný	k2eNgFnPc4d1	nedávná
stresující	stresující	k2eAgFnPc4d1	stresující
životní	životní	k2eAgFnPc4d1	životní
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ztráta	ztráta	k1gFnSc1	ztráta
člena	člen	k1gMnSc2	člen
rodiny	rodina	k1gFnSc2	rodina
nebo	nebo	k8xC	nebo
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
izolace	izolace	k1gFnSc1	izolace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
osamělý	osamělý	k2eAgInSc1d1	osamělý
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyššímu	vysoký	k2eAgNnSc3d2	vyšší
riziku	riziko	k1gNnSc3	riziko
jsou	být	k5eAaImIp3nP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nikdy	nikdy	k6eAd1	nikdy
nevstoupili	vstoupit	k5eNaPmAgMnP	vstoupit
do	do	k7c2	do
manželského	manželský	k2eAgInSc2d1	manželský
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
aktivní	aktivní	k2eAgFnSc1d1	aktivní
víra	víra	k1gFnSc1	víra
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
přičítáno	přičítán	k2eAgNnSc4d1	přičítáno
negativnímu	negativní	k2eAgInSc3d1	negativní
postoji	postoj	k1gInSc3	postoj
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
mnohá	mnohý	k2eAgNnPc1d1	mnohé
náboženství	náboženství	k1gNnPc1	náboženství
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
<g/>
,	,	kIx,	,
a	a	k8xC	a
větší	veliký	k2eAgFnSc2d2	veliký
vztahové	vztahový	k2eAgFnSc2d1	vztahová
pospolitosti	pospolitost	k1gFnSc2	pospolitost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
náboženství	náboženství	k1gNnSc1	náboženství
může	moct	k5eAaImIp3nS	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
nižší	nízký	k2eAgInSc1d2	nižší
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
sebevraždu	sebevražda	k1gFnSc4	sebevražda
spáchat	spáchat	k5eAaPmF	spáchat
jako	jako	k9	jako
únik	únik	k1gInSc4	únik
před	před	k7c7	před
šikanou	šikana	k1gFnSc7	šikana
či	či	k8xC	či
předsudky	předsudek	k1gInPc7	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgNnSc1d1	sexuální
zneužívání	zneužívání	k1gNnSc1	zneužívání
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
čas	čas	k1gInSc1	čas
strávený	strávený	k2eAgInSc1d1	strávený
v	v	k7c6	v
pěstounské	pěstounský	k2eAgFnSc6d1	pěstounská
péči	péče	k1gFnSc6	péče
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k6eAd1	rovněž
dalšími	další	k2eAgInPc7d1	další
z	z	k7c2	z
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sexuální	sexuální	k2eAgNnSc1d1	sexuální
zneužívání	zneužívání	k1gNnSc1	zneužívání
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
riziku	riziko	k1gNnSc3	riziko
přispívá	přispívat	k5eAaImIp3nS	přispívat
až	až	k6eAd1	až
z	z	k7c2	z
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vývojová	vývojový	k2eAgFnSc1d1	vývojová
psychologie	psychologie	k1gFnSc1	psychologie
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
inkluzivní	inkluzivní	k2eAgFnSc4d1	inkluzivní
zdatnost	zdatnost	k1gFnSc4	zdatnost
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
páchá	páchat	k5eAaImIp3nS	páchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nemůže	moct	k5eNaImIp3nS	moct
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
naživu	naživu	k6eAd1	naživu
<g/>
,	,	kIx,	,
odčerpává	odčerpávat	k5eAaImIp3nS	odčerpávat
zdroje	zdroj	k1gInPc4	zdroj
a	a	k8xC	a
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
využívat	využívat	k5eAaPmF	využívat
její	její	k3xOp3gNnSc4	její
příbuzenstvo	příbuzenstvo	k1gNnSc4	příbuzenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Námitkou	námitka	k1gFnSc7	námitka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
zdravých	zdravý	k2eAgMnPc2d1	zdravý
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
reprodukční	reprodukční	k2eAgFnSc4d1	reprodukční
schopnost	schopnost	k1gFnSc4	schopnost
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nijak	nijak	k6eAd1	nijak
nezvýší	zvýšit	k5eNaPmIp3nS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
odlišné	odlišný	k2eAgNnSc4d1	odlišné
původní	původní	k2eAgNnSc4d1	původní
prostředí	prostředí	k1gNnSc4	prostředí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
současném	současný	k2eAgNnSc6d1	současné
kontraproduktivní	kontraproduktivní	k2eAgInPc4d1	kontraproduktivní
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
souvisí	souviset	k5eAaImIp3nS	souviset
také	také	k9	také
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
chudoba	chudoba	k1gFnSc1	chudoba
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
okolím	okolí	k1gNnSc7	okolí
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
rizika	riziko	k1gNnSc2	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
spáchalo	spáchat	k5eAaPmAgNnS	spáchat
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
sebevraždu	sebevražda	k1gFnSc4	sebevražda
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
dluhy	dluh	k1gInPc7	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
třikrát	třikrát	k6eAd1	třikrát
pravděpodobnější	pravděpodobný	k2eAgFnSc1d2	pravděpodobnější
ve	v	k7c6	v
venkovských	venkovský	k2eAgInPc6d1	venkovský
regionech	region	k1gInPc6	region
než	než	k8xS	než
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
finančních	finanční	k2eAgFnPc2d1	finanční
obtíží	obtíž	k1gFnPc2	obtíž
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
problematice	problematika	k1gFnSc6	problematika
sebevražd	sebevražda	k1gFnPc2	sebevražda
hrají	hrát	k5eAaImIp3nP	hrát
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
sebevraždu	sebevražda	k1gFnSc4	sebevražda
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
prominentně	prominentně	k6eAd1	prominentně
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
uváděných	uváděný	k2eAgInPc2d1	uváděný
a	a	k8xC	a
sebevraždu	sebevražda	k1gFnSc4	sebevražda
glorifikujících	glorifikující	k2eAgInPc2d1	glorifikující
nebo	nebo	k8xC	nebo
romantizujících	romantizující	k2eAgInPc2d1	romantizující
<g/>
,	,	kIx,	,
negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
uveden	uveden	k2eAgInSc1d1	uveden
podrobný	podrobný	k2eAgInSc1d1	podrobný
popis	popis	k1gInSc1	popis
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
konkrétními	konkrétní	k2eAgInPc7d1	konkrétní
prostředky	prostředek	k1gInPc7	prostředek
sebevraždu	sebevražda	k1gFnSc4	sebevražda
spáchat	spáchat	k5eAaPmF	spáchat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
četnost	četnost	k1gFnSc1	četnost
použití	použití	k1gNnSc2	použití
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
sebevraždy	sebevražda	k1gFnSc2	sebevražda
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
populací	populace	k1gFnSc7	populace
zvýšit	zvýšit	k5eAaPmF	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spouštěcí	spouštěcí	k2eAgInSc1d1	spouštěcí
faktor	faktor	k1gInSc1	faktor
epidemií	epidemie	k1gFnPc2	epidemie
sebevražd	sebevražda	k1gFnPc2	sebevražda
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
sebevražd	sebevražda	k1gFnPc2	sebevražda
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Wertherův	Wertherův	k2eAgInSc1d1	Wertherův
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
protagonistovi	protagonista	k1gMnSc6	protagonista
Goetheho	Goethe	k1gMnSc2	Goethe
díla	dílo	k1gNnSc2	dílo
Utrpení	utrpení	k1gNnSc2	utrpení
mladého	mladý	k2eAgNnSc2d1	mladé
Werthera	Werthero	k1gNnSc2	Werthero
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sebevraždu	sebevražda	k1gFnSc4	sebevražda
sám	sám	k3xTgMnSc1	sám
spáchal	spáchat	k5eAaPmAgInS	spáchat
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
u	u	k7c2	u
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
smrt	smrt	k1gFnSc4	smrt
romantizovat	romantizovat	k5eAaImF	romantizovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
vliv	vliv	k1gInSc4	vliv
sdělovacích	sdělovací	k2eAgInPc2d1	sdělovací
prostředků	prostředek	k1gInPc2	prostředek
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
zábavních	zábavní	k2eAgInPc2d1	zábavní
prostředků	prostředek	k1gInPc2	prostředek
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vymezit	vymezit	k5eAaPmF	vymezit
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
návrhy	návrh	k1gInPc7	návrh
<g/>
,	,	kIx,	,
aby	aby	k9	aby
protikladem	protiklad	k1gInSc7	protiklad
Wertherova	Wertherův	k2eAgInSc2d1	Wertherův
efektu	efekt	k1gInSc2	efekt
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
Papagenův	Papagenův	k2eAgInSc4d1	Papagenův
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poskytování	poskytování	k1gNnSc1	poskytování
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
mechanismech	mechanismus	k1gInPc6	mechanismus
zvládání	zvládání	k1gNnSc2	zvládání
náročných	náročný	k2eAgFnPc2d1	náročná
situací	situace	k1gFnPc2	situace
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
ochranný	ochranný	k2eAgInSc4d1	ochranný
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
opery	opera	k1gFnSc2	opera
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
milované	milovaný	k2eAgFnSc2d1	milovaná
dívky	dívka	k1gFnSc2	dívka
chystá	chystat	k5eAaImIp3nS	chystat
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
mu	on	k3xPp3gMnSc3	on
přátelé	přítel	k1gMnPc1	přítel
nepomohou	pomoct	k5eNaPmIp3nP	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
média	médium	k1gNnPc1	médium
podávala	podávat	k5eAaImAgNnP	podávat
zprávy	zpráva	k1gFnSc2	zpráva
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
možno	možno	k6eAd1	možno
riziko	riziko	k1gNnSc1	riziko
sebevražd	sebevražda	k1gFnPc2	sebevražda
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Získání	získání	k1gNnSc1	získání
podpory	podpora	k1gFnSc2	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
mediálního	mediální	k2eAgInSc2d1	mediální
průmyslu	průmysl	k1gInSc2	průmysl
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zejména	zejména	k9	zejména
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Racionální	racionální	k2eAgFnSc1d1	racionální
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
promyšlené	promyšlený	k2eAgNnSc4d1	promyšlené
a	a	k8xC	a
odůvodněné	odůvodněný	k2eAgNnSc4d1	odůvodněné
odnětí	odnětí	k1gNnSc4	odnětí
si	se	k3xPyFc3	se
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sebevražda	sebevražda	k1gFnSc1	sebevražda
není	být	k5eNaImIp3nS	být
logická	logický	k2eAgNnPc4d1	logické
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
dobrovolného	dobrovolný	k2eAgInSc2d1	dobrovolný
odchodu	odchod	k1gInSc2	odchod
ze	z	k7c2	z
života	život	k1gInSc2	život
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
druhých	druhý	k4xOgFnPc2	druhý
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
altruistická	altruistický	k2eAgFnSc1d1	altruistická
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
starý	starý	k2eAgMnSc1d1	starý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc1	život
ukončí	ukončit	k5eAaPmIp3nS	ukončit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mladším	mladý	k2eAgMnPc3d2	mladší
lidem	člověk	k1gMnPc3	člověk
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
zanechal	zanechat	k5eAaPmAgMnS	zanechat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
eskymáckých	eskymácký	k2eAgFnPc6d1	eskymácká
kulturách	kultura	k1gFnPc6	kultura
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
projev	projev	k1gInSc1	projev
vážnosti	vážnost	k1gFnSc2	vážnost
<g/>
,	,	kIx,	,
odvahy	odvaha	k1gFnSc2	odvaha
nebo	nebo	k8xC	nebo
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
útok	útok	k1gInSc1	útok
je	být	k5eAaImIp3nS	být
politický	politický	k2eAgInSc4d1	politický
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
útočník	útočník	k1gMnSc1	útočník
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
vlastní	vlastní	k2eAgFnSc3d1	vlastní
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
sebevražední	sebevražedný	k2eAgMnPc1d1	sebevražedný
bomboví	bombový	k2eAgMnPc1d1	bombový
útočníci	útočník	k1gMnPc1	útočník
páchají	páchat	k5eAaImIp3nP	páchat
útoky	útok	k1gInPc4	útok
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
titulu	titul	k1gInSc2	titul
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
kamikadze	kamikadze	k1gMnPc2	kamikadze
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
jako	jako	k8xS	jako
úkol	úkol	k1gInSc4	úkol
motivovaný	motivovaný	k2eAgInSc4d1	motivovaný
vyšším	vysoký	k2eAgInSc7d2	vyšší
principem	princip	k1gInSc7	princip
nebo	nebo	k8xC	nebo
morální	morální	k2eAgFnSc7d1	morální
povinností	povinnost	k1gFnSc7	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
skutek	skutek	k1gInSc4	skutek
vraždy	vražda	k1gFnSc2	vražda
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
následovaný	následovaný	k2eAgMnSc1d1	následovaný
sebevraždou	sebevražda	k1gFnSc7	sebevražda
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vraždu	vražda	k1gFnSc4	vražda
spáchala	spáchat	k5eAaPmAgFnS	spáchat
<g/>
.	.	kIx.	.
</s>
<s>
Hromadné	hromadný	k2eAgFnPc1d1	hromadná
sebevraždy	sebevražda	k1gFnPc1	sebevražda
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
páchány	páchat	k5eAaImNgFnP	páchat
pod	pod	k7c7	pod
sociálním	sociální	k2eAgInSc7d1	sociální
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
vzdávají	vzdávat	k5eAaImIp3nP	vzdávat
své	svůj	k3xOyFgFnPc4	svůj
osobní	osobní	k2eAgFnPc4d1	osobní
nezávislosti	nezávislost	k1gFnPc4	nezávislost
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Masovou	masový	k2eAgFnSc4d1	masová
sebevraždu	sebevražda	k1gFnSc4	sebevražda
mohou	moct	k5eAaImIp3nP	moct
spáchat	spáchat	k5eAaPmF	spáchat
i	i	k9	i
pouzí	pouhý	k2eAgMnPc1d1	pouhý
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
-	-	kIx~	-
taková	takový	k3xDgFnSc1	takový
sebevražda	sebevražda	k1gFnSc1	sebevražda
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
pokračování	pokračování	k1gNnSc1	pokračování
života	život	k1gInSc2	život
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k9	jako
nesnesitelné	snesitelný	k2eNgFnPc1d1	nesnesitelná
<g/>
,	,	kIx,	,
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
lidé	člověk	k1gMnPc1	člověk
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
jako	jako	k8xS	jako
k	k	k7c3	k
prostředku	prostředek	k1gInSc3	prostředek
úniku	únik	k1gInSc2	únik
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vězni	vězeň	k1gMnPc1	vězeň
v	v	k7c6	v
nacistických	nacistický	k2eAgInPc6d1	nacistický
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
volili	volit	k5eAaImAgMnP	volit
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
smrt	smrt	k1gFnSc4	smrt
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
záměrně	záměrně	k6eAd1	záměrně
dotkli	dotknout	k5eAaPmAgMnP	dotknout
elektřinou	elektřina	k1gFnSc7	elektřina
napájených	napájený	k2eAgInPc2d1	napájený
plotů	plot	k1gInPc2	plot
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInPc1d3	nejčastější
způsoby	způsob	k1gInPc1	způsob
provedení	provedení	k1gNnSc2	provedení
sebevraždy	sebevražda	k1gFnSc2	sebevražda
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
nejběžnějším	běžný	k2eAgMnPc3d3	Nejběžnější
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
sebevražda	sebevražda	k1gFnSc1	sebevražda
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
,	,	kIx,	,
otrávením	otrávení	k1gNnSc7	otrávení
pesticidy	pesticid	k1gInPc4	pesticid
a	a	k8xC	a
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
střelné	střelný	k2eAgFnSc2d1	střelná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
existují	existovat	k5eAaImIp3nP	existovat
částečně	částečně	k6eAd1	částečně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dostupnosti	dostupnost	k1gFnSc2	dostupnost
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
spáchání	spáchání	k1gNnSc4	spáchání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
provedený	provedený	k2eAgInSc1d1	provedený
v	v	k7c6	v
56	[number]	k4	56
zemích	zem	k1gFnPc6	zem
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
formou	forma	k1gFnSc7	forma
sebevraždy	sebevražda	k1gFnSc2	sebevražda
oběšení	oběšení	k1gNnSc4	oběšení
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
se	se	k3xPyFc4	se
přičítá	přičítat	k5eAaImIp3nS	přičítat
53	[number]	k4	53
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
39	[number]	k4	39
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
30	[number]	k4	30
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
spácháno	spáchat	k5eAaPmNgNnS	spáchat
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
využití	využití	k1gNnSc2	využití
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
se	se	k3xPyFc4	se
však	však	k9	však
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
-	-	kIx~	-
od	od	k7c2	od
4	[number]	k4	4
%	%	kIx~	%
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
je	být	k5eAaImIp3nS	být
nejpřístupnější	přístupný	k2eAgNnSc1d3	nejpřístupnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
stojí	stát	k5eAaImIp3nS	stát
předávkování	předávkování	k1gNnSc2	předávkování
léky	lék	k1gInPc4	lék
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
30	[number]	k4	30
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
sebevražd	sebevražda	k1gFnPc2	sebevražda
je	být	k5eAaImIp3nS	být
neplánovaných	plánovaný	k2eNgInPc2d1	neplánovaný
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
akutních	akutní	k2eAgNnPc2d1	akutní
období	období	k1gNnPc2	období
rozpolcenosti	rozpolcenost	k1gFnSc2	rozpolcenost
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
použitých	použitý	k2eAgInPc2d1	použitý
prostředků	prostředek	k1gInPc2	prostředek
<g/>
:	:	kIx,	:
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
80-90	[number]	k4	80-90
%	%	kIx~	%
<g/>
,	,	kIx,	,
utonutí	utonutí	k1gNnSc1	utonutí
65-80	[number]	k4	65-80
%	%	kIx~	%
<g/>
,	,	kIx,	,
oběšení	oběšení	k1gNnSc1	oběšení
60-85	[number]	k4	60-85
%	%	kIx~	%
<g/>
,	,	kIx,	,
otrava	otrava	k1gFnSc1	otrava
výfukovými	výfukový	k2eAgInPc7d1	výfukový
plyny	plyn	k1gInPc7	plyn
40-60	[number]	k4	40-60
%	%	kIx~	%
<g/>
,	,	kIx,	,
sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
skok	skok	k1gInSc1	skok
35-60	[number]	k4	35-60
%	%	kIx~	%
<g/>
,	,	kIx,	,
sebevražedná	sebevražedný	k2eAgFnSc1d1	sebevražedná
otrava	otrava	k1gFnSc1	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
40-50	[number]	k4	40-50
%	%	kIx~	%
<g/>
,	,	kIx,	,
pesticidy	pesticid	k1gInPc1	pesticid
6-75	[number]	k4	6-75
%	%	kIx~	%
<g/>
,	,	kIx,	,
předávkování	předávkování	k1gNnSc2	předávkování
léky	lék	k1gInPc4	lék
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgInPc1d3	nejobvyklejší
způsoby	způsob	k1gInPc1	způsob
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
se	se	k3xPyFc4	se
od	od	k7c2	od
nejčastějších	častý	k2eAgInPc2d3	nejčastější
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
způsobů	způsob	k1gInPc2	způsob
liší	lišit	k5eAaImIp3nS	lišit
-	-	kIx~	-
až	až	k9	až
85	[number]	k4	85
%	%	kIx~	%
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
předávkováním	předávkování	k1gNnSc7	předávkování
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
57	[number]	k4	57
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
použití	použití	k1gNnSc2	použití
střelné	střelný	k2eAgFnSc2d1	střelná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
běžnější	běžný	k2eAgInSc1d2	běžnější
spíše	spíše	k9	spíše
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
častými	častý	k2eAgFnPc7d1	častá
metodami	metoda	k1gFnPc7	metoda
jsou	být	k5eAaImIp3nP	být
oběšení	oběšený	k2eAgMnPc1d1	oběšený
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
otrava	otrava	k1gFnSc1	otrava
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
společně	společně	k6eAd1	společně
stojí	stát	k5eAaImIp3nP	stát
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
střelnou	střelný	k2eAgFnSc4d1	střelná
zbraň	zbraň	k1gFnSc4	zbraň
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
počtu	počet	k1gInSc2	počet
sebevraždy	sebevražda	k1gFnSc2	sebevražda
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražedné	sebevražedný	k2eAgInPc1d1	sebevražedný
skoky	skok	k1gInPc1	skok
jsou	být	k5eAaImIp3nP	být
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
80	[number]	k4	80
%	%	kIx~	%
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgInSc7d3	nejčastější
způsobem	způsob	k1gInSc7	způsob
sebevraždy	sebevražda	k1gFnSc2	sebevražda
pozření	pozření	k1gNnSc2	pozření
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
stále	stále	k6eAd1	stále
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sebevraždám	sebevražda	k1gFnPc3	sebevražda
vlastnoručním	vlastnoruční	k2eAgNnSc7d1	vlastnoruční
proříznutím	proříznutí	k1gNnSc7	proříznutí
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
,	,	kIx,	,
známému	známý	k1gMnSc3	známý
rovněž	rovněž	k9	rovněž
jako	jako	k8xC	jako
seppuku	seppuk	k1gInSc2	seppuk
nebo	nebo	k8xC	nebo
harakiri	harakiri	k1gNnSc2	harakiri
<g/>
,	,	kIx,	,
nejobvyklejším	obvyklý	k2eAgInSc7d3	nejobvyklejší
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
však	však	k9	však
oběšení	oběšení	k1gNnSc1	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
naplánované	naplánovaný	k2eAgNnSc4d1	naplánované
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
nevidí	vidět	k5eNaImIp3nS	vidět
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
dál	daleko	k6eAd2	daleko
setrvávat	setrvávat	k5eAaImF	setrvávat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
dokonce	dokonce	k9	dokonce
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
detaily	detail	k1gInPc4	detail
provedení	provedení	k1gNnSc2	provedení
připravuje	připravovat	k5eAaImIp3nS	připravovat
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
velice	velice	k6eAd1	velice
pečlivě	pečlivě	k6eAd1	pečlivě
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
zálibně	zálibně	k6eAd1	zálibně
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
plánuje	plánovat	k5eAaImIp3nS	plánovat
v	v	k7c6	v
naprostém	naprostý	k2eAgNnSc6d1	naprosté
soukromí	soukromí	k1gNnSc6	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
této	tento	k3xDgFnSc3	tento
sebevraždě	sebevražda	k1gFnSc3	sebevražda
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bez	bez	k7c2	bez
odborné	odborný	k2eAgFnSc2d1	odborná
pomoci	pomoc	k1gFnSc2	pomoc
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
klidu	klid	k1gInSc6	klid
a	a	k8xC	a
dosažení	dosažení	k1gNnSc6	dosažení
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
ráje	ráj	k1gInSc2	ráj
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
přípravám	příprava	k1gFnPc3	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
podílu	podíl	k1gInSc2	podíl
viny	vina	k1gFnSc2	vina
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
okolí	okolí	k1gNnSc2	okolí
dotyčného	dotyčný	k2eAgNnSc2d1	dotyčné
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
dokáže	dokázat	k5eAaPmIp3nS	dokázat
odhadnout	odhadnout	k5eAaPmF	odhadnout
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
známky	známka	k1gFnPc4	známka
"	"	kIx"	"
<g/>
volání	volání	k1gNnSc1	volání
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vůbec	vůbec	k9	vůbec
rozpoznatelné	rozpoznatelný	k2eAgInPc1d1	rozpoznatelný
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
příprav	příprava	k1gFnPc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Bilanční	bilanční	k2eAgFnPc1d1	bilanční
sebevraždy	sebevražda	k1gFnPc1	sebevražda
často	často	k6eAd1	často
páchají	páchat	k5eAaImIp3nP	páchat
vážně	vážně	k6eAd1	vážně
nemocní	mocný	k2eNgMnPc1d1	nemocný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
náhlého	náhlý	k2eAgInSc2d1	náhlý
popudu	popud	k1gInSc2	popud
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
při	při	k7c6	při
dlouhodobých	dlouhodobý	k2eAgFnPc6d1	dlouhodobá
depresích	deprese	k1gFnPc6	deprese
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
velmi	velmi	k6eAd1	velmi
náhle	náhle	k6eAd1	náhle
<g/>
.	.	kIx.	.
</s>
<s>
Šok	šok	k1gInSc1	šok
z	z	k7c2	z
momentálního	momentální	k2eAgInSc2d1	momentální
psychického	psychický	k2eAgInSc2d1	psychický
úrazu	úraz	k1gInSc2	úraz
překryje	překrýt	k5eAaPmIp3nS	překrýt
vše	všechen	k3xTgNnSc1	všechen
ostatní	ostatní	k2eAgNnSc1d1	ostatní
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
překrýt	překrýt	k5eAaPmF	překrýt
i	i	k9	i
pud	pud	k1gInSc1	pud
sebezáchovy	sebezáchova	k1gFnSc2	sebezáchova
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
sebevraha	sebevrah	k1gMnSc2	sebevrah
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
potlačení	potlačení	k1gNnSc4	potlačení
této	tento	k3xDgFnSc2	tento
nejjednodušší	jednoduchý	k2eAgFnSc2d3	nejjednodušší
zničující	zničující	k2eAgFnSc2d1	zničující
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Obnoví	obnovit	k5eAaPmIp3nS	obnovit
se	se	k3xPyFc4	se
potřeba	potřeba	k6eAd1	potřeba
žít	žít	k5eAaImF	žít
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
strach	strach	k1gInSc4	strach
z	z	k7c2	z
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
impulzivní	impulzivní	k2eAgMnSc1d1	impulzivní
sebevrah	sebevrah	k1gMnSc1	sebevrah
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
nechá	nechat	k5eAaPmIp3nS	nechat
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
podvědomě	podvědomě	k6eAd1	podvědomě
volá	volat	k5eAaImIp3nS	volat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Předstíraná	předstíraný	k2eAgFnSc1d1	předstíraná
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
nástroj	nástroj	k1gInSc1	nástroj
citového	citový	k2eAgNnSc2d1	citové
vydírání	vydírání	k1gNnSc2	vydírání
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
obětí	oběť	k1gFnPc2	oběť
jednoznačně	jednoznačně	k6eAd1	jednoznačně
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Sebevrah	sebevrah	k1gMnSc1	sebevrah
si	se	k3xPyFc3	se
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
někdo	někdo	k3yInSc1	někdo
přišel	přijít	k5eAaPmAgMnS	přijít
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Bolest	bolest	k1gFnSc1	bolest
z	z	k7c2	z
pořezaných	pořezaný	k2eAgNnPc2d1	pořezané
zápěstí	zápěstí	k1gNnPc2	zápěstí
nebo	nebo	k8xC	nebo
předloktí	předloktí	k1gNnSc1	předloktí
vnímá	vnímat	k5eAaImIp3nS	vnímat
optikou	optika	k1gFnSc7	optika
velikosti	velikost	k1gFnSc2	velikost
trestu	trest	k1gInSc2	trest
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
zavinil	zavinit	k5eAaPmAgMnS	zavinit
-	-	kIx~	-
tedy	tedy	k9	tedy
vydírané	vydíraný	k2eAgFnPc1d1	vydíraná
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bolí	bolet	k5eAaImIp3nP	bolet
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
byl	být	k5eAaImAgMnS	být
zlý	zlý	k2eAgMnSc1d1	zlý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hrubší	hrubý	k2eAgNnSc1d2	hrubší
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
vyděračem	vyděrač	k1gMnSc7	vyděrač
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
celkem	celkem	k6eAd1	celkem
přínosné	přínosný	k2eAgNnSc1d1	přínosné
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zdravotnických	zdravotnický	k2eAgNnPc6d1	zdravotnické
zařízeních	zařízení	k1gNnPc6	zařízení
bývali	bývat	k5eAaImAgMnP	bývat
"	"	kIx"	"
<g/>
práškaři	práškař	k1gMnPc1	práškař
<g/>
"	"	kIx"	"
nuceni	nucen	k2eAgMnPc1d1	nucen
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
úklidu	úklid	k1gInSc6	úklid
po	po	k7c6	po
výplachu	výplach	k1gInSc6	výplach
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
řezné	řezný	k2eAgFnPc1d1	řezná
rány	rána	k1gFnPc1	rána
se	se	k3xPyFc4	se
zašívaly	zašívat	k5eAaImAgFnP	zašívat
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
umrtvení	umrtvení	k1gNnSc2	umrtvení
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přistoupení	přistoupení	k1gNnSc1	přistoupení
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
sebevraha	sebevrah	k1gMnSc2	sebevrah
znamená	znamenat	k5eAaImIp3nS	znamenat
jeho	jeho	k3xOp3gNnSc7	jeho
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
případy	případ	k1gInPc1	případ
(	(	kIx(	(
<g/>
neteroristických	teroristický	k2eNgInPc2d1	teroristický
<g/>
)	)	kIx)	)
aktů	akt	k1gInPc2	akt
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
politické	politický	k2eAgInPc4d1	politický
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
burcují	burcovat	k5eAaImIp3nP	burcovat
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
oslavovány	oslavován	k2eAgInPc1d1	oslavován
jako	jako	k8xS	jako
hrdinské	hrdinský	k2eAgInPc1d1	hrdinský
činy	čin	k1gInPc1	čin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
sebeupálení	sebeupálení	k1gNnSc4	sebeupálení
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Zajíce	Zajíc	k1gMnSc2	Zajíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
Československa	Československo	k1gNnSc2	Československo
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
Československa	Československo	k1gNnSc2	Československo
polský	polský	k2eAgMnSc1d1	polský
účetní	účetní	k1gMnSc1	účetní
Ryszard	Ryszard	k1gMnSc1	Ryszard
Siwiec	Siwiec	k1gMnSc1	Siwiec
<g/>
.	.	kIx.	.
</s>
<s>
Sebeupálením	sebeupálení	k1gNnPc3	sebeupálení
protestoval	protestovat	k5eAaBmAgMnS	protestovat
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
mnich	mnich	k1gInSc4	mnich
Thich	Thich	k1gMnSc1	Thich
Quang	Quang	k1gMnSc1	Quang
Duc	duc	k0	duc
v	v	k7c6	v
Saigonu	Saigon	k1gInSc2	Saigon
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
)	)	kIx)	)
a	a	k8xC	a
Norman	Norman	k1gMnSc1	Norman
Morrison	Morrison	k1gMnSc1	Morrison
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
také	také	k9	také
protestní	protestní	k2eAgFnSc1d1	protestní
hladovka	hladovka	k1gFnSc1	hladovka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
málem	málem	k6eAd1	málem
postihlo	postihnout	k5eAaPmAgNnS	postihnout
Mahátmu	Mahátma	k1gFnSc4	Mahátma
Gándhího	Gándhí	k1gMnSc2	Gándhí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c7	mezi
hinduisty	hinduista	k1gMnPc7	hinduista
a	a	k8xC	a
muslimy	muslim	k1gMnPc7	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
vlastním	vlastní	k2eAgNnSc7d1	vlastní
přičiněním	přičinění	k1gNnSc7	přičinění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgInSc2d1	předchozí
úmyslu	úmysl	k1gInSc2	úmysl
<g/>
,	,	kIx,	,
náhodou	náhoda	k1gFnSc7	náhoda
(	(	kIx(	(
<g/>
opilec	opilec	k1gMnSc1	opilec
se	se	k3xPyFc4	se
smrtelně	smrtelně	k6eAd1	smrtelně
poraní	poranit	k5eAaPmIp3nS	poranit
pádem	pád	k1gInSc7	pád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biická	Biický	k2eAgFnSc1d1	Biický
sebevražda	sebevražda	k1gFnSc1	sebevražda
-	-	kIx~	-
motiv	motiv	k1gInSc1	motiv
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
realitě	realita	k1gFnSc6	realita
<g/>
,	,	kIx,	,
když	když	k8xS	když
člověk	člověk	k1gMnSc1	člověk
např.	např.	kA	např.
kvůli	kvůli	k7c3	kvůli
dluhům	dluh	k1gInPc3	dluh
vyhodnotí	vyhodnotit	k5eAaPmIp3nS	vyhodnotit
svou	svůj	k3xOyFgFnSc4	svůj
situaci	situace	k1gFnSc4	situace
jako	jako	k8xS	jako
bezvýchodnou	bezvýchodný	k2eAgFnSc4d1	bezvýchodná
<g/>
;	;	kIx,	;
patická	patická	k1gFnSc1	patická
-	-	kIx~	-
původ	původ	k1gInSc1	původ
v	v	k7c4	v
patologii	patologie	k1gFnSc4	patologie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
schizofrenik	schizofrenik	k1gMnSc1	schizofrenik
se	se	k3xPyFc4	se
zabije	zabít	k5eAaPmIp3nS	zabít
kvůli	kvůli	k7c3	kvůli
strachu	strach	k1gInSc3	strach
z	z	k7c2	z
halucinovaných	halucinovaný	k2eAgInPc2d1	halucinovaný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
vražda	vražda	k1gFnSc1	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
sebevrah	sebevrah	k1gMnSc1	sebevrah
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
i	i	k9	i
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
svých	svůj	k3xOyFgMnPc2	svůj
blízkých	blízký	k2eAgMnPc2d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přitom	přitom	k6eAd1	přitom
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc4d2	lepší
východisko	východisko	k1gNnSc4	východisko
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
blízké	blízký	k2eAgInPc4d1	blízký
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sebevrah	sebevrah	k1gMnSc1	sebevrah
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
vrahem	vrah	k1gMnSc7	vrah
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Romana	Romana	k1gFnSc1	Romana
Zienertová	Zienertová	k1gFnSc1	Zienertová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nestihla	stihnout	k5eNaPmAgFnS	stihnout
dokonat	dokonat	k5eAaPmF	dokonat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sebevraždu	sebevražda	k1gFnSc4	sebevražda
ani	ani	k8xC	ani
depresi	deprese	k1gFnSc4	deprese
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
jednotná	jednotný	k2eAgFnSc1d1	jednotná
základní	základní	k2eAgFnSc1d1	základní
patofyziologie	patofyziologie	k1gFnSc1	patofyziologie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
souhry	souhra	k1gFnSc2	souhra
řady	řada	k1gFnSc2	řada
behaviorálních	behaviorální	k2eAgInPc2d1	behaviorální
<g/>
,	,	kIx,	,
socioenvironmentálních	socioenvironmentální	k2eAgInPc2d1	socioenvironmentální
a	a	k8xC	a
psychiatrických	psychiatrický	k2eAgInPc2d1	psychiatrický
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
hladina	hladina	k1gFnSc1	hladina
mozkového	mozkový	k2eAgInSc2d1	mozkový
neurotrofického	urotrofický	k2eNgInSc2d1	neurotrofický
faktoru	faktor	k1gInSc2	faktor
(	(	kIx(	(
<g/>
BDNF	BDNF	kA	BDNF
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
sebevraždou	sebevražda	k1gFnSc7	sebevražda
spojována	spojovat	k5eAaImNgFnS	spojovat
přímo	přímo	k6eAd1	přímo
i	i	k8xC	i
nepřímo	přímo	k6eNd1	přímo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jeho	jeho	k3xOp3gFnSc2	jeho
role	role	k1gFnSc2	role
v	v	k7c6	v
depresi	deprese	k1gFnSc6	deprese
<g/>
,	,	kIx,	,
posttraumatické	posttraumatický	k2eAgFnSc3d1	posttraumatická
stresové	stresový	k2eAgFnSc3d1	stresová
poruše	porucha	k1gFnSc3	porucha
<g/>
,	,	kIx,	,
schizofrenii	schizofrenie	k1gFnSc6	schizofrenie
a	a	k8xC	a
obsesivně	obsesivně	k6eAd1	obsesivně
kompulzivní	kompulzivní	k2eAgFnSc3d1	kompulzivní
poruše	porucha	k1gFnSc3	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgFnPc1d1	pitevní
studie	studie	k1gFnPc1	studie
zjistily	zjistit	k5eAaPmAgFnP	zjistit
sníženou	snížený	k2eAgFnSc4d1	snížená
hladinu	hladina	k1gFnSc4	hladina
BDNF	BDNF	kA	BDNF
v	v	k7c6	v
hipokampu	hipokamp	k1gInSc6	hipokamp
a	a	k8xC	a
prefrontální	prefrontální	k2eAgFnSc3d1	prefrontální
mozkové	mozkový	k2eAgFnSc3d1	mozková
kůře	kůra	k1gFnSc3	kůra
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
psychiatrickými	psychiatrický	k2eAgNnPc7d1	psychiatrické
onemocněními	onemocnění	k1gNnPc7	onemocnění
i	i	k9	i
u	u	k7c2	u
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jimi	on	k3xPp3gInPc7	on
netrpí	trpět	k5eNaImIp3nS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hladina	hladina	k1gFnSc1	hladina
mozkového	mozkový	k2eAgInSc2d1	mozkový
neurotransmiteru	neurotransmiter	k1gInSc2	neurotransmiter
serotoninu	serotonin	k1gInSc2	serotonin
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
páchajících	páchající	k2eAgFnPc2d1	páchající
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
domněnka	domněnka	k1gFnSc1	domněnka
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
prokázaně	prokázaně	k6eAd1	prokázaně
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
hladině	hladina	k1gFnSc3	hladina
5-HT2A	[number]	k4	5-HT2A
receptorů	receptor	k1gInPc2	receptor
zjištěné	zjištěný	k2eAgFnSc2d1	zjištěná
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
důkazy	důkaz	k1gInPc4	důkaz
patří	patřit	k5eAaImIp3nS	patřit
snížená	snížený	k2eAgFnSc1d1	snížená
hladina	hladina	k1gFnSc1	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
5	[number]	k4	5
<g/>
-hydroxyindoloctové	ydroxyindoloctová	k1gFnSc2	-hydroxyindoloctová
<g/>
,	,	kIx,	,
produktu	produkt	k1gInSc2	produkt
rozkladu	rozklad	k1gInSc2	rozklad
serotoninu	serotonin	k1gInSc2	serotonin
<g/>
,	,	kIx,	,
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgInPc4d1	přímý
důkazy	důkaz	k1gInPc4	důkaz
je	být	k5eAaImIp3nS	být
však	však	k9	však
obtížné	obtížný	k2eAgNnSc1d1	obtížné
shromáždit	shromáždit	k5eAaPmF	shromáždit
<g/>
.	.	kIx.	.
</s>
<s>
Epigenetika	Epigenetika	k1gFnSc1	Epigenetika
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
genetické	genetický	k2eAgFnSc2d1	genetická
exprese	exprese	k1gFnSc2	exprese
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
environmentální	environmentální	k2eAgInPc4d1	environmentální
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nemění	měnit	k5eNaImIp3nP	měnit
základní	základní	k2eAgNnPc4d1	základní
DNA	dno	k1gNnPc4	dno
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
určování	určování	k1gNnSc6	určování
rizika	riziko	k1gNnSc2	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
rovněž	rovněž	k9	rovněž
jistou	jistý	k2eAgFnSc4d1	jistá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
sebevražd	sebevražda	k1gFnPc2	sebevražda
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
kolektivní	kolektivní	k2eAgFnPc4d1	kolektivní
snahy	snaha	k1gFnPc4	snaha
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
snížit	snížit	k5eAaPmF	snížit
počet	počet	k1gInSc4	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
preventivních	preventivní	k2eAgNnPc2d1	preventivní
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
určitým	určitý	k2eAgInPc3d1	určitý
prostředkům	prostředek	k1gInPc3	prostředek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ke	k	k7c3	k
střelným	střelný	k2eAgFnPc3d1	střelná
zbraním	zbraň	k1gFnPc3	zbraň
nebo	nebo	k8xC	nebo
jedům	jed	k1gInPc3	jed
<g/>
,	,	kIx,	,
riziko	riziko	k1gNnSc1	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
opatření	opatření	k1gNnSc4	opatření
patří	patřit	k5eAaImIp3nS	patřit
omezení	omezení	k1gNnSc4	omezení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
dřevěnému	dřevěný	k2eAgNnSc3d1	dřevěné
uhlí	uhlí	k1gNnSc3	uhlí
(	(	kIx(	(
<g/>
zapříčiňující	zapříčiňující	k2eAgFnSc4d1	zapříčiňující
otravu	otrava	k1gFnSc4	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgFnPc3d1	uhelnatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
bariéry	bariéra	k1gFnPc1	bariéra
na	na	k7c6	na
mostech	most	k1gInPc6	most
a	a	k8xC	a
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Účinná	účinný	k2eAgFnSc1d1	účinná
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
léčba	léčba	k1gFnSc1	léčba
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
a	a	k8xC	a
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
deprese	deprese	k1gFnSc1	deprese
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
pokusily	pokusit	k5eAaPmAgInP	pokusit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jako	jako	k9	jako
preventivní	preventivní	k2eAgFnSc4d1	preventivní
strategii	strategie	k1gFnSc4	strategie
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
omezení	omezení	k1gNnSc4	omezení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
alkoholu	alkohol	k1gInSc3	alkohol
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
snížením	snížení	k1gNnSc7	snížení
počtu	počet	k1gInSc2	počet
barů	bar	k1gInPc2	bar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
krizové	krizový	k2eAgFnPc4d1	krizová
linky	linka	k1gFnPc4	linka
běžné	běžný	k2eAgFnPc4d1	běžná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
či	či	k8xC	či
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
jejich	jejich	k3xOp3gFnSc2	jejich
efektivity	efektivita	k1gFnSc2	efektivita
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
málo	málo	k4c1	málo
důkazů	důkaz	k1gInPc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
adolescentů	adolescent	k1gMnPc2	adolescent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
přemýšleli	přemýšlet	k5eAaImAgMnP	přemýšlet
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc4	výsledek
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
kognitivně	kognitivně	k6eAd1	kognitivně
behaviorální	behaviorální	k2eAgFnSc1d1	behaviorální
terapie	terapie	k1gFnSc1	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvoj	rozvoj	k1gInSc1	rozvoj
může	moct	k5eAaImIp3nS	moct
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
vlivu	vliv	k1gInSc3	vliv
na	na	k7c6	na
šíření	šíření	k1gNnSc6	šíření
chudoby	chudoba	k1gFnSc2	chudoba
počet	počet	k1gInSc4	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
také	také	k6eAd1	také
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
frekvence	frekvence	k1gFnSc2	frekvence
společenských	společenský	k2eAgInPc2d1	společenský
styků	styk	k1gInPc2	styk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
účinné	účinný	k2eAgInPc1d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dopadu	dopad	k1gInSc6	dopad
soustavného	soustavný	k2eAgNnSc2d1	soustavné
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
na	na	k7c4	na
výsledný	výsledný	k2eAgInSc4d1	výsledný
počet	počet	k1gInSc4	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
minimum	minimum	k1gNnSc1	minimum
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
testu	test	k1gInSc2	test
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
existovalo	existovat	k5eAaImAgNnS	existovat
riziko	riziko	k1gNnSc1	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
panují	panovat	k5eAaImIp3nP	panovat
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
zvýšit	zvýšit	k5eAaPmF	zvýšit
vytížení	vytížení	k1gNnSc4	vytížení
orgánů	orgán	k1gInPc2	orgán
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Posouzení	posouzení	k1gNnSc1	posouzení
stavu	stav	k1gInSc2	stav
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dotazy	dotaz	k1gInPc4	dotaz
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
sebevražednosti	sebevražednost	k1gFnSc2	sebevražednost
samotné	samotný	k2eAgNnSc4d1	samotné
riziko	riziko	k1gNnSc4	riziko
nijak	nijak	k6eAd1	nijak
nezvyšují	zvyšovat	k5eNaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
s	s	k7c7	s
duševním	duševní	k2eAgNnSc7d1	duševní
zdravím	zdraví	k1gNnSc7	zdraví
může	moct	k5eAaImIp3nS	moct
riziko	riziko	k1gNnSc4	riziko
sebevraždy	sebevražda	k1gFnSc2	sebevražda
snížit	snížit	k5eAaPmF	snížit
řada	řada	k1gFnSc1	řada
léčebných	léčebný	k2eAgInPc2d1	léčebný
postupů	postup	k1gInPc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
sebevražedné	sebevražedný	k2eAgFnPc1d1	sebevražedná
osoby	osoba	k1gFnPc1	osoba
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přijaty	přijmout	k5eAaPmNgFnP	přijmout
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
péče	péče	k1gFnSc2	péče
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dobrovolného	dobrovolný	k2eAgNnSc2d1	dobrovolné
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
či	či	k8xC	či
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnPc1d1	osobní
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
by	by	kYmCp3nS	by
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
mohl	moct	k5eAaImAgMnS	moct
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
sebepoškození	sebepoškození	k1gNnSc3	sebepoškození
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
zabaveny	zabaven	k2eAgFnPc1d1	zabavena
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kliničtí	klinický	k2eAgMnPc1d1	klinický
lékaři	lékař	k1gMnPc1	lékař
dávají	dávat	k5eAaImIp3nP	dávat
svým	svůj	k3xOyFgMnPc3	svůj
pacientům	pacient	k1gMnPc3	pacient
k	k	k7c3	k
podpisu	podpis	k1gInSc6	podpis
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
prevenci	prevence	k1gFnSc4	prevence
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
pacienti	pacient	k1gMnPc1	pacient
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
propuštění	propuštění	k1gNnSc2	propuštění
neublíží	ublížit	k5eNaPmIp3nP	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
však	však	k9	však
žádný	žádný	k3yNgInSc4	žádný
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
této	tento	k3xDgFnSc2	tento
praxe	praxe	k1gFnSc2	praxe
nedokládají	dokládat	k5eNaImIp3nP	dokládat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
osoby	osoba	k1gFnSc2	osoba
riziko	riziko	k1gNnSc1	riziko
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
ambulantní	ambulantní	k2eAgFnSc1d1	ambulantní
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
hraniční	hraniční	k2eAgFnSc7d1	hraniční
poruchou	porucha	k1gFnSc7	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
s	s	k7c7	s
chronickými	chronický	k2eAgInPc7d1	chronický
sebevražednými	sebevražedný	k2eAgInPc7d1	sebevražedný
sklony	sklon	k1gInPc7	sklon
se	se	k3xPyFc4	se
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
neukázala	ukázat	k5eNaPmAgFnS	ukázat
být	být	k5eAaImF	být
účinnější	účinný	k2eAgFnSc1d2	účinnější
než	než	k8xS	než
komunitní	komunitní	k2eAgFnSc1d1	komunitní
péče	péče	k1gFnSc1	péče
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zlepšení	zlepšení	k1gNnSc2	zlepšení
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
předběžné	předběžný	k2eAgInPc4d1	předběžný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
dialektická	dialektický	k2eAgFnSc1d1	dialektická
behaviorální	behaviorální	k2eAgFnSc1d1	behaviorální
terapie	terapie	k1gFnSc1	terapie
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
sebevražednost	sebevražednost	k1gFnSc4	sebevražednost
u	u	k7c2	u
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
i	i	k9	i
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
hraniční	hraniční	k2eAgFnSc7d1	hraniční
poruchou	porucha	k1gFnSc7	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
důkazy	důkaz	k1gInPc1	důkaz
pro	pro	k7c4	pro
úbytek	úbytek	k1gInSc4	úbytek
dokonaných	dokonaný	k2eAgFnPc2d1	dokonaná
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
výhodám	výhoda	k1gFnPc3	výhoda
a	a	k8xC	a
škodlivosti	škodlivost	k1gFnPc1	škodlivost
antidepresiv	antidepresivum	k1gNnPc2	antidepresivum
panují	panovat	k5eAaImIp3nP	panovat
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
mladších	mladý	k2eAgFnPc2d2	mladší
osob	osoba	k1gFnPc2	osoba
novější	nový	k2eAgMnPc1d2	novější
antidepresiva	antidepresivum	k1gNnSc2	antidepresivum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
SSRI	SSRI	kA	SSRI
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
sebevražednosti	sebevražednost	k1gFnSc2	sebevražednost
z	z	k7c2	z
25	[number]	k4	25
na	na	k7c4	na
40	[number]	k4	40
promile	promile	k1gNnPc2	promile
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
toto	tento	k3xDgNnSc4	tento
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
účinné	účinný	k2eAgInPc1d1	účinný
při	při	k7c6	při
snižování	snižování	k1gNnSc6	snižování
rizika	riziko	k1gNnSc2	riziko
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
bipolární	bipolární	k2eAgFnSc7d1	bipolární
poruchou	porucha	k1gFnSc7	porucha
a	a	k8xC	a
unipolární	unipolární	k2eAgFnSc7d1	unipolární
depresí	deprese	k1gFnSc7	deprese
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
úroveň	úroveň	k1gFnSc4	úroveň
jako	jako	k9	jako
u	u	k7c2	u
obecné	obecný	k2eAgFnSc2d1	obecná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
neskončí	skončit	k5eNaPmIp3nS	skončit
smrtí	smrt	k1gFnSc7	smrt
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
psychiatrické	psychiatrický	k2eAgNnSc4d1	psychiatrické
vyšetření	vyšetření	k1gNnSc4	vyšetření
a	a	k8xC	a
případně	případně	k6eAd1	případně
následná	následný	k2eAgFnSc1d1	následná
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
neopakoval	opakovat	k5eNaImAgMnS	opakovat
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
najít	najít	k5eAaPmF	najít
příčinu	příčina	k1gFnSc4	příčina
pokusu	pokus	k1gInSc2	pokus
a	a	k8xC	a
tu	ten	k3xDgFnSc4	ten
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Sebevraždou	sebevražda	k1gFnSc7	sebevražda
ukončí	ukončit	k5eAaPmIp3nS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
přibližně	přibližně	k6eAd1	přibližně
0,5	[number]	k4	0,5
%	%	kIx~	%
až	až	k9	až
1,4	[number]	k4	1,4
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
globálně	globálně	k6eAd1	globálně
desátou	desátá	k1gFnSc7	desátá
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ročně	ročně	k6eAd1	ročně
takto	takto	k6eAd1	takto
zemře	zemřít	k5eAaPmIp3nS	zemřít
zhruba	zhruba	k6eAd1	zhruba
800	[number]	k4	800
000	[number]	k4	000
až	až	k6eAd1	až
milión	milión	k4xCgInSc4	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
11,6	[number]	k4	11,6
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
o	o	k7c4	o
60	[number]	k4	60
%	%	kIx~	%
<g/>
;	;	kIx,	;
výrazné	výrazný	k2eAgNnSc1d1	výrazné
zvýšení	zvýšení	k1gNnSc1	zvýšení
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
zejména	zejména	k9	zejména
v	v	k7c6	v
rozvojovém	rozvojový	k2eAgInSc6d1	rozvojový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
sebevraždu	sebevražda	k1gFnSc4	sebevražda
končící	končící	k2eAgFnSc7d1	končící
smrtí	smrt	k1gFnSc7	smrt
připadá	připadat	k5eAaImIp3nS	připadat
10	[number]	k4	10
až	až	k9	až
40	[number]	k4	40
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
časovými	časový	k2eAgNnPc7d1	časové
obdobími	období	k1gNnPc7	období
existují	existovat	k5eAaImIp3nP	existovat
velké	velký	k2eAgInPc4d1	velký
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tvořily	tvořit	k5eAaImAgFnP	tvořit
sebevraždy	sebevražda	k1gFnPc1	sebevražda
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
0,5	[number]	k4	0,5
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
1,9	[number]	k4	1,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
1,2	[number]	k4	1,2
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
1,4	[number]	k4	1,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
sebevraždou	sebevražda	k1gFnSc7	sebevražda
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
8,6	[number]	k4	8,6
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
11,1	[number]	k4	11,1
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
12,7	[number]	k4	12,7
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
23,2	[number]	k4	23,2
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
7,6	[number]	k4	7,6
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
11,4	[number]	k4	11,4
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
desátou	desátá	k1gFnSc7	desátá
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přibližně	přibližně	k6eAd1	přibližně
36	[number]	k4	36
000	[number]	k4	000
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
650	[number]	k4	650
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
přijato	přijmout	k5eAaPmNgNnS	přijmout
na	na	k7c4	na
pohotovostní	pohotovostní	k2eAgNnSc4d1	pohotovostní
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
poměrný	poměrný	k2eAgInSc1d1	poměrný
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Zeměmi	zem	k1gFnPc7	zem
s	s	k7c7	s
absolutně	absolutně	k6eAd1	absolutně
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
jsou	být	k5eAaImIp3nP	být
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovině	polovina	k1gFnSc3	polovina
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
dokonce	dokonce	k9	dokonce
pátou	pátá	k1gFnSc4	pátá
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
umírá	umírat	k5eAaImIp3nS	umírat
sebevraždou	sebevražda	k1gFnSc7	sebevražda
třikrát	třikrát	k6eAd1	třikrát
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
více	hodně	k6eAd2	hodně
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
muži	muž	k1gMnPc1	muž
užívají	užívat	k5eAaImIp3nP	užívat
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
života	život	k1gInSc2	život
podstatně	podstatně	k6eAd1	podstatně
nebezpečnější	bezpečný	k2eNgInPc4d2	nebezpečnější
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
výraznější	výrazný	k2eAgMnSc1d2	výraznější
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
starších	starší	k1gMnPc2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věkové	věkový	k2eAgFnSc6d1	věková
kategorii	kategorie	k1gFnSc6	kategorie
páchá	páchat	k5eAaImIp3nS	páchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
desetkrát	desetkrát	k6eAd1	desetkrát
více	hodně	k6eAd2	hodně
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Čínu	Čína	k1gFnSc4	Čína
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
počtů	počet	k1gInPc2	počet
ženských	ženský	k2eAgFnPc2d1	ženská
sebevražd	sebevražda	k1gFnPc2	sebevražda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
současně	současně	k6eAd1	současně
také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
častější	častý	k2eAgFnSc2d2	častější
než	než	k8xS	než
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
0,9	[number]	k4	0,9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
téměř	téměř	k6eAd1	téměř
vyrovnán	vyrovnat	k5eAaPmNgInS	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
-	-	kIx~	-
22	[number]	k4	22
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
rovněž	rovněž	k6eAd1	rovněž
vysoké	vysoký	k2eAgInPc4d1	vysoký
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
západním	západní	k2eAgNnSc6d1	západní
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
statistického	statistický	k2eAgNnSc2d1	statistické
zpracování	zpracování	k1gNnSc2	zpracování
a	a	k8xC	a
interpretace	interpretace	k1gFnSc2	interpretace
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
míra	míra	k1gFnSc1	míra
sebevražednosti	sebevražednost	k1gFnSc2	sebevražednost
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
nebo	nebo	k8xC	nebo
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgInSc1d1	absolutní
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
mezi	mezi	k7c7	mezi
15	[number]	k4	15
a	a	k8xC	a
29	[number]	k4	29
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
věková	věkový	k2eAgFnSc1d1	věková
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jsou	být	k5eAaImIp3nP	být
údaje	údaj	k1gInPc1	údaj
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
europoidní	europoidní	k2eAgFnSc2d1	europoidní
rasy	rasa	k1gFnSc2	rasa
starších	starý	k2eAgFnPc2d2	starší
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
u	u	k7c2	u
adolescentů	adolescent	k1gMnPc2	adolescent
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c4	po
úmrtí	úmrtí	k1gNnSc4	úmrtí
na	na	k7c4	na
následky	následek	k1gInPc4	následek
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
rozvinutém	rozvinutý	k2eAgInSc6d1	rozvinutý
světě	svět	k1gInSc6	svět
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojovém	rozvojový	k2eAgInSc6d1	rozvojový
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
však	však	k9	však
nižší	nízký	k2eAgNnSc4d2	nižší
procento	procento	k1gNnSc4	procento
všech	všecek	k3xTgFnPc2	všecek
úmrtí	úmrť	k1gFnPc2	úmrť
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyššího	vysoký	k2eAgInSc2d2	vyšší
počtu	počet	k1gInSc2	počet
úmrtí	úmrť	k1gFnPc2	úmrť
zaviněných	zaviněný	k2eAgFnPc2d1	zaviněná
jinými	jiný	k2eAgInPc7d1	jiný
úrazy	úraz	k1gInPc7	úraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
světa	svět	k1gInSc2	svět
k	k	k7c3	k
sebevraždám	sebevražda	k1gFnPc3	sebevražda
mladých	mladý	k2eAgFnPc2d1	mladá
žen	žena	k1gFnPc2	žena
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
než	než	k8xS	než
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
starších	starý	k2eAgFnPc2d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
nepřirozeného	přirozený	k2eNgNnSc2d1	nepřirozené
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zaznamenáno	zaznamenán	k2eAgNnSc4d1	zaznamenáno
8	[number]	k4	8
023	[number]	k4	023
sebevražd	sebevražda	k1gFnPc2	sebevražda
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
7	[number]	k4	7
010	[number]	k4	010
osob	osoba	k1gFnPc2	osoba
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tvoří	tvořit	k5eAaImIp3nS	tvořit
1,48	[number]	k4	1,48
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
23,3	[number]	k4	23,3
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc6	úmrtí
vnějšími	vnější	k2eAgFnPc7d1	vnější
příčinami	příčina	k1gFnPc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dobrovolně	dobrovolně	k6eAd1	dobrovolně
odešlo	odejít	k5eAaPmAgNnS	odejít
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
375	[number]	k4	375
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
647	[number]	k4	647
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
1	[number]	k4	1
740	[number]	k4	740
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
(	(	kIx(	(
<g/>
80	[number]	k4	80
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
sebevraždu	sebevražda	k1gFnSc4	sebevražda
páchají	páchat	k5eAaImIp3nP	páchat
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zabilo	zabít	k5eAaPmAgNnS	zabít
277	[number]	k4	277
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
1	[number]	k4	1
370	[number]	k4	370
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
332	[number]	k4	332
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
1	[number]	k4	1
408	[number]	k4	408
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
způsoben	způsoben	k2eAgInSc1d1	způsoben
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
muži	muž	k1gMnPc1	muž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
důslednější	důsledný	k2eAgMnSc1d2	důslednější
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
k	k	k7c3	k
takovému	takový	k3xDgInSc3	takový
kroku	krok	k1gInSc3	krok
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
čin	čin	k1gInSc4	čin
skutečně	skutečně	k6eAd1	skutečně
vykonají	vykonat	k5eAaPmIp3nP	vykonat
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
přitom	přitom	k6eAd1	přitom
střelnou	střelný	k2eAgFnSc4d1	střelná
zbraň	zbraň	k1gFnSc4	zbraň
nebo	nebo	k8xC	nebo
oběšení	oběšení	k1gNnSc4	oběšení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
spíše	spíše	k9	spíše
k	k	k7c3	k
předávkování	předávkování	k1gNnSc4	předávkování
léky	lék	k1gInPc4	lék
a	a	k8xC	a
lékařům	lékař	k1gMnPc3	lékař
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
je	on	k3xPp3gNnSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sebevraždy	sebevražda	k1gFnPc4	sebevražda
demonstrativní	demonstrativní	k2eAgInSc1d1	demonstrativní
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
ženy	žena	k1gFnSc2	žena
takové	takový	k3xDgInPc1	takový
pokusy	pokus	k1gInPc1	pokus
dělají	dělat	k5eAaImIp3nP	dělat
4	[number]	k4	4
<g/>
×	×	k?	×
častěji	často	k6eAd2	často
než	než	k8xS	než
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
činu	čin	k1gInSc3	čin
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
lidé	člověk	k1gMnPc1	člověk
mezi	mezi	k7c7	mezi
45	[number]	k4	45
a	a	k8xC	a
50	[number]	k4	50
lety	let	k1gInPc7	let
<g/>
,	,	kIx,	,
nejčastějším	častý	k2eAgInSc7d3	nejčastější
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
oběšení	oběšení	k1gNnSc1	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
je	být	k5eAaImIp3nS	být
30-90	[number]	k4	30-90
%	%	kIx~	%
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
postoji	postoj	k1gInSc6	postoj
autora	autor	k1gMnSc2	autor
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
)	)	kIx)	)
sebevražd	sebevražda	k1gFnPc2	sebevražda
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
psychickými	psychický	k2eAgFnPc7d1	psychická
poruchami	porucha	k1gFnPc7	porucha
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
s	s	k7c7	s
depresemi	deprese	k1gFnPc7	deprese
<g/>
,	,	kIx,	,
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
a	a	k8xC	a
poruchami	porucha	k1gFnPc7	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
(	(	kIx(	(
<g/>
nejvíce	hodně	k6eAd3	hodně
s	s	k7c7	s
emočně	emočně	k6eAd1	emočně
nestabilní	stabilní	k2eNgFnSc7d1	nestabilní
a	a	k8xC	a
antisociální	antisociální	k2eAgFnSc7d1	antisociální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
<g/>
)	)	kIx)	)
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
snižoval	snižovat	k5eAaImAgInS	snižovat
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
přechodnému	přechodný	k2eAgNnSc3d1	přechodné
zvyšování	zvyšování	k1gNnSc3	zvyšování
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
také	také	k6eAd1	také
kvalitnějšími	kvalitní	k2eAgNnPc7d2	kvalitnější
antidepresivy	antidepresivum	k1gNnPc7	antidepresivum
a	a	k8xC	a
kvalitnější	kvalitní	k2eAgFnSc7d2	kvalitnější
psychiatrickou	psychiatrický	k2eAgFnSc7d1	psychiatrická
péčí	péče	k1gFnSc7	péče
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
nárůsty	nárůst	k1gInPc1	nárůst
počtu	počet	k1gInSc2	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
jsou	být	k5eAaImIp3nP	být
svázány	svázán	k2eAgInPc1d1	svázán
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
zásadních	zásadní	k2eAgFnPc2d1	zásadní
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k6eAd1	již
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
třicátá	třicátý	k4xOgNnPc4	třicátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přelom	přelom	k1gInSc1	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
maximum	maximum	k1gNnSc1	maximum
patří	patřit	k5eAaImIp3nS	patřit
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
s	s	k7c7	s
2	[number]	k4	2
824	[number]	k4	824
zemřelými	zemřelý	k2eAgFnPc7d1	zemřelá
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
sebevraždy	sebevražda	k1gFnPc4	sebevražda
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
35	[number]	k4	35
případů	případ	k1gInPc2	případ
(	(	kIx(	(
<g/>
věková	věkový	k2eAgFnSc1d1	věková
skupina	skupina	k1gFnSc1	skupina
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
sebevraždy	sebevražda	k1gFnSc2	sebevražda
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
špatná	špatný	k2eAgFnSc1d1	špatná
známka	známka	k1gFnSc1	známka
na	na	k7c4	na
vysvědčení	vysvědčení	k1gNnSc4	vysvědčení
a	a	k8xC	a
strach	strach	k1gInSc4	strach
z	z	k7c2	z
reakce	reakce	k1gFnSc2	reakce
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Častým	častý	k2eAgInSc7d1	častý
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
také	také	k9	také
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
Aténách	Atény	k1gFnPc6	Atény
nebyl	být	k5eNaImAgInS	být
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
bez	bez	k7c2	bez
svolení	svolení	k1gNnSc2	svolení
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
umožněn	umožnit	k5eAaPmNgInS	umožnit
normální	normální	k2eAgInSc1d1	normální
pohřeb	pohřeb	k1gInSc1	pohřeb
s	s	k7c7	s
příslušnými	příslušný	k2eAgFnPc7d1	příslušná
poctami	pocta	k1gFnPc7	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgMnSc1	takový
člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
osamoceně	osamoceně	k6eAd1	osamoceně
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
náhrobní	náhrobní	k2eAgInSc4d1	náhrobní
kámen	kámen	k1gInSc4	kámen
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc4d1	jiné
označení	označení	k1gNnSc4	označení
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
se	se	k3xPyFc4	se
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
porážkou	porážka	k1gFnSc7	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
odchodu	odchod	k1gInSc2	odchod
ze	z	k7c2	z
života	život	k1gInSc2	život
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
zpočátku	zpočátku	k6eAd1	zpočátku
povolen	povolit	k5eAaPmNgInS	povolit
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
obdobích	období	k1gNnPc6	období
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
proti	proti	k7c3	proti
státu	stát	k1gInSc3	stát
kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
stát	stát	k1gInSc1	stát
musel	muset	k5eAaImAgInS	muset
hradit	hradit	k5eAaImF	hradit
<g/>
.	.	kIx.	.
</s>
<s>
Trestní	trestní	k2eAgInSc1d1	trestní
výnos	výnos	k1gInSc1	výnos
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
zakotvoval	zakotvovat	k5eAaImAgInS	zakotvovat
mnohem	mnohem	k6eAd1	mnohem
přísnější	přísný	k2eAgNnSc4d2	přísnější
potrestání	potrestání	k1gNnSc4	potrestání
-	-	kIx~	-
tělo	tělo	k1gNnSc4	tělo
mrtvého	mrtvý	k1gMnSc2	mrtvý
bylo	být	k5eAaImAgNnS	být
vláčeno	vláčet	k5eAaImNgNnS	vláčet
ulicemi	ulice	k1gFnPc7	ulice
obličejem	obličej	k1gInSc7	obličej
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
pak	pak	k6eAd1	pak
oběšeno	oběšen	k2eAgNnSc1d1	oběšeno
či	či	k8xC	či
pohozeno	pohozen	k2eAgNnSc1d1	pohozeno
na	na	k7c4	na
smetiště	smetiště	k1gNnSc4	smetiště
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgMnS	být
zkonfiskován	zkonfiskován	k2eAgInSc4d1	zkonfiskován
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
exkomunikováni	exkomunikován	k2eAgMnPc1d1	exkomunikován
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sebevraždou	sebevražda	k1gFnSc7	sebevražda
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
mimo	mimo	k7c4	mimo
posvěcené	posvěcený	k2eAgInPc4d1	posvěcený
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c2	za
obdobu	obdoba	k1gFnSc4	obdoba
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
potrestán	potrestat	k5eAaPmNgInS	potrestat
i	i	k9	i
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
změnil	změnit	k5eAaPmAgInS	změnit
-	-	kIx~	-
sebevražda	sebevražda	k1gFnSc1	sebevražda
už	už	k6eAd1	už
nebyla	být	k5eNaImAgFnS	být
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k9	jako
skutek	skutek	k1gInSc1	skutek
zaviněný	zaviněný	k2eAgInSc1d1	zaviněný
hříchem	hřích	k1gInSc7	hřích
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
důsledek	důsledek	k1gInSc4	důsledek
nepříčetnosti	nepříčetnost	k1gFnSc2	nepříčetnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
do	do	k7c2	do
přinejmenším	přinejmenším	k6eAd1	přinejmenším
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tomu	ten	k3xDgMnSc3	ten
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
islámských	islámský	k2eAgInPc2d1	islámský
států	stát	k1gInPc2	stát
ji	on	k3xPp3gFnSc4	on
stále	stále	k6eAd1	stále
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
sebevražda	sebevražda	k1gFnSc1	sebevražda
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
však	však	k9	však
je	být	k5eAaImIp3nS	být
radit	radit	k5eAaImF	radit
<g/>
,	,	kIx,	,
podněcovat	podněcovat	k5eAaImF	podněcovat
nebo	nebo	k8xC	nebo
podporovat	podporovat	k5eAaImF	podporovat
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
napomáhat	napomáhat	k5eAaBmF	napomáhat
druhému	druhý	k4xOgMnSc3	druhý
v	v	k7c6	v
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
výslovně	výslovně	k6eAd1	výslovně
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
každé	každý	k3xTgFnSc3	každý
osobě	osoba	k1gFnSc3	osoba
použít	použít	k5eAaPmF	použít
"	"	kIx"	"
<g/>
takovou	takový	k3xDgFnSc4	takový
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
spáchání	spáchání	k1gNnSc1	spáchání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
jinému	jiný	k1gMnSc3	jiný
člověku	člověk	k1gMnSc3	člověk
znemožněno	znemožněn	k2eAgNnSc1d1	znemožněno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Teritoriu	teritorium	k1gNnSc6	teritorium
Austrálie	Austrálie	k1gFnSc1	Austrálie
byla	být	k5eAaImAgFnS	být
lékařem	lékař	k1gMnSc7	lékař
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
krátce	krátce	k6eAd1	krátce
legální	legální	k2eAgFnSc1d1	legální
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
až	až	k9	až
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nepovažuje	považovat	k5eNaImIp3nS	považovat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nebo	nebo	k8xC	nebo
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
za	za	k7c4	za
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
Wales	Wales	k1gInSc4	Wales
dekriminalizovaly	dekriminalizovat	k5eAaPmAgInP	dekriminalizovat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
vydáním	vydání	k1gNnSc7	vydání
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
následovala	následovat	k5eAaImAgFnS	následovat
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
spáchat	spáchat	k5eAaPmF	spáchat
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nelegálností	nelegálnost	k1gFnSc7	nelegálnost
tohoto	tento	k3xDgInSc2	tento
aktu	akt	k1gInSc2	akt
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
organizace	organizace	k1gFnPc1	organizace
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
postavily	postavit	k5eAaPmAgFnP	postavit
kvůli	kvůli	k7c3	kvůli
negativnímu	negativní	k2eAgInSc3d1	negativní
významu	význam	k1gInSc3	význam
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
stále	stále	k6eAd1	stále
nelegální	legální	k2eNgFnSc1d1	nelegální
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
zesnulého	zesnulý	k1gMnSc2	zesnulý
může	moct	k5eAaImIp3nS	moct
čelit	čelit	k5eAaImF	čelit
právním	právní	k2eAgFnPc3d1	právní
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
protiprávní	protiprávní	k2eAgFnSc1d1	protiprávní
aktivní	aktivní	k2eAgFnSc1d1	aktivní
eutanazie	eutanazie	k1gFnSc1	eutanazie
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
sebevražednému	sebevražedný	k2eAgInSc3d1	sebevražedný
pokusu	pokus	k1gInSc3	pokus
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stíhán	stíhat	k5eAaImNgMnS	stíhat
za	za	k7c4	za
neposkytnutí	neposkytnutí	k1gNnSc4	neposkytnutí
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
naléhavém	naléhavý	k2eAgInSc6d1	naléhavý
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
zcela	zcela	k6eAd1	zcela
dekriminalizovalo	dekriminalizovat	k5eAaPmAgNnS	dekriminalizovat
asistovanou	asistovaný	k2eAgFnSc4d1	asistovaná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
článkem	článek	k1gInSc7	článek
115	[number]	k4	115
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
vždy	vždy	k6eAd1	vždy
legální	legální	k2eAgFnPc1d1	legální
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tak	tak	k6eAd1	tak
pomáhající	pomáhající	k2eAgNnSc1d1	pomáhající
koná	konat	k5eAaImIp3nS	konat
ze	z	k7c2	z
zištných	zištný	k2eAgInPc2d1	zištný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
pomoc	pomoc	k1gFnSc1	pomoc
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
legální	legální	k2eAgFnSc1d1	legální
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
motivována	motivovat	k5eAaBmNgFnS	motivovat
osobním	osobní	k2eAgInSc7d1	osobní
prospěchem	prospěch	k1gInSc7	prospěch
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
dotčeného	dotčený	k2eAgMnSc2d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgInS	vydat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
bylo	být	k5eAaImAgNnS	být
anonymnímu	anonymní	k2eAgMnSc3d1	anonymní
nemocnému	nemocný	k1gMnSc3	nemocný
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
trpícímu	trpící	k2eAgInSc3d1	trpící
psychiatrickými	psychiatrický	k2eAgFnPc7d1	psychiatrická
potížemi	potíž	k1gFnPc7	potíž
uděleno	udělen	k2eAgNnSc1d1	uděleno
právo	právo	k1gNnSc4	právo
ukončit	ukončit	k5eAaPmF	ukončit
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
není	být	k5eNaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
protiprávní	protiprávní	k2eAgFnSc1d1	protiprávní
<g/>
,	,	kIx,	,
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
pokusí	pokusit	k5eAaPmIp3nP	pokusit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uvaleny	uvalen	k2eAgFnPc4d1	uvalena
sankce	sankce	k1gFnPc4	sankce
<g/>
.	.	kIx.	.
</s>
<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
legalizována	legalizovat	k5eAaBmNgFnS	legalizovat
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Oregon	Oregon	k1gInSc1	Oregon
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
Vermont	Vermont	k1gInSc1	Vermont
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
dekriminalizována	dekriminalizován	k2eAgFnSc1d1	dekriminalizována
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Montana	Montana	k1gFnSc1	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
rovněž	rovněž	k9	rovněž
legální	legální	k2eAgFnSc1d1	legální
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
(	(	kIx(	(
<g/>
eutanázie	eutanázie	k1gFnSc1	eutanázie
legalizována	legalizován	k2eAgFnSc1d1	legalizována
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lékařem	lékař	k1gMnSc7	lékař
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
rezidenty	rezident	k1gMnPc4	rezident
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zákon	zákon	k1gInSc1	zákon
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
žadatel	žadatel	k1gMnSc1	žadatel
o	o	k7c6	o
asistenci	asistence	k1gFnSc6	asistence
měli	mít	k5eAaImAgMnP	mít
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
historii	historie	k1gFnSc4	historie
medicínského	medicínský	k2eAgInSc2d1	medicínský
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
legalizováno	legalizován	k2eAgNnSc4d1	legalizováno
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
eutanázií	eutanázie	k1gFnSc7	eutanázie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
(	(	kIx(	(
<g/>
legální	legální	k2eAgFnPc1d1	legální
pouze	pouze	k6eAd1	pouze
asistence	asistence	k1gFnPc4	asistence
lékařů	lékař	k1gMnPc2	lékař
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
legalizováno	legalizovat	k5eAaBmNgNnS	legalizovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
eutanázií	eutanázie	k1gFnSc7	eutanázie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
forem	forma	k1gFnPc2	forma
křesťanství	křesťanství	k1gNnSc2	křesťanství
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vychází	vycházet	k5eAaImIp3nS	vycházet
zejména	zejména	k9	zejména
ze	z	k7c2	z
spisů	spis	k1gInPc2	spis
vlivných	vlivný	k2eAgMnPc2d1	vlivný
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
myslitelů	myslitel	k1gMnPc2	myslitel
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
svatý	svatý	k2eAgMnSc1d1	svatý
Augustin	Augustin	k1gMnSc1	Augustin
a	a	k8xC	a
svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
byzantského	byzantský	k2eAgInSc2d1	byzantský
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
Justiniánova	Justiniánův	k2eAgInSc2d1	Justiniánův
zákoníku	zákoník	k1gInSc2	zákoník
však	však	k9	však
sebevražda	sebevražda	k1gFnSc1	sebevražda
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zakládá	zakládat	k5eAaImIp3nS	zakládat
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
na	na	k7c4	na
přikázání	přikázání	k1gNnSc4	přikázání
"	"	kIx"	"
<g/>
Nezabiješ	zabít	k5eNaPmIp2nS	zabít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stanoveného	stanovený	k2eAgInSc2d1	stanovený
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Nové	Nové	k2eAgFnSc2d1	Nové
smlouvy	smlouva	k1gFnSc2	smlouva
Ježíšem	Ježíš	k1gMnSc7	Ježíš
v	v	k7c6	v
evangeliu	evangelium	k1gNnSc6	evangelium
svatého	svatý	k2eAgMnSc2d1	svatý
Matouše	Matouš	k1gMnSc2	Matouš
19,18	[number]	k4	19,18
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
dar	dar	k1gInSc4	dar
daný	daný	k2eAgInSc4d1	daný
nám	my	k3xPp1nPc3	my
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
zavrhován	zavrhovat	k5eAaImNgMnS	zavrhovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
přirozenému	přirozený	k2eAgInSc3d1	přirozený
řádu	řád	k1gInSc3	řád
<g/>
"	"	kIx"	"
a	a	k8xC	a
tedy	tedy	k9	tedy
proti	proti	k7c3	proti
Božímu	boží	k2eAgInSc3d1	boží
plánu	plán	k1gInSc3	plán
se	s	k7c7	s
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychické	psychický	k2eAgNnSc1d1	psychické
onemocnění	onemocnění	k1gNnSc1	onemocnění
nebo	nebo	k8xC	nebo
velký	velký	k2eAgInSc1d1	velký
strach	strach	k1gInSc1	strach
z	z	k7c2	z
utrpení	utrpení	k1gNnSc2	utrpení
snižují	snižovat	k5eAaImIp3nP	snižovat
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
sebevraždu	sebevražda	k1gFnSc4	sebevražda
spáchat	spáchat	k5eAaPmF	spáchat
<g/>
.	.	kIx.	.
</s>
<s>
Argumenty	argument	k1gInPc1	argument
proti	proti	k7c3	proti
sebevraždě	sebevražda	k1gFnSc3	sebevražda
pak	pak	k6eAd1	pak
uvádějí	uvádět	k5eAaImIp3nP	uvádět
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
následující	následující	k2eAgFnPc1d1	následující
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
:	:	kIx,	:
Páté	pátá	k1gFnPc1	pátá
přikázání	přikázání	k1gNnPc2	přikázání
lze	lze	k6eAd1	lze
přesněji	přesně	k6eAd2	přesně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Nezavraždíš	zavraždit	k5eNaPmIp2nS	zavraždit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
vztahovat	vztahovat	k5eAaImF	vztahovat
na	na	k7c4	na
jedince	jedinec	k1gMnPc4	jedinec
uvažujícího	uvažující	k2eAgInSc2d1	uvažující
o	o	k7c6	o
zabití	zabití	k1gNnSc6	zabití
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
<g/>
;	;	kIx,	;
Bůh	bůh	k1gMnSc1	bůh
dal	dát	k5eAaPmAgMnS	dát
lidem	člověk	k1gMnPc3	člověk
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
vůli	vůle	k1gFnSc4	vůle
<g/>
;	;	kIx,	;
ukončení	ukončení	k1gNnSc4	ukončení
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
neporušuje	porušovat	k5eNaImIp3nS	porušovat
Boží	boží	k2eAgInSc1d1	boží
<g />
.	.	kIx.	.
</s>
<s>
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
více	hodně	k6eAd2	hodně
než	než	k8xS	než
léčba	léčba	k1gFnSc1	léčba
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
;	;	kIx,	;
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
je	být	k5eAaImIp3nS	být
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
řada	řada	k1gFnSc1	řada
sebevražd	sebevražda	k1gFnPc2	sebevražda
Božích	boží	k2eAgMnPc2d1	boží
následovníků	následovník	k1gMnPc2	následovník
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
tyto	tento	k3xDgFnPc4	tento
byly	být	k5eAaImAgFnP	být
odsuzovány	odsuzovat	k5eAaImNgFnP	odsuzovat
atd.	atd.	kA	atd.
Judaismus	judaismus	k1gInSc1	judaismus
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
vážit	vážit	k5eAaImF	vážit
si	se	k3xPyFc3	se
tohoto	tento	k3xDgInSc2	tento
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
sebevražda	sebevražda	k1gFnSc1	sebevražda
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
popírání	popírání	k1gNnSc1	popírání
Boží	boží	k2eAgFnSc2d1	boží
dobroty	dobrota	k1gFnSc2	dobrota
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
extrémních	extrémní	k2eAgFnPc2d1	extrémní
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
pouze	pouze	k6eAd1	pouze
smrt	smrt	k1gFnSc4	smrt
nebo	nebo	k8xC	nebo
zrazení	zrazení	k1gNnSc4	zrazení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
však	však	k9	však
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
páchali	páchat	k5eAaImAgMnP	páchat
Židé	Žid	k1gMnPc1	Žid
individuální	individuální	k2eAgFnSc2d1	individuální
nebo	nebo	k8xC	nebo
masové	masový	k2eAgFnSc2d1	masová
sebevraždy	sebevražda	k1gFnSc2	sebevražda
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Masada	Masada	k1gFnSc1	Masada
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
pronásledování	pronásledování	k1gNnSc1	pronásledování
Židů	Žid	k1gMnPc2	Žid
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Yorku	York	k1gInSc6	York
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pochmurnou	pochmurný	k2eAgFnSc7d1	pochmurná
připomínkou	připomínka	k1gFnSc7	připomínka
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
modlitba	modlitba	k1gFnSc1	modlitba
z	z	k7c2	z
židovské	židovský	k2eAgFnSc2d1	židovská
liturgie	liturgie	k1gFnSc2	liturgie
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nůž	nůž	k1gInSc1	nůž
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
hrdla	hrdla	k1gFnSc1	hrdla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
umírají	umírat	k5eAaImIp3nP	umírat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
"	"	kIx"	"
<g/>
posvětili	posvětit	k5eAaPmAgMnP	posvětit
Boží	božit	k5eAaImIp3nP	božit
jméno	jméno	k1gNnSc4	jméno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mučednictví	mučednictví	k1gNnSc4	mučednictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
skutky	skutek	k1gInPc1	skutek
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgInP	setkat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
židovských	židovský	k2eAgFnPc2d1	židovská
autorit	autorita	k1gFnPc2	autorita
se	s	k7c7	s
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
reakcí	reakce	k1gFnSc7	reakce
-	-	kIx~	-
jedni	jeden	k4xCgMnPc1	jeden
je	on	k3xPp3gMnPc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
hrdinského	hrdinský	k2eAgNnSc2d1	hrdinské
mučednictví	mučednictví	k1gNnSc2	mučednictví
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebylo	být	k5eNaImAgNnS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
mučednického	mučednický	k2eAgInSc2d1	mučednický
titulu	titul	k1gInSc2	titul
vzali	vzít	k5eAaPmAgMnP	vzít
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nepovoluje	povolovat	k5eNaImIp3nS	povolovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hinduismu	hinduismus	k1gInSc6	hinduismus
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
dívají	dívat	k5eAaImIp3nP	dívat
s	s	k7c7	s
pohrdáním	pohrdání	k1gNnSc7	pohrdání
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
stejně	stejně	k6eAd1	stejně
hříšnou	hříšný	k2eAgFnSc4d1	hříšná
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vražda	vražda	k1gFnSc1	vražda
jiného	jiný	k2eAgMnSc2d1	jiný
člena	člen	k1gMnSc2	člen
současné	současný	k2eAgFnSc2d1	současná
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hinduistické	hinduistický	k2eAgFnPc1d1	hinduistická
posvátné	posvátný	k2eAgFnPc1d1	posvátná
knihy	kniha	k1gFnPc1	kniha
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
součástí	součást	k1gFnSc7	součást
světa	svět	k1gInSc2	svět
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
potulovat	potulovat	k5eAaImF	potulovat
po	po	k7c6	po
Zemi	zem	k1gFnSc6	zem
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nP	by
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nespáchal	spáchat	k5eNaPmAgMnS	spáchat
<g/>
.	.	kIx.	.
</s>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
však	však	k9	však
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
člověku	člověk	k1gMnSc3	člověk
právo	právo	k1gNnSc4	právo
ukončit	ukončit	k5eAaPmF	ukončit
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
nenásilným	násilný	k2eNgInSc7d1	nenásilný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vyhladověním	vyhladovění	k1gNnSc7	vyhladovění
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
Prayopavesa	Prayopavesa	k1gFnSc1	Prayopavesa
<g/>
.	.	kIx.	.
</s>
<s>
Prayopavesa	Prayopavesa	k1gFnSc1	Prayopavesa
se	se	k3xPyFc4	se
ale	ale	k9	ale
týká	týkat	k5eAaImIp3nS	týkat
jen	jen	k9	jen
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
už	už	k6eAd1	už
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
životě	život	k1gInSc6	život
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
touhu	touha	k1gFnSc4	touha
nebo	nebo	k8xC	nebo
ambice	ambice	k1gFnPc4	ambice
a	a	k8xC	a
také	také	k9	také
žádnou	žádný	k3yNgFnSc4	žádný
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
džinismus	džinismus	k1gInSc4	džinismus
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
podobná	podobný	k2eAgFnSc1d1	podobná
praxe	praxe	k1gFnSc1	praxe
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Santhara	Santhara	k1gFnSc1	Santhara
<g/>
.	.	kIx.	.
</s>
<s>
Satí	Satí	k2eAgNnSc1d1	Satí
neboli	neboli	k8xC	neboli
rituální	rituální	k2eAgNnSc1d1	rituální
sebeupálení	sebeupálení	k1gNnSc1	sebeupálení
vdovy	vdova	k1gFnSc2	vdova
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
hindské	hindský	k2eAgFnSc6d1	hindská
společnosti	společnost	k1gFnSc6	společnost
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Filozofie	filozofie	k1gFnSc1	filozofie
sebevraždy	sebevražda	k1gFnSc2	sebevražda
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
řadou	řada	k1gFnSc7	řada
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
podstatou	podstata	k1gFnSc7	podstata
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
racionální	racionální	k2eAgFnSc7d1	racionální
volbou	volba	k1gFnSc7	volba
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
či	či	k8xC	či
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
morálně	morálně	k6eAd1	morálně
přípustná	přípustný	k2eAgFnSc1d1	přípustná
<g/>
.	.	kIx.	.
</s>
<s>
Filozofické	filozofický	k2eAgInPc1d1	filozofický
argumenty	argument	k1gInPc1	argument
o	o	k7c6	o
morální	morální	k2eAgFnSc6d1	morální
přípustnosti	přípustnost	k1gFnSc6	přípustnost
sebevraždy	sebevražda	k1gFnSc2	sebevražda
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
postojů	postoj	k1gInPc2	postoj
-	-	kIx~	-
od	od	k7c2	od
silné	silný	k2eAgFnSc2d1	silná
opozice	opozice	k1gFnSc2	opozice
(	(	kIx(	(
<g/>
pohlížející	pohlížející	k2eAgFnSc1d1	pohlížející
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
jako	jako	k8xC	jako
neetický	etický	k2eNgInSc1d1	neetický
a	a	k8xC	a
amorální	amorální	k2eAgInSc1d1	amorální
počin	počin	k1gInSc1	počin
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
vnímání	vnímání	k1gNnSc6	vnímání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
jako	jako	k8xC	jako
posvátného	posvátný	k2eAgNnSc2d1	posvátné
práva	právo	k1gNnSc2	právo
každého	každý	k3xTgMnSc2	každý
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
mladého	mladý	k2eAgMnSc2d1	mladý
a	a	k8xC	a
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pečlivého	pečlivý	k2eAgNnSc2d1	pečlivé
racionálního	racionální	k2eAgNnSc2d1	racionální
uvážení	uvážení	k1gNnSc2	uvážení
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oponentům	oponent	k1gMnPc3	oponent
sebevraždy	sebevražda	k1gFnSc2	sebevražda
patří	patřit	k5eAaImIp3nS	patřit
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
filozofové	filozof	k1gMnPc1	filozof
jako	jako	k8xC	jako
Augustin	Augustin	k1gMnSc1	Augustin
z	z	k7c2	z
Hippa	Hipp	k1gMnSc2	Hipp
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
-	-	kIx~	-
Millův	Millův	k2eAgInSc1d1	Millův
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
význam	význam	k1gInSc4	význam
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
nezávislosti	nezávislost	k1gFnSc2	nezávislost
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítal	odmítat	k5eAaImAgMnS	odmítat
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
člověku	člověk	k1gMnSc3	člověk
bránila	bránit	k5eAaImAgFnS	bránit
v	v	k7c6	v
učinění	učinění	k1gNnSc6	učinění
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k1gMnPc1	jiný
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
pohlížejí	pohlížet	k5eAaImIp3nP	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
legitimní	legitimní	k2eAgFnSc4d1	legitimní
záležitost	záležitost	k1gFnSc4	záležitost
osobní	osobní	k2eAgFnSc2d1	osobní
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
tohoto	tento	k3xDgInSc2	tento
postoje	postoj	k1gInSc2	postoj
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
nucen	nutit	k5eAaImNgMnS	nutit
trpět	trpět	k5eAaImF	trpět
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c2	za
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
nevyléčitelné	vyléčitelný	k2eNgFnPc4d1	nevyléčitelná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
psychická	psychický	k2eAgNnPc4d1	psychické
onemocnění	onemocnění	k1gNnPc4	onemocnění
a	a	k8xC	a
stáří	stáří	k1gNnSc4	stáří
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neexistuje	existovat	k5eNaImIp3nS	existovat
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Odmítají	odmítat	k5eAaImIp3nP	odmítat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
iracionální	iracionální	k2eAgMnSc1d1	iracionální
<g/>
,	,	kIx,	,
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
posledním	poslední	k2eAgNnSc7d1	poslední
možným	možný	k2eAgNnSc7d1	možné
řešením	řešení	k1gNnSc7	řešení
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
nesnesitelnou	snesitelný	k2eNgFnSc7d1	nesnesitelná
bolestí	bolest	k1gFnSc7	bolest
nebo	nebo	k8xC	nebo
zažívají	zažívat	k5eAaImIp3nP	zažívat
hluboké	hluboký	k2eAgNnSc4d1	hluboké
trauma	trauma	k1gNnSc4	trauma
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
radikálnější	radikální	k2eAgInSc1d2	radikálnější
postoj	postoj	k1gInSc1	postoj
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
lidem	člověk	k1gMnPc3	člověk
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
nezávisle	závisle	k6eNd1	závisle
zvolit	zvolit	k5eAaPmF	zvolit
smrt	smrt	k1gFnSc4	smrt
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
trpí	trpět	k5eAaImIp3nS	trpět
či	či	k8xC	či
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Význačnými	význačný	k2eAgMnPc7d1	význačný
obhájci	obhájce	k1gMnPc7	obhájce
této	tento	k3xDgFnSc2	tento
myšlenkové	myšlenkový	k2eAgFnSc2d1	myšlenková
školy	škola	k1gFnSc2	škola
jsou	být	k5eAaImIp3nP	být
skotský	skotský	k2eAgMnSc1d1	skotský
empirik	empirik	k1gMnSc1	empirik
David	David	k1gMnSc1	David
Hume	Hume	k1gNnSc4	Hume
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
bioetik	bioetik	k1gMnSc1	bioetik
Jacob	Jacoba	k1gFnPc2	Jacoba
Appel	Appel	k1gMnSc1	Appel
<g/>
.	.	kIx.	.
</s>
<s>
Vyznamný	Vyznamný	k2eAgMnSc1d1	Vyznamný
český	český	k2eAgMnSc1d1	český
politolog	politolog	k1gMnSc1	politolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
státnik	státnik	k1gMnSc1	státnik
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
se	se	k3xPyFc4	se
mimo	mimo	k6eAd1	mimo
jíné	jíné	k6eAd1	jíné
věnoval	věnovat	k5eAaImAgMnS	věnovat
otázce	otázka	k1gFnSc3	otázka
seběvraždy	seběvražda	k1gMnSc2	seběvražda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vidní	vidný	k2eAgMnPc1d1	vidný
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
sociologickou	sociologický	k2eAgFnSc7d1	sociologická
prací	práce	k1gFnSc7	práce
o	o	k7c6	o
seběvraždě	seběvražda	k1gFnSc6	seběvražda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Seběvražda	Seběvražda	k1gMnSc1	Seběvražda
hromadným	hromadný	k2eAgInSc7d1	hromadný
jevem	jev	k1gInSc7	jev
společenským	společenský	k2eAgInSc7d1	společenský
moderní	moderní	k2eAgFnPc4d1	moderní
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
Selbstmord	Selbstmord	k1gInSc1	Selbstmord
als	als	k?	als
soziale	soziale	k6eAd1	soziale
Massenerscheinung	Massenerscheinung	k1gInSc1	Massenerscheinung
der	drát	k5eAaImRp2nS	drát
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
jako	jako	k8xS	jako
masový	masový	k2eAgInSc1d1	masový
sociální	sociální	k2eAgInSc1d1	sociální
jev	jev	k1gInSc1	jev
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
bylo	být	k5eAaImAgNnS	být
vydané	vydaný	k2eAgNnSc1d1	vydané
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
o	o	k7c4	o
23	[number]	k4	23
lety	léto	k1gNnPc7	léto
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
popsat	popsat	k5eAaPmF	popsat
a	a	k8xC	a
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
přičiny	přičina	k1gFnPc1	přičina
vzniku	vznik	k1gInSc2	vznik
trendu	trend	k1gInSc2	trend
seběvražd	seběvražd	k6eAd1	seběvražd
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
hledal	hledat	k5eAaImAgMnS	hledat
zdroje	zdroj	k1gInPc4	zdroj
seběvražedných	seběvražedný	k2eAgFnPc2d1	seběvražedný
myšlenek	myšlenka	k1gFnPc2	myšlenka
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
přírodních	přírodní	k2eAgInPc6d1	přírodní
<g/>
,	,	kIx,	,
geografických	geografický	k2eAgInPc6d1	geografický
<g/>
,	,	kIx,	,
hospodářských	hospodářský	k2eAgInPc6d1	hospodářský
i	i	k8xC	i
sociálních	sociální	k2eAgInPc6d1	sociální
faktorech	faktor	k1gInPc6	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
společnost	společnost	k1gFnSc4	společnost
působily	působit	k5eAaImAgFnP	působit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
důvod	důvod	k1gInSc1	důvod
"	"	kIx"	"
<g/>
choroby	choroba	k1gFnSc2	choroba
<g/>
"	"	kIx"	"
společnosti	společnost	k1gFnSc2	společnost
však	však	k9	však
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
úpadku	úpadek	k1gInSc6	úpadek
zbožnosti	zbožnost	k1gFnSc2	zbožnost
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
nedostatku	nedostatek	k1gInSc6	nedostatek
mravních	mravní	k2eAgInPc2d1	mravní
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
společnost	společnost	k1gFnSc1	společnost
podle	podle	k7c2	podle
Masaryka	Masaryk	k1gMnSc2	Masaryk
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většinou	většinou	k6eAd1	většinou
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
pouze	pouze	k6eAd1	pouze
napůl	napůl	k6eAd1	napůl
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgInSc1d3	veliký
čast	čast	k1gInSc1	čast
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
má	mít	k5eAaImIp3nS	mít
základní	základní	k2eAgFnPc4d1	základní
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
ziskané	ziskaný	k2eAgMnPc4d1	ziskaný
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
další	další	k2eAgInPc4d1	další
konkretní	konkretní	k2eAgInPc4d1	konkretní
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
přemyšlet	přemyšlet	k5eAaPmF	přemyšlet
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
vlastního	vlastní	k2eAgNnSc2d1	vlastní
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
a	a	k8xC	a
občas	občas	k6eAd1	občas
to	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
připadě	připad	k1gInSc6	připad
vést	vést	k5eAaImF	vést
k	k	k7c3	k
seběvraždě	seběvražda	k1gFnSc3	seběvražda
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
moderního	moderní	k2eAgMnSc4d1	moderní
člověka	člověk	k1gMnSc4	člověk
s	s	k7c7	s
divochem	divoch	k1gMnSc7	divoch
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
sebevražedné	sebevražedný	k2eAgFnPc1d1	sebevražedná
tendence	tendence	k1gFnPc1	tendence
spíše	spíše	k9	spíše
neobjevovaly	objevovat	k5eNaImAgFnP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
typy	typ	k1gInPc4	typ
lidí	člověk	k1gMnPc2	člověk
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
nahlížení	nahlížení	k1gNnSc6	nahlížení
na	na	k7c4	na
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
divoch	divoch	k1gMnSc1	divoch
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
svět	svět	k1gInSc4	svět
okolní	okolní	k2eAgInSc4d1	okolní
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poznával	poznávat	k5eAaImAgInS	poznávat
především	především	k6eAd1	především
smyslově	smyslově	k6eAd1	smyslově
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgMnSc1d1	moderní
člověk	člověk	k1gMnSc1	člověk
vše	všechen	k3xTgNnSc4	všechen
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
niternému	niterný	k2eAgInSc3d1	niterný
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
nutí	nutit	k5eAaImIp3nS	nutit
pokládat	pokládat	k5eAaImF	pokládat
si	se	k3xPyFc3	se
existenciální	existenciální	k2eAgFnPc4d1	existenciální
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Otázky	otázka	k1gFnPc1	otázka
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
člověka	člověk	k1gMnSc4	člověk
často	často	k6eAd1	často
zavést	zavést	k5eAaPmF	zavést
k	k	k7c3	k
negativním	negativní	k2eAgFnPc3d1	negativní
myšlenkám	myšlenka	k1gFnPc3	myšlenka
a	a	k8xC	a
k	k	k7c3	k
pochybnostem	pochybnost	k1gFnPc3	pochybnost
vedoucím	vedoucí	k1gMnPc3	vedoucí
ke	k	k7c3	k
sklíčenosti	sklíčenost	k1gFnSc3	sklíčenost
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
popisuje	popisovat	k5eAaImIp3nS	popisovat
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
nabožensví	nabožensev	k1gFnPc2	nabožensev
na	na	k7c4	na
moralní	moralný	k2eAgMnPc1d1	moralný
stáv	stáv	k1gMnSc1	stáv
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přitomnost	Přitomnost	k1gFnSc1	Přitomnost
Boha	bůh	k1gMnSc2	bůh
v	v	k7c6	v
životě	život	k1gInSc6	život
společností	společnost	k1gFnPc2	společnost
podle	podle	k7c2	podle
myšlenek	myšlenka	k1gFnPc2	myšlenka
Masaryka	Masaryk	k1gMnSc2	Masaryk
určuje	určovat	k5eAaImIp3nS	určovat
moralní	moralný	k2eAgMnPc1d1	moralný
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
davá	davat	k5eAaPmIp3nS	davat
lidem	člověk	k1gMnPc3	člověk
ochranu	ochrana	k1gFnSc4	ochrana
od	od	k7c2	od
chmurných	chmurný	k2eAgFnPc2d1	chmurná
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ztráta	ztráta	k1gFnSc1	ztráta
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
ztrátu	ztráta	k1gFnSc4	ztráta
morálních	morální	k2eAgMnPc2d1	morální
úkazatelů	úkazatel	k1gMnPc2	úkazatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zpusobkauje	zpusobkaovat	k5eAaImIp3nS	zpusobkaovat
frustrace	frustrace	k1gFnSc1	frustrace
a	a	k8xC	a
jíné	jíný	k2eAgInPc1d1	jíný
psychické	psychický	k2eAgInPc1d1	psychický
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
hlavním	hlavní	k2eAgNnSc6d1	hlavní
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc5	Le
Suicide	Suicid	k1gMnSc5	Suicid
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sociolog	sociolog	k1gMnSc1	sociolog
Emile	Emil	k1gMnSc5	Emil
Durkheim	Durkheim	k1gInPc3	Durkheim
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
fenomén	fenomén	k1gInSc1	fenomén
sebevraždy	sebevražda	k1gFnSc2	sebevražda
ze	z	k7c2	z
sociologického	sociologický	k2eAgInSc2d1	sociologický
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
klasiku	klasika	k1gFnSc4	klasika
metodologie	metodologie	k1gFnSc2	metodologie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pomoci	pomoct	k5eAaPmF	pomoct
analýzy	analýza	k1gFnPc4	analýza
existujícího	existující	k2eAgInSc2d1	existující
statistického	statistický	k2eAgInSc2d1	statistický
výzkumu	výzkum	k1gInSc2	výzkum
Durkheim	Durkheim	k1gMnSc1	Durkheim
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
roli	role	k1gFnSc4	role
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
sebevraždy	sebevražda	k1gFnSc2	sebevražda
nehrají	hrát	k5eNaImIp3nP	hrát
psychologické	psychologický	k2eAgFnPc1d1	psychologická
příčiny	příčina	k1gFnPc1	příčina
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sociologické	sociologický	k2eAgInPc4d1	sociologický
faktory	faktor	k1gInPc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Durkheim	Durkheim	k1gMnSc1	Durkheim
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
definici	definice	k1gFnSc4	definice
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
aplikována	aplikován	k2eAgFnSc1d1	aplikována
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
případy	případ	k1gInPc4	případ
smrtí	smrtit	k5eAaImIp3nS	smrtit
vyplývající	vyplývající	k2eAgMnSc1d1	vyplývající
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
z	z	k7c2	z
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
nebo	nebo	k8xC	nebo
negativního	negativní	k2eAgInSc2d1	negativní
činu	čin	k1gInSc2	čin
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
čin	čin	k1gInSc1	čin
způsobí	způsobit	k5eAaPmIp3nS	způsobit
tento	tento	k3xDgInSc4	tento
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Durkheim	Durkheim	k1gMnSc1	Durkheim
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
sebevražední	sebevražedný	k2eAgMnPc1d1	sebevražedný
činy	čin	k1gInPc1	čin
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
Egoistická	egoistický	k2eAgFnSc1d1	egoistická
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Egoismus	egoismus	k1gInSc1	egoismus
-	-	kIx~	-
snaha	snaha	k1gFnSc1	snaha
milovat	milovat	k5eAaImF	milovat
jen	jen	k6eAd1	jen
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Egoistické	egoistický	k2eAgFnSc3d1	egoistická
sebevraždě	sebevražda	k1gFnSc3	sebevražda
předchází	předcházet	k5eAaImIp3nS	předcházet
stav	stav	k1gInSc1	stav
apatie	apatie	k1gFnSc2	apatie
a	a	k8xC	a
melancholie	melancholie	k1gFnSc2	melancholie
<g/>
.	.	kIx.	.
</s>
<s>
Egoista	egoista	k1gMnSc1	egoista
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
lhostejně	lhostejně	k6eAd1	lhostejně
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
povinnostem	povinnost	k1gFnPc3	povinnost
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgFnSc3d1	veřejná
službě	služba	k1gFnSc3	služba
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k9	jako
radost	radost	k1gFnSc1	radost
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
být	být	k5eAaImF	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Altruistická	altruistický	k2eAgFnSc1d1	altruistická
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Altruismus	altruismus	k1gInSc1	altruismus
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
protikladem	protiklad	k1gInSc7	protiklad
egoismu	egoismus	k1gInSc2	egoismus
<g/>
,	,	kIx,	,
nesobeckost	nesobeckost	k1gFnSc4	nesobeckost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchází	předcházet	k5eAaImIp3nS	předcházet
jí	on	k3xPp3gFnSc2	on
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
vášeň	vášeň	k1gFnSc4	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
dovádí	dovádět	k5eAaImIp3nS	dovádět
jedince	jedinec	k1gMnPc4	jedinec
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
za	za	k7c7	za
vyšším	vysoký	k2eAgInSc7d2	vyšší
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velice	velice	k6eAd1	velice
vysoké	vysoký	k2eAgFnPc4d1	vysoká
propojenosti	propojenost	k1gFnPc4	propojenost
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
považuje	považovat	k5eAaImIp3nS	považovat
sebeobětování	sebeobětování	k1gNnSc4	sebeobětování
bohu	bůh	k1gMnSc3	bůh
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
smysl	smysl	k1gInSc4	smysl
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Anomická	Anomický	k2eAgFnSc1d1	Anomická
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Anomie	Anomie	k1gFnSc1	Anomie
-	-	kIx~	-
stav	stav	k1gInSc1	stav
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neplatí	platit	k5eNaImIp3nP	platit
normy	norma	k1gFnPc1	norma
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anomické	Anomický	k2eAgFnSc3d1	Anomická
sebevraždě	sebevražda	k1gFnSc3	sebevražda
předchází	předcházet	k5eAaImIp3nS	předcházet
podráždění	podráždění	k1gNnSc1	podráždění
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc1	zoufalství
<g/>
.	.	kIx.	.
</s>
<s>
Spácháním	spáchání	k1gNnSc7	spáchání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
člověk	člověk	k1gMnSc1	člověk
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
životu	život	k1gInSc3	život
obecně	obecně	k6eAd1	obecně
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
otravuje	otravovat	k5eAaImIp3nS	otravovat
mu	on	k3xPp3gMnSc3	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Fatalistická	fatalistický	k2eAgFnSc1d1	fatalistická
sebevražda	sebevražda	k1gFnSc1	sebevražda
(	(	kIx(	(
<g/>
Fatalismus	fatalismus	k1gInSc1	fatalismus
=	=	kIx~	=
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechno	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
předurčeno	předurčit	k5eAaPmNgNnS	předurčit
osudem	osud	k1gInSc7	osud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fatalistické	fatalistický	k2eAgFnSc3d1	fatalistická
sebevraždě	sebevražda	k1gFnSc3	sebevražda
Durkheim	Durkheima	k1gFnPc2	Durkheima
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
věnuje	věnovat	k5eAaImIp3nS	věnovat
nejméně	málo	k6eAd3	málo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
hněvu	hněv	k1gInSc2	hněv
a	a	k8xC	a
zklamání	zklamání	k1gNnSc4	zklamání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
způsobena	způsoben	k2eAgFnSc1d1	způsobena
nadbytkem	nadbytkem	k6eAd1	nadbytkem
regulaci	regulace	k1gFnSc3	regulace
a	a	k8xC	a
existencí	existence	k1gFnSc7	existence
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němž	jenž	k3xRgInSc6	jenž
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
bezmocný	bezmocný	k2eAgMnSc1d1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
obhajována	obhajován	k2eAgFnSc1d1	obhajována
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kulturách	kultura	k1gFnPc6	kultura
a	a	k8xC	a
subkulturách	subkultura	k1gFnPc6	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
armáda	armáda	k1gFnSc1	armáda
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
podporovala	podporovat	k5eAaImAgFnS	podporovat
a	a	k8xC	a
oslavovala	oslavovat	k5eAaImAgFnS	oslavovat
útoky	útok	k1gInPc4	útok
kamikadze	kamikadze	k1gMnPc2	kamikadze
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgFnP	být
sebevražedné	sebevražedný	k2eAgInPc4d1	sebevražedný
útoky	útok	k1gInPc4	útok
vojenských	vojenský	k2eAgMnPc2d1	vojenský
letců	letec	k1gMnPc2	letec
japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
na	na	k7c6	na
námořní	námořní	k2eAgFnSc6d1	námořní
lodi	loď	k1gFnSc6	loď
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
závěrečných	závěrečný	k2eAgFnPc6d1	závěrečná
fázích	fáze	k1gFnPc6	fáze
tichomořské	tichomořský	k2eAgFnSc2d1	tichomořská
kampaně	kampaň	k1gFnSc2	kampaň
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
společnost	společnost	k1gFnSc1	společnost
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
byla	být	k5eAaImAgFnS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sebevraždu	sebevražda	k1gFnSc4	sebevražda
tolerující	tolerující	k2eAgFnSc4d1	tolerující
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sebevražda	sebevražda	k1gFnSc1	sebevražda
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
objevují	objevovat	k5eAaImIp3nP	objevovat
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c4	v
10-30	[number]	k4	10-30
%	%	kIx~	%
nabádají	nabádat	k5eAaImIp3nP	nabádat
k	k	k7c3	k
sebevražedným	sebevražedný	k2eAgInPc3d1	sebevražedný
pokusům	pokus	k1gInPc3	pokus
nebo	nebo	k8xC	nebo
je	on	k3xPp3gInPc4	on
jinak	jinak	k6eAd1	jinak
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobné	podobný	k2eAgFnPc1d1	podobná
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
mohou	moct	k5eAaImIp3nP	moct
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
jistými	jistý	k2eAgInPc7d1	jistý
předpoklady	předpoklad	k1gInPc7	předpoklad
představovat	představovat	k5eAaImF	představovat
poslední	poslední	k2eAgInSc4d1	poslední
krok	krok	k1gInSc4	krok
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
sebevražedných	sebevražedný	k2eAgFnPc2d1	sebevražedná
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
buď	buď	k8xC	buď
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
znají	znát	k5eAaImIp3nP	znát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
"	"	kIx"	"
<g/>
potkali	potkat	k5eAaPmAgMnP	potkat
<g/>
"	"	kIx"	"
v	v	k7c6	v
chat	chata	k1gFnPc2	chata
roomech	roomo	k1gNnPc6	roomo
nebo	nebo	k8xC	nebo
na	na	k7c6	na
virtuálních	virtuální	k2eAgFnPc6d1	virtuální
nástěnkách	nástěnka	k1gFnPc6	nástěnka
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
však	však	k9	však
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
může	moct	k5eAaImIp3nS	moct
sebevraždám	sebevražda	k1gFnPc3	sebevražda
předejít	předejít	k5eAaPmF	předejít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
izolovaní	izolovaný	k2eAgMnPc1d1	izolovaný
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
nějaké	nějaký	k3yIgFnSc2	nějaký
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
kulturních	kulturní	k2eAgFnPc2d1	kulturní
nebo	nebo	k8xC	nebo
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
prosluly	proslout	k5eAaPmAgInP	proslout
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
třeba	třeba	k6eAd1	třeba
most	most	k1gInSc4	most
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gat	k1gInSc2	Gat
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgInSc1d1	japonský
les	les	k1gInSc1	les
Aokigahara	Aokigahara	k1gFnSc1	Aokigahara
<g/>
,	,	kIx,	,
jihoanglický	jihoanglický	k2eAgInSc1d1	jihoanglický
útes	útes	k1gInSc1	útes
Beachy	Beacha	k1gFnSc2	Beacha
Head	Heada	k1gFnPc2	Heada
nebo	nebo	k8xC	nebo
torontský	torontský	k2eAgInSc1d1	torontský
viadukt	viadukt	k1gInSc1	viadukt
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Bloor	Bloor	k1gMnSc1	Bloor
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
od	od	k7c2	od
postavení	postavení	k1gNnSc2	postavení
mostu	most	k1gInSc2	most
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gate	k1gNnPc2	Gate
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
spáchali	spáchat	k5eAaPmAgMnP	spáchat
skokem	skok	k1gInSc7	skok
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
sebevraždám	sebevražda	k1gFnPc3	sebevražda
běžně	běžně	k6eAd1	běžně
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc4d1	vybudována
bariéry	bariéra	k1gFnPc4	bariéra
sebevražedným	sebevražedný	k2eAgInPc3d1	sebevražedný
pokusům	pokus	k1gInPc3	pokus
zabraňující	zabraňující	k2eAgFnSc2d1	zabraňující
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Světelný	světelný	k2eAgInSc1d1	světelný
závoj	závoj	k1gInSc1	závoj
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
bariéry	bariéra	k1gFnPc4	bariéra
na	na	k7c6	na
Eiffelově	Eiffelův	k2eAgFnSc6d1	Eiffelova
věži	věž	k1gFnSc6	věž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
či	či	k8xC	či
budově	budova	k1gFnSc6	budova
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
budována	budován	k2eAgFnSc1d1	budována
bariéra	bariéra	k1gFnSc1	bariéra
i	i	k8xC	i
na	na	k7c6	na
mostu	most	k1gInSc2	most
Golden	Goldno	k1gNnPc2	Goldno
Gate	Gat	k1gInSc2	Gat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zábrany	zábrana	k1gFnPc1	zábrana
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
být	být	k5eAaImF	být
obecně	obecně	k6eAd1	obecně
velice	velice	k6eAd1	velice
účinné	účinný	k2eAgNnSc1d1	účinné
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
sebevražda	sebevražda	k1gFnSc1	sebevražda
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vědomé	vědomý	k2eAgNnSc4d1	vědomé
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
nelze	lze	k6eNd1	lze
hovořit	hovořit	k5eAaImF	hovořit
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražedné	sebevražedný	k2eAgNnSc1d1	sebevražedné
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
salmonelly	salmonella	k1gFnSc2	salmonella
snažících	snažící	k2eAgFnPc2d1	snažící
se	se	k3xPyFc4	se
překonat	překonat	k5eAaPmF	překonat
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
,	,	kIx,	,
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
bakterie	bakterie	k1gFnSc2	bakterie
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spustí	spustit	k5eAaPmIp3nS	spustit
odpověď	odpověď	k1gFnSc4	odpověď
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
sebevražedné	sebevražedný	k2eAgNnSc1d1	sebevražedné
obranné	obranný	k2eAgNnSc1d1	obranné
chování	chování	k1gNnSc1	chování
dělníků	dělník	k1gMnPc2	dělník
mravenců	mravenec	k1gMnPc2	mravenec
rodu	rod	k1gInSc2	rod
Forelius	Forelius	k1gMnSc1	Forelius
pusillus	pusillus	k1gMnSc1	pusillus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
malá	malý	k2eAgFnSc1d1	malá
skupinka	skupinka	k1gFnSc1	skupinka
mravenců	mravenec	k1gMnPc2	mravenec
opustí	opustit	k5eAaPmIp3nS	opustit
každý	každý	k3xTgInSc1	každý
večer	večer	k1gInSc1	večer
bezpečí	bezpečí	k1gNnSc2	bezpečí
mraveniště	mraveniště	k1gNnSc2	mraveniště
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
utěsnit	utěsnit	k5eAaPmF	utěsnit
vchod	vchod	k1gInSc4	vchod
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
mšice	mšice	k1gFnSc1	mšice
kyjatka	kyjatka	k1gFnSc1	kyjatka
hrachová	hrachový	k2eAgFnSc1d1	hrachová
napadena	napadnout	k5eAaPmNgFnS	napadnout
slunéčkem	slunéčko	k1gNnSc7	slunéčko
sedmitečným	sedmitečný	k2eAgNnSc7d1	sedmitečné
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
explodovat	explodovat	k5eAaBmF	explodovat
<g/>
;	;	kIx,	;
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgMnPc1d1	další
jedinci	jedinec	k1gMnPc1	jedinec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
rozptýleni	rozptýlit	k5eAaPmNgMnP	rozptýlit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
ochráněni	ochráněn	k2eAgMnPc1d1	ochráněn
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
slunéčka	slunéčko	k1gNnSc2	slunéčko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
samotné	samotný	k2eAgNnSc1d1	samotné
někdy	někdy	k6eAd1	někdy
hyne	hynout	k5eAaImIp3nS	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
termitů	termit	k1gMnPc2	termit
mají	mít	k5eAaImIp3nP	mít
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
také	také	k9	také
explodují	explodovat	k5eAaBmIp3nP	explodovat
a	a	k8xC	a
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
lepivou	lepivý	k2eAgFnSc7d1	lepivá
hmotou	hmota	k1gFnSc7	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
byly	být	k5eAaImAgFnP	být
i	i	k9	i
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
psech	pes	k1gMnPc6	pes
<g/>
,	,	kIx,	,
koních	kůň	k1gMnPc6	kůň
a	a	k8xC	a
delfínech	delfín	k1gMnPc6	delfín
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
důkazy	důkaz	k1gInPc1	důkaz
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
příliš	příliš	k6eAd1	příliš
přesvědčivé	přesvědčivý	k2eAgNnSc1d1	přesvědčivé
<g/>
.	.	kIx.	.
</s>
<s>
Sebevraždám	sebevražda	k1gFnPc3	sebevražda
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k6eAd1	málo
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
hromadné	hromadný	k2eAgFnSc2d1	hromadná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
jonestownská	jonestownský	k2eAgFnSc1d1	jonestownský
<g/>
"	"	kIx"	"
kultovní	kultovní	k2eAgFnSc1d1	kultovní
sebevražda	sebevražda	k1gFnSc1	sebevražda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
918	[number]	k4	918
členů	člen	k1gMnPc2	člen
tzv.	tzv.	kA	tzv.
Chrámu	chrámat	k5eAaImIp1nS	chrámat
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
americké	americký	k2eAgFnSc2d1	americká
sekty	sekta	k1gFnSc2	sekta
vedené	vedený	k2eAgFnSc2d1	vedená
Jimem	Jim	k1gMnSc7	Jim
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
po	po	k7c6	po
vypití	vypití	k1gNnSc6	vypití
hroznového	hroznový	k2eAgInSc2d1	hroznový
nealkoholického	alkoholický	k2eNgInSc2d1	nealkoholický
nápoje	nápoj	k1gInSc2	nápoj
Flavor	Flavora	k1gFnPc2	Flavora
Aid	Aida	k1gFnPc2	Aida
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
přidán	přidán	k2eAgInSc1d1	přidán
kyanid	kyanid	k1gInSc1	kyanid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Saipan	Saipan	k1gInSc4	Saipan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
spáchalo	spáchat	k5eAaPmAgNnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
japonských	japonský	k2eAgMnPc2d1	japonský
civilistů	civilista	k1gMnPc2	civilista
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
ukončili	ukončit	k5eAaPmAgMnP	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
skokem	skokem	k6eAd1	skokem
z	z	k7c2	z
Útesu	útes	k1gInSc2	útes
sebevrahů	sebevrah	k1gMnPc2	sebevrah
a	a	k8xC	a
Útesu	útes	k1gInSc2	útes
Banzai	Banza	k1gFnSc2	Banza
<g/>
.	.	kIx.	.
</s>
<s>
Irská	irský	k2eAgFnSc1d1	irská
stávka	stávka	k1gFnSc1	stávka
hladovkou	hladovka	k1gFnSc7	hladovka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Bobbym	Bobbym	k1gInSc4	Bobbym
Sandsem	Sandso	k1gNnSc7	Sandso
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
10	[number]	k4	10
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
smrti	smrt	k1gFnSc2	smrt
bylo	být	k5eAaImAgNnS	být
koronerem	koroner	k1gMnSc7	koroner
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
"	"	kIx"	"
<g/>
dobrovolné	dobrovolný	k2eAgNnSc4d1	dobrovolné
vyhladovění	vyhladovění	k1gNnSc4	vyhladovění
<g/>
"	"	kIx"	"
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
;	;	kIx,	;
původní	původní	k2eAgFnSc3d1	původní
"	"	kIx"	"
<g/>
vyhladovění	vyhladovění	k1gNnSc1	vyhladovění
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
úmrtních	úmrtní	k2eAgInPc6d1	úmrtní
listech	list	k1gInPc6	list
opraveno	opraven	k2eAgNnSc1d1	opraveno
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
rodin	rodina	k1gFnPc2	rodina
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
stávkujících	stávkující	k1gMnPc2	stávkující
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
Erwinu	Erwin	k1gMnSc3	Erwin
Rommelovi	Rommel	k1gMnSc3	Rommel
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předem	předem	k6eAd1	předem
věděl	vědět	k5eAaImAgMnS	vědět
o	o	k7c6	o
spiknutí	spiknutí	k1gNnSc6	spiknutí
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
zaměřeném	zaměřený	k2eAgInSc6d1	zaměřený
proti	proti	k7c3	proti
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vyhrožováno	vyhrožován	k2eAgNnSc1d1	vyhrožováno
veřejným	veřejný	k2eAgInSc7d1	veřejný
soudním	soudní	k2eAgInSc7d1	soudní
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
popravou	poprava	k1gFnSc7	poprava
a	a	k8xC	a
represáliemi	represálie	k1gFnPc7	represálie
vůči	vůči	k7c3	vůči
jeho	jeho	k3xOp3gFnSc3	jeho
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
nevezme	vzít	k5eNaPmIp3nS	vzít
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
