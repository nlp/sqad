<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
stále	stále	k6eAd1	stále
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sebevraždám	sebevražda	k1gFnPc3	sebevražda
vlastnoručním	vlastnoruční	k2eAgNnSc7d1	vlastnoruční
proříznutím	proříznutí	k1gNnSc7	proříznutí
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
,	,	kIx,	,
známému	známý	k1gMnSc3	známý
rovněž	rovněž	k9	rovněž
jako	jako	k8xC	jako
seppuku	seppuk	k1gInSc2	seppuk
nebo	nebo	k8xC	nebo
harakiri	harakiri	k1gNnSc2	harakiri
<g/>
,	,	kIx,	,
nejobvyklejším	obvyklý	k2eAgInSc7d3	nejobvyklejší
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
však	však	k9	však
oběšení	oběšení	k1gNnSc1	oběšení
<g/>
.	.	kIx.	.
</s>
