<s>
Džunka	džunka	k1gFnSc1	džunka
či	či	k8xC	či
čínská	čínský	k2eAgFnSc1d1	čínská
džunka	džunka	k1gFnSc1	džunka
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
čínské	čínský	k2eAgFnSc2d1	čínská
plachetní	plachetní	k2eAgFnSc2d1	plachetní
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
typy	typ	k1gInPc4	typ
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
široce	široko	k6eAd1	široko
používána	používat	k5eAaImNgFnS	používat
přes	přes	k7c4	přes
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
220	[number]	k4	220
až	až	k9	až
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zdokonalována	zdokonalovat	k5eAaImNgFnS	zdokonalovat
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
staletích	staletí	k1gNnPc6	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
plachetnice	plachetnice	k1gFnPc1	plachetnice
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
lodě	loď	k1gFnPc1	loď
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1421	[number]	k4	1421
z	z	k7c2	z
rozkazu	rozkaz	k1gInSc2	rozkaz
císaře	císař	k1gMnSc2	císař
Ču	Ču	k1gMnSc2	Ču
Tiho	Tiho	k6eAd1	Tiho
v	v	k7c6	v
loděnici	loděnice	k1gFnSc6	loděnice
v	v	k7c6	v
Lung-ťiangu	Lung-ťiang	k1gInSc6	Lung-ťiang
poblíž	poblíž	k7c2	poblíž
Nankingu	Nanking	k1gInSc2	Nanking
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
až	až	k9	až
140	[number]	k4	140
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
36	[number]	k4	36
m	m	kA	m
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
lodě	loď	k1gFnPc4	loď
pokladů	poklad	k1gInPc2	poklad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
lodi	loď	k1gFnPc4	loď
je	být	k5eAaImIp3nS	být
trup	trup	k1gInSc1	trup
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
dnem	den	k1gInSc7	den
a	a	k8xC	a
čínské	čínský	k2eAgFnPc1d1	čínská
lugrové	lugrový	k2eAgFnPc1d1	lugrový
plachty	plachta	k1gFnPc1	plachta
zpevněné	zpevněný	k2eAgFnPc1d1	zpevněná
výztuhami	výztuha	k1gFnPc7	výztuha
<g/>
.	.	kIx.	.
</s>
<s>
Džunky	džunka	k1gFnPc1	džunka
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
uměleckých	umělecký	k2eAgInPc6d1	umělecký
dílech	díl	k1gInPc6	díl
odehrávajících	odehrávající	k2eAgMnPc2d1	odehrávající
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
tematicky	tematicky	k6eAd1	tematicky
čerpajících	čerpající	k2eAgFnPc2d1	čerpající
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgNnPc2d1	jiné
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
také	také	k9	také
Arthur	Arthur	k1gMnSc1	Arthur
Ransome	Ransom	k1gInSc5	Ransom
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Slečna	slečna	k1gFnSc1	slečna
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
</s>
