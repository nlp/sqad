<p>
<s>
Tautomerie	tautomerie	k1gFnSc1	tautomerie
(	(	kIx(	(
<g/>
též	též	k9	též
tautomerismus	tautomerismus	k1gInSc1	tautomerismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
izomerie	izomerie	k1gFnSc2	izomerie
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
izomery	izomer	k1gInPc1	izomer
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
zde	zde	k6eAd1	zde
tautomery	tautomer	k1gInPc1	tautomer
<g/>
)	)	kIx)	)
snadno	snadno	k6eAd1	snadno
přecházet	přecházet	k5eAaImF	přecházet
v	v	k7c6	v
jiné	jiná	k1gFnSc6	jiná
reakcí	reakce	k1gFnPc2	reakce
zvanou	zvaný	k2eAgFnSc4d1	zvaná
tautomerizace	tautomerizace	k1gFnSc1	tautomerizace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
běžně	běžně	k6eAd1	běžně
představuje	představovat	k5eAaImIp3nS	představovat
migraci	migrace	k1gFnSc3	migrace
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
či	či	k8xC	či
protonu	proton	k1gInSc2	proton
<g/>
,	,	kIx,	,
doplněná	doplněná	k1gFnSc1	doplněná
prohozením	prohození	k1gNnSc7	prohození
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
přiléhající	přiléhající	k2eAgFnPc4d1	přiléhající
vazby	vazba	k1gFnPc4	vazba
dvojné	dvojný	k2eAgFnPc4d1	dvojná
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
přechodu	přechod	k1gInSc2	přechod
jednoho	jeden	k4xCgInSc2	jeden
izomeru	izomer	k1gInSc2	izomer
v	v	k7c4	v
jiný	jiný	k2eAgInSc4d1	jiný
se	se	k3xPyFc4	se
tautomery	tautomer	k1gInPc1	tautomer
obecně	obecně	k6eAd1	obecně
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
totožnou	totožný	k2eAgFnSc4d1	totožná
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Tautomerie	tautomerie	k1gFnSc1	tautomerie
je	být	k5eAaImIp3nS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
strukturální	strukturální	k2eAgFnSc2d1	strukturální
izomerie	izomerie	k1gFnSc2	izomerie
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
nekanonickém	kanonický	k2eNgNnSc6d1	nekanonické
párování	párování	k1gNnSc6	párování
bází	báze	k1gFnPc2	báze
v	v	k7c6	v
molekulách	molekula	k1gFnPc6	molekula
DNA	DNA	kA	DNA
a	a	k8xC	a
zejména	zejména	k9	zejména
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemie	chemie	k1gFnSc1	chemie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
tautomerizace	tautomerizace	k1gFnSc1	tautomerizace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
chemické	chemický	k2eAgFnPc4d1	chemická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
(	(	kIx(	(
<g/>
ekvilibria	ekvilibrium	k1gNnSc2	ekvilibrium
<g/>
)	)	kIx)	)
tautomerů	tautomer	k1gInPc2	tautomer
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
poměr	poměr	k1gInSc1	poměr
tautomerů	tautomer	k1gInPc2	tautomer
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
teplotě	teplota	k1gFnSc3	teplota
<g/>
,	,	kIx,	,
rozpouštědle	rozpouštědlo	k1gNnSc6	rozpouštědlo
a	a	k8xC	a
pH	ph	kA	ph
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
běžné	běžný	k2eAgFnPc4d1	běžná
tautomerní	tautomerní	k2eAgFnPc4d1	tautomerní
páry	pára	k1gFnPc4	pára
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
keton	keton	k1gInSc1	keton
-	-	kIx~	-
enol	enol	k1gInSc1	enol
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
acetonu	aceton	k1gInSc2	aceton
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
keto-enol	ketonol	k1gInSc4	keto-enol
tautomerie	tautomerie	k1gFnSc2	tautomerie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
keten	keten	k1gInSc1	keten
-	-	kIx~	-
ynol	ynol	k1gInSc1	ynol
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
ethenonu	ethenon	k1gInSc2	ethenon
</s>
</p>
<p>
<s>
amid	amid	k1gInSc1	amid
-	-	kIx~	-
imidová	imidový	k2eAgFnSc1d1	imidový
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
hydrolýzy	hydrolýza	k1gFnSc2	hydrolýza
nitrilů	nitril	k1gInPc2	nitril
</s>
</p>
<p>
<s>
laktam	laktam	k1gInSc1	laktam
-	-	kIx~	-
laktim	laktim	k1gInSc1	laktim
<g/>
,	,	kIx,	,
tautomerie	tautomerie	k1gFnSc1	tautomerie
v	v	k7c6	v
heterocyklických	heterocyklický	k2eAgInPc6d1	heterocyklický
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
nukleobází	nukleobáze	k1gFnPc2	nukleobáze
guaninu	guanin	k1gInSc2	guanin
<g/>
,	,	kIx,	,
thyminu	thymin	k2eAgFnSc4d1	thymin
a	a	k8xC	a
cytosinu	cytosin	k2eAgFnSc4d1	cytosin
</s>
</p>
<p>
<s>
enamin	enamin	k1gInSc1	enamin
-	-	kIx~	-
imin	imin	k1gInSc1	imin
</s>
</p>
<p>
<s>
enamin	enamin	k1gInSc1	enamin
-	-	kIx~	-
enamin	enamin	k1gInSc1	enamin
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
enzymatických	enzymatický	k2eAgFnPc6d1	enzymatická
reakcích	reakce	k1gFnPc6	reakce
katalyzovaných	katalyzovaný	k2eAgFnPc6d1	katalyzovaná
pyridoxal-fosfátem	pyridoxalosfát	k1gInSc7	pyridoxal-fosfát
</s>
</p>
<p>
<s>
anomery	anomera	k1gFnPc1	anomera
redukujících	redukující	k2eAgInPc2d1	redukující
cukrů	cukr	k1gInPc2	cukr
se	se	k3xPyFc4	se
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
přes	přes	k7c4	přes
formu	forma	k1gFnSc4	forma
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
řetězcem	řetězec	k1gInSc7	řetězec
</s>
</p>
<p>
<s>
==	==	k?	==
Prototropie	Prototropie	k1gFnSc2	Prototropie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
formou	forma	k1gFnSc7	forma
tautomerie	tautomerie	k1gFnSc2	tautomerie
je	být	k5eAaImIp3nS	být
prototropie	prototropie	k1gFnSc1	prototropie
<g/>
;	;	kIx,	;
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
relokací	relokace	k1gFnSc7	relokace
protonu	proton	k1gInSc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
Prototropní	Prototropní	k2eAgFnSc4d1	Prototropní
tautomerii	tautomerie	k1gFnSc4	tautomerie
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
podmnožinu	podmnožina	k1gFnSc4	podmnožina
acidobazického	acidobazický	k2eAgNnSc2d1	acidobazický
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Prototropní	Prototropní	k2eAgInPc1d1	Prototropní
tautomery	tautomer	k1gInPc1	tautomer
jsou	být	k5eAaImIp3nP	být
sada	sada	k1gFnSc1	sada
izomerních	izomerní	k2eAgInPc2d1	izomerní
protonačních	protonační	k2eAgInPc2d1	protonační
stavů	stav	k1gInPc2	stav
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
empirickým	empirický	k2eAgInSc7d1	empirický
vzorcem	vzorec	k1gInSc7	vzorec
a	a	k8xC	a
celkovým	celkový	k2eAgInSc7d1	celkový
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tautomerizaci	Tautomerizace	k1gFnSc4	Tautomerizace
lze	lze	k6eAd1	lze
katalyzovat	katalyzovat	k5eAaPmF	katalyzovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zásadou	zásada	k1gFnSc7	zásada
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
deprotonace	deprotonace	k1gFnSc2	deprotonace
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
vznik	vznik	k1gInSc1	vznik
delokalizovaného	delokalizovaný	k2eAgInSc2d1	delokalizovaný
aniontu	anion	k1gInSc2	anion
<g/>
,	,	kIx,	,
např.	např.	kA	např.
enolátu	enolát	k1gInSc2	enolát
<g/>
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
protonace	protonace	k1gFnSc2	protonace
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
pozici	pozice	k1gFnSc6	pozice
aniontu	anion	k1gInSc2	anion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kyselinou	kyselina	k1gFnSc7	kyselina
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
protonace	protonace	k1gFnSc2	protonace
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
tvorba	tvorba	k1gFnSc1	tvorba
delokalizovaného	delokalizovaný	k2eAgInSc2d1	delokalizovaný
kationtu	kation	k1gInSc2	kation
<g/>
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
deprotonace	deprotonace	k1gFnSc2	deprotonace
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
pozici	pozice	k1gFnSc6	pozice
přiléhající	přiléhající	k2eAgFnSc6d1	přiléhající
ke	k	k7c3	k
kationtu	kation	k1gInSc3	kation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Anulární	Anulární	k2eAgFnSc1d1	Anulární
tautomerie	tautomerie	k1gFnSc1	tautomerie
</s>
</p>
<p>
<s>
druh	druh	k1gInSc1	druh
prototropní	prototropní	k2eAgFnSc2d1	prototropní
tautomerie	tautomerie	k1gFnSc2	tautomerie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
proton	proton	k1gInSc1	proton
zaujímat	zaujímat	k5eAaImF	zaujímat
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
heterocyklickém	heterocyklický	k2eAgInSc6d1	heterocyklický
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
1H-	[number]	k4	1H-
a	a	k8xC	a
3	[number]	k4	3
<g/>
H-imidazol	Hmidazola	k1gFnPc2	H-imidazola
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
H-	H-	k1gFnPc2	H-
<g/>
,	,	kIx,	,
2H-	[number]	k4	2H-
a	a	k8xC	a
4H-	[number]	k4	4H-
1,2	[number]	k4	1,2
<g/>
,4	,4	k4	,4
<g/>
-triazol	riazola	k1gFnPc2	-triazola
<g/>
;	;	kIx,	;
1H-	[number]	k4	1H-
a	a	k8xC	a
2H-	[number]	k4	2H-
isoindol	isoindola	k1gFnPc2	isoindola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tautomerie	tautomerie	k1gFnSc1	tautomerie
kruh-řetězec	kruh-řetězec	k1gInSc1	kruh-řetězec
</s>
</p>
<p>
<s>
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
s	s	k7c7	s
přesunem	přesun	k1gInSc7	přesun
protonu	proton	k1gInSc2	proton
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
otevřené	otevřený	k2eAgFnSc2d1	otevřená
struktury	struktura	k1gFnSc2	struktura
na	na	k7c4	na
kruh	kruh	k1gInSc4	kruh
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
otevřené	otevřený	k2eAgFnSc2d1	otevřená
a	a	k8xC	a
pyranové	pyranový	k2eAgFnSc2d1	pyranový
formy	forma	k1gFnSc2	forma
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
furanové	furanový	k2eAgFnSc2d1	furanový
formy	forma	k1gFnSc2	forma
fruktózy	fruktóza	k1gFnSc2	fruktóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Valenční	valenční	k2eAgFnSc2d1	valenční
tautomerie	tautomerie	k1gFnSc2	tautomerie
==	==	k?	==
</s>
</p>
<p>
<s>
Valenční	valenční	k2eAgFnSc1d1	valenční
tautomerie	tautomerie	k1gFnSc1	tautomerie
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
tautomerie	tautomerie	k1gFnSc2	tautomerie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
dvojné	dvojný	k2eAgFnPc1d1	dvojná
vazby	vazba	k1gFnPc1	vazba
rychle	rychle	k6eAd1	rychle
vznikají	vznikat	k5eAaImIp3nP	vznikat
a	a	k8xC	a
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
migraci	migrace	k1gFnSc3	migrace
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
prototropní	prototropní	k2eAgFnSc2d1	prototropní
tautomerie	tautomerie	k1gFnSc2	tautomerie
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
procesy	proces	k1gInPc4	proces
s	s	k7c7	s
rychlou	rychlý	k2eAgFnSc7d1	rychlá
reorganizací	reorganizace	k1gFnSc7	reorganizace
vazebných	vazebný	k2eAgInPc2d1	vazebný
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
tautomerie	tautomerie	k1gFnSc2	tautomerie
je	být	k5eAaImIp3nS	být
bullvalen	bullvalen	k2eAgMnSc1d1	bullvalen
nebo	nebo	k8xC	nebo
otevřené	otevřený	k2eAgFnPc1d1	otevřená
a	a	k8xC	a
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
formy	forma	k1gFnPc1	forma
některých	některý	k3yIgFnPc2	některý
heterocyklických	heterocyklický	k2eAgFnPc2d1	heterocyklická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
azid	azid	k1gInSc1	azid
–	–	k?	–
tetrazol	tetrazol	k1gInSc1	tetrazol
či	či	k8xC	či
mezoiontový	mezoiontový	k2eAgInSc1d1	mezoiontový
münchnon-acylaminoketen	münchnoncylaminoketen	k2eAgInSc1d1	münchnon-acylaminoketen
<g/>
.	.	kIx.	.
</s>
<s>
Valenční	valenční	k2eAgFnSc1d1	valenční
tautomerie	tautomerie	k1gFnSc1	tautomerie
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
molekulární	molekulární	k2eAgFnSc6d1	molekulární
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
neměla	mít	k5eNaImAgFnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
kanonickými	kanonický	k2eAgFnPc7d1	kanonická
rezonančními	rezonanční	k2eAgFnPc7d1	rezonanční
strukturami	struktura	k1gFnPc7	struktura
nebo	nebo	k8xC	nebo
mezomery	mezomer	k1gInPc7	mezomer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tautomer	tautomer	k1gInSc1	tautomer
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
