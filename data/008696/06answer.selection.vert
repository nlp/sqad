<s>
Tautomerie	tautomerie	k1gFnSc1	tautomerie
(	(	kIx(	(
<g/>
též	též	k9	též
tautomerismus	tautomerismus	k1gInSc1	tautomerismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
izomerie	izomerie	k1gFnSc2	izomerie
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
izomery	izomer	k1gInPc1	izomer
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
zde	zde	k6eAd1	zde
tautomery	tautomer	k1gInPc1	tautomer
<g/>
)	)	kIx)	)
snadno	snadno	k6eAd1	snadno
přecházet	přecházet	k5eAaImF	přecházet
v	v	k7c6	v
jiné	jiná	k1gFnSc6	jiná
reakcí	reakce	k1gFnPc2	reakce
zvanou	zvaný	k2eAgFnSc4d1	zvaná
tautomerizace	tautomerizace	k1gFnSc1	tautomerizace
<g/>
.	.	kIx.	.
</s>
