<p>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
či	či	k8xC	či
spíše	spíše	k9	spíše
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
barev	barva	k1gFnPc2	barva
<g/>
)	)	kIx)	)
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
smícháním	smíchání	k1gNnSc7	smíchání
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
aditivním	aditivní	k2eAgNnSc6d1	aditivní
míchání	míchání	k1gNnSc6	míchání
barev	barva	k1gFnPc2	barva
vzniká	vznikat	k5eAaImIp3nS	vznikat
šedá	šedá	k1gFnSc1	šedá
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
stejného	stejný	k2eAgNnSc2d1	stejné
množství	množství	k1gNnSc2	množství
všech	všecek	k3xTgFnPc2	všecek
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
všechny	všechen	k3xTgFnPc4	všechen
barvy	barva	k1gFnPc4	barva
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
;	;	kIx,	;
k	k	k7c3	k
<g/>
;	;	kIx,	;
k	k	k7c3	k
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
šedé	šedý	k2eAgInPc1d1	šedý
(	(	kIx(	(
<g/>
o	o	k7c6	o
různé	různý	k2eAgFnSc6d1	různá
světlosti	světlost	k1gFnSc6	světlost
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
subtraktivním	subtraktivní	k2eAgNnSc6d1	subtraktivní
míchání	míchání	k1gNnSc6	míchání
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
CMY	CMY	kA	CMY
platí	platit	k5eAaImIp3nS	platit
totéž	týž	k3xTgNnSc1	týž
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
prakticky	prakticky	k6eAd1	prakticky
používaném	používaný	k2eAgInSc6d1	používaný
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
CMYK	CMYK	kA	CMYK
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
používá	používat	k5eAaImIp3nS	používat
čistě	čistě	k6eAd1	čistě
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
ve	v	k7c6	v
zvolené	zvolený	k2eAgFnSc6d1	zvolená
intenzitě	intenzita	k1gFnSc6	intenzita
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
0	[number]	k4	0
<g/>
;	;	kIx,	;
0	[number]	k4	0
<g/>
;	;	kIx,	;
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
stříbrnou	stříbrná	k1gFnSc4	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
barva	barva	k1gFnSc1	barva
šedá	šedá	k1gFnSc1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
U	u	k7c2	u
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
repro	repro	k6eAd1	repro
bedny	bedna	k1gFnPc4	bedna
<g/>
,	,	kIx,	,
průkazy	průkaz	k1gInPc4	průkaz
od	od	k7c2	od
vozidla	vozidlo	k1gNnSc2	vozidlo
</s>
</p>
<p>
<s>
Šedá	Šedá	k1gFnSc1	Šedá
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
všednosti	všednost	k1gFnSc2	všednost
a	a	k8xC	a
obyčejnosti	obyčejnost	k1gFnSc2	obyčejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šedý	šedý	k2eAgInSc1d1	šedý
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
nezajímavý	zajímavý	k2eNgInSc4d1	nezajímavý
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc4d1	průměrný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
barva	barva	k1gFnSc1	barva
označuje	označovat	k5eAaImIp3nS	označovat
něco	něco	k3yInSc1	něco
mezi	mezi	k7c7	mezi
zlem	zlo	k1gNnSc7	zlo
(	(	kIx(	(
<g/>
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobrem	dobro	k1gNnSc7	dobro
(	(	kIx(	(
<g/>
bílou	bílý	k2eAgFnSc7d1	bílá
barvou	barva	k1gFnSc7	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
zóna	zóna	k1gFnSc1	zóna
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
pomezí	pomezí	k1gNnSc4	pomezí
mezi	mezi	k7c7	mezi
zákonným	zákonný	k2eAgInSc7d1	zákonný
a	a	k8xC	a
nezákonným	zákonný	k2eNgInSc7d1	nezákonný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šedou	šedý	k2eAgFnSc4d1	šedá
barvu	barva	k1gFnSc4	barva
měly	mít	k5eAaImAgFnP	mít
uniformy	uniforma	k1gFnPc1	uniforma
vojáků	voják	k1gMnPc2	voják
Konfederace	konfederace	k1gFnSc2	konfederace
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
vojáci	voják	k1gMnPc1	voják
Unie	unie	k1gFnSc2	unie
nosili	nosit	k5eAaImAgMnP	nosit
modré	modrý	k2eAgFnPc4d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidské	lidský	k2eAgInPc1d1	lidský
vlasy	vlas	k1gInPc1	vlas
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
šediví	šedivět	k5eAaImIp3nS	šedivět
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
šedá	šedý	k2eAgFnSc1d1	šedá
barva	barva	k1gFnSc1	barva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
stáří	stáří	k1gNnSc2	stáří
či	či	k8xC	či
vyzrálosti	vyzrálost	k1gFnSc2	vyzrálost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
přemýšlením	přemýšlení	k1gNnSc7	přemýšlení
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
šedá	šedat	k5eAaImIp3nS	šedat
kůra	kůra	k1gFnSc1	kůra
mozková	mozkový	k2eAgFnSc1d1	mozková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
šedá	šedý	k2eAgFnSc1d1	šedá
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
8	[number]	k4	8
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
šedá	šedat	k5eAaImIp3nS	šedat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
