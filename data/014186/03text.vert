<s>
Božena	Božena	k1gFnSc1
Tymichová	Tymichová	k1gFnSc1
</s>
<s>
Božena	Božena	k1gFnSc1
Tymichová	Tymichový	k2eAgFnSc1d1
Narození	narození	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
(	(	kIx(
<g/>
76	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Božena	Božena	k1gFnSc1
Tymichová	Tymichová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
pohádkových	pohádkový	k2eAgFnPc2d1
knížek	knížka	k1gFnPc2
<g/>
,	,	kIx,
výtvarnice	výtvarnice	k1gFnSc1
a	a	k8xC
divadelnice	divadelnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
otci	otec	k1gMnSc3
Janu	Jan	k1gMnSc3
Lukačinovi	Lukačin	k1gMnSc3
<g/>
,	,	kIx,
pocházejícímu	pocházející	k2eAgMnSc3d1
z	z	k7c2
Podkarpatské	podkarpatský	k2eAgFnSc2d1
Rusi	Rus	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Boženě	Božena	k1gFnSc3
Lukačinové	Lukačinový	k2eAgFnSc3d1
(	(	kIx(
<g/>
rozené	rozený	k2eAgFnSc3d1
Proškové	Prošková	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc4
krásné	krásný	k2eAgNnSc4d1
dětství	dětství	k1gNnSc4
prožila	prožít	k5eAaPmAgFnS
na	na	k7c6
pražských	pražský	k2eAgInPc6d1
Vinohradech	Vinohrady	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
jednoho	jeden	k4xCgMnSc4
bratra	bratr	k1gMnSc4
a	a	k8xC
dceru	dcera	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
žije	žít	k5eAaImIp3nS
již	již	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
a	a	k8xC
střední	střední	k2eAgFnSc6d1
pedagogické	pedagogický	k2eAgFnSc6d1
škole	škola	k1gFnSc6
začala	začít	k5eAaPmAgFnS
pracovat	pracovat	k5eAaImF
jako	jako	k9
učitelka	učitelka	k1gFnSc1
v	v	k7c6
mateřských	mateřský	k2eAgFnPc6d1
školách	škola	k1gFnPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
tak	tak	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
školství	školství	k1gNnSc2
strávila	strávit	k5eAaPmAgFnS
27	#num#	k4
let	rok	k1gInPc2
<g/>
,	,	kIx,
práci	práce	k1gFnSc4
s	s	k7c7
dětmi	dítě	k1gFnPc7
a	a	k8xC
mládeží	mládež	k1gFnSc7
se	se	k3xPyFc4
však	však	k9
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
již	již	k6eAd1
prostřednictvím	prostřednictvím	k7c2
svých	svůj	k3xOyFgFnPc2
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
ilustrací	ilustrace	k1gFnPc2
a	a	k8xC
loutkových	loutkový	k2eAgInPc2d1
a	a	k8xC
maňáskových	maňáskový	k2eAgInPc2d1
scénářů	scénář	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jako	jako	k9
cvičitelka	cvičitelka	k1gFnSc1
dětské	dětský	k2eAgFnSc2d1
jógy	jóga	k1gFnSc2
<g/>
,	,	kIx,
učitelka	učitelka	k1gFnSc1
flétny	flétna	k1gFnSc2
ve	v	k7c6
družině	družina	k1gFnSc6
a	a	k8xC
organizátorka	organizátorka	k1gFnSc1
různých	různý	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
působila	působit	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
manželem	manžel	k1gMnSc7
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
východočeského	východočeský	k2eAgNnSc2d1
Opočna	Opočno	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
záhy	záhy	k6eAd1
našla	najít	k5eAaPmAgFnS
práci	práce	k1gFnSc4
v	v	k7c6
kulturní	kulturní	k2eAgFnSc6d1
sekci	sekce	k1gFnSc6
a	a	k8xC
působila	působit	k5eAaImAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
až	až	k6eAd1
do	do	k7c2
odchodu	odchod	k1gInSc2
do	do	k7c2
důchodu	důchod	k1gInSc2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
spolku	spolek	k1gInSc2
Veselá	veselý	k2eAgFnSc1d1
kupa	kupa	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
na	na	k7c6
svém	svůj	k3xOyFgInSc6
programu	program	k1gInSc6
právě	právě	k6eAd1
různá	různý	k2eAgNnPc1d1
pohádková	pohádkový	k2eAgNnPc1d1
divadelní	divadelní	k2eAgNnPc1d1
představení	představení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prvním	první	k4xOgMnSc6
představením	představení	k1gNnSc7
patřila	patřit	k5eAaImAgFnS
například	například	k6eAd1
pohádka	pohádka	k1gFnSc1
Mrazík	mrazík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc7d1
cvičitelkou	cvičitelka	k1gFnSc7
jógy	jóga	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
kurzy	kurz	k1gInPc1
v	v	k7c6
Opočně	Opočno	k1gNnSc6
organizuje	organizovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
autorce	autorka	k1gFnSc6
vyšly	vyjít	k5eAaPmAgInP
dvě	dva	k4xCgFnPc4
knihy	kniha	k1gFnPc4
Pohádky	pohádka	k1gFnSc2
ze	z	k7c2
zámeckého	zámecký	k2eAgInSc2d1
parku	park	k1gInSc2
a	a	k8xC
Poselství	poselství	k1gNnSc1
z	z	k7c2
říše	říš	k1gFnSc2
skřítků	skřítek	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
knihy	kniha	k1gFnPc1
si	se	k3xPyFc3
sama	sám	k3xTgFnSc1
ilustrovala	ilustrovat	k5eAaBmAgFnS
a	a	k8xC
na	na	k7c4
motivy	motiv	k1gInPc4
první	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
vydala	vydat	k5eAaPmAgFnS
omalovánky	omalovánka	k1gFnPc4
„	„	k?
<g/>
Pohádky	pohádka	k1gFnPc4
ze	z	k7c2
zámeckého	zámecký	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Tvorba	tvorba	k1gFnSc1
je	být	k5eAaImIp3nS
inspirována	inspirovat	k5eAaBmNgFnS
zákoutími	zákoutí	k1gNnPc7
města	město	k1gNnSc2
Opočna	Opočno	k1gNnSc2
(	(	kIx(
<g/>
hlavně	hlavně	k6eAd1
parkem	park	k1gInSc7
a	a	k8xC
historickým	historický	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
města	město	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
ovlivněna	ovlivnit	k5eAaPmNgFnS
autorčinou	autorčin	k2eAgFnSc7d1
láskou	láska	k1gFnSc7
k	k	k7c3
přírodě	příroda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místa	místo	k1gNnPc1
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
místní	místní	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
jako	jako	k9
zdroj	zdroj	k1gInSc4
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
fantazii	fantazie	k1gFnSc4
<g/>
,	,	kIx,
metodou	metoda	k1gFnSc7
pro	pro	k7c4
její	její	k3xOp3gFnSc4
tvůrčí	tvůrčí	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
jí	on	k3xPp3gFnSc2
je	být	k5eAaImIp3nS
meditace	meditace	k1gFnPc4
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yRgFnSc3,k3yIgFnSc3,k3yQgFnSc3
se	se	k3xPyFc4
vlastně	vlastně	k9
k	k	k7c3
psaní	psaní	k1gNnSc3
knih	kniha	k1gFnPc2
dostala	dostat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
plánuje	plánovat	k5eAaImIp3nS
psaní	psaní	k1gNnSc4
další	další	k2eAgFnSc2d1
pohádkové	pohádkový	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
inspirována	inspirovat	k5eAaBmNgFnS
výstavou	výstava	k1gFnSc7
Františka	František	k1gMnSc2
Kupky	Kupka	k1gMnSc2
na	na	k7c6
Trčkově	Trčkův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Opočně	Opočno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Píše	psát	k5eAaImIp3nS
ale	ale	k8xC
také	také	k9
scénáře	scénář	k1gInPc4
k	k	k7c3
loutkovým	loutkový	k2eAgInPc3d1
a	a	k8xC
maňáskovým	maňáskový	k2eAgNnSc7d1
představením	představení	k1gNnSc7
v	v	k7c6
rámci	rámec	k1gInSc6
spolku	spolek	k1gInSc2
Divadélko	divadélko	k1gNnSc1
Batole	batole	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
k	k	k7c3
nim	on	k3xPp3gInPc3
scénu	scéna	k1gFnSc4
a	a	k8xC
kulisy	kulisa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
v	v	k7c6
divadélku	divadélko	k1gNnSc6
aktivně	aktivně	k6eAd1
působí	působit	k5eAaImIp3nS
jako	jako	k9
vodička	vodička	k1gFnSc1
loutek	loutka	k1gFnPc2
a	a	k8xC
organizátorka	organizátorka	k1gFnSc1
maňáskových	maňáskový	k2eAgNnPc2d1
představení	představení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
</s>
<s>
Pohádky	pohádka	k1gFnPc1
ze	z	k7c2
zámeckého	zámecký	k2eAgInSc2d1
parku	park	k1gInSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kniha	kniha	k1gFnSc1
Pohádky	pohádka	k1gFnSc2
ze	z	k7c2
zámeckého	zámecký	k2eAgInSc2d1
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
pohádkovou	pohádkový	k2eAgFnSc7d1
knížkou	knížka	k1gFnSc7
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děj	děj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
města	město	k1gNnSc2
Opočna	Opočno	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc6
blízkém	blízký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
najdeme	najít	k5eAaPmIp1nP
zde	zde	k6eAd1
zmínky	zmínka	k1gFnSc2
o	o	k7c6
celém	celý	k2eAgInSc6d1
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgFnPc7d1
postavami	postava	k1gFnPc7
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgMnPc1
skřítkové	skřítek	k1gMnPc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
a	a	k8xC
Klíček	klíček	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
dostali	dostat	k5eAaPmAgMnP
jméno	jméno	k1gNnSc4
po	po	k7c6
květině	květina	k1gFnSc6
petrklíč	petrklíč	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
a	a	k8xC
Klíček	klíček	k1gInSc1
se	se	k3xPyFc4
narodili	narodit	k5eAaPmAgMnP
týden	týden	k1gInSc4
před	před	k7c7
Štědrým	štědrý	k2eAgInSc7d1
večerem	večer	k1gInSc7
dvěma	dva	k4xCgMnPc3
lesním	lesní	k2eAgMnPc3d1
skřítkům	skřítek	k1gMnPc3
na	na	k7c6
louce	louka	k1gFnSc6
u	u	k7c2
opočenského	opočenský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
lesní	lesní	k2eAgMnPc1d1
skřítci	skřítek	k1gMnPc1
rostou	růst	k5eAaImIp3nP
rychleji	rychle	k6eAd2
než	než	k8xS
lidské	lidský	k2eAgFnPc1d1
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
září	září	k1gNnSc6
už	už	k6eAd1
mohou	moct	k5eAaImIp3nP
začít	začít	k5eAaPmF
chodit	chodit	k5eAaImF
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prožívají	prožívat	k5eAaImIp3nP
společně	společně	k6eAd1
různá	různý	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
prokládána	prokládán	k2eAgNnPc1d1
vyprávěními	vyprávění	k1gNnPc7
starších	starý	k2eAgMnPc2d2
skřítků	skřítek	k1gMnPc2
o	o	k7c6
vzniku	vznik	k1gInSc6
různých	různý	k2eAgInPc2d1
stromů	strom	k1gInPc2
a	a	k8xC
skal	skála	k1gFnPc2
v	v	k7c6
opočenském	opočenský	k2eAgInSc6d1
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíček	klíček	k1gInSc1
odejde	odejít	k5eAaPmIp3nS
na	na	k7c4
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yRgInPc2,k3yIgInPc2,k3yQgInPc2
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
knihy	kniha	k1gFnSc2
vrátí	vrátit	k5eAaPmIp3nS
a	a	k8xC
děj	děj	k1gInSc1
knihy	kniha	k1gFnSc2
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
Klíček	klíček	k1gInSc1
pořádá	pořádat	k5eAaImIp3nS
Pomněnku	pomněnka	k1gFnSc4
o	o	k7c4
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
maminka	maminka	k1gFnSc1
Violka	violka	k1gFnSc1
<g/>
,	,	kIx,
teta	teta	k1gFnSc1
Hortensie	Hortensie	k1gFnSc1
<g/>
,	,	kIx,
kamarádka	kamarádka	k1gFnSc1
Pomněnka	pomněnka	k1gFnSc1
<g/>
,	,	kIx,
nesou	nést	k5eAaImIp3nP
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Petr	Petr	k1gMnSc1
a	a	k8xC
Klíček	klíček	k1gInSc1
<g/>
,	,	kIx,
také	také	k9
jména	jméno	k1gNnSc2
po	po	k7c6
květinách	květina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
knize	kniha	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
kromě	kromě	k7c2
skřítků	skřítek	k1gMnPc2
nalézt	nalézt	k5eAaPmF,k5eAaBmF
i	i	k9
další	další	k2eAgFnPc1d1
pohádkové	pohádkový	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
hejkala	hejkal	k1gMnSc4
<g/>
,	,	kIx,
vodníka	vodník	k1gMnSc4
<g/>
,	,	kIx,
čarodějnici	čarodějnice	k1gFnSc4
nebo	nebo	k8xC
čerta	čert	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
pohádková	pohádkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
najdeme	najít	k5eAaPmIp1nP
v	v	k7c6
ní	on	k3xPp3gFnSc6
i	i	k9
spoustu	spousta	k1gFnSc4
pověstí	pověst	k1gFnPc2
ze	z	k7c2
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
knihy	kniha	k1gFnSc2
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
autorčin	autorčin	k2eAgInSc1d1
postoj	postoj	k1gInSc1
zejména	zejména	k9
k	k	k7c3
přírodě	příroda	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
životu	život	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
i	i	k8xC
zmínky	zmínka	k1gFnPc4
o	o	k7c4
umění	umění	k1gNnSc4
Feng	Fenga	k1gFnPc2
shui	shui	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
knihy	kniha	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
autorčin	autorčin	k2eAgInSc1d1
doslov	doslov	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
meditovat	meditovat	k5eAaImF
a	a	k8xC
načerpat	načerpat	k5eAaPmF
novou	nový	k2eAgFnSc4d1
životní	životní	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Celá	celý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
ilustrace	ilustrace	k1gFnPc4
samotné	samotný	k2eAgFnSc2d1
autorky	autorka	k1gFnSc2
a	a	k8xC
fotografie	fotografia	k1gFnSc2
paličkovaných	paličkovaný	k2eAgInPc2d1
výtvorů	výtvor	k1gInPc2
různých	různý	k2eAgFnPc2d1
autorek	autorka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorka	autorka	k1gFnSc1
na	na	k7c4
konec	konec	k1gInSc4
knihy	kniha	k1gFnSc2
přidala	přidat	k5eAaPmAgFnS
také	také	k9
kreslenou	kreslený	k2eAgFnSc4d1
mapu	mapa	k1gFnSc4
zámeckého	zámecký	k2eAgInSc2d1
parku	park	k1gInSc2
a	a	k8xC
pár	pár	k4xCyI
prázdných	prázdný	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgFnPc2,k3yRgFnPc2,k3yIgFnPc2
si	se	k3xPyFc3
děti	dítě	k1gFnPc1
mohou	moct	k5eAaImIp3nP
namalovat	namalovat	k5eAaPmF
vlastní	vlastní	k2eAgFnSc4d1
pohádku	pohádka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
příloze	příloha	k1gFnSc6
knihy	kniha	k1gFnSc2
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
fakta	faktum	k1gNnPc1
o	o	k7c4
městu	město	k1gNnSc3
Opočně	Opočno	k1gNnSc6
a	a	k8xC
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
jsou	být	k5eAaImIp3nP
čerpána	čerpat	k5eAaImNgNnP
z	z	k7c2
Průvodce	průvodka	k1gFnSc3
Opočenskou	opočenský	k2eAgFnSc7d1
naučnou	naučný	k2eAgFnSc7d1
stezkou	stezka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Poselství	poselství	k1gNnSc1
z	z	k7c2
říše	říš	k1gFnSc2
skřítků	skřítek	k1gMnPc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pohádková	pohádkový	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorka	autorka	k1gFnSc1
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
tyto	tento	k3xDgInPc4
pohádkové	pohádkový	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
na	na	k7c6
základě	základ	k1gInSc6
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
žáky	žák	k1gMnPc7
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
ZŠ	ZŠ	kA
Opočno	Opočno	k1gNnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
družiny	družina	k1gFnSc2
za	za	k7c2
pomoci	pomoc	k1gFnSc2
učitelky	učitelka	k1gFnSc2
Nadi	Naďa	k1gFnSc2
Burýškové	Burýšková	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupráce	spolupráce	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
na	na	k7c6
základě	základ	k1gInSc6
„	„	k?
<g/>
literární	literární	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dospělý	dospělý	k1gMnSc1
navodí	navodit	k5eAaPmIp3nS,k5eAaBmIp3nS
téma	téma	k1gNnSc4
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
dětmi	dítě	k1gFnPc7
dotvoří	dotvořit	k5eAaPmIp3nS
děj	děj	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Tento	tento	k3xDgInSc4
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
děti	dítě	k1gFnPc1
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
kreativního	kreativní	k2eAgInSc2d1
procesu	proces	k1gInSc2
vytváření	vytváření	k1gNnSc2
pohádek	pohádka	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
pozitivně	pozitivně	k6eAd1
ohodnocen	ohodnotit	k5eAaPmNgMnS
tamní	tamní	k2eAgFnSc7d1
ředitelkou	ředitelka	k1gFnSc7
i	i	k8xC
starostou	starosta	k1gMnSc7
Opočna	Opočno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
k	k	k7c3
pohádkám	pohádka	k1gFnPc3
vytvořila	vytvořit	k5eAaPmAgFnS
sama	sám	k3xTgFnSc1
autorka	autorka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
nich	on	k3xPp3gInPc2
jsou	být	k5eAaImIp3nP
provedeny	provést	k5eAaPmNgInP
pouze	pouze	k6eAd1
černobíle	černobíle	k6eAd1
a	a	k8xC
slouží	sloužit	k5eAaImIp3nP
dětem	dítě	k1gFnPc3
jako	jako	k9
omalovánky	omalovánka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtenáři	čtenář	k1gMnSc3
se	se	k3xPyFc4
tedy	tedy	k9
mohou	moct	k5eAaImIp3nP
účastnit	účastnit	k5eAaImF
příběhu	příběh	k1gInSc3
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
své	svůj	k3xOyFgFnSc2
fantazie	fantazie	k1gFnSc2
vybarví	vybarvit	k5eAaPmIp3nS
tyto	tento	k3xDgInPc4
obrázky	obrázek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohádky	pohádka	k1gFnSc2
se	se	k3xPyFc4
inspirují	inspirovat	k5eAaBmIp3nP
reálným	reálný	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
města	město	k1gNnSc2
Opočna	Opočno	k1gNnSc2
<g/>
,	,	kIx,
odehrávají	odehrávat	k5eAaImIp3nP
se	se	k3xPyFc4
např.	např.	kA
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
,	,	kIx,
kolem	kolem	k7c2
Zlatého	zlatý	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
pobývají	pobývat	k5eAaImIp3nP
městští	městský	k2eAgMnPc1d1
skřítci	skřítek	k1gMnPc1
<g/>
,	,	kIx,
o	o	k7c6
jejichž	jejichž	k3xOyRp3gNnPc6
dobrodružstvích	dobrodružství	k1gNnPc6
kniha	kniha	k1gFnSc1
vypráví	vyprávět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městští	městský	k2eAgMnPc1d1
skřítci	skřítek	k1gMnPc1
bydlí	bydlet	k5eAaImIp3nP
v	v	k7c6
domech	dům	k1gInPc6
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
ti	ten	k3xDgMnPc1
o	o	k7c4
jejich	jejich	k3xOp3gFnSc4
existenci	existence	k1gFnSc4
nevědí	vědět	k5eNaImIp3nP
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
skřítci	skřítek	k1gMnPc1
jsou	být	k5eAaImIp3nP
neviditelní	viditelný	k2eNgMnPc1d1
pro	pro	k7c4
lidské	lidský	k2eAgNnSc4d1
oko	oko	k1gNnSc4
a	a	k8xC
skřítci	skřítek	k1gMnPc1
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
uctivě	uctivě	k6eAd1
k	k	k7c3
lidským	lidský	k2eAgFnPc3d1
věcem	věc	k1gFnPc3
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
si	se	k3xPyFc3
půjčí	půjčit	k5eAaPmIp3nS
<g/>
,	,	kIx,
vrací	vracet	k5eAaImIp3nS
na	na	k7c4
stejné	stejný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skřítci	skřítek	k1gMnPc1
mají	mít	k5eAaImIp3nP
velmi	velmi	k6eAd1
rádi	rád	k2eAgMnPc1d1
své	svůj	k3xOyFgMnPc4
domácí	domácí	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
nazývají	nazývat	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
nic	nic	k6eAd1
netušící	tušící	k2eNgMnPc4d1
hostitele	hostitel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgMnPc7d1
hrdiny	hrdina	k1gMnPc7
na	na	k7c6
sebe	sebe	k3xPyFc4
navazujících	navazující	k2eAgInPc2d1
pohádkových	pohádkový	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
jsou	být	k5eAaImIp3nP
členové	člen	k1gMnPc1
rodiny	rodina	k1gFnSc2
skřítků	skřítek	k1gMnPc2
z	z	k7c2
neosídleného	osídlený	k2eNgInSc2d1
domku	domek	k1gInSc2
U	u	k7c2
Zvonice	zvonice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
pět	pět	k4xCc4
skřítků	skřítek	k1gMnPc2
<g/>
,	,	kIx,
maminka	maminka	k1gFnSc1
<g/>
,	,	kIx,
tatínek	tatínek	k1gMnSc1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
tři	tři	k4xCgFnPc1
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
prostřední	prostřední	k2eAgMnSc1d1
Vítek	Vítek	k1gMnSc1
a	a	k8xC
nejmenší	malý	k2eAgFnSc1d3
Anička	Anička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
sourozenci	sourozenec	k1gMnPc1
spolu	spolu	k6eAd1
s	s	k7c7
dětmi	dítě	k1gFnPc7
ze	z	k7c2
skřítkovské	skřítkovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
z	z	k7c2
vedlejšího	vedlejší	k2eAgInSc2d1
domu	dům	k1gInSc2
zažívají	zažívat	k5eAaImIp3nP
spoustu	spousta	k1gFnSc4
dobrodružství	dobrodružství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rádi	rád	k2eAgMnPc1d1
chodí	chodit	k5eAaImIp3nP
společně	společně	k6eAd1
na	na	k7c4
výlety	výlet	k1gInPc4
<g/>
,	,	kIx,
hrají	hrát	k5eAaImIp3nP
divadlo	divadlo	k1gNnSc4
<g/>
,	,	kIx,
vyprávějí	vyprávět	k5eAaImIp3nP
si	se	k3xPyFc3
strašidelné	strašidelný	k2eAgFnPc4d1
historky	historka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohádkové	pohádkový	k2eAgInPc1d1
příběhy	příběh	k1gInPc1
učí	učit	k5eAaImIp3nP
své	svůj	k3xOyFgMnPc4
čtenáře	čtenář	k1gMnPc4
důležitých	důležitý	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
přátelství	přátelství	k1gNnSc1
<g/>
,	,	kIx,
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
úcta	úcta	k1gFnSc1
ke	k	k7c3
starší	starý	k2eAgFnSc3d2
generaci	generace	k1gFnSc3
(	(	kIx(
<g/>
k	k	k7c3
prarodičům	prarodič	k1gMnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
potřeba	potřeba	k6eAd1
pomáhat	pomáhat	k5eAaImF
si	se	k3xPyFc3
mezi	mezi	k7c7
sourozenci	sourozenec	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
poslouchat	poslouchat	k5eAaImF
rodiče	rodič	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Tymichová	Tymichový	k2eAgFnSc1d1
Božena	Božena	k1gFnSc1
-	-	kIx~
Středisko	středisko	k1gNnSc1
východočeských	východočeský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
www.spisovatelevc.cz	www.spisovatelevc.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŠNAJDR	Šnajdr	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prázdninové	prázdninový	k2eAgNnSc4d1
putování	putování	k1gNnSc4
Za	za	k7c7
tajemstvím	tajemství	k1gNnSc7
opočenských	opočenský	k2eAgMnPc2d1
skřítků	skřítek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orlický	orlický	k2eAgInSc1d1
týdeník	týdeník	k1gInSc1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
částečně	částečně	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
rozhovoru	rozhovor	k1gInSc2
s	s	k7c7
autorkou	autorka	k1gFnSc7
a	a	k8xC
na	na	k7c6
rozboru	rozbor	k1gInSc6
autorčina	autorčin	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TYMICHOVÁ	TYMICHOVÁ	kA
<g/>
,	,	kIx,
Božena	Božena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poselství	poselství	k1gNnPc1
z	z	k7c2
říše	říš	k1gFnSc2
skřítků	skřítek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opočno	Opočno	k1gNnSc1
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Opočno	Opočno	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
260	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
726	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TYMICHOVÁ	TYMICHOVÁ	kA
<g/>
,	,	kIx,
Božena	Božena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohádky	pohádka	k1gFnSc2
ze	z	k7c2
zámeckého	zámecký	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opočno	Opočno	k1gNnSc1
<g/>
:	:	kIx,
o.	o.	k?
s.	s.	k?
ABAKUS	abakus	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
6075	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20100111014	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7541	#num#	k4
9122	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
106939162	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
