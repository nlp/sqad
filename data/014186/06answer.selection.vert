<s desamb="1">
Celkově	celkově	k6eAd1
tak	tak	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
školství	školství	k1gNnSc2
strávila	strávit	k5eAaPmAgFnS
27	#num#	k4
let	rok	k1gInPc2
<g/>
,	,	kIx,
práci	práce	k1gFnSc4
s	s	k7c7
dětmi	dítě	k1gFnPc7
a	a	k8xC
mládeží	mládež	k1gFnSc7
se	se	k3xPyFc4
však	však	k9
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
již	již	k6eAd1
prostřednictvím	prostřednictvím	k7c2
svých	svůj	k3xOyFgFnPc2
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
ilustrací	ilustrace	k1gFnPc2
a	a	k8xC
loutkových	loutkový	k2eAgInPc2d1
a	a	k8xC
maňáskových	maňáskový	k2eAgInPc2d1
scénářů	scénář	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jako	jako	k9
cvičitelka	cvičitelka	k1gFnSc1
dětské	dětský	k2eAgFnSc2d1
jógy	jóga	k1gFnSc2
<g/>
,	,	kIx,
učitelka	učitelka	k1gFnSc1
flétny	flétna	k1gFnSc2
ve	v	k7c6
družině	družina	k1gFnSc6
a	a	k8xC
organizátorka	organizátorka	k1gFnSc1
různých	různý	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>