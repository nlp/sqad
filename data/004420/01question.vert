<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Hrubého	Hrubého	k2eAgInSc2d1	Hrubého
Jeseníku	Jeseník	k1gInSc2	Jeseník
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
vůbec	vůbec	k9	vůbec
<g/>
?	?	kIx.	?
</s>
