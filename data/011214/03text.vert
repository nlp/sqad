<p>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
dovolená	dovolená	k1gFnSc1	dovolená
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc4	období
dovolené	dovolená	k1gFnSc2	dovolená
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rodiči	rodič	k1gMnPc7	rodič
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
a	a	k8xC	a
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
počítat	počítat	k5eAaImF	počítat
nejdříve	dříve	k6eAd3	dříve
osm	osm	k4xCc4	osm
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
však	však	k9	však
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
termínem	termín	k1gInSc7	termín
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
týdnů	týden	k1gInPc2	týden
při	při	k7c6	při
narození	narození	k1gNnSc2	narození
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
37	[number]	k4	37
týdnu	týden	k1gInSc6	týden
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dvou	dva	k4xCgNnPc2	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mateřské	mateřský	k2eAgFnSc2d1	mateřská
dovolené	dovolená	k1gFnSc2	dovolená
vyplácí	vyplácet	k5eAaImIp3nS	vyplácet
stát	stát	k5eAaImF	stát
rodiči	rodič	k1gMnSc3	rodič
peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
<g/>
.	.	kIx.	.
</s>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nevzniká	vznikat	k5eNaImIp3nS	vznikat
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Peněžitá	peněžitý	k2eAgFnSc1d1	peněžitá
pomoc	pomoc	k1gFnSc1	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
==	==	k?	==
</s>
</p>
<p>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
mají	mít	k5eAaImIp3nP	mít
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
zaměstnaneckém	zaměstnanecký	k2eAgInSc6d1	zaměstnanecký
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgNnSc4	který
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
odváděl	odvádět	k5eAaImAgMnS	odvádět
nemocenské	nemocenský	k2eAgNnSc4d1	nemocenský
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ženy	žena	k1gFnPc1	žena
musí	muset	k5eAaImIp3nP	muset
dále	daleko	k6eAd2	daleko
splňovat	splňovat	k5eAaImF	splňovat
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nemocenské	nemocenský	k2eAgNnSc4d1	nemocenský
pojištění	pojištění	k1gNnSc4	pojištění
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
odváděl	odvádět	k5eAaImAgMnS	odvádět
alespoň	alespoň	k9	alespoň
270	[number]	k4	270
dní	den	k1gInPc2	den
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
dovolenou	dovolená	k1gFnSc4	dovolená
byla	být	k5eAaImAgFnS	být
žena	žena	k1gFnSc1	žena
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
nebyl	být	k5eNaImAgInS	být
její	její	k3xOp3gInSc1	její
pracovní	pracovní	k2eAgInSc1d1	pracovní
poměr	poměr	k1gInSc1	poměr
ukončení	ukončení	k1gNnSc2	ukončení
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
před	před	k7c7	před
180	[number]	k4	180
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
ženy	žena	k1gFnPc1	žena
pracující	pracující	k2eAgFnSc1d1	pracující
jako	jako	k8xC	jako
OSVČ	OSVČ	kA	OSVČ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ženy	žena	k1gFnPc1	žena
musí	muset	k5eAaImIp3nP	muset
dále	daleko	k6eAd2	daleko
splňovat	splňovat	k5eAaImF	splňovat
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Žena	žena	k1gFnSc1	žena
pracující	pracující	k2eAgFnSc1d1	pracující
jako	jako	k8xC	jako
OSVČ	OSVČ	kA	OSVČ
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
minimálně	minimálně	k6eAd1	minimálně
270	[number]	k4	270
dní	den	k1gInPc2	den
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
platit	platit	k5eAaImF	platit
dobrovolně	dobrovolně	k6eAd1	dobrovolně
nemocenské	nemocenský	k2eAgNnSc4d1	nemocenský
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alespoň	alespoň	k9	alespoň
180	[number]	k4	180
dní	den	k1gInPc2	den
z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
270	[number]	k4	270
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
<g/>
.	.	kIx.	.
<g/>
Ani	ani	k8xC	ani
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
nesplní	splnit	k5eNaPmIp3nS	splnit
<g/>
,	,	kIx,	,
nezůstane	zůstat	k5eNaPmIp3nS	zůstat
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
má	mít	k5eAaImIp3nS	mít
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dítěte	dítě	k1gNnSc2	dítě
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
rodičovský	rodičovský	k2eAgInSc4d1	rodičovský
příspěvek	příspěvek	k1gInSc4	příspěvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
činí	činit	k5eAaImIp3nS	činit
220	[number]	k4	220
tisíc	tisíc	k4xCgInPc2	tisíc
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
rozložit	rozložit	k5eAaPmF	rozložit
mezi	mezi	k7c4	mezi
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
až	až	k6eAd1	až
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
může	moct	k5eAaImIp3nS	moct
čerpat	čerpat	k5eAaImF	čerpat
také	také	k9	také
otec	otec	k1gMnSc1	otec
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
on	on	k3xPp3gMnSc1	on
musí	muset	k5eAaImIp3nS	muset
splnit	splnit	k5eAaPmF	splnit
výše	vysoce	k6eAd2	vysoce
popsané	popsaný	k2eAgFnPc4d1	popsaná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Čerpat	čerpat	k5eAaImF	čerpat
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
matka	matka	k1gFnSc1	matka
podmínky	podmínka	k1gFnSc2	podmínka
nesplní	splnit	k5eNaPmIp3nS	splnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
doba	doba	k1gFnSc1	doba
pobírání	pobírání	k1gNnSc2	pobírání
peněžité	peněžitý	k2eAgFnSc2d1	peněžitá
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
dítěte	dítě	k1gNnSc2	dítě
čerpá	čerpat	k5eAaImIp3nS	čerpat
peněžní	peněžní	k2eAgFnSc4d1	peněžní
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
nejdříve	dříve	k6eAd3	dříve
od	od	k7c2	od
7	[number]	k4	7
týdne	týden	k1gInSc2	týden
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
22	[number]	k4	22
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
31	[number]	k4	31
týdnů	týden	k1gInPc2	týden
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
vícerčat	vícerče	k1gNnPc2	vícerče
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
zákoník	zákoník	k1gInSc1	zákoník
práce	práce	k1gFnPc4	práce
stanoví	stanovit	k5eAaPmIp3nS	stanovit
kratší	krátký	k2eAgFnSc4d2	kratší
dobu	doba	k1gFnSc4	doba
mateřské	mateřský	k2eAgFnSc2d1	mateřská
dovolené	dovolená	k1gFnSc2	dovolená
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
narodilo	narodit	k5eAaPmAgNnS	narodit
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
,	,	kIx,	,
mateřské	mateřský	k2eAgFnPc1d1	mateřská
dovolená	dovolená	k1gFnSc1	dovolená
trvá	trvat	k5eAaImIp3nS	trvat
jen	jen	k9	jen
14	[number]	k4	14
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
dovolená	dovolená	k1gFnSc1	dovolená
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
kratší	krátký	k2eAgFnSc4d2	kratší
než	než	k8xS	než
14	[number]	k4	14
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
skončit	skončit	k5eAaPmF	skončit
ani	ani	k8xC	ani
být	být	k5eAaImF	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
před	před	k7c7	před
uplynutím	uplynutí	k1gNnSc7	uplynutí
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
poskytnout	poskytnout	k5eAaPmF	poskytnout
zaměstnankyni	zaměstnankyně	k1gFnSc4	zaměstnankyně
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
dovolenou	dovolená	k1gFnSc4	dovolená
i	i	k9	i
bez	bez	k7c2	bez
její	její	k3xOp3gFnSc2	její
žádosti	žádost	k1gFnSc2	žádost
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnankyně	zaměstnankyně	k1gFnSc1	zaměstnankyně
nemusí	muset	k5eNaImIp3nS	muset
o	o	k7c6	o
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
mateřské	mateřský	k2eAgFnSc2d1	mateřská
dovolené	dovolená	k1gFnSc2	dovolená
zaměstnavatele	zaměstnavatel	k1gMnSc4	zaměstnavatel
žádat	žádat	k5eAaImF	žádat
<g/>
.	.	kIx.	.
</s>
<s>
Postačí	postačit	k5eAaPmIp3nS	postačit
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaměstnavateli	zaměstnavatel	k1gMnSc3	zaměstnavatel
nástup	nástup	k1gInSc4	nástup
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
dovolenou	dovolená	k1gFnSc4	dovolená
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
.	.	kIx.	.
<g/>
Doba	doba	k1gFnSc1	doba
čerpání	čerpání	k1gNnSc2	čerpání
mateřské	mateřský	k2eAgFnSc2d1	mateřská
dovolené	dovolená	k1gFnSc2	dovolená
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
dovolené	dovolená	k1gFnSc2	dovolená
na	na	k7c4	na
zotavenou	zotavená	k1gFnSc4	zotavená
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
jako	jako	k9	jako
výkon	výkon	k1gInSc4	výkon
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
započítává	započítávat	k5eAaImIp3nS	započítávat
do	do	k7c2	do
dnů	den	k1gInPc2	den
podmiňujících	podmiňující	k2eAgInPc2d1	podmiňující
vznik	vznik	k1gInSc4	vznik
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
-li	i	k?	-li
zaměstnankyně	zaměstnankyně	k1gFnSc2	zaměstnankyně
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
o	o	k7c4	o
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
dovolené	dovolená	k1gFnSc2	dovolená
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navazovala	navazovat	k5eAaImAgFnS	navazovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c6	na
skončení	skončení	k1gNnSc6	skončení
mateřské	mateřský	k2eAgFnSc2d1	mateřská
dovolené	dovolená	k1gFnSc2	dovolená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
povinen	povinen	k2eAgMnSc1d1	povinen
její	její	k3xOp3gFnSc1	její
žádosti	žádost	k1gFnSc2	žádost
vyhovět	vyhovět	k5eAaPmF	vyhovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výše	výše	k1gFnSc1	výše
peněžité	peněžitý	k2eAgFnSc2d1	peněžitá
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
peněžité	peněžitý	k2eAgFnSc2d1	peněžitá
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
zaměstnané	zaměstnaný	k2eAgFnSc2d1	zaměstnaná
ženy	žena	k1gFnSc2	žena
důležité	důležitý	k2eAgInPc4d1	důležitý
předchozí	předchozí	k2eAgInPc4d1	předchozí
výdělky	výdělek	k1gInPc4	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
hrubým	hrubý	k2eAgInSc7d1	hrubý
příjmem	příjem	k1gInSc7	příjem
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
je	být	k5eAaImIp3nS	být
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
denního	denní	k2eAgInSc2d1	denní
vyměřovacího	vyměřovací	k2eAgInSc2d1	vyměřovací
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
snižuje	snižovat	k5eAaImIp3nS	snižovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
redukčních	redukční	k2eAgFnPc2d1	redukční
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hranice	hranice	k1gFnPc1	hranice
vypadají	vypadat	k5eAaPmIp3nP	vypadat
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2018	[number]	k4	2018
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
z	z	k7c2	z
částky	částka	k1gFnSc2	částka
do	do	k7c2	do
1000	[number]	k4	1000
Kč	Kč	kA	Kč
se	se	k3xPyFc4	se
započítá	započítat	k5eAaPmIp3nS	započítat
100	[number]	k4	100
%	%	kIx~	%
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
částky	částka	k1gFnSc2	částka
nad	nad	k7c7	nad
1000	[number]	k4	1000
Kč	Kč	kA	Kč
do	do	k7c2	do
1499	[number]	k4	1499
Kč	Kč	kA	Kč
se	se	k3xPyFc4	se
započítá	započítat	k5eAaPmIp3nS	započítat
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
částky	částka	k1gFnSc2	částka
na	na	k7c4	na
1499	[number]	k4	1499
Kč	Kč	kA	Kč
do	do	k7c2	do
2889	[number]	k4	2889
Kč	Kč	kA	Kč
započítá	započítat	k5eAaPmIp3nS	započítat
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
k	k	k7c3	k
částce	částka	k1gFnSc3	částka
nad	nad	k7c4	nad
2889	[number]	k4	2889
Kč	Kč	kA	Kč
se	se	k3xPyFc4	se
nepřihlíží	přihlížet	k5eNaImIp3nS	přihlížet
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
jako	jako	k9	jako
OSVČ	OSVČ	kA	OSVČ
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
peněžité	peněžitý	k2eAgFnSc2d1	peněžitá
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
výše	výše	k1gFnSc2	výše
nemocenského	mocenský	k2eNgNnSc2d1	nemocenské
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
minimálních	minimální	k2eAgFnPc6d1	minimální
platbách	platba	k1gFnPc6	platba
za	za	k7c4	za
nemocenskou	nemocenská	k1gFnSc4	nemocenská
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
činí	činit	k5eAaImIp3nS	činit
115	[number]	k4	115
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
peněžitá	peněžitý	k2eAgFnSc1d1	peněžitá
pomoc	pomoc	k1gFnSc1	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
celkem	celek	k1gInSc7	celek
na	na	k7c4	na
22736	[number]	k4	22736
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnSc1	práce
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
dovolená	dovolená	k1gFnSc1	dovolená
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
dovolenou	dovolená	k1gFnSc4	dovolená
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
<g/>
,	,	kIx,	,
o	o	k7c4	o
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
nepřijde	přijít	k5eNaPmIp3nS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
tehdy	tehdy	k6eAd1	tehdy
nesmí	smět	k5eNaImIp3nS	smět
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
mateřské	mateřský	k2eAgNnSc1d1	mateřské
propustit	propustit	k5eAaPmF	propustit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jí	on	k3xPp3gFnSc3	on
musí	muset	k5eAaImIp3nS	muset
podržet	podržet	k5eAaPmF	podržet
stejné	stejný	k2eAgNnSc4d1	stejné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc4	jaký
měla	mít	k5eAaImAgFnS	mít
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
navazuje	navazovat	k5eAaImIp3nS	navazovat
matka	matka	k1gFnSc1	matka
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
dovolenou	dovolená	k1gFnSc4	dovolená
rodičovskou	rodičovský	k2eAgFnSc4d1	rodičovská
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
až	až	k9	až
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
či	či	k8xC	či
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
jen	jen	k9	jen
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
náplni	náplň	k1gFnSc3	náplň
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
není	být	k5eNaImIp3nS	být
již	již	k9	již
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
povinen	povinen	k2eAgMnSc1d1	povinen
místo	místo	k6eAd1	místo
držet	držet	k5eAaImF	držet
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
však	však	k9	však
žena	žena	k1gFnSc1	žena
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
nebo	nebo	k8xC	nebo
rodičovskou	rodičovský	k2eAgFnSc4d1	rodičovská
dovolenou	dovolená	k1gFnSc4	dovolená
s	s	k7c7	s
pracovní	pracovní	k2eAgFnSc7d1	pracovní
smlouvou	smlouva	k1gFnSc7	smlouva
podepsanou	podepsaný	k2eAgFnSc7d1	podepsaná
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
určitou	určitý	k2eAgFnSc4d1	určitá
<g/>
,	,	kIx,	,
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
vypršení	vypršení	k1gNnSc6	vypršení
nemusí	muset	k5eNaImIp3nS	muset
prodloužit	prodloužit	k5eAaPmF	prodloužit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
mateřské	mateřský	k2eAgFnSc2d1	mateřská
dovolené	dovolená	k1gFnSc2	dovolená
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgNnSc4	který
ženám	žena	k1gFnPc3	žena
obvykle	obvykle	k6eAd1	obvykle
náleží	náležet	k5eAaImIp3nS	náležet
peněžitá	peněžitý	k2eAgFnSc1d1	peněžitá
pomoc	pomoc	k1gFnSc1	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
také	také	k6eAd1	také
přivydělávat	přivydělávat	k5eAaImF	přivydělávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
však	však	k9	však
nesmí	smět	k5eNaImIp3nS	smět
pracovat	pracovat	k5eAaImF	pracovat
u	u	k7c2	u
stejného	stejné	k1gNnSc2	stejné
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
na	na	k7c4	na
zkrácený	zkrácený	k2eAgInSc4d1	zkrácený
úvazek	úvazek	k1gInSc4	úvazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
jinou	jiný	k2eAgFnSc4d1	jiná
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
pozici	pozice	k1gFnSc4	pozice
může	moct	k5eAaImIp3nS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
u	u	k7c2	u
jiného	jiný	k2eAgMnSc2d1	jiný
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
její	její	k3xOp3gMnSc1	její
současný	současný	k2eAgMnSc1d1	současný
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
pracující	pracující	k2eAgFnSc1d1	pracující
jako	jako	k8xC	jako
OSVČ	OSVČ	kA	OSVČ
nesmí	smět	k5eNaImIp3nS	smět
vůbec	vůbec	k9	vůbec
vykonávat	vykonávat	k5eAaImF	vykonávat
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
<g/>
,	,	kIx,	,
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
nebo	nebo	k8xC	nebo
publicistickou	publicistický	k2eAgFnSc4d1	publicistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Otcovská	otcovský	k2eAgFnSc1d1	otcovská
dovolená	dovolená	k1gFnSc1	dovolená
</s>
</p>
<p>
<s>
Rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
dovolená	dovolená	k1gFnSc1	dovolená
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
správa	správa	k1gFnSc1	správa
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
:	:	kIx,	:
Peněžitá	peněžitý	k2eAgFnSc1d1	peněžitá
pomoc	pomoc	k1gFnSc1	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
</s>
</p>
<p>
<s>
Odborně	odborně	k6eAd1	odborně
garantovaný	garantovaný	k2eAgInSc1d1	garantovaný
portál	portál	k1gInSc1	portál
www.babyonline.cz	www.babyonline.cza	k1gFnPc2	www.babyonline.cza
</s>
</p>
<p>
<s>
Praktické	praktický	k2eAgFnPc1d1	praktická
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
mateřské	mateřský	k2eAgFnSc6d1	mateřská
dovolené	dovolená	k1gFnSc6	dovolená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
</s>
</p>
