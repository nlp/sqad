<p>
<s>
Za	za	k7c4	za
homografy	homograf	k1gMnPc4	homograf
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ὁ	ὁ	k?	ὁ
<g/>
,	,	kIx,	,
homos	homos	k1gInSc1	homos
–	–	k?	–
"	"	kIx"	"
<g/>
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
"	"	kIx"	"
a	a	k8xC	a
γ	γ	k?	γ
<g/>
,	,	kIx,	,
grafó	grafó	k?	grafó
–	–	k?	–
"	"	kIx"	"
<g/>
píši	psát	k5eAaImIp1nS	psát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
označována	označován	k2eAgNnPc1d1	označováno
odlišná	odlišný	k2eAgNnPc1d1	odlišné
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
shodně	shodně	k6eAd1	shodně
píší	psát	k5eAaImIp3nP	psát
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	s	k7c7	s
–	–	k?	–
vedle	vedle	k7c2	vedle
slov	slovo	k1gNnPc2	slovo
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
vyslovujících	vyslovující	k2eAgInPc2d1	vyslovující
<g/>
,	,	kIx,	,
homofonů	homofon	k1gInPc2	homofon
–	–	k?	–
o	o	k7c4	o
nepravá	pravý	k2eNgNnPc4d1	nepravé
homonyma	homonymum	k1gNnPc4	homonymum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pravá	pravý	k2eAgNnPc4d1	pravé
homonyma	homonymum	k1gNnPc4	homonymum
jsou	být	k5eAaImIp3nP	být
pak	pak	k9	pak
považována	považován	k2eAgNnPc1d1	považováno
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
písemnou	písemný	k2eAgFnSc4d1	písemná
i	i	k8xC	i
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
jen	jen	k9	jen
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
čisté	čistý	k2eAgInPc1d1	čistý
homografy	homograf	k1gInPc1	homograf
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
a	a	k8xC	a
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
si	se	k3xPyFc3	se
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc4d1	různé
slova	slovo	k1gNnPc4	slovo
panický	panický	k2eAgInSc1d1	panický
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
panický	panický	k2eAgMnSc1d1	panický
<g/>
]	]	kIx)	]
od	od	k7c2	od
panic	panice	k1gFnPc2	panice
×	×	k?	×
[	[	kIx(	[
<g/>
panycký	panycký	k1gMnSc1	panycký
<g/>
]	]	kIx)	]
od	od	k7c2	od
panika	panika	k1gFnSc1	panika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homografa	Homograf	k1gMnSc4	Homograf
jsou	být	k5eAaImIp3nP	být
problémem	problém	k1gInSc7	problém
při	při	k7c6	při
strojovém	strojový	k2eAgNnSc6d1	strojové
zpracování	zpracování	k1gNnSc6	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
