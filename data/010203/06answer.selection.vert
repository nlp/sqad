<s>
Za	za	k7c4	za
homografy	homograf	k1gMnPc4	homograf
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ὁ	ὁ	k?	ὁ
<g/>
,	,	kIx,	,
homos	homos	k1gInSc1	homos
–	–	k?	–
"	"	kIx"	"
<g/>
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
"	"	kIx"	"
a	a	k8xC	a
γ	γ	k?	γ
<g/>
,	,	kIx,	,
grafó	grafó	k?	grafó
–	–	k?	–
"	"	kIx"	"
<g/>
píši	psát	k5eAaImIp1nS	psát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
označována	označován	k2eAgNnPc1d1	označováno
odlišná	odlišný	k2eAgNnPc1d1	odlišné
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
shodně	shodně	k6eAd1	shodně
píší	psát	k5eAaImIp3nP	psát
<g/>
.	.	kIx.	.
</s>
