<s>
ISDN	ISDN	kA
</s>
<s>
ISDN	ISDN	kA
telefon	telefon	k1gInSc1
</s>
<s>
ISDN	ISDN	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
termínu	termín	k1gInSc2
Integrated	Integrated	k1gMnSc1
Services	Services	k1gMnSc1
Digital	Digital	kA
Network	network	k1gInSc1
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
síť	síť	k1gFnSc4
je	být	k5eAaImIp3nS
Digitální	digitální	k2eAgFnSc1d1
síť	síť	k1gFnSc1
integrovaných	integrovaný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc4
komunikačních	komunikační	k2eAgInPc2d1
celosvětových	celosvětový	k2eAgInPc2d1
standardů	standard	k1gInPc2
pro	pro	k7c4
digitální	digitální	k2eAgInSc4d1
simultánní	simultánní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
videa	video	k1gNnSc2
<g/>
,	,	kIx,
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
paketů	paket	k1gInPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
síťových	síťový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
tradičními	tradiční	k2eAgInPc7d1
obvody	obvod	k1gInPc7
veřejné	veřejný	k2eAgFnSc2d1
telefonní	telefonní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
public	publicum	k1gNnPc2
switched	switched	k1gInSc1
telephone	telephon	k1gInSc5
network	network	k1gInPc6
<g/>
)	)	kIx)
v	v	k7c6
digitálním	digitální	k2eAgInSc6d1
formátu	formát	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Představuje	představovat	k5eAaImIp3nS
snahu	snaha	k1gFnSc4
o	o	k7c4
přeměnu	přeměna	k1gFnSc4
existující	existující	k2eAgFnSc2d1
analogové	analogový	k2eAgFnSc2d1
telefonní	telefonní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
na	na	k7c4
digitální	digitální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
jako	jako	k9
výsledek	výsledek	k1gInSc4
úsilí	úsilí	k1gNnSc2
o	o	k7c4
jednotnou	jednotný	k2eAgFnSc4d1
komunikační	komunikační	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
přenášet	přenášet	k5eAaImF
a	a	k8xC
přepojovat	přepojovat	k5eAaImF
všechny	všechen	k3xTgFnPc4
služby	služba	k1gFnPc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
formě	forma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
ISDN	ISDN	kA
byla	být	k5eAaImAgFnS
definovaná	definovaný	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
v	v	k7c6
CCITT	CCITT	kA
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
přejmenovaná	přejmenovaný	k2eAgFnSc1d1
na	na	k7c4
ITU-T	ITU-T	k1gFnSc4
<g/>
)	)	kIx)
red	red	k?
book	book	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
doporučeních	doporučení	k1gNnPc6
série	série	k1gFnSc2
I.	I.	kA
<g/>
xxx	xxx	k?
standardizovala	standardizovat	k5eAaBmAgFnS
základní	základní	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
sítí	sítí	k1gNnSc2
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s>
ISDN	ISDN	kA
nabízí	nabízet	k5eAaImIp3nS
dva	dva	k4xCgInPc4
typy	typ	k1gInPc4
přípojek	přípojka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
BRI	BRI	kA
–	–	k?
Basic	Basic	kA
Rate	Rate	k1gInSc1
Interface	interface	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
účastnická	účastnický	k2eAgFnSc1d1
přípojka	přípojka	k1gFnSc1
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
lze	lze	k6eAd1
připojit	připojit	k5eAaPmF
až	až	k9
8	#num#	k4
koncových	koncový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
telefon	telefon	k1gInSc1
<g/>
,	,	kIx,
fax	fax	k1gInSc1
<g/>
,	,	kIx,
modem	modem	k1gInSc1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
2B	2B	k4
+	+	kIx~
D	D	kA
</s>
<s>
PRI	PRI	kA
–	–	k?
Primary	Primar	k1gInPc1
Rate	Rate	k1gFnSc1
Interface	interface	k1gInSc1
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
přípojky	přípojka	k1gFnSc2
je	být	k5eAaImIp3nS
určený	určený	k2eAgInSc1d1
k	k	k7c3
připojení	připojení	k1gNnSc3
pobočkových	pobočkový	k2eAgFnPc2d1
ústředen	ústředna	k1gFnPc2
<g/>
,	,	kIx,
nelze	lze	k6eNd1
ho	on	k3xPp3gMnSc4
využívat	využívat	k5eAaPmF,k5eAaImF
pro	pro	k7c4
připojení	připojení	k1gNnPc4
koncových	koncový	k2eAgNnPc2d1
účastnických	účastnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
30B	30B	k4
+	+	kIx~
D	D	kA
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
Japonsku	Japonsko	k1gNnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pouze	pouze	k6eAd1
23B	23B	k4
+	+	kIx~
D.	D.	kA
</s>
<s>
Přípojka	přípojka	k1gFnSc1
2B	2B	k4
+	+	kIx~
D	D	kA
(	(	kIx(
<g/>
základní	základní	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
)	)	kIx)
znamená	znamenat	k5eAaImIp3nS
tedy	tedy	k9
dva	dva	k4xCgInPc4
nezávislé	závislý	k2eNgInPc4d1
B	B	kA
kanály	kanál	k1gInPc4
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
tzv.	tzv.	kA
DS0	DS0	k1gMnSc1
kanály	kanál	k1gInPc7
viz	vidět	k5eAaImRp2nS
PCM	PCM	kA
<g/>
)	)	kIx)
určené	určený	k2eAgFnPc1d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
faxu	fax	k1gInSc2
<g/>
,	,	kIx,
obrazu	obraz	k1gInSc2
<g/>
,	,	kIx,
dat	datum	k1gNnPc2
atd.	atd.	kA
a	a	k8xC
jednoho	jeden	k4xCgInSc2
D	D	kA
kanálu	kanál	k1gInSc2
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
16	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
určeného	určený	k2eAgInSc2d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
signalizace	signalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přípojka	přípojka	k1gFnSc1
30B	30B	k4
+	+	kIx~
D	D	kA
(	(	kIx(
<g/>
primární	primární	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
)	)	kIx)
znamená	znamenat	k5eAaImIp3nS
tedy	tedy	k9
třicet	třicet	k4xCc4
nezávislých	závislý	k2eNgInPc2d1
B	B	kA
kanálů	kanál	k1gInPc2
o	o	k7c4
rychlosti	rychlost	k1gFnPc4
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
DS	DS	kA
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jeden	jeden	k4xCgInSc1
D	D	kA
kanál	kanál	k1gInSc1
také	také	k9
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
signalizace	signalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kanály	kanál	k1gInPc4
lze	lze	k6eAd1
používat	používat	k5eAaImF
zcela	zcela	k6eAd1
nezávisle	závisle	k6eNd1
např.	např.	kA
u	u	k7c2
2B	2B	k4
+	+	kIx~
D	D	kA
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
současně	současně	k6eAd1
jedním	jeden	k4xCgInSc7
B	B	kA
kanálem	kanál	k1gInSc7
telefonovat	telefonovat	k5eAaImF
a	a	k8xC
druhým	druhý	k4xOgNnSc7
přenášet	přenášet	k5eAaImF
fax	fax	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možné	možný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
sdružování	sdružování	k1gNnSc4
kanálů	kanál	k1gInPc2
například	například	k6eAd1
při	při	k7c6
přístupu	přístup	k1gInSc6
na	na	k7c4
Internet	Internet	k1gInSc4
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
tyto	tento	k3xDgInPc4
účely	účel	k1gInPc4
spíše	spíše	k9
používá	používat	k5eAaImIp3nS
ADSL	ADSL	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
lze	lze	k6eAd1
provozovat	provozovat	k5eAaImF
současně	současně	k6eAd1
s	s	k7c7
přípojkou	přípojka	k1gFnSc7
BRI	BRI	kA
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
přenos	přenos	k1gInSc4
signalizace	signalizace	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
ISDN	ISDN	kA
používá	používat	k5eAaImIp3nS
účastnická	účastnický	k2eAgFnSc1d1
signalizace	signalizace	k1gFnSc1
DSS1	DSS1	k1gMnSc1
(	(	kIx(
<g/>
Digital	Digital	kA
Subscriber	Subscriber	k1gInSc4
System	Syst	k1gInSc7
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
SS7	SS7	k1gFnSc1
(	(	kIx(
<g/>
Signaling	Signaling	k1gInSc1
System	Syst	k1gInSc7
No	no	k9
<g/>
.	.	kIx.
7	#num#	k4
<g/>
)	)	kIx)
pro	pro	k7c4
signalizaci	signalizace	k1gFnSc4
mezi	mezi	k7c7
ústřednami	ústředna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
umožňuje	umožňovat	k5eAaImIp3nS
pomocí	pomocí	k7c2
doplňkových	doplňkový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
identifikaci	identifikace	k1gFnSc3
volajícího	volající	k2eAgMnSc2d1
<g/>
,	,	kIx,
tarifikační	tarifikační	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
atd.	atd.	kA
</s>
<s>
ISDN	ISDN	kA
pokrývá	pokrývat	k5eAaImIp3nS
první	první	k4xOgInSc1
3	#num#	k4
vrstvy	vrstva	k1gFnPc4
modelu	model	k1gInSc2
OSI	OSI	kA
<g/>
:	:	kIx,
</s>
<s>
fyzická	fyzický	k2eAgFnSc1d1
<g/>
:	:	kIx,
Protokol	protokol	k1gInSc1
I	i	k9
<g/>
.431	.431	k4
–	–	k?
PRI	PRI	kA
a	a	k8xC
I	I	kA
<g/>
.430	.430	k4
–	–	k?
BRI	BRI	kA
</s>
<s>
linková	linkový	k2eAgFnSc1d1
<g/>
:	:	kIx,
Protokol	protokol	k1gInSc1
Q	Q	kA
<g/>
.921	.921	k4
</s>
<s>
síťová	síťový	k2eAgFnSc1d1
<g/>
:	:	kIx,
Protokol	protokol	k1gInSc1
Q	Q	kA
<g/>
.931	.931	k4
</s>
<s>
Motivace	motivace	k1gFnSc1
</s>
<s>
Dnešní	dnešní	k2eAgFnPc1d1
telefonní	telefonní	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgFnP
na	na	k7c6
digitálních	digitální	k2eAgFnPc6d1
telefonních	telefonní	k2eAgFnPc6d1
ústřednách	ústředna	k1gFnPc6
a	a	k8xC
také	také	k9
přenosové	přenosový	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
mezi	mezi	k7c7
ústřednami	ústředna	k1gFnPc7
jsou	být	k5eAaImIp3nP
plně	plně	k6eAd1
digitalizovány	digitalizován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
analogovou	analogový	k2eAgFnSc7d1
částí	část	k1gFnSc7
sítě	síť	k1gFnSc2
tak	tak	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
účastnická	účastnický	k2eAgFnSc1d1
přípojka	přípojka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
poslední	poslední	k2eAgFnSc1d1
část	část	k1gFnSc1
od	od	k7c2
ústředny	ústředna	k1gFnSc2
k	k	k7c3
telefonnímu	telefonní	k2eAgInSc3d1
přístroji	přístroj	k1gInSc3
(	(	kIx(
<g/>
modemu	modem	k1gInSc2
<g/>
,	,	kIx,
faxu	fax	k1gInSc2
atd.	atd.	kA
<g/>
)	)	kIx)
účastníka	účastník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
nabízí	nabízet	k5eAaImIp3nS
plně	plně	k6eAd1
digitální	digitální	k2eAgInSc1d1
přenos	přenos	k1gInSc1
až	až	k9
k	k	k7c3
účastníkovi	účastník	k1gMnSc3
(	(	kIx(
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
D	D	kA
a	a	k8xC
D	D	kA
<g/>
/	/	kIx~
<g/>
A	a	k8xC
převod	převod	k1gInSc1
signálu	signál	k1gInSc2
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
přímo	přímo	k6eAd1
v	v	k7c6
koncovém	koncový	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
dále	daleko	k6eAd2
nabízí	nabízet	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
komunikovat	komunikovat	k5eAaImF
pomocí	pomocí	k7c2
jedné	jeden	k4xCgFnSc2
digitální	digitální	k2eAgFnSc2d1
účastnické	účastnický	k2eAgFnSc2d1
přípojky	přípojka	k1gFnSc2
pomocí	pomocí	k7c2
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
textu	text	k1gInSc2
a	a	k8xC
obrazu	obraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
pak	pak	k6eAd1
mluvíme	mluvit	k5eAaImIp1nP
o	o	k7c4
multimediální	multimediální	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
přípojku	přípojek	k1gInSc2
lze	lze	k6eAd1
pomocí	pomocí	k7c2
takzvaného	takzvaný	k2eAgInSc2d1
terminálového	terminálový	k2eAgInSc2d1
adaptéru	adaptér	k1gInSc2
(	(	kIx(
<g/>
TA	ten	k3xDgFnSc1
<g/>
)	)	kIx)
nesprávně	správně	k6eNd1
„	„	k?
<g/>
ISDN	ISDN	kA
modem	modem	k1gInSc1
<g/>
“	“	k?
samozřejmě	samozřejmě	k6eAd1
použít	použít	k5eAaPmF
také	také	k9
pro	pro	k7c4
připojení	připojení	k1gNnSc4
do	do	k7c2
sítě	síť	k1gFnSc2
Internet	Internet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
bylo	být	k5eAaImAgNnS
po	po	k7c6
prvních	první	k4xOgInPc6
počátečních	počáteční	k2eAgInPc6d1
problémech	problém	k1gInPc6
v	v	k7c6
kompatibilitě	kompatibilita	k1gFnSc6
zavedeno	zavést	k5eAaPmNgNnS
tzv.	tzv.	kA
EURO-ISDN	EURO-ISDN	k1gMnSc4
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zaručuje	zaručovat	k5eAaImIp3nS
shodnou	shodný	k2eAgFnSc4d1
implementaci	implementace	k1gFnSc4
ISDN	ISDN	kA
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
tedy	tedy	k9
pod	pod	k7c7
pojmem	pojem	k1gInSc7
ISDN	ISDN	kA
myslí	myslet	k5eAaImIp3nS
vždy	vždy	k6eAd1
EURO-ISDN	EURO-ISDN	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
B-ISDN	B-ISDN	k?
</s>
<s>
B-ISDN	B-ISDN	k?
(	(	kIx(
<g/>
broadband	broadband	k1gInSc1
ISDN	ISDN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
širokopásmové	širokopásmový	k2eAgNnSc4d1
ISDN	ISDN	kA
[	[	kIx(
<g/>
klasické	klasický	k2eAgFnSc2d1
ISDN	ISDN	kA
je	být	k5eAaImIp3nS
nazvané	nazvaný	k2eAgNnSc1d1
úzkopásmové	úzkopásmový	k2eAgNnSc1d1
(	(	kIx(
<g/>
narrowband	narrowband	k1gInSc1
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytuje	poskytovat	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
služby	služba	k1gFnPc4
přes	přes	k7c4
jednotné	jednotný	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
jako	jako	k8xC,k8xS
ISDN	ISDN	kA
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
podstatně	podstatně	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přenosu	přenos	k1gInSc6
musejí	muset	k5eAaImIp3nP
být	být	k5eAaImF
měděné	měděný	k2eAgInPc1d1
kabely	kabel	k1gInPc1
(	(	kIx(
<g/>
potřebné	potřebné	k1gNnSc1
pro	pro	k7c4
normální	normální	k2eAgNnSc4d1
ISDN	ISDN	kA
<g/>
)	)	kIx)
nahrazeny	nahradit	k5eAaPmNgFnP
optickým	optický	k2eAgNnSc7d1
vláknem	vlákno	k1gNnSc7
(	(	kIx(
<g/>
fibre	fibr	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
ISDN	ISDN	kA
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
DSL	DSL	kA
</s>
<s>
Technologie	technologie	k1gFnSc1
DSL	DSL	kA
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
součástí	součást	k1gFnSc7
ISDN	ISDN	kA
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
oddělila	oddělit	k5eAaPmAgFnS
a	a	k8xC
dnes	dnes	k6eAd1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
samostatná	samostatný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
výhodnější	výhodný	k2eAgInSc1d2
DSL	DSL	kA
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
ISDN	ISDN	kA
celosvětově	celosvětově	k6eAd1
pomalu	pomalu	k6eAd1
na	na	k7c6
ústupu	ústup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
k	k	k7c3
tomu	ten	k3xDgNnSc3
3	#num#	k4
základní	základní	k2eAgInPc4d1
důvody	důvod	k1gInPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
DSL	DSL	kA
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
než	než	k8xS
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
DSL	DSL	kA
zasílá	zasílat	k5eAaImIp3nS
pakety	paket	k1gInPc4
rychlostí	rychlost	k1gFnPc2
od	od	k7c2
128	#num#	k4
kbit	kbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
do	do	k7c2
24	#num#	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
ADSL	ADSL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
nabízí	nabízet	k5eAaImIp3nS
2	#num#	k4
rychlosti	rychlost	k1gFnSc2
–	–	k?
64	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
a	a	k8xC
128	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Tedy	tedy	k9
i	i	k9
nejpomalejší	pomalý	k2eAgNnSc4d3
připojení	připojení	k1gNnSc4
DSL	DSL	kA
je	být	k5eAaImIp3nS
rychlejší	rychlý	k2eAgFnSc1d2
než	než	k8xS
jakékoli	jakýkoli	k3yIgNnSc1
připojení	připojení	k1gNnSc1
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s>
DSL	DSL	kA
je	být	k5eAaImIp3nS
lacinější	laciný	k2eAgInSc1d2
než	než	k8xS
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
prvé	prvý	k4xOgFnPc4
<g/>
,	,	kIx,
uživatel	uživatel	k1gMnSc1
nemusí	muset	k5eNaImIp3nS
sledovat	sledovat	k5eAaImF
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
během	během	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
je	on	k3xPp3gMnPc4
online	onlinout	k5eAaPmIp3nS
a	a	k8xC
za	za	k7c4
druhé	druhý	k4xOgNnSc4
<g/>
,	,	kIx,
při	při	k7c6
DSL	DSL	kA
není	být	k5eNaImIp3nS
potřeba	potřeba	k6eAd1
žádný	žádný	k3yNgInSc4
speciální	speciální	k2eAgInSc4d1
hardware	hardware	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Linka	linka	k1gFnSc1
DSL	DSL	kA
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
připojená	připojený	k2eAgFnSc1d1
(	(	kIx(
<g/>
always	always	k6eAd1
on	on	k3xPp3gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ISDN	ISDN	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
založené	založený	k2eAgNnSc1d1
na	na	k7c6
vytáčeném	vytáčený	k2eAgNnSc6d1
připojení	připojení	k1gNnSc6
(	(	kIx(
<g/>
dial-up	dial-up	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
jako	jako	k8xS,k8xC
klasická	klasický	k2eAgFnSc1d1
telefonní	telefonní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpadá	odpadat	k5eAaImIp3nS
vytáčení	vytáčení	k1gNnSc4
linky	linka	k1gFnSc2
a	a	k8xC
ověření	ověření	k1gNnSc4
i	i	k8xC
připojení	připojení	k1gNnSc4
uživatele	uživatel	k1gMnSc2
k	k	k7c3
internetu	internet	k1gInSc3
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
okamžité	okamžitý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Přenosové	přenosový	k2eAgNnSc1d1
médium	médium	k1gNnSc1
</s>
<s>
Pro	pro	k7c4
přenos	přenos	k1gInSc4
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
klasické	klasický	k2eAgInPc1d1
metalické	metalický	k2eAgInPc1d1
telefonní	telefonní	k2eAgInPc1d1
kabely	kabel	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kanály	kanál	k1gInPc1
</s>
<s>
Jsou	být	k5eAaImIp3nP
určené	určený	k2eAgInPc1d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
uživatelských	uživatelský	k2eAgNnPc2d1
dat	datum	k1gNnPc2
a	a	k8xC
signalizace	signalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenášejí	přenášet	k5eAaImIp3nP
se	se	k3xPyFc4
přes	přes	k7c4
rozhraní	rozhraní	k1gNnSc4
UNI	UNI	kA
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
kanálů	kanál	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
standardizované	standardizovaný	k2eAgNnSc1d1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	A	kA
–	–	k?
analogový	analogový	k2eAgInSc4d1
telefonní	telefonní	k2eAgInSc4d1
kanál	kanál	k1gInSc4
se	s	k7c7
šířkou	šířka	k1gFnSc7
pásma	pásmo	k1gNnSc2
4	#num#	k4
kHz	khz	kA
</s>
<s>
B	B	kA
–	–	k?
digitální	digitální	k2eAgInSc4d1
kanál	kanál	k1gInSc4
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
64	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
pro	pro	k7c4
přenos	přenos	k1gInSc4
hlasu	hlas	k1gInSc2
(	(	kIx(
<g/>
PMC	PMC	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
uživatelské	uživatelský	k2eAgFnPc4d1
informace	informace	k1gFnPc4
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
,	,	kIx,
data	datum	k1gNnPc1
<g/>
,	,	kIx,
video	video	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
připojeno	připojit	k5eAaPmNgNnS
nejvýše	nejvýše	k6eAd1,k6eAd3
osm	osm	k4xCc1
sériových	sériový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
jakými	jaký	k3yIgNnPc7,k3yQgNnPc7,k3yRgNnPc7
jsou	být	k5eAaImIp3nP
např.	např.	kA
osobní	osobní	k2eAgInPc4d1
počítače	počítač	k1gInPc4
<g/>
,	,	kIx,
terminály	terminál	k1gInPc4
a	a	k8xC
faxy	fax	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
C	C	kA
–	–	k?
digitální	digitální	k2eAgInSc4d1
kanál	kanál	k1gInSc4
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
8	#num#	k4
nebo	nebo	k8xC
16	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
D	D	kA
–	–	k?
digitální	digitální	k2eAgInSc4d1
(	(	kIx(
<g/>
řídicí	řídicí	k2eAgInSc4d1
<g/>
)	)	kIx)
kanál	kanál	k1gInSc4
pro	pro	k7c4
služební	služební	k2eAgInPc4d1
účely	účel	k1gInPc4
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
16	#num#	k4
nebo	nebo	k8xC
64	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
signalizaci	signalizace	k1gFnSc4
a	a	k8xC
řízení	řízení	k1gNnSc4
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
vrstvový	vrstvový	k2eAgInSc1d1
protokol	protokol	k1gInSc1
v	v	k7c6
paketovém	paketový	k2eAgInSc6d1
režimu	režim	k1gInSc6
založeném	založený	k2eAgInSc6d1
na	na	k7c6
standardu	standard	k1gInSc6
CCITT	CCITT	kA
X	X	kA
<g/>
.25	.25	k4
</s>
<s>
E	E	kA
–	–	k?
digitální	digitální	k2eAgInSc4d1
kanál	kanál	k1gInSc4
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
64	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
pro	pro	k7c4
interní	interní	k2eAgFnPc4d1
potřeby	potřeba	k1gFnPc4
ISDN	ISDN	kA
</s>
<s>
H	H	kA
–	–	k?
digitální	digitální	k2eAgInSc4d1
kanál	kanál	k1gInSc4
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
384	#num#	k4
<g/>
,	,	kIx,
1536	#num#	k4
nebo	nebo	k8xC
1920	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
násobky	násobek	k1gInPc1
rychlosti	rychlost	k1gFnSc2
kanálu	kanál	k1gInSc2
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Standardizované	standardizovaný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
kombinace	kombinace	k1gFnPc4
těchto	tento	k3xDgInPc2
kanálů	kanál	k1gInPc2
</s>
<s>
Basic	Basic	kA
Rate	Rate	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
kanály	kanál	k1gInPc7
B	B	kA
a	a	k8xC
1	#num#	k4
kanál	kanál	k1gInSc1
D	D	kA
<g/>
)	)	kIx)
–	–	k?
vhodný	vhodný	k2eAgInSc4d1
pro	pro	k7c4
domácnosti	domácnost	k1gFnPc4
resp.	resp.	kA
malé	malý	k2eAgFnPc4d1
kanceláře	kancelář	k1gFnPc4
</s>
<s>
Primary	Primar	k1gInPc1
Rate	Rat	k1gFnSc2
(	(	kIx(
<g/>
23	#num#	k4
B	B	kA
kanálů	kanál	k1gInPc2
a	a	k8xC
1	#num#	k4
D	D	kA
kanál	kanál	k1gInSc4
v	v	k7c6
USA	USA	kA
a	a	k8xC
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
30	#num#	k4
kanálů	kanál	k1gInPc2
B	B	kA
a	a	k8xC
1	#num#	k4
kanál	kanál	k1gInSc1
D	D	kA
jinde	jinde	k6eAd1
<g/>
)	)	kIx)
–	–	k?
vhodný	vhodný	k2eAgInSc4d1
pro	pro	k7c4
větší	veliký	k2eAgFnPc4d2
organizace	organizace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
si	se	k3xPyFc3
k	k	k7c3
přípojce	přípojka	k1gFnSc3
připojí	připojit	k5eAaPmIp3nP
vlastní	vlastní	k2eAgFnSc4d1
pobočkovou	pobočkový	k2eAgFnSc4d1
ústřednu	ústředna	k1gFnSc4
</s>
<s>
Hybrid	hybrid	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
kanál	kanál	k1gInSc1
A	A	kA
a	a	k8xC
1	#num#	k4
kanál	kanál	k1gInSc1
C	C	kA
<g/>
)	)	kIx)
–	–	k?
umožňuje	umožňovat	k5eAaImIp3nS
připojení	připojení	k1gNnSc4
běžných	běžný	k2eAgInPc2d1
analogových	analogový	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
na	na	k7c6
ISDN	ISDN	kA
</s>
<s>
Základní	základní	k2eAgNnPc1d1
účastnická	účastnický	k2eAgNnPc1d1
rozhraní	rozhraní	k1gNnPc1
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1
pro	pro	k7c4
základní	základní	k2eAgInSc4d1
přístup	přístup	k1gInSc4
–	–	k?
BRI	BRI	kA
(	(	kIx(
<g/>
basic	basic	k1gMnSc1
rate	rat	k1gFnSc2
interface	interface	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navrhnout	k5eAaPmNgNnS
pro	pro	k7c4
zařízení	zařízení	k1gNnSc4
s	s	k7c7
relativně	relativně	k6eAd1
malou	malý	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
(	(	kIx(
<g/>
např.	např.	kA
terminály	terminál	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
2B	2B	k4
+	+	kIx~
D.	D.	kA
Přenáší	přenášet	k5eAaImIp3nP
dva	dva	k4xCgInPc4
nezávislé	závislý	k2eNgInPc4d1
plně	plně	k6eAd1
duplexní	duplexní	k2eAgInPc4d1
B	B	kA
kanály	kanál	k1gInPc4
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
určené	určený	k2eAgNnSc4d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
faxu	fax	k1gInSc2
<g/>
,	,	kIx,
obrazu	obraz	k1gInSc2
<g/>
,	,	kIx,
dat	datum	k1gNnPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
D	D	kA
kanál	kanál	k1gInSc1
s	s	k7c7
rychlostí	rychlost	k1gFnSc7
16	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
signalizace	signalizace	k1gFnSc2
a	a	k8xC
pomalých	pomalý	k2eAgNnPc2d1
dat	datum	k1gNnPc2
v	v	k7c6
paketové	paketový	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
připojit	připojit	k5eAaPmF
až	až	k9
8	#num#	k4
koncových	koncový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
telefon	telefon	k1gInSc1
<g/>
,	,	kIx,
fax	fax	k1gInSc1
<g/>
,	,	kIx,
modem	modem	k1gInSc1
<g/>
,	,	kIx,
...	...	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
mohou	moct	k5eAaImIp3nP
komunikovat	komunikovat	k5eAaImF
2	#num#	k4
terminály	terminál	k1gInPc7
na	na	k7c6
dvou	dva	k4xCgInPc6
B	B	kA
kanálech	kanál	k1gInPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
1	#num#	k4
multimediální	multimediální	k2eAgInSc4d1
terminál	terminál	k1gInSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
obsadí	obsadit	k5eAaPmIp3nP
oba	dva	k4xCgMnPc1
B	B	kA
informační	informační	k2eAgInPc4d1
kanály	kanál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanály	kanál	k1gInPc7
dokážou	dokázat	k5eAaPmIp3nP
poskytnout	poskytnout	k5eAaPmF
i	i	k9
dvě	dva	k4xCgNnPc1
nezávislá	závislý	k2eNgNnPc1d1
spojení	spojení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
192	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
uživatelská	uživatelský	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
128	#num#	k4
Kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Základní	základní	k2eAgFnSc1d1
přípojka	přípojka	k1gFnSc1
je	být	k5eAaImIp3nS
realizována	realizovat	k5eAaBmNgFnS
dvěma	dva	k4xCgInPc7
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
spojení	spojení	k1gNnSc1
point-to-point	point-to-pointa	k1gFnPc2
–	–	k?
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
linka	linka	k1gFnSc1
od	od	k7c2
operátora	operátor	k1gMnSc2
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
pobočkovou	pobočkový	k2eAgFnSc7d1
ústřednou	ústředna	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
obsluhuje	obsluhovat	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc4
k	k	k7c3
ní	on	k3xPp3gFnSc7
připojená	připojený	k2eAgFnSc1d1
(	(	kIx(
<g/>
telefon	telefon	k1gInSc1
<g/>
,	,	kIx,
fax	fax	k1gInSc1
<g/>
,	,	kIx,
...	...	k?
<g/>
)	)	kIx)
</s>
<s>
spojení	spojení	k1gNnSc1
point-to-multipoint	point-to-multipoint	k1gInSc1
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc1
od	od	k7c2
operátora	operátor	k1gMnSc2
končí	končit	k5eAaImIp3nS
v	v	k7c6
zásuvce	zásuvka	k1gFnSc6
NT	NT	kA
(	(	kIx(
<g/>
Network	network	k1gInSc1
termination	termination	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tuto	tento	k3xDgFnSc4
zásuvku	zásuvka	k1gFnSc4
lze	lze	k6eAd1
napojit	napojit	k5eAaPmF
8	#num#	k4
různých	různý	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
mít	mít	k5eAaImF
na	na	k7c6
jedné	jeden	k4xCgFnSc6
přípojce	přípojka	k1gFnSc6
více	hodně	k6eAd2
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
každé	každý	k3xTgNnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
jiné	jiný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
můžeme	moct	k5eAaImIp1nP
dělat	dělat	k5eAaImF
více	hodně	k6eAd2
věcí	věc	k1gFnPc2
současně	současně	k6eAd1
–	–	k?
např.	např.	kA
telefonovat	telefonovat	k5eAaImF
a	a	k8xC
zároveň	zároveň	k6eAd1
surfovat	surfovat	k5eAaImF
na	na	k7c6
internetu	internet	k1gInSc6
či	či	k8xC
odesílat	odesílat	k5eAaImF
fax	fax	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1
pro	pro	k7c4
primární	primární	k2eAgInSc4d1
přístup	přístup	k1gInSc4
–	–	k?
PRI	PRI	kA
(	(	kIx(
<g/>
primary	primar	k1gInPc1
rate	rate	k1gFnSc1
interface	interface	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
navržené	navržený	k2eAgNnSc1d1
pro	pro	k7c4
systémy	systém	k1gInPc4
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
(	(	kIx(
<g/>
např.	např.	kA
pobočkové	pobočkový	k2eAgFnPc4d1
ústředny	ústředna	k1gFnPc4
k	k	k7c3
veřejné	veřejný	k2eAgFnSc3d1
telekomunikační	telekomunikační	k2eAgFnSc3d1
síti	síť	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
použít	použít	k5eAaPmF
pro	pro	k7c4
připojení	připojení	k1gNnPc4
koncových	koncový	k2eAgNnPc2d1
účastnických	účastnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
přenáší	přenášet	k5eAaImIp3nS
30B	30B	k4
+	+	kIx~
D	D	kA
kanál	kanál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
Japonsku	Japonsko	k1gNnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
23B	23B	k4
+	+	kIx~
D	D	kA
kanál	kanál	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tedy	tedy	k9
30	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
nezávislých	závislý	k2eNgInPc2d1
B	B	kA
kanálů	kanál	k1gInPc2
o	o	k7c4
rychlosti	rychlost	k1gFnPc4
64	#num#	k4
kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
určených	určený	k2eAgInPc2d1
na	na	k7c4
přenášení	přenášení	k1gNnSc4
hovorů	hovor	k1gInPc2
<g/>
,	,	kIx,
textu	text	k1gInSc2
<g/>
,	,	kIx,
obrazu	obraz	k1gInSc2
a	a	k8xC
dat	datum	k1gNnPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
D	D	kA
kanál	kanál	k1gInSc4
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
64	#num#	k4
kb	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
signalizace	signalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
2.048	2.048	k4
Mb	Mb	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
u	u	k7c2
amerických	americký	k2eAgInPc2d1
systémů	systém	k1gInPc2
1.544	1.544	k4
Mb	Mb	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uživatelská	uživatelský	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
1	#num#	k4
920	#num#	k4
kb	kb	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
Primární	primární	k2eAgFnSc4d1
přípojku	přípojka	k1gFnSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zapojit	zapojit	k5eAaPmF
jako	jako	k9
point-to-point	point-to-point	k1gInSc4
na	na	k7c4
pobočkovou	pobočkový	k2eAgFnSc4d1
ústřednu	ústředna	k1gFnSc4
</s>
<s>
Uživatelské	uživatelský	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
User	usrat	k5eAaPmRp2nS
network	network	k1gInSc1
interface	interface	k1gInPc1
–	–	k?
UNI	UNI	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
UNI	UNI	kA
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
každé	každý	k3xTgNnSc1
rozhraní	rozhraní	k1gNnSc1
mezi	mezi	k7c7
koncovým	koncový	k2eAgInSc7d1
terminálem	terminál	k1gInSc7
a	a	k8xC
ISDN	ISDN	kA
sítí	síť	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
tohoto	tento	k3xDgNnSc2
rozhraní	rozhraní	k1gNnSc2
se	se	k3xPyFc4
realizuje	realizovat	k5eAaBmIp3nS
výměna	výměna	k1gFnSc1
uživatelských	uživatelský	k2eAgFnPc2d1
a	a	k8xC
řídicích	řídicí	k2eAgFnPc2d1
informací	informace	k1gFnPc2
se	s	k7c7
síťovým	síťový	k2eAgInSc7d1
uzlem	uzel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
ISDN	ISDN	kA
sítí	síť	k1gFnPc2
je	být	k5eAaImIp3nS
několik	několik	k4yIc1
možných	možný	k2eAgNnPc2d1
uživatelských	uživatelský	k2eAgNnPc2d1
rozhraní	rozhraní	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
ISDN	ISDN	kA
terminál	terminál	k1gInSc1
</s>
<s>
Vícenásobné	vícenásobný	k2eAgInPc4d1
ISDN	ISDN	kA
terminály	terminál	k1gInPc4
připojené	připojený	k2eAgInPc4d1
přes	přes	k7c4
vícenásobnou	vícenásobný	k2eAgFnSc4d1
účastnickou	účastnický	k2eAgFnSc4d1
přípojku	přípojka	k1gFnSc4
</s>
<s>
PBX	PBX	kA
<g/>
,	,	kIx,
LAN	lano	k1gNnPc2
<g/>
,	,	kIx,
privátní	privátní	k2eAgMnPc1d1
sítě	síť	k1gFnSc2
</s>
<s>
Banky	banka	k1gFnPc1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
systém	systém	k1gInSc1
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
informací	informace	k1gFnPc2
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
systémy	systém	k1gInPc1
jako	jako	k9
např.	např.	kA
multiservisní	multiservisní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
podobná	podobný	k2eAgFnSc1d1
ISDN	ISDN	kA
</s>
<s>
Doporučení	doporučení	k1gNnSc1
CCITT	CCITT	kA
(	(	kIx(
<g/>
ITU-T	ITU-T	k1gMnSc2
<g/>
)	)	kIx)
ke	k	k7c3
standardům	standard	k1gInPc3
ISDN	ISDN	kA
</s>
<s>
Na	na	k7c6
doporučeních	doporučení	k1gNnPc6
se	se	k3xPyFc4
pracovalo	pracovat	k5eAaImAgNnS
8	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jsou	být	k5eAaImIp3nP
doporučení	doporučení	k1gNnSc4
série	série	k1gFnSc2
I.	I.	kA
<g/>
xxx	xxx	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
–	–	k?
Koncepty	koncept	k1gInPc4
<g/>
,	,	kIx,
terminologie	terminologie	k1gFnPc4
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgNnPc4d1
doporučení	doporučení	k1gNnPc4
</s>
<s>
E	E	kA
–	–	k?
Standardy	standard	k1gInPc1
ISDN	ISDN	kA
telefonních	telefonní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
</s>
<s>
Q	Q	kA
–	–	k?
Přepojování	přepojování	k1gNnSc1
a	a	k8xC
signalizace	signalizace	k1gFnSc1
</s>
<s>
Referenční	referenční	k2eAgInSc1d1
model	model	k1gInSc1
OSI	OSI	kA
</s>
<s>
ISDN	ISDN	kA
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
první	první	k4xOgInSc1
3	#num#	k4
vrstvy	vrstva	k1gFnPc4
modelu	model	k1gInSc2
OSI	OSI	kA
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
vrstva	vrstva	k1gFnSc1
<g/>
:	:	kIx,
fyzická	fyzický	k2eAgFnSc1d1
</s>
<s>
Funkcí	funkce	k1gFnSc7
je	být	k5eAaImIp3nS
přenos	přenos	k1gInSc1
bitového	bitový	k2eAgInSc2d1
proudu	proud	k1gInSc2
na	na	k7c6
fyzickém	fyzický	k2eAgNnSc6d1
přenosovém	přenosový	k2eAgNnSc6d1
médiu	médium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenosové	přenosový	k2eAgNnSc1d1
médium	médium	k1gNnSc1
je	být	k5eAaImIp3nS
totožné	totožný	k2eAgNnSc1d1
pro	pro	k7c4
B	B	kA
i	i	k8xC
D	D	kA
kanál	kanál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnPc1
jsou	být	k5eAaImIp3nP
specifikované	specifikovaný	k2eAgMnPc4d1
v	v	k7c6
doporučeních	doporučení	k1gNnPc6
ITU	ITU	kA
série	série	k1gFnSc2
I	I	kA
a	a	k8xC
G.	G.	kA
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
Protokol	protokol	k1gInSc1
I	i	k9
<g/>
.431	.431	k4
–	–	k?
PRI	PRI	kA
a	a	k8xC
I	I	kA
<g/>
.430	.430	k4
–	–	k?
BRI	BRI	kA
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
vrstva	vrstva	k1gFnSc1
<g/>
:	:	kIx,
datová	datový	k2eAgFnSc1d1
</s>
<s>
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
služby	služba	k1gFnPc4
fyzické	fyzický	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabezpečuje	zabezpečovat	k5eAaImIp3nS
spolehlivý	spolehlivý	k2eAgInSc4d1
bezchybný	bezchybný	k2eAgInSc4d1
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
ze	z	k7c2
síťové	síťový	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
rámce	rámec	k1gInPc4
pro	pro	k7c4
transparentní	transparentní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
informací	informace	k1gFnPc2
ze	z	k7c2
síťové	síťový	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
,	,	kIx,
kontroluje	kontrolovat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
detekuje	detekovat	k5eAaImIp3nS
chyby	chyba	k1gFnPc4
při	při	k7c6
přenosu	přenos	k1gInSc6
<g/>
,	,	kIx,
opakovaně	opakovaně	k6eAd1
vysílá	vysílat	k5eAaImIp3nS
rámce	rámec	k1gInPc4
v	v	k7c6
případě	případ	k1gInSc6
detekování	detekování	k1gNnSc2
chyby	chyba	k1gFnSc2
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nS
tok	tok	k1gInSc1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
provádí	provádět	k5eAaImIp3nS
údržbu	údržba	k1gFnSc4
a	a	k8xC
řídí	řídit	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
vrstvě	vrstva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
protokol	protokol	k1gInSc1
LAPD	LAPD	kA
<g/>
.	.	kIx.
</s>
<s>
Blíže	blíže	k1gFnSc1
je	být	k5eAaImIp3nS
specifikovaná	specifikovaný	k2eAgFnSc1d1
v	v	k7c6
doporučeních	doporučení	k1gNnPc6
ITU	ITU	kA
série	série	k1gFnSc2
Q	Q	kA
(	(	kIx(
<g/>
Q	Q	kA
<g/>
.920	.920	k4
<g/>
-Q	-Q	k?
<g/>
.923	.923	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
vrstva	vrstva	k1gFnSc1
<g/>
:	:	kIx,
síťová	síťový	k2eAgFnSc1d1
</s>
<s>
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
zřízení	zřízení	k1gNnSc4
<g/>
,	,	kIx,
udržení	udržení	k1gNnSc4
a	a	k8xC
zrušení	zrušení	k1gNnSc4
ISDN	ISDN	kA
spojení	spojení	k1gNnPc2
mezi	mezi	k7c4
2	#num#	k4
zařízeními	zařízení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
řízení	řízení	k1gNnSc4
doplňkových	doplňkový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnPc1
jsou	být	k5eAaImIp3nP
specifikované	specifikovaný	k2eAgMnPc4d1
v	v	k7c6
doporučeních	doporučení	k1gNnPc6
ITU	ITU	kA
série	série	k1gFnSc2
Q	Q	kA
(	(	kIx(
<g/>
Q	Q	kA
<g/>
.930	.930	k4
<g/>
-Q	-Q	k?
<g/>
.939	.939	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
ISDN	ISDN	kA
protokolový	protokolový	k2eAgInSc1d1
referenční	referenční	k2eAgInSc1d1
model	model	k1gInSc1
(	(	kIx(
<g/>
PRM	PRM	kA
ISDN	ISDN	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
modelovat	modelovat	k5eAaImF
spojení	spojení	k1gNnSc4
a	a	k8xC
výměnu	výměna	k1gFnSc4
informací	informace	k1gFnPc2
přes	přes	k7c4
ISDN	ISDN	kA
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
uvnitř	uvnitř	k7c2
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
referenčního	referenční	k2eAgInSc2d1
modelu	model	k1gInSc2
OSI	OSI	kA
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
tam	tam	k6eAd1
jisté	jistý	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RM	RM	kA
ISDN	ISDN	kA
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
OSI	OSI	kA
modeluje	modelovat	k5eAaImIp3nS
informační	informační	k2eAgInSc1d1
tok	tok	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
:	:	kIx,
standardní	standardní	k2eAgFnPc4d1
<g/>
,	,	kIx,
transportní	transportní	k2eAgFnPc4d1
a	a	k8xC
doplňkové	doplňkový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
odpovídají	odpovídat	k5eAaImIp3nP
vrstvám	vrstva	k1gFnPc3
definovaným	definovaný	k2eAgFnPc3d1
v	v	k7c6
RM	RM	kA
OSI	OSI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
t.	t.	k?
j.	j.	k?
1	#num#	k4
–	–	k?
fyzická	fyzický	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
–	–	k?
linková	linkový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
...	...	k?
<g/>
)	)	kIx)
Některé	některý	k3yIgFnPc1
vrstvy	vrstva	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
prázdné	prázdné	k1gNnSc4
–	–	k?
nemají	mít	k5eNaImIp3nP
v	v	k7c6
dané	daný	k2eAgFnSc6d1
aplikaci	aplikace	k1gFnSc6
žádnou	žádný	k3yNgFnSc4
funkčnost	funkčnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ISDN	ISDN	kA
je	být	k5eAaImIp3nS
synchronní	synchronní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
s	s	k7c7
přepojováním	přepojování	k1gNnSc7
okruhů	okruh	k1gInPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
síťové	síťový	k2eAgFnPc1d1
komponenty	komponenta	k1gFnPc1
v	v	k7c6
uživatelské	uživatelský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
používaly	používat	k5eAaImAgInP
protokoly	protokol	k1gInPc1
nad	nad	k7c7
3	#num#	k4
vrstvou	vrstva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Referenční	referenční	k2eAgInSc1d1
model	model	k1gInSc1
je	být	k5eAaImIp3nS
členěný	členěný	k2eAgInSc1d1
na	na	k7c4
vrstvy	vrstva	k1gFnPc4
a	a	k8xC
roviny	rovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousední	sousední	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
rovině	rovina	k1gFnSc6
komunikují	komunikovat	k5eAaImIp3nP
pomocí	pomoc	k1gFnSc7
primitiv	primitiv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnPc4d1
roviny	rovina	k1gFnPc4
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
uživatelská	uživatelský	k2eAgFnSc1d1
<g/>
,	,	kIx,
řídicí	řídicí	k2eAgFnSc1d1
a	a	k8xC
management	management	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Uživatelská	uživatelský	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
(	(	kIx(
<g/>
U-plane	U-plan	k1gMnSc5
<g/>
)	)	kIx)
–	–	k?
hlavní	hlavní	k2eAgFnSc7d1
úlohou	úloha	k1gFnSc7
je	být	k5eAaImIp3nS
transparentní	transparentní	k2eAgInSc1d1
přenos	přenos	k1gInSc1
informace	informace	k1gFnSc2
mezi	mezi	k7c7
uživatelskými	uživatelský	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informací	informace	k1gFnPc2
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
digitalizovaný	digitalizovaný	k2eAgInSc1d1
hovorový	hovorový	k2eAgInSc1d1
signál	signál	k1gInSc1
<g/>
,	,	kIx,
data	datum	k1gNnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
jiné	jiný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
si	se	k3xPyFc3
účastníci	účastník	k1gMnPc1
vyměňují	vyměňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
režimu	režim	k1gInSc6
přepojování	přepojování	k1gNnSc2
okruhů	okruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Řídicí	řídicí	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
(	(	kIx(
<g/>
C-plane	C-plan	k1gMnSc5
<g/>
)	)	kIx)
–	–	k?
zabezpečuje	zabezpečovat	k5eAaImIp3nS
přenos	přenos	k1gInSc1
řídicí	řídicí	k2eAgFnSc2d1
informace	informace	k1gFnSc2
pro	pro	k7c4
řízení	řízení	k1gNnSc4
spojení	spojení	k1gNnSc2
v	v	k7c6
uživatelské	uživatelský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
:	:	kIx,
sestavení	sestavení	k1gNnSc1
a	a	k8xC
zrušení	zrušení	k1gNnSc1
spojení	spojení	k1gNnSc2
<g/>
,	,	kIx,
dohled	dohled	k1gInSc1
nad	nad	k7c7
spojením	spojení	k1gNnSc7
a	a	k8xC
zabezpečení	zabezpečení	k1gNnSc1
doplňkových	doplňkový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rozdělená	rozdělený	k2eAgFnSc1d1
na	na	k7c4
2	#num#	k4
podroviny	podrovina	k1gFnSc2
-	-	kIx~
globální	globální	k2eAgFnSc1d1
řídicí	řídicí	k2eAgFnSc1d1
podrovina	podrovina	k1gFnSc1
a	a	k8xC
místní	místní	k2eAgFnSc1d1
řídicí	řídicí	k2eAgFnSc1d1
podrovina	podrovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Management	management	k1gInSc1
rovina	rovina	k1gFnSc1
(	(	kIx(
<g/>
M-plane	M-plan	k1gMnSc5
<g/>
)	)	kIx)
–	–	k?
má	mít	k5eAaImIp3nS
celkový	celkový	k2eAgInSc4d1
dohled	dohled	k1gInSc4
nad	nad	k7c7
sítí	síť	k1gFnSc7
a	a	k8xC
nad	nad	k7c7
ostatními	ostatní	k2eAgFnPc7d1
rovinami	rovina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koordinuje	koordinovat	k5eAaBmIp3nS
činnost	činnost	k1gFnSc4
rovin	rovina	k1gFnPc2
a	a	k8xC
vrstev	vrstva	k1gFnPc2
v	v	k7c6
protokolovém	protokolový	k2eAgInSc6d1
modelu	model	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
ISDN	ISDN	kA
ve	v	k7c6
světě	svět	k1gInSc6
a	a	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Standardizační	standardizační	k2eAgInSc1d1
proces	proces	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyspělé	vyspělý	k2eAgFnPc1d1
země	zem	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
rychle	rychle	k6eAd1
realizovat	realizovat	k5eAaBmF
první	první	k4xOgFnPc4
ISDN	ISDN	kA
sítě	síť	k1gFnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
aplikace	aplikace	k1gFnPc1
někdy	někdy	k6eAd1
předběhly	předběhnout	k5eAaPmAgFnP
standardizační	standardizační	k2eAgInPc4d1
procesy	proces	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byly	být	k5eAaImAgInP
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
technické	technický	k2eAgFnSc6d1
aplikaci	aplikace	k1gFnSc6
ISDN	ISDN	kA
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Výrobci	výrobce	k1gMnPc1
implementovali	implementovat	k5eAaImAgMnP
některé	některý	k3yIgFnPc4
části	část	k1gFnPc4
ISDN	ISDN	kA
rozdílně	rozdílně	k6eAd1
<g/>
,	,	kIx,
proto	proto	k8xC
mezi	mezi	k7c7
ISDN	ISDN	kA
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
jsou	být	k5eAaImIp3nP
rozdíly	rozdíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
USA	USA	kA
</s>
<s>
Proces	proces	k1gInSc1
zahájení	zahájení	k1gNnSc2
sjednocení	sjednocení	k1gNnSc2
ISDN	ISDN	kA
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
USA	USA	kA
dostal	dostat	k5eAaPmAgMnS
název	název	k1gInSc4
National	National	k1gMnSc2
ISDN	ISDN	kA
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
unifikovaná	unifikovaný	k2eAgFnSc1d1
síť	síť	k1gFnSc1
ISDN	ISDN	kA
plně	plně	k6eAd1
kompatibilní	kompatibilní	k2eAgFnSc1d1
s	s	k7c7
doporučeními	doporučení	k1gNnPc7
ITU	ITU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
3	#num#	k4
etapy	etapa	k1gFnSc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
–	–	k?
rozšířila	rozšířit	k5eAaPmAgFnS
počet	počet	k1gInSc4
doplňkových	doplňkový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
a	a	k8xC
definovala	definovat	k5eAaBmAgFnS
paketové	paketový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
nad	nad	k7c7
ISDN	ISDN	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
rok	rok	k1gInSc1
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Proces	proces	k1gInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
ukončen	ukončit	k5eAaPmNgMnS
touto	tento	k3xDgFnSc7
3	#num#	k4
etapou	etapa	k1gFnSc7
<g/>
,	,	kIx,
stále	stále	k6eAd1
přibývají	přibývat	k5eAaImIp3nP
nové	nový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
musí	muset	k5eAaImIp3nP
síť	síť	k1gFnSc4
ISDN	ISDN	kA
respektovat	respektovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
při	při	k7c6
videokonferencích	videokonference	k1gFnPc6
a	a	k8xC
zálohování	zálohování	k1gNnSc4
frame	framat	k5eAaPmIp3nS
relay	relaa	k1gFnPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
velmi	velmi	k6eAd1
rozšířené	rozšířený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
Japonska	Japonsko	k1gNnSc2
proniklo	proniknout	k5eAaPmAgNnS
ISDN	ISDN	kA
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
INS	INS	kA
–	–	k?
Net	Net	k1gMnSc1
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
resp.	resp.	kA
INS-Net	INS-Net	k1gInSc4
64	#num#	k4
a	a	k8xC
INS-Net	INS-Net	k1gInSc1
1500	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
po	po	k7c6
uvedení	uvedení	k1gNnSc6
ADSL	ADSL	kA
je	být	k5eAaImIp3nS
ISDN	ISDN	kA
pomalu	pomalu	k6eAd1
vytlačované	vytlačovaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
existoval	existovat	k5eAaImAgInS
obrovský	obrovský	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c4
západní	západní	k2eAgNnSc4d1
a	a	k8xC
východní	východní	k2eAgFnSc1d1
častí	častit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
snaze	snaha	k1gFnSc6
sjednotit	sjednotit	k5eAaPmF
evropské	evropský	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
na	na	k7c4
jednotnou	jednotný	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
vznikl	vzniknout	k5eAaPmAgInS
proces	proces	k1gInSc1
tvorby	tvorba	k1gFnSc2
Euro-ISDN	Euro-ISDN	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
tedy	tedy	k9
pod	pod	k7c7
pojmem	pojem	k1gInSc7
ISDN	ISDN	kA
myslí	myslet	k5eAaImIp3nS
vždy	vždy	k6eAd1
EURO-ISDN	EURO-ISDN	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Euro-ISDN	Euro-ISDN	k?
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
standard	standard	k1gInSc1
vycházející	vycházející	k2eAgInSc1d1
z	z	k7c2
celosvětového	celosvětový	k2eAgInSc2d1
standardu	standard	k1gInSc2
ISDN	ISDN	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
podepsaný	podepsaný	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
(	(	kIx(
<g/>
Memorandum	memorandum	k1gNnSc4
of	of	k?
Understanding	Understanding	k1gInSc1
<g/>
)	)	kIx)
dvaceti	dvacet	k4xCc6
šesti	šest	k4xCc7
evropskými	evropský	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
sjednocení	sjednocení	k1gNnSc4
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
budou	být	k5eAaImBp3nP
poskytovat	poskytovat	k5eAaImF
evropští	evropský	k2eAgMnPc1d1
provozovatelé	provozovatel	k1gMnPc1
ISDN	ISDN	kA
(	(	kIx(
<g/>
a	a	k8xC
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
jistá	jistý	k2eAgFnSc1d1
volnost	volnost	k1gFnSc1
při	při	k7c6
implementaci	implementace	k1gFnSc6
národních	národní	k2eAgFnPc2d1
ISDN	ISDN	kA
sítí	síť	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
zájmu	zájem	k1gInSc6
bezproblémového	bezproblémový	k2eAgNnSc2d1
poskytování	poskytování	k1gNnSc2
služeb	služba	k1gFnPc2
v	v	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
základních	základní	k2eAgFnPc2d1
charakteristik	charakteristika	k1gFnPc2
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
protokolu	protokol	k1gInSc2
DSS1	DSS1	k1gFnSc2
(	(	kIx(
<g/>
signalizační	signalizační	k2eAgInSc1d1
protokol	protokol	k1gInSc1
na	na	k7c6
D	D	kA
kanálu	kanál	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
datový	datový	k2eAgInSc4d1
signál	signál	k1gInSc4
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
8	#num#	k4
bitů	bit	k1gInPc2
a	a	k8xC
rámec	rámec	k1gInSc1
představuje	představovat	k5eAaImIp3nS
32	#num#	k4
kanálů	kanál	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
30	#num#	k4
hlasových	hlasový	k2eAgFnPc2d1
a	a	k8xC
2	#num#	k4
řídicí	řídicí	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přepočtu	přepočet	k1gInSc6
vychází	vycházet	k5eAaImIp3nS
2048	#num#	k4
kb	kb	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
Nasazení	nasazení	k1gNnSc1
BRI	BRI	kA
ISDN	ISDN	kA
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
byly	být	k5eAaImAgFnP
integrované	integrovaný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
ISDN	ISDN	kA
v	v	k7c6
druhém	druhý	k4xOgNnSc6
pololetí	pololetí	k1gNnSc6
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
službám	služba	k1gFnPc3
byl	být	k5eAaImAgInS
realizovaný	realizovaný	k2eAgInSc1d1
prostřednictvím	prostřednictvím	k7c2
digitálního	digitální	k2eAgNnSc2d1
propojení	propojení	k1gNnSc2
na	na	k7c4
digitální	digitální	k2eAgFnSc4d1
veřejnou	veřejný	k2eAgFnSc4d1
ústřednu	ústředna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatou	podstata	k1gFnSc7
propojení	propojení	k1gNnSc2
bylo	být	k5eAaImAgNnS
vytvoření	vytvoření	k1gNnSc4
dvouvodičového	dvouvodičový	k2eAgInSc2d1
max	max	kA
<g/>
.	.	kIx.
sedm	sedm	k4xCc4
kilometrů	kilometr	k1gInPc2
dlouhého	dlouhý	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
digitální	digitální	k2eAgFnSc2d1
veřejné	veřejný	k2eAgFnSc2d1
ústředny	ústředna	k1gFnSc2
se	s	k7c7
síťovým	síťový	k2eAgNnSc7d1
zakončením	zakončení	k1gNnSc7
umístěným	umístěný	k2eAgNnSc7d1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
účastníka	účastník	k1gMnSc2
–	–	k?
v	v	k7c6
domě	dům	k1gInSc6
nebo	nebo	k8xC
stoupačce	stoupačka	k1gFnSc6
bytovky	bytovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síťové	síťový	k2eAgNnSc1d1
zakončení	zakončení	k1gNnSc1
je	být	k5eAaImIp3nS
s	s	k7c7
účastnickým	účastnický	k2eAgNnSc7d1
koncovým	koncový	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
propojené	propojený	k2eAgNnSc1d1
čtyřvodičovým	čtyřvodičův	k2eAgNnSc7d1
účastnickým	účastnický	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
–	–	k?
pasivní	pasivní	k2eAgFnSc7d1
sběrnicí	sběrnice	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gFnSc1
délka	délka	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
až	až	k9
několik	několik	k4yIc1
set	sto	k4xCgNnPc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřvodičové	Čtyřvodičové	k?
účastnické	účastnický	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zakončené	zakončený	k2eAgInPc4d1
až	až	k8xS
16	#num#	k4
zásuvkami	zásuvka	k1gFnPc7
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgInPc2,k3yRgInPc2,k3yIgInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
současně	současně	k6eAd1
připojeno	připojit	k5eAaPmNgNnS
až	až	k9
osm	osm	k4xCc1
různých	různý	k2eAgFnPc2d1
koncových	koncový	k2eAgFnPc2d1
účastnických	účastnický	k2eAgFnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Služby	služba	k1gFnPc1
</s>
<s>
ISDN	ISDN	kA
poskytuje	poskytovat	k5eAaImIp3nS
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
hovorových	hovorový	k2eAgFnPc2d1
i	i	k8xC
nehovorových	hovorový	k2eNgFnPc2d1
služeb	služba	k1gFnPc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc4
telekomunikačních	telekomunikační	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
obrázek	obrázek	k1gInSc1
popisující	popisující	k2eAgFnSc2d1
ISDN	ISDN	kA
služby	služba	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Transportní	transportní	k2eAgInSc1d1
(	(	kIx(
<g/>
bearer	bearer	k1gInSc1
<g/>
)	)	kIx)
služby	služba	k1gFnPc1
</s>
<s>
Nazývané	nazývaný	k2eAgNnSc1d1
také	také	k9
přenosové	přenosový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
úlohou	úloha	k1gFnSc7
je	být	k5eAaImIp3nS
přenos	přenos	k1gInSc1
informací	informace	k1gFnPc2
–	–	k?
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracují	pracovat	k5eAaImIp3nP
v	v	k7c6
režimu	režim	k1gInSc6
přepojování	přepojování	k1gNnSc2
paketů	paket	k1gInPc2
a	a	k8xC
v	v	k7c6
režimu	režim	k1gInSc6
přepojování	přepojování	k1gNnSc2
okruhů	okruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
referenčního	referenční	k2eAgInSc2d1
modelu	model	k1gInSc2
OSI	OSI	kA
zahrnují	zahrnovat	k5eAaImIp3nP
jen	jen	k6eAd1
funkce	funkce	k1gFnPc4
nižších	nízký	k2eAgFnPc2d2
vrstev	vrstva	k1gFnPc2
–	–	k?
jsou	být	k5eAaImIp3nP
zabezpečené	zabezpečený	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
1	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
úrovně	úroveň	k1gFnSc2
referenčního	referenční	k2eAgInSc2d1
modelu	model	k1gInSc2
OSI	OSI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tvořené	tvořený	k2eAgFnPc1d1
2	#num#	k4
kategoriemi	kategorie	k1gFnPc7
služeb	služba	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Transportní	transportní	k2eAgFnPc1d1
služby	služba	k1gFnPc1
s	s	k7c7
přepojováním	přepojování	k1gNnSc7
okruhů	okruh	k1gInPc2
</s>
<s>
Jsou	být	k5eAaImIp3nP
definované	definovaný	k2eAgFnPc1d1
protokolem	protokol	k1gInSc7
X	X	kA
<g/>
.25	.25	k4
doporučení	doporučení	k1gNnSc2
ITU	ITU	kA
1.231	1.231	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenášejí	přenášet	k5eAaImIp3nP
uživatelskou	uživatelský	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
po	po	k7c6
jednom	jeden	k4xCgInSc6
typu	typ	k1gInSc6
kanálu	kanál	k1gInSc2
a	a	k8xC
signalizaci	signalizace	k1gFnSc4
jiným	jiný	k2eAgInSc7d1
typem	typ	k1gInSc7
komunikačního	komunikační	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Transportní	transportní	k2eAgFnPc1d1
služby	služba	k1gFnPc1
s	s	k7c7
přepojováním	přepojování	k1gNnSc7
paketů	paket	k1gInPc2
</s>
<s>
Služby	služba	k1gFnPc1
umožňují	umožňovat	k5eAaImIp3nP
zřízení	zřízení	k1gNnSc4
virtuálních	virtuální	k2eAgNnPc2d1
spojení	spojení	k1gNnPc2
nebo	nebo	k8xC
emulaci	emulace	k1gFnSc3
přepojování	přepojování	k1gNnSc2
okruhů	okruh	k1gInPc2
přes	přes	k7c4
virtuální	virtuální	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučení	doporučení	k1gNnSc1
ITU	ITU	kA
1.232	1.232	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Standardní	standardní	k2eAgInSc1d1
(	(	kIx(
<g/>
teleservices	teleservices	k1gInSc1
<g/>
)	)	kIx)
služby	služba	k1gFnPc1
</s>
<s>
V	v	k7c6
doporučeních	doporučení	k1gNnPc6
CCITT	CCITT	kA
byly	být	k5eAaImAgFnP
definovány	definovat	k5eAaBmNgFnP
následující	následující	k2eAgFnPc1d1
standardní	standardní	k2eAgFnPc1d1
služby	služba	k1gFnPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Telefonní	telefonní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
</s>
<s>
Zabezpečuje	zabezpečovat	k5eAaImIp3nS
přenos	přenos	k1gInSc1
a	a	k8xC
přepojování	přepojování	k1gNnSc1
hovorového	hovorový	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikace	komunikace	k1gFnSc1
je	být	k5eAaImIp3nS
obousměrná	obousměrný	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
obou	dva	k4xCgInPc6
směrech	směr	k1gInPc6
spojitá	spojitý	k2eAgFnSc1d1
a	a	k8xC
současná	současný	k2eAgFnSc1d1
během	během	k7c2
trvání	trvání	k1gNnSc2
hovoru	hovor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
používat	používat	k5eAaImF
dva	dva	k4xCgInPc4
telefonní	telefonní	k2eAgInPc4d1
hovory	hovor	k1gInPc4
po	po	k7c6
jedné	jeden	k4xCgFnSc6
fyzické	fyzický	k2eAgFnSc6d1
dvoudrátové	dvoudrátový	k2eAgFnSc6d1
lince	linka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISDN	ISDN	kA
může	moct	k5eAaImIp3nS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
spojení	spojení	k1gNnPc2
vykonávat	vykonávat	k5eAaImF
určitý	určitý	k2eAgInSc4d1
druh	druh	k1gInSc4
zpracování	zpracování	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
analogový	analogový	k2eAgInSc1d1
přenos	přenos	k1gInSc1
<g/>
,	,	kIx,
anti-echo	anti-echo	k6eAd1
opatření	opatření	k1gNnSc2
apod.	apod.	kA
2	#num#	k4
typy	typ	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
Telefonní	telefonní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
se	s	k7c7
šířkou	šířka	k1gFnSc7
pásma	pásmo	k1gNnSc2
3,1	3,1	k4
kHz	khz	kA
–	–	k?
telefonní	telefonní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
kvalitou	kvalita	k1gFnSc7
odpovídající	odpovídající	k2eAgInSc1d1
klasické	klasický	k2eAgFnSc3d1
telefonní	telefonní	k2eAgFnSc3d1
lince	linka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Telefonní	telefonní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
se	s	k7c7
šířkou	šířka	k1gFnSc7
pásma	pásmo	k1gNnSc2
7	#num#	k4
kHz	khz	kA
–	–	k?
telefonní	telefonní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
kvalitou	kvalita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přináší	přinášet	k5eAaImIp3nS
kvalitu	kvalita	k1gFnSc4
rozhlasového	rozhlasový	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
službu	služba	k1gFnSc4
audio	audio	k2eAgFnSc4d1
na	na	k7c4
požádání	požádání	k1gNnSc4
</s>
<s>
Referenčními	referenční	k2eAgInPc7d1
standardy	standard	k1gInPc7
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
jsou	být	k5eAaImIp3nP
doporučení	doporučení	k1gNnSc4
ITU-T	ITU-T	k1gFnSc2
I	i	k9
<g/>
.241	.241	k4
<g/>
.1	.1	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ETSI	ETSI	kA
standard	standard	k1gInSc4
ETS	ETS	kA
300	#num#	k4
111	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teletex	Teletex	k1gInSc1
</s>
<s>
Služba	služba	k1gFnSc1
umožňující	umožňující	k2eAgFnSc1d1
účastníkům	účastník	k1gMnPc3
výměnu	výměna	k1gFnSc4
korespondence	korespondence	k1gFnSc2
ve	v	k7c6
formě	forma	k1gFnSc6
dokumentů	dokument	k1gInPc2
kódovaných	kódovaný	k2eAgInPc2d1
ve	v	k7c6
formátu	formát	k1gInSc6
Teletex	Teletex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
ISDN	ISDN	kA
představuje	představovat	k5eAaImIp3nS
terminálovou	terminálový	k2eAgFnSc4d1
aplikaci	aplikace	k1gFnSc4
nosné	nosný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
režim	režim	k1gInSc1
přepojování	přepojování	k1gNnPc1
okruhů	okruh	k1gInPc2
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
se	s	k7c7
strukturou	struktura	k1gFnSc7
8	#num#	k4
kHz	khz	kA
bez	bez	k7c2
omezení	omezení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teletex	Teletex	k1gInSc1
je	být	k5eAaImIp3nS
specifikovaný	specifikovaný	k2eAgInSc1d1
v	v	k7c6
ITU-T	ITU-T	k1gFnSc6
doporučení	doporučení	k1gNnSc2
I	i	k9
<g/>
.241	.241	k4
<g/>
.2	.2	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefax	telefax	k1gInSc1
4	#num#	k4
(	(	kIx(
<g/>
Telefax	telefax	k1gInSc1
Group	Group	k1gInSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
přenos	přenos	k1gInSc4
<g/>
,	,	kIx,
uchovávání	uchovávání	k1gNnSc4
a	a	k8xC
reprodukci	reprodukce	k1gFnSc4
graficky	graficky	k6eAd1
vyjádřené	vyjádřený	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
psaného	psaný	k2eAgInSc2d1
textu	text	k1gInSc2
nebo	nebo	k8xC
statického	statický	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
sítě	síť	k1gFnSc2
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
účely	účel	k1gInPc4
přenosu	přenos	k1gInSc2
a	a	k8xC
uchovávání	uchovávání	k1gNnPc4
informací	informace	k1gFnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
telefaxové	telefaxový	k2eAgNnSc4d1
kódování	kódování	k1gNnSc4
(	(	kIx(
<g/>
faksimilní	faksimilný	k2eAgMnPc1d1
formát	formát	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
v	v	k7c6
B-kanálu	B-kanála	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
korespondence	korespondence	k1gFnSc2
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
je	být	k5eAaImIp3nS
stránka	stránka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
specifikovaná	specifikovaný	k2eAgFnSc1d1
v	v	k7c6
ITU-T	ITU-T	k1gFnSc6
doporučení	doporučení	k1gNnSc2
I	i	k9
<g/>
.241	.241	k4
<g/>
.3	.3	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
ETSI	ETSI	kA
standardu	standard	k1gInSc2
ETS	ETS	kA
300	#num#	k4
120	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíšený	smíšený	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
kombinovanou	kombinovaný	k2eAgFnSc4d1
textovou	textový	k2eAgFnSc4d1
i	i	k8xC
faxovou	faxový	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
specifikovaná	specifikovaný	k2eAgFnSc1d1
v	v	k7c6
ITU-T	ITU-T	k1gFnSc6
doporučení	doporučení	k1gNnSc2
I	i	k9
<g/>
.241	.241	k4
<g/>
.4	.4	k4
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Videotex	Videotex	k1gInSc1
syntaxově	syntaxově	k6eAd1
orientovaný	orientovaný	k2eAgInSc1d1
</s>
<s>
Je	být	k5eAaImIp3nS
rozšířením	rozšíření	k1gNnSc7
klasické	klasický	k2eAgFnSc2d1
služby	služba	k1gFnSc2
VIDEOTEX	VIDEOTEX	kA
obohaceným	obohacený	k2eAgInSc7d1
o	o	k7c6
funkci	funkce	k1gFnSc6
Retrieval	Retrieval	k1gFnSc4
a	a	k8xC
Mailbox	mailbox	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
interaktivní	interaktivní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
<g/>
,	,	kIx,
uchovávání	uchovávání	k1gNnSc4
a	a	k8xC
reprodukci	reprodukce	k1gFnSc4
informace	informace	k1gFnSc2
ve	v	k7c6
vizuální	vizuální	k2eAgFnSc6d1
formě	forma	k1gFnSc6
s	s	k7c7
možností	možnost	k1gFnSc7
zvukové	zvukový	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenos	přenos	k1gInSc1
se	se	k3xPyFc4
uskutečňuje	uskutečňovat	k5eAaImIp3nS
mezi	mezi	k7c7
uživatelem	uživatel	k1gMnSc7
a	a	k8xC
vzdálenými	vzdálený	k2eAgInPc7d1
počítačovými	počítačový	k2eAgInPc7d1
servery	server	k1gInPc7
nebo	nebo	k8xC
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
Retrieval	Retrieval	k1gFnSc2
všeobecně	všeobecně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
přístupu	přístup	k1gInSc2
k	k	k7c3
bance	banka	k1gFnSc3
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
textové	textový	k2eAgInPc1d1
<g/>
,	,	kIx,
obrazové	obrazový	k2eAgInPc1d1
<g/>
,	,	kIx,
video	video	k1gNnSc1
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
telekomunikační	telekomunikační	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
Mailbox	mailbox	k1gInSc1
(	(	kIx(
<g/>
poštovní	poštovní	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
<g/>
)	)	kIx)
dovoluje	dovolovat	k5eAaImIp3nS
ukládání	ukládání	k1gNnSc1
přijaté	přijatý	k2eAgFnSc2d1
informace	informace	k1gFnSc2
na	na	k7c4
paměťové	paměťový	k2eAgNnSc4d1
médium	médium	k1gNnSc4
<g/>
,	,	kIx,
rezervované	rezervovaný	k2eAgInPc4d1
pro	pro	k7c4
daného	daný	k2eAgMnSc4d1
účastníka	účastník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedená	uvedený	k2eAgFnSc1d1
služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
specifikovaná	specifikovaný	k2eAgFnSc1d1
v	v	k7c6
ITU-T	ITU-T	k1gFnSc6
doporučení	doporučení	k1gNnSc2
I	i	k9
<g/>
.241	.241	k4
<g/>
.5	.5	k4
a	a	k8xC
v	v	k7c6
ETSI	ETSI	kA
standardu	standard	k1gInSc2
ETS	ETS	kA
300	#num#	k4
262	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telex	telex	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
zabezpečující	zabezpečující	k2eAgFnSc4d1
interaktivní	interaktivní	k2eAgFnSc4d1
textovou	textový	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
<g/>
;	;	kIx,
telexovou	telexový	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
upravují	upravovat	k5eAaImIp3nP
mezinárodní	mezinárodní	k2eAgInSc4d1
standardy	standard	k1gInPc4
pro	pro	k7c4
telexovou	telexový	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
doporučení	doporučení	k1gNnSc2
pro	pro	k7c4
síť	síť	k1gFnSc4
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s>
+	+	kIx~
další	další	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Přenos	přenos	k1gInSc1
hovorů	hovor	k1gInPc2
se	s	k7c7
šířkou	šířka	k1gFnSc7
pásma	pásmo	k1gNnSc2
7	#num#	k4
kHz	khz	kA
(	(	kIx(
<g/>
Telephony	Telephona	k1gFnSc2
7	#num#	k4
kHz	khz	kA
Teleservice	Teleservice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
služba	služba	k1gFnSc1
umožňující	umožňující	k2eAgFnPc1d1
přenos	přenos	k1gInSc4
informací	informace	k1gFnPc2
hovorových	hovorový	k2eAgInPc2d1
signálů	signál	k1gInPc2
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
se	s	k7c7
šířkou	šířka	k1gFnSc7
pásma	pásmo	k1gNnSc2
7	#num#	k4
nebo	nebo	k8xC
3,1	3,1	k4
kHz	khz	kA
prostřednictvím	prostřednictvím	k7c2
jednoho	jeden	k4xCgInSc2
kanálu	kanál	k1gInSc2
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
64	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Referenčními	referenční	k2eAgInPc7d1
standardy	standard	k1gInPc7
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
jsou	být	k5eAaImIp3nP
ITU-T	ITU-T	k1gFnSc4
doporučení	doporučení	k1gNnSc2
I	i	k9
<g/>
.241	.241	k4
<g/>
.7	.7	k4
a	a	k8xC
ETSI	ETSI	kA
standard	standard	k1gInSc4
ETS	ETS	kA
300	#num#	k4
263	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protokol	protokol	k1gInSc1
DSS1	DSS1	k1gFnSc2
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
je	být	k5eAaImIp3nS
specifikovaný	specifikovaný	k2eAgInSc1d1
v	v	k7c6
ETSI	ETSI	kA
standardu	standard	k1gInSc2
ETS	ETS	kA
300	#num#	k4
367	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Videotelefon	videotelefon	k1gInSc1
(	(	kIx(
<g/>
Videotelephony	Videotelephon	k1gInPc1
Teleservice	Teleservice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
je	být	k5eAaImIp3nS
audiovizuální	audiovizuální	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
hovorová	hovorový	k2eAgFnSc1d1
a	a	k8xC
obrazová	obrazový	k2eAgFnSc1d1
informace	informace	k1gFnSc1
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
přenáší	přenášet	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
nebo	nebo	k8xC
dvěma	dva	k4xCgFnPc7
64	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
B-kanály	B-kanál	k1gInPc7
v	v	k7c6
rámci	rámec	k1gInSc6
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
kompresi	komprese	k1gFnSc4
obrazu	obraz	k1gInSc2
a	a	k8xC
zvuku	zvuk	k1gInSc2
a	a	k8xC
v	v	k7c6
reálné	reálný	k2eAgFnSc6d1
aplikaci	aplikace	k1gFnSc6
obsazuje	obsazovat	k5eAaImIp3nS
oba	dva	k4xCgInPc1
dva	dva	k4xCgInPc4
B	B	kA
kanály	kanál	k1gInPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
i	i	k9
současný	současný	k2eAgInSc4d1
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedená	uvedený	k2eAgFnSc1d1
služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
specifikovaná	specifikovaný	k2eAgFnSc1d1
v	v	k7c4
doporučení	doporučení	k1gNnSc4
ITU-T	ITU-T	k1gFnSc2
F.	F.	kA
<g/>
721	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
ETSI	ETSI	kA
standard	standard	k1gInSc4
ETS	ETS	kA
300	#num#	k4
264	#num#	k4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Doplňkové	doplňkový	k2eAgInPc4d1
(	(	kIx(
<g/>
supplementary	supplementar	k1gInPc4
<g/>
)	)	kIx)
služby	služba	k1gFnPc4
</s>
<s>
Doplňkové	doplňkový	k2eAgFnPc4d1
(	(	kIx(
<g/>
přídavné	přídavný	k2eAgFnPc4d1
<g/>
)	)	kIx)
služby	služba	k1gFnPc4
(	(	kIx(
<g/>
Supplementary	Supplementar	k1gInPc4
Services	Services	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
určené	určený	k2eAgFnPc1d1
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
možností	možnost	k1gFnPc2
základních	základní	k2eAgFnPc2d1
transportních	transportní	k2eAgFnPc2d1
a	a	k8xC
standardních	standardní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
–	–	k?
doplňkové	doplňkový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
integrované	integrovaný	k2eAgInPc1d1
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
počet	počet	k1gInSc1
není	být	k5eNaImIp3nS
konečný	konečný	k2eAgInSc1d1
<g/>
,	,	kIx,
rozšiřuje	rozšiřovat	k5eAaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
operátorů	operátor	k1gMnPc2
a	a	k8xC
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
rozsahem	rozsah	k1gInSc7
tvoří	tvořit	k5eAaImIp3nP
největší	veliký	k2eAgFnSc4d3
skupinu	skupina	k1gFnSc4
služeb	služba	k1gFnPc2
ISDN	ISDN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživateli	uživatel	k1gMnSc3
dávají	dávat	k5eAaImIp3nP
větší	veliký	k2eAgFnPc4d2
manipulační	manipulační	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
a	a	k8xC
zvyšují	zvyšovat	k5eAaImIp3nP
úroveň	úroveň	k1gFnSc4
komunikačních	komunikační	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplňkové	doplňkový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
vlastně	vlastně	k9
rozšiřují	rozšiřovat	k5eAaImIp3nP
původní	původní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
digitální	digitální	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Standard	standard	k1gInSc1
Euro	euro	k1gNnSc1
<g/>
–	–	k?
<g/>
ISDN	ISDN	kA
zavazuje	zavazovat	k5eAaImIp3nS
používat	používat	k5eAaImF
těchto	tento	k3xDgInPc2
5	#num#	k4
služeb	služba	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
CLIP	CLIP	kA
–	–	k?
identifikace	identifikace	k1gFnSc1
volajícího	volající	k2eAgNnSc2d1
</s>
<s>
CLIR	CLIR	kA
–	–	k?
zamezení	zamezení	k1gNnSc1
identifikace	identifikace	k1gFnSc2
volajícího	volající	k2eAgMnSc2d1
</s>
<s>
DDI	DDI	kA
–	–	k?
provolba	provolba	k1gFnSc1
</s>
<s>
MSN	MSN	kA
–	–	k?
vícenásobné	vícenásobný	k2eAgNnSc1d1
účastnické	účastnický	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
TP	TP	kA
–	–	k?
přenositelnost	přenositelnost	k1gFnSc1
koncového	koncový	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Digitálna	Digitálna	k1gFnSc1
sieť	sieť	k1gFnSc1
integrovaných	integrovaný	k2eAgFnPc2d1
služieb	služiba	k1gFnPc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Pavol	Pavola	k1gFnPc2
Kukura	Kukura	k1gFnSc1
<g/>
:	:	kIx,
ISDN	ISDN	kA
<g/>
,	,	kIx,
B-ISDN	B-ISDN	k1gFnSc1
<g/>
,	,	kIx,
ATM	ATM	kA
<g/>
:	:	kIx,
digitálne	digitálnout	k5eAaPmIp3nS,k5eAaImIp3nS
sítě	síť	k1gFnPc4
s	s	k7c7
integrovanými	integrovaný	k2eAgFnPc7d1
službami	služba	k1gFnPc7
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
FABER	FABER	kA
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
967503	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
↑	↑	k?
Annabel	Annabel	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Z.	Z.	kA
Dodd	Dodd	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Essential	Essential	k1gMnSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Telecommunications	Telecommunications	k1gInSc1
<g/>
,	,	kIx,
Upper	Upper	k1gMnSc1
Saddle	Saddle	k1gMnSc1
River	River	k1gMnSc1
<g/>
,	,	kIx,
NJ	NJ	kA
<g/>
:	:	kIx,
Prentice	Prentice	k1gFnSc1
Hall	Hall	k1gInSc1
PTR	PTR	kA
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
305	#num#	k4
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
64907	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
↑	↑	k?
Issue	Issue	k1gNnPc2
Date	Date	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
May	May	k1gMnSc1
1986	#num#	k4
Volume	volum	k1gInSc5
<g/>
:	:	kIx,
4	#num#	k4
Issue	Issue	k1gFnPc2
<g/>
:	:	kIx,
3	#num#	k4
On	on	k3xPp3gMnSc1
page	pagat	k5eAaPmIp3nS
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
320-325	320-325	k4
ISSN	ISSN	kA
0	#num#	k4
<g/>
733	#num#	k4
<g/>
-	-	kIx~
<g/>
8716	#num#	k4
<g/>
↑	↑	k?
http://www.topbits.com/difference-between-isdn-and-dsl.html	http://www.topbits.com/difference-between-isdn-and-dsl.html	k1gMnSc1
<g/>
↑	↑	k?
Konvit	Konvit	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
:	:	kIx,
Sprievodca	Sprievodca	k1gMnSc1
po	po	k7c6
datových	datový	k2eAgFnPc6d1
a	a	k8xC
počítačových	počítačový	k2eAgFnPc6d1
komunikáciách	komunikáciy	k1gFnPc6
<g/>
,	,	kIx,
Poradca	Poradca	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
↑	↑	k?
Stan	stan	k1gInSc1
Schatt	Schatta	k1gFnPc2
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
LAN	lano	k1gNnPc2
od	od	k7c2
A	a	k8xC
do	do	k7c2
Z	Z	kA
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
108	#num#	k4
-1091	-1091	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
ITU	ITU	kA
http://www.itu.int/rec/T-REC-I/e	http://www.itu.int/rec/T-REC-I/	k1gInSc2
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.nationalisdncouncil.com	www.nationalisdncouncil.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.infopage.net	www.infopage.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:52000DC0267:EN:HTML	http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:52000DC0267:EN:HTML	k4
<g/>
↑	↑	k?
TELECOMMUNICATION	TELECOMMUNICATION	kA
STANDARDIZATION	STANDARDIZATION	kA
SECTOR	SECTOR	kA
OF	OF	kA
ITU	ITU	kA
http://web.archive.org/web/20120111152330/	http://web.archive.org/web/20120111152330/	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
eu	eu	k?
<g/>
.	.	kIx.
<g/>
sabotage	sabotage	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.catr.cn/radar/itut/201007/P020100707484599419033.pdf%5B%5D	http://www.catr.cn/radar/itut/201007/P020100707484599419033.pdf%5B%5D	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
tkhf	tkhf	k1gInSc1
<g/>
.	.	kIx.
<g/>
adaxas	adaxas	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Telefon	telefon	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
vývoj	vývoj	k1gInSc1
</s>
<s>
Telefonní	telefonní	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
</s>
<s>
Modem	modem	k1gInSc1
</s>
<s>
VoIP	VoIP	k?
–	–	k?
Voice	Voika	k1gFnSc3
over	over	k1gMnSc1
IP	IP	kA
</s>
<s>
TCP	TCP	kA
<g/>
/	/	kIx~
<g/>
IP	IP	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ISDN	ISDN	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.ralphb.net/ISDN/	http://www.ralphb.net/ISDN/	k?
</s>
<s>
http://www.tntel.sk/informacne-a-telekomunikacne-systemy/podpora/slovnik-pojmov-isdn/%5B%5D	http://www.tntel.sk/informacne-a-telekomunikacne-systemy/podpora/slovnik-pojmov-isdn/%5B%5D	k4
</s>
<s>
http://www.camblab.com/nugget/isdn_bkk.pdf	http://www.camblab.com/nugget/isdn_bkk.pdf	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
The	The	k?
Isdn	Isdn	k1gMnSc1
Subscriber	Subscriber	k1gMnSc1
Loop	Loop	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Připojení	připojení	k1gNnSc1
k	k	k7c3
Internetu	Internet	k1gInSc3
</s>
<s>
Dial-up	Dial-up	k1gInSc1
•	•	k?
DSL	DSL	kA
•	•	k?
Elektrická	elektrický	k2eAgFnSc1d1
síť	síť	k1gFnSc1
•	•	k?
Flash-OFDM	Flash-OFDM	k1gFnSc2
•	•	k?
Optický	optický	k2eAgInSc1d1
kabel	kabel	k1gInSc1
•	•	k?
ISDN	ISDN	kA
•	•	k?
Kabelová	kabelový	k2eAgFnSc1d1
TV	TV	kA
•	•	k?
Satelitní	satelitní	k2eAgInSc4d1
přístup	přístup	k1gInSc4
•	•	k?
Wi-Fi	Wi-F	k1gFnSc2
•	•	k?
WiMAX	WiMAX	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4114048-5	4114048-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
86000852	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
86000852	#num#	k4
</s>
