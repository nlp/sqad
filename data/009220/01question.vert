<s>
Byl	být	k5eAaImAgInS	být
Kuusinen	Kuusinen	k1gInSc1	Kuusinen
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
vůdců	vůdce	k1gMnPc2	vůdce
radikálního	radikální	k2eAgNnSc2d1	radikální
křídla	křídlo	k1gNnSc2	křídlo
finských	finský	k2eAgMnPc2d1	finský
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
rudého	rudý	k2eAgNnSc2d1	Rudé
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
ve	v	k7c4	v
finskou	finský	k2eAgFnSc4d1	finská
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
?	?	kIx.	?
</s>
