<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Ville	Ville	k1gFnSc2	Ville
(	(	kIx(	(
<g/>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
)	)	kIx)	)
Kuusinen	Kuusinen	k1gInSc1	Kuusinen
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
О	О	k?	О
В	В	k?	В
К	К	k?	К
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
Laukaa	Laukaa	k1gFnSc1	Laukaa
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
finský	finský	k2eAgMnSc1d1	finský
a	a	k8xC	a
sovětský	sovětský	k2eAgMnSc1d1	sovětský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kuusinen	Kuusinen	k1gInSc4	Kuusinen
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
vůdců	vůdce	k1gMnPc2	vůdce
radikálního	radikální	k2eAgNnSc2d1	radikální
křídla	křídlo	k1gNnSc2	křídlo
finských	finský	k2eAgMnPc2d1	finský
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
rudého	rudý	k2eAgNnSc2d1	Rudé
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
ve	v	k7c4	v
finskou	finský	k2eAgFnSc4d1	finská
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc2	povstání
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
politika	politik	k1gMnSc4	politik
-	-	kIx~	-
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
sekretariátu	sekretariát	k1gInSc2	sekretariát
Kominterny	Kominterna	k1gFnSc2	Kominterna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Velkého	velký	k2eAgInSc2d1	velký
teroru	teror	k1gInSc2	teror
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
posloužil	posloužit	k5eAaPmAgMnS	posloužit
Stalinovi	Stalin	k1gMnSc3	Stalin
při	při	k7c6	při
decimaci	decimace	k1gFnSc6	decimace
finské	finský	k2eAgFnSc6d1	finská
KS	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
přepadení	přepadení	k1gNnSc4	přepadení
Finska	Finsko	k1gNnSc2	Finsko
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
loutkovou	loutkový	k2eAgFnSc4d1	loutková
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
finskou	finský	k2eAgFnSc4d1	finská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vládu	vláda	k1gFnSc4	vláda
z	z	k7c2	z
Terijoki	Terijok	k1gFnSc2	Terijok
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Zelenogorsk	Zelenogorsk	k1gInSc1	Zelenogorsk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
Stalinem	Stalin	k1gMnSc7	Stalin
a	a	k8xC	a
kterou	který	k3yIgFnSc4	který
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
jím	jíst	k5eAaImIp1nS	jíst
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
vládu	vláda	k1gFnSc4	vláda
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vývoji	vývoj	k1gInSc6	vývoj
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
SSSR	SSSR	kA	SSSR
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
zdařilý	zdařilý	k2eAgInSc1d1	zdařilý
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
myšlenka	myšlenka	k1gFnSc1	myšlenka
této	tento	k3xDgFnSc2	tento
vlády	vláda	k1gFnSc2	vláda
opuštěna	opustit	k5eAaPmNgFnS	opustit
a	a	k8xC	a
Kuusinen	Kuusinen	k1gInSc1	Kuusinen
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
sovětského	sovětský	k2eAgNnSc2d1	sovětské
politika	politikum	k1gNnSc2	politikum
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
Karelo-finské	Kareloinský	k2eAgFnSc2d1	Karelo-finská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
měl	mít	k5eAaImAgInS	mít
Kuusinen	Kuusinen	k1gInSc1	Kuusinen
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
pověst	pověst	k1gFnSc4	pověst
zrádce	zrádce	k1gMnSc1	zrádce
jen	jen	k9	jen
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
po	po	k7c6	po
Zimní	zimní	k2eAgFnSc6d1	zimní
válce	válka	k1gFnSc6	válka
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
nenáviděn	návidět	k5eNaImNgInS	návidět
prakticky	prakticky	k6eAd1	prakticky
všemi	všecek	k3xTgMnPc7	všecek
Finy	Fin	k1gMnPc7	Fin
jakožto	jakožto	k8xS	jakožto
neoddiskutovatelný	oddiskutovatelný	k2eNgMnSc1d1	neoddiskutovatelný
zrádce	zrádce	k1gMnSc1	zrádce
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
SSSR	SSSR	kA	SSSR
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
znovu	znovu	k6eAd1	znovu
povolena	povolen	k2eAgFnSc1d1	povolena
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Kuusinenovi	Kuusinen	k1gMnSc3	Kuusinen
nebylo	být	k5eNaImAgNnS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
domů	dům	k1gInPc2	dům
a	a	k8xC	a
převzít	převzít	k5eAaPmF	převzít
její	její	k3xOp3gNnSc4	její
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
by	by	kYmCp3nP	by
strana	strana	k1gFnSc1	strana
nemohla	moct	k5eNaImAgFnS	moct
doufat	doufat	k5eAaImF	doufat
ve	v	k7c4	v
slušný	slušný	k2eAgInSc4d1	slušný
výsledek	výsledek	k1gInSc4	výsledek
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
namísto	namísto	k7c2	namísto
něj	on	k3xPp3gInSc2	on
řídila	řídit	k5eAaImAgFnS	řídit
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Hertta	Hertt	k1gInSc2	Hertt
<g/>
.	.	kIx.	.
</s>
<s>
Kuusinen	Kuusinen	k1gInSc4	Kuusinen
sám	sám	k3xTgInSc4	sám
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
komunistického	komunistický	k2eAgNnSc2d1	komunistické
politika	politikum	k1gNnSc2	politikum
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
a	a	k8xC	a
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Otto	Otto	k1gMnSc1	Otto
Ville	Ville	k1gNnSc2	Ville
Kuusinen	Kuusinno	k1gNnPc2	Kuusinno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Otto	Otto	k1gMnSc1	Otto
Ville	Ville	k1gFnSc2	Ville
Kuusinen	Kuusinen	k1gInSc1	Kuusinen
</s>
</p>
