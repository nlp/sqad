<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
česká	český	k2eAgFnSc1d1	Česká
levicová	levicový	k2eAgFnSc1d1	levicová
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
internacionály	internacionála	k1gFnSc2	internacionála
<g/>
,	,	kIx,	,
Progresivní	progresivní	k2eAgFnSc2d1	progresivní
aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
Strany	strana	k1gFnSc2	strana
evropských	evropský	k2eAgMnPc2d1	evropský
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnPc1d1	demokratická
jsou	být	k5eAaImIp3nP	být
patrny	patrn	k2eAgFnPc1d1	patrna
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
frakce	frakce	k1gFnPc1	frakce
strany	strana	k1gFnSc2	strana
<g/>
:	:	kIx,	:
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hájí	hájit	k5eAaImIp3nS	hájit
víc	hodně	k6eAd2	hodně
tradiční	tradiční	k2eAgFnPc4d1	tradiční
sociálnědemokratické	sociálnědemokratický	k2eAgFnPc4d1	sociálnědemokratická
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
levicový	levicový	k2eAgInSc4d1	levicový
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
euroskepticistická	euroskepticistický	k2eAgFnSc1d1	euroskepticistický
<g/>
,	,	kIx,	,
a	a	k8xC	a
liberální	liberální	k2eAgFnSc1d1	liberální
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
progresivismu	progresivismus	k1gInSc2	progresivismus
či	či	k8xC	či
sociálnímu	sociální	k2eAgInSc3d1	sociální
liberalismu	liberalismus	k1gInSc3	liberalismus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
proevropská	proevropský	k2eAgFnSc1d1	proevropská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
41	[number]	k4	41
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgInPc1d1	demokratický
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
zvolen	zvolen	k2eAgMnSc1d1	zvolen
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
a	a	k8xC	a
statutárním	statutární	k2eAgMnSc7d1	statutární
místopředsedou	místopředseda	k1gMnSc7	místopředseda
bývalý	bývalý	k2eAgMnSc1d1	bývalý
primátor	primátor	k1gMnSc1	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
Dělnické	dělnický	k2eAgNnSc1d1	dělnické
hnutí	hnutí	k1gNnSc1	hnutí
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
českého	český	k2eAgNnSc2d1	české
dělnického	dělnický	k2eAgNnSc2d1	dělnické
hnutí	hnutí	k1gNnSc2	hnutí
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
odborové	odborový	k2eAgFnPc1d1	odborová
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInPc1d1	vzdělávací
spolky	spolek	k1gInPc1	spolek
českého	český	k2eAgNnSc2d1	české
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
svépomocné	svépomocný	k2eAgNnSc1d1	svépomocné
družstvo	družstvo	k1gNnSc1	družstvo
Oul	Oul	k1gFnSc2	Oul
vedené	vedený	k2eAgFnSc2d1	vedená
Františkem	František	k1gMnSc7	František
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Chleborádem	Chleborád	k1gMnSc7	Chleborád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
masová	masový	k2eAgFnSc1d1	masová
demonstrace	demonstrace	k1gFnSc1	demonstrace
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
"	"	kIx"	"
<g/>
tábor	tábor	k1gInSc1	tábor
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1878	[number]	k4	1878
se	se	k3xPyFc4	se
v	v	k7c6	v
břevnovském	břevnovský	k2eAgInSc6d1	břevnovský
hostinci	hostinec	k1gInSc6	hostinec
U	u	k7c2	u
Kaštanu	kaštan	k1gInSc2	kaštan
konal	konat	k5eAaImAgInS	konat
ustavující	ustavující	k2eAgInSc1d1	ustavující
sjezd	sjezd	k1gInSc1	sjezd
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
organizace	organizace	k1gFnSc2	organizace
českých	český	k2eAgMnPc2d1	český
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celorakouské	celorakouský	k2eAgFnSc2d1	celorakouská
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
českoslovanská	českoslovanský	k2eAgFnSc1d1	českoslovanská
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proces	proces	k1gInSc1	proces
osamostatňování	osamostatňování	k1gNnSc2	osamostatňování
české	český	k2eAgFnSc2d1	Česká
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
rakouské	rakouský	k2eAgFnSc2d1	rakouská
internacionály	internacionála	k1gFnSc2	internacionála
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
v	v	k7c6	v
založení	založení	k1gNnSc6	založení
takticky	takticky	k6eAd1	takticky
a	a	k8xC	a
organizačně	organizačně	k6eAd1	organizačně
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
4	[number]	k4	4
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
v	v	k7c6	v
říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
roku	rok	k1gInSc3	rok
1897	[number]	k4	1897
učinilo	učinit	k5eAaPmAgNnS	učinit
pět	pět	k4xCc1	pět
sociálně	sociálně	k6eAd1	sociálně
demokratických	demokratický	k2eAgMnPc2d1	demokratický
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Berner	Berner	k1gMnSc1	Berner
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Cingr	Cingr	k1gMnSc1	Cingr
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hybeš	Hybeš	k1gMnSc1	Hybeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Steiner	Steiner	k1gMnSc1	Steiner
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Vrátný	vrátný	k1gMnSc1	vrátný
<g/>
)	)	kIx)	)
protistátoprávní	protistátoprávní	k2eAgNnSc1d1	protistátoprávní
prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
ČSNS	ČSNS	kA	ČSNS
<g/>
.	.	kIx.	.
</s>
<s>
Programově	programově	k6eAd1	programově
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
II	II	kA	II
<g/>
.	.	kIx.	.
internacionále	internacionála	k1gFnSc3	internacionála
<g/>
,	,	kIx,	,
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
uznání	uznání	k1gNnSc4	uznání
osmihodinové	osmihodinový	k2eAgFnSc2d1	osmihodinová
pracovní	pracovní	k2eAgFnSc2d1	pracovní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
úpravu	úprava	k1gFnSc4	úprava
mzdových	mzdový	k2eAgInPc2d1	mzdový
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
1907	[number]	k4	1907
česká	český	k2eAgFnSc1d1	Česká
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Antonína	Antonín	k1gMnSc2	Antonín
Němce	Němec	k1gMnSc2	Němec
poprvé	poprvé	k6eAd1	poprvé
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
přepočtení	přepočtení	k1gNnSc6	přepočtení
na	na	k7c4	na
kurie	kurie	k1gFnPc4	kurie
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
za	za	k7c7	za
agrární	agrární	k2eAgFnSc7d1	agrární
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
získala	získat	k5eAaPmAgFnS	získat
6	[number]	k4	6
<g/>
.	.	kIx.	.
nejpočetnější	početní	k2eAgInSc1d3	nejpočetnější
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
1911	[number]	k4	1911
strana	strana	k1gFnSc1	strana
ještě	ještě	k9	ještě
posílila	posílit	k5eAaPmAgFnS	posílit
a	a	k8xC	a
klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
získal	získat	k5eAaPmAgInS	získat
další	další	k2eAgInSc4d1	další
3	[number]	k4	3
poslance	poslanec	k1gMnSc2	poslanec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
opakovala	opakovat	k5eAaImAgFnS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejpočetnější	početní	k2eAgInSc1d3	nejpočetnější
klub	klub	k1gInSc1	klub
za	za	k7c7	za
Českým	český	k2eAgInSc7d1	český
klubem	klub	k1gInSc7	klub
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
strana	strana	k1gFnSc1	strana
odmítala	odmítat	k5eAaImAgFnS	odmítat
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
plně	plně	k6eAd1	plně
loajální	loajální	k2eAgFnSc1d1	loajální
k	k	k7c3	k
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc3	Rakousku-Uhersko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
===	===	k?	===
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
(	(	kIx(	(
<g/>
ČSDSD	ČSDSD	kA	ČSDSD
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přejmenováním	přejmenování	k1gNnSc7	přejmenování
Českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vytvoření	vytvoření	k1gNnSc2	vytvoření
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Revolučním	revoluční	k2eAgNnSc6d1	revoluční
národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
47	[number]	k4	47
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
vycházelo	vycházet	k5eAaImAgNnS	vycházet
se	se	k3xPyFc4	se
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
zastoupení	zastoupení	k1gNnSc1	zastoupení
třemi	tři	k4xCgInPc7	tři
ministry	ministr	k1gMnPc7	ministr
v	v	k7c6	v
první	první	k4xOgFnSc6	první
československé	československý	k2eAgFnSc6d1	Československá
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
druhé	druhý	k4xOgNnSc4	druhý
(	(	kIx(	(
<g/>
rudozelené	rudozelený	k2eAgFnSc3d1	rudozelená
<g/>
)	)	kIx)	)
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
sociálnědemokratický	sociálnědemokratický	k2eAgMnSc1d1	sociálnědemokratický
vůdce	vůdce	k1gMnSc1	vůdce
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tusar	Tusar	k1gMnSc1	Tusar
místo	místo	k7c2	místo
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
obdržela	obdržet	k5eAaPmAgFnS	obdržet
vítězná	vítězný	k2eAgFnSc1d1	vítězná
ČSDSD	ČSDSD	kA	ČSDSD
25,7	[number]	k4	25,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
74	[number]	k4	74
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
rozmachu	rozmach	k1gInSc2	rozmach
však	však	k9	však
již	již	k6eAd1	již
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
probíhal	probíhat	k5eAaImAgInS	probíhat
politický	politický	k2eAgInSc1d1	politický
rozkol	rozkol	k1gInSc1	rozkol
mezi	mezi	k7c7	mezi
vedoucím	vedoucí	k2eAgInSc7d1	vedoucí
státotvorným	státotvorný	k2eAgInSc7d1	státotvorný
proudem	proud	k1gInSc7	proud
a	a	k8xC	a
marxisticko-leninskou	marxistickoeninský	k2eAgFnSc7d1	marxisticko-leninská
opozicí	opozice	k1gFnSc7	opozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
ke	k	k7c3	k
Komunistické	komunistický	k2eAgFnSc3d1	komunistická
internacionále	internacionála	k1gFnSc3	internacionála
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnSc1d1	opoziční
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
ustavila	ustavit	k5eAaPmAgFnS	ustavit
se	se	k3xPyFc4	se
jako	jako	k9	jako
ČSDSD-levice	ČSDSDevice	k1gFnSc1	ČSDSD-levice
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
samostatný	samostatný	k2eAgInSc4d1	samostatný
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
23	[number]	k4	23
poslanců	poslanec	k1gMnPc2	poslanec
tohoto	tento	k3xDgInSc2	tento
křídla	křídlo	k1gNnPc4	křídlo
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Rozkol	rozkol	k1gInSc1	rozkol
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
prakticky	prakticky	k6eAd1	prakticky
dovršen	dovršit	k5eAaPmNgInS	dovršit
sjezdem	sjezd	k1gInSc7	sjezd
zbytku	zbytek	k1gInSc2	zbytek
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
nový	nový	k2eAgInSc1d1	nový
Dělnický	dělnický	k2eAgInSc1d1	dělnický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
program	program	k1gInSc1	program
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
odmítnuty	odmítnut	k2eAgFnPc1d1	odmítnuta
zásady	zásada	k1gFnPc1	zásada
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
internacionály	internacionála	k1gFnSc2	internacionála
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1921	[number]	k4	1921
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdvojení	rozdvojení	k1gNnSc1	rozdvojení
ČSDSD	ČSDSD	kA	ČSDSD
oslabilo	oslabit	k5eAaPmAgNnS	oslabit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
obdržela	obdržet	k5eAaPmAgFnS	obdržet
pouhých	pouhý	k2eAgNnPc6d1	pouhé
8,9	[number]	k4	8,9
%	%	kIx~	%
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
13,2	[number]	k4	13,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
let	let	k1gInSc4	let
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
(	(	kIx(	(
<g/>
období	období	k1gNnSc4	období
vlády	vláda	k1gFnSc2	vláda
panské	panský	k2eAgFnSc2d1	Panská
koalice	koalice	k1gFnSc2	koalice
<g/>
)	)	kIx)	)
strana	strana	k1gFnSc1	strana
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
vládách	vláda	k1gFnPc6	vláda
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
tiskovým	tiskový	k2eAgInSc7d1	tiskový
orgánem	orgán	k1gInSc7	orgán
bylo	být	k5eAaImAgNnS	být
Právo	právo	k1gNnSc1	právo
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
zvolen	zvolit	k5eAaPmNgMnS	zvolit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tajemník	tajemník	k1gMnSc1	tajemník
Svazu	svaz	k1gInSc2	svaz
kovodělníků	kovodělník	k1gMnPc2	kovodělník
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
NS	NS	kA	NS
Antonín	Antonín	k1gMnSc1	Antonín
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Antonína	Antonín	k1gMnSc4	Antonín
Němce	Němec	k1gMnSc4	Němec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
ČSDSD	ČSDSD	kA	ČSDSD
mladoboleslavský	mladoboleslavský	k2eAgMnSc1d1	mladoboleslavský
senátor	senátor	k1gMnSc1	senátor
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Dundr	Dundr	k1gInSc4	Dundr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
spojence	spojenec	k1gMnPc4	spojenec
ČSDSD	ČSDSD	kA	ČSDSD
Československá	československý	k2eAgFnSc1d1	Československá
národně	národně	k6eAd1	národně
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
Německá	německý	k2eAgFnSc1d1	německá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
sloučení	sloučení	k1gNnSc4	sloučení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
kvůli	kvůli	k7c3	kvůli
některým	některý	k3yIgFnPc3	některý
rozlišně	rozlišně	k6eAd1	rozlišně
chápaným	chápaný	k2eAgFnPc3d1	chápaná
programový	programový	k2eAgMnSc1d1	programový
bodům	bod	k1gInPc3	bod
(	(	kIx(	(
<g/>
internacionální	internacionální	k2eAgFnSc1d1	internacionální
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
menšinu	menšina	k1gFnSc4	menšina
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozkolu	rozkol	k1gInSc3	rozkol
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc3	jejich
protistátotvornému	protistátotvorný	k2eAgInSc3d1	protistátotvorný
postoji	postoj	k1gInSc3	postoj
však	však	k9	však
příliš	příliš	k6eAd1	příliš
rozvrstvená	rozvrstvený	k2eAgFnSc1d1	rozvrstvená
československá	československý	k2eAgFnSc1d1	Československá
levice	levice	k1gFnSc1	levice
ztratila	ztratit	k5eAaPmAgFnS	ztratit
post	post	k1gInSc4	post
rozhodujícího	rozhodující	k2eAgMnSc2d1	rozhodující
vládního	vládní	k2eAgMnSc2d1	vládní
člena	člen	k1gMnSc2	člen
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
umisťovala	umisťovat	k5eAaImAgFnS	umisťovat
až	až	k9	až
za	za	k7c7	za
Agrární	agrární	k2eAgFnSc7d1	agrární
republikánskou	republikánský	k2eAgFnSc7d1	republikánská
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
"	"	kIx"	"
<g/>
Panské	panský	k2eAgFnSc2d1	Panská
koalice	koalice	k1gFnSc2	koalice
<g/>
"	"	kIx"	"
k	k	k7c3	k
Národní	národní	k2eAgFnSc3d1	národní
straně	strana	k1gFnSc3	strana
práce	práce	k1gFnSc2	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
sjezdu	sjezd	k1gInSc2	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
tradicím	tradice	k1gFnPc3	tradice
marxismu	marxismus	k1gInSc2	marxismus
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
sjezdu	sjezd	k1gInSc6	sjezd
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
nový	nový	k2eAgInSc1d1	nový
stranický	stranický	k2eAgInSc1d1	stranický
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
orientaci	orientace	k1gFnSc4	orientace
revidoval	revidovat	k5eAaImAgMnS	revidovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
strana	strana	k1gFnSc1	strana
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
spoluzaložili	spoluzaložit	k5eAaPmAgMnP	spoluzaložit
Národní	národní	k2eAgFnSc4d1	národní
stranu	strana	k1gFnSc4	strana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
též	též	k9	též
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
evropskými	evropský	k2eAgFnPc7d1	Evropská
sociálně	sociálně	k6eAd1	sociálně
demokratickými	demokratický	k2eAgFnPc7d1	demokratická
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
aktivní	aktivní	k2eAgFnSc7d1	aktivní
členkou	členka	k1gFnSc7	členka
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
internacionály	internacionála	k1gFnSc2	internacionála
(	(	kIx(	(
<g/>
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
František	František	k1gMnSc1	František
Soukup	Soukup	k1gMnSc1	Soukup
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
Betty	Betty	k1gFnSc1	Betty
Karpíšková	Karpíšková	k1gFnSc1	Karpíšková
a	a	k8xC	a
Lev	Lev	k1gMnSc1	Lev
Winter	Winter	k1gMnSc1	Winter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
spolupracovala	spolupracovat	k5eAaImAgNnP	spolupracovat
s	s	k7c7	s
Německou	německý	k2eAgFnSc7d1	německá
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
(	(	kIx(	(
<g/>
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1919	[number]	k4	1919
Josefem	Josef	k1gMnSc7	Josef
Seligerem	Seliger	k1gMnSc7	Seliger
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
organizovala	organizovat	k5eAaBmAgFnS	organizovat
pomoc	pomoc	k1gFnSc1	pomoc
německé	německý	k2eAgFnSc2d1	německá
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
rakouské	rakouský	k2eAgFnSc6d1	rakouská
<g/>
)	)	kIx)	)
demokratické	demokratický	k2eAgFnSc6d1	demokratická
a	a	k8xC	a
socialistické	socialistický	k2eAgFnSc6d1	socialistická
emigraci	emigrace	k1gFnSc6	emigrace
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
podporovala	podporovat	k5eAaImAgFnS	podporovat
boj	boj	k1gInSc4	boj
španělské	španělský	k2eAgFnSc2d1	španělská
levice	levice	k1gFnSc2	levice
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
sdílela	sdílet	k5eAaImAgFnS	sdílet
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
Benešovu	Benešův	k2eAgFnSc4d1	Benešova
koncepci	koncepce	k1gFnSc4	koncepce
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
ženevský	ženevský	k2eAgInSc4d1	ženevský
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
východní	východní	k2eAgNnSc1d1	východní
Locarno	Locarno	k1gNnSc1	Locarno
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
pakt	pakt	k1gInSc1	pakt
<g/>
,	,	kIx,	,
obranné	obranný	k2eAgFnSc2d1	obranná
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Hodžův	Hodžův	k2eAgInSc1d1	Hodžův
plán	plán	k1gInSc1	plán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
v	v	k7c6	v
letech	let	k1gInPc6	let
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
===	===	k?	===
</s>
</p>
<p>
<s>
viz	vidět	k5eAaImRp2nS	vidět
Národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
Národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
pracující	pracující	k2eAgFnSc2d1	pracující
mládeže	mládež	k1gFnSc2	mládež
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
bývalých	bývalý	k2eAgMnPc2d1	bývalý
členů	člen	k1gMnPc2	člen
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
zapojila	zapojit	k5eAaPmAgNnP	zapojit
do	do	k7c2	do
odbojové	odbojový	k2eAgFnSc2d1	odbojová
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
měla	mít	k5eAaImAgFnS	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
vládě	vláda	k1gFnSc6	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
radě	rada	k1gFnSc6	rada
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léta	léto	k1gNnSc2	léto
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
(	(	kIx(	(
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
obnovena	obnoven	k2eAgFnSc1d1	obnovena
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
povolených	povolený	k2eAgFnPc2d1	povolená
českých	český	k2eAgFnPc2d1	Česká
stran	strana	k1gFnPc2	strana
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
obdržela	obdržet	k5eAaPmAgFnS	obdržet
15,6	[number]	k4	15,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
nejméně	málo	k6eAd3	málo
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
českých	český	k2eAgFnPc2d1	Česká
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
levicových	levicový	k2eAgMnPc2d1	levicový
voličů	volič	k1gMnPc2	volič
se	se	k3xPyFc4	se
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
ke	k	k7c3	k
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
k	k	k7c3	k
ČSNS	ČSNS	kA	ČSNS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
představitelé	představitel	k1gMnPc1	představitel
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
práce	práce	k1gFnSc2	práce
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fierlinger	Fierlinger	k1gMnSc1	Fierlinger
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
první	první	k4xOgFnSc2	první
poválečné	poválečný	k2eAgFnSc2d1	poválečná
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
však	však	k9	však
opět	opět	k6eAd1	opět
vykrystalizovaly	vykrystalizovat	k5eAaPmAgInP	vykrystalizovat
dva	dva	k4xCgInPc1	dva
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1947	[number]	k4	1947
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
centrista	centrista	k1gMnSc1	centrista
Bohumil	Bohumil	k1gMnSc1	Bohumil
Laušman	Laušman	k1gMnSc1	Laušman
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
zastánce	zastánce	k1gMnSc2	zastánce
samostatného	samostatný	k2eAgInSc2d1	samostatný
vývoje	vývoj	k1gInSc2	vývoj
strany	strana	k1gFnSc2	strana
Václava	Václav	k1gMnSc2	Václav
Majera	Majer	k1gMnSc2	Majer
<g/>
)	)	kIx)	)
a	a	k8xC	a
komunisty	komunista	k1gMnSc2	komunista
ovládnuté	ovládnutý	k2eAgNnSc1d1	ovládnuté
Fierlingerovo	Fierlingerův	k2eAgNnSc1d1	Fierlingerův
křídlo	křídlo	k1gNnSc1	křídlo
bylo	být	k5eAaImAgNnS	být
dočasně	dočasně	k6eAd1	dočasně
poraženo	poražen	k2eAgNnSc1d1	poraženo
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
obnovena	obnoven	k2eAgFnSc1d1	obnovena
být	být	k5eAaImF	být
nemohla	moct	k5eNaImAgFnS	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prokomunistické	prokomunistický	k2eAgNnSc1d1	prokomunistické
křídlo	křídlo	k1gNnSc1	křídlo
a	a	k8xC	a
prokomunistická	prokomunistický	k2eAgFnSc1d1	prokomunistická
infiltrace	infiltrace	k1gFnSc1	infiltrace
existovaly	existovat	k5eAaImAgInP	existovat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
českých	český	k2eAgFnPc6d1	Česká
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
u	u	k7c2	u
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
únorového	únorový	k2eAgInSc2d1	únorový
převratu	převrat	k1gInSc2	převrat
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
nepřidala	přidat	k5eNaPmAgFnS	přidat
k	k	k7c3	k
hromadné	hromadný	k2eAgFnSc3d1	hromadná
demisi	demise	k1gFnSc3	demise
ministrů	ministr	k1gMnPc2	ministr
za	za	k7c4	za
Národně	národně	k6eAd1	národně
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
Lidovou	lidový	k2eAgFnSc4d1	lidová
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Masarykem	Masaryk	k1gMnSc7	Masaryk
zmařila	zmařit	k5eAaPmAgFnS	zmařit
jejich	jejich	k3xOp3gFnSc4	jejich
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
vynucený	vynucený	k2eAgInSc4d1	vynucený
pád	pád	k1gInSc4	pád
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Gottwaldovi	Gottwald	k1gMnSc3	Gottwald
doplnit	doplnit	k5eAaPmF	doplnit
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
rezignujících	rezignující	k2eAgMnPc2d1	rezignující
ministrů	ministr	k1gMnPc2	ministr
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
KSČ	KSČ	kA	KSČ
(	(	kIx(	(
<g/>
nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
ministři	ministr	k1gMnPc1	ministr
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
za	za	k7c4	za
sociální	sociální	k2eAgFnSc4d1	sociální
demokracii	demokracie	k1gFnSc4	demokracie
posléze	posléze	k6eAd1	posléze
nerespektovali	respektovat	k5eNaImAgMnP	respektovat
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
rezignovali	rezignovat	k5eAaBmAgMnP	rezignovat
<g/>
,	,	kIx,	,
problematická	problematický	k2eAgFnSc1d1	problematická
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
i	i	k9	i
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
komunikace	komunikace	k1gFnSc1	komunikace
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
stran	strana	k1gFnPc2	strana
k	k	k7c3	k
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
odešli	odejít	k5eAaPmAgMnP	odejít
někteří	některý	k3yIgMnPc1	některý
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
představitelé	představitel	k1gMnPc1	představitel
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1948	[number]	k4	1948
ustavili	ustavit	k5eAaPmAgMnP	ustavit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
ústřední	ústřední	k2eAgInSc1d1	ústřední
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
protiprávně	protiprávně	k6eAd1	protiprávně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
činnost	činnost	k1gFnSc1	činnost
strany	strana	k1gFnSc2	strana
nuceným	nucený	k2eAgNnSc7d1	nucené
sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
stanovami	stanova	k1gFnPc7	stanova
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Slučovací	slučovací	k2eAgFnSc4d1	slučovací
přihlášku	přihláška	k1gFnSc4	přihláška
následně	následně	k6eAd1	následně
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
členů	člen	k1gInPc2	člen
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
likvidaci	likvidace	k1gFnSc6	likvidace
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
tak	tak	k6eAd1	tak
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
;	;	kIx,	;
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
dočasně	dočasně	k6eAd1	dočasně
Blažej	Blažej	k1gMnSc1	Blažej
Vilím	Vilím	k1gMnSc1	Vilím
<g/>
,	,	kIx,	,
od	od	k7c2	od
září	září	k1gNnSc2	září
1948	[number]	k4	1948
Václav	Václav	k1gMnSc1	Václav
Majer	Majer	k1gMnSc1	Majer
<g/>
.	.	kIx.	.
</s>
<s>
Generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
Blažej	Blažej	k1gMnSc1	Blažej
Vilím	Vilím	k1gMnSc1	Vilím
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
Václav	Václav	k1gMnSc1	Václav
Holub	Holub	k1gMnSc1	Holub
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
reprezentantům	reprezentant	k1gMnPc3	reprezentant
patřili	patřit	k5eAaImAgMnP	patřit
Vilém	Vilém	k1gMnSc1	Vilém
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
Mirko	Mirko	k1gMnSc1	Mirko
Sedlák	Sedlák	k1gMnSc1	Sedlák
a	a	k8xC	a
Arno	Arno	k1gMnSc1	Arno
Hais	Haisa	k1gFnPc2	Haisa
<g/>
,	,	kIx,	,
z	z	k7c2	z
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
pak	pak	k6eAd1	pak
Radomír	Radomír	k1gMnSc1	Radomír
Luža	Luža	k1gMnSc1	Luža
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Horák	Horák	k1gMnSc1	Horák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
ústředního	ústřední	k2eAgNnSc2d1	ústřední
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
v	v	k7c6	v
letech	let	k1gInPc6	let
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
vycházel	vycházet	k5eAaImAgMnS	vycházet
tiskový	tiskový	k2eAgMnSc1d1	tiskový
orgán	orgán	k1gMnSc1	orgán
–	–	k?	–
časopis	časopis	k1gInSc1	časopis
Demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
socialismus	socialismus	k1gInSc1	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
formovaly	formovat	k5eAaImAgFnP	formovat
zemské	zemský	k2eAgFnPc1d1	zemská
organizace	organizace	k1gFnPc1	organizace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
zemskými	zemský	k2eAgMnPc7d1	zemský
důvěrníky	důvěrník	k1gMnPc7	důvěrník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
centra	centrum	k1gNnPc1	centrum
stranické	stranický	k2eAgFnSc2d1	stranická
činnosti	činnost	k1gFnSc2	činnost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
SRN	SRN	kA	SRN
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
SPÖ	SPÖ	kA	SPÖ
působila	působit	k5eAaImAgFnS	působit
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
bývalí	bývalý	k2eAgMnPc1d1	bývalý
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
zelených	zelený	k2eAgFnPc2d1	zelená
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyjadřovaly	vyjadřovat	k5eAaImAgFnP	vyjadřovat
odpor	odpor	k1gInSc4	odpor
jak	jak	k6eAd1	jak
k	k	k7c3	k
západnímu	západní	k2eAgInSc3d1	západní
kapitalismu	kapitalismus	k1gInSc3	kapitalismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
sovětskému	sovětský	k2eAgInSc3d1	sovětský
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaImNgInS	učinit
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
samostatné	samostatný	k2eAgFnSc2d1	samostatná
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tyto	tento	k3xDgInPc4	tento
plány	plán	k1gInPc4	plán
zhatila	zhatit	k5eAaPmAgFnS	zhatit
invaze	invaze	k1gFnSc1	invaze
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samostatnou	samostatný	k2eAgFnSc4d1	samostatná
izolovanou	izolovaný	k2eAgFnSc4d1	izolovaná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
skupinu	skupina	k1gFnSc4	skupina
tvořili	tvořit	k5eAaImAgMnP	tvořit
členové	člen	k1gMnPc1	člen
tzv.	tzv.	kA	tzv.
Zinnerova	Zinnerův	k2eAgInSc2d1	Zinnerův
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
přesvědčení	přesvědčený	k2eAgMnPc1d1	přesvědčený
němečtí	německý	k2eAgMnPc1d1	německý
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
loajální	loajální	k2eAgFnSc2d1	loajální
Československu	Československo	k1gNnSc3	Československo
a	a	k8xC	a
odkazu	odkaz	k1gInSc3	odkaz
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Czecha	Czech	k1gMnSc2	Czech
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
vymezili	vymezit	k5eAaPmAgMnP	vymezit
proti	proti	k7c3	proti
křídlu	křídlo	k1gNnSc3	křídlo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
německé	německý	k2eAgFnSc2d1	německá
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
Wenzela	Wenzela	k1gMnSc2	Wenzela
Jaksche	Jaksch	k1gMnSc2	Jaksch
a	a	k8xC	a
Seliger-Gemeinde	Seliger-Gemeind	k1gInSc5	Seliger-Gemeind
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	se	k3xPyFc4	se
sudetoněmeckým	sudetoněmecký	k2eAgInSc7d1	sudetoněmecký
landmanšaftem	landmanšaft	k1gInSc7	landmanšaft
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Strana	strana	k1gFnSc1	strana
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
obnovení	obnovení	k1gNnSc4	obnovení
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
plánováno	plánovat	k5eAaImNgNnS	plánovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
se	se	k3xPyFc4	se
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
9	[number]	k4	9
<g/>
.	.	kIx.	.
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Slavomír	Slavomír	k1gMnSc1	Slavomír
Klaban	Klaban	k1gMnSc1	Klaban
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ke	k	k7c3	k
členům	člen	k1gMnPc3	člen
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
patřil	patřit	k5eAaImAgInS	patřit
již	již	k6eAd1	již
před	před	k7c7	před
únorem	únor	k1gInSc7	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Obnovovací	obnovovací	k2eAgFnSc1d1	obnovovací
XXIV	XXIV	kA	XXIV
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
24	[number]	k4	24
<g/>
.	.	kIx.	.
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
Břevnově	břevnově	k6eAd1	břevnově
a	a	k8xC	a
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
předválečné	předválečný	k2eAgFnSc2d1	předválečná
ČSDSD	ČSDSD	kA	ČSDSD
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Jiří	Jiří	k1gMnSc1	Jiří
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
aktérů	aktér	k1gMnPc2	aktér
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
Valtr	Valtr	k1gMnSc1	Valtr
Komárek	Komárek	k1gMnSc1	Komárek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
s	s	k7c7	s
Občanským	občanský	k2eAgNnSc7d1	občanské
fórem	fórum	k1gNnSc7	fórum
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
kandidovat	kandidovat	k5eAaImF	kandidovat
i	i	k9	i
za	za	k7c4	za
OF	OF	kA	OF
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
strana	strana	k1gFnSc1	strana
moc	moc	k6eAd1	moc
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
opět	opět	k6eAd1	opět
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozpory	rozpor	k1gInPc7	rozpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
významně	významně	k6eAd1	významně
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
její	její	k3xOp3gInSc4	její
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
utvořil	utvořit	k5eAaPmAgInS	utvořit
Klub	klub	k1gInSc1	klub
poslanců	poslanec	k1gMnPc2	poslanec
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zasedaly	zasedat	k5eAaImAgFnP	zasedat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
získala	získat	k5eAaPmAgFnS	získat
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
16	[number]	k4	16
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
změnila	změnit	k5eAaPmAgFnS	změnit
strana	strana	k1gFnSc1	strana
název	název	k1gInSc4	název
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
stranu	strana	k1gFnSc4	strana
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1996	[number]	k4	1996
se	se	k3xPyFc4	se
ČSSD	ČSSD	kA	ČSSD
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
a	a	k8xC	a
tolerovala	tolerovat	k5eAaImAgFnS	tolerovat
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
vládu	vláda	k1gFnSc4	vláda
koalice	koalice	k1gFnSc2	koalice
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
ODA	ODA	kA	ODA
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
Klausova	Klausův	k2eAgFnSc1d1	Klausova
vláda	vláda	k1gFnSc1	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1998	[number]	k4	1998
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
a	a	k8xC	a
sestavila	sestavit	k5eAaPmAgFnS	sestavit
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
Zemanovým	Zemanův	k2eAgNnSc7d1	Zemanovo
předsednictvím	předsednictví	k1gNnSc7	předsednictví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
působení	působení	k1gNnSc4	působení
umožnila	umožnit	k5eAaPmAgFnS	umožnit
tzv.	tzv.	kA	tzv.
opoziční	opoziční	k2eAgFnSc1d1	opoziční
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
mezi	mezi	k7c7	mezi
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
ODS	ODS	kA	ODS
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
sestavila	sestavit	k5eAaPmAgFnS	sestavit
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
a	a	k8xC	a
US-DEU	US-DEU	k1gFnSc7	US-DEU
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koalice	koalice	k1gFnSc1	koalice
ovšem	ovšem	k9	ovšem
disponovala	disponovat	k5eAaBmAgFnS	disponovat
pouze	pouze	k6eAd1	pouze
těsnou	těsný	k2eAgFnSc7d1	těsná
většinou	většina	k1gFnSc7	většina
(	(	kIx(	(
<g/>
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2001	[number]	k4	2001
na	na	k7c4	na
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
provázela	provázet	k5eAaImAgFnS	provázet
nejednota	nejednota	k1gFnSc1	nejednota
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
i	i	k8xC	i
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnSc1d1	opoziční
ODS	ODS	kA	ODS
jí	on	k3xPp3gFnSc7	on
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
levicovou	levicový	k2eAgFnSc4d1	levicová
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
další	další	k2eAgFnSc1d1	další
opoziční	opoziční	k2eAgFnSc1d1	opoziční
strana	strana	k1gFnSc1	strana
KSČM	KSČM	kA	KSČM
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČSSD	ČSSD	kA	ČSSD
na	na	k7c4	na
levicové	levicový	k2eAgFnPc4d1	levicová
hodnoty	hodnota	k1gFnPc4	hodnota
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
v	v	k7c6	v
eurovolbách	eurovolba	k1gFnPc6	eurovolba
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
premiér	premiér	k1gMnSc1	premiér
Stanislav	Stanislav	k1gMnSc1	Stanislav
Gross	Gross	k1gMnSc1	Gross
vydržel	vydržet	k5eAaPmAgMnS	vydržet
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
a	a	k8xC	a
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
kvůli	kvůli	k7c3	kvůli
bytové	bytový	k2eAgFnSc3d1	bytová
aféře	aféra	k1gFnSc3	aféra
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stranu	strana	k1gFnSc4	strana
sjednotil	sjednotit	k5eAaPmAgMnS	sjednotit
a	a	k8xC	a
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
její	její	k3xOp3gNnSc4	její
postavení	postavení	k1gNnSc4	postavení
(	(	kIx(	(
<g/>
nárůst	nárůst	k1gInSc1	nárůst
preferencí	preference	k1gFnPc2	preference
–	–	k?	–
získání	získání	k1gNnSc4	získání
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
jak	jak	k8xC	jak
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
tak	tak	k9	tak
nerozhodnutých	rozhodnutý	k2eNgMnPc2d1	nerozhodnutý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
však	však	k9	však
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
aférami	aféra	k1gFnPc7	aféra
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
samých	samý	k3xTgMnPc2	samý
pak	pak	k6eAd1	pak
strana	strana	k1gFnSc1	strana
skončila	skončit	k5eAaPmAgFnS	skončit
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
historicky	historicky	k6eAd1	historicky
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
volební	volební	k2eAgInSc4d1	volební
výsledek	výsledek	k1gInSc4	výsledek
druhá	druhý	k4xOgFnSc1	druhý
za	za	k7c4	za
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
strana	strana	k1gFnSc1	strana
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
krajské	krajský	k2eAgFnPc4d1	krajská
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
všech	všecek	k3xTgMnPc2	všecek
13	[number]	k4	13
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
slavila	slavit	k5eAaImAgFnS	slavit
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
získala	získat	k5eAaPmAgFnS	získat
23	[number]	k4	23
nových	nový	k2eAgMnPc2d1	nový
senátorů	senátor	k1gMnPc2	senátor
z	z	k7c2	z
27	[number]	k4	27
možných	možný	k2eAgInPc2d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ČSSD	ČSSD	kA	ČSSD
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
získaných	získaný	k2eAgInPc2d1	získaný
mandátů	mandát	k1gInPc2	mandát
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
levicové	levicový	k2eAgFnSc2d1	levicová
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
proto	proto	k8xC	proto
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
stal	stát	k5eAaPmAgMnS	stát
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
sněmovními	sněmovní	k2eAgFnPc7d1	sněmovní
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
===	===	k?	===
</s>
</p>
<p>
<s>
ČSSD	ČSSD	kA	ČSSD
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
získala	získat	k5eAaPmAgFnS	získat
22,1	[number]	k4	22,1
%	%	kIx~	%
a	a	k8xC	a
56	[number]	k4	56
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
disponovala	disponovat	k5eAaBmAgFnS	disponovat
54	[number]	k4	54
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
Prosadíme	prosadit	k5eAaPmIp1nP	prosadit
dobře	dobře	k6eAd1	dobře
fungující	fungující	k2eAgInSc4d1	fungující
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
celostátním	celostátní	k2eAgMnSc7d1	celostátní
lídrem	lídr	k1gMnSc7	lídr
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
premiérského	premiérský	k2eAgMnSc2d1	premiérský
kandidáta	kandidát	k1gMnSc2	kandidát
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
nejprve	nejprve	k6eAd1	nejprve
jednomyslně	jednomyslně	k6eAd1	jednomyslně
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
volbě	volba	k1gFnSc6	volba
57	[number]	k4	57
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
i	i	k8xC	i
ÚVV	ÚVV	kA	ÚVV
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
cíle	cíl	k1gInPc4	cíl
ČSSD	ČSSD	kA	ČSSD
patřilo	patřit	k5eAaImAgNnS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
snížení	snížení	k1gNnSc4	snížení
DPH	DPH	kA	DPH
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgNnSc4d1	progresivní
zdanění	zdanění	k1gNnSc4	zdanění
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
příjmy	příjem	k1gInPc7	příjem
či	či	k8xC	či
opětovné	opětovný	k2eAgNnSc1d1	opětovné
otevření	otevření	k1gNnSc1	otevření
otázky	otázka	k1gFnSc2	otázka
církevních	církevní	k2eAgFnPc2d1	církevní
restitucí	restituce	k1gFnPc2	restituce
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
byli	být	k5eAaImAgMnP	být
podle	podle	k7c2	podle
předvolebních	předvolební	k2eAgInPc2d1	předvolební
průzkumů	průzkum	k1gInPc2	průzkum
jednoznačným	jednoznačný	k2eAgMnSc7d1	jednoznačný
favoritem	favorit	k1gMnSc7	favorit
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
projevoval	projevovat	k5eAaImAgInS	projevovat
vnitrostranický	vnitrostranický	k2eAgInSc1d1	vnitrostranický
rozpor	rozpor	k1gInSc1	rozpor
mezi	mezi	k7c7	mezi
stoupenci	stoupenec	k1gMnPc7	stoupenec
(	(	kIx(	(
<g/>
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
Škromach	Škromach	k1gMnSc1	Škromach
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
názorovými	názorový	k2eAgMnPc7d1	názorový
oponenty	oponent	k1gMnPc7	oponent
(	(	kIx(	(
<g/>
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
měla	mít	k5eAaImAgFnS	mít
též	též	k9	též
problém	problém	k1gInSc4	problém
s	s	k7c7	s
žalobami	žaloba	k1gFnPc7	žaloba
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
podali	podat	k5eAaPmAgMnP	podat
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neuspěli	uspět	k5eNaPmAgMnP	uspět
ve	v	k7c6	v
vnitrostranických	vnitrostranický	k2eAgFnPc6d1	vnitrostranická
primárkách	primárky	k1gFnPc6	primárky
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
projevilo	projevit	k5eAaPmAgNnS	projevit
během	během	k7c2	během
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
lánské	lánský	k2eAgFnSc2d1	lánská
schůzky	schůzka	k1gFnSc2	schůzka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
stoupenci	stoupenec	k1gMnPc7	stoupenec
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
prezidentově	prezidentův	k2eAgInSc6d1	prezidentův
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
a	a	k8xC	a
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
domluvit	domluvit	k5eAaPmF	domluvit
na	na	k7c6	na
dosazení	dosazení	k1gNnSc6	dosazení
Michala	Michal	k1gMnSc4	Michal
Haška	Hašek	k1gMnSc4	Hašek
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
odstavení	odstavení	k1gNnSc2	odstavení
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
vzpoura	vzpoura	k1gFnSc1	vzpoura
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
severočeském	severočeský	k2eAgInSc6d1	severočeský
Duchcově	Duchcův	k2eAgInSc6d1	Duchcův
zformována	zformován	k2eAgFnSc1d1	zformována
radniční	radniční	k2eAgFnSc1d1	radniční
koalice	koalice	k1gFnSc1	koalice
mezi	mezi	k7c7	mezi
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
DSSS	DSSS	kA	DSSS
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nástupnickou	nástupnický	k2eAgFnSc7d1	nástupnická
organizací	organizace	k1gFnSc7	organizace
neonacistické	neonacistický	k2eAgFnSc2d1	neonacistická
Dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Sobotkou	Sobotka	k1gMnSc7	Sobotka
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
situaci	situace	k1gFnSc4	situace
ostře	ostro	k6eAd1	ostro
odsoudilo	odsoudit	k5eAaPmAgNnS	odsoudit
a	a	k8xC	a
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
zrušení	zrušení	k1gNnSc4	zrušení
stranické	stranický	k2eAgFnSc2d1	stranická
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
Duchcově	Duchcův	k2eAgNnSc6d1	Duchcův
<g/>
.	.	kIx.	.
</s>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
vedení	vedení	k1gNnSc1	vedení
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
podřídit	podřídit	k5eAaPmF	podřídit
pokynům	pokyn	k1gInPc3	pokyn
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
duchcovskou	duchcovský	k2eAgFnSc4d1	Duchcovská
buňku	buňka	k1gFnSc4	buňka
nezrušilo	zrušit	k5eNaPmAgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
2017	[number]	k4	2017
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
snižující	snižující	k2eAgFnSc1d1	snižující
volební	volební	k2eAgFnSc1d1	volební
preference	preference	k1gFnSc1	preference
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
:	:	kIx,	:
premiérem	premiér	k1gMnSc7	premiér
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
dočasným	dočasný	k2eAgMnSc7d1	dočasný
předsedou	předseda	k1gMnSc7	předseda
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
statutární	statutární	k2eAgMnSc1d1	statutární
předseda	předseda	k1gMnSc1	předseda
Milan	Milan	k1gMnSc1	Milan
Chovanec	Chovanec	k1gMnSc1	Chovanec
a	a	k8xC	a
volebním	volební	k2eAgMnSc7d1	volební
lídrem	lídr	k1gMnSc7	lídr
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Lubomír	Lubomír	k1gMnSc1	Lubomír
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
však	však	k9	však
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
ČSSD	ČSSD	kA	ČSSD
získala	získat	k5eAaPmAgFnS	získat
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
výsledek	výsledek	k1gInSc4	výsledek
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
novodobých	novodobý	k2eAgFnPc6d1	novodobá
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgNnPc2d1	pouhé
7,27	[number]	k4	7,27
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sjezd	sjezd	k1gInSc4	sjezd
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
===	===	k?	===
</s>
</p>
<p>
<s>
Mimořádný	mimořádný	k2eAgInSc1d1	mimořádný
40	[number]	k4	40
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
ČSSD	ČSSD	kA	ČSSD
začal	začít	k5eAaPmAgInS	začít
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
sjezdu	sjezd	k1gInSc2	sjezd
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
nejhoršího	zlý	k2eAgInSc2d3	Nejhorší
výsledku	výsledek	k1gInSc2	výsledek
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
trvající	trvající	k2eAgNnSc4d1	trvající
období	období	k1gNnSc4	období
bez	bez	k7c2	bez
řádně	řádně	k6eAd1	řádně
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
důležitost	důležitost	k1gFnSc4	důležitost
podtrhují	podtrhovat	k5eAaImIp3nP	podtrhovat
také	také	k9	také
slova	slovo	k1gNnSc2	slovo
některých	některý	k3yIgMnPc2	některý
straníků	straník	k1gMnPc2	straník
o	o	k7c6	o
boji	boj	k1gInSc6	boj
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bojujeme	bojovat	k5eAaImIp1nP	bojovat
o	o	k7c6	o
přežití	přežití	k1gNnSc6	přežití
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
na	na	k7c6	na
ČSSD	ČSSD	kA	ČSSD
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
,	,	kIx,	,
zapomněli	zapomnět	k5eAaImAgMnP	zapomnět
na	na	k7c4	na
osobní	osobní	k2eAgFnPc4d1	osobní
neshody	neshoda	k1gFnPc4	neshoda
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
táhnout	táhnout	k5eAaImF	táhnout
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
provaz	provaz	k1gInSc4	provaz
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
například	například	k6eAd1	například
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ČSSD	ČSSD	kA	ČSSD
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
rozcestí	rozcestí	k1gNnSc6	rozcestí
<g/>
,	,	kIx,	,
dokládá	dokládat	k5eAaImIp3nS	dokládat
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
kandidovalo	kandidovat	k5eAaImAgNnS	kandidovat
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc1	sedm
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
zkušených	zkušený	k2eAgMnPc2d1	zkušený
politiků	politik	k1gMnPc2	politik
po	po	k7c4	po
poměrně	poměrně	k6eAd1	poměrně
neznámé	známý	k2eNgFnPc4d1	neznámá
tváře	tvář	k1gFnPc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Jana	Jan	k1gMnSc2	Jan
Hamáčka	Hamáček	k1gMnSc2	Hamáček
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgInS	mít
pět	pět	k4xCc4	pět
nominací	nominace	k1gFnPc2	nominace
od	od	k7c2	od
krajských	krajský	k2eAgFnPc2d1	krajská
organizací	organizace	k1gFnPc2	organizace
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
Milan	Milan	k1gMnSc1	Milan
Chovanec	Chovanec	k1gMnSc1	Chovanec
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc1	tři
nominace	nominace	k1gFnPc1	nominace
od	od	k7c2	od
krajských	krajský	k2eAgFnPc2d1	krajská
organizací	organizace	k1gFnPc2	organizace
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Zimola	Zimola	k1gFnSc1	Zimola
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Staněk	Staněk	k1gMnSc1	Staněk
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Romana	Romana	k1gFnSc1	Romana
Žatecká	žatecký	k2eAgFnSc1d1	Žatecká
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jukl	juknout	k5eAaPmAgMnS	juknout
(	(	kIx(	(
<g/>
žádná	žádný	k3yNgFnSc1	žádný
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Krejčík	Krejčík	k1gMnSc1	Krejčík
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
18	[number]	k4	18
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
byl	být	k5eAaImAgMnS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
nad	nad	k7c7	nad
exhejtmanem	exhejtman	k1gMnSc7	exhejtman
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
Jiřím	Jiří	k1gMnSc7	Jiří
Zimolou	Zimola	k1gMnSc7	Zimola
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
později	pozdě	k6eAd2	pozdě
zvolil	zvolit	k5eAaPmAgInS	zvolit
Jiřího	Jiří	k1gMnSc4	Jiří
Zimolu	Zimol	k1gInSc2	Zimol
statutárním	statutární	k2eAgMnSc7d1	statutární
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
20	[number]	k4	20
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
byl	být	k5eAaImAgInS	být
sjezd	sjezd	k1gInSc1	sjezd
přerušen	přerušit	k5eAaPmNgInS	přerušit
až	až	k6eAd1	až
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
řadovými	řadový	k2eAgMnPc7d1	řadový
místopředsedy	místopředseda	k1gMnPc7	místopředseda
strany	strana	k1gFnSc2	strana
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Jaroslav	Jaroslava	k1gFnPc2	Jaroslava
Foldyna	Foldyno	k1gNnSc2	Foldyno
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Netolický	netolický	k2eAgMnSc1d1	netolický
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Fialová	Fialová	k1gFnSc1	Fialová
<g/>
;	;	kIx,	;
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
následně	následně	k6eAd1	následně
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
<g/>
.	.	kIx.	.
</s>
<s>
Delegáti	delegát	k1gMnPc1	delegát
sjezdu	sjezd	k1gInSc2	sjezd
zároveň	zároveň	k6eAd1	zároveň
posvětili	posvětit	k5eAaPmAgMnP	posvětit
konec	konec	k1gInSc4	konec
jednání	jednání	k1gNnSc2	jednání
ČSSD	ČSSD	kA	ČSSD
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sjezd	sjezd	k1gInSc4	sjezd
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
41	[number]	k4	41
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
433	[number]	k4	433
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
bylo	být	k5eAaImAgNnS	být
57	[number]	k4	57
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
11	[number]	k4	11
hlasů	hlas	k1gInPc2	hlas
bylo	být	k5eAaImAgNnS	být
neplatných	platný	k2eNgFnPc2d1	neplatná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Statutárním	statutární	k2eAgMnSc7d1	statutární
místopředsedou	místopředseda	k1gMnSc7	místopředseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
<g/>
,	,	kIx,	,
také	také	k9	také
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
–	–	k?	–
získal	získat	k5eAaPmAgMnS	získat
410	[number]	k4	410
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
bylo	být	k5eAaImAgNnS	být
80	[number]	k4	80
delegátů	delegát	k1gMnPc2	delegát
a	a	k8xC	a
5	[number]	k4	5
hlasů	hlas	k1gInPc2	hlas
bylo	být	k5eAaImAgNnS	být
neplatných	platný	k2eNgNnPc2d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
Řadovými	řadový	k2eAgMnPc7d1	řadový
místopředsedy	místopředseda	k1gMnPc7	místopředseda
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
472	[number]	k4	472
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Michal	Michal	k1gMnSc1	Michal
Šmarda	Šmarda	k1gMnSc1	Šmarda
(	(	kIx(	(
<g/>
392	[number]	k4	392
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Maláčová	Maláčová	k1gFnSc1	Maláčová
(	(	kIx(	(
<g/>
390	[number]	k4	390
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
313	[number]	k4	313
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Petříček	Petříček	k1gMnSc1	Petříček
(	(	kIx(	(
<g/>
256	[number]	k4	256
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgMnSc1	pátý
řadový	řadový	k2eAgMnSc1d1	řadový
místopředseda	místopředseda	k1gMnSc1	místopředseda
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ani	ani	k9	ani
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
a	a	k8xC	a
slučování	slučování	k1gNnSc1	slučování
ČSSD	ČSSD	kA	ČSSD
==	==	k?	==
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
–	–	k?	–
Odchod	odchod	k1gInSc1	odchod
skupiny	skupina	k1gFnSc2	skupina
členů	člen	k1gMnPc2	člen
okolo	okolo	k7c2	okolo
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Battěka	Battěk	k1gMnSc2	Battěk
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgNnSc4d1	demokratické
hnutí	hnutí	k1gNnSc4	hnutí
–	–	k?	–
Vznik	vznik	k1gInSc1	vznik
v	v	k7c6	v
září	září	k1gNnSc6	září
1999	[number]	k4	1999
odchodem	odchod	k1gInSc7	odchod
skupiny	skupina	k1gFnSc2	skupina
více	hodně	k6eAd2	hodně
vlastenecky	vlastenecky	k6eAd1	vlastenecky
založených	založený	k2eAgMnPc2d1	založený
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
České	český	k2eAgNnSc1d1	české
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
jednotu	jednota	k1gFnSc4	jednota
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
přátel	přítel	k1gMnPc2	přítel
piva	pivo	k1gNnSc2	pivo
–	–	k?	–
Původně	původně	k6eAd1	původně
strana	strana	k1gFnSc1	strana
recese	recese	k1gFnSc2	recese
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
sloučila	sloučit	k5eAaPmAgFnS	sloučit
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	s	k7c7	s
sloučením	sloučení	k1gNnSc7	sloučení
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
aktivitách	aktivita	k1gFnPc6	aktivita
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
dále	daleko	k6eAd2	daleko
jako	jako	k8xC	jako
Sdružení	sdružení	k1gNnSc1	sdružení
přátel	přítel	k1gMnPc2	přítel
piva	pivo	k1gNnSc2	pivo
</s>
</p>
<p>
<s>
==	==	k?	==
Frakce	frakce	k1gFnPc1	frakce
v	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
existují	existovat	k5eAaImIp3nP	existovat
některé	některý	k3yIgFnPc4	některý
platformy	platforma	k1gFnPc4	platforma
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
politickou	politický	k2eAgFnSc4d1	politická
agendu	agenda	k1gFnSc4	agenda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Křesťansko-sociální	křesťanskoociální	k2eAgFnSc1d1	křesťansko-sociální
platforma	platforma	k1gFnSc1	platforma
ČSSD	ČSSD	kA	ČSSD
</s>
</p>
<p>
<s>
Zvonečník	zvonečník	k1gInSc1	zvonečník
–	–	k?	–
ekologická	ekologický	k2eAgFnSc1d1	ekologická
platforma	platforma	k1gFnSc1	platforma
ČSSDDále	ČSSDDála	k1gFnSc3	ČSSDDála
však	však	k9	však
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
vyhlášeny	vyhlásit	k5eAaPmNgInP	vyhlásit
i	i	k9	i
uskupení	uskupení	k1gNnSc1	uskupení
více	hodně	k6eAd2	hodně
podobné	podobný	k2eAgFnPc4d1	podobná
běžným	běžný	k2eAgFnPc3d1	běžná
názorovým	názorový	k2eAgFnPc3d1	názorová
frakcím	frakce	k1gFnPc3	frakce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc4	žádný
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
příliš	příliš	k6eAd1	příliš
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
frakce	frakce	k1gFnSc1	frakce
ČSSD	ČSSD	kA	ČSSD
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
levicová	levicový	k2eAgFnSc1d1	levicová
platforma	platforma	k1gFnSc1	platforma
ČSSD	ČSSD	kA	ČSSD
</s>
</p>
<p>
<s>
Levicová	levicový	k2eAgFnSc1d1	levicová
platforma	platforma	k1gFnSc1	platforma
Doleva	doleva	k6eAd1	doleva
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Zachraňme	zachránit	k5eAaPmRp1nP	zachránit
ČSSD	ČSSD	kA	ČSSD
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Kromě	kromě	k7c2	kromě
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
frakcí	frakce	k1gFnPc2	frakce
a	a	k8xC	a
platforem	platforma	k1gFnPc2	platforma
lze	lze	k6eAd1	lze
v	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
rozeznat	rozeznat	k5eAaPmF	rozeznat
minimálně	minimálně	k6eAd1	minimálně
dvě	dva	k4xCgNnPc4	dva
názorová	názorový	k2eAgNnPc4d1	názorové
křídla	křídlo	k1gNnPc4	křídlo
<g/>
:	:	kIx,	:
liberální	liberální	k2eAgMnPc1d1	liberální
a	a	k8xC	a
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Liberální	liberální	k2eAgInSc1d1	liberální
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mj.	mj.	kA	mj.
vyšší	vysoký	k2eAgFnSc7d2	vyšší
citlivostí	citlivost	k1gFnSc7	citlivost
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
občanských	občanský	k2eAgNnPc2d1	občanské
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
práv	právo	k1gNnPc2	právo
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ochraně	ochrana	k1gFnSc6	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
problematice	problematika	k1gFnSc3	problematika
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
veřejné	veřejný	k2eAgFnSc2d1	veřejná
kontroly	kontrola	k1gFnSc2	kontrola
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýraznější	výrazný	k2eAgMnPc4d3	nejvýraznější
představitele	představitel	k1gMnPc4	představitel
liberálního	liberální	k2eAgInSc2d1	liberální
proudu	proud	k1gInSc2	proud
patří	patřit	k5eAaImIp3nS	patřit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
či	či	k8xC	či
Lubomír	Lubomír	k1gMnSc1	Lubomír
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
proud	proud	k1gInSc1	proud
v	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
jednostranné	jednostranný	k2eAgFnSc3d1	jednostranná
podpoře	podpora	k1gFnSc3	podpora
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
a	a	k8xC	a
sociálních	sociální	k2eAgNnPc2d1	sociální
práv	právo	k1gNnPc2	právo
před	před	k7c7	před
ostatními	ostatní	k2eAgNnPc7d1	ostatní
občanskými	občanský	k2eAgNnPc7d1	občanské
a	a	k8xC	a
politickými	politický	k2eAgNnPc7d1	politické
právy	právo	k1gNnPc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
nejvýraznější	výrazný	k2eAgMnPc4d3	nejvýraznější
představitele	představitel	k1gMnPc4	představitel
lze	lze	k6eAd1	lze
započítat	započítat	k5eAaPmF	započítat
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Škromacha	Škromach	k1gMnSc2	Škromach
<g/>
,	,	kIx,	,
Michala	Michal	k1gMnSc2	Michal
Haška	Hašek	k1gMnSc2	Hašek
nebo	nebo	k8xC	nebo
Jiřího	Jiří	k1gMnSc2	Jiří
Zimolu	Zimol	k1gInSc2	Zimol
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2013	[number]	k4	2013
liberální	liberální	k2eAgNnSc1d1	liberální
křídlo	křídlo	k1gNnSc1	křídlo
podporovalo	podporovat	k5eAaImAgNnS	podporovat
Jiřího	Jiří	k1gMnSc4	Jiří
Dienstbiera	Dienstbier	k1gMnSc4	Dienstbier
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
konzervativní	konzervativní	k2eAgNnSc1d1	konzervativní
křídlo	křídlo	k1gNnSc1	křídlo
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
včetně	včetně	k7c2	včetně
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnSc2	kolo
volby	volba	k1gFnSc2	volba
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
proti	proti	k7c3	proti
oficiálnímu	oficiální	k2eAgMnSc3d1	oficiální
kandidátovi	kandidát	k1gMnSc3	kandidát
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
opakovala	opakovat	k5eAaImAgFnS	opakovat
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
ČR	ČR	kA	ČR
Milan	Milan	k1gMnSc1	Milan
Štěch	Štěch	k1gMnSc1	Štěch
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
sociálnědemokratických	sociálnědemokratický	k2eAgMnPc2d1	sociálnědemokratický
hejtmanů	hejtman	k1gMnPc2	hejtman
či	či	k8xC	či
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Jiřího	Jiří	k1gMnSc4	Jiří
Drahoše	Drahoš	k1gMnSc4	Drahoš
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
značně	značně	k6eAd1	značně
rozštěpená	rozštěpený	k2eAgFnSc1d1	rozštěpená
ČSSD	ČSSD	kA	ČSSD
nepodpořila	podpořit	k5eNaPmAgFnS	podpořit
žádného	žádný	k3yNgMnSc4	žádný
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Preference	preference	k1gFnSc2	preference
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Horákova	Horákův	k2eAgNnSc2d1	Horákovo
vedení	vedení	k1gNnSc2	vedení
byly	být	k5eAaImAgFnP	být
preference	preference	k1gFnPc1	preference
ČSSD	ČSSD	kA	ČSSD
nevýrazné	výrazný	k2eNgFnPc1d1	nevýrazná
<g/>
,	,	kIx,	,
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
podpora	podpora	k1gFnSc1	podpora
navyšovat	navyšovat	k5eAaImF	navyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pak	pak	k6eAd1	pak
slábla	slábnout	k5eAaImAgFnS	slábnout
během	během	k7c2	během
opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Špidlovy	Špidlův	k2eAgFnSc2d1	Špidlova
vlády	vláda	k1gFnSc2	vláda
však	však	k9	však
sociální	sociální	k2eAgFnSc4d1	sociální
demokracii	demokracie	k1gFnSc4	demokracie
začala	začít	k5eAaPmAgFnS	začít
podpora	podpora	k1gFnSc1	podpora
prudce	prudko	k6eAd1	prudko
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
12	[number]	k4	12
%	%	kIx~	%
a	a	k8xC	a
i	i	k9	i
KSČM	KSČM	kA	KSČM
měla	mít	k5eAaImAgFnS	mít
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
eurovolbách	eurovolba	k1gFnPc6	eurovolba
2004	[number]	k4	2004
se	se	k3xPyFc4	se
podpora	podpora	k1gFnSc1	podpora
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Grossova	Grossův	k2eAgNnSc2d1	Grossovo
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
preference	preference	k1gFnPc1	preference
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
aféra	aféra	k1gFnSc1	aféra
však	však	k9	však
důvěru	důvěra	k1gFnSc4	důvěra
znovu	znovu	k6eAd1	znovu
podlomila	podlomit	k5eAaPmAgFnS	podlomit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Paroubka	Paroubek	k1gMnSc2	Paroubek
se	se	k3xPyFc4	se
preference	preference	k1gFnPc1	preference
zlepšily	zlepšit	k5eAaPmAgFnP	zlepšit
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
měla	mít	k5eAaImAgFnS	mít
ČSSD	ČSSD	kA	ČSSD
až	až	k6eAd1	až
40	[number]	k4	40
%	%	kIx~	%
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
během	během	k7c2	během
pádu	pád	k1gInSc2	pád
Topolánkovy	Topolánkův	k2eAgFnSc2d1	Topolánkova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
jakožto	jakožto	k8xS	jakožto
opoziční	opoziční	k2eAgFnSc1d1	opoziční
strana	strana	k1gFnSc1	strana
znovu	znovu	k6eAd1	znovu
sílila	sílit	k5eAaImAgFnS	sílit
<g/>
,	,	kIx,	,
v	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
ale	ale	k8xC	ale
oproti	oproti	k7c3	oproti
průzkumům	průzkum	k1gInPc3	průzkum
dostala	dostat	k5eAaPmAgFnS	dostat
jen	jen	k9	jen
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
průzkumů	průzkum	k1gInPc2	průzkum
by	by	kYmCp3nS	by
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
pouze	pouze	k6eAd1	pouze
16	[number]	k4	16
procent	procento	k1gNnPc2	procento
a	a	k8xC	a
dle	dle	k7c2	dle
Trendy	trend	k1gInPc7	trend
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
ČT	ČT	kA	ČT
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2017	[number]	k4	2017
připravila	připravit	k5eAaPmAgFnS	připravit
společnost	společnost	k1gFnSc1	společnost
TNS	TNS	kA	TNS
Kantar	Kantar	k1gInSc1	Kantar
dokonce	dokonce	k9	dokonce
pouze	pouze	k6eAd1	pouze
12,5	[number]	k4	12,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
znatelný	znatelný	k2eAgInSc4d1	znatelný
propad	propad	k1gInSc4	propad
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
v	v	k7c6	v
několika	několik	k4yIc6	několik
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
až	až	k6eAd1	až
týdnech	týden	k1gInPc6	týden
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stranu	strana	k1gFnSc4	strana
kauza	kauza	k1gFnSc1	kauza
lithium	lithium	k1gNnSc4	lithium
připravila	připravit	k5eAaPmAgFnS	připravit
o	o	k7c4	o
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
a	a	k8xC	a
z	z	k7c2	z
předpovídaných	předpovídaný	k2eAgNnPc2d1	předpovídané
deseti	deset	k4xCc2	deset
až	až	k9	až
jedenácti	jedenáct	k4xCc2	jedenáct
procent	procento	k1gNnPc2	procento
se	se	k3xPyFc4	se
propadla	propadnout	k5eAaPmAgFnS	propadnout
na	na	k7c4	na
7	[number]	k4	7
procent	procento	k1gNnPc2	procento
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
průzkumu	průzkum	k1gInSc6	průzkum
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
pětiprocentní	pětiprocentní	k2eAgFnSc4d1	pětiprocentní
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
pouhých	pouhý	k2eAgNnPc2d1	pouhé
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
průzkumech	průzkum	k1gInPc6	průzkum
pak	pak	k6eAd1	pak
zesílila	zesílit	k5eAaPmAgFnS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
průzkumy	průzkum	k1gInPc1	průzkum
sociální	sociální	k2eAgFnSc4d1	sociální
demokracii	demokracie	k1gFnSc4	demokracie
věštily	věštit	k5eAaImAgInP	věštit
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
již	již	k6eAd1	již
opět	opět	k6eAd1	opět
12,5	[number]	k4	12,5
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
grémium	grémium	k1gNnSc1	grémium
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
41	[number]	k4	41
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
zvoleno	zvolen	k2eAgNnSc1d1	zvoleno
nejužší	úzký	k2eAgNnSc1d3	nejužší
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
strany	strana	k1gFnSc2	strana
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
Vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
–	–	k?	–
statutární	statutární	k2eAgMnSc1d1	statutární
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Šmarda	Šmarda	k1gMnSc1	Šmarda
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Maláčová	Maláčový	k2eAgFnSc1d1	Maláčová
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ministryně	ministryně	k1gFnSc1	ministryně
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Veselý	Veselý	k1gMnSc1	Veselý
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
Poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Písek	Písek	k1gInSc1	Písek
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Petříček	Petříček	k1gMnSc1	Petříček
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
ČRDalší	ČRDalší	k2eAgMnPc1d1	ČRDalší
členové	člen	k1gMnPc1	člen
grémia	grémium	k1gNnSc2	grémium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Chvojka	Chvojka	k1gMnSc1	Chvojka
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Štěch	Štěch	k1gMnSc1	Štěch
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
senátní	senátní	k2eAgInSc4d1	senátní
obvod	obvod	k1gInSc4	obvod
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Poc	Poc	k1gMnSc1	Poc
–	–	k?	–
vedoucí	vedoucí	k1gMnSc1	vedoucí
delegace	delegace	k1gFnSc2	delegace
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Váňa	Váňa	k1gMnSc1	Váňa
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
komise	komise	k1gFnSc2	komise
ČSSD	ČSSD	kA	ČSSD
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Vícha	Vích	k1gMnSc2	Vích
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
senátorského	senátorský	k2eAgInSc2d1	senátorský
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
senátní	senátní	k2eAgInSc4d1	senátní
obvod	obvod	k1gInSc4	obvod
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Bohumína	Bohumín	k1gInSc2	Bohumín
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Starec	Starec	k?	Starec
–	–	k?	–
ekonomický	ekonomický	k2eAgMnSc1d1	ekonomický
ředitel	ředitel	k1gMnSc1	ředitel
ČSSD	ČSSD	kA	ČSSD
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnSc3d1	významná
osobnosti	osobnost	k1gFnSc3	osobnost
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
==	==	k?	==
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Boleslav	Boleslav	k1gMnSc1	Boleslav
Pecka-Strahovský	Pecka-Strahovský	k2eAgMnSc1d1	Pecka-Strahovský
–	–	k?	–
zakladatel	zakladatel	k1gMnSc1	zakladatel
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hybeš	Hybeš	k1gMnSc1	Hybeš
–	–	k?	–
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Zápotocký-Budečský	Zápotocký-Budečský	k2eAgMnSc1d1	Zápotocký-Budečský
–	–	k?	–
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Schwarz	Schwarz	k1gMnSc1	Schwarz
–	–	k?	–
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Steiner	Steiner	k1gMnSc1	Steiner
</s>
</p>
<p>
<s>
Karla	Karla	k1gFnSc1	Karla
Máchová-Kostelecká	Máchová-Kostelecký	k2eAgFnSc1d1	Máchová-Kostelecký
–	–	k?	–
bojovnice	bojovnice	k1gFnSc1	bojovnice
za	za	k7c4	za
politická	politický	k2eAgNnPc4d1	politické
práva	právo	k1gNnPc4	právo
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
Charlotta	Charlotta	k1gFnSc1	Charlotta
Garrigue-Masaryková	Garrigue-Masarykový	k2eAgFnSc1d1	Garrigue-Masarykový
–	–	k?	–
manželka	manželka	k1gFnSc1	manželka
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
ČSR	ČSR	kA	ČSR
</s>
</p>
<p>
<s>
Bohumír	Bohumír	k1gMnSc1	Bohumír
Šmeral	Šmeral	k1gMnSc1	Šmeral
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tusar	Tusar	k1gMnSc1	Tusar
–	–	k?	–
sociálně	sociálně	k6eAd1	sociálně
demokratický	demokratický	k2eAgMnSc1d1	demokratický
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Hampl	Hampl	k1gMnSc1	Hampl
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
1924	[number]	k4	1924
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fierlinger	Fierlinger	k1gMnSc1	Fierlinger
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Laušman	Laušman	k1gMnSc1	Laušman
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Majer	Majer	k1gMnSc1	Majer
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
exilový	exilový	k2eAgMnSc1d1	exilový
předseda	předseda	k1gMnSc1	předseda
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Horák	Horák	k1gMnSc1	Horák
–	–	k?	–
první	první	k4xOgFnSc4	první
porevoluční	porevoluční	k2eAgFnSc4d1	porevoluční
předseda	předseda	k1gMnSc1	předseda
obnovené	obnovený	k2eAgFnSc2d1	obnovená
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Gross	Gross	k1gMnSc1	Gross
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
–	–	k?	–
statutární	statutární	k2eAgMnSc1d1	statutární
místopředseda	místopředseda	k1gMnSc1	místopředseda
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
–	–	k?	–
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Erazim	Erazim	k1gMnSc1	Erazim
Kohák	Kohák	k?	Kohák
–	–	k?	–
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
orientace	orientace	k1gFnSc1	orientace
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
politického	politický	k2eAgNnSc2d1	politické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
sociálně	sociálně	k6eAd1	sociálně
tržní	tržní	k2eAgNnSc4d1	tržní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c6	na
myšlenkách	myšlenka	k1gFnPc6	myšlenka
keynesiánství	keynesiánství	k1gNnSc2	keynesiánství
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
neusilovala	usilovat	k5eNaImAgFnS	usilovat
o	o	k7c6	o
splácení	splácení	k1gNnSc6	splácení
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
konjunktury	konjunktura	k1gFnSc2	konjunktura
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
což	což	k3yQnSc1	což
za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
)	)	kIx)	)
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nárůst	nárůst	k1gInSc4	nárůst
těchto	tento	k3xDgInPc2	tento
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Schodky	schodek	k1gInPc4	schodek
vytvářené	vytvářený	k2eAgInPc4d1	vytvářený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
až	až	k9	až
2002	[number]	k4	2002
jsou	být	k5eAaImIp3nP	být
kombinací	kombinace	k1gFnSc7	kombinace
cyklických	cyklický	k2eAgInPc2d1	cyklický
a	a	k8xC	a
strukturálních	strukturální	k2eAgInPc2d1	strukturální
(	(	kIx(	(
<g/>
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
politickým	politický	k2eAgNnSc7d1	politické
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
<g/>
)	)	kIx)	)
schodků	schodek	k1gInPc2	schodek
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
hluboké	hluboký	k2eAgInPc4d1	hluboký
rozpočtové	rozpočtový	k2eAgInPc4d1	rozpočtový
schodky	schodek	k1gInPc4	schodek
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
schodkům	schodek	k1gInPc3	schodek
strukturálním	strukturální	k2eAgNnSc7d1	strukturální
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
neboť	neboť	k8xC	neboť
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
čase	čas	k1gInSc6	čas
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
strukturální	strukturální	k2eAgInSc1d1	strukturální
rozpočtový	rozpočtový	k2eAgInSc1d1	rozpočtový
schodek	schodek	k1gInSc1	schodek
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
čase	čas	k1gInSc6	čas
vysokého	vysoký	k2eAgInSc2d1	vysoký
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
kombinovaného	kombinovaný	k2eAgMnSc4d1	kombinovaný
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
sociálními	sociální	k2eAgInPc7d1	sociální
předvolebními	předvolební	k2eAgInPc7d1	předvolební
výdaji	výdaj	k1gInPc7	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
a	a	k8xC	a
třetímu	třetí	k4xOgMnSc3	třetí
nejhlubšímu	hluboký	k2eAgInSc3d3	nejhlubší
strukturálnímu	strukturální	k2eAgInSc3d1	strukturální
rozpočtovému	rozpočtový	k2eAgInSc3d1	rozpočtový
schodku	schodek	k1gInSc3	schodek
patří	patřit	k5eAaImIp3nS	patřit
schodky	schodek	k1gInPc4	schodek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
ČSSD	ČSSD	kA	ČSSD
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
zavedení	zavedení	k1gNnSc1	zavedení
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
bankovní	bankovní	k2eAgFnSc2d1	bankovní
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
část	část	k1gFnSc4	část
bankovních	bankovní	k2eAgInPc2d1	bankovní
zisků	zisk	k1gInPc2	zisk
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
státním	státní	k2eAgInSc6d1	státní
rozpočtu	rozpočet	k1gInSc6	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
je	být	k5eAaImIp3nS	být
Česko	Česko	k1gNnSc1	Česko
"	"	kIx"	"
<g/>
daňovým	daňový	k2eAgInSc7d1	daňový
rájem	ráj	k1gInSc7	ráj
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
banky	banka	k1gFnPc4	banka
a	a	k8xC	a
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2000	[number]	k4	2000
a	a	k8xC	a
2017	[number]	k4	2017
"	"	kIx"	"
<g/>
odplynulo	odplynout	k5eAaPmAgNnS	odplynout
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dividend	dividenda	k1gFnPc2	dividenda
z	z	k7c2	z
bank	banka	k1gFnPc2	banka
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
460	[number]	k4	460
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
ČSSD	ČSSD	kA	ČSSD
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
pravicovou	pravicový	k2eAgFnSc7d1	pravicová
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
politiky	politik	k1gMnPc4	politik
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
při	při	k7c6	při
prosazování	prosazování	k1gNnSc6	prosazování
některých	některý	k3yIgInPc2	některý
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
straně	strana	k1gFnSc6	strana
teoreticky	teoreticky	k6eAd1	teoreticky
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
tzv.	tzv.	kA	tzv.
Bohumínské	bohumínský	k2eAgNnSc1d1	Bohumínské
usnesení	usnesení	k1gNnSc1	usnesení
<g/>
.	.	kIx.	.
</s>
<s>
Příznivci	příznivec	k1gMnPc1	příznivec
ČSSD	ČSSD	kA	ČSSD
namítají	namítat	k5eAaImIp3nP	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunisté	komunista	k1gMnPc1	komunista
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
jedinou	jediný	k2eAgFnSc7d1	jediná
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
a	a	k8xC	a
ochotná	ochotný	k2eAgFnSc1d1	ochotná
podpořit	podpořit	k5eAaPmF	podpořit
levicové	levicový	k2eAgInPc4d1	levicový
návrhy	návrh	k1gInPc4	návrh
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
politiky	politik	k1gMnPc4	politik
ODS	ODS	kA	ODS
obviněna	obvinit	k5eAaPmNgFnS	obvinit
ze	z	k7c2	z
zneužívání	zneužívání	k1gNnSc2	zneužívání
policie	policie	k1gFnSc2	policie
k	k	k7c3	k
politickým	politický	k2eAgInPc3d1	politický
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc4	důkaz
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
tzv.	tzv.	kA	tzv.
Kubiceho	Kubice	k1gMnSc4	Kubice
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nic	nic	k3yNnSc4	nic
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
obsahu	obsah	k1gInSc2	obsah
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepotvrdilo	potvrdit	k5eNaPmAgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
obvinila	obvinit	k5eAaPmAgFnS	obvinit
ODS	ODS	kA	ODS
z	z	k7c2	z
nelegální	legální	k2eNgFnSc2d1	nelegální
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajně	údajně	k6eAd1	údajně
nesprávného	správný	k2eNgNnSc2d1	nesprávné
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
utajovanými	utajovaný	k2eAgInPc7d1	utajovaný
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
tzv.	tzv.	kA	tzv.
Kubiceho	Kubice	k1gMnSc2	Kubice
zprávy	zpráva	k1gFnSc2	zpráva
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
====	====	k?	====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Čechy	Čech	k1gMnPc7	Čech
=====	=====	k?	=====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Morava	Morava	k1gFnSc1	Morava
=====	=====	k?	=====
</s>
</p>
<p>
<s>
===	===	k?	===
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
1946	[number]	k4	1946
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Sněmovna	sněmovna	k1gFnSc1	sněmovna
lidu	lid	k1gInSc2	lid
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Sněmovna	sněmovna	k1gFnSc1	sněmovna
národů	národ	k1gInPc2	národ
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
====	====	k?	====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Celkové	celkový	k2eAgInPc1d1	celkový
výsledky	výsledek	k1gInPc1	výsledek
=====	=====	k?	=====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Výsledky	výsledek	k1gInPc4	výsledek
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
=====	=====	k?	=====
</s>
</p>
<p>
<s>
====	====	k?	====
Senát	senát	k1gInSc1	senát
====	====	k?	====
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
zisku	zisk	k1gInSc2	zisk
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Komunální	komunální	k2eAgFnPc1d1	komunální
volby	volba	k1gFnPc1	volba
====	====	k?	====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Krajské	krajský	k2eAgFnSc2d1	krajská
volby	volba	k1gFnSc2	volba
=====	=====	k?	=====
</s>
</p>
<p>
<s>
====	====	k?	====
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Sociálně-demokratická	Sociálněemokratický	k2eAgFnSc1d1	Sociálně-demokratická
strana	strana	k1gFnSc1	strana
českoslovanská	českoslovanský	k2eAgFnSc1d1	českoslovanská
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
rakouské	rakouský	k2eAgFnSc2d1	rakouská
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Českoslovanská	českoslovanský	k2eAgFnSc1d1	českoslovanská
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
takticky	takticky	k6eAd1	takticky
a	a	k8xC	a
organizačně	organizačně	k6eAd1	organizačně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
;	;	kIx,	;
jednotná	jednotný	k2eAgFnSc1d1	jednotná
levicová	levicový	k2eAgFnSc1d1	levicová
strana	strana	k1gFnSc1	strana
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
národních	národní	k2eAgMnPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
–	–	k?	–
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
sloučena	sloučen	k2eAgFnSc1d1	sloučena
s	s	k7c7	s
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
paralelně	paralelně	k6eAd1	paralelně
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
fungovala	fungovat	k5eAaImAgFnS	fungovat
také	také	k9	také
exilová	exilový	k2eAgFnSc1d1	exilová
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
centrálou	centrála	k1gFnSc7	centrála
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KAPLAN	Kaplan	k1gMnSc1	Kaplan
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7239	[number]	k4	7239
<g/>
-	-	kIx~	-
<g/>
255	[number]	k4	255
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOPPE	HOPPE	kA	HOPPE
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Opozice	opozice	k1gFnSc1	opozice
'	'	kIx"	'
<g/>
68	[number]	k4	68
<g/>
:	:	kIx,	:
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
KAN	KAN	kA	KAN
a	a	k8xC	a
K	K	kA	K
231	[number]	k4	231
v	v	k7c6	v
období	období	k1gNnSc6	období
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Prostor	prostor	k1gInSc1	prostor
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
HRUBEC	hrubec	k1gMnSc1	hrubec
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
–	–	k?	–
BÁRTA	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
československého	československý	k2eAgNnSc2d1	Československé
sociálnědemokratického	sociálnědemokratický	k2eAgNnSc2d1	sociálnědemokratické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
Praha-Brno	Praha-Brno	k1gNnSc1	Praha-Brno
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
akademie	akademie	k1gFnSc1	akademie
–	–	k?	–
Doplněk	doplněk	k1gInSc1	doplněk
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
HRUBÝ	Hrubý	k1gMnSc1	Hrubý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Léta	léto	k1gNnPc4	léto
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
:	:	kIx,	:
K	k	k7c3	k
historii	historie	k1gFnSc3	historie
Československé	československý	k2eAgFnSc2d1	Československá
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v.	v.	k?	v.
<g/>
n.	n.	k?	n.
b.d.	b.d.	k?	b.d.
[	[	kIx(	[
<g/>
1997	[number]	k4	1997
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
HRUBÝ	Hrubý	k1gMnSc1	Hrubý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Opozice	opozice	k1gFnSc1	opozice
v	v	k7c6	v
exilové	exilový	k2eAgFnSc6d1	exilová
sociální	sociální	k2eAgFnSc6d1	sociální
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
:	:	kIx,	:
Nad	nad	k7c7	nad
knihou	kniha	k1gFnSc7	kniha
Radomíra	Radomír	k1gMnSc2	Radomír
Luži	Luže	k1gFnSc6	Luže
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Soudobé	soudobý	k2eAgFnPc1d1	soudobá
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
188	[number]	k4	188
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANÝR	JANÝR	kA	JANÝR
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
:	:	kIx,	:
Neznámá	známý	k2eNgFnSc1d1	neznámá
kapitola	kapitola	k1gFnSc1	kapitola
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
:	:	kIx,	:
Zápas	zápas	k1gInSc1	zápas
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
Československé	československý	k2eAgFnSc2d1	Československá
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
KREJČÍ	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
demokracií	demokracie	k1gFnSc7	demokracie
a	a	k8xC	a
diktaturou	diktatura	k1gFnSc7	diktatura
<g/>
:	:	kIx,	:
Domov	domov	k1gInSc1	domov
a	a	k8xC	a
exil	exil	k1gInSc1	exil
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Votobia	Votobia	k1gFnSc1	Votobia
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
LOEWY	LOEWY	kA	LOEWY
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Úseky	úsek	k1gInPc1	úsek
polojasna	polojasno	k1gNnSc2	polojasno
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
LUŽA	LUŽA	kA	LUŽA
<g/>
,	,	kIx,	,
Radomír	Radomír	k1gMnSc1	Radomír
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
:	:	kIx,	:
Kapitoly	kapitola	k1gFnPc1	kapitola
z	z	k7c2	z
let	léto	k1gNnPc2	léto
exilu	exil	k1gInSc2	exil
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Československé	československý	k2eAgNnSc1d1	Československé
dokumentační	dokumentační	k2eAgNnSc1d1	dokumentační
středisko	středisko	k1gNnSc1	středisko
–	–	k?	–
Doplněk	doplněk	k1gInSc4	doplněk
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
MEZNÍK	mezník	k1gInSc1	mezník
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
komunistů	komunista	k1gMnPc2	komunista
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
MITROFANOV	MITROFANOV	kA	MITROFANOV
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
:	:	kIx,	:
Za	za	k7c7	za
fasádou	fasáda	k1gFnSc7	fasáda
Lidového	lidový	k2eAgInSc2d1	lidový
domu	dům	k1gInSc2	dům
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
události	událost	k1gFnPc1	událost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Aurora	Aurora	k1gFnSc1	Aurora
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZAHRADNÍČEK	Zahradníček	k1gMnSc1	Zahradníček
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
:	:	kIx,	:
Rozděleni	rozdělen	k2eAgMnPc1d1	rozdělen
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
v	v	k7c6	v
letech	let	k1gInPc6	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Soudobé	soudobý	k2eAgFnPc1d1	soudobá
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
333	[number]	k4	333
<g/>
–	–	k?	–
<g/>
358	[number]	k4	358
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
mýlil	mýlit	k5eAaImAgMnS	mýlit
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
OKURKA	okurka	k1gFnSc1	okurka
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Zapomenutí	zapomenutý	k2eAgMnPc1d1	zapomenutý
hrdinové	hrdina	k1gMnPc1	hrdina
/	/	kIx~	/
Vergessene	Vergessen	k1gInSc5	Vergessen
Helden	Heldna	k1gFnPc2	Heldna
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Lidový	lidový	k2eAgInSc1d1	lidový
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Cíl	cíl	k1gInSc1	cíl
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socialistické	socialistický	k2eAgFnPc1d1	socialistická
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Nález	nález	k1gInSc1	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
právní	právní	k2eAgFnSc3d1	právní
kontinuitě	kontinuita	k1gFnSc3	kontinuita
ČSSD	ČSSD	kA	ČSSD
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
se	se	k3xPyFc4	se
s	s	k7c7	s
minulostí	minulost	k1gFnSc7	minulost
–	–	k?	–
Kapitola	kapitola	k1gFnSc1	kapitola
Tomáše	Tomáš	k1gMnSc2	Tomáš
Zahradníčka	Zahradníček	k1gMnSc2	Zahradníček
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
minulostí	minulost	k1gFnSc7	minulost
<g/>
:	:	kIx,	:
Vytváření	vytváření	k1gNnSc1	vytváření
politických	politický	k2eAgFnPc2d1	politická
identit	identita	k1gFnPc2	identita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
====	====	k?	====
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
====	====	k?	====
</s>
</p>
<p>
<s>
www.cssd.cz	www.cssd.cz	k1gInSc1	www.cssd.cz
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
internacionála	internacionála	k1gFnSc1	internacionála
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
evropských	evropský	k2eAgMnPc2d1	evropský
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
</s>
</p>
<p>
<s>
====	====	k?	====
Historie	historie	k1gFnSc1	historie
strany	strana	k1gFnSc2	strana
====	====	k?	====
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
strany	strana	k1gFnSc2	strana
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
sjezdů	sjezd	k1gInPc2	sjezd
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
====	====	k?	====
</s>
</p>
<p>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
pod	pod	k7c7	pod
rukama	ruka	k1gFnPc7	ruka
<g/>
"	"	kIx"	"
–	–	k?	–
Zvukové	zvukový	k2eAgInPc1d1	zvukový
záznamy	záznam	k1gInPc1	záznam
ze	z	k7c2	z
zasedání	zasedání	k1gNnSc2	zasedání
ÚVV	ÚVV	kA	ÚVV
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
pádu	pád	k1gInSc6	pád
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
</s>
</p>
