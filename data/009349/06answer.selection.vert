<s>
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
česká	český	k2eAgFnSc1d1	Česká
levicová	levicový	k2eAgFnSc1d1	levicová
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
internacionály	internacionála	k1gFnSc2	internacionála
<g/>
,	,	kIx,	,
Progresivní	progresivní	k2eAgFnSc2d1	progresivní
aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
Strany	strana	k1gFnSc2	strana
evropských	evropský	k2eAgMnPc2d1	evropský
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
