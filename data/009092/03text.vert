<p>
<s>
Farmakognozie	Farmakognozie	k1gFnSc1	Farmakognozie
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
"	"	kIx"	"
<g/>
farmakon	farmakon	k1gMnSc1	farmakon
<g/>
"	"	kIx"	"
=	=	kIx~	=
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
gnosis	gnosis	k1gFnSc1	gnosis
<g/>
"	"	kIx"	"
=	=	kIx~	=
poznání	poznání	k1gNnSc1	poznání
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
popisem	popis	k1gInSc7	popis
léčiv	léčivo	k1gNnPc2	léčivo
přírodního	přírodní	k2eAgInSc2d1	přírodní
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
získáváním	získávání	k1gNnSc7	získávání
<g/>
,	,	kIx,	,
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
uchováváním	uchovávání	k1gNnSc7	uchovávání
a	a	k8xC	a
medicínským	medicínský	k2eAgNnSc7d1	medicínské
využitím	využití	k1gNnSc7	využití
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
suroviny	surovina	k1gFnPc1	surovina
se	se	k3xPyFc4	se
ve	v	k7c6	v
farmakognozii	farmakognozie	k1gFnSc6	farmakognozie
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
popisuje	popisovat	k5eAaImIp3nS	popisovat
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
drog	droga	k1gFnPc2	droga
(	(	kIx(	(
<g/>
fytochemie	fytochemie	k1gFnSc1	fytochemie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
obsah	obsah	k1gInSc1	obsah
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
biosyntézu	biosyntéza	k1gFnSc4	biosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
účinky	účinek	k1gInPc1	účinek
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Látky	látka	k1gFnPc1	látka
získávané	získávaný	k2eAgFnPc1d1	získávaná
ze	z	k7c2	z
surovin	surovina	k1gFnPc2	surovina
přírodního	přírodní	k2eAgInSc2d1	přírodní
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
primární	primární	k2eAgFnSc4d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgFnSc4d1	sekundární
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Primární	primární	k2eAgFnPc1d1	primární
látky	látka	k1gFnPc1	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
látky	látka	k1gFnPc4	látka
zapojené	zapojený	k2eAgFnPc4d1	zapojená
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
metabolismu	metabolismus	k1gInSc2	metabolismus
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
plnící	plnící	k2eAgFnPc4d1	plnící
základní	základní	k2eAgFnPc4d1	základní
stavební	stavební	k2eAgFnPc4d1	stavební
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
a	a	k8xC	a
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
oleje	olej	k1gInPc1	olej
a	a	k8xC	a
tuky	tuk	k1gInPc1	tuk
<g/>
,	,	kIx,	,
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sekundární	sekundární	k2eAgFnPc1d1	sekundární
látky	látka	k1gFnPc1	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
produkty	produkt	k1gInPc4	produkt
tzv.	tzv.	kA	tzv.
sekundárního	sekundární	k2eAgInSc2d1	sekundární
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obrannou	obranný	k2eAgFnSc4d1	obranná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
i	i	k9	i
o	o	k7c4	o
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
není	být	k5eNaImIp3nS	být
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tyto	tento	k3xDgFnPc4	tento
látky	látka	k1gFnPc1	látka
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
objasněn	objasněn	k2eAgInSc1d1	objasněn
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
sekundární	sekundární	k2eAgInPc4d1	sekundární
metabolity	metabolit	k1gInPc4	metabolit
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
glykosidy	glykosid	k1gInPc1	glykosid
–	–	k?	–
látky	látka	k1gFnSc2	látka
vážící	vážící	k2eAgInPc4d1	vážící
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
cukr	cukr	k1gInSc4	cukr
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
alkaloidy	alkaloid	k1gInPc1	alkaloid
–	–	k?	–
látky	látka	k1gFnSc2	látka
obsahující	obsahující	k2eAgInSc4d1	obsahující
dusík	dusík	k1gInSc4	dusík
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
silice	silice	k1gFnSc1	silice
–	–	k?	–
tzv.	tzv.	kA	tzv.
éterické	éterický	k2eAgInPc1d1	éterický
oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
složité	složitý	k2eAgFnPc1d1	složitá
směsi	směs	k1gFnPc1	směs
terpenických	terpenický	k2eAgFnPc2d1	terpenický
látek	látka	k1gFnPc2	látka
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
flavonoidy	flavonoid	k1gInPc1	flavonoid
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
třísloviny	tříslovina	k1gFnPc1	tříslovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Droga	droga	k1gFnSc1	droga
(	(	kIx(	(
<g/>
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgFnPc1d1	virtuální
učebnice	učebnice	k1gFnPc1	učebnice
farmakognozie	farmakognozie	k1gFnSc2	farmakognozie
</s>
</p>
<p>
<s>
Mikroskopie	mikroskopie	k1gFnSc1	mikroskopie
drog	droga	k1gFnPc2	droga
</s>
</p>
