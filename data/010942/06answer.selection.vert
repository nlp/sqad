<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
objevený	objevený	k2eAgInSc1d1	objevený
měsíc	měsíc	k1gInSc1	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
šestá	šestý	k4xOgFnSc1	šestý
objevená	objevený	k2eAgFnSc1d1	objevená
oběžnice	oběžnice	k1gFnSc1	oběžnice
planety	planeta	k1gFnSc2	planeta
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
čtyřech	čtyři	k4xCgInPc6	čtyři
Galileových	Galileův	k2eAgInPc6d1	Galileův
měsících	měsíc	k1gInPc6	měsíc
obíhajících	obíhající	k2eAgInPc6d1	obíhající
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
