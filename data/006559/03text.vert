<s>
Transkripce	transkripce	k1gFnSc1	transkripce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
přepis	přepis	k1gInSc1	přepis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
zapsané	zapsaný	k2eAgFnSc2d1	zapsaná
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
DNA	DNA	kA	DNA
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
řetězec	řetězec	k1gInSc1	řetězec
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
obvykle	obvykle	k6eAd1	obvykle
představuje	představovat	k5eAaImIp3nS	představovat
prostředníka	prostředník	k1gMnSc4	prostředník
mezi	mezi	k7c7	mezi
genetickým	genetický	k2eAgInSc7d1	genetický
materiálem	materiál	k1gInSc7	materiál
a	a	k8xC	a
bílkovinami	bílkovina	k1gFnPc7	bílkovina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
některé	některý	k3yIgInPc1	některý
nekódující	kódující	k2eNgInPc1d1	nekódující
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
protein	protein	k1gInSc1	protein
nevzniká	vznikat	k5eNaImIp3nS	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
součást	součást	k1gFnSc1	součást
tzv.	tzv.	kA	tzv.
centrálního	centrální	k2eAgNnSc2d1	centrální
dogmatu	dogma	k1gNnSc2	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
organizmů	organizmus	k1gInPc2	organizmus
včetně	včetně	k7c2	včetně
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
volně	volně	k6eAd1	volně
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
vyšších	vysoký	k2eAgInPc2d2	vyšší
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
eukaryota	eukaryota	k1gFnSc1	eukaryota
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
transkripce	transkripce	k1gFnSc2	transkripce
stojí	stát	k5eAaImIp3nS	stát
enzym	enzym	k1gInSc1	enzym
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgInSc1d1	schopný
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
DNA	dno	k1gNnSc2	dno
vyrábět	vyrábět	k5eAaImF	vyrábět
kopii	kopie	k1gFnSc4	kopie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
rozplete	rozplést	k5eAaPmIp3nS	rozplést
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
genu	gen	k1gInSc2	gen
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
na	na	k7c4	na
nukleotidy	nukleotid	k1gInPc4	nukleotid
DNA	dna	k1gFnSc1	dna
připojovat	připojovat	k5eAaImF	připojovat
komplementární	komplementární	k2eAgInPc4d1	komplementární
nukleotidy	nukleotid	k1gInPc4	nukleotid
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
kupříkladu	kupříkladu	k6eAd1	kupříkladu
řetězec	řetězec	k1gInSc1	řetězec
DNA	DNA	kA	DNA
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
A-T-C-G-G	A-T-C-G-G	k1gFnPc2	A-T-C-G-G
se	se	k3xPyFc4	se
do	do	k7c2	do
RNA	RNA	kA	RNA
přepíše	přepsat	k5eAaPmIp3nS	přepsat
jako	jako	k9	jako
U-A-G-C-C	U-A-G-C-C	k1gFnSc1	U-A-G-C-C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
mRNA	mRNA	k?	mRNA
přepíše	přepsat	k5eAaPmIp3nS	přepsat
celý	celý	k2eAgInSc1d1	celý
gen	gen	k1gInSc1	gen
<g/>
,	,	kIx,	,
jednořetězcová	jednořetězcový	k2eAgFnSc1d1	jednořetězcová
lineární	lineární	k2eAgFnSc1d1	lineární
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
odpojí	odpojit	k5eAaPmIp3nS	odpojit
a	a	k8xC	a
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
putuje	putovat	k5eAaImIp3nS	putovat
k	k	k7c3	k
ribozomu	ribozom	k1gInSc3	ribozom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
translace	translace	k1gFnSc2	translace
vzniká	vznikat	k5eAaImIp3nS	vznikat
bílkovina	bílkovina	k1gFnSc1	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
schéma	schéma	k1gNnSc1	schéma
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
organizmů	organizmus	k1gInPc2	organizmus
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
bližším	blízký	k2eAgInSc6d2	bližší
pohledu	pohled	k1gInSc6	pohled
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
bakterie	bakterie	k1gFnSc1	bakterie
(	(	kIx(	(
<g/>
prokaryota	prokaryota	k1gFnSc1	prokaryota
<g/>
)	)	kIx)	)
odchylují	odchylovat	k5eAaImIp3nP	odchylovat
od	od	k7c2	od
typického	typický	k2eAgInSc2d1	typický
modelu	model	k1gInSc2	model
známého	známý	k2eAgInSc2d1	známý
např.	např.	kA	např.
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
či	či	k8xC	či
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
transkripce	transkripce	k1gFnSc2	transkripce
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
asi	asi	k9	asi
1	[number]	k4	1
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
000	[number]	k4	000
nukleotidů	nukleotid	k1gInPc2	nukleotid
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
je	být	k5eAaImIp3nS	být
však	však	k9	však
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
(	(	kIx(	(
<g/>
in	in	k?	in
vitro	vitro	k1gNnSc4	vitro
<g/>
)	)	kIx)	)
možno	možno	k6eAd1	možno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
poněkud	poněkud	k6eAd1	poněkud
nižší	nízký	k2eAgFnPc4d2	nižší
rychlosti	rychlost	k1gFnPc4	rychlost
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
nukleotidů	nukleotid	k1gInPc2	nukleotid
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Chybovost	chybovost	k1gFnSc1	chybovost
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnPc1	polymeráza
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
hlavního	hlavní	k2eAgInSc2d1	hlavní
enzymu	enzym	k1gInSc2	enzym
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
DNA	dno	k1gNnSc2	dno
polymeráz	polymeráza	k1gFnPc2	polymeráza
ovládajících	ovládající	k2eAgFnPc2d1	ovládající
replikaci	replikace	k1gFnSc4	replikace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
chyba	chyba	k1gFnSc1	chyba
za	za	k7c4	za
10	[number]	k4	10
000	[number]	k4	000
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
chybovost	chybovost	k1gFnSc1	chybovost
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
na	na	k7c4	na
závadu	závada	k1gFnSc4	závada
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
životnost	životnost	k1gFnSc1	životnost
RNA	RNA	kA	RNA
molekul	molekula	k1gFnPc2	molekula
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
poměrně	poměrně	k6eAd1	poměrně
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc4	příčina
chybovosti	chybovost	k1gFnSc2	chybovost
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnPc1	polymeráza
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
například	například	k6eAd1	například
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
vznikající	vznikající	k2eAgFnSc2d1	vznikající
RNA	RNA	kA	RNA
začleněn	začleněn	k2eAgInSc4d1	začleněn
místo	místo	k7c2	místo
guaninu	guanin	k1gInSc2	guanin
adenin	adenin	k1gInSc1	adenin
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
neopravená	opravený	k2eNgFnSc1d1	neopravená
deaminace	deaminace	k1gFnSc1	deaminace
cytosinu	cytosina	k1gFnSc4	cytosina
na	na	k7c4	na
uracil	uracil	k1gInSc4	uracil
ve	v	k7c6	v
vzorovém	vzorový	k2eAgNnSc6d1	vzorové
vlákně	vlákno	k1gNnSc6	vlákno
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
podobná	podobný	k2eAgFnSc1d1	podobná
replikaci	replikace	k1gFnSc3	replikace
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jednoho	jeden	k4xCgInSc2	jeden
řetězce	řetězec	k1gInSc2	řetězec
DNA	dno	k1gNnSc2	dno
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vlákno	vlákno	k1gNnSc4	vlákno
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
procesy	proces	k1gInPc1	proces
jsou	být	k5eAaImIp3nP	být
koneckonců	koneckonců	k9	koneckonců
součástí	součást	k1gFnSc7	součást
centrálního	centrální	k2eAgNnSc2d1	centrální
dogmatu	dogma	k1gNnSc2	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
proteiny	protein	k1gInPc1	protein
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
vzoru	vzor	k1gInSc2	vzor
zapsaného	zapsaný	k2eAgInSc2d1	zapsaný
v	v	k7c6	v
genech	gen	k1gInPc6	gen
v	v	k7c6	v
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
geny	gen	k1gInPc1	gen
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
transkripce	transkripce	k1gFnSc2	transkripce
přepsány	přepsat	k5eAaPmNgFnP	přepsat
do	do	k7c2	do
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
primární	primární	k2eAgInSc1d1	primární
transkript	transkript	k1gInSc1	transkript
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
všech	všecek	k3xTgFnPc2	všecek
RNA	RNA	kA	RNA
vznikajících	vznikající	k2eAgInPc2d1	vznikající
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
transkriptom	transkriptom	k1gInSc1	transkriptom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
vzniká	vznikat	k5eAaImIp3nS	vznikat
zejména	zejména	k9	zejména
mRNA	mRNA	k?	mRNA
(	(	kIx(	(
<g/>
jakýsi	jakýsi	k3yIgInSc1	jakýsi
"	"	kIx"	"
<g/>
návod	návod	k1gInSc1	návod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jiné	jiný	k2eAgMnPc4d1	jiný
druhy	druh	k1gMnPc4	druh
RNA	RNA	kA	RNA
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
funkcí	funkce	k1gFnSc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tzv.	tzv.	kA	tzv.
nekódující	kódující	k2eNgFnPc1d1	nekódující
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
mRNA	mRNA	k?	mRNA
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nekódují	kódovat	k5eNaBmIp3nP	kódovat
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
<g/>
:	:	kIx,	:
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
–	–	k?	–
stavební	stavební	k2eAgFnPc4d1	stavební
funkce	funkce	k1gFnPc4	funkce
v	v	k7c6	v
ribozomu	ribozom	k1gInSc6	ribozom
(	(	kIx(	(
<g/>
tRNA	trnout	k5eAaImSgMnS	trnout
<g/>
)	)	kIx)	)
tRNA	trnout	k5eAaImSgMnS	trnout
(	(	kIx(	(
<g/>
transferová	transferový	k2eAgFnSc1d1	transferová
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
–	–	k?	–
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
transport	transport	k1gInSc4	transport
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
ribozomu	ribozom	k1gInSc3	ribozom
miRNA	miRNA	k?	miRNA
(	(	kIx(	(
<g/>
microRNA	microRNA	k?	microRNA
<g/>
)	)	kIx)	)
–	–	k?	–
regulace	regulace	k1gFnSc2	regulace
genové	genový	k2eAgFnSc2d1	genová
exprese	exprese	k1gFnSc2	exprese
některých	některý	k3yIgInPc2	některý
genů	gen	k1gInPc2	gen
siRNA	siRNA	k?	siRNA
(	(	kIx(	(
<g/>
small	small	k1gInSc1	small
interfering	interfering	k1gInSc1	interfering
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
RNA	RNA	kA	RNA
interference	interference	k1gFnPc1	interference
snRNA	snRNA	k?	snRNA
(	(	kIx(	(
<g/>
small	small	k1gMnSc1	small
nuclear	nuclear	k1gMnSc1	nuclear
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
–	–	k?	–
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
splicingu	splicing	k1gInSc6	splicing
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
podle	podle	k7c2	podle
DNA	dno	k1gNnSc2	dno
molekuly	molekula	k1gFnSc2	molekula
řetězec	řetězec	k1gInSc1	řetězec
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kontrolován	kontrolovat	k5eAaImNgInS	kontrolovat
složitou	složitý	k2eAgFnSc7d1	složitá
molekulární	molekulární	k2eAgFnSc7d1	molekulární
mašinérií	mašinérie	k1gFnSc7	mašinérie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
centru	centrum	k1gNnSc6	centrum
stojí	stát	k5eAaImIp3nS	stát
enzym	enzym	k1gInSc1	enzym
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
polymeráza	polymeráza	k1gFnSc1	polymeráza
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
genu	gen	k1gInSc2	gen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
přepsán	přepsat	k5eAaPmNgInS	přepsat
do	do	k7c2	do
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
postupuje	postupovat	k5eAaImIp3nS	postupovat
nukleotid	nukleotid	k1gInSc4	nukleotid
po	po	k7c6	po
nukleotidu	nukleotid	k1gInSc6	nukleotid
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ribonukleotidů	ribonukleotid	k1gInPc2	ribonukleotid
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
CTP	CTP	kA	CTP
<g/>
,	,	kIx,	,
GTP	GTP	kA	GTP
<g/>
,	,	kIx,	,
UTP	UTP	kA	UTP
<g/>
)	)	kIx)	)
řetězec	řetězec	k1gInSc1	řetězec
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
gen	gen	k1gInSc1	gen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přepisován	přepisovat	k5eAaImNgMnS	přepisovat
zároveň	zároveň	k6eAd1	zároveň
několika	několik	k4yIc7	několik
RNA	RNA	kA	RNA
polymerázami	polymeráza	k1gFnPc7	polymeráza
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
mnohdy	mnohdy	k6eAd1	mnohdy
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
genu	gen	k1gInSc2	gen
až	až	k6eAd1	až
tisíc	tisíc	k4xCgInPc2	tisíc
transkriptů	transkript	k1gInPc2	transkript
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
transkripci	transkripce	k1gFnSc4	transkripce
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
Navázání	navázání	k1gNnSc1	navázání
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
DNA	DNA	kA	DNA
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xS	jako
promotor	promotor	k1gInSc4	promotor
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
aktivace	aktivace	k1gFnSc1	aktivace
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
regulační	regulační	k2eAgInSc1d1	regulační
systém	systém	k1gInSc1	systém
ovládající	ovládající	k2eAgInSc1d1	ovládající
transkripci	transkripce	k1gFnSc4	transkripce
Iniciace	iniciace	k1gFnSc2	iniciace
–	–	k?	–
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
se	se	k3xPyFc4	se
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
vytvářet	vytvářet	k5eAaImF	vytvářet
RNA	RNA	kA	RNA
a	a	k8xC	a
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
promotoru	promotor	k1gInSc2	promotor
Elongace	elongace	k1gFnSc1	elongace
–	–	k?	–
prodlužování	prodlužování	k1gNnSc1	prodlužování
řetězce	řetězec	k1gInSc2	řetězec
Terminace	terminace	k1gFnSc2	terminace
–	–	k?	–
ukončení	ukončení	k1gNnSc1	ukončení
transkripce	transkripce	k1gFnSc2	transkripce
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc2	uvolnění
RNA	RNA	kA	RNA
molekuly	molekula	k1gFnSc2	molekula
<g/>
;	;	kIx,	;
následuje	následovat	k5eAaImIp3nS	následovat
několik	několik	k4yIc1	několik
posttranskripčních	posttranskripční	k2eAgFnPc2d1	posttranskripční
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
procesu	proces	k1gInSc2	proces
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
některé	některý	k3yIgFnPc4	některý
zákonitosti	zákonitost	k1gFnPc4	zákonitost
<g/>
:	:	kIx,	:
K	k	k7c3	k
transkripci	transkripce	k1gFnSc3	transkripce
dochází	docházet	k5eAaImIp3nS	docházet
zásadně	zásadně	k6eAd1	zásadně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
replikace	replikace	k1gFnSc2	replikace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgInPc4d1	nutný
jisté	jistý	k2eAgInPc4d1	jistý
transkripční	transkripční	k2eAgInPc4d1	transkripční
faktory	faktor	k1gInPc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
buď	buď	k8xC	buď
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
nebo	nebo	k8xC	nebo
na	na	k7c4	na
konec	konec	k1gInSc4	konec
genu	gen	k1gInSc2	gen
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c4	na
samotnou	samotný	k2eAgFnSc4d1	samotná
RNA	RNA	kA	RNA
polymerázu	polymeráza	k1gFnSc4	polymeráza
<g/>
,	,	kIx,	,
a	a	k8xC	a
regulují	regulovat	k5eAaImIp3nP	regulovat
transkripci	transkripce	k1gFnSc4	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
organizmy	organizmus	k1gInPc1	organizmus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pivní	pivní	k2eAgFnPc1d1	pivní
kvasinky	kvasinka	k1gFnPc1	kvasinka
(	(	kIx(	(
<g/>
S.	S.	kA	S.
cerevisiae	cerevisia	k1gFnSc2	cerevisia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
kolem	kolem	k7c2	kolem
200	[number]	k4	200
transkripčních	transkripční	k2eAgInPc2d1	transkripční
faktorů	faktor	k1gInPc2	faktor
<g/>
;	;	kIx,	;
člověk	člověk	k1gMnSc1	člověk
jich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kopírování	kopírování	k1gNnSc3	kopírování
kratších	krátký	k2eAgInPc2d2	kratší
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
než	než	k8xS	než
při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
výkonů	výkon	k1gInPc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
přepisovaný	přepisovaný	k2eAgInSc1d1	přepisovaný
gen	gen	k1gInSc1	gen
je	být	k5eAaImIp3nS	být
dystrofin	dystrofin	k1gInSc4	dystrofin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
primární	primární	k2eAgInSc1d1	primární
transkript	transkript	k1gInSc1	transkript
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
2	[number]	k4	2
400	[number]	k4	400
000	[number]	k4	000
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
tohoto	tento	k3xDgInSc2	tento
genu	gen	k1gInSc2	gen
trvá	trvat	k5eAaImIp3nS	trvat
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc4d1	složitý
bílkovinný	bílkovinný	k2eAgInSc4d1	bílkovinný
komplex	komplex	k1gInSc4	komplex
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
několika	několik	k4yIc2	několik
podjednotek	podjednotka	k1gFnPc2	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
iniciaci	iniciace	k1gFnSc4	iniciace
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
počáteční	počáteční	k2eAgFnSc3d1	počáteční
fázi	fáze	k1gFnSc3	fáze
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
především	především	k9	především
podjednotka	podjednotka	k1gFnSc1	podjednotka
sigma	sigma	k1gNnSc2	sigma
(	(	kIx(	(
<g/>
sigma	sigma	k1gNnSc1	sigma
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
σ	σ	k?	σ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
prokaryotické	prokaryotický	k2eAgFnSc6d1	prokaryotická
buňce	buňka	k1gFnSc6	buňka
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgInPc4d1	zvaný
nukleoid	nukleoid	k1gInSc4	nukleoid
(	(	kIx(	(
<g/>
jakási	jakýsi	k3yIgFnSc1	jakýsi
prokaryotická	prokaryotický	k2eAgFnSc1d1	prokaryotická
obdoba	obdoba	k1gFnSc1	obdoba
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
volně	volně	k6eAd1	volně
klouže	klouzat	k5eAaImIp3nS	klouzat
po	po	k7c6	po
dvoušroubovici	dvoušroubovice	k1gFnSc6	dvoušroubovice
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
rozezná	rozeznat	k5eAaPmIp3nS	rozeznat
promotor	promotor	k1gInSc4	promotor
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
jistou	jistý	k2eAgFnSc4d1	jistá
sekvenci	sekvence	k1gFnSc4	sekvence
DNA	DNA	kA	DNA
označující	označující	k2eAgInSc4d1	označující
začátek	začátek	k1gInSc4	začátek
genu	gen	k1gInSc2	gen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naváže	navázat	k5eAaPmIp3nS	navázat
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
na	na	k7c4	na
DNA	dno	k1gNnPc4	dno
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
z	z	k7c2	z
enzymu	enzym	k1gInSc2	enzym
pryč	pryč	k6eAd1	pryč
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
zmíněná	zmíněný	k2eAgNnPc4d1	zmíněné
sigma	sigma	k1gNnPc4	sigma
podjednotka	podjednotka	k1gFnSc1	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
genu	gen	k1gInSc2	gen
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
palindromické	palindromický	k2eAgFnPc1d1	palindromický
sekvence	sekvence	k1gFnPc1	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
možnosti	možnost	k1gFnPc1	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
ukončit	ukončit	k5eAaPmF	ukončit
transkripci	transkripce	k1gFnSc4	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
genu	gen	k1gInSc2	gen
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
opakující	opakující	k2eAgMnSc1d1	opakující
se	se	k3xPyFc4	se
guanin	guanin	k1gInSc1	guanin
<g/>
–	–	k?	–
<g/>
cytosin	cytosin	k1gInSc1	cytosin
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
–	–	k?	–
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
následně	následně	k6eAd1	následně
sekvenci	sekvence	k1gFnSc6	sekvence
několika	několik	k4yIc2	několik
adeninových	adeninový	k2eAgFnPc2d1	adeninový
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
řetězec	řetězec	k1gInSc1	řetězec
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
této	tento	k3xDgFnSc2	tento
sekvence	sekvence	k1gFnSc2	sekvence
"	"	kIx"	"
<g/>
zacuchá	zacuchat	k5eAaPmIp3nS	zacuchat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
prostorové	prostorový	k2eAgNnSc4d1	prostorové
uspořádání	uspořádání	k1gNnSc4	uspořádání
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
signál	signál	k1gInSc4	signál
pro	pro	k7c4	pro
RNA	RNA	kA	RNA
polymerázu	polymeráza	k1gFnSc4	polymeráza
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
terminaci	terminace	k1gFnSc3	terminace
transkripce	transkripce	k1gFnSc2	transkripce
nutný	nutný	k2eAgInSc1d1	nutný
protein	protein	k1gInSc1	protein
rho	rho	k?	rho
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
jistou	jistý	k2eAgFnSc4d1	jistá
sekvenci	sekvence	k1gFnSc4	sekvence
na	na	k7c6	na
RNA	RNA	kA	RNA
a	a	k8xC	a
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
konec	konec	k1gInSc1	konec
RNA	RNA	kA	RNA
řetězce	řetězec	k1gInPc1	řetězec
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
terminace	terminace	k1gFnSc2	terminace
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
jisté	jistý	k2eAgFnPc1d1	jistá
pomocné	pomocný	k2eAgFnPc1d1	pomocná
bílkoviny	bílkovina	k1gFnPc1	bílkovina
a	a	k8xC	a
pomocné	pomocný	k2eAgFnPc1d1	pomocná
okolní	okolní	k2eAgFnPc1d1	okolní
sekvence	sekvence	k1gFnPc1	sekvence
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jejich	jejich	k3xOp3gInSc4	jejich
průběh	průběh	k1gInSc4	průběh
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
například	například	k6eAd1	například
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
či	či	k8xC	či
prvoci	prvok	k1gMnPc1	prvok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
poněkud	poněkud	k6eAd1	poněkud
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ne	ne	k9	ne
jednu	jeden	k4xCgFnSc4	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovnou	rovnou	k6eAd1	rovnou
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
RNA	RNA	kA	RNA
polymeráz	polymeráza	k1gFnPc2	polymeráza
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
II	II	kA	II
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
geny	gen	k1gInPc4	gen
kódující	kódující	k2eAgNnSc1d1	kódující
všechny	všechen	k3xTgFnPc4	všechen
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
většinu	většina	k1gFnSc4	většina
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
snRNA	snRNA	k?	snRNA
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
I	i	k8xC	i
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jadérku	jadérko	k1gNnSc6	jadérko
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přepis	přepis	k1gInSc1	přepis
většiny	většina	k1gFnSc2	většina
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
rRNA	rRNA	k?	rRNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
III	III	kA	III
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
transkripce	transkripce	k1gFnSc1	transkripce
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
rRNA	rRNA	k?	rRNA
a	a	k8xC	a
snRNA	snRNA	k?	snRNA
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
však	však	k9	však
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
další	další	k2eAgFnSc2d1	další
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
iniciaci	iniciace	k1gFnSc4	iniciace
(	(	kIx(	(
<g/>
spuštění	spuštění	k1gNnSc2	spuštění
<g/>
)	)	kIx)	)
transkripce	transkripce	k1gFnSc1	transkripce
nutná	nutný	k2eAgFnSc1d1	nutná
přítomnost	přítomnost	k1gFnSc1	přítomnost
specifické	specifický	k2eAgFnPc1d1	specifická
sekvence	sekvence	k1gFnPc1	sekvence
na	na	k7c6	na
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
promotoru	promotor	k1gInSc2	promotor
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
některých	některý	k3yIgMnPc2	některý
transkripčních	transkripční	k2eAgMnPc2d1	transkripční
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
obecné	obecný	k2eAgInPc4d1	obecný
transkripční	transkripční	k2eAgInPc4d1	transkripční
faktory	faktor	k1gInPc4	faktor
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
u	u	k7c2	u
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
II	II	kA	II
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
jmenovitě	jmenovitě	k6eAd1	jmenovitě
TFIIA	TFIIA	kA	TFIIA
<g/>
,	,	kIx,	,
TFIIB	TFIIB	kA	TFIIB
<g/>
,	,	kIx,	,
TFIID	TFIID	kA	TFIID
<g/>
,	,	kIx,	,
TFIIE	TFIIE	kA	TFIIE
<g/>
,	,	kIx,	,
TFIIF	TFIIF	kA	TFIIF
a	a	k8xC	a
TFIIH	TFIIH	kA	TFIIH
<g/>
.	.	kIx.	.
</s>
<s>
Alespoň	alespoň	k9	alespoň
pět	pět	k4xCc1	pět
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
musí	muset	k5eAaImIp3nP	muset
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
RNA	RNA	kA	RNA
polymerázu	polymeráza	k1gFnSc4	polymeráza
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tzv.	tzv.	kA	tzv.
preiniciační	preiniciační	k2eAgInSc1d1	preiniciační
komplex	komplex	k1gInSc1	komplex
nutný	nutný	k2eAgInSc1d1	nutný
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
transkripce	transkripce	k1gFnSc1	transkripce
vůbec	vůbec	k9	vůbec
začala	začít	k5eAaPmAgFnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
transkripčního	transkripční	k2eAgInSc2d1	transkripční
faktoru	faktor	k1gInSc2	faktor
TFIID	TFIID	kA	TFIID
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
TATA-vazebný	TATAazebný	k2eAgInSc1d1	TATA-vazebný
protein	protein	k1gInSc1	protein
(	(	kIx(	(
<g/>
TBP	TBP	kA	TBP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
dalších	další	k2eAgFnPc2d1	další
bílkovin	bílkovina	k1gFnPc2	bílkovina
obvykle	obvykle	k6eAd1	obvykle
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
TATA	tata	k1gMnSc1	tata
box	box	k1gInSc4	box
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
klíčová	klíčový	k2eAgFnSc1d1	klíčová
část	část	k1gFnSc1	část
promotoru	promotor	k1gInSc2	promotor
umístěného	umístěný	k2eAgInSc2d1	umístěný
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
genu	gen	k1gInSc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
TFIIH	TFIIH	kA	TFIIH
zase	zase	k9	zase
rozplete	rozplést	k5eAaPmIp3nS	rozplést
dvoušroubovici	dvoušroubovice	k1gFnSc4	dvoušroubovice
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
helikázová	helikázový	k2eAgFnSc1d1	helikázový
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
transkripční	transkripční	k2eAgInPc1d1	transkripční
faktory	faktor	k1gInPc1	faktor
–	–	k?	–
až	až	k8xS	až
na	na	k7c6	na
TFIIF	TFIIF	kA	TFIIF
–	–	k?	–
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
z	z	k7c2	z
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
elongaci	elongace	k1gFnSc3	elongace
řetězce	řetězec	k1gInSc2	řetězec
přidáváním	přidávání	k1gNnSc7	přidávání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ribonukleotidů	ribonukleotid	k1gInPc2	ribonukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
ve	v	k7c6	v
výsledném	výsledný	k2eAgNnSc6d1	výsledné
primárním	primární	k2eAgNnSc6d1	primární
transkriptu	transkripto	k1gNnSc6	transkripto
(	(	kIx(	(
<g/>
molekule	molekula	k1gFnSc6	molekula
RNA	RNA	kA	RNA
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
transkripci	transkripce	k1gFnSc6	transkripce
<g/>
)	)	kIx)	)
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejen	nejen	k6eAd1	nejen
kódující	kódující	k2eAgFnPc1d1	kódující
sekvence	sekvence	k1gFnPc1	sekvence
(	(	kIx(	(
<g/>
exony	exona	k1gFnPc1	exona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
introny	intron	k1gMnPc4	intron
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
žádný	žádný	k3yNgInSc4	žádný
protein	protein	k1gInSc4	protein
nekódují	kódovat	k5eNaBmIp3nP	kódovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
RNA	RNA	kA	RNA
mnohdy	mnohdy	k6eAd1	mnohdy
výrazně	výrazně	k6eAd1	výrazně
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
terminaci	terminace	k1gFnSc4	terminace
transkripce	transkripce	k1gFnSc2	transkripce
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
sekvence	sekvence	k1gFnSc1	sekvence
opakujících	opakující	k2eAgFnPc2d1	opakující
se	se	k3xPyFc4	se
adeninových	adeninový	k2eAgFnPc2d1	adeninový
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
poly-A	poly-A	k?	poly-A
<g/>
)	)	kIx)	)
za	za	k7c7	za
posledním	poslední	k2eAgNnSc7d1	poslední
exonem	exono	k1gNnSc7	exono
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
transkripce	transkripce	k1gFnSc1	transkripce
poměrně	poměrně	k6eAd1	poměrně
nekompromisně	kompromisně	k6eNd1	kompromisně
regulována	regulován	k2eAgFnSc1d1	regulována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
nadměrné	nadměrný	k2eAgFnSc3d1	nadměrná
genové	genový	k2eAgFnSc3d1	genová
expresi	exprese	k1gFnSc3	exprese
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
totiž	totiž	k9	totiž
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mnohobuněčného	mnohobuněčný	k2eAgInSc2d1	mnohobuněčný
organizmu	organizmus	k1gInSc2	organizmus
<g/>
)	)	kIx)	)
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
buňce	buňka	k1gFnSc6	buňka
vytvářet	vytvářet	k5eAaImF	vytvářet
všechny	všechen	k3xTgInPc1	všechen
možné	možný	k2eAgInPc1d1	možný
proteiny	protein	k1gInPc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
regulační	regulační	k2eAgFnPc1d1	regulační
události	událost	k1gFnPc1	událost
se	se	k3xPyFc4	se
ději	děj	k1gInPc7	děj
již	již	k6eAd1	již
před	před	k7c7	před
transkripcí	transkripce	k1gFnSc7	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
totiž	totiž	k9	totiž
hraje	hrát	k5eAaImIp3nS	hrát
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
DNA	DNA	kA	DNA
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
sbalena	sbalit	k5eAaPmNgFnS	sbalit
(	(	kIx(	(
<g/>
kondenzována	kondenzovat	k5eAaImNgFnS	kondenzovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
tzv.	tzv.	kA	tzv.
nukleozomy	nukleozom	k1gInPc1	nukleozom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
transkripci	transkripce	k1gFnSc4	transkripce
podporuje	podporovat	k5eAaImIp3nS	podporovat
acetylace	acetylace	k1gFnSc1	acetylace
histonů	histon	k1gInPc2	histon
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc2	jejich
deacetylace	deacetylace	k1gFnSc2	deacetylace
naopak	naopak	k6eAd1	naopak
transkripci	transkripce	k1gFnSc4	transkripce
tlumí	tlumit	k5eAaImIp3nS	tlumit
<g/>
.	.	kIx.	.
</s>
<s>
Transkripci	transkripce	k1gFnSc4	transkripce
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
také	také	k9	také
tlumí	tlumit	k5eAaImIp3nS	tlumit
(	(	kIx(	(
<g/>
různými	různý	k2eAgInPc7d1	různý
mechanismy	mechanismus	k1gInPc7	mechanismus
<g/>
)	)	kIx)	)
metylace	metylace	k1gFnSc1	metylace
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
transkripci	transkripce	k1gFnSc4	transkripce
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
tzv.	tzv.	kA	tzv.
Z-DNA	Z-DNA	k1gFnSc4	Z-DNA
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
určitý	určitý	k2eAgInSc1d1	určitý
typ	typ	k1gInSc1	typ
prostorového	prostorový	k2eAgNnSc2d1	prostorové
uspořádání	uspořádání	k1gNnSc2	uspořádání
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
samotné	samotný	k2eAgFnSc2d1	samotná
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
,	,	kIx,	,
významnými	významný	k2eAgInPc7d1	významný
regulátory	regulátor	k1gInPc7	regulátor
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
tzv.	tzv.	kA	tzv.
transkripční	transkripční	k2eAgInPc4d1	transkripční
faktory	faktor	k1gInPc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
citovaným	citovaný	k2eAgInSc7d1	citovaný
příkladem	příklad	k1gInSc7	příklad
regulace	regulace	k1gFnSc2	regulace
transkripce	transkripce	k1gFnSc2	transkripce
tzv.	tzv.	kA	tzv.
lac	lac	k?	lac
operon	operon	k1gInSc1	operon
<g/>
.	.	kIx.	.
</s>
