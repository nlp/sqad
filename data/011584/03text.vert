<p>
<s>
Enrique	Enrique	k6eAd1	Enrique
Martín	Martín	k1gMnSc1	Martín
Morales	Morales	k1gMnSc1	Morales
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1971	[number]	k4	1971
San	San	k1gMnSc1	San
Juan	Juan	k1gMnSc1	Juan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Ricky	Ricka	k1gFnSc2	Ricka
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
portorický	portorický	k2eAgMnSc1d1	portorický
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
kapely	kapela	k1gFnSc2	kapela
Menudo	Menudo	k1gNnSc1	Menudo
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
sólový	sólový	k2eAgMnSc1d1	sólový
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
několika	několik	k4yIc2	několik
alb	album	k1gNnPc2	album
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
,	,	kIx,	,
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Cup	cup	k1gInSc1	cup
of	of	k?	of
Life	Life	k1gInSc1	Life
<g/>
"	"	kIx"	"
na	na	k7c4	na
41	[number]	k4	41
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
show	show	k1gFnSc1	show
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
pro	pro	k7c4	pro
latinský	latinský	k2eAgInSc4d1	latinský
pop	pop	k1gInSc4	pop
a	a	k8xC	a
uvedla	uvést	k5eAaPmAgFnS	uvést
tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
americké	americký	k2eAgFnSc2d1	americká
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Livin	Livin	k1gInSc1	Livin
'	'	kIx"	'
<g/>
La	la	k1gNnSc1	la
Vida	Vida	k?	Vida
Loca	Loc	k1gInSc2	Loc
<g/>
"	"	kIx"	"
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgInS	pomoct
získat	získat	k5eAaPmF	získat
obrovský	obrovský	k2eAgInSc1d1	obrovský
úspěch	úspěch	k1gInSc1	úspěch
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
vnímána	vnímat	k5eAaImNgFnS	vnímat
začátkem	začátkem	k7c2	začátkem
latinské	latinský	k2eAgFnSc2d1	Latinská
exploze	exploze	k1gFnSc2	exploze
především	především	k9	především
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
od	od	k7c2	od
teto	teta	k1gFnSc5	teta
doby	doba	k1gFnPc1	doba
měly	mít	k5eAaImAgFnP	mít
ostatní	ostatní	k2eAgInSc4d1	ostatní
španělský	španělský	k2eAgInSc4d1	španělský
zpívající	zpívající	k2eAgInSc4d1	zpívající
interpreti	interpret	k1gMnPc1	interpret
snadnější	snadný	k2eAgInSc1d2	snadnější
přechod	přechod	k1gInSc1	přechod
anglicky	anglicky	k6eAd1	anglicky
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
písně	píseň	k1gFnSc2	píseň
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
do	do	k7c2	do
dnešních	dnešní	k2eAgMnPc2d1	dnešní
dnů	den	k1gInPc2	den
přes	přes	k7c4	přes
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgInPc2d3	nejprodávanější
singlů	singl	k1gInPc2	singl
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
anglické	anglický	k2eAgNnSc1d1	anglické
album	album	k1gNnSc1	album
názvem	název	k1gInSc7	název
jednoduše	jednoduše	k6eAd1	jednoduše
Ricky	Rick	k1gInPc1	Rick
Martin	Martina	k1gFnPc2	Martina
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
22	[number]	k4	22
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
další	další	k2eAgNnPc1d1	další
studiová	studiový	k2eAgNnPc1d1	studiové
alba	album	k1gNnPc1	album
<g/>
:	:	kIx,	:
Me	Me	k1gFnSc1	Me
Amarás	Amarása	k1gFnPc2	Amarása
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A	a	k8xC	a
Medio	Medio	k1gMnSc1	Medio
Vivir	Vivir	k1gMnSc1	Vivir
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vuelve	Vuelev	k1gFnSc2	Vuelev
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sound	Sound	k1gMnSc1	Sound
Loaded	Loaded	k1gMnSc1	Loaded
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Almas	Almas	k1gMnSc1	Almas
del	del	k?	del
Silencio	Silencio	k1gMnSc1	Silencio
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Life	Life	k1gFnSc1	Life
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
Música	Músic	k2eAgFnSc1d1	Música
+	+	kIx~	+
Alma	alma	k1gFnSc1	alma
+	+	kIx~	+
Sexo	Sexo	k1gMnSc1	Sexo
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prodej	prodej	k1gInSc1	prodej
jeho	jeho	k3xOp3gNnPc2	jeho
alb	album	k1gNnPc2	album
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
celosvětově	celosvětově	k6eAd1	celosvětově
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
(	(	kIx(	(
<g/>
nejaktuálnější	aktuální	k2eAgFnPc1d3	nejaktuálnější
zpravy	zprava	k1gFnPc1	zprava
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
85	[number]	k4	85
milionech	milion	k4xCgInPc6	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
(	(	kIx(	(
<g/>
jednou	jednou	k6eAd1	jednou
Grammy	Gramma	k1gFnPc4	Gramma
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
Latin	Latin	k1gMnSc1	Latin
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
AMAs	AMAsa	k1gFnPc2	AMAsa
a	a	k8xC	a
osm	osm	k4xCc1	osm
World	Worlda	k1gFnPc2	Worlda
Music	Musice	k1gInPc2	Musice
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
nahrál	nahrát	k5eAaBmAgMnS	nahrát
jedenáct	jedenáct	k4xCc4	jedenáct
"	"	kIx"	"
<g/>
number-one	numbern	k1gInSc5	number-on
<g/>
"	"	kIx"	"
hitů	hit	k1gInPc2	hit
(	(	kIx(	(
<g/>
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
"	"	kIx"	"
<g/>
number-one	numbern	k1gInSc5	number-on
<g/>
"	"	kIx"	"
alb	album	k1gNnPc2	album
(	(	kIx(	(
<g/>
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gFnSc2	jeho
nahrávky	nahrávka	k1gFnSc2	nahrávka
obdržely	obdržet	k5eAaPmAgFnP	obdržet
95	[number]	k4	95
platinových	platinový	k2eAgFnPc2d1	platinová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Koncertoval	koncertovat	k5eAaImAgInS	koncertovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
prvním	první	k4xOgMnSc6	první
mužem	muž	k1gMnSc7	muž
latino	latina	k1gFnSc5	latina
popu	pop	k1gInSc2	pop
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
především	především	k6eAd1	především
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
směsí	směs	k1gFnSc7	směs
španělštiny	španělština	k1gFnSc2	španělština
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
zvané	zvaný	k2eAgFnSc2d1	zvaná
spanglish	spanglish	k1gInSc4	spanglish
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
v	v	k7c6	v
portorickém	portorický	k2eAgMnSc6d1	portorický
San	San	k1gMnSc6	San
Juanu	Juan	k1gMnSc6	Juan
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
zajištěné	zajištěný	k2eAgFnSc2d1	zajištěná
rodiny	rodina	k1gFnSc2	rodina
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Enrique	Enriqu	k1gFnSc2	Enriqu
Martín	Martín	k1gInSc1	Martín
Negroni	Negroň	k1gFnSc3	Negroň
byl	být	k5eAaImAgInS	být
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Nereida	nereida	k1gFnSc1	nereida
Morales	Morales	k1gInSc4	Morales
účetní	účetní	k2eAgInSc1d1	účetní
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
společná	společný	k2eAgFnSc1d1	společná
rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
radost	radost	k1gFnSc1	radost
z	z	k7c2	z
vánočního	vánoční	k2eAgInSc2d1	vánoční
dárku	dárek	k1gInSc2	dárek
netrvala	trvat	k5eNaImAgFnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
Enriquově	Enriquov	k1gInSc6	Enriquov
narození	narození	k1gNnSc2	narození
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
si	se	k3xPyFc3	se
nové	nový	k2eAgFnPc4d1	nová
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
si	se	k3xPyFc3	se
stačil	stačit	k5eAaBmAgMnS	stačit
pořídit	pořídit	k5eAaPmF	pořídit
ještě	ještě	k9	ještě
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ricky	Ricky	k6eAd1	Ricky
Martin	Martin	k1gMnSc1	Martin
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
dva	dva	k4xCgInPc4	dva
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
(	(	kIx(	(
<g/>
Fernando	Fernanda	k1gFnSc5	Fernanda
a	a	k8xC	a
Angel	angel	k1gMnSc1	angel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
Enrique	Enriqu	k1gFnSc2	Enriqu
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodině	rodina	k1gFnSc6	rodina
spíše	spíše	k9	spíše
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
případem	případ	k1gInSc7	případ
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
vázán	vázán	k2eAgInSc1d1	vázán
na	na	k7c4	na
šoubyznys	šoubyznys	k1gInSc4	šoubyznys
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
v	v	k7c4	v
boy	boa	k1gFnPc4	boa
bandu	band	k1gInSc2	band
Menudo	Menudo	k1gNnSc1	Menudo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
prošel	projít	k5eAaPmAgInS	projít
a	a	k8xC	a
poznal	poznat	k5eAaPmAgInS	poznat
kus	kus	k1gInSc4	kus
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
dodělat	dodělat	k5eAaPmF	dodělat
si	se	k3xPyFc3	se
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
komerčním	komerční	k2eAgInSc6d1	komerční
úspěchu	úspěch	k1gInSc6	úspěch
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
připravil	připravit	k5eAaPmAgMnS	připravit
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
nazpívané	nazpívaný	k2eAgFnSc2d1	nazpívaná
anglicky	anglicky	k6eAd1	anglicky
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
eponymní	eponymní	k2eAgNnSc4d1	eponymní
album	album	k1gNnSc4	album
které	který	k3yIgFnSc2	který
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgInPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
bylo	být	k5eAaImAgNnS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
květná	květný	k2eAgFnSc1d1	Květná
1999	[number]	k4	1999
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
albem	album	k1gNnSc7	album
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
US	US	kA	US
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
překonávající	překonávající	k2eAgFnSc4d1	překonávající
661	[number]	k4	661
000	[number]	k4	000
prodávaných	prodávaný	k2eAgFnPc2d1	prodávaná
kusu	kus	k1gInSc2	kus
debutoval	debutovat	k5eAaBmAgMnS	debutovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
nové	nový	k2eAgNnSc4d1	nové
španělské	španělský	k2eAgNnSc4d1	španělské
album	album	k1gNnSc4	album
Almas	Almas	k1gMnSc1	Almas
del	del	k?	del
Silencio	Silencio	k1gMnSc1	Silencio
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Tal	Tal	k1gFnSc1	Tal
Vez	vézt	k5eAaImRp2nS	vézt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
singl	singl	k1gInSc1	singl
okamžitě	okamžitě	k6eAd1	okamžitě
debutoval	debutovat	k5eAaBmAgInS	debutovat
</s>
</p>
<p>
<s>
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
US	US	kA	US
Hot	hot	k0	hot
Latin	latina	k1gFnPc2	latina
Songs	Songs	k1gInSc4	Songs
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c4	na
teto	teta	k1gFnSc5	teta
příčce	příčka	k1gFnSc6	příčka
jedenáct	jedenáct	k4xCc4	jedenáct
týdnů	týden	k1gInPc2	týden
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejspěšnějším	spěšný	k2eAgMnSc7d3	spěšný
Latin	Latin	k1gMnSc1	Latin
singlem	singl	k1gInSc7	singl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Almas	Almas	k1gInSc1	Almas
del	del	k?	del
Silencio	Silencio	k6eAd1	Silencio
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
Top	topit	k5eAaImRp2nS	topit
Latin	Latin	k1gMnSc1	Latin
Albums	Albums	k1gInSc4	Albums
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
zde	zde	k6eAd1	zde
setrvalo	setrvat	k5eAaPmAgNnS	setrvat
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc4	milion
</s>
</p>
<p>
<s>
kusu	kus	k1gInSc3	kus
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
singly	singl	k1gInPc7	singl
byly	být	k5eAaImAgFnP	být
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jaleo	Jaleo	k6eAd1	Jaleo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
v	v	k7c6	v
Hot	hot	k0	hot
Latin	latina	k1gFnPc2	latina
Songs	Songs	k1gInSc1	Songs
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Y	Y	kA	Y
Todo	Todo	k1gMnSc1	Todo
Queda	Queda	k1gMnSc1	Queda
en	en	k?	en
Nada	Nada	k1gMnSc1	Nada
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
v	v	k7c6	v
Hot	hot	k0	hot
Latin	latina	k1gFnPc2	latina
Songs	Songs	k1gInSc1	Songs
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
taktéž	taktéž	k?	taktéž
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Jaleo	Jaleo	k1gMnSc1	Jaleo
<g/>
"	"	kIx"	"
trumfoval	trumfovat	k5eAaImAgInS	trumfovat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
debutoval	debutovat	k5eAaBmAgInS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
pobýval	pobývat	k5eAaImAgInS	pobývat
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
anglické	anglický	k2eAgNnSc4d1	anglické
album	album	k1gNnSc4	album
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Life	Life	k1gNnSc1	Life
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgNnSc1d1	poslední
anglické	anglický	k2eAgNnSc1d1	anglické
album	album	k1gNnSc1	album
Sound	Sound	k1gMnSc1	Sound
Loaded	Loaded	k1gMnSc1	Loaded
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k8xC	i
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Care	car	k1gMnSc5	car
<g/>
"	"	kIx"	"
píseň	píseň	k1gFnSc4	píseň
představuje	představovat	k5eAaImIp3nS	představovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
portorickým	portorický	k2eAgNnSc7d1	portorický
raperem	rapero	k1gNnSc7	rapero
Fat	fatum	k1gNnPc2	fatum
Joem	Joe	k1gNnSc7	Joe
a	a	k8xC	a
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Amerií	Amerie	k1gFnPc2	Amerie
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
Hot	hot	k0	hot
Dance	Danka	k1gFnSc3	Danka
Club	club	k1gInSc1	club
Play	play	k0	play
a	a	k8xC	a
šedesátém	šedesátý	k4xOgInSc6	šedesátý
pátém	pátý	k4xOgNnSc6	pátý
na	na	k7c4	na
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
píseň	píseň	k1gFnSc1	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Alright	Alrighta	k1gFnPc2	Alrighta
<g/>
"	"	kIx"	"
také	také	k9	také
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
speciální	speciální	k2eAgFnSc4d1	speciální
verzi	verze	k1gFnSc4	verze
teto	teta	k1gFnSc5	teta
písně	píseň	k1gFnPc4	píseň
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
zpěvákem	zpěvák	k1gMnSc7	zpěvák
M.	M.	kA	M.
Pokorou	pokora	k1gFnSc7	pokora
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
One	One	k1gMnSc2	One
Night	Night	k1gMnSc1	Night
Only	Onla	k1gMnSc2	Onla
with	with	k1gMnSc1	with
Ricky	Ricka	k1gMnSc2	Ricka
Martin	Martin	k1gMnSc1	Martin
tour	tour	k1gMnSc1	tour
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
zprávy	zpráva	k1gFnSc2	zpráva
tour	toura	k1gFnPc2	toura
začalo	začít	k5eAaPmAgNnS	začít
15.11	[number]	k4	15.11
2005	[number]	k4	2005
v	v	k7c4	v
Mexico	Mexico	k1gNnSc4	Mexico
City	city	k1gNnSc1	city
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Odehrávalo	odehrávat	k5eAaImAgNnS	odehrávat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdoby	obdoba	k1gFnPc1	obdoba
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
akustiky	akustika	k1gFnSc2	akustika
a	a	k8xC	a
světového	světový	k2eAgNnSc2d1	světové
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
jedno	jeden	k4xCgNnSc1	jeden
se	se	k3xPyFc4	se
série	série	k1gFnSc1	série
televizních	televizní	k2eAgInPc2d1	televizní
koncertů	koncert	k1gInPc2	koncert
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
přijato	přijmout	k5eAaPmNgNnS	přijmout
pozitivně	pozitivně	k6eAd1	pozitivně
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
komerčně	komerčně	k6eAd1	komerčně
významný	významný	k2eAgInSc4d1	významný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
29	[number]	k4	29
000	[number]	k4	000
kusu	kus	k1gInSc2	kus
<g/>
,	,	kIx,	,
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
38	[number]	k4	38
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezí	mez	k1gFnSc7	mez
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
Unplugged	Unplugged	k1gMnSc1	Unplugged
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
nazpívané	nazpívaný	k2eAgInPc1d1	nazpívaný
zcela	zcela	k6eAd1	zcela
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
také	také	k9	také
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
US	US	kA	US
Top	topit	k5eAaImRp2nS	topit
Latin	Latin	k1gMnSc1	Latin
Albums	Albums	k1gInSc1	Albums
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
předávání	předávání	k1gNnSc4	předávání
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Latin	latina	k1gFnPc2	latina
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
pop	pop	k1gInSc4	pop
vocal	vocal	k1gInSc1	vocal
album	album	k1gNnSc1	album
<g/>
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
hudební	hudební	k2eAgNnSc4d1	hudební
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
propagace	propagace	k1gFnSc1	propagace
alba	album	k1gNnSc2	album
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
putoval	putovat	k5eAaImAgInS	putovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	s	k7c7	s
svým	své	k1gNnSc7	své
Black	Black	k1gInSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc4	Tour
začalo	začít	k5eAaPmAgNnS	začít
19.2	[number]	k4	19.2
2007	[number]	k4	2007
s	s	k7c7	s
vystoupením	vystoupení	k1gNnSc7	vystoupení
v	v	k7c6	v
Portorické	portorický	k2eAgFnSc6d1	portorická
aréně	aréna	k1gFnSc6	aréna
José	Josá	k1gFnSc2	Josá
Miguel	Miguel	k1gInSc1	Miguel
Agrelot	Agrelot	k1gMnSc1	Agrelot
Coliseum	Coliseum	k1gInSc1	Coliseum
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
14.10	[number]	k4	14.10
2007	[number]	k4	2007
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
svého	svůj	k3xOyFgNnSc2	svůj
celosvětového	celosvětový	k2eAgNnSc2d1	celosvětové
turné	turné	k1gNnSc2	turné
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
pauzu	pauza	k1gFnSc4	pauza
s	s	k7c7	s
hudebním	hudební	k2eAgInSc7d1	hudební
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
se	se	k3xPyFc4	se
na	na	k7c6	na
soukromém	soukromý	k2eAgInSc6d1	soukromý
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
s	s	k7c7	s
názvem	název	k1gInSc7	název
17	[number]	k4	17
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
shrnovalo	shrnovat	k5eAaImAgNnS	shrnovat
sedmnáctiletou	sedmnáctiletý	k2eAgFnSc4d1	sedmnáctiletá
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
převážně	převážně	k6eAd1	převážně
španělské	španělský	k2eAgFnPc4d1	španělská
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdoby	obdoba	k1gFnPc1	obdoba
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
dalšího	další	k2eAgNnSc2d1	další
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
účinkování	účinkování	k1gNnSc1	účinkování
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
a	a	k8xC	a
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
comeback	comeback	k6eAd1	comeback
na	na	k7c6	na
Broadwayi	Broadway	k1gFnSc6	Broadway
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
kniha	kniha	k1gFnSc1	kniha
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
jednoduše	jednoduše	k6eAd1	jednoduše
Me	Me	k1gFnSc3	Me
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
i	i	k8xC	i
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
těchto	tento	k3xDgInPc6	tento
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
dvě	dva	k4xCgNnPc4	dva
písmena	písmeno	k1gNnPc4	písmeno
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
ME	ME	kA	ME
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
YO	YO	kA	YO
a	a	k8xC	a
česky	česky	k6eAd1	česky
JÁ	já	k1gNnSc1	já
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stala	stát	k5eAaPmAgFnS	stát
Bestsellerem	bestseller	k1gInSc7	bestseller
newyorského	newyorský	k2eAgInSc2d1	newyorský
deníku	deník	k1gInSc2	deník
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
<g/>
,	,	kIx,	,
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
literatury	literatura	k1gFnSc2	literatura
faktu	fakt	k1gInSc2	fakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rok	rok	k1gInSc1	rok
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
deváté	devátý	k4xOgNnSc4	devátý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
pojmenované	pojmenovaný	k2eAgNnSc4d1	pojmenované
Música	Música	k1gFnSc1	Música
+	+	kIx~	+
Alma	alma	k1gFnSc1	alma
+	+	kIx~	+
Sexo	Sexo	k6eAd1	Sexo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
dvou	dva	k4xCgFnPc6	dva
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Thing	Thing	k1gMnSc1	Thing
About	About	k1gMnSc1	About
Me	Me	k1gMnSc1	Me
Is	Is	k1gMnSc1	Is
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Joss	Joss	k6eAd1	Joss
Stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
74	[number]	k4	74
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
španělská	španělský	k2eAgFnSc1d1	španělská
verze	verze	k1gFnSc1	verze
"	"	kIx"	"
<g/>
Lo	Lo	k1gMnSc1	Lo
Mejor	Mejor	k1gMnSc1	Mejor
de	de	k?	de
Mi	já	k3xPp1nSc3	já
Vida	Vida	k?	Vida
Eres	Eres	k1gInSc1	Eres
Tú	tú	k0	tú
<g/>
"	"	kIx"	"
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
US	US	kA	US
Hot	hot	k0	hot
Latin	Latin	k1gMnSc1	Latin
Songs	Songsa	k1gFnPc2	Songsa
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
setrvala	setrvat	k5eAaPmAgFnS	setrvat
tam	tam	k6eAd1	tam
i	i	k9	i
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
singly	singl	k1gInPc1	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Más	Más	k1gFnSc1	Más
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Frío	Frío	k6eAd1	Frío
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Música	Música	k1gFnSc1	Música
+	+	kIx~	+
Alma	alma	k1gFnSc1	alma
+	+	kIx~	+
Sexo	Sexo	k6eAd1	Sexo
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
debut	debut	k1gInSc1	debut
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
že	že	k8xS	že
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
strávilo	strávit	k5eAaPmAgNnS	strávit
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
Top	topit	k5eAaImRp2nS	topit
Latin	Latin	k1gMnSc1	Latin
Albums	Albums	k1gInSc1	Albums
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
nazvané	nazvaný	k2eAgNnSc4d1	nazvané
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
alba	album	k1gNnSc2	album
Música	Música	k1gFnSc1	Música
+	+	kIx~	+
Alma	alma	k1gFnSc1	alma
+	+	kIx~	+
Sexo	Sexo	k1gMnSc1	Sexo
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11.7	[number]	k4	11.7
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
17	[number]	k4	17
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
výlučně	výlučně	k6eAd1	výlučně
ve	v	k7c6	v
Velké	velká	k1gFnSc6	velká
Británií	Británie	k1gFnPc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Broadway	Broadwaa	k1gFnPc1	Broadwaa
jaro	jaro	k1gNnSc1	jaro
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
v	v	k7c6	v
úloze	úloha	k1gFnSc6	úloha
revolucionáře	revolucionář	k1gMnSc2	revolucionář
Ernesta	Ernest	k1gMnSc2	Ernest
"	"	kIx"	"
<g/>
Che	che	k0	che
<g/>
"	"	kIx"	"
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
muzikálu	muzikál	k1gInSc2	muzikál
Evita	Evit	k1gInSc2	Evit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
jako	jako	k9	jako
trenér	trenér	k1gMnSc1	trenér
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
Hlas	hlas	k1gInSc1	hlas
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
Voice	Voice	k1gFnSc1	Voice
of	of	k?	of
Australia	Australia	k1gFnSc1	Australia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Martin	Martin	k1gMnSc1	Martin
vydal	vydat	k5eAaPmAgMnS	vydat
Greatest	Greatest	k1gFnSc4	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
<g/>
:	:	kIx,	:
Souvenir	Souvenira	k1gFnPc2	Souvenira
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jen	jen	k9	jen
v	v	k7c6	v
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Australské	australský	k2eAgFnSc6d1	australská
hitparádě	hitparáda	k1gFnSc6	hitparáda
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
stalo	stát	k5eAaPmAgNnS	stát
"	"	kIx"	"
<g/>
number-two	numberwo	k6eAd1	number-two
<g/>
"	"	kIx"	"
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
certifikováno	certifikovat	k5eAaImNgNnS	certifikovat
zlatou	zlatý	k2eAgFnSc7d1	zlatá
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
vydal	vydat	k5eAaPmAgMnS	vydat
novy	nova	k1gFnSc2	nova
anglicky	anglicky	k6eAd1	anglicky
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Come	Com	k1gMnSc2	Com
with	with	k1gMnSc1	with
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
nadcházejícího	nadcházející	k2eAgNnSc2d1	nadcházející
anglicky	anglicky	k6eAd1	anglicky
zpívaného	zpívaný	k2eAgNnSc2d1	zpívané
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
někdy	někdy	k6eAd1	někdy
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nadcházejícího	nadcházející	k2eAgNnSc2d1	nadcházející
alba	album	k1gNnSc2	album
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
připravované	připravovaný	k2eAgNnSc4d1	připravované
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
odehrát	odehrát	k5eAaPmF	odehrát
v	v	k7c6	v
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
obdoby	obdoba	k1gFnPc1	obdoba
patří	patřit	k5eAaImIp3nP	patřit
další	další	k2eAgInPc4d1	další
singly	singl	k1gInPc4	singl
vydané	vydaný	k2eAgInPc4d1	vydaný
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Jennifer	Jennifra	k1gFnPc2	Jennifra
Lopezovou	Lopezový	k2eAgFnSc7d1	Lopezová
a	a	k8xC	a
portorickým	portorický	k2eAgMnSc7d1	portorický
raperem	raper	k1gMnSc7	raper
Wisinem	Wisin	k1gMnSc7	Wisin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
otcem	otec	k1gMnSc7	otec
dvojčat	dvojče	k1gNnPc2	dvojče
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
Matteo	Matteo	k6eAd1	Matteo
a	a	k8xC	a
Valentino	Valentina	k1gFnSc5	Valentina
<g/>
,	,	kIx,	,
narozených	narozený	k2eAgFnPc2d1	narozená
pomocí	pomocí	k7c2	pomocí
náhradní	náhradní	k2eAgFnSc2d1	náhradní
matky	matka	k1gFnSc2	matka
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Diskografie	diskografie	k1gFnPc1	diskografie
(	(	kIx(	(
<g/>
alba	album	k1gNnPc1	album
a	a	k8xC	a
singly	singl	k1gInPc1	singl
<g/>
)	)	kIx)	)
i	i	k8xC	i
Videografie	Videografie	k1gFnSc1	Videografie
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Love	lov	k1gInSc5	lov
Boat	Boat	k1gInSc1	Boat
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Por	Por	k1gMnSc5	Por
siempre	siempr	k1gMnSc5	siempr
amigos	amigos	k1gMnSc1	amigos
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Alcanzar	Alcanzar	k1gMnSc1	Alcanzar
una	una	k?	una
estrella	estrella	k1gMnSc1	estrella
II	II	kA	II
(	(	kIx(	(
<g/>
TV	TV	kA	TV
series	series	k1gInSc1	series
<g/>
)	)	kIx)	)
as	as	k9	as
Pablo	Pablo	k1gNnSc1	Pablo
Loredo	Loredo	k1gNnSc1	Loredo
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Más	Más	k1gFnSc1	Más
que	que	k?	que
alcanzar	alcanzar	k1gInSc1	alcanzar
una	una	k?	una
estrella	estrella	k6eAd1	estrella
as	as	k9	as
Enrique	Enrique	k1gFnSc1	Enrique
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
Getting	Getting	k1gInSc1	Getting
By	by	k9	by
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
General	General	k1gMnSc1	General
Hospital	Hospital	k1gMnSc1	Hospital
(	(	kIx(	(
<g/>
TV	TV	kA	TV
series	series	k1gInSc1	series
<g/>
)	)	kIx)	)
as	as	k9	as
Miguel	Miguel	k1gMnSc1	Miguel
Morez	Morez	k1gMnSc1	Morez
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Barefoot	Barefoot	k1gInSc1	Barefoot
in	in	k?	in
Paradice	Paradice	k1gFnSc2	Paradice
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Hercules	Hercules	k1gInSc1	Hercules
(	(	kIx(	(
<g/>
Spanish	Spanish	k1gInSc1	Spanish
Language	language	k1gFnSc2	language
<g/>
,	,	kIx,	,
Voice	Voice	k1gMnSc2	Voice
Only	Onla	k1gMnSc2	Onla
<g/>
)	)	kIx)	)
as	as	k9	as
Hercules	Hercules	k1gInSc1	Hercules
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
MADtv	MADtv	k1gInSc1	MADtv
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Jsi	být	k5eAaImIp2nS	být
můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Oprah	Opraha	k1gFnPc2	Opraha
Winfrey	Winfrea	k1gFnSc2	Winfrea
Show	show	k1gFnSc2	show
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Susana	Susana	k1gFnSc1	Susana
Giménez	Giménez	k1gMnSc1	Giménez
guest	guest	k1gMnSc1	guest
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
American	American	k1gMnSc1	American
Dad	Dad	k1gMnSc1	Dad
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Glee	Gle	k1gInSc2	Gle
(	(	kIx(	(
<g/>
muzikálový	muzikálový	k2eAgInSc1d1	muzikálový
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
David	David	k1gMnSc1	David
Martinez	Martinez	k1gMnSc1	Martinez
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
Učitel	učitel	k1gMnSc1	učitel
španělštiny	španělština	k1gFnSc2	španělština
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Voice	Voice	k1gFnSc1	Voice
Australia	Australia	k1gFnSc1	Australia
jako	jako	k8xS	jako
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Dancing	dancing	k1gInSc1	dancing
with	with	k1gInSc1	with
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
jako	jako	k8xS	jako
soudce	soudce	k1gMnSc1	soudce
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Voice	Voice	k1gFnSc1	Voice
Australia	Australia	k1gFnSc1	Australia
jako	jako	k8xC	jako
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
La	la	k1gNnSc2	la
Voz	vozit	k5eAaImRp2nS	vozit
<g/>
...	...	k?	...
México	México	k6eAd1	México
as	as	k9	as
Himself	Himself	k1gMnSc1	Himself
(	(	kIx(	(
<g/>
trenér	trenér	k1gMnSc1	trenér
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Voice	Voice	k1gMnSc4	Voice
Arabia	Arabius	k1gMnSc4	Arabius
as	as	k9	as
Himself	Himself	k1gMnSc1	Himself
jako	jako	k8xS	jako
soudce	soudce	k1gMnSc1	soudce
</s>
</p>
<p>
<s>
==	==	k?	==
Turné	turné	k1gNnSc2	turné
==	==	k?	==
</s>
</p>
<p>
<s>
Ricky	Ricky	k6eAd1	Ricky
Martin	Martin	k1gMnSc1	Martin
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Me	Me	k?	Me
Amaras	Amaras	k1gMnSc1	Amaras
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Medio	Medio	k1gMnSc1	Medio
Vivir	Vivir	k1gMnSc1	Vivir
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vuelve	Vuelvat	k5eAaPmIp3nS	Vuelvat
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Livin	Livin	k1gInSc1	Livin
<g/>
'	'	kIx"	'
la	la	k1gNnSc1	la
Vida	Vida	k?	Vida
Loca	Loca	k1gMnSc1	Loca
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
One	One	k?	One
Night	Night	k1gMnSc1	Night
Only	Onla	k1gMnSc2	Onla
with	with	k1gMnSc1	with
Ricky	Ricka	k1gMnSc2	Ricka
Martin	Martin	k1gMnSc1	Martin
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Black	Black	k1gInSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Música	Música	k1gFnSc1	Música
+	+	kIx~	+
Alma	alma	k1gFnSc1	alma
+	+	kIx~	+
Sexo	Sexo	k1gMnSc1	Sexo
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Australian	Australian	k1gMnSc1	Australian
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
One	One	k?	One
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
scéna	scéna	k1gFnSc1	scéna
==	==	k?	==
</s>
</p>
<p>
<s>
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Broadway	Broadway	k1gInPc1	Broadway
–	–	k?	–
Marius	Marius	k1gInSc1	Marius
</s>
</p>
<p>
<s>
Evita	Evita	k1gFnSc1	Evita
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Broadway	Broadwa	k2eAgFnPc4d1	Broadwa
–	–	k?	–
Ché	Ché	k1gFnPc4	Ché
</s>
</p>
<p>
<s>
==	==	k?	==
Nadace	nadace	k1gFnSc2	nadace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ricky	Ricka	k1gFnSc2	Ricka
Martin	Martin	k1gMnSc1	Martin
Foundation	Foundation	k1gInSc1	Foundation
===	===	k?	===
</s>
</p>
<p>
<s>
Ricky	Ricky	k6eAd1	Ricky
Martin	Martin	k2eAgInSc4d1	Martin
Foundation	Foundation	k1gInSc4	Foundation
(	(	kIx(	(
<g/>
Ricky	Ricka	k1gMnSc2	Ricka
Martin	Martin	k1gMnSc1	Martin
Nadace	nadace	k1gFnSc1	nadace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
nadace	nadace	k1gFnSc1	nadace
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Ricky	Ricek	k1gMnPc7	Ricek
Martinem	Martin	k1gMnSc7	Martin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
s	s	k7c7	s
posláním	poslání	k1gNnSc7	poslání
obhajovat	obhajovat	k5eAaImF	obhajovat
pro	pro	k7c4	pro
blaho	blaho	k1gNnSc4	blaho
dětí	dítě	k1gFnPc2	dítě
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
UNICEF	UNICEF	kA	UNICEF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ricky	Ricka	k1gFnSc2	Ricka
Martin	Martin	k1gInSc1	Martin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
hlavní	hlavní	k2eAgFnSc1d1	hlavní
stránka	stránka	k1gFnSc1	stránka
Rickyho	Ricky	k1gMnSc2	Ricky
Martina	Martin	k1gMnSc2	Martin
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
Rickyho	Ricky	k1gMnSc2	Ricky
Martina	Martin	k1gMnSc2	Martin
</s>
</p>
<p>
<s>
Ricky	Ricky	k6eAd1	Ricky
Martin	Martin	k1gMnSc1	Martin
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Allmusic	Allmusice	k1gFnPc2	Allmusice
</s>
</p>
<p>
<s>
Ricky	Ricky	k6eAd1	Ricky
Martin	Martin	k1gMnSc1	Martin
Texty	text	k1gInPc1	text
</s>
</p>
