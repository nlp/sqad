<s>
Louis	Louis	k1gMnSc1	Louis
Victor	Victor	k1gMnSc1	Victor
Pierre	Pierr	k1gInSc5	Pierr
Raymond	Raymond	k1gInSc1	Raymond
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Broglie	Broglie	k1gFnSc1	Broglie
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1892	[number]	k4	1892
Dieppe	Diepp	k1gInSc5	Diepp
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1987	[number]	k4	1987
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kvantový	kvantový	k2eAgMnSc1d1	kvantový
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
princip	princip	k1gInSc4	princip
duality	dualita	k1gFnSc2	dualita
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
