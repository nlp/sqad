<s>
Louis	Louis	k1gMnSc1	Louis
Victor	Victor	k1gMnSc1	Victor
Pierre	Pierr	k1gInSc5	Pierr
Raymond	Raymond	k1gInSc1	Raymond
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Broglie	Broglie	k1gFnSc1	Broglie
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1892	[number]	k4	1892
Dieppe	Diepp	k1gInSc5	Diepp
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1987	[number]	k4	1987
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kvantový	kvantový	k2eAgMnSc1d1	kvantový
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
princip	princip	k1gInSc4	princip
duality	dualita	k1gFnSc2	dualita
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
letech	let	k1gInPc6	let
však	však	k9	však
zahájil	zahájit	k5eAaPmAgInS	zahájit
studium	studium	k1gNnSc4	studium
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
celý	celý	k2eAgInSc4d1	celý
jeho	jeho	k3xOp3gInSc4	jeho
pozdější	pozdní	k2eAgInSc4d2	pozdější
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
1924	[number]	k4	1924
-	-	kIx~	-
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
doktorát	doktorát	k1gInSc4	doktorát
za	za	k7c4	za
teorii	teorie	k1gFnSc4	teorie
elektronové	elektronový	k2eAgFnSc2d1	elektronová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Vlnové	vlnový	k2eAgFnPc1d1	vlnová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
částic	částice	k1gFnPc2	částice
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
:	:	kIx,	:
λ	λ	k?	λ
=	=	kIx~	=
h	h	k?	h
<g/>
/	/	kIx~	/
<g/>
mv	mv	k?	mv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
λ	λ	k?	λ
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
h	h	k?	h
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
v	v	k7c4	v
její	její	k3xOp3gFnSc4	její
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Broglieho	Broglieha	k1gMnSc5	Broglieha
teorii	teorie	k1gFnSc3	teorie
hmotných	hmotný	k2eAgFnPc2d1	hmotná
vln	vlna	k1gFnPc2	vlna
elektronu	elektron	k1gInSc2	elektron
později	pozdě	k6eAd2	pozdě
užil	užít	k5eAaPmAgMnS	užít
Erwin	Erwin	k1gMnSc1	Erwin
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
1928	[number]	k4	1928
-	-	kIx~	-
profesorem	profesor	k1gMnSc7	profesor
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
Henriho	Henri	k1gMnSc4	Henri
Poincarého	Poincarý	k2eAgInSc2d1	Poincarý
<g/>
.	.	kIx.	.
1929	[number]	k4	1929
-	-	kIx~	-
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
objev	objev	k1gInSc4	objev
vlnově	vlnově	k6eAd1	vlnově
korpuskulárního	korpuskulární	k2eAgInSc2d1	korpuskulární
dualismu	dualismus	k1gInSc2	dualismus
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
-	-	kIx~	-
poradce	poradce	k1gMnSc1	poradce
francouzské	francouzský	k2eAgFnSc2d1	francouzská
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Broglia	Broglia	k1gFnSc1	Broglia
di	di	k?	di
Chieri	Chier	k1gFnSc2	Chier
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
piemontský	piemontský	k2eAgInSc4d1	piemontský
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1643	[number]	k4	1643
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Victor	Victor	k1gMnSc1	Victor
<g/>
,	,	kIx,	,
duc	duc	k0	duc
de	de	k?	de
Broglie	Broglie	k1gFnPc4	Broglie
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
ještě	ještě	k6eAd1	ještě
dalšího	další	k2eAgMnSc4d1	další
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc6	Maurika
de	de	k?	de
Broglie	Broglie	k1gFnSc2	Broglie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
také	také	k9	také
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
de	de	k?	de
Broglie	Broglie	k1gFnSc2	Broglie
napsal	napsat	k5eAaPmAgMnS	napsat
řadu	řada	k1gFnSc4	řada
populárně	populárně	k6eAd1	populárně
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
<g/>
:	:	kIx,	:
Hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
Revoluce	revoluce	k1gFnSc1	revoluce
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
Fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
mikrofyzika	mikrofyzika	k1gFnSc1	mikrofyzika
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
Nové	Nové	k2eAgFnPc1d1	Nové
perspektivy	perspektiva	k1gFnPc1	perspektiva
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
</s>
