<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Paolini	Paolin	k2eAgMnPc1d1	Paolin
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1983	[number]	k4	1983
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
Eragon	Eragon	k1gMnSc1	Eragon
<g/>
,	,	kIx,	,
Eldest	Eldest	k1gMnSc1	Eldest
<g/>
,	,	kIx,	,
Brisingr	Brisingr	k1gMnSc1	Brisingr
a	a	k8xC	a
Inheritance	inheritance	k1gFnSc1	inheritance
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Odkazu	odkaz	k1gInSc2	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Brisingr	Brisingr	k1gInSc1	Brisingr
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Inheritance	inheritance	k1gFnSc1	inheritance
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
vybraných	vybraný	k2eAgInPc6d1	vybraný
trzích	trh	k1gInPc6	trh
objevil	objevit	k5eAaPmAgInS	objevit
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
;	;	kIx,	;
v	v	k7c6	v
ČR	ČR	kA	ČR
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
fantasy	fantas	k1gInPc1	fantas
světě	svět	k1gInSc6	svět
Alagaësia	Alagaësium	k1gNnSc2	Alagaësium
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
mladého	mladý	k2eAgMnSc2d1	mladý
muže	muž	k1gMnSc2	muž
Eragona	Eragon	k1gMnSc2	Eragon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
nalezení	nalezení	k1gNnSc6	nalezení
dračího	dračí	k2eAgNnSc2d1	dračí
vejce	vejce	k1gNnSc2	vejce
stane	stanout	k5eAaPmIp3nS	stanout
Dračím	dračí	k2eAgMnSc7d1	dračí
jezdcem	jezdec	k1gMnSc7	jezdec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dračice	dračice	k1gFnSc1	dračice
Safiry	Safira	k1gFnSc2	Safira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
žil	žíla	k1gFnPc2	žíla
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Angelou	Angela	k1gFnSc7	Angela
a	a	k8xC	a
kočkou	kočka	k1gFnSc7	kočka
s	s	k7c7	s
kokršpanělem	kokršpaněl	k1gMnSc7	kokršpaněl
v	v	k7c6	v
Montaně	Montana	k1gFnSc6	Montana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
ho	on	k3xPp3gInSc4	on
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
místní	místní	k2eAgFnSc1d1	místní
nádherná	nádherný	k2eAgFnSc1d1	nádherná
příroda	příroda	k1gFnSc1	příroda
a	a	k8xC	a
půvab	půvab	k1gInSc1	půvab
zdejších	zdejší	k2eAgNnPc2d1	zdejší
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
psal	psát	k5eAaImAgMnS	psát
krátké	krátký	k2eAgInPc4d1	krátký
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
rád	rád	k6eAd1	rád
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
četl	číst	k5eAaImAgMnS	číst
knihy	kniha	k1gFnPc4	kniha
žánru	žánr	k1gInSc2	žánr
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
se	se	k3xPyFc4	se
nechával	nechávat	k5eAaImAgMnS	nechávat
inspirovat	inspirovat	k5eAaBmF	inspirovat
vážnou	vážný	k2eAgFnSc7d1	vážná
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
například	například	k6eAd1	například
Beethovena	Beethoven	k1gMnSc4	Beethoven
nebo	nebo	k8xC	nebo
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svojit	k5eAaImIp3nS	svojit
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
Eragona	Eragon	k1gMnSc2	Eragon
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
ji	on	k3xPp3gFnSc4	on
nechtěl	chtít	k5eNaImAgMnS	chtít
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
;	;	kIx,	;
psal	psát	k5eAaImAgMnS	psát
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
postavy	postava	k1gFnPc4	postava
si	se	k3xPyFc3	se
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
Angely	Angela	k1gFnSc2	Angela
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yIgFnSc6	který
najdete	najít	k5eAaPmIp2nP	najít
jeho	jeho	k3xOp3gFnSc4	jeho
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
tuto	tento	k3xDgFnSc4	tento
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
upravil	upravit	k5eAaPmAgMnS	upravit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
přečíst	přečíst	k5eAaPmF	přečíst
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
grafickou	grafický	k2eAgFnSc4d1	grafická
úpravu	úprava	k1gFnSc4	úprava
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
i	i	k9	i
mapu	mapa	k1gFnSc4	mapa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Spoustu	spoustu	k6eAd1	spoustu
času	čas	k1gInSc2	čas
mu	on	k3xPp3gInSc3	on
trvalo	trvat	k5eAaImAgNnS	trvat
vytvořit	vytvořit	k5eAaPmF	vytvořit
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
obrázek	obrázek	k1gInSc1	obrázek
oka	oko	k1gNnSc2	oko
dračice	dračice	k1gFnSc2	dračice
Safiry	Safira	k1gFnSc2	Safira
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
přišla	přijít	k5eAaPmAgFnS	přijít
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
knihovnách	knihovna	k1gFnPc6	knihovna
a	a	k8xC	a
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
rodinném	rodinný	k2eAgNnSc6d1	rodinné
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
Paolini	Paolin	k2eAgMnPc1d1	Paolin
International	International	k1gMnPc1	International
LLC	LLC	kA	LLC
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
poté	poté	k6eAd1	poté
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Knopf	Knopf	k1gInSc4	Knopf
Books	Books	k1gInSc4	Books
for	forum	k1gNnPc2	forum
Young	Younga	k1gFnPc2	Younga
Readers	Readersa	k1gFnPc2	Readersa
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
překladech	překlad	k1gInPc6	překlad
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c4	v
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Fragment	fragment	k1gInSc4	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Paolini	Paolin	k2eAgMnPc1d1	Paolin
o	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
změnil	změnit	k5eAaPmAgMnS	změnit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
Eragona	Eragon	k1gMnSc4	Eragon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Christopher	Christophra	k1gFnPc2	Christophra
Paolini	Paolin	k2eAgMnPc1d1	Paolin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
knih	kniha	k1gFnPc2	kniha
Eragon	Eragona	k1gFnPc2	Eragona
a	a	k8xC	a
Eldest	Eldest	k1gFnSc4	Eldest
</s>
</p>
<p>
<s>
Webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
věnované	věnovaný	k2eAgFnPc4d1	věnovaná
knihám	kniha	k1gFnPc3	kniha
z	z	k7c2	z
edice	edice	k1gFnSc2	edice
Odkaz	odkaz	k1gInSc1	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
</s>
</p>
<p>
<s>
Webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
knize	kniha	k1gFnSc3	kniha
Brisingr	Brisingra	k1gFnPc2	Brisingra
</s>
</p>
<p>
<s>
Alagaesie	Alagaesie	k1gFnSc1	Alagaesie
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
Nejnovější	nový	k2eAgFnPc4d3	nejnovější
informace	informace	k1gFnPc4	informace
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Paolini	Paolin	k2eAgMnPc1d1	Paolin
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Christopherem	Christophero	k1gNnSc7	Christophero
Paolinim	Paolinima	k1gFnPc2	Paolinima
</s>
</p>
