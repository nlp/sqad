<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
obec	obec	k1gFnSc1	obec
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
ve	v	k7c6	v
frýdlantském	frýdlantský	k2eAgInSc6d1	frýdlantský
výběžku	výběžek	k1gInSc6	výběžek
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
jí	on	k3xPp3gFnSc7	on
Jindřichovický	Jindřichovický	k2eAgInSc1d1	Jindřichovický
potok	potok	k1gInSc1	potok
<g/>
?	?	kIx.	?
</s>
