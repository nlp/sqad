<s>
Jindřichovice	Jindřichovice	k1gFnSc1	Jindřichovice
pod	pod	k7c7	pod
Smrkem	smrk	k1gInSc7	smrk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Heinersdorf	Heinersdorf	k1gInSc1	Heinersdorf
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Tafelfichte	Tafelficht	k1gMnSc5	Tafelficht
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
okrese	okres	k1gInSc6	okres
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
vsí	ves	k1gFnPc2	ves
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Frýdlantského	frýdlantský	k2eAgInSc2d1	frýdlantský
výběžku	výběžek	k1gInSc2	výběžek
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
km	km	kA	km
od	od	k7c2	od
Frýdlantu	Frýdlant	k1gInSc2	Frýdlant
a	a	k8xC	a
35	[number]	k4	35
km	km	kA	km
od	od	k7c2	od
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
protéká	protékat	k5eAaImIp3nS	protékat
Jindřichovický	Jindřichovický	k2eAgInSc4d1	Jindřichovický
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
má	mít	k5eAaImIp3nS	mít
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Frýdlantem	Frýdlant	k1gInSc7	Frýdlant
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
spojení	spojení	k1gNnSc1	spojení
silniční	silniční	k2eAgNnSc1d1	silniční
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obce	obec	k1gFnSc2	obec
jezdí	jezdit	k5eAaImIp3nP	jezdit
autobusy	autobus	k1gInPc1	autobus
z	z	k7c2	z
Liberce	Liberec	k1gInSc2	Liberec
(	(	kIx(	(
<g/>
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
školka	školka	k1gFnSc1	školka
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
knihovna	knihovna	k1gFnSc1	knihovna
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
v	v	k7c6	v
ČR	ČR	kA	ČR
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
aktivitám	aktivita	k1gFnPc3	aktivita
svého	svůj	k3xOyFgMnSc2	svůj
bývalého	bývalý	k2eAgMnSc2d1	bývalý
starosty	starosta	k1gMnSc2	starosta
Ing.	ing.	kA	ing.
Petra	Petr	k1gMnSc2	Petr
Pávka	pávek	k1gMnSc2	pávek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
úřadování	úřadování	k1gNnSc2	úřadování
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
postavena	postaven	k2eAgFnSc1d1	postavena
dvojice	dvojice	k1gFnSc1	dvojice
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
kotelna	kotelna	k1gFnSc1	kotelna
na	na	k7c4	na
biomasu	biomasa	k1gFnSc4	biomasa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
vyhláška	vyhláška	k1gFnSc1	vyhláška
zakazující	zakazující	k2eAgFnSc1d1	zakazující
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
vstup	vstup	k1gInSc1	vstup
úředníkům	úředník	k1gMnPc3	úředník
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgNnSc2d1	předchozí
ohlášení	ohlášení	k1gNnSc2	ohlášení
a	a	k8xC	a
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
libereckou	liberecký	k2eAgFnSc7d1	liberecká
Technickou	technický	k2eAgFnSc7d1	technická
univerzitou	univerzita	k1gFnSc7	univerzita
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
a	a	k8xC	a
inovační	inovační	k2eAgNnSc1d1	inovační
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgInSc1d1	okolní
kraj	kraj	k1gInSc1	kraj
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1278	[number]	k4	1278
rod	rod	k1gInSc4	rod
Biberštejnů	Biberštejn	k1gInPc2	Biberštejn
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
během	během	k7c2	během
jejich	jejich	k3xOp3gFnSc2	jejich
správy	správa	k1gFnSc2	správa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
panství	panství	k1gNnSc6	panství
také	také	k9	také
nevelká	velký	k2eNgFnSc1d1	nevelká
víska	víska	k1gFnSc1	víska
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
chalupách	chalupa	k1gFnPc6	chalupa
obývaných	obývaný	k2eAgFnPc6d1	obývaná
převážně	převážně	k6eAd1	převážně
dřevorubci	dřevorubec	k1gMnSc3	dřevorubec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
zmiňována	zmiňován	k2eAgFnSc1d1	zmiňována
jménem	jméno	k1gNnSc7	jméno
Heynrichsdorf	Heynrichsdorf	k1gInSc1	Heynrichsdorf
ym	ym	k?	ym
gebirge	gebirgat	k5eAaPmIp3nS	gebirgat
gelegen	gelegen	k1gInSc1	gelegen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
obci	obec	k1gFnSc6	obec
postaven	postavit	k5eAaPmNgInS	postavit
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
v	v	k7c6	v
románském	románský	k2eAgInSc6d1	románský
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1431	[number]	k4	1431
se	se	k3xPyFc4	se
ze	z	k7c2	z
spanilé	spanilý	k2eAgFnSc2d1	spanilá
jízdy	jízda	k1gFnSc2	jízda
od	od	k7c2	od
Baltu	Balt	k1gInSc2	Balt
vracel	vracet	k5eAaImAgInS	vracet
oddíl	oddíl	k1gInSc1	oddíl
husitských	husitský	k2eAgMnPc2d1	husitský
bojovníků	bojovník	k1gMnPc2	bojovník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
Sán	sán	k2eAgMnSc1d1	sán
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
se	se	k3xPyFc4	se
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
osadě	osada	k1gFnSc6	osada
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
zdejší	zdejší	k2eAgMnPc1d1	zdejší
obyvatele	obyvatel	k1gMnSc4	obyvatel
obrátit	obrátit	k5eAaPmF	obrátit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Ves	ves	k1gFnSc4	ves
zapálili	zapálit	k5eAaPmAgMnP	zapálit
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
pobořili	pobořit	k5eAaPmAgMnP	pobořit
<g/>
,	,	kIx,	,
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
přežili	přežít	k5eAaPmAgMnP	přežít
jen	jen	k9	jen
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
okolních	okolní	k2eAgInPc2d1	okolní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
uprchlíci	uprchlík	k1gMnPc1	uprchlík
založili	založit	k5eAaPmAgMnP	založit
novou	nový	k2eAgFnSc4d1	nová
osadu	osada	k1gFnSc4	osada
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Ves	ves	k1gFnSc1	ves
zde	zde	k6eAd1	zde
založená	založený	k2eAgFnSc1d1	založená
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
Heinrichsdorf	Heinrichsdorf	k1gInSc1	Heinrichsdorf
(	(	kIx(	(
<g/>
Jindřichova	Jindřichův	k2eAgFnSc1d1	Jindřichova
ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
měla	mít	k5eAaImAgFnS	mít
51	[number]	k4	51
čísel	číslo	k1gNnPc2	číslo
popisných	popisný	k2eAgMnPc2d1	popisný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1895	[number]	k4	1895
existovaly	existovat	k5eAaImAgFnP	existovat
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
klimatické	klimatický	k2eAgFnPc1d1	klimatická
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
letovisko	letovisko	k1gNnSc1	letovisko
nabízející	nabízející	k2eAgFnSc4d1	nabízející
léčbu	léčba	k1gFnSc4	léčba
klidem	klid	k1gInSc7	klid
pro	pro	k7c4	pro
rekonvalescenty	rekonvalescent	k1gMnPc4	rekonvalescent
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
lázní	lázeň	k1gFnPc2	lázeň
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nezdařilo	zdařit	k5eNaPmAgNnS	zdařit
zmapovat	zmapovat	k5eAaPmF	zmapovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
během	běh	k1gInSc7	běh
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světově	světově	k6eAd1	světově
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozmach	rozmach	k1gInSc4	rozmach
zažila	zažít	k5eAaPmAgFnS	zažít
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1902	[number]	k4	1902
mohli	moct	k5eAaImAgMnP	moct
tamní	tamní	k2eAgMnPc1d1	tamní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
využívat	využívat	k5eAaImF	využívat
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
a	a	k8xC	a
hned	hned	k6eAd1	hned
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
přeshraniční	přeshraniční	k2eAgFnSc2d1	přeshraniční
trati	trať	k1gFnSc2	trať
do	do	k7c2	do
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Friedebergu	Friedeberg	k1gInSc2	Friedeberg
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Mirsk	Mirsk	k1gInSc1	Mirsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Jindřichovic	Jindřichovice	k1gFnPc2	Jindřichovice
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
349	[number]	k4	349
domků	domek	k1gInPc2	domek
<g/>
,	,	kIx,	,
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
žilo	žít	k5eAaImAgNnS	žít
2440	[number]	k4	2440
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2525	[number]	k4	2525
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
Malá	malý	k2eAgFnSc1d1	malá
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatele	obyvatel	k1gMnPc4	obyvatel
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
okolních	okolní	k2eAgMnPc2d1	okolní
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
především	především	k6eAd1	především
prosperující	prosperující	k2eAgFnSc1d1	prosperující
textilka	textilka	k1gFnSc1	textilka
E.	E.	kA	E.
<g/>
Heintschel	Heintschel	k1gInSc1	Heintschel
&	&	k?	&
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Jindřichovice	Jindřichovice	k1gFnSc2	Jindřichovice
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
na	na	k7c4	na
důležitosti	důležitost	k1gFnPc4	důležitost
a	a	k8xC	a
vzrůstající	vzrůstající	k2eAgFnSc2d1	vzrůstající
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
využili	využít	k5eAaPmAgMnP	využít
henleinovci	henleinovec	k1gMnPc1	henleinovec
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
1049	[number]	k4	1049
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
odsunuto	odsunout	k5eAaPmNgNnS	odsunout
551	[number]	k4	551
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
přišlo	přijít	k5eAaPmAgNnS	přijít
210	[number]	k4	210
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Poválečný	poválečný	k2eAgInSc1d1	poválečný
vývoj	vývoj	k1gInSc1	vývoj
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
obec	obec	k1gFnSc4	obec
období	období	k1gNnSc2	období
úpadku	úpadek	k1gInSc2	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Textilní	textilní	k2eAgFnSc1d1	textilní
továrna	továrna	k1gFnSc1	továrna
se	se	k3xPyFc4	se
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
na	na	k7c4	na
betonárku	betonárka	k1gFnSc4	betonárka
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c4	na
skladiště	skladiště	k1gNnSc4	skladiště
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
srovnala	srovnat	k5eAaPmAgFnS	srovnat
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
se	s	k7c7	s
zemí	zem	k1gFnPc2	zem
veškeré	veškerý	k3xTgFnSc2	veškerý
momentálně	momentálně	k6eAd1	momentálně
nepoužívané	používaný	k2eNgFnSc2d1	nepoužívaná
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
Jindřichovice	Jindřichovice	k1gFnPc1	Jindřichovice
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
o	o	k7c4	o
Dětřichovec	Dětřichovec	k1gInSc4	Dětřichovec
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
Novému	nový	k2eAgNnSc3d1	nové
Městu	město	k1gNnSc3	město
pod	pod	k7c7	pod
Smrkem	smrk	k1gInSc7	smrk
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
odtrhly	odtrhnout	k5eAaPmAgInP	odtrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Jindřichovice	Jindřichovice	k1gFnSc1	Jindřichovice
pod	pod	k7c7	pod
Smrkem	smrk	k1gInSc7	smrk
Dětřichovec	Dětřichovec	k1gMnSc1	Dětřichovec
Dvě	dva	k4xCgFnPc1	dva
větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
o	o	k7c6	o
nominálním	nominální	k2eAgInSc6d1	nominální
výkonu	výkon	k1gInSc6	výkon
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
600	[number]	k4	600
kW	kW	kA	kW
dodávají	dodávat	k5eAaImIp3nP	dodávat
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
elektřinu	elektřina	k1gFnSc4	elektřina
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
před	před	k7c7	před
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
řadu	řada	k1gFnSc4	řada
exponátů	exponát	k1gInPc2	exponát
od	od	k7c2	od
velkých	velký	k2eAgInPc2d1	velký
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
strojů	stroj	k1gInPc2	stroj
až	až	k9	až
po	po	k7c6	po
nádobí	nádobí	k1gNnSc6	nádobí
a	a	k8xC	a
nářadí	nářadí	k1gNnSc6	nářadí
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
exponátů	exponát	k1gInPc2	exponát
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Ruiny	ruina	k1gFnPc1	ruina
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
Statek	statek	k1gInSc1	statek
Jiřího	Jiří	k1gMnSc2	Jiří
Zezulky	Zezulky	k?	Zezulky
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
získala	získat	k5eAaPmAgFnS	získat
obec	obec	k1gFnSc1	obec
od	od	k7c2	od
předsedy	předseda	k1gMnSc2	předseda
parlamentu	parlament	k1gInSc2	parlament
znak	znak	k1gInSc1	znak
a	a	k8xC	a
prapor	prapor	k1gInSc1	prapor
a	a	k8xC	a
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
pravidelně	pravidelně	k6eAd1	pravidelně
konají	konat	k5eAaImIp3nP	konat
Jindřichovické	Jindřichovický	k2eAgInPc4d1	Jindřichovický
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znaku	znak	k1gInSc6	znak
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
nacházejí	nacházet	k5eAaImIp3nP	nacházet
smrkové	smrkový	k2eAgFnPc1d1	smrková
větvičky	větvička	k1gFnPc1	větvička
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
Smrkem	smrk	k1gInSc7	smrk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
je	být	k5eAaImIp3nS	být
jelení	jelení	k2eAgInSc1d1	jelení
paroh	paroh	k1gInSc1	paroh
z	z	k7c2	z
erbu	erb	k1gInSc2	erb
Biberštejnů	Biberštejn	k1gInPc2	Biberštejn
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
praporu	prapor	k1gInSc2	prapor
tvoří	tvořit	k5eAaImIp3nS	tvořit
zelené	zelený	k2eAgNnSc4d1	zelené
karé	karé	k1gNnSc4	karé
nad	nad	k7c7	nad
žlutým	žlutý	k2eAgNnSc7d1	žluté
čtvercovým	čtvercový	k2eAgNnSc7d1	čtvercové
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
žlutý	žlutý	k2eAgInSc4d1	žlutý
a	a	k8xC	a
zelený	zelený	k2eAgInSc4d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
karé	karé	k1gNnSc6	karé
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
smrková	smrkový	k2eAgFnSc1d1	smrková
větvička	větvička	k1gFnSc1	větvička
<g/>
,	,	kIx,	,
ve	v	k7c6	v
žlutém	žlutý	k2eAgNnSc6d1	žluté
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
stočený	stočený	k2eAgInSc1d1	stočený
jelení	jelení	k2eAgInSc1d1	jelení
paroh	paroh	k1gInSc1	paroh
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Jindřichovicích	Jindřichovice	k1gFnPc6	Jindřichovice
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
farma	farma	k1gFnSc1	farma
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
osazených	osazený	k2eAgInPc2d1	osazený
jednotkami	jednotka	k1gFnPc7	jednotka
Enercon	Enercon	k1gMnSc1	Enercon
E-	E-	k1gFnSc2	E-
<g/>
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
6.44	[number]	k4	6.44
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
nominálním	nominální	k2eAgInSc6d1	nominální
výkonu	výkon	k1gInSc6	výkon
2	[number]	k4	2
<g/>
x	x	k?	x
600	[number]	k4	600
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
s	s	k7c7	s
investičními	investiční	k2eAgInPc7d1	investiční
náklady	náklad	k1gInPc7	náklad
62	[number]	k4	62
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
51	[number]	k4	51
650	[number]	k4	650
Kč	Kč	kA	Kč
<g/>
/	/	kIx~	/
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
významné	významný	k2eAgFnSc2d1	významná
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
53	[number]	k4	53
miliónů	milión	k4xCgInPc2	milión
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
financován	financovat	k5eAaBmNgInS	financovat
Státním	státní	k2eAgInSc7d1	státní
fondem	fond	k1gInSc7	fond
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
formou	forma	k1gFnSc7	forma
nevratné	vratný	k2eNgFnPc1d1	nevratná
dotace	dotace	k1gFnPc1	dotace
(	(	kIx(	(
<g/>
27,9	[number]	k4	27,9
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
a	a	k8xC	a
nízkoúročené	nízkoúročený	k2eAgFnPc1d1	nízkoúročená
půjčky	půjčka	k1gFnPc1	půjčka
(	(	kIx(	(
<g/>
24,8	[number]	k4	24,8
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
přidělení	přidělení	k1gNnSc4	přidělení
dotace	dotace	k1gFnSc2	dotace
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
osobně	osobně	k6eAd1	osobně
ministr	ministr	k1gMnSc1	ministr
ŽP	ŽP	kA	ŽP
Libor	Libor	k1gMnSc1	Libor
Ambrozek	Ambrozek	k1gInSc1	Ambrozek
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
farmy	farma	k1gFnSc2	farma
byly	být	k5eAaImAgFnP	být
způsobeny	způsobit	k5eAaPmNgFnP	způsobit
potřebou	potřeba	k1gFnSc7	potřeba
vybudování	vybudování	k1gNnSc2	vybudování
základů	základ	k1gInPc2	základ
<g/>
,	,	kIx,	,
manipulačních	manipulační	k2eAgFnPc2d1	manipulační
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
přípojky	přípojka	k1gFnSc2	přípojka
do	do	k7c2	do
VN	VN	kA	VN
22	[number]	k4	22
kV	kV	k?	kV
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
jeřábů	jeřáb	k1gMnPc2	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
výroba	výroba	k1gFnSc1	výroba
1	[number]	k4	1
228,4	[number]	k4	228,4
MWh	MWh	k1gFnPc2	MWh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
koeficient	koeficient	k1gInSc1	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
11,7	[number]	k4	11,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgNnPc1d2	novější
data	datum	k1gNnPc1	datum
o	o	k7c6	o
výrobě	výroba	k1gFnSc6	výroba
VE	v	k7c6	v
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
zveřejňována	zveřejňován	k2eAgFnSc1d1	zveřejňována
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
klesla	klesnout	k5eAaPmAgFnS	klesnout
výroba	výroba	k1gFnSc1	výroba
na	na	k7c4	na
1	[number]	k4	1
085	[number]	k4	085
MWh	MWh	k1gFnPc2	MWh
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc1	využití
10,3	[number]	k4	10,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
meziročně	meziročně	k6eAd1	meziročně
klesl	klesnout	k5eAaPmAgInS	klesnout
z	z	k7c2	z
955	[number]	k4	955
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
na	na	k7c4	na
cca	cca	kA	cca
360	[number]	k4	360
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Kritikové	kritik	k1gMnPc1	kritik
například	například	k6eAd1	například
porovnávají	porovnávat	k5eAaImIp3nP	porovnávat
investičními	investiční	k2eAgInPc7d1	investiční
náklady	náklad	k1gInPc7	náklad
62	[number]	k4	62
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
51	[number]	k4	51
650	[number]	k4	650
Kč	Kč	kA	Kč
<g/>
/	/	kIx~	/
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
s	s	k7c7	s
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
si	se	k3xPyFc3	se
od	od	k7c2	od
prodeje	prodej	k1gInSc2	prodej
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
energie	energie	k1gFnSc2	energie
slibovala	slibovat	k5eAaImAgFnS	slibovat
vysoké	vysoký	k2eAgInPc4d1	vysoký
příjmy	příjem	k1gInPc4	příjem
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
dosahované	dosahovaný	k2eAgFnPc1d1	dosahovaná
tržby	tržba	k1gFnPc1	tržba
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
ani	ani	k9	ani
20	[number]	k4	20
<g/>
%	%	kIx~	%
tržeb	tržba	k1gFnPc2	tržba
<g/>
,	,	kIx,	,
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
instalovanému	instalovaný	k2eAgInSc3d1	instalovaný
výkonu	výkon	k1gInSc3	výkon
<g/>
.	.	kIx.	.
</s>
