<p>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Prokop	Prokop	k1gMnSc1	Prokop
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1380	[number]	k4	1380
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
Lipany	lipan	k1gMnPc7	lipan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
radikální	radikální	k2eAgMnSc1d1	radikální
husitský	husitský	k2eAgMnSc1d1	husitský
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
duchovním	duchovní	k2eAgMnSc7d1	duchovní
správcem	správce	k1gMnSc7	správce
táborské	táborský	k2eAgFnSc2d1	táborská
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
neformální	formální	k2eNgFnSc2d1	neformální
hlavou	hlava	k1gFnSc7	hlava
husitských	husitský	k2eAgFnPc2d1	husitská
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
mládí	mládí	k1gNnSc6	mládí
historické	historický	k2eAgInPc4d1	historický
prameny	pramen	k1gInPc4	pramen
prakticky	prakticky	k6eAd1	prakticky
nehovoří	hovořit	k5eNaImIp3nP	hovořit
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
bohatým	bohatý	k2eAgMnSc7d1	bohatý
pražským	pražský	k2eAgMnSc7d1	pražský
obchodníkem	obchodník	k1gMnSc7	obchodník
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
vůle	vůle	k1gFnSc2	vůle
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
kněžského	kněžský	k2eAgNnSc2d1	kněžské
vysvěcení	vysvěcení	k1gNnSc2	vysvěcení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
ideově	ideově	k6eAd1	ideově
následoval	následovat	k5eAaImAgInS	následovat
hejtmana	hejtman	k1gMnSc4	hejtman
Jana	Jan	k1gMnSc4	Jan
Žižku	Žižka	k1gMnSc4	Žižka
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
propracoval	propracovat	k5eAaPmAgInS	propracovat
mezi	mezi	k7c4	mezi
přední	přední	k2eAgMnPc4d1	přední
husitské	husitský	k2eAgMnPc4d1	husitský
představitele	představitel	k1gMnPc4	představitel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1426	[number]	k4	1426
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
táboritů	táborita	k1gMnPc2	táborita
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
husitskými	husitský	k2eAgInPc7d1	husitský
svazy	svaz	k1gInPc7	svaz
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
většinu	většina	k1gFnSc4	většina
Čech	Čechy	k1gFnPc2	Čechy
pod	pod	k7c4	pod
husitskou	husitský	k2eAgFnSc4d1	husitská
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
kombinací	kombinace	k1gFnSc7	kombinace
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
a	a	k8xC	a
vojenského	vojenský	k2eAgInSc2d1	vojenský
nátlaku	nátlak	k1gInSc2	nátlak
(	(	kIx(	(
<g/>
rejsy	rejsa	k1gFnPc1	rejsa
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
německých	německý	k2eAgFnPc2d1	německá
a	a	k8xC	a
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
)	)	kIx)	)
přinutil	přinutit	k5eAaPmAgMnS	přinutit
západní	západní	k2eAgMnPc4d1	západní
katolické	katolický	k2eAgMnPc4d1	katolický
představitele	představitel	k1gMnPc4	představitel
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
programu	program	k1gInSc6	program
čtyř	čtyři	k4xCgInPc2	čtyři
pražských	pražský	k2eAgMnPc2d1	pražský
artikulů	artikul	k1gInPc2	artikul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
1433	[number]	k4	1433
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
delegace	delegace	k1gFnSc2	delegace
přizvané	přizvaný	k2eAgFnSc2d1	přizvaná
k	k	k7c3	k
rokování	rokování	k1gNnSc3	rokování
na	na	k7c6	na
basilejském	basilejský	k2eAgInSc6d1	basilejský
koncilu	koncil	k1gInSc6	koncil
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ještě	ještě	k9	ještě
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
před	před	k7c7	před
obleženou	obležený	k2eAgFnSc7d1	obležená
Plzní	Plzeň	k1gFnSc7	Plzeň
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
u	u	k7c2	u
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
politické	politický	k2eAgFnSc2d1	politická
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
husitského	husitský	k2eAgNnSc2d1	husitské
radikálního	radikální	k2eAgNnSc2d1	radikální
křídla	křídlo	k1gNnSc2	křídlo
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1434	[number]	k4	1434
<g/>
.	.	kIx.	.
</s>
<s>
Zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
následujícího	následující	k2eAgInSc2d1	následující
měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
čase	čas	k1gInSc6	čas
narození	narození	k1gNnSc2	narození
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
osobností	osobnost	k1gFnPc2	osobnost
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc4d1	historický
záznamy	záznam	k1gInPc4	záznam
nehovoří	hovořit	k5eNaImIp3nP	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
však	však	k9	však
část	část	k1gFnSc1	část
historických	historický	k2eAgMnPc2d1	historický
badatelů	badatel	k1gMnPc2	badatel
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1370	[number]	k4	1370
<g/>
–	–	k?	–
<g/>
1379	[number]	k4	1379
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
datum	datum	k1gNnSc4	datum
klade	klást	k5eAaImIp3nS	klást
teprve	teprve	k6eAd1	teprve
do	do	k7c2	do
let	léto	k1gNnPc2	léto
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
letech	let	k1gInPc6	let
dospívání	dospívání	k1gNnSc2	dospívání
také	také	k9	také
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
lakonické	lakonický	k2eAgFnPc1d1	lakonická
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
Jan	Jana	k1gFnPc2	Jana
z	z	k7c2	z
Cách	Cáchy	k1gFnPc2	Cáchy
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgMnSc1d1	bohatý
patricij	patricij	k1gMnSc1	patricij
původem	původ	k1gInSc7	původ
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
Cách	Cáchy	k1gFnPc2	Cáchy
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
jeho	jeho	k3xOp3gNnSc1	jeho
syn	syn	k1gMnSc1	syn
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
;	;	kIx,	;
oba	dva	k4xCgMnPc1	dva
mužové	muž	k1gMnPc1	muž
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nejbohatším	bohatý	k2eAgMnPc3d3	nejbohatší
pražským	pražský	k2eAgMnPc3d1	pražský
obchodníkům	obchodník	k1gMnPc3	obchodník
a	a	k8xC	a
váženým	vážený	k2eAgMnPc3d1	vážený
patricijům	patricij	k1gMnPc3	patricij
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
italského	italský	k2eAgMnSc2d1	italský
humanisty	humanista	k1gMnSc2	humanista
Enea	Enea	k1gMnSc1	Enea
Silvia	Silvia	k1gFnSc1	Silvia
Piccolominiho	Piccolomini	k1gMnSc2	Piccolomini
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
Prokopem	Prokop	k1gMnSc7	Prokop
Holým	Holý	k1gMnSc7	Holý
osobně	osobně	k6eAd1	osobně
setkal	setkat	k5eAaPmAgMnS	setkat
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
na	na	k7c6	na
basilejském	basilejský	k2eAgInSc6d1	basilejský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
relací	relace	k1gFnSc7	relace
je	být	k5eAaImIp3nS	být
věta	věta	k1gFnSc1	věta
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
polským	polský	k2eAgMnSc7d1	polský
kronikářem	kronikář	k1gMnSc7	kronikář
Janem	Jan	k1gMnSc7	Jan
Długoszem	Długosz	k1gMnSc7	Długosz
v	v	k7c6	v
literárním	literární	k2eAgInSc6d1	literární
díle	díl	k1gInSc6	díl
Annales	Annales	k1gInSc1	Annales
Poloniae	Polonia	k1gInPc1	Polonia
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rodinné	rodinný	k2eAgFnPc4d1	rodinná
vazby	vazba	k1gFnPc4	vazba
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
literáti	literát	k1gMnPc1	literát
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prokopův	Prokopův	k2eAgMnSc1d1	Prokopův
potenciální	potenciální	k2eAgMnSc1d1	potenciální
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
bratrem	bratr	k1gMnSc7	bratr
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
prve	prve	k6eAd1	prve
jmenovaného	jmenovaný	k2eAgMnSc2d1	jmenovaný
<g/>
,	,	kIx,	,
omylu	omyl	k1gInSc2	omyl
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dopustili	dopustit	k5eAaPmAgMnP	dopustit
v	v	k7c6	v
důvodu	důvod	k1gInSc6	důvod
osvojení	osvojení	k1gNnSc1	osvojení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Cách	Cáchy	k1gFnPc2	Cáchy
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
minimálně	minimálně	k6eAd1	minimálně
ještě	ještě	k6eAd1	ještě
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
vlastní	vlastní	k2eAgMnPc4d1	vlastní
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
mladý	mladý	k2eAgInSc1d1	mladý
Prokop	prokop	k1gInSc1	prokop
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
opatrovnictví	opatrovnictví	k1gNnSc2	opatrovnictví
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
po	po	k7c6	po
matčině	matčin	k2eAgNnSc6d1	matčino
ovdovění	ovdovění	k1gNnSc6	ovdovění
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
budoucí	budoucí	k2eAgMnSc1d1	budoucí
husitský	husitský	k2eAgMnSc1d1	husitský
politik	politik	k1gMnSc1	politik
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
velmi	velmi	k6eAd1	velmi
přínosným	přínosný	k2eAgNnSc7d1	přínosné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
díky	díky	k7c3	díky
strýcově	strýcův	k2eAgFnSc3d1	strýcova
bohatství	bohatství	k1gNnSc3	bohatství
a	a	k8xC	a
obchodním	obchodní	k2eAgInPc3d1	obchodní
stykům	styk	k1gInPc3	styk
osobně	osobně	k6eAd1	osobně
poznal	poznat	k5eAaPmAgMnS	poznat
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgInS	navštívit
chrám	chrám	k1gInSc1	chrám
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
v	v	k7c6	v
komfortu	komfort	k1gInSc6	komfort
a	a	k8xC	a
právě	právě	k6eAd1	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
etapě	etapa	k1gFnSc6	etapa
života	život	k1gInSc2	život
si	se	k3xPyFc3	se
osvojil	osvojit	k5eAaPmAgInS	osvojit
rozhled	rozhled	k1gInSc1	rozhled
a	a	k8xC	a
znalost	znalost	k1gFnSc1	znalost
života	život	k1gInSc2	život
vyšších	vysoký	k2eAgInPc2d2	vyšší
společenských	společenský	k2eAgInPc2d1	společenský
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
Prokopovo	Prokopův	k2eAgNnSc4d1	Prokopovo
pokročilejší	pokročilý	k2eAgNnSc4d2	pokročilejší
vzdělání	vzdělání	k1gNnSc4	vzdělání
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
postarala	postarat	k5eAaPmAgFnS	postarat
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
jej	on	k3xPp3gInSc4	on
dal	dát	k5eAaPmAgMnS	dát
strýc	strýc	k1gMnSc1	strýc
vysvětit	vysvětit	k5eAaPmF	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
řádu	řád	k1gInSc2	řád
minoritů	minorita	k1gMnPc2	minorita
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
však	však	k9	však
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnPc4	působení
za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Doba	doba	k1gFnSc1	doba
Žižkova	Žižkov	k1gInSc2	Žižkov
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1424	[number]	k4	1424
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
stal	stát	k5eAaPmAgMnS	stát
stoupencem	stoupenec	k1gMnSc7	stoupenec
učení	učení	k1gNnSc2	učení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc1d1	historický
dokumenty	dokument	k1gInPc1	dokument
nevypovídají	vypovídat	k5eNaPmIp3nP	vypovídat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
připomínán	připomínán	k2eAgInSc1d1	připomínán
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
rukopisu	rukopis	k1gInSc6	rukopis
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Břeňkem	Břeněk	k1gInSc7	Břeněk
Švihovským	Švihovský	k2eAgInSc7d1	Švihovský
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
a	a	k8xC	a
knězem	kněz	k1gMnSc7	kněz
Korandou	Koranda	k1gFnSc7	Koranda
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
z	z	k7c2	z
obležené	obležený	k2eAgFnSc2d1	obležená
Plzně	Plzeň	k1gFnSc2	Plzeň
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
Hradišti	Hradiště	k1gNnSc3	Hradiště
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
zakládala	zakládat	k5eAaImAgFnS	zakládat
na	na	k7c6	na
reálném	reálný	k2eAgInSc6d1	reálný
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyplývat	vyplývat	k5eAaImF	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
aktérů	aktér	k1gMnPc2	aktér
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
půlročního	půlroční	k2eAgNnSc2d1	půlroční
příměří	příměří	k1gNnSc2	příměří
mezi	mezi	k7c7	mezi
husity	husita	k1gMnPc7	husita
a	a	k8xC	a
královskou	královský	k2eAgFnSc7d1	královská
stranou	strana	k1gFnSc7	strana
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
opustili	opustit	k5eAaPmAgMnP	opustit
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
účastníkům	účastník	k1gMnPc3	účastník
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
událostech	událost	k1gFnPc6	událost
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgMnPc2d1	český
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
Táboře	Tábor	k1gInSc6	Tábor
nacházel	nacházet	k5eAaImAgMnS	nacházet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gMnSc4	on
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
jako	jako	k8xS	jako
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
roztržky	roztržka	k1gFnSc2	roztržka
mezi	mezi	k7c7	mezi
táborskými	táborský	k2eAgMnPc7d1	táborský
kněžími	kněz	k1gMnPc7	kněz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
historiků	historik	k1gMnPc2	historik
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
Prokop	prokop	k1gInSc1	prokop
Holý	holý	k2eAgInSc1d1	holý
určitou	určitý	k2eAgFnSc7d1	určitá
měrou	míra	k1gFnSc7wR	míra
inklinoval	inklinovat	k5eAaImAgInS	inklinovat
k	k	k7c3	k
pikartství	pikartství	k1gNnSc3	pikartství
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
oficiálním	oficiální	k2eAgInSc6d1	oficiální
postoji	postoj	k1gInSc6	postoj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
k	k	k7c3	k
eucharistii	eucharistie	k1gFnSc3	eucharistie
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
o	o	k7c6	o
neměnnosti	neměnnost	k1gFnSc6	neměnnost
podstaty	podstata	k1gFnSc2	podstata
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
při	při	k7c6	při
posvěcování	posvěcování	k1gNnSc6	posvěcování
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
umírněné	umírněný	k2eAgFnSc2d1	umírněná
větve	větev	k1gFnSc2	větev
pikartů	pikart	k1gMnPc2	pikart
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	let	k1gInSc1	let
dospěl	dochvít	k5eAaPmAgInS	dochvít
ke	k	k7c3	k
stanovisku	stanovisko	k1gNnSc3	stanovisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
svátost	svátost	k1gFnSc1	svátost
oltářní	oltářní	k2eAgFnSc1d1	oltářní
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
chlebem	chléb	k1gInSc7	chléb
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
Kristus	Kristus	k1gMnSc1	Kristus
přítomen	přítomen	k2eAgMnSc1d1	přítomen
jen	jen	k9	jen
obrazně	obrazně	k6eAd1	obrazně
<g/>
,	,	kIx,	,
posvátně	posvátně	k6eAd1	posvátně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
podporují	podporovat	k5eAaImIp3nP	podporovat
i	i	k9	i
události	událost	k1gFnPc1	událost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1433	[number]	k4	1433
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hájil	hájit	k5eAaImAgMnS	hájit
remanenční	remanenční	k2eAgNnSc4d1	remanenční
učení	učení	k1gNnSc4	učení
před	před	k7c7	před
účastníky	účastník	k1gMnPc7	účastník
basilejského	basilejský	k2eAgInSc2d1	basilejský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
za	za	k7c2	za
neznámých	známý	k2eNgFnPc2d1	neznámá
okolností	okolnost	k1gFnPc2	okolnost
octl	octnout	k5eAaPmAgInS	octnout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
pikartství	pikartství	k1gNnSc2	pikartství
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
myšlenky	myšlenka	k1gFnPc1	myšlenka
mu	on	k3xPp3gNnSc3	on
vynesly	vynést	k5eAaPmAgFnP	vynést
několikadenní	několikadenní	k2eAgNnSc4d1	několikadenní
vězení	vězení	k1gNnSc4	vězení
pod	pod	k7c7	pod
Staroměstskou	staroměstský	k2eAgFnSc7d1	Staroměstská
radnicí	radnice	k1gFnSc7	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
byl	být	k5eAaImAgInS	být
však	však	k9	však
vysvobozen	vysvobodit	k5eAaPmNgInS	vysvobodit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zásluhou	zásluhou	k7c2	zásluhou
Jana	Jan	k1gMnSc2	Jan
Želivského	želivský	k2eAgMnSc2d1	želivský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
novoměstskou	novoměstský	k2eAgFnSc7d1	Novoměstská
obcí	obec	k1gFnSc7	obec
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
konšely	konšel	k1gMnPc4	konšel
obou	dva	k4xCgFnPc6	dva
pražských	pražský	k2eAgFnPc2d1	Pražská
radnic	radnice	k1gFnPc2	radnice
a	a	k8xC	a
obě	dva	k4xCgNnPc4	dva
Města	město	k1gNnPc4	město
posléze	posléze	k6eAd1	posléze
spojil	spojit	k5eAaPmAgMnS	spojit
pod	pod	k7c4	pod
jednu	jeden	k4xCgFnSc4	jeden
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
metropoli	metropol	k1gFnSc6	metropol
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Želivským	želivský	k2eAgMnSc7d1	želivský
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
postupoval	postupovat	k5eAaImAgMnS	postupovat
proti	proti	k7c3	proti
Jakoubkovi	Jakoubek	k1gMnSc3	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
pojilo	pojit	k5eAaImAgNnS	pojit
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
určitou	určitý	k2eAgFnSc4d1	určitá
záštitu	záštita	k1gFnSc4	záštita
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
působit	působit	k5eAaImF	působit
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
postoji	postoj	k1gInPc7	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Želivského	želivský	k2eAgInSc2d1	želivský
poprava	poprava	k1gFnSc1	poprava
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1422	[number]	k4	1422
však	však	k9	však
naprosto	naprosto	k6eAd1	naprosto
změnila	změnit	k5eAaPmAgFnS	změnit
pražské	pražský	k2eAgInPc4d1	pražský
poměry	poměr	k1gInPc4	poměr
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1423	[number]	k4	1423
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
opět	opět	k6eAd1	opět
nalézal	nalézat	k5eAaImAgMnS	nalézat
u	u	k7c2	u
táborského	táborský	k2eAgNnSc2d1	táborské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
u	u	k7c2	u
Konopiště	Konopiště	k1gNnSc2	Konopiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
urovnání	urovnání	k1gNnSc3	urovnání
roztržky	roztržka	k1gFnSc2	roztržka
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Táborem	Tábor	k1gInSc7	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
celého	celý	k2eAgNnSc2d1	celé
rokování	rokování	k1gNnSc2	rokování
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
soustředil	soustředit	k5eAaPmAgInS	soustředit
kolem	kolem	k7c2	kolem
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vedli	vést	k5eAaImAgMnP	vést
hádky	hádka	k1gFnPc4	hádka
o	o	k7c6	o
sporných	sporný	k2eAgFnPc6d1	sporná
věroučných	věroučný	k2eAgFnPc6d1	věroučná
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
řádky	řádek	k1gInPc1	řádek
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Žižkovou	Žižkův	k2eAgFnSc7d1	Žižkova
smrtí	smrt	k1gFnSc7	smrt
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgMnPc4d1	přední
táborské	táborští	k1gMnPc4	táborští
kazatele	kazatel	k1gMnSc4	kazatel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jménem	jméno	k1gNnSc7	jméno
táborského	táborský	k2eAgNnSc2d1	táborské
kněžstva	kněžstvo	k1gNnSc2	kněžstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pražských	pražský	k2eAgMnPc2d1	pražský
duchovních	duchovní	k1gMnPc2	duchovní
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
neoblékalo	oblékat	k5eNaImAgNnS	oblékat
do	do	k7c2	do
ornátů	ornát	k1gMnPc2	ornát
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
najevo	najevo	k6eAd1	najevo
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vůli	vůle	k1gFnSc4	vůle
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
a	a	k8xC	a
projevil	projevit	k5eAaPmAgInS	projevit
odtažitý	odtažitý	k2eAgInSc4d1	odtažitý
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
malicherným	malicherný	k2eAgFnPc3d1	malicherná
věroučným	věroučný	k2eAgFnPc3d1	věroučná
třenicím	třenice	k1gFnPc3	třenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
husitů	husita	k1gMnPc2	husita
(	(	kIx(	(
<g/>
1426	[number]	k4	1426
<g/>
–	–	k?	–
<g/>
1434	[number]	k4	1434
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1426	[number]	k4	1426
====	====	k?	====
</s>
</p>
<p>
<s>
Vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
se	se	k3xPyFc4	se
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1424	[number]	k4	1424
a	a	k8xC	a
1426	[number]	k4	1426
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pouštět	pouštět	k5eAaImF	pouštět
se	se	k3xPyFc4	se
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
nasnadě	nasnadě	k6eAd1	nasnadě
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
musel	muset	k5eAaImAgInS	muset
úzce	úzko	k6eAd1	úzko
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
táborské	táborský	k2eAgFnSc2d1	táborská
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
patrně	patrně	k6eAd1	patrně
získal	získat	k5eAaPmAgMnS	získat
značnou	značný	k2eAgFnSc4d1	značná
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
připomínáno	připomínat	k5eAaImNgNnS	připomínat
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgMnPc7d1	hlavní
aktéry	aktér	k1gMnPc7	aktér
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
severočeským	severočeský	k2eAgFnPc3d1	Severočeská
enklávám	enkláva	k1gFnPc3	enkláva
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
saského	saský	k2eAgMnSc2d1	saský
knížete	kníže	k1gMnSc2	kníže
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Bojovného	bojovný	k2eAgMnSc2d1	bojovný
a	a	k8xC	a
bitvy	bitva	k1gFnPc4	bitva
u	u	k7c2	u
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
vojáci	voják	k1gMnPc1	voják
všech	všecek	k3xTgInPc2	všecek
husitských	husitský	k2eAgInPc2d1	husitský
svazů	svaz	k1gInPc2	svaz
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
strůjcem	strůjce	k1gMnSc7	strůjce
tohoto	tento	k3xDgInSc2	tento
federativního	federativní	k2eAgInSc2d1	federativní
společenství	společenství	k1gNnPc2	společenství
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
vedle	vedle	k7c2	vedle
táborského	táborský	k2eAgMnSc2d1	táborský
vojenského	vojenský	k2eAgMnSc2d1	vojenský
velitele	velitel	k1gMnSc2	velitel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
z	z	k7c2	z
Bukoviny	Bukovina	k1gFnSc2	Bukovina
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc2	správce
táborské	táborský	k2eAgFnSc2d1	táborská
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
spolehlivého	spolehlivý	k2eAgInSc2d1	spolehlivý
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
byl	být	k5eAaImAgInS	být
evangelický	evangelický	k2eAgMnSc1d1	evangelický
historik	historik	k1gMnSc1	historik
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
Theobald	Theobald	k1gMnSc1	Theobald
(	(	kIx(	(
<g/>
Hussitenkrieg	Hussitenkrieg	k1gMnSc1	Hussitenkrieg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prý	prý	k9	prý
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
táborského	táborský	k2eAgNnSc2d1	táborské
vojska	vojsko	k1gNnSc2	vojsko
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
Ústí	ústí	k1gNnSc3	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
zpustošil	zpustošit	k5eAaPmAgInS	zpustošit
Třebenice	Třebenice	k1gFnPc4	Třebenice
<g/>
,	,	kIx,	,
Duchcov	Duchcov	k1gInSc1	Duchcov
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
a	a	k8xC	a
městečko	městečko	k1gNnSc1	městečko
Krupku	krupka	k1gFnSc4	krupka
<g/>
;	;	kIx,	;
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
informaci	informace	k1gFnSc3	informace
však	však	k9	však
část	část	k1gFnSc4	část
historické	historický	k2eAgFnSc2d1	historická
obce	obec	k1gFnSc2	obec
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
poněkud	poněkud	k6eAd1	poněkud
zdrženlivě	zdrženlivě	k6eAd1	zdrženlivě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ústeckém	ústecký	k2eAgNnSc6d1	ústecké
vítězství	vítězství	k1gNnSc6	vítězství
táborité	táborita	k1gMnPc1	táborita
a	a	k8xC	a
sirotci	sirotek	k1gMnPc1	sirotek
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
,	,	kIx,	,
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Žižkova	Žižkov	k1gInSc2	Žižkov
spojence	spojenec	k1gMnSc4	spojenec
Hynka	Hynek	k1gMnSc4	Hynek
Bočka	Boček	k1gMnSc4	Boček
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
katolickými	katolický	k2eAgMnPc7d1	katolický
pány	pan	k1gMnPc7	pan
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
však	však	k9	však
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
odhodlanou	odhodlaný	k2eAgFnSc4d1	odhodlaná
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
až	až	k9	až
do	do	k7c2	do
podzimních	podzimní	k2eAgInPc2d1	podzimní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
zeť	zeť	k1gMnSc1	zeť
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
vévoda	vévoda	k1gMnSc1	vévoda
Albrecht	Albrecht	k1gMnSc1	Albrecht
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
patrně	patrně	k6eAd1	patrně
obě	dva	k4xCgFnPc1	dva
husitské	husitský	k2eAgFnPc1d1	husitská
obce	obec	k1gFnPc1	obec
uznaly	uznat	k5eAaPmAgFnP	uznat
za	za	k7c2	za
formálního	formální	k2eAgMnSc2d1	formální
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
,	,	kIx,	,
upustil	upustit	k5eAaPmAgMnS	upustit
od	od	k7c2	od
akcí	akce	k1gFnPc2	akce
proti	proti	k7c3	proti
Poděbradům	Poděbrady	k1gInPc3	Poděbrady
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
a	a	k8xC	a
rychlým	rychlý	k2eAgInSc7d1	rychlý
pochodem	pochod	k1gInSc7	pochod
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vítězně	vítězně	k6eAd1	vítězně
udeřil	udeřit	k5eAaPmAgInS	udeřit
na	na	k7c4	na
Albrechtovy	Albrechtův	k2eAgFnPc4d1	Albrechtova
síly	síla	k1gFnPc4	síla
u	u	k7c2	u
obléhané	obléhaný	k2eAgFnSc2d1	obléhaná
Břeclavi	Břeclav	k1gFnSc2	Břeclav
a	a	k8xC	a
dodal	dodat	k5eAaPmAgInS	dodat
posádce	posádka	k1gFnSc3	posádka
vyčerpané	vyčerpaný	k2eAgFnSc2d1	vyčerpaná
tříměsíčním	tříměsíční	k2eAgNnSc7d1	tříměsíční
obklíčením	obklíčení	k1gNnSc7	obklíčení
zásoby	zásoba	k1gFnSc2	zásoba
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
pití	pití	k1gNnSc2	pití
i	i	k8xC	i
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
se	se	k3xPyFc4	se
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
neodhodlal	odhodlat	k5eNaPmAgMnS	odhodlat
dál	daleko	k6eAd2	daleko
stavět	stavět	k5eAaImF	stavět
husitům	husita	k1gMnPc3	husita
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
domovské	domovský	k2eAgNnSc4d1	domovské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1427	[number]	k4	1427
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
1427	[number]	k4	1427
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
využil	využít	k5eAaPmAgMnS	využít
příznivé	příznivý	k2eAgFnPc4d1	příznivá
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
ofenzivních	ofenzivní	k2eAgFnPc6d1	ofenzivní
vojenských	vojenský	k2eAgFnPc6d1	vojenská
operacích	operace	k1gFnPc6	operace
na	na	k7c6	na
rakouském	rakouský	k2eAgNnSc6d1	rakouské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
jeho	jeho	k3xOp3gInSc7	jeho
vojáci	voják	k1gMnPc1	voják
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
město	město	k1gNnSc4	město
Světlá	světlat	k5eAaImIp3nS	světlat
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
však	však	k9	však
byli	být	k5eAaImAgMnP	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
napadeni	napadnout	k5eAaPmNgMnP	napadnout
oddíly	oddíl	k1gInPc1	oddíl
pod	pod	k7c7	pod
korouhví	korouhev	k1gFnSc7	korouhev
Albrechta	Albrecht	k1gMnSc2	Albrecht
Rakouského	rakouský	k2eAgMnSc2d1	rakouský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhodinové	čtyřhodinový	k2eAgFnSc6d1	čtyřhodinová
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Světlé	světlý	k2eAgFnSc2d1	světlá
husité	husita	k1gMnPc1	husita
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
rozhodující	rozhodující	k2eAgNnSc4d1	rozhodující
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
současníků	současník	k1gMnPc2	současník
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
významný	významný	k2eAgInSc1d1	významný
nejen	nejen	k6eAd1	nejen
svým	svůj	k3xOyFgInSc7	svůj
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
novou	nový	k2eAgFnSc4d1	nová
kapitolu	kapitola	k1gFnSc4	kapitola
husitského	husitský	k2eAgNnSc2d1	husitské
válečnictví	válečnictví	k1gNnSc2	válečnictví
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
prvního	první	k4xOgInSc2	první
výrazného	výrazný	k2eAgInSc2d1	výrazný
úspěchu	úspěch	k1gInSc2	úspěch
na	na	k7c6	na
zahraničním	zahraniční	k2eAgNnSc6d1	zahraniční
území	území	k1gNnSc6	území
a	a	k8xC	a
zasadili	zasadit	k5eAaPmAgMnP	zasadit
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
ránu	rána	k1gFnSc4	rána
jednomu	jeden	k4xCgMnSc3	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
nejnebezpečnějších	bezpečný	k2eNgMnPc2d3	nejnebezpečnější
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
i	i	k9	i
rejsu	rejsa	k1gFnSc4	rejsa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
zamířila	zamířit	k5eAaPmAgFnS	zamířit
do	do	k7c2	do
Lužice	Lužice	k1gFnSc2	Lužice
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Táborité	táborita	k1gMnPc1	táborita
a	a	k8xC	a
sirotci	sirotek	k1gMnPc1	sirotek
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
oblouku	oblouk	k1gInSc6	oblouk
protáhli	protáhnout	k5eAaPmAgMnP	protáhnout
kolem	kolem	k7c2	kolem
Žitavy	Žitava	k1gFnSc2	Žitava
na	na	k7c4	na
Lubno	Lubno	k1gNnSc4	Lubno
<g/>
,	,	kIx,	,
Lemberg	Lemberg	k1gInSc1	Lemberg
a	a	k8xC	a
Javor	javor	k1gInSc1	javor
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
přes	přes	k7c4	přes
Trutnov	Trutnov	k1gInSc4	Trutnov
a	a	k8xC	a
Náchod	Náchod	k1gInSc4	Náchod
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
táborské	táborský	k2eAgNnSc1d1	táborské
polní	polní	k2eAgNnSc1d1	polní
vojsko	vojsko	k1gNnSc1	vojsko
cestou	cestou	k7c2	cestou
dobylo	dobýt	k5eAaPmAgNnS	dobýt
hrad	hrad	k1gInSc4	hrad
Žleby	žleb	k1gInPc1	žleb
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
spanilá	spanilý	k2eAgFnSc1d1	spanilá
jízda	jízda	k1gFnSc1	jízda
nejenže	nejenže	k6eAd1	nejenže
přinesla	přinést	k5eAaPmAgFnS	přinést
husitům	husita	k1gMnPc3	husita
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kořist	kořist	k1gFnSc4	kořist
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
,	,	kIx,	,
stříbře	stříbro	k1gNnSc6	stříbro
a	a	k8xC	a
dobytku	dobytek	k1gInSc6	dobytek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
zcela	zcela	k6eAd1	zcela
narušila	narušit	k5eAaPmAgFnS	narušit
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
nástupní	nástupní	k2eAgFnPc4d1	nástupní
prostory	prostora	k1gFnPc4	prostora
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
organizoval	organizovat	k5eAaBmAgMnS	organizovat
třetí	třetí	k4xOgFnSc4	třetí
protihusitskou	protihusitský	k2eAgFnSc4d1	protihusitská
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
síly	síla	k1gFnPc1	síla
protivníka	protivník	k1gMnSc2	protivník
shromažďovaly	shromažďovat	k5eAaImAgInP	shromažďovat
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
vojenské	vojenský	k2eAgInPc4d1	vojenský
svazy	svaz	k1gInPc4	svaz
mobilizovat	mobilizovat	k5eAaBmF	mobilizovat
svoji	svůj	k3xOyFgFnSc4	svůj
brannou	branný	k2eAgFnSc4d1	Branná
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příslušníci	příslušník	k1gMnPc1	příslušník
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
uznali	uznat	k5eAaPmAgMnP	uznat
svým	svůj	k3xOyFgMnSc7	svůj
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
pražané	pražan	k1gMnPc1	pražan
<g/>
,	,	kIx,	,
táboři	tábor	k1gMnPc1	tábor
<g/>
,	,	kIx,	,
sirotci	sirotek	k1gMnPc1	sirotek
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
příslušníci	příslušník	k1gMnPc1	příslušník
kališnické	kališnický	k2eAgFnSc2d1	kališnická
šlechty	šlechta	k1gFnSc2	šlechta
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
ke	k	k7c3	k
Karlštejnu	Karlštejn	k1gInSc3	Karlštejn
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
strategické	strategický	k2eAgFnSc2d1	strategická
pozice	pozice	k1gFnSc2	pozice
se	se	k3xPyFc4	se
dali	dát	k5eAaPmAgMnP	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Rokycany	Rokycany	k1gInPc4	Rokycany
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
obležení	obležení	k1gNnSc6	obležení
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
připravovanému	připravovaný	k2eAgNnSc3d1	připravované
střetnutí	střetnutí	k1gNnSc3	střetnutí
však	však	k9	však
vůbec	vůbec	k9	vůbec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
katoličtí	katolický	k2eAgMnPc1d1	katolický
vojáci	voják	k1gMnPc1	voják
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
jak	jak	k6eAd1	jak
od	od	k7c2	od
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
od	od	k7c2	od
Tachova	Tachov	k1gInSc2	Tachov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
část	část	k1gFnSc1	část
ustupujících	ustupující	k2eAgMnPc2d1	ustupující
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
kardinál	kardinál	k1gMnSc1	kardinál
Jindřich	Jindřich	k1gMnSc1	Jindřich
Beaufort	Beaufort	k1gInSc1	Beaufort
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Tachova	Tachov	k1gInSc2	Tachov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Prokopa	Prokop	k1gMnSc4	Prokop
Holého	Holý	k1gMnSc4	Holý
osobně	osobně	k6eAd1	osobně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vítězství	vítězství	k1gNnSc3	vítězství
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
složité	složitý	k2eAgFnSc6d1	složitá
vnitropolitické	vnitropolitický	k2eAgFnSc6d1	vnitropolitická
situaci	situace	k1gFnSc6	situace
podařilo	podařit	k5eAaPmAgNnS	podařit
stmelit	stmelit	k5eAaPmF	stmelit
všechny	všechen	k3xTgInPc4	všechen
husitské	husitský	k2eAgInPc4d1	husitský
svazy	svaz	k1gInPc4	svaz
pod	pod	k7c4	pod
jednu	jeden	k4xCgFnSc4	jeden
korouhev	korouhev	k1gFnSc4	korouhev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopným	schopný	k2eAgMnSc7d1	schopný
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
organizační	organizační	k2eAgFnPc4d1	organizační
a	a	k8xC	a
politické	politický	k2eAgFnPc4d1	politická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
značně	značně	k6eAd1	značně
pozvedlo	pozvednout	k5eAaPmAgNnS	pozvednout
jeho	jeho	k3xOp3gFnSc4	jeho
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
upevnil	upevnit	k5eAaPmAgMnS	upevnit
i	i	k9	i
dobytím	dobytí	k1gNnSc7	dobytí
Tachova	Tachov	k1gInSc2	Tachov
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
dále	daleko	k6eAd2	daleko
stupňoval	stupňovat	k5eAaImAgMnS	stupňovat
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
domácí	domácí	k2eAgNnSc4d1	domácí
katolické	katolický	k2eAgNnSc4d1	katolické
panstvo	panstvo	k1gNnSc4	panstvo
a	a	k8xC	a
nasměřoval	nasměřovat	k5eAaImAgMnS	nasměřovat
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Plzni	Plzeň	k1gFnSc3	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
podnik	podnik	k1gInSc4	podnik
však	však	k9	však
disponoval	disponovat	k5eAaBmAgMnS	disponovat
pouze	pouze	k6eAd1	pouze
silami	síla	k1gFnPc7	síla
sirotků	sirotek	k1gMnPc2	sirotek
a	a	k8xC	a
táborů	tábor	k1gMnPc2	tábor
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
patrně	patrně	k6eAd1	patrně
dobře	dobře	k6eAd1	dobře
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
obléhání	obléhání	k1gNnSc1	obléhání
dobře	dobře	k6eAd1	dobře
zásobeného	zásobený	k2eAgNnSc2d1	zásobené
města	město	k1gNnSc2	město
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
značně	značně	k6eAd1	značně
protáhnout	protáhnout	k5eAaPmF	protáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
proto	proto	k8xC	proto
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
protivníka	protivník	k1gMnSc2	protivník
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
landfrýdu	landfrýd	k1gInSc2	landfrýd
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mělo	mít	k5eAaImAgNnS	mít
trvat	trvat	k5eAaImF	trvat
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1428	[number]	k4	1428
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
svaz	svaz	k1gInSc1	svaz
západočeské	západočeský	k2eAgFnSc2d1	Západočeská
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
jednání	jednání	k1gNnSc2	jednání
katolických	katolický	k2eAgMnPc2d1	katolický
a	a	k8xC	a
husitských	husitský	k2eAgMnPc2d1	husitský
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
se	se	k3xPyFc4	se
připravovalo	připravovat	k5eAaImAgNnS	připravovat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
a	a	k8xC	a
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Žebráku	Žebrák	k1gInSc2	Žebrák
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
v	v	k7c6	v
západočeské	západočeský	k2eAgFnSc6d1	Západočeská
metropoli	metropol	k1gFnSc6	metropol
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
pokusili	pokusit	k5eAaPmAgMnP	pokusit
neúspěšně	úspěšně	k6eNd1	úspěšně
převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
příslušníci	příslušník	k1gMnPc1	příslušník
husitské	husitský	k2eAgFnSc2d1	husitská
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Hynkem	Hynek	k1gMnSc7	Hynek
z	z	k7c2	z
Kolštejna	Kolštejn	k1gInSc2	Kolštejn
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Smiřickým	smiřický	k2eAgMnSc7d1	smiřický
<g/>
.	.	kIx.	.
</s>
<s>
Sirotci	Sirotek	k1gMnPc1	Sirotek
a	a	k8xC	a
táborité	táborita	k1gMnPc1	táborita
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
Království	království	k1gNnSc1	království
českého	český	k2eAgNnSc2d1	české
o	o	k7c6	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
je	být	k5eAaImIp3nS	být
zažehnáno	zažehnat	k5eAaPmNgNnS	zažehnat
<g/>
,	,	kIx,	,
obrátili	obrátit	k5eAaPmAgMnP	obrátit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
posílené	posílená	k1gFnPc4	posílená
o	o	k7c4	o
pražskou	pražský	k2eAgFnSc4d1	Pražská
hotovost	hotovost	k1gFnSc4	hotovost
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Kolínu	Kolín	k1gInSc3	Kolín
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
spiklencům	spiklenec	k1gMnPc3	spiklenec
domovskou	domovský	k2eAgFnSc4d1	domovská
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
pod	pod	k7c7	pod
Prokopovým	Prokopův	k2eAgNnSc7d1	Prokopovo
přímým	přímý	k2eAgNnSc7d1	přímé
velením	velení	k1gNnSc7	velení
se	se	k3xPyFc4	se
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
velitel	velitel	k1gMnSc1	velitel
obránců	obránce	k1gMnPc2	obránce
Diviš	Diviš	k1gMnSc1	Diviš
Bořek	Bořek	k1gMnSc1	Bořek
z	z	k7c2	z
Miletínka	Miletínko	k1gNnSc2	Miletínko
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
místní	místní	k2eAgFnSc2d1	místní
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
opozice	opozice	k1gFnSc2	opozice
přinucen	přinutit	k5eAaPmNgMnS	přinutit
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1428	[number]	k4	1428
====	====	k?	====
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Kolína	Kolín	k1gInSc2	Kolín
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
rejsu	rejsa	k1gFnSc4	rejsa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
směřovala	směřovat	k5eAaImAgFnS	směřovat
přes	přes	k7c4	přes
Moravu	Morava	k1gFnSc4	Morava
na	na	k7c6	na
území	území	k1gNnSc6	území
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
příslušníků	příslušník	k1gMnPc2	příslušník
polních	polní	k2eAgFnPc2d1	polní
obcí	obec	k1gFnPc2	obec
sirotčího	sirotčí	k2eAgInSc2d1	sirotčí
a	a	k8xC	a
táborského	táborský	k2eAgInSc2d1	táborský
svazu	svaz	k1gInSc2	svaz
účastnila	účastnit	k5eAaImAgFnS	účastnit
také	také	k9	také
část	část	k1gFnSc1	část
pražanů	pražan	k1gMnPc2	pražan
a	a	k8xC	a
husité	husita	k1gMnPc1	husita
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
uherských	uherský	k2eAgFnPc2d1	uherská
hranic	hranice	k1gFnPc2	hranice
postupovala	postupovat	k5eAaImAgFnS	postupovat
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
na	na	k7c4	na
Senici	Senice	k1gFnSc4	Senice
<g/>
,	,	kIx,	,
Bíňovice	Bíňovice	k1gFnSc1	Bíňovice
<g/>
,	,	kIx,	,
Pezinok	Pezinok	k1gInSc1	Pezinok
a	a	k8xC	a
Svätý	Svätý	k2eAgInSc1d1	Svätý
Jur	jura	k1gFnPc2	jura
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
její	její	k3xOp3gNnPc4	její
příslušníci	příslušník	k1gMnPc1	příslušník
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
Prešpurku	Prešpurku	k?	Prešpurku
a	a	k8xC	a
po	po	k7c6	po
vyplenění	vyplenění	k1gNnSc6	vyplenění
předměstí	předměstí	k1gNnSc2	předměstí
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
moravským	moravský	k2eAgFnPc3d1	Moravská
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
dorazili	dorazit	k5eAaPmAgMnP	dorazit
obtíženi	obtížit	k5eAaPmNgMnP	obtížit
kořistí	kořist	k1gFnSc7	kořist
přes	přes	k7c4	přes
Šintavu	Šintava	k1gFnSc4	Šintava
a	a	k8xC	a
Nové	Nové	k2eAgNnSc4d1	Nové
Mesto	Mesto	k1gNnSc4	Mesto
nad	nad	k7c4	nad
Váhom	Váhom	k1gInSc4	Váhom
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
celé	celý	k2eAgFnSc2d1	celá
akce	akce	k1gFnSc2	akce
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
uštědřit	uštědřit	k5eAaPmF	uštědřit
účinnou	účinný	k2eAgFnSc4d1	účinná
lekci	lekce	k1gFnSc4	lekce
uherským	uherský	k2eAgMnSc7d1	uherský
šlechticům	šlechtic	k1gMnPc3	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyprázdnění	vyprázdnění	k1gNnSc6	vyprázdnění
vozů	vůz	k1gInPc2	vůz
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
zamířily	zamířit	k5eAaPmAgInP	zamířit
oddíly	oddíl	k1gInPc1	oddíl
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
přes	přes	k7c4	přes
hrad	hrad	k1gInSc4	hrad
Odry	Odra	k1gFnSc2	Odra
ke	k	k7c3	k
slezskému	slezský	k2eAgNnSc3d1	Slezské
území	území	k1gNnSc3	území
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
připojily	připojit	k5eAaPmAgInP	připojit
polské	polský	k2eAgInPc1d1	polský
oddíly	oddíl	k1gInPc1	oddíl
šlechtice	šlechtic	k1gMnSc2	šlechtic
Dobeslava	Dobeslav	k1gMnSc2	Dobeslav
Puchaly	Puchal	k1gMnPc7	Puchal
a	a	k8xC	a
muži	muž	k1gMnPc7	muž
hejtmana	hejtman	k1gMnSc2	hejtman
táborské	táborský	k2eAgFnPc1d1	táborská
posádky	posádka	k1gFnPc1	posádka
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
knížete	kníže	k1gMnSc2	kníže
Fedora	Fedor	k1gMnSc2	Fedor
z	z	k7c2	z
Ostrogu	Ostrog	k1gInSc2	Ostrog
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
tažení	tažení	k1gNnSc2	tažení
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
husitů	husita	k1gMnPc2	husita
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
dostaly	dostat	k5eAaPmAgFnP	dostat
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Hukvaldy	Hukvaldy	k1gInPc1	Hukvaldy
<g/>
,	,	kIx,	,
Osoblaha	Osoblaha	k1gFnSc1	Osoblaha
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
Hlohov	Hlohov	k1gInSc1	Hlohov
i	i	k8xC	i
jiné	jiný	k2eAgFnSc2d1	jiná
lokality	lokalita	k1gFnSc2	lokalita
a	a	k8xC	a
příměří	příměří	k1gNnSc2	příměří
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Bolek	Bolek	k1gMnSc1	Bolek
Opolský	opolský	k2eAgMnSc1d1	opolský
a	a	k8xC	a
opavský	opavský	k2eAgMnSc1d1	opavský
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
vážnějšímu	vážní	k2eAgInSc3d2	vážnější
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
protivníkem	protivník	k1gMnSc7	protivník
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
dostali	dostat	k5eAaPmAgMnP	dostat
teprve	teprve	k6eAd1	teprve
u	u	k7c2	u
města	město	k1gNnSc2	město
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
vojsko	vojsko	k1gNnSc4	vojsko
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
biskup	biskup	k1gMnSc1	biskup
Konrád	Konrád	k1gMnSc1	Konrád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
velením	velení	k1gNnSc7	velení
nad	nad	k7c7	nad
shromážděnými	shromážděný	k2eAgInPc7d1	shromážděný
oddíly	oddíl	k1gInPc7	oddíl
pověřil	pověřit	k5eAaPmAgMnS	pověřit
kladského	kladský	k2eAgMnSc4d1	kladský
hejtmana	hejtman	k1gMnSc4	hejtman
Půtu	Půta	k1gMnSc4	Půta
z	z	k7c2	z
Častolovic	Častolovice	k1gFnPc2	Častolovice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Nisy	Nisa	k1gFnSc2	Nisa
došlo	dojít	k5eAaPmAgNnS	dojít
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
husité	husita	k1gMnPc1	husita
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
hladkého	hladký	k2eAgInSc2d1	hladký
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
samotné	samotný	k2eAgNnSc1d1	samotné
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
prvním	první	k4xOgMnPc3	první
útokem	útok	k1gInSc7	útok
dobýt	dobýt	k5eAaPmF	dobýt
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
obléháním	obléhání	k1gNnSc7	obléhání
se	se	k3xPyFc4	se
však	však	k9	však
nezdržovali	zdržovat	k5eNaImAgMnP	zdržovat
a	a	k8xC	a
bleskovými	bleskový	k2eAgInPc7d1	bleskový
výpady	výpad	k1gInPc7	výpad
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
poměrně	poměrně	k6eAd1	poměrně
široké	široký	k2eAgNnSc4d1	široké
slezské	slezský	k2eAgNnSc4d1	Slezské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
obsadili	obsadit	k5eAaPmAgMnP	obsadit
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
dali	dát	k5eAaPmAgMnP	dát
namísto	namísto	k7c2	namísto
obrany	obrana	k1gFnSc2	obrana
přednost	přednost	k1gFnSc1	přednost
útěku	útěk	k1gInSc3	útěk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přijali	přijmout	k5eAaPmAgMnP	přijmout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
finanční	finanční	k2eAgFnPc4d1	finanční
částky	částka	k1gFnPc4	částka
od	od	k7c2	od
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zmírnit	zmírnit	k5eAaPmF	zmírnit
důsledky	důsledek	k1gInPc4	důsledek
vpádu	vpád	k1gInSc2	vpád
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
nabídkou	nabídka	k1gFnSc7	nabídka
vysokého	vysoký	k2eAgNnSc2d1	vysoké
výkupného	výkupné	k1gNnSc2	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
husitské	husitský	k2eAgNnSc1d1	husitské
vojsko	vojsko	k1gNnSc1	vojsko
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
k	k	k7c3	k
Vratislavi	Vratislav	k1gFnSc3	Vratislav
<g/>
,	,	kIx,	,
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
předměstí	předměstí	k1gNnSc1	předměstí
a	a	k8xC	a
zamířilo	zamířit	k5eAaPmAgNnS	zamířit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
domovské	domovský	k2eAgNnSc4d1	domovské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
<g/>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
sněmu	sněm	k1gInSc6	sněm
husitských	husitský	k2eAgMnPc2d1	husitský
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gNnSc3	jehož
svolání	svolání	k1gNnSc3	svolání
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
neznámém	známý	k2eNgNnSc6d1	neznámé
místě	místo	k1gNnSc6	místo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
hrozící	hrozící	k2eAgFnSc2d1	hrozící
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
ohlášena	ohlášet	k5eAaImNgFnS	ohlášet
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
další	další	k2eAgInSc4d1	další
vpád	vpád	k1gInSc4	vpád
na	na	k7c4	na
rakouské	rakouský	k2eAgNnSc4d1	rakouské
území	území	k1gNnSc4	území
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
táborské	táborský	k2eAgNnSc1d1	táborské
vojsko	vojsko	k1gNnSc1	vojsko
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
Bechyni	Bechyně	k1gFnSc4	Bechyně
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
pevnost	pevnost	k1gFnSc1	pevnost
důkladně	důkladně	k6eAd1	důkladně
obklíčena	obklíčit	k5eAaPmNgFnS	obklíčit
a	a	k8xC	a
soustavně	soustavně	k6eAd1	soustavně
odstřelována	odstřelován	k2eAgFnSc1d1	odstřelována
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
složila	složit	k5eAaPmAgFnS	složit
zbraně	zbraň	k1gFnPc4	zbraň
až	až	k6eAd1	až
kolem	kolem	k7c2	kolem
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
byl	být	k5eAaImAgMnS	být
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
odvolán	odvolat	k5eAaPmNgMnS	odvolat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
urovnal	urovnat	k5eAaPmAgMnS	urovnat
spory	spor	k1gInPc4	spor
mezi	mezi	k7c7	mezi
husitskými	husitský	k2eAgMnPc7d1	husitský
kněžími	kněz	k1gMnPc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
Bechyně	Bechyně	k1gFnSc2	Bechyně
nezůstal	zůstat	k5eNaPmAgInS	zůstat
bez	bez	k7c2	bez
výrazného	výrazný	k2eAgInSc2d1	výrazný
ohlasu	ohlas	k1gInSc2	ohlas
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přinutil	přinutit	k5eAaPmAgMnS	přinutit
plzeňské	plzeňský	k2eAgNnSc4d1	plzeňské
a	a	k8xC	a
jihočeské	jihočeský	k2eAgNnSc4d1	Jihočeské
katolické	katolický	k2eAgNnSc4d1	katolické
panstvo	panstvo	k1gNnSc4	panstvo
ke	k	k7c3	k
sjednání	sjednání	k1gNnSc3	sjednání
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
u	u	k7c2	u
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
organizování	organizování	k1gNnSc2	organizování
nové	nový	k2eAgFnSc2d1	nová
rejsy	rejsa	k1gFnSc2	rejsa
za	za	k7c4	za
rakouské	rakouský	k2eAgFnPc4d1	rakouská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
spanilé	spanilý	k2eAgFnSc2d1	spanilá
jízdy	jízda	k1gFnSc2	jízda
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc1	žádný
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
táboři	tábor	k1gMnPc1	tábor
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
rozlehlé	rozlehlý	k2eAgNnSc4d1	rozlehlé
území	území	k1gNnSc4	území
až	až	k9	až
k	k	k7c3	k
Dunaji	Dunaj	k1gInSc3	Dunaj
a	a	k8xC	a
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
Eggenburk	Eggenburk	k1gInSc4	Eggenburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1429	[number]	k4	1429
====	====	k?	====
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1428	[number]	k4	1428
učinil	učinit	k5eAaPmAgMnS	učinit
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
husitům	husita	k1gMnPc3	husita
nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
rozhovorům	rozhovor	k1gInPc3	rozhovor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
hornouherském	hornouherský	k2eAgInSc6d1	hornouherský
Prešpurku	Prešpurku	k?	Prešpurku
(	(	kIx(	(
<g/>
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
účasti	účast	k1gFnSc6	účast
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
novoročním	novoroční	k2eAgInSc6d1	novoroční
sjezdu	sjezd	k1gInSc6	sjezd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
současně	současně	k6eAd1	současně
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
i	i	k9	i
členové	člen	k1gMnPc1	člen
delegace	delegace	k1gFnSc2	delegace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
čela	čelo	k1gNnSc2	čelo
byl	být	k5eAaImAgMnS	být
postaven	postavit	k5eAaPmNgMnS	postavit
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
s	s	k7c7	s
uherským	uherský	k2eAgMnSc7d1	uherský
a	a	k8xC	a
německým	německý	k2eAgMnSc7d1	německý
panovníkem	panovník	k1gMnSc7	panovník
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
podmínky	podmínka	k1gFnPc4	podmínka
přítomnosti	přítomnost	k1gFnSc2	přítomnost
husitů	husita	k1gMnPc2	husita
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
nebo	nebo	k8xC	nebo
únoru	únor	k1gInSc6	únor
1429	[number]	k4	1429
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
stojící	stojící	k2eAgFnSc4d1	stojící
u	u	k7c2	u
Eggenburgu	Eggenburg	k1gInSc2	Eggenburg
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prešpurku	Prešpurku	k?	Prešpurku
dorazila	dorazit	k5eAaPmAgFnS	dorazit
husitská	husitský	k2eAgFnSc1d1	husitská
delegace	delegace	k1gFnSc1	delegace
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
rádců	rádce	k1gMnPc2	rádce
a	a	k8xC	a
učenců	učenec	k1gMnPc2	učenec
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
husity	husita	k1gMnPc4	husita
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
lůna	lůno	k1gNnSc2	lůno
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
mu	on	k3xPp3gMnSc3	on
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
letitou	letitý	k2eAgFnSc4d1	letitá
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
obhajobu	obhajoba	k1gFnSc4	obhajoba
čtyř	čtyři	k4xCgInPc2	čtyři
pražských	pražský	k2eAgInPc2d1	pražský
článků	článek	k1gInPc2	článek
před	před	k7c7	před
zástupci	zástupce	k1gMnPc7	zástupce
všeho	všecek	k3xTgNnSc2	všecek
křesťanstva	křesťanstvo	k1gNnSc2	křesťanstvo
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
pronesl	pronést	k5eAaPmAgInS	pronést
svůj	svůj	k3xOyFgInSc4	svůj
projev	projev	k1gInSc4	projev
mistr	mistr	k1gMnSc1	mistr
Petr	Petr	k1gMnSc1	Petr
Payne	Payn	k1gInSc5	Payn
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
krále	král	k1gMnPc4	král
vybídl	vybídnout	k5eAaPmAgInS	vybídnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
k	k	k7c3	k
artikulům	artikul	k1gInPc3	artikul
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
otevře	otevřít	k5eAaPmIp3nS	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
poslušnosti	poslušnost	k1gFnSc3	poslušnost
celého	celý	k2eAgInSc2d1	celý
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
podmínku	podmínka	k1gFnSc4	podmínka
panovník	panovník	k1gMnSc1	panovník
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
laik	laik	k1gMnSc1	laik
není	být	k5eNaImIp3nS	být
kompetentní	kompetentní	k2eAgMnSc1d1	kompetentní
posuzovat	posuzovat	k5eAaImF	posuzovat
duchovní	duchovní	k2eAgInPc4d1	duchovní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
neváhal	váhat	k5eNaImAgMnS	váhat
před	před	k7c7	před
husity	husita	k1gMnPc7	husita
nadnést	nadnést	k5eAaPmF	nadnést
otázku	otázka	k1gFnSc4	otázka
uzavření	uzavření	k1gNnSc2	uzavření
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
na	na	k7c4	na
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
zisk	zisk	k1gInSc4	zisk
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
husitskými	husitský	k2eAgFnPc7d1	husitská
předáky	předák	k1gMnPc4	předák
odmítnut	odmítnut	k2eAgInSc4d1	odmítnut
a	a	k8xC	a
nic	nic	k3yNnSc4	nic
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
stanovisku	stanovisko	k1gNnSc6	stanovisko
nezměnila	změnit	k5eNaPmAgFnS	změnit
ani	ani	k8xC	ani
soukromá	soukromý	k2eAgFnSc1d1	soukromá
schůzka	schůzka	k1gFnSc1	schůzka
mezi	mezi	k7c7	mezi
králem	král	k1gMnSc7	král
a	a	k8xC	a
Prokopem	Prokop	k1gMnSc7	Prokop
Holým	Holý	k1gMnSc7	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
aktuálním	aktuální	k2eAgInSc7d1	aktuální
bodem	bod	k1gInSc7	bod
jednání	jednání	k1gNnSc2	jednání
byla	být	k5eAaImAgFnS	být
účast	účast	k1gFnSc1	účast
husitů	husita	k1gMnPc2	husita
na	na	k7c6	na
příštím	příští	k2eAgInSc6d1	příští
církevním	církevní	k2eAgInSc6d1	církevní
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
bodu	bod	k1gInSc3	bod
přistupovaly	přistupovat	k5eAaImAgInP	přistupovat
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
požadavky	požadavek	k1gInPc7	požadavek
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
výsledkem	výsledek	k1gInSc7	výsledek
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
setkání	setkání	k1gNnSc2	setkání
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
memorandum	memorandum	k1gNnSc4	memorandum
husitských	husitský	k2eAgMnPc2d1	husitský
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vylučovalo	vylučovat	k5eAaImAgNnS	vylučovat
příměří	příměří	k1gNnSc4	příměří
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgNnSc2d1	vlastní
slyšení	slyšení	k1gNnSc2	slyšení
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
artikulech	artikul	k1gInPc6	artikul
a	a	k8xC	a
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
i	i	k8xC	i
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
přísluší	příslušet	k5eAaImIp3nS	příslušet
zemskému	zemský	k2eAgInSc3d1	zemský
sněmu	sněm	k1gInSc3	sněm
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
svolání	svolání	k1gNnSc3	svolání
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Karlově	Karlův	k2eAgFnSc6d1	Karlova
koleji	kolej	k1gFnSc6	kolej
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
diplomatického	diplomatický	k2eAgNnSc2d1	diplomatické
poselstva	poselstvo	k1gNnSc2	poselstvo
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byly	být	k5eAaImAgInP	být
týdenní	týdenní	k2eAgInPc1d1	týdenní
rozhovory	rozhovor	k1gInPc1	rozhovor
poznamenány	poznamenán	k2eAgInPc1d1	poznamenán
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
straně	strana	k1gFnSc6	strana
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
shodného	shodný	k2eAgNnSc2d1	shodné
stanoviska	stanovisko	k1gNnSc2	stanovisko
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
projednávaných	projednávaný	k2eAgFnPc6d1	projednávaná
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
též	též	k9	též
účastnil	účastnit	k5eAaImAgMnS	účastnit
další	další	k2eAgFnPc4d1	další
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
mise	mise	k1gFnPc4	mise
do	do	k7c2	do
Prešpurku	Prešpurku	k?	Prešpurku
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
projednány	projednat	k5eAaPmNgFnP	projednat
veškeré	veškerý	k3xTgFnPc1	veškerý
problematické	problematický	k2eAgFnPc1d1	problematická
otázky	otázka	k1gFnPc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
sporných	sporný	k2eAgInPc2d1	sporný
bodů	bod	k1gInPc2	bod
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
husitská	husitský	k2eAgFnSc1d1	husitská
delegace	delegace	k1gFnSc1	delegace
se	s	k7c7	s
proto	proto	k8xC	proto
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
frontě	fronta	k1gFnSc6	fronta
mezitím	mezitím	k6eAd1	mezitím
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyostření	vyostření	k1gNnSc3	vyostření
roztržky	roztržka	k1gFnSc2	roztržka
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgNnSc7d1	staré
a	a	k8xC	a
Novým	nový	k2eAgNnSc7d1	nové
Městem	město	k1gNnSc7	město
pražským	pražský	k2eAgNnSc7d1	Pražské
<g/>
,	,	kIx,	,
zapříčiněné	zapříčiněný	k2eAgFnSc2d1	zapříčiněná
zejména	zejména	k9	zejména
neochotou	neochota	k1gFnSc7	neochota
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
podělit	podělit	k5eAaPmF	podělit
se	se	k3xPyFc4	se
o	o	k7c4	o
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc4d1	obchodní
výhody	výhoda	k1gFnPc4	výhoda
a	a	k8xC	a
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
obou	dva	k4xCgFnPc2	dva
pražských	pražský	k2eAgFnPc2d1	Pražská
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
opevňovat	opevňovat	k5eAaImF	opevňovat
a	a	k8xC	a
neváhali	váhat	k5eNaImAgMnP	váhat
povolat	povolat	k5eAaPmF	povolat
své	svůj	k3xOyFgMnPc4	svůj
spojence	spojenec	k1gMnPc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Rozkol	rozkol	k1gInSc1	rozkol
eskaloval	eskalovat	k5eAaImAgInS	eskalovat
až	až	k9	až
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
ostřelování	ostřelování	k1gNnSc1	ostřelování
zápalnými	zápalný	k2eAgInPc7d1	zápalný
šípy	šíp	k1gInPc7	šíp
a	a	k8xC	a
puškami	puška	k1gFnPc7	puška
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
urovnání	urovnání	k1gNnSc3	urovnání
byl	být	k5eAaImAgInS	být
povolán	povolán	k2eAgMnSc1d1	povolán
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
podařilo	podařit	k5eAaPmAgNnS	podařit
uzavřít	uzavřít	k5eAaPmF	uzavřít
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
volbě	volba	k1gFnSc6	volba
přátelských	přátelský	k2eAgInPc2d1	přátelský
úmluvců	úmluvce	k1gMnPc2	úmluvce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
staroměstští	staroměstský	k2eAgMnPc1d1	staroměstský
konšelé	konšel	k1gMnPc1	konšel
opulentní	opulentní	k2eAgInSc4d1	opulentní
oběd	oběd	k1gInSc4	oběd
v	v	k7c6	v
malostranském	malostranský	k2eAgInSc6d1	malostranský
šestipanském	šestipanský	k2eAgInSc6d1	šestipanský
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnosti	přítomnost	k1gFnPc1	přítomnost
předáků	předák	k1gMnPc2	předák
sirotčího	sirotčí	k2eAgNnSc2d1	sirotčí
vojska	vojsko	k1gNnSc2	vojsko
posléze	posléze	k6eAd1	posléze
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
využil	využít	k5eAaPmAgMnS	využít
k	k	k7c3	k
naplánování	naplánování	k1gNnSc3	naplánování
nového	nový	k2eAgInSc2d1	nový
úderu	úder	k1gInSc2	úder
za	za	k7c4	za
české	český	k2eAgFnPc4d1	Česká
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
veliteli	velitel	k1gMnPc7	velitel
dalších	další	k2eAgInPc2d1	další
sborů	sbor	k1gInPc2	sbor
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Carda	Carda	k1gMnSc1	Carda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Beh	Beh	k1gMnSc1	Beh
z	z	k7c2	z
Těšnice	Těšnice	k1gFnSc2	Těšnice
a	a	k8xC	a
Otík	Otík	k1gMnSc1	Otík
z	z	k7c2	z
Lozy	loza	k1gFnSc2	loza
<g/>
)	)	kIx)	)
opustil	opustit	k5eAaPmAgMnS	opustit
Prahu	Praha	k1gFnSc4	Praha
po	po	k7c6	po
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
a	a	k8xC	a
den	den	k1gInSc4	den
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
do	do	k7c2	do
pole	pole	k1gNnSc2	pole
i	i	k8xC	i
sirotci	sirotek	k1gMnPc1	sirotek
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
žitavskému	žitavský	k2eAgInSc3d1	žitavský
klášteru	klášter	k1gInSc3	klášter
Ojvín	Ojvína	k1gFnPc2	Ojvína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k9	již
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
operovaly	operovat	k5eAaImAgFnP	operovat
táborské	táborský	k2eAgFnPc1d1	táborská
jednotky	jednotka	k1gFnPc1	jednotka
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
vrchního	vrchní	k2eAgMnSc2d1	vrchní
hejtmana	hejtman	k1gMnSc2	hejtman
Jakuba	Jakub	k1gMnSc2	Jakub
Kroměšína	Kroměšín	k1gMnSc2	Kroměšín
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
střídavými	střídavý	k2eAgInPc7d1	střídavý
úspěchy	úspěch	k1gInPc7	úspěch
pak	pak	k6eAd1	pak
spojené	spojený	k2eAgFnSc2d1	spojená
polní	polní	k2eAgFnSc2d1	polní
obce	obec	k1gFnSc2	obec
táhly	táhnout	k5eAaImAgInP	táhnout
ke	k	k7c3	k
Zhořelci	Zhořelec	k1gInSc3	Zhořelec
a	a	k8xC	a
Budyšínu	Budyšín	k1gInSc3	Budyšín
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
po	po	k7c6	po
třídenním	třídenní	k2eAgNnSc6d1	třídenní
bombardování	bombardování	k1gNnSc6	bombardování
raději	rád	k6eAd2	rád
vykoupili	vykoupit	k5eAaPmAgMnP	vykoupit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
postupu	postup	k1gInSc6	postup
na	na	k7c4	na
západ	západ	k1gInSc4	západ
husité	husita	k1gMnPc1	husita
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kamenec	Kamenec	k1gInSc4	Kamenec
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
k	k	k7c3	k
saskému	saský	k2eAgNnSc3d1	Saské
městu	město	k1gNnSc3	město
Grossenheim	Grossenheima	k1gFnPc2	Grossenheima
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zamířit	zamířit	k5eAaPmF	zamířit
na	na	k7c6	na
území	území	k1gNnSc6	území
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přes	přes	k7c4	přes
Luck	Luck	k1gInSc4	Luck
a	a	k8xC	a
Chotěbuz	Chotěbuz	k1gFnSc4	Chotěbuz
dorazil	dorazit	k5eAaPmAgMnS	dorazit
ke	k	k7c3	k
Kubínu	Kubín	k1gInSc3	Kubín
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
dorazila	dorazit	k5eAaPmAgFnS	dorazit
rejsa	rejsa	k1gFnSc1	rejsa
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přes	přes	k7c4	přes
Crossen	Crossen	k1gInSc4	Crossen
<g/>
,	,	kIx,	,
Boleslavec	Boleslavec	k1gMnSc1	Boleslavec
<g/>
,	,	kIx,	,
Lubno	Lubno	k6eAd1	Lubno
a	a	k8xC	a
Žitavu	Žitava	k1gFnSc4	Žitava
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
si	se	k3xPyFc3	se
sebou	se	k3xPyFc7	se
přiváželi	přivážet	k5eAaImAgMnP	přivážet
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
přihnali	přihnat	k5eAaPmAgMnP	přihnat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
začal	začít	k5eAaPmAgMnS	začít
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
organizovat	organizovat	k5eAaBmF	organizovat
nový	nový	k2eAgInSc4d1	nový
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
výpad	výpad	k1gInSc4	výpad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
tentokrát	tentokrát	k6eAd1	tentokrát
směřovat	směřovat	k5eAaImF	směřovat
do	do	k7c2	do
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
spanilou	spanilý	k2eAgFnSc4d1	spanilá
jízdu	jízda	k1gFnSc4	jízda
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
účastnit	účastnit	k5eAaImF	účastnit
zástupci	zástupce	k1gMnPc1	zástupce
všech	všecek	k3xTgInPc2	všecek
husitských	husitský	k2eAgInPc2d1	husitský
svazů	svaz	k1gInPc2	svaz
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
snad	snad	k9	snad
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
u	u	k7c2	u
severních	severní	k2eAgFnPc2d1	severní
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
překročili	překročit	k5eAaPmAgMnP	překročit
patrně	patrně	k6eAd1	patrně
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
kolonách	kolona	k1gFnPc6	kolona
20	[number]	k4	20
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Zkonsolidované	zkonsolidovaný	k2eAgNnSc1d1	zkonsolidované
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
ubíralo	ubírat	k5eAaImAgNnS	ubírat
podél	podél	k7c2	podél
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
Pirny	Pirna	k1gFnSc2	Pirna
<g/>
,	,	kIx,	,
Drážďan	Drážďany	k1gInPc2	Drážďany
a	a	k8xC	a
Míšně	Míšeň	k1gFnSc2	Míšeň
k	k	k7c3	k
městu	město	k1gNnSc3	město
Grimma	Grimmum	k1gNnSc2	Grimmum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hejtmani	hejtman	k1gMnPc1	hejtman
doufali	doufat	k5eAaImAgMnP	doufat
svést	svést	k5eAaPmF	svést
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
bitvu	bitva	k1gFnSc4	bitva
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
sbory	sbor	k1gInPc1	sbor
saského	saský	k2eAgMnSc2d1	saský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Bedřicha	Bedřich	k1gMnSc2	Bedřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dobromyslného	dobromyslný	k2eAgInSc2d1	dobromyslný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1430	[number]	k4	1430
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
Grimmy	Grimma	k1gFnSc2	Grimma
ke	k	k7c3	k
střetnutí	střetnutí	k1gNnSc3	střetnutí
skutečně	skutečně	k6eAd1	skutečně
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
menší	malý	k2eAgFnSc4d2	menší
bitvu	bitva	k1gFnSc4	bitva
obou	dva	k4xCgInPc2	dva
předvojů	předvoj	k1gInPc2	předvoj
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
německé	německý	k2eAgInPc1d1	německý
oddíly	oddíl	k1gInPc1	oddíl
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
dolnolužického	dolnolužický	k2eAgMnSc2d1	dolnolužický
fojta	fojt	k1gMnSc2	fojt
Hanuše	Hanuš	k1gMnSc2	Hanuš
z	z	k7c2	z
Polenska	Polensko	k1gNnSc2	Polensko
utrpěly	utrpět	k5eAaPmAgFnP	utrpět
výraznou	výrazný	k2eAgFnSc4d1	výrazná
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
srážce	srážka	k1gFnSc6	srážka
husité	husita	k1gMnPc1	husita
očekávali	očekávat	k5eAaImAgMnP	očekávat
útok	útok	k1gInSc4	útok
zbytku	zbytek	k1gInSc2	zbytek
protivníkova	protivníkův	k2eAgNnSc2d1	protivníkovo
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
však	však	k9	však
neodhodlal	odhodlat	k5eNaPmAgMnS	odhodlat
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
další	další	k2eAgFnSc3d1	další
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
většinu	většina	k1gFnSc4	většina
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
obraně	obrana	k1gFnSc6	obrana
opevněných	opevněný	k2eAgNnPc2d1	opevněné
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
hlavní	hlavní	k2eAgInSc4d1	hlavní
protivníkův	protivníkův	k2eAgInSc4d1	protivníkův
voj	voj	k1gInSc4	voj
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Lipsku	Lipsko	k1gNnSc3	Lipsko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
obléháním	obléhání	k1gNnSc7	obléhání
se	se	k3xPyFc4	se
však	však	k9	však
nezdržovali	zdržovat	k5eNaImAgMnP	zdržovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
do	do	k7c2	do
pěti	pět	k4xCc2	pět
kolon	kolona	k1gFnPc2	kolona
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
na	na	k7c6	na
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
km	km	kA	km
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
širokém	široký	k2eAgInSc6d1	široký
rozestupu	rozestup	k1gInSc6	rozestup
postupoval	postupovat	k5eAaImAgMnS	postupovat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
části	část	k1gFnSc2	část
Saska	Sasko	k1gNnSc2	Sasko
<g/>
,	,	kIx,	,
biskupství	biskupství	k1gNnSc2	biskupství
bamberského	bamberský	k2eAgNnSc2d1	bamberské
<g/>
,	,	kIx,	,
purkrabství	purkrabství	k1gNnSc2	purkrabství
norimberského	norimberský	k2eAgNnSc2d1	norimberské
a	a	k8xC	a
Falce	Falc	k1gFnSc2	Falc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
husité	husita	k1gMnPc1	husita
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Plavno	Plavno	k1gNnSc4	Plavno
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
vypálili	vypálit	k5eAaPmAgMnP	vypálit
Hof	Hof	k1gMnPc1	Hof
<g/>
;	;	kIx,	;
Bayreuth	Bayreuth	k1gMnSc1	Bayreuth
a	a	k8xC	a
Kulmbach	Kulmbacha	k1gFnPc2	Kulmbacha
padly	padnout	k5eAaPmAgFnP	padnout
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
spojeneckého	spojenecký	k2eAgNnSc2d1	spojenecké
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
odeslal	odeslat	k5eAaPmAgMnS	odeslat
list	list	k1gInSc4	list
konšelům	konšel	k1gMnPc3	konšel
Bamberku	Bamberk	k1gInSc2	Bamberk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c6	o
uzavření	uzavření	k1gNnSc6	uzavření
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
jeho	jeho	k3xOp3gFnSc1	jeho
podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
přijetí	přijetí	k1gNnSc1	přijetí
čtyř	čtyři	k4xCgFnPc2	čtyři
artikulů	artikul	k1gInPc2	artikul
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neochoty	neochota	k1gFnSc2	neochota
požadoval	požadovat	k5eAaImAgMnS	požadovat
částku	částka	k1gFnSc4	částka
50	[number]	k4	50
000	[number]	k4	000
rýnských	rýnský	k2eAgFnPc2d1	Rýnská
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
podobně	podobně	k6eAd1	podobně
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
list	list	k1gInSc1	list
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Korandou	Koranda	k1gFnSc7	Koranda
a	a	k8xC	a
třemi	tři	k4xCgInPc7	tři
dalšími	další	k2eAgInPc7d1	další
duchovními	duchovní	k2eAgInPc7d1	duchovní
správci	správce	k1gMnPc7	správce
husitských	husitský	k2eAgNnPc2d1	husitské
vojsk	vojsko	k1gNnPc2	vojsko
odeslal	odeslat	k5eAaPmAgMnS	odeslat
měšťanům	měšťan	k1gMnPc3	měšťan
Norimberka	Norimberk	k1gInSc2	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
intervence	intervence	k1gFnSc2	intervence
Fridricha	Fridrich	k1gMnSc2	Fridrich
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
byly	být	k5eAaImAgInP	být
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
na	na	k7c6	na
Beheimsteinu	Beheimstein	k1gInSc6	Beheimstein
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
úmluvy	úmluva	k1gFnPc1	úmluva
jak	jak	k8xS	jak
o	o	k7c4	o
zaplacení	zaplacení	k1gNnSc4	zaplacení
výkupného	výkupné	k1gNnSc2	výkupné
<g/>
,	,	kIx,	,
tak	tak	k9	tak
o	o	k7c4	o
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
čtyř	čtyři	k4xCgInPc2	čtyři
artikulech	artikul	k1gInPc6	artikul
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
konat	konat	k5eAaImF	konat
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Ratifikací	ratifikace	k1gFnSc7	ratifikace
bamberské	bamberský	k2eAgFnSc2d1	Bamberská
úmluvy	úmluva	k1gFnSc2	úmluva
též	též	k9	též
vstupovalo	vstupovat	k5eAaImAgNnS	vstupovat
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
trvat	trvat	k5eAaImF	trvat
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odjel	odjet	k5eAaPmAgMnS	odjet
společně	společně	k6eAd1	společně
s	s	k7c7	s
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
do	do	k7c2	do
Norimberku	Norimberk	k1gInSc2	Norimberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
ubytování	ubytování	k1gNnSc4	ubytování
husitského	husitský	k2eAgNnSc2d1	husitské
poselstva	poselstvo	k1gNnSc2	poselstvo
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
částí	část	k1gFnSc7	část
vojska	vojsko	k1gNnSc2	vojsko
přitáhl	přitáhnout	k5eAaPmAgInS	přitáhnout
k	k	k7c3	k
Chebu	Cheb	k1gInSc3	Cheb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
vyslanci	vyslanec	k1gMnSc3	vyslanec
z	z	k7c2	z
města	město	k1gNnSc2	město
přinesli	přinést	k5eAaPmAgMnP	přinést
darem	dar	k1gInSc7	dar
dvanáct	dvanáct	k4xCc4	dvanáct
loktů	loket	k1gInPc2	loket
vynikajícího	vynikající	k2eAgNnSc2d1	vynikající
bruselského	bruselský	k2eAgNnSc2d1	bruselské
sukna	sukno	k1gNnSc2	sukno
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
usmlouvali	usmlouvat	k5eAaPmAgMnP	usmlouvat
nabídku	nabídka	k1gFnSc4	nabídka
příměří	příměří	k1gNnSc2	příměří
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
900	[number]	k4	900
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
spanilá	spanilý	k2eAgFnSc1d1	spanilá
<g/>
,	,	kIx,	,
že	že	k8xS	že
pamětníkuov	pamětníkuov	k1gInSc1	pamětníkuov
není	být	k5eNaImIp3nS	být
aniž	aniž	k8xC	aniž
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
kdy	kdy	k6eAd1	kdy
Čechové	Čech	k1gMnPc1	Čech
sami	sám	k3xTgMnPc1	sám
učinili	učinit	k5eAaImAgMnP	učinit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
ukončena	ukončen	k2eAgFnSc1d1	ukončena
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
složení	složení	k1gNnSc6	složení
husitské	husitský	k2eAgFnSc2d1	husitská
delegace	delegace	k1gFnSc2	delegace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgNnP	mít
účastnit	účastnit	k5eAaImF	účastnit
jednání	jednání	k1gNnPc1	jednání
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
poradě	porada	k1gFnSc6	porada
uspořádané	uspořádaný	k2eAgFnSc2d1	uspořádaná
v	v	k7c6	v
Karlově	Karlův	k2eAgFnSc6d1	Karlova
koleji	kolej	k1gFnSc6	kolej
asi	asi	k9	asi
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
duchovního	duchovní	k2eAgInSc2d1	duchovní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
Fridrich	Fridrich	k1gMnSc1	Fridrich
Braniborský	braniborský	k2eAgInSc1d1	braniborský
snažil	snažit	k5eAaImAgMnS	snažit
své	svůj	k3xOyFgNnSc4	svůj
slovo	slovo	k1gNnSc4	slovo
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
,	,	kIx,	,
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
odpor	odpor	k1gInSc4	odpor
papežského	papežský	k2eAgInSc2d1	papežský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
jednání	jednání	k1gNnPc4	jednání
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
mise	mise	k1gFnSc2	mise
sejde	sejít	k5eAaPmIp3nS	sejít
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zorganizovány	zorganizován	k2eAgFnPc1d1	zorganizována
další	další	k2eAgFnPc1d1	další
rejsy	rejsa	k1gFnPc1	rejsa
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
těch	ten	k3xDgFnPc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
zaneprázdněn	zaneprázdnit	k5eAaPmNgInS	zaneprázdnit
politickými	politický	k2eAgFnPc7d1	politická
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
osobě	osoba	k1gFnSc6	osoba
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
k	k	k7c3	k
16	[number]	k4	16
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Nymburku	Nymburk	k1gInSc6	Nymburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
místním	místní	k2eAgMnSc7d1	místní
hejtmanem	hejtman	k1gMnSc7	hejtman
Otíkem	Otík	k1gMnSc7	Otík
z	z	k7c2	z
Lozy	loza	k1gFnSc2	loza
vyřizoval	vyřizovat	k5eAaImAgInS	vyřizovat
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
korespondenci	korespondence	k1gFnSc4	korespondence
týkající	týkající	k2eAgFnSc4d1	týkající
se	se	k3xPyFc4	se
zajatců	zajatec	k1gMnPc2	zajatec
a	a	k8xC	a
výkupného	výkupné	k1gNnSc2	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
pak	pak	k6eAd1	pak
vyslal	vyslat	k5eAaPmAgMnS	vyslat
pomocné	pomocný	k2eAgInPc4d1	pomocný
oddíly	oddíl	k1gInPc4	oddíl
ohrožené	ohrožený	k2eAgFnSc3d1	ohrožená
posádce	posádka	k1gFnSc3	posádka
ve	v	k7c6	v
slezském	slezský	k2eAgInSc6d1	slezský
Němčí	němčit	k5eAaImIp3nP	němčit
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jakubem	Jakub	k1gMnSc7	Jakub
Kromšínem	Kromšín	k1gMnSc7	Kromšín
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
vpádu	vpád	k1gInSc2	vpád
na	na	k7c6	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
okolí	okolí	k1gNnSc4	okolí
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
dobyli	dobýt	k5eAaPmAgMnP	dobýt
hrad	hrad	k1gInSc4	hrad
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
obléhání	obléhání	k1gNnSc2	obléhání
donesly	donést	k5eAaPmAgFnP	donést
k	k	k7c3	k
Prokopovi	Prokop	k1gMnSc3	Prokop
Holému	Holý	k1gMnSc3	Holý
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
připravovaném	připravovaný	k2eAgInSc6d1	připravovaný
útoku	útok	k1gInSc6	útok
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
pevnosti	pevnost	k1gFnSc2	pevnost
zamířil	zamířit	k5eAaPmAgInS	zamířit
k	k	k7c3	k
Plzni	Plzeň	k1gFnSc3	Plzeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
srpnových	srpnový	k2eAgInPc6d1	srpnový
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypleněni	vyplenit	k5eAaPmNgMnP	vyplenit
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
okolí	okolí	k1gNnSc2	okolí
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
k	k	k7c3	k
Tachovu	Tachov	k1gInSc3	Tachov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
marně	marně	k6eAd1	marně
očekával	očekávat	k5eAaImAgMnS	očekávat
útoku	útok	k1gInSc3	útok
protivníka	protivník	k1gMnSc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
pominulo	pominout	k5eAaPmAgNnS	pominout
<g/>
,	,	kIx,	,
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
husité	husita	k1gMnPc1	husita
příměří	příměří	k1gNnPc2	příměří
s	s	k7c7	s
plzeňským	plzeňský	k2eAgInSc7d1	plzeňský
landfrýdem	landfrýd	k1gInSc7	landfrýd
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Prokopem	Prokop	k1gMnSc7	Prokop
Holým	Holý	k1gMnSc7	Holý
opět	opět	k6eAd1	opět
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
posádkám	posádka	k1gFnPc3	posádka
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Otmuchov	Otmuchov	k1gInSc4	Otmuchov
a	a	k8xC	a
Vrbno	Vrbno	k6eAd1	Vrbno
a	a	k8xC	a
následně	následně	k6eAd1	následně
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
Namyslav	Namyslav	k1gMnSc1	Namyslav
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dobře	dobře	k6eAd1	dobře
opevněné	opevněný	k2eAgNnSc1d1	opevněné
město	město	k1gNnSc1	město
nepodařilo	podařit	k5eNaPmAgNnS	podařit
přinutit	přinutit	k5eAaPmF	přinutit
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
,	,	kIx,	,
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
zpáteční	zpáteční	k2eAgInSc4d1	zpáteční
pochod	pochod	k1gInSc4	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
ještě	ještě	k6eAd1	ještě
sirotci	sirotek	k1gMnPc1	sirotek
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
podnikli	podniknout	k5eAaPmAgMnP	podniknout
další	další	k2eAgFnSc4d1	další
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1431	[number]	k4	1431
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
roku	rok	k1gInSc2	rok
1431	[number]	k4	1431
navázal	navázat	k5eAaPmAgMnS	navázat
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
mír	mír	k1gInSc4	mír
mezi	mezi	k7c7	mezi
husity	husita	k1gMnPc7	husita
a	a	k8xC	a
katolickou	katolický	k2eAgFnSc7d1	katolická
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
přijatelný	přijatelný	k2eAgInSc1d1	přijatelný
i	i	k9	i
pro	pro	k7c4	pro
sirotky	sirotek	k1gMnPc4	sirotek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odmítali	odmítat	k5eAaImAgMnP	odmítat
mír	mír	k1gInSc4	mír
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
králem	král	k1gMnSc7	král
Zikmundem	Zikmund	k1gMnSc7	Zikmund
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
diskutováno	diskutovat	k5eAaImNgNnS	diskutovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
svolán	svolat	k5eAaPmNgInS	svolat
kolem	kolem	k7c2	kolem
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
do	do	k7c2	do
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
zevrubnější	zevrubný	k2eAgInPc1d2	zevrubnější
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnPc1	událost
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
účastnících	účastnící	k2eAgInPc6d1	účastnící
delegace	delegace	k1gFnSc2	delegace
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
a	a	k8xC	a
dvanáctičlenné	dvanáctičlenný	k2eAgFnSc6d1	dvanáctičlenná
prozatímní	prozatímní	k2eAgFnSc6d1	prozatímní
zemské	zemský	k2eAgFnSc6d1	zemská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
měl	mít	k5eAaImAgMnS	mít
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
vyrazit	vyrazit	k5eAaPmF	vyrazit
společně	společně	k6eAd1	společně
s	s	k7c7	s
mistrem	mistr	k1gMnSc7	mistr
Petrem	Petr	k1gMnSc7	Petr
Paynem	Payn	k1gMnSc7	Payn
<g/>
,	,	kIx,	,
Vilémem	Vilém	k1gMnSc7	Vilém
Kostkou	Kostka	k1gMnSc7	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
a	a	k8xC	a
Bedřichem	Bedřich	k1gMnSc7	Bedřich
ze	z	k7c2	z
Strážnice	Strážnice	k1gFnSc2	Strážnice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
do	do	k7c2	do
dvanáctičlenného	dvanáctičlenný	k2eAgInSc2d1	dvanáctičlenný
vládního	vládní	k2eAgInSc2d1	vládní
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
jména	jméno	k1gNnSc2	jméno
zvolených	zvolený	k2eAgInPc2d1	zvolený
se	se	k3xPyFc4	se
však	však	k9	však
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
diplomaté	diplomat	k1gMnPc1	diplomat
však	však	k9	však
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
ani	ani	k8xC	ani
místními	místní	k2eAgMnPc7d1	místní
teology	teolog	k1gMnPc7	teolog
nenalezli	naleznout	k5eNaPmAgMnP	naleznout
společnou	společný	k2eAgFnSc4d1	společná
řeč	řeč	k1gFnSc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
postoj	postoj	k1gInSc1	postoj
krakovského	krakovský	k2eAgInSc2d1	krakovský
biskupa	biskup	k1gInSc2	biskup
Zbigniewa	Zbigniewum	k1gNnSc2	Zbigniewum
Olešnického	olešnický	k2eAgNnSc2d1	Olešnické
a	a	k8xC	a
neochota	neochota	k1gFnSc1	neochota
husitů	husita	k1gMnPc2	husita
souhlasit	souhlasit	k5eAaImF	souhlasit
s	s	k7c7	s
Vladislavovým	Vladislavův	k2eAgInSc7d1	Vladislavův
požadavkem	požadavek	k1gInSc7	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
podrobili	podrobit	k5eAaPmAgMnP	podrobit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
budoucího	budoucí	k2eAgInSc2d1	budoucí
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
nepřál	přát	k5eNaImAgMnS	přát
uzavřít	uzavřít	k5eAaPmF	uzavřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
pokračovaní	pokračovaný	k2eAgMnPc1d1	pokračovaný
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
polskému	polský	k2eAgMnSc3d1	polský
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
návrhy	návrh	k1gInPc7	návrh
seznámí	seznámit	k5eAaPmIp3nP	seznámit
účastníky	účastník	k1gMnPc7	účastník
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
že	že	k8xS	že
sněmovní	sněmovní	k2eAgNnSc1d1	sněmovní
stanovisko	stanovisko	k1gNnSc1	stanovisko
bude	být	k5eAaImBp3nS	být
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rokování	rokování	k1gNnSc1	rokování
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zahraničně	zahraničně	k6eAd1	zahraničně
politická	politický	k2eAgNnPc4d1	politické
témata	téma	k1gNnPc4	téma
jak	jak	k8xS	jak
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
nepřinesla	přinést	k5eNaPmAgFnS	přinést
další	další	k2eAgInPc4d1	další
podněty	podnět	k1gInPc4	podnět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ke	k	k7c3	k
vpádu	vpád	k1gInSc3	vpád
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
formovala	formovat	k5eAaImAgFnS	formovat
další	další	k2eAgFnSc1d1	další
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
schylovalo	schylovat	k5eAaImAgNnS	schylovat
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vojenskému	vojenský	k2eAgInSc3d1	vojenský
úderu	úder	k1gInSc3	úder
<g/>
,	,	kIx,	,
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
další	další	k2eAgFnSc4d1	další
rejsu	rejsa	k1gFnSc4	rejsa
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Lužice	Lužice	k1gFnSc2	Lužice
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
nejpozději	pozdě	k6eAd3	pozdě
před	před	k7c7	před
polovinou	polovina	k1gFnSc7	polovina
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třinácti	třináct	k4xCc2	třináct
dní	den	k1gInPc2	den
pak	pak	k6eAd1	pak
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
vojskem	vojsko	k1gNnSc7	vojsko
podnikl	podniknout	k5eAaPmAgInS	podniknout
cestu	cesta	k1gFnSc4	cesta
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
přes	přes	k7c4	přes
200	[number]	k4	200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
domovské	domovský	k2eAgNnSc4d1	domovské
území	území	k1gNnSc4	území
posléze	posléze	k6eAd1	posléze
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
Plzeňsko	Plzeňsko	k1gNnSc4	Plzeňsko
svolávat	svolávat	k5eAaImF	svolávat
husitské	husitský	k2eAgInPc4d1	husitský
sbory	sbor	k1gInPc4	sbor
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
táborským	táborský	k2eAgNnSc7d1	táborské
vojskem	vojsko	k1gNnSc7	vojsko
na	na	k7c4	na
shromaždiště	shromaždiště	k1gNnSc4	shromaždiště
dorazil	dorazit	k5eAaPmAgMnS	dorazit
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
zahájil	zahájit	k5eAaPmAgMnS	zahájit
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
příslušníkům	příslušník	k1gMnPc3	příslušník
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
landfrýdu	landfrýd	k1gInSc2	landfrýd
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
veden	veden	k2eAgInSc1d1	veden
pouze	pouze	k6eAd1	pouze
pustošením	pustošení	k1gNnSc7	pustošení
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
shromážděné	shromážděný	k2eAgNnSc1d1	shromážděné
husitské	husitský	k2eAgNnSc1d1	husitské
vojsko	vojsko	k1gNnSc1	vojsko
posunulo	posunout	k5eAaPmAgNnS	posunout
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
na	na	k7c6	na
linii	linie	k1gFnSc6	linie
Tachov	Tachov	k1gInSc1	Tachov
–	–	k?	–
Horšovský	horšovský	k2eAgInSc1d1	horšovský
Týn	Týn	k1gInSc1	Týn
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
setrvalo	setrvat	k5eAaPmAgNnS	setrvat
téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neochota	neochota	k1gFnSc1	neochota
protivníka	protivník	k1gMnSc2	protivník
zahájit	zahájit	k5eAaPmF	zahájit
pochod	pochod	k1gInSc4	pochod
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
proviantu	proviant	k1gInSc2	proviant
přinutily	přinutit	k5eAaPmAgFnP	přinutit
Prokopa	Prokop	k1gMnSc4	Prokop
Holého	Holého	k2eAgNnSc1d1	Holého
odvést	odvést	k5eAaPmF	odvést
vojáky	voják	k1gMnPc4	voják
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
velitel	velitel	k1gMnSc1	velitel
kruciáty	kruciáta	k1gFnSc2	kruciáta
Fridrich	Fridrich	k1gMnSc1	Fridrich
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
překročit	překročit	k5eAaPmF	překročit
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
zahájit	zahájit	k5eAaPmF	zahájit
bojové	bojový	k2eAgFnPc4d1	bojová
operace	operace	k1gFnPc4	operace
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
se	se	k3xPyFc4	se
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
u	u	k7c2	u
Berouna	Beroun	k1gInSc2	Beroun
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
k	k	k7c3	k
Chotěšovu	Chotěšův	k2eAgNnSc3d1	Chotěšův
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
zhruba	zhruba	k6eAd1	zhruba
75	[number]	k4	75
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
úsek	úsek	k1gInSc4	úsek
celé	celý	k2eAgNnSc1d1	celé
vojsko	vojsko	k1gNnSc1	vojsko
urazilo	urazit	k5eAaPmAgNnS	urazit
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
husitský	husitský	k2eAgInSc1d1	husitský
štáb	štáb	k1gInSc1	štáb
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
o	o	k7c6	o
svízelné	svízelný	k2eAgFnSc6d1	svízelná
situaci	situace	k1gFnSc6	situace
posádky	posádka	k1gFnSc2	posádka
obléhaných	obléhaný	k2eAgFnPc2d1	obléhaná
Domažlic	Domažlice	k1gFnPc2	Domažlice
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ráno	ráno	k6eAd1	ráno
vojáci	voják	k1gMnPc1	voják
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
rychlým	rychlý	k2eAgInSc7d1	rychlý
pochodem	pochod	k1gInSc7	pochod
proti	proti	k7c3	proti
křižákům	křižák	k1gInPc3	křižák
<g/>
,	,	kIx,	,
shromážděným	shromážděný	k2eAgNnSc7d1	shromážděné
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezí	mez	k1gFnPc2	mez
Domažlicemi	Domažlice	k1gFnPc7	Domažlice
a	a	k8xC	a
městem	město	k1gNnSc7	město
Kdyně	Kdyně	k1gFnSc2	Kdyně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
většího	veliký	k2eAgInSc2d2	veliký
rozsahu	rozsah	k1gInSc2	rozsah
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
německé	německý	k2eAgInPc1d1	německý
sbory	sbor	k1gInPc1	sbor
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
rozutekly	rozutéct	k5eAaPmAgFnP	rozutéct
a	a	k8xC	a
Všerubským	všerubský	k2eAgInSc7d1	všerubský
průsmykem	průsmyk	k1gInSc7	průsmyk
zamířily	zamířit	k5eAaPmAgFnP	zamířit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
domovské	domovský	k2eAgNnSc4d1	domovské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInSc2	boj
se	se	k3xPyFc4	se
odvážila	odvážit	k5eAaPmAgFnS	odvážit
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
pěchoty	pěchota	k1gFnSc2	pěchota
ponechaná	ponechaný	k2eAgFnSc1d1	ponechaná
ve	v	k7c6	v
vozové	vozový	k2eAgFnSc6d1	vozová
hradbě	hradba	k1gFnSc6	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
u	u	k7c2	u
zemské	zemský	k2eAgFnSc2d1	zemská
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
německé	německý	k2eAgNnSc1d1	německé
vojsko	vojsko	k1gNnSc1	vojsko
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
opět	opět	k6eAd1	opět
obnovit	obnovit	k5eAaPmF	obnovit
ofenzivu	ofenziva	k1gFnSc4	ofenziva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nabyl	nabýt	k5eAaPmAgInS	nabýt
jistoty	jistota	k1gFnPc4	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohrožení	ohrožení	k1gNnSc1	ohrožení
je	být	k5eAaImIp3nS	být
odvráceno	odvrácen	k2eAgNnSc1d1	odvrácen
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
táborského	táborský	k2eAgInSc2d1	táborský
svazu	svaz	k1gInSc2	svaz
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
posádce	posádka	k1gFnSc3	posádka
v	v	k7c6	v
Němčí	němčit	k5eAaImIp3nP	němčit
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
oblehly	oblehnout	k5eAaPmAgFnP	oblehnout
jednotky	jednotka	k1gFnPc4	jednotka
místních	místní	k2eAgMnPc2d1	místní
feudálů	feudál	k1gMnPc2	feudál
<g/>
.	.	kIx.	.
</s>
<s>
Slezané	Slezan	k1gMnPc1	Slezan
se	se	k3xPyFc4	se
však	však	k9	však
při	při	k7c6	při
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
příchodu	příchod	k1gInSc6	příchod
rozešli	rozejít	k5eAaPmAgMnP	rozejít
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
v	v	k7c6	v
opevněných	opevněný	k2eAgNnPc6d1	opevněné
sídlech	sídlo	k1gNnPc6	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
rejse	rejsa	k1gFnSc6	rejsa
se	se	k3xPyFc4	se
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ukončení	ukončení	k1gNnSc6	ukončení
táboři	tábor	k1gMnPc1	tábor
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
přes	přes	k7c4	přes
Opavu	Opava	k1gFnSc4	Opava
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
boku	bok	k1gInSc6	bok
sirotků	sirotek	k1gMnPc2	sirotek
udeřili	udeřit	k5eAaPmAgMnP	udeřit
na	na	k7c4	na
vojsko	vojsko	k1gNnSc4	vojsko
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
se	se	k3xPyFc4	se
však	však	k9	však
neodhodlal	odhodlat	k5eNaPmAgMnS	odhodlat
k	k	k7c3	k
boji	boj	k1gInSc3	boj
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
zpět	zpět	k6eAd1	zpět
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
proto	proto	k8xC	proto
dál	daleko	k6eAd2	daleko
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
ofenzivě	ofenziva	k1gFnSc6	ofenziva
a	a	k8xC	a
zamířili	zamířit	k5eAaPmAgMnP	zamířit
na	na	k7c6	na
území	území	k1gNnSc6	území
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
u	u	k7c2	u
Žiliny	Žilina	k1gFnSc2	Žilina
obě	dva	k4xCgFnPc1	dva
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Likavu	Likava	k1gFnSc4	Likava
<g/>
,	,	kIx,	,
Lednici	Lednice	k1gFnSc4	Lednice
a	a	k8xC	a
Nitru	Nitra	k1gFnSc4	Nitra
a	a	k8xC	a
sirotčí	sirotčí	k2eAgFnSc1d1	sirotčí
posádka	posádka	k1gFnSc1	posádka
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dočasně	dočasně	k6eAd1	dočasně
obsadila	obsadit	k5eAaPmAgFnS	obsadit
i	i	k9	i
Topoľčany	Topoľčan	k1gMnPc4	Topoľčan
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
úspěchy	úspěch	k1gInPc4	úspěch
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c4	mezi
sirotky	sirotka	k1gFnPc4	sirotka
a	a	k8xC	a
tábory	tábor	k1gInPc4	tábor
začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
odchod	odchod	k1gInSc4	odchod
táborů	tábor	k1gInPc2	tábor
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
údajně	údajně	k6eAd1	údajně
strhli	strhnout	k5eAaPmAgMnP	strhnout
most	most	k1gInSc4	most
přes	přes	k7c4	přes
Váh	Váh	k1gInSc4	Váh
v	v	k7c6	v
Hlohovci	Hlohovec	k1gInSc6	Hlohovec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
okolnost	okolnost	k1gFnSc1	okolnost
měla	mít	k5eAaImAgFnS	mít
fatální	fatální	k2eAgInPc4d1	fatální
následky	následek	k1gInPc4	následek
pro	pro	k7c4	pro
sirotky	sirotka	k1gFnPc4	sirotka
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Čapkem	Čapek	k1gMnSc7	Čapek
ze	z	k7c2	z
Sán	sán	k2eAgInSc1d1	sán
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
následovali	následovat	k5eAaImAgMnP	následovat
tábory	tábor	k1gInPc4	tábor
s	s	k7c7	s
několikadenním	několikadenní	k2eAgNnSc7d1	několikadenní
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Sirotci	Sirotek	k1gMnPc1	Sirotek
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
decimovali	decimovat	k5eAaBmAgMnP	decimovat
Uherští	uherský	k2eAgMnPc1d1	uherský
vojáci	voják	k1gMnPc1	voják
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
patách	pata	k1gFnPc6	pata
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
vinu	vinout	k5eAaImIp1nS	vinout
za	za	k7c4	za
fiasko	fiasko	k1gNnSc4	fiasko
přičítali	přičítat	k5eAaImAgMnP	přičítat
Prokopovi	Prokop	k1gMnSc3	Prokop
Holému	Holý	k1gMnSc3	Holý
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
Jan	Jan	k1gMnSc1	Jan
Čapek	Čapek	k1gMnSc1	Čapek
neváhal	váhat	k5eNaImAgMnS	váhat
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
zrádce	zrádce	k1gMnPc4	zrádce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
proti	proti	k7c3	proti
táborskému	táborský	k2eAgMnSc3d1	táborský
duchovnímu	duchovní	k2eAgMnSc3d1	duchovní
správci	správce	k1gMnSc3	správce
zvedlo	zvednout	k5eAaPmAgNnS	zvednout
značné	značný	k2eAgNnSc1d1	značné
rozhořčení	rozhořčení	k1gNnSc1	rozhořčení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
rejsy	rejsa	k1gFnSc2	rejsa
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
upoutala	upoutat	k5eAaPmAgFnS	upoutat
blíže	blízce	k6eAd2	blízce
nekonkretizovaná	konkretizovaný	k2eNgFnSc1d1	nekonkretizovaná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
odeslal	odeslat	k5eAaPmAgMnS	odeslat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c6	o
vyslání	vyslání	k1gNnSc6	vyslání
doktora	doktor	k1gMnSc2	doktor
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
posledního	poslední	k2eAgInSc2d1	poslední
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
nezdaru	nezdar	k1gInSc2	nezdar
Pražané	Pražan	k1gMnPc1	Pražan
odpověděli	odpovědět	k5eAaPmAgMnP	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
raději	rád	k6eAd2	rád
pošlou	poslat	k5eAaPmIp3nP	poslat
kata	kata	k9	kata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1432	[number]	k4	1432
====	====	k?	====
</s>
</p>
<p>
<s>
Možnost	možnost	k1gFnSc1	možnost
mocenské	mocenský	k2eAgFnSc2d1	mocenská
izolace	izolace	k1gFnSc2	izolace
táborské	táborský	k2eAgFnSc2d1	táborská
strany	strana	k1gFnSc2	strana
po	po	k7c6	po
novoročním	novoroční	k2eAgInSc6d1	novoroční
husitském	husitský	k2eAgInSc6d1	husitský
sněmu	sněm	k1gInSc6	sněm
a	a	k8xC	a
souběžné	souběžný	k2eAgFnSc6d1	souběžná
synodě	synoda	k1gFnSc6	synoda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
představitelů	představitel	k1gMnPc2	představitel
basilejského	basilejský	k2eAgInSc2d1	basilejský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
nabízející	nabízející	k2eAgNnSc1d1	nabízející
jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
artikulech	artikul	k1gInPc6	artikul
<g/>
,	,	kIx,	,
přinutily	přinutit	k5eAaPmAgFnP	přinutit
již	již	k6eAd1	již
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
opustit	opustit	k5eAaPmF	opustit
lůžko	lůžko	k1gNnSc4	lůžko
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
jeho	jeho	k3xOp3gInSc7	jeho
počinem	počin	k1gInSc7	počin
bylo	být	k5eAaImAgNnS	být
svolání	svolání	k1gNnSc1	svolání
shromáždění	shromáždění	k1gNnSc2	shromáždění
starších	starý	k2eAgMnPc2d2	starší
táborských	táborský	k2eAgMnPc2d1	táborský
hejtmanů	hejtman	k1gMnPc2	hejtman
a	a	k8xC	a
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
jednotný	jednotný	k2eAgInSc4d1	jednotný
postup	postup	k1gInSc4	postup
svazových	svazový	k2eAgMnPc2d1	svazový
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
delegace	delegace	k1gFnSc2	delegace
táborských	táborský	k2eAgFnPc2d1	táborská
obcí	obec	k1gFnPc2	obec
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
Oldřichem	Oldřich	k1gMnSc7	Oldřich
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
roční	roční	k2eAgNnSc4d1	roční
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
řádné	řádný	k2eAgNnSc4d1	řádné
vyručení	vyručení	k1gNnSc4	vyručení
zajatců	zajatec	k1gMnPc2	zajatec
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Bejdova	Bejdův	k2eAgMnSc2d1	Bejdův
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jihočeskému	jihočeský	k2eAgMnSc3d1	jihočeský
velmožovi	velmož	k1gMnSc3	velmož
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
husitské	husitský	k2eAgNnSc4d1	husitské
poselstvo	poselstvo	k1gNnSc4	poselstvo
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Třeboně	Třeboň	k1gFnSc2	Třeboň
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
10	[number]	k4	10
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
svolán	svolán	k2eAgInSc1d1	svolán
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
husitský	husitský	k2eAgInSc1d1	husitský
sněm	sněm	k1gInSc1	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
v	v	k7c6	v
ležení	ležení	k1gNnSc6	ležení
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
příkaz	příkaz	k1gInSc4	příkaz
organizovaly	organizovat	k5eAaBmAgFnP	organizovat
domácí	domácí	k2eAgFnPc1d1	domácí
a	a	k8xC	a
polní	polní	k2eAgFnPc1d1	polní
táborské	táborský	k2eAgFnPc1d1	táborská
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jen	jen	k9	jen
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
jednak	jednak	k8xC	jednak
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
konsenzu	konsenz	k1gInSc3	konsenz
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
husitů	husita	k1gMnPc2	husita
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
k	k	k7c3	k
ustanovení	ustanovení	k1gNnSc3	ustanovení
předběžné	předběžný	k2eAgFnSc2d1	předběžná
schůzky	schůzka	k1gFnSc2	schůzka
s	s	k7c7	s
basilejskými	basilejský	k2eAgMnPc7d1	basilejský
představiteli	představitel	k1gMnPc7	představitel
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
a	a	k8xC	a
ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
reprezentativního	reprezentativní	k2eAgNnSc2d1	reprezentativní
poselstva	poselstvo	k1gNnSc2	poselstvo
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgNnSc2	jenž
jako	jako	k9	jako
čelní	čelní	k2eAgMnSc1d1	čelní
táborský	táborský	k2eAgMnSc1d1	táborský
vůdce	vůdce	k1gMnSc1	vůdce
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
pražského	pražský	k2eAgInSc2d1	pražský
sněmu	sněm	k1gInSc2	sněm
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
táborská	táborský	k2eAgNnPc4d1	táborské
a	a	k8xC	a
sirotčí	sirotčí	k2eAgNnPc4d1	sirotčí
polní	polní	k2eAgNnPc4d1	polní
vojska	vojsko	k1gNnPc4	vojsko
na	na	k7c4	na
jízdu	jízda	k1gFnSc4	jízda
do	do	k7c2	do
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc2d1	dolní
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
akce	akce	k1gFnSc2	akce
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
napáchání	napáchání	k1gNnSc1	napáchání
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
škod	škoda	k1gFnPc2	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
braniborského	braniborský	k2eAgMnSc2d1	braniborský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
Slezanům	Slezan	k1gMnPc3	Slezan
a	a	k8xC	a
Lužičanům	Lužičan	k1gMnPc3	Lužičan
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Táboři	tábor	k1gMnPc1	tábor
se	se	k3xPyFc4	se
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
u	u	k7c2	u
Mladé	mladý	k2eAgFnSc2d1	mladá
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
k	k	k7c3	k
Frýdlantu	Frýdlant	k1gInSc3	Frýdlant
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vypálili	vypálit	k5eAaPmAgMnP	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
zpustošili	zpustošit	k5eAaPmAgMnP	zpustošit
okolí	okolí	k1gNnSc4	okolí
Zhořelce	Zhořelec	k1gInSc2	Zhořelec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
připojili	připojit	k5eAaPmAgMnP	připojit
sirotci	sirotek	k1gMnPc1	sirotek
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Jana	Jan	k1gMnSc2	Jan
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
Sán	sán	k2eAgInSc4d1	sán
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
vydala	vydat	k5eAaPmAgFnS	vydat
k	k	k7c3	k
Boleslavci	Boleslavec	k1gMnSc3	Boleslavec
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
k	k	k7c3	k
Frankfurtu	Frankfurt	k1gInSc3	Frankfurt
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
opět	opět	k6eAd1	opět
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
a	a	k8xC	a
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
kolonách	kolona	k1gFnPc6	kolona
postupovala	postupovat	k5eAaImAgFnS	postupovat
na	na	k7c4	na
Müncheberg	Müncheberg	k1gInSc4	Müncheberg
a	a	k8xC	a
Alt	Alt	kA	Alt
Landsberg	Landsberg	k1gInSc1	Landsberg
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
k	k	k7c3	k
Bernau	Bernaus	k1gInSc3	Bernaus
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
začátkem	začátek	k1gInSc7	začátek
května	květen	k1gInSc2	květen
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
osobnost	osobnost	k1gFnSc4	osobnost
husitské	husitský	k2eAgFnSc2d1	husitská
levice	levice	k1gFnSc2	levice
očekáván	očekávat	k5eAaImNgInS	očekávat
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazil	dorazit	k5eAaPmAgMnS	dorazit
s	s	k7c7	s
osmidenním	osmidenní	k2eAgNnSc7d1	osmidenní
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Chebské	chebský	k2eAgInPc1d1	chebský
rozhovory	rozhovor	k1gInPc1	rozhovor
zahájené	zahájený	k2eAgInPc1d1	zahájený
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květno	k1gNnSc2	květno
byly	být	k5eAaImAgFnP	být
kvůli	kvůli	k7c3	kvůli
vzájemným	vzájemný	k2eAgNnPc3d1	vzájemné
stanoviskům	stanovisko	k1gNnPc3	stanovisko
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
téměř	téměř	k6eAd1	téměř
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
i	i	k9	i
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
husité	husita	k1gMnPc1	husita
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
příměří	příměří	k1gNnSc4	příměří
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
koncilem	koncil	k1gInSc7	koncil
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
důrazně	důrazně	k6eAd1	důrazně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
z	z	k7c2	z
příčiny	příčina	k1gFnSc2	příčina
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc6	jenž
husité	husita	k1gMnPc1	husita
nebyli	být	k5eNaImAgMnP	být
ochotní	ochotný	k2eAgMnPc1d1	ochotný
uzavřít	uzavřít	k5eAaPmF	uzavřít
mír	mír	k1gInSc4	mír
s	s	k7c7	s
kterýmkoliv	kterýkoliv	k3yIgMnSc7	kterýkoliv
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
doporučil	doporučit	k5eAaPmAgMnS	doporučit
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nP	by
s	s	k7c7	s
Čechami	Čechy	k1gFnPc7	Čechy
chtěli	chtít	k5eAaImAgMnP	chtít
dohodnout	dohodnout	k5eAaPmF	dohodnout
klid	klid	k1gInSc4	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svou	svůj	k3xOyFgFnSc4	svůj
žádost	žádost	k1gFnSc4	žádost
předložili	předložit	k5eAaPmAgMnP	předložit
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
zemskému	zemský	k2eAgInSc3d1	zemský
sněmu	sněm	k1gInSc3	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tlak	tlak	k1gInSc1	tlak
obyvatel	obyvatel	k1gMnPc2	obyvatel
Chebu	Cheb	k1gInSc2	Cheb
<g/>
,	,	kIx,	,
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Fridricha	Fridrich	k1gMnSc2	Fridrich
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
a	a	k8xC	a
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
basilejské	basilejský	k2eAgMnPc4d1	basilejský
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
,	,	kIx,	,
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
úmluvy	úmluva	k1gFnSc2	úmluva
známé	známý	k2eAgFnPc4d1	známá
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Soudce	soudce	k1gMnSc1	soudce
chebský	chebský	k2eAgInSc1d1	chebský
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
listina	listina	k1gFnSc1	listina
byla	být	k5eAaImAgFnS	být
vítězstvím	vítězství	k1gNnSc7	vítězství
husitů	husita	k1gMnPc2	husita
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
jejich	jejich	k3xOp3gInPc2	jejich
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc4	církev
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
uznala	uznat	k5eAaPmAgFnS	uznat
"	"	kIx"	"
<g/>
kacíře	kacíř	k1gMnSc2	kacíř
<g/>
"	"	kIx"	"
za	za	k7c4	za
rovnoprávného	rovnoprávný	k2eAgMnSc4d1	rovnoprávný
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
netrvala	trvat	k5eNaImAgFnS	trvat
na	na	k7c6	na
bezpodmínečné	bezpodmínečný	k2eAgFnSc6d1	bezpodmínečná
správnosti	správnost	k1gFnSc6	správnost
svých	svůj	k3xOyFgInPc2	svůj
názorů	názor	k1gInPc2	názor
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
autoritu	autorita	k1gFnSc4	autorita
boží	boží	k2eAgInSc4d1	boží
zákon	zákon	k1gInSc4	zákon
obsažený	obsažený	k2eAgInSc4d1	obsažený
v	v	k7c6	v
Písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
písemně	písemně	k6eAd1	písemně
informoval	informovat	k5eAaBmAgMnS	informovat
krále	král	k1gMnSc4	král
Zikmunda	Zikmund	k1gMnSc4	Zikmund
o	o	k7c6	o
výsledcích	výsledek	k1gInPc6	výsledek
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
účastnil	účastnit	k5eAaImAgMnS	účastnit
jednání	jednání	k1gNnPc4	jednání
husitů	husita	k1gMnPc2	husita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
táborského	táborský	k2eAgMnSc2d1	táborský
a	a	k8xC	a
sirotčího	sirotčí	k2eAgNnSc2d1	sirotčí
vojska	vojsko	k1gNnSc2	vojsko
k	k	k7c3	k
Olomouci	Olomouc	k1gFnSc3	Olomouc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vypomohl	vypomoct	k5eAaPmAgMnS	vypomoct
moravským	moravský	k2eAgMnPc3d1	moravský
husitům	husita	k1gMnPc3	husita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
kláštera	klášter	k1gInSc2	klášter
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzápětí	vzápětí	k6eAd1	vzápětí
byli	být	k5eAaImAgMnP	být
obleženi	oblehnout	k5eAaPmNgMnP	oblehnout
vojskem	vojsko	k1gNnSc7	vojsko
Albrechta	Albrecht	k1gMnSc4	Albrecht
Habsburského	habsburský	k2eAgMnSc4d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
příchodu	příchod	k1gInSc6	příchod
českých	český	k2eAgNnPc2d1	české
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
obrátili	obrátit	k5eAaPmAgMnP	obrátit
k	k	k7c3	k
Olomouci	Olomouc	k1gFnSc3	Olomouc
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
okolí	okolí	k1gNnSc6	okolí
způsobili	způsobit	k5eAaPmAgMnP	způsobit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
červnové	červnový	k2eAgFnSc2d1	červnová
dekády	dekáda	k1gFnSc2	dekáda
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
Prokopovi	Prokop	k1gMnSc3	Prokop
Holému	Holý	k1gMnSc3	Holý
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
zničení	zničení	k1gNnSc4	zničení
zásobovacího	zásobovací	k2eAgInSc2d1	zásobovací
konvoje	konvoj	k1gInSc2	konvoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vypraven	vypravit	k5eAaPmNgInS	vypravit
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
do	do	k7c2	do
slezského	slezský	k2eAgNnSc2d1	Slezské
Němčí	němčit	k5eAaImIp3nS	němčit
<g/>
.	.	kIx.	.
</s>
<s>
Přepadení	přepadení	k1gNnPc1	přepadení
měla	mít	k5eAaImAgNnP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
střelínská	střelínský	k2eAgFnSc1d1	střelínská
posádka	posádka	k1gFnSc1	posádka
a	a	k8xC	a
táborský	táborský	k2eAgMnSc1d1	táborský
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc1	správce
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
okamžitém	okamžitý	k2eAgInSc6d1	okamžitý
protiúderu	protiúder	k1gInSc6	protiúder
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
21	[number]	k4	21
mil	míle	k1gFnPc2	míle
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
krmení	krmení	k1gNnSc4	krmení
<g/>
,	,	kIx,	,
dorazili	dorazit	k5eAaPmAgMnP	dorazit
ke	k	k7c3	k
Střelínu	Střelín	k1gInSc3	Střelín
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
posíleni	posílit	k5eAaPmNgMnP	posílit
o	o	k7c4	o
posádku	posádka	k1gFnSc4	posádka
z	z	k7c2	z
Němčí	němčit	k5eAaImIp3nS	němčit
<g/>
,	,	kIx,	,
nečekaným	čekaný	k2eNgInSc7d1	nečekaný
úderem	úder	k1gInSc7	úder
rozprášili	rozprášit	k5eAaPmAgMnP	rozprášit
nedaleko	nedaleko	k7c2	nedaleko
jeho	jeho	k3xOp3gFnPc2	jeho
hradeb	hradba	k1gFnPc2	hradba
přítomné	přítomný	k1gMnPc4	přítomný
vojenské	vojenský	k2eAgInPc1d1	vojenský
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
protivníkových	protivníkův	k2eAgMnPc2d1	protivníkův
vojáků	voják	k1gMnPc2	voják
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ústup	ústup	k1gInSc1	ústup
však	však	k9	však
byl	být	k5eAaImAgInS	být
jen	jen	k6eAd1	jen
dočasným	dočasný	k2eAgNnSc7d1	dočasné
řešením	řešení	k1gNnSc7	řešení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
600	[number]	k4	600
obránců	obránce	k1gMnPc2	obránce
složilo	složit	k5eAaPmAgNnS	složit
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
zamířil	zamířit	k5eAaPmAgMnS	zamířit
k	k	k7c3	k
Vratislavi	Vratislav	k1gFnSc3	Vratislav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyplenil	vyplenit	k5eAaPmAgMnS	vyplenit
její	její	k3xOp3gNnSc4	její
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
zpustošil	zpustošit	k5eAaPmAgInS	zpustošit
část	část	k1gFnSc4	část
knížectví	knížectví	k1gNnSc2	knížectví
olešnického	olešnický	k2eAgNnSc2d1	Olešnické
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
k	k	k7c3	k
Stinavě	Stinavě	k1gFnSc3	Stinavě
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
lstí	lest	k1gFnPc2	lest
dobýt	dobýt	k5eAaPmF	dobýt
most	most	k1gInSc4	most
a	a	k8xC	a
překročit	překročit	k5eAaPmF	překročit
Odru	Odra	k1gFnSc4	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Vypálení	vypálení	k1gNnSc1	vypálení
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
a	a	k8xC	a
městeček	městečko	k1gNnPc2	městečko
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
Olešnice	Olešnice	k1gFnSc1	Olešnice
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohnulo	pohnout	k5eAaPmAgNnS	pohnout
slezské	slezský	k2eAgInPc4d1	slezský
stavy	stav	k1gInPc4	stav
uzavřít	uzavřít	k5eAaPmF	uzavřít
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
1600	[number]	k4	1600
kop	kop	k1gInSc4	kop
českých	český	k2eAgInPc2d1	český
grošů	groš	k1gInPc2	groš
dvouleté	dvouletý	k2eAgNnSc1d1	dvouleté
příměří	příměří	k1gNnSc1	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
husitských	husitský	k2eAgMnPc2d1	husitský
úspěchů	úspěch	k1gInPc2	úspěch
roku	rok	k1gInSc2	rok
1432	[number]	k4	1432
bylo	být	k5eAaImAgNnS	být
uzavření	uzavření	k1gNnSc1	uzavření
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
v	v	k7c6	v
Pabianicích	Pabianice	k1gFnPc6	Pabianice
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nedostatečných	dostatečný	k2eNgInPc2d1	nedostatečný
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
však	však	k9	však
není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
důležité	důležitý	k2eAgFnSc2d1	důležitá
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
politicky	politicky	k6eAd1	politicky
choulostivé	choulostivý	k2eAgFnSc2d1	choulostivá
schůzky	schůzka	k1gFnSc2	schůzka
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
patrně	patrně	k6eAd1	patrně
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
uzavřením	uzavření	k1gNnSc7	uzavření
příměří	příměří	k1gNnSc2	příměří
se	s	k7c7	s
slezskými	slezský	k2eAgInPc7d1	slezský
stavy	stav	k1gInPc7	stav
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
dnech	den	k1gInPc6	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
přítomen	přítomno	k1gNnPc2	přítomno
na	na	k7c6	na
všehusitském	všehusitský	k2eAgInSc6d1	všehusitský
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
oficiálně	oficiálně	k6eAd1	oficiálně
potvrzeny	potvrzen	k2eAgFnPc1d1	potvrzena
chebské	chebský	k2eAgFnPc1d1	Chebská
úmluvy	úmluva	k1gFnPc1	úmluva
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
diplomatického	diplomatický	k2eAgNnSc2d1	diplomatické
poselstva	poselstvo	k1gNnSc2	poselstvo
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Prokopem	Prokop	k1gMnSc7	Prokop
Holým	Holý	k1gMnSc7	Holý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
patnáctičlenné	patnáctičlenný	k2eAgFnSc2d1	patnáctičlenná
delegace	delegace	k1gFnSc2	delegace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
cesty	cesta	k1gFnSc2	cesta
účastnit	účastnit	k5eAaImF	účastnit
například	například	k6eAd1	například
mistr	mistr	k1gMnSc1	mistr
Petr	Petr	k1gMnSc1	Petr
Payne	Payn	k1gInSc5	Payn
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Kostka	Kostka	k1gMnSc1	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
z	z	k7c2	z
Mokrovous	Mokrovous	k1gMnSc1	Mokrovous
<g/>
,	,	kIx,	,
Jíra	Jíra	k1gMnSc1	Jíra
z	z	k7c2	z
Řečice	Řečice	k1gFnSc2	Řečice
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
Louda	Louda	k1gMnSc1	Louda
z	z	k7c2	z
Chlumčan	Chlumčan	k1gMnSc1	Chlumčan
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Lupáč	Lupáč	k1gMnSc1	Lupáč
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
přijal	přijmout	k5eAaPmAgMnS	přijmout
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
polské	polský	k2eAgMnPc4d1	polský
vyslance	vyslanec	k1gMnPc4	vyslanec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
opětovně	opětovně	k6eAd1	opětovně
projednal	projednat	k5eAaPmAgMnS	projednat
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Římskoněmecké	římskoněmecký	k2eAgFnSc2d1	římskoněmecká
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
poselstvem	poselstvo	k1gNnSc7	poselstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
čítalo	čítat	k5eAaImAgNnS	čítat
na	na	k7c4	na
padesát	padesát	k4xCc4	padesát
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1433	[number]	k4	1433
====	====	k?	====
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
husitské	husitský	k2eAgNnSc1d1	husitské
poselstvo	poselstvo	k1gNnSc1	poselstvo
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
příjezdem	příjezd	k1gInSc7	příjezd
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Enea	Enea	k6eAd1	Enea
Silvio	Silvio	k6eAd1	Silvio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
přímým	přímý	k2eAgMnSc7d1	přímý
účastníkem	účastník	k1gMnSc7	účastník
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Po	po	k7c6	po
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
přivítání	přivítání	k1gNnSc6	přivítání
konšely	konšel	k1gMnPc4	konšel
města	město	k1gNnSc2	město
byli	být	k5eAaImAgMnP	být
čeští	český	k2eAgMnPc1d1	český
vyslanci	vyslanec	k1gMnPc1	vyslanec
ubytovaní	ubytovaný	k2eAgMnPc1d1	ubytovaný
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
předních	přední	k2eAgInPc6d1	přední
hostincích	hostinec	k1gInPc6	hostinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
Prokopa	Prokop	k1gMnSc4	Prokop
Holého	Holý	k1gMnSc4	Holý
a	a	k8xC	a
Viléma	Vilém	k1gMnSc4	Vilém
Kostku	Kostka	k1gMnSc4	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gInPc2	Postupice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
táborský	táborský	k2eAgMnSc1d1	táborský
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc2	správce
zápolení	zápolení	k1gNnSc2	zápolení
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
přičiněním	přičinění	k1gNnSc7	přičinění
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
jednota	jednota	k1gFnSc1	jednota
husitů	husita	k1gMnPc2	husita
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
ohrožoval	ohrožovat	k5eAaImAgInS	ohrožovat
zejména	zejména	k9	zejména
postoj	postoj	k1gInSc1	postoj
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Rokycany	Rokycany	k1gInPc4	Rokycany
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
němuž	jenž	k3xRgNnSc3	jenž
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
reagoval	reagovat	k5eAaBmAgMnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
raději	rád	k6eAd2	rád
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
před	před	k7c7	před
koncilem	koncil	k1gInSc7	koncil
dopustil	dopustit	k5eAaPmAgMnS	dopustit
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
rozepře	rozepře	k1gFnSc2	rozepře
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jednání	jednání	k1gNnSc2	jednání
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
nepřipravenými	připravený	k2eNgInPc7d1	nepřipravený
projevy	projev	k1gInPc7	projev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
šly	jít	k5eAaImAgInP	jít
spíše	spíše	k9	spíše
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
výbuchy	výbuch	k1gInPc1	výbuch
vírou	víra	k1gFnSc7	víra
přeplněné	přeplněný	k2eAgFnSc2d1	přeplněná
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
o	o	k7c6	o
naprosté	naprostý	k2eAgFnSc6d1	naprostá
oprávněnosti	oprávněnost	k1gFnSc6	oprávněnost
své	svůj	k3xOyFgFnSc2	svůj
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
postoj	postoj	k1gInSc1	postoj
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
mistru	mistr	k1gMnSc3	mistr
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
university	universita	k1gFnSc2	universita
dominikánovi	dominikán	k1gMnSc3	dominikán
Janu	Jan	k1gMnSc3	Jan
Stojkovičovi	Stojkovič	k1gMnSc3	Stojkovič
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
zbytečně	zbytečně	k6eAd1	zbytečně
protahoval	protahovat	k5eAaImAgMnS	protahovat
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
nazýval	nazývat	k5eAaImAgMnS	nazývat
husity	husita	k1gMnPc4	husita
kacíři	kacíř	k1gMnSc6	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
označení	označení	k1gNnSc3	označení
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
ohradil	ohradit	k5eAaPmAgMnS	ohradit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
já	já	k3xPp1nSc1	já
s	s	k7c7	s
druhými	druhý	k4xOgMnPc7	druhý
bratřími	bratr	k1gMnPc7	bratr
tvrdím	tvrdit	k5eAaImIp1nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsme	být	k5eNaImIp1nP	být
kacíři	kacíř	k1gMnPc1	kacíř
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc4	ten
dosud	dosud	k6eAd1	dosud
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přede	příst	k5eAaImIp3nS	příst
onen	onen	k3xDgMnSc1	onen
mnich	mnich	k1gMnSc1	mnich
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
neustále	neustále	k6eAd1	neustále
nás	my	k3xPp1nPc2	my
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
za	za	k7c4	za
kacíře	kacíř	k1gMnSc4	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
to	ten	k3xDgNnSc4	ten
vezměte	vzít	k5eAaPmRp2nP	vzít
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdybych	kdyby	kYmCp1nS	kdyby
to	ten	k3xDgNnSc4	ten
věděl	vědět	k5eAaImAgMnS	vědět
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
najisto	najisto	k9	najisto
bych	by	kYmCp1nS	by
sem	sem	k6eAd1	sem
nebyl	být	k5eNaImAgInS	být
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
vy	vy	k3xPp2nPc1	vy
jednáte	jednat	k5eAaImIp2nP	jednat
proti	proti	k7c3	proti
úmluvám	úmluva	k1gFnPc3	úmluva
sjednaným	sjednaný	k2eAgFnPc3d1	sjednaná
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vědí	vědět	k5eAaImIp3nP	vědět
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tam	tam	k6eAd1	tam
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
doktor	doktor	k1gMnSc1	doktor
Tok	toka	k1gFnPc2	toka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Uražený	uražený	k2eAgMnSc1d1	uražený
táborský	táborský	k2eAgMnSc1d1	táborský
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc1	správce
poté	poté	k6eAd1	poté
opustil	opustit	k5eAaPmAgMnS	opustit
auditorium	auditorium	k1gNnSc4	auditorium
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdykoliv	kdykoliv	k6eAd1	kdykoliv
hovořil	hovořit	k5eAaImAgMnS	hovořit
Jan	Jan	k1gMnSc1	Jan
Stojkovič	Stojkovič	k1gMnSc1	Stojkovič
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc1	jednání
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
Kostkou	Kostka	k1gMnSc7	Kostka
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
řeč	řeč	k1gFnSc4	řeč
pronesl	pronést	k5eAaPmAgMnS	pronést
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
poselstvo	poselstvo	k1gNnSc1	poselstvo
loučilo	loučit	k5eAaImAgNnS	loučit
s	s	k7c7	s
koncilem	koncil	k1gInSc7	koncil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
projevu	projev	k1gInSc6	projev
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
nad	nad	k7c7	nad
nápravou	náprava	k1gFnSc7	náprava
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
příčinami	příčina	k1gFnPc7	příčina
církevních	církevní	k2eAgInPc2d1	církevní
zlořádů	zlořád	k1gInPc2	zlořád
<g/>
.	.	kIx.	.
</s>
<s>
Basilej	Basilej	k1gFnSc1	Basilej
čeští	český	k2eAgMnPc1d1	český
diplomaté	diplomat	k1gMnPc1	diplomat
opustili	opustit	k5eAaPmAgMnP	opustit
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
vyslanci	vyslanec	k1gMnPc7	vyslanec
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
husity	husita	k1gMnPc7	husita
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
debatě	debata	k1gFnSc6	debata
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
<g/>
,	,	kIx,	,
dorazili	dorazit	k5eAaPmAgMnP	dorazit
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
katolických	katolický	k2eAgMnPc2d1	katolický
delegátů	delegát	k1gMnPc2	delegát
se	se	k3xPyFc4	se
však	však	k9	však
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
kazatele	kazatel	k1gMnSc2	kazatel
Jakuba	Jakub	k1gMnSc2	Jakub
Vlka	Vlk	k1gMnSc2	Vlk
a	a	k8xC	a
novoměstských	novoměstský	k2eAgMnPc2d1	novoměstský
sirotků	sirotek	k1gMnPc2	sirotek
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
biskup	biskup	k1gMnSc1	biskup
Filibert	Filibert	k1gMnSc1	Filibert
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
biřmoval	biřmovat	k5eAaImAgMnS	biřmovat
několik	několik	k4yIc4	několik
českých	český	k2eAgFnPc2d1	Česká
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
pražská	pražský	k2eAgFnSc1d1	Pražská
chudina	chudina	k1gFnSc1	chudina
připravila	připravit	k5eAaPmAgFnS	připravit
noční	noční	k2eAgNnSc4d1	noční
přepadení	přepadení	k1gNnSc4	přepadení
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
vyslanci	vyslanec	k1gMnPc1	vyslanec
ubytovaní	ubytovaný	k2eAgMnPc1d1	ubytovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
díky	díky	k7c3	díky
zásahu	zásah	k1gInSc3	zásah
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
o	o	k7c6	o
útoku	útok	k1gInSc6	útok
včas	včas	k6eAd1	včas
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
a	a	k8xC	a
pobouřenému	pobouřený	k2eAgInSc3d1	pobouřený
lidu	lid	k1gInSc3	lid
jeho	on	k3xPp3gNnSc2	on
záměr	záměr	k1gInSc1	záměr
rozmluvil	rozmluvit	k5eAaPmAgInS	rozmluvit
<g/>
,	,	kIx,	,
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádným	žádný	k3yNgFnPc3	žádný
násilnostem	násilnost	k1gFnPc3	násilnost
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
za	za	k7c4	za
mimořádně	mimořádně	k6eAd1	mimořádně
velké	velký	k2eAgFnSc2d1	velká
účasti	účast	k1gFnSc2	účast
zástupců	zástupce	k1gMnPc2	zástupce
kališnické	kališnický	k2eAgFnSc2d1	kališnická
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
měst	město	k1gNnPc2	město
a	a	k8xC	a
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
řady	řada	k1gFnSc2	řada
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
zahájen	zahájen	k2eAgInSc4d1	zahájen
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
svatotrojický	svatotrojický	k2eAgInSc4d1	svatotrojický
sněm	sněm	k1gInSc4	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvítacích	uvítací	k2eAgInPc6d1	uvítací
ceremoniálech	ceremoniál	k1gInPc6	ceremoniál
a	a	k8xC	a
prvních	první	k4xOgInPc6	první
projevech	projev	k1gInPc6	projev
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
pronesl	pronést	k5eAaPmAgMnS	pronést
svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
i	i	k9	i
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
Jana	Jan	k1gMnSc2	Jan
Palomara	Palomar	k1gMnSc2	Palomar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
husity	husita	k1gMnPc4	husita
vybízel	vybízet	k5eAaImAgInS	vybízet
k	k	k7c3	k
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
příměří	příměří	k1gNnSc3	příměří
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
postoj	postoj	k1gInSc4	postoj
proti	proti	k7c3	proti
žalobám	žaloba	k1gFnPc3	žaloba
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
opustit	opustit	k5eAaPmF	opustit
od	od	k7c2	od
krvavých	krvavý	k2eAgInPc2d1	krvavý
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
libuje	libovat	k5eAaImIp3nS	libovat
si	se	k3xPyFc3	se
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sněmu	sněm	k1gInSc2	sněm
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
vyslanci	vyslanec	k1gMnPc7	vyslanec
koncilu	koncil	k1gInSc2	koncil
projednat	projednat	k5eAaPmF	projednat
podrobný	podrobný	k2eAgInSc4d1	podrobný
výklad	výklad	k1gInSc4	výklad
čtyř	čtyři	k4xCgInPc2	čtyři
pražských	pražský	k2eAgInPc2d1	pražský
artikulů	artikul	k1gInPc2	artikul
sestavený	sestavený	k2eAgInSc4d1	sestavený
Janem	Jan	k1gMnSc7	Jan
Rokycanou	Rokycaný	k2eAgFnSc4d1	Rokycaný
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
basilejští	basilejský	k2eAgMnPc1d1	basilejský
diplomaté	diplomat	k1gMnPc1	diplomat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zorientovali	zorientovat	k5eAaPmAgMnP	zorientovat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
získávat	získávat	k5eAaImF	získávat
první	první	k4xOgInPc4	první
důležité	důležitý	k2eAgInPc4d1	důležitý
kontakty	kontakt	k1gInPc4	kontakt
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc4d1	další
jednání	jednání	k1gNnSc4	jednání
záměrně	záměrně	k6eAd1	záměrně
prodlužovali	prodlužovat	k5eAaImAgMnP	prodlužovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
čas	čas	k1gInSc4	čas
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
širší	široký	k2eAgFnSc2d2	širší
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
reagoval	reagovat	k5eAaBmAgMnS	reagovat
táborský	táborský	k2eAgMnSc1d1	táborský
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc4	správce
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprojeví	projevit	k5eNaPmIp3nS	projevit
<g/>
-li	i	k?	-li
koncil	koncil	k1gInSc1	koncil
ochotu	ochota	k1gFnSc4	ochota
k	k	k7c3	k
ústupkům	ústupek	k1gInPc3	ústupek
<g/>
,	,	kIx,	,
husité	husita	k1gMnPc1	husita
oblehnou	oblehnout	k5eAaPmIp3nP	oblehnout
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
baštu	bašta	k1gFnSc4	bašta
českých	český	k2eAgMnPc2d1	český
katolíků	katolík	k1gMnPc2	katolík
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
přinutí	přinutit	k5eAaPmIp3nS	přinutit
ji	on	k3xPp3gFnSc4	on
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
politický	politický	k2eAgInSc4d1	politický
nátlak	nátlak	k1gInSc4	nátlak
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
tedy	tedy	k9	tedy
k	k	k7c3	k
západočeské	západočeský	k2eAgFnSc3d1	Západočeská
metropoli	metropol	k1gFnSc6	metropol
dorazily	dorazit	k5eAaPmAgInP	dorazit
první	první	k4xOgInPc1	první
táborské	táborský	k2eAgInPc1d1	táborský
sbory	sbor	k1gInPc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
postupně	postupně	k6eAd1	postupně
posílili	posílit	k5eAaPmAgMnP	posílit
další	další	k2eAgMnPc1d1	další
příslušníci	příslušník	k1gMnPc1	příslušník
husitských	husitský	k2eAgInPc2d1	husitský
svazů	svaz	k1gInPc2	svaz
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
zde	zde	k6eAd1	zde
stanulo	stanout	k5eAaPmAgNnS	stanout
na	na	k7c4	na
14	[number]	k4	14
000	[number]	k4	000
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
Plzni	Plzeň	k1gFnSc3	Plzeň
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
vojskům	vojsko	k1gNnPc3	vojsko
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
hradby	hradba	k1gFnPc4	hradba
poškodit	poškodit	k5eAaPmF	poškodit
dělostřelbou	dělostřelba	k1gFnSc7	dělostřelba
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
možností	možnost	k1gFnSc7	možnost
proto	proto	k8xC	proto
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
město	město	k1gNnSc1	město
i	i	k9	i
s	s	k7c7	s
početnou	početný	k2eAgFnSc7d1	početná
posádkou	posádka	k1gFnSc7	posádka
vyhladovět	vyhladovět	k5eAaPmF	vyhladovět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
podnik	podnik	k1gInSc1	podnik
však	však	k9	však
znamenal	znamenat	k5eAaImAgInS	znamenat
i	i	k9	i
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
zátěž	zátěž	k1gFnSc4	zátěž
na	na	k7c4	na
logistiku	logistika	k1gFnSc4	logistika
husitů	husita	k1gMnPc2	husita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1433	[number]	k4	1433
postihla	postihnout	k5eAaPmAgFnS	postihnout
značná	značný	k2eAgFnSc1d1	značná
neúroda	neúroda	k1gFnSc1	neúroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
proto	proto	k8xC	proto
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
rejsa	rejsa	k1gFnSc1	rejsa
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
hejtmana	hejtman	k1gMnSc2	hejtman
Jana	Jan	k1gMnSc2	Jan
Parduse	pardus	k1gInSc5	pardus
a	a	k8xC	a
domažlického	domažlický	k2eAgMnSc2d1	domažlický
hejtmana	hejtman	k1gMnSc2	hejtman
Jana	Jan	k1gMnSc2	Jan
Řitky	Řitka	k1gMnSc2	Řitka
z	z	k7c2	z
Bezdědic	Bezdědice	k1gInPc2	Bezdědice
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
oblehatelům	oblehatel	k1gMnPc3	oblehatel
dostatek	dostatek	k1gInSc4	dostatek
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
však	však	k9	však
byla	být	k5eAaImAgFnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Hiltersried	Hiltersried	k1gInSc4	Hiltersried
drtivě	drtivě	k6eAd1	drtivě
poražena	poražen	k2eAgFnSc1d1	poražena
domácí	domácí	k2eAgFnSc7d1	domácí
hotovostí	hotovost	k1gFnSc7	hotovost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zachránit	zachránit	k5eAaPmF	zachránit
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
oběma	dva	k4xCgMnPc7	dva
velitelům	velitel	k1gMnPc3	velitel
a	a	k8xC	a
několika	několik	k4yIc3	několik
desítkám	desítka	k1gFnPc3	desítka
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ležení	ležení	k1gNnSc6	ležení
před	před	k7c7	před
Plzní	Plzeň	k1gFnSc7	Plzeň
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
neúspěchu	neúspěch	k1gInSc6	neúspěch
značnou	značný	k2eAgFnSc4d1	značná
bouři	bouře	k1gFnSc4	bouře
rozhořčení	rozhořčení	k1gNnSc2	rozhořčení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vygradovala	vygradovat	k5eAaPmAgFnS	vygradovat
na	na	k7c6	na
poradě	porada	k1gFnSc6	porada
zástupců	zástupce	k1gMnPc2	zástupce
táborských	táborský	k2eAgFnPc2d1	táborská
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
diskuse	diskuse	k1gFnSc2	diskuse
byly	být	k5eAaImAgInP	být
patrně	patrně	k6eAd1	patrně
příčiny	příčina	k1gFnPc4	příčina
debaklu	debakl	k1gInSc2	debakl
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
a	a	k8xC	a
vina	vina	k1gFnSc1	vina
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
oba	dva	k4xCgMnPc4	dva
hejtmany	hejtman	k1gMnPc4	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pardus	pardus	k1gInSc4	pardus
byl	být	k5eAaImAgMnS	být
dokonce	dokonce	k9	dokonce
svázán	svázat	k5eAaPmNgMnS	svázat
a	a	k8xC	a
rozčilení	rozčilený	k2eAgMnPc1d1	rozčilený
vojáci	voják	k1gMnPc1	voják
požadovali	požadovat	k5eAaImAgMnP	požadovat
jeho	jeho	k3xOp3gFnSc4	jeho
popravu	poprava	k1gFnSc4	poprava
jako	jako	k8xS	jako
zrádce	zrádce	k1gMnSc4	zrádce
a	a	k8xC	a
zbabělce	zbabělec	k1gMnSc4	zbabělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
hejtmana	hejtman	k1gMnSc2	hejtman
zastal	zastat	k5eAaPmAgInS	zastat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xS	jako
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc1	správce
zneužil	zneužít	k5eAaPmAgMnS	zneužít
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
přivlastňoval	přivlastňovat	k5eAaImAgInS	přivlastňovat
si	se	k3xPyFc3	se
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
bral	brát	k5eAaImAgInS	brát
peníze	peníz	k1gInPc4	peníz
ze	z	k7c2	z
společné	společný	k2eAgFnSc2d1	společná
pokladnice	pokladnice	k1gFnSc2	pokladnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
vřavě	vřava	k1gFnSc6	vřava
jej	on	k3xPp3gMnSc4	on
pak	pak	k9	pak
podhejtman	podhejtman	k1gMnSc1	podhejtman
Tvaroh	tvaroh	k1gInSc4	tvaroh
udeřil	udeřit	k5eAaPmAgMnS	udeřit
stoličkou	stolička	k1gFnSc7	stolička
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
svázán	svázat	k5eAaPmNgMnS	svázat
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Pardusem	pardus	k1gInSc7	pardus
a	a	k8xC	a
dalším	další	k2eAgMnSc7d1	další
hejtmanem	hejtman	k1gMnSc7	hejtman
vsazen	vsazen	k2eAgInSc4d1	vsazen
do	do	k7c2	do
vězení	vězení	k1gNnPc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
ležení	ležení	k1gNnSc6	ležení
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
žádosti	žádost	k1gFnPc4	žádost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dál	daleko	k6eAd2	daleko
setrval	setrvat	k5eAaPmAgMnS	setrvat
ve	v	k7c6	v
velení	velení	k1gNnSc6	velení
<g/>
,	,	kIx,	,
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Ponížený	ponížený	k2eAgMnSc1d1	ponížený
a	a	k8xC	a
zbavený	zbavený	k2eAgMnSc1d1	zbavený
rozhodujícího	rozhodující	k2eAgNnSc2d1	rozhodující
politického	politický	k2eAgNnSc2d1	politické
postavení	postavení	k1gNnSc2	postavení
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgMnS	hodlat
léčit	léčit	k5eAaImF	léčit
a	a	k8xC	a
žít	žít	k5eAaImF	žít
po	po	k7c4	po
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
konal	konat	k5eAaImAgInS	konat
tzv.	tzv.	kA	tzv.
svatomartinský	svatomartinský	k2eAgInSc1d1	svatomartinský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nový	nový	k2eAgMnSc1d1	nový
zemský	zemský	k2eAgMnSc1d1	zemský
správce	správce	k1gMnSc1	správce
Aleš	Aleš	k1gMnSc1	Aleš
Vřešťovský	Vřešťovský	k1gMnSc1	Vřešťovský
z	z	k7c2	z
Rýzmburka	Rýzmburek	k1gMnSc2	Rýzmburek
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
dvanáctičlenný	dvanáctičlenný	k2eAgInSc1d1	dvanáctičlenný
poradní	poradní	k2eAgInSc1d1	poradní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
rozhovory	rozhovor	k1gInPc1	rozhovor
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
basilejského	basilejský	k2eAgInSc2d1	basilejský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
účasti	účast	k1gFnSc6	účast
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
vyčíst	vyčíst	k5eAaPmF	vyčíst
z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachoval	zachovat	k5eAaPmAgMnS	zachovat
konstruktivní	konstruktivní	k2eAgInSc4d1	konstruktivní
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
usnadnit	usnadnit	k5eAaPmF	usnadnit
vítězství	vítězství	k1gNnSc4	vítězství
husitského	husitský	k2eAgInSc2d1	husitský
středu	střed	k1gInSc2	střed
a	a	k8xC	a
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
stanoviskem	stanovisko	k1gNnSc7	stanovisko
překvapil	překvapit	k5eAaPmAgMnS	překvapit
své	svůj	k3xOyFgMnPc4	svůj
dosavadní	dosavadní	k2eAgMnPc4d1	dosavadní
přátele	přítel	k1gMnPc4	přítel
i	i	k8xC	i
protivníky	protivník	k1gMnPc4	protivník
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
sirotčího	sirotčí	k2eAgNnSc2d1	sirotčí
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Prokopovi	Prokop	k1gMnSc6	Prokop
Holém	Holý	k1gMnSc6	Holý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1433	[number]	k4	1433
a	a	k8xC	a
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
k	k	k7c3	k
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rok	rok	k1gInSc1	rok
1434	[number]	k4	1434
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jakých	jaký	k3yQgInPc2	jaký
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtvrt	čtvrt	k1xP	čtvrt
roku	rok	k1gInSc2	rok
vytratilo	vytratit	k5eAaPmAgNnS	vytratit
ze	z	k7c2	z
záznamů	záznam	k1gInPc2	záznam
v	v	k7c6	v
dobových	dobový	k2eAgFnPc6d1	dobová
relacích	relace	k1gFnPc6	relace
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
indicie	indicie	k1gFnPc1	indicie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
1434	[number]	k4	1434
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostal	dostat	k5eAaPmAgMnS	dostat
mezi	mezi	k7c4	mezi
úzký	úzký	k2eAgInSc4d1	úzký
okruh	okruh	k1gInSc4	okruh
předních	přední	k2eAgMnPc2d1	přední
táborských	táborští	k1gMnPc2	táborští
představitelů	představitel	k1gMnPc2	představitel
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
blíže	blíž	k1gFnSc2	blíž
o	o	k7c6	o
příčinách	příčina	k1gFnPc6	příčina
jeho	jeho	k3xOp3gNnSc2	jeho
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
nehovoří	hovořit	k5eNaImIp3nP	hovořit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Petra	Petr	k1gMnSc2	Petr
Čorneje	Čornej	k1gMnSc2	Čornej
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
impulsem	impuls	k1gInSc7	impuls
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
kališnicko	kališnicko	k6eAd1	kališnicko
<g/>
–	–	k?	–
<g/>
katolické	katolický	k2eAgFnSc2d1	katolická
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vážně	vážně	k6eAd1	vážně
ohrozila	ohrozit	k5eAaPmAgFnS	ohrozit
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
dílo	dílo	k1gNnSc4	dílo
husitských	husitský	k2eAgInPc2d1	husitský
radikálů	radikál	k1gInPc2	radikál
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
politickou	politický	k2eAgFnSc4d1	politická
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
proti	proti	k7c3	proti
mysli	mysl	k1gFnSc3	mysl
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
husitského	husitský	k2eAgInSc2d1	husitský
středu	střed	k1gInSc2	střed
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
program	program	k1gInSc4	program
čtyř	čtyři	k4xCgInPc2	čtyři
artikulů	artikul	k1gInPc2	artikul
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kališnická	kališnický	k2eAgFnSc1d1	kališnická
šlechta	šlechta	k1gFnSc1	šlechta
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
katolíky	katolík	k1gMnPc7	katolík
proti	proti	k7c3	proti
sirotkům	sirotek	k1gMnPc3	sirotek
i	i	k8xC	i
táborům	tábor	k1gMnPc3	tábor
a	a	k8xC	a
vidina	vidina	k1gFnSc1	vidina
pokojného	pokojný	k2eAgNnSc2d1	pokojné
vyřešení	vyřešení	k1gNnSc2	vyřešení
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
setrvat	setrvat	k5eAaPmF	setrvat
u	u	k7c2	u
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgMnPc4	jenž
dosud	dosud	k6eAd1	dosud
bojoval	bojovat	k5eAaImAgMnS	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
květnových	květnový	k2eAgInPc6d1	květnový
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
táborský	táborský	k2eAgMnSc1d1	táborský
duchovní	duchovní	k1gMnSc1	duchovní
správce	správce	k1gMnSc1	správce
opět	opět	k6eAd1	opět
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dobytím	dobytí	k1gNnSc7	dobytí
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
příslušníky	příslušník	k1gMnPc7	příslušník
zemské	zemský	k2eAgFnSc2d1	zemská
hotovosti	hotovost	k1gFnSc2	hotovost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
proti	proti	k7c3	proti
radikálním	radikální	k2eAgNnPc3d1	radikální
bratrstvům	bratrstvo	k1gNnPc3	bratrstvo
svolal	svolat	k5eAaPmAgMnS	svolat
zemský	zemský	k2eAgMnSc1d1	zemský
správce	správce	k1gMnSc1	správce
Aleš	Aleš	k1gMnSc1	Aleš
Vřešťovský	Vřešťovský	k1gMnSc1	Vřešťovský
z	z	k7c2	z
Rýzmburka	Rýzmburek	k1gMnSc2	Rýzmburek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patrně	patrně	k6eAd1	patrně
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
sepsal	sepsat	k5eAaPmAgMnS	sepsat
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
informoval	informovat	k5eAaBmAgMnS	informovat
Prokopa	Prokop	k1gMnSc4	Prokop
Malého	Malý	k1gMnSc4	Malý
a	a	k8xC	a
hejtmany	hejtman	k1gMnPc4	hejtman
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
o	o	k7c6	o
ztrátě	ztráta	k1gFnSc6	ztráta
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
svolával	svolávat	k5eAaImAgMnS	svolávat
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
protiútoku	protiútok	k1gInSc3	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
ještě	ještě	k6eAd1	ještě
nemohl	moct	k5eNaImAgMnS	moct
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
Přibíkovi	Přibíkův	k2eAgMnPc1d1	Přibíkův
z	z	k7c2	z
Klenové	klenový	k2eAgNnSc1d1	Klenové
podařilo	podařit	k5eAaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
obklíčením	obklíčení	k1gNnSc7	obklíčení
a	a	k8xC	a
dodat	dodat	k5eAaPmF	dodat
obléhaným	obléhaný	k2eAgFnPc3d1	obléhaná
potřebný	potřebný	k2eAgInSc1d1	potřebný
materiál	materiál	k1gInSc4	materiál
včetně	včetně	k7c2	včetně
70	[number]	k4	70
000	[number]	k4	000
litrů	litr	k1gInPc2	litr
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnPc1	naděje
na	na	k7c4	na
brzké	brzký	k2eAgNnSc4d1	brzké
dobytí	dobytí	k1gNnSc4	dobytí
západočeské	západočeský	k2eAgFnSc2d1	Západočeská
metropole	metropol	k1gFnSc2	metropol
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
odložily	odložit	k5eAaPmAgInP	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
zprávy	zpráva	k1gFnSc2	zpráva
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
začala	začít	k5eAaPmAgNnP	začít
polní	polní	k2eAgNnPc1d1	polní
bratrstva	bratrstvo	k1gNnPc1	bratrstvo
opouštět	opouštět	k5eAaImF	opouštět
plzeňský	plzeňský	k2eAgInSc4d1	plzeňský
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
posily	posila	k1gFnPc4	posila
a	a	k8xC	a
již	již	k6eAd1	již
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
neúspěšně	úspěšně	k6eNd1	úspěšně
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Borotín	Borotín	k1gMnSc1	Borotín
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
potrestat	potrestat	k5eAaPmF	potrestat
za	za	k7c4	za
zradu	zrada	k1gFnSc4	zrada
jeho	jeho	k3xOp3gMnSc2	jeho
majitele	majitel	k1gMnSc2	majitel
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
z	z	k7c2	z
Landštejna	Landštejn	k1gInSc2	Landštejn
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
přesunul	přesunout	k5eAaPmAgMnS	přesunout
k	k	k7c3	k
Sedlčanům	Sedlčany	k1gInPc3	Sedlčany
nebo	nebo	k8xC	nebo
k	k	k7c3	k
Miličínu	Miličín	k1gInSc3	Miličín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
oddíly	oddíl	k1gInPc7	oddíl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
sem	sem	k6eAd1	sem
přitáhly	přitáhnout	k5eAaPmAgInP	přitáhnout
od	od	k7c2	od
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
táborské	táborský	k2eAgNnSc1d1	táborské
vojsko	vojsko	k1gNnSc1	vojsko
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
rozložilo	rozložit	k5eAaPmAgNnS	rozložit
poblíž	poblíž	k6eAd1	poblíž
Krče	Krč	k1gFnSc2	Krč
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sirotky	sirotek	k1gMnPc7	sirotek
Jana	Jan	k1gMnSc2	Jan
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
Sán	sán	k2eAgInSc4d1	sán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
tábor	tábor	k1gInSc1	tábor
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Jirny	Jirny	k1gInPc4	Jirny
<g/>
,	,	kIx,	,
podnikal	podnikat	k5eAaImAgMnS	podnikat
akce	akce	k1gFnSc2	akce
proti	proti	k7c3	proti
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pokusy	pokus	k1gInPc4	pokus
vylákat	vylákat	k5eAaPmF	vylákat
protivníka	protivník	k1gMnSc2	protivník
k	k	k7c3	k
ozbrojené	ozbrojený	k2eAgFnSc3d1	ozbrojená
konfrontaci	konfrontace	k1gFnSc3	konfrontace
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
nebo	nebo	k8xC	nebo
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
své	svůj	k3xOyFgMnPc4	svůj
přívržence	přívrženec	k1gMnPc4	přívrženec
uvnitř	uvnitř	k7c2	uvnitř
metropole	metropol	k1gFnSc2	metropol
k	k	k7c3	k
převratu	převrat	k1gInSc6	převrat
nebyly	být	k5eNaImAgInP	být
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
příslušníci	příslušník	k1gMnPc1	příslušník
zemské	zemský	k2eAgFnSc2d1	zemská
hotovosti	hotovost	k1gFnSc2	hotovost
zcela	zcela	k6eAd1	zcela
ovládali	ovládat	k5eAaImAgMnP	ovládat
jak	jak	k8xS	jak
Staré	Staré	k2eAgInPc1d1	Staré
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Nové	Nové	k2eAgNnSc4d1	Nové
Město	město	k1gNnSc4	město
a	a	k8xC	a
vyčkávali	vyčkávat	k5eAaImAgMnP	vyčkávat
příchodu	příchod	k1gInSc2	příchod
vojáků	voják	k1gMnPc2	voják
Oldřicha	Oldřich	k1gMnSc2	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
posil	posila	k1gFnPc2	posila
ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Karlštejnska	Karlštejnsko	k1gNnSc2	Karlštejnsko
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
těchto	tento	k3xDgInPc2	tento
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zásobovací	zásobovací	k2eAgFnPc4d1	zásobovací
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
obava	obava	k1gFnSc1	obava
z	z	k7c2	z
boje	boj	k1gInSc2	boj
s	s	k7c7	s
početnějším	početní	k2eAgMnSc7d2	početnější
protivníkem	protivník	k1gMnSc7	protivník
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
výhodných	výhodný	k2eAgFnPc6d1	výhodná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
vojska	vojsko	k1gNnPc1	vojsko
husitských	husitský	k2eAgMnPc2d1	husitský
radikálů	radikál	k1gMnPc2	radikál
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
u	u	k7c2	u
Hradešína	Hradešín	k1gInSc2	Hradešín
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
přes	přes	k7c4	přes
Přišimasy	Přišimasa	k1gFnPc4	Přišimasa
a	a	k8xC	a
Limuzy	Limuz	k1gInPc4	Limuz
na	na	k7c4	na
Kolínsko	Kolínsko	k1gNnSc4	Kolínsko
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
sirotčího	sirotčí	k2eAgInSc2d1	sirotčí
svazu	svaz	k1gInSc2	svaz
opatří	opatřit	k5eAaPmIp3nS	opatřit
proviant	proviant	k1gInSc1	proviant
i	i	k8xC	i
posily	posila	k1gFnPc1	posila
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
manévry	manévr	k1gInPc7	manévr
příslušníci	příslušník	k1gMnPc1	příslušník
zemské	zemský	k2eAgFnSc2d1	zemská
hotovosti	hotovost	k1gFnSc2	hotovost
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Divišem	Diviš	k1gMnSc7	Diviš
Bořkem	Bořek	k1gMnSc7	Bořek
z	z	k7c2	z
Miletínka	Miletínko	k1gNnSc2	Miletínko
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
k	k	k7c3	k
Českému	český	k2eAgInSc3d1	český
Brodu	Brod	k1gInSc3	Brod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
jen	jen	k9	jen
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
polní	polní	k2eAgNnPc1d1	polní
vojska	vojsko	k1gNnPc1	vojsko
vyprovokovali	vyprovokovat	k5eAaPmAgMnP	vyprovokovat
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Táboři	tábor	k1gMnPc1	tábor
a	a	k8xC	a
sirotci	sirotek	k1gMnPc1	sirotek
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
prošli	projít	k5eAaPmAgMnP	projít
Kouřimí	Kouřim	k1gFnPc2	Kouřim
a	a	k8xC	a
vozovou	vozový	k2eAgFnSc4d1	vozová
hradbu	hradba	k1gFnSc4	hradba
opřeli	opřít	k5eAaPmAgMnP	opřít
o	o	k7c4	o
Lipskou	lipský	k2eAgFnSc4d1	Lipská
horu	hora	k1gFnSc4	hora
mezi	mezi	k7c7	mezi
vesnicemi	vesnice	k1gFnPc7	vesnice
Hřiby	hřib	k1gInPc4	hřib
a	a	k8xC	a
Lipany	lipan	k1gMnPc4	lipan
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
průběhu	průběh	k1gInSc6	průběh
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
zahynul	zahynout	k5eAaPmAgMnS	zahynout
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
osud	osud	k1gInSc1	osud
se	se	k3xPyFc4	se
naplnil	naplnit	k5eAaPmAgInS	naplnit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokorně	pokorně	k6eAd1	pokorně
vyčkal	vyčkat	k5eAaPmAgMnS	vyčkat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
italský	italský	k2eAgMnSc1d1	italský
humanista	humanista	k1gMnSc1	humanista
Enea	Enea	k1gMnSc1	Enea
Silvio	Silvio	k1gMnSc1	Silvio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
jeho	jeho	k3xOp3gInSc4	jeho
skon	skon	k1gInSc4	skon
dramatičtějším	dramatický	k2eAgInSc7d2	dramatičtější
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Osoba	osoba	k1gFnSc1	osoba
a	a	k8xC	a
osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
osobností	osobnost	k1gFnPc2	osobnost
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
je	být	k5eAaImIp3nS	být
i	i	k9	i
s	s	k7c7	s
personáliemi	personálie	k1gFnPc7	personálie
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
spjata	spjat	k2eAgFnSc1d1	spjata
řada	řada	k1gFnSc1	řada
otazníků	otazník	k1gInPc2	otazník
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgInPc4	jenž
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
většinou	většinou	k6eAd1	většinou
nabízí	nabízet	k5eAaImIp3nS	nabízet
jen	jen	k6eAd1	jen
částečné	částečný	k2eAgFnSc3d1	částečná
nebo	nebo	k8xC	nebo
nejasné	jasný	k2eNgFnSc3d1	nejasná
odpovědi	odpověď	k1gFnSc3	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jakého	jaký	k3yIgInSc2	jaký
byl	být	k5eAaImAgMnS	být
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
vzezření	vzezření	k1gNnPc2	vzezření
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
Enea	Enea	k1gMnSc1	Enea
Silvio	Silvio	k1gMnSc1	Silvio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
kardinálu	kardinál	k1gMnSc3	kardinál
Carvajalovi	Carvajal	k1gMnSc3	Carvajal
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
popisu	popis	k1gInSc2	popis
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
střední	střední	k2eAgFnPc4d1	střední
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
silného	silný	k2eAgNnSc2d1	silné
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
tmavé	tmavý	k2eAgMnPc4d1	tmavý
<g/>
,	,	kIx,	,
snědé	snědý	k2eAgFnPc1d1	snědá
(	(	kIx(	(
<g/>
načernalé	načernalý	k2eAgFnPc1d1	načernalá
<g/>
)	)	kIx)	)
tváře	tvář	k1gFnPc1	tvář
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
vyhlížely	vyhlížet	k5eAaImAgFnP	vyhlížet
živé	živá	k1gFnPc4	živá
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc4d1	velké
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
byly	být	k5eAaImAgFnP	být
svědectvím	svědectví	k1gNnSc7	svědectví
jiskřivé	jiskřivý	k2eAgFnSc2d1	jiskřivá
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
bystrosti	bystrost	k1gFnPc1	bystrost
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
pohotovosti	pohotovost	k1gFnSc2	pohotovost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ostře	ostro	k6eAd1	ostro
řezaným	řezaný	k2eAgInSc7d1	řezaný
nosem	nos	k1gInSc7	nos
měl	mít	k5eAaImAgInS	mít
knír	knír	k1gInSc1	knír
nebo	nebo	k8xC	nebo
na	na	k7c6	na
bradě	brada	k1gFnSc6	brada
malou	malý	k2eAgFnSc4d1	malá
bradku	bradka	k1gFnSc4	bradka
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
měl	mít	k5eAaImAgInS	mít
tvář	tvář	k1gFnSc4	tvář
holou	holý	k2eAgFnSc4d1	holá
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Balbína	Balbín	k1gMnSc2	Balbín
<g/>
,	,	kIx,	,
dodávají	dodávat	k5eAaImIp3nP	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
nos	nos	k1gInSc1	nos
měl	mít	k5eAaImAgInS	mít
podobu	podoba	k1gFnSc4	podoba
jestřábího	jestřábí	k2eAgInSc2d1	jestřábí
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
zahnutý	zahnutý	k2eAgInSc1d1	zahnutý
<g/>
,	,	kIx,	,
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
že	že	k8xS	že
Prokop	Prokop	k1gMnSc1	Prokop
byl	být	k5eAaImAgMnS	být
podoby	podoba	k1gFnPc4	podoba
hrozné	hrozný	k2eAgFnPc4d1	hrozná
<g/>
.	.	kIx.	.
<g/>
Atributem	atribut	k1gInSc7	atribut
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holého	k2eAgMnSc2d1	Holého
odlišoval	odlišovat	k5eAaImAgMnS	odlišovat
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
militantních	militantní	k2eAgMnPc2d1	militantní
přívrženců	přívrženec	k1gMnPc2	přívrženec
kalicha	kalich	k1gInSc2	kalich
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nesporně	sporně	k6eNd1	sporně
jeho	jeho	k3xOp3gNnSc1	jeho
psychické	psychický	k2eAgNnSc1d1	psychické
založení	založení	k1gNnSc1	založení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
stála	stát	k5eAaImAgFnS	stát
výchova	výchova	k1gFnSc1	výchova
v	v	k7c6	v
patricijském	patricijský	k2eAgNnSc6d1	patricijské
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vzdělanec	vzdělanec	k1gMnSc1	vzdělanec
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
voják	voják	k1gMnSc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
také	také	k9	také
značně	značně	k6eAd1	značně
lišil	lišit	k5eAaImAgInS	lišit
i	i	k9	i
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
neokázalého	okázalý	k2eNgMnSc2d1	neokázalý
předchůdce	předchůdce	k1gMnSc2	předchůdce
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
jej	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
současníci	současník	k1gMnPc1	současník
často	často	k6eAd1	často
srovnávali	srovnávat	k5eAaImAgMnP	srovnávat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
diference	diference	k1gFnPc4	diference
patřil	patřit	k5eAaImAgInS	patřit
například	například	k6eAd1	například
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
životním	životní	k2eAgFnPc3d1	životní
radostem	radost	k1gFnPc3	radost
a	a	k8xC	a
pohodlí	pohodlí	k1gNnPc2	pohodlí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
písemných	písemný	k2eAgFnPc2d1	písemná
zpráv	zpráva	k1gFnPc2	zpráva
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nosil	nosit	k5eAaImAgInS	nosit
krátký	krátký	k2eAgInSc4d1	krátký
šat	šat	k1gInSc4	šat
potažmo	potažmo	k6eAd1	potažmo
černý	černý	k2eAgInSc4d1	černý
talár	talár	k1gInSc4	talár
podobný	podobný	k2eAgInSc4d1	podobný
šatu	šata	k1gFnSc4	šata
františkánů	františkán	k1gMnPc2	františkán
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
hrubý	hrubý	k2eAgInSc1d1	hrubý
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
však	však	k9	však
podšitý	podšitý	k2eAgInSc1d1	podšitý
drahou	drahý	k2eAgFnSc7d1	drahá
tkaninou	tkanina	k1gFnSc7	tkanina
a	a	k8xC	a
namísto	namísto	k7c2	namísto
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
stanu	stan	k1gInSc6	stan
rád	rád	k6eAd1	rád
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
pohodlí	pohodlí	k1gNnSc4	pohodlí
domů	dům	k1gInPc2	dům
či	či	k8xC	či
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
paláců	palác	k1gInPc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
přízvisko	přízvisko	k1gNnSc4	přízvisko
Holý	Holý	k1gMnSc1	Holý
snad	snad	k9	snad
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
komfortu	komfort	k1gInSc3	komfort
nenosil	nosit	k5eNaImAgMnS	nosit
plnovous	plnovous	k1gInSc4	plnovous
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
starozákonních	starozákonní	k2eAgMnPc2d1	starozákonní
proroků	prorok	k1gMnPc2	prorok
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
táborských	táborský	k2eAgMnPc2d1	táborský
kněží	kněz	k1gMnPc2	kněz
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc4	svůj
tvář	tvář	k1gFnSc4	tvář
zdobil	zdobit	k5eAaImAgInS	zdobit
jen	jen	k9	jen
knírem	knír	k1gInSc7	knír
nebo	nebo	k8xC	nebo
bradkou	bradka	k1gFnSc7	bradka
<g/>
.	.	kIx.	.
</s>
<s>
Prostotu	prostota	k1gFnSc4	prostota
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
konzervativním	konzervativní	k2eAgNnSc7d1	konzervativní
okolím	okolí	k1gNnSc7	okolí
patrně	patrně	k6eAd1	patrně
nucen	nutit	k5eAaImNgMnS	nutit
jen	jen	k6eAd1	jen
předstírat	předstírat	k5eAaImF	předstírat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c4	v
soukromí	soukromí	k1gNnSc4	soukromí
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgMnS	počínat
jako	jako	k9	jako
příslušník	příslušník	k1gMnSc1	příslušník
panského	panský	k2eAgInSc2d1	panský
stavu	stav	k1gInSc2	stav
nebo	nebo	k8xC	nebo
patriciátu	patriciát	k1gInSc2	patriciát
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
jeho	jeho	k3xOp3gNnSc4	jeho
chování	chování	k1gNnSc4	chování
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
když	když	k8xS	když
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
namísto	namísto	k7c2	namísto
účasti	účast	k1gFnSc2	účast
na	na	k7c4	na
slyšení	slyšení	k1gNnSc4	slyšení
u	u	k7c2	u
koncilu	koncil	k1gInSc2	koncil
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
návštěvě	návštěva	k1gFnSc3	návštěva
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
se	se	k3xPyFc4	se
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
výrazně	výrazně	k6eAd1	výrazně
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
rukou	ruka	k1gFnSc7	ruka
neprolil	prolít	k5eNaPmAgMnS	prolít
ani	ani	k8xC	ani
krůpěj	krůpěj	k1gFnSc4	krůpěj
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
míň	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
někoho	někdo	k3yInSc4	někdo
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Respekt	respekt	k1gInSc1	respekt
u	u	k7c2	u
zkušených	zkušený	k2eAgMnPc2d1	zkušený
válečníků	válečník	k1gMnPc2	válečník
i	i	k9	i
v	v	k7c6	v
politických	politický	k2eAgInPc6d1	politický
kruzích	kruh	k1gInPc6	kruh
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
zajistila	zajistit	k5eAaPmAgFnS	zajistit
jeho	jeho	k3xOp3gFnSc1	jeho
věrnost	věrnost	k1gFnSc1	věrnost
svým	svůj	k3xOyFgFnPc3	svůj
zásadám	zásada	k1gFnPc3	zásada
<g/>
,	,	kIx,	,
neúplatnost	neúplatnost	k1gFnSc4	neúplatnost
a	a	k8xC	a
zejména	zejména	k9	zejména
vůdčí	vůdčí	k2eAgFnPc4d1	vůdčí
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
František	František	k1gMnSc1	František
Šmahel	Šmahel	k1gMnSc1	Šmahel
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
přistupoval	přistupovat	k5eAaImAgMnS	přistupovat
k	k	k7c3	k
zjevnému	zjevný	k2eAgInSc3d1	zjevný
rozporu	rozpor	k1gInSc3	rozpor
mezi	mezi	k7c7	mezi
ideálním	ideální	k2eAgNnSc7d1	ideální
posláním	poslání	k1gNnSc7	poslání
a	a	k8xC	a
mocenskými	mocenský	k2eAgInPc7d1	mocenský
prostředky	prostředek	k1gInPc7	prostředek
husitského	husitský	k2eAgInSc2d1	husitský
programu	program	k1gInSc2	program
s	s	k7c7	s
nadhledem	nadhled	k1gInSc7	nadhled
reálně	reálně	k6eAd1	reálně
uvažujícího	uvažující	k2eAgNnSc2d1	uvažující
politika	politikum	k1gNnSc2	politikum
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
neustále	neustále	k6eAd1	neustále
musel	muset	k5eAaImAgMnS	muset
vyvažovat	vyvažovat	k5eAaImF	vyvažovat
protichůdné	protichůdný	k2eAgInPc4d1	protichůdný
tlaky	tlak	k1gInPc4	tlak
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
odrážely	odrážet	k5eAaImAgFnP	odrážet
nejen	nejen	k6eAd1	nejen
charakterové	charakterový	k2eAgFnPc1d1	charakterová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kolektivní	kolektivní	k2eAgInPc4d1	kolektivní
zájmy	zájem	k1gInPc4	zájem
či	či	k8xC	či
postoje	postoj	k1gInPc4	postoj
širších	široký	k2eAgFnPc2d2	širší
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Upokojit	upokojit	k5eAaPmF	upokojit
zaslepené	zaslepený	k2eAgInPc4d1	zaslepený
a	a	k8xC	a
hašteřivé	hašteřivý	k2eAgMnPc4d1	hašteřivý
bohoslovce	bohoslovec	k1gMnPc4	bohoslovec
bylo	být	k5eAaImAgNnS	být
právě	právě	k6eAd1	právě
tak	tak	k6eAd1	tak
obtížné	obtížný	k2eAgNnSc1d1	obtížné
jako	jako	k8xS	jako
vyjít	vyjít	k5eAaPmF	vyjít
vstříc	vstříc	k6eAd1	vstříc
kořistnickým	kořistnický	k2eAgFnPc3d1	kořistnická
choutkám	choutka	k1gFnPc3	choutka
některých	některý	k3yIgMnPc2	některý
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
bedlivě	bedlivě	k6eAd1	bedlivě
musel	muset	k5eAaImAgInS	muset
sledovat	sledovat	k5eAaImF	sledovat
urozené	urozený	k2eAgMnPc4d1	urozený
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
někdy	někdy	k6eAd1	někdy
kalichu	kalich	k1gInSc6	kalich
prokazovali	prokazovat	k5eAaImAgMnP	prokazovat
cenné	cenný	k2eAgFnPc4d1	cenná
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
ho	on	k3xPp3gMnSc4	on
zrazovali	zrazovat	k5eAaImAgMnP	zrazovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Patrně	patrně	k6eAd1	patrně
výchova	výchova	k1gFnSc1	výchova
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
jeho	jeho	k3xOp3gFnSc2	jeho
adoptivní	adoptivní	k2eAgFnSc2d1	adoptivní
rodiny	rodina	k1gFnSc2	rodina
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jasně	jasně	k6eAd1	jasně
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
důležité	důležitý	k2eAgInPc1d1	důležitý
jsou	být	k5eAaImIp3nP	být
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
styky	styk	k1gInPc4	styk
i	i	k8xC	i
vazby	vazba	k1gFnPc4	vazba
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
myšlenky	myšlenka	k1gFnSc2	myšlenka
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
měřítku	měřítko	k1gNnSc6	měřítko
byly	být	k5eAaImAgInP	být
naprostým	naprostý	k2eAgInSc7d1	naprostý
základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnSc2	jeho
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
osobnostním	osobnostní	k2eAgInSc7d1	osobnostní
rysem	rys	k1gInSc7	rys
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nechuť	nechuť	k1gFnSc1	nechuť
k	k	k7c3	k
teologickým	teologický	k2eAgFnPc3d1	teologická
třenicím	třenice	k1gFnPc3	třenice
a	a	k8xC	a
nevíra	nevíra	k1gFnSc1	nevíra
ve	v	k7c4	v
všemohoucnost	všemohoucnost	k1gFnSc4	všemohoucnost
náboženských	náboženský	k2eAgInPc2d1	náboženský
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
i	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
otázkách	otázka	k1gFnPc6	otázka
projevoval	projevovat	k5eAaImAgInS	projevovat
jasně	jasně	k6eAd1	jasně
politický	politický	k2eAgInSc4d1	politický
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prokázal	prokázat	k5eAaPmAgInS	prokázat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1423	[number]	k4	1423
u	u	k7c2	u
Konopiště	Konopiště	k1gNnSc2	Konopiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
Basileji	Basilej	k1gFnSc6	Basilej
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Leccos	leccos	k3yInSc1	leccos
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
svátosti	svátost	k1gFnSc3	svátost
posledního	poslední	k2eAgNnSc2d1	poslední
pomazání	pomazání	k1gNnSc2	pomazání
(	(	kIx(	(
<g/>
svatého	svatý	k2eAgInSc2d1	svatý
oleje	olej	k1gInSc2	olej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
požívala	požívat	k5eAaImAgFnS	požívat
u	u	k7c2	u
katolíků	katolík	k1gMnPc2	katolík
i	i	k8xC	i
Pražanů	Pražan	k1gMnPc2	Pražan
nebeské	nebeský	k2eAgFnSc2d1	nebeská
úcty	úcta	k1gFnSc2	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
střelou	střela	k1gFnSc7	střela
<g/>
,	,	kIx,	,
neváhal	váhat	k5eNaImAgMnS	váhat
oleje	olej	k1gInPc4	olej
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
desinfekčního	desinfekční	k2eAgInSc2d1	desinfekční
prostředku	prostředek	k1gInSc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Příbrami	Příbram	k1gFnSc2	Příbram
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
skutku	skutek	k1gInSc3	skutek
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Co	co	k3yQnSc1	co
Holý	Holý	k1gMnSc1	Holý
Prokop	Prokop	k1gMnSc1	Prokop
řekl	říct	k5eAaPmAgMnS	říct
o	o	k7c6	o
té	ten	k3xDgFnSc6	ten
svátosti	svátost	k1gFnSc6	svátost
<g/>
.	.	kIx.	.
</s>
<s>
Item	Item	k6eAd1	Item
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
rúhaje	rúhaj	k1gInSc2	rúhaj
sě	sě	k?	sě
té	ten	k3xDgFnSc6	ten
svátosti	svátost	k1gFnSc6	svátost
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
–	–	k?	–
co	co	k9	co
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
toho	ten	k3xDgNnSc2	ten
oleje	olej	k1gInSc2	olej
na	na	k7c4	na
svů	svů	k?	svů
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
poroka	poroek	k1gInSc2	poroek
<g/>
,	,	kIx,	,
řiť	řiť	k1gFnSc4	řiť
vymazal	vymazat	k5eAaPmAgMnS	vymazat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
postřelen	postřelen	k2eAgMnSc1d1	postřelen
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
Zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
tím	ten	k3xDgNnSc7	ten
dal	dát	k5eAaPmAgInS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
cení	cenit	k5eAaImIp3nS	cenit
více	hodně	k6eAd2	hodně
života	život	k1gInSc2	život
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
průchod	průchod	k1gInSc1	průchod
zdravému	zdravý	k2eAgInSc3d1	zdravý
rozumu	rozum	k1gInSc3	rozum
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
strachoval	strachovat	k5eAaImAgMnS	strachovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gNnSc4	on
stihnou	stihnout	k5eAaPmIp3nP	stihnout
pekelné	pekelný	k2eAgInPc1d1	pekelný
tresty	trest	k1gInPc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Prokopovi	Prokop	k1gMnSc6	Prokop
Holém	Holý	k1gMnSc6	Holý
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
proslýchalo	proslýchat	k5eAaImAgNnS	proslýchat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
ženat	ženat	k2eAgMnSc1d1	ženat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
manželství	manželství	k1gNnSc6	manželství
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
nepodařilo	podařit	k5eNaPmAgNnS	podařit
ani	ani	k9	ani
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jakého	jaký	k3yIgInSc2	jaký
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
kvalifikovaný	kvalifikovaný	k2eAgInSc1d1	kvalifikovaný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
basilejských	basilejský	k2eAgFnPc6d1	Basilejská
klášterních	klášterní	k2eAgFnPc6d1	klášterní
knihovnách	knihovna	k1gFnPc6	knihovna
však	však	k9	však
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
značné	značný	k2eAgFnPc4d1	značná
náklonnosti	náklonnost	k1gFnPc4	náklonnost
ke	k	k7c3	k
vzdělanosti	vzdělanost	k1gFnSc3	vzdělanost
samotné	samotný	k2eAgFnSc3d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
řadovými	řadový	k2eAgMnPc7d1	řadový
vojáky	voják	k1gMnPc7	voják
nejednou	jednou	k6eNd1	jednou
obviňován	obviňován	k2eAgInSc1d1	obviňován
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
obohacování	obohacování	k1gNnSc2	obohacování
a	a	k8xC	a
nesprávného	správný	k2eNgNnSc2d1	nesprávné
dělení	dělení	k1gNnSc2	dělení
kořisti	kořist	k1gFnSc2	kořist
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
výhrady	výhrada	k1gFnPc1	výhrada
patrně	patrně	k6eAd1	patrně
nepostrádaly	postrádat	k5eNaImAgFnP	postrádat
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
věrnost	věrnost	k1gFnSc1	věrnost
odkazu	odkaz	k1gInSc2	odkaz
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
nezištnou	zištný	k2eNgFnSc4d1	nezištná
neústupnost	neústupnost	k1gFnSc4	neústupnost
zásadám	zásada	k1gFnPc3	zásada
za	za	k7c4	za
něž	jenž	k3xRgNnSc4	jenž
bojoval	bojovat	k5eAaImAgMnS	bojovat
prokázal	prokázat	k5eAaPmAgMnS	prokázat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1434	[number]	k4	1434
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
zradě	zrada	k1gFnSc6	zrada
husitských	husitský	k2eAgInPc2d1	husitský
ideálů	ideál	k1gInPc2	ideál
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
a	a	k8xC	a
utrakvistické	utrakvistický	k2eAgFnSc2d1	utrakvistická
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
volil	volit	k5eAaImAgInS	volit
raději	rád	k6eAd2	rád
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
nasnadě	nasnadě	k6eAd1	nasnadě
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
riskuje	riskovat	k5eAaBmIp3nS	riskovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
Petr	Petr	k1gMnSc1	Petr
Čornej	Čornej	k1gMnSc1	Čornej
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
jednání	jednání	k1gNnSc3	jednání
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jen	jen	k9	jen
málo	málo	k4c4	málo
příkladů	příklad	k1gInPc2	příklad
takové	takový	k3xDgFnSc2	takový
osobní	osobní	k2eAgFnSc2d1	osobní
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
politik	politik	k1gMnSc1	politik
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
formátu	formát	k1gInSc2	formát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
i	i	k9	i
po	po	k7c6	po
případné	případný	k2eAgFnSc6d1	případná
změně	změna	k1gFnSc6	změna
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
odvrhne	odvrhnout	k5eAaPmIp3nS	odvrhnout
osobní	osobní	k2eAgInPc4d1	osobní
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
zapomene	zapomenout	k5eAaPmIp3nS	zapomenout
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
spálí	spálit	k5eAaPmIp3nP	spálit
za	za	k7c7	za
sebou	se	k3xPyFc7	se
všechny	všechen	k3xTgInPc4	všechen
mosty	most	k1gInPc4	most
a	a	k8xC	a
nasadí	nasadit	k5eAaPmIp3nS	nasadit
život	život	k1gInSc4	život
v	v	k7c6	v
takřka	takřka	k6eAd1	takřka
již	již	k6eAd1	již
prohraném	prohraný	k2eAgInSc6d1	prohraný
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Věrnost	věrnost	k1gFnSc1	věrnost
poznané	poznaný	k2eAgFnSc2d1	poznaná
pravdě	pravda	k1gFnSc3	pravda
a	a	k8xC	a
pohotovost	pohotovost	k1gFnSc1	pohotovost
k	k	k7c3	k
mučednictví	mučednictví	k1gNnSc3	mučednictví
spojuje	spojovat	k5eAaImIp3nS	spojovat
Prokopa	Prokop	k1gMnSc4	Prokop
s	s	k7c7	s
jeho	jeho	k3xOp3gNnPc7	jeho
neméně	málo	k6eNd2	málo
velkými	velký	k2eAgMnPc7d1	velký
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
:	:	kIx,	:
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
a	a	k8xC	a
Jeronýmem	Jeroným	k1gMnSc7	Jeroným
Pražským	pražský	k2eAgMnSc7d1	pražský
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
a	a	k8xC	a
odkaz	odkaz	k1gInSc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
Janu	Jan	k1gMnSc3	Jan
Žižkovi	Žižka	k1gMnSc3	Žižka
zanechala	zanechat	k5eAaPmAgFnS	zanechat
historická	historický	k2eAgFnSc1d1	historická
obec	obec	k1gFnSc1	obec
o	o	k7c6	o
jeho	jeho	k3xOp3gMnSc6	jeho
následníkovi	následník	k1gMnSc6	následník
Prokopovi	Prokop	k1gMnSc6	Prokop
Holém	Holý	k1gMnSc6	Holý
jen	jen	k6eAd1	jen
skromné	skromný	k2eAgNnSc4d1	skromné
množství	množství	k1gNnSc4	množství
literárních	literární	k2eAgFnPc2d1	literární
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
moderní	moderní	k2eAgFnSc7d1	moderní
českou	český	k2eAgFnSc7d1	Česká
biografií	biografie	k1gFnSc7	biografie
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kniha	kniha	k1gFnSc1	kniha
historika	historik	k1gMnSc2	historik
Josefa	Josef	k1gMnSc2	Josef
Macka	Macek	k1gMnSc2	Macek
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
silně	silně	k6eAd1	silně
poznamenaná	poznamenaný	k2eAgFnSc1d1	poznamenaná
autorovým	autorův	k2eAgNnSc7d1	autorovo
raným	raný	k2eAgNnSc7d1	rané
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
smýšlením	smýšlení	k1gNnSc7	smýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
faktografické	faktografický	k2eAgFnPc1d1	faktografická
kapitoly	kapitola	k1gFnPc1	kapitola
nebo	nebo	k8xC	nebo
populární	populární	k2eAgInPc1d1	populární
články	článek	k1gInPc1	článek
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
větších	veliký	k2eAgFnPc2d2	veliký
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
studie	studie	k1gFnSc1	studie
A.	A.	kA	A.
Neubauera	Neubauer	k1gMnSc2	Neubauer
otištěná	otištěný	k2eAgNnPc1d1	otištěné
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
časopisu	časopis	k1gInSc6	časopis
historickém	historický	k2eAgInSc6d1	historický
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapitola	kapitola	k1gFnSc1	kapitola
Ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
Žižkových	Žižková	k1gFnPc2	Žižková
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Kosiny	Kosina	k1gMnSc2	Kosina
Velikáni	velikán	k1gMnPc1	velikán
našich	náš	k3xOp1gFnPc2	náš
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
knihy	kniha	k1gFnSc2	kniha
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Urbánka	Urbánek	k1gMnSc2	Urbánek
Lipany	lipan	k1gMnPc4	lipan
a	a	k8xC	a
konec	konec	k1gInSc4	konec
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
otázka	otázka	k1gFnSc1	otázka
a	a	k8xC	a
článek	článek	k1gInSc1	článek
historika	historik	k1gMnSc2	historik
Františka	František	k1gMnSc2	František
Michálka	Michálek	k1gMnSc2	Michálek
Bartoše	Bartoš	k1gMnSc2	Bartoš
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Světci	světec	k1gMnPc1	světec
a	a	k8xC	a
kacíři	kacíř	k1gMnPc1	kacíř
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prací	práce	k1gFnPc2	práce
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
životem	život	k1gInSc7	život
táborského	táborský	k2eAgMnSc2d1	táborský
duchovního	duchovní	k1gMnSc2	duchovní
správce	správce	k1gMnSc2	správce
zabývá	zabývat	k5eAaImIp3nS	zabývat
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
výše	vysoce	k6eAd2	vysoce
citovaná	citovaný	k2eAgFnSc1d1	citovaná
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
od	od	k7c2	od
Enea	Ene	k2eAgFnSc1d1	Enea
Silvia	Silvia	k1gFnSc1	Silvia
Piccolominiho	Piccolomini	k1gMnSc2	Piccolomini
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc2d2	pozdější
práce	práce	k1gFnSc2	práce
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
Theobalda	Theobald	k1gMnSc2	Theobald
(	(	kIx(	(
<g/>
Hussitenkrieg	Hussitenkrieg	k1gInSc1	Hussitenkrieg
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Jacquese	Jacques	k1gMnSc2	Jacques
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
enfanta	enfant	k1gMnSc2	enfant
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
la	la	k0	la
guerre	guerr	k1gInSc5	guerr
des	des	k1gNnSc7	des
Hussites	Hussites	k1gInSc1	Hussites
et	et	k?	et
du	du	k?	du
concile	concile	k6eAd1	concile
de	de	k?	de
Basle	Basle	k1gInSc1	Basle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
osvícenec	osvícenec	k1gMnSc1	osvícenec
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wolf	Wolf	k1gMnSc1	Wolf
anonymně	anonymně	k6eAd1	anonymně
knihu	kniha	k1gFnSc4	kniha
Leben	Lebna	k1gFnPc2	Lebna
der	drát	k5eAaImRp2nS	drát
beiden	beidno	k1gNnPc2	beidno
Prokope	prokopat	k5eAaPmIp3nS	prokopat
(	(	kIx(	(
<g/>
Život	život	k1gInSc4	život
obou	dva	k4xCgMnPc2	dva
Prokopů	Prokop	k1gMnPc2	Prokop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
nadšeně	nadšeně	k6eAd1	nadšeně
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
českým	český	k2eAgMnSc7d1	český
Hanibalem	Hanibal	k1gMnSc7	Hanibal
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
českým	český	k2eAgMnSc7d1	český
lvem	lev	k1gMnSc7	lev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
oslava	oslava	k1gFnSc1	oslava
husitů	husita	k1gMnPc2	husita
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
George	Georg	k1gMnSc2	Georg
Sandové	Sandová	k1gFnSc2	Sandová
s	s	k7c7	s
názvem	název	k1gInSc7	název
Procope	Procop	k1gInSc5	Procop
le	le	k?	le
Grand	grand	k1gMnSc1	grand
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
hodnotila	hodnotit	k5eAaImAgFnS	hodnotit
Prokopa	Prokop	k1gMnSc4	Prokop
Holého	Holý	k1gMnSc4	Holý
jako	jako	k8xS	jako
bojovníka	bojovník	k1gMnSc4	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
sociální	sociální	k2eAgInSc4d1	sociální
pokrok	pokrok	k1gInSc4	pokrok
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
podání	podání	k1gNnSc6	podání
byl	být	k5eAaImAgInS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
postav	postava	k1gFnPc2	postava
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
Ale	ale	k9	ale
Prokop	Prokop	k1gMnSc1	Prokop
stál	stát	k5eAaImAgMnS	stát
vzpřímen	vzpřímen	k2eAgInSc4d1	vzpřímen
uprostřed	uprostřed	k7c2	uprostřed
svých	svůj	k3xOyFgInPc2	svůj
hrdých	hrdý	k2eAgInPc2d1	hrdý
táborů	tábor	k1gInPc2	tábor
<g/>
;	;	kIx,	;
Prokop	Prokop	k1gMnSc1	Prokop
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
zbabělé	zbabělý	k2eAgFnSc3d1	zbabělá
úmluvě	úmluva	k1gFnSc3	úmluva
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Prokop	Prokop	k1gMnSc1	Prokop
padl	padnout	k5eAaPmAgMnS	padnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
mohli	moct	k5eAaImAgMnP	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přes	přes	k7c4	přes
mrtvolu	mrtvola	k1gFnSc4	mrtvola
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
ani	ani	k8xC	ani
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
české	český	k2eAgFnSc2d1	Česká
historiografie	historiografie	k1gFnSc2	historiografie
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
Dějinách	dějiny	k1gFnPc6	dějiny
národu	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
Moravě	Morava	k1gFnSc6	Morava
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nerovnal	rovnat	k5eNaImAgMnS	rovnat
<g/>
-li	i	k?	-li
se	s	k7c7	s
Žižkovi	Žižka	k1gMnSc3	Žižka
geniálností	geniálnost	k1gFnSc7	geniálnost
válečnickou	válečnický	k2eAgFnSc7d1	válečnická
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
jej	on	k3xPp3gInSc4	on
předčil	předčit	k5eAaBmAgInS	předčit
politickým	politický	k2eAgInSc7d1	politický
rozhledem	rozhled	k1gInSc7	rozhled
<g/>
.	.	kIx.	.
</s>
<s>
Uznával	uznávat	k5eAaImAgMnS	uznávat
Prokopovo	Prokopův	k2eAgNnSc4d1	Prokopovo
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postrádal	postrádat	k5eAaImAgInS	postrádat
jasné	jasný	k2eAgFnPc4d1	jasná
hranice	hranice	k1gFnPc4	hranice
působnosti	působnost	k1gFnSc2	působnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
národ	národ	k1gInSc1	národ
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
a	a	k8xC	a
rozsápaný	rozsápaný	k2eAgInSc1d1	rozsápaný
ve	v	k7c4	v
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
roty	rota	k1gFnPc4	rota
<g/>
,	,	kIx,	,
neznal	znát	k5eNaImAgInS	znát
více	hodně	k6eAd2	hodně
organické	organický	k2eAgFnSc2d1	organická
jednoty	jednota	k1gFnSc2	jednota
celku	celek	k1gInSc2	celek
svého	své	k1gNnSc2	své
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hmotné	hmotný	k2eAgFnPc4d1	hmotná
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
připomínat	připomínat	k5eAaImF	připomínat
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
budoucím	budoucí	k2eAgFnPc3d1	budoucí
generacím	generace	k1gFnPc3	generace
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
realizaci	realizace	k1gFnSc6	realizace
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
popularity	popularita	k1gFnPc4	popularita
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sice	sice	k8xC	sice
katolík	katolík	k1gMnSc1	katolík
Pavel	Pavel	k1gMnSc1	Pavel
Žídek	Žídek	k1gMnSc1	Žídek
radil	radit	k5eAaImAgMnS	radit
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
padlému	padlý	k2eAgMnSc3d1	padlý
táborskému	táborský	k2eAgMnSc3d1	táborský
duchovnímu	duchovní	k1gMnSc3	duchovní
správci	správce	k1gMnSc3	správce
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
druhům	druh	k1gMnPc3	druh
postavil	postavit	k5eAaPmAgInS	postavit
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
kostelík	kostelík	k1gInSc4	kostelík
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
stavbě	stavba	k1gFnSc3	stavba
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nebyla	být	k5eNaImAgFnS	být
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
zahájena	zahájen	k2eAgFnSc1d1	zahájena
stavba	stavba	k1gFnSc1	stavba
jiného	jiný	k2eAgInSc2d1	jiný
pamětního	pamětní	k2eAgInSc2d1	pamětní
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Balbína	Balbín	k1gMnSc2	Balbín
stávala	stávat	k5eAaImAgFnS	stávat
jeho	jeho	k3xOp3gFnSc1	jeho
kovová	kovový	k2eAgFnSc1d1	kovová
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
mostě	most	k1gInSc6	most
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgMnSc1	týž
autor	autor	k1gMnSc1	autor
zároveň	zároveň	k6eAd1	zároveň
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Karlově	Karlův	k2eAgFnSc6d1	Karlova
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgMnS	být
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
vymalován	vymalován	k2eAgMnSc1d1	vymalován
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
domu	dům	k1gInSc2	dům
"	"	kIx"	"
<g/>
U	u	k7c2	u
Adama	Adam	k1gMnSc2	Adam
a	a	k8xC	a
Evy	Eva	k1gFnSc2	Eva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
čp.	čp.	k?	čp.
146	[number]	k4	146
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1868	[number]	k4	1868
se	se	k3xPyFc4	se
na	na	k7c6	na
Lipské	lipský	k2eAgFnSc6d1	Lipská
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
u	u	k7c2	u
smrku	smrk	k1gInSc2	smrk
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgMnSc7	jenž
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
scházet	scházet	k5eAaImF	scházet
mohutné	mohutný	k2eAgFnSc2d1	mohutná
politické	politický	k2eAgFnSc2d1	politická
manifestace	manifestace	k1gFnSc2	manifestace
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
přimět	přimět	k5eAaPmF	přimět
rakouský	rakouský	k2eAgInSc4d1	rakouský
dvůr	dvůr	k1gInSc4	dvůr
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
historického	historický	k2eAgNnSc2d1	historické
státního	státní	k2eAgNnSc2d1	státní
práva	právo	k1gNnSc2	právo
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
příslušníků	příslušník	k1gMnPc2	příslušník
těchto	tento	k3xDgFnPc2	tento
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
organizovaných	organizovaný	k2eAgInPc2d1	organizovaný
ve	v	k7c6	v
spolku	spolek	k1gInSc6	spolek
"	"	kIx"	"
<g/>
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vzešla	vzejít	k5eAaPmAgFnS	vzejít
iniciativa	iniciativa	k1gFnSc1	iniciativa
vybudování	vybudování	k1gNnSc2	vybudování
Prokopovy	Prokopův	k2eAgFnSc2d1	Prokopova
mohyly	mohyla	k1gFnSc2	mohyla
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Lipské	lipský	k2eAgFnSc6d1	Lipská
hoře	hora	k1gFnSc6	hora
zahájena	zahájit	k5eAaPmNgFnS	zahájit
i	i	k8xC	i
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
samostatné	samostatný	k2eAgInPc1d1	samostatný
památníky	památník	k1gInPc1	památník
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
socha	socha	k1gFnSc1	socha
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
Karla	Karel	k1gMnSc2	Karel
Opatrného	opatrný	k2eAgMnSc2d1	opatrný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1908	[number]	k4	1908
až	až	k9	až
1910	[number]	k4	1910
na	na	k7c6	na
Husově	Husův	k2eAgNnSc6d1	Husovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Brodě	Brod	k1gInSc6	Brod
a	a	k8xC	a
pamětní	pamětní	k2eAgInSc1d1	pamětní
kámen	kámen	k1gInSc1	kámen
na	na	k7c6	na
Mírovém	mírový	k2eAgNnSc6d1	Mírové
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Kouřimi	Kouřim	k1gFnSc6	Kouřim
odhalený	odhalený	k2eAgInSc1d1	odhalený
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Biografie	biografie	k1gFnSc2	biografie
====	====	k?	====
</s>
</p>
<p>
<s>
BARTOŠ	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
Světci	světec	k1gMnPc1	světec
a	a	k8xC	a
kacíři	kacíř	k1gMnPc1	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Husova	Husův	k2eAgFnSc1d1	Husova
československá	československý	k2eAgFnSc1d1	Československá
evangelická	evangelický	k2eAgFnSc1d1	evangelická
fakulta	fakulta	k1gFnSc1	fakulta
bohoslovecká	bohoslovecký	k2eAgFnSc1d1	bohoslovecká
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
334	[number]	k4	334
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KOSINA	Kosina	k1gMnSc1	Kosina
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Velikáni	velikán	k1gMnPc1	velikán
našich	náš	k3xOp1gFnPc2	náš
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
555	[number]	k4	555
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
211	[number]	k4	211
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SANDOVÁ	SANDOVÁ	kA	SANDOVÁ
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Procope	Procop	k1gMnSc5	Procop
le	le	k?	le
Grand	grand	k1gMnSc1	grand
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
:	:	kIx,	:
M.	M.	kA	M.
Lévy	Léva	k1gFnSc2	Léva
<g/>
,	,	kIx,	,
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
46	[number]	k4	46
s.	s.	k?	s.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
URBÁNEK	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Lipany	lipan	k1gMnPc4	lipan
a	a	k8xC	a
konec	konec	k1gInSc4	konec
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gInSc1	Melantrich
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
262	[number]	k4	262
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
567	[number]	k4	567
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
/	/	kIx~	/
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Augusta	Augusta	k1gMnSc1	Augusta
...	...	k?	...
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
324	[number]	k4	324
<g/>
–	–	k?	–
<g/>
325	[number]	k4	325
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Prameny	pramen	k1gInPc1	pramen
====	====	k?	====
</s>
</p>
<p>
<s>
SILVIO	SILVIO	kA	SILVIO
<g/>
,	,	kIx,	,
Enea	Eneum	k1gNnSc2	Eneum
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
:	:	kIx,	:
Dialog	dialog	k1gInSc1	dialog
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7382	[number]	k4	7382
<g/>
-	-	kIx~	-
<g/>
136	[number]	k4	136
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
THEOBALD	THEOBALD	kA	THEOBALD
<g/>
,	,	kIx,	,
Zacharias	Zacharias	k1gInSc1	Zacharias
<g/>
.	.	kIx.	.
</s>
<s>
Hussitenkrieg	Hussitenkrieg	k1gMnSc1	Hussitenkrieg
<g/>
.	.	kIx.	.
</s>
<s>
Hildesheim	Hildesheim	k1gInSc1	Hildesheim
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Georg	Georg	k1gInSc1	Georg
Olms	Olmsa	k1gFnPc2	Olmsa
Verlag	Verlaga	k1gFnPc2	Verlaga
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
487	[number]	k4	487
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6973	[number]	k4	6973
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽATECKÝ	žatecký	k2eAgMnSc1d1	žatecký
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
Petra	Petr	k1gMnSc2	Petr
Žateckého	žatecký	k2eAgMnSc2d1	žatecký
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
292	[number]	k4	292
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prameny	pramen	k1gInPc1	pramen
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V	v	k7c6	v
komisi	komise	k1gFnSc6	komise
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Grégr	Grégr	k1gMnSc1	Grégr
a	a	k8xC	a
Ferd	Ferd	k1gMnSc1	Ferd
<g/>
.	.	kIx.	.
</s>
<s>
Dattel	Dattel	k1gInSc1	Dattel
258	[number]	k4	258
s.	s.	k?	s.
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Kronika	kronika	k1gFnSc1	kronika
starého	starý	k2eAgMnSc2d1	starý
kolegiáta	kolegiát	k1gMnSc2	kolegiát
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
kronik	kronika	k1gFnPc2	kronika
doby	doba	k1gFnSc2	doba
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
491	[number]	k4	491
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Kronika	kronika	k1gFnSc1	kronika
Bartoška	Bartošek	k1gMnSc2	Bartošek
z	z	k7c2	z
Drahonic	Drahonice	k1gFnPc2	Drahonice
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
580	[number]	k4	580
s.	s.	k?	s.
</s>
</p>
<p>
<s>
====	====	k?	====
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
literatura	literatura	k1gFnSc1	literatura
====	====	k?	====
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Lipanská	lipanský	k2eAgFnSc1d1	Lipanská
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
277	[number]	k4	277
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
261	[number]	k4	261
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
383	[number]	k4	383
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Lipanské	lipanský	k2eAgFnPc1d1	Lipanská
ozvěny	ozvěna	k1gFnPc1	ozvěna
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
:	:	kIx,	:
H	H	kA	H
&	&	k?	&
H	H	kA	H
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
203	[number]	k4	203
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85787	[number]	k4	85787
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
V.	V.	kA	V.
1402	[number]	k4	1402
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
790	[number]	k4	790
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
296	[number]	k4	296
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FRANKENBERGER	FRANKENBERGER	kA	FRANKENBERGER
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
velká	velký	k2eAgFnSc1d1	velká
armáda	armáda	k1gFnSc1	armáda
-	-	kIx~	-
díl	díl	k1gInSc1	díl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
156	[number]	k4	156
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FRANKENBERGER	FRANKENBERGER	kA	FRANKENBERGER
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
velká	velký	k2eAgFnSc1d1	velká
armáda	armáda	k1gFnSc1	armáda
-	-	kIx~	-
díl	díl	k1gInSc1	díl
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
188	[number]	k4	188
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PALACKÝ	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
národu	národ	k1gInSc2	národ
Českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Tempský	Tempský	k2eAgMnSc1d1	Tempský
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
402	[number]	k4	402
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
420	[number]	k4	420
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMAN	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
válečnictví	válečnictví	k1gNnSc1	válečnictví
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
Žižkovy	Žižkův	k2eAgFnSc2d1	Žižkova
a	a	k8xC	a
Prokopovy	Prokopův	k2eAgFnSc2d1	Prokopova
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nákladem	náklad	k1gInSc7	náklad
jubilejního	jubilejní	k2eAgInSc2d1	jubilejní
fondu	fond	k1gInSc2	fond
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
Společnosti	společnost	k1gFnPc1	společnost
Nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
468	[number]	k4	468
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Prokop	Prokop	k1gMnSc1	Prokop
Holý	holý	k2eAgMnSc1d1	holý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
