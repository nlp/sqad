<s>
Prokop	Prokop	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
též	též	k9
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS
Prokop	Prokop	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
(	(	kIx(
<g/>
asi	asi	k9
1380	[number]	k4
–	–	k?
30	[number]	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1434	[number]	k4
<g/>
,	,	kIx,
Lipany	lipan	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
radikální	radikální	k2eAgMnSc1d1
husitský	husitský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
vojevůdce	vojevůdce	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
duchovním	duchovní	k2eAgMnSc7d1
správcem	správce	k1gMnSc7
táborské	táborský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
neformální	formální	k2eNgFnSc2d1
hlavou	hlava	k1gFnSc7
husitských	husitský	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>