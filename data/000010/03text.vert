<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Madagaskarská	madagaskarský	k2eAgFnSc1d1	madagaskarská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
zastarale	zastarale	k6eAd1	zastarale
Malgašsko	Malgašsko	k1gNnSc1	Malgašsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
při	při	k7c6	při
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
nejbližších	blízký	k2eAgInPc6d3	Nejbližší
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
africké	africký	k2eAgFnSc2d1	africká
pevniny	pevnina	k1gFnSc2	pevnina
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhohor	druhohory	k1gFnPc2	druhohory
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
90	[number]	k4	90
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
2	[number]	k4	2
cm	cm	kA	cm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
pevniny	pevnina	k1gFnSc2	pevnina
(	(	kIx(	(
<g/>
Mosambik	Mosambik	k1gInSc1	Mosambik
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
400	[number]	k4	400
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Mosambický	mosambický	k2eAgInSc1d1	mosambický
průliv	průliv	k1gInSc1	průliv
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
flóra	flóra	k1gFnSc1	flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
dlouhodobé	dlouhodobý	k2eAgFnSc3d1	dlouhodobá
izolaci	izolace	k1gFnSc3	izolace
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
endemická	endemický	k2eAgNnPc4d1	endemické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
okolí	okolí	k1gNnSc6	okolí
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
Réunion	Réunion	k1gInSc1	Réunion
<g/>
,	,	kIx,	,
Tromelin	Tromelin	k2eAgInSc1d1	Tromelin
<g/>
,	,	kIx,	,
Mayotte	Mayott	k1gMnSc5	Mayott
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
Bassas	Bassas	k1gInSc1	Bassas
da	da	k?	da
India	indium	k1gNnSc2	indium
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
stát	stát	k1gInSc1	stát
Komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
Seychely	Seychely	k1gFnPc4	Seychely
a	a	k8xC	a
východně	východně	k6eAd1	východně
Mauricius	Mauricius	k1gMnSc1	Mauricius
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
se	se	k3xPyFc4	se
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
přeplavili	přeplavit	k5eAaPmAgMnP	přeplavit
na	na	k7c6	na
dvojitých	dvojitý	k2eAgFnPc6d1	dvojitá
kánoích	kánoe	k1gFnPc6	kánoe
přes	přes	k7c4	přes
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Afriku	Afrika	k1gFnSc4	Afrika
z	z	k7c2	z
Indonésie	Indonésie	k1gFnSc2	Indonésie
asi	asi	k9	asi
před	před	k7c7	před
2000	[number]	k4	2000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
usadili	usadit	k5eAaPmAgMnP	usadit
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
dnešní	dnešní	k2eAgMnPc1d1	dnešní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
(	(	kIx(	(
<g/>
Malgaši	Malgaš	k1gMnPc1	Malgaš
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
smíšený	smíšený	k2eAgInSc4d1	smíšený
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
centrální	centrální	k2eAgFnSc6d1	centrální
vysočině	vysočina	k1gFnSc6	vysočina
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
kmeny	kmen	k1gInPc4	kmen
austronéského	austronéský	k2eAgInSc2d1	austronéský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Merina	merino	k1gNnSc2	merino
<g/>
,	,	kIx,	,
Sihanaka	Sihanak	k1gMnSc2	Sihanak
a	a	k8xC	a
Betsileo	Betsileo	k1gMnSc1	Betsileo
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
dominantní	dominantní	k2eAgNnSc4d1	dominantní
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnPc1	pobřeží
obývají	obývat	k5eAaImIp3nP	obývat
tzv.	tzv.	kA	tzv.
Côtiers	Côtiers	k1gInSc1	Côtiers
<g/>
,	,	kIx,	,
kmeny	kmen	k1gInPc1	kmen
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
společný	společný	k2eAgInSc1d1	společný
jazyk	jazyk	k1gInSc1	jazyk
malgaština	malgaština	k1gFnSc1	malgaština
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
indonéskému	indonéský	k2eAgInSc3d1	indonéský
jazyku	jazyk	k1gInSc3	jazyk
jazyku	jazyk	k1gInSc3	jazyk
Maanjanců	Maanjanec	k1gInPc2	Maanjanec
z	z	k7c2	z
Kalimantanu	Kalimantan	k1gInSc2	Kalimantan
<g/>
.	.	kIx.	.
</s>
<s>
Zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
včetně	včetně	k7c2	včetně
uctívání	uctívání	k1gNnSc2	uctívání
předků	předek	k1gMnPc2	předek
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
indonéskou	indonéský	k2eAgFnSc7d1	Indonéská
a	a	k8xC	a
africkou	africký	k2eAgFnSc7d1	africká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Arabové	Arab	k1gMnPc1	Arab
založili	založit	k5eAaPmAgMnP	založit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
první	první	k4xOgInSc1	první
sultanát	sultanát	k1gInSc1	sultanát
<g/>
,	,	kIx,	,
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
existoval	existovat	k5eAaImAgInS	existovat
stát	stát	k1gInSc1	stát
Imerina	Imerino	k1gNnSc2	Imerino
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
objevili	objevit	k5eAaPmAgMnP	objevit
Madagaskar	Madagaskar	k1gInSc4	Madagaskar
Portugalci	Portugalec	k1gMnPc1	Portugalec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
přišli	přijít	k5eAaPmAgMnP	přijít
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
evropští	evropský	k2eAgMnPc1d1	evropský
misionáři	misionář	k1gMnPc1	misionář
obrátili	obrátit	k5eAaPmAgMnP	obrátit
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
království	království	k1gNnSc4	království
Merina	merino	k1gNnSc2	merino
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
podrobilo	podrobit	k5eAaPmAgNnS	podrobit
většinu	většina	k1gFnSc4	většina
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
francouzský	francouzský	k2eAgInSc1d1	francouzský
protektorát	protektorát	k1gInSc1	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
ostrov	ostrov	k1gInSc4	ostrov
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
kolonií	kolonie	k1gFnSc7	kolonie
až	až	k6eAd1	až
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgMnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
název	název	k1gInSc1	název
za	za	k7c4	za
současný	současný	k2eAgInSc4d1	současný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mírovém	mírový	k2eAgNnSc6d1	Mírové
desetiletí	desetiletí	k1gNnSc6	desetiletí
profrancouzské	profrancouzský	k2eAgFnSc2d1	profrancouzská
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
nedávná	dávný	k2eNgFnSc1d1	nedávná
historie	historie	k1gFnSc1	historie
bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
revoluce	revoluce	k1gFnSc2	revoluce
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
prezident	prezident	k1gMnSc1	prezident
Didier	Didier	k1gMnSc1	Didier
Ratsiraka	Ratsirak	k1gMnSc4	Ratsirak
jako	jako	k9	jako
faktický	faktický	k2eAgMnSc1d1	faktický
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyznával	vyznávat	k5eAaImAgMnS	vyznávat
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
protizápadní	protizápadní	k2eAgFnSc4d1	protizápadní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
znárodnila	znárodnit	k5eAaPmAgFnS	znárodnit
mnoho	mnoho	k4c4	mnoho
obchodů	obchod	k1gInPc2	obchod
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
včetně	včetně	k7c2	včetně
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
ho	on	k3xPp3gInSc4	on
stávky	stávka	k1gFnPc1	stávka
a	a	k8xC	a
demonstrace	demonstrace	k1gFnPc1	demonstrace
přinutily	přinutit	k5eAaPmAgFnP	přinutit
přijmout	přijmout	k5eAaPmF	přijmout
demokratičtější	demokratický	k2eAgFnSc4d2	demokratičtější
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
přivedly	přivést	k5eAaPmAgFnP	přivést
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Albertem	Albert	k1gMnSc7	Albert
Zafym	Zafym	k1gInSc4	Zafym
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
Marc	Marc	k1gFnSc1	Marc
Ravalomanana	Ravalomanana	k1gFnSc1	Ravalomanana
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
obsadila	obsadit	k5eAaPmAgFnS	obsadit
armáda	armáda	k1gFnSc1	armáda
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
palác	palác	k1gInSc4	palác
a	a	k8xC	a
převzala	převzít	k5eAaPmAgFnS	převzít
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
následně	následně	k6eAd1	následně
předala	předat	k5eAaPmAgFnS	předat
opozičnímu	opoziční	k2eAgMnSc3d1	opoziční
vůdci	vůdce	k1gMnSc3	vůdce
Andrymu	Andrym	k1gInSc2	Andrym
Rajoelinovi	Rajoelin	k1gMnSc3	Rajoelin
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
prezident	prezident	k1gMnSc1	prezident
Ravalomanana	Ravalomanan	k1gMnSc4	Ravalomanan
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byl	být	k5eAaImAgInS	být
inaugurován	inaugurovat	k5eAaBmNgMnS	inaugurovat
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Rajoelina	Rajoelina	k1gFnSc1	Rajoelina
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
se	se	k3xPyFc4	se
od	od	k7c2	od
africké	africký	k2eAgFnSc2d1	africká
pevniny	pevnina	k1gFnSc2	pevnina
postupně	postupně	k6eAd1	postupně
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
během	během	k7c2	během
druhohor	druhohory	k1gFnPc2	druhohory
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc4	povrch
tvoří	tvořit	k5eAaImIp3nS	tvořit
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
zvolna	zvolna	k6eAd1	zvolna
se	s	k7c7	s
sklánějící	sklánějící	k2eAgFnSc7d1	sklánějící
k	k	k7c3	k
západu	západ	k1gInSc3	západ
a	a	k8xC	a
příkře	příkro	k6eAd1	příkro
spadající	spadající	k2eAgInSc1d1	spadající
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
rovnému	rovný	k2eAgInSc3d1	rovný
úzkému	úzký	k2eAgInSc3d1	úzký
pruhu	pruh	k1gInSc3	pruh
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
vulkanický	vulkanický	k2eAgInSc1d1	vulkanický
masiv	masiv	k1gInSc1	masiv
Tsaratanana	Tsaratanana	k1gFnSc1	Tsaratanana
nebo	nebo	k8xC	nebo
Maromokotro	Maromokotro	k1gNnSc1	Maromokotro
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
2	[number]	k4	2
876	[number]	k4	876
m	m	kA	m
n.	n.	k?	n.
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
dřívější	dřívější	k2eAgFnSc2d1	dřívější
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
jsou	být	k5eAaImIp3nP	být
dnešní	dnešní	k2eAgNnPc4d1	dnešní
kráterová	kráterový	k2eAgNnPc4d1	kráterové
jezírka	jezírko	k1gNnPc4	jezírko
a	a	k8xC	a
horká	horký	k2eAgNnPc4d1	horké
vřídla	vřídlo	k1gNnPc4	vřídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
ostrova	ostrov	k1gInSc2	ostrov
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pohoří	pohoří	k1gNnSc4	pohoří
Ankaratra	Ankaratrum	k1gNnSc2	Ankaratrum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Tsiafajavena	Tsiafajaven	k2eAgFnSc1d1	Tsiafajaven
(	(	kIx(	(
<g/>
2	[number]	k4	2
643	[number]	k4	643
m	m	kA	m
n.	n.	k?	n.
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
leží	ležet	k5eAaImIp3nS	ležet
masiv	masiv	k1gInSc1	masiv
Andringitra	Andringitrum	k1gNnSc2	Andringitrum
nebo	nebo	k8xC	nebo
Pic	pic	k0	pic
Boby	bob	k1gInPc1	bob
(	(	kIx(	(
<g/>
2	[number]	k4	2
658	[number]	k4	658
m	m	kA	m
n.	n.	k?	n.
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
masiv	masiv	k1gInSc4	masiv
Isalo	Isalo	k1gNnSc4	Isalo
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
hustou	hustý	k2eAgFnSc7d1	hustá
říční	říční	k2eAgFnSc7d1	říční
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Betsiboka	Betsiboko	k1gNnPc4	Betsiboko
<g/>
,	,	kIx,	,
Mahavavy	Mahavava	k1gFnPc4	Mahavava
<g/>
,	,	kIx,	,
Bemarivo	Bemariva	k1gFnSc5	Bemariva
<g/>
,	,	kIx,	,
Maevarano	Maevarana	k1gFnSc5	Maevarana
<g/>
,	,	kIx,	,
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
,	,	kIx,	,
Mahajamba	Mahajamba	k1gFnSc1	Mahajamba
<g/>
,	,	kIx,	,
Ikopa	Ikopa	k1gFnSc1	Ikopa
<g/>
,	,	kIx,	,
Mangoky	Mangok	k1gInPc1	Mangok
<g/>
,	,	kIx,	,
Onilahy	Onilaha	k1gFnPc1	Onilaha
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Mangoro	Mangora	k1gFnSc5	Mangora
<g/>
.	.	kIx.	.
</s>
<s>
Umělým	umělý	k2eAgNnSc7d1	umělé
spojením	spojení	k1gNnSc7	spojení
lagun	laguna	k1gFnPc2	laguna
na	na	k7c6	na
východě	východ	k1gInSc6	východ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
Canal	Canal	k1gInSc1	Canal
des	des	k1gNnPc1	des
Pangalane	Pangalan	k1gMnSc5	Pangalan
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
jezery	jezero	k1gNnPc7	jezero
jsou	být	k5eAaImIp3nP	být
Tsimanampetsotsa	Tsimanampetsots	k1gMnSc4	Tsimanampetsots
<g/>
,	,	kIx,	,
Ihotry	Ihotr	k1gMnPc4	Ihotr
<g/>
,	,	kIx,	,
Bemamba	Bemamba	k1gFnSc1	Bemamba
<g/>
,	,	kIx,	,
Kinkony	Kinkon	k1gInPc1	Kinkon
a	a	k8xC	a
Alaotra	Alaotra	k1gFnSc1	Alaotra
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
necelých	celý	k2eNgInPc2d1	necelý
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
průměrně	průměrně	k6eAd1	průměrně
velkým	velký	k2eAgInSc7d1	velký
africkým	africký	k2eAgInSc7d1	africký
státem	stát	k1gInSc7	stát
a	a	k8xC	a
46	[number]	k4	46
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
v	v	k7c6	v
jihojihozápadně-severoseverovýchodním	jihojihozápadněeveroseverovýchodní	k2eAgInSc6d1	jihojihozápadně-severoseverovýchodní
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
asi	asi	k9	asi
1600	[number]	k4	1600
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
560	[number]	k4	560
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
4	[number]	k4	4
828	[number]	k4	828
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
členité	členitý	k2eAgNnSc1d1	členité
(	(	kIx(	(
<g/>
nejvíce	hodně	k6eAd3	hodně
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
7,5	[number]	k4	7,5
<g/>
x	x	k?	x
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
ostrovů	ostrov	k1gInPc2	ostrov
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
za	za	k7c7	za
Grónskem	Grónsko	k1gNnSc7	Grónsko
<g/>
,	,	kIx,	,
Irianem	Irian	k1gInSc7	Irian
(	(	kIx(	(
<g/>
Novou	nový	k2eAgFnSc7d1	nová
Guineou	Guinea	k1gFnSc7	Guinea
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kalimantanem	Kalimantan	k1gInSc7	Kalimantan
(	(	kIx(	(
<g/>
Borneem	Borneo	k1gNnSc7	Borneo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
Sokotra	Sokotra	k1gFnSc1	Sokotra
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
řády	řád	k1gInPc4	řád
menší	malý	k2eAgMnSc1d2	menší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
si	se	k3xPyFc3	se
za	za	k7c7	za
základní	základní	k2eAgFnSc7d1	základní
linií	linie	k1gFnSc7	linie
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
tato	tento	k3xDgNnPc4	tento
mořská	mořský	k2eAgNnPc4d1	mořské
území	území	k1gNnPc4	území
<g/>
:	:	kIx,	:
pobřežní	pobřežní	k2eAgNnSc4d1	pobřežní
moře	moře	k1gNnSc4	moře
-	-	kIx~	-
12	[number]	k4	12
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
22,2	[number]	k4	22,2
km	km	kA	km
<g/>
)	)	kIx)	)
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
zóna	zóna	k1gFnSc1	zóna
-	-	kIx~	-
24	[number]	k4	24
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
44,4	[number]	k4	44,4
km	km	kA	km
<g/>
)	)	kIx)	)
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
linie	linie	k1gFnSc2	linie
výlučná	výlučný	k2eAgFnSc1d1	výlučná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
zóna	zóna	k1gFnSc1	zóna
-	-	kIx~	-
200	[number]	k4	200
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
370,4	[number]	k4	370,4
km	km	kA	km
<g/>
)	)	kIx)	)
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
linie	linie	k1gFnSc2	linie
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
šelf	šelf	k1gInSc1	šelf
-	-	kIx~	-
200	[number]	k4	200
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
370,4	[number]	k4	370,4
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
<s>
Klima	klima	k1gNnSc1	klima
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
,	,	kIx,	,
ovlivňované	ovlivňovaný	k2eAgInPc1d1	ovlivňovaný
východními	východní	k2eAgInPc7d1	východní
pasáty	pasát	k1gInPc7	pasát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
srážek	srážka	k1gFnPc2	srážka
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
mm	mm	kA	mm
spadne	spadnout	k5eAaPmIp3nS	spadnout
ročně	ročně	k6eAd1	ročně
na	na	k7c6	na
návětrném	návětrný	k2eAgNnSc6d1	návětrné
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
ostrova	ostrov	k1gInSc2	ostrov
spadne	spadnout	k5eAaPmIp3nS	spadnout
nejméně	málo	k6eAd3	málo
500	[number]	k4	500
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Poloostrovu	poloostrov	k1gInSc3	poloostrov
Masoala	Masoala	k1gFnSc1	Masoala
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
nočník	nočník	k1gInSc1	nočník
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
spadne	spadnout	k5eAaPmIp3nS	spadnout
4	[number]	k4	4
000	[number]	k4	000
-	-	kIx~	-
6	[number]	k4	6
000	[number]	k4	000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
je	být	k5eAaImIp3nS	být
sušší	suchý	k2eAgMnSc1d2	sušší
<g/>
:	:	kIx,	:
500	[number]	k4	500
-	-	kIx~	-
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnPc1d1	roční
teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
chladnějším	chladný	k2eAgNnSc6d2	chladnější
období	období	k1gNnSc6	období
po	po	k7c4	po
27	[number]	k4	27
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
období	období	k1gNnSc6	období
teplejším	teplý	k2eAgNnSc6d2	teplejší
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinná	rostlinný	k2eAgNnPc1d1	rostlinné
společenstva	společenstvo	k1gNnPc1	společenstvo
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
jsou	být	k5eAaImIp3nP	být
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
různé	různý	k2eAgFnSc3d1	různá
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
horách	hora	k1gFnPc6	hora
najdeme	najít	k5eAaPmIp1nP	najít
horské	horský	k2eAgFnPc1d1	horská
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
níž	nízce	k6eAd2	nízce
tropické	tropický	k2eAgInPc1d1	tropický
deštné	deštný	k2eAgInPc1d1	deštný
pralesy	prales	k1gInPc1	prales
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
čelí	čelit	k5eAaImIp3nP	čelit
lidské	lidský	k2eAgFnSc3d1	lidská
těžbě	těžba	k1gFnSc3	těžba
a	a	k8xC	a
vypadá	vypadat	k5eAaImIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
budou	být	k5eAaImBp3nP	být
zcela	zcela	k6eAd1	zcela
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
původních	původní	k2eAgInPc2d1	původní
opadavých	opadavý	k2eAgInPc2d1	opadavý
lesů	les	k1gInPc2	les
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
bambusové	bambusový	k2eAgFnPc4d1	bambusová
křoviny	křovina	k1gFnPc4	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
extra	extra	k6eAd1	extra
suché	suchý	k2eAgNnSc1d1	suché
podnebí	podnebí	k1gNnSc1	podnebí
(	(	kIx(	(
<g/>
350	[number]	k4	350
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
trnité	trnitý	k2eAgFnPc1d1	trnitá
buše	buš	k1gFnPc1	buš
nebo	nebo	k8xC	nebo
savany	savana	k1gFnPc1	savana
<g/>
.	.	kIx.	.
</s>
<s>
Degradovaným	degradovaný	k2eAgInPc3d1	degradovaný
travnatým	travnatý	k2eAgInPc3d1	travnatý
porostům	porost	k1gInPc3	porost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
badlandy	badlanda	k1gFnSc2	badlanda
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
mangrovové	mangrovový	k2eAgInPc4d1	mangrovový
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
izolaci	izolace	k1gFnSc3	izolace
od	od	k7c2	od
okolních	okolní	k2eAgInPc2d1	okolní
pevninských	pevninský	k2eAgInPc2d1	pevninský
bloků	blok	k1gInPc2	blok
(	(	kIx(	(
<g/>
asi	asi	k9	asi
130	[number]	k4	130
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgInP	vázat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
lemuři	lemur	k1gMnPc1	lemur
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
20	[number]	k4	20
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
zde	zde	k6eAd1	zde
však	však	k9	však
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dravé	dravý	k2eAgFnPc4d1	dravá
šelmy	šelma	k1gFnPc4	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
fosa	fosa	k1gFnSc1	fosa
madagaskarská	madagaskarský	k2eAgFnSc1d1	madagaskarská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osídlení	osídlení	k1gNnSc6	osídlení
člověkem	člověk	k1gMnSc7	člověk
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
krajina	krajina	k1gFnSc1	krajina
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
ubylo	ubýt	k5eAaPmAgNnS	ubýt
postupně	postupně	k6eAd1	postupně
mnoho	mnoho	k4c1	mnoho
unikátních	unikátní	k2eAgMnPc2d1	unikátní
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
byl	být	k5eAaImAgInS	být
např.	např.	kA	např.
i	i	k8xC	i
lemur	lemur	k1gMnSc1	lemur
Megaladapis	Megaladapis	k1gInSc1	Megaladapis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
velký	velký	k2eAgMnSc1d1	velký
jako	jako	k8xC	jako
osel	osel	k1gMnSc1	osel
a	a	k8xC	a
vážil	vážit	k5eAaImAgMnS	vážit
200	[number]	k4	200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
přežily	přežít	k5eAaPmAgFnP	přežít
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
za	za	k7c4	za
živoucí	živoucí	k2eAgFnPc4d1	živoucí
fosilie	fosilie	k1gFnPc4	fosilie
<g/>
,	,	kIx,	,
všude	všude	k6eAd1	všude
jinde	jinde	k6eAd1	jinde
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
v	v	k7c6	v
nelítostné	lítostný	k2eNgFnSc6d1	nelítostná
konkurenci	konkurence	k1gFnSc6	konkurence
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
mladším	mladý	k2eAgMnSc6d2	mladší
<g/>
,	,	kIx,	,
modernějším	moderní	k2eAgMnSc6d2	modernější
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
však	však	k9	však
nepoznaly	poznat	k5eNaPmAgFnP	poznat
žádnou	žádný	k3yNgFnSc4	žádný
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
zeměmi	zem	k1gFnPc7	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
budovat	budovat	k5eAaImF	budovat
systém	systém	k1gInSc4	systém
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
osm	osm	k4xCc4	osm
a	a	k8xC	a
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
plochu	plocha	k1gFnSc4	plocha
284	[number]	k4	284
919	[number]	k4	919
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
síť	síť	k1gFnSc1	síť
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
50	[number]	k4	50
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
plochu	plocha	k1gFnSc4	plocha
1	[number]	k4	1
698	[number]	k4	698
640	[number]	k4	640
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
23	[number]	k4	23
202	[number]	k4	202
000	[number]	k4	000
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozloze	rozloha	k1gFnSc3	rozloha
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
přibližně	přibližně	k6eAd1	přibližně
39,5	[number]	k4	39,5
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
pouze	pouze	k6eAd1	pouze
malgaština	malgaština	k1gFnSc1	malgaština
<g/>
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
je	být	k5eAaImIp3nS	být
však	však	k9	však
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
styku	styk	k1gInSc6	styk
(	(	kIx(	(
<g/>
i	i	k9	i
na	na	k7c6	na
oficiálních	oficiální	k2eAgInPc6d1	oficiální
dokumentech	dokument	k1gInPc6	dokument
<g/>
)	)	kIx)	)
natolik	natolik	k6eAd1	natolik
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
neformálně	formálně	k6eNd1	formálně
oficiální	oficiální	k2eAgFnSc4d1	oficiální
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
49,5	[number]	k4	49,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
protestanti	protestant	k1gMnPc1	protestant
22,7	[number]	k4	22,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
20,3	[number]	k4	20,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
animistická	animistický	k2eAgNnPc4d1	animistické
náboženství	náboženství	k1gNnPc4	náboženství
48	[number]	k4	48
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
je	být	k5eAaImIp3nS	být
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
1,9	[number]	k4	1,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
náboženství	náboženství	k1gNnPc1	náboženství
(	(	kIx(	(
<g/>
0,6	[number]	k4	0,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
šest	šest	k4xCc4	šest
autonomních	autonomní	k2eAgFnPc2d1	autonomní
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
faritany	faritan	k1gInPc1	faritan
mizakatena	mizakaten	k2eAgNnPc1d1	mizakaten
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
dále	daleko	k6eAd2	daleko
dělily	dělit	k5eAaImAgFnP	dělit
na	na	k7c4	na
22	[number]	k4	22
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
faritra	faritrum	k1gNnSc2	faritrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
referenda	referendum	k1gNnSc2	referendum
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
se	se	k3xPyFc4	se
kraje	kraj	k1gInPc1	kraj
mají	mít	k5eAaImIp3nP	mít
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
116	[number]	k4	116
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
1548	[number]	k4	1548
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
16	[number]	k4	16
969	[number]	k4	969
fokontany	fokontan	k1gInPc7	fokontan
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
města	město	k1gNnPc1	město
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
statut	statut	k1gInSc4	statut
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
commune	commun	k1gMnSc5	commun
urbane	urban	k1gMnSc5	urban
<g/>
"	"	kIx"	"
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k9	jako
okresy	okres	k1gInPc4	okres
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
pouhých	pouhý	k2eAgNnPc2d1	pouhé
5	[number]	k4	5
%	%	kIx~	%
půdy	půda	k1gFnPc4	půda
lze	lze	k6eAd1	lze
obdělávat	obdělávat	k5eAaImF	obdělávat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
velkou	velký	k2eAgFnSc4d1	velká
rozlohu	rozloha	k1gFnSc4	rozloha
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgNnPc4d1	malé
naleziště	naleziště	k1gNnPc4	naleziště
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc4d1	nízká
výrobu	výroba	k1gFnSc4	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Dobývá	dobývat	k5eAaImIp3nS	dobývat
se	se	k3xPyFc4	se
chrom	chrom	k1gInSc1	chrom
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
slída	slída	k1gFnSc1	slída
<g/>
,	,	kIx,	,
polodrahokamy	polodrahokam	k1gInPc1	polodrahokam
a	a	k8xC	a
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
a	a	k8xC	a
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nS	sklízet
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc1	maniok
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
batáty	batáty	k1gInPc1	batáty
<g/>
,	,	kIx,	,
lilek	lilek	k1gInSc1	lilek
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
sisal	sisal	k1gInSc1	sisal
<g/>
,	,	kIx,	,
kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
<g/>
,	,	kIx,	,
ananasy	ananas	k1gInPc1	ananas
<g/>
,	,	kIx,	,
pomeranče	pomeranč	k1gInPc1	pomeranč
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
pepř	pepř	k1gInSc1	pepř
<g/>
,	,	kIx,	,
vanilka	vanilka	k1gFnSc1	vanilka
<g/>
,	,	kIx,	,
hřebíček	hřebíček	k1gInSc1	hřebíček
a	a	k8xC	a
káva	káva	k1gFnSc1	káva
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
na	na	k7c4	na
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
významný	významný	k2eAgInSc4d1	významný
rybolov	rybolov	k1gInSc4	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
žulovým	žulový	k2eAgInPc3d1	žulový
masivům	masiv	k1gInPc3	masiv
oblasti	oblast	k1gFnSc2	oblast
Andringitra	Andringitrum	k1gNnSc2	Andringitrum
-	-	kIx~	-
Tsaranoro	Tsaranora	k1gFnSc5	Tsaranora
<g/>
,	,	kIx,	,
Karambony	Karambon	k1gInPc7	Karambon
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
populární	populární	k2eAgFnSc7d1	populární
arénou	aréna	k1gFnSc7	aréna
bigwallových	bigwallův	k2eAgInPc2d1	bigwallův
horolezeckých	horolezecký	k2eAgInPc2d1	horolezecký
výstupů	výstup	k1gInPc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
horolezectví	horolezectví	k1gNnSc2	horolezectví
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zapsali	zapsat	k5eAaPmAgMnP	zapsat
také	také	k9	také
naši	náš	k3xOp1gMnPc1	náš
horolezci	horolezec	k1gMnPc1	horolezec
Ondřej	Ondřej	k1gMnSc1	Ondřej
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Sobotka	Sobotka	k1gMnSc1	Sobotka
prvovýstupem	prvovýstup	k1gInSc7	prvovýstup
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
oblasti	oblast	k1gFnSc2	oblast
Tsaranoro	Tsaranora	k1gFnSc5	Tsaranora
Be	Be	k1gFnPc2	Be
vedeným	vedený	k2eAgFnPc3d1	vedená
v	v	k7c6	v
ideální	ideální	k2eAgFnSc6d1	ideální
přímé	přímý	k2eAgFnSc6d1	přímá
linii	linie	k1gFnSc6	linie
s	s	k7c7	s
názvem	název	k1gInSc7	název
Shortcut	Shortcut	k1gInSc1	Shortcut
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
Adam	Adam	k1gMnSc1	Adam
Ondra	Ondra	k1gMnSc1	Ondra
prvním	první	k4xOgInSc7	první
volným	volný	k2eAgInSc7d1	volný
výstupem	výstup	k1gInSc7	výstup
cesty	cesta	k1gFnSc2	cesta
Tough	Tough	k1gInSc1	Tough
enough	enough	k1gInSc1	enough
na	na	k7c4	na
Karambony	Karambon	k1gMnPc4	Karambon
<g/>
.	.	kIx.	.
</s>
