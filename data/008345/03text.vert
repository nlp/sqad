<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Daniel	Daniel	k1gMnSc1	Daniel
Konečný	Konečný	k1gMnSc1	Konečný
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
Boskovice	Boskovice	k1gInPc1	Boskovice
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
Praha-Řeporyje	Praha-Řeporyje	k1gFnPc2	Praha-Řeporyje
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
šoumen	šoumen	k2eAgMnSc1d1	šoumen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
jihomoravských	jihomoravský	k2eAgInPc6d1	jihomoravský
Boskovicích	Boskovice	k1gInPc6	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
studoval	studovat	k5eAaImAgMnS	studovat
herectví	herectví	k1gNnSc1	herectví
DAMU	DAMU	kA	DAMU
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
prošel	projít	k5eAaPmAgInS	projít
různými	různý	k2eAgNnPc7d1	různé
povoláními	povolání	k1gNnPc7	povolání
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
porodnici	porodnice	k1gFnSc6	porodnice
či	či	k8xC	či
jako	jako	k8xS	jako
hrobník	hrobník	k1gMnSc1	hrobník
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Laura	Laura	k1gFnSc1	Laura
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
založil	založit	k5eAaPmAgMnS	založit
kapelu	kapela	k1gFnSc4	kapela
Šum	šuma	k1gFnPc2	šuma
svistu	svist	k1gInSc2	svist
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
nahrál	nahrát	k5eAaBmAgMnS	nahrát
třeba	třeba	k6eAd1	třeba
hity	hit	k1gInPc4	hit
Sexy	sex	k1gInPc4	sex
Hafanana	Hafanana	k1gFnSc1	Hafanana
nebo	nebo	k8xC	nebo
Jsem	být	k5eAaImIp1nS	být
boss	boss	k1gMnSc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
vystoupení	vystoupení	k1gNnSc4	vystoupení
byla	být	k5eAaImAgFnS	být
typická	typický	k2eAgFnSc1d1	typická
atmosférou	atmosféra	k1gFnSc7	atmosféra
brazilských	brazilský	k2eAgMnPc2d1	brazilský
karnevalů	karneval	k1gInPc2	karneval
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
exotická	exotický	k2eAgNnPc4d1	exotické
zvířata	zvíře	k1gNnPc4	zvíře
jako	jako	k8xC	jako
lvy	lev	k1gMnPc4	lev
<g/>
,	,	kIx,	,
tygry	tygr	k1gMnPc4	tygr
či	či	k8xC	či
papoušky	papoušek	k1gMnPc4	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Sólově	sólově	k6eAd1	sólově
nahrál	nahrát	k5eAaBmAgInS	nahrát
album	album	k1gNnSc4	album
Nekonečný	konečný	k2eNgInSc1d1	nekonečný
šum	šum	k1gInSc1	šum
<g/>
.	.	kIx.	.
</s>
<s>
Označoval	označovat	k5eAaImAgMnS	označovat
se	se	k3xPyFc4	se
za	za	k7c4	za
krále	král	k1gMnSc4	král
rozkoše	rozkoš	k1gFnSc2	rozkoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
například	například	k6eAd1	například
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
sérii	série	k1gFnSc6	série
Historky	historka	k1gFnSc2	historka
od	od	k7c2	od
krbu	krb	k1gInSc2	krb
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Eliška	Eliška	k1gFnSc1	Eliška
má	mít	k5eAaImIp3nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
divočinu	divočina	k1gFnSc4	divočina
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
krotí	krotit	k5eAaImIp3nP	krotit
krokodýli	krokodýl	k1gMnPc1	krokodýl
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtvý	mrtvý	k1gMnSc1	mrtvý
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
fotografií	fotografia	k1gFnPc2	fotografia
v	v	k7c6	v
pronajatém	pronajatý	k2eAgInSc6d1	pronajatý
rodinném	rodinný	k2eAgInSc6d1	rodinný
domě	dům	k1gInSc6	dům
<g/>
)	)	kIx)	)
v	v	k7c6	v
pražských	pražský	k2eAgNnPc6d1	Pražské
Řeporyjích	Řeporyjí	k1gNnPc6	Řeporyjí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Zuzana	Zuzana	k1gFnSc1	Zuzana
Kardová	Kardová	k1gFnSc1	Kardová
později	pozdě	k6eAd2	pozdě
uvedla	uvést	k5eAaPmAgFnS	uvést
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
a	a	k8xC	a
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
z	z	k7c2	z
pitvy	pitva	k1gFnSc2	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
neléčený	léčený	k2eNgInSc1d1	neléčený
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
<g/>
Pohřeb	pohřeb	k1gInSc1	pohřeb
zpěváka	zpěvák	k1gMnSc2	zpěvák
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
o	o	k7c4	o
smuteční	smuteční	k2eAgFnSc4d1	smuteční
událost	událost	k1gFnSc4	událost
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
života	život	k1gInSc2	život
Daniela	Daniel	k1gMnSc2	Daniel
Nekonečného	konečný	k2eNgMnSc2d1	nekonečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Daniel	Daniel	k1gMnSc1	Daniel
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Nekonečný	konečný	k2eNgMnSc1d1	nekonečný
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
