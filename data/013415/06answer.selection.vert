<s>
Kostel	kostel	k1gInSc1	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
při	při	k7c6	při
farnosti	farnost	k1gFnSc6	farnost
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
v	v	k7c6	v
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
architektem	architekt	k1gMnSc7	architekt
Franzem	Franz	k1gMnSc7	Franz
Holikem	Holik	k1gMnSc7	Holik
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
