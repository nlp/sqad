<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
při	při	k7c6	při
farnosti	farnost	k1gFnSc6	farnost
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
v	v	k7c6	v
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
architektem	architekt	k1gMnSc7	architekt
Franzem	Franz	k1gMnSc7	Franz
Holikem	Holik	k1gMnSc7	Holik
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
stával	stávat	k5eAaImAgInS	stávat
špitál	špitál	k1gInSc1	špitál
pro	pro	k7c4	pro
malomocné	malomocný	k1gMnPc4	malomocný
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1382	[number]	k4	1382
markrabě	markrabě	k1gMnSc1	markrabě
Jošt	Jošt	k1gMnSc1	Jošt
daroval	darovat	k5eAaPmAgMnS	darovat
špitál	špitál	k1gInSc4	špitál
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
byl	být	k5eAaImAgInS	být
špitál	špitál	k1gInSc1	špitál
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
zbořen	zbořit	k5eAaPmNgInS	zbořit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
byla	být	k5eAaImAgFnS	být
Křenová	křenový	k2eAgFnSc1d1	Křenová
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
osadou	osada	k1gFnSc7	osada
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
i	i	k9	i
samostatným	samostatný	k2eAgNnSc7d1	samostatné
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
i	i	k8xC	i
předměstí	předměstí	k1gNnSc6	předměstí
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
věřících	věřící	k1gMnPc2	věřící
už	už	k9	už
byly	být	k5eAaImAgInP	být
brněnské	brněnský	k2eAgInPc1d1	brněnský
kostely	kostel	k1gInPc1	kostel
kapacitně	kapacitně	k6eAd1	kapacitně
nedostačující	dostačující	k2eNgInPc1d1	nedostačující
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
si	se	k3xPyFc3	se
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
budování	budování	k1gNnSc4	budování
nových	nový	k2eAgInPc2d1	nový
kostelů	kostel	k1gInPc2	kostel
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
periferii	periferie	k1gFnSc4	periferie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
kostela	kostel	k1gInSc2	kostel
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
brněnský	brněnský	k2eAgMnSc1d1	brněnský
biskup	biskup	k1gMnSc1	biskup
Pavel	Pavel	k1gMnSc1	Pavel
Huyn	Huyn	k1gMnSc1	Huyn
jako	jako	k8xC	jako
připomínku	připomínka	k1gFnSc4	připomínka
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
panování	panování	k1gNnSc2	panování
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
jubilejní	jubilejní	k2eAgNnSc4d1	jubilejní
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc4	pozemek
darovalo	darovat	k5eAaPmAgNnS	darovat
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
posvěcen	posvětit	k5eAaPmNgInS	posvětit
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1910	[number]	k4	1910
v	v	k7c4	v
den	den	k1gInSc4	den
panovníkových	panovníkův	k2eAgInPc2d1	panovníkův
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
financování	financování	k1gNnSc4	financování
stavby	stavba	k1gFnSc2	stavba
založil	založit	k5eAaPmAgMnS	založit
biskup	biskup	k1gMnSc1	biskup
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
diecézi	diecéze	k1gFnSc6	diecéze
Jednotu	jednota	k1gFnSc4	jednota
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
katolických	katolický	k2eAgInPc2d1	katolický
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
byl	být	k5eAaImAgInS	být
zadán	zadán	k2eAgInSc1d1	zadán
projekt	projekt	k1gInSc1	projekt
kostela	kostel	k1gInSc2	kostel
Franzi	Franze	k1gFnSc4	Franze
Holikovi	Holik	k1gMnSc6	Holik
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žáku	žák	k1gMnSc3	žák
Otto	Otto	k1gMnSc1	Otto
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
byl	být	k5eAaImAgInS	být
Holik	Holik	k1gInSc1	Holik
brněnským	brněnský	k2eAgMnSc7d1	brněnský
městským	městský	k2eAgMnSc7d1	městský
architektem	architekt	k1gMnSc7	architekt
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
projektem	projekt	k1gInSc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Návrhů	návrh	k1gInPc2	návrh
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
farním	farní	k2eAgInSc6d1	farní
archivu	archiv	k1gInSc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
vedl	vést	k5eAaImAgMnS	vést
stavitel	stavitel	k1gMnSc1	stavitel
Josef	Josef	k1gMnSc1	Josef
Müller	Müller	k1gMnSc1	Müller
a	a	k8xC	a
stavební	stavební	k2eAgInSc1d1	stavební
dohled	dohled	k1gInSc1	dohled
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
inženýr	inženýr	k1gMnSc1	inženýr
Emanuel	Emanuel	k1gMnSc1	Emanuel
Straka	Straka	k1gMnSc1	Straka
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Holik	Holik	k1gMnSc1	Holik
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
kostel	kostel	k1gInSc4	kostel
včetně	včetně	k7c2	včetně
detailů	detail	k1gInPc2	detail
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
výzdoby	výzdoba	k1gFnSc2	výzdoba
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
také	také	k9	také
faru	fara	k1gFnSc4	fara
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nestabilní	stabilní	k2eNgNnSc4d1	nestabilní
podloží	podloží	k1gNnSc4	podloží
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
bažin	bažina	k1gFnPc2	bažina
jsou	být	k5eAaImIp3nP	být
základy	základ	k1gInPc1	základ
na	na	k7c6	na
železobetonových	železobetonový	k2eAgInPc6d1	železobetonový
pilotech	pilot	k1gInPc6	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klenbu	klenba	k1gFnSc4	klenba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
litý	litý	k2eAgInSc1d1	litý
beton	beton	k1gInSc1	beton
a	a	k8xC	a
pro	pro	k7c4	pro
střešní	střešní	k2eAgInSc4d1	střešní
krov	krov	k1gInSc4	krov
ocelová	ocelový	k2eAgFnSc1d1	ocelová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
hotova	hotov	k2eAgFnSc1d1	hotova
<g/>
.18	.18	k4	.18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1914	[number]	k4	1914
kostel	kostel	k1gInSc4	kostel
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
biskup	biskup	k1gMnSc1	biskup
Pavel	Pavel	k1gMnSc1	Pavel
Huyn	Huyn	k1gNnSc4	Huyn
a	a	k8xC	a
slavnosti	slavnost	k1gFnPc4	slavnost
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Bedřich	Bedřich	k1gMnSc1	Bedřich
Rakousko-Těšínský	rakouskoěšínský	k2eAgMnSc1d1	rakousko-těšínský
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
mše	mše	k1gFnSc2	mše
byli	být	k5eAaImAgMnP	být
architekt	architekt	k1gMnSc1	architekt
Franz	Franz	k1gMnSc1	Franz
Holik	Holik	k1gMnSc1	Holik
a	a	k8xC	a
inženýr	inženýr	k1gMnSc1	inženýr
Emanuel	Emanuel	k1gMnSc1	Emanuel
Straka	Straka	k1gMnSc1	Straka
v	v	k7c6	v
sakristii	sakristie	k1gFnSc6	sakristie
odměněni	odměnit	k5eAaPmNgMnP	odměnit
záslužným	záslužný	k2eAgInSc7d1	záslužný
zlatým	zlatý	k2eAgInSc7d1	zlatý
křížem	kříž	k1gInSc7	kříž
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
biskup	biskup	k1gMnSc1	biskup
Huyn	Huyn	k1gMnSc1	Huyn
přenesl	přenést	k5eAaPmAgMnS	přenést
farní	farní	k2eAgFnSc4d1	farní
správu	správa	k1gFnSc4	správa
z	z	k7c2	z
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Maří	mařit	k5eAaImIp3nS	mařit
Magdalény	Magdaléna	k1gFnPc4	Magdaléna
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
na	na	k7c4	na
Křenové	křenový	k2eAgNnSc4d1	Křenové
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
farním	farní	k2eAgInSc7d1	farní
chrámem	chrám	k1gInSc7	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
vybavení	vybavení	k1gNnSc1	vybavení
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výzdoba	výzdoba	k1gFnSc1	výzdoba
byla	být	k5eAaImAgFnS	být
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c4	v
den	den	k1gInSc4	den
svěcení	svěcení	k1gNnSc2	svěcení
ještě	ještě	k6eAd1	ještě
chyběly	chybět	k5eAaImAgFnP	chybět
lavice	lavice	k1gFnPc1	lavice
<g/>
,	,	kIx,	,
zpovědnice	zpovědnice	k1gFnPc1	zpovědnice
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
bočních	boční	k2eAgInPc2d1	boční
oltářů	oltář	k1gInPc2	oltář
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
kostela	kostel	k1gInSc2	kostel
==	==	k?	==
</s>
</p>
<p>
<s>
Věž	věž	k1gFnSc1	věž
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
ji	on	k3xPp3gFnSc4	on
trojdílná	trojdílný	k2eAgFnSc1d1	trojdílná
helma	helma	k1gFnSc1	helma
s	s	k7c7	s
měděným	měděný	k2eAgInSc7d1	měděný
plechem	plech	k1gInSc7	plech
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
ocelové	ocelový	k2eAgInPc1d1	ocelový
zvony	zvon	k1gInPc1	zvon
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
věnoval	věnovat	k5eAaImAgMnS	věnovat
brněnský	brněnský	k2eAgMnSc1d1	brněnský
majitel	majitel	k1gMnSc1	majitel
slévárny	slévárna	k1gFnSc2	slévárna
Heinrich	Heinrich	k1gMnSc1	Heinrich
Storek	Storek	k1gMnSc1	Storek
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
vstupujeme	vstupovat	k5eAaImIp1nP	vstupovat
třemi	tři	k4xCgInPc7	tři
vchody	vchod	k1gInPc7	vchod
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
předsíněmi	předsíň	k1gFnPc7	předsíň
a	a	k8xC	a
nad	nad	k7c7	nad
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
trojdílný	trojdílný	k2eAgInSc1d1	trojdílný
kůr	kůr	k1gInSc1	kůr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
zdobí	zdobit	k5eAaImIp3nS	zdobit
portál	portál	k1gInSc4	portál
z	z	k7c2	z
umělého	umělý	k2eAgInSc2d1	umělý
kamene	kámen	k1gInSc2	kámen
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
zvěstování	zvěstování	k1gNnSc1	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
holubicí	holubice	k1gFnSc7	holubice
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgMnSc1d1	symbolizující
Ducha	duch	k1gMnSc2	duch
Svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
lemují	lemovat	k5eAaImIp3nP	lemovat
ornamenty	ornament	k1gInPc4	ornament
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
,	,	kIx,	,
lilií	lilie	k1gFnPc2	lilie
a	a	k8xC	a
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Šikmo	šikmo	k6eAd1	šikmo
nad	nad	k7c7	nad
dvěma	dva	k4xCgInPc7	dva
postranními	postranní	k2eAgInPc7d1	postranní
vchody	vchod	k1gInPc7	vchod
jsou	být	k5eAaImIp3nP	být
znázorněny	znázornit	k5eAaPmNgFnP	znázornit
sochy	socha	k1gFnPc1	socha
andělů	anděl	k1gMnPc2	anděl
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
drží	držet	k5eAaImIp3nP	držet
latinské	latinský	k2eAgInPc4d1	latinský
nápisy	nápis	k1gInPc4	nápis
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
dokončení	dokončení	k1gNnSc4	dokončení
a	a	k8xC	a
vysvěcení	vysvěcení	k1gNnSc4	vysvěcení
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
půdorys	půdorys	k1gInSc4	půdorys
latinského	latinský	k2eAgInSc2d1	latinský
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
na	na	k7c4	na
kněžiště	kněžiště	k1gNnSc4	kněžiště
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
navazuje	navazovat	k5eAaImIp3nS	navazovat
kaple	kaple	k1gFnSc1	kaple
a	a	k8xC	a
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Secesní	secesní	k2eAgFnPc1d1	secesní
lavice	lavice	k1gFnPc1	lavice
a	a	k8xC	a
zpovědnice	zpovědnice	k1gFnPc1	zpovědnice
byly	být	k5eAaImAgFnP	být
navrženy	navržen	k2eAgInPc1d1	navržen
také	také	k6eAd1	také
architektem	architekt	k1gMnSc7	architekt
Holikem	Holik	k1gMnSc7	Holik
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívané	používaný	k2eNgFnSc3d1	nepoužívaná
kazatelně	kazatelna	k1gFnSc3	kazatelna
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
hlavní	hlavní	k2eAgFnPc1d1	hlavní
lodě	loď	k1gFnPc1	loď
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
reliéfy	reliéf	k1gInPc7	reliéf
čtyř	čtyři	k4xCgNnPc2	čtyři
evangelistů	evangelista	k1gMnPc2	evangelista
a	a	k8xC	a
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
holubice	holubice	k1gFnSc1	holubice
symbolizující	symbolizující	k2eAgFnSc1d1	symbolizující
Ducha	duch	k1gMnSc4	duch
Svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
lodi	loď	k1gFnSc2	loď
stojí	stát	k5eAaImIp3nS	stát
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tepané	tepaný	k2eAgNnSc1d1	tepané
víko	víko	k1gNnSc1	víko
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
polokoule	polokoule	k1gFnSc2	polokoule
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
umělecký	umělecký	k2eAgMnSc1d1	umělecký
kovář	kovář	k1gMnSc1	kovář
Rudolf	Rudolf	k1gMnSc1	Rudolf
Opluštil	Opluštil	k1gMnSc1	Opluštil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
víku	víko	k1gNnSc6	víko
bývala	bývat	k5eAaImAgFnS	bývat
kovová	kovový	k2eAgFnSc1d1	kovová
soška	soška	k1gFnSc1	soška
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
odcizena	odcizen	k2eAgFnSc1d1	odcizena
<g/>
.	.	kIx.	.
</s>
<s>
Křtitelnici	křtitelnice	k1gFnSc4	křtitelnice
zdobí	zdobit	k5eAaImIp3nP	zdobit
čtyři	čtyři	k4xCgInPc1	čtyři
reliéfy	reliéf	k1gInPc1	reliéf
církevních	církevní	k2eAgMnPc2d1	církevní
otců	otec	k1gMnPc2	otec
–	–	k?	–
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc2	Řehoř
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Ambrože	Ambrož	k1gMnSc4	Ambrož
a	a	k8xC	a
sv.	sv.	kA	sv.
Jeronýma	Jeroným	k1gMnSc4	Jeroným
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Kněžiště	kněžiště	k1gNnSc1	kněžiště
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
kovaná	kovaný	k2eAgFnSc1d1	kovaná
mříž	mříž	k1gFnSc1	mříž
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
a	a	k8xC	a
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Podlahu	podlaha	k1gFnSc4	podlaha
presbytáře	presbytář	k1gInSc2	presbytář
zdobí	zdobit	k5eAaImIp3nS	zdobit
barevný	barevný	k2eAgInSc1d1	barevný
znak	znak	k1gInSc1	znak
biskupa	biskup	k1gMnSc2	biskup
Huyna	Huyn	k1gMnSc2	Huyn
z	z	k7c2	z
umělého	umělý	k2eAgInSc2d1	umělý
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Hollas	Hollas	k1gMnSc1	Hollas
doplnil	doplnit	k5eAaPmAgMnS	doplnit
kněžiště	kněžiště	k1gNnSc1	kněžiště
zavěšenými	zavěšený	k2eAgFnPc7d1	zavěšená
tepanými	tepaný	k2eAgFnPc7d1	tepaná
lampami	lampa	k1gFnPc7	lampa
na	na	k7c6	na
ozdobných	ozdobný	k2eAgFnPc6d1	ozdobná
konzolách	konzola	k1gFnPc6	konzola
pro	pro	k7c4	pro
věčné	věčné	k1gNnSc4	věčné
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
oprava	oprava	k1gFnSc1	oprava
kostela	kostel	k1gInSc2	kostel
s	s	k7c7	s
restaurátorskými	restaurátorský	k2eAgFnPc7d1	restaurátorská
pracemi	práce	k1gFnPc7	práce
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
liturgické	liturgický	k2eAgFnSc6d1	liturgická
reformě	reforma	k1gFnSc6	reforma
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
pořízen	pořízen	k2eAgInSc4d1	pořízen
nový	nový	k2eAgInSc4d1	nový
obětní	obětní	k2eAgInSc4d1	obětní
stůl	stůl	k1gInSc4	stůl
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Kolka	Kolek	k1gMnSc4	Kolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oltáře	Oltář	k1gInPc4	Oltář
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
umělého	umělý	k2eAgInSc2d1	umělý
růžového	růžový	k2eAgInSc2d1	růžový
mramoru	mramor	k1gInSc2	mramor
kostelu	kostel	k1gInSc3	kostel
věnovali	věnovat	k5eAaImAgMnP	věnovat
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Bedřich	Bedřich	k1gMnSc1	Bedřich
Rakousko-Těšínský	rakouskoěšínský	k2eAgMnSc1d1	rakousko-těšínský
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
stojí	stát	k5eAaImIp3nS	stát
bronzové	bronzový	k2eAgFnPc4d1	bronzová
sochy	socha	k1gFnPc4	socha
jejich	jejich	k3xOp3gInPc2	jejich
patronů	patron	k1gInPc2	patron
–	–	k?	–
sv.	sv.	kA	sv.
Bedřicha	Bedřich	k1gMnSc4	Bedřich
a	a	k8xC	a
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnPc4	Alžběta
Durynské	durynský	k2eAgFnPc4d1	Durynská
od	od	k7c2	od
brněnského	brněnský	k2eAgMnSc2d1	brněnský
sochaře	sochař	k1gMnSc2	sochař
Carla	Carl	k1gMnSc2	Carl
Wolleka	Wolleek	k1gMnSc2	Wolleek
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oltářním	oltářní	k2eAgInSc6d1	oltářní
obraze	obraz	k1gInSc6	obraz
od	od	k7c2	od
brněnského	brněnský	k2eAgMnSc2d1	brněnský
malíře	malíř	k1gMnSc2	malíř
Johanna	Johann	k1gMnSc2	Johann
Friedricha	Friedrich	k1gMnSc2	Friedrich
Wachy	Wacha	k1gFnSc2	Wacha
se	se	k3xPyFc4	se
patroni	patron	k1gMnPc1	patron
Brna	Brno	k1gNnSc2	Brno
sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
modlí	modlit	k5eAaImIp3nS	modlit
k	k	k7c3	k
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
(	(	kIx(	(
<g/>
Immaculata	Immacule	k1gNnPc1	Immacule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
a	a	k8xC	a
hadovi	had	k1gMnSc6	had
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
namaloval	namalovat	k5eAaPmAgMnS	namalovat
autor	autor	k1gMnSc1	autor
vedutu	veduta	k1gFnSc4	veduta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
svatostánkem	svatostánek	k1gInSc7	svatostánek
je	být	k5eAaImIp3nS	být
oblouk	oblouk	k1gInSc1	oblouk
s	s	k7c7	s
tepanými	tepaný	k2eAgFnPc7d1	tepaná
a	a	k8xC	a
pozlacenými	pozlacený	k2eAgFnPc7d1	pozlacená
ozdobami	ozdoba	k1gFnPc7	ozdoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zhotovili	zhotovit	k5eAaPmAgMnP	zhotovit
cizelér	cizelér	k1gMnSc1	cizelér
Josef	Josef	k1gMnSc1	Josef
Hollas	Hollas	k1gMnSc1	Hollas
a	a	k8xC	a
pozlacovač	pozlacovač	k1gMnSc1	pozlacovač
Jan	Jan	k1gMnSc1	Jan
Hysan	Hysan	k1gMnSc1	Hysan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
levého	levý	k2eAgInSc2d1	levý
bočního	boční	k2eAgInSc2d1	boční
oltáře	oltář	k1gInSc2	oltář
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
Johann	Johann	k1gMnSc1	Johann
Wacha	Wacha	k1gMnSc1	Wacha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
namalován	namalovat	k5eAaPmNgInS	namalovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
válečných	válečný	k2eAgFnPc2d1	válečná
útrap	útrapa	k1gFnPc2	útrapa
a	a	k8xC	a
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Nejsvětější	nejsvětější	k2eAgNnSc1d1	nejsvětější
Srdce	srdce	k1gNnSc1	srdce
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
a	a	k8xC	a
invalidní	invalidní	k2eAgMnSc1d1	invalidní
voják	voják	k1gMnSc1	voják
hledí	hledět	k5eAaImIp3nS	hledět
ke	k	k7c3	k
Kristovu	Kristův	k2eAgNnSc3d1	Kristovo
Srdci	srdce	k1gNnSc3	srdce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
těšit	těšit	k5eAaImF	těšit
zarmoucené	zarmoucený	k2eAgNnSc1d1	zarmoucené
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
boční	boční	k2eAgInSc1d1	boční
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc4	Josef
namaloval	namalovat	k5eAaPmAgMnS	namalovat
a	a	k8xC	a
daroval	darovat	k5eAaPmAgMnS	darovat
Eduard	Eduard	k1gMnSc1	Eduard
Csank	Csank	k1gMnSc1	Csank
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
příční	příční	k2eAgFnSc6d1	příční
lodi	loď	k1gFnSc6	loď
oltář	oltář	k1gInSc1	oltář
sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc2	Kříž
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
původního	původní	k2eAgNnSc2d1	původní
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
narušuje	narušovat	k5eAaImIp3nS	narušovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
secesní	secesní	k2eAgInSc4d1	secesní
styl	styl	k1gInSc4	styl
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
Psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Černovicích	Černovice	k1gFnPc6	Černovice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
farnosti	farnost	k1gFnSc2	farnost
kostela	kostel	k1gInSc2	kostel
na	na	k7c4	na
Křenové	křenový	k2eAgNnSc4d1	Křenové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vitráže	vitráž	k1gFnSc2	vitráž
===	===	k?	===
</s>
</p>
<p>
<s>
Vitráž	vitráž	k1gFnSc1	vitráž
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
průčelním	průčelní	k2eAgNnSc6d1	průčelní
okně	okno	k1gNnSc6	okno
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Alžbětu	Alžběta	k1gFnSc4	Alžběta
Durynskou	durynský	k2eAgFnSc4d1	Durynská
rozdávající	rozdávající	k2eAgFnSc4d1	rozdávající
almužnu	almužna	k1gFnSc4	almužna
chudým	chudý	k1gMnPc3	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
umístil	umístit	k5eAaPmAgMnS	umístit
autor	autor	k1gMnSc1	autor
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
1912	[number]	k4	1912
–	–	k?	–
Věnováno	věnován	k2eAgNnSc4d1	věnováno
od	od	k7c2	od
první	první	k4xOgFnSc2	první
moravské	moravský	k2eAgFnSc2d1	Moravská
spořitelny	spořitelna	k1gFnSc2	spořitelna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
výše	vysoce	k6eAd2	vysoce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
menší	malý	k2eAgNnSc4d2	menší
okno	okno	k1gNnSc4	okno
zobrazující	zobrazující	k2eAgFnSc1d1	zobrazující
Krista	Krista	k1gFnSc1	Krista
Krále	Král	k1gMnSc2	Král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
a	a	k8xC	a
příční	příční	k2eAgFnSc6d1	příční
lodi	loď	k1gFnSc6	loď
jsou	být	k5eAaImIp3nP	být
vitráže	vitráž	k1gFnPc1	vitráž
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
sv.	sv.	kA	sv.
Aloise	Alois	k1gMnSc4	Alois
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
neznámého	známý	k2eNgMnSc2d1	neznámý
biskupa	biskup	k1gMnSc2	biskup
s	s	k7c7	s
nemocným	nemocný	k1gMnSc7	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příční	příční	k2eAgFnSc6d1	příční
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
velká	velký	k2eAgFnSc1d1	velká
vitráž	vitráž	k1gFnSc4	vitráž
zachycující	zachycující	k2eAgInPc4d1	zachycující
boje	boj	k1gInPc4	boj
se	s	k7c7	s
Švédy	Švéd	k1gMnPc7	Švéd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
brněnské	brněnský	k2eAgNnSc1d1	brněnské
paladium	paladium	k1gNnSc1	paladium
(	(	kIx(	(
<g/>
milostný	milostný	k2eAgInSc1d1	milostný
obraz	obraz	k1gInSc1	obraz
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Svatotomské	Svatotomský	k2eAgFnSc2d1	Svatotomská
<g/>
)	)	kIx)	)
a	a	k8xC	a
rektora	rektor	k1gMnSc2	rektor
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
Martina	Martin	k2eAgInSc2d1	Martin
Středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
stojí	stát	k5eAaImIp3nS	stát
sv.	sv.	kA	sv.
Šimon	Šimon	k1gMnSc1	Šimon
a	a	k8xC	a
sv.	sv.	kA	sv.
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kněžišti	kněžiště	k1gNnSc6	kněžiště
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc1	čtyři
oválná	oválný	k2eAgNnPc1d1	oválné
okna	okno	k1gNnPc1	okno
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zničena	zničit	k5eAaPmNgNnP	zničit
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
obnovena	obnoven	k2eAgFnSc1d1	obnovena
s	s	k7c7	s
podobiznami	podobizna	k1gFnPc7	podobizna
sv.	sv.	kA	sv.
Filomény	Filoména	k1gFnPc1	Filoména
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Bosca	Boscus	k1gMnSc2	Boscus
a	a	k8xC	a
sv.	sv.	kA	sv.
Jana	Jana	k1gFnSc1	Jana
Vianneye	Vianneye	k1gFnSc1	Vianneye
<g/>
.	.	kIx.	.
</s>
<s>
Okenní	okenní	k2eAgFnSc1d1	okenní
vitráž	vitráž	k1gFnSc1	vitráž
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
zasnoubení	zasnoubení	k1gNnSc1	zasnoubení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
se	s	k7c7	s
sv.	sv.	kA	sv.
Josefem	Josef	k1gMnSc7	Josef
a	a	k8xC	a
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
retabulum	retabulum	k1gNnSc1	retabulum
<g/>
.	.	kIx.	.
</s>
<s>
Okna	okno	k1gNnPc1	okno
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
chrámové	chrámový	k2eAgFnSc2d1	chrámová
lodi	loď	k1gFnSc2	loď
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
zničena	zničit	k5eAaPmNgFnS	zničit
tlakovou	tlakový	k2eAgFnSc7d1	tlaková
vlnou	vlna	k1gFnSc7	vlna
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
čirým	čirý	k2eAgNnSc7d1	čiré
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
předsíni	předsíň	k1gFnSc6	předsíň
je	být	k5eAaImIp3nS	být
torzo	torzo	k1gNnSc1	torzo
vitráže	vitráž	k1gFnSc2	vitráž
doplněné	doplněný	k2eAgFnSc2d1	doplněná
obyčejným	obyčejný	k2eAgNnSc7d1	obyčejné
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
vitráže	vitráž	k1gFnPc4	vitráž
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
brněnský	brněnský	k2eAgInSc1d1	brněnský
závod	závod	k1gInSc1	závod
Evžena	Evžen	k1gMnSc2	Evžen
Škardy	Škarda	k1gMnSc2	Škarda
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
malíře	malíř	k1gMnSc2	malíř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Malého	Malý	k1gMnSc2	Malý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výmalba	výmalba	k1gFnSc1	výmalba
===	===	k?	===
</s>
</p>
<p>
<s>
Nástěnnou	nástěnný	k2eAgFnSc4d1	nástěnná
secesní	secesní	k2eAgFnSc4d1	secesní
malbu	malba	k1gFnSc4	malba
interiéru	interiér	k1gInSc2	interiér
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
malíře	malíř	k1gMnSc2	malíř
Hanse	Hans	k1gMnSc2	Hans
Kalmsteinera	Kalmsteiner	k1gMnSc2	Kalmsteiner
provedl	provést	k5eAaPmAgInS	provést
temperovými	temperový	k2eAgFnPc7d1	temperová
barvami	barva	k1gFnPc7	barva
František	František	k1gMnSc1	František
Kolbábek	Kolbábek	k1gMnSc1	Kolbábek
z	z	k7c2	z
Náměště	Náměšť	k1gFnSc2	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
strop	strop	k1gInSc4	strop
i	i	k8xC	i
stěny	stěna	k1gFnPc4	stěna
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
souvislá	souvislý	k2eAgFnSc1d1	souvislá
barevná	barevný	k2eAgFnSc1d1	barevná
ornamentální	ornamentální	k2eAgFnSc1d1	ornamentální
malba	malba	k1gFnSc1	malba
s	s	k7c7	s
mariánským	mariánský	k2eAgInSc7d1	mariánský
motivem	motiv	k1gInSc7	motiv
lilie	lilie	k1gFnSc2	lilie
jako	jako	k8xC	jako
symbolem	symbol	k1gInSc7	symbol
čistoty	čistota	k1gFnSc2	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obloucích	oblouk	k1gInPc6	oblouk
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
motivy	motiv	k1gInPc4	motiv
růží	růž	k1gFnPc2	růž
a	a	k8xC	a
ptáčků	ptáček	k1gInPc2	ptáček
a	a	k8xC	a
na	na	k7c6	na
oblouku	oblouk	k1gInSc6	oblouk
mezi	mezi	k7c7	mezi
hlavní	hlavní	k2eAgFnSc7d1	hlavní
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
kněžištěm	kněžiště	k1gNnSc7	kněžiště
zase	zase	k9	zase
zlacená	zlacený	k2eAgFnSc1d1	zlacená
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
kůru	kůr	k1gInSc2	kůr
je	být	k5eAaImIp3nS	být
vymalována	vymalován	k2eAgFnSc1d1	vymalována
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
barevnými	barevný	k2eAgMnPc7d1	barevný
motýly	motýl	k1gMnPc7	motýl
a	a	k8xC	a
brouky	brouk	k1gMnPc7	brouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Betlém	Betlém	k1gInSc4	Betlém
===	===	k?	===
</s>
</p>
<p>
<s>
Betlém	Betlém	k1gInSc1	Betlém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vánoční	vánoční	k2eAgInPc4d1	vánoční
mezi	mezi	k7c7	mezi
vchody	vchod	k1gInPc7	vchod
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
figury	figura	k1gFnPc1	figura
jsou	být	k5eAaImIp3nP	být
veliké	veliký	k2eAgFnPc1d1	veliká
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FILIP	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
