<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byla	být	k5eAaImAgFnS	být
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
název	název	k1gInSc1	název
Detroit	Detroit	k1gInSc1	Detroit
Rock	rock	k1gInSc1	rock
City	City	k1gFnSc2	City
o	o	k7c6	o
čtyřech	čtyři	k4xCgMnPc6	čtyři
teenagerech	teenager	k1gMnPc6	teenager
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
udělat	udělat	k5eAaPmF	udělat
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
<g/>
.	.	kIx.	.
</s>
