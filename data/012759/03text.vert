<p>
<s>
Sonar	sonar	k1gInSc1	sonar
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
SOund	SOund	k1gInSc4	SOund
Navigation	Navigation	k1gInSc1	Navigation
And	Anda	k1gFnPc2	Anda
Ranging	Ranging	k1gInSc4	Ranging
–	–	k?	–
zvuková	zvukový	k2eAgFnSc1d1	zvuková
navigace	navigace	k1gFnSc1	navigace
a	a	k8xC	a
zaměřování	zaměřování	k1gNnSc1	zaměřování
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c6	na
principu	princip	k1gInSc6	princip
radaru	radar	k1gInSc2	radar
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
místo	místo	k7c2	místo
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
používá	používat	k5eAaImIp3nS	používat
ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
ponorkami	ponorka	k1gFnPc7	ponorka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rádiové	rádiový	k2eAgFnPc1d1	rádiová
vlny	vlna	k1gFnPc1	vlna
mají	mít	k5eAaImIp3nP	mít
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgInSc1d2	menší
dosah	dosah	k1gInSc1	dosah
než	než	k8xS	než
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
a	a	k8xC	a
(	(	kIx(	(
<g/>
ultra	ultra	k2eAgMnSc1d1	ultra
<g/>
)	)	kIx)	)
<g/>
zvuk	zvuk	k1gInSc1	zvuk
naopak	naopak	k6eAd1	naopak
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc4d1	významné
použití	použití	k1gNnSc4	použití
dostaly	dostat	k5eAaPmAgInP	dostat
sonary	sonar	k1gInPc1	sonar
také	také	k9	také
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
jakožto	jakožto	k8xS	jakožto
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
neinvazivních	invazivní	k2eNgFnPc2d1	neinvazivní
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1	vyšetřovací
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
sonografy	sonograf	k1gInPc1	sonograf
slouží	sloužit	k5eAaImIp3nP	sloužit
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
nenarozených	narozený	k2eNgFnPc2d1	nenarozená
dětí	dítě	k1gFnPc2	dítě
u	u	k7c2	u
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
též	též	k9	též
v	v	k7c6	v
interním	interní	k2eAgNnSc6d1	interní
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc7d1	přírodní
verzí	verze	k1gFnSc7	verze
sonaru	sonar	k1gInSc2	sonar
je	být	k5eAaImIp3nS	být
echolokace	echolokace	k1gFnSc1	echolokace
netopýrů	netopýr	k1gMnPc2	netopýr
a	a	k8xC	a
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc1	první
pasivní	pasivní	k2eAgNnPc1d1	pasivní
zařízení	zařízení	k1gNnPc1	zařízení
na	na	k7c6	na
principu	princip	k1gInSc6	princip
sonaru	sonar	k1gInSc2	sonar
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Lewis	Lewis	k1gFnSc4	Lewis
Nixon	Nixon	k1gMnSc1	Nixon
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
sonar	sonar	k1gInSc1	sonar
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Paul	Paul	k1gMnSc1	Paul
Langevin	Langevina	k1gFnPc2	Langevina
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sonar	sonar	k1gInSc1	sonar
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Radar	radar	k1gInSc1	radar
</s>
</p>
<p>
<s>
Echolokace	Echolokace	k1gFnSc1	Echolokace
</s>
</p>
<p>
<s>
Hydrofon	hydrofon	k1gInSc1	hydrofon
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sonar	sonar	k1gInSc1	sonar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
