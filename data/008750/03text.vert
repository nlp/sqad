<p>
<s>
Europa	Europa	k1gFnSc1	Europa
(	(	kIx(	(
<g/>
též	též	k9	též
Jupiter	Jupiter	k1gMnSc1	Jupiter
II	II	kA	II
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejmenší	malý	k2eAgInPc1d3	nejmenší
z	z	k7c2	z
Galileovských	Galileovský	k2eAgInPc2d1	Galileovský
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahrnutí	zahrnutí	k1gNnSc6	zahrnutí
malých	malý	k2eAgInPc2d1	malý
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
šestý	šestý	k4xOgMnSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Objevili	objevit	k5eAaPmAgMnP	objevit
ji	on	k3xPp3gFnSc4	on
už	už	k6eAd1	už
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc2	Galilei
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
současník	současník	k1gMnSc1	současník
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
Európy	Európa	k1gFnSc2	Európa
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
milenek	milenka	k1gFnPc2	milenka
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
vládce	vládce	k1gMnSc4	vládce
Týru	Týra	k1gMnSc4	Týra
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Mariova	Mariův	k2eAgInSc2d1	Mariův
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Europa	Europa	k1gFnSc1	Europa
má	mít	k5eAaImIp3nS	mít
3100	[number]	k4	3100
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc1d1	pozemský
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
šestým	šestý	k4xOgInSc7	šestý
největším	veliký	k2eAgInSc7d3	veliký
měsícem	měsíc	k1gInSc7	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
plášť	plášť	k1gInSc1	plášť
Europy	Europa	k1gFnSc2	Europa
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
železné	železný	k2eAgNnSc1d1	železné
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Europy	Europa	k1gFnSc2	Europa
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
slabá	slabý	k2eAgFnSc1d1	slabá
atmosféra	atmosféra	k1gFnSc1	atmosféra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ledová	ledový	k2eAgFnSc1d1	ledová
krusta	krusta	k1gFnSc1	krusta
tvořící	tvořící	k2eAgNnSc4d1	tvořící
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Europy	Europa	k1gFnSc2	Europa
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
řídce	řídce	k6eAd1	řídce
poset	posít	k5eAaPmNgInS	posít
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
místy	místy	k6eAd1	místy
porušen	porušen	k2eAgInSc4d1	porušen
a	a	k8xC	a
protkán	protkán	k2eAgInSc4d1	protkán
systémy	systém	k1gInPc4	systém
prasklin	prasklina	k1gFnPc2	prasklina
a	a	k8xC	a
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
praskliny	prasklina	k1gFnPc1	prasklina
vedly	vést	k5eAaImAgFnP	vést
vědce	vědec	k1gMnPc4	vědec
k	k	k7c3	k
hypotéze	hypotéza	k1gFnSc3	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
ledovým	ledový	k2eAgInSc7d1	ledový
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
teoreticky	teoreticky	k6eAd1	teoreticky
mohl	moct	k5eAaImAgInS	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
útočiště	útočiště	k1gNnSc2	útočiště
jednoduchému	jednoduchý	k2eAgInSc3d1	jednoduchý
mimozemskému	mimozemský	k2eAgInSc3d1	mimozemský
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Io	Io	k1gFnSc1	Io
i	i	k8xC	i
Europa	Europa	k1gFnSc1	Europa
je	být	k5eAaImIp3nS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
silným	silný	k2eAgFnPc3d1	silná
slapovým	slapový	k2eAgFnPc3d1	slapová
jevům	jev	k1gInPc3	jev
vlivem	vliv	k1gInSc7	vliv
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
teoreticky	teoreticky	k6eAd1	teoreticky
udržovat	udržovat	k5eAaImF	udržovat
oceán	oceán	k1gInSc1	oceán
tekutý	tekutý	k2eAgInSc1d1	tekutý
a	a	k8xC	a
umožňovat	umožňovat	k5eAaImF	umožňovat
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
geologickou	geologický	k2eAgFnSc4d1	geologická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
<g/>
Okolo	okolo	k7c2	okolo
Europy	Europa	k1gFnSc2	Europa
proletělo	proletět	k5eAaPmAgNnS	proletět
několik	několik	k4yIc1	několik
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podrobněji	podrobně	k6eAd2	podrobně
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
vlastnosti	vlastnost	k1gFnPc1	vlastnost
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejčerstvější	čerstvý	k2eAgNnPc1d3	nejčerstvější
data	datum	k1gNnPc1	datum
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
měsíc	měsíc	k1gInSc4	měsíc
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
i	i	k9	i
dvojice	dvojice	k1gFnSc1	dvojice
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2020	[number]	k4	2020
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
vyslat	vyslat	k5eAaPmF	vyslat
k	k	k7c3	k
Europě	Europa	k1gFnSc3	Europa
další	další	k2eAgFnSc4d1	další
sondu	sonda	k1gFnSc4	sonda
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
pokusit	pokusit	k5eAaPmF	pokusit
poodhalit	poodhalit	k5eAaPmF	poodhalit
existenci	existence	k1gFnSc4	existence
oceánu	oceán	k1gInSc2	oceán
pod	pod	k7c7	pod
ledovým	ledový	k2eAgInSc7d1	ledový
příkrovem	příkrov	k1gInSc7	příkrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Europa	Europa	k1gFnSc1	Europa
je	být	k5eAaImIp3nS	být
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
složením	složení	k1gNnSc7	složení
nejspíše	nejspíše	k9	nejspíše
podobná	podobný	k2eAgNnPc4d1	podobné
velkým	velký	k2eAgFnPc3d1	velká
terestrickým	terestrický	k2eAgFnPc3d1	terestrická
planetám	planeta	k1gFnPc3	planeta
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnPc1d1	hlavní
minerální	minerální	k2eAgNnPc1d1	minerální
zastoupení	zastoupení	k1gNnPc1	zastoupení
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
pak	pak	k6eAd1	pak
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
silikáty	silikát	k1gInPc4	silikát
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
silikátové	silikátový	k2eAgFnSc2d1	silikátová
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
pláště	plášť	k1gInSc2	plášť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejspíše	nejspíše	k9	nejspíše
obrovský	obrovský	k2eAgInSc1d1	obrovský
oceán	oceán	k1gInSc1	oceán
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
mocný	mocný	k2eAgInSc4d1	mocný
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
km	km	kA	km
obepínající	obepínající	k2eAgFnSc2d1	obepínající
celé	celá	k1gFnSc2	celá
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
vystaveného	vystavený	k2eAgInSc2d1	vystavený
interakcí	interakce	k1gFnSc7	interakce
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgNnSc1d1	nedávné
měření	měření	k1gNnSc1	měření
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Europa	Europa	k1gFnSc1	Europa
má	mít	k5eAaImIp3nS	mít
stálé	stálý	k2eAgNnSc1d1	stálé
indukované	indukovaný	k2eAgNnSc1d1	indukované
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
interakcí	interakce	k1gFnSc7	interakce
s	s	k7c7	s
Jupiterovým	Jupiterův	k2eAgNnSc7d1	Jupiterovo
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
současně	současně	k6eAd1	současně
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
musí	muset	k5eAaImIp3nS	muset
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
podpovrchová	podpovrchový	k2eAgFnSc1d1	podpovrchová
vodivá	vodivý	k2eAgFnSc1d1	vodivá
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořená	tvořený	k2eAgFnSc1d1	tvořená
slanou	slaný	k2eAgFnSc7d1	slaná
vodou	voda	k1gFnSc7	voda
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
krusta	krusta	k1gFnSc1	krusta
se	se	k3xPyFc4	se
otočila	otočit	k5eAaPmAgFnS	otočit
o	o	k7c4	o
80	[number]	k4	80
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
převrátila	převrátit	k5eAaPmAgFnS	převrátit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
led	led	k1gInSc1	led
byl	být	k5eAaImAgInS	být
pevně	pevně	k6eAd1	pevně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Europa	Europa	k1gFnSc1	Europa
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kovové	kovový	k2eAgNnSc1d1	kovové
jádro	jádro	k1gNnSc1	jádro
tvořené	tvořený	k2eAgNnSc1d1	tvořené
nejspíše	nejspíše	k9	nejspíše
železem	železo	k1gNnSc7	železo
a	a	k8xC	a
niklem	nikl	k1gInSc7	nikl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
===	===	k?	===
</s>
</p>
<p>
<s>
Europa	Europa	k1gFnSc1	Europa
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tělesa	těleso	k1gNnPc4	těleso
s	s	k7c7	s
nejhladším	hladký	k2eAgInSc7d3	nejhladší
povrchem	povrch	k1gInSc7	povrch
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nejviditelnější	viditelný	k2eAgMnSc1d3	nejviditelnější
křížem	křížem	k6eAd1	křížem
krážem	krážem	k6eAd1	krážem
procházející	procházející	k2eAgFnPc1d1	procházející
linie	linie	k1gFnPc1	linie
jsou	být	k5eAaImIp3nP	být
nejspíše	nejspíše	k9	nejspíše
albedové	albedový	k2eAgInPc4d1	albedový
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
nízkou	nízký	k2eAgFnSc4d1	nízká
topografii	topografie	k1gFnSc4	topografie
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
velice	velice	k6eAd1	velice
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
tektonicky	tektonicky	k6eAd1	tektonicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
tedy	tedy	k9	tedy
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Ledová	ledový	k2eAgFnSc1d1	ledová
kůra	kůra	k1gFnSc1	kůra
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
současně	současně	k6eAd1	současně
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
albed	albed	k1gInSc4	albed
dosahující	dosahující	k2eAgFnSc2d1	dosahující
hodnoty	hodnota	k1gFnSc2	hodnota
0,64	[number]	k4	0,64
–	–	k?	–
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
měsíci	měsíc	k1gInPc7	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
četnosti	četnost	k1gFnSc2	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
bombardováním	bombardování	k1gNnPc3	bombardování
kometami	kometa	k1gFnPc7	kometa
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
mladý	mladý	k2eAgMnSc1d1	mladý
a	a	k8xC	a
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
věk	věk	k1gInSc1	věk
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
až	až	k9	až
180	[number]	k4	180
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
neexistuje	existovat	k5eNaImIp3nS	existovat
konsenzus	konsenzus	k1gInSc1	konsenzus
mezi	mezi	k7c7	mezi
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
obcí	obec	k1gFnSc7	obec
u	u	k7c2	u
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
vysvětlovány	vysvětlován	k2eAgInPc1d1	vysvětlován
zcela	zcela	k6eAd1	zcela
protichůdnými	protichůdný	k2eAgFnPc7d1	protichůdná
teoriemi	teorie	k1gFnPc7	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lineární	lineární	k2eAgFnPc1d1	lineární
praskliny	prasklina	k1gFnPc1	prasklina
====	====	k?	====
</s>
</p>
<p>
<s>
Nejviditelnější	viditelný	k2eAgInPc1d3	nejviditelnější
povrchové	povrchový	k2eAgInPc1d1	povrchový
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
jsou	být	k5eAaImIp3nP	být
série	série	k1gFnPc4	série
tmavších	tmavý	k2eAgFnPc2d2	tmavší
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
křižují	křižovat	k5eAaImIp3nP	křižovat
nahodile	nahodile	k6eAd1	nahodile
a	a	k8xC	a
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
povrch	povrch	k7c2wR	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	blízký	k2eAgNnSc1d2	bližší
pozorování	pozorování	k1gNnSc1	pozorování
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
těchto	tento	k3xDgFnPc2	tento
prasklin	prasklina	k1gFnPc2	prasklina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
spolu	spolu	k6eAd1	spolu
spojené	spojený	k2eAgFnPc1d1	spojená
a	a	k8xC	a
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
vlivem	vliv	k1gInSc7	vliv
posunu	posun	k1gInSc2	posun
části	část	k1gFnSc2	část
ledu	led	k1gInSc2	led
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Praskliny	prasklina	k1gFnPc1	prasklina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
desítky	desítka	k1gFnPc1	desítka
kilometrů	kilometr	k1gInPc2	kilometr
široké	široký	k2eAgFnPc1d1	široká
<g/>
.	.	kIx.	.
<g/>
Nejpřijímanější	přijímaný	k2eAgFnPc1d3	přijímaný
teorie	teorie	k1gFnPc1	teorie
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trhliny	trhlina	k1gFnPc1	trhlina
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
popraskáním	popraskání	k1gNnSc7	popraskání
ledové	ledový	k2eAgFnSc2d1	ledová
kůry	kůra	k1gFnSc2	kůra
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
série	série	k1gFnPc4	série
erupcí	erupce	k1gFnPc2	erupce
teplejšího	teplý	k2eAgInSc2d2	teplejší
ledu	led	k1gInSc2	led
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pak	pak	k6eAd1	pak
oslabil	oslabit	k5eAaPmAgInS	oslabit
povrchový	povrchový	k2eAgInSc1d1	povrchový
led	led	k1gInSc1	led
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc4	vznik
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
o	o	k7c4	o
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobal	podobat	k5eAaImAgMnS	podobat
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Europa	Europa	k1gFnSc1	Europa
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc2	Jupiter
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
a	a	k8xC	a
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
přivrácenou	přivrácený	k2eAgFnSc4d1	přivrácená
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
by	by	kYmCp3nP	by
mít	mít	k5eAaImF	mít
praskliny	prasklina	k1gFnPc4	prasklina
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
strukturu	struktura	k1gFnSc4	struktura
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
snadno	snadno	k6eAd1	snadno
předvídatelných	předvídatelný	k2eAgInPc6d1	předvídatelný
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pouze	pouze	k6eAd1	pouze
nejmladší	mladý	k2eAgFnPc1d3	nejmladší
praskliny	prasklina	k1gFnPc1	prasklina
mají	mít	k5eAaImIp3nP	mít
tuto	tento	k3xDgFnSc4	tento
orientaci	orientace	k1gFnSc4	orientace
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnPc1d2	starší
praskliny	prasklina	k1gFnPc1	prasklina
jsou	být	k5eAaImIp3nP	být
nahodile	nahodile	k6eAd1	nahodile
orientované	orientovaný	k2eAgInPc1d1	orientovaný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
jinou	jiný	k2eAgFnSc7d1	jiná
rychlostí	rychlost	k1gFnSc7	rychlost
než	než	k8xS	než
vnitřek	vnitřek	k1gInSc1	vnitřek
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
rychlosti	rychlost	k1gFnPc1	rychlost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgInP	být
možné	možný	k2eAgInPc1d1	možný
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
byl	být	k5eAaImAgInS	být
led	led	k1gInSc1	led
pevně	pevně	k6eAd1	pevně
uchycen	uchytit	k5eAaPmNgInS	uchytit
k	k	k7c3	k
podloží	podloží	k1gNnSc3	podloží
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vrstvy	vrstva	k1gFnSc2	vrstva
mezi	mezi	k7c7	mezi
ledem	led	k1gInSc7	led
a	a	k8xC	a
horninou	hornina	k1gFnSc7	hornina
umožňující	umožňující	k2eAgInSc4d1	umožňující
rozdílně	rozdílně	k6eAd1	rozdílně
rychlý	rychlý	k2eAgInSc4d1	rychlý
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
fotografií	fotografia	k1gFnPc2	fotografia
ze	z	k7c2	z
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
a	a	k8xC	a
Galileo	Galilea	k1gFnSc5	Galilea
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
určit	určit	k5eAaPmF	určit
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
tohoto	tento	k3xDgInSc2	tento
hypotetického	hypotetický	k2eAgInSc2d1	hypotetický
skluzu	skluz	k1gInSc2	skluz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
na	na	k7c4	na
12	[number]	k4	12
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
skluzu	skluz	k1gInSc3	skluz
ledového	ledový	k2eAgInSc2d1	ledový
krunýře	krunýř	k1gInSc2	krunýř
vůči	vůči	k7c3	vůči
horninovému	horninový	k2eAgInSc3d1	horninový
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgInPc1d1	další
geologické	geologický	k2eAgInPc1d1	geologický
útvary	útvar	k1gInPc1	útvar
====	====	k?	====
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
útvary	útvar	k1gInPc1	útvar
přítomné	přítomný	k2eAgInPc1d1	přítomný
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Europy	Europa	k1gFnSc2	Europa
jsou	být	k5eAaImIp3nP	být
kruhové	kruhový	k2eAgFnPc1d1	kruhová
či	či	k8xC	či
eliptické	eliptický	k2eAgFnPc1d1	eliptická
skvrny	skvrna	k1gFnPc1	skvrna
tzv.	tzv.	kA	tzv.
lentikuly	lentikout	k5eAaPmAgFnP	lentikout
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc4d1	různý
tvary	tvar	k1gInPc4	tvar
od	od	k7c2	od
dómů	dóm	k1gInPc2	dóm
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kruhové	kruhový	k2eAgFnPc4d1	kruhová
deprese	deprese	k1gFnPc4	deprese
až	až	k9	až
po	po	k7c4	po
hladké	hladký	k2eAgFnPc4d1	hladká
tmavé	tmavý	k2eAgFnPc4d1	tmavá
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
neuspořádanou	uspořádaný	k2eNgFnSc4d1	neuspořádaná
či	či	k8xC	či
hrubou	hrubý	k2eAgFnSc4d1	hrubá
nerovnoměrnou	rovnoměrný	k2eNgFnSc4d1	nerovnoměrná
texturu	textura	k1gFnSc4	textura
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
dómy	dóm	k1gInPc1	dóm
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k8xC	jako
oblasti	oblast	k1gFnPc1	oblast
starších	starý	k2eAgFnPc2d2	starší
planin	planina	k1gFnPc2	planina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vytlačeny	vytlačit	k5eAaPmNgFnP	vytlačit
nahoru	nahoru	k6eAd1	nahoru
silou	síla	k1gFnSc7	síla
působící	působící	k2eAgFnSc7d1	působící
zespoda	zespoda	k7c2	zespoda
<g/>
.	.	kIx.	.
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
hypotéza	hypotéza	k1gFnSc1	hypotéza
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lentikuly	lentikula	k1gFnPc1	lentikula
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jako	jako	k9	jako
diapiry	diapira	k1gFnPc1	diapira
teplejšího	teplý	k2eAgInSc2d2	teplejší
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stoupal	stoupat	k5eAaImAgInS	stoupat
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
skrze	skrze	k?	skrze
studený	studený	k2eAgInSc4d1	studený
led	led	k1gInSc4	led
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
magmatický	magmatický	k2eAgInSc1d1	magmatický
krb	krb	k1gInSc1	krb
v	v	k7c6	v
pozemské	pozemský	k2eAgFnSc6d1	pozemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Hladké	Hladké	k2eAgFnPc1d1	Hladké
temné	temný	k2eAgFnPc1d1	temná
skvrny	skvrna	k1gFnPc1	skvrna
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
teplejší	teplý	k2eAgInSc4d2	teplejší
led	led	k1gInSc4	led
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roztavil	roztavit	k5eAaPmAgInS	roztavit
okolní	okolní	k2eAgInSc4d1	okolní
studený	studený	k2eAgInSc4d1	studený
led	led	k1gInSc4	led
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgInS	zanechat
bazén	bazén	k1gInSc4	bazén
teplejší	teplý	k2eAgFnSc2d2	teplejší
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Neuspořádané	uspořádaný	k2eNgInPc1d1	neuspořádaný
lentikuly	lentikul	k1gInPc1	lentikul
by	by	kYmCp3nP	by
pak	pak	k6eAd1	pak
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
znovu	znovu	k6eAd1	znovu
zmrzlou	zmrzlý	k2eAgFnSc7d1	zmrzlá
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
malých	malá	k1gFnPc2	malá
fragmentu	fragment	k1gInSc2	fragment
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
zamrzlého	zamrzlý	k2eAgNnSc2d1	zamrzlé
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
ledovými	ledový	k2eAgFnPc7d1	ledová
krami	kra	k1gFnPc7	kra
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
<g/>
Alternativní	alternativní	k2eAgNnSc1d1	alternativní
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lentikuly	lentikula	k1gFnPc1	lentikula
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
malé	malý	k2eAgFnPc1d1	malá
oblasti	oblast	k1gFnPc1	oblast
nahodilého	nahodilý	k2eAgInSc2d1	nahodilý
chaotického	chaotický	k2eAgInSc2d1	chaotický
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
že	že	k8xS	že
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
skvrny	skvrna	k1gFnPc1	skvrna
a	a	k8xC	a
dómy	dóm	k1gInPc1	dóm
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
nízkého	nízký	k2eAgNnSc2d1	nízké
rozlišení	rozlišení	k1gNnSc2	rozlišení
snímků	snímek	k1gInPc2	snímek
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
žádné	žádný	k3yNgInPc4	žádný
přírodní	přírodní	k2eAgInPc4d1	přírodní
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
led	led	k1gInSc1	led
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
tlustý	tlustý	k2eAgMnSc1d1	tlustý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc1	vznik
konvektivního	konvektivní	k2eAgInSc2d1	konvektivní
přenosu	přenos	k1gInSc2	přenos
tepla	teplo	k1gNnSc2	teplo
diapirem	diapirma	k1gFnPc2	diapirma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
planetologů	planetolog	k1gMnPc2	planetolog
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
ledovým	ledový	k2eAgInSc7d1	ledový
krunýřem	krunýř	k1gInSc7	krunýř
Europy	Europa	k1gFnSc2	Europa
nachází	nacházet	k5eAaImIp3nS	nacházet
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zahříván	zahřívat	k5eAaImNgInS	zahřívat
a	a	k8xC	a
udržován	udržovat	k5eAaImNgInS	udržovat
tekutý	tekutý	k2eAgInSc1d1	tekutý
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k6eAd1	teplo
uvolňované	uvolňovaný	k2eAgNnSc1d1	uvolňované
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xS	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
horniny	hornina	k1gFnSc2	hornina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
dodávat	dodávat	k5eAaImF	dodávat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
kapalného	kapalný	k2eAgInSc2d1	kapalný
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
průměr	průměr	k1gInSc1	průměr
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
například	například	k6eAd1	například
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
na	na	k7c4	na
generování	generování	k1gNnSc4	generování
potřebného	potřebný	k2eAgNnSc2d1	potřebné
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Europy	Europa	k1gFnSc2	Europa
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
-160	-160	k4	-160
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
klesají	klesat	k5eAaImIp3nP	klesat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
-220	-220	k4	-220
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
udržuje	udržovat	k5eAaImIp3nS	udržovat
led	led	k1gInSc1	led
silně	silně	k6eAd1	silně
podchlazený	podchlazený	k2eAgInSc1d1	podchlazený
a	a	k8xC	a
velice	velice	k6eAd1	velice
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c6	o
tvrdosti	tvrdost	k1gFnSc6	tvrdost
žuly	žula	k1gFnSc2	žula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
náznaky	náznak	k1gInPc1	náznak
tekutého	tekutý	k2eAgInSc2d1	tekutý
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
teoretických	teoretický	k2eAgFnPc2d1	teoretická
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c4	o
teple	teple	k6eAd1	teple
vznikajícím	vznikající	k2eAgNnSc7d1	vznikající
slapovým	slapový	k2eAgNnSc7d1	slapové
působením	působení	k1gNnSc7	působení
(	(	kIx(	(
<g/>
důsledek	důsledek	k1gInSc1	důsledek
mírně	mírně	k6eAd1	mírně
excentrické	excentrický	k2eAgFnSc2d1	excentrická
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Laplaceovy	Laplaceův	k2eAgFnSc2d1	Laplaceova
resonance	resonance	k1gFnSc2	resonance
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
Galileovými	Galileův	k2eAgInPc7d1	Galileův
měsíci	měsíc	k1gInPc7	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
vyhodnocující	vyhodnocující	k2eAgInPc1d1	vyhodnocující
snímky	snímek	k1gInPc1	snímek
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
podporuje	podporovat	k5eAaImIp3nS	podporovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
existence	existence	k1gFnSc2	existence
kapalného	kapalný	k2eAgInSc2d1	kapalný
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
doložit	doložit	k5eAaPmF	doložit
analýzou	analýza	k1gFnSc7	analýza
snímků	snímek	k1gInPc2	snímek
povrchu	povrch	k1gInSc2	povrch
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
a	a	k8xC	a
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
<g/>
Nejvíce	nejvíce	k6eAd1	nejvíce
průkazné	průkazný	k2eAgInPc1d1	průkazný
jsou	být	k5eAaImIp3nP	být
snímky	snímek	k1gInPc1	snímek
chaotického	chaotický	k2eAgInSc2d1	chaotický
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
častých	častý	k2eAgInPc2d1	častý
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
některými	některý	k3yIgMnPc7	některý
vědci	vědec	k1gMnPc7	vědec
vyhodnocovány	vyhodnocován	k2eAgFnPc4d1	vyhodnocována
jako	jako	k8xC	jako
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
protavil	protavit	k5eAaPmAgInS	protavit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
skrze	skrze	k?	skrze
ledovou	ledový	k2eAgFnSc4d1	ledová
krustu	krusta	k1gFnSc4	krusta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
interpretace	interpretace	k1gFnSc1	interpretace
ale	ale	k9	ale
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
mezi	mezi	k7c7	mezi
vědeckými	vědecký	k2eAgInPc7d1	vědecký
kruhy	kruh	k1gInPc7	kruh
velkou	velký	k2eAgFnSc4d1	velká
kontroverzi	kontroverze	k1gFnSc3	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
geologů	geolog	k1gMnPc2	geolog
studující	studující	k2eAgFnSc4d1	studující
Europu	Europa	k1gFnSc4	Europa
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
zatracuje	zatracovat	k5eAaImIp3nS	zatracovat
<g/>
,	,	kIx,	,
namítají	namítat	k5eAaImIp3nP	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledový	ledový	k2eAgInSc1d1	ledový
obal	obal	k1gInSc1	obal
není	být	k5eNaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc2d2	veliký
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
či	či	k8xC	či
případně	případně	k6eAd1	případně
na	na	k7c4	na
neexistenci	neexistence	k1gFnSc4	neexistence
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
modely	model	k1gInPc1	model
pak	pak	k6eAd1	pak
dávají	dávat	k5eAaImIp3nP	dávat
hodnoty	hodnota	k1gFnPc1	hodnota
ledového	ledový	k2eAgInSc2d1	ledový
obalu	obal	k1gInSc2	obal
mezi	mezi	k7c7	mezi
několika	několik	k4yIc7	několik
kilometry	kilometr	k1gInPc7	kilometr
až	až	k9	až
po	po	k7c4	po
desítky	desítka	k1gFnPc4	desítka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvhodnější	vhodný	k2eAgNnPc1d3	nejvhodnější
místa	místo	k1gNnPc1	místo
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
tloušťky	tloušťka	k1gFnSc2	tloušťka
ledu	led	k1gInSc2	led
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Europy	Europa	k1gFnSc2	Europa
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
největší	veliký	k2eAgInPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopen	k2eAgInPc1d1	obklopen
kruhovými	kruhový	k2eAgInPc7d1	kruhový
koncentrickými	koncentrický	k2eAgInPc7d1	koncentrický
kruhy	kruh	k1gInPc7	kruh
a	a	k8xC	a
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vyplněny	vyplněn	k2eAgFnPc4d1	vyplněna
relativně	relativně	k6eAd1	relativně
hladkým	hladký	k2eAgInSc7d1	hladký
čerstvým	čerstvý	k2eAgInSc7d1	čerstvý
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
pozorováních	pozorování	k1gNnPc6	pozorování
a	a	k8xC	a
výpočtech	výpočet	k1gInPc6	výpočet
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
generovaného	generovaný	k2eAgNnSc2d1	generované
slapovými	slapový	k2eAgInPc7d1	slapový
jevy	jev	k1gInPc7	jev
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnější	vnější	k2eAgFnSc1d1	vnější
kůra	kůra	k1gFnSc1	kůra
pevného	pevný	k2eAgInSc2d1	pevný
ledu	led	k1gInSc2	led
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
mocná	mocný	k2eAgFnSc1d1	mocná
včetně	včetně	k7c2	včetně
vrstvy	vrstva	k1gFnSc2	vrstva
teplejšího	teplý	k2eAgInSc2d2	teplejší
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oceán	oceán	k1gInSc1	oceán
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
hluboký	hluboký	k2eAgMnSc1d1	hluboký
okolo	okolo	k7c2	okolo
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
závěrů	závěr	k1gInPc2	závěr
byly	být	k5eAaImAgFnP	být
učiněny	učiněn	k2eAgFnPc1d1	učiněna
pokusy	pokus	k1gInPc7	pokus
odhadnout	odhadnout	k5eAaPmF	odhadnout
celkové	celkový	k2eAgNnSc4d1	celkové
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
1018	[number]	k4	1018
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
oceánu	oceán	k1gInSc6	oceán
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
další	další	k2eAgInSc4d1	další
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledová	ledový	k2eAgFnSc1d1	ledová
pokrývka	pokrývka	k1gFnSc1	pokrývka
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
planetologů	planetolog	k1gMnPc2	planetolog
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pouze	pouze	k6eAd1	pouze
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vrstvy	vrstva	k1gFnSc2	vrstva
kůry	kůra	k1gFnSc2	kůra
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
pružně	pružně	k6eAd1	pružně
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
slapovými	slapový	k2eAgInPc7d1	slapový
jevy	jev	k1gInPc7	jev
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
budiž	budiž	k9	budiž
ohybová	ohybový	k2eAgFnSc1d1	ohybová
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
rovina	rovina	k1gFnSc1	rovina
nebo	nebo	k8xC	nebo
koule	koule	k1gFnSc1	koule
zatěžována	zatěžovat	k5eAaImNgFnS	zatěžovat
a	a	k8xC	a
ohýbána	ohýbat	k5eAaImNgFnS	ohýbat
velkým	velký	k2eAgNnSc7d1	velké
závažím	závaží	k1gNnSc7	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
jako	jako	k8xC	jako
tento	tento	k3xDgInSc4	tento
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnější	vnější	k2eAgFnSc1d1	vnější
pružná	pružný	k2eAgFnSc1d1	pružná
část	část	k1gFnSc1	část
ledového	ledový	k2eAgInSc2d1	ledový
příkrovu	příkrov	k1gInSc2	příkrov
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
slabá	slabý	k2eAgFnSc1d1	slabá
jen	jen	k9	jen
200	[number]	k4	200
m.	m.	k?	m.
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
krusta	krusta	k1gFnSc1	krusta
skutečně	skutečně	k6eAd1	skutečně
silná	silný	k2eAgFnSc1d1	silná
jen	jen	k9	jen
několik	několik	k4yIc1	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
reálná	reálný	k2eAgFnSc1d1	reálná
šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
skrze	skrze	k?	skrze
zlomy	zlom	k1gInPc1	zlom
a	a	k8xC	a
praskliny	prasklina	k1gFnPc1	prasklina
v	v	k7c6	v
ledovém	ledový	k2eAgInSc6d1	ledový
obalu	obal	k1gInSc6	obal
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
chaotického	chaotický	k2eAgInSc2d1	chaotický
terénu	terén	k1gInSc2	terén
při	při	k7c6	při
průvalu	průval	k1gInSc6	průval
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiter	Jupiter	k1gMnSc1	Jupiter
může	moct	k5eAaImIp3nS	moct
udržovat	udržovat	k5eAaImF	udržovat
oceány	oceán	k1gInPc4	oceán
Europy	Europa	k1gFnPc1	Europa
teplé	teplý	k2eAgFnPc1d1	teplá
vytvářením	vytváření	k1gNnSc7	vytváření
velkých	velký	k2eAgFnPc2d1	velká
planetárních	planetární	k2eAgFnPc2d1	planetární
slapových	slapový	k2eAgFnPc2d1	slapová
vln	vlna	k1gFnPc2	vlna
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
malého	malý	k2eAgNnSc2d1	malé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenulového	nulový	k2eNgInSc2d1	nenulový
sklonu	sklon	k1gInSc2	sklon
její	její	k3xOp3gFnSc2	její
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
původně	původně	k6eAd1	původně
neuvažované	uvažovaný	k2eNgFnPc1d1	neuvažovaná
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Rossbyho	Rossby	k1gMnSc2	Rossby
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
velkou	velký	k2eAgFnSc4d1	velká
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
sklonu	sklon	k1gInSc2	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
0,1	[number]	k4	0,1
<g/>
°	°	k?	°
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
uchovávat	uchovávat	k5eAaImF	uchovávat
až	až	k9	až
7,3	[number]	k4	7,3
<g/>
×	×	k?	×
<g/>
1017	[number]	k4	1017
J	J	kA	J
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vln	vlna	k1gFnPc2	vlna
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
dominantními	dominantní	k2eAgFnPc7d1	dominantní
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
zdrojem	zdroj	k1gInSc7	zdroj
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
oceány	oceán	k1gInPc4	oceán
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
objevila	objevit	k5eAaPmAgNnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Europa	Europa	k1gFnSc1	Europa
má	mít	k5eAaImIp3nS	mít
slabý	slabý	k2eAgInSc4d1	slabý
magnetický	magnetický	k2eAgInSc4d1	magnetický
moment	moment	k1gInSc4	moment
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vyvoláván	vyvolávat	k5eAaImNgInS	vyvolávat
různými	různý	k2eAgFnPc7d1	různá
částmi	část	k1gFnPc7	část
Jupiterova	Jupiterův	k2eAgNnSc2d1	Jupiterovo
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
magnetického	magnetický	k2eAgInSc2d1	magnetický
rovníku	rovník	k1gInSc2	rovník
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
okolo	okolo	k7c2	okolo
120	[number]	k4	120
nT	nT	k?	nT
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
šestina	šestina	k1gFnSc1	šestina
magnetického	magnetický	k2eAgInSc2d1	magnetický
momentu	moment	k1gInSc2	moment
Ganymédu	Ganyméd	k1gMnSc3	Ganyméd
a	a	k8xC	a
šestkrát	šestkrát	k6eAd1	šestkrát
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
u	u	k7c2	u
Callista	Callista	k1gMnSc1	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
indukovaného	indukovaný	k2eAgNnSc2d1	indukované
pole	pole	k1gNnSc2	pole
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vrstvu	vrstva	k1gFnSc4	vrstva
vysoce	vysoce	k6eAd1	vysoce
elektricky	elektricky	k6eAd1	elektricky
vodivého	vodivý	k2eAgInSc2d1	vodivý
materiálu	materiál	k1gInSc2	materiál
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
vhodným	vhodný	k2eAgMnSc7d1	vhodný
kandidátem	kandidát	k1gMnSc7	kandidát
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Spektroskopické	spektroskopický	k2eAgNnSc4d1	spektroskopické
měření	měření	k1gNnSc4	měření
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tmavé	tmavý	k2eAgFnPc1d1	tmavá
skvrny	skvrna	k1gFnPc1	skvrna
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
útvary	útvar	k1gInPc1	útvar
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c6	na
soli	sůl	k1gFnSc6	sůl
jako	jako	k8xC	jako
např.	např.	kA	např.
síran	síran	k1gInSc4	síran
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ukládal	ukládat	k5eAaImAgInS	ukládat
během	během	k7c2	během
vypařování	vypařování	k1gNnSc2	vypařování
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc4	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
jsou	být	k5eAaImIp3nP	být
dalším	další	k2eAgNnSc7d1	další
možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
pro	pro	k7c4	pro
spektroskopická	spektroskopický	k2eAgNnPc4d1	spektroskopické
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
obě	dva	k4xCgFnPc1	dva
zmiňované	zmiňovaný	k2eAgFnPc1d1	zmiňovaná
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
či	či	k8xC	či
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
se	se	k3xPyFc4	se
v	v	k7c6	v
ledu	led	k1gInSc6	led
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
nějaké	nějaký	k3yIgFnPc1	nějaký
další	další	k2eAgFnPc1d1	další
sloučeniny	sloučenina	k1gFnPc1	sloučenina
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc2d1	tvořící
červené	červený	k2eAgFnSc2d1	červená
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Složení	složení	k1gNnSc1	složení
oceánu	oceán	k1gInSc2	oceán
====	====	k?	====
</s>
</p>
<p>
<s>
Oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
potenciálně	potenciálně	k6eAd1	potenciálně
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
pod	pod	k7c7	pod
ledovým	ledový	k2eAgInSc7d1	ledový
krunýřem	krunýř	k1gInSc7	krunýř
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
vodou	voda	k1gFnSc7	voda
obohacenou	obohacený	k2eAgFnSc4d1	obohacená
o	o	k7c4	o
kyseliny	kyselina	k1gFnPc4	kyselina
a	a	k8xC	a
peroxid	peroxid	k1gInSc4	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
velice	velice	k6eAd1	velice
agresivní	agresivní	k2eAgNnSc4d1	agresivní
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
případný	případný	k2eAgInSc4d1	případný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhodnocení	vyhodnocení	k1gNnSc6	vyhodnocení
měření	měření	k1gNnSc2	měření
odraženého	odražený	k2eAgNnSc2d1	odražené
záření	záření	k1gNnSc2	záření
od	od	k7c2	od
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
provedeného	provedený	k2eAgInSc2d1	provedený
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
není	být	k5eNaImIp3nS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
vnitřku	vnitřek	k1gInSc2	vnitřek
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
interakcí	interakce	k1gFnSc7	interakce
s	s	k7c7	s
nabitými	nabitý	k2eAgFnPc7d1	nabitá
částicemi	částice	k1gFnPc7	částice
dopadajícími	dopadající	k2eAgFnPc7d1	dopadající
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
zdroje	zdroj	k1gInSc2	zdroj
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
látky	látka	k1gFnPc1	látka
potřebné	potřebný	k2eAgFnPc1d1	potřebná
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
těchto	tento	k3xDgFnPc2	tento
kyselin	kyselina	k1gFnPc2	kyselina
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
probíhající	probíhající	k2eAgFnSc7d1	probíhající
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podvodní	podvodní	k2eAgFnPc1d1	podvodní
sopky	sopka	k1gFnPc1	sopka
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
teoreticky	teoreticky	k6eAd1	teoreticky
vyvrhovat	vyvrhovat	k5eAaImF	vyvrhovat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
Europa	Europa	k1gFnSc1	Europa
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
velice	velice	k6eAd1	velice
podobala	podobat	k5eAaImAgFnS	podobat
měsíci	měsíc	k1gInPc7	měsíc
Io	Io	k1gMnPc2	Io
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
pevným	pevný	k2eAgInSc7d1	pevný
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
potenciálním	potenciální	k2eAgInSc7d1	potenciální
zdrojem	zdroj	k1gInSc7	zdroj
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
samotné	samotný	k2eAgFnSc2d1	samotná
soli	sůl	k1gFnSc2	sůl
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
tvořené	tvořený	k2eAgFnPc4d1	tvořená
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
===	===	k?	===
</s>
</p>
<p>
<s>
Pozorováním	pozorování	k1gNnSc7	pozorování
pomoci	pomoc	k1gFnSc2	pomoc
Goddard	Goddarda	k1gFnPc2	Goddarda
High	Higha	k1gFnPc2	Higha
Resolution	Resolution	k1gInSc1	Resolution
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
přineslo	přinést	k5eAaPmAgNnS	přinést
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
poznatky	poznatek	k1gInPc1	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
slabou	slabý	k2eAgFnSc4d1	slabá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Europy	Europa	k1gFnSc2	Europa
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
0,1	[number]	k4	0,1
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
přítomnost	přítomnost	k1gFnSc1	přítomnost
slabé	slabý	k2eAgFnSc2d1	slabá
ionosféry	ionosféra	k1gFnSc2	ionosféra
okolo	okolo	k7c2	okolo
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
interakcí	interakce	k1gFnSc7	interakce
slunečních	sluneční	k2eAgFnPc2d1	sluneční
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
energetických	energetický	k2eAgFnPc2d1	energetická
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
opět	opět	k6eAd1	opět
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
existenci	existence	k1gFnSc4	existence
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
kyslíku	kyslík	k1gInSc3	kyslík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc4	kyslík
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
není	být	k5eNaImIp3nS	být
biologického	biologický	k2eAgInSc2d1	biologický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
tělesa	těleso	k1gNnSc2	těleso
vzniká	vznikat	k5eAaImIp3nS	vznikat
radiolýzou	radiolýza	k1gFnSc7	radiolýza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rozkladný	rozkladný	k2eAgInSc1d1	rozkladný
proces	proces	k1gInSc1	proces
molekul	molekula	k1gFnPc2	molekula
vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
vlivem	vliv	k1gInSc7	vliv
radiace	radiace	k1gFnSc2	radiace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
dopadá	dopadat	k5eAaImIp3nS	dopadat
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnSc1d1	sluneční
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
ionty	ion	k1gInPc1	ion
a	a	k8xC	a
elektrony	elektron	k1gInPc1	elektron
<g/>
)	)	kIx)	)
z	z	k7c2	z
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
kolidují	kolidovat	k5eAaImIp3nP	kolidovat
s	s	k7c7	s
ledovým	ledový	k2eAgInSc7d1	ledový
povrchem	povrch	k1gInSc7	povrch
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
následně	následně	k6eAd1	následně
unikají	unikat	k5eAaImIp3nP	unikat
vlivem	vliv	k1gInSc7	vliv
dalšího	další	k2eAgNnSc2d1	další
působení	působení	k1gNnSc2	působení
radiace	radiace	k1gFnSc2	radiace
do	do	k7c2	do
řídké	řídký	k2eAgFnSc2d1	řídká
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgInSc1d1	molekulární
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
nejčetnější	četný	k2eAgFnSc7d3	nejčetnější
složkou	složka	k1gFnSc7	složka
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
molekulární	molekulární	k2eAgInSc4d1	molekulární
kyslík	kyslík	k1gInSc4	kyslík
se	se	k3xPyFc4	se
jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
dostává	dostávat	k5eAaImIp3nS	dostávat
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
zvíření	zvíření	k1gNnSc6	zvíření
opět	opět	k6eAd1	opět
vrátí	vrátit	k5eAaPmIp3nS	vrátit
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
nezmrzne	zmrznout	k5eNaPmIp3nS	zmrznout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
odrazí	odrazit	k5eAaPmIp3nS	odrazit
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
balistické	balistický	k2eAgFnSc6d1	balistická
křivce	křivka	k1gFnSc6	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
molekulární	molekulární	k2eAgInSc4d1	molekulární
vodík	vodík	k1gInSc4	vodík
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
lehký	lehký	k2eAgInSc1d1	lehký
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
utéci	utéct	k5eAaPmF	utéct
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
Europy	Europa	k1gFnSc2	Europa
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
kosmu	kosmos	k1gInSc2	kosmos
<g/>
.	.	kIx.	.
<g/>
Pozorování	pozorování	k1gNnSc1	pozorování
povrchu	povrch	k1gInSc2	povrch
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všechen	všechen	k3xTgInSc1	všechen
kyslík	kyslík	k1gInSc1	kyslík
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
radiolýzou	radiolýza	k1gFnSc7	radiolýza
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
vystřelen	vystřelit	k5eAaPmNgInS	vystřelit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
molekuly	molekula	k1gFnPc1	molekula
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
desorpcí	desorpce	k1gFnSc7	desorpce
do	do	k7c2	do
pravděpodobného	pravděpodobný	k2eAgInSc2d1	pravděpodobný
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
tektonickými	tektonický	k2eAgInPc7d1	tektonický
pochody	pochod	k1gInPc7	pochod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
následně	následně	k6eAd1	následně
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
teoretických	teoretický	k2eAgInPc6d1	teoretický
biologických	biologický	k2eAgInPc6d1	biologický
procesech	proces	k1gInPc6	proces
uvnitř	uvnitř	k7c2	uvnitř
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
studie	studie	k1gFnSc1	studie
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
obměny	obměna	k1gFnSc2	obměna
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
maximálního	maximální	k2eAgNnSc2d1	maximální
stáří	stáří	k1gNnSc2	stáří
okolo	okolo	k7c2	okolo
0,5	[number]	k4	0,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
subdukce	subdukce	k1gFnSc1	subdukce
radiolýzou	radiolýza	k1gFnSc7	radiolýza
uvolněného	uvolněný	k2eAgInSc2d1	uvolněný
kyslíku	kyslík	k1gInSc2	kyslík
může	moct	k5eAaImIp3nS	moct
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
obsah	obsah	k1gInSc4	obsah
volného	volný	k2eAgInSc2d1	volný
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
až	až	k9	až
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
srovnat	srovnat	k5eAaPmF	srovnat
s	s	k7c7	s
hodnotami	hodnota	k1gFnPc7	hodnota
v	v	k7c6	v
pozemských	pozemský	k2eAgInPc6d1	pozemský
oceánech	oceán	k1gInPc6	oceán
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
.	.	kIx.	.
<g/>
Molekulární	molekulární	k2eAgInSc4d1	molekulární
vodík	vodík	k1gInSc4	vodík
uniklý	uniklý	k2eAgInSc4d1	uniklý
z	z	k7c2	z
gravitační	gravitační	k2eAgFnSc2d1	gravitační
studně	studně	k1gFnSc2	studně
Europy	Europa	k1gFnSc2	Europa
společně	společně	k6eAd1	společně
s	s	k7c7	s
atomárním	atomární	k2eAgInSc7d1	atomární
a	a	k8xC	a
molekulárním	molekulární	k2eAgInSc7d1	molekulární
kyslíkem	kyslík	k1gInSc7	kyslík
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
torus	torus	k1gInSc1	torus
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Europy	Europa	k1gFnSc2	Europa
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Oblak	oblak	k1gInSc1	oblak
neutrálně	neutrálně	k6eAd1	neutrálně
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
detekovat	detekovat	k5eAaImF	detekovat
jak	jak	k6eAd1	jak
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
tak	tak	k8xC	tak
i	i	k9	i
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
měření	měření	k1gNnSc2	měření
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
torus	torus	k1gInSc1	torus
Europy	Europa	k1gFnSc2	Europa
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
než	než	k8xS	než
torus	torus	k1gInSc1	torus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
vulkanický	vulkanický	k2eAgInSc4d1	vulkanický
měsíc	měsíc	k1gInSc4	měsíc
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
předpovídají	předpovídat	k5eAaImIp3nP	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc4	každý
atom	atom	k1gInSc4	atom
nebo	nebo	k8xC	nebo
molekula	molekula	k1gFnSc1	molekula
v	v	k7c6	v
torusu	torus	k1gInSc6	torus
Europy	Europa	k1gFnSc2	Europa
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
ionizována	ionizován	k2eAgFnSc1d1	ionizována
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
tvoří	tvořit	k5eAaImIp3nS	tvořit
významný	významný	k2eAgInSc1d1	významný
zdroj	zdroj	k1gInSc1	zdroj
plazmatu	plazma	k1gNnSc2	plazma
v	v	k7c6	v
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
<g/>
Měření	měření	k1gNnSc1	měření
teploty	teplota	k1gFnSc2	teplota
povrchu	povrch	k1gInSc2	povrch
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Europa	Europa	k1gFnSc1	Europa
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiná	jiný	k2eAgNnPc1d1	jiné
tělesa	těleso	k1gNnPc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
teplot	teplota	k1gFnPc2	teplota
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
teploty	teplota	k1gFnPc1	teplota
okolo	okolo	k7c2	okolo
-160	-160	k4	-160
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
pólů	pól	k1gInPc2	pól
klesají	klesat	k5eAaImIp3nP	klesat
až	až	k9	až
na	na	k7c4	na
-220	-220	k4	-220
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Europa	Europa	k1gFnSc1	Europa
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
670	[number]	k4	670
900	[number]	k4	900
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejvnitřnější	vnitřní	k2eAgInSc4d3	nejvnitřnější
z	z	k7c2	z
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
drahami	draha	k1gFnPc7	draha
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnPc2	Io
a	a	k8xC	a
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
malých	malý	k2eAgInPc2d1	malý
(	(	kIx(	(
<g/>
známých	známý	k2eAgInPc2d1	známý
<g/>
)	)	kIx)	)
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
Europa	Europa	k1gFnSc1	Europa
šestým	šestý	k4xOgMnSc7	šestý
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rotace	rotace	k1gFnSc1	rotace
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oběžné	oběžný	k2eAgFnSc6d1	oběžná
rezonanci	rezonance	k1gFnSc6	rezonance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Io	Io	k1gFnPc2	Io
a	a	k8xC	a
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stihne	stihnout	k5eAaPmIp3nS	stihnout
vykonat	vykonat	k5eAaPmF	vykonat
dva	dva	k4xCgInPc4	dva
oběhy	oběh	k1gInPc4	oběh
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
než	než	k8xS	než
Ganymed	Ganymed	k1gMnSc1	Ganymed
jednou	jednou	k6eAd1	jednou
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
rezonance	rezonance	k1gFnSc1	rezonance
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržet	udržet	k5eAaPmF	udržet
sklon	sklon	k1gInSc4	sklon
oběžné	oběžný	k2eAgFnSc2d1	oběžná
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
0,009	[number]	k4	0,009
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
generovat	generovat	k5eAaImF	generovat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
teplo	teplo	k1gNnSc4	teplo
potřebné	potřebný	k2eAgNnSc4d1	potřebné
pro	pro	k7c4	pro
možnou	možný	k2eAgFnSc4d1	možná
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
činnost	činnost	k1gFnSc4	činnost
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rotace	rotace	k1gFnSc2	rotace
===	===	k?	===
</s>
</p>
<p>
<s>
Oběh	oběh	k1gInSc1	oběh
Europě	Europa	k1gFnSc6	Europa
trvá	trvat	k5eAaImIp3nS	trvat
3,551	[number]	k4	3,551
<g/>
181	[number]	k4	181
dne	den	k1gInSc2	den
při	při	k7c6	při
průměrné	průměrný	k2eAgFnSc6d1	průměrná
rychlosti	rychlost	k1gFnSc6	rychlost
13,740	[number]	k4	13,740
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
či	či	k8xC	či
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
obíhá	obíhat	k5eAaImIp3nS	obíhat
i	i	k9	i
Europa	Europa	k1gFnSc1	Europa
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
Europa	Europa	k1gFnSc1	Europa
má	mít	k5eAaImIp3nS	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Možný	možný	k2eAgInSc1d1	možný
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
oblasti	oblast	k1gFnPc1	oblast
Europy	Europa	k1gFnSc2	Europa
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nejžhavějšího	žhavý	k2eAgMnSc4d3	nejžhavější
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c6	na
objevení	objevení	k1gNnSc6	objevení
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
života	život	k1gInSc2	život
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
případném	případný	k2eAgInSc6d1	případný
oceánu	oceán	k1gInSc6	oceán
pod	pod	k7c7	pod
ledovou	ledový	k2eAgFnSc7d1	ledová
krustou	krusta	k1gFnSc7	krusta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
a	a	k8xC	a
adaptovat	adaptovat	k5eAaBmF	adaptovat
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
bez	bez	k7c2	bez
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
oceánů	oceán	k1gInPc2	oceán
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
černých	černý	k2eAgMnPc2d1	černý
kuřáků	kuřák	k1gMnPc2	kuřák
či	či	k8xC	či
jezera	jezero	k1gNnSc2	jezero
Vostok	Vostok	k1gInSc1	Vostok
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
sice	sice	k8xC	sice
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
výskyt	výskyt	k1gInSc1	výskyt
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
je	být	k5eAaImIp3nS	být
silným	silný	k2eAgInSc7d1	silný
argumentem	argument	k1gInSc7	argument
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
Europu	Europa	k1gFnSc4	Europa
byla	být	k5eAaImAgFnS	být
vyslána	vyslán	k2eAgFnSc1d1	vyslána
sonda	sonda	k1gFnSc1	sonda
<g/>
.	.	kIx.	.
<g/>
Až	až	k6eAd1	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
zdroji	zdroj	k1gInSc6	zdroj
energie	energie	k1gFnSc2	energie
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
energii	energie	k1gFnSc4	energie
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
jí	on	k3xPp3gFnSc3	on
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
na	na	k7c4	na
cukry	cukr	k1gInPc4	cukr
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
uvolňujíce	uvolňovat	k5eAaImSgFnP	uvolňovat
přitom	přitom	k6eAd1	přitom
kyslík	kyslík	k1gInSc4	kyslík
jako	jako	k8xS	jako
odpadní	odpadní	k2eAgInSc4d1	odpadní
produkt	produkt	k1gInSc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
následně	následně	k6eAd1	následně
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
dalších	další	k2eAgInPc2d1	další
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
kyslík	kyslík	k1gInSc4	kyslík
naopak	naopak	k6eAd1	naopak
přijímají	přijímat	k5eAaImIp3nP	přijímat
(	(	kIx(	(
<g/>
dýchají	dýchat	k5eAaImIp3nP	dýchat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivně	efektivně	k6eAd1	efektivně
získávat	získávat	k5eAaImF	získávat
energii	energie	k1gFnSc4	energie
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
hlubokomořské	hlubokomořský	k2eAgInPc1d1	hlubokomořský
organismy	organismus	k1gInPc1	organismus
v	v	k7c4	v
bentosu	bentosa	k1gFnSc4	bentosa
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
životu	život	k1gInSc3	život
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
se	s	k7c7	s
slunečním	sluneční	k2eAgNnSc7d1	sluneční
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
ale	ale	k9	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
učinit	učinit	k5eAaImF	učinit
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
změnil	změnit	k5eAaPmAgInS	změnit
chápání	chápání	k1gNnSc4	chápání
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
existence	existence	k1gFnSc2	existence
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průzkumu	průzkum	k1gInSc2	průzkum
Galapážského	galapážský	k2eAgInSc2d1	galapážský
riftu	rift	k1gInSc2	rift
vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
kolonie	kolonie	k1gFnPc4	kolonie
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hydrotermálních	hydrotermální	k2eAgInPc2d1	hydrotermální
průduchů	průduch	k1gInPc2	průduch
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
získávaly	získávat	k5eAaImAgFnP	získávat
jak	jak	k8xS	jak
živiny	živina	k1gFnPc1	živina
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
životu	život	k1gInSc3	život
tak	tak	k9	tak
tyto	tento	k3xDgInPc4	tento
společenstva	společenstvo	k1gNnSc2	společenstvo
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
žádné	žádný	k3yNgInPc1	žádný
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
a	a	k8xC	a
i	i	k9	i
jejich	jejich	k3xOp3gInSc1	jejich
potravní	potravní	k2eAgInSc1d1	potravní
řetězec	řetězec	k1gInSc1	řetězec
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
zcela	zcela	k6eAd1	zcela
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
rostlin	rostlina	k1gFnPc2	rostlina
základ	základ	k1gInSc4	základ
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
tvoří	tvořit	k5eAaImIp3nP	tvořit
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
získávat	získávat	k5eAaImF	získávat
energii	energie	k1gFnSc4	energie
oxidací	oxidace	k1gFnPc2	oxidace
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc4	vodík
či	či	k8xC	či
sulfan	sulfan	k1gInSc4	sulfan
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
znamenal	znamenat	k5eAaImAgInS	znamenat
zvrat	zvrat	k1gInSc4	zvrat
v	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
existovat	existovat	k5eAaImF	existovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
zřejmě	zřejmě	k6eAd1	zřejmě
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
živiny	živina	k1gFnPc1	živina
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
totiž	totiž	k9	totiž
některé	některý	k3yIgInPc4	některý
organismy	organismus	k1gInPc4	organismus
ve	v	k7c6	v
zmíněných	zmíněný	k2eAgFnPc6d1	zmíněná
hydrotermálních	hydrotermální	k2eAgFnPc6d1	hydrotermální
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
dýchají	dýchat	k5eAaImIp3nP	dýchat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
i	i	k9	i
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
anaerobní	anaerobní	k2eAgInSc4d1	anaerobní
–	–	k?	–
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
životu	život	k1gInSc3	život
kyslík	kyslík	k1gInSc1	kyslík
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
organismy	organismus	k1gInPc1	organismus
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
kyslíku	kyslík	k1gInSc6	kyslík
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
velkou	velký	k2eAgFnSc7d1	velká
nadějí	naděje	k1gFnSc7	naděje
pro	pro	k7c4	pro
možný	možný	k2eAgInSc4d1	možný
život	život	k1gInSc4	život
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
nejspíše	nejspíše	k9	nejspíše
volné	volný	k2eAgInPc4d1	volný
atomy	atom	k1gInPc4	atom
kyslíku	kyslík	k1gInSc2	kyslík
i	i	k8xC	i
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
existují	existovat	k5eAaImIp3nP	existovat
–	–	k?	–
pronikají	pronikat	k5eAaImIp3nP	pronikat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
ledové	ledový	k2eAgFnSc2d1	ledová
krusty	krusta	k1gFnSc2	krusta
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nP	by
teoreticky	teoreticky	k6eAd1	teoreticky
mohly	moct	k5eAaImAgFnP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
vhodné	vhodný	k2eAgNnSc4d1	vhodné
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
organismy	organismus	k1gInPc4	organismus
vyžadující	vyžadující	k2eAgInPc4d1	vyžadující
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
uvolňovaná	uvolňovaný	k2eAgFnSc1d1	uvolňovaná
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
řídí	řídit	k5eAaImIp3nS	řídit
geologické	geologický	k2eAgInPc1d1	geologický
procesy	proces	k1gInPc1	proces
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c6	na
sesterském	sesterský	k2eAgInSc6d1	sesterský
měsíci	měsíc	k1gInSc6	měsíc
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
měsíc	měsíc	k1gInSc4	měsíc
nejspíše	nejspíše	k9	nejspíše
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
zásoby	zásoba	k1gFnPc4	zásoba
energie	energie	k1gFnSc2	energie
získávané	získávaný	k2eAgMnPc4d1	získávaný
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
získávána	získáván	k2eAgFnSc1d1	získávána
ze	z	k7c2	z
slapů	slap	k1gInPc2	slap
několikanásobně	několikanásobně	k6eAd1	několikanásobně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
život	život	k1gInSc1	život
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
zdroji	zdroj	k1gInSc6	zdroj
energie	energie	k1gFnSc2	energie
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
bohatý	bohatý	k2eAgInSc4d1	bohatý
ekosystém	ekosystém	k1gInSc4	ekosystém
jako	jako	k8xC	jako
ten	ten	k3xDgInSc4	ten
pozemský	pozemský	k2eAgInSc4d1	pozemský
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc4	život
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
byl	být	k5eAaImAgInS	být
nejspíše	nejspíše	k9	nejspíše
jen	jen	k9	jen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
žily	žít	k5eAaImAgFnP	žít
poblíž	poblíž	k6eAd1	poblíž
geotermálních	geotermální	k2eAgFnPc2d1	geotermální
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nP	by
zde	zde	k6eAd1	zde
měly	mít	k5eAaImAgFnP	mít
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
či	či	k8xC	či
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
vznášely	vznášet	k5eAaImAgFnP	vznášet
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
oceán	oceán	k1gInSc1	oceán
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
příliš	příliš	k6eAd1	příliš
studený	studený	k2eAgMnSc1d1	studený
<g/>
,	,	kIx,	,
biologické	biologický	k2eAgInPc1d1	biologický
procesy	proces	k1gInPc1	proces
známé	známý	k2eAgInPc1d1	známý
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
by	by	kYmCp3nP	by
nemohly	moct	k5eNaImAgFnP	moct
probíhat	probíhat	k5eAaImF	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
příliš	příliš	k6eAd1	příliš
slaná	slaný	k2eAgFnSc1d1	slaná
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
by	by	kYmCp3nP	by
zde	zde	k6eAd1	zde
žít	žít	k5eAaImF	žít
pouze	pouze	k6eAd1	pouze
extrémně	extrémně	k6eAd1	extrémně
odolné	odolný	k2eAgInPc1d1	odolný
organismy	organismus	k1gInPc1	organismus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Robert	Robert	k1gMnSc1	Robert
T.	T.	kA	T.
Pappalardo	Pappalardo	k1gNnSc1	Pappalardo
z	z	k7c2	z
Laboratory	Laborator	k1gMnPc4	Laborator
for	forum	k1gNnPc2	forum
Atmospheric	Atmospherice	k1gFnPc2	Atmospherice
and	and	k?	and
Space	Spaec	k1gInSc2	Spaec
Physics	Physicsa	k1gFnPc2	Physicsa
při	při	k7c6	při
Univerzitě	univerzita	k1gFnSc6	univerzita
Colorado	Colorado	k1gNnSc1	Colorado
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
pozorování	pozorování	k1gNnSc1	pozorování
a	a	k8xC	a
pojmenování	pojmenování	k1gNnSc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Europa	Europa	k1gFnSc1	Europa
společně	společně	k6eAd1	společně
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
dalšími	další	k2eAgInPc7d1	další
velkými	velký	k2eAgInPc7d1	velký
měsíci	měsíc	k1gInPc7	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
Io	Io	k1gFnPc1	Io
<g/>
,	,	kIx,	,
Ganymede	Ganymed	k1gMnSc5	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
italským	italský	k2eAgInSc7d1	italský
astronomem	astronom	k1gMnSc7	astronom
Galileo	Galilea	k1gFnSc5	Galilea
Galileim	Galileimo	k1gNnPc2	Galileimo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
všechny	všechen	k3xTgInPc4	všechen
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
i	i	k8xC	i
Europa	Europa	k1gFnSc1	Europa
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
milence	milenka	k1gFnSc6	milenka
Dia	Dia	k1gFnPc2	Dia
Európě	Európa	k1gFnSc3	Európa
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Európa	Európa	k1gFnSc1	Európa
či	či	k8xC	či
též	též	k9	též
Európé	Európý	k2eAgNnSc1d1	Európé
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
sídónského	sídónský	k2eAgMnSc2d1	sídónský
krále	král	k1gMnSc2	král
Agénora	Agénor	k1gMnSc2	Agénor
(	(	kIx(	(
<g/>
v	v	k7c6	v
Ovidiových	Ovidiův	k2eAgFnPc6d1	Ovidiova
Proměnách	proměna	k1gFnPc6	proměna
zvána	zván	k2eAgFnSc1d1	zvána
"	"	kIx"	"
<g/>
Agénorovna	Agénorovna	k1gFnSc1	Agénorovna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Télefassy	Télefassa	k1gFnSc2	Télefassa
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
všechny	všechen	k3xTgInPc4	všechen
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
milenkách	milenka	k1gFnPc6	milenka
Dia	Dia	k1gFnSc2	Dia
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
současník	současník	k1gMnSc1	současník
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
objevil	objevit	k5eAaPmAgMnS	objevit
satelity	satelit	k1gMnPc4	satelit
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Galileovi	Galileus	k1gMnSc6	Galileus
a	a	k8xC	a
kterého	který	k3yQgMnSc4	který
Galileo	Galilea	k1gFnSc5	Galilea
podezíral	podezírat	k5eAaImAgMnS	podezírat
z	z	k7c2	z
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
objevu	objev	k1gInSc6	objev
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
pojmenování	pojmenování	k1gNnSc4	pojmenování
předal	předat	k5eAaPmAgMnS	předat
Marius	Marius	k1gMnSc1	Marius
Keplerovi	Keplerův	k2eAgMnPc1d1	Keplerův
<g/>
.	.	kIx.	.
<g/>
Jména	jméno	k1gNnSc2	jméno
postupně	postupně	k6eAd1	postupně
zapadla	zapadnout	k5eAaPmAgFnS	zapadnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
zájmu	zájem	k1gInSc2	zájem
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
rozšířeně	rozšířeně	k6eAd1	rozšířeně
používány	používat	k5eAaImNgFnP	používat
do	do	k7c2	do
polovičky	polovička	k1gFnSc2	polovička
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vzkříšení	vzkříšení	k1gNnSc3	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
odborné	odborný	k2eAgFnSc6d1	odborná
astronomické	astronomický	k2eAgFnSc6d1	astronomická
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
Europa	Europa	k1gFnSc1	Europa
popisována	popisován	k2eAgFnSc1d1	popisována
jednoduše	jednoduše	k6eAd1	jednoduše
pomocí	pomocí	k7c2	pomocí
římské	římský	k2eAgFnSc2d1	římská
číslice	číslice	k1gFnSc2	číslice
Jupiter	Jupiter	k1gMnSc1	Jupiter
II	II	kA	II
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
systému	systém	k1gInSc2	systém
navrženým	navržený	k2eAgMnSc7d1	navržený
Galileem	Galileus	k1gMnSc7	Galileus
<g/>
)	)	kIx)	)
či	či	k8xC	či
jako	jako	k9	jako
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc1	druhý
satelit	satelit	k1gInSc1	satelit
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevení	objevení	k1gNnSc3	objevení
dalšího	další	k2eAgInSc2d1	další
satelitu	satelit	k1gInSc2	satelit
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc4	měsíc
Amalthea	Amalthe	k1gInSc2	Amalthe
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
bližší	blízký	k2eAgFnSc6d2	bližší
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
,	,	kIx,	,
než	než	k8xS	než
všechny	všechen	k3xTgInPc4	všechen
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
posunutí	posunutí	k1gNnSc3	posunutí
Europy	Europa	k1gFnSc2	Europa
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
pozice	pozice	k1gFnSc2	pozice
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
sondy	sonda	k1gFnPc1	sonda
z	z	k7c2	z
programu	program	k1gInSc2	program
Voyager	Voyagra	k1gFnPc2	Voyagra
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
objevily	objevit	k5eAaPmAgFnP	objevit
další	další	k2eAgInPc4d1	další
3	[number]	k4	3
satelity	satelit	k1gInPc4	satelit
ležící	ležící	k2eAgFnSc2d1	ležící
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
než	než	k8xS	než
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Europu	Europa	k1gFnSc4	Europa
odsunulo	odsunout	k5eAaPmAgNnS	odsunout
až	až	k9	až
na	na	k7c4	na
šestou	šestý	k4xOgFnSc4	šestý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
ale	ale	k9	ale
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
dnešních	dnešní	k2eAgInPc6d1	dnešní
dnech	den	k1gInPc6	den
starší	starý	k2eAgMnSc1d2	starší
Galileovo	Galileův	k2eAgNnSc4d1	Galileovo
označení	označení	k1gNnSc4	označení
Jupiter	Jupiter	k1gMnSc1	Jupiter
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průzkum	průzkum	k1gInSc1	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
lidských	lidský	k2eAgFnPc2d1	lidská
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
Europě	Europa	k1gFnSc6	Europa
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
série	série	k1gFnSc2	série
přeletů	přelet	k1gInPc2	přelet
sond	sonda	k1gFnPc2	sonda
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
prolétly	prolétnout	k5eAaPmAgFnP	prolétnout
americké	americký	k2eAgFnPc1d1	americká
sondy	sonda	k1gFnPc1	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
a	a	k8xC	a
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jako	jako	k9	jako
první	první	k4xOgFnPc1	první
navštívily	navštívit	k5eAaPmAgFnP	navštívit
planetu	planeta	k1gFnSc4	planeta
Jupiter	Jupiter	k1gInSc4	Jupiter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
respektive	respektive	k9	respektive
druhá	druhý	k4xOgFnSc1	druhý
sonda	sonda	k1gFnSc1	sonda
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
pořízené	pořízený	k2eAgInPc4d1	pořízený
snímky	snímek	k1gInPc4	snímek
měsíce	měsíc	k1gInSc2	měsíc
od	od	k7c2	od
sond	sonda	k1gFnPc2	sonda
Pioneer	Pioneer	kA	Pioneer
zaslané	zaslaný	k2eAgInPc1d1	zaslaný
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
neostré	ostrý	k2eNgFnPc1d1	neostrá
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
dvě	dva	k4xCgFnPc4	dva
sondy	sonda	k1gFnPc4	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
proletěly	proletět	k5eAaPmAgFnP	proletět
soustavou	soustava	k1gFnSc7	soustava
Jupitera	Jupiter	k1gMnSc2	Jupiter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
získaly	získat	k5eAaPmAgFnP	získat
kvalitnější	kvalitní	k2eAgFnPc1d2	kvalitnější
fotografie	fotografia	k1gFnPc1	fotografia
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
Europu	Europa	k1gFnSc4	Europa
další	další	k2eAgFnSc1d1	další
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
určená	určený	k2eAgFnSc1d1	určená
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
obíhajících	obíhající	k2eAgInPc2d1	obíhající
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
pak	pak	k6eAd1	pak
úspěšně	úspěšně	k6eAd1	úspěšně
dorazila	dorazit	k5eAaPmAgFnS	dorazit
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c4	po
osm	osm	k4xCc4	osm
let	let	k1gInSc4	let
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
Jupiter	Jupiter	k1gInSc4	Jupiter
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
nakonec	nakonec	k6eAd1	nakonec
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
až	až	k9	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shořela	shořet	k5eAaPmAgFnS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
zánikem	zánik	k1gInSc7	zánik
proletěla	proletět	k5eAaPmAgFnS	proletět
kolem	kolem	k7c2	kolem
Europy	Europa	k1gFnSc2	Europa
pouze	pouze	k6eAd1	pouze
12	[number]	k4	12
krát	krát	k6eAd1	krát
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
průlet	průlet	k1gInSc4	průlet
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
detailně	detailně	k6eAd1	detailně
nafotit	nafotit	k5eAaPmF	nafotit
jenom	jenom	k9	jenom
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
plánované	plánovaný	k2eAgFnPc4d1	plánovaná
mise	mise	k1gFnPc4	mise
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
mnoho	mnoho	k4c1	mnoho
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tyto	tento	k3xDgFnPc4	tento
mise	mise	k1gFnPc4	mise
provést	provést	k5eAaPmF	provést
a	a	k8xC	a
jak	jak	k8xC	jak
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
Europu	Europa	k1gFnSc4	Europa
nejlépe	dobře	k6eAd3	dobře
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
mise	mise	k1gFnSc1	mise
k	k	k7c3	k
Europě	Europa	k1gFnSc3	Europa
si	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
poradit	poradit	k5eAaPmF	poradit
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
radiací	radiace	k1gFnSc7	radiace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
panuje	panovat	k5eAaImIp3nS	panovat
vlivem	vliv	k1gInSc7	vliv
pozice	pozice	k1gFnPc4	pozice
uvnitř	uvnitř	k7c2	uvnitř
radiačních	radiační	k2eAgInPc2d1	radiační
pásů	pás	k1gInPc2	pás
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Europa	Europa	k1gFnSc1	Europa
obdrží	obdržet	k5eAaPmIp3nS	obdržet
okolo	okolo	k7c2	okolo
5,4	[number]	k4	5,4
Sv	sv	kA	sv
radiace	radiace	k1gFnSc2	radiace
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
budoucích	budoucí	k2eAgFnPc2d1	budoucí
misí	mise	k1gFnPc2	mise
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
potenciálních	potenciální	k2eAgFnPc6d1	potenciální
známkách	známka	k1gFnPc6	známka
života	život	k1gInSc2	život
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Budoucí	budoucí	k2eAgInSc4d1	budoucí
výzkum	výzkum	k1gInSc4	výzkum
===	===	k?	===
</s>
</p>
<p>
<s>
Plány	plán	k1gInPc1	plán
USA	USA	kA	USA
na	na	k7c6	na
vyslání	vyslání	k1gNnSc6	vyslání
sondy	sonda	k1gFnSc2	sonda
určené	určený	k2eAgFnSc2d1	určená
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
případných	případný	k2eAgFnPc2d1	případná
známek	známka	k1gFnPc2	známka
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
možného	možný	k2eAgInSc2d1	možný
života	život	k1gInSc2	život
byly	být	k5eAaImAgInP	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
sužovány	sužován	k2eAgInPc1d1	sužován
škrty	škrt	k1gInPc1	škrt
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2020	[number]	k4	2020
naplánován	naplánován	k2eAgInSc4d1	naplánován
start	start	k1gInSc4	start
sondy	sonda	k1gFnSc2	sonda
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
agentur	agentura	k1gFnPc2	agentura
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
startem	start	k1gInSc7	start
mise	mise	k1gFnSc2	mise
Titan	titan	k1gInSc1	titan
Saturn	Saturn	k1gMnSc1	Saturn
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
měsíce	měsíc	k1gInSc2	měsíc
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
evropská	evropský	k2eAgFnSc1d1	Evropská
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
čelit	čelit	k5eAaImF	čelit
možným	možný	k2eAgFnPc3d1	možná
rozpočtovým	rozpočtový	k2eAgFnPc3d1	rozpočtová
potížím	potíž	k1gFnPc3	potíž
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
i	i	k9	i
nadále	nadále	k6eAd1	nadále
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
o	o	k7c4	o
dotaci	dotace	k1gFnSc4	dotace
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
EJSM	EJSM	kA	EJSM
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
modulu	modul	k1gInSc2	modul
Jupiter	Jupiter	k1gMnSc1	Jupiter
Europa	Europ	k1gMnSc2	Europ
Orbiter	Orbiter	k1gMnSc1	Orbiter
a	a	k8xC	a
evropského	evropský	k2eAgInSc2d1	evropský
modulu	modul	k1gInSc2	modul
Jupiter	Jupiter	k1gMnSc1	Jupiter
Ganymede	Ganymed	k1gMnSc5	Ganymed
Orbiter	Orbitra	k1gFnPc2	Orbitra
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
připojí	připojit	k5eAaPmIp3nS	připojit
i	i	k9	i
japonská	japonský	k2eAgFnSc1d1	japonská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
JAXA	JAXA	kA	JAXA
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
modulem	modul	k1gInSc7	modul
Jupiter	Jupiter	k1gMnSc1	Jupiter
Magnetospheric	Magnetospheric	k1gMnSc1	Magnetospheric
Orbiter	Orbiter	k1gMnSc1	Orbiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
misí	mise	k1gFnSc7	mise
EJSM	EJSM	kA	EJSM
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zrušena	zrušit	k5eAaPmNgFnS	zrušit
příliš	příliš	k6eAd1	příliš
ambiciózní	ambiciózní	k2eAgFnSc1d1	ambiciózní
mise	mise	k1gFnSc1	mise
Jupiter	Jupiter	k1gMnSc1	Jupiter
Icy	Icy	k1gMnSc1	Icy
Moons	Moonsa	k1gFnPc2	Moonsa
Orbiter	Orbiter	k1gMnSc1	Orbiter
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgInPc4d1	další
plány	plán	k1gInPc4	plán
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
návrh	návrh	k1gInSc4	návrh
na	na	k7c6	na
misi	mise	k1gFnSc6	mise
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ice	Ice	k1gFnSc2	Ice
Clipper	clipper	k1gInSc1	clipper
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
sondu	sonda	k1gFnSc4	sonda
s	s	k7c7	s
impaktorem	impaktor	k1gInSc7	impaktor
podobnému	podobný	k2eAgNnSc3d1	podobné
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
použila	použít	k5eAaPmAgFnS	použít
sonda	sonda	k1gFnSc1	sonda
Deep	Deep	k1gMnSc1	Deep
Impact	Impact	k1gInSc1	Impact
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
vyvrhnout	vyvrhnout	k5eAaPmF	vyvrhnout
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
orbitálním	orbitální	k2eAgInSc7d1	orbitální
modulem	modul	k1gInSc7	modul
či	či	k8xC	či
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
odběr	odběr	k1gInSc4	odběr
vzorků	vzorek	k1gInPc2	vzorek
tohoto	tento	k3xDgInSc2	tento
oblaku	oblak	k1gInSc2	oblak
zachycených	zachycený	k2eAgFnPc2d1	zachycená
prolétající	prolétající	k2eAgFnSc7d1	prolétající
sondou	sonda	k1gFnSc7	sonda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
odvážnější	odvážný	k2eAgInPc4d2	odvážnější
plány	plán	k1gInPc4	plán
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
Europy	Europa	k1gFnSc2	Europa
jako	jako	k8xS	jako
například	například	k6eAd1	například
návrh	návrh	k1gInSc4	návrh
vyslat	vyslat	k5eAaPmF	vyslat
k	k	k7c3	k
měsíci	měsíc	k1gInSc3	měsíc
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
hledat	hledat	k5eAaImF	hledat
stopy	stopa	k1gFnPc4	stopa
potenciálního	potenciální	k2eAgInSc2d1	potenciální
života	život	k1gInSc2	život
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
v	v	k7c6	v
povrchovém	povrchový	k2eAgInSc6d1	povrchový
ledu	led	k1gInSc6	led
či	či	k8xC	či
plány	plán	k1gInPc4	plán
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
průzkum	průzkum	k1gInSc4	průzkum
případného	případný	k2eAgInSc2d1	případný
oceánu	oceán	k1gInSc2	oceán
rozkládajícího	rozkládající	k2eAgInSc2d1	rozkládající
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
konceptů	koncept	k1gInPc2	koncept
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
vyslat	vyslat	k5eAaPmF	vyslat
kryobota	kryobota	k1gFnSc1	kryobota
s	s	k7c7	s
atomovým	atomový	k2eAgInSc7d1	atomový
reaktorem	reaktor	k1gInSc7	reaktor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
protavit	protavit	k5eAaPmF	protavit
skrz	skrz	k7c4	skrz
vrstvu	vrstva	k1gFnSc4	vrstva
ledu	led	k1gInSc2	led
až	až	k9	až
k	k	k7c3	k
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
protavení	protavení	k1gNnSc6	protavení
k	k	k7c3	k
oceánu	oceán	k1gInSc3	oceán
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
sonda	sonda	k1gFnSc1	sonda
vypustila	vypustit	k5eAaPmAgFnS	vypustit
hydrobota	hydrobot	k1gMnSc4	hydrobot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
oceán	oceán	k1gInSc4	oceán
a	a	k8xC	a
získaná	získaný	k2eAgNnPc4d1	získané
data	datum	k1gNnPc4	datum
poslat	poslat	k5eAaPmF	poslat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
zamezení	zamezení	k1gNnSc1	zamezení
kontaminace	kontaminace	k1gFnSc2	kontaminace
oceánu	oceán	k1gInSc2	oceán
pozemskými	pozemský	k2eAgInPc7d1	pozemský
organismy	organismus	k1gInPc7	organismus
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc4	dva
dvě	dva	k4xCgFnPc4	dva
sondy	sonda	k1gFnPc4	sonda
by	by	kYmCp3nP	by
proto	proto	k8xC	proto
prošly	projít	k5eAaPmAgFnP	projít
důkladnou	důkladný	k2eAgFnSc7d1	důkladná
sterilizací	sterilizace	k1gFnSc7	sterilizace
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
seriozních	seriozní	k2eAgFnPc2d1	seriozní
příprav	příprava	k1gFnPc2	příprava
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
pracovní	pracovní	k2eAgFnSc4d1	pracovní
koncepci	koncepce	k1gFnSc4	koncepce
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Europa	Europ	k1gMnSc2	Europ
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sci-fi	scii	k1gNnPc3	sci-fi
===	===	k?	===
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Europa	Europa	k1gFnSc1	Europa
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
přitahoval	přitahovat	k5eAaImAgMnS	přitahovat
pozornost	pozornost	k1gFnSc4	pozornost
spisovatelů	spisovatel	k1gMnPc2	spisovatel
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
potenciálního	potenciální	k2eAgInSc2d1	potenciální
extraterestrického	extraterestrický	k2eAgInSc2d1	extraterestrický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
Europa	Europa	k1gFnSc1	Europa
figuruje	figurovat	k5eAaImIp3nS	figurovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Arthura	Arthur	k1gMnSc2	Arthur
C.	C.	kA	C.
Clarka	Clarek	k1gMnSc2	Clarek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Druhá	druhý	k4xOgFnSc1	druhý
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
2061	[number]	k4	2061
<g/>
:	:	kIx,	:
Třetí	třetí	k4xOgFnSc1	třetí
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mimozemské	mimozemský	k2eAgInPc4d1	mimozemský
monolity	monolit	k1gInPc4	monolit
způsobí	způsobit	k5eAaPmIp3nS	způsobit
přeměnu	přeměna	k1gFnSc4	přeměna
Jupiteru	Jupiter	k1gInSc2	Jupiter
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
roztátí	roztátý	k2eAgMnPc1d1	roztátý
ledové	ledový	k2eAgFnPc1d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
nastartování	nastartování	k1gNnSc1	nastartování
evoluce	evoluce	k1gFnSc2	evoluce
původního	původní	k2eAgInSc2d1	původní
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
pokračování	pokračování	k1gNnSc6	pokračování
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc1	měsíc
přeměněn	přeměněn	k2eAgInSc1d1	přeměněn
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
tropické	tropický	k2eAgNnSc4d1	tropické
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vstup	vstup	k1gInSc1	vstup
lidem	člověk	k1gMnPc3	člověk
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zapovězen	zapovědět	k5eAaPmNgInS	zapovědět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
spisovatele	spisovatel	k1gMnPc4	spisovatel
pojednávající	pojednávající	k2eAgMnPc4d1	pojednávající
o	o	k7c6	o
Europě	Europa	k1gFnSc6	Europa
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Greg	Greg	k1gMnSc1	Greg
Bear	Bear	k1gMnSc1	Bear
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
The	The	k1gMnSc2	The
Forge	Forg	k1gMnSc2	Forg
of	of	k?	of
God	God	k1gMnSc2	God
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
popisující	popisující	k2eAgFnSc4d1	popisující
mimozemskou	mimozemský	k2eAgFnSc4d1	mimozemská
rasu	rasa	k1gFnSc4	rasa
využívající	využívající	k2eAgInPc4d1	využívající
kusy	kus	k1gInPc4	kus
ledu	led	k1gInSc2	led
k	k	k7c3	k
terraformování	terraformování	k1gNnSc3	terraformování
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
Europa	Europa	k1gFnSc1	Europa
Strike	Strike	k1gFnSc1	Strike
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
od	od	k7c2	od
Iana	Ianus	k1gMnSc2	Ianus
Douglase	Douglasa	k1gFnSc3	Douglasa
popisuje	popisovat	k5eAaImIp3nS	popisovat
objevení	objevení	k1gNnSc1	objevení
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
lodi	loď	k1gFnSc2	loď
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
bitvu	bitva	k1gFnSc4	bitva
mezi	mezi	k7c4	mezi
Číňany	Číňan	k1gMnPc4	Číňan
a	a	k8xC	a
Američany	Američan	k1gMnPc4	Američan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
misi	mise	k1gFnSc6	mise
na	na	k7c6	na
Europu	Europ	k1gInSc6	Europ
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
sci-fi	scii	k1gFnSc1	sci-fi
thriller	thriller	k1gInSc1	thriller
ekvádorského	ekvádorský	k2eAgMnSc2d1	ekvádorský
režiséra	režisér	k1gMnSc2	režisér
Sebastiána	Sebastián	k1gMnSc2	Sebastián
Cordera	Corder	k1gMnSc2	Corder
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Europě	Europa	k1gFnSc6	Europa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Europa	Europa	k1gFnSc1	Europa
(	(	kIx(	(
<g/>
moon	moon	k1gInSc1	moon
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAGENAL	BAGENAL	kA	BAGENAL
<g/>
,	,	kIx,	,
Fran	Fran	k1gNnSc1	Fran
<g/>
,	,	kIx,	,
Dowling	Dowling	k1gInSc1	Dowling
<g/>
,	,	kIx,	,
Timothy	Timotha	k1gFnPc1	Timotha
Edward	Edward	k1gMnSc1	Edward
<g/>
;	;	kIx,	;
and	and	k?	and
McKinnon	McKinnon	k1gInSc1	McKinnon
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
B.	B.	kA	B.
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
Satellites	Satellitesa	k1gFnPc2	Satellitesa
and	and	k?	and
Magnetosphere	Magnetospher	k1gInSc5	Magnetospher
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
521818087	[number]	k4	521818087
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROTHERY	ROTHERY	kA	ROTHERY
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
A.	A.	kA	A.
Satellites	Satellites	k1gMnSc1	Satellites
of	of	k?	of
the	the	k?	the
Outer	Outer	k1gInSc1	Outer
Planets	Planets	k1gInSc1	Planets
<g/>
:	:	kIx,	:
Worlds	Worlds	k1gInSc1	Worlds
in	in	k?	in
Their	Their	k1gInSc1	Their
Own	Own	k1gMnSc1	Own
Right	Right	k1gMnSc1	Right
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
US	US	kA	US
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
512555	[number]	k4	512555
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HARLAND	HARLAND	kA	HARLAND
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
M.	M.	kA	M.
Jupiter	Jupiter	k1gMnSc1	Jupiter
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
NASA	NASA	kA	NASA
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Galileo	Galilea	k1gFnSc5	Galilea
Mission	Mission	k1gInSc4	Mission
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1852333014	[number]	k4	1852333014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jupiterovy	Jupiterův	k2eAgInPc4d1	Jupiterův
měsíce	měsíc	k1gInPc4	měsíc
</s>
</p>
<p>
<s>
Kolonizace	kolonizace	k1gFnSc1	kolonizace
Europy	Europa	k1gFnSc2	Europa
</s>
</p>
<p>
<s>
Vostok	Vostok	k1gInSc1	Vostok
(	(	kIx(	(
<g/>
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Europa	Europ	k1gMnSc2	Europ
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
mapa	mapa	k1gFnSc1	mapa
povrchu	povrch	k1gInSc2	povrch
Europy	Europa	k1gFnSc2	Europa
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
technologii	technologie	k1gFnSc4	technologie
Google	Google	k1gFnSc2	Google
</s>
</p>
<p>
<s>
Dočkáme	dočkat	k5eAaPmIp1nP	dočkat
se	se	k3xPyFc4	se
výzkumu	výzkum	k1gInSc3	výzkum
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
měsíce	měsíc	k1gInSc2	měsíc
Europa	Europ	k1gMnSc4	Europ
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
článek	článek	k1gInSc1	článek
astro	astra	k1gFnSc5	astra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
převzatý	převzatý	k2eAgInSc1d1	převzatý
z	z	k7c2	z
HowStuffWorks	HowStuffWorksa	k1gFnPc2	HowStuffWorksa
</s>
</p>
