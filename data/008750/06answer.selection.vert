<s>
Europa	Europa	k1gFnSc1	Europa
(	(	kIx(	(
<g/>
též	též	k9	též
Jupiter	Jupiter	k1gMnSc1	Jupiter
II	II	kA	II
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejmenší	malý	k2eAgInPc1d3	nejmenší
z	z	k7c2	z
Galileovských	Galileovský	k2eAgInPc2d1	Galileovský
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
