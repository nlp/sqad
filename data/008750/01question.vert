<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
měsíc	měsíc	k1gInSc1	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc4	druhý
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejmenší	malý	k2eAgInPc1d3	nejmenší
z	z	k7c2	z
Galileovských	Galileovský	k2eAgInPc2d1	Galileovský
měsíců	měsíc	k1gInPc2	měsíc
<g/>
?	?	kIx.	?
</s>
