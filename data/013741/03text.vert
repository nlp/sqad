<s>
Megazostrodon	Megazostrodon	k1gMnSc1
</s>
<s>
MegazostrodonStratigrafický	MegazostrodonStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Spodní	spodní	k2eAgFnSc1d1
jura	jura	k1gFnSc1
<g/>
,	,	kIx,
před	před	k7c7
201	#num#	k4
až	až	k8xS
200	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
Rekonstrukce	rekonstrukce	k1gFnSc2
vzezření	vzezření	k1gNnSc2
megazostrodona	megazostrodon	k1gMnSc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
Therapsida	Therapsida	k1gFnSc1
Řád	řád	k1gInSc1
</s>
<s>
Cynodontia	Cynodontia	k1gFnSc1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Megazostrodontidae	Megazostrodontidae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
MegazostrodonCrompton	MegazostrodonCrompton	k1gInSc1
&	&	k?
Jenkins	Jenkins	k1gInSc1
<g/>
,	,	kIx,
1968	#num#	k4
Typový	typový	k2eAgInSc1d1
druh	druh	k1gInSc1
</s>
<s>
Megazostrodon	Megazostrodon	k1gInSc1
rudneraeCrompton	rudneraeCrompton	k1gInSc1
&	&	k?
Jenkins	Jenkins	k1gInSc1
<g/>
,	,	kIx,
1968	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Megazostrodon	Megazostrodon	k1gNnSc1
(	(	kIx(
<g/>
„	„	kIx"
<g/>
Velký	velký	k2eAgInSc1d1
páskovaný	páskovaný	k2eAgInSc1d1
zub	zub	k1gInSc1
<g/>
“	“	kIx"
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
pravěkých	pravěký	k2eAgMnPc2d1
savcům	savec	k1gMnPc3
podobných	podobný	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
(	(	kIx(
<g/>
klad	klad	k1gInSc1
Mammaliaformes	Mammaliaformes	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žijících	žijící	k2eAgFnPc2d1
v	v	k7c6
období	období	k1gNnSc6
přelomu	přelom	k1gInSc2
triasu	trias	k1gInSc2
a	a	k8xC
jury	jura	k1gFnSc2
(	(	kIx(
<g/>
geologický	geologický	k2eAgInSc1d1
věk	věk	k1gInSc1
hettang	hettang	k1gInSc1
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
200	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
současné	současný	k2eAgFnSc2d1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Tento	tento	k3xDgMnSc1
malý	malý	k2eAgMnSc1d1
tvor	tvor	k1gMnSc1
o	o	k7c6
délce	délka	k1gFnSc6
10	#num#	k4
až	až	k9
12	#num#	k4
cm	cm	kA
bývá	bývat	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
možného	možný	k2eAgMnSc4d1
předka	předek	k1gMnSc4
či	či	k8xC
přinejmenším	přinejmenším	k6eAd1
blízkého	blízký	k2eAgMnSc2d1
příbuzného	příbuzný	k1gMnSc2
pravých	pravý	k2eAgMnPc2d1
savců	savec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zřejmě	zřejmě	k6eAd1
nočním	noční	k2eAgMnSc7d1
hmyzožravcem	hmyzožravec	k1gMnSc7
a	a	k8xC
měl	mít	k5eAaImAgInS
již	jenž	k3xRgFnSc4
srst	srst	k1gFnSc4
<g/>
,	,	kIx,
hmatové	hmatový	k2eAgInPc4d1
vousky	vousek	k1gInPc4
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
byl	být	k5eAaImAgInS
teplokrevný	teplokrevný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ještě	ještě	k9
nerodil	rodit	k5eNaImAgInS
živá	živý	k2eAgNnPc4d1
mláďata	mládě	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
kladl	klást	k5eAaImAgMnS
vejce	vejce	k1gNnSc4
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
dnešní	dnešní	k2eAgMnPc1d1
ptakořitní	ptakořitní	k2eAgMnPc1d1
savci	savec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
příbuzné	příbuzný	k2eAgInPc4d1
rody	rod	k1gInPc4
patřil	patřit	k5eAaImAgMnS
například	například	k6eAd1
Morganucodon	Morganucodon	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
z	z	k7c2
území	území	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fosilie	fosilie	k1gFnSc1
megazostrodona	megazostrodona	k1gFnSc1
byly	být	k5eAaImAgFnP
poprvé	poprvé	k6eAd1
objeveny	objevit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
Lesotha	Lesoth	k1gMnSc2
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
popsán	popsán	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Patřil	patřit	k5eAaImAgMnS
k	k	k7c3
drobným	drobný	k2eAgMnPc3d1
savcovitým	savcovitý	k2eAgMnPc3d1
tvorům	tvor	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c4
období	období	k1gNnSc4
druhohor	druhohory	k1gFnPc2
dosahovali	dosahovat	k5eAaImAgMnP
pouze	pouze	k6eAd1
velikosti	velikost	k1gFnSc2
jezevce	jezevec	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bi	Bi	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Three	Three	k1gFnSc1
new	new	k?
Jurassic	Jurassice	k1gFnPc2
euharamiyidan	euharamiyidany	k1gInPc2
species	species	k1gFnSc2
reinforce	reinforka	k1gFnSc6
early	earl	k1gMnPc4
divergence	divergence	k1gFnPc4
of	of	k?
mammals	mammals	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
,	,	kIx,
514	#num#	k4
<g/>
:	:	kIx,
579	#num#	k4
<g/>
-	-	kIx~
<g/>
584	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Crompton	Crompton	k1gInSc1
<g/>
;	;	kIx,
Jenkins	Jenkins	k1gInSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Molar	Molar	k1gInSc1
occlusion	occlusion	k1gInSc1
in	in	k?
late	lat	k1gInSc5
Triassic	Triassic	k1gMnSc1
mammals	mammals	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biological	Biological	k1gFnSc1
Reviews	Reviewsa	k1gFnPc2
<g/>
.	.	kIx.
43	#num#	k4
<g/>
:	:	kIx,
427	#num#	k4
<g/>
–	–	k?
<g/>
458	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1469	.1469	k4
<g/>
-	-	kIx~
<g/>
185	#num#	k4
<g/>
x	x	k?
<g/>
.1968	.1968	k4
<g/>
.	.	kIx.
<g/>
tb	tb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
966	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Didelphodon	Didelphodon	k1gMnSc1
<g/>
,	,	kIx,
savec	savec	k1gMnSc1
z	z	k7c2
doby	doba	k1gFnSc2
tyranosaurů	tyranosaur	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jason	Jason	k1gInSc4
A.	A.	kA
Lillegraven	Lillegraven	k2eAgMnSc1d1
<g/>
,	,	kIx,
Zofia	Zofia	k1gFnSc1
Kielan-Jaworowska	Kielan-Jaworowska	k1gFnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
A.	A.	kA
Clemens	Clemens	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mesozoic	Mesozoice	k1gInPc2
Mammals	Mammalsa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
First	First	k1gMnSc1
Two-Thirds	Two-Thirds	k1gInSc1
of	of	k?
Mammalian	Mammalian	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
California	Californium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
-	-	kIx~
321	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Close	Close	k1gFnSc1
<g/>
,	,	kIx,
Roger	Roger	k1gInSc1
A.	A.	kA
<g/>
;	;	kIx,
Friedman	Friedman	k1gMnSc1
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
<g/>
;	;	kIx,
Lloyd	Lloyd	k1gMnSc1
<g/>
,	,	kIx,
Graeme	Graem	k1gInSc5
T.	T.	kA
<g/>
;	;	kIx,
Benson	Benson	k1gMnSc1
<g/>
,	,	kIx,
Roger	Roger	k1gMnSc1
B.	B.	kA
J.	J.	kA
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Evidence	evidence	k1gFnPc1
for	forum	k1gNnPc2
a	a	k8xC
mid-Jurassic	mid-Jurassice	k1gFnPc2
adaptive	adaptiv	k1gInSc5
radiation	radiation	k1gInSc4
in	in	k?
mammals	mammals	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
25	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2137	#num#	k4
<g/>
–	–	k?
<g/>
2142	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cub	cub	k?
<g/>
.2015	.2015	k4
<g/>
.06	.06	k4
<g/>
.047	.047	k4
</s>
<s>
Mélina	Mélina	k1gMnSc1
A.	A.	kA
Celik	Celik	k1gMnSc1
and	and	k?
Matthew	Matthew	k1gMnSc1
J.	J.	kA
Phillips	Phillips	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conflict	Conflict	k2eAgInSc1d1
Resolution	Resolution	k1gInSc1
for	forum	k1gNnPc2
Mesozoic	Mesozoice	k1gFnPc2
Mammals	Mammalsa	k1gFnPc2
<g/>
:	:	kIx,
Reconciling	Reconciling	k1gInSc1
Phylogenetic	Phylogenetice	k1gFnPc2
Incongruence	Incongruence	k1gFnSc2
Among	Amonga	k1gFnPc2
Anatomical	Anatomical	k1gFnSc7
Regions	Regionsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frontiers	Frontiers	k1gInSc1
in	in	k?
Genetics	Genetics	k1gInSc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
:	:	kIx,
0	#num#	k4
<g/>
651	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.338	10.338	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
fgene	fgenout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.2020	.2020	k4
<g/>
.00651	.00651	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c6
webu	web	k1gInSc6
Prehistoric	Prehistorice	k1gFnPc2
Wildlife	Wildlif	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c4
databázi	databáze	k1gFnSc4
Fossilworks	Fossilworksa	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
