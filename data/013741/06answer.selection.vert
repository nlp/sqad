<s>
Megazostrodon	Megazostrodon	k1gNnSc1
(	(	kIx(
<g/>
„	„	kIx"
<g/>
Velký	velký	k2eAgInSc1d1
páskovaný	páskovaný	k2eAgInSc1d1
zub	zub	k1gInSc1
<g/>
“	“	kIx"
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
pravěkých	pravěký	k2eAgMnPc2d1
savcům	savec	k1gMnPc3
podobných	podobný	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
(	(	kIx(
<g/>
klad	klad	k1gInSc1
Mammaliaformes	Mammaliaformes	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žijících	žijící	k2eAgFnPc2d1
v	v	k7c6
období	období	k1gNnSc6
přelomu	přelom	k1gInSc2
triasu	trias	k1gInSc2
a	a	k8xC
jury	jura	k1gFnSc2
(	(	kIx(
<g/>
geologický	geologický	k2eAgInSc1d1
věk	věk	k1gInSc1
hettang	hettang	k1gInSc1
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
200	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
současné	současný	k2eAgFnSc2d1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>