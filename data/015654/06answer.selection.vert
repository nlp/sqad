<s>
Po	po	k7c6
vystudování	vystudování	k1gNnSc6
reálného	reálný	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
studoval	studovat	k5eAaImAgMnS
Bechyně	Bechyně	k1gMnSc1
stavební	stavební	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
technické	technický	k2eAgFnSc6d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgMnSc1d1
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
absolvoval	absolvovat	k5eAaPmAgMnS
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
vynikajícím	vynikající	k2eAgInSc7d1
prospěchem	prospěch	k1gInSc7
<g/>
.	.	kIx.
</s>