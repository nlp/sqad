<s>
Stanislav	Stanislav	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Stanislav	Stanislav	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
Narození	narození	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1887	#num#	k4
<g/>
PřibyslavRakousko-Uhersko	PřibyslavRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1973	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
stavitelstatikvysokoškolský	stavitelstatikvysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
Titul	titul	k1gInSc4
</s>
<s>
Ing.	ing.	kA
Dr	dr	kA
<g/>
.	.	kIx.
<g/>
h.c.	h.c.	k?
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
práce	práce	k1gFnSc2
Zlatá	zlatý	k2eAgFnSc1d1
Felberova	Felberův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Řád	řád	k1gInSc1
republiky	republika	k1gFnSc2
Zlatá	zlatý	k2eAgFnSc1d1
plaketa	plaketa	k1gFnSc1
ČSAV	ČSAV	kA
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
lidstvo	lidstvo	k1gNnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
Bechyněsynovec	Bechyněsynovec	k1gMnSc1
Jan	Jan	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1887	#num#	k4
Přibyslav	Přibyslav	k1gFnSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1973	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc7
stěžejními	stěžejní	k2eAgInPc7d1
díly	díl	k1gInPc7
jsou	být	k5eAaImIp3nP
železobetonové	železobetonový	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
kamenné	kamenný	k2eAgInPc1d1
a	a	k8xC
betonové	betonový	k2eAgInPc1d1
mosty	most	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
konstruktérů	konstruktér	k1gMnPc2
dokázal	dokázat	k5eAaPmAgMnS
ocenit	ocenit	k5eAaPmF
vlastnosti	vlastnost	k1gFnPc4
betonu	beton	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgFnPc4
pak	pak	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
dávkou	dávka	k1gFnSc7
invence	invence	k1gFnSc2
<g/>
,	,	kIx,
využíval	využívat	k5eAaPmAgMnS,k5eAaImAgMnS
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
projektech	projekt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
popudu	popud	k1gInSc2
vznikl	vzniknout	k5eAaPmAgInS
i	i	k9
nový	nový	k2eAgInSc1d1
obor	obor	k1gInSc1
<g/>
,	,	kIx,
stavitelství	stavitelství	k1gNnSc1
kamenných	kamenný	k2eAgInPc2d1
a	a	k8xC
betonových	betonový	k2eAgInPc2d1
mostů	most	k1gInPc2
<g/>
,	,	kIx,
jemuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
publikačně	publikačně	k6eAd1
i	i	k9
pedagogicky	pedagogicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Vysvědčení	vysvědčení	k1gNnSc1
o	o	k7c4
vykonání	vykonání	k1gNnSc4
2	#num#	k4
<g/>
.	.	kIx.
státní	státní	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1910	#num#	k4
</s>
<s>
Po	po	k7c6
vystudování	vystudování	k1gNnSc6
reálného	reálný	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
studoval	studovat	k5eAaImAgMnS
Bechyně	Bechyně	k1gMnSc1
stavební	stavební	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
technické	technický	k2eAgFnSc6d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgMnSc1d1
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
absolvoval	absolvovat	k5eAaPmAgMnS
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
vynikajícím	vynikající	k2eAgInSc7d1
prospěchem	prospěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
byl	být	k5eAaImAgInS
také	také	k9
jmenován	jmenován	k2eAgInSc1d1
roku	rok	k1gInSc3
1920	#num#	k4
řádným	řádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
statiky	statika	k1gFnSc2
a	a	k8xC
dynamiky	dynamika	k1gFnSc2
staveb	stavba	k1gFnPc2
železobetonu	železobeton	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
pedagog	pedagog	k1gMnSc1
působil	působit	k5eAaImAgMnS
na	na	k7c6
Stavební	stavební	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
ČVUT	ČVUT	kA
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
svou	svůj	k3xOyFgFnSc4
vědeckou	vědecký	k2eAgFnSc4d1
a	a	k8xC
odbornou	odborný	k2eAgFnSc4d1
práci	práce	k1gFnSc4
byl	být	k5eAaImAgInS
oceněn	ocenit	k5eAaPmNgInS
řadou	řada	k1gFnSc7
vyznamenání	vyznamenání	k1gNnSc1
(	(	kIx(
<g/>
Řád	řád	k1gInSc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
Felberova	Felberův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
,	,	kIx,
Řád	řád	k1gInSc1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
plaketa	plaketa	k1gFnSc1
ČSAV	ČSAV	kA
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
lidstvo	lidstvo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1953	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
akademikem	akademik	k1gMnSc7
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
státní	státní	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
Klementa	Klement	k1gMnSc2
Gottwalda	Gottwald	k1gMnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1972	#num#	k4
získal	získat	k5eAaPmAgInS
čestný	čestný	k2eAgInSc1d1
titul	titul	k1gInSc1
doktora	doktor	k1gMnSc2
technických	technický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
Dr	dr	kA
<g/>
.	.	kIx.
h.	h.	k?
c.	c.	k?
<g/>
)	)	kIx)
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
absolutoriu	absolutorium	k1gNnSc6
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
k	k	k7c3
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc3d1
stavební	stavební	k2eAgFnSc3d1
firmě	firma	k1gFnSc3
Dr	dr	kA
<g/>
.	.	kIx.
Ing.	ing.	kA
Karel	Karel	k1gMnSc1
Skorkovský	Skorkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgMnSc1d1
se	s	k7c7
stavbou	stavba	k1gFnSc7
betonových	betonový	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
správu	správa	k1gFnSc4
vyvinul	vyvinout	k5eAaPmAgMnS
Ing.	ing.	kA
Bechyně	Bechyně	k1gMnSc1
speciální	speciální	k2eAgFnSc4d1
recepturu	receptura	k1gFnSc4
portlandského	portlandský	k2eAgInSc2d1
cementu	cement	k1gInSc2
(	(	kIx(
<g/>
nazývanou	nazývaný	k2eAgFnSc4d1
A	a	k8xC
cement	cement	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
používanou	používaný	k2eAgFnSc4d1
při	při	k7c6
stavbě	stavba	k1gFnSc6
československého	československý	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
cementu	cement	k1gInSc2
měl	mít	k5eAaImAgInS
prodloužený	prodloužený	k2eAgInSc1d1
počátek	počátek	k1gInSc1
doby	doba	k1gFnSc2
tuhnutí	tuhnutí	k1gNnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
bylo	být	k5eAaImAgNnS
zajištěno	zajištěn	k2eAgNnSc1d1
dobré	dobrý	k2eAgNnSc1d1
propojení	propojení	k1gNnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
betonu	beton	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
omezilo	omezit	k5eAaPmAgNnS
množství	množství	k1gNnSc1
hydratačního	hydratační	k2eAgNnSc2d1
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
vytvářeného	vytvářený	k2eAgNnSc2d1
při	při	k7c6
tuhnutí	tuhnutí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
pochován	pochován	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
<g/>
,	,	kIx,
podle	podle	k7c2
svého	svůj	k3xOyFgNnSc2
přání	přání	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
rodné	rodný	k2eAgFnSc6d1
Přibyslavi	Přibyslav	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenováno	pojmenovat	k5eAaPmNgNnS
i	i	k9
náměstí	náměstí	k1gNnSc1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3
stavbou	stavba	k1gFnSc7
je	být	k5eAaImIp3nS
bezesporu	bezesporu	k9
palác	palác	k1gInSc4
Lucerna	lucerna	k1gFnSc1
a	a	k8xC
Veletržní	veletržní	k2eAgInSc1d1
palác	palác	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
i	i	k9
průmyslovým	průmyslový	k2eAgFnPc3d1
stavbám	stavba	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
odrážejí	odrážet	k5eAaImIp3nP
funkcionalismus	funkcionalismus	k1gInSc4
a	a	k8xC
precizní	precizní	k2eAgNnSc4d1
zvládnutí	zvládnutí	k1gNnSc4
použitých	použitý	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
projekt	projekt	k1gInSc1
mostu	most	k1gInSc2
přes	přes	k7c4
Nuselské	nuselský	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
<g/>
,	,	kIx,
se	s	k7c7
třemi	tři	k4xCgInPc7
oblouky	oblouk	k1gInPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
ovšem	ovšem	k9
nakonec	nakonec	k6eAd1
realizace	realizace	k1gFnSc2
nedočkal	dočkat	k5eNaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díla	dílo	k1gNnPc1
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
<s>
Most	most	k1gInSc1
v	v	k7c6
Hořepníku	hořepník	k1gInSc6
přes	přes	k7c4
Trnavu	Trnava	k1gFnSc4
</s>
<s>
cementárna	cementárna	k1gFnSc1
Králův	Králův	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
</s>
<s>
hangáry	hangár	k1gInPc1
Letňany	Letňan	k1gMnPc7
</s>
<s>
most	most	k1gInSc1
v	v	k7c6
Hořepníku	hořepník	k1gInSc6
-	-	kIx~
jeden	jeden	k4xCgInSc1
z	z	k7c2
prvních	první	k4xOgInPc2
železobetonových	železobetonový	k2eAgInPc2d1
mostů	most	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
(	(	kIx(
<g/>
obloukový	obloukový	k2eAgInSc1d1
most	most	k1gInSc1
se	s	k7c7
zavěšenou	zavěšený	k2eAgFnSc7d1
mostovkou	mostovka	k1gFnSc7
a	a	k8xC
táhlem	táhlo	k1gNnSc7
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
železárna	železárna	k1gFnSc1
Hrádek	hrádek	k1gInSc1
u	u	k7c2
Rokycan	Rokycany	k1gInPc2
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
–	–	k?
doktorská	doktorský	k2eAgFnSc1d1
disertační	disertační	k2eAgFnSc1d1
práce	práce	k1gFnSc1
</s>
<s>
automobilka	automobilka	k1gFnSc1
Praga	Praga	k1gFnSc1
Praha	Praha	k1gFnSc1
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
–	–	k?
unikátní	unikátní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
(	(	kIx(
<g/>
sloupy	sloup	k1gInPc7
z	z	k7c2
ovinuté	ovinutý	k2eAgFnSc2d1
litiny	litina	k1gFnSc2
a	a	k8xC
hřibové	hřibový	k2eAgInPc1d1
stropy	strop	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
nosná	nosný	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
paláce	palác	k1gInSc2
Lucerna	lucerna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
skladiště	skladiště	k1gNnSc4
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
–	–	k?
parabolická	parabolický	k2eAgFnSc1d1
skořepinová	skořepinový	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Stránovský	Stránovský	k2eAgInSc1d1
viadukt	viadukt	k1gInSc1
–	–	k?
železniční	železniční	k2eAgInSc1d1
most	most	k1gInSc1
u	u	k7c2
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ocelárna	ocelárna	k1gFnSc1
Kladno	Kladno	k1gNnSc1
</s>
<s>
most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Chrudimku	Chrudimek	k1gInSc2
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
dálniční	dálniční	k2eAgInSc1d1
most	most	k1gInSc1
Senohraby	Senohraba	k1gFnSc2
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Váh	Váh	k1gInSc4
v	v	k7c6
Komárně	Komárno	k1gNnSc6
–	–	k?
plochý	plochý	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
s	s	k7c7
rozpětím	rozpětí	k1gNnSc7
112,5	112,5	k4
m	m	kA
a	a	k8xC
vzepětím	vzepětí	k1gNnSc7
pouze	pouze	k6eAd1
8,5	8,5	k4
m	m	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
průřezu	průřez	k1gInSc2
ve	v	k7c6
vrcholu	vrchol	k1gInSc6
pouze	pouze	k6eAd1
1,12	1,12	k4
m	m	kA
</s>
<s>
přesun	přesun	k1gInSc1
děkanského	děkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Mostě	most	k1gInSc6
</s>
<s>
Díla	dílo	k1gNnPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Napomáhal	napomáhat	k5eAaBmAgMnS,k5eAaImAgMnS
ale	ale	k9
i	i	k9
s	s	k7c7
rekonstrukcemi	rekonstrukce	k1gFnPc7
stavebních	stavební	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
přesun	přesun	k1gInSc1
rotundy	rotunda	k1gFnSc2
Máří	Máří	k?
Magdaleny	Magdalena	k1gFnSc2
u	u	k7c2
Čechova	Čechův	k2eAgInSc2d1
mostu	most	k1gInSc2
</s>
<s>
úprava	úprava	k1gFnSc1
mostovky	mostovka	k1gFnSc2
na	na	k7c6
Karlově	Karlův	k2eAgInSc6d1
mostě	most	k1gInSc6
</s>
<s>
rekonstrukce	rekonstrukce	k1gFnSc1
Anežského	anežský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
</s>
<s>
podchycení	podchycení	k1gNnSc1
budovy	budova	k1gFnSc2
Národního	národní	k2eAgNnSc2d1
technického	technický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
při	při	k7c6
stavbě	stavba	k1gFnSc6
letenského	letenský	k2eAgInSc2d1
tunelu	tunel	k1gInSc2
</s>
<s>
Publikace	publikace	k1gFnSc1
</s>
<s>
Sepsal	sepsat	k5eAaPmAgMnS
také	také	k9
řadu	řada	k1gFnSc4
publikací	publikace	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
shrnul	shrnout	k5eAaPmAgMnS
svoje	svůj	k3xOyFgFnPc4
vědomosti	vědomost	k1gFnPc4
o	o	k7c6
mostech	most	k1gInPc6
a	a	k8xC
jiných	jiný	k2eAgFnPc6d1
betonových	betonový	k2eAgFnPc6d1
stavbách	stavba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Betonové	betonový	k2eAgNnSc1d1
stavitelství	stavitelství	k1gNnSc1
</s>
<s>
Stavitelství	stavitelství	k1gNnSc1
mostů	most	k1gInPc2
kamenných	kamenný	k2eAgInPc2d1
a	a	k8xC
betonových	betonový	k2eAgInPc2d1
</s>
<s>
Technický	technický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
</s>
<s>
Mosty	most	k1gInPc1
trámové	trámový	k2eAgFnSc2d1
a	a	k8xC
rámové	rámový	k2eAgFnSc2d1
</s>
<s>
Mosty	most	k1gInPc1
obloukové	obloukový	k2eAgInPc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
OCEŇOVÁNÍ	oceňování	k1gNnSc1
VE	v	k7c6
VĚDĚ	věda	k1gFnSc6
A	a	k9
VÝZKUMU	výzkum	k1gInSc3
<g/>
,	,	kIx,
Felberova	Felberův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
↑	↑	k?
Masarykův	Masarykův	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
,	,	kIx,
Plaketa	plaketa	k1gFnSc1
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
lidstvo	lidstvo	k1gNnSc4
<g/>
↑	↑	k?
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
KOVÁŘOVÁ	Kovářová	k1gFnSc1
<g/>
,	,	kIx,
Stanislava	Stanislava	k1gFnSc1
<g/>
;	;	kIx,
TRAPL	TRAPL	kA
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobnosti	osobnost	k1gFnSc2
českých	český	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
ALDA	ALDA	kA
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85600	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Bechyně	Bechyně	k1gFnSc2
Stanislav	Stanislav	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
ČERNÝ	Černý	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemožitelé	přemožitel	k1gMnPc1
času	čas	k1gInSc2
sv.	sv.	kA
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Stanislav	Stanislav	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
57	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Významné	významný	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
byl	být	k5eAaImAgInS
udělen	udělen	k2eAgInSc4d1
čestný	čestný	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
www.cvut.cz	www.cvut.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Budoucnost	budoucnost	k1gFnSc1
patří	patřit	k5eAaImIp3nS
betonu	beton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-03-14	2005-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MIKOLÁŠEK	Mikolášek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
tunelářská	tunelářský	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
ITA-AITES	ITA-AITES	k1gFnSc1
<g/>
,	,	kIx,
z.	z.	k?
s	s	k7c7
<g/>
,	,	kIx,
0	#num#	k4
<g/>
2.201	2.201	k4
<g/>
2	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
byl	být	k5eAaImAgMnS
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
dějinách	dějiny	k1gFnPc6
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
<g/>
,	,	kIx,
A	A	kA
<g/>
–	–	k?
<g/>
M	M	kA
/	/	kIx~
Milan	Milan	k1gMnSc1
Churaň	Churaň	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
..	..	k?
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
467	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
33	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
-	-	kIx~
Česko	Česko	k1gNnSc1
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
823	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
796	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
I.	I.	kA
A	A	kA
<g/>
–	–	k?
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
634	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
sešit	sešit	k1gInSc1
:	:	kIx,
Bas	bas	k1gInSc1
<g/>
–	–	k?
<g/>
Bend	Bend	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
264	#num#	k4
<g/>
–	–	k?
<g/>
375	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
287	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
342	#num#	k4
<g/>
–	–	k?
<g/>
343	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stanislav	Stanislava	k1gFnPc2
Bechyně	Bechyně	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Bechyně	Bechyně	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Městského	městský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Přibyslav	Přibyslava	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1011389	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5660	#num#	k4
0700	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83663658	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
