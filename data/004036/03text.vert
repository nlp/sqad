<s>
Radana	Radana	k1gFnSc1	Radana
Labajová	Labajový	k2eAgFnSc1d1	Labajová
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Studénka	studénka	k1gFnSc1	studénka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zpěvem	zpěv	k1gInSc7	zpěv
začínala	začínat	k5eAaImAgFnS	začínat
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
pěveckém	pěvecký	k2eAgInSc6d1	pěvecký
sboru	sbor	k1gInSc6	sbor
Studeňáček	Studeňáček	k1gMnSc1	Studeňáček
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
rodné	rodný	k2eAgFnSc6d1	rodná
Studénce	studénka	k1gFnSc6	studénka
<g/>
.	.	kIx.	.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
obor	obor	k1gInSc1	obor
pop-zpěv	poppěv	k1gInSc1	pop-zpěv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
do	do	k7c2	do
dívčí	dívčí	k2eAgFnSc2d1	dívčí
skupiny	skupina	k1gFnSc2	skupina
Holki	Holk	k1gFnSc2	Holk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Holki	Holki	k1gNnSc2	Holki
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
přibližně	přibližně	k6eAd1	přibližně
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
natočit	natočit	k5eAaBmF	natočit
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
Kaleidoskop	kaleidoskop	k1gInSc1	kaleidoskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sólové	sólový	k2eAgFnSc6d1	sólová
kariéře	kariéra	k1gFnSc6	kariéra
jí	on	k3xPp3gFnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
Petr	Petr	k1gMnSc1	Petr
Fider	Fider	k1gMnSc1	Fider
–	–	k?	–
autor	autor	k1gMnSc1	autor
všech	všecek	k3xTgFnPc2	všecek
desek	deska	k1gFnPc2	deska
Holek	holka	k1gFnPc2	holka
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
člen	člen	k1gMnSc1	člen
dua	duo	k1gNnSc2	duo
Verona	Verona	k1gFnSc1	Verona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
muzikále	muzikál	k1gInSc5	muzikál
Pomáda	pomáda	k1gFnSc1	pomáda
v	v	k7c6	v
roli	role	k1gFnSc6	role
RIZZO	RIZZO	kA	RIZZO
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Teodor	Teodor	k1gMnSc1	Teodor
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
.	.	kIx.	.
</s>
