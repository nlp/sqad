<s>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
(	(	kIx(
<g/>
BLM	BLM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
decentralizované	decentralizovaný	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
a	a	k8xC
sociální	sociální	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
protestuje	protestovat	k5eAaBmIp3nS
proti	proti	k7c3
policejní	policejní	k2eAgFnSc3d1
brutalitě	brutalita	k1gFnSc3
<g/>
,	,	kIx,
rasové	rasový	k2eAgFnSc3d1
nerovnosti	nerovnost	k1gFnSc3
<g/>
,	,	kIx,
nespravedlnosti	nespravedlnost	k1gFnSc3
a	a	k8xC
systematickému	systematický	k2eAgInSc3d1
rasismu	rasismus	k1gInSc3
vůči	vůči	k7c3
černochům	černoch	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
aktivní	aktivní	k2eAgMnSc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>