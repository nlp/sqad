<s>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
</s>
<s>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
BLM	BLM	kA
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Shaun	Shaun	k1gMnSc1
King	King	k1gMnSc1
<g/>
,	,	kIx,
DeRay	DeRaa	k1gFnPc1
Mckesson	Mckesson	k1gNnSc1
<g/>
,	,	kIx,
Johnetta	Johnetta	k1gFnSc1
Elzie	Elzie	k1gFnSc1
<g/>
,	,	kIx,
Tef	tef	k1gInSc1
Poe	Poe	k1gFnSc2
<g/>
,	,	kIx,
Erica	Erica	k1gMnSc1
Garner	Garner	k1gMnSc1
<g/>
,	,	kIx,
Alicia	Alicia	k1gFnSc1
Garza	Garza	k1gFnSc1
<g/>
,	,	kIx,
Patrisse	Patrisse	k1gFnSc1
Khan-Cullors	Khan-Cullors	k1gInSc1
a	a	k8xC
Opal	opal	k1gInSc1
Tometi	Tome	k1gNnSc6
Vznik	vznik	k1gInSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
sociální	sociální	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
Působnost	působnost	k1gFnSc1
</s>
<s>
převážně	převážně	k6eAd1
v	v	k7c6
USA	USA	kA
Zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
Alicia	Alicia	k1gFnSc1
GarzaPatrisse	GarzaPatrisse	k1gFnSc1
CullorsOpal	CullorsOpal	k1gFnSc7
Tometi	Tomet	k1gMnPc1
Klíčové	klíčový	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
</s>
<s>
Shaun	Shaun	k1gInSc1
KingDeRay	KingDeRaa	k1gMnSc2
MckessonJohnetta	MckessonJohnett	k1gMnSc2
Elzie	Elzie	k1gFnSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Sydney	Sydney	k1gNnSc4
Peace	Peace	k1gFnSc2
Prize	Prize	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
blacklivesmatter	blacklivesmatter	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
(	(	kIx(
<g/>
BLM	BLM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
decentralizované	decentralizovaný	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
a	a	k8xC
sociální	sociální	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
protestuje	protestovat	k5eAaBmIp3nS
proti	proti	k7c3
policejní	policejní	k2eAgFnSc3d1
brutalitě	brutalita	k1gFnSc3
<g/>
,	,	kIx,
rasové	rasový	k2eAgFnSc3d1
nerovnosti	nerovnost	k1gFnSc3
<g/>
,	,	kIx,
nespravedlnosti	nespravedlnost	k1gFnSc3
a	a	k8xC
systematickému	systematický	k2eAgInSc3d1
rasismu	rasismus	k1gInSc3
vůči	vůči	k7c3
černochům	černoch	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
aktivní	aktivní	k2eAgMnSc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Hnutí	hnutí	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
případem	případ	k1gInSc7
stát	stát	k5eAaPmF,k5eAaImF
versus	versus	k7c1
George	Georg	k1gMnSc2
Zimmerman	Zimmerman	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
šířit	šířit	k5eAaImF
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
pod	pod	k7c7
hashtagem	hashtag	k1gInSc7
#	#	kIx~
<g/>
BlackLivesMatter	BlackLivesMatter	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Floridský	floridský	k2eAgMnSc1d1
koordinátor	koordinátor	k1gMnSc1
místní	místní	k2eAgFnSc2d1
domobrany	domobrana	k1gFnSc2
George	George	k1gFnSc1
Zimmerman	Zimmerman	k1gMnSc1
tehdy	tehdy	k6eAd1
zastřelil	zastřelit	k5eAaPmAgMnS
17	#num#	k4
<g/>
letého	letý	k2eAgMnSc2d1
neozbrojeného	ozbrojený	k2eNgMnSc2d1
černocha	černoch	k1gMnSc2
Trayvona	Trayvon	k1gMnSc2
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimmerman	Zimmerman	k1gMnSc1
byl	být	k5eAaImAgMnS
vzat	vzít	k5eAaPmNgMnS
do	do	k7c2
vazby	vazba	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
po	po	k7c6
pěti	pět	k4xCc6
hodinách	hodina	k1gFnPc6
výslechu	výslech	k1gInSc2
propuštěn	propuštěn	k2eAgMnSc1d1
s	s	k7c7
odůvodněním	odůvodnění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
sebeobranu	sebeobrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
podnět	podnět	k1gInSc4
a	a	k8xC
zvýšení	zvýšení	k1gNnSc4
povědomí	povědomí	k1gNnSc2
o	o	k7c6
tomto	tento	k3xDgInSc6
hnutí	hnutí	k1gNnSc6
následovalo	následovat	k5eAaImAgNnS
po	po	k7c6
zastřelení	zastřelení	k1gNnSc6
Michaela	Michael	k1gMnSc2
Browna	Brown	k1gMnSc2
ve	v	k7c6
Fergusonu	Ferguson	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
tohoto	tento	k3xDgInSc2
případu	případ	k1gInSc2
se	se	k3xPyFc4
jedním	jeden	k4xCgInSc7
ze	z	k7c2
sloganů	slogan	k1gInPc2
hnutí	hnutí	k1gNnSc2
stalo	stát	k5eAaPmAgNnS
„	„	k?
<g/>
hands	hands	k6eAd1
up	up	k?
<g/>
,	,	kIx,
don	don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
shoot	shoot	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
ruce	ruka	k1gFnPc4
vzhůru	vzhůru	k6eAd1
<g/>
,	,	kIx,
nestřílejte	střílet	k5eNaImRp2nP
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
nebo	nebo	k8xC
jen	jen	k9
zkráceně	zkráceně	k6eAd1
„	„	k?
<g/>
hands	hands	k1gInSc1
up	up	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
signál	signál	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
měl	mít	k5eAaImAgMnS
Michael	Michael	k1gMnSc1
Brown	Brown	k1gMnSc1
ukazovat	ukazovat	k5eAaImF
před	před	k7c7
svým	svůj	k3xOyFgNnSc7
zastřelením	zastřelení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
června	červen	k1gInSc2
2016	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
hnutí	hnutí	k1gNnSc2
BLM	BLM	kA
uspořádalo	uspořádat	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
112	#num#	k4
celostátních	celostátní	k2eAgInPc2d1
protestů	protest	k1gInPc2
v	v	k7c6
88	#num#	k4
amerických	americký	k2eAgInPc6d1
městech	město	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgFnP
odpověďmi	odpověď	k1gFnPc7
na	na	k7c6
policejní	policejní	k2eAgFnSc6d1
zásahy	zásah	k1gInPc1
končící	končící	k2eAgInPc1d1
fatálně	fatálně	k6eAd1
pro	pro	k7c4
občany	občan	k1gMnPc4
tmavé	tmavý	k2eAgFnSc2d1
pleti	pleť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc1
upozorňuje	upozorňovat	k5eAaImIp3nS
také	také	k9
na	na	k7c4
rasové	rasový	k2eAgNnSc4d1
profilování	profilování	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ústí	ústit	k5eAaImIp3nS
v	v	k7c4
nepoměrně	poměrně	k6eNd1
vyšší	vysoký	k2eAgNnSc4d2
zastavování	zastavování	k1gNnSc4
Afro-	Afro-	k1gMnPc2
a	a	k8xC
Latinoameričanů	Latinoameričan	k1gMnPc2
policisty	policista	k1gMnSc2
v	v	k7c6
ulicích	ulice	k1gFnPc6
amerických	americký	k2eAgFnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
policejní	policejní	k2eAgFnSc4d1
brutalitu	brutalita	k1gFnSc4
a	a	k8xC
obecně	obecně	k6eAd1
dvojí	dvojí	k4xRgInSc4
metr	metr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
údajně	údajně	k6eAd1
policie	policie	k1gFnSc1
aplikuje	aplikovat	k5eAaBmIp3nS
vůči	vůči	k7c3
Američanům	Američan	k1gMnPc3
na	na	k7c6
základě	základ	k1gInSc6
jejich	jejich	k3xOp3gFnSc2
barvy	barva	k1gFnSc2
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
demonstrace	demonstrace	k1gFnSc1
Black	Black	k1gInSc1
Lives	Lives	k1gMnSc1
Matter	Mattra	k1gFnPc2
uskutečnily	uskutečnit	k5eAaPmAgInP
také	také	k9
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Kanadě	Kanada	k1gFnSc6
nebo	nebo	k8xC
Nizozemí	Nizozemí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
protesty	protest	k1gInPc1
a	a	k8xC
nepokoje	nepokoj	k1gInPc1
opět	opět	k6eAd1
vzedmuly	vzedmout	k5eAaPmAgInP
po	po	k7c6
smrti	smrt	k1gFnSc6
George	Georg	k1gMnSc2
Floyda	Floyd	k1gMnSc2
<g/>
,	,	kIx,
Breonny	Breonna	k1gMnSc2
Taylorové	Taylorová	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
a	a	k8xC
podpora	podpora	k1gFnSc1
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2015	#num#	k4
podpořil	podpořit	k5eAaPmAgMnS
demonstraci	demonstrace	k1gFnSc4
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
v	v	k7c6
New	New	k1gFnPc6
Yorku	York	k1gInSc2
americký	americký	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Quentin	Quentin	k1gMnSc1
Tarantino	Tarantin	k2eAgNnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Podporu	podpor	k1gInSc2
jim	on	k3xPp3gMnPc3
v	v	k7c6
minulosti	minulost	k1gFnSc6
vyjádřili	vyjádřit	k5eAaPmAgMnP
i	i	k9
Beyoncé	Beyoncá	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Snoop	Snoop	k1gInSc1
Dogg	Dogga	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Kanye	Kany	k1gInSc2
West	West	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Samuel	Samuel	k1gMnSc1
L.	L.	kA
Jackson	Jackson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
Stevie	Stevie	k1gFnSc1
Wonder	Wondra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Mattrum	k1gNnPc2
také	také	k9
vyjadřovalo	vyjadřovat	k5eAaImAgNnS
podporu	podpora	k1gFnSc4
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
demokratů	demokrat	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
republikáni	republikán	k1gMnPc1
se	se	k3xPyFc4
k	k	k7c3
hnutí	hnutí	k1gNnSc3
stavěli	stavět	k5eAaImAgMnP
spíše	spíše	k9
zdrženlivě	zdrženlivě	k6eAd1
nebo	nebo	k8xC
odmítavě	odmítavě	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
první	první	k4xOgFnSc2
republikánské	republikánský	k2eAgFnSc2d1
prezidentské	prezidentský	k2eAgFnSc2d1
debaty	debata	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
například	například	k6eAd1
Scott	Scott	k2eAgInSc4d1
Walker	Walker	k1gInSc4
hnutí	hnutí	k1gNnSc2
odsoudil	odsoudit	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vyvolává	vyvolávat	k5eAaImIp3nS
antipolicejní	antipolicejní	k2eAgFnPc4d1
nálady	nálada	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
zatímco	zatímco	k8xS
Marco	Marco	k1gMnSc1
Rubio	Rubio	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pohnutky	pohnutka	k1gFnPc4
hnutí	hnutí	k1gNnPc2
chápe	chápat	k5eAaImIp3nS
a	a	k8xC
sympatizuje	sympatizovat	k5eAaImIp3nS
s	s	k7c7
ním	on	k3xPp3gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Řada	řada	k1gFnSc1
republikánských	republikánský	k2eAgMnPc2d1
odpůrců	odpůrce	k1gMnPc2
začala	začít	k5eAaPmAgFnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
#	#	kIx~
<g/>
BlackLivesMatter	BlackLivesMatter	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c6
černošských	černošský	k2eAgInPc6d1
životech	život	k1gInPc6
záleží	záležet	k5eAaImIp3nS
<g/>
)	)	kIx)
užívat	užívat	k5eAaImF
hashtag	hashtag	k1gInSc4
#	#	kIx~
<g/>
AllLivesMatter	AllLivesMatter	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c6
všech	všecek	k3xTgInPc6
životech	život	k1gInPc6
záleží	záležet	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barack	Barack	k1gInSc1
Obama	Obama	k?
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
reagoval	reagovat	k5eAaBmAgMnS
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Myslím	myslet	k5eAaImIp1nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
důvod	důvod	k1gInSc1
<g/>
,	,	kIx,
proč	proč	k6eAd1
organizátoři	organizátor	k1gMnPc1
použili	použít	k5eAaPmAgMnP
frázi	fráze	k1gFnSc4
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
nebyl	být	k5eNaImAgMnS
z	z	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
ostatních	ostatní	k2eAgInPc6d1
životech	život	k1gInPc6
nezáleží	záležet	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tak	tak	k6eAd1
poukázali	poukázat	k5eAaPmAgMnP
na	na	k7c4
specifický	specifický	k2eAgInSc4d1
problém	problém	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
afroamerickým	afroamerický	k2eAgFnPc3d1
komunitám	komunita	k1gFnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
ostatním	ostatní	k2eAgFnPc3d1
komunitám	komunita	k1gFnPc3
v	v	k7c6
této	tento	k3xDgFnSc6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
věci	věc	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgNnSc4
hnutí	hnutí	k1gNnSc4
upozorňuje	upozorňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
legitimními	legitimní	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yIgNnPc6,k3yRgNnPc6,k3yQgNnPc6
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
začít	začít	k5eAaPmF
bavit	bavit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Americký	americký	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
David	David	k1gMnSc1
Theo	Thea	k1gFnSc5
Goldberg	Goldberg	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
provolání	provolání	k1gNnSc4
All	All	k1gFnSc2
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
ignoruje	ignorovat	k5eAaImIp3nS
specifické	specifický	k2eAgInPc4d1
rasové	rasový	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
se	se	k3xPyFc4
černoši	černoch	k1gMnPc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
bělochů	běloch	k1gMnPc2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
potýkat	potýkat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
užívání	užívání	k1gNnSc1
akcentuje	akcentovat	k5eAaImIp3nS
nepochopení	nepochopení	k1gNnSc2
témat	téma	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
hnutí	hnutí	k1gNnSc1
snaží	snažit	k5eAaImIp3nS
upozorňovat	upozorňovat	k5eAaImF
<g/>
,	,	kIx,
přestože	přestože	k8xS
primárním	primární	k2eAgInSc7d1
cílem	cíl	k1gInSc7
hnutí	hnutí	k1gNnSc2
je	být	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
právě	právě	k9
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
na	na	k7c6
všech	všecek	k3xTgInPc6
životech	život	k1gInPc6
záleželo	záležet	k5eAaImAgNnS
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ovšem	ovšem	k9
neodráží	odrážet	k5eNaImIp3nS
současný	současný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1
na	na	k7c4
Karl-Marx-Allee	Karl-Marx-Allee	k1gFnSc4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
v	v	k7c6
červnu	červen	k1gInSc6
2020	#num#	k4
po	po	k7c6
smrti	smrt	k1gFnSc6
George	Georg	k1gMnSc2
Floyda	Floyd	k1gMnSc2
</s>
<s>
Během	během	k7c2
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
v	v	k7c6
prezidentských	prezidentský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2016	#num#	k4
podpořila	podpořit	k5eAaPmAgFnS
Black	Black	k1gInSc4
Lives	Livesa	k1gFnPc2
Matter	Mattra	k1gFnPc2
kandidátka	kandidátek	k1gMnSc2
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Hillary	Hillara	k1gFnSc2
Clintonová	Clintonová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
aktivisty	aktivista	k1gMnPc7
z	z	k7c2
Black	Blacka	k1gFnPc2
Lives	Lives	k1gInSc1
Matter	Matter	k1gMnSc1
Clintonová	Clintonová	k1gFnSc1
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
rasismus	rasismus	k1gInSc1
je	být	k5eAaImIp3nS
prvotním	prvotní	k2eAgInSc7d1
hříchem	hřích	k1gInSc7
Ameriky	Amerika	k1gFnSc2
s	s	k7c7
odkazem	odkaz	k1gInSc7
na	na	k7c4
otroctví	otroctví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
24	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc2
přitom	přitom	k6eAd1
proti	proti	k7c3
Clintonové	Clintonová	k1gFnSc3
v	v	k7c6
minulosti	minulost	k1gFnSc6
vystupovalo	vystupovat	k5eAaImAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
podle	podle	k7c2
nich	on	k3xPp3gMnPc2
tématům	téma	k1gNnPc3
policejního	policejní	k2eAgNnSc2d1
násilí	násilí	k1gNnSc2
a	a	k8xC
rasismu	rasismus	k1gInSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kampani	kampaň	k1gFnSc6
dlouhodobě	dlouhodobě	k6eAd1
nevěnovala	věnovat	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Odmítavě	odmítavě	k6eAd1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
k	k	k7c3
hnutí	hnutí	k1gNnSc3
stavěl	stavět	k5eAaImAgMnS
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k9
během	během	k7c2
své	svůj	k3xOyFgFnSc2
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
či	či	k8xC
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlna	vlna	k1gFnSc1
kritiky	kritika	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c4
Black	Black	k1gInSc4
Lives	Lives	k1gInSc4
Matter	Mattra	k1gFnPc2
snesla	snést	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
jejím	její	k3xOp3gInSc6
poklidném	poklidný	k2eAgInSc6d1
protestu	protest	k1gInSc6
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
v	v	k7c6
Dallasu	Dallas	k1gInSc6
při	při	k7c6
útoku	útok	k1gInSc6
odstřelovače	odstřelovač	k1gMnSc2
zastřeleno	zastřelit	k5eAaPmNgNnS
pět	pět	k4xCc1
příslušníků	příslušník	k1gMnPc2
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
veterán	veterán	k1gMnSc1
z	z	k7c2
Afghánistánu	Afghánistán	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
dle	dle	k7c2
vlastních	vlastní	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
naštvaný	naštvaný	k2eAgMnSc1d1
jak	jak	k8xC,k8xS
na	na	k7c4
samotné	samotný	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k9
na	na	k7c4
příslušníky	příslušník	k1gMnPc4
policie	policie	k1gFnSc2
a	a	k8xC
bělochy	běloch	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelé	představitel	k1gMnPc1
Black	Blacka	k1gFnPc2
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
útok	útok	k1gInSc4
odsoudili	odsoudit	k5eAaPmAgMnP
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
hnutí	hnutí	k1gNnSc4
proti	proti	k7c3
násilí	násilí	k1gNnSc3
bojuje	bojovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ne	ne	k9
ho	on	k3xPp3gMnSc4
podporuje	podporovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Konkrétně	konkrétně	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
prohlášení	prohlášení	k1gNnSc6
na	na	k7c6
Twitteru	Twitter	k1gInSc6
uvedli	uvést	k5eAaPmAgMnP
<g/>
:	:	kIx,
„	„	k?
<g/>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
obhajuje	obhajovat	k5eAaImIp3nS
důstojnost	důstojnost	k1gFnSc4
<g/>
,	,	kIx,
spravedlnost	spravedlnost	k1gFnSc4
a	a	k8xC
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
vraždění	vražděný	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
útok	útok	k1gInSc4
v	v	k7c6
Dallasu	Dallas	k1gInSc6
se	se	k3xPyFc4
mezi	mezi	k7c7
odpůrci	odpůrce	k1gMnPc7
hnutí	hnutí	k1gNnSc2
začal	začít	k5eAaPmAgInS
šířit	šířit	k5eAaImF
hashtag	hashtag	k1gInSc1
#	#	kIx~
<g/>
BlueLivesMatter	BlueLivesMatter	k1gInSc1
(	(	kIx(
<g/>
blue	blue	k1gFnSc1
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
barva	barva	k1gFnSc1
policejních	policejní	k2eAgFnPc2d1
uniforem	uniforma	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Diskuse	diskuse	k1gFnSc1
hnutí	hnutí	k1gNnSc2
opět	opět	k6eAd1
rozproudilo	rozproudit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
reagujíc	reagovat	k5eAaImSgFnS
na	na	k7c4
novou	nový	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
protestů	protest	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ať	ať	k9
kontroverzí	kontroverze	k1gFnSc7
ohledně	ohledně	k7c2
provolání	provolání	k1gNnSc2
„	„	k?
<g/>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
případně	případně	k6eAd1
protestů	protest	k1gInPc2
jako	jako	k8xS,k8xC
takových	takový	k3xDgFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
místy	místy	k6eAd1
násilný	násilný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
kritizováno	kritizovat	k5eAaImNgNnS
i	i	k9
za	za	k7c4
nedostatek	nedostatek	k1gInSc4
intersekcionality	intersekcionalita	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
podle	podle	k7c2
některých	některý	k3yIgMnPc2
dostatečně	dostatečně	k6eAd1
nezaměřovalo	zaměřovat	k5eNaImAgNnS
i	i	k9
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
nebělošské	bělošský	k2eNgFnPc4d1
menšiny	menšina	k1gFnPc4
<g/>
.	.	kIx.
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
s	s	k7c7
rasismem	rasismus	k1gInSc7
mohou	moct	k5eAaImIp3nP
potýkat	potýkat	k5eAaImF
<g/>
,	,	kIx,
či	či	k8xC
intersekcinalitu	intersekcinalita	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
samotného	samotný	k2eAgNnSc2d1
černošského	černošský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Protestující	protestující	k2eAgFnPc1d1
v	v	k7c4
New	New	k1gFnPc4
York	York	k1gInSc4
City	city	k1gNnSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2014	#num#	k4
</s>
<s>
Protesty	protest	k1gInPc1
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
2014	#num#	k4
po	po	k7c6
zastřelení	zastřelení	k1gNnSc6
Erica	Ericus	k1gMnSc2
Garnera	Garner	k1gMnSc2
</s>
<s>
Protesty	protest	k1gInPc1
v	v	k7c6
Minneapolis	Minneapolis	k1gFnSc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2015	#num#	k4
po	po	k7c6
zastřelení	zastřelení	k1gNnSc6
Jamara	Jamar	k1gMnSc2
Clarka	Clarek	k1gMnSc2
</s>
<s>
Protestující	protestující	k2eAgFnPc1d1
v	v	k7c4
Saint	Saint	k1gInSc4
Paul	Paula	k1gFnPc2
v	v	k7c6
Minnesotě	Minnesota	k1gFnSc6
po	po	k7c6
zastřelení	zastřelení	k1gNnSc6
Philanda	Philando	k1gNnSc2
Castila	Castil	k1gMnSc2
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
</s>
<s>
Protesty	protest	k1gInPc1
proti	proti	k7c3
policejnímu	policejní	k2eAgNnSc3d1
násilí	násilí	k1gNnSc3
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Franciscus	k1gInSc2
v	v	k7c6
červenci	červenec	k1gInSc6
2016	#num#	k4
</s>
<s>
Nepokoje	nepokoj	k1gInPc1
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
,	,	kIx,
D.C	D.C	k1gFnSc1
v	v	k7c6
květnu	květen	k1gInSc6
2020	#num#	k4
po	po	k7c6
smrti	smrt	k1gFnSc6
George	Georg	k1gMnSc2
Floyda	Floyd	k1gMnSc2
</s>
<s>
Protesty	protest	k1gInPc1
po	po	k7c6
smrti	smrt	k1gFnSc6
George	Georg	k1gMnSc2
Floyda	Floyd	k1gMnSc2
v	v	k7c6
Bruselu	Brusel	k1gInSc6
v	v	k7c6
Belgii	Belgie	k1gFnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
LUIBRAND	LUIBRAND	kA
<g/>
,	,	kIx,
Shannon	Shannon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnPc2
a	a	k8xC
death	deatha	k1gFnPc2
in	in	k?
Ferguson	Ferguson	k1gMnSc1
sparked	sparked	k1gMnSc1
a	a	k8xC
movement	movement	k1gMnSc1
in	in	k?
America	America	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBC	CBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FRIEDERSDORF	FRIEDERSDORF	kA
<g/>
,	,	kIx,
Conor	Conor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
to	ten	k3xDgNnSc1
Distinguish	Distinguish	k1gInSc1
Between	Between	k1gInSc1
Antifa	Antif	k1gMnSc2
<g/>
,	,	kIx,
White	Whit	k1gInSc5
Supremacists	Supremacists	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Atlantic	Atlantice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-31	2017-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BANKS	BANKS	kA
<g/>
,	,	kIx,
Chloe	Chloe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disciplining	Disciplining	k1gInSc1
Black	Black	k1gMnSc1
activism	activism	k1gMnSc1
<g/>
:	:	kIx,
post-racial	post-racial	k1gInSc1
rhetoric	rhetorice	k1gFnPc2
<g/>
,	,	kIx,
public	publicum	k1gNnPc2
memory	memora	k1gFnSc2
and	and	k?
decorum	decorum	k1gInSc1
in	in	k?
news	news	k1gInSc1
media	medium	k1gNnSc2
framing	framing	k1gInSc1
of	of	k?
the	the	k?
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
movement	movement	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Continuum	Continuum	k1gNnSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
32	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
709	#num#	k4
<g/>
–	–	k?
<g/>
720	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1030	#num#	k4
<g/>
-	-	kIx~
<g/>
4312	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
10304312.2018	10304312.2018	k4
<g/>
.1525920	.1525920	k4
<g/>
.	.	kIx.
↑	↑	k?
Definition	Definition	k1gInSc1
of	of	k?
Black	Black	k1gInSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
|	|	kIx~
Dictionary	Dictionar	k1gInPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
www.dictionary.com	www.dictionary.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MCKANZIE	MCKANZIE	kA
<g/>
,	,	kIx,
Sheena	Sheeno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
protests	protests	k6eAd1
spread	spread	k6eAd1
to	ten	k3xDgNnSc1
Europe	Europ	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
#	#	kIx~
<g/>
BlackLivesMatter	BlackLivesMatter	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
birth	birth	k1gInSc1
of	of	k?
a	a	k8xC
new	new	k?
civil	civil	k1gMnSc1
rights	rights	k6eAd1
movement	movement	k1gInSc4
<g/>
.	.	kIx.
the	the	k?
Guardian	Guardian	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-19	2015-07-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAY	DAY	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
#	#	kIx~
<g/>
BlackLivesMatter	BlackLivesMatter	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
birth	birth	k1gInSc1
of	of	k?
a	a	k8xC
new	new	k?
civil	civil	k1gMnSc1
rights	rights	k6eAd1
movement	movement	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEE	Lea	k1gFnSc6
<g/>
,	,	kIx,
Jasmine	Jasmin	k1gInSc5
C.	C.	kA
<g/>
;	;	kIx,
MYKHYALYSHYN	MYKHYALYSHYN	kA
<g/>
,	,	kIx,
Iaryna	Iaryna	k1gFnSc1
<g/>
;	;	kIx,
OMRI	OMRI	kA
<g/>
,	,	kIx,
Rudy	Rudy	k1gMnSc1
<g/>
;	;	kIx,
SINGHVI	SINGHVI	kA
<g/>
,	,	kIx,
Anjali	Anjali	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
At	At	k1gFnSc1
Least	Least	k1gFnSc1
88	#num#	k4
Cities	Cities	k1gMnSc1
Have	Hav	k1gInSc2
Had	had	k1gMnSc1
Protests	Protestsa	k1gFnPc2
in	in	k?
the	the	k?
Past	pasta	k1gFnPc2
13	#num#	k4
Days	Daysa	k1gFnPc2
Over	Overa	k1gFnPc2
Police	police	k1gFnSc2
Killings	Killings	k1gInSc1
of	of	k?
Blacks	Blacks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
|	|	kIx~
C-SPAN	C-SPAN	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
www.c-span.org	www.c-span.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivisté	aktivista	k1gMnPc1
proti	proti	k7c3
rasismu	rasismus	k1gInSc3
způsobili	způsobit	k5eAaPmAgMnP
v	v	k7c6
Británii	Británie	k1gFnSc6
dopravní	dopravní	k2eAgInSc1d1
kolaps	kolaps	k1gInSc1
<g/>
,	,	kIx,
policie	policie	k1gFnSc1
jich	on	k3xPp3gMnPc2
19	#num#	k4
zatkla	zatknout	k5eAaPmAgFnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HASSAN	HASSAN	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gMnSc1
the	the	k?
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
movement	movement	k1gMnSc1
is	is	k?
sweeping	sweeping	k1gInSc1
the	the	k?
globe	globus	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnPc2
Washington	Washington	k1gInSc4
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KLUDT	KLUDT	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quentin	Quentina	k1gFnPc2
Tarantino	Tarantin	k2eAgNnSc4d1
won	won	k1gInSc4
<g/>
'	'	kIx"
<g/>
t	t	k?
be	be	k?
'	'	kIx"
<g/>
intimidated	intimidated	k1gMnSc1
<g/>
'	'	kIx"
by	by	kYmCp3nS
police	police	k1gFnSc1
boycott	boycotta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PETERSON	PETERSON	kA
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beyoncé	Beyoncá	k1gFnSc2
is	is	k?
a	a	k8xC
powerful	powerful	k1gInSc1
voice	voice	k1gFnSc2
for	forum	k1gNnPc2
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Some	Some	k1gNnSc1
people	people	k6eAd1
hate	hate	k1gInSc1
her	hra	k1gFnPc2
for	forum	k1gNnPc2
it.	it.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnPc2
Washington	Washington	k1gInSc4
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LANDRUM	LANDRUM	kA
JR	JR	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snoop	Snoop	k1gInSc1
Dogg	Dogg	k1gMnSc1
honoured	honoured	k1gMnSc1
at	at	k?
politically	politicalla	k1gFnSc2
charged	charged	k1gMnSc1
BET	BET	kA
Hip-Hop	Hip-Hop	k1gInSc1
Awards	Awards	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBC	CBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Kanye	Kany	k1gMnSc2
West	West	k1gMnSc1
Has	hasit	k5eAaImRp2nS
Joined	Joined	k1gMnSc1
The	The	k1gMnSc1
#	#	kIx~
<g/>
BlackLivesMatter	BlackLivesMatter	k1gMnSc1
Movement	Movement	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
MTV	MTV	kA
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Samuel	Samuel	k1gMnSc1
L.	L.	kA
Jackson	Jackson	k1gMnSc1
dares	dares	k1gMnSc1
stars	stars	k6eAd1
to	ten	k3xDgNnSc4
call	calnout	k5eAaPmAgMnS
out	out	k?
police	police	k1gFnPc4
racism	racism	k6eAd1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
MSNBC	MSNBC	kA
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stevie	Stevie	k1gFnSc2
Wonder	Wondra	k1gFnPc2
tells	tellsa	k1gFnPc2
audience	audience	k1gFnSc2
at	at	k?
London	London	k1gMnSc1
concert	concert	k1gMnSc1
'	'	kIx"
<g/>
black	black	k1gMnSc1
lives	lives	k1gMnSc1
matter	matter	k1gMnSc1
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CUMMINGS	CUMMINGS	kA
<g/>
,	,	kIx,
Moriba	Moriba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stevie	Stevie	k1gFnSc1
Wonder	Wondra	k1gFnPc2
Has	hasit	k5eAaImRp2nS
a	a	k8xC
Message	Message	k1gFnSc3
for	forum	k1gNnPc2
Fans	Fans	k1gInSc1
Who	Who	k1gMnSc1
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Approve	Approv	k1gInSc5
of	of	k?
His	his	k1gNnPc2
Support	support	k1gInSc1
of	of	k?
#	#	kIx~
<g/>
BlackLivesMatter	BlackLivesMatter	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BET	BET	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOPEZ	LOPEZ	kA
<g/>
,	,	kIx,
German	German	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scott	Scott	k1gMnSc1
Walker	Walker	k1gMnSc1
suggested	suggested	k1gMnSc1
it	it	k?
<g/>
’	’	k?
<g/>
s	s	k7c7
more	mor	k1gInSc5
dangerous	dangerous	k1gInSc1
to	ten	k3xDgNnSc4
be	be	k?
a	a	k8xC
cop	cop	k1gInSc1
today	todaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
It	It	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
actually	actuall	k1gMnPc7
much	moucha	k1gFnPc2
safer	safer	k1gInSc1
<g/>
..	..	k?
Vox	Vox	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-03	2015-09-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOPEZ	LOPEZ	kA
<g/>
,	,	kIx,
German	German	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marco	Marco	k1gNnSc1
Rubio	Rubio	k6eAd1
shows	shows	k6eAd1
other	other	k1gMnSc1
Republicans	Republicansa	k1gFnPc2
how	how	k?
to	ten	k3xDgNnSc1
respond	responda	k1gFnPc2
to	ten	k3xDgNnSc4
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vox	Vox	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-30	2015-09-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
LOPEZ	LOPEZ	kA
<g/>
,	,	kIx,
German	German	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Watch	Watch	k1gMnSc1
<g/>
:	:	kIx,
President	president	k1gMnSc1
Obama	Obama	k?
explains	explains	k1gInSc1
what	what	k1gInSc1
"	"	kIx"
<g/>
all	all	k?
lives	lives	k1gMnSc1
matter	matter	k1gMnSc1
<g/>
"	"	kIx"
gets	gets	k1gInSc1
wrong	wronga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vox	Vox	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-22	2015-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GOLDBERG	GOLDBERG	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
Theo	Thea	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gMnSc1
"	"	kIx"
<g/>
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
"	"	kIx"
Because	Because	k1gFnSc1
All	All	k1gMnSc1
Lives	Lives	k1gMnSc1
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Matter	Matter	k1gMnSc1
in	in	k?
America	America	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HuffPost	HuffPost	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-25	2015-09-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARGOLIN	MARGOLIN	kA
<g/>
,	,	kIx,
Emma	Emma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hillary	Hillar	k1gInPc1
Clinton	Clinton	k1gMnSc1
<g/>
:	:	kIx,
‘	‘	k?
<g/>
Yes	Yes	k1gMnSc1
<g/>
,	,	kIx,
black	black	k1gMnSc1
lives	lives	k1gMnSc1
matter	matter	k1gMnSc1
<g/>
’	’	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MSNBC	MSNBC	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SEITZ-WALD	SEITZ-WALD	k1gMnSc1
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
;	;	kIx,
ALBA	alba	k1gFnSc1
<g/>
,	,	kIx,
Monica	Monica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hillary	Hillar	k1gInPc1
Clinton	Clinton	k1gMnSc1
<g/>
:	:	kIx,
‘	‘	k?
<g/>
Racism	Racism	k1gInSc1
is	is	k?
America	America	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
original	originat	k5eAaPmAgMnS,k5eAaImAgMnS
sin	sin	kA
<g/>
’	’	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MSNBC	MSNBC	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCOTT	SCOTT	kA
<g/>
,	,	kIx,
Eugene	Eugen	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
protesters	protestersa	k1gFnPc2
confront	confront	k1gMnSc1
Clinton	Clinton	k1gMnSc1
at	at	k?
a	a	k8xC
fundraiser	fundraiser	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MADHANI	MADHANI	kA
<g/>
,	,	kIx,
Aamer	Aamer	k1gMnSc1
<g/>
;	;	kIx,
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Kevin	Kevin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
protesters	protesters	k6eAd1
want	want	k5eAaPmF
to	ten	k3xDgNnSc4
send	send	k6eAd1
message	messagat	k5eAaPmIp3nS
to	ten	k3xDgNnSc4
Clinton	Clinton	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NIEDZWIADEK	NIEDZWIADEK	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gMnSc1
goes	goesa	k1gFnPc2
after	after	k1gMnSc1
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
<g/>
,	,	kIx,
‘	‘	k?
<g/>
toxic	toxice	k1gFnPc2
propaganda	propaganda	k1gFnSc1
<g/>
’	’	k?
in	in	k?
schools	schools	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
POLITICO	POLITICO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CNN	CNN	kA
<g/>
,	,	kIx,
Kevin	Kevin	k2eAgInSc1d1
Liptak	Liptak	k1gInSc1
and	and	k?
Kristen	Kristen	k2eAgInSc1d1
Holmes	Holmes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gMnSc1
calls	callsa	k1gFnPc2
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
a	a	k8xC
'	'	kIx"
<g/>
symbol	symbol	k1gInSc1
of	of	k?
hate	hate	k1gInSc1
<g/>
'	'	kIx"
as	as	k9
he	he	k0
digs	digs	k6eAd1
in	in	k?
on	on	k3xPp3gMnSc1
race	racat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
REUTERS	REUTERS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dallas	Dallas	k1gInSc1
Shootings	Shootings	k1gInSc4
<g/>
:	:	kIx,
Sniper	Sniper	k1gInSc1
Fire	Fire	k1gNnSc1
Kills	Killsa	k1gFnPc2
Five	Fiv	k1gInSc2
Police	police	k1gFnSc2
Officers	Officersa	k1gFnPc2
at	at	k?
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
Protest	protest	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Newsweek	Newsweky	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
odsoudilo	odsoudit	k5eAaPmAgNnS
útok	útok	k1gInSc4
v	v	k7c6
Dallasu	Dallas	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BUNYASI	BUNYASI	kA
<g/>
,	,	kIx,
Tehama	Tehama	k1gFnSc1
Lopez	Lopez	k1gMnSc1
<g/>
;	;	kIx,
SMITH	SMITH	kA
<g/>
,	,	kIx,
Candis	Candis	k1gInSc4
Watts	Wattsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
All	All	k1gFnSc2
Black	Black	k1gMnSc1
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
Equally	Equalla	k1gFnSc2
to	ten	k3xDgNnSc4
Black	Black	k1gMnSc1
People	People	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Respectability	Respectabilita	k1gFnSc2
Politics	Politicsa	k1gFnPc2
and	and	k?
the	the	k?
Limitations	Limitations	k1gInSc1
of	of	k?
Linked	Linked	k1gInSc1
Fate	Fate	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Race	Rac	k1gFnSc2
<g/>
,	,	kIx,
Ethnicity	Ethnicita	k1gFnSc2
and	and	k?
Politics	Politics	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
180	#num#	k4
<g/>
–	–	k?
<g/>
215	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2056	#num#	k4
<g/>
-	-	kIx~
<g/>
6085	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
rep	rep	k?
<g/>
.2018	.2018	k4
<g/>
.33	.33	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ferguson	Ferguson	k1gNnSc1
(	(	kIx(
<g/>
Missouri	Missouri	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Zastřelení	zastřelení	k1gNnSc1
Trayvona	Trayvon	k1gMnSc2
Martina	Martin	k1gMnSc2
</s>
<s>
Zastřelení	zastřelení	k1gNnSc1
Tamira	Tamir	k1gInSc2
Rice	Ric	k1gFnSc2
</s>
<s>
Smrt	smrt	k1gFnSc1
George	Georg	k1gMnSc2
Floyda	Floyd	k1gMnSc2
</s>
<s>
Smrt	smrt	k1gFnSc1
Raysharda	Raysharda	k1gFnSc1
Brookse	Brookse	k1gFnSc1
</s>
<s>
Thin	Thin	k1gInSc1
blue	blue	k6eAd1
line	linout	k5eAaImIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Black	Black	k1gInSc4
Lives	Lives	k1gInSc4
Matter	Mattrum	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1153800764	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2016001442	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
28152079058807111043	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2016001442	#num#	k4
</s>
