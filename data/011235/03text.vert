<p>
<s>
Město	město	k1gNnSc1	město
Měčín	Měčína	k1gFnPc2	Měčína
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Metschin	Metschin	k2eAgInSc1d1	Metschin
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
usadil	usadit	k5eAaPmAgInS	usadit
rod	rod	k1gInSc4	rod
Drslaviců	Drslavice	k1gMnPc2	Drslavice
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
koupil	koupit	k5eAaPmAgMnS	koupit
(	(	kIx(	(
<g/>
neznámo	neznámo	k6eAd1	neznámo
od	od	k7c2	od
koho	kdo	k3yRnSc2	kdo
<g/>
)	)	kIx)	)
statek	statek	k1gInSc1	statek
Měčín	Měčína	k1gFnPc2	Měčína
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Měčín	Měčín	k1gMnSc1	Měčín
získal	získat	k5eAaPmAgMnS	získat
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
titulu	titul	k1gInSc6	titul
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1413	[number]	k4	1413
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Měčín	Měčín	k1gMnSc1	Měčín
dostal	dostat	k5eAaPmAgMnS	dostat
(	(	kIx(	(
<g/>
podrobnosti	podrobnost	k1gFnPc4	podrobnost
opět	opět	k6eAd1	opět
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
)	)	kIx)	)
k	k	k7c3	k
panství	panství	k1gNnSc3	panství
hradu	hrad	k1gInSc2	hrad
Švihova	Švihův	k2eAgInSc2d1	Švihův
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
držitelem	držitel	k1gMnSc7	držitel
byl	být	k5eAaImAgMnS	být
Půta	Půta	k1gMnSc1	Půta
Švihovský	Švihovský	k2eAgMnSc1d1	Švihovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
majitelka	majitelka	k1gFnSc1	majitelka
Měčína	Měčína	k1gFnSc1	Měčína
hraběnka	hraběnka	k1gFnSc1	hraběnka
Alžběta	Alžběta	k1gFnSc1	Alžběta
z	z	k7c2	z
Klenové	klenový	k2eAgFnSc2d1	Klenová
a	a	k8xC	a
Janovic	Janovice	k1gFnPc2	Janovice
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
z	z	k7c2	z
Walmerode	Walmerod	k1gInSc5	Walmerod
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
náležel	náležet	k5eAaImAgInS	náležet
Měčín	Měčín	k1gInSc1	Měčín
k	k	k7c3	k
žinkovskému	žinkovský	k2eAgNnSc3d1	žinkovský
panství	panství	k1gNnSc3	panství
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
okresu	okres	k1gInSc2	okres
Přeštice	Přeštice	k1gFnPc1	Přeštice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
reformy	reforma	k1gFnSc2	reforma
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
odprodány	odprodat	k5eAaPmNgInP	odprodat
poškozenému	poškozený	k2eAgMnSc3d1	poškozený
bývalému	bývalý	k2eAgMnSc3d1	bývalý
řediteli	ředitel	k1gMnSc3	ředitel
velkostatku	velkostatek	k1gInSc2	velkostatek
Bohumilu	Bohumil	k1gMnSc3	Bohumil
Kubecovi	Kubeec	k1gMnSc3	Kubeec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Marii	Maria	k1gFnSc3	Maria
statky	statek	k1gInPc1	statek
Měčín	Měčín	k1gInSc1	Měčín
(	(	kIx(	(
<g/>
73,50	[number]	k4	73,50
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malinec	Malinec	k1gInSc1	Malinec
a	a	k8xC	a
Mečkov	Mečkov	k1gInSc1	Mečkov
(	(	kIx(	(
<g/>
80	[number]	k4	80
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
až	až	k9	až
do	do	k7c2	do
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
reformy	reforma	k1gFnSc2	reforma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Měčín	Měčín	k1gMnSc1	Měčín
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
okresu	okres	k1gInSc3	okres
Klatovy	Klatovy	k1gInPc4	Klatovy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
Měčínu	Měčína	k1gFnSc4	Měčína
zrušen	zrušen	k2eAgInSc4d1	zrušen
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
obnoven	obnovit	k5eAaPmNgInS	obnovit
byl	být	k5eAaImAgInS	být
zákonem	zákon	k1gInSc7	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
španělském	španělský	k2eAgInSc6d1	španělský
štítu	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
berla	berla	k1gFnSc1	berla
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
v	v	k7c6	v
heraldicky	heraldicky	k6eAd1	heraldicky
pravé	pravý	k2eAgFnSc6d1	pravá
polovině	polovina	k1gFnSc6	polovina
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
zasvěcení	zasvěcení	k1gNnSc1	zasvěcení
zdejšího	zdejší	k2eAgInSc2d1	zdejší
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Mikuláši	Mikuláš	k1gMnPc1	Mikuláš
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgInPc1d1	bílý
pruhy	pruh	k1gInPc1	pruh
v	v	k7c6	v
heraldicky	heraldicky	k6eAd1	heraldicky
levé	levý	k2eAgFnSc6d1	levá
polovině	polovina	k1gFnSc6	polovina
připomínají	připomínat	k5eAaImIp3nP	připomínat
první	první	k4xOgMnPc1	první
majitele	majitel	k1gMnSc4	majitel
Měčína	Měčín	k1gInSc2	Měčín
–	–	k?	–
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
rod	rod	k1gInSc1	rod
Rýzmberků	Rýzmberka	k1gMnPc2	Rýzmberka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Měčín	Měčína	k1gFnPc2	Měčína
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
fara	fara	k1gFnSc1	fara
s	s	k7c7	s
mansardovou	mansardový	k2eAgFnSc7d1	mansardová
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
středovým	středový	k2eAgInSc7d1	středový
rizalitem	rizalit	k1gInSc7	rizalit
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
</s>
</p>
<p>
<s>
Secesní	secesní	k2eAgFnSc1d1	secesní
budova	budova	k1gFnSc1	budova
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
městským	městský	k2eAgInSc7d1	městský
úřadem	úřad	k1gInSc7	úřad
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Měčín	Měčín	k1gMnSc1	Měčín
</s>
</p>
<p>
<s>
Bíluky	Bíluk	k1gInPc1	Bíluk
</s>
</p>
<p>
<s>
Hráz	hráz	k1gFnSc1	hráz
</s>
</p>
<p>
<s>
Nedanice	Nedanice	k1gFnSc1	Nedanice
</s>
</p>
<p>
<s>
Nedaničky	Nedanička	k1gFnPc1	Nedanička
</s>
</p>
<p>
<s>
Osobovy	Osobovy	k?	Osobovy
</s>
</p>
<p>
<s>
Petrovice	Petrovice	k1gFnSc1	Petrovice
</s>
</p>
<p>
<s>
Radkovice	Radkovice	k1gFnSc1	Radkovice
</s>
</p>
<p>
<s>
Třebýcina	Třebýcina	k1gFnSc1	Třebýcina
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Měčín	Měčína	k1gFnPc2	Měčína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Měčín	Měčína	k1gFnPc2	Měčína
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Měčín	Měčín	k1gInSc1	Měčín
na	na	k7c4	na
mesta	mest	k1gMnSc4	mest
<g/>
.	.	kIx.	.
<g/>
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
