<s>
Šuměnka	šuměnka	k1gFnSc1
</s>
<s>
Šuměnka	šuměnka	k1gFnSc1
či	či	k8xC
hovorově	hovorově	k6eAd1
šumák	šumák	k1gInSc1
byl	být	k5eAaImAgInS
práškový	práškový	k2eAgMnSc1d1
(	(	kIx(
<g/>
instantní	instantní	k2eAgMnSc1d1
<g/>
)	)	kIx)
šumivý	šumivý	k2eAgInSc1d1
nápoj	nápoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Základem	základ	k1gInSc7
šumáku	šumák	k1gInSc2
je	být	k5eAaImIp3nS
směs	směs	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
citronové	citronový	k2eAgFnSc2d1
a	a	k8xC
hydrogenuhličitanu	hydrogenuhličitan	k1gInSc2
sodného	sodný	k2eAgInSc2d1
<g/>
,	,	kIx,
známého	známý	k2eAgMnSc2d1
spíše	spíše	k9
jako	jako	k8xC,k8xS
jedlá	jedlý	k2eAgFnSc1d1
soda	soda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
směs	směs	k1gFnSc1
při	při	k7c6
rozpuštění	rozpuštění	k1gNnSc6
ve	v	k7c6
vodě	voda	k1gFnSc6
chemickou	chemický	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
uvolňuje	uvolňovat	k5eAaImIp3nS
oxid	oxid	k1gInSc1
uhličitý	uhličitý	k2eAgInSc1d1
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
je	být	k5eAaImIp3nS
nápoj	nápoj	k1gInSc1
perlivý	perlivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
byl	být	k5eAaImAgInS
přidán	přidat	k5eAaPmNgInS
cukr	cukr	k1gInSc1
a	a	k8xC
trocha	trocha	k1gFnSc1
potravinářského	potravinářský	k2eAgNnSc2d1
barviva	barvivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
zabaleno	zabalen	k2eAgNnSc1d1
v	v	k7c6
sáčku	sáček	k1gInSc6
z	z	k7c2
voskového	voskový	k2eAgInSc2d1
papíru	papír	k1gInSc2
–	–	k?
pokud	pokud	k8xS
hygroskopická	hygroskopický	k2eAgFnSc1d1
směs	směs	k1gFnSc1
zvlhla	zvlhnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
pak	pak	k6eAd1
již	již	k6eAd1
nešuměla	šumět	k5eNaImAgFnS
a	a	k8xC
výslednou	výsledný	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
bylo	být	k5eAaImAgNnS
obtížné	obtížný	k2eAgNnSc1d1
také	také	k9
rozpustit	rozpustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgInP
trvanlivější	trvanlivý	k2eAgInPc1d2
sáčky	sáček	k1gInPc1
z	z	k7c2
plastické	plastický	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dříve	dříve	k6eAd2
sáček	sáček	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
asi	asi	k9
dvě	dva	k4xCgFnPc4
čajové	čajový	k2eAgFnPc4d1
lžičky	lžička	k1gFnPc4
prášku	prášek	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc7
rozpuštěním	rozpuštění	k1gNnSc7
v	v	k7c6
chladné	chladný	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
vzniklo	vzniknout	k5eAaPmAgNnS
cca	cca	kA
půl	půl	k1xP
litru	litr	k1gInSc6
mírně	mírně	k6eAd1
nakyslého	nakyslý	k2eAgInSc2d1
perlivého	perlivý	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dostání	dostání	k1gNnSc3
byly	být	k5eAaImAgInP
dříve	dříve	k6eAd2
jen	jen	k9
šuměnky	šuměnka	k1gFnPc1
žluté	žlutý	k2eAgFnPc1d1
a	a	k8xC
červené	červený	k2eAgFnPc1d1
(	(	kIx(
<g/>
citron	citron	k1gInSc1
a	a	k8xC
pomeranč	pomeranč	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
lišily	lišit	k5eAaImAgFnP
jen	jen	k9
přidaným	přidaný	k2eAgNnSc7d1
barvivem	barvivo	k1gNnSc7
a	a	k8xC
potiskem	potisk	k1gInSc7
na	na	k7c6
obalu	obal	k1gInSc6
sáčku	sáček	k1gInSc2
a	a	k8xC
dále	daleko	k6eAd2
přechodně	přechodně	k6eAd1
existoval	existovat	k5eAaImAgInS
šumák	šumák	k1gInSc1
v	v	k7c6
lisované	lisovaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
–	–	k?
dvě	dva	k4xCgFnPc4
kostičky	kostička	k1gFnPc4
ve	v	k7c6
společném	společný	k2eAgInSc6d1
obalu	obal	k1gInSc6
s	s	k7c7
názvem	název	k1gInSc7
Tiki	Tik	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složení	složení	k1gNnSc1
bylo	být	k5eAaImAgNnS
stejné	stejný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Herbex	Herbex	k1gInSc1
Šuměnka	šuměnka	k1gFnSc1
10	#num#	k4
<g/>
g	g	kA
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
k	k	k7c3
dostání	dostání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příchuti	příchuť	k1gFnSc3
citron	citron	k1gInSc1
<g/>
,	,	kIx,
mandarinka	mandarinka	k1gFnSc1
a	a	k8xC
malina	malina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obdoba	obdoba	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Pima	Pima	k1gFnSc1
<g/>
,	,	kIx,
název	název	k1gInSc1
šuměnka	šuměnka	k1gFnSc1
neužívá	užívat	k5eNaImIp3nS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
chuť	chuť	k1gFnSc4
mírně	mírně	k6eAd1
vylepšenou	vylepšený	k2eAgFnSc4d1
a	a	k8xC
existuje	existovat	k5eAaImIp3nS
i	i	k9
ve	v	k7c6
verzi	verze	k1gFnSc6
pro	pro	k7c4
diabetiky	diabetik	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobek	výrobek	k1gInSc1
společnosti	společnost	k1gFnSc2
Dr	dr	kA
<g/>
.	.	kIx.
Oetker	Oetker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.oetker.cz	www.oetker.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4799873-8	4799873-8	k4
</s>
