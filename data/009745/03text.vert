<p>
<s>
Hektar	hektar	k1gInSc1	hektar
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ha	ha	kA	ha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
plošný	plošný	k2eAgInSc4d1	plošný
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
plochy	plocha	k1gFnSc2	plocha
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
stavebních	stavební	k2eAgFnPc2d1	stavební
parcel	parcela	k1gFnPc2	parcela
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
především	především	k9	především
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc6	lesnictví
a	a	k8xC	a
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
katastrů	katastr	k1gInPc2	katastr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
není	být	k5eNaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
akceptováno	akceptován	k2eAgNnSc1d1	akceptováno
její	její	k3xOp3gNnSc4	její
používání	používání	k1gNnSc4	používání
společně	společně	k6eAd1	společně
s	s	k7c7	s
SI	si	k1gNnSc7	si
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
<g/>
Hektar	hektar	k1gInSc1	hektar
je	být	k5eAaImIp3nS	být
stonásobkem	stonásobek	k1gInSc7	stonásobek
jednotky	jednotka	k1gFnSc2	jednotka
ar	ar	k1gInSc1	ar
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
předpona	předpona	k1gFnSc1	předpona
hekto-	hekto-	k?	hekto-
<g/>
.	.	kIx.	.
</s>
<s>
Hektar	hektar	k1gInSc1	hektar
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
roven	roven	k2eAgInSc1d1	roven
10	[number]	k4	10
000	[number]	k4	000
m2	m2	k4	m2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
čtverce	čtverec	k1gInSc2	čtverec
o	o	k7c6	o
straně	strana	k1gFnSc6	strana
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
100	[number]	k4	100
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Převod	převod	k1gInSc1	převod
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
jednotky	jednotka	k1gFnPc4	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
hektar	hektar	k1gInSc1	hektar
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
</s>
</p>
<p>
<s>
0,01	[number]	k4	0,01
km2	km2	k4	km2
</s>
</p>
<p>
<s>
10	[number]	k4	10
dekarů	dekar	k1gInPc2	dekar
(	(	kIx(	(
<g/>
daa	daa	k?	daa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
100	[number]	k4	100
arů	ar	k1gInPc2	ar
</s>
</p>
<p>
<s>
10	[number]	k4	10
000	[number]	k4	000
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
(	(	kIx(	(
<g/>
hm	hm	k?	hm
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
hektometr	hektometr	k1gInSc4	hektometr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
1010	[number]	k4	1010
mm2	mm2	k4	mm2
</s>
</p>
<p>
<s>
107	[number]	k4	107
639	[number]	k4	639
čtverečních	čtvereční	k2eAgFnPc2d1	čtvereční
stop	stopa	k1gFnPc2	stopa
</s>
</p>
<p>
<s>
0,00386	[number]	k4	0,00386
102	[number]	k4	102
čtverečních	čtvereční	k2eAgFnPc2d1	čtvereční
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
statutárních	statutární	k2eAgInPc2d1	statutární
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
hřiště	hřiště	k1gNnSc4	hřiště
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
zápasy	zápas	k1gInPc4	zápas
má	mít	k5eAaImIp3nS	mít
plochu	plocha	k1gFnSc4	plocha
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
hektaru	hektar	k1gInSc2	hektar
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
pravidla	pravidlo	k1gNnSc2	pravidlo
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
rozmezí	rozmezí	k1gNnSc4	rozmezí
0,4	[number]	k4	0,4
<g/>
–	–	k?	–
<g/>
1,08	[number]	k4	1,08
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přes	přes	k7c4	přes
4	[number]	k4	4
hektary	hektar	k1gInPc4	hektar
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
682	[number]	k4	682
<g/>
×	×	k?	×
<g/>
60	[number]	k4	60
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celé	celý	k2eAgNnSc1d1	celé
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
téměř	téměř	k6eAd1	téměř
7,9	[number]	k4	7,9
milionu	milion	k4xCgInSc2	milion
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hektar	hektar	k1gInSc1	hektar
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Hektar	hektar	k1gInSc1	hektar
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
jednotky	jednotka	k1gFnPc1	jednotka
mimo	mimo	k7c4	mimo
SI	si	k1gNnSc4	si
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
přijaté	přijatý	k2eAgFnPc1d1	přijatá
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
s	s	k7c7	s
SI	si	k1gNnSc7	si
od	od	k7c2	od
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
