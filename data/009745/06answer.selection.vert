<s>
Hektar	hektar	k1gInSc1	hektar
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ha	ha	kA	ha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
plošný	plošný	k2eAgInSc4d1	plošný
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
plochy	plocha	k1gFnSc2	plocha
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
stavebních	stavební	k2eAgFnPc2d1	stavební
parcel	parcela	k1gFnPc2	parcela
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
především	především	k9	především
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc6	lesnictví
a	a	k8xC	a
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
katastrů	katastr	k1gInPc2	katastr
<g/>
.	.	kIx.	.
</s>
