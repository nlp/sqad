<s>
Protože	protože	k8xS
je	být	k5eAaImIp3nS
Venuše	Venuše	k1gFnSc1
ke	k	k7c3
Slunci	slunce	k1gNnSc3
blíže	blíž	k1gFnSc2
než	než	k8xS
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
úhlová	úhlový	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
od	od	k7c2
Slunce	slunce	k1gNnSc2
nemůže	moct	k5eNaImIp3nS
překročit	překročit	k5eAaPmF
určitou	určitý	k2eAgFnSc4d1
mez	mez	k1gFnSc4
(	(	kIx(
<g/>
největší	veliký	k2eAgFnSc1d3
elongace	elongace	k1gFnSc1
je	být	k5eAaImIp3nS
47,8	[number]	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
a	a	k8xC
lze	lze	k6eAd1
ji	on	k3xPp3gFnSc4
ze	z	k7c2
Země	zem	k1gFnSc2
vidět	vidět	k5eAaImF
jen	jen	k9
před	před	k7c7
úsvitem	úsvit	k1gInSc7
nebo	nebo	k8xC
po	po	k7c6
soumraku	soumrak	k1gInSc6
<g/>
.	.	kIx.
</s>