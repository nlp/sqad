<p>
<s>
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc4d1	sledovaný
vlaky	vlak	k1gInPc4	vlak
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
poválečné	poválečný	k2eAgFnSc2d1	poválečná
prózy	próza	k1gFnSc2	próza
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
Ostře	ostro	k6eAd1	ostro
sledovaných	sledovaný	k2eAgInPc2d1	sledovaný
vlaků	vlak	k1gInPc2	vlak
se	se	k3xPyFc4	se
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
událostí	událost	k1gFnSc7	událost
z	z	k7c2	z
období	období	k1gNnSc2	období
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
odpálením	odpálení	k1gNnPc3	odpálení
německého	německý	k2eAgInSc2d1	německý
muničního	muniční	k2eAgInSc2d1	muniční
vlaku	vlak	k1gInSc2	vlak
podskupinou	podskupina	k1gFnSc7	podskupina
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
skupiny	skupina	k1gFnSc2	skupina
Podřipsko	Podřipsko	k1gNnSc1	Podřipsko
nedaleko	nedaleko	k7c2	nedaleko
stanice	stanice	k1gFnSc2	stanice
Stratov	Stratov	k1gInSc1	Stratov
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
odrážejí	odrážet	k5eAaImIp3nP	odrážet
Hrabalovy	Hrabalův	k2eAgFnPc1d1	Hrabalova
zkušenosti	zkušenost	k1gFnPc1	zkušenost
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Kostomlaty	Kostomle	k1gNnPc7	Kostomle
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
zastával	zastávat	k5eAaImAgMnS	zastávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
funkci	funkce	k1gFnSc4	funkce
výpravčího	výpravčí	k1gMnSc2	výpravčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
Hrabalovy	Hrabalův	k2eAgFnSc2d1	Hrabalova
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
přechod	přechod	k1gInSc1	přechod
chlapce	chlapec	k1gMnSc2	chlapec
v	v	k7c4	v
muže	muž	k1gMnPc4	muž
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
do	do	k7c2	do
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
běžných	běžný	k2eAgMnPc2d1	běžný
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
měnila	měnit	k5eAaImAgFnS	měnit
jejich	jejich	k3xOp3gInSc4	jejich
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
protektorátních	protektorátní	k2eAgFnPc6d1	protektorátní
Čechách	Čechy	k1gFnPc6	Čechy
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
malé	malý	k2eAgFnSc2d1	malá
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
hrdina	hrdina	k1gMnSc1	hrdina
novely	novela	k1gFnSc2	novela
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Hrma	hrma	k1gFnSc1	hrma
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
příběhu	příběh	k1gInSc2	příběh
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
průvodčí	průvodčí	k1gFnSc7	průvodčí
Mášou	Máša	k1gFnSc7	Máša
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
končí	končit	k5eAaImIp3nS	končit
sexuálním	sexuální	k2eAgInSc7d1	sexuální
nezdarem	nezdar	k1gInSc7	nezdar
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
první	první	k4xOgInSc4	první
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
neúspěchu	neúspěch	k1gInSc6	neúspěch
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
naštěstí	naštěstí	k6eAd1	naštěstí
je	být	k5eAaImIp3nS	být
však	však	k9	však
zachráněn	zachráněn	k2eAgMnSc1d1	zachráněn
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Miloš	Miloš	k1gMnSc1	Miloš
zaškoluje	zaškolovat	k5eAaImIp3nS	zaškolovat
na	na	k7c4	na
výpravčího	výpravčí	k1gMnSc4	výpravčí
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pravidelně	pravidelně	k6eAd1	pravidelně
projíždějí	projíždět	k5eAaImIp3nP	projíždět
ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
německé	německý	k2eAgInPc1d1	německý
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
převážející	převážející	k2eAgFnPc4d1	převážející
zásoby	zásoba	k1gFnPc4	zásoba
nebo	nebo	k8xC	nebo
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
perverzním	perverzní	k2eAgInSc7d1	perverzní
a	a	k8xC	a
bezostyšným	bezostyšný	k2eAgMnSc7d1	bezostyšný
školitelem	školitel	k1gMnSc7	školitel
výpravčím	výpravčí	k1gMnSc7	výpravčí
Hubičkou	hubička	k1gFnSc7	hubička
a	a	k8xC	a
přednostou	přednosta	k1gMnSc7	přednosta
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chová	chovat	k5eAaImIp3nS	chovat
holuby	holub	k1gMnPc4	holub
a	a	k8xC	a
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
povýšení	povýšení	k1gNnSc6	povýšení
<g/>
,	,	kIx,	,
pozná	poznat	k5eAaPmIp3nS	poznat
manželku	manželka	k1gFnSc4	manželka
pana	pan	k1gMnSc2	pan
přednosty	přednosta	k1gMnSc2	přednosta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ale	ale	k8xC	ale
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
Milošovi	Miloš	k1gMnSc3	Miloš
pomoci	pomoct	k5eAaPmF	pomoct
získat	získat	k5eAaPmF	získat
erotickou	erotický	k2eAgFnSc4d1	erotická
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
partyzánkou	partyzánka	k1gFnSc7	partyzánka
Viktorií	Viktoria	k1gFnSc7	Viktoria
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
první	první	k4xOgMnSc1	první
zdařený	zdařený	k2eAgInSc1d1	zdařený
sexuální	sexuální	k2eAgInSc1d1	sexuální
zážitek	zážitek	k1gInSc1	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
sabotážní	sabotážní	k2eAgFnPc1d1	sabotážní
akce	akce	k1gFnPc1	akce
a	a	k8xC	a
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
projíždějících	projíždějící	k2eAgInPc2d1	projíždějící
německých	německý	k2eAgInPc2d1	německý
vlaků	vlak	k1gInPc2	vlak
shodí	shodit	k5eAaPmIp3nS	shodit
z	z	k7c2	z
návěstidla	návěstidlo	k1gNnSc2	návěstidlo
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
doručila	doručit	k5eAaPmAgFnS	doručit
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
postřelí	postřelit	k5eAaPmIp3nS	postřelit
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
vojákem	voják	k1gMnSc7	voják
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Inspirace	inspirace	k1gFnSc1	inspirace
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
novely	novela	k1gFnSc2	novela
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
natočen	natočen	k2eAgInSc1d1	natočen
známý	známý	k2eAgInSc1d1	známý
oscarový	oscarový	k2eAgInSc1d1	oscarový
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Václav	Václav	k1gMnSc1	Václav
Neckář	Neckář	k1gMnSc1	Neckář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
