<s>
Muzeum	muzeum	k1gNnSc1
iluzivního	iluzivní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
Praha	Praha	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Muzeum	muzeum	k1gNnSc1
iluzivního	iluzivní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
Trick-artová	Trick-artový	k2eAgFnSc1d1
malba	malba	k1gFnSc1
v	v	k7c4
Illusion	Illusion	k1gInSc4
Art	Art	k1gFnSc2
Museu	museum	k1gNnSc6
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
malby	malba	k1gFnSc2
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Daněk	Daněk	k1gMnSc1
<g/>
)	)	kIx)
Údaje	údaj	k1gInPc1
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Město	město	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Staroměstské	staroměstský	k2eAgNnSc1d1
nám.	nám.	k?
480	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
,	,	kIx,
Praha-Josefov	Praha-Josefov	k1gInSc1
Založeno	založit	k5eAaPmNgNnS
</s>
<s>
2018	#num#	k4
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
12,12	12,12	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
15,5	15,5	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Muzeum	muzeum	k1gNnSc1
iluzivního	iluzivní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Illusion	Illusion	k1gInSc4
Art	Art	k1gFnSc2
Museum	museum	k1gNnSc4
Prague	Prague	k1gNnSc2
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
IAM	IAM	kA
Prague	Pragu	k1gFnSc2
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
skupinou	skupina	k1gFnSc7
českého	český	k2eAgMnSc2d1
vizuálního	vizuální	k2eAgMnSc2d1
umělce	umělec	k1gMnSc2
Patrika	Patrik	k1gMnSc2
Proška	Prošek	k1gMnSc2
a	a	k8xC
producenta	producent	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
Bechyně	Bechyně	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgNnSc4
muzeum	muzeum	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
výhradně	výhradně	k6eAd1
iluzivnímu	iluzivní	k2eAgNnSc3d1
a	a	k8xC
trikovému	trikový	k2eAgNnSc3d1
umění	umění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expozice	expozice	k1gFnSc1
IAM	IAM	kA
Prague	Prague	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
svého	svůj	k3xOyFgInSc2
počátku	počátek	k1gInSc2
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
historické	historický	k2eAgFnSc6d1
budově	budova	k1gFnSc6
domu	dům	k1gInSc2
U	u	k7c2
Červené	Červené	k2eAgFnSc2d1
lišky	liška	k1gFnSc2
na	na	k7c6
Staroměstském	staroměstský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
expozice	expozice	k1gFnSc2
</s>
<s>
Po	po	k7c6
otevření	otevření	k1gNnSc6
muzea	muzeum	k1gNnSc2
se	se	k3xPyFc4
expozice	expozice	k1gFnSc1
věnovaná	věnovaný	k2eAgFnSc1d1
iluzivnímu	iluzivní	k2eAgInSc3d1
umění	umění	k1gNnSc2
nacházela	nacházet	k5eAaImAgFnS
pouze	pouze	k6eAd1
v	v	k7c6
prvním	první	k4xOgInSc6
patře	patro	k1gNnSc6
zatímco	zatímco	k8xS
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
probíhala	probíhat	k5eAaImAgFnS
výstava	výstava	k1gFnSc1
tvorby	tvorba	k1gFnSc2
předního	přední	k2eAgMnSc4d1
umělce	umělec	k1gMnSc4
Patrika	Patrik	k1gMnSc4
Proška	Prošek	k1gMnSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
obsáhlá	obsáhlý	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
streetartového	streetartový	k2eAgNnSc2d1
umění	umění	k1gNnSc2
věnovaná	věnovaný	k2eAgFnSc1d1
výročí	výročí	k1gNnSc3
sta	sto	k4xCgNnPc4
let	léto	k1gNnPc2
od	od	k7c2
založení	založení	k1gNnSc2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
expozice	expozice	k1gFnSc1
iluzí	iluze	k1gFnPc2
rozprostřena	rozprostřen	k2eAgFnSc1d1
ve	v	k7c6
všech	všecek	k3xTgNnPc6
třech	tři	k4xCgNnPc6
poschodích	poschodí	k1gNnPc6
domu	dům	k1gInSc2
U	u	k7c2
Červené	Červené	k2eAgFnSc2d1
lišky	liška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
budově	budova	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
gotické	gotický	k2eAgNnSc1d1
sklepení	sklepení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
využíváno	využívat	k5eAaImNgNnS,k5eAaPmNgNnS
jako	jako	k8xC,k8xS
galerijní	galerijní	k2eAgInSc1d1
prostor	prostor	k1gInSc1
pro	pro	k7c4
krátkodobé	krátkodobý	k2eAgFnPc4d1
výstavy	výstava	k1gFnPc4
českých	český	k2eAgMnPc2d1
i	i	k8xC
zahraničních	zahraniční	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přízemí	přízemí	k1gNnSc1
domu	dům	k1gInSc2
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
kavárna	kavárna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgFnPc1d1
umístění	umístění	k1gNnSc4
expozice	expozice	k1gFnSc2
v	v	k7c6
historické	historický	k2eAgFnSc6d1
budově	budova	k1gFnSc6
umožňuje	umožňovat	k5eAaImIp3nS
návštěvníkům	návštěvník	k1gMnPc3
prohlédnout	prohlédnout	k5eAaPmF
si	se	k3xPyFc3
raně	raně	k6eAd1
barokní	barokní	k2eAgFnPc4d1
nástropní	nástropní	k2eAgFnPc4d1
malby	malba	k1gFnPc4
a	a	k8xC
vychutnat	vychutnat	k5eAaPmF
si	se	k3xPyFc3
výhled	výhled	k1gInSc4
na	na	k7c4
Staroměstské	staroměstský	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
a	a	k8xC
především	především	k9
na	na	k7c4
pražský	pražský	k2eAgInSc4d1
orloj	orloj	k1gInSc4
<g/>
,	,	kIx,
naproti	naproti	k7c3
kterému	který	k3yQgInSc3,k3yIgInSc3,k3yRgInSc3
se	se	k3xPyFc4
budova	budova	k1gFnSc1
muzea	muzeum	k1gNnSc2
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Současná	současný	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
</s>
<s>
V	v	k7c6
muzeu	muzeum	k1gNnSc6
je	být	k5eAaImIp3nS
k	k	k7c3
vidění	vidění	k1gNnSc3
množství	množství	k1gNnSc2
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
využívajících	využívající	k2eAgInPc2d1
optické	optický	k2eAgMnPc4d1
iluze	iluze	k1gFnSc2
a	a	k8xC
děl	dělo	k1gNnPc2
založených	založený	k2eAgMnPc2d1
na	na	k7c6
různé	různý	k2eAgFnSc6d1
technice	technika	k1gFnSc6
tvorby	tvorba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
trick	tricka	k1gFnPc2
artu	aríst	k5eAaPmIp1nS
lze	lze	k6eAd1
obdivovat	obdivovat	k5eAaImF
tajemství	tajemství	k1gNnSc4
stereogramů	stereogram	k1gInPc2
<g/>
,	,	kIx,
exponátů	exponát	k1gInPc2
založených	založený	k2eAgInPc2d1
na	na	k7c6
principu	princip	k1gInSc6
převrácené	převrácený	k2eAgFnSc2d1
perspektivy	perspektiva	k1gFnSc2
nebo	nebo	k8xC
tzv.	tzv.	kA
anamorfóz	anamorfóza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tématem	téma	k1gNnSc7
uměleckých	umělecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
známe	znát	k5eAaImIp1nP
osobnosti	osobnost	k1gFnPc4
či	či	k8xC
události	událost	k1gFnPc4
světových	světový	k2eAgFnPc2d1
a	a	k8xC
českých	český	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
William	William	k1gInSc4
Shakespeare	Shakespeare	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Baťa	Baťa	k1gMnSc1
<g/>
,	,	kIx,
Nikola	Nikola	k1gMnSc1
Tesla	Tesla	k1gFnSc1
<g/>
,	,	kIx,
pražská	pražský	k2eAgFnSc1d1
defenestrace	defenestrace	k1gFnSc1
atd.	atd.	kA
Dále	daleko	k6eAd2
si	se	k3xPyFc3
návštěvníci	návštěvník	k1gMnPc1
mohou	moct	k5eAaImIp3nP
vyzkoušet	vyzkoušet	k5eAaPmF
techniku	technika	k1gFnSc4
light	lighta	k1gFnPc2
art	art	k?
<g/>
,	,	kIx,
vyvinutý	vyvinutý	k2eAgInSc4d1
umělcem	umělec	k1gMnSc7
Alexem	Alex	k1gMnSc7
Dowisem	Dowis	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ho	on	k3xPp3gMnSc4
prezentoval	prezentovat	k5eAaBmAgInS
v	v	k7c6
talentových	talentový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Iluze	iluze	k1gFnSc1
vystavované	vystavovaný	k2eAgNnSc1d1
v	v	k7c6
IAM	IAM	kA
Prague	Prague	k1gNnSc1
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
mnohých	mnohý	k2eAgMnPc2d1
českých	český	k2eAgMnPc2d1
i	i	k8xC
světových	světový	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystavuje	vystavovat	k5eAaImIp3nS
zde	zde	k6eAd1
například	například	k6eAd1
Ivana	Ivana	k1gFnSc1
Štenclová	Štenclová	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Daněk	Daněk	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Herel	Herel	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Strauzz	Strauzz	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
metalurgickou	metalurgický	k2eAgFnSc4d1
malbu	malba	k1gFnSc4
zde	zde	k6eAd1
předvádí	předvádět	k5eAaImIp3nS
Ladislav	Ladislav	k1gMnSc1
Vlna	vlna	k1gFnSc1
a	a	k8xC
nechybí	chybět	k5eNaImIp3nS,k5eNaPmIp3nS
ani	ani	k8xC
tak	tak	k9
významní	významný	k2eAgMnPc1d1
současní	současný	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Patrick	Patrick	k1gMnSc1
Hughes	Hughes	k1gMnSc1
či	či	k8xC
Patrik	Patrik	k1gMnSc1
Proško	Proška	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
kromě	kromě	k7c2
osobní	osobní	k2eAgFnSc2d1
návštěvy	návštěva	k1gFnSc2
navštívit	navštívit	k5eAaPmF
Illusion	Illusion	k1gInSc4
Art	Art	k1gFnSc2
Museum	museum	k1gNnSc1
ze	z	k7c2
všech	všecek	k3xTgInPc2
koutů	kout	k1gInPc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
byla	být	k5eAaImAgFnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c6
pandemii	pandemie	k1gFnSc6
koronaviru	koronavir	k1gInSc2
připravena	připravit	k5eAaPmNgFnS
obsáhlá	obsáhlý	k2eAgFnSc1d1
virtuální	virtuální	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
celé	celý	k2eAgFnSc2d1
expozice	expozice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
na	na	k7c6
stránkách	stránka	k1gFnPc6
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Illusion	Illusion	k1gInSc1
Art	Art	k1gFnPc2
Museum	museum	k1gNnSc1
<g/>
,	,	kIx,
Prague	Prague	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Illusion	Illusion	k1gInSc4
Art	Art	k1gFnSc2
Museum	museum	k1gNnSc4
Prague	Prague	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
