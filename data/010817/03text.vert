<p>
<s>
Troodos	Troodos	k1gInSc1	Troodos
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
největšího	veliký	k2eAgNnSc2d3	veliký
kyperského	kyperský	k2eAgNnSc2d1	kyperské
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Olymp	Olymp	k1gInSc1	Olymp
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1	[number]	k4	1
952	[number]	k4	952
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
starobylých	starobylý	k2eAgFnPc2d1	starobylá
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
množství	množství	k1gNnSc1	množství
kostelů	kostel	k1gInPc2	kostel
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kostely	kostel	k1gInPc1	kostel
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgInPc1d1	plný
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
maleb	malba	k1gFnPc2	malba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCa	Unesco	k1gNnSc2	Unesco
<g/>
;	;	kIx,	;
desátý	desátý	k4xOgMnSc1	desátý
byl	být	k5eAaImAgInS	být
přidán	přidán	k2eAgInSc1d1	přidán
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Troodos	Troodosa	k1gFnPc2	Troodosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
