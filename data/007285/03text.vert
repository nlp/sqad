<s>
Motivace	motivace	k1gFnSc1	motivace
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
nebo	nebo	k8xC	nebo
vnější	vnější	k2eAgInSc1d1	vnější
faktor	faktor	k1gInSc1	faktor
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc1	soubor
faktorů	faktor	k1gInPc2	faktor
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
k	k	k7c3	k
energetizaci	energetizace	k1gFnSc3	energetizace
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc1	motivace
usměrňuje	usměrňovat	k5eAaImIp3nS	usměrňovat
naše	náš	k3xOp1gNnSc4	náš
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
jednání	jednání	k1gNnSc4	jednání
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
určitého	určitý	k2eAgInSc2d1	určitý
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
souhrn	souhrn	k1gInSc4	souhrn
všech	všecek	k3xTgFnPc2	všecek
skutečností	skutečnost	k1gFnPc2	skutečnost
–	–	k?	–
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
zvídavost	zvídavost	k1gFnSc4	zvídavost
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
radostné	radostný	k2eAgNnSc1d1	radostné
očekávání	očekávání	k1gNnSc1	očekávání
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
nebo	nebo	k8xC	nebo
tlumí	tlumit	k5eAaImIp3nP	tlumit
jedince	jedinec	k1gMnSc4	jedinec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
něco	něco	k3yInSc4	něco
konal	konat	k5eAaImAgInS	konat
nebo	nebo	k8xC	nebo
nekonal	konat	k5eNaImAgInS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Motivy	motiv	k1gInPc1	motiv
jsou	být	k5eAaImIp3nP	být
osobní	osobní	k2eAgFnPc4d1	osobní
příčiny	příčina	k1gFnPc4	příčina
určitého	určitý	k2eAgNnSc2d1	určité
chování	chování	k1gNnSc2	chování
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pohnutky	pohnutka	k1gFnPc1	pohnutka
<g/>
,	,	kIx,	,
psychologické	psychologický	k2eAgFnPc1d1	psychologická
příčiny	příčina	k1gFnPc1	příčina
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
jednání	jednání	k1gNnSc1	jednání
člověka	člověk	k1gMnSc2	člověk
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
uspokojování	uspokojování	k1gNnSc4	uspokojování
určitých	určitý	k2eAgFnPc2d1	určitá
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
formu	forma	k1gFnSc4	forma
motivů	motiv	k1gInPc2	motiv
jsou	být	k5eAaImIp3nP	být
pokládány	pokládán	k2eAgFnPc1d1	pokládána
potřeby	potřeba	k1gFnPc1	potřeba
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
z	z	k7c2	z
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
nedostatku	nedostatek	k1gInSc2	nedostatek
nebo	nebo	k8xC	nebo
nadbytku	nadbytek	k1gInSc2	nadbytek
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k9	co
nás	my	k3xPp1nPc4	my
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
tuto	tento	k3xDgFnSc4	tento
potřebu	potřeba	k1gFnSc4	potřeba
uspokojujeme	uspokojovat	k5eAaImIp1nP	uspokojovat
<g/>
.	.	kIx.	.
</s>
<s>
Potřeby	potřeba	k1gFnPc1	potřeba
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
biologické	biologický	k2eAgNnSc4d1	biologické
(	(	kIx(	(
<g/>
biogenní	biogenní	k2eAgNnSc4d1	biogenní
<g/>
,	,	kIx,	,
primární	primární	k2eAgNnSc4d1	primární
<g/>
,	,	kIx,	,
vrozené	vrozený	k2eAgNnSc4d1	vrozené
<g/>
)	)	kIx)	)
-	-	kIx~	-
potřeba	potřeba	k1gFnSc1	potřeba
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
,	,	kIx,	,
spánku	spánek	k1gInSc2	spánek
apod.	apod.	kA	apod.
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
<g/>
,	,	kIx,	,
reprodukci	reprodukce	k1gFnSc3	reprodukce
nebo	nebo	k8xC	nebo
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
sociální	sociální	k2eAgFnSc2d1	sociální
(	(	kIx(	(
<g/>
psychogenní	psychogenní	k2eAgFnSc2d1	psychogenní
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgMnSc1d1	sekundární
<g/>
,	,	kIx,	,
získané	získaný	k2eAgInPc1d1	získaný
<g/>
)	)	kIx)	)
-	-	kIx~	-
kulturní	kulturní	k2eAgNnSc4d1	kulturní
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInSc4d1	kulturní
život	život	k1gInSc4	život
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
psychické	psychický	k2eAgNnSc1d1	psychické
(	(	kIx(	(
<g/>
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
adaptaci	adaptace	k1gFnSc4	adaptace
na	na	k7c4	na
sociální	sociální	k2eAgFnPc4d1	sociální
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
v	v	k7c6	v
sociálním	sociální	k2eAgNnSc6d1	sociální
bytí	bytí	k1gNnSc6	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc1	motivace
může	moct	k5eAaImIp3nS	moct
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
nebo	nebo	k8xC	nebo
vnějších	vnější	k2eAgFnPc2d1	vnější
pohnutek	pohnutka	k1gFnPc2	pohnutka
a	a	k8xC	a
podnětů	podnět	k1gInPc2	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
kombinací	kombinace	k1gFnSc7	kombinace
obou	dva	k4xCgFnPc2	dva
<g/>
.	.	kIx.	.
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
motivace	motivace	k1gFnSc1	motivace
-	-	kIx~	-
výsledek	výsledek	k1gInSc1	výsledek
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
zájmů	zájem	k1gInPc2	zájem
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
potřeba	potřeba	k6eAd1	potřeba
poznávací	poznávací	k2eAgFnPc4d1	poznávací
<g/>
,	,	kIx,	,
seberealizace	seberealizace	k1gFnPc4	seberealizace
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc4d1	kulturní
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
)	)	kIx)	)
vnější	vnější	k2eAgFnPc4d1	vnější
motivace	motivace	k1gFnPc4	motivace
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
působením	působení	k1gNnSc7	působení
vnějších	vnější	k2eAgInPc2d1	vnější
podnětů	podnět	k1gInPc2	podnět
(	(	kIx(	(
<g/>
hrozba	hrozba	k1gFnSc1	hrozba
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
odměny	odměna	k1gFnSc2	odměna
<g/>
)	)	kIx)	)
Americký	americký	k2eAgMnSc1d1	americký
psycholog	psycholog	k1gMnSc1	psycholog
Abraham	Abraham	k1gMnSc1	Abraham
Maslow	Maslow	k1gMnSc1	Maslow
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
stupňovitého	stupňovitý	k2eAgNnSc2d1	stupňovité
řazení	řazení	k1gNnSc2	řazení
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
hierarchickém	hierarchický	k2eAgInSc6d1	hierarchický
systému	systém	k1gInSc6	systém
organizoval	organizovat	k5eAaBmAgInS	organizovat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
naléhavosti	naléhavost	k1gFnSc2	naléhavost
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Potřeby	potřeba	k1gFnPc1	potřeba
vyšší	vysoký	k2eAgFnPc1d2	vyšší
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
až	až	k9	až
po	po	k7c4	po
uspokojení	uspokojení	k1gNnSc4	uspokojení
potřeb	potřeba	k1gFnPc2	potřeba
nižších	nízký	k2eAgFnPc2d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
potřebu	potřeba	k1gFnSc4	potřeba
seberealizace	seberealizace	k1gFnSc2	seberealizace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
hladový	hladový	k2eAgMnSc1d1	hladový
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
,	,	kIx,	,
milován	milován	k2eAgInSc1d1	milován
a	a	k8xC	a
uznáván	uznáván	k2eAgInSc1d1	uznáván
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
člověk	člověk	k1gMnSc1	člověk
netouží	toužit	k5eNaImIp3nP	toužit
po	po	k7c6	po
nových	nový	k2eAgInPc6d1	nový
závěsech	závěs	k1gInPc6	závěs
do	do	k7c2	do
pokoje	pokoj	k1gInSc2	pokoj
nebo	nebo	k8xC	nebo
obraze	obraz	k1gInSc6	obraz
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ohrožován	ohrožován	k2eAgMnSc1d1	ohrožován
nějakou	nějaký	k3yIgFnSc7	nějaký
katastrofou	katastrofa	k1gFnSc7	katastrofa
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
hladový	hladový	k2eAgMnSc1d1	hladový
(	(	kIx(	(
<g/>
nenaplněný	naplněný	k2eNgMnSc1d1	nenaplněný
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pud	pud	k1gInSc1	pud
–	–	k?	–
vrozená	vrozený	k2eAgFnSc1d1	vrozená
pohnutka	pohnutka	k1gFnSc1	pohnutka
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
nebo	nebo	k8xC	nebo
cílenou	cílený	k2eAgFnSc4d1	cílená
činnost	činnost	k1gFnSc4	činnost
až	až	k8xS	až
nutkání	nutkání	k1gNnSc4	nutkání
(	(	kIx(	(
<g/>
pud	pud	k1gInSc4	pud
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
<g/>
,	,	kIx,	,
mateřský	mateřský	k2eAgInSc4d1	mateřský
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Zájem	zájem	k1gInSc1	zájem
–	–	k?	–
získaný	získaný	k2eAgInSc1d1	získaný
motiv	motiv	k1gInSc1	motiv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
kladným	kladný	k2eAgInSc7d1	kladný
vztahem	vztah	k1gInSc7	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
předmětům	předmět	k1gInPc3	předmět
nebo	nebo	k8xC	nebo
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ho	on	k3xPp3gMnSc4	on
upoutávají	upoutávat	k5eAaImIp3nP	upoutávat
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
poznávací	poznávací	k2eAgFnSc6d1	poznávací
nebo	nebo	k8xC	nebo
citové	citový	k2eAgFnSc6d1	citová
<g/>
.	.	kIx.	.
</s>
<s>
Vyhraněný	vyhraněný	k2eAgInSc4d1	vyhraněný
zájem	zájem	k1gInSc4	zájem
označujeme	označovat	k5eAaImIp1nP	označovat
pojmem	pojem	k1gInSc7	pojem
záliba	záliba	k1gFnSc1	záliba
<g/>
.	.	kIx.	.
</s>
<s>
Aspirace	aspirace	k1gFnSc1	aspirace
(	(	kIx(	(
<g/>
ambice	ambice	k1gFnSc1	ambice
<g/>
)	)	kIx)	)
–	–	k?	–
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
sebeuplatnění	sebeuplatnění	k1gNnSc4	sebeuplatnění
<g/>
,	,	kIx,	,
vyniknutí	vyniknutí	k1gNnSc4	vyniknutí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
ctižádost	ctižádost	k1gFnSc1	ctižádost
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc1	cíl
–	–	k?	–
uvědomělý	uvědomělý	k2eAgInSc1d1	uvědomělý
směr	směr	k1gInSc1	směr
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
když	když	k8xS	když
chceme	chtít	k5eAaImIp1nP	chtít
něčeho	něco	k3yInSc2	něco
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
,	,	kIx,	,
něčemu	něco	k3yInSc3	něco
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
dělat	dělat	k5eAaImF	dělat
či	či	k8xC	či
nedělat	dělat	k5eNaImF	dělat
apod.	apod.	kA	apod.
Ideály	ideál	k1gInPc1	ideál
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
vzorové	vzorový	k2eAgInPc1d1	vzorový
cíle	cíl	k1gInPc1	cíl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ideál	ideál	k1gInSc1	ideál
životního	životní	k2eAgMnSc2d1	životní
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
způsobu	způsoba	k1gFnSc4	způsoba
života	život	k1gInSc2	život
apod.	apod.	kA	apod.
Zvyk	zvyk	k1gInSc1	zvyk
–	–	k?	–
tendence	tendence	k1gFnSc1	tendence
vykonávat	vykonávat	k5eAaImF	vykonávat
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
určitou	určitý	k2eAgFnSc4d1	určitá
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Odmaturuj	odmaturovat	k5eAaPmRp2nS	odmaturovat
<g/>
!	!	kIx.	!
</s>
<s>
Ze	z	k7c2	z
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Didaktis	Didaktis	k1gFnPc2	Didaktis
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
JELÍNEK	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
<g/>
;	;	kIx,	;
JETMAROVÁ	JETMAROVÁ	kA	JETMAROVÁ
<g/>
,	,	kIx,	,
Kamila	Kamila	k1gFnSc1	Kamila
<g/>
.	.	kIx.	.
</s>
<s>
Neztraťte	ztratit	k5eNaPmRp2nP	ztratit
motivaci	motivace	k1gFnSc4	motivace
v	v	k7c6	v
době	doba	k1gFnSc6	doba
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
,	,	kIx,	,
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-262-1196-9	[number]	k4	978-80-262-1196-9
</s>
