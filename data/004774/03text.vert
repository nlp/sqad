<s>
Virus	virus	k1gInSc1	virus
Marburg	Marburg	k1gInSc1	Marburg
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
virus	virus	k1gInSc4	virus
patřící	patřící	k2eAgInPc4d1	patřící
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
filovirů	filovir	k1gInPc2	filovir
<g/>
.	.	kIx.	.
</s>
<s>
Marburg	Marburg	k1gInSc1	Marburg
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Marburg	Marburg	k1gInSc4	Marburg
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
25	[number]	k4	25
laboratorních	laboratorní	k2eAgMnPc2d1	laboratorní
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
6	[number]	k4	6
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
patologa	patolog	k1gMnSc2	patolog
se	se	k3xPyFc4	se
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
od	od	k7c2	od
nemocných	nemocný	k2eAgMnPc2d1	nemocný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
nakažených	nakažený	k2eAgMnPc2d1	nakažený
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
nákazy	nákaza	k1gFnSc2	nákaza
pocházel	pocházet	k5eAaImAgInS	pocházet
od	od	k7c2	od
opic	opice	k1gFnPc2	opice
dovezených	dovezený	k2eAgFnPc2d1	dovezená
na	na	k7c4	na
pokusy	pokus	k1gInPc4	pokus
z	z	k7c2	z
africké	africký	k2eAgFnSc2d1	africká
Ugandy	Uganda	k1gFnSc2	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
nákaz	nákaza	k1gFnPc2	nákaza
tímto	tento	k3xDgInSc7	tento
virem	vir	k1gInSc7	vir
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jih	jih	k1gInSc4	jih
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
Kongo	Kongo	k1gNnSc4	Kongo
a	a	k8xC	a
Angolu	Angola	k1gFnSc4	Angola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
399	[number]	k4	399
nakažených	nakažený	k2eAgFnPc2d1	nakažená
osob	osoba	k1gFnPc2	osoba
jich	on	k3xPp3gMnPc2	on
tehdy	tehdy	k6eAd1	tehdy
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
355	[number]	k4	355
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
virem	vir	k1gInSc7	vir
nakazila	nakazit	k5eAaPmAgFnS	nakazit
Nizozemka	Nizozemka	k1gFnSc1	Nizozemka
během	během	k7c2	během
dovolené	dovolená	k1gFnSc2	dovolená
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívila	navštívit	k5eAaPmAgFnS	navštívit
jeskyně	jeskyně	k1gFnSc1	jeskyně
obývané	obývaný	k2eAgMnPc4d1	obývaný
netopýry	netopýr	k1gMnPc4	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
následně	následně	k6eAd1	následně
viru	vir	k1gInSc2	vir
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
po	po	k7c6	po
selhání	selhání	k1gNnSc6	selhání
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
silném	silný	k2eAgNnSc6d1	silné
krvácení	krvácení	k1gNnSc6	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ebola	ebola	k6eAd1	ebola
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
krvácivé	krvácivý	k2eAgNnSc1d1	krvácivé
(	(	kIx(	(
<g/>
hemorhagické	hemorhagický	k2eAgFnSc2d1	hemorhagická
<g/>
)	)	kIx)	)
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
příznaky	příznak	k1gInPc7	příznak
je	být	k5eAaImIp3nS	být
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc4	zvracení
a	a	k8xC	a
silné	silný	k2eAgNnSc4d1	silné
krvácení	krvácení	k1gNnSc4	krvácení
z	z	k7c2	z
tělesných	tělesný	k2eAgInPc2d1	tělesný
otvorů	otvor	k1gInPc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Nakažené	nakažený	k2eAgFnPc1d1	nakažená
oběti	oběť	k1gFnPc1	oběť
obvykle	obvykle	k6eAd1	obvykle
umírají	umírat	k5eAaImIp3nP	umírat
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
Marburg	Marburg	k1gInSc1	Marburg
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
výkalů	výkal	k1gInPc2	výkal
<g/>
,	,	kIx,	,
slin	slina	k1gFnPc2	slina
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvratků	zvratek	k1gInPc2	zvratek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Marburg	Marburg	k1gInSc4	Marburg
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
ebolu	ebolo	k1gNnSc6	ebolo
neexistuje	existovat	k5eNaImIp3nS	existovat
účinný	účinný	k2eAgInSc1d1	účinný
lék	lék	k1gInSc1	lék
ani	ani	k8xC	ani
preventivní	preventivní	k2eAgNnSc1d1	preventivní
očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nebezpečnější	bezpečný	k2eNgFnSc1d2	nebezpečnější
než	než	k8xS	než
ebola	ebola	k1gFnSc1	ebola
<g/>
,	,	kIx,	,
mortalita	mortalita	k1gFnSc1	mortalita
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
87	[number]	k4	87
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gMnSc1	původ
viru	vir	k1gInSc2	vir
Marburg	Marburg	k1gMnSc1	Marburg
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
však	však	k9	však
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
kaloňů	kaloň	k1gMnPc2	kaloň
a	a	k8xC	a
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
ale	ale	k9	ale
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
žádné	žádný	k3yNgFnPc4	žádný
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
a	a	k8xC	a
Angole	Angola	k1gFnSc6	Angola
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Virus	virus	k1gInSc1	virus
Marburg	Marburg	k1gInSc4	Marburg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Filovirus	Filovirus	k1gMnSc1	Filovirus
Ebola	Ebola	k1gMnSc1	Ebola
</s>
