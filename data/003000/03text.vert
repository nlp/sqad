<s>
Eskymáci	Eskymák	k1gMnPc1	Eskymák
či	či	k8xC	či
Inuité	Inuita	k1gMnPc1	Inuita
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
mongoloidního	mongoloidní	k2eAgInSc2d1	mongoloidní
původu	původ	k1gInSc2	původ
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
)	)	kIx)	)
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
Asie	Asie	k1gFnSc1	Asie
(	(	kIx(	(
<g/>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Inuity	Inuita	k1gFnPc4	Inuita
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
především	především	k9	především
Eskymáci	Eskymák	k1gMnPc1	Eskymák
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Aljašce	Aljaška	k1gFnSc6	Aljaška
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Jupikové	Jupik	k1gMnPc1	Jupik
<g/>
,	,	kIx,	,
též	též	k9	též
Juité	Juita	k1gMnPc1	Juita
<g/>
,	,	kIx,	,
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
Kalaallité	Kalaallitý	k2eAgNnSc1d1	Kalaallitý
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
eskymácko-aleutskými	eskymáckoleutský	k2eAgInPc7d1	eskymácko-aleutský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Eskymák	Eskymák	k1gMnSc1	Eskymák
(	(	kIx(	(
<g/>
Eskimo	Eskima	k1gFnSc5	Eskima
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
algonkinských	algonkinský	k2eAgInPc2d1	algonkinský
jazyků	jazyk	k1gInPc2	jazyk
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
etymologie	etymologie	k1gFnSc1	etymologie
z	z	k7c2	z
kríského	kríský	k2eAgNnSc2d1	kríský
slova	slovo	k1gNnSc2	slovo
askamičiw	askamičiw	k?	askamičiw
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
jí	jíst	k5eAaImIp3nS	jíst
syrové	syrový	k2eAgNnSc1d1	syrové
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Jedlík	jedlík	k1gMnSc1	jedlík
syrového	syrový	k2eAgNnSc2d1	syrové
masa	maso	k1gNnSc2	maso
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc1	původ
z	z	k7c2	z
montaněského	montaněský	k1gMnSc2	montaněský
assime	assimat	k5eAaPmIp3nS	assimat
<g/>
·	·	k?	·
<g/>
w	w	k?	w
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
sněźnice	sněźnice	k1gFnSc1	sněźnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
Inuité	Inuita	k1gMnPc1	Inuita
jej	on	k3xPp3gInSc4	on
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
urážlivý	urážlivý	k2eAgMnSc1d1	urážlivý
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
užívali	užívat	k5eAaImAgMnP	užívat
název	název	k1gInSc4	název
Skræ	Skræ	k1gMnPc1	Skræ
"	"	kIx"	"
<g/>
Křiklouni	křikloun	k1gMnPc1	křikloun
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
inuitská	inuitský	k2eAgNnPc1d1	inuitský
slova	slovo	k1gNnPc1	slovo
přešla	přejít	k5eAaPmAgNnP	přejít
do	do	k7c2	do
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
anorak	anorak	k1gInSc1	anorak
<g/>
,	,	kIx,	,
kajak	kajak	k1gInSc1	kajak
<g/>
,	,	kIx,	,
nanuk	nanuk	k1gInSc1	nanuk
<g/>
,	,	kIx,	,
iglú	iglú	k1gNnSc1	iglú
nebo	nebo	k8xC	nebo
malamut	malamut	k1gInSc1	malamut
<g/>
.	.	kIx.	.
</s>
<s>
Předkové	předek	k1gMnPc1	předek
Eskymáků	Eskymák	k1gMnPc2	Eskymák
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Čukotka	Čukotka	k1gFnSc1	Čukotka
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
12	[number]	k4	12
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
přešli	přejít	k5eAaPmAgMnP	přejít
přes	přes	k7c4	přes
Aljašku	Aljaška	k1gFnSc4	Aljaška
podél	podél	k7c2	podél
kanadského	kanadský	k2eAgNnSc2d1	kanadské
pobřeží	pobřeží	k1gNnSc2	pobřeží
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
až	až	k9	až
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
znaky	znak	k1gInPc7	znak
tohoto	tento	k3xDgInSc2	tento
lidu	lid	k1gInSc2	lid
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
rovné	rovný	k2eAgInPc1d1	rovný
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
až	až	k9	až
světle	světle	k6eAd1	světle
hnědá	hnědat	k5eAaImIp3nS	hnědat
pleť	pleť	k1gFnSc4	pleť
<g/>
,	,	kIx,	,
mongoloidní	mongoloidní	k2eAgInPc4d1	mongoloidní
rysy	rys	k1gInPc4	rys
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
vzrůst	vzrůst	k1gInSc4	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
žijí	žít	k5eAaImIp3nP	žít
polokočovným	polokočovný	k2eAgInSc7d1	polokočovný
typem	typ	k1gInSc7	typ
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
pod	pod	k7c7	pod
stany	stan	k1gInPc7	stan
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
ve	v	k7c6	v
sněhovém	sněhový	k2eAgNnSc6d1	sněhové
iglú	iglú	k1gNnSc6	iglú
nebo	nebo	k8xC	nebo
v	v	k7c6	v
polopodzemních	polopodzemní	k2eAgNnPc6d1	polopodzemní
obydlích	obydlí	k1gNnPc6	obydlí
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
drnů	drn	k1gInPc2	drn
<g/>
.	.	kIx.	.
</s>
<s>
Přežití	přežití	k1gNnSc1	přežití
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
oblastech	oblast	k1gFnPc6	oblast
jim	on	k3xPp3gMnPc3	on
usnadňovalo	usnadňovat	k5eAaImAgNnS	usnadňovat
mnoho	mnoho	k4c1	mnoho
vynálezů	vynález	k1gInPc2	vynález
<g/>
.	.	kIx.	.
např.	např.	kA	např.
kajak	kajak	k1gInSc1	kajak
<g/>
,	,	kIx,	,
olejová	olejový	k2eAgFnSc1d1	olejová
lampa	lampa	k1gFnSc1	lampa
qudliq	qudliq	k?	qudliq
<g/>
,	,	kIx,	,
iglú	iglú	k1gNnSc4	iglú
<g/>
,	,	kIx,	,
brýle	brýle	k1gFnPc4	brýle
proti	proti	k7c3	proti
sněžné	sněžný	k2eAgFnSc3d1	sněžná
slepotě	slepota	k1gFnSc3	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
využívali	využívat	k5eAaPmAgMnP	využívat
hlavně	hlavně	k9	hlavně
čluny	člun	k1gInPc1	člun
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kajak	kajak	k1gInSc1	kajak
je	být	k5eAaImIp3nS	být
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
člun	člun	k1gInSc1	člun
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
využívaný	využívaný	k2eAgMnSc1d1	využívaný
hlavně	hlavně	k9	hlavně
lovci	lovec	k1gMnPc1	lovec
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
otevřený	otevřený	k2eAgInSc4d1	otevřený
umiak	umiak	k1gInSc4	umiak
používaly	používat	k5eAaImAgFnP	používat
ženy	žena	k1gFnPc1	žena
při	při	k7c6	při
stěhování	stěhování	k1gNnSc6	stěhování
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
sloužily	sloužit	k5eAaImAgFnP	sloužit
sněžnice	sněžnice	k1gFnPc1	sněžnice
a	a	k8xC	a
zejména	zejména	k9	zejména
saně	saně	k1gFnPc1	saně
se	s	k7c7	s
psím	psí	k2eAgNnSc7d1	psí
spřežením	spřežení	k1gNnSc7	spřežení
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgInSc7d1	přirozený
zdrojem	zdroj	k1gInSc7	zdroj
potravy	potrava	k1gFnSc2	potrava
Inuitů	Inuita	k1gMnPc2	Inuita
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
maso	maso	k1gNnSc1	maso
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
tuleni	tuleň	k1gMnPc1	tuleň
či	či	k8xC	či
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
uvízly	uvíznout	k5eAaPmAgFnP	uvíznout
na	na	k7c6	na
mělčině	mělčina	k1gFnSc6	mělčina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sob	sob	k1gMnSc1	sob
či	či	k8xC	či
pižmoň	pižmoň	k1gMnSc1	pižmoň
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
často	často	k6eAd1	často
syrové	syrový	k2eAgNnSc4d1	syrové
a	a	k8xC	a
zmražené	zmražený	k2eAgNnSc4d1	zmražené
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
masa	maso	k1gNnSc2	maso
jedí	jíst	k5eAaImIp3nP	jíst
často	často	k6eAd1	často
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
morek	morek	k1gInSc1	morek
a	a	k8xC	a
tuk	tuk	k1gInSc1	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
sezónně	sezónně	k6eAd1	sezónně
i	i	k9	i
ptačí	ptačí	k2eAgNnPc4d1	ptačí
vejce	vejce	k1gNnPc4	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nS	tvořit
bobule	bobule	k1gFnSc1	bobule
šichy	šicha	k1gFnSc2	šicha
<g/>
,	,	kIx,	,
klikvy	klikva	k1gFnSc2	klikva
<g/>
,	,	kIx,	,
brusinek	brusinka	k1gFnPc2	brusinka
<g/>
,	,	kIx,	,
ostružiníku	ostružiník	k1gInSc3	ostružiník
morušky	moruška	k1gFnSc2	moruška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zkvašené	zkvašený	k2eAgInPc4d1	zkvašený
listy	list	k1gInPc4	list
a	a	k8xC	a
pupen	pupen	k1gInSc4	pupen
zakrslých	zakrslý	k2eAgFnPc2d1	zakrslá
vrb	vrba	k1gFnPc2	vrba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
natrávený	natrávený	k2eAgInSc4d1	natrávený
lišejník	lišejník	k1gInSc4	lišejník
z	z	k7c2	z
bachoru	bachor	k1gInSc2	bachor
zabitých	zabitý	k2eAgMnPc2d1	zabitý
sobů	sob	k1gMnPc2	sob
a	a	k8xC	a
pižmoňů	pižmoň	k1gMnPc2	pižmoň
<g/>
.	.	kIx.	.
</s>
<s>
Oděv	oděv	k1gInSc1	oděv
zvaný	zvaný	k2eAgInSc1d1	zvaný
parka	parka	k6eAd1	parka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
teplý	teplý	k2eAgInSc1d1	teplý
kabátec	kabátec	k1gInSc1	kabátec
s	s	k7c7	s
kapucí	kapuce	k1gFnSc7	kapuce
<g/>
,	,	kIx,	,
šili	šít	k5eAaImAgMnP	šít
z	z	k7c2	z
kožešin	kožešina	k1gFnPc2	kožešina
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
ledních	lední	k2eAgMnPc2d1	lední
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
mladých	mladý	k2eAgMnPc2d1	mladý
tuleňů	tuleň	k1gMnPc2	tuleň
či	či	k8xC	či
pižmoňů	pižmoň	k1gMnPc2	pižmoň
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývala	bývat	k5eAaImAgFnS	bývat
parka	parka	k1gFnSc1	parka
sešitá	sešitý	k2eAgFnSc1d1	sešitá
z	z	k7c2	z
kalhotami	kalhoty	k1gFnPc7	kalhoty
do	do	k7c2	do
kombinézy	kombinéza	k1gFnSc2	kombinéza
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
vynálezem	vynález	k1gInSc7	vynález
byl	být	k5eAaImAgInS	být
anorak	anorak	k1gInSc1	anorak
nepromokavá	promokavý	k2eNgFnSc1d1	nepromokavá
bunda	bunda	k1gFnSc1	bunda
šitá	šitý	k2eAgFnSc1d1	šitá
ze	z	k7c2	z
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nosili	nosit	k5eAaImAgMnP	nosit
lovci	lovec	k1gMnPc1	lovec
do	do	k7c2	do
kajaku	kajak	k1gInSc2	kajak
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc4	nástroj
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
s	s	k7c7	s
dýmkami	dýmka	k1gFnPc7	dýmka
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
uměleckými	umělecký	k2eAgInPc7d1	umělecký
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
z	z	k7c2	z
mrožích	mroží	k2eAgInPc2d1	mroží
klů	kel	k1gInPc2	kel
nebo	nebo	k8xC	nebo
velrybích	velrybí	k2eAgFnPc2d1	velrybí
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
v	v	k7c6	v
domech	dům	k1gInPc6	dům
s	s	k7c7	s
elektřinou	elektřina	k1gFnSc7	elektřina
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
modernějšími	moderní	k2eAgInPc7d2	modernější
předměty	předmět	k1gInPc7	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
nové	nový	k2eAgFnSc2d1	nová
technické	technický	k2eAgFnSc2d1	technická
doby	doba	k1gFnSc2	doba
však	však	k9	však
nastala	nastat	k5eAaPmAgFnS	nastat
velmi	velmi	k6eAd1	velmi
problematická	problematický	k2eAgFnSc1d1	problematická
sociální	sociální	k2eAgFnSc1d1	sociální
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gInPc7	její
důsledky	důsledek	k1gInPc7	důsledek
jsou	být	k5eAaImIp3nP	být
zánik	zánik	k1gInSc4	zánik
původních	původní	k2eAgFnPc2d1	původní
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
<g/>
,	,	kIx,	,
negramotnost	negramotnost	k1gFnSc1	negramotnost
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stát	stát	k1gInSc1	stát
Kanada	Kanada	k1gFnSc1	Kanada
snaží	snažit	k5eAaImIp3nS	snažit
zvýšit	zvýšit	k5eAaPmF	zvýšit
jejich	jejich	k3xOp3gFnSc4	jejich
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
partnery	partner	k1gMnPc7	partner
byly	být	k5eAaImAgFnP	být
tradičně	tradičně	k6eAd1	tradičně
volné	volný	k2eAgFnPc1d1	volná
<g/>
,	,	kIx,	,
nevěra	nevěra	k1gFnSc1	nevěra
nepředstavovala	představovat	k5eNaImAgFnS	představovat
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Rodiny	rodina	k1gFnPc1	rodina
bývaly	bývat	k5eAaImAgFnP	bývat
často	často	k6eAd1	často
párové	párový	k2eAgFnPc1d1	párová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
se	se	k3xPyFc4	se
též	též	k9	též
polyandrie	polyandrie	k1gFnSc1	polyandrie
(	(	kIx(	(
<g/>
mnohomužství	mnohomužství	k1gNnSc1	mnohomužství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
se	se	k3xPyFc4	se
současní	současný	k2eAgMnPc1d1	současný
Inuité	Inuita	k1gMnPc1	Inuita
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
snaží	snažit	k5eAaImIp3nP	snažit
legalizovat	legalizovat	k5eAaBmF	legalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Inuité	Inuita	k1gMnPc1	Inuita
jsou	být	k5eAaImIp3nP	být
mírumilovní	mírumilovný	k2eAgMnPc1d1	mírumilovný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
většinou	většina	k1gFnSc7	většina
nevedli	vést	k5eNaImAgMnP	vést
války	válka	k1gFnSc2	válka
a	a	k8xC	a
neznali	neznat	k5eAaImAgMnP	neznat
pojem	pojem	k1gInSc4	pojem
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
uvnitř	uvnitř	k7c2	uvnitř
komunity	komunita	k1gFnSc2	komunita
se	se	k3xPyFc4	se
často	často	k6eAd1	často
řešily	řešit	k5eAaImAgFnP	řešit
formou	forma	k1gFnSc7	forma
řečnického	řečnický	k2eAgMnSc2d1	řečnický
či	či	k8xC	či
písňového	písňový	k2eAgInSc2d1	písňový
souboje	souboj	k1gInSc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
extrémně	extrémně	k6eAd1	extrémně
tvrdým	tvrdý	k2eAgFnPc3d1	tvrdá
podmínkám	podmínka	k1gFnPc3	podmínka
však	však	k9	však
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Evropanů	Evropan	k1gMnPc2	Evropan
velmi	velmi	k6eAd1	velmi
drsné	drsný	k2eAgNnSc1d1	drsné
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
infanticida	infanticida	k1gFnSc1	infanticida
(	(	kIx(	(
<g/>
usmrcování	usmrcování	k1gNnSc1	usmrcování
neduživých	duživý	k2eNgFnPc2d1	neduživá
nebo	nebo	k8xC	nebo
přebytečných	přebytečný	k2eAgFnPc2d1	přebytečná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
utopili	utopit	k5eAaPmAgMnP	utopit
nebo	nebo	k8xC	nebo
nechali	nechat	k5eAaPmAgMnP	nechat
umrznout	umrznout	k5eAaPmF	umrznout
<g/>
)	)	kIx)	)
a	a	k8xC	a
senicida	senicida	k1gFnSc1	senicida
(	(	kIx(	(
<g/>
usmrcování	usmrcování	k1gNnSc1	usmrcování
nebo	nebo	k8xC	nebo
nucené	nucený	k2eAgFnPc1d1	nucená
sebevraždy	sebevražda	k1gFnPc1	sebevražda
starých	starý	k2eAgInPc2d1	starý
a	a	k8xC	a
nevyléčitelně	vyléčitelně	k6eNd1	vyléčitelně
nemocných	nemocný	k2eAgMnPc2d1	nemocný
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eskymáci	Eskymák	k1gMnPc1	Eskymák
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
nazývají	nazývat	k5eAaImIp3nP	nazývat
Inuité	Inuitý	k2eAgInPc4d1	Inuitý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
lidské	lidský	k2eAgFnPc4d1	lidská
bytosti	bytost	k1gFnPc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Inuité	Inuitý	k2eAgNnSc1d1	Inuité
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příroda	příroda	k1gFnSc1	příroda
je	být	k5eAaImIp3nS	být
oživována	oživovat	k5eAaImNgFnS	oživovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
mocných	mocný	k2eAgMnPc2d1	mocný
duchů	duch	k1gMnPc2	duch
–	–	k?	–
Inuatů	Inuat	k1gInPc2	Inuat
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
mohou	moct	k5eAaImIp3nP	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
pouze	pouze	k6eAd1	pouze
šamani	šaman	k1gMnPc1	šaman
angákokové	angákokový	k2eAgFnSc2d1	angákokový
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
Inuaty	Inuat	k1gInPc4	Inuat
patří	patřit	k5eAaImIp3nS	patřit
Aninga	Aninga	k1gFnSc1	Aninga
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgMnSc1d1	mužský
duch	duch	k1gMnSc1	duch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sleduje	sledovat	k5eAaImIp3nS	sledovat
Zemi	zem	k1gFnSc4	zem
dírou	díra	k1gFnSc7	díra
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Lidem	člověk	k1gMnPc3	člověk
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
přátelsky	přátelsky	k6eAd1	přátelsky
nakloněn	nakloněn	k2eAgMnSc1d1	nakloněn
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
či	či	k8xC	či
neúspěch	neúspěch	k1gInSc1	neúspěch
rybolovu	rybolov	k1gInSc2	rybolov
a	a	k8xC	a
lovu	lov	k1gInSc2	lov
tuleňů	tuleň	k1gMnPc2	tuleň
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
vládkyni	vládkyně	k1gFnSc4	vládkyně
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Sedna	Sedna	k1gFnSc1	Sedna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Stařena	stařena	k1gFnSc1	stařena
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mýtu	mýtus	k1gInSc2	mýtus
byla	být	k5eAaImAgFnS	být
Sedna	Seden	k2eAgFnSc1d1	Sedna
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
unesl	unést	k5eAaPmAgMnS	unést
mořský	mořský	k2eAgMnSc1d1	mořský
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
osvobodit	osvobodit	k5eAaPmF	osvobodit
a	a	k8xC	a
odvést	odvést	k5eAaPmF	odvést
v	v	k7c6	v
kajaku	kajak	k1gInSc6	kajak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhněvaní	rozhněvaný	k2eAgMnPc1d1	rozhněvaný
ptáci	pták	k1gMnPc1	pták
způsobili	způsobit	k5eAaPmAgMnP	způsobit
bouři	bouře	k1gFnSc4	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Sednu	sednout	k5eAaPmIp1nS	sednout
utopit	utopit	k5eAaPmF	utopit
a	a	k8xC	a
uřízl	uříznout	k5eAaPmAgMnS	uříznout
jí	on	k3xPp3gFnSc3	on
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
zachytila	zachytit	k5eAaPmAgFnS	zachytit
kajaku	kajak	k1gInSc3	kajak
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
ruce	ruka	k1gFnPc4	ruka
bez	bez	k7c2	bez
prstů	prst	k1gInPc2	prst
podobné	podobný	k2eAgFnSc2d1	podobná
ploutvím	ploutev	k1gFnPc3	ploutev
tuleně	tuleň	k1gMnPc4	tuleň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
prstů	prst	k1gInPc2	prst
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
,	,	kIx,	,
mroži	mrož	k1gMnPc1	mrož
a	a	k8xC	a
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
lov	lov	k1gInSc1	lov
může	moct	k5eAaImIp3nS	moct
Sedna	Sedno	k1gNnPc4	Sedno
lidem	člověk	k1gMnPc3	člověk
umožnit	umožnit	k5eAaPmF	umožnit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
smutnému	smutný	k2eAgInSc3d1	smutný
osudu	osud	k1gInSc3	osud
je	být	k5eAaImIp3nS	být
Sedna	Sedna	k1gFnSc1	Sedna
vůči	vůči	k7c3	vůči
lidem	člověk	k1gMnPc3	člověk
nedůvěřivá	důvěřivý	k2eNgNnPc1d1	nedůvěřivé
až	až	k6eAd1	až
mstivá	mstivý	k2eAgNnPc1d1	mstivé
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
větrem	vítr	k1gInSc7	vítr
má	mít	k5eAaImIp3nS	mít
Velký	velký	k2eAgMnSc1d1	velký
duch	duch	k1gMnSc1	duch
Síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pánem	pán	k1gMnSc7	pán
pozemní	pozemní	k2eAgFnSc2d1	pozemní
zvěře	zvěř	k1gFnSc2	zvěř
(	(	kIx(	(
<g/>
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
pižmoňů	pižmoň	k1gMnPc2	pižmoň
<g/>
,	,	kIx,	,
kožešinové	kožešinový	k2eAgFnPc1d1	kožešinová
zvěře	zvěř	k1gFnPc1	zvěř
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Tekkeitsertok	Tekkeitsertok	k1gInSc4	Tekkeitsertok
<g/>
.	.	kIx.	.
</s>
<s>
Inuité	Inuita	k1gMnPc1	Inuita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
tvrdých	tvrdé	k1gNnPc6	tvrdé
a	a	k8xC	a
mrazivých	mrazivý	k2eAgFnPc6d1	mrazivá
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
být	být	k5eAaImF	být
s	s	k7c7	s
duchy	duch	k1gMnPc7	duch
zadobře	zadobře	k6eAd1	zadobře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
právě	právě	k9	právě
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
závisí	záviset	k5eAaImIp3nS	záviset
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
představ	představa	k1gFnPc2	představa
dobrý	dobrý	k2eAgInSc4d1	dobrý
lov	lov	k1gInSc4	lov
zvěře	zvěř	k1gFnSc2	zvěř
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
jejich	jejich	k3xOp3gNnSc4	jejich
samotné	samotný	k2eAgNnSc4d1	samotné
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Inuité	Inuita	k1gMnPc1	Inuita
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
źe	źe	k?	źe
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
uctívat	uctívat	k5eAaImF	uctívat
zlé	zlý	k2eAgMnPc4d1	zlý
duchy	duch	k1gMnPc4	duch
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
usmířit	usmířit	k5eAaPmF	usmířit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dobří	dobrý	k2eAgMnPc1d1	dobrý
duchové	duch	k1gMnPc1	duch
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
laskaví	laskavět	k5eAaImIp3nS	laskavět
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
je	on	k3xPp3gInPc4	on
udobřovat	udobřovat	k5eAaImF	udobřovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zlých	zlý	k2eAgMnPc2d1	zlý
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
nestvůr	nestvůra	k1gFnPc2	nestvůra
je	být	k5eAaImIp3nS	být
nejobávanější	obávaný	k2eAgMnSc1d3	nejobávanější
Tupilak	Tupilak	k1gMnSc1	Tupilak
<g/>
,	,	kIx,	,
šeredný	šeredný	k2eAgMnSc1d1	šeredný
nemrtvý	nemrtvý	k1gMnSc1	nemrtvý
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
šaman	šaman	k1gMnSc1	šaman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
zahubil	zahubit	k5eAaPmAgInS	zahubit
jiného	jiný	k2eAgMnSc4d1	jiný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Připomíná	připomínat	k5eAaImIp3nS	připomínat
křížence	kříženec	k1gMnSc4	kříženec
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
medvěda	medvěd	k1gMnSc4	medvěd
s	s	k7c7	s
tuleněm	tuleň	k1gMnSc7	tuleň
<g/>
.	.	kIx.	.
</s>
<s>
Inuitské	Inuitský	k2eAgInPc4d1	Inuitský
mýty	mýtus	k1gInPc4	mýtus
a	a	k8xC	a
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
zejména	zejména	k9	zejména
antropolog	antropolog	k1gMnSc1	antropolog
Knud	Knud	k1gMnSc1	Knud
Rasmussen	Rasmussen	k1gInSc4	Rasmussen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
drsné	drsný	k2eAgNnSc1d1	drsné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
zemitý	zemitý	k2eAgInSc4d1	zemitý
humor	humor	k1gInSc4	humor
a	a	k8xC	a
komické	komický	k2eAgFnPc4d1	komická
scény	scéna	k1gFnPc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
NANSEN	NANSEN	kA	NANSEN
<g/>
,	,	kIx,	,
Fridtjof	Fridtjof	k1gInSc1	Fridtjof
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Eskymáků	Eskymák	k1gMnPc2	Eskymák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
RASMUSSEN	RASMUSSEN	kA	RASMUSSEN
<g/>
,	,	kIx,	,
Knud	Knud	k1gInSc1	Knud
<g/>
.	.	kIx.	.
</s>
<s>
Grónské	grónský	k2eAgInPc1d1	grónský
mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Eskymácký	eskymácký	k2eAgInSc1d1	eskymácký
obrat	obrat	k1gInSc1	obrat
Eskymácká	eskymácký	k2eAgFnSc1d1	eskymácká
hudba	hudba	k1gFnSc1	hudba
Eskymácký	eskymácký	k2eAgInSc4d1	eskymácký
pes	pes	k1gMnSc1	pes
</s>
