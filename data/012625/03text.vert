<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
socialistické	socialistický	k2eAgNnSc1d1	socialistické
hnutí	hnutí	k1gNnSc1	hnutí
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
National	National	k1gMnSc1	National
Socialist	socialist	k1gMnSc1	socialist
Movement	Movement	k1gMnSc1	Movement
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neonacistická	neonacistický	k2eAgFnSc1d1	neonacistická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Robert	Robert	k1gMnSc1	Robert
Brannen	Brannen	k2eAgMnSc1d1	Brannen
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Americké	americký	k2eAgFnSc2d1	americká
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
zastřelil	zastřelit	k5eAaPmAgInS	zastřelit
šéfa	šéf	k1gMnSc4	šéf
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
pobočky	pobočka	k1gFnSc2	pobočka
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Jeffa	Jeff	k1gMnSc2	Jeff
Halla	Hall	k1gMnSc4	Hall
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
desetiletý	desetiletý	k2eAgMnSc1d1	desetiletý
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
