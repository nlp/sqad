<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1289	[number]	k4	1289
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sedmý	sedmý	k4xOgMnSc1	sedmý
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
polský	polský	k2eAgMnSc1d1	polský
(	(	kIx(	(
<g/>
1305	[number]	k4	1305
<g/>
–	–	k?	–
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
(	(	kIx(	(
<g/>
1301	[number]	k4	1301
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
