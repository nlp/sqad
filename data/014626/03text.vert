<s>
Vysílač	vysílač	k1gInSc1
Dyleň	Dyleň	k1gFnSc1
</s>
<s>
Vysílač	vysílač	k1gInSc1
Mariánské	mariánský	k2eAgFnSc2d1
Lázně	lázeň	k1gFnSc2
–	–	k?
Dyleň	Dyleň	k1gFnSc1
Vysílač	vysílač	k1gInSc1
DyleňZákladní	DyleňZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Kóta	kóta	k1gFnSc1
</s>
<s>
940	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Výstavba	výstavba	k1gFnSc1
</s>
<s>
70	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
Stát	stát	k5eAaImF,k5eAaPmF
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Cheb	Cheb	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
4,51	4,51	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
10,79	10,79	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Vysílač	vysílač	k1gInSc1
Dyleň	Dyleň	k1gFnSc1
</s>
<s>
Vysílač	vysílač	k1gInSc1
Dyleň	Dyleň	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Technické	technický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Výška	výška	k1gFnSc1
stavby	stavba	k1gFnSc2
</s>
<s>
35	#num#	k4
m	m	kA
Stav	stav	k1gInSc1
</s>
<s>
funkční	funkční	k2eAgFnPc1d1
Vysílané	vysílaný	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
DVB-T2	DVB-T2	k1gFnSc2
</s>
<s>
MUX	MUX	kA
24	#num#	k4
(	(	kIx(
<g/>
K	k	k7c3
<g/>
45	#num#	k4
<g/>
)	)	kIx)
Rádia	rádius	k1gInSc2
FM	FM	kA
</s>
<s>
Radio	radio	k1gNnSc1
Blaník	Blaník	k1gInSc1
(	(	kIx(
<g/>
92,5	92,5	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ČRo	ČRo	k1gMnSc1
Radiožurnál	radiožurnál	k1gInSc1
(	(	kIx(
<g/>
97,6	97,6	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ČRo	ČRo	k1gFnSc4
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
(	(	kIx(
<g/>
100,8	100,8	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysílač	vysílač	k1gInSc1
Dyleň	Dyleň	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
vrchu	vrch	k1gInSc6
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
940	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Svým	svůj	k3xOyFgNnSc7
vysíláním	vysílání	k1gNnSc7
pokrývá	pokrývat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
území	území	k1gNnSc4
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vysílač	vysílač	k1gInSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
výrazný	výrazný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
s	s	k7c7
dalekým	daleký	k2eAgInSc7d1
rozhledem	rozhled	k1gInSc7
do	do	k7c2
Bavorska	Bavorsko	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
na	na	k7c6
něm	on	k3xPp3gNnSc6
postavena	postaven	k2eAgFnSc1d1
obytná	obytný	k2eAgFnSc1d1
budova	budova	k1gFnSc1
pro	pro	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
<g/>
,	,	kIx,
pozorovací	pozorovací	k2eAgFnSc4d1
a	a	k8xC
odposlouchávací	odposlouchávací	k2eAgFnSc4d1
věž	věž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhal	probíhat	k5eAaImAgMnS
zde	zde	k6eAd1
monitoring	monitoring	k1gInSc4
rádiového	rádiový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
v	v	k7c6
téměř	téměř	k6eAd1
celém	celý	k2eAgNnSc6d1
Bavorsku	Bavorsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
věž	věž	k1gFnSc1
sloužící	sloužící	k1gFnSc1
ke	k	k7c3
stejným	stejný	k2eAgInPc3d1
účelům	účel	k1gInPc3
naopak	naopak	k6eAd1
amerických	americký	k2eAgFnPc2d1
výzvědných	výzvědný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
stojí	stát	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
v	v	k7c6
Německu	Německo	k1gNnSc6
na	na	k7c6
hoře	hora	k1gFnSc6
Schneeberg	Schneeberg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odposlouchávací	odposlouchávací	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
význam	význam	k1gInSc4
po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
bylo	být	k5eAaImAgNnS
její	její	k3xOp3gNnSc1
vybavení	vybavení	k1gNnSc1
demontováno	demontovat	k5eAaBmNgNnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
rekonstruována	rekonstruovat	k5eAaBmNgFnS
a	a	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
retranslační	retranslační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
vysílač	vysílač	k1gInSc1
televizních	televizní	k2eAgInPc2d1
a	a	k8xC
rozhlasových	rozhlasový	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
výhodné	výhodný	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
vysílače	vysílač	k1gMnSc2
lze	lze	k6eAd1
programy	program	k1gInPc4
zachytit	zachytit	k5eAaPmF
v	v	k7c6
dalekém	daleký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
nejen	nejen	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Falci	Falc	k1gFnSc6
a	a	k8xC
Horních	horní	k2eAgInPc6d1
Francích	Franky	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
i	i	k8xC
okolní	okolní	k2eAgInPc1d1
objekty	objekt	k1gInPc1
jsou	být	k5eAaImIp3nP
dosud	dosud	k6eAd1
obehnány	obehnat	k5eAaPmNgFnP
původním	původní	k2eAgNnSc7d1
oplocením	oplocení	k1gNnSc7
z	z	k7c2
ostnatého	ostnatý	k2eAgInSc2d1
drátu	drát	k1gInSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
nepřístupné	přístupný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vysílané	vysílaný	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
televizních	televizní	k2eAgInPc2d1
multiplexů	multiplex	k1gInPc2
šířených	šířený	k2eAgInPc2d1
z	z	k7c2
vrchu	vrch	k1gInSc2
Dyleň	Dyleň	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Multiplex	multiplex	k1gInSc1
</s>
<s>
Kanál	kanál	k1gInSc1
</s>
<s>
Kmitočet	kmitočet	k1gInSc1
</s>
<s>
Výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
ERP	ERP	kA
<g/>
)	)	kIx)
</s>
<s>
Polarizace	polarizace	k1gFnSc1
</s>
<s>
Spuštěno	spuštěn	k2eAgNnSc1d1
</s>
<s>
DVB-T2	DVB-T2	k4
</s>
<s>
Multiplex	multiplex	k1gInSc1
24	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
666	#num#	k4
MHz	Mhz	kA
</s>
<s>
50	#num#	k4
kW	kW	kA
</s>
<s>
horizontální	horizontální	k2eAgFnSc1d1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
Rozhlas	rozhlas	k1gInSc1
</s>
<s>
Přehled	přehled	k1gInSc1
rozhlasových	rozhlasový	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
vysílaných	vysílaný	k2eAgFnPc2d1
z	z	k7c2
vrchu	vrch	k1gInSc2
Dyleň	Dyleň	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stanice	stanice	k1gFnSc1
</s>
<s>
Kmitočet	kmitočet	k1gInSc1
</s>
<s>
Výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
ERP	ERP	kA
<g/>
)	)	kIx)
</s>
<s>
Polarizace	polarizace	k1gFnSc1
</s>
<s>
FM	FM	kA
</s>
<s>
Radio	radio	k1gNnSc1
Blaník	Blaník	k1gInSc1
</s>
<s>
92,5	92,5	k4
MHz	Mhz	kA
</s>
<s>
5	#num#	k4
kW	kW	kA
</s>
<s>
vertikální	vertikální	k2eAgFnSc1d1
</s>
<s>
FM	FM	kA
</s>
<s>
ČRo	ČRo	k?
Radiožurnál	radiožurnál	k1gInSc1
</s>
<s>
97,6	97,6	k4
MHz	Mhz	kA
</s>
<s>
1	#num#	k4
kW	kW	kA
</s>
<s>
vertikální	vertikální	k2eAgFnSc1d1
</s>
<s>
FM	FM	kA
</s>
<s>
ČRo	ČRo	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
100,8	100,8	k4
MHz	Mhz	kA
</s>
<s>
1	#num#	k4
kW	kW	kA
</s>
<s>
vertikální	vertikální	k2eAgFnSc1d1
</s>
<s>
Ukončené	ukončený	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
</s>
<s>
Digitální	digitální	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
DVB-T	DVB-T	k1gFnSc2
</s>
<s>
Vypínání	vypínání	k1gNnSc1
vysílání	vysílání	k1gNnSc2
probíhalo	probíhat	k5eAaImAgNnS
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Multiplex	multiplex	k1gInSc1
4	#num#	k4
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
a	a	k8xC
Přechodová	přechodový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
13	#num#	k4
byla	být	k5eAaImAgFnS
spuštěna	spuštěn	k2eAgFnSc1d1
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vypnuto	vypnut	k2eAgNnSc1d1
</s>
<s>
Multiplex	multiplex	k1gInSc1
</s>
<s>
Kanál	kanál	k1gInSc1
</s>
<s>
Kmitočet	kmitočet	k1gInSc1
</s>
<s>
Výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
ERP	ERP	kA
<g/>
)	)	kIx)
</s>
<s>
Polarizace	polarizace	k1gFnSc1
</s>
<s>
Spuštěno	spuštěn	k2eAgNnSc1d1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
DVB-T	DVB-T	k?
</s>
<s>
Multiplex	multiplex	k1gInSc1
4	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
666	#num#	k4
MHz	Mhz	kA
</s>
<s>
50	#num#	k4
kW	kW	kA
</s>
<s>
horizontální	horizontální	k2eAgFnSc1d1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
DVB-T2	DVB-T2	k4
</s>
<s>
Přechodová	přechodový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
13	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
530	#num#	k4
MHz	Mhz	kA
</s>
<s>
16	#num#	k4
kW	kW	kA
</s>
<s>
horizontální	horizontální	k2eAgFnSc1d1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GSMweb	GSMwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
gsmweb	gsmwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PARABOLA	parabola	k1gFnSc1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
FM	FM	kA
<g/>
/	/	kIx~
<g/>
VKV	VKV	kA
-	-	kIx~
CZ	CZ	kA
-	-	kIx~
Vysílače	vysílač	k1gMnSc2
<g/>
.	.	kIx.
www.parabola.cz	www.parabola.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Televizniweb	Televizniwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-18	2020-02-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
POTŮČEK	Potůček	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multiplex	multiplex	k1gInSc1
4	#num#	k4
dokončen	dokončit	k5eAaPmNgInS
<g/>
:	:	kIx,
spustil	spustit	k5eAaPmAgInS
vysílače	vysílač	k1gMnPc4
Frenštát	Frenštát	k1gInSc4
<g/>
,	,	kIx,
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
n.	n.	k?
<g/>
/	/	kIx~
<g/>
O.	O.	kA
a	a	k8xC
Sušice	Sušice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
V	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
Lázních	lázeň	k1gFnPc6
odstartoval	odstartovat	k5eAaPmAgMnS
DVB-T2	DVB-T2	k1gMnSc1
multiplex	multiplex	k1gInSc4
s	s	k7c7
kanálem	kanál	k1gInSc7
JOJ	jojo	k1gNnPc2
Family	Famil	k1gMnPc7
HD	HD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgInSc4d1
rádio	rádio	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-29	2018-03-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.vojensko.cz/stanoviste-dylen	http://www.vojensko.cz/stanoviste-dylen	k1gInSc1
</s>
<s>
http://www.cestanahoru.cz/dylen/vysilac/	http://www.cestanahoru.cz/dylen/vysilac/	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
České	český	k2eAgFnPc1d1
televizní	televizní	k2eAgFnPc1d1
a	a	k8xC
rozhlasové	rozhlasový	k2eAgInPc1d1
vysílače	vysílač	k1gInPc1
hlavní	hlavní	k2eAgInPc1d1
vysílače	vysílač	k1gInPc4
ČRa	ČRa	k1gMnPc2
</s>
<s>
Barvičova	barvičův	k2eAgNnPc1d1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Buková	bukový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Cukrák	Cukrák	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Černá	černý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Trutnov	Trutnov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Děvín	Děvín	k1gInSc1
(	(	kIx(
<g/>
Mikulov	Mikulov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hády	hádes	k1gInPc4
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Hladnov	Hladnov	k1gInSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Hošťálkovice	Hošťálkovice	k1gFnSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Javořice	Javořice	k1gFnSc1
(	(	kIx(
<g/>
Jihlava	Jihlava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jedlová	jedlový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Chomutov	Chomutov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Ještěd	Ještěd	k1gInSc1
(	(	kIx(
<g/>
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kleť	Kleť	k1gFnSc1
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Klínovec	Klínovec	k1gInSc1
(	(	kIx(
<g/>
Jáchymov	Jáchymov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kojál	Kojál	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Krásné	krásný	k2eAgInPc1d1
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Krašov	Krašov	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Lysá	Lysá	k1gFnSc1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Frýdek-Místek	Frýdek-Místek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Mařský	Mařský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
Vimperk	Vimperk	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Mezivrata	Mezivrata	k1gFnSc1
(	(	kIx(
<g/>
Votice	Votice	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Ploštiny	ploština	k1gFnSc2
(	(	kIx(
<g/>
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Praděd	praděd	k1gMnSc1
(	(	kIx(
<g/>
Jeseník	Jeseník	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Svatobor	Svatobor	k1gInSc1
(	(	kIx(
<g/>
Sušice	Sušice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tlustá	tlustý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Zlín	Zlín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vraní	vraní	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
Domažlice	Domažlice	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Zelená	zelený	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Cheb	Cheb	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
hlavní	hlavní	k2eAgInPc1d1
vysílače	vysílač	k1gInPc1
DB	db	kA
</s>
<s>
Andrlův	Andrlův	k2eAgInSc1d1
Chlum	chlum	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
)	)	kIx)
•	•	k?
Dyleň	Dyleň	k1gFnSc1
(	(	kIx(
<g/>
Cheb	Cheb	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hůrka	hůrka	k1gFnSc1
(	(	kIx(
<g/>
Opava	Opava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Chlum	chlum	k1gInSc1
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Ládví	Ládví	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Louštín	Louštín	k1gInSc1
(	(	kIx(
<g/>
Rakovník	Rakovník	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Panorama	panorama	k1gNnSc1
(	(	kIx(
<g/>
Trutnov	Trutnov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
ÚTB	ÚTB	kA
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Velký	velký	k2eAgInSc1d1
Javorník	Javorník	k1gInSc1
(	(	kIx(
<g/>
Frenštát	Frenštát	k1gInSc1
pod	pod	k7c7
Radhoštěm	Radhošť	k1gMnSc7
<g/>
)	)	kIx)
AM	AM	kA
vysílače	vysílač	k1gInSc2
</s>
<s>
Dobrochov	Dobrochov	k1gInSc1
(	(	kIx(
<g/>
Prostějov	Prostějov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Husova	Husův	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Liblice	Liblice	k1gFnPc4
B	B	kA
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Topolná	Topolný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
)	)	kIx)
ostatní	ostatní	k2eAgInPc1d1
vysílače	vysílač	k1gInPc1
</s>
<s>
Alexandrova	Alexandrův	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
(	(	kIx(
<g/>
Adamov	Adamov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Babí	babit	k5eAaImIp3nS
lom	lom	k1gInSc1
(	(	kIx(
<g/>
Hodonín	Hodonín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Barák	barák	k1gInSc1
(	(	kIx(
<g/>
Klatovy	Klatovy	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Bradlec	Bradlec	k1gInSc1
(	(	kIx(
<g/>
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Harusův	Harusův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
)	)	kIx)
•	•	k?
Hoděšovice	Hoděšovice	k1gFnSc1
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Jarník	jarník	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Písek	Písek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
Horka	horka	k1gFnSc1
(	(	kIx(
<g/>
Svitavy	Svitava	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Krkavec	krkavec	k1gMnSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kubánkov	Kubánkov	k1gInSc1
(	(	kIx(
<g/>
Frýdek-Místek	Frýdek-Místek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Litický	litický	k2eAgInSc4d1
Chlum	chlum	k1gInSc4
(	(	kIx(
<g/>
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
<g/>
)	)	kIx)
•	•	k?
Malý	Malý	k1gMnSc1
Javorový	javorový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Třinec	Třinec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Nehošť	Nehošť	k1gFnSc1
(	(	kIx(
<g/>
Kralupy	Kralupy	k1gInPc1
nad	nad	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
Vltavou	Vltava	k1gFnSc7
<g/>
)	)	kIx)
•	•	k?
Novotných	Novotných	k2eAgInSc4d1
vršek	vršek	k1gInSc4
(	(	kIx(
<g/>
Zdíkov	Zdíkov	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Petřín	Petřín	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Popovický	popovický	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Radeč	Radeč	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Radhošť	Radhošť	k1gInSc1
(	(	kIx(
<g/>
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Radimovice	Radimovice	k1gFnPc4
u	u	k7c2
Želče	Želč	k1gFnSc2
(	(	kIx(
<g/>
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Radíkov	Radíkov	k1gInSc1
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Strahov	Strahov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Strážný	strážný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
Třebíč	Třebíč	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
TKB	TKB	kA
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
U	u	k7c2
rozhledny	rozhledna	k1gFnSc2
(	(	kIx(
<g/>
Frýdlant	Frýdlant	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Veselský	veselský	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
)	)	kIx)
kategorie	kategorie	k1gFnSc2
</s>
