<s>
Programátorská	programátorský	k2eAgFnSc1d1	programátorská
chyba	chyba	k1gFnSc1	chyba
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
softwarové	softwarový	k2eAgFnSc2d1	softwarová
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
udělal	udělat	k5eAaPmAgMnS	udělat
programátor	programátor	k1gMnSc1	programátor
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Programátorskou	programátorský	k2eAgFnSc4d1	programátorská
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c4	v
software	software	k1gInSc4	software
způsobí	způsobit	k5eAaPmIp3nS	způsobit
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
využívající	využívající	k2eAgInSc1d1	využívající
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
je	být	k5eAaImIp3nS	být
exploit	exploit	k1gInSc1	exploit
<g/>
.	.	kIx.	.
</s>
<s>
Chyba	chyba	k1gFnSc1	chyba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
<g/>
,	,	kIx,	,
sémantická	sémantický	k2eAgFnSc1d1	sémantická
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
neočekávané	očekávaný	k2eNgFnSc2d1	neočekávaná
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
chyba	chyba	k1gFnSc1	chyba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
narušení	narušení	k1gNnSc6	narušení
syntaxe	syntax	k1gFnSc2	syntax
gramatiky	gramatika	k1gFnSc2	gramatika
použitého	použitý	k2eAgInSc2d1	použitý
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kompilovaných	kompilovaný	k2eAgInPc2d1	kompilovaný
programů	program	k1gInPc2	program
ji	on	k3xPp3gFnSc4	on
překladač	překladač	k1gInSc1	překladač
zahlásí	zahlásit	k5eAaPmIp3nS	zahlásit
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
během	během	k7c2	během
syntaktické	syntaktický	k2eAgFnSc2d1	syntaktická
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
přeloží	přeložit	k5eAaPmIp3nS	přeložit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedělá	dělat	k5eNaImIp3nS	dělat
co	co	k9	co
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
nekonečném	konečný	k2eNgInSc6d1	nekonečný
cyklu	cyklus	k1gInSc6	cyklus
<g/>
,	,	kIx,	,
spadne	spadnout	k5eAaPmIp3nS	spadnout
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
násilně	násilně	k6eAd1	násilně
ukončen	ukončit	k5eAaPmNgInS	ukončit
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
pro	pro	k7c4	pro
porušení	porušení	k1gNnPc4	porušení
přidělených	přidělený	k2eAgNnPc2d1	přidělené
práv	právo	k1gNnPc2	právo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vydá	vydat	k5eAaPmIp3nS	vydat
naprosto	naprosto	k6eAd1	naprosto
špatný	špatný	k2eAgInSc1d1	špatný
výsledek	výsledek	k1gInSc1	výsledek
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
ta	ten	k3xDgFnSc1	ten
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
možná	možný	k2eAgFnSc1d1	možná
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
složitějším	složitý	k2eAgInSc6d2	složitější
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
program	program	k1gInSc1	program
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
chybné	chybný	k2eAgFnSc2d1	chybná
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
nebo	nebo	k8xC	nebo
ukončí	ukončit	k5eAaPmIp3nS	ukončit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
specifickým	specifický	k2eAgNnSc7d1	specifické
chybovým	chybový	k2eAgNnSc7d1	chybové
hlášením	hlášení	k1gNnSc7	hlášení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běhu	běh	k1gInSc6	běh
programu	program	k1gInSc2	program
nastane	nastat	k5eAaPmIp3nS	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
programátor	programátor	k1gMnSc1	programátor
nepočítal	počítat	k5eNaImAgMnS	počítat
a	a	k8xC	a
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
neumí	umět	k5eNaImIp3nS	umět
program	program	k1gInSc4	program
správně	správně	k6eAd1	správně
zareagovat	zareagovat	k5eAaPmF	zareagovat
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
situace	situace	k1gFnPc1	situace
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
psát	psát	k5eAaImF	psát
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
disk	disk	k1gInSc1	disk
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
hodnota	hodnota	k1gFnSc1	hodnota
vstupu	vstup	k1gInSc2	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
způsobí	způsobit	k5eAaPmIp3nS	způsobit
chybu	chyba	k1gFnSc4	chyba
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
překlep	překlep	k1gInSc4	překlep
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
jméně	jméno	k1gNnSc6	jméno
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc2	použití
1	[number]	k4	1
místo	místo	k7c2	místo
0	[number]	k4	0
nebo	nebo	k8xC	nebo
<	<	kIx(	<
místo	místo	k1gNnSc1	místo
<	<	kIx(	<
<g/>
=	=	kIx~	=
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
chyby	chyba	k1gFnPc1	chyba
se	se	k3xPyFc4	se
zvlášť	zvlášť	k6eAd1	zvlášť
špatně	špatně	k6eAd1	špatně
hledají	hledat	k5eAaImIp3nP	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
chyb	chyba	k1gFnPc2	chyba
je	být	k5eAaImIp3nS	být
chybné	chybný	k2eAgNnSc4d1	chybné
nebo	nebo	k8xC	nebo
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
použití	použití	k1gNnSc4	použití
synchronizačních	synchronizační	k2eAgInPc2d1	synchronizační
primitiv	primitiv	k1gMnSc1	primitiv
při	při	k7c6	při
přístupu	přístup	k1gInSc6	přístup
ke	k	k7c3	k
sdíleným	sdílený	k2eAgInPc3d1	sdílený
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
chyba	chyba	k1gFnSc1	chyba
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
programu	program	k1gInSc6	program
vydržet	vydržet	k5eAaPmF	vydržet
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
projevit	projevit	k5eAaPmF	projevit
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
specifickém	specifický	k2eAgNnSc6d1	specifické
pořadí	pořadí	k1gNnSc6	pořadí
naplánování	naplánování	k1gNnSc2	naplánování
vláken	vlákno	k1gNnPc2	vlákno
na	na	k7c6	na
procesoru	procesor	k1gInSc6	procesor
nebo	nebo	k8xC	nebo
procesorech	procesor	k1gInPc6	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Podmnožinou	podmnožina	k1gFnSc7	podmnožina
těchto	tento	k3xDgFnPc2	tento
chyb	chyba	k1gFnPc2	chyba
je	být	k5eAaImIp3nS	být
deadlock	deadlock	k6eAd1	deadlock
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
programátorských	programátorský	k2eAgFnPc2d1	programátorská
chyb	chyba	k1gFnPc2	chyba
<g/>
:	:	kIx,	:
opomenutí	opomenutí	k1gNnSc1	opomenutí
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
,	,	kIx,	,
logická	logický	k2eAgFnSc1d1	logická
chyba	chyba	k1gFnSc1	chyba
<g/>
,	,	kIx,	,
překlep	překlep	k1gInSc1	překlep
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
chyby	chyba	k1gFnPc4	chyba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
náš	náš	k3xOp1gInSc1	náš
program	program	k1gInSc1	program
začne	začít	k5eAaPmIp3nS	začít
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
nedůvěryhodné	důvěryhodný	k2eNgInPc4d1	nedůvěryhodný
vstupy	vstup	k1gInPc4	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vstupy	vstup	k1gInPc4	vstup
důvěryhodné	důvěryhodný	k2eAgInPc4d1	důvěryhodný
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
si	se	k3xPyFc3	se
připravujeme	připravovat	k5eAaImIp1nP	připravovat
my	my	k3xPp1nPc1	my
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
menší	malý	k2eAgInSc4d2	menší
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdybychom	kdyby	kYmCp1nP	kdyby
dostávali	dostávat	k5eAaImAgMnP	dostávat
vstupy	vstup	k1gInPc4	vstup
z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
a	a	k8xC	a
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
nám	my	k3xPp1nPc3	my
mohl	moct	k5eAaImAgMnS	moct
cokoliv	cokoliv	k3yInSc4	cokoliv
podvrhnout	podvrhnout	k5eAaPmF	podvrhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
veliký	veliký	k2eAgInSc4d1	veliký
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
programů	program	k1gInPc2	program
přijímá	přijímat	k5eAaImIp3nS	přijímat
výhradně	výhradně	k6eAd1	výhradně
nedůvěryhodné	důvěryhodný	k2eNgInPc4d1	nedůvěryhodný
vstupy	vstup	k1gInPc4	vstup
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc7	jejich
zdroji	zdroj	k1gInPc7	zdroj
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
internet	internet	k1gInSc1	internet
a	a	k8xC	a
programy	program	k1gInPc1	program
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
obdrží	obdržet	k5eAaPmIp3nS	obdržet
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
v	v	k7c6	v
protokolu	protokol	k1gInSc6	protokol
SMB	SMB	kA	SMB
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
neděje	dít	k5eNaImIp3nS	dít
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokud	pokud	k8xS	pokud
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
dva	dva	k4xCgInPc4	dva
počítače	počítač	k1gInPc4	počítač
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
prolomit	prolomit	k5eAaPmF	prolomit
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
můžeme	moct	k5eAaImIp1nP	moct
počítači	počítač	k1gInSc3	počítač
něco	něco	k3yInSc4	něco
podvrhnout	podvrhnout	k5eAaPmF	podvrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Data	datum	k1gNnSc2	datum
přicházející	přicházející	k2eAgNnSc4d1	přicházející
z	z	k7c2	z
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Internetu	Internet	k1gInSc2	Internet
<g/>
,	,	kIx,	,
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
nebo	nebo	k8xC	nebo
e-mailový	eailový	k2eAgMnSc1d1	e-mailový
klient	klient	k1gMnSc1	klient
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
složitých	složitý	k2eAgFnPc2d1	složitá
implementací	implementace	k1gFnPc2	implementace
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
volání	volání	k1gNnSc4	volání
procedur	procedura	k1gFnPc2	procedura
RPC	RPC	kA	RPC
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
napíšu	napsat	k5eAaBmIp1nS	napsat
proceduru	procedura	k1gFnSc4	procedura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
čte	číst	k5eAaImIp3nS	číst
data	datum	k1gNnPc4	datum
z	z	k7c2	z
disku	disk	k1gInSc2	disk
a	a	k8xC	a
následně	následně	k6eAd1	následně
ji	on	k3xPp3gFnSc4	on
přizpůsobím	přizpůsobit	k5eAaPmIp1nS	přizpůsobit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
přijímat	přijímat	k5eAaImF	přijímat
požadavky	požadavek	k1gInPc4	požadavek
ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
dostávám	dostávat	k5eAaImIp1nS	dostávat
nově	nově	k6eAd1	nově
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
síťový	síťový	k2eAgInSc1d1	síťový
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
a	a	k8xC	a
primitivní	primitivní	k2eAgInSc1d1	primitivní
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zle	zle	k6eAd1	zle
využít	využít	k5eAaPmF	využít
sdílení	sdílení	k1gNnSc4	sdílení
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
SMB	SMB	kA	SMB
nebo	nebo	k8xC	nebo
NFS	NFS	kA	NFS
<g/>
.	.	kIx.	.
</s>
<s>
Síťové	síťový	k2eAgInPc4d1	síťový
servery	server	k1gInPc4	server
Apache	Apache	k1gNnSc2	Apache
a	a	k8xC	a
PHP	PHP	kA	PHP
jsou	být	k5eAaImIp3nP	být
bezpečné	bezpečný	k2eAgInPc1d1	bezpečný
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
toho	ten	k3xDgNnSc2	ten
co	co	k9	co
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
jenomže	jenomže	k8xC	jenomže
když	když	k8xS	když
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
píší	psát	k5eAaImIp3nP	psát
své	svůj	k3xOyFgInPc4	svůj
kódy	kód	k1gInPc4	kód
nevzdělaní	vzdělaný	k2eNgMnPc1d1	nevzdělaný
programátoři	programátor	k1gMnPc1	programátor
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
vše	všechen	k3xTgNnSc1	všechen
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
funkcí	funkce	k1gFnPc2	funkce
vzniká	vznikat	k5eAaImIp3nS	vznikat
chyba	chyba	k6eAd1	chyba
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
programátor	programátor	k1gMnSc1	programátor
si	se	k3xPyFc3	se
něco	něco	k3yInSc1	něco
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
doporučení	doporučení	k1gNnSc4	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Funkce	funkce	k1gFnSc1	funkce
strcpy	strcpa	k1gFnSc2	strcpa
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
systematickou	systematický	k2eAgFnSc4d1	systematická
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
co	co	k3yInSc4	co
dostane	dostat	k5eAaPmIp3nS	dostat
v	v	k7c6	v
parametru	parametr	k1gInSc6	parametr
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
někam	někam	k6eAd1	někam
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
žádné	žádný	k3yNgFnPc4	žádný
omezení	omezení	k1gNnSc1	omezení
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
přemazání	přemazání	k1gNnSc3	přemazání
námi	my	k3xPp1nPc7	my
neznámého	známý	k2eNgInSc2d1	neznámý
obsahu	obsah	k1gInSc2	obsah
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analýzách	analýza	k1gFnPc6	analýza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
veřejně	veřejně	k6eAd1	veřejně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
o	o	k7c4	o
možných	možný	k2eAgInPc2d1	možný
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
chybách	chyba	k1gFnPc6	chyba
na	na	k7c6	na
1000	[number]	k4	1000
řádcích	řádek	k1gInPc6	řádek
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
Linuxu	linux	k1gInSc2	linux
je	být	k5eAaImIp3nS	být
0,5	[number]	k4	0,5
chyby	chyba	k1gFnPc4	chyba
na	na	k7c4	na
1000	[number]	k4	1000
řádků	řádek	k1gInPc2	řádek
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
spoustě	spousta	k1gFnSc6	spousta
univerzitách	univerzita	k1gFnPc6	univerzita
vyučující	vyučující	k2eAgMnSc1d1	vyučující
v	v	k7c6	v
předmětech	předmět	k1gInPc6	předmět
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
problematikou	problematika	k1gFnSc7	problematika
zabývali	zabývat	k5eAaImAgMnP	zabývat
na	na	k7c4	na
cvičeních	cvičení	k1gNnPc6	cvičení
hledáním	hledání	k1gNnSc7	hledání
systémových	systémový	k2eAgFnPc2d1	systémová
chyb	chyba	k1gFnPc2	chyba
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
automatický	automatický	k2eAgInSc1d1	automatický
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tyto	tento	k3xDgFnPc4	tento
chyby	chyba	k1gFnPc1	chyba
v	v	k7c6	v
kódu	kód	k1gInSc6	kód
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
třeba	třeba	k6eAd1	třeba
ve	v	k7c6	v
Windows	Windows	kA	Windows
není	být	k5eNaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
statistika	statistika	k1gFnSc1	statistika
nijak	nijak	k6eAd1	nijak
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Microsoft	Microsoft	kA	Microsoft
nikdy	nikdy	k6eAd1	nikdy
nedal	dát	k5eNaPmAgInS	dát
své	svůj	k3xOyFgInPc4	svůj
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Prohlížeče	prohlížeč	k1gInPc1	prohlížeč
nebo	nebo	k8xC	nebo
editory	editor	k1gInPc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
software	software	k1gInSc1	software
méně	málo	k6eAd2	málo
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
více	hodně	k6eAd2	hodně
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
pád	pád	k1gInSc1	pád
aplikace	aplikace	k1gFnSc2	aplikace
je	být	k5eAaImIp3nS	být
hrozba	hrozba	k1gFnSc1	hrozba
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
potenciálně	potenciálně	k6eAd1	potenciálně
zneužitelná	zneužitelný	k2eAgFnSc1d1	zneužitelná
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
aplikace	aplikace	k1gFnSc1	aplikace
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
kdy	kdy	k6eAd1	kdy
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
spadne	spadnout	k5eAaPmIp3nS	spadnout
<g/>
"	"	kIx"	"
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
potenciálního	potenciální	k2eAgNnSc2d1	potenciální
zneužití	zneužití	k1gNnSc2	zneužití
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
nastat	nastat	k5eAaPmF	nastat
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
pád	pád	k1gInSc1	pád
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
většinou	většinou	k6eAd1	většinou
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
způsobem	způsob	k1gInSc7	způsob
zneužít	zneužít	k5eAaPmF	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kdy	kdy	k6eAd1	kdy
nemáme	mít	k5eNaImIp1nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
testovaní	testovaný	k2eAgMnPc1d1	testovaný
pádů	pád	k1gInPc2	pád
aplikace	aplikace	k1gFnPc4	aplikace
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
způsob	způsob	k1gInSc4	způsob
jak	jak	k8xS	jak
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Programátorská	programátorský	k2eAgFnSc1d1	programátorská
chyba	chyba	k1gFnSc1	chyba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
označuje	označovat	k5eAaImIp3nS	označovat
anglickým	anglický	k2eAgInSc7d1	anglický
výrazem	výraz	k1gInSc7	výraz
bug	bug	k?	bug
a	a	k8xC	a
proces	proces	k1gInSc1	proces
jejího	její	k3xOp3gNnSc2	její
odstraňování	odstraňování	k1gNnSc2	odstraňování
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
debugování	debugování	k1gNnSc2	debugování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bug	Bug	k?	Bug
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
moucha	moucha	k1gFnSc1	moucha
<g/>
,	,	kIx,	,
štěnice	štěnice	k1gFnSc1	štěnice
nebo	nebo	k8xC	nebo
obecně	obecně	k6eAd1	obecně
brouk	brouk	k1gMnSc1	brouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
chyba	chyba	k1gFnSc1	chyba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
konstruktérská	konstruktérský	k2eAgFnSc1d1	konstruktérská
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
už	už	k6eAd1	už
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
–	–	k?	–
použil	použít	k5eAaPmAgInS	použít
ho	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
Thomas	Thomas	k1gMnSc1	Thomas
Edison	Edison	k1gMnSc1	Edison
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
když	když	k8xS	když
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
vynálezech	vynález	k1gInPc6	vynález
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počítači	počítač	k1gInPc7	počítač
pak	pak	k6eAd1	pak
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
původem	původ	k1gInSc7	původ
tohoto	tento	k3xDgInSc2	tento
významu	význam	k1gInSc2	význam
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
způsobený	způsobený	k2eAgInSc1d1	způsobený
skutečným	skutečný	k2eAgInSc7d1	skutečný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
historka	historka	k1gFnSc1	historka
o	o	k7c6	o
molu	molo	k1gNnSc6	molo
zachyceném	zachycený	k2eAgNnSc6d1	zachycené
na	na	k7c6	na
relé	relé	k1gNnSc6	relé
počítače	počítač	k1gInSc2	počítač
Mark	Mark	k1gMnSc1	Mark
II	II	kA	II
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Mol	mol	k1gMnSc1	mol
byl	být	k5eAaImAgMnS	být
pečlivě	pečlivě	k6eAd1	pečlivě
vyproštěn	vyprostit	k5eAaPmNgMnS	vyprostit
a	a	k8xC	a
nalepen	nalepit	k5eAaPmNgMnS	nalepit
do	do	k7c2	do
záznamu	záznam	k1gInSc2	záznam
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
"	"	kIx"	"
<g/>
první	první	k4xOgInSc4	první
skutečný	skutečný	k2eAgInSc4d1	skutečný
případ	případ	k1gInSc4	případ
nalezeného	nalezený	k2eAgInSc2d1	nalezený
bugu	bugus	k1gInSc2	bugus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
nejen	nejen	k6eAd1	nejen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
první	první	k4xOgInSc4	první
výskyt	výskyt	k1gInSc4	výskyt
termínu	termín	k1gInSc2	termín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
operátoři	operátor	k1gMnPc1	operátor
o	o	k7c6	o
konstruktérském	konstruktérský	k2eAgNnSc6d1	konstruktérské
použití	použití	k1gNnSc6	použití
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
chyb	chyba	k1gFnPc2	chyba
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
na	na	k7c4	na
následek	následek	k1gInSc4	následek
dominový	dominový	k2eAgInSc1d1	dominový
efekt	efekt	k1gInSc1	efekt
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
následky	následek	k1gInPc7	následek
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
chyby	chyba	k1gFnPc1	chyba
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
funkčnost	funkčnost	k1gFnSc4	funkčnost
minimální	minimální	k2eAgInSc4d1	minimální
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
neobjeveny	objeven	k2eNgFnPc1d1	neobjevena
<g/>
.	.	kIx.	.
</s>
<s>
Vážnější	vážní	k2eAgFnPc1d2	vážnější
chyby	chyba	k1gFnPc1	chyba
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
zamrznutí	zamrznutí	k1gNnSc3	zamrznutí
programu	program	k1gInSc2	program
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
ztratě	ztrata	k1gFnSc6	ztrata
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
nejvážnější	vážní	k2eAgFnPc1d3	nejvážnější
chyby	chyba	k1gFnPc1	chyba
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
neoprávněnému	oprávněný	k2eNgInSc3d1	neoprávněný
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
kosmické	kosmický	k2eAgFnSc6d1	kosmická
agentuře	agentura	k1gFnSc6	agentura
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
nosná	nosný	k2eAgFnSc1d1	nosná
raketa	raketa	k1gFnSc1	raketa
Ariane	Arian	k1gMnSc5	Arian
5	[number]	k4	5
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nesla	nést	k5eAaImAgFnS	nést
družice	družice	k1gFnPc4	družice
Cluster	cluster	k1gInSc4	cluster
za	za	k7c2	za
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
minutu	minuta	k1gFnSc4	minuta
po	po	k7c6	po
startu	start	k1gInSc6	start
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
v	v	k7c6	v
programu	program	k1gInSc6	program
řídícího	řídící	k2eAgInSc2d1	řídící
počítače	počítač	k1gInSc2	počítač
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
zadané	zadaný	k2eAgFnSc6d1	zadaná
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
závěr	závěr	k1gInSc1	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
softwarové	softwarový	k2eAgFnPc1d1	softwarová
chyby	chyba	k1gFnPc1	chyba
připravily	připravit	k5eAaPmAgFnP	připravit
americkou	americký	k2eAgFnSc4d1	americká
ekonomiku	ekonomika	k1gFnSc4	ekonomika
o	o	k7c4	o
cca	cca	kA	cca
59	[number]	k4	59
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
0,6	[number]	k4	0,6
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Chyby	chyba	k1gFnPc1	chyba
v	v	k7c6	v
programech	program	k1gInPc6	program
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
lidského	lidský	k2eAgInSc2d1	lidský
faktoru	faktor	k1gInSc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
přehlédnutím	přehlédnutí	k1gNnSc7	přehlédnutí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
nepochopením	nepochopení	k1gNnSc7	nepochopení
ve	v	k7c6	v
vývojovém	vývojový	k2eAgInSc6d1	vývojový
týmu	tým	k1gInSc6	tým
během	během	k7c2	během
specifikace	specifikace	k1gFnSc2	specifikace
kódování	kódování	k1gNnSc2	kódování
a	a	k8xC	a
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Při	při	k7c6	při
vytváření	vytváření	k1gNnSc3	vytváření
relativně	relativně	k6eAd1	relativně
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
programu	program	k1gInSc2	program
na	na	k7c6	na
řazení	řazení	k1gNnSc6	řazení
slov	slovo	k1gNnPc2	slovo
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
nachází	nacházet	k5eAaImIp3nS	nacházet
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
kódování	kódování	k1gNnPc2	kódování
do	do	k7c2	do
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
off-by-one	offyn	k1gInSc5	off-by-on
chyba	chyba	k1gFnSc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
při	při	k7c6	při
kódování	kódování	k1gNnSc6	kódování
výpisu	výpis	k1gInSc2	výpis
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
'	'	kIx"	'
<g/>
<	<	kIx(	<
<g/>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
<g/>
>	>	kIx)	>
<g/>
'	'	kIx"	'
a	a	k8xC	a
následně	následně	k6eAd1	následně
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vypsání	vypsání	k1gNnSc3	vypsání
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
abecedním	abecední	k2eAgNnSc6d1	abecední
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Složitější	složitý	k2eAgFnPc1d2	složitější
chyby	chyba	k1gFnPc1	chyba
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
programu	program	k1gInSc6	program
pracuje	pracovat	k5eAaImIp3nS	pracovat
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
horším	zlý	k2eAgInSc6d2	horší
případě	případ	k1gInSc6	případ
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
během	během	k7c2	během
delšího	dlouhý	k2eAgNnSc2d2	delší
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
mezi	mezi	k7c7	mezi
částmi	část	k1gFnPc7	část
programů	program	k1gInPc2	program
vznikat	vznikat	k5eAaImF	vznikat
nežádoucí	žádoucí	k2eNgFnPc4d1	nežádoucí
interakce	interakce	k1gFnPc4	interakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
nalézt	nalézt	k5eAaPmF	nalézt
<g/>
,	,	kIx,	,
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
předejít	předejít	k5eAaPmF	předejít
této	tento	k3xDgFnSc3	tento
situaci	situace	k1gFnSc3	situace
vznikají	vznikat	k5eAaImIp3nP	vznikat
podrobné	podrobný	k2eAgFnPc1d1	podrobná
dokumentace	dokumentace	k1gFnPc1	dokumentace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
i	i	k8xC	i
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
kategorie	kategorie	k1gFnSc1	kategorie
chyb	chyba	k1gFnPc2	chyba
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
vláknům	vlákno	k1gNnPc3	vlákno
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
zpracovávaný	zpracovávaný	k2eAgInSc1d1	zpracovávaný
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednom	jeden	k4xCgInSc6	jeden
vlákně	vlákno	k1gNnSc6	vlákno
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgNnPc1	ten
nejsou	být	k5eNaImIp3nP	být
správně	správně	k6eAd1	správně
synchronizována	synchronizován	k2eAgNnPc1d1	synchronizováno
<g/>
.	.	kIx.	.
</s>
<s>
Najít	najít	k5eAaPmF	najít
a	a	k8xC	a
opravit	opravit	k5eAaPmF	opravit
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
debugování	debugování	k1gNnSc1	debugování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
složitostí	složitost	k1gFnSc7	složitost
programu	program	k1gInSc2	program
stoupá	stoupat	k5eAaImIp3nS	stoupat
počet	počet	k1gInSc1	počet
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
také	také	k9	také
obtížnost	obtížnost	k1gFnSc1	obtížnost
je	být	k5eAaImIp3nS	být
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
opravit	opravit	k5eAaPmF	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
programátoři	programátor	k1gMnPc1	programátor
stráví	strávit	k5eAaPmIp3nP	strávit
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
hledáním	hledání	k1gNnSc7	hledání
a	a	k8xC	a
opravováním	opravování	k1gNnSc7	opravování
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
než	než	k8xS	než
psaním	psaní	k1gNnSc7	psaní
nového	nový	k2eAgInSc2d1	nový
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Softwaroví	softwarový	k2eAgMnPc1d1	softwarový
testeři	tester	k1gMnPc1	tester
jsou	být	k5eAaImIp3nP	být
profesionálové	profesionál	k1gMnPc1	profesionál
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
jediným	jediný	k2eAgInSc7d1	jediný
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
opravit	opravit	k5eAaPmF	opravit
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
napsat	napsat	k5eAaBmF	napsat
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
projektů	projekt	k1gInPc2	projekt
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
prostředků	prostředek	k1gInPc2	prostředek
vynaloženo	vynaložen	k2eAgNnSc4d1	vynaloženo
na	na	k7c6	na
testování	testování	k1gNnSc6	testování
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
část	část	k1gFnSc4	část
ladění	ladění	k1gNnSc2	ladění
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
chybu	chyba	k1gFnSc4	chyba
v	v	k7c6	v
kódu	kód	k1gInSc6	kód
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
oprava	oprava	k1gFnSc1	oprava
nebývá	bývat	k5eNaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pomůcek	pomůcka	k1gFnPc2	pomůcka
při	při	k7c6	při
debugování	debugování	k1gNnSc6	debugování
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
krokování	krokování	k1gNnSc1	krokování
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
procházen	procházen	k2eAgInSc1d1	procházen
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
příkazech	příkaz	k1gInPc6	příkaz
při	při	k7c6	při
neustálém	neustálý	k2eAgNnSc6d1	neustálé
sledování	sledování	k1gNnSc6	sledování
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
této	tento	k3xDgFnSc2	tento
možnosti	možnost	k1gFnSc2	možnost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jiný	jiný	k2eAgInSc1d1	jiný
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
programu	program	k1gInSc2	program
jsou	být	k5eAaImIp3nP	být
proměnné	proměnná	k1gFnPc4	proměnná
vypisovány	vypisován	k2eAgFnPc4d1	vypisována
např.	např.	kA	např.
do	do	k7c2	do
konzole	konzola	k1gFnSc6	konzola
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
k	k	k7c3	k
lokalizaci	lokalizace	k1gFnSc3	lokalizace
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
těmito	tento	k3xDgFnPc7	tento
pomůckami	pomůcka	k1gFnPc7	pomůcka
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
lokalizace	lokalizace	k1gFnSc1	lokalizace
chyb	chyba	k1gFnPc2	chyba
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
z	z	k7c2	z
pravidla	pravidlo	k1gNnSc2	pravidlo
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chyby	chyba	k1gFnPc1	chyba
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
programu	program	k1gInSc2	program
způsobí	způsobit	k5eAaPmIp3nS	způsobit
pád	pád	k1gInSc4	pád
v	v	k7c6	v
úplně	úplně	k6eAd1	úplně
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
chyby	chyba	k1gFnPc1	chyba
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgFnP	způsobit
špatným	špatný	k2eAgNnSc7d1	špatné
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
plánováním	plánování	k1gNnSc7	plánování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
programátora	programátor	k1gMnSc2	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
chyby	chyba	k1gFnPc1	chyba
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
přepsání	přepsání	k1gNnSc4	přepsání
části	část	k1gFnSc2	část
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
incident	incident	k1gInSc4	incident
s	s	k7c7	s
Ariane	Arian	k1gMnSc5	Arian
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
kladeno	klást	k5eAaImNgNnS	klást
značné	značný	k2eAgNnSc1d1	značné
úsilí	úsilí	k1gNnSc1	úsilí
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
pomůcek	pomůcka	k1gFnPc2	pomůcka
pro	pro	k7c4	pro
automatické	automatický	k2eAgNnSc4d1	automatické
ladění	ladění	k1gNnSc4	ladění
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
např.	např.	kA	např.
metody	metoda	k1gFnSc2	metoda
statické	statický	k2eAgFnSc2d1	statická
analýzy	analýza	k1gFnSc2	analýza
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
typem	typ	k1gInSc7	typ
chyb	chyba	k1gFnPc2	chyba
jsou	být	k5eAaImIp3nP	být
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
programátor	programátor	k1gMnSc1	programátor
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c6	na
dokumentaci	dokumentace	k1gFnSc6	dokumentace
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
není	být	k5eNaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
byt	byt	k1gInSc1	byt
program	program	k1gInSc1	program
napsaný	napsaný	k2eAgInSc1d1	napsaný
dobře	dobře	k6eAd1	dobře
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
dokumentaci	dokumentace	k1gFnSc6	dokumentace
ale	ale	k8xC	ale
už	už	k6eAd1	už
ne	ne	k9	ne
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
skutečný	skutečný	k2eAgInSc4d1	skutečný
hardware	hardware	k1gInSc4	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Debugger	Debugger	k1gMnSc1	Debugger
Extrémní	extrémní	k2eAgNnSc1d1	extrémní
programování	programování	k1gNnSc1	programování
Objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
programování	programování	k1gNnSc4	programování
Paralelní	paralelní	k2eAgNnSc1d1	paralelní
programování	programování	k1gNnSc1	programování
Bug	Bug	k1gMnPc2	Bug
tracking	tracking	k1gInSc4	tracking
system	syst	k1gInSc7	syst
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
programátorská	programátorský	k2eAgFnSc1d1	programátorská
chyba	chyba	k1gFnSc1	chyba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc1	seznam
25	[number]	k4	25
nejnebezpečnějších	bezpečný	k2eNgFnPc2d3	nejnebezpečnější
programátorských	programátorský	k2eAgFnPc2d1	programátorská
chyb	chyba	k1gFnPc2	chyba
podle	podle	k7c2	podle
SANS	SANS	kA	SANS
a	a	k8xC	a
MITRE	MITRE	kA	MITRE
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
