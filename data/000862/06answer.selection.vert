<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
izotopů	izotop	k1gInPc2	izotop
lawrencia	lawrencius	k1gMnSc2	lawrencius
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstabilnější	stabilní	k2eAgFnSc1d3	nejstabilnější
je	být	k5eAaImIp3nS	být
262	[number]	k4	262
<g/>
Lr	Lr	k1gFnPc2	Lr
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
216	[number]	k4	216
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
