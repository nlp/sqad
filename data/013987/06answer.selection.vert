<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
s	s	k7c7
hlavní	hlavní	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
na	na	k7c6
letišti	letiště	k1gNnSc6
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Navazují	navazovat	k5eAaImIp3nP
na	na	k7c4
tradici	tradice	k1gFnSc4
<g/>
,	,	kIx,
identitu	identita	k1gFnSc4
a	a	k8xC
původně	původně	k6eAd1
i	i	k9
značku	značka	k1gFnSc4
letecké	letecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
1992	#num#	k4
transformována	transformovat	k5eAaBmNgFnS
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
a	a	k8xC
se	s	k7c7
zánikem	zánik	k1gInSc7
ČSFR	ČSFR	kA
majetkově	majetkově	k6eAd1
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
a	a	k8xC
slovenskou	slovenský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Slov-Air	Slov-Aira	k1gFnPc2
<g/>
.	.	kIx.
</s>