<p>
<s>
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gMnSc1	Courbet
<g/>
,	,	kIx,	,
plným	plný	k2eAgNnSc7d1	plné
jménem	jméno	k1gNnSc7	jméno
Jean	Jean	k1gMnSc1	Jean
Désiré	Désirý	k2eAgFnPc1d1	Désirý
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gMnSc1	Courbet
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1819	[number]	k4	1819
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
postavu	postava	k1gFnSc4	postava
francouzského	francouzský	k2eAgInSc2d1	francouzský
malířského	malířský	k2eAgInSc2d1	malířský
realismu	realismus	k1gInSc2	realismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gMnSc1	Courbet
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Besançonu	Besançon	k1gInSc2	Besançon
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
dobře	dobře	k6eAd1	dobře
situovaní	situovaný	k2eAgMnPc1d1	situovaný
statkáři	statkář	k1gMnPc1	statkář
<g/>
,	,	kIx,	,
po	po	k7c6	po
Gustavovi	Gustav	k1gMnSc6	Gustav
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodily	narodit	k5eAaPmAgFnP	narodit
ještě	ještě	k9	ještě
čtyři	čtyři	k4xCgFnPc1	čtyři
dcery	dcera	k1gFnPc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
zemřela	zemřít	k5eAaPmAgFnS	zemřít
předčasně	předčasně	k6eAd1	předčasně
<g/>
)	)	kIx)	)
Courbet	Courbet	k1gInSc1	Courbet
později	pozdě	k6eAd2	pozdě
často	často	k6eAd1	často
portrétoval	portrétovat	k5eAaImAgMnS	portrétovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
začal	začít	k5eAaPmAgMnS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
základy	základ	k1gInPc4	základ
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
Královského	královský	k2eAgNnSc2d1	královské
kolegia	kolegium	k1gNnSc2	kolegium
v	v	k7c6	v
Besançonu	Besançon	k1gInSc6	Besançon
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
ateliér	ateliér	k1gInSc4	ateliér
Charlese	Charles	k1gMnSc2	Charles
Antoine	Antoin	k1gInSc5	Antoin
Flajoulota	Flajoulota	k1gFnSc1	Flajoulota
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
rodičů	rodič	k1gMnPc2	rodič
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
usadil	usadit	k5eAaPmAgInS	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
kurzy	kurz	k1gInPc4	kurz
malířství	malířství	k1gNnSc2	malířství
u	u	k7c2	u
Augusta	August	k1gMnSc2	August
Hesse	Hess	k1gMnSc2	Hess
a	a	k8xC	a
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Steubena	Steuben	k2eAgFnSc1d1	Steubena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
kopíroval	kopírovat	k5eAaImAgMnS	kopírovat
díla	dílo	k1gNnSc2	dílo
španělských	španělský	k2eAgMnPc2d1	španělský
<g/>
,	,	kIx,	,
benátských	benátský	k2eAgMnPc2d1	benátský
a	a	k8xC	a
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
malířů	malíř	k1gMnPc2	malíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
předchozích	předchozí	k2eAgInPc6d1	předchozí
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
pokusech	pokus	k1gInPc6	pokus
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
porota	porota	k1gFnSc1	porota
každoročně	každoročně	k6eAd1	každoročně
pořádaného	pořádaný	k2eAgInSc2d1	pořádaný
Salónu	salón	k1gInSc2	salón
přijala	přijmout	k5eAaPmAgFnS	přijmout
obraz	obraz	k1gInSc4	obraz
Courbet	Courbeta	k1gFnPc2	Courbeta
a	a	k8xC	a
černý	černý	k2eAgMnSc1d1	černý
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Courbet	Courbet	k1gInSc1	Courbet
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
fixovaný	fixovaný	k2eAgInSc1d1	fixovaný
na	na	k7c4	na
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
Ornans	Ornans	k1gInSc1	Ornans
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgInS	zřídit
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
poznat	poznat	k5eAaPmF	poznat
originály	originál	k1gInPc4	originál
svých	svůj	k3xOyFgInPc2	svůj
malířských	malířský	k2eAgInPc2d1	malířský
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
Andler	Andler	k1gInSc4	Andler
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházela	scházet	k5eAaImAgFnS	scházet
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
bohéma	bohéma	k1gFnSc1	bohéma
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Champfleurym	Champfleurym	k1gInSc4	Champfleurym
<g/>
,	,	kIx,	,
teoretikem	teoretik	k1gMnSc7	teoretik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
udržoval	udržovat	k5eAaImAgMnS	udržovat
blízké	blízký	k2eAgNnSc4d1	blízké
přátelství	přátelství	k1gNnSc4	přátelství
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
legitimoval	legitimovat	k5eAaBmAgMnS	legitimovat
svého	svůj	k3xOyFgMnSc4	svůj
nemanželského	manželský	k2eNgMnSc4d1	nemanželský
syna	syn	k1gMnSc4	syn
Désiré	Désirý	k2eAgNnSc1d1	Désiré
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
modelkou	modelka	k1gFnSc7	modelka
Virginií	Virginie	k1gFnSc7	Virginie
Binetovou	Binetový	k2eAgFnSc7d1	Binetový
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Přátelil	přátelit	k5eAaImAgMnS	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
básníkem	básník	k1gMnSc7	básník
Baudelairem	Baudelair	k1gMnSc7	Baudelair
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
ateliéru	ateliér	k1gInSc6	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
namaloval	namalovat	k5eAaPmAgMnS	namalovat
jeho	jeho	k3xOp3gInSc4	jeho
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Blízce	blízce	k6eAd1	blízce
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
Proudhonem	Proudhon	k1gInSc7	Proudhon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc7	jehož
socialistickými	socialistický	k2eAgInPc7d1	socialistický
názory	názor	k1gInPc7	názor
zůstal	zůstat	k5eAaPmAgInS	zůstat
natrvalo	natrvalo	k6eAd1	natrvalo
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
<g/>
.	.	kIx.	.
<g/>
Prvního	první	k4xOgInSc2	první
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gNnSc3	on
porota	porota	k1gFnSc1	porota
Salónu	salón	k1gInSc2	salón
udělila	udělit	k5eAaPmAgFnS	udělit
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
obraz	obraz	k1gInSc4	obraz
Odpoledne	odpoledne	k6eAd1	odpoledne
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
opravňovalo	opravňovat	k5eAaImAgNnS	opravňovat
obesílat	obesílat	k5eAaImF	obesílat
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
Salóny	salón	k1gInPc4	salón
bez	bez	k7c2	bez
schválení	schválení	k1gNnSc2	schválení
kvalifikační	kvalifikační	k2eAgFnSc7d1	kvalifikační
komisí	komise	k1gFnSc7	komise
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
rozměry	rozměra	k1gFnPc1	rozměra
i	i	k8xC	i
významem	význam	k1gInSc7	význam
velká	velký	k2eAgNnPc4d1	velké
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgInPc4d1	tvořící
pilíře	pilíř	k1gInPc4	pilíř
realistické	realistický	k2eAgFnSc2d1	realistická
malby	malba	k1gFnSc2	malba
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
Pohřeb	pohřeb	k1gInSc1	pohřeb
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
Vesničané	vesničan	k1gMnPc1	vesničan
z	z	k7c2	z
Flagey	Flagea	k1gFnSc2	Flagea
<g/>
:	:	kIx,	:
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
Lamači	lamač	k1gMnPc1	lamač
kamene	kámen	k1gInSc2	kámen
–	–	k?	–
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
letech	let	k1gInPc6	let
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
.	.	kIx.	.
</s>
<s>
Skandál	skandál	k1gInSc1	skandál
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
obraz	obraz	k1gInSc4	obraz
Koupající	koupající	k2eAgInSc4d1	koupající
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vystavený	vystavený	k2eAgInSc1d1	vystavený
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
ho	on	k3xPp3gNnSc4	on
sběratel	sběratel	k1gMnSc1	sběratel
Alfred	Alfred	k1gMnSc1	Alfred
Bruyas	Bruyas	k1gMnSc1	Bruyas
z	z	k7c2	z
Montpellier	Montpellira	k1gFnPc2	Montpellira
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Courbetovým	Courbetový	k2eAgMnSc7d1	Courbetový
hlavním	hlavní	k2eAgMnSc7d1	hlavní
mecenášem	mecenáš	k1gMnSc7	mecenáš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
ho	on	k3xPp3gInSc4	on
Courbet	Courbet	k1gInSc4	Courbet
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
;	;	kIx,	;
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
obraz	obraz	k1gInSc1	obraz
Setkání	setkání	k1gNnSc1	setkání
(	(	kIx(	(
<g/>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Courbete	Courbete	k1gMnSc5	Courbete
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
konala	konat	k5eAaImAgFnS	konat
světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Bruyasova	Bruyasův	k2eAgNnSc2d1	Bruyasův
přispění	přispění	k1gNnSc2	přispění
si	se	k3xPyFc3	se
Courbet	Courbet	k1gMnSc1	Courbet
poblíž	poblíž	k7c2	poblíž
výstaviště	výstaviště	k1gNnSc2	výstaviště
postavil	postavit	k5eAaPmAgInS	postavit
vlastní	vlastní	k2eAgInSc1d1	vlastní
pavilón	pavilón	k1gInSc1	pavilón
s	s	k7c7	s
názvem	název	k1gInSc7	název
Realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vystavil	vystavit	k5eAaPmAgInS	vystavit
40	[number]	k4	40
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
včetně	včetně	k7c2	včetně
Ateliéru	ateliér	k1gInSc2	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
Coubertova	Coubertův	k2eAgFnSc1d1	Coubertův
samostatná	samostatný	k2eAgFnSc1d1	samostatná
výstava	výstava	k1gFnSc1	výstava
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
nějaký	nějaký	k3yIgMnSc1	nějaký
malíř	malíř	k1gMnSc1	malíř
takovýmto	takovýto	k3xDgInSc7	takovýto
způsobem	způsob	k1gInSc7	způsob
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
tamní	tamní	k2eAgFnSc1d1	tamní
Akademie	akademie	k1gFnSc1	akademie
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Havre	Havr	k1gInSc5	Havr
v	v	k7c6	v
Normandii	Normandie	k1gFnSc4	Normandie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
poznal	poznat	k5eAaPmAgMnS	poznat
malíře	malíř	k1gMnSc2	malíř
Eugè	Eugè	k1gMnSc1	Eugè
Boudina	Boudina	k1gFnSc1	Boudina
a	a	k8xC	a
mladého	mladý	k2eAgMnSc2d1	mladý
Claude	Claud	k1gInSc5	Claud
Moneta	moneta	k1gFnSc1	moneta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
stát	stát	k5eAaPmF	stát
modelem	model	k1gInSc7	model
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
variaci	variace	k1gFnSc6	variace
na	na	k7c4	na
Manetovu	Manetův	k2eAgFnSc4d1	Manetova
Snídani	snídaně	k1gFnSc4	snídaně
v	v	k7c6	v
trávě	tráva	k1gFnSc6	tráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
trvalo	trvat	k5eAaImAgNnS	trvat
jeho	jeho	k3xOp3gNnSc1	jeho
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
působení	působení	k1gNnSc1	působení
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
ateliéru	ateliér	k1gInSc6	ateliér
přijímal	přijímat	k5eAaImAgInS	přijímat
kolem	kolem	k7c2	kolem
30	[number]	k4	30
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
Fantin	Fantin	k2eAgInSc1d1	Fantin
<g/>
–	–	k?	–
<g/>
Latoura	Latoura	k1gFnSc1	Latoura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
města	město	k1gNnSc2	město
Saintes	Saintesa	k1gFnPc2	Saintesa
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Charente-Maritime	Charente-Maritim	k1gMnSc5	Charente-Maritim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
maloval	malovat	k5eAaImAgMnS	malovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Corotem	Corot	k1gInSc7	Corot
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
zajížděl	zajíždět	k5eAaImAgMnS	zajíždět
do	do	k7c2	do
rodného	rodný	k2eAgMnSc2d1	rodný
Ornans	Ornans	k1gInSc4	Ornans
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
si	se	k3xPyFc3	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
okraji	okraj	k1gInSc6	okraj
postavil	postavit	k5eAaPmAgMnS	postavit
nový	nový	k2eAgInSc4d1	nový
ateliér	ateliér	k1gInSc4	ateliér
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
původní	původní	k2eAgMnSc1d1	původní
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
korespondoval	korespondovat	k5eAaImAgInS	korespondovat
s	s	k7c7	s
Victorem	Victor	k1gMnSc7	Victor
Hugem	Hugo	k1gMnSc7	Hugo
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
ho	on	k3xPp3gNnSc4	on
portrétovat	portrétovat	k5eAaImF	portrétovat
<g/>
,	,	kIx,	,
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
ale	ale	k9	ale
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
pobýval	pobývat	k5eAaImAgMnS	pobývat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
malířem	malíř	k1gMnSc7	malíř
Jamesem	James	k1gMnSc7	James
Whistlerem	Whistler	k1gMnSc7	Whistler
v	v	k7c6	v
normandském	normandský	k2eAgInSc6d1	normandský
Trouville	Trouvill	k1gInSc6	Trouvill
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
si	se	k3xPyFc3	se
Courbet	Courbet	k1gMnSc1	Courbet
opět	opět	k6eAd1	opět
postavil	postavit	k5eAaPmAgMnS	postavit
–	–	k?	–
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
Bruyasovým	Bruyasův	k2eAgNnSc7d1	Bruyasův
–	–	k?	–
samostatný	samostatný	k2eAgInSc4d1	samostatný
pavilón	pavilón	k1gInSc4	pavilón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představil	představit	k5eAaPmAgInS	představit
133	[number]	k4	133
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
expozici	expozice	k1gFnSc4	expozice
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
tentokrát	tentokrát	k6eAd1	tentokrát
připravil	připravit	k5eAaPmAgInS	připravit
i	i	k9	i
Manet	manet	k1gInSc1	manet
<g/>
.	.	kIx.	.
<g/>
Rád	rád	k2eAgMnSc1d1	rád
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došla	dojít	k5eAaPmAgFnS	dojít
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
značné	značný	k2eAgFnSc2d1	značná
obliby	obliba	k1gFnSc2	obliba
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
německého	německý	k2eAgNnSc2d1	německé
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Courbet	Courbet	k1gMnSc1	Courbet
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
několika	několik	k4yIc6	několik
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
mu	on	k3xPp3gMnSc3	on
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgInSc1d1	bavorský
udělil	udělit	k5eAaPmAgInS	udělit
Řád	řád	k1gInSc1	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
ho	on	k3xPp3gMnSc4	on
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
bruselské	bruselský	k2eAgFnSc2d1	bruselská
výstavy	výstava	k1gFnSc2	výstava
král	král	k1gMnSc1	král
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Belgický	belgický	k2eAgMnSc1d1	belgický
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
zlatou	zlatá	k1gFnSc7	zlatá
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
převzít	převzít	k5eAaPmF	převzít
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
Monet	moneta	k1gFnPc2	moneta
vybral	vybrat	k5eAaPmAgInS	vybrat
za	za	k7c4	za
svědka	svědek	k1gMnSc4	svědek
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
zástupcem	zástupce	k1gMnSc7	zástupce
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
Federace	federace	k1gFnSc2	federace
umělců	umělec	k1gMnPc2	umělec
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
ochranu	ochrana	k1gFnSc4	ochrana
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dal	dát	k5eAaPmAgMnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
poražení	poražení	k1gNnSc3	poražení
Vendômského	Vendômský	k2eAgInSc2d1	Vendômský
sloupu	sloup	k1gInSc2	sloup
<g/>
,	,	kIx,	,
symbolu	symbol	k1gInSc2	symbol
bonapartismu	bonapartismus	k1gInSc2	bonapartismus
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
aktivitu	aktivita	k1gFnSc4	aktivita
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Komuny	komuna	k1gFnSc2	komuna
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
6	[number]	k4	6
měsícům	měsíc	k1gInPc3	měsíc
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1872	[number]	k4	1872
odjel	odjet	k5eAaPmAgMnS	odjet
Courbet	Courbet	k1gMnSc1	Courbet
do	do	k7c2	do
Ornans	Ornansa	k1gFnPc2	Ornansa
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
pařížský	pařížský	k2eAgMnSc1d1	pařížský
galerista	galerista	k1gMnSc1	galerista
Paul	Paul	k1gMnSc1	Paul
Durand-Ruel	Durand-Ruel	k1gInSc4	Durand-Ruel
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
výstavu	výstava	k1gFnSc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Désiré	Désirý	k2eAgFnSc2d1	Désirý
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
otce	otec	k1gMnSc4	otec
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
francouzské	francouzský	k2eAgNnSc1d1	francouzské
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vendômský	Vendômský	k2eAgInSc1d1	Vendômský
sloup	sloup	k1gInSc1	sloup
bude	být	k5eAaImBp3nS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
a	a	k8xC	a
náklady	náklad	k1gInPc1	náklad
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
ponese	ponést	k5eAaPmIp3nS	ponést
Courbet	Courbet	k1gInSc4	Courbet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
obrazů	obraz	k1gInPc2	obraz
byl	být	k5eAaImAgInS	být
zabaven	zabavit	k5eAaPmNgInS	zabavit
státem	stát	k1gInSc7	stát
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
La	la	k1gNnSc6	la
<g/>
–	–	k?	–
<g/>
Tour	Tour	k1gInSc1	Tour
<g/>
–	–	k?	–
<g/>
de	de	k?	de
<g/>
–	–	k?	–
<g/>
Peilz	Peilz	k1gMnSc1	Peilz
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výstavu	výstava	k1gFnSc4	výstava
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
malíř	malíř	k1gMnSc1	malíř
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
vynesl	vynést	k5eAaPmAgInS	vynést
pařížský	pařížský	k2eAgInSc1d1	pařížský
soud	soud	k1gInSc1	soud
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgNnSc3	jenž
již	již	k9	již
nebylo	být	k5eNaImAgNnS	být
odvolání	odvolání	k1gNnSc3	odvolání
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
musí	muset	k5eAaImIp3nS	muset
Courbet	Courbet	k1gInSc1	Courbet
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
sloupu	sloup	k1gInSc2	sloup
uhradit	uhradit	k5eAaPmF	uhradit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
malíř	malíř	k1gMnSc1	malíř
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
dna	dna	k1gFnSc1	dna
a	a	k8xC	a
cirhóza	cirhóza	k1gFnSc1	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
'	'	kIx"	'
<g/>
Cimetiè	Cimetiè	k1gFnSc1	Cimetiè
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ornans	Ornans	k1gInSc1	Ornans
<g/>
'	'	kIx"	'
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Doubs	Doubs	k1gInSc1	Doubs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Courbet	Courbet	k1gMnSc1	Courbet
do	do	k7c2	do
sebe	se	k3xPyFc2	se
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
vstřebal	vstřebat	k5eAaPmAgMnS	vstřebat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
evropské	evropský	k2eAgFnSc2d1	Evropská
malířské	malířský	k2eAgFnSc2d1	malířská
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
zejména	zejména	k9	zejména
holandské	holandský	k2eAgFnPc4d1	holandská
mistry	mistr	k1gMnPc7	mistr
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Rembrandta	Rembrandt	k1gMnSc4	Rembrandt
a	a	k8xC	a
Halse	Hals	k1gMnSc4	Hals
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dílech	díl	k1gInPc6	díl
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
ohlasy	ohlas	k1gInPc4	ohlas
malby	malba	k1gFnSc2	malba
Giorgiona	Giorgion	k1gMnSc2	Giorgion
a	a	k8xC	a
Tiziana	Tizian	k1gMnSc2	Tizian
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
postulátem	postulát	k1gInSc7	postulát
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
nezávislý	závislý	k2eNgInSc1d1	nezávislý
a	a	k8xC	a
svobodný	svobodný	k2eAgInSc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přišel	přijít	k5eAaPmAgMnS	přijít
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
udával	udávat	k5eAaImAgMnS	udávat
malířství	malířství	k1gNnSc4	malířství
tón	tón	k1gInSc1	tón
akademismus	akademismus	k1gInSc4	akademismus
představovaný	představovaný	k2eAgInSc4d1	představovaný
díly	díl	k1gInPc7	díl
Davidovými	Davidův	k2eAgInPc7d1	Davidův
a	a	k8xC	a
Ingresovými	Ingresový	k2eAgInPc7d1	Ingresový
<g/>
.	.	kIx.	.
</s>
<s>
Mladého	mladý	k2eAgNnSc2d1	mladé
Courbeta	Courbeto	k1gNnSc2	Courbeto
ale	ale	k8xC	ale
přísná	přísný	k2eAgNnPc1d1	přísné
pravidla	pravidlo	k1gNnPc1	pravidlo
akademismu	akademismus	k1gInSc2	akademismus
svazovala	svazovat	k5eAaImAgNnP	svazovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
k	k	k7c3	k
romantickému	romantický	k2eAgInSc3d1	romantický
směru	směr	k1gInSc3	směr
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
představovanému	představovaný	k2eAgNnSc3d1	představované
Delacroixem	Delacroix	k1gInSc7	Delacroix
a	a	k8xC	a
Géricaultem	Géricault	k1gInSc7	Géricault
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
studijní	studijní	k2eAgFnSc2d1	studijní
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
se	se	k3xPyFc4	se
však	však	k9	však
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
romantismu	romantismus	k1gInSc2	romantismus
odklánět	odklánět	k5eAaImF	odklánět
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
obrazu	obraz	k1gInSc2	obraz
Pohřeb	pohřeb	k1gInSc1	pohřeb
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
malíř	malíř	k1gMnSc1	malíř
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
pohřeb	pohřeb	k1gInSc4	pohřeb
jeho	on	k3xPp3gMnSc2	on
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
,	,	kIx,	,
symbolicky	symbolicky	k6eAd1	symbolicky
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
pohřeb	pohřeb	k1gInSc4	pohřeb
romantických	romantický	k2eAgFnPc2d1	romantická
tendencí	tendence	k1gFnPc2	tendence
v	v	k7c6	v
Courbetově	Courbetův	k2eAgFnSc6d1	Courbetův
malbě	malba	k1gFnSc6	malba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
během	během	k7c2	během
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
publikoval	publikovat	k5eAaBmAgMnS	publikovat
manifest	manifest	k1gInSc4	manifest
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
své	svůj	k3xOyFgFnPc4	svůj
označení	označení	k1gNnSc1	označení
za	za	k7c4	za
realistu	realista	k1gMnSc4	realista
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
zařazování	zařazování	k1gNnSc1	zařazování
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
do	do	k7c2	do
obecných	obecný	k2eAgInPc2d1	obecný
pojmů	pojem	k1gInPc2	pojem
je	být	k5eAaImIp3nS	být
zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
<g/>
.	.	kIx.	.
</s>
<s>
Courbet	Courbet	k1gInSc1	Courbet
byl	být	k5eAaImAgInS	být
vášnivě	vášnivě	k6eAd1	vášnivě
zaujatý	zaujatý	k2eAgInSc1d1	zaujatý
hmotnou	hmotný	k2eAgFnSc7d1	hmotná
skutečností	skutečnost	k1gFnSc7	skutečnost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
zobrazování	zobrazování	k1gNnSc6	zobrazování
čehokoliv	cokoliv	k3yInSc2	cokoliv
jiného	jiný	k2eAgNnSc2d1	jiné
je	být	k5eAaImIp3nS	být
odklonem	odklon	k1gInSc7	odklon
od	od	k7c2	od
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Domnívám	domnívat	k5eAaImIp1nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
malířství	malířství	k1gNnSc1	malířství
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc4	umění
nezměrně	nezměrně	k6eAd1	nezměrně
korektní	korektní	k2eAgMnSc1d1	korektní
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
stát	stát	k5eAaImF	stát
na	na	k7c4	na
prezentování	prezentování	k1gNnSc4	prezentování
pouze	pouze	k6eAd1	pouze
věcí	věc	k1gFnSc7	věc
reálných	reálný	k2eAgFnPc2d1	reálná
a	a	k8xC	a
existujících	existující	k2eAgFnPc2d1	existující
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
krédo	krédo	k1gNnSc1	krédo
malíře	malíř	k1gMnSc2	malíř
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
i	i	k9	i
tento	tento	k3xDgInSc4	tento
jeho	on	k3xPp3gInSc4	on
výrok	výrok	k1gInSc4	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Krása	krása	k1gFnSc1	krása
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
v	v	k7c6	v
realitě	realita	k1gFnSc6	realita
se	se	k3xPyFc4	se
vyjevuje	vyjevovat	k5eAaImIp3nS	vyjevovat
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
objevena	objevit	k5eAaPmNgFnS	objevit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
malíři	malíř	k1gMnPc1	malíř
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Courbetův	Courbetův	k2eAgInSc1d1	Courbetův
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
evropského	evropský	k2eAgNnSc2d1	Evropské
malířství	malířství	k1gNnSc2	malířství
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
Nesmírně	smírně	k6eNd1	smírně
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vážil	vážit	k5eAaImAgInS	vážit
Cézanne	Cézann	k1gInSc5	Cézann
a	a	k8xC	a
impresionisté	impresionista	k1gMnPc1	impresionista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Monet	moneta	k1gFnPc2	moneta
a	a	k8xC	a
Renoir	Renoira	k1gFnPc2	Renoira
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
často	často	k6eAd1	často
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
otázky	otázka	k1gFnPc4	otázka
techniky	technika	k1gFnSc2	technika
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc4	dílo
Manetova	Manetův	k2eAgFnSc1d1	Manetova
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
konfrontaci	konfrontace	k1gFnSc6	konfrontace
s	s	k7c7	s
pracemi	práce	k1gFnPc7	práce
Courbetovými	Courbetový	k2eAgFnPc7d1	Courbetový
<g/>
.	.	kIx.	.
</s>
<s>
Courbet	Courbet	k1gMnSc1	Courbet
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
inspiračních	inspirační	k2eAgInPc2d1	inspirační
vzorů	vzor	k1gInPc2	vzor
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
pro	pro	k7c4	pro
Matisse	Matisse	k1gFnPc4	Matisse
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
nadšený	nadšený	k2eAgInSc1d1	nadšený
jeho	jeho	k3xOp3gInSc7	jeho
obrazem	obraz	k1gInSc7	obraz
Dívky	dívka	k1gFnSc2	dívka
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Seiny	Seina	k1gFnSc2	Seina
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
namaloval	namalovat	k5eAaPmAgMnS	namalovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zastoupení	zastoupení	k1gNnSc3	zastoupení
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
a	a	k8xC	a
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
tři	tři	k4xCgFnPc1	tři
Courbetovy	Courbetův	k2eAgFnPc1d1	Courbetův
olejomalby	olejomalba	k1gFnPc1	olejomalba
<g/>
:	:	kIx,	:
Žena	žena	k1gFnSc1	žena
s	s	k7c7	s
kvítím	kvítí	k1gNnSc7	kvítí
na	na	k7c6	na
klobouku	klobouk	k1gInSc6	klobouk
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lesní	lesní	k2eAgFnSc1d1	lesní
sluj	sluj	k1gFnSc1	sluj
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jurská	jurský	k2eAgFnSc1d1	jurská
krajina	krajina	k1gFnSc1	krajina
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
s	s	k7c7	s
černým	černý	k2eAgMnSc7d1	černý
kokršpanělem	kokršpaněl	k1gMnSc7	kokršpaněl
<g/>
,	,	kIx,	,
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
46	[number]	k4	46
x	x	k?	x
56	[number]	k4	56
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc1	Palais
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Courbet	Courbet	k1gInSc1	Courbet
vystavil	vystavit	k5eAaPmAgInS	vystavit
na	na	k7c6	na
Salónu	salón	k1gInSc6	salón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Baudelaire	Baudelair	k1gMnSc5	Baudelair
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
54	[number]	k4	54
x	x	k?	x
65	[number]	k4	65
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Muséus	k1gMnSc5	Muséus
Fabre	Fabr	k1gMnSc5	Fabr
<g/>
,	,	kIx,	,
Montpellier	Montpellira	k1gFnPc2	Montpellira
<g/>
.	.	kIx.	.
</s>
<s>
Baudelaire	Baudelair	k1gMnSc5	Baudelair
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
jako	jako	k8xS	jako
učenec	učenec	k1gMnSc1	učenec
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgInSc1d1	osobní
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
modelem	model	k1gInSc7	model
není	být	k5eNaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
<g/>
.	.	kIx.	.
</s>
<s>
Baudelaire	Baudelair	k1gMnSc5	Baudelair
dával	dávat	k5eAaImAgInS	dávat
před	před	k7c7	před
Courbetem	Courbet	k1gInSc7	Courbet
přednost	přednost	k1gFnSc4	přednost
Delacroixovi	Delacroixa	k1gMnSc3	Delacroixa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
315	[number]	k4	315
x	x	k?	x
668	[number]	k4	668
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gFnSc2	Orsaa
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prezentaci	prezentace	k1gFnSc6	prezentace
na	na	k7c6	na
Salónu	salón	k1gInSc6	salón
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
obraz	obraz	k1gInSc1	obraz
rozruch	rozruch	k1gInSc4	rozruch
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
vyhrazené	vyhrazený	k2eAgFnPc4d1	vyhrazená
dosud	dosud	k6eAd1	dosud
pro	pro	k7c4	pro
historickou	historický	k2eAgFnSc4d1	historická
malbu	malba	k1gFnSc4	malba
<g/>
,	,	kIx,	,
přesadil	přesadit	k5eAaPmAgInS	přesadit
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
francouzský	francouzský	k2eAgInSc4d1	francouzský
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
byl	být	k5eAaImAgMnS	být
Courbet	Courbet	k1gMnSc1	Courbet
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
realistického	realistický	k2eAgMnSc4d1	realistický
malíře	malíř	k1gMnSc4	malíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Setkání	setkání	k1gNnSc1	setkání
(	(	kIx(	(
<g/>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Courbete	Courbete	k1gMnSc5	Courbete
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
129	[number]	k4	129
x	x	k?	x
149	[number]	k4	149
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Muséus	k1gMnSc5	Muséus
Fabre	Fabr	k1gMnSc5	Fabr
<g/>
,	,	kIx,	,
Montpellier	Montpellira	k1gFnPc2	Montpellira
<g/>
.	.	kIx.	.
</s>
<s>
Mecenáš	mecenáš	k1gMnSc1	mecenáš
Alfred	Alfred	k1gMnSc1	Alfred
Bruyas	Bruyas	k1gMnSc1	Bruyas
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
sloužícího	sloužící	k1gMnSc2	sloužící
a	a	k8xC	a
psa	pes	k1gMnSc2	pes
přichází	přicházet	k5eAaImIp3nS	přicházet
vstříc	vstříc	k7c3	vstříc
malíři	malíř	k1gMnSc3	malíř
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc2	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
mecenáš	mecenáš	k1gMnSc1	mecenáš
i	i	k8xC	i
malíř	malíř	k1gMnSc1	malíř
rovnocennou	rovnocenný	k2eAgFnSc4d1	rovnocenná
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Courbet	Courbet	k1gMnSc1	Courbet
se	se	k3xPyFc4	se
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
stylizuje	stylizovat	k5eAaImIp3nS	stylizovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
Ahasvera	Ahasver	k1gMnSc2	Ahasver
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženy	žena	k1gFnPc1	žena
prosévající	prosévající	k2eAgNnSc1d1	prosévající
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
131	[number]	k4	131
x	x	k?	x
167	[number]	k4	167
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
des	des	k1gNnSc2	des
Beaux-Arts	Beaux-Artsa	k1gFnPc2	Beaux-Artsa
<g/>
,	,	kIx,	,
Nantes	Nantesa	k1gFnPc2	Nantesa
<g/>
.	.	kIx.	.
</s>
<s>
Modelem	model	k1gInSc7	model
byly	být	k5eAaImAgFnP	být
malíři	malíř	k1gMnPc1	malíř
jeho	jeho	k3xOp3gFnSc2	jeho
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
připomíná	připomínat	k5eAaImIp3nS	připomínat
žánrové	žánrový	k2eAgInPc4d1	žánrový
obrazy	obraz	k1gInPc4	obraz
nizizemským	nizizemský	k2eAgInSc7d1	nizizemský
malířů	malíř	k1gMnPc2	malíř
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
Courbetových	Courbetový	k2eAgNnPc2d1	Courbetový
děl	dělo	k1gNnPc2	dělo
ho	on	k3xPp3gMnSc4	on
považoval	považovat	k5eAaImAgMnS	považovat
Cézane	Cézan	k1gInSc5	Cézan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ateliér	ateliér	k1gInSc1	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
alegorie	alegorie	k1gFnSc1	alegorie
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
mého	můj	k3xOp1gInSc2	můj
uměleckého	umělecký	k2eAgInSc2d1	umělecký
a	a	k8xC	a
morálního	morální	k2eAgInSc2d1	morální
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
359	[number]	k4	359
x	x	k?	x
598	[number]	k4	598
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gFnSc2	Orsaa
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
plánu	plán	k1gInSc6	plán
jsou	být	k5eAaImIp3nP	být
zobrazeni	zobrazen	k2eAgMnPc1d1	zobrazen
umělcovi	umělcův	k2eAgMnPc1d1	umělcův
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vtaženi	vtáhnout	k5eAaPmNgMnP	vtáhnout
do	do	k7c2	do
dění	dění	k1gNnSc2	dění
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Baudelaire	Baudelair	k1gMnSc5	Baudelair
čte	číst	k5eAaImIp3nS	číst
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
na	na	k7c6	na
taburetu	taburet	k1gInSc6	taburet
sedí	sedit	k5eAaImIp3nP	sedit
Champfleury	Champfleur	k1gInPc1	Champfleur
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Proudhon	Proudhon	k1gInSc4	Proudhon
a	a	k8xC	a
Bruyas	Bruyas	k1gInSc4	Bruyas
<g/>
.	.	kIx.	.
</s>
<s>
Venkované	venkovan	k1gMnPc1	venkovan
a	a	k8xC	a
řemeslníci	řemeslník	k1gMnPc1	řemeslník
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
malířově	malířův	k2eAgFnSc3d1	malířova
(	(	kIx(	(
<g/>
Courbetově	Courbetův	k2eAgFnSc3d1	Courbetův
<g/>
)	)	kIx)	)
tvorbě	tvorba	k1gFnSc3	tvorba
staví	stavit	k5eAaImIp3nS	stavit
nezúčastněně	zúčastněně	k6eNd1	zúčastněně
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
za	za	k7c7	za
malířem	malíř	k1gMnSc7	malíř
představuje	představovat	k5eAaImIp3nS	představovat
Múzu	Múza	k1gFnSc4	Múza
<g/>
,	,	kIx,	,
malý	malý	k2eAgMnSc1d1	malý
pasáček	pasáček	k1gMnSc1	pasáček
Dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
46	[number]	k4	46
x	x	k?	x
55	[number]	k4	55
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gFnSc2	Orsaa
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
namalován	namalovat	k5eAaPmNgInS	namalovat
pro	pro	k7c4	pro
tureckého	turecký	k2eAgMnSc4d1	turecký
sběratele	sběratel	k1gMnSc4	sběratel
a	a	k8xC	a
diplomata	diplomat	k1gMnSc4	diplomat
Khalila	Khalila	k1gMnSc4	Khalila
Beye	Bey	k1gMnSc4	Bey
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
rovněž	rovněž	k9	rovněž
proslulou	proslulý	k2eAgFnSc4d1	proslulá
Ingresovu	Ingresův	k2eAgFnSc4d1	Ingresův
Tureckou	turecký	k2eAgFnSc4d1	turecká
lázeň	lázeň	k1gFnSc4	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
nepochází	pocházet	k5eNaImIp3nS	pocházet
od	od	k7c2	od
Courbeta	Courbeto	k1gNnSc2	Courbeto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dialogem	dialog	k1gInSc7	dialog
s	s	k7c7	s
Manetovou	Manetův	k2eAgFnSc7d1	Manetova
Olympií	Olympia	k1gFnSc7	Olympia
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
nahá	nahý	k2eAgFnSc1d1	nahá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
ale	ale	k9	ale
rukou	ruka	k1gFnSc7	ruka
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
klín	klín	k1gInSc4	klín
<g/>
.	.	kIx.	.
</s>
<s>
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gMnSc2	Orsaa
má	mít	k5eAaImIp3nS	mít
obraz	obraz	k1gInSc4	obraz
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spánek	spánek	k1gInSc1	spánek
(	(	kIx(	(
<g/>
Lenost	lenost	k1gFnSc1	lenost
a	a	k8xC	a
neřest	neřest	k1gFnSc1	neřest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
135	[number]	k4	135
x	x	k?	x
200	[number]	k4	200
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
du	du	k?	du
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc1	Palais
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Spící	spící	k2eAgFnPc1d1	spící
nahé	nahý	k2eAgFnPc1d1	nahá
ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
jsou	být	k5eAaImIp3nP	být
odrazem	odraz	k1gInSc7	odraz
jedné	jeden	k4xCgFnSc2	jeden
Baudelairovy	Baudelairův	k2eAgFnSc2d1	Baudelairova
básně	báseň	k1gFnSc2	báseň
z	z	k7c2	z
Květů	květ	k1gInPc2	květ
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Courbet	Courbet	k5eAaBmF	Courbet
zde	zde	k6eAd1	zde
svrchovanými	svrchovaný	k2eAgInPc7d1	svrchovaný
malířskými	malířský	k2eAgInPc7d1	malířský
prostředky	prostředek	k1gInPc7	prostředek
znázornil	znázornit	k5eAaPmAgMnS	znázornit
lesbickou	lesbický	k2eAgFnSc4d1	lesbická
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
nestoudnost	nestoudnost	k1gFnSc4	nestoudnost
a	a	k8xC	a
něhu	něha	k1gFnSc4	něha
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pobřežní	pobřežní	k2eAgInPc4d1	pobřežní
útesy	útes	k1gInPc4	útes
v	v	k7c6	v
Étretatu	Étretat	k1gInSc6	Étretat
po	po	k7c6	po
bouři	bouř	k1gFnSc6	bouř
<g/>
,	,	kIx,	,
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
133	[number]	k4	133
x	x	k?	x
162	[number]	k4	162
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gFnSc2	Orsaa
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Courbetových	Courbetový	k2eAgFnPc2d1	Courbetový
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
místa	místo	k1gNnSc2	místo
namaloval	namalovat	k5eAaPmAgMnS	namalovat
ještě	ještě	k9	ještě
další	další	k2eAgInSc4d1	další
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Heydt-Museum	Heydt-Museum	k1gInSc4	Heydt-Museum
ve	v	k7c6	v
Wuppertalu	Wuppertal	k1gMnSc6	Wuppertal
<g/>
.	.	kIx.	.
</s>
<s>
Poučné	poučný	k2eAgNnSc1d1	poučné
je	být	k5eAaImIp3nS	být
srovnání	srovnání	k1gNnSc1	srovnání
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
pracuje	pracovat	k5eAaImIp3nS	pracovat
rozdílně	rozdílně	k6eAd1	rozdílně
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
předmět	předmět	k1gInSc1	předmět
obrazu	obraz	k1gInSc2	obraz
nasvícen	nasvítit	k5eAaPmNgInS	nasvítit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bonjour	Bonjour	k1gMnSc1	Bonjour
<g/>
,	,	kIx,	,
Monsieur	Monsieur	k1gMnSc1	Monsieur
Courbet	Courbet	k1gMnSc1	Courbet
<g/>
!	!	kIx.	!
</s>
<s>
The	The	k?	The
Bruyas	Bruyas	k1gInSc1	Bruyas
Collection	Collection	k1gInSc1	Collection
from	from	k1gInSc1	from
the	the	k?	the
Musée	Muséus	k1gMnSc5	Muséus
Fabre	Fabr	k1gMnSc5	Fabr
<g/>
,	,	kIx,	,
Montpellier	Montpellier	k1gInSc4	Montpellier
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgInSc4d1	výstavní
katalog	katalog	k1gInSc4	katalog
<g/>
,	,	kIx,	,
Réunion	Réunion	k1gInSc4	Réunion
des	des	k1gNnSc2	des
Musées	Muséesa	k1gFnPc2	Muséesa
Nationaux	Nationaux	k1gInSc1	Nationaux
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-7118-4778-0	[number]	k4	2-7118-4778-0
</s>
</p>
<p>
<s>
Courbet	Courbet	k1gInSc1	Courbet
et	et	k?	et
la	la	k0	la
Commune	Commun	k1gMnSc5	Commun
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgInSc4d1	výstavní
katalog	katalog	k1gInSc4	katalog
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc4	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gFnSc2	Orsaa
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-7118-4006-9	[number]	k4	2-7118-4006-9
</s>
</p>
<p>
<s>
Faunce	faunka	k1gFnSc3	faunka
<g/>
,	,	kIx,	,
Sarah	Sarah	k1gFnSc1	Sarah
<g/>
,	,	kIx,	,
Nochlin	Nochlina	k1gFnPc2	Nochlina
<g/>
,	,	kIx,	,
Linda	Linda	k1gFnSc1	Linda
<g/>
,	,	kIx,	,
Courbet	Courbet	k1gInSc1	Courbet
Reconsidered	Reconsidered	k1gInSc1	Reconsidered
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgInSc1d1	výstavní
katalog	katalog	k1gInSc1	katalog
<g/>
,	,	kIx,	,
Brooklynské	brooklynský	k2eAgNnSc1d1	Brooklynské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gMnSc1	Courbet
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
přel	přít	k5eAaImAgInS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Smetanová	Smetanová	k1gFnSc1	Smetanová
<g/>
;	;	kIx,	;
doslov	doslov	k1gInSc1	doslov
Genese	Genesis	k1gFnSc2	Genesis
courbetovského	courbetovský	k2eAgInSc2d1	courbetovský
realismu	realismus	k1gInSc2	realismus
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
uspoř	uspořit	k5eAaPmRp2nS	uspořit
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Míčko	Míčko	k1gNnSc1	Míčko
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
</s>
</p>
<p>
<s>
Haddad	Haddad	k6eAd1	Haddad
<g/>
,	,	kIx,	,
Michè	Michè	k1gMnSc5	Michè
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gInSc1	Courbet
<g/>
,	,	kIx,	,
peinture	peintur	k1gMnSc5	peintur
et	et	k?	et
histoire	histoir	k1gMnSc5	histoir
<g/>
,	,	kIx,	,
Presses	Presses	k1gMnSc1	Presses
du	du	k?	du
Belvédè	Belvédè	k1gMnSc1	Belvédè
<g/>
,	,	kIx,	,
Sainte-Croix	Sainte-Croix	k1gInSc1	Sainte-Croix
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-88419-085-6	[number]	k4	2-88419-085-6
</s>
</p>
<p>
<s>
Herding	Herding	k1gInSc1	Herding
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Realismus	realismus	k1gInSc1	realismus
als	als	k?	als
Widerspruch	Widerspruch	k1gInSc1	Widerspruch
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Wirklichkeit	Wirklichkeit	k1gInSc1	Wirklichkeit
in	in	k?	in
Courbets	Courbets	k1gInSc1	Courbets
Malerei	Malere	k1gFnSc2	Malere
<g/>
,	,	kIx,	,
Suhrkamp	Suhrkamp	k1gInSc1	Suhrkamp
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
a.	a.	k?	a.
M.	M.	kA	M.
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-518-36993-8	[number]	k4	3-518-36993-8
</s>
</p>
<p>
<s>
Kotalík	Kotalík	k1gMnSc1	Kotalík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Courbet	Courbet	k1gInSc1	Courbet
<g/>
,	,	kIx,	,
Tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1950	[number]	k4	1950
</s>
</p>
<p>
<s>
Nikodem	Nikod	k1gInSc7	Nikod
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gMnSc1	Courbet
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
V.U.	V.U.	k1gMnSc1	V.U.
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
</s>
</p>
<p>
<s>
Rubin	Rubin	k1gMnSc1	Rubin
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
Courbet	Courbet	k1gMnSc1	Courbet
<g/>
,	,	kIx,	,
Phaidon	Phaidon	k1gMnSc1	Phaidon
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7148-3180-8	[number]	k4	0-7148-3180-8
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
kompozice	kompozice	k1gFnSc2	kompozice
obrazů	obraz	k1gInPc2	obraz
</s>
</p>
<p>
<s>
Realismus	realismus	k1gInSc1	realismus
(	(	kIx(	(
<g/>
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Impresionismus	impresionismus	k1gInSc1	impresionismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gInSc4	Courbet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbeta	k1gFnPc2	Courbeta
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gInSc4	Courbet
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
<g/>
:	:	kIx,	:
Courbet	Courbet	k1gMnSc1	Courbet
Gustave	Gustav	k1gMnSc5	Gustav
</s>
</p>
<p>
<s>
ČT	ČT	kA	ČT
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gInSc1	Courbet
<g/>
:	:	kIx,	:
Žena	žena	k1gFnSc1	žena
s	s	k7c7	s
kvítím	kvítí	k1gNnSc7	kvítí
na	na	k7c6	na
klobouku	klobouk	k1gInSc6	klobouk
</s>
</p>
<p>
<s>
Reprodukce	reprodukce	k1gFnSc1	reprodukce
obrazů	obraz	k1gInPc2	obraz
</s>
</p>
<p>
<s>
Komplexní	komplexní	k2eAgInPc1d1	komplexní
údaje	údaj	k1gInPc1	údaj
+	+	kIx~	+
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Komplexní	komplexní	k2eAgInPc1d1	komplexní
údaje	údaj	k1gInPc1	údaj
+	+	kIx~	+
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
malíři	malíř	k1gMnSc6	malíř
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Courbetova	Courbetův	k2eAgNnSc2d1	Courbetův
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
jen	jen	k9	jen
francouzsky	francouzsky	k6eAd1	francouzsky
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
