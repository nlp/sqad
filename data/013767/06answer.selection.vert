<s>
Fiat	Fiat	k1gInSc1
Coupé	Coupé	k1gNnSc1
(	(	kIx(
<g/>
typ	typ	k1gInSc1
175	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
dvoudveřový	dvoudveřový	k2eAgInSc1d1
čtyřmístný	čtyřmístný	k2eAgInSc1d1
osobní	osobní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
provedení	provedení	k1gNnSc2
kupé	kupé	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
vyráběno	vyrábět	k5eAaImNgNnS
italskou	italský	k2eAgFnSc7d1
automobilkou	automobilka	k1gFnSc7
Fiat	Fiat	k1gInSc1
v	v	k7c6
letech	léto	k1gNnPc6
1994	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
představeno	představen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1993	#num#	k4
na	na	k7c6
bruselském	bruselský	k2eAgInSc6d1
autosalonu	autosalon	k1gInSc6
<g/>
.	.	kIx.
</s>