<s>
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgFnSc2d1
</s>
<s>
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgInPc1d1
Fiat	fiat	k1gInSc4
CoupéVýrobce	CoupéVýrobec	k1gInSc2
</s>
<s>
Fiat	fiat	k1gInSc1
Koncern	koncern	k1gInSc1
</s>
<s>
Fiat	fiat	k1gInSc1
Další	další	k2eAgInSc1d1
jména	jméno	k1gNnPc4
</s>
<s>
Coupé	Coupý	k2eAgInPc1d1
Roky	rok	k1gInPc1
produkce	produkce	k1gFnSc2
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
</s>
<s>
72	#num#	k4
762	#num#	k4
kusů	kus	k1gInPc2
Místa	místo	k1gNnSc2
výroby	výroba	k1gFnSc2
</s>
<s>
Turín	Turín	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
Modernizace	modernizace	k1gFnSc1
</s>
<s>
1998	#num#	k4
Karosárna	karosárna	k1gFnSc1
</s>
<s>
Pininfarina	Pininfarina	k1gFnSc1
Příbuzné	příbuzná	k1gFnSc2
vozy	vůz	k1gInPc7
</s>
<s>
Fiat	fiat	k1gInSc1
Barchetta	Barchetto	k1gNnSc2
Karoserie	karoserie	k1gFnSc2
</s>
<s>
kupé	kupé	k1gNnSc1
Designér	designér	k1gMnSc1
</s>
<s>
Chris	Chris	k1gFnSc1
Bangle	Bangle	k1gFnSc1
Platforma	platforma	k1gFnSc1
</s>
<s>
Fiat	fiat	k1gInSc1
Tipo	Tipo	k6eAd1
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Délka	délka	k1gFnSc1
</s>
<s>
4	#num#	k4
255	#num#	k4
Šířka	šířka	k1gFnSc1
</s>
<s>
1	#num#	k4
766	#num#	k4
Výška	výška	k1gFnSc1
</s>
<s>
1	#num#	k4
340	#num#	k4
Rozvor	rozvora	k1gFnPc2
</s>
<s>
2	#num#	k4
540	#num#	k4
Objem	objem	k1gInSc1
zavaz	zavazit	k5eAaPmRp2nS
<g/>
.	.	kIx.
prostoru	prostor	k1gInSc6
</s>
<s>
295	#num#	k4
l	l	kA
Počet	počet	k1gInSc1
míst	místo	k1gNnPc2
</s>
<s>
4	#num#	k4
Objem	objem	k1gInSc1
nádrže	nádrž	k1gFnSc2
</s>
<s>
63	#num#	k4
l	l	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fiat	Fiat	k1gInSc1
Coupé	Coupé	k1gNnSc1
(	(	kIx(
<g/>
typ	typ	k1gInSc1
175	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
dvoudveřový	dvoudveřový	k2eAgInSc1d1
čtyřmístný	čtyřmístný	k2eAgInSc1d1
osobní	osobní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
provedení	provedení	k1gNnSc2
kupé	kupé	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
vyráběno	vyrábět	k5eAaImNgNnS
italskou	italský	k2eAgFnSc7d1
automobilkou	automobilka	k1gFnSc7
Fiat	Fiat	k1gInSc1
v	v	k7c6
letech	léto	k1gNnPc6
1994	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
představeno	představen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1993	#num#	k4
na	na	k7c6
bruselském	bruselský	k2eAgInSc6d1
autosalonu	autosalon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Design	design	k1gInSc1
navrhl	navrhnout	k5eAaPmAgInS
Chris	Chris	k1gFnSc4
Bangle	Bangle	k1gNnSc2
ze	z	k7c2
STILO	STILO	kA
FIAT	fiat	k1gInSc1
centra	centrum	k1gNnSc2
<g/>
,	,	kIx,
interiér	interiér	k1gInSc4
vytvořil	vytvořit	k5eAaPmAgMnS
Pininfarina	Pininfarina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgNnSc1d1
nejrychlejším	rychlý	k2eAgInSc7d3
evropským	evropský	k2eAgInSc7d1
automobilem	automobil	k1gInSc7
s	s	k7c7
pohonem	pohon	k1gInSc7
předních	přední	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
nejrychlejším	rychlý	k2eAgInSc7d3
sériovým	sériový	k2eAgInSc7d1
Fiatem	fiat	k1gInSc7
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
redukce	redukce	k1gFnSc1
podružností	podružnost	k1gFnPc2
a	a	k8xC
neency	neenca	k1gFnSc2
</s>
<s>
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgFnSc2d1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
požadavku	požadavek	k1gInSc2
vedení	vedení	k1gNnSc2
automobilky	automobilka	k1gFnSc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
levného	levný	k2eAgInSc2d1
lidového	lidový	k2eAgInSc2d1
sportovního	sportovní	k2eAgInSc2d1
automobilu	automobil	k1gInSc2
na	na	k7c6
osvědčené	osvědčený	k2eAgFnSc6d1
platformě	platforma	k1gFnSc6
Tipo	Tipo	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Tempra	Tempr	k1gInSc2
modifikované	modifikovaný	k2eAgNnSc1d1
pro	pro	k7c4
Fiat	fiat	k1gInSc4
Bravo	bravo	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Brava	bravo	k1gMnSc2
<g/>
/	/	kIx~
<g/>
Marea	Marea	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Barchetta	Barchetta	k1gMnSc1
<g/>
,	,	kIx,
Alfa	alfa	k1gFnSc1
Romeo	Romeo	k1gMnSc1
145	#num#	k4
<g/>
/	/	kIx~
<g/>
146	#num#	k4
<g/>
/	/	kIx~
<g/>
155	#num#	k4
a	a	k8xC
Lancia	Lancia	k1gFnSc1
Dedra	Dedra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navazoval	navazovat	k5eAaImAgMnS
na	na	k7c4
slavné	slavný	k2eAgInPc4d1
modely	model	k1gInPc4
lidových	lidový	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
Fiat	fiat	k1gInSc1
šedesátých	šedesátý	k4xOgNnPc2
a	a	k8xC
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
společná	společný	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
Fiat	fiat	k1gInSc4
Coupé	Coupý	k2eAgNnSc1d1
<g/>
/	/	kIx~
<g/>
Lancia	Lancia	k1gFnSc1
Delta	delta	k1gFnSc1
Nuova	Nuova	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sdílela	sdílet	k5eAaImAgFnS
stejné	stejný	k2eAgInPc4d1
podvozkové	podvozkový	k2eAgInPc4d1
díly	díl	k1gInPc4
(	(	kIx(
<g/>
širší	široký	k2eAgInSc4d2
přední	přední	k2eAgNnPc4d1
kovaná	kovaný	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
s	s	k7c7
tvrdším	tvrdý	k2eAgNnSc7d2
uložením	uložení	k1gNnSc7
<g/>
,	,	kIx,
silnější	silný	k2eAgMnSc1d2
stabilizátor	stabilizátor	k1gMnSc1
a	a	k8xC
vzadu	vzadu	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
uložení	uložení	k1gNnSc2
ramen	rameno	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
mechaniky	mechanika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
Coupé	Coupý	k2eAgNnSc4d1
byla	být	k5eAaImAgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
menšího	malý	k2eAgInSc2d2
objemu	objem	k1gInSc2
produkce	produkce	k1gFnSc2
a	a	k8xC
mnoha	mnoho	k4c2
odlišností	odlišnost	k1gFnPc2
svěřena	svěřit	k5eAaPmNgFnS
dílnám	dílna	k1gFnPc3
Pininfarina	Pininfarino	k1gNnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
byly	být	k5eAaImAgFnP
vozy	vůz	k1gInPc4
na	na	k7c4
kvalitativně	kvalitativně	k6eAd1
velmi	velmi	k6eAd1
vysoké	vysoký	k2eAgFnSc3d1
úrovni	úroveň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Dvoudveřové	dvoudveřový	k2eAgNnSc1d1
kupé	kupé	k1gNnSc1
nabízí	nabízet	k5eAaImIp3nS
řadu	řada	k1gFnSc4
pozoruhodných	pozoruhodný	k2eAgInPc2d1
detailů	detail	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
umístění	umístění	k1gNnSc1
velkého	velký	k2eAgNnSc2d1
víčka	víčko	k1gNnSc2
nádrže	nádrž	k1gFnSc2
na	na	k7c6
zadním	zadní	k2eAgInSc6d1
blatníku	blatník	k1gInSc6
<g/>
,	,	kIx,
řešení	řešení	k1gNnSc1
zadních	zadní	k2eAgFnPc2d1
svítilen	svítilna	k1gFnPc2
<g/>
,	,	kIx,
umístění	umístění	k1gNnSc1
klik	klika	k1gFnPc2
dveří	dveře	k1gFnPc2
ve	v	k7c6
sloupku	sloupek	k1gInSc6
střechy	střecha	k1gFnSc2
(	(	kIx(
<g/>
obdobně	obdobně	k6eAd1
jako	jako	k8xS,k8xC
později	pozdě	k6eAd2
Alfa	alfa	k1gFnSc1
Romeo	Romeo	k1gMnSc1
156	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
horizontální	horizontální	k2eAgInSc4d1
pás	pás	k1gInSc4
táhnoucí	táhnoucí	k2eAgInSc4d1
se	se	k3xPyFc4
přes	přes	k7c4
celou	celý	k2eAgFnSc4d1
šíři	šíře	k1gFnSc4
přístrojové	přístrojový	k2eAgFnSc2d1
desky	deska	k1gFnSc2
a	a	k8xC
vyvedený	vyvedený	k2eAgMnSc1d1
v	v	k7c6
barvě	barva	k1gFnSc6
karoserie	karoserie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapivě	překvapivě	k6eAd1
velký	velký	k2eAgInSc1d1
je	být	k5eAaImIp3nS
u	u	k7c2
kupé	kupé	k1gNnSc2
zavazadlový	zavazadlový	k2eAgInSc1d1
prostor	prostor	k1gInSc1
svým	svůj	k3xOyFgInSc7
objemem	objem	k1gInSc7
295	#num#	k4
dm³	dm³	k?
a	a	k8xC
vytvořeným	vytvořený	k2eAgInSc7d1
dvoutřetinovým	dvoutřetinový	k2eAgInSc7d1
průchodem	průchod	k1gInSc7
pro	pro	k7c4
objemné	objemný	k2eAgInPc4d1
či	či	k8xC
dlouhé	dlouhý	k2eAgInPc4d1
předměty	předmět	k1gInPc4
pevnou	pevný	k2eAgFnSc7d1
příčkou	příčka	k1gFnSc7
za	za	k7c7
zadními	zadní	k2eAgNnPc7d1
sedadly	sedadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Podvozek	podvozek	k1gInSc1
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgInS
nízkou	nízký	k2eAgFnSc7d1
světlostí	světlost	k1gFnSc7
a	a	k8xC
tuhou	tuhý	k2eAgFnSc7d1
charakteristikou	charakteristika	k1gFnSc7
odpružení	odpružení	k1gNnSc1
<g/>
,	,	kIx,
hřebenové	hřebenový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
s	s	k7c7
hydraulickým	hydraulický	k2eAgInSc7d1
posilovačem	posilovač	k1gInSc7
bylo	být	k5eAaImAgNnS
přesné	přesný	k2eAgNnSc1d1
a	a	k8xC
mělo	mít	k5eAaImAgNnS
ostřejší	ostrý	k2eAgFnSc4d2
charakteristiku	charakteristika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
je	být	k5eAaImIp3nS
konstrukčně	konstrukčně	k6eAd1
poměrně	poměrně	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
a	a	k8xC
levná	levný	k2eAgFnSc1d1
<g/>
,	,	kIx,
autoři	autor	k1gMnPc1
chytře	chytro	k6eAd1
usadili	usadit	k5eAaPmAgMnP
robustní	robustní	k2eAgFnSc4d1
nápravnici	nápravnice	k1gFnSc4
do	do	k7c2
čtveřice	čtveřice	k1gFnSc2
silentbloků	silentblok	k1gInPc2
a	a	k8xC
vlastní	vlastní	k2eAgNnPc4d1
vlečená	vlečený	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
byla	být	k5eAaImAgFnS
uložena	uložen	k2eAgFnSc1d1
"	"	kIx"
<g/>
na	na	k7c4
tvrdo	tvrdo	k1gNnSc4
<g/>
"	"	kIx"
ve	v	k7c6
valivých	valivý	k2eAgNnPc6d1
ložiskách	ložisko	k1gNnPc6
<g/>
,	,	kIx,
takže	takže	k8xS
jízdní	jízdní	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
vozu	vůz	k1gInSc2
byly	být	k5eAaImAgFnP
i	i	k9
díky	díky	k7c3
nízkému	nízký	k2eAgNnSc3d1
těžišti	těžiště	k1gNnSc3
překvapivě	překvapivě	k6eAd1
dobré	dobrý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
uvedení	uvedení	k1gNnSc6
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
lednu	leden	k1gInSc6
1994	#num#	k4
byly	být	k5eAaImAgInP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dva	dva	k4xCgInPc1
dvoulitry	dvoulitr	k1gInPc1
"	"	kIx"
<g/>
Lampredi	Lampred	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
byl	být	k5eAaImAgInS
přepracovaný	přepracovaný	k2eAgInSc1d1
z	z	k7c2
modelu	model	k1gInSc2
Tipo	Tipo	k1gNnSc1
Sedicivalvole	Sedicivalvole	k1gFnSc2
<g/>
,	,	kIx,
robustní	robustní	k2eAgFnSc2d1
čtyřválec	čtyřválec	k1gInSc4
s	s	k7c7
litinovým	litinový	k2eAgInSc7d1
blokem	blok	k1gInSc7
a	a	k8xC
dvojicí	dvojice	k1gFnSc7
protiběžných	protiběžný	k2eAgFnPc2d1
vyvažovacích	vyvažovací	k2eAgFnPc2d1
hřídelí	hřídel	k1gFnPc2
s	s	k7c7
vrtáním	vrtání	k1gNnSc7
84,0	84,0	k4
mm	mm	kA
a	a	k8xC
zdvihem	zdvih	k1gInSc7
90	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
zdvihový	zdvihový	k2eAgInSc1d1
objem	objem	k1gInSc1
1995	#num#	k4
cm³	cm³	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
hlavou	hlava	k1gFnSc7
válců	válec	k1gInPc2
z	z	k7c2
lehkých	lehký	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
a	a	k8xC
ventilovými	ventilový	k2eAgNnPc7d1
sedly	sedlo	k1gNnPc7
z	z	k7c2
tvrdokovů	tvrdokov	k1gInPc2
s	s	k7c7
rozvodem	rozvod	k1gInSc7
DOHC	DOHC	kA
16	#num#	k4
<g/>
v	v	k7c6
a	a	k8xC
elektronikou	elektronika	k1gFnSc7
Bosch	Bosch	kA
Motronic	Motronice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytoval	poskytovat	k5eAaImAgMnS
maximální	maximální	k2eAgInSc4d1
výkon	výkon	k1gInSc4
102	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
139	#num#	k4
koní	kůň	k1gMnPc2
CEE	CEE	kA
<g/>
)	)	kIx)
při	při	k7c6
6000	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
max	max	kA
<g/>
.	.	kIx.
krouticí	krouticí	k2eAgInSc4d1
moment	moment	k1gInSc4
186	#num#	k4
Nm	Nm	k1gFnPc2
při	při	k7c6
4300	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
Druhým	druhý	k4xOgInSc7
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
přeplňovaný	přeplňovaný	k2eAgInSc1d1
turbodmychadlem	turbodmychadlo	k1gNnSc7
Garrett	Garrettum	k1gNnPc2
TBO367	TBO367	k1gFnSc2
a	a	k8xC
špičkovým	špičkový	k2eAgInSc7d1
motormanagementem	motormanagement	k1gInSc7
Weber-Magneti-Marelli	Weber-Magneti-Marell	k1gMnPc1
IAW	IAW	kA
WH	Wh	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
P	P	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
odvozený	odvozený	k2eAgInSc1d1
od	od	k7c2
motoru	motor	k1gInSc2
legendární	legendární	k2eAgFnSc2d1
Lancie	Lancia	k1gFnSc2
Delta	delta	k1gFnSc1
HF	HF	kA
Integrale	Integral	k1gMnSc5
Evoluzione	Evoluzion	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
díky	díky	k7c3
novým	nový	k2eAgInPc3d1
pístům	píst	k1gInPc3
se	s	k7c7
zlepšenou	zlepšená	k1gFnSc7
kompresí	komprese	k1gFnSc7
(	(	kIx(
<g/>
kompresní	kompresní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
8,0	8,0	k4
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
plnicí	plnicí	k2eAgInSc1d1
tlak	tlak	k1gInSc1
1,0	1,0	k4
baru	bar	k1gInSc2
v	v	k7c6
overboostu	overboost	k1gInSc6
<g/>
/	/	kIx~
<g/>
0,7	0,7	k4
baru	bar	k1gInSc2
lineárně	lineárně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
novým	nový	k2eAgInSc7d1
sacím	sací	k2eAgInSc7d1
traktem	trakt	k1gInSc7
poskytoval	poskytovat	k5eAaImAgInS
nejvyšší	vysoký	k2eAgInSc1d3
výkon	výkon	k1gInSc1
140	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
190	#num#	k4
koní	kůň	k1gMnPc2
CEE	CEE	kA
<g/>
)	)	kIx)
při	při	k7c6
5500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
maximální	maximální	k2eAgInSc4d1
krouticí	krouticí	k2eAgInSc4d1
moment	moment	k1gInSc4
291	#num#	k4
Nm	Nm	k1gFnPc2
při	při	k7c6
3400	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
Motor	motor	k1gInSc1
plnil	plnit	k5eAaImAgInS
emisní	emisní	k2eAgFnSc4d1
normu	norma	k1gFnSc4
Euro	euro	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převodovka	převodovka	k1gFnSc1
byla	být	k5eAaImAgFnS
pětistupňová	pětistupňový	k2eAgFnSc1d1
<g/>
,	,	kIx,
plně	plně	k6eAd1
synchronizovaná	synchronizovaný	k2eAgFnSc1d1
s	s	k7c7
řazením	řazení	k1gNnSc7
lanovody	lanovod	k1gInPc7
<g/>
,	,	kIx,
zvláštností	zvláštnost	k1gFnSc7
byla	být	k5eAaImAgFnS
35	#num#	k4
<g/>
%	%	kIx~
uzávěrky	uzávěrka	k1gFnSc2
předního	přední	k2eAgInSc2d1
diferenciálu	diferenciál	k1gInSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
malé	malý	k2eAgFnSc2d1
viskózní	viskózní	k2eAgFnSc2d1
spojky	spojka	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
terminologii	terminologie	k1gFnSc6
Fiatu	fiat	k1gInSc2
Viscodrive	Viscodriev	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fiat	fiat	k1gInSc4
Coupé	Coupý	k2eAgFnSc2d1
16	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
dosahoval	dosahovat	k5eAaImAgInS
nejvyšší	vysoký	k2eAgFnSc3d3
rychlosti	rychlost	k1gFnSc3
230	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
z	z	k7c2
0	#num#	k4
na	na	k7c4
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
dokázal	dokázat	k5eAaPmAgMnS
zrychlit	zrychlit	k5eAaPmF
za	za	k7c4
méně	málo	k6eAd2
než	než	k8xS
7,5	7,5	k4
s	s	k7c7
a	a	k8xC
1	#num#	k4
km	km	kA
s	s	k7c7
pevným	pevný	k2eAgInSc7d1
startem	start	k1gInSc7
zvládl	zvládnout	k5eAaPmAgMnS
za	za	k7c4
26,9	26,9	k4
s.	s.	k?
Jeho	jeho	k3xOp3gFnSc7
slabší	slabý	k2eAgFnSc7d2
stránkou	stránka	k1gFnSc7
byly	být	k5eAaImAgFnP
přední	přední	k2eAgFnPc1d1
brzdy	brzda	k1gFnPc1
Girling	Girling	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
jednopístkovými	jednopístkův	k2eAgInPc7d1
plovoucími	plovoucí	k2eAgInPc7d1
třmeny	třmen	k1gInPc7
a	a	k8xC
kotouči	kotouč	k1gInPc7
284	#num#	k4
x	x	k?
22	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
4	#num#	k4
x	x	k?
98	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
v	v	k7c6
kolech	kolo	k1gNnPc6
6	#num#	k4
x	x	k?
15	#num#	k4
<g/>
"	"	kIx"
z	z	k7c2
lehkých	lehký	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
Speedline	Speedlin	k1gInSc5
(	(	kIx(
<g/>
verze	verze	k1gFnPc1
Plud	Plud	k1gInSc4
byly	být	k5eAaImAgFnP
osazovány	osazován	k2eAgFnPc1d1
koly	kola	k1gFnPc1
Speedline	Speedlin	k1gInSc5
6,5	6,5	k4
x	x	k?
16	#num#	k4
<g/>
"	"	kIx"
a	a	k8xC
pneumatikami	pneumatika	k1gFnPc7
205	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
WR	WR	kA
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1996	#num#	k4
přibyl	přibýt	k5eAaPmAgInS
čtyřválec	čtyřválec	k1gInSc1
1.8	1.8	k4
16	#num#	k4
<g/>
v	v	k7c4
(	(	kIx(
<g/>
Barchetta	Barchetta	k1gFnSc1
<g/>
,	,	kIx,
Lancia	Lancia	k1gFnSc1
Dedra	Dedra	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Delta	delta	k1gFnSc1
836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
opět	opět	k6eAd1
litinový	litinový	k2eAgInSc1d1
blok	blok	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
vyvažovacích	vyvažovací	k2eAgFnPc2d1
hřídelí	hřídel	k1gFnPc2
a	a	k8xC
naopak	naopak	k6eAd1
disponoval	disponovat	k5eAaBmAgInS
variabilním	variabilní	k2eAgNnSc7d1
natáčením	natáčení	k1gNnSc7
sací	sací	k2eAgFnSc2d1
vačky	vačky	k?
(	(	kIx(
<g/>
olejovým	olejový	k2eAgFnPc3d1
<g/>
)	)	kIx)
rozvodu	rozvod	k1gInSc6
DOHC	DOHC	kA
16	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
v	v	k7c6
a	a	k8xC
z	z	k7c2
objemu	objem	k1gInSc2
1747	#num#	k4
cm³	cm³	k?
poskytoval	poskytovat	k5eAaImAgInS
maximální	maximální	k2eAgInSc4d1
výkon	výkon	k1gInSc4
96	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
131	#num#	k4
koní	kůň	k1gMnPc2
CEE	CEE	kA
<g/>
)	)	kIx)
při	při	k7c6
6300	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
max	max	kA
<g/>
.	.	kIx.
krouticí	krouticí	k2eAgInSc4d1
moment	moment	k1gInSc4
164	#num#	k4
Nm	Nm	k1gFnPc2
při	při	k7c6
3500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
Do	do	k7c2
dubna	duben	k1gInSc2
1997	#num#	k4
motor	motor	k1gInSc1
1.8	1.8	k4
16	#num#	k4
<g/>
v	v	k7c6
splňoval	splňovat	k5eAaImAgMnS
emisní	emisní	k2eAgFnSc4d1
normu	norma	k1gFnSc4
Euro	euro	k1gNnSc1
1	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
výroby	výroba	k1gFnSc2
modelu	model	k1gInSc2
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2000	#num#	k4
<g/>
)	)	kIx)
novější	nový	k2eAgFnSc4d2
emisní	emisní	k2eAgFnSc4d1
normu	norma	k1gFnSc4
Euro	euro	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1997	#num#	k4
byly	být	k5eAaImAgFnP
z	z	k7c2
nabídky	nabídka	k1gFnSc2
vyřazeny	vyřadit	k5eAaPmNgInP
oba	dva	k4xCgInPc1
původní	původní	k2eAgInPc1d1
dvoulitrové	dvoulitrový	k2eAgInPc1d1
čtyřválce	čtyřválec	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
nahradily	nahradit	k5eAaPmAgInP
dva	dva	k4xCgInPc1
pětiválcové	pětiválcový	k2eAgInPc1d1
motory	motor	k1gInPc1
2,0	2,0	k4
l	l	kA
"	"	kIx"
<g/>
Pratola	Pratola	k1gFnSc1
Serra	Serra	k1gFnSc1
<g/>
"	"	kIx"
homologované	homologovaný	k2eAgNnSc1d1
pro	pro	k7c4
emisní	emisní	k2eAgFnSc4d1
normu	norma	k1gFnSc4
Euro	euro	k1gNnSc1
2	#num#	k4
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
plněný	plněný	k2eAgInSc4d1
atmosféricky	atmosféricky	k6eAd1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
s	s	k7c7
přeplňováním	přeplňování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blok	blok	k1gInSc1
motoru	motor	k1gInSc2
byl	být	k5eAaImAgInS
opět	opět	k6eAd1
litinový	litinový	k2eAgInSc1d1
a	a	k8xC
opět	opět	k6eAd1
disponoval	disponovat	k5eAaBmAgInS
protiběžným	protiběžný	k2eAgNnSc7d1
vyvažováním	vyvažování	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
pěti	pět	k4xCc7
válci	válec	k1gInPc7
zajišťovalo	zajišťovat	k5eAaImAgNnS
takřka	takřka	k6eAd1
sametový	sametový	k2eAgInSc4d1
chod	chod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtání	vrtání	k1gNnSc1
82	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
zdvih	zdvih	k1gInSc1
75,65	75,65	k4
mm	mm	kA
<g/>
,	,	kIx,
zdvihový	zdvihový	k2eAgInSc1d1
objem	objem	k1gInSc1
1998	#num#	k4
cm³	cm³	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
válců	válec	k1gInPc2
byla	být	k5eAaImAgFnS
vyrobena	vyroben	k2eAgFnSc1d1
z	z	k7c2
lehkých	lehký	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
s	s	k7c7
ventilovými	ventilový	k2eAgNnPc7d1
sedly	sedlo	k1gNnPc7
z	z	k7c2
tvrdokovů	tvrdokov	k1gInPc2
<g/>
,	,	kIx,
rozvod	rozvod	k1gInSc4
DOHC	DOHC	kA
s	s	k7c7
20	#num#	k4
ventily	ventil	k1gInPc7
(	(	kIx(
<g/>
4	#num#	k4
pro	pro	k7c4
každý	každý	k3xTgInSc4
válec	válec	k1gInSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
ovládán	ovládán	k2eAgInSc4d1
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
motorů	motor	k1gInPc2
Lampredi	Lampred	k1gMnPc1
–	–	k?
prostřednictvím	prostřednictvím	k7c2
hydraulických	hydraulický	k2eAgNnPc2d1
ventilových	ventilový	k2eAgNnPc2d1
zdvihátek	zdvihátko	k1gNnPc2
se	s	k7c7
samočinným	samočinný	k2eAgNnSc7d1
vymezováním	vymezování	k1gNnSc7
ventilové	ventilový	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektronika	elektronika	k1gFnSc1
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
Bosch	Bosch	kA
Motronic	Motronice	k1gFnPc2
M	M	kA
<g/>
2.10	2.10	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atmosférický	atmosférický	k2eAgInSc1d1
motor	motor	k1gInSc1
poskytoval	poskytovat	k5eAaImAgInS
nejvyšší	vysoký	k2eAgInSc4d3
výkon	výkon	k1gInSc4
108	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
147	#num#	k4
koní	kůň	k1gMnPc2
CEE	CEE	kA
<g/>
)	)	kIx)
při	při	k7c6
6100	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
největší	veliký	k2eAgInSc4d3
krouticí	krouticí	k2eAgInSc4d1
moment	moment	k1gInSc4
186	#num#	k4
Nm	Nm	k1gFnPc2
při	při	k7c6
3500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
Coupé	Coupý	k2eAgInPc4d1
s	s	k7c7
ním	on	k3xPp3gNnSc7
jelo	jet	k5eAaImAgNnS
215	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
z	z	k7c2
klidu	klid	k1gInSc2
na	na	k7c4
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
zrychlilo	zrychlit	k5eAaPmAgNnS
za	za	k7c4
8,4	8,4	k4
s.	s.	k?
Od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
u	u	k7c2
tohoto	tento	k3xDgInSc2
motoru	motor	k1gInSc2
použito	použit	k2eAgNnSc1d1
variabilní	variabilní	k2eAgNnSc1d1
sání	sání	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
přispělo	přispět	k5eAaPmAgNnS
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
nejvyššího	vysoký	k2eAgInSc2d3
výkonu	výkon	k1gInSc2
na	na	k7c4
113	#num#	k4
kW	kW	kA
při	při	k7c6
6500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Přeplňovaná	přeplňovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
téhož	týž	k3xTgInSc2
pětiválce	pětiválec	k1gInSc2
2.0	2.0	k4
20	#num#	k4
<g/>
v	v	k7c6
s	s	k7c7
turbodmychadlem	turbodmychadlo	k1gNnSc7
Garrett	Garretta	k1gFnPc2
TB2811	TB2811	k1gFnSc2
měla	mít	k5eAaImAgFnS
variabilní	variabilní	k2eAgNnSc4d1
ovládání	ovládání	k1gNnSc4
sací	sací	k2eAgFnSc2d1
vačky	vačky	k?
<g/>
,	,	kIx,
kompresní	kompresní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
8,5	8,5	k4
:	:	kIx,
1	#num#	k4
a	a	k8xC
poskytovala	poskytovat	k5eAaImAgFnS
při	při	k7c6
plnicím	plnicí	k2eAgInSc6d1
tlaku	tlak	k1gInSc6
1,15	1,15	k4
baru	bar	k1gInSc2
v	v	k7c6
overboostu	overboost	k1gInSc6
nejvyšší	vysoký	k2eAgInSc4d3
výkon	výkon	k1gInSc4
162	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
220	#num#	k4
koní	kůň	k1gMnPc2
CEE	CEE	kA
<g/>
)	)	kIx)
při	při	k7c6
5750	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc4d3
krouticí	krouticí	k2eAgInSc4d1
moment	moment	k1gInSc4
310	#num#	k4
Nm	Nm	k1gFnPc2
při	při	k7c6
2500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	min	kA
<g/>
.	.	kIx.
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
přes	přes	k7c4
jednokotoučovou	jednokotoučový	k2eAgFnSc4d1
suchou	suchý	k2eAgFnSc4d1
spojku	spojka	k1gFnSc4
Valeo	Valeo	k6eAd1
a	a	k8xC
pětistupňovou	pětistupňový	k2eAgFnSc4d1
převodovku	převodovka	k1gFnSc4
Fiat	fiat	k1gInSc1
<g/>
/	/	kIx~
<g/>
ZF	ZF	kA
přenášel	přenášet	k5eAaImAgMnS
na	na	k7c4
přední	přední	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
prostřednictvím	prostřednictvím	k7c2
diferenciálu	diferenciál	k1gInSc2
s	s	k7c7
omezenou	omezený	k2eAgFnSc7d1
svornosti	svornost	k1gFnSc6
Viscodrive	Viscodriev	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fiat	fiat	k1gInSc4
Coupé	Coupý	k2eAgFnSc2d1
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
s	s	k7c7
pětistupňovou	pětistupňový	k2eAgFnSc7d1
převodovkou	převodovka	k1gFnSc7
uměl	umět	k5eAaImAgMnS
jet	jet	k5eAaImNgMnS
až	až	k9
243	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
z	z	k7c2
klidu	klid	k1gInSc2
na	na	k7c4
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
dokázal	dokázat	k5eAaPmAgMnS
zrychlit	zrychlit	k5eAaPmF
během	během	k7c2
6,2	6,2	k4
s.	s.	k?
Sprint	sprint	k1gInSc1
na	na	k7c4
1000	#num#	k4
m	m	kA
s	s	k7c7
pevným	pevný	k2eAgInSc7d1
startem	start	k1gInSc7
zvládl	zvládnout	k5eAaPmAgMnS
za	za	k7c4
26,5	26,5	k4
s.	s.	k?
Zvýšení	zvýšení	k1gNnSc1
výkonu	výkon	k1gInSc2
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
zcela	zcela	k6eAd1
novou	nový	k2eAgFnSc4d1
přední	přední	k2eAgFnSc4d1
brzdovou	brzdový	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
Brembo	Bremba	k1gFnSc5
s	s	k7c7
pevnými	pevný	k2eAgInPc7d1
čtyřpístkovými	čtyřpístkův	k2eAgInPc7d1
třmeny	třmen	k1gInPc7
a	a	k8xC
kotouči	kotouč	k1gInPc7
305	#num#	k4
x	x	k?
28	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
4	#num#	k4
x	x	k?
98	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
nim	on	k3xPp3gMnPc3
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
použito	použít	k5eAaPmNgNnS
větších	veliký	k2eAgFnPc6d2
kol	kolo	k1gNnPc2
Speedline	Speedlin	k1gInSc5
z	z	k7c2
lehkých	lehký	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
6,5	6,5	k4
x	x	k?
16	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
mohl	moct	k5eAaImAgMnS
také	také	k9
majitel	majitel	k1gMnSc1
tohoto	tento	k3xDgNnSc2
vozidla	vozidlo	k1gNnSc2
spouštět	spouštět	k5eAaImF
motor	motor	k1gInSc4
tlačítkem	tlačítko	k1gNnSc7
Start	start	k1gInSc1
<g/>
,	,	kIx,
místo	místo	k1gNnSc1
klasickým	klasický	k2eAgFnPc3d1
klíčkema	klíčkem	k1gMnSc2
u	u	k7c2
série	série	k1gFnSc2
Limited	limited	k2eAgInSc4d1
Edition	Edition	k1gInSc4
či	či	k8xC
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
Plus	plus	k1gNnSc7
byla	být	k5eAaImAgFnS
standardem	standard	k1gInSc7
nová	nový	k2eAgFnSc1d1
šestistupňová	šestistupňový	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
díky	díky	k7c3
níž	jenž	k3xRgFnSc3
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
maximální	maximální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
na	na	k7c4
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Vozy	vůz	k1gInPc1
série	série	k1gFnSc2
Limited	limited	k2eAgInSc1d1
Edition	Edition	k1gInSc1
byly	být	k5eAaImAgFnP
označeny	označit	k5eAaPmNgInP
štítkem	štítek	k1gInSc7
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
výrobním	výrobní	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgInSc4
vyrobený	vyrobený	k2eAgInSc4d1
kus	kus	k1gInSc4
vlastnil	vlastnit	k5eAaImAgMnS
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnější	vnější	k2eAgFnPc1d1
odlišnosti	odlišnost	k1gFnPc1
spočívaly	spočívat	k5eAaImAgFnP
v	v	k7c6
doplňcích	doplněk	k1gInPc6
karoserie	karoserie	k1gFnSc2
(	(	kIx(
<g/>
plasty	plast	k1gInPc1
světlometů	světlomet	k1gInPc2
<g/>
,	,	kIx,
maska	maska	k1gFnSc1
chladiče	chladič	k1gInSc2
<g/>
,	,	kIx,
disky	disk	k1gInPc4
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
spoilery	spoiler	k1gInPc4
a	a	k8xC
prahy	práh	k1gInPc4
v	v	k7c6
barvě	barva	k1gFnSc6
karoserie	karoserie	k1gFnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
doplňky	doplněk	k1gInPc4
v	v	k7c6
titanové	titanový	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
<g/>
,	,	kIx,
jiné	jiný	k2eAgNnSc4d1
víčko	víčko	k1gNnSc4
nádrže	nádrž	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
poznávací	poznávací	k2eAgNnSc4d1
znamení	znamení	k1gNnSc4
těchto	tento	k3xDgInPc2
vozů	vůz	k1gInPc2
bylo	být	k5eAaImAgNnS
i	i	k9
typové	typový	k2eAgNnSc1d1
označení	označení	k1gNnSc1
"	"	kIx"
<g/>
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
<g/>
"	"	kIx"
na	na	k7c6
B	B	kA
sloupku	sloupek	k1gInSc2
v	v	k7c6
červené	červený	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
(	(	kIx(
<g/>
ostatní	ostatní	k2eAgFnSc2d1
série	série	k1gFnSc2
v	v	k7c6
chromovém	chromový	k2eAgInSc6d1
eloxu	elox	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
motorovém	motorový	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
najdeme	najít	k5eAaPmIp1nP
rozpěrnou	rozpěrný	k2eAgFnSc4d1
tyč	tyč	k1gFnSc4
SPARCO	SPARCO	kA
a	a	k8xC
rudé	rudý	k2eAgNnSc1d1
víko	víko	k1gNnSc1
hlavy	hlava	k1gFnSc2
válců	válec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1
kožené	kožený	k2eAgFnPc1d1
sedačky	sedačka	k1gFnPc1
nahradily	nahradit	k5eAaPmAgFnP
červeno-černé	červeno-černý	k2eAgFnPc1d1
sedačky	sedačka	k1gFnPc1
RECARO	RECARO	kA
<g/>
,	,	kIx,
podobě	podoba	k1gFnSc6
byla	být	k5eAaImAgFnS
upravena	upravit	k5eAaPmNgFnS
hlavice	hlavice	k1gFnSc1
řadicí	řadicí	k2eAgFnSc2d1
páky	páka	k1gFnSc2
<g/>
,	,	kIx,
ruční	ruční	k2eAgFnSc2d1
brzdy	brzda	k1gFnSc2
<g/>
,	,	kIx,
výplně	výplň	k1gFnSc2
dveří	dveře	k1gFnPc2
a	a	k8xC
volantu	volant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
drobností	drobnost	k1gFnPc2
dále	daleko	k6eAd2
byla	být	k5eAaImAgFnS
jiná	jiný	k2eAgFnSc1d1
opěrka	opěrka	k1gFnSc1
levé	levý	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
řidiče	řidič	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
pedály	pedál	k1gInPc1
od	od	k7c2
firmy	firma	k1gFnSc2
SPARCO	SPARCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
volantu	volant	k1gInSc2
<g/>
,	,	kIx,
napravo	napravo	k6eAd1
<g/>
,	,	kIx,
najdeme	najít	k5eAaPmIp1nP
ještě	ještě	k9
startovací	startovací	k2eAgNnSc4d1
tlačítko	tlačítko	k1gNnSc4
po	po	k7c6
vzoru	vzor	k1gInSc6
dřívějších	dřívější	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
rychlosměr	rychlosměr	k1gInSc1
byl	být	k5eAaImAgInS
ocejchován	ocejchovat	k5eAaPmNgInS
až	až	k6eAd1
do	do	k7c2
280	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
místo	místo	k7c2
původních	původní	k2eAgInPc2d1
260	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgFnSc2d1
zaskočil	zaskočit	k5eAaPmAgMnS
konkurenci	konkurence	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
daném	daný	k2eAgInSc6d1
cenovém	cenový	k2eAgInSc6d1
segmentu	segment	k1gInSc6
neměla	mít	k5eNaImAgFnS
proti	proti	k7c3
přeplňovaným	přeplňovaný	k2eAgFnPc3d1
verzím	verze	k1gFnPc3
modelu	model	k1gInSc2
Coupé	Coupý	k2eAgFnPc1d1
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
konkurovat	konkurovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
očekáváním	očekávání	k1gNnSc7
vůz	vůz	k1gInSc4
překvapil	překvapit	k5eAaPmAgInS
i	i	k9
dobrými	dobrý	k2eAgFnPc7d1
jízdními	jízdní	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
a	a	k8xC
celkovou	celkový	k2eAgFnSc7d1
kvalitou	kvalita	k1gFnSc7
zpracování	zpracování	k1gNnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgFnSc1d2
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
i	i	k9
excelentními	excelentní	k2eAgFnPc7d1
brzdami	brzda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzávěrka	uzávěrka	k1gFnSc1
předního	přední	k2eAgInSc2d1
diferenciálu	diferenciál	k1gInSc2
u	u	k7c2
16	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
a	a	k8xC
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
a	a	k8xC
šestistupňová	šestistupňový	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
20	#num#	k4
turbo	turba	k1gFnSc5
Limited	limited	k2eAgFnPc1d1
Edition	Edition	k1gInSc4
a	a	k8xC
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
Plus	plus	k1gInSc1
byly	být	k5eAaImAgFnP
třešničkami	třešnička	k1gFnPc7
na	na	k7c6
dortu	dort	k1gInSc6
<g/>
,	,	kIx,
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
bylo	být	k5eAaImAgNnS
těžko	těžko	k6eAd1
konkurovat	konkurovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slabší	slabý	k2eAgFnSc7d2
stránkou	stránka	k1gFnSc7
byla	být	k5eAaImAgFnS
menší	malý	k2eAgFnSc1d2
tuhost	tuhost	k1gFnSc1
karoserie	karoserie	k1gFnSc2
v	v	k7c6
krutu	krut	k1gInSc6
<g/>
,	,	kIx,
ventilace	ventilace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nedokázala	dokázat	k5eNaPmAgFnS
zajistit	zajistit	k5eAaPmF
odmlžení	odmlžení	k1gNnSc4
oken	okno	k1gNnPc2
za	za	k7c2
nepříznivého	příznivý	k2eNgNnSc2d1
počasí	počasí	k1gNnSc2
a	a	k8xC
u	u	k7c2
modelu	model	k1gInSc2
16	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
slabší	slabý	k2eAgFnSc1d2
brzdová	brzdový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
modelů	model	k1gInPc2
1.8	1.8	k4
16	#num#	k4
<g/>
v	v	k7c6
bylo	být	k5eAaImAgNnS
nutno	nutno	k6eAd1
včas	včas	k6eAd1
řešit	řešit	k5eAaImF
těsnost	těsnost	k1gFnSc4
olejového	olejový	k2eAgInSc2d1
tlakového	tlakový	k2eAgInSc2d1
variátoru	variátor	k1gInSc2
sací	sací	k2eAgFnSc2d1
vačky	vačky	k?
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
pořizovací	pořizovací	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
i	i	k9
servisní	servisní	k2eAgFnPc4d1
nenáročnosti	nenáročnost	k1gFnPc4
splnil	splnit	k5eAaPmAgInS
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgFnSc2d1
do	do	k7c2
puntíku	puntík	k1gInSc2
zadání	zadání	k1gNnSc2
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
králem	král	k1gMnSc7
levných	levný	k2eAgInPc2d1
lidových	lidový	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
motorizací	motorizace	k1gFnPc2
</s>
<s>
Model	model	k1gInSc1
</s>
<s>
1.8	1.8	k4
16	#num#	k4
<g/>
v	v	k7c6
</s>
<s>
2.0	2.0	k4
16	#num#	k4
<g/>
v	v	k7c6
</s>
<s>
2.0	2.0	k4
20	#num#	k4
<g/>
v	v	k7c6
</s>
<s>
2.0	2.0	k4
20	#num#	k4
<g/>
v	v	k7c6
</s>
<s>
2.0	2.0	k4
16	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
</s>
<s>
2.0	2.0	k4
20	#num#	k4
<g/>
v	v	k7c6
turbo	turba	k1gFnSc5
</s>
<s>
Rok	rok	k1gInSc1
výroby	výroba	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
válců	válec	k1gInPc2
</s>
<s>
4	#num#	k4
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
5	#num#	k4
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
4	#num#	k4
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
5	#num#	k4
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
Druh	druh	k1gInSc1
motoru	motor	k1gInSc2
</s>
<s>
zážehový	zážehový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Otto	Otto	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
zážehový	zážehový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Otto	Otto	k1gMnSc1
<g/>
)	)	kIx)
s	s	k7c7
přeplňováním	přeplňování	k1gNnSc7
</s>
<s>
Objem	objem	k1gInSc1
(	(	kIx(
<g/>
cm³	cm³	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
747	#num#	k4
</s>
<s>
1	#num#	k4
995	#num#	k4
</s>
<s>
1	#num#	k4
998	#num#	k4
</s>
<s>
1	#num#	k4
998	#num#	k4
</s>
<s>
1	#num#	k4
995	#num#	k4
</s>
<s>
1	#num#	k4
998	#num#	k4
</s>
<s>
Kompresní	kompresní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
</s>
<s>
10,3	10,3	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
10,5	10,5	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
10,0	10,0	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
10,7	10,7	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
8,0	8,0	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
8,5	8,5	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Turbodmychadlo	turbodmychadlo	k1gNnSc1
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Garret	Garret	k1gMnSc1
TBO367	TBO367	k1gMnSc1
</s>
<s>
Garret	Garret	k1gMnSc1
TB2811	TB2811	k1gMnSc1
</s>
<s>
Plnící	plnící	k2eAgInSc1d1
tlak	tlak	k1gInSc1
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
1,0	1,0	k4
bar	bar	k1gInSc1
overboost	overboost	k1gInSc4
</s>
<s>
1,15	1,15	k4
bar	bar	k1gInSc1
overboost	overboost	k1gInSc4
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
kW	kW	kA
<g/>
)	)	kIx)
</s>
<s>
98	#num#	k4
při	při	k7c6
6300	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
102	#num#	k4
při	při	k7c6
6000	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
108	#num#	k4
při	při	k7c6
6100	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
113	#num#	k4
při	při	k7c6
6500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
140	#num#	k4
při	při	k7c6
5500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
164	#num#	k4
při	při	k7c6
5750	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
kroutící	kroutící	k2eAgInSc1d1
moment	moment	k1gInSc1
(	(	kIx(
<g/>
Nm	Nm	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
164	#num#	k4
při	při	k7c6
3400	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
186	#num#	k4
při	při	k7c6
4300	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
186	#num#	k4
při	při	k7c6
3500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
186	#num#	k4
při	při	k7c6
3700	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
291	#num#	k4
při	při	k7c6
3400	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
310	#num#	k4
při	při	k7c6
2500	#num#	k4
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
povolená	povolený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
205	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
208	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
212	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
215	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
225	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
243	#num#	k4
<g/>
–	–	k?
<g/>
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Zrychlení	zrychlení	k1gNnSc1
0	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
9,2	9,2	k4
s	s	k7c7
</s>
<s>
9,2	9,2	k4
s	s	k7c7
</s>
<s>
8,9	8,9	k4
s	s	k7c7
</s>
<s>
8,4	8,4	k4
s	s	k7c7
</s>
<s>
7,5	7,5	k4
s	s	k7c7
</s>
<s>
6,3	6,3	k4
s	s	k7c7
</s>
<s>
Pohotovostní	pohotovostní	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
CEE	CEE	kA
<g/>
)	)	kIx)
</s>
<s>
1180	#num#	k4
kg	kg	kA
</s>
<s>
1250	#num#	k4
kg	kg	kA
</s>
<s>
1270	#num#	k4
kg	kg	kA
</s>
<s>
1270	#num#	k4
kg	kg	kA
</s>
<s>
1320	#num#	k4
kg	kg	kA
</s>
<s>
1310	#num#	k4
kg	kg	kA
</s>
<s>
Poměr	poměr	k1gInSc1
hmotnost	hmotnost	k1gFnSc1
<g/>
/	/	kIx~
<g/>
výkon	výkon	k1gInSc1
</s>
<s>
12,00	12,00	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
kW	kW	kA
</s>
<s>
12,25	12,25	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
kW	kW	kA
</s>
<s>
11,65	11,65	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
kW	kW	kA
</s>
<s>
11,00	11,00	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
kW	kW	kA
</s>
<s>
9,43	9,43	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
kW	kW	kA
</s>
<s>
7,99	7,99	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
kW	kW	kA
</s>
<s>
Pneumatiky	pneumatika	k1gFnPc1
</s>
<s>
195	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
R	R	kA
<g/>
15	#num#	k4
<g/>
V	v	k7c6
</s>
<s>
195	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
R	R	kA
<g/>
15	#num#	k4
<g/>
V	v	k7c4
205	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
ZR	ZR	kA
<g/>
15	#num#	k4
</s>
<s>
205	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
R	R	kA
<g/>
15	#num#	k4
<g/>
W	W	kA
</s>
<s>
205	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
R	R	kA
<g/>
15	#num#	k4
<g/>
W	W	kA
</s>
<s>
205	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
ZR	ZR	kA
<g/>
15	#num#	k4
205	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
ZR	ZR	kA
<g/>
16	#num#	k4
</s>
<s>
205	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
R	R	kA
<g/>
16	#num#	k4
<g/>
Y	Y	kA
225	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
<g/>
R	R	kA
<g/>
16	#num#	k4
<g/>
Z	z	k7c2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fiat	fiat	k1gInSc1
Coupé	Coupý	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
≺	≺	k?
předchozí	předchozí	k2eAgFnSc1d1
–	–	k?
Automobily	automobil	k1gInPc4
značky	značka	k1gFnSc2
Fiat	fiat	k1gInSc1
S.	S.	kA
<g/>
p.	p.	k?
<g/>
A.	A.	kA
1980	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
199020002010	#num#	k4
</s>
<s>
0123456789012345678901234567890123456789	#num#	k4
</s>
<s>
Mini	mini	k2eAgFnSc1d1
</s>
<s>
126	#num#	k4
</s>
<s>
Cinquecento	cinquecento	k1gNnSc1
</s>
<s>
Seicento	Seicento	k1gNnSc1
</s>
<s>
500	#num#	k4
</s>
<s>
Panda	panda	k1gFnSc1
I	i	k9
</s>
<s>
Panda	panda	k1gFnSc1
II	II	kA
</s>
<s>
Panda	panda	k1gFnSc1
III	III	kA
</s>
<s>
Malé	Malé	k2eAgInPc1d1
</s>
<s>
127	#num#	k4
</s>
<s>
Uno	Uno	k?
</s>
<s>
Punto	punto	k1gNnSc1
I	i	k9
</s>
<s>
Punto	punto	k1gNnSc1
II	II	kA
</s>
<s>
Punto	punto	k1gNnSc1
III	III	kA
</s>
<s>
Elba	Elba	k6eAd1
</s>
<s>
Palio	Palio	k6eAd1
I	i	k9
</s>
<s>
Palio	Palio	k1gMnSc1
II	II	kA
</s>
<s>
Duna	duna	k1gFnSc1
</s>
<s>
Siena	Siena	k1gFnSc1
</s>
<s>
Albea	Albea	k6eAd1
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
Ritmo	Ritmo	k6eAd1
</s>
<s>
Tipo	Tipo	k6eAd1
</s>
<s>
Bravo	bravo	k6eAd1
I	i	k9
</s>
<s>
Stilo	Stilo	k1gNnSc1
</s>
<s>
Bravo	bravo	k1gMnSc1
II	II	kA
</s>
<s>
Tipo	Tipo	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Egea	Egea	k1gMnSc1
</s>
<s>
Brava	bravo	k1gMnSc4
</s>
<s>
131	#num#	k4
</s>
<s>
Regata	regata	k1gFnSc1
</s>
<s>
Tempra	Tempra	k6eAd1
</s>
<s>
Marea	Marea	k6eAd1
</s>
<s>
Linea	linea	k1gFnSc1
</s>
<s>
Střední	střední	k2eAgFnSc1d1
</s>
<s>
132	#num#	k4
</s>
<s>
Argenta	argentum	k1gNnPc1
</s>
<s>
Croma	Croma	k1gFnSc1
I	i	k9
</s>
<s>
Croma	Croma	k1gFnSc1
II	II	kA
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
</s>
<s>
X	X	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
</s>
<s>
Coupé	Coupý	k2eAgNnSc1d1
</s>
<s>
124	#num#	k4
Sport	sport	k1gInSc1
Spider	Spider	k1gInSc4
</s>
<s>
Barchetta	Barchetta	k1gFnSc1
</s>
<s>
124	#num#	k4
Spider	Spider	k1gInSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
MPV	MPV	kA
</s>
<s>
Idea	idea	k1gFnSc1
</s>
<s>
500L	500L	k4
</s>
<s>
Střední	střední	k2eAgFnSc1d1
MPV	MPV	kA
</s>
<s>
Multipla	Multipnout	k5eAaPmAgFnS
</s>
<s>
Velká	velká	k1gFnSc1
MPV	MPV	kA
</s>
<s>
Ulysse	Ulysse	k6eAd1
I	i	k9
</s>
<s>
Ulysse	Ulysse	k1gFnSc1
II	II	kA
</s>
<s>
Freemont	Freemont	k1gMnSc1
</s>
<s>
Pick-up	Pick-up	k1gMnSc1
</s>
<s>
Strada	strada	k1gFnSc1
</s>
<s>
Off-road	Off-road	k6eAd1
</s>
<s>
Campagnola	Campagnola	k1gFnSc1
</s>
<s>
Crossover	Crossover	k1gMnSc1
</s>
<s>
Sedici	Sedik	k1gMnPc1
</s>
<s>
500X	500X	k4
</s>
<s>
Užitkové	užitkový	k2eAgNnSc1d1
</s>
<s>
Fiorino	Fiorino	k1gNnSc1
I	i	k9
</s>
<s>
Fiorino	Fiorino	k1gNnSc1
II	II	kA
</s>
<s>
Fiorino	Fiorino	k1gNnSc1
III	III	kA
</s>
<s>
Doblò	Doblò	k?
I	i	k9
</s>
<s>
Doblò	Doblò	k?
II	II	kA
</s>
<s>
Daily	Daila	k1gFnPc1
</s>
<s>
Scudo	Scudo	k1gNnSc1
I	i	k9
</s>
<s>
Scudo	Scudo	k1gNnSc1
II	II	kA
</s>
<s>
Ducato	Ducato	k6eAd1
I	i	k9
</s>
<s>
Ducato	Ducato	k6eAd1
II	II	kA
</s>
<s>
Ducato	Ducato	k6eAd1
III	III	kA
</s>
<s>
licence	licence	k1gFnSc1
Iveco	Iveco	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
