<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rygl	Rygl	k?	Rygl
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Polubný	Polubný	k2eAgInSc1d1	Polubný
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
sdruženář	sdruženář	k1gMnSc1	sdruženář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
severské	severský	k2eAgFnSc2d1	severská
kombinace	kombinace	k1gFnSc2	kombinace
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Týž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
anketu	anketa	k1gFnSc4	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
pak	pak	k6eAd1	pak
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rygl	Rygl	k?	Rygl
mladší	mladý	k2eAgFnSc4d2	mladší
se	se	k3xPyFc4	se
také	také	k9	také
věnuje	věnovat	k5eAaImIp3nS	věnovat
severské	severský	k2eAgFnSc3d1	severská
kombinaci	kombinace	k1gFnSc3	kombinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rygl	Rygl	k?	Rygl
st.	st.	kA	st.
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kořenova	Kořenův	k2eAgNnSc2d1	Kořenův
<g/>
,	,	kIx,	,
vsi	ves	k1gFnSc2	ves
která	který	k3yIgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
lyžařského	lyžařský	k2eAgNnSc2d1	lyžařské
střediska	středisko	k1gNnSc2	středisko
Harrachova	Harrachov	k1gInSc2	Harrachov
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
sportovně	sportovně	k6eAd1	sportovně
založený	založený	k2eAgInSc4d1	založený
a	a	k8xC	a
tak	tak	k6eAd1	tak
vedle	vedle	k7c2	vedle
sportování	sportování	k1gNnSc2	sportování
a	a	k8xC	a
pořádání	pořádání	k1gNnSc2	pořádání
lyžařských	lyžařský	k2eAgInPc2d1	lyžařský
závodů	závod	k1gInPc2	závod
pomohl	pomoct	k5eAaPmAgInS	pomoct
poměr	poměr	k1gInSc1	poměr
ke	k	k7c3	k
sportu	sport	k1gInSc3	sport
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
lyžařskému	lyžařský	k2eAgMnSc3d1	lyžařský
<g/>
,	,	kIx,	,
přenést	přenést	k5eAaPmF	přenést
i	i	k9	i
na	na	k7c4	na
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
též	též	k9	též
jeho	jeho	k3xOp3gInSc1	jeho
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
trenérem	trenér	k1gMnSc7	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
v	v	k7c6	v
Kořenově	kořenově	k6eAd1	kořenově
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
lyžařských	lyžařský	k2eAgInPc6d1	lyžařský
závodech	závod	k1gInPc6	závod
dostávali	dostávat	k5eAaImAgMnP	dostávat
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
závodníci	závodník	k1gMnPc1	závodník
sportovní	sportovní	k2eAgFnSc2d1	sportovní
třídy	třída	k1gFnSc2	třída
volno	volno	k6eAd1	volno
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
sledovat	sledovat	k5eAaImF	sledovat
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
světové	světový	k2eAgMnPc4d1	světový
závodníky	závodník	k1gMnPc4	závodník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
nastartovalo	nastartovat	k5eAaPmAgNnS	nastartovat
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgInSc4d1	aktivní
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přestěhováním	přestěhování	k1gNnSc7	přestěhování
do	do	k7c2	do
Tanvaldu	Tanvald	k1gInSc2	Tanvald
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
FTVS	FTVS	kA	FTVS
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
závodil	závodit	k5eAaImAgMnS	závodit
za	za	k7c4	za
Sl.	Sl.	k1gFnSc4	Sl.
Praha	Praha	k1gFnSc1	Praha
VŠ	vš	k0	vš
až	až	k9	až
do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
skupiny	skupina	k1gFnPc1	skupina
sdruženářů	sdruženář	k1gMnPc2	sdruženář
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
trenér	trenér	k1gMnSc1	trenér
B.	B.	kA	B.
Rázl	Rázl	k1gMnSc1	Rázl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
prvním	první	k4xOgInSc7	první
větším	veliký	k2eAgInSc7d2	veliký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
umístění	umístění	k1gNnSc1	umístění
Ladislava	Ladislav	k1gMnSc2	Ladislav
Rygla	Rygl	k1gMnSc2	Rygl
na	na	k7c6	na
ME	ME	kA	ME
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
Murau	Muraus	k1gInSc6	Muraus
1967	[number]	k4	1967
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	s	k7c7	s
2	[number]	k4	2
<g/>
×	×	k?	×
ZOH	ZOH	kA	ZOH
(	(	kIx(	(
<g/>
Grenoble	Grenoble	k1gInSc1	Grenoble
a	a	k8xC	a
Sapporo	Sappora	k1gFnSc5	Sappora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
aktivní	aktivní	k2eAgMnSc1d1	aktivní
závodník	závodník	k1gMnSc1	závodník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k8xC	jako
trenér	trenér	k1gMnSc1	trenér
ještě	ještě	k9	ještě
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
1	[number]	k4	1
<g/>
×	×	k?	×
MS	MS	kA	MS
–	–	k?	–
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
umístění	umístění	k1gNnSc2	umístění
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
nebylo	být	k5eNaImAgNnS	být
tolik	tolik	k4xDc1	tolik
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
závodů	závod	k1gInPc2	závod
jako	jako	k9	jako
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
MS	MS	kA	MS
se	se	k3xPyFc4	se
pořádalo	pořádat	k5eAaImAgNnS	pořádat
jako	jako	k8xS	jako
ZOH	ZOH	kA	ZOH
1	[number]	k4	1
<g/>
×	×	k?	×
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
na	na	k7c6	na
programu	program	k1gInSc6	program
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
umístil	umístit	k5eAaPmAgMnS	umístit
ve	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
žebříčku	žebříček	k1gInSc6	žebříček
kombiňáků	kombiňák	k1gMnPc2	kombiňák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
vlastně	vlastně	k9	vlastně
předchůdcem	předchůdce	k1gMnSc7	předchůdce
hodnocení	hodnocení	k1gNnSc4	hodnocení
světových	světový	k2eAgInPc2d1	světový
pohárů	pohár	k1gInPc2	pohár
<g/>
,	,	kIx,	,
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
závodech	závod	k1gInPc6	závod
sbíral	sbírat	k5eAaImAgInS	sbírat
důstojná	důstojný	k2eAgNnPc4d1	důstojné
umístění	umístění	k1gNnSc4	umístění
<g/>
,	,	kIx,	,
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
m.	m.	k?	m.
<g/>
j.	j.	k?	j.
stojí	stát	k5eAaImIp3nS	stát
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Holmenkollenu	Holmenkollen	k1gInSc6	Holmenkollen
(	(	kIx(	(
<g/>
výsledek	výsledek	k1gInSc1	výsledek
zatím	zatím	k6eAd1	zatím
od	od	k7c2	od
českých	český	k2eAgMnPc2d1	český
závodníků	závodník	k1gMnPc2	závodník
nepřekonaný	překonaný	k2eNgMnSc1d1	nepřekonaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Falun	Falun	k1gInSc1	Falun
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Reit	Reit	k2eAgInSc4d1	Reit
im	im	k?	im
Winkl	Winkl	k1gInSc4	Winkl
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
Lahti	Lahti	k1gNnSc1	Lahti
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
Universiáda	universiáda	k1gFnSc1	universiáda
v	v	k7c6	v
Lake	Lake	k1gFnSc6	Lake
Placid	Placida	k1gFnPc2	Placida
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
sportovci	sportovec	k1gMnPc1	sportovec
dostávali	dostávat	k5eAaImAgMnP	dostávat
dříve	dříve	k6eAd2	dříve
ohodnocení	ohodnocení	k1gNnSc4	ohodnocení
za	za	k7c4	za
sportovní	sportovní	k2eAgInPc4d1	sportovní
výsledky	výsledek	k1gInPc4	výsledek
víceméně	víceméně	k9	víceméně
jenom	jenom	k6eAd1	jenom
čestná	čestný	k2eAgFnSc1d1	čestná
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
považuje	považovat	k5eAaImIp3nS	považovat
i	i	k9	i
titulu	titul	k1gInSc2	titul
Zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
mistr	mistr	k1gMnSc1	mistr
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
a	a	k8xC	a
který	který	k3yQgMnSc1	který
již	již	k9	již
žádný	žádný	k3yNgMnSc1	žádný
ze	z	k7c2	z
sdruženářů	sdruženář	k1gMnPc2	sdruženář
této	tento	k3xDgFnSc2	tento
a	a	k8xC	a
dřívější	dřívější	k2eAgFnSc2d1	dřívější
doby	doba	k1gFnSc2	doba
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sportovní	sportovní	k2eAgInPc4d1	sportovní
výkony	výkon	k1gInPc4	výkon
byl	být	k5eAaImAgMnS	být
též	též	k6eAd1	též
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
sportovcem	sportovec	k1gMnSc7	sportovec
ČSSR	ČSSR	kA	ČSSR
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
MS	MS	kA	MS
1970	[number]	k4	1970
utrpěl	utrpět	k5eAaPmAgInS	utrpět
úraz	úraz	k1gInSc1	úraz
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
operace	operace	k1gFnSc1	operace
meziobratlové	meziobratlový	k2eAgFnSc2d1	meziobratlová
ploténky	ploténka	k1gFnSc2	ploténka
znamenala	znamenat	k5eAaImAgFnS	znamenat
pauzu	pauza	k1gFnSc4	pauza
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
v	v	k7c6	v
tréninku	trénink	k1gInSc6	trénink
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ukončení	ukončení	k1gNnSc2	ukončení
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
pak	pak	k6eAd1	pak
jako	jako	k9	jako
trenér	trenér	k1gMnSc1	trenér
sportovních	sportovní	k2eAgFnPc2d1	sportovní
tříd	třída	k1gFnPc2	třída
v	v	k7c6	v
Tanvaldě	Tanvalda	k1gFnSc6	Tanvalda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
a	a	k8xC	a
trenérem	trenér	k1gMnSc7	trenér
Střediska	středisko	k1gNnSc2	středisko
vrcholového	vrcholový	k2eAgInSc2d1	vrcholový
sportu	sport	k1gInSc2	sport
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
mnoho	mnoho	k4c1	mnoho
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
i	i	k9	i
jako	jako	k8xS	jako
trenér	trenér	k1gMnSc1	trenér
juniorského	juniorský	k2eAgNnSc2d1	juniorské
družstva	družstvo	k1gNnSc2	družstvo
sdruženářů	sdruženář	k1gMnPc2	sdruženář
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neuváženém	uvážený	k2eNgInSc6d1	neuvážený
rušení	rušení	k1gNnSc2	rušení
těchto	tento	k3xDgNnPc2	tento
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
Středisek	středisko	k1gNnPc2	středisko
vrcholového	vrcholový	k2eAgInSc2d1	vrcholový
sportu	sport	k1gInSc2	sport
ČSTV	ČSTV	kA	ČSTV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
celkem	celek	k1gInSc7	celek
5	[number]	k4	5
let	léto	k1gNnPc2	léto
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
trenér	trenér	k1gMnSc1	trenér
u	u	k7c2	u
B-družstva	Bružstvo	k1gNnSc2	B-družstvo
a	a	k8xC	a
potom	potom	k6eAd1	potom
u	u	k7c2	u
A-družstva	Aružstvo	k1gNnSc2	A-družstvo
sdruženářů	sdruženář	k1gMnPc2	sdruženář
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
trenér	trenér	k1gMnSc1	trenér
i	i	k9	i
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
nejlepším	dobrý	k2eAgNnSc6d3	nejlepší
zařízení	zařízení	k1gNnSc6	zařízení
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgFnSc4d1	sportovní
přípravu	příprava	k1gFnSc4	příprava
u	u	k7c2	u
lyžařů	lyžař	k1gMnPc2	lyžař
reprezentantů	reprezentant	k1gMnPc2	reprezentant
v	v	k7c6	v
Dukle	Dukla	k1gFnSc6	Dukla
Liberec	Liberec	k1gInSc4	Liberec
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
reprezentačního	reprezentační	k2eAgMnSc2d1	reprezentační
trenéra	trenér	k1gMnSc2	trenér
severské	severský	k2eAgFnSc2d1	severská
kombinace	kombinace	k1gFnSc2	kombinace
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
sportovním	sportovní	k2eAgInSc6d1	sportovní
růstu	růst	k1gInSc6	růst
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Ladislava	Ladislav	k1gMnSc4	Ladislav
<g/>
,	,	kIx,	,
závodníka	závodník	k1gMnSc4	závodník
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
kombinaci	kombinace	k1gFnSc6	kombinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rygl	Rygl	k?	Rygl
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
federace	federace	k1gFnSc2	federace
</s>
</p>
