<s>
Provozovatelem	provozovatel	k1gMnSc7
dráhy	dráha	k1gFnSc2
i	i	k8xC
drážní	drážní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
ní	on	k3xPp3gFnSc6
je	být	k5eAaImIp3nS
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
První	první	k4xOgInSc4
úsek	úsek	k1gInSc4
pražského	pražský	k2eAgNnSc2d1
metra	metro	k1gNnSc2
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
9	[number]	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1974	[number]	k4
(	(	kIx(
<g/>
úsek	úsek	k1gInSc1
Florenc	Florenc	k1gFnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Sokolovská	sokolovský	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Kačerov	Kačerov	k1gInSc1
trasy	trasa	k1gFnSc2
C	C	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
poslední	poslední	k2eAgInSc1d1
úsek	úsek	k1gInSc1
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
6	[number]	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	[number]	k4
(	(	kIx(
<g/>
prodloužení	prodloužení	k1gNnSc3
trasy	trasa	k1gFnSc2
A	a	k9
v	v	k7c6
úseku	úsek	k1gInSc6
Dejvická	dejvický	k2eAgFnSc1d1
<g/>
–	–	k?
Nemocnice	nemocnice	k1gFnSc1
Motol	Motol	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>