<s>
Salvatore	Salvator	k1gMnSc5	Salvator
Quasimodo	Quasimodo	k1gMnSc5	Quasimodo
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Modica	Modica	k1gFnSc1	Modica
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
kvůli	kvůli	k7c3	kvůli
otcovu	otcův	k2eAgNnSc3d1	otcovo
železničářství	železničářství	k1gNnSc3	železničářství
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Messiny	Messina	k1gFnSc2	Messina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydleli	bydlet	k5eAaImAgMnP	bydlet
v	v	k7c6	v
odstaveném	odstavený	k2eAgInSc6d1	odstavený
železničním	železniční	k2eAgInSc6d1	železniční
voze	vůz	k1gInSc6	vůz
na	na	k7c6	na
slepé	slepý	k2eAgFnSc6d1	slepá
koleji	kolej	k1gFnSc6	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
povinné	povinný	k2eAgFnSc2d1	povinná
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
začal	začít	k5eAaPmAgMnS	začít
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
psát	psát	k5eAaImF	psát
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
prózu	próza	k1gFnSc4	próza
<g/>
.	.	kIx.	.
</s>
<s>
Společečně	Společečně	k6eAd1	Společečně
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
založil	založit	k5eAaPmAgMnS	založit
časopis	časopis	k1gInSc1	časopis
Nuovo	Nuovo	k1gNnSc4	Nuovo
Giornale	Giornale	k1gMnPc2	Giornale
Letterario	Letterario	k1gMnSc1	Letterario
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
však	však	k9	však
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jen	jen	k9	jen
málo	málo	k4c1	málo
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
diplomu	diplom	k1gInSc2	diplom
na	na	k7c6	na
matematicko-fyzikálním	matematickoyzikální	k2eAgInSc6d1	matematicko-fyzikální
technickém	technický	k2eAgInSc6d1	technický
institutu	institut	k1gInSc6	institut
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
–	–	k?	–
polnohospodářskou	polnohospodářský	k2eAgFnSc4d1	polnohospodářský
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Zlé	zlé	k1gNnSc1	zlé
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
poměry	poměra	k1gFnSc2	poměra
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
zabránily	zabránit	k5eAaPmAgInP	zabránit
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
různé	různý	k2eAgFnSc3d1	různá
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
technický	technický	k2eAgMnSc1d1	technický
kreslič	kreslič	k1gMnSc1	kreslič
a	a	k8xC	a
jako	jako	k8xS	jako
prodavač	prodavač	k1gMnSc1	prodavač
v	v	k7c6	v
železářství	železářství	k1gNnSc6	železářství
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
Acque	Acque	k1gFnSc4	Acque
e	e	k0	e
terre	terr	k1gInSc5	terr
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
druhá	druhý	k4xOgFnSc1	druhý
sbírka	sbírka	k1gFnSc1	sbírka
Oboe	oboe	k1gNnSc2	oboe
sommerso	sommersa	k1gFnSc5	sommersa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
poznal	poznat	k5eAaPmAgMnS	poznat
mnoho	mnoho	k6eAd1	mnoho
tehdy	tehdy	k6eAd1	tehdy
významných	významný	k2eAgMnPc2d1	významný
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Eugenio	Eugenia	k1gMnSc5	Eugenia
Montale	Montal	k1gMnSc5	Montal
<g/>
,	,	kIx,	,
Elio	Elio	k6eAd1	Elio
Vittorini	Vittorin	k1gMnPc1	Vittorin
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
začal	začít	k5eAaPmAgMnS	začít
působit	působit	k5eAaImF	působit
na	na	k7c6	na
tamní	tamní	k2eAgFnSc6d1	tamní
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
první	první	k4xOgFnSc2	první
manželky	manželka	k1gFnSc2	manželka
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
40	[number]	k4	40
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Salvatore	Salvator	k1gMnSc5	Salvator
Quasimodo	Quasimodo	k1gMnSc5	Quasimodo
Acque	Acquus	k1gMnSc5	Acquus
e	e	k0	e
terre	terr	k1gInSc5	terr
–	–	k?	–
1930	[number]	k4	1930
Oboe	oboe	k1gNnSc2	oboe
sommerso	sommersa	k1gFnSc5	sommersa
–	–	k?	–
1932	[number]	k4	1932
Erato	Erato	k1gNnSc4	Erato
e	e	k0	e
Apollion	Apollion	k1gInSc4	Apollion
–	–	k?	–
1936	[number]	k4	1936
Poesie	poesie	k1gFnSc1	poesie
(	(	kIx(	(
<g/>
antologie	antologie	k1gFnSc1	antologie
<g/>
)	)	kIx)	)
–	–	k?	–
1938	[number]	k4	1938
Lirici	Lirice	k1gFnSc6	Lirice
greci	grece	k1gFnSc6	grece
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
)	)	kIx)	)
–	–	k?	–
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
Ed	Ed	k1gFnSc1	Ed
è	è	k?	è
subito	subito	k6eAd1	subito
sera	srát	k5eAaImSgInS	srát
–	–	k?	–
1942	[number]	k4	1942
Giorno	Giorno	k1gNnSc4	Giorno
dopo	dopo	k6eAd1	dopo
giorno	giorno	k1gNnSc4	giorno
–	–	k?	–
1947	[number]	k4	1947
La	la	k1gNnPc2	la
vita	vít	k5eAaImNgFnS	vít
non	non	k?	non
è	è	k?	è
un	un	k?	un
sogno	sogno	k1gNnSc4	sogno
–	–	k?	–
1949	[number]	k4	1949
Il	Il	k1gFnSc1	Il
falso	falsa	k1gFnSc5	falsa
e	e	k0	e
vero	vero	k1gMnSc1	vero
verde	verde	k6eAd1	verde
–	–	k?	–
1954	[number]	k4	1954
La	la	k1gNnSc2	la
terra	terra	k6eAd1	terra
impareggiabile	impareggiabile	k6eAd1	impareggiabile
–	–	k?	–
1958	[number]	k4	1958
Dare	dar	k1gInSc5	dar
e	e	k0	e
avere	aver	k1gInSc5	aver
–	–	k?	–
1966	[number]	k4	1966
</s>
