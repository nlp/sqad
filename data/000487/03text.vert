<s>
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
známá	známý	k2eAgFnSc1d1	známá
přirozená	přirozený	k2eAgFnSc1d1	přirozená
družice	družice	k1gFnSc1	družice
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
formální	formální	k2eAgNnSc4d1	formální
jméno	jméno	k1gNnSc4	jméno
než	než	k8xS	než
"	"	kIx"	"
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odborně	odborně	k6eAd1	odborně
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
básnicky	básnicky	k6eAd1	básnicky
nazýván	nazývat	k5eAaImNgInS	nazývat
Luna	luna	k1gFnSc1	luna
(	(	kIx(	(
<g/>
slovanský	slovanský	k2eAgMnSc1d1	slovanský
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
latinský	latinský	k2eAgInSc1d1	latinský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
symbolem	symbol	k1gInSc7	symbol
je	být	k5eAaImIp3nS	být
srpek	srpek	k1gInSc4	srpek
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
:	:	kIx,	:
☾	☾	k?	☾
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
slova	slovo	k1gNnSc2	slovo
lunární	lunární	k2eAgFnSc1d1	lunární
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
starořecké	starořecký	k2eAgFnSc2d1	starořecká
bohyně	bohyně	k1gFnSc2	bohyně
Měsíce	měsíc	k1gInSc2	měsíc
Seléné	Seléná	k1gFnSc2	Seléná
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
odkazu	odkaz	k1gInSc3	odkaz
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
též	též	k6eAd1	též
kmene	kmen	k1gInSc2	kmen
selene	selen	k1gInSc5	selen
nebo	nebo	k8xC	nebo
seleno	selen	k2eAgNnSc1d1	Seleno
(	(	kIx(	(
<g/>
selenocentrický	selenocentrický	k2eAgMnSc1d1	selenocentrický
<g/>
,	,	kIx,	,
Selenité	Selenitý	k2eAgNnSc1d1	Selenitý
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Měsíce	měsíc	k1gInSc2	měsíc
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
384403	[number]	k4	384403
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
průměr	průměr	k1gInSc1	průměr
činí	činit	k5eAaImIp3nS	činit
3476	[number]	k4	3476
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
přistáli	přistát	k5eAaImAgMnP	přistát
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Edwin	Edwin	k2eAgInSc1d1	Edwin
Aldrin	aldrin	k1gInSc1	aldrin
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
i	i	k9	i
prvními	první	k4xOgMnPc7	první
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jiného	jiný	k2eAgNnSc2d1	jiné
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
Měsíc	měsíc	k1gInSc4	měsíc
zatím	zatím	k6eAd1	zatím
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
dvanáct	dvanáct	k4xCc1	dvanáct
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
synchronní	synchronní	k2eAgFnSc6d1	synchronní
rotaci	rotace	k1gFnSc6	rotace
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
obrácená	obrácený	k2eAgFnSc1d1	obrácená
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
odvrácenou	odvrácený	k2eAgFnSc4d1	odvrácená
stranu	strana	k1gFnSc4	strana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
nelze	lze	k6eNd1	lze
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
malých	malý	k2eAgFnPc2d1	malá
částí	část	k1gFnPc2	část
poblíž	poblíž	k7c2	poblíž
okraje	okraj	k1gInSc2	okraj
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
příležitostně	příležitostně	k6eAd1	příležitostně
spatřeny	spatřit	k5eAaPmNgFnP	spatřit
díky	díky	k7c3	díky
libraci	librace	k1gFnSc3	librace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
odvrácené	odvrácený	k2eAgFnSc2d1	odvrácená
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
éry	éra	k1gFnSc2	éra
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
zcela	zcela	k6eAd1	zcela
neznámá	známý	k2eNgNnPc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
synchronní	synchronní	k2eAgFnSc1d1	synchronní
rotace	rotace	k1gFnSc1	rotace
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zpomalovaly	zpomalovat	k5eAaImAgFnP	zpomalovat
rotaci	rotace	k1gFnSc4	rotace
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rané	raný	k2eAgFnSc6d1	raná
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rezonanci	rezonance	k1gFnSc3	rezonance
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
rotace	rotace	k1gFnSc2	rotace
(	(	kIx(	(
<g/>
vázané	vázaný	k2eAgFnSc6d1	vázaná
rotaci	rotace	k1gFnSc6	rotace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
nazývána	nazývat	k5eAaImNgFnS	nazývat
také	také	k9	také
"	"	kIx"	"
<g/>
temnou	temný	k2eAgFnSc7d1	temná
stranou	strana	k1gFnSc7	strana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Temná	temnat	k5eAaImIp3nS	temnat
<g/>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
neznámá	známý	k2eNgNnPc1d1	neznámé
a	a	k8xC	a
skrytá	skrytý	k2eAgNnPc1d1	skryté
<g/>
"	"	kIx"	"
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
"	"	kIx"	"
<g/>
postrádající	postrádající	k2eAgNnSc1d1	postrádající
světlo	světlo	k1gNnSc1	světlo
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
přijímá	přijímat	k5eAaImIp3nS	přijímat
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
zhruba	zhruba	k6eAd1	zhruba
stejné	stejný	k2eAgNnSc4d1	stejné
množství	množství	k1gNnSc4	množství
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xS	jako
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
odříznuta	odříznout	k5eAaPmNgFnS	odříznout
od	od	k7c2	od
přímé	přímý	k2eAgFnSc2d1	přímá
radiové	radiový	k2eAgFnSc2d1	radiová
komunikace	komunikace	k1gFnSc2	komunikace
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Odlišujícím	odlišující	k2eAgInSc7d1	odlišující
rysem	rys	k1gInSc7	rys
odvrácené	odvrácený	k2eAgFnSc2d1	odvrácená
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
úplná	úplný	k2eAgFnSc1d1	úplná
absence	absence	k1gFnSc1	absence
tmavých	tmavý	k2eAgFnPc2d1	tmavá
skvrn	skvrna	k1gFnPc2	skvrna
(	(	kIx(	(
<g/>
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
albedem	albed	k1gInSc7	albed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
moří	mořit	k5eAaImIp3nP	mořit
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
vykoná	vykonat	k5eAaPmIp3nS	vykonat
kompletní	kompletní	k2eAgInSc4d1	kompletní
oběh	oběh	k1gInSc4	oběh
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
29,530	[number]	k4	29,530
<g/>
588	[number]	k4	588
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
synodický	synodický	k2eAgInSc1d1	synodický
měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
posune	posunout	k5eAaPmIp3nS	posunout
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
o	o	k7c4	o
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
zhruba	zhruba	k6eAd1	zhruba
rovnou	rovnou	k6eAd1	rovnou
jeho	jeho	k3xOp3gInSc3	jeho
úhlovému	úhlový	k2eAgInSc3d1	úhlový
průměru	průměr	k1gInSc3	průměr
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
0,5	[number]	k4	0,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
satelitů	satelit	k1gInPc2	satelit
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
orbita	orbita	k1gFnSc1	orbita
blízká	blízký	k2eAgFnSc1d1	blízká
rovině	rovina	k1gFnSc3	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
rovině	rovina	k1gFnSc6	rovina
zemského	zemský	k2eAgInSc2d1	zemský
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
způsoby	způsob	k1gInPc1	způsob
nazírání	nazírání	k1gNnSc2	nazírání
na	na	k7c4	na
oběh	oběh	k1gInSc4	oběh
jsou	být	k5eAaImIp3nP	být
podrobněji	podrobně	k6eAd2	podrobně
probrány	probrat	k5eAaPmNgInP	probrat
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dva	dva	k4xCgMnPc1	dva
nejběžnější	běžný	k2eAgMnPc1d3	Nejběžnější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
siderický	siderický	k2eAgInSc4d1	siderický
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
úplného	úplný	k2eAgInSc2d1	úplný
oběhu	oběh	k1gInSc2	oběh
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInPc1d1	trvající
asi	asi	k9	asi
27,3	[number]	k4	27,3
dnů	den	k1gInPc2	den
a	a	k8xC	a
synodický	synodický	k2eAgInSc4d1	synodický
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zabere	zabrat	k5eAaPmIp3nS	zabrat
dosažení	dosažení	k1gNnSc3	dosažení
téže	týž	k3xTgFnSc2	týž
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přibližně	přibližně	k6eAd1	přibližně
29,5	[number]	k4	29,5
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
oběhu	oběh	k1gInSc2	oběh
urazí	urazit	k5eAaPmIp3nP	urazit
Země	zem	k1gFnPc1	zem
i	i	k8xC	i
Měsíc	měsíc	k1gInSc4	měsíc
určitou	určitý	k2eAgFnSc4d1	určitá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
na	na	k7c6	na
orbitě	orbita	k1gFnSc6	orbita
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Měsíc	měsíc	k1gInSc1	měsíc
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
nejlépe	dobře	k6eAd3	dobře
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
na	na	k7c4	na
střídání	střídání	k1gNnSc4	střídání
mořského	mořský	k2eAgInSc2d1	mořský
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
je	být	k5eAaImIp3nS	být
synchronizována	synchronizovat	k5eAaBmNgFnS	synchronizovat
s	s	k7c7	s
oběhem	oběh	k1gInSc7	oběh
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Slapová	slapový	k2eAgNnPc1d1	slapové
vzdutí	vzdutí	k1gNnPc1	vzdutí
Země	zem	k1gFnSc2	zem
způsobená	způsobený	k2eAgFnSc1d1	způsobená
měsíční	měsíční	k2eAgFnSc7d1	měsíční
gravitací	gravitace	k1gFnSc7	gravitace
se	se	k3xPyFc4	se
zpožďují	zpožďovat	k5eAaImIp3nP	zpožďovat
za	za	k7c7	za
odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
polohou	poloha	k1gFnSc7	poloha
Měsíce	měsíc	k1gInSc2	měsíc
kvůli	kvůli	k7c3	kvůli
odporu	odpor	k1gInSc3	odpor
oceánského	oceánský	k2eAgInSc2d1	oceánský
systému	systém	k1gInSc2	systém
-	-	kIx~	-
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
setrvačnosti	setrvačnost	k1gFnSc3	setrvačnost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
přelévá	přelévat	k5eAaImIp3nS	přelévat
přes	přes	k7c4	přes
oceánské	oceánský	k2eAgNnSc4d1	oceánské
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
zálivů	záliv	k1gInPc2	záliv
a	a	k8xC	a
ústí	ústí	k1gNnSc2	ústí
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
zase	zase	k9	zase
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
mořského	mořský	k2eAgInSc2d1	mořský
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
ke	k	k7c3	k
vdmutí	vdmutí	k1gNnSc3	vdmutí
a	a	k8xC	a
poklesu	pokles	k1gInSc3	pokles
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
zemského	zemský	k2eAgInSc2d1	zemský
rotačního	rotační	k2eAgInSc2d1	rotační
momentu	moment	k1gInSc2	moment
pozvolna	pozvolna	k6eAd1	pozvolna
přeměňována	přeměňován	k2eAgFnSc1d1	přeměňována
do	do	k7c2	do
oběhového	oběhový	k2eAgInSc2d1	oběhový
momentu	moment	k1gInSc2	moment
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
pomalu	pomalu	k6eAd1	pomalu
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
rychlostí	rychlost	k1gFnPc2	rychlost
asi	asi	k9	asi
38	[number]	k4	38
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
stejných	stejný	k2eAgFnPc2d1	stejná
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
o	o	k7c4	o
1,7	[number]	k4	1,7
milisekundy	milisekunda	k1gFnPc4	milisekunda
za	za	k7c4	za
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
tohoto	tento	k3xDgInSc2	tento
úbytku	úbytek	k1gInSc2	úbytek
hybnosti	hybnost	k1gFnSc2	hybnost
je	být	k5eAaImIp3nS	být
předána	předán	k2eAgFnSc1d1	předána
Měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Synchronnost	synchronnost	k1gFnSc1	synchronnost
rotace	rotace	k1gFnSc1	rotace
je	být	k5eAaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měsíční	měsíční	k2eAgFnSc1d1	měsíční
orbita	orbita	k1gFnSc1	orbita
má	mít	k5eAaImIp3nS	mít
jistou	jistý	k2eAgFnSc4d1	jistá
výstřednost	výstřednost	k1gFnSc4	výstřednost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
perigeu	perigeum	k1gNnSc6	perigeum
(	(	kIx(	(
<g/>
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
rotace	rotace	k1gFnSc1	rotace
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
než	než	k8xS	než
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nám	my	k3xPp1nPc3	my
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vidět	vidět	k5eAaImF	vidět
asi	asi	k9	asi
osm	osm	k4xCc4	osm
stupňů	stupeň	k1gInPc2	stupeň
délky	délka	k1gFnSc2	délka
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
východní	východní	k2eAgFnSc2d1	východní
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgFnSc2d1	pravá
<g/>
)	)	kIx)	)
strany	strana	k1gFnPc1	strana
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
apogea	apogeum	k1gNnSc2	apogeum
(	(	kIx(	(
<g/>
odzemí	odzemí	k1gNnSc2	odzemí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
rotace	rotace	k1gFnSc1	rotace
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
než	než	k8xS	než
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc4	osm
stupňů	stupeň	k1gInPc2	stupeň
délky	délka	k1gFnSc2	délka
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
západní	západní	k2eAgFnSc2d1	západní
(	(	kIx(	(
<g/>
levé	levý	k2eAgFnSc2d1	levá
<g/>
)	)	kIx)	)
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
optickou	optický	k2eAgFnSc7d1	optická
librací	librace	k1gFnSc7	librace
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
měsíční	měsíční	k2eAgFnSc1d1	měsíční
orbita	orbita	k1gFnSc1	orbita
nakloněna	nakloněn	k2eAgFnSc1d1	nakloněna
k	k	k7c3	k
zemskému	zemský	k2eAgInSc3d1	zemský
rovníku	rovník	k1gInSc3	rovník
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
oscilovat	oscilovat	k5eAaImF	oscilovat
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
lidská	lidský	k2eAgFnSc1d1	lidská
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
když	když	k8xS	když
pokyvuje	pokyvovat	k5eAaImIp3nS	pokyvovat
na	na	k7c4	na
souhlas	souhlas	k1gInSc4	souhlas
<g/>
)	)	kIx)	)
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
ekliptikální	ekliptikální	k2eAgFnSc6d1	ekliptikální
šířce	šířka	k1gFnSc6	šířka
(	(	kIx(	(
<g/>
deklinaci	deklinace	k1gFnSc6	deklinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
optická	optický	k2eAgFnSc1d1	optická
librace	librace	k1gFnSc1	librace
v	v	k7c6	v
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
Měsíce	měsíc	k1gInSc2	měsíc
přibližně	přibližně	k6eAd1	přibližně
sedm	sedm	k4xCc1	sedm
stupňů	stupeň	k1gInPc2	stupeň
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
konec	konec	k1gInSc4	konec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc4	měsíc
vzdálen	vzdálit	k5eAaPmNgMnS	vzdálit
jen	jen	k9	jen
asi	asi	k9	asi
60	[number]	k4	60
zemských	zemský	k2eAgInPc2d1	zemský
poloměrů	poloměr	k1gInPc2	poloměr
<g/>
,	,	kIx,	,
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
vidí	vidět	k5eAaImIp3nS	vidět
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
noci	noc	k1gFnSc2	noc
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
bodů	bod	k1gInPc2	bod
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
jeden	jeden	k4xCgInSc1	jeden
zemský	zemský	k2eAgInSc1d1	zemský
průměr	průměr	k1gInSc1	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
optická	optický	k2eAgFnSc1d1	optická
librace	librace	k1gFnSc1	librace
paralaktická	paralaktický	k2eAgFnSc1d1	paralaktická
a	a	k8xC	a
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
asi	asi	k9	asi
jeden	jeden	k4xCgInSc1	jeden
stupeň	stupeň	k1gInSc1	stupeň
měsíční	měsíční	k2eAgFnSc2d1	měsíční
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
jejich	jejich	k3xOp3gNnSc2	jejich
barycentra	barycentrum	k1gNnSc2	barycentrum
nebo	nebo	k8xC	nebo
obecněji	obecně	k6eAd2	obecně
těžiště	těžiště	k1gNnSc1	těžiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
4	[number]	k4	4
700	[number]	k4	700
km	km	kA	km
od	od	k7c2	od
zemského	zemský	k2eAgInSc2d1	zemský
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
asi	asi	k9	asi
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
barycentrum	barycentrum	k1gNnSc1	barycentrum
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zemský	zemský	k2eAgInSc1d1	zemský
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
obecně	obecně	k6eAd1	obecně
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
kolébání	kolébání	k1gNnSc1	kolébání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podíváme	podívat	k5eAaImIp1nP	podívat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Měsíc	měsíc	k1gInSc4	měsíc
rotují	rotovat	k5eAaImIp3nP	rotovat
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
okolo	okolo	k7c2	okolo
jejich	jejich	k3xOp3gFnPc2	jejich
os	osa	k1gFnPc2	osa
<g/>
;	;	kIx,	;
Měsíc	měsíc	k1gInSc1	měsíc
obíhá	obíhat	k5eAaImIp3nS	obíhat
Zemi	zem	k1gFnSc4	zem
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
a	a	k8xC	a
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc4	slunce
také	také	k9	také
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaImF	vypadat
zvláštně	zvláštně	k6eAd1	zvláštně
<g/>
,	,	kIx,	,
že	že	k8xS	že
sklon	sklon	k1gInSc1	sklon
lunární	lunární	k2eAgFnSc2d1	lunární
orbity	orbita	k1gFnSc2	orbita
a	a	k8xC	a
vychýlení	vychýlení	k1gNnSc2	vychýlení
měsíční	měsíční	k2eAgFnSc2d1	měsíční
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc1	rotace
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přehledu	přehled	k1gInSc6	přehled
vypsány	vypsán	k2eAgFnPc4d1	vypsána
jako	jako	k8xS	jako
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
měnící	měnící	k2eAgFnPc1d1	měnící
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sklon	sklon	k1gInSc1	sklon
orbity	orbita	k1gFnSc2	orbita
je	být	k5eAaImIp3nS	být
měřen	měřit	k5eAaImNgMnS	měřit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
primární	primární	k2eAgFnSc3d1	primární
rovníkové	rovníkový	k2eAgFnSc3d1	Rovníková
rovině	rovina	k1gFnSc3	rovina
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zemské	zemský	k2eAgNnSc1d1	zemské
<g/>
)	)	kIx)	)
a	a	k8xC	a
vychýlení	vychýlení	k1gNnSc1	vychýlení
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
normále	normála	k1gFnSc3	normála
vůči	vůči	k7c3	vůči
rovině	rovina	k1gFnSc3	rovina
orbity	orbita	k1gFnSc2	orbita
satelitu	satelit	k1gInSc2	satelit
(	(	kIx(	(
<g/>
měsíční	měsíční	k2eAgInSc1d1	měsíční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
satelitů	satelit	k1gMnPc2	satelit
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
pro	pro	k7c4	pro
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
konvence	konvence	k1gFnPc1	konvence
odrážejí	odrážet	k5eAaImIp3nP	odrážet
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
realitu	realita	k1gFnSc4	realita
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
hodnoty	hodnota	k1gFnPc4	hodnota
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
formují	formovat	k5eAaImIp3nP	formovat
prakticky	prakticky	k6eAd1	prakticky
"	"	kIx"	"
<g/>
dvojplanetu	dvojplanet	k1gInSc6	dvojplanet
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
těsněji	těsně	k6eAd2	těsně
spjati	spjat	k2eAgMnPc1d1	spjat
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
než	než	k8xS	než
jeden	jeden	k4xCgMnSc1	jeden
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Rovina	rovina	k1gFnSc1	rovina
měsíční	měsíční	k2eAgFnSc2d1	měsíční
orbity	orbita	k1gFnSc2	orbita
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
sklon	sklon	k1gInSc4	sklon
5,145	[number]	k4	5,145
396	[number]	k4	396
<g/>
°	°	k?	°
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ekliptice	ekliptika	k1gFnSc3	ekliptika
(	(	kIx(	(
<g/>
orbitální	orbitální	k2eAgFnSc6d1	orbitální
rovině	rovina	k1gFnSc6	rovina
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
měsíční	měsíční	k2eAgFnSc1d1	měsíční
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc1	rotace
má	mít	k5eAaImIp3nS	mít
stálou	stálý	k2eAgFnSc4d1	stálá
výchylku	výchylka	k1gFnSc4	výchylka
1,542	[number]	k4	1,542
<g/>
4	[number]	k4	4
<g/>
°	°	k?	°
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
normále	normála	k1gFnSc3	normála
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Rovina	rovina	k1gFnSc1	rovina
měsíční	měsíční	k2eAgFnSc2d1	měsíční
orbity	orbita	k1gFnSc2	orbita
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
precesi	precese	k1gFnSc4	precese
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
její	její	k3xOp3gInSc4	její
průnik	průnik	k1gInSc4	průnik
s	s	k7c7	s
ekliptikou	ekliptika	k1gFnSc7	ekliptika
rotuje	rotovat	k5eAaImIp3nS	rotovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
)	)	kIx)	)
během	během	k7c2	během
6793,5	[number]	k4	6793,5
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
18,599	[number]	k4	18,599
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
gravitačnímu	gravitační	k2eAgInSc3d1	gravitační
vlivu	vliv	k1gInSc3	vliv
zemské	zemský	k2eAgFnSc2d1	zemská
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
deformace	deformace	k1gFnSc2	deformace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
periody	perioda	k1gFnSc2	perioda
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
sklon	sklon	k1gInSc1	sklon
roviny	rovina	k1gFnSc2	rovina
měsíční	měsíční	k2eAgFnSc2d1	měsíční
orbity	orbita	k1gFnSc2	orbita
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c4	mezi
23,45	[number]	k4	23,45
<g/>
°	°	k?	°
+	+	kIx~	+
5,15	[number]	k4	5,15
<g/>
°	°	k?	°
=	=	kIx~	=
28,60	[number]	k4	28,60
<g/>
°	°	k?	°
a	a	k8xC	a
23,45	[number]	k4	23,45
<g/>
°	°	k?	°
-	-	kIx~	-
<g/>
°	°	k?	°
=	=	kIx~	=
18,30	[number]	k4	18,30
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výchylka	výchylka	k1gFnSc1	výchylka
osy	osa	k1gFnSc2	osa
měsíční	měsíční	k2eAgFnSc1d1	měsíční
rotace	rotace	k1gFnSc1	rotace
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
normále	normála	k1gFnSc3	normála
na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
měsíce	měsíc	k1gInSc2	měsíc
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
5,15	[number]	k4	5,15
<g/>
°	°	k?	°
+	+	kIx~	+
1,54	[number]	k4	1,54
<g/>
°	°	k?	°
=	=	kIx~	=
6,69	[number]	k4	6,69
<g/>
°	°	k?	°
a	a	k8xC	a
5,15	[number]	k4	5,15
<g/>
°	°	k?	°
-	-	kIx~	-
<g/>
°	°	k?	°
=	=	kIx~	=
3,60	[number]	k4	3,60
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
výchylka	výchylka	k1gFnSc1	výchylka
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
také	také	k9	také
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
a	a	k8xC	a
sama	sám	k3xTgMnSc4	sám
kolísá	kolísat	k5eAaImIp3nS	kolísat
o	o	k7c4	o
0,002	[number]	k4	0,002
56	[number]	k4	56
<g/>
°	°	k?	°
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
stranu	strana	k1gFnSc4	strana
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nutace	nutace	k1gFnSc1	nutace
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
Měsíc	měsíc	k1gInSc1	měsíc
protíná	protínat	k5eAaImIp3nS	protínat
ekliptiku	ekliptika	k1gFnSc4	ekliptika
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
lunární	lunární	k2eAgInPc1d1	lunární
uzly	uzel	k1gInPc1	uzel
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
vzestupný	vzestupný	k2eAgInSc1d1	vzestupný
<g/>
)	)	kIx)	)
uzel	uzel	k1gInSc1	uzel
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Měsíc	měsíc	k1gInSc1	měsíc
přechází	přecházet	k5eAaImIp3nS	přecházet
k	k	k7c3	k
severu	sever	k1gInSc3	sever
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
;	;	kIx,	;
jižní	jižní	k2eAgFnSc2d1	jižní
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
sestupný	sestupný	k2eAgMnSc1d1	sestupný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přechází	přecházet	k5eAaImIp3nS	přecházet
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Zatmění	zatmění	k1gNnSc1	zatmění
Slunce	slunce	k1gNnSc2	slunce
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
uzel	uzel	k1gInSc1	uzel
střetne	střetnout	k5eAaPmIp3nS	střetnout
s	s	k7c7	s
Měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
novu	nov	k1gInSc6	nov
<g/>
;	;	kIx,	;
zatmění	zatmění	k1gNnSc1	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
uzel	uzel	k1gInSc1	uzel
střetne	střetnout	k5eAaPmIp3nS	střetnout
s	s	k7c7	s
Měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
měsíční	měsíční	k2eAgInPc1d1	měsíční
intervaly	interval	k1gInPc1	interval
nejsou	být	k5eNaImIp3nP	být
konstantní	konstantní	k2eAgFnSc7d1	konstantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
vyjádřeny	vyjádřit	k5eAaPmNgFnP	vyjádřit
jako	jako	k8xS	jako
součet	součet	k1gInSc1	součet
oběhové	oběhový	k2eAgFnSc2d1	oběhová
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
roční	roční	k2eAgFnPc4d1	roční
odchylky	odchylka	k1gFnPc4	odchylka
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřit	k5eAaPmNgFnP	vyjádřit
ve	v	k7c6	v
dnech	den	k1gInPc6	den
jako	jako	k9	jako
86	[number]	k4	86
400	[number]	k4	400
sekund	sekunda	k1gFnPc2	sekunda
podle	podle	k7c2	podle
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
platná	platný	k2eAgNnPc1d1	platné
pro	pro	k7c4	pro
datum	datum	k1gNnSc4	datum
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Parametr	parametr	k1gInSc1	parametr
r	r	kA	r
určuje	určovat	k5eAaImIp3nS	určovat
počet	počet	k1gInSc4	počet
let	léto	k1gNnPc2	léto
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
podle	podle	k7c2	podle
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Siderický	siderický	k2eAgInSc4d1	siderický
měsíční	měsíční	k2eAgInSc4d1	měsíční
interval	interval	k1gInSc4	interval
platný	platný	k2eAgInSc4d1	platný
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
podle	podle	k7c2	podle
vzorce	vzorec	k1gInSc2	vzorec
<g/>
:	:	kIx,	:
27,321	[number]	k4	27,321
661	[number]	k4	661
547	[number]	k4	547
+	+	kIx~	+
0,000	[number]	k4	0,000
000	[number]	k4	000
001	[number]	k4	001
857	[number]	k4	857
·	·	k?	·
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
měsíční	měsíční	k2eAgFnSc2d1	měsíční
dráhy	dráha	k1gFnSc2	dráha
činí	činit	k5eAaImIp3nS	činit
dost	dost	k6eAd1	dost
nepravděpodobnou	pravděpodobný	k2eNgFnSc4d1	nepravděpodobná
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
zachycen	zachytit	k5eAaPmNgInS	zachytit
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
spekulací	spekulace	k1gFnPc2	spekulace
-	-	kIx~	-
teorie	teorie	k1gFnSc1	teorie
odtržení	odtržení	k1gNnSc2	odtržení
-	-	kIx~	-
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
ze	z	k7c2	z
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
vlivem	vlivem	k7c2	vlivem
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
zanechávaje	zanechávat	k5eAaImSgMnS	zanechávat
za	za	k7c4	za
sebou	se	k3xPyFc7	se
dnešní	dnešní	k2eAgFnSc1d1	dnešní
oceánské	oceánský	k2eAgNnSc4d1	oceánské
dno	dno	k1gNnSc4	dno
jako	jako	k8xS	jako
jizvu	jizva	k1gFnSc4	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
by	by	kYmCp3nS	by
však	však	k9	však
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
příliš	příliš	k6eAd1	příliš
rychlou	rychlý	k2eAgFnSc4d1	rychlá
počáteční	počáteční	k2eAgFnSc4d1	počáteční
rotaci	rotace	k1gFnSc4	rotace
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
zformoval	zformovat	k5eAaPmAgInS	zformovat
jinde	jinde	k6eAd1	jinde
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zachycen	zachytit	k5eAaPmNgMnS	zachytit
na	na	k7c4	na
nynější	nynější	k2eAgFnSc4d1	nynější
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
teorie	teorie	k1gFnSc1	teorie
zachycení	zachycení	k1gNnSc2	zachycení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
však	však	k9	však
nepředpokládají	předpokládat	k5eNaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
malé	malý	k2eAgNnSc1d1	malé
těleso	těleso	k1gNnSc1	těleso
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
schopno	schopen	k2eAgNnSc1d1	schopno
zachytit	zachytit	k5eAaPmF	zachytit
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
velikosti	velikost	k1gFnSc2	velikost
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc4	takový
schopnost	schopnost	k1gFnSc4	schopnost
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
naši	náš	k3xOp1gFnSc4	náš
soustavě	soustava	k1gFnSc6	soustava
díky	díky	k7c3	díky
větší	veliký	k2eAgFnSc3d2	veliký
gravitaci	gravitace	k1gFnSc3	gravitace
pouze	pouze	k6eAd1	pouze
velké	velký	k2eAgFnSc2d1	velká
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
především	především	k9	především
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
společné	společný	k2eAgFnSc2d1	společná
akreace	akreace	k1gFnSc2	akreace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
Země	zem	k1gFnPc4	zem
a	a	k8xC	a
Měsíc	měsíc	k1gInSc4	měsíc
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
z	z	k7c2	z
akreačního	akreační	k2eAgInSc2d1	akreační
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
neumí	umět	k5eNaImIp3nS	umět
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
nedostatek	nedostatek	k1gInSc4	nedostatek
železa	železo	k1gNnSc2	železo
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Měsíc	měsíc	k1gInSc1	měsíc
mohl	moct	k5eAaImAgInS	moct
zformovat	zformovat	k5eAaPmF	zformovat
z	z	k7c2	z
úlomků	úlomek	k1gInPc2	úlomek
zachycených	zachycený	k2eAgInPc2d1	zachycený
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
asteroidů	asteroid	k1gInPc2	asteroid
nebo	nebo	k8xC	nebo
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
přijímána	přijímán	k2eAgFnSc1d1	přijímána
teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
impaktu	impakt	k1gInSc2	impakt
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
Měsíc	měsíc	k1gInSc4	měsíc
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
formující	formující	k2eAgFnSc1d1	formující
se	se	k3xPyFc4	se
žhnoucí	žhnoucí	k2eAgFnSc1d1	žhnoucí
Země	země	k1gFnSc1	země
s	s	k7c7	s
planetesimálou	planetesimála	k1gFnSc7	planetesimála
velikosti	velikost	k1gFnSc2	velikost
Marsu	Mars	k1gInSc2	Mars
(	(	kIx(	(
<g/>
pracovně	pracovně	k6eAd1	pracovně
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Theia	Theia	k1gFnSc1	Theia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určená	určený	k2eAgNnPc1d1	určené
geologická	geologický	k2eAgNnPc1d1	geologické
období	období	k1gNnPc1	období
Měsíce	měsíc	k1gInSc2	měsíc
jsou	být	k5eAaImIp3nP	být
definována	definován	k2eAgFnSc1d1	definována
na	na	k7c6	na
základě	základ	k1gInSc6	základ
datování	datování	k1gNnPc2	datování
různých	různý	k2eAgInPc2d1	různý
významných	významný	k2eAgInPc2d1	významný
impaktů	impakt	k1gInPc2	impakt
v	v	k7c6	v
měsíční	měsíční	k2eAgFnSc6d1	měsíční
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
stupně	stupeň	k1gInSc2	stupeň
jejich	jejich	k3xOp3gInSc2	jejich
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
překrytu	překryt	k2eAgFnSc4d1	překryta
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
deformovaly	deformovat	k5eAaImAgFnP	deformovat
dříve	dříve	k6eAd2	dříve
žhavý	žhavý	k2eAgInSc4d1	žhavý
Měsíc	měsíc	k1gInSc4	měsíc
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
elipsoidu	elipsoid	k1gInSc2	elipsoid
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
osou	osa	k1gFnSc7	osa
nasměrovanou	nasměrovaný	k2eAgFnSc7d1	nasměrovaná
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
jeho	jeho	k3xOp3gFnSc4	jeho
nesymetrii	nesymetrie	k1gFnSc4	nesymetrie
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
alternativní	alternativní	k2eAgFnSc1d1	alternativní
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
impaktu	impakt	k1gInSc6	impakt
Thei	Thea	k1gFnSc3	Thea
se	se	k3xPyFc4	se
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
z	z	k7c2	z
trosek	troska	k1gFnPc2	troska
zformovala	zformovat	k5eAaPmAgNnP	zformovat
hned	hned	k6eAd1	hned
dvě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tyto	tento	k3xDgInPc1	tento
nakonec	nakonec	k6eAd1	nakonec
"	"	kIx"	"
<g/>
měkce	měkko	k6eAd1	měkko
<g/>
"	"	kIx"	"
splynuly	splynout	k5eAaPmAgInP	splynout
<g/>
:	:	kIx,	:
Přímá	přímý	k2eAgFnSc1d1	přímá
i	i	k8xC	i
boční	boční	k2eAgFnSc1d1	boční
srážka	srážka	k1gFnSc1	srážka
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
rozbila	rozbít	k5eAaPmAgFnS	rozbít
<g/>
,	,	kIx,	,
na	na	k7c6	na
orbitě	orbita	k1gFnSc6	orbita
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přibližovala	přibližovat	k5eAaImAgFnS	přibližovat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
navíc	navíc	k6eAd1	navíc
začala	začít	k5eAaPmAgFnS	začít
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
obíhat	obíhat	k5eAaImF	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
zmenšovala	zmenšovat	k5eAaImAgFnS	zmenšovat
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
svázala	svázat	k5eAaPmAgFnS	svázat
rotace	rotace	k1gFnSc1	rotace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gInPc4	jejich
povrchy	povrch	k1gInPc4	povrch
se	se	k3xPyFc4	se
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
netřely	třít	k5eNaImAgFnP	třít
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
binárního	binární	k2eAgInSc2d1	binární
systému	systém	k1gInSc2	systém
postupně	postupně	k6eAd1	postupně
unikala	unikat	k5eAaImAgFnS	unikat
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vlivem	vliv	k1gInSc7	vliv
blízké	blízký	k2eAgFnSc2d1	blízká
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přibližovala	přibližovat	k5eAaImAgFnS	přibližovat
až	až	k6eAd1	až
se	se	k3xPyFc4	se
dotkla	dotknout	k5eAaPmAgFnS	dotknout
a	a	k8xC	a
splynula	splynout	k5eAaPmAgFnS	splynout
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
Měsíc	měsíc	k1gInSc1	měsíc
převzal	převzít	k5eAaPmAgInS	převzít
jejich	jejich	k3xOp3gFnSc4	jejich
rotační	rotační	k2eAgFnSc4d1	rotační
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
ale	ale	k8xC	ale
Země	země	k1gFnSc1	země
rychle	rychle	k6eAd1	rychle
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
:	:	kIx,	:
Dokonce	dokonce	k9	dokonce
prý	prý	k9	prý
za	za	k7c4	za
pouhých	pouhý	k2eAgNnPc2d1	pouhé
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
protoměsíců	protoměsíce	k1gMnPc2	protoměsíce
trochu	trochu	k6eAd1	trochu
jiné	jiný	k2eAgInPc4d1	jiný
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc1	složení
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
po	po	k7c4	po
splynutí	splynutí	k1gNnSc4	splynutí
zůstala	zůstat	k5eAaPmAgFnS	zůstat
patrná	patrný	k2eAgFnSc1d1	patrná
deformace	deformace	k1gFnSc1	deformace
<g/>
/	/	kIx~	/
<g/>
asymetrie	asymetrie	k1gFnSc1	asymetrie
<g/>
:	:	kIx,	:
Nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
tvar	tvar	k1gInSc1	tvar
Měsíce	měsíc	k1gInSc2	měsíc
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
kulový	kulový	k2eAgMnSc1d1	kulový
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
hmota	hmota	k1gFnSc1	hmota
není	být	k5eNaImIp3nS	být
rozložená	rozložený	k2eAgFnSc1d1	rozložená
úplně	úplně	k6eAd1	úplně
symetricky	symetricky	k6eAd1	symetricky
<g/>
:	:	kIx,	:
zůstala	zůstat	k5eAaPmAgFnS	zůstat
mírně	mírně	k6eAd1	mírně
excentrická	excentrický	k2eAgFnSc1d1	excentrická
<g/>
,	,	kIx,	,
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
svůj	svůj	k3xOyFgInSc4	svůj
geometrický	geometrický	k2eAgInSc4d1	geometrický
střed	střed	k1gInSc4	střed
<g/>
.	.	kIx.	.
</s>
<s>
Sloučením	sloučení	k1gNnSc7	sloučení
sice	sice	k8xC	sice
jádra	jádro	k1gNnSc2	jádro
obou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
klesla	klesnout	k5eAaPmAgFnS	klesnout
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
ale	ale	k9	ale
možná	možná	k9	možná
nesplynula	splynout	k5eNaPmAgFnS	splynout
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
asymetrie	asymetrie	k1gFnSc1	asymetrie
už	už	k6eAd1	už
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Zůstalo	zůstat	k5eAaPmAgNnS	zůstat
však	však	k9	však
také	také	k9	také
teplo	teplo	k1gNnSc1	teplo
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
plášť	plášť	k1gInSc1	plášť
roztavil	roztavit	k5eAaPmAgInS	roztavit
<g/>
:	:	kIx,	:
Měsíc	měsíc	k1gInSc1	měsíc
měl	mít	k5eAaImAgInS	mít
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Tmavá	tmavý	k2eAgNnPc1d1	tmavé
lávová	lávový	k2eAgNnPc1d1	lávové
pole	pole	k1gNnPc1	pole
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
jen	jen	k9	jen
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
silnější	silný	k2eAgFnSc1d2	silnější
a	a	k8xC	a
láva	láva	k1gFnSc1	láva
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
:	:	kIx,	:
Povrch	povrch	k6eAd1wR	povrch
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
krátery	kráter	k1gInPc7	kráter
ve	v	k7c6	v
světlém	světlý	k2eAgInSc6d1	světlý
měsíčním	měsíční	k2eAgInSc6d1	měsíční
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
různých	různý	k2eAgFnPc2d1	různá
teorií	teorie	k1gFnPc2	teorie
buď	buď	k8xC	buď
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Země	země	k1gFnSc1	země
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
hustším	hustý	k2eAgNnSc7d2	hustší
a	a	k8xC	a
žhavějším	žhavý	k2eAgNnSc7d2	žhavější
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
uloženým	uložený	k2eAgNnSc7d1	uložené
v	v	k7c6	v
Měsíci	měsíc	k1gInSc6	měsíc
excentricky	excentricky	k6eAd1	excentricky
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lávová	lávový	k2eAgNnPc1d1	lávové
pole	pole	k1gNnPc1	pole
na	na	k7c6	na
tenčí	tenký	k2eAgFnSc6d2	tenčí
kůře	kůra	k1gFnSc6	kůra
původně	původně	k6eAd1	původně
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
kdekoli	kdekoli	k6eAd1	kdekoli
<g/>
,	,	kIx,	,
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
se	se	k3xPyFc4	se
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
přetočila	přetočit	k5eAaPmAgFnS	přetočit
<g/>
.	.	kIx.	.
</s>
<s>
Anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
rotace	rotace	k1gFnSc1	rotace
Měsíce	měsíc	k1gInSc2	měsíc
nejdříve	dříve	k6eAd3	dříve
svázala	svázat	k5eAaPmAgFnS	svázat
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
až	až	k8xS	až
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
na	na	k7c6	na
teprve	teprve	k6eAd1	teprve
láva	láva	k1gFnSc1	láva
vylila	vylít	k5eAaPmAgFnS	vylít
<g/>
,	,	kIx,	,
možná	možná	k9	možná
až	až	k9	až
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc4	důsledek
působení	působení	k1gNnSc2	působení
Země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
Sálavý	sálavý	k2eAgInSc4d1	sálavý
žár	žár	k1gInSc4	žár
ze	z	k7c2	z
srážkou	srážka	k1gFnSc7	srážka
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
roztavené	roztavený	k2eAgFnSc2d1	roztavená
Země	zem	k1gFnSc2	zem
totiž	totiž	k9	totiž
mohl	moct	k5eAaImAgInS	moct
odpařovat	odpařovat	k5eAaImF	odpařovat
lehčí	lehký	k2eAgInPc4d2	lehčí
materiály	materiál	k1gInPc4	materiál
z	z	k7c2	z
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
usazovaly	usazovat	k5eAaImAgFnP	usazovat
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
oslabená	oslabený	k2eAgFnSc1d1	oslabená
kůra	kůra	k1gFnSc1	kůra
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
místem	místo	k1gNnSc7	místo
nejsnazšího	snadný	k2eAgInSc2d3	nejsnazší
výronu	výron	k1gInSc2	výron
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k9	už
by	by	kYmCp3nS	by
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
události	událost	k1gFnSc3	událost
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
Měsíc	měsíc	k1gInSc1	měsíc
rozhodně	rozhodně	k6eAd1	rozhodně
byl	být	k5eAaImAgInS	být
polotekutý	polotekutý	k2eAgInSc1d1	polotekutý
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
plášť	plášť	k1gInSc1	plášť
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vychladlý	vychladlý	k2eAgInSc4d1	vychladlý
a	a	k8xC	a
ztuhlý	ztuhlý	k2eAgInSc4d1	ztuhlý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
aktivitě	aktivita	k1gFnSc6	aktivita
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
:	:	kIx,	:
Vysvětlovalo	vysvětlovat	k5eAaImAgNnS	vysvětlovat
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
sporadicky	sporadicky	k6eAd1	sporadicky
hlášené	hlášený	k2eAgInPc1d1	hlášený
červené	červený	k2eAgInPc1d1	červený
záblesky	záblesk	k1gInPc1	záblesk
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
i	i	k8xC	i
možné	možný	k2eAgFnPc1d1	možná
struktury	struktura	k1gFnPc1	struktura
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
po	po	k7c6	po
výtryscích	výtrysk	k1gInPc6	výtrysk
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prachu	prach	k1gInSc2	prach
hnaného	hnaný	k2eAgInSc2d1	hnaný
unikajícím	unikající	k2eAgInSc7d1	unikající
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
stavu	stav	k1gInSc6	stav
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
tekutosti	tekutost	k1gFnSc6	tekutost
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemáme	mít	k5eNaImIp1nP	mít
konkrétní	konkrétní	k2eAgNnPc4d1	konkrétní
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4,5	[number]	k4	4,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
pokrýval	pokrývat	k5eAaImAgMnS	pokrývat
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
tekutý	tekutý	k2eAgInSc4d1	tekutý
oceán	oceán	k1gInSc4	oceán
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc4	jeden
typ	typ	k1gInSc4	typ
lunárních	lunární	k2eAgInPc2d1	lunární
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
KREEP	KREEP	kA	KREEP
(	(	kIx(	(
<g/>
K	K	kA	K
-	-	kIx~	-
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
REE	Rea	k1gFnSc6	Rea
-	-	kIx~	-
rare	rarat	k5eAaPmIp3nS	rarat
earth	earth	k1gMnSc1	earth
elements	elements	k1gInSc1	elements
-	-	kIx~	-
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
fosfor	fosfor	k1gInSc4	fosfor
<g/>
)	)	kIx)	)
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
po	po	k7c6	po
chemické	chemický	k2eAgFnSc6d1	chemická
stránce	stránka	k1gFnSc6	stránka
zbytek	zbytek	k1gInSc1	zbytek
tohoto	tento	k3xDgInSc2	tento
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
KREEP	KREEP	kA	KREEP
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
směsice	směsice	k1gFnSc1	směsice
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vědci	vědec	k1gMnPc1	vědec
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
nekompatibilní	kompatibilní	k2eNgInPc1d1	nekompatibilní
prvky	prvek	k1gInPc1	prvek
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nemohly	moct	k5eNaImAgFnP	moct
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
krystalické	krystalický	k2eAgFnSc2d1	krystalická
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
vyplavaly	vyplavat	k5eAaPmAgFnP	vyplavat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výzkumníky	výzkumník	k1gMnPc4	výzkumník
je	být	k5eAaImIp3nS	být
KREEP	KREEP	kA	KREEP
vhodným	vhodný	k2eAgInSc7d1	vhodný
svědkem	svědek	k1gMnSc7	svědek
schopným	schopný	k2eAgMnPc3d1	schopný
podat	podat	k5eAaPmF	podat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
historii	historie	k1gFnSc6	historie
měsíční	měsíční	k2eAgFnSc2d1	měsíční
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
frekvenci	frekvence	k1gFnSc4	frekvence
dopadů	dopad	k1gInPc2	dopad
komet	kometa	k1gFnPc2	kometa
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
množství	množství	k1gNnSc2	množství
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
thoria	thorium	k1gNnSc2	thorium
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bombardování	bombardování	k1gNnSc1	bombardování
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
zářením	záření	k1gNnSc7	záření
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
vlastní	vlastní	k2eAgFnSc4d1	vlastní
radiaci	radiace	k1gFnSc4	radiace
jako	jako	k8xC	jako
gama	gama	k1gNnSc1	gama
paprsky	paprsek	k1gInPc1	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
jako	jako	k8xS	jako
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
thorium	thorium	k1gNnSc1	thorium
a	a	k8xC	a
draslík	draslík	k1gInSc4	draslík
jsou	být	k5eAaImIp3nP	být
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
gama	gama	k1gNnSc1	gama
paprsky	paprsek	k1gInPc7	paprsek
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Gama	gama	k1gNnSc2	gama
paprsky	paprsek	k1gInPc1	paprsek
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	on	k3xPp3gNnSc4	on
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
navzájem	navzájem	k6eAd1	navzájem
různé	různý	k2eAgFnPc1d1	různá
-	-	kIx~	-
všechny	všechen	k3xTgFnPc1	všechen
produkují	produkovat	k5eAaImIp3nP	produkovat
jedinečné	jedinečný	k2eAgFnSc2d1	jedinečná
spektrální	spektrální	k2eAgFnSc2d1	spektrální
čáry	čára	k1gFnSc2	čára
<g/>
,	,	kIx,	,
detekovatelné	detekovatelný	k2eAgFnSc2d1	detekovatelná
spektrometrem	spektrometr	k1gInSc7	spektrometr
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgNnSc4d1	kompletní
globální	globální	k2eAgNnSc4d1	globální
zmapování	zmapování	k1gNnSc4	zmapování
Měsíce	měsíc	k1gInSc2	měsíc
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
výskytu	výskyt	k1gInSc2	výskyt
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kosmické	kosmický	k2eAgFnPc1d1	kosmická
lodě	loď	k1gFnPc1	loď
jej	on	k3xPp3gInSc4	on
však	však	k9	však
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
na	na	k7c6	na
části	část	k1gFnSc6	část
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
;	;	kIx,	;
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
činností	činnost	k1gFnSc7	činnost
zabývala	zabývat	k5eAaImAgFnS	zabývat
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkové	celkový	k2eAgNnSc1d1	celkové
složení	složení	k1gNnSc1	složení
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k8xC	jako
zemské	zemský	k2eAgNnSc1d1	zemské
až	až	k6eAd1	až
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
těkavých	těkavý	k2eAgInPc2d1	těkavý
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
desítkami	desítka	k1gFnPc7	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
kráterů	kráter	k1gInPc2	kráter
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
větším	veliký	k2eAgInSc6d2	veliký
než	než	k8xS	než
1	[number]	k4	1
kilometr	kilometr	k1gInSc1	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
stovky	stovka	k1gFnPc1	stovka
miliónů	milión	k4xCgInPc2	milión
nebo	nebo	k8xC	nebo
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
geologických	geologický	k2eAgInPc2d1	geologický
procesů	proces	k1gInPc2	proces
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
prakticky	prakticky	k6eAd1	prakticky
navždy	navždy	k6eAd1	navždy
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
vskutku	vskutku	k9	vskutku
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc1d1	známý
kráter	kráter	k1gInSc1	kráter
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
tvoří	tvořit	k5eAaImIp3nS	tvořit
pánev	pánev	k1gFnSc1	pánev
South	Southa	k1gFnPc2	Southa
Pole-Aitken	Pole-Aitkna	k1gFnPc2	Pole-Aitkna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kráter	kráter	k1gInSc1	kráter
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
poblíž	poblíž	k7c2	poblíž
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
240	[number]	k4	240
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
hloubku	hloubek	k1gInSc6	hloubek
13	[number]	k4	13
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
koryta	koryto	k1gNnPc1	koryto
Sinuous	Sinuous	k1gMnSc1	Sinuous
rilles	rilles	k1gMnSc1	rilles
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgFnPc1d1	tmavá
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
jednotvárné	jednotvárný	k2eAgFnPc4d1	jednotvárná
měsíční	měsíční	k2eAgFnPc4d1	měsíční
pláně	pláň	k1gFnPc4	pláň
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
mare	mare	k6eAd1	mare
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
maria	marium	k1gNnSc2	marium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
staří	starý	k2eAgMnPc1d1	starý
astronomové	astronom	k1gMnPc1	astronom
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
moře	moře	k1gNnSc4	moře
naplněná	naplněný	k2eAgFnSc1d1	naplněná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rozlehlé	rozlehlý	k2eAgInPc4d1	rozlehlý
prastaré	prastarý	k2eAgInPc4d1	prastarý
čedičové	čedičový	k2eAgInPc4d1	čedičový
proudy	proud	k1gInPc4	proud
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyplnily	vyplnit	k5eAaPmAgFnP	vyplnit
pánve	pánev	k1gFnPc4	pánev
velkých	velký	k2eAgMnPc2d1	velký
impaktních	impaktní	k2eAgMnPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Světlejší	světlý	k2eAgFnPc1d2	světlejší
vrchoviny	vrchovina	k1gFnPc1	vrchovina
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
pevniny	pevnina	k1gFnSc2	pevnina
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
terra	terra	k6eAd1	terra
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
terrae	terra	k1gFnSc2	terra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
na	na	k7c4	na
odvrácené	odvrácený	k2eAgMnPc4d1	odvrácený
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
rozptýlených	rozptýlený	k2eAgInPc2d1	rozptýlený
fleků	flek	k1gInPc2	flek
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
asymetrie	asymetrie	k1gFnSc1	asymetrie
v	v	k7c6	v
měsíční	měsíční	k2eAgFnSc6d1	měsíční
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
synchronizací	synchronizace	k1gFnSc7	synchronizace
mezi	mezi	k7c7	mezi
měsíční	měsíční	k2eAgFnSc7d1	měsíční
rotací	rotace	k1gFnSc7	rotace
a	a	k8xC	a
oběhem	oběh	k1gInSc7	oběh
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
synchronizace	synchronizace	k1gFnSc1	synchronizace
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
odvrácenou	odvrácený	k2eAgFnSc4d1	odvrácená
stranu	strana	k1gFnSc4	strana
Měsíce	měsíc	k1gInSc2	měsíc
častějším	častý	k2eAgNnSc7d2	častější
dopadům	dopad	k1gInPc3	dopad
asteroidů	asteroid	k1gInPc2	asteroid
a	a	k8xC	a
meteoritů	meteorit	k1gInPc2	meteorit
než	než	k8xS	než
přivrácenou	přivrácený	k2eAgFnSc4d1	přivrácená
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
nebyla	být	k5eNaImAgFnS	být
moře	moře	k1gNnSc2	moře
překryta	překrýt	k5eAaPmNgFnS	překrýt
krátery	kráter	k1gInPc7	kráter
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
část	část	k1gFnSc1	část
měsíční	měsíční	k2eAgFnSc2d1	měsíční
kůry	kůra	k1gFnSc2	kůra
tvoří	tvořit	k5eAaImIp3nS	tvořit
nesoudržná	soudržný	k2eNgFnSc1d1	nesoudržná
kamenná	kamenný	k2eAgFnSc1d1	kamenná
vrstva	vrstva	k1gFnSc1	vrstva
rozdrcených	rozdrcený	k2eAgFnPc2d1	rozdrcená
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
zvaná	zvaný	k2eAgFnSc1d1	zvaná
regolit	regolit	k1gInSc1	regolit
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
i	i	k8xC	i
regolit	regolit	k1gInSc1	regolit
nejsou	být	k5eNaImIp3nP	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Měsíci	měsíc	k1gInSc6	měsíc
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Mocnost	mocnost	k1gFnSc1	mocnost
kůry	kůra	k1gFnSc2	kůra
zjištěná	zjištěný	k2eAgFnSc1d1	zjištěná
ze	z	k7c2	z
seismických	seismický	k2eAgNnPc2d1	seismické
měření	měření	k1gNnPc2	měření
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
5	[number]	k4	5
km	km	kA	km
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
do	do	k7c2	do
100	[number]	k4	100
km	km	kA	km
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
regolitu	regolit	k1gInSc2	regolit
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
5	[number]	k4	5
m	m	kA	m
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
a	a	k8xC	a
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
20	[number]	k4	20
m	m	kA	m
na	na	k7c6	na
vrchovinách	vrchovina	k1gFnPc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zjistili	zjistit	k5eAaPmAgMnP	zjistit
vědci	vědec	k1gMnPc1	vědec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
snímků	snímek	k1gInPc2	snímek
získaných	získaný	k2eAgInPc2d1	získaný
sondou	sonda	k1gFnSc7	sonda
Clementine	Clementin	k1gMnSc5	Clementin
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtyři	čtyři	k4xCgFnPc1	čtyři
hornaté	hornatý	k2eAgFnPc1d1	hornatá
oblasti	oblast	k1gFnPc1	oblast
lemující	lemující	k2eAgFnPc1d1	lemující
73	[number]	k4	73
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
kráter	kráter	k1gInSc4	kráter
Peary	Peara	k1gFnSc2	Peara
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
osvětleny	osvětlit	k5eAaPmNgFnP	osvětlit
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
měsíční	měsíční	k2eAgInSc4d1	měsíční
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
"	"	kIx"	"
<g/>
vrcholy	vrchol	k1gInPc7	vrchol
věčného	věčný	k2eAgNnSc2d1	věčné
světla	světlo	k1gNnSc2	světlo
<g/>
"	"	kIx"	"
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
díky	díky	k7c3	díky
extrémně	extrémně	k6eAd1	extrémně
malé	malý	k2eAgFnSc3d1	malá
výchylce	výchylka	k1gFnSc3	výchylka
měsíční	měsíční	k2eAgFnSc2d1	měsíční
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
také	také	k9	také
existenci	existence	k1gFnSc4	existence
věčného	věčný	k2eAgInSc2d1	věčný
stínu	stín	k1gInSc2	stín
na	na	k7c6	na
dnech	den	k1gInPc6	den
mnoha	mnoho	k4c2	mnoho
polárních	polární	k2eAgInPc2d1	polární
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
méně	málo	k6eAd2	málo
hornatém	hornatý	k2eAgNnSc6d1	hornaté
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
oblasti	oblast	k1gFnSc2	oblast
věčného	věčný	k2eAgNnSc2d1	věčné
světla	světlo	k1gNnSc2	světlo
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
okraj	okraj	k1gInSc4	okraj
kráteru	kráter	k1gInSc2	kráter
Shackleton	Shackleton	k1gInSc1	Shackleton
je	být	k5eAaImIp3nS	být
osvětlen	osvětlit	k5eAaPmNgInS	osvětlit
po	po	k7c6	po
80	[number]	k4	80
%	%	kIx~	%
měsíčního	měsíční	k2eAgInSc2d1	měsíční
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
Clementine	Clementin	k1gInSc5	Clementin
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
<g/>
,	,	kIx,	,
když	když	k8xS	když
severní	severní	k2eAgFnSc1d1	severní
měsíční	měsíční	k2eAgFnSc1d1	měsíční
polokoule	polokoule	k1gFnSc1	polokoule
zažívala	zažívat	k5eAaImAgFnS	zažívat
letní	letní	k2eAgNnSc4d1	letní
období	období	k1gNnSc4	období
-	-	kIx~	-
nicméně	nicméně	k8xC	nicméně
pozorování	pozorování	k1gNnSc1	pozorování
sondy	sonda	k1gFnSc2	sonda
SMART-1	SMART-1	k1gFnSc3	SMART-1
stejné	stejný	k2eAgFnSc2d1	stejná
oblasti	oblast	k1gFnSc2	oblast
během	během	k7c2	během
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
místa	místo	k1gNnPc4	místo
s	s	k7c7	s
celoročním	celoroční	k2eAgNnSc7d1	celoroční
slunečním	sluneční	k2eAgNnSc7d1	sluneční
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
naměřila	naměřit	k5eAaBmAgFnS	naměřit
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Lunar	Lunara	k1gFnPc2	Lunara
Reconnaissance	Reconnaissance	k1gFnSc1	Reconnaissance
Orbiter	Orbiter	k1gInSc1	Orbiter
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
trvalého	trvalý	k2eAgInSc2d1	trvalý
stínu	stín	k1gInSc2	stín
v	v	k7c6	v
kráterech	kráter	k1gInPc6	kráter
okolo	okolo	k7c2	okolo
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
dosud	dosud	k6eAd1	dosud
známou	známý	k2eAgFnSc4d1	známá
teplotu	teplota	k1gFnSc4	teplota
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
°	°	k?	°
<g/>
C	C	kA	C
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
fyzických	fyzický	k2eAgInPc2d1	fyzický
rysů	rys	k1gInPc2	rys
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
používání	používání	k1gNnSc1	používání
termínu	termín	k1gInSc2	termín
geografie	geografie	k1gFnSc2	geografie
či	či	k8xC	či
jiných	jiný	k2eAgNnPc2d1	jiné
slov	slovo	k1gNnPc2	slovo
s	s	k7c7	s
předponou	předpona	k1gFnSc7	předpona
geo-	geo-	k?	geo-
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgFnSc1	tento
předpona	předpona	k1gFnSc1	předpona
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
latinském	latinský	k2eAgInSc6d1	latinský
původu	původ	k1gInSc6	původ
referuje	referovat	k5eAaBmIp3nS	referovat
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nelogické	logický	k2eNgNnSc1d1	nelogické
její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
pro	pro	k7c4	pro
mimozemská	mimozemský	k2eAgNnPc4d1	mimozemské
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovaným	navrhovaný	k2eAgInSc7d1	navrhovaný
alternativním	alternativní	k2eAgInSc7d1	alternativní
pojmem	pojem	k1gInSc7	pojem
je	být	k5eAaImIp3nS	být
selenografie	selenografie	k1gFnSc1	selenografie
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
předpona	předpona	k1gFnSc1	předpona
seleno-	seleno-	k?	seleno-
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc1	měsíc
vytrvale	vytrvale	k6eAd1	vytrvale
bombardován	bombardován	k2eAgInSc1d1	bombardován
kometami	kometa	k1gFnPc7	kometa
a	a	k8xC	a
meteoroidy	meteoroid	k1gInPc7	meteoroid
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
bohatých	bohatý	k2eAgInPc6d1	bohatý
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
energie	energie	k1gFnSc1	energie
ji	on	k3xPp3gFnSc4	on
následně	následně	k6eAd1	následně
disociuje	disociovat	k5eAaBmIp3nS	disociovat
(	(	kIx(	(
<g/>
rozštěpí	rozštěpit	k5eAaPmIp3nS	rozštěpit
<g/>
)	)	kIx)	)
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
základní	základní	k2eAgInPc4d1	základní
prvky	prvek	k1gInPc4	prvek
vodík	vodík	k1gInSc1	vodík
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
unikají	unikat	k5eAaImIp3nP	unikat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
existuje	existovat	k5eAaImIp3nS	existovat
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
mohou	moct	k5eAaImIp3nP	moct
zůstávat	zůstávat	k5eAaImF	zůstávat
významné	významný	k2eAgInPc4d1	významný
zbytky	zbytek	k1gInPc4	zbytek
vody	voda	k1gFnSc2	voda
buďto	buďto	k8xC	buďto
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nebo	nebo	k8xC	nebo
uvězněny	uvězněn	k2eAgFnPc4d1	uvězněna
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
mise	mise	k1gFnSc2	mise
Clementine	Clementin	k1gInSc5	Clementin
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
malé	malý	k2eAgFnSc2d1	malá
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
kapsy	kapsa	k1gFnSc2	kapsa
ledu	led	k1gInSc2	led
(	(	kIx(	(
<g/>
zbytky	zbytek	k1gInPc1	zbytek
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
bohatých	bohatý	k2eAgFnPc2d1	bohatá
komet	kometa	k1gFnPc2	kometa
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nerozmrazeny	rozmrazit	k5eNaPmNgInP	rozmrazit
uchovány	uchován	k2eAgInPc1d1	uchován
uvnitř	uvnitř	k7c2	uvnitř
měsíční	měsíční	k2eAgFnSc2d1	měsíční
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
o	o	k7c6	o
kapsách	kapsa	k1gFnPc6	kapsa
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
jako	jako	k9	jako
o	o	k7c6	o
malých	malá	k1gFnPc6	malá
<g/>
,	,	kIx,	,
celkové	celkový	k2eAgNnSc1d1	celkové
předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
významné	významný	k2eAgNnSc1d1	významné
-	-	kIx~	-
1	[number]	k4	1
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
vodní	vodní	k2eAgFnPc1d1	vodní
molekuly	molekula	k1gFnPc1	molekula
mohly	moct	k5eAaImAgFnP	moct
poletovat	poletovat	k5eAaImF	poletovat
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
být	být	k5eAaImF	být
zachyceny	zachycen	k2eAgInPc4d1	zachycen
uvnitř	uvnitř	k7c2	uvnitř
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
měsíčních	měsíční	k2eAgInPc6d1	měsíční
pólech	pól	k1gInPc6	pól
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
mírné	mírný	k2eAgFnSc3d1	mírná
výchylce	výchylka	k1gFnSc3	výchylka
měsíční	měsíční	k2eAgFnSc2d1	měsíční
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
jen	jen	k9	jen
1,5	[number]	k4	1,5
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
hlubokých	hluboký	k2eAgInPc2d1	hluboký
kráterů	kráter	k1gInPc2	kráter
nikdy	nikdy	k6eAd1	nikdy
nezasvitne	zasvitnout	k5eNaPmIp3nS	zasvitnout
světlo	světlo	k1gNnSc1	světlo
Slunce	slunce	k1gNnSc2	slunce
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
trvalý	trvalý	k2eAgInSc4d1	trvalý
stín	stín	k1gInSc4	stín
<g/>
.	.	kIx.	.
</s>
<s>
Clementine	Clementin	k1gMnSc5	Clementin
zmapovala	zmapovat	k5eAaPmAgFnS	zmapovat
krátery	kráter	k1gInPc4	kráter
na	na	k7c6	na
měsíčním	měsíční	k2eAgInSc6d1	měsíční
jižním	jižní	k2eAgInSc6d1	jižní
pólu	pól	k1gInSc6	pól
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zastíněny	zastíněn	k2eAgFnPc1d1	zastíněna
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
vůbec	vůbec	k9	vůbec
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nP	by
podle	podle	k7c2	podle
vědců	vědec	k1gMnPc2	vědec
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
právě	právě	k9	právě
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
kráterech	kráter	k1gInPc6	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
led	led	k1gInSc1	led
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
těžen	těžit	k5eAaImNgInS	těžit
a	a	k8xC	a
rozštěpen	rozštěpit	k5eAaPmNgInS	rozštěpit
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
elektrárnami	elektrárna	k1gFnPc7	elektrárna
založenými	založený	k2eAgFnPc7d1	založená
na	na	k7c6	na
solárních	solární	k2eAgInPc6d1	solární
panelech	panel	k1gInPc6	panel
nebo	nebo	k8xC	nebo
nukleárním	nukleární	k2eAgInSc7d1	nukleární
reaktorem	reaktor	k1gInSc7	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
použitelného	použitelný	k2eAgNnSc2d1	použitelné
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
osídlení	osídlení	k1gNnSc4	osídlení
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nákladnost	nákladnost	k1gFnSc1	nákladnost
přepravy	přeprava	k1gFnSc2	přeprava
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
by	by	k9	by
podobný	podobný	k2eAgInSc4d1	podobný
projekt	projekt	k1gInSc4	projekt
prakticky	prakticky	k6eAd1	prakticky
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Kameny	kámen	k1gInPc1	kámen
z	z	k7c2	z
měsíčního	měsíční	k2eAgInSc2d1	měsíční
rovníku	rovník	k1gInSc2	rovník
sesbírané	sesbíraný	k2eAgMnPc4d1	sesbíraný
astronauty	astronaut	k1gMnPc4	astronaut
z	z	k7c2	z
Apolla	Apollo	k1gNnSc2	Apollo
neobsahovaly	obsahovat	k5eNaImAgInP	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
stopy	stopa	k1gFnPc4	stopa
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Lunar	Lunar	k1gMnSc1	Lunar
Prospector	Prospector	k1gInSc1	Prospector
ani	ani	k8xC	ani
dřívější	dřívější	k2eAgNnSc1d1	dřívější
mapování	mapování	k1gNnSc1	mapování
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
organizované	organizovaný	k2eAgFnPc4d1	organizovaná
například	například	k6eAd1	například
Smithsonovým	Smithsonův	k2eAgInSc7d1	Smithsonův
ústavem	ústav	k1gInSc7	ústav
<g/>
,	,	kIx,	,
nepřinesly	přinést	k5eNaPmAgFnP	přinést
žádný	žádný	k3yNgInSc4	žádný
přímý	přímý	k2eAgInSc4d1	přímý
důkaz	důkaz	k1gInSc4	důkaz
měsíční	měsíční	k2eAgFnSc2d1	měsíční
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ledu	led	k1gInSc2	led
nebo	nebo	k8xC	nebo
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
sondy	sonda	k1gFnSc2	sonda
Lunar	Lunara	k1gFnPc2	Lunara
Prospector	Prospector	k1gMnSc1	Prospector
však	však	k9	však
přesto	přesto	k8xC	přesto
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
stálého	stálý	k2eAgInSc2d1	stálý
stínu	stín	k1gInSc2	stín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
nacházet	nacházet	k5eAaImF	nacházet
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
uskutečněné	uskutečněný	k2eAgNnSc4d1	uskutečněné
radarové	radarový	k2eAgNnSc4d1	radarové
pozorování	pozorování	k1gNnSc4	pozorování
oblasti	oblast	k1gFnSc2	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
Měsíce	měsíc	k1gInSc2	měsíc
přítomnost	přítomnost	k1gFnSc1	přítomnost
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
na	na	k7c6	na
dně	dna	k1gFnSc6	dna
kráterů	kráter	k1gInPc2	kráter
neprokázala	prokázat	k5eNaPmAgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Měsíc	měsíc	k1gInSc1	měsíc
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgFnSc2d1	slabá
magnetické	magnetický	k2eAgFnSc2d1	magnetická
pole	pole	k1gFnSc2	pole
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
být	být	k5eAaImF	být
nemuselo	muset	k5eNaImAgNnS	muset
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Měsíc	měsíc	k1gInSc1	měsíc
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
současné	současný	k2eAgNnSc1d1	současné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
bylo	být	k5eAaImAgNnS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
4,25	[number]	k4	4,25
až	až	k9	až
3,56	[number]	k4	3,56
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
část	část	k1gFnSc1	část
měsíčního	měsíční	k2eAgInSc2d1	měsíční
magnetismu	magnetismus	k1gInSc2	magnetismus
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
vlastní	vlastní	k2eAgNnSc4d1	vlastní
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
pásmo	pásmo	k1gNnSc1	pásmo
měsíční	měsíční	k2eAgFnSc2d1	měsíční
kůry	kůra	k1gFnSc2	kůra
zvané	zvaný	k2eAgFnSc2d1	zvaná
Rima	Rima	k1gFnSc1	Rima
Sirsalis	Sirsalis	k1gFnPc7	Sirsalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolize	kolize	k1gFnSc1	kolize
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
nebeskými	nebeský	k2eAgFnPc7d1	nebeská
tělesy	těleso	k1gNnPc7	těleso
jeho	jeho	k3xOp3gFnSc7	jeho
magnetické	magnetický	k2eAgFnPc4d1	magnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
posílila	posílit	k5eAaPmAgFnS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
těleso	těleso	k1gNnSc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
bez	bez	k7c2	bez
atmosféry	atmosféra	k1gFnSc2	atmosféra
jako	jako	k8xS	jako
Měsíc	měsíc	k1gInSc1	měsíc
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
magnetismus	magnetismus	k1gInSc4	magnetismus
díky	díky	k7c3	díky
dopadům	dopad	k1gInPc3	dopad
komet	kometa	k1gFnPc2	kometa
a	a	k8xC	a
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přetrvávající	přetrvávající	k2eAgFnSc7d1	přetrvávající
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
otázkou	otázka	k1gFnSc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Magnetická	magnetický	k2eAgNnPc1d1	magnetické
měření	měření	k1gNnPc1	měření
mohou	moct	k5eAaImIp3nP	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
také	také	k9	také
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
elektrické	elektrický	k2eAgFnSc6d1	elektrická
vodivosti	vodivost	k1gFnSc6	vodivost
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
jádra	jádro	k1gNnSc2	jádro
-	-	kIx~	-
tyto	tento	k3xDgInPc1	tento
výsledky	výsledek	k1gInPc1	výsledek
by	by	kYmCp3nP	by
vědcům	vědec	k1gMnPc3	vědec
pomohly	pomoct	k5eAaPmAgInP	pomoct
lépe	dobře	k6eAd2	dobře
porozumět	porozumět	k5eAaPmF	porozumět
původu	původ	k1gInSc3	původ
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
magnetických	magnetický	k2eAgInPc2d1	magnetický
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
železo	železo	k1gNnSc4	železo
<g/>
)	)	kIx)	)
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ubralo	ubrat	k5eAaPmAgNnS	ubrat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
teorii	teorie	k1gFnSc4	teorie
velkého	velký	k2eAgInSc2d1	velký
impaktu	impakt	k1gInSc2	impakt
na	na	k7c6	na
věrohodnosti	věrohodnost	k1gFnSc6	věrohodnost
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
alternativní	alternativní	k2eAgNnPc4d1	alternativní
vysvětlení	vysvětlení	k1gNnPc4	vysvětlení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgFnPc2	který
by	by	kYmCp3nS	by
měsíční	měsíční	k2eAgFnSc1d1	měsíční
kůra	kůra	k1gFnSc1	kůra
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
obsahovat	obsahovat	k5eAaImF	obsahovat
méně	málo	k6eAd2	málo
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
nevýznamnou	významný	k2eNgFnSc4d1	nevýznamná
a	a	k8xC	a
řídkou	řídký	k2eAgFnSc4d1	řídká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
v	v	k7c6	v
takto	takto	k6eAd1	takto
řídké	řídký	k2eAgFnSc6d1	řídká
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
téměř	téměř	k6eAd1	téměř
nesrážejí	srážet	k5eNaImIp3nP	srážet
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnSc1	jejich
střední	střední	k2eAgFnSc1d1	střední
volná	volný	k2eAgFnSc1d1	volná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
této	tento	k3xDgFnSc2	tento
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
odplynování	odplynování	k1gNnSc1	odplynování
-	-	kIx~	-
uvolňování	uvolňování	k1gNnSc1	uvolňování
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
radonu	radon	k1gInSc2	radon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
hluboko	hluboko	k6eAd1	hluboko
z	z	k7c2	z
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
nitra	nitro	k1gNnSc2	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
rychle	rychle	k6eAd1	rychle
zachycován	zachycovat	k5eAaImNgInS	zachycovat
měsíční	měsíční	k2eAgFnSc7d1	měsíční
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zatmění	zatmění	k1gNnSc2	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
jde	jít	k5eAaImIp3nS	jít
vskutku	vskutku	k9	vskutku
jen	jen	k9	jen
o	o	k7c4	o
shodu	shoda	k1gFnSc4	shoda
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
úhlové	úhlový	k2eAgInPc1d1	úhlový
průměry	průměr	k1gInPc1	průměr
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
viděné	viděná	k1gFnSc2	viděná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
změn	změna	k1gFnPc2	změna
schopny	schopen	k2eAgInPc1d1	schopen
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
překrývat	překrývat	k5eAaImF	překrývat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jak	jak	k8xC	jak
úplné	úplný	k2eAgNnSc1d1	úplné
tak	tak	k6eAd1	tak
i	i	k9	i
prstencové	prstencový	k2eAgNnSc4d1	prstencové
zatmění	zatmění	k1gNnSc4	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úplném	úplný	k2eAgNnSc6d1	úplné
zatmění	zatmění	k1gNnSc6	zatmění
Měsíc	měsíc	k1gInSc1	měsíc
kompletně	kompletně	k6eAd1	kompletně
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
sluneční	sluneční	k2eAgInSc1d1	sluneční
disk	disk	k1gInSc1	disk
a	a	k8xC	a
sluneční	sluneční	k2eAgFnSc1d1	sluneční
koróna	koróna	k1gFnSc1	koróna
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
Měsícem	měsíc	k1gInSc7	měsíc
a	a	k8xC	a
Zemí	zem	k1gFnSc7	zem
během	během	k7c2	během
času	čas	k1gInSc2	čas
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
,	,	kIx,	,
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
několika	několik	k4yIc7	několik
milióny	milión	k4xCgInPc1	milión
let	léto	k1gNnPc2	léto
při	při	k7c6	při
slunečním	sluneční	k2eAgNnSc6d1	sluneční
zatmění	zatmění	k1gNnSc6	zatmění
Měsíc	měsíc	k1gInSc4	měsíc
Slunce	slunce	k1gNnSc2	slunce
vždycky	vždycky	k6eAd1	vždycky
úplně	úplně	k6eAd1	úplně
zakryl	zakrýt	k5eAaPmAgMnS	zakrýt
a	a	k8xC	a
nemohlo	moct	k5eNaImAgNnS	moct
nastat	nastat	k5eAaPmF	nastat
žádné	žádný	k3yNgNnSc4	žádný
prstencové	prstencový	k2eAgNnSc4d1	prstencové
zatmění	zatmění	k1gNnSc4	zatmění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
za	za	k7c4	za
několik	několik	k4yIc4	několik
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
už	už	k9	už
nebude	být	k5eNaImBp3nS	být
Měsíc	měsíc	k1gInSc1	měsíc
schopen	schopen	k2eAgInSc1d1	schopen
Slunce	slunce	k1gNnSc4	slunce
úplně	úplně	k6eAd1	úplně
zakrýt	zakrýt	k5eAaPmF	zakrýt
a	a	k8xC	a
žádná	žádný	k3yNgNnPc1	žádný
úplná	úplný	k2eAgNnPc1d1	úplné
zatmění	zatmění	k1gNnPc1	zatmění
už	už	k6eAd1	už
nebudou	být	k5eNaImBp3nP	být
nastávat	nastávat	k5eAaImF	nastávat
<g/>
.	.	kIx.	.
</s>
<s>
Zatmění	zatmění	k1gNnPc1	zatmění
nastávají	nastávat	k5eAaImIp3nP	nastávat
jen	jen	k9	jen
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnPc1d1	sluneční
zatmění	zatmění	k1gNnPc1	zatmění
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
novu	nov	k1gInSc6	nov
<g/>
;	;	kIx,	;
zatmění	zatmění	k1gNnSc1	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíční	měsíční	k2eAgFnSc2d1	měsíční
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
většími	veliký	k2eAgInPc7d2	veliký
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
horizontu	horizont	k1gInSc3	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čistě	čistě	k6eAd1	čistě
psychologický	psychologický	k2eAgInSc4d1	psychologický
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Měsíční	měsíční	k2eAgFnSc2d1	měsíční
iluze	iluze	k1gFnPc1	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
Měsíce	měsíc	k1gInSc2	měsíc
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
půl	půl	k1xP	půl
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgNnSc1d1	různé
světleji	světle	k6eAd2	světle
a	a	k8xC	a
tmavěji	tmavě	k6eAd2	tmavě
zabarvené	zabarvený	k2eAgFnSc6d1	zabarvená
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
především	především	k9	především
měsíční	měsíční	k2eAgNnSc1d1	měsíční
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzor	vzor	k1gInSc1	vzor
viděný	viděný	k2eAgInSc1d1	viděný
různými	různý	k2eAgFnPc7d1	různá
kulturami	kultura	k1gFnPc7	kultura
jako	jako	k9	jako
Muž	muž	k1gMnSc1	muž
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
králík	králík	k1gMnSc1	králík
a	a	k8xC	a
bizon	bizon	k1gMnSc1	bizon
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
a	a	k8xC	a
horské	horský	k2eAgInPc1d1	horský
hřbety	hřbet	k1gInPc1	hřbet
také	také	k9	také
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nápadné	nápadný	k2eAgInPc4d1	nápadný
měsíční	měsíční	k2eAgInPc4d1	měsíční
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nejjasnějšího	jasný	k2eAgInSc2d3	nejjasnější
úplňku	úplněk	k1gInSc2	úplněk
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
Měsíc	měsíc	k1gInSc1	měsíc
magnitudu	magnitudu	k6eAd1	magnitudu
asi	asi	k9	asi
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
magnitudu	magnitudu	k6eAd1	magnitudu
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgInSc1d3	nejjasnější
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gMnSc4	on
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
kolísá	kolísat	k5eAaImIp3nS	kolísat
největší	veliký	k2eAgFnSc1d3	veliký
výška	výška	k1gFnSc1	výška
Měsíce	měsíc	k1gInSc2	měsíc
ve	v	k7c6	v
dne	den	k1gInSc2	den
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
mezích	mez	k1gFnPc6	mez
jako	jako	k9	jako
největší	veliký	k2eAgFnSc1d3	veliký
výška	výška	k1gFnSc1	výška
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
a	a	k8xC	a
měsíční	měsíční	k2eAgFnSc6d1	měsíční
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
putuje	putovat	k5eAaImIp3nS	putovat
Měsíc	měsíc	k1gInSc4	měsíc
nejvýše	vysoce	k6eAd3	vysoce
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
a	a	k8xC	a
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
putuje	putovat	k5eAaImIp3nS	putovat
nejvýše	nejvýše	k6eAd1	nejvýše
právě	právě	k6eAd1	právě
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
putuje	putovat	k5eAaImIp3nS	putovat
nejvýše	vysoce	k6eAd3	vysoce
mj.	mj.	kA	mj.
i	i	k9	i
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
v	v	k7c6	v
první	první	k4xOgFnSc6	první
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
cca	cca	kA	cca
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
až	až	k9	až
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
násl	násla	k1gFnPc2	násla
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
až	až	k6eAd1	až
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
násl	násla	k1gFnPc2	násla
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
mj.	mj.	kA	mj.
i	i	k9	i
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
až	až	k9	až
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
v	v	k7c6	v
první	první	k4xOgFnSc6	první
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
až	až	k9	až
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
zapadá	zapadat	k5eAaImIp3nS	zapadat
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
oběhu	oběh	k1gInSc3	oběh
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
cca	cca	kA	cca
o	o	k7c4	o
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
předchozí	předchozí	k2eAgInSc4d1	předchozí
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zpožďování	zpožďování	k1gNnSc1	zpožďování
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
cca	cca	kA	cca
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
kolik	kolik	k4yRc4	kolik
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgNnSc4d2	menší
zpoždění	zpoždění	k1gNnSc4	zpoždění
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
o	o	k7c4	o
tolik	tolik	k4yIc4	tolik
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc4d2	veliký
zpoždění	zpoždění	k1gNnSc4	zpoždění
západu	západ	k1gInSc2	západ
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
hodnoty	hodnota	k1gFnPc1	hodnota
nastávají	nastávat	k5eAaImIp3nP	nastávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Měsíc	měsíc	k1gInSc1	měsíc
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
své	svůj	k3xOyFgFnPc4	svůj
nejsevernější	severní	k2eAgFnPc4d3	nejsevernější
nebo	nebo	k8xC	nebo
nejjižnější	jižní	k2eAgFnPc4d3	nejjižnější
deklinace	deklinace	k1gFnPc4	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
jsou	být	k5eAaImIp3nP	být
nejmenší	malý	k2eAgInPc1d3	nejmenší
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
východy	východ	k1gInPc7	východ
(	(	kIx(	(
<g/>
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
mezi	mezi	k7c7	mezi
západy	západ	k1gInPc7	západ
<g/>
)	)	kIx)	)
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Měsíc	měsíc	k1gInSc1	měsíc
prochází	procházet	k5eAaImIp3nS	procházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jarního	jarní	k2eAgInSc2d1	jarní
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
dráha	dráha	k1gFnSc1	dráha
Měsíce	měsíc	k1gInSc2	měsíc
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
při	při	k7c6	při
východu	východ	k1gInSc6	východ
svírá	svírat	k5eAaImIp3nS	svírat
s	s	k7c7	s
obzorem	obzor	k1gInSc7	obzor
menší	malý	k2eAgInSc4d2	menší
úhel	úhel	k1gInSc4	úhel
než	než	k8xS	než
nebeský	nebeský	k2eAgInSc4d1	nebeský
rovník	rovník	k1gInSc4	rovník
a	a	k8xC	a
při	při	k7c6	při
západu	západ	k1gInSc6	západ
větší	veliký	k2eAgInSc4d2	veliký
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
první	první	k4xOgFnSc6	první
čtvrti	čtvrt	k1gFnSc6	čtvrt
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
úplňku	úplněk	k1gInSc2	úplněk
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrti	čtvrt	k1gFnSc6	čtvrt
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
další	další	k2eAgInSc4d1	další
průvodní	průvodní	k2eAgInSc4d1	průvodní
jev	jev	k1gInSc4	jev
-	-	kIx~	-
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
stojatý	stojatý	k2eAgMnSc1d1	stojatý
při	při	k7c6	při
východu	východ	k1gInSc6	východ
a	a	k8xC	a
ležatý	ležatý	k2eAgMnSc1d1	ležatý
při	při	k7c6	při
západu	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
sledovat	sledovat	k5eAaImF	sledovat
"	"	kIx"	"
<g/>
starý	starý	k2eAgInSc4d1	starý
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
"	"	kIx"	"
v	v	k7c6	v
časném	časný	k2eAgNnSc6d1	časné
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
utápí	utápět	k5eAaImIp3nS	utápět
příliš	příliš	k6eAd1	příliš
nízko	nízko	k6eAd1	nízko
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
začíná	začínat	k5eAaImIp3nS	začínat
světlat	světlat	k5eAaImF	světlat
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
"	"	kIx"	"
<g/>
mladého	mladý	k2eAgInSc2d1	mladý
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
"	"	kIx"	"
na	na	k7c6	na
večerní	večerní	k2eAgFnSc6d1	večerní
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Opačné	opačný	k2eAgFnPc1d1	opačná
podmínky	podmínka	k1gFnPc1	podmínka
nastávají	nastávat	k5eAaImIp3nP	nastávat
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
podzimního	podzimní	k2eAgInSc2d1	podzimní
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
dráha	dráha	k1gFnSc1	dráha
přechází	přecházet	k5eAaImIp3nS	přecházet
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
východy	východ	k1gInPc7	východ
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgMnSc1d1	maximální
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
úhel	úhel	k1gInSc1	úhel
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
obzoru	obzor	k1gInSc3	obzor
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
západy	západ	k1gInPc7	západ
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgInSc1d1	minimální
(	(	kIx(	(
<g/>
malý	malý	k2eAgInSc1d1	malý
úhel	úhel	k1gInSc1	úhel
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
obzoru	obzor	k1gInSc3	obzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrti	čtvrt	k1gFnSc6	čtvrt
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
úplňku	úplněk	k1gInSc2	úplněk
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
první	první	k4xOgFnSc3	první
čtvrti	čtvrt	k1gFnSc3	čtvrt
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgMnSc4	ten
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadné	snadný	k2eAgNnSc4d1	snadné
pozorování	pozorování	k1gNnSc4	pozorování
"	"	kIx"	"
<g/>
starého	starý	k2eAgInSc2d1	starý
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
"	"	kIx"	"
v	v	k7c6	v
časném	časný	k2eAgInSc6d1	časný
podzimu	podzim	k1gInSc6	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podzimních	podzimní	k2eAgInPc6d1	podzimní
večerech	večer	k1gInPc6	večer
obtížné	obtížný	k2eAgNnSc1d1	obtížné
sledovat	sledovat	k5eAaImF	sledovat
"	"	kIx"	"
<g/>
mladý	mladý	k2eAgInSc4d1	mladý
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
"	"	kIx"	"
<g/>
starý	starý	k2eAgInSc1d1	starý
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
"	"	kIx"	"
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
osvětlená	osvětlený	k2eAgFnSc1d1	osvětlená
část	část	k1gFnSc1	část
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
jasné	jasný	k2eAgFnSc6d1	jasná
obloze	obloha	k1gFnSc6	obloha
viditelný	viditelný	k2eAgInSc1d1	viditelný
i	i	k8xC	i
jeho	jeho	k3xOp3gInSc1	jeho
neosvětlený	osvětlený	k2eNgInSc1d1	neosvětlený
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gInSc4	on
ozařuje	ozařovat	k5eAaImIp3nS	ozařovat
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
vůči	vůči	k7c3	vůči
Měsíci	měsíc	k1gInSc3	měsíc
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
osvětlená	osvětlený	k2eAgFnSc1d1	osvětlená
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
popelavý	popelavý	k2eAgInSc1d1	popelavý
svit	svit	k1gInSc1	svit
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
prvním	první	k4xOgInPc3	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jej	on	k3xPp3gInSc4	on
správně	správně	k6eAd1	správně
vyložil	vyložit	k5eAaPmAgMnS	vyložit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
5	[number]	k4	5
000	[number]	k4	000
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
otesaný	otesaný	k2eAgInSc1d1	otesaný
kámen	kámen	k1gInSc1	kámen
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
Knowth	Knowth	k1gInSc1	Knowth
asi	asi	k9	asi
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dosud	dosud	k6eAd1	dosud
objevené	objevený	k2eAgNnSc4d1	objevené
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
objevením	objevení	k1gNnSc7	objevení
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
uznali	uznat	k5eAaPmAgMnP	uznat
Měsíc	měsíc	k1gInSc4	měsíc
za	za	k7c4	za
sféru	sféra	k1gFnSc4	sféra
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dokonale	dokonale	k6eAd1	dokonale
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
v	v	k7c6	v
Leicesteerském	Leicesteerský	k2eAgInSc6d1	Leicesteerský
kodexu	kodex	k1gInSc6	kodex
(	(	kIx(	(
<g/>
napsán	napsat	k5eAaBmNgInS	napsat
mezi	mezi	k7c4	mezi
1506	[number]	k4	1506
a	a	k8xC	a
1510	[number]	k4	1510
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
hmotné	hmotný	k2eAgNnSc4d1	hmotné
těleso	těleso	k1gNnSc4	těleso
těžší	těžký	k2eAgNnSc4d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc4	vzduch
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
správně	správně	k6eAd1	správně
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
jev	jev	k1gInSc4	jev
tzv.	tzv.	kA	tzv.
popelavého	popelavý	k2eAgInSc2d1	popelavý
svitu	svit	k1gInSc2	svit
jako	jako	k8xC	jako
odraz	odraz	k1gInSc1	odraz
záře	zář	k1gFnSc2	zář
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
měsíčního	měsíční	k2eAgInSc2d1	měsíční
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1609	[number]	k4	1609
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnSc3	Galile
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
Sidereus	Sidereus	k1gMnSc1	Sidereus
Nuncius	nuncius	k1gMnSc1	nuncius
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgFnPc2	první
kreseb	kresba	k1gFnPc2	kresba
Měsíce	měsíc	k1gInSc2	měsíc
pozorovaného	pozorovaný	k2eAgInSc2d1	pozorovaný
dalekohledem	dalekohled	k1gInSc7	dalekohled
a	a	k8xC	a
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
hladký	hladký	k2eAgMnSc1d1	hladký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
krátery	kráter	k1gInPc4	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nakreslili	nakreslit	k5eAaPmAgMnP	nakreslit
Giovanni	Giovann	k1gMnPc1	Giovann
Battista	Battista	k1gMnSc1	Battista
Riccioli	Ricciole	k1gFnSc6	Ricciole
a	a	k8xC	a
Francesco	Francesco	k6eAd1	Francesco
Maria	Maria	k1gFnSc1	Maria
Grimaldi	Grimald	k1gMnPc1	Grimald
mapu	mapa	k1gFnSc4	mapa
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
řadu	řada	k1gFnSc4	řada
kráterů	kráter	k1gInPc2	kráter
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
známe	znát	k5eAaImIp1nP	znát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
se	se	k3xPyFc4	se
temné	temný	k2eAgFnPc1d1	temná
části	část	k1gFnPc1	část
měsíčního	měsíční	k2eAgInSc2d1	měsíční
povrchu	povrch	k1gInSc2	povrch
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc4	moře
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
mare	mare	k6eAd1	mare
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
maria	marium	k1gNnSc2	marium
<g/>
)	)	kIx)	)
a	a	k8xC	a
světlejší	světlý	k2eAgFnPc1d2	světlejší
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
pevniny	pevnina	k1gFnPc4	pevnina
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
terra	terra	k6eAd1	terra
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
terrae	terra	k1gFnSc2	terra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
existence	existence	k1gFnSc2	existence
vegetace	vegetace	k1gFnSc2	vegetace
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
osídlení	osídlení	k1gNnSc2	osídlení
"	"	kIx"	"
<g/>
selenity	selenita	k1gFnSc2	selenita
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
seriózně	seriózně	k6eAd1	seriózně
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
některými	některý	k3yIgMnPc7	některý
významnými	významný	k2eAgMnPc7d1	významný
astronomy	astronom	k1gMnPc7	astronom
až	až	k6eAd1	až
do	do	k7c2	do
prvních	první	k4xOgNnPc2	první
desetiletí	desetiletí	k1gNnPc2	desetiletí
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
nechala	nechat	k5eAaPmAgFnS	nechat
napálit	napálit	k5eAaPmF	napálit
sérií	série	k1gFnSc7	série
článků	článek	k1gInPc2	článek
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Sun	Sun	kA	Sun
o	o	k7c6	o
smyšleném	smyšlený	k2eAgInSc6d1	smyšlený
objevu	objev	k1gInSc6	objev
exotických	exotický	k2eAgNnPc2d1	exotické
zvířat	zvíře	k1gNnPc2	zvíře
žijících	žijící	k2eAgNnPc2d1	žijící
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1834	[number]	k4	1834
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
publikovali	publikovat	k5eAaBmAgMnP	publikovat
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Beer	Beer	k1gMnSc1	Beer
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mädler	Mädler	k1gMnSc1	Mädler
své	svůj	k3xOyFgNnSc4	svůj
čtyřdílné	čtyřdílný	k2eAgNnSc4d1	čtyřdílné
kartografické	kartografický	k2eAgNnSc4d1	kartografické
dílo	dílo	k1gNnSc4	dílo
Mappa	Mapp	k1gMnSc2	Mapp
Selenographica	Selenographicus	k1gMnSc2	Selenographicus
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
knihu	kniha	k1gFnSc4	kniha
Der	drát	k5eAaImRp2nS	drát
Mond	mond	k1gInSc1	mond
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
solidním	solidní	k2eAgInSc7d1	solidní
způsobem	způsob	k1gInSc7	způsob
zdůvodnila	zdůvodnit	k5eAaPmAgFnS	zdůvodnit
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Měsíc	měsíc	k1gInSc1	měsíc
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
vodní	vodní	k2eAgFnPc4d1	vodní
plochy	plocha	k1gFnPc4	plocha
ani	ani	k8xC	ani
patrnou	patrný	k2eAgFnSc4d1	patrná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Spornou	sporný	k2eAgFnSc7d1	sporná
otázkou	otázka	k1gFnSc7	otázka
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
rysy	rys	k1gInPc1	rys
Měsíce	měsíc	k1gInSc2	měsíc
mohou	moct	k5eAaImIp3nP	moct
podléhat	podléhat	k5eAaImF	podléhat
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
prohlašovali	prohlašovat	k5eAaImAgMnP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jisté	jistý	k2eAgInPc1d1	jistý
malé	malý	k2eAgInPc1d1	malý
krátery	kráter	k1gInPc1	kráter
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
a	a	k8xC	a
zase	zase	k9	zase
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
omyly	omyl	k1gInPc4	omyl
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
světelnými	světelný	k2eAgFnPc7d1	světelná
podmínkami	podmínka	k1gFnPc7	podmínka
nebo	nebo	k8xC	nebo
nepřesnostmi	nepřesnost	k1gFnPc7	nepřesnost
ve	v	k7c6	v
starých	starý	k2eAgInPc6d1	starý
nákresech	nákres	k1gInPc6	nákres
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
dnes	dnes	k6eAd1	dnes
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jevu	jev	k1gInSc3	jev
odplynování	odplynování	k1gNnSc2	odplynování
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nacistického	nacistický	k2eAgNnSc2d1	nacistické
období	období	k1gNnSc2	období
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
nacističtí	nacistický	k2eAgMnPc1d1	nacistický
vůdci	vůdce	k1gMnPc1	vůdce
teorii	teorie	k1gFnSc4	teorie
Welteislehre	Welteislehr	k1gInSc5	Welteislehr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pevným	pevný	k2eAgInSc7d1	pevný
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
člověkem	člověk	k1gMnSc7	člověk
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
automatická	automatický	k2eAgFnSc1d1	automatická
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Luna	luna	k1gFnSc1	luna
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1959	[number]	k4	1959
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
Z.	Z.	kA	Z.
</s>
<s>
Odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
neznámá	známý	k2eNgFnSc1d1	neznámá
až	až	k9	až
do	do	k7c2	do
průletu	průlet	k1gInSc2	průlet
sovětské	sovětský	k2eAgFnSc2d1	sovětská
sondy	sonda	k1gFnSc2	sonda
Luna	luna	k1gFnSc1	luna
3	[number]	k4	3
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
zmapování	zmapování	k1gNnSc1	zmapování
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
amerického	americký	k2eAgInSc2d1	americký
programu	program	k1gInSc2	program
Lunar	Lunar	k1gMnSc1	Lunar
Orbiter	Orbiter	k1gMnSc1	Orbiter
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Luna	luna	k1gFnSc1	luna
9	[number]	k4	9
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měkce	měkko	k6eAd1	měkko
přistála	přistát	k5eAaPmAgFnS	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1966	[number]	k4	1966
přenesla	přenést	k5eAaPmAgFnS	přenést
obrázky	obrázek	k1gInPc4	obrázek
měsíčního	měsíční	k2eAgInSc2d1	měsíční
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
umělým	umělý	k2eAgInSc7d1	umělý
satelitem	satelit	k1gInSc7	satelit
Měsíce	měsíc	k1gInSc2	měsíc
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Luna	luna	k1gFnSc1	luna
10	[number]	k4	10
(	(	kIx(	(
<g/>
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
Apolla	Apollo	k1gNnSc2	Apollo
8	[number]	k4	8
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Borman	Borman	k1gMnSc1	Borman
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Lovell	Lovell	k1gMnSc1	Lovell
a	a	k8xC	a
William	William	k1gInSc1	William
Anders	Andersa	k1gFnPc2	Andersa
<g/>
,	,	kIx,	,
se	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
stali	stát	k5eAaPmAgMnP	stát
prvními	první	k4xOgMnPc7	první
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
viděli	vidět	k5eAaImAgMnP	vidět
odvrácenou	odvrácený	k2eAgFnSc4d1	odvrácená
stranu	strana	k1gFnSc4	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
poprvé	poprvé	k6eAd1	poprvé
přistáli	přistát	k5eAaPmAgMnP	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
studenou	studený	k2eAgFnSc7d1	studená
válkou	válka	k1gFnSc7	válka
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
závod	závod	k1gInSc1	závod
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgInPc7d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
kráčejícím	kráčející	k2eAgMnSc7d1	kráčející
po	po	k7c6	po
měsíčním	měsíční	k2eAgInSc6d1	měsíční
povrchu	povrch	k1gInSc6	povrch
byl	být	k5eAaImAgMnS	být
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
americké	americký	k2eAgFnSc2d1	americká
mise	mise	k1gFnSc2	mise
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Eugene	Eugen	k1gInSc5	Eugen
Cernan	Cernan	k1gMnSc1	Cernan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
Apollo	Apollo	k1gNnSc1	Apollo
17	[number]	k4	17
kráčel	kráčet	k5eAaImAgMnS	kráčet
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
návštěvníků	návštěvník	k1gMnPc2	návštěvník
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgInPc1d1	měsíční
vzorky	vzorek	k1gInPc1	vzorek
přivezené	přivezený	k2eAgInPc1d1	přivezený
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
šesti	šest	k4xCc2	šest
misí	mise	k1gFnPc2	mise
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
a	a	k8xC	a
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
misí	mise	k1gFnPc2	mise
Luna	luna	k1gFnSc1	luna
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
16	[number]	k4	16
<g/>
,	,	kIx,	,
20	[number]	k4	20
a	a	k8xC	a
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
se	se	k3xPyFc4	se
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
plánu	plán	k1gInSc3	plán
na	na	k7c6	na
obnovení	obnovení	k1gNnSc6	obnovení
letů	let	k1gInPc2	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
2005	[number]	k4	2005
organizace	organizace	k1gFnSc2	organizace
NASA	NASA	kA	NASA
upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
tyto	tento	k3xDgInPc4	tento
plány	plán	k1gInPc4	plán
a	a	k8xC	a
oznámila	oznámit	k5eAaPmAgFnS	oznámit
jako	jako	k9	jako
cílové	cílový	k2eAgNnSc4d1	cílové
datum	datum	k1gNnSc4	datum
nového	nový	k2eAgNnSc2d1	nové
přistání	přistání	k1gNnSc2	přistání
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
rok	rok	k1gInSc4	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
předcházet	předcházet	k5eAaImF	předcházet
sonda	sonda	k1gFnSc1	sonda
Lunar	Lunar	k1gInSc4	Lunar
Reconnaissance	Reconnaissance	k1gFnSc2	Reconnaissance
Orbiter	Orbitra	k1gFnPc2	Orbitra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
astronautů	astronaut	k1gMnPc2	astronaut
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
vyvíjena	vyvíjet	k5eAaImNgFnS	vyvíjet
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Orion	orion	k1gInSc1	orion
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
plán	plán	k1gInSc4	plán
na	na	k7c4	na
brzké	brzký	k2eAgNnSc4d1	brzké
vypuštění	vypuštění	k1gNnSc4	vypuštění
sond	sonda	k1gFnPc2	sonda
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
Smart	Smart	k1gInSc1	Smart
1	[number]	k4	1
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2003	[number]	k4	2003
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
měsíční	měsíční	k2eAgFnSc4d1	měsíční
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Sledovala	sledovat	k5eAaImAgFnS	sledovat
měsíční	měsíční	k2eAgInSc4d1	měsíční
povrch	povrch	k1gInSc4	povrch
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vytvářet	vytvářet	k5eAaImF	vytvářet
jeho	jeho	k3xOp3gFnSc4	jeho
rentgenovou	rentgenový	k2eAgFnSc4d1	rentgenová
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
dráhu	dráha	k1gFnSc4	dráha
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2006	[number]	k4	2006
v	v	k7c6	v
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Pádem	Pád	k1gInSc7	Pád
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
oblak	oblak	k1gInSc1	oblak
hornin	hornina	k1gFnPc2	hornina
zasahující	zasahující	k2eAgFnSc1d1	zasahující
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
pak	pak	k6eAd1	pak
vědci	vědec	k1gMnPc1	vědec
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
spektroskopicky	spektroskopicky	k6eAd1	spektroskopicky
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
studovat	studovat	k5eAaImF	studovat
složení	složení	k1gNnSc4	složení
povrchu	povrch	k1gInSc2	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
ambiciózní	ambiciózní	k2eAgInPc4d1	ambiciózní
plány	plán	k1gInPc4	plán
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
zkoumání	zkoumání	k1gNnSc2	zkoumání
vhodných	vhodný	k2eAgNnPc2d1	vhodné
nalezišť	naleziště	k1gNnPc2	naleziště
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
hledání	hledání	k1gNnSc4	hledání
izotopu	izotop	k1gInSc2	izotop
hélium	hélium	k1gNnSc4	hélium
3	[number]	k4	3
využitelného	využitelný	k2eAgNnSc2d1	využitelné
jako	jako	k8xS	jako
energetický	energetický	k2eAgInSc1d1	energetický
zdroj	zdroj	k1gInSc1	zdroj
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
se	se	k3xPyFc4	se
také	také	k9	také
chystají	chystat	k5eAaImIp3nP	chystat
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
již	již	k6eAd1	již
načrtli	načrtnout	k5eAaPmAgMnP	načrtnout
plány	plán	k1gInPc4	plán
svých	svůj	k3xOyFgFnPc2	svůj
nadcházejících	nadcházející	k2eAgFnPc2d1	nadcházející
misí	mise	k1gFnPc2	mise
k	k	k7c3	k
našemu	náš	k3xOp1gMnSc3	náš
sousedovi	soused	k1gMnSc3	soused
<g/>
:	:	kIx,	:
Lunar-A	Lunar-A	k1gMnSc3	Lunar-A
a	a	k8xC	a
Selene	selen	k1gInSc5	selen
<g/>
.	.	kIx.	.
</s>
<s>
Japonskou	japonský	k2eAgFnSc7d1	japonská
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
agenturou	agentura	k1gFnSc7	agentura
(	(	kIx(	(
<g/>
JAXA	JAXA	kA	JAXA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
plánována	plánován	k2eAgFnSc1d1	plánována
obydlená	obydlený	k2eAgFnSc1d1	obydlená
lunární	lunární	k2eAgFnSc1d1	lunární
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
pokusem	pokus	k1gInSc7	pokus
Indie	Indie	k1gFnSc2	Indie
byl	být	k5eAaImAgInS	být
automatický	automatický	k2eAgInSc1d1	automatický
orbitální	orbitální	k2eAgInSc1d1	orbitální
satelit	satelit	k1gInSc1	satelit
Chandrayan	Chandrayana	k1gFnPc2	Chandrayana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
mise	mise	k1gFnSc2	mise
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Program	program	k1gInSc1	program
Ranger	Ranger	k1gInSc1	Ranger
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Program	program	k1gInSc1	program
Zond	Zond	k1gInSc1	Zond
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Program	program	k1gInSc1	program
<g />
.	.	kIx.	.
</s>
<s>
Surveyor	Surveyor	k1gInSc1	Surveyor
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Program	program	k1gInSc1	program
Lunar	Lunar	k1gMnSc1	Lunar
Orbiter	Orbiter	k1gMnSc1	Orbiter
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Program	program	k1gInSc1	program
Lunar	Lunar	k1gMnSc1	Lunar
Explorer	Explorer	k1gMnSc1	Explorer
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hiten	Hiten	k1gInSc1	Hiten
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Clementine	Clementin	k1gMnSc5	Clementin
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lunar	Lunar	k1gMnSc1	Lunar
Prospector	Prospector	k1gMnSc1	Prospector
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
astronaut	astronaut	k1gMnSc1	astronaut
nacházející	nacházející	k2eAgMnSc1d1	nacházející
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
odpoutat	odpoutat	k5eAaPmF	odpoutat
jak	jak	k6eAd1	jak
od	od	k7c2	od
Měsíce	měsíc	k1gInSc2	měsíc
tak	tak	k6eAd1	tak
i	i	k9	i
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
potřebná	potřebný	k2eAgFnSc1d1	potřebná
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
odmocninou	odmocnina	k1gFnSc7	odmocnina
součtu	součet	k1gInSc2	součet
čtverců	čtverec	k1gInPc2	čtverec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
únikových	únikový	k2eAgFnPc2d1	úniková
rychlostí	rychlost	k1gFnPc2	rychlost
-	-	kIx~	-
2,4	[number]	k4	2,4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
od	od	k7c2	od
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
dají	dát	k5eAaPmIp3nP	dát
celkově	celkově	k6eAd1	celkově
2,8	[number]	k4	2,8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Využije	využít	k5eAaPmIp3nS	využít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
orbitální	orbitální	k2eAgFnSc1d1	orbitální
rychlost	rychlost	k1gFnSc1	rychlost
1,1	[number]	k4	1,1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
urychlí	urychlit	k5eAaPmIp3nS	urychlit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
2,4	[number]	k4	2,4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dohromady	dohromady	k6eAd1	dohromady
dost	dost	k6eAd1	dost
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
si	se	k3xPyFc3	se
vědci	vědec	k1gMnPc1	vědec
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k6eAd1	uvnitř
zcela	zcela	k6eAd1	zcela
vychladlý	vychladlý	k2eAgInSc1d1	vychladlý
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
experimenty	experiment	k1gInPc1	experiment
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
že	že	k8xS	že
hlubší	hluboký	k2eAgFnPc1d2	hlubší
měsíční	měsíční	k2eAgFnPc1d1	měsíční
vrstvy	vrstva	k1gFnPc1	vrstva
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
rozžhavené	rozžhavený	k2eAgInPc1d1	rozžhavený
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
teplo	teplo	k1gNnSc1	teplo
proudí	proudit	k5eAaPmIp3nS	proudit
zevnitř	zevnitř	k6eAd1	zevnitř
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgNnSc1d1	měsíční
nitro	nitro	k1gNnSc1	nitro
zdaleka	zdaleka	k6eAd1	zdaleka
neprodukuje	produkovat	k5eNaImIp3nS	produkovat
takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
jako	jako	k8xS	jako
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
i	i	k9	i
Měsíc	měsíc	k1gInSc1	měsíc
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
lehké	lehký	k2eAgFnSc2d1	lehká
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
zhuštěného	zhuštěný	k2eAgNnSc2d1	zhuštěné
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
700	[number]	k4	700
km	km	kA	km
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zemské	zemský	k2eAgNnSc1d1	zemské
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
kapalné	kapalný	k2eAgNnSc1d1	kapalné
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
složení	složení	k1gNnSc4	složení
jádra	jádro	k1gNnSc2	jádro
Měsíce	měsíc	k1gInSc2	měsíc
ještě	ještě	k6eAd1	ještě
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
NASA	NASA	kA	NASA
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
Měsíce	měsíc	k1gInSc2	měsíc
pevné	pevný	k2eAgFnSc2d1	pevná
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
240	[number]	k4	240
km	km	kA	km
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgNnSc1d1	vnější
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
tekuté	tekutý	k2eAgNnSc1d1	tekuté
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
330	[number]	k4	330
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zemského	zemský	k2eAgNnSc2d1	zemské
jádra	jádro	k1gNnSc2	jádro
ho	on	k3xPp3gMnSc4	on
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
roztavená	roztavený	k2eAgFnSc1d1	roztavená
hraniční	hraniční	k2eAgFnSc1d1	hraniční
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
480	[number]	k4	480
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
nepřibližuje	přibližovat	k5eNaImIp3nS	přibližovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
(	(	kIx(	(
<g/>
cca	cca	kA	cca
o	o	k7c4	o
4	[number]	k4	4
cm	cm	kA	cm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
období	období	k1gNnSc2	období
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
prakticky	prakticky	k6eAd1	prakticky
stejně	stejně	k6eAd1	stejně
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xC	jako
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Osvětlení	osvětlení	k1gNnSc1	osvětlení
Země	zem	k1gFnSc2	zem
měsíčním	měsíční	k2eAgInSc7d1	měsíční
úplňkem	úplněk	k1gInSc7	úplněk
(	(	kIx(	(
<g/>
cca	cca	kA	cca
0,25	[number]	k4	0,25
luxu	lux	k1gInSc2	lux
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
400	[number]	k4	400
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
osvětlení	osvětlení	k1gNnSc1	osvětlení
dané	daný	k2eAgFnPc1d1	daná
Sluncem	slunce	k1gNnSc7	slunce
v	v	k7c6	v
nadhlavníku	nadhlavník	k1gInSc6	nadhlavník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
zazněla	zaznět	k5eAaImAgFnS	zaznět
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
magnetofonová	magnetofonový	k2eAgFnSc1d1	magnetofonová
nahrávka	nahrávka	k1gFnSc1	nahrávka
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
Novosvětské	novosvětský	k2eAgFnSc2d1	Novosvětská
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
umístila	umístit	k5eAaPmAgFnS	umístit
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
15	[number]	k4	15
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
asi	asi	k9	asi
8	[number]	k4	8
cm	cm	kA	cm
velkou	velký	k2eAgFnSc4d1	velká
hliníkovou	hliníkový	k2eAgFnSc4d1	hliníková
sošku	soška	k1gFnSc4	soška
astronauta	astronaut	k1gMnSc2	astronaut
ve	v	k7c6	v
skafandru	skafandr	k1gInSc6	skafandr
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Fallen	Fallen	k2eAgMnSc1d1	Fallen
Astronaut	astronaut	k1gMnSc1	astronaut
od	od	k7c2	od
belgického	belgický	k2eAgMnSc2d1	belgický
umělce	umělec	k1gMnSc2	umělec
Paula	Paul	k1gMnSc2	Paul
Van	van	k1gInSc4	van
Hoeydoncka	Hoeydoncko	k1gNnSc2	Hoeydoncko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
plaketou	plaketa	k1gFnSc7	plaketa
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
uvedena	uveden	k2eAgNnPc1d1	uvedeno
jména	jméno	k1gNnPc1	jméno
osmi	osm	k4xCc2	osm
amerických	americký	k2eAgMnPc2d1	americký
astronautů	astronaut	k1gMnPc2	astronaut
a	a	k8xC	a
šesti	šest	k4xCc2	šest
sovětských	sovětský	k2eAgMnPc2d1	sovětský
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kosmickým	kosmický	k2eAgInSc7d1	kosmický
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
RÜKL	RÜKL	kA	RÜKL
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85277	[number]	k4	85277
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
E.	E.	kA	E.
Westfall	Westfall	k1gMnSc1	Westfall
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
of	of	k?	of
the	the	k?	the
Lunar	Lunar	k1gMnSc1	Lunar
Terminator	Terminator	k1gMnSc1	Terminator
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-521-59002-7	[number]	k4	0-521-59002-7
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Detailní	detailní	k2eAgFnSc1d1	detailní
fotografie	fotografie	k1gFnSc1	fotografie
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
Seznam	seznam	k1gInSc1	seznam
návštěvníků	návštěvník	k1gMnPc2	návštěvník
Měsíce	měsíc	k1gInSc2	měsíc
Měsíční	měsíční	k2eAgNnSc4d1	měsíční
moře	moře	k1gNnSc4	moře
Seznam	seznam	k1gInSc4	seznam
moří	mořit	k5eAaImIp3nP	mořit
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Seznam	seznam	k1gInSc4	seznam
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Seznam	seznam	k1gInSc1	seznam
údolí	údolí	k1gNnSc2	údolí
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Kolonizace	kolonizace	k1gFnSc2	kolonizace
Měsíce	měsíc	k1gInSc2	měsíc
Lunar	Lunar	k1gInSc1	Lunar
Embassy	Embassa	k1gFnSc2	Embassa
Seléné	Seléná	k1gFnSc2	Seléná
<g/>
,	,	kIx,	,
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
Měsíce	měsíc	k1gInSc2	měsíc
Měsíční	měsíční	k2eAgFnSc2d1	měsíční
<g />
.	.	kIx.	.
</s>
<s>
přechodné	přechodný	k2eAgInPc4d1	přechodný
jevy	jev	k1gInPc4	jev
Selenografie	Selenografie	k1gFnSc2	Selenografie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Měsíc	měsíc	k1gInSc1	měsíc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Měsíc	měsíc	k1gInSc1	měsíc
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
Měsíc	měsíc	k1gInSc1	měsíc
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
NASA	NASA	kA	NASA
-	-	kIx~	-
jádro	jádro	k1gNnSc1	jádro
Měsíce	měsíc	k1gInSc2	měsíc
US	US	kA	US
Naval	navalit	k5eAaPmRp2nS	navalit
Observatory	Observator	k1gInPc1	Observator
<g/>
:	:	kIx,	:
fáze	fáze	k1gFnSc1	fáze
Měsíce	měsíc	k1gInSc2	měsíc
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
datum	datum	k1gInSc4	datum
a	a	k8xC	a
čas	čas	k1gInSc4	čas
1800	[number]	k4	1800
<g/>
-	-	kIx~	-
<g/>
2199	[number]	k4	2199
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
měsíční	měsíční	k2eAgFnPc1d1	měsíční
fáze	fáze	k1gFnPc1	fáze
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Podrobné	podrobný	k2eAgFnPc4d1	podrobná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
měsíčních	měsíční	k2eAgFnPc6d1	měsíční
fázích	fáze	k1gFnPc6	fáze
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
-	-	kIx~	-
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
<s>
Východy	východ	k1gInPc1	východ
a	a	k8xC	a
západy	západ	k1gInPc1	západ
Měsíce	měsíc	k1gInSc2	měsíc
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
datum	datum	k1gNnSc4	datum
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
2027	[number]	k4	2027
<g/>
)	)	kIx)	)
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Digitální	digitální	k2eAgInSc4d1	digitální
fotografický	fotografický	k2eAgInSc4d1	fotografický
atlas	atlas	k1gInSc4	atlas
Měsíce	měsíc	k1gInSc2	měsíc
z	z	k7c2	z
programu	program	k1gInSc2	program
Lunar	Lunar	k1gMnSc1	Lunar
Orbiter	Orbiter	k1gMnSc1	Orbiter
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Archív	archív	k1gInSc4	archív
projektu	projekt	k1gInSc2	projekt
Apollo	Apollo	k1gMnSc1	Apollo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Prohlížeč	prohlížeč	k1gInSc1	prohlížeč
obrázků	obrázek	k1gInPc2	obrázek
Měsíce	měsíc	k1gInSc2	měsíc
ze	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
sondy	sonda	k1gFnPc4	sonda
Clementine	Clementin	k1gInSc5	Clementin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://mesic.astronomie.cz	[url]	k1gInSc1	http://mesic.astronomie.cz
-	-	kIx~	-
Prohlídka	prohlídka	k1gFnSc1	prohlídka
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Moon	Moona	k1gFnPc2	Moona
-	-	kIx~	-
od	od	k7c2	od
Rosanny	Rosanna	k1gFnSc2	Rosanna
a	a	k8xC	a
Calvina	Calvina	k1gFnSc1	Calvina
Hamiltonových	Hamiltonový	k2eAgMnPc2d1	Hamiltonový
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Moon	Moona	k1gFnPc2	Moona
-	-	kIx~	-
od	od	k7c2	od
Billa	Bill	k1gMnSc2	Bill
Arnetta	Arnett	k1gMnSc2	Arnett
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Nestálý	stálý	k2eNgInSc1d1	nestálý
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
"	"	kIx"	"
-	-	kIx~	-
od	od	k7c2	od
Kevina	Kevin	k2eAgInSc2d1	Kevin
Clarka	Clark	k1gInSc2	Clark
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Moon	Moona	k1gFnPc2	Moona
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
neziskový	ziskový	k2eNgInSc1d1	neziskový
výukový	výukový	k2eAgInSc1d1	výukový
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Geologická	geologický	k2eAgFnSc1d1	geologická
historie	historie	k1gFnSc1	historie
Měsíce	měsíc	k1gInSc2	měsíc
od	od	k7c2	od
Dona	Don	k1gMnSc2	Don
Wilhelmse	Wilhelms	k1gMnSc2	Wilhelms
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
-	-	kIx~	-
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
<s>
"	"	kIx"	"
<g/>
Nestávají	stávat	k5eNaImIp3nP	stávat
se	se	k3xPyFc4	se
podivné	podivný	k2eAgFnSc3d1	podivná
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
od	od	k7c2	od
Cecila	Cecil	k1gMnSc2	Cecil
Adamse	Adams	k1gMnSc2	Adams
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Straight	Straight	k2eAgInSc1d1	Straight
Dope	Dope	k1gInSc1	Dope
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Jednou	jednou	k9	jednou
za	za	k7c4	za
modrý	modrý	k2eAgInSc4d1	modrý
(	(	kIx(	(
<g/>
uherský	uherský	k2eAgInSc4d1	uherský
<g/>
)	)	kIx)	)
měsíc	měsíc	k1gInSc4	měsíc
-	-	kIx~	-
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
modrý	modrý	k2eAgInSc1d1	modrý
měsíc	měsíc	k1gInSc1	měsíc
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
od	od	k7c2	od
Ann-Marie	Ann-Marie	k1gFnSc2	Ann-Marie
Imbornoni	Imbornon	k1gMnPc1	Imbornon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Králík	Králík	k1gMnSc1	Králík
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
"	"	kIx"	"
-	-	kIx~	-
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Hardyho	Hardy	k1gMnSc2	Hardy
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Žijeme	žít	k5eAaImIp1nP	žít
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
Prohlídka	prohlídka	k1gFnSc1	prohlídka
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Měsíční	měsíční	k2eAgInSc1d1	měsíční
deník	deník	k1gInSc1	deník
-	-	kIx~	-
blog	blog	k1gInSc1	blog
Pavla	Pavel	k1gMnSc2	Pavel
Gabzdyla	Gabzdyla	k1gMnSc2	Gabzdyla
o	o	k7c6	o
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
příbuzných	příbuzný	k2eAgFnPc6d1	příbuzná
záležitostech	záležitost	k1gFnPc6	záležitost
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
apogeu	apogeum	k1gNnSc6	apogeum
a	a	k8xC	a
perigeu	perigeum	k1gNnSc6	perigeum
(	(	kIx(	(
<g/>
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
fotografické	fotografický	k2eAgNnSc1d1	fotografické
porovnání	porovnání	k1gNnSc1	porovnání
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
Měsíc	měsíc	k1gInSc1	měsíc
větší	veliký	k2eAgInSc1d2	veliký
poblíž	poblíž	k7c2	poblíž
horizontu	horizont	k1gInSc2	horizont
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
The	The	k1gFnSc7	The
Straight	Straight	k2eAgInSc4d1	Straight
Dope	Dope	k1gInSc4	Dope
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Špatná	špatný	k2eAgFnSc1d1	špatná
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Philip	Philip	k1gMnSc1	Philip
Plait	Plait	k1gMnSc1	Plait
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
astronomie	astronomie	k1gFnSc2	astronomie
na	na	k7c6	na
Státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Sonomě	Sonomě	k1gFnSc6	Sonomě
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
webu	web	k1gInSc6	web
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
mnoho	mnoho	k4c4	mnoho
případů	případ	k1gInPc2	případ
nekorektní	korektní	k2eNgFnSc2d1	nekorektní
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
prezentované	prezentovaný	k2eAgFnSc2d1	prezentovaná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
astrologie	astrologie	k1gFnSc2	astrologie
a	a	k8xC	a
označování	označování	k1gNnSc4	označování
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc4	Apollo
za	za	k7c4	za
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Falšované	falšovaný	k2eAgInPc1d1	falšovaný
<g/>
"	"	kIx"	"
snímky	snímek	k1gInPc1	snímek
Měsíce	měsíc	k1gInSc2	měsíc
-	-	kIx~	-
zpráva	zpráva	k1gFnSc1	zpráva
BBC	BBC	kA	BBC
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Virtuální	virtuální	k2eAgInSc1d1	virtuální
atlas	atlas	k1gInSc1	atlas
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
české	český	k2eAgFnSc2d1	Česká
lokalizace	lokalizace	k1gFnSc2	lokalizace
Astrologický	astrologický	k2eAgInSc1d1	astrologický
výpočet	výpočet	k1gInSc1	výpočet
postavení	postavení	k1gNnSc2	postavení
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
