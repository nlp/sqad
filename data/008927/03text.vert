<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Alois	Alois	k1gMnSc1	Alois
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Thal	Thal	k1gInSc1	Thal
poblíž	poblíž	k7c2	poblíž
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rakousko-americký	rakouskomerický	k2eAgMnSc1d1	rakousko-americký
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
republikánský	republikánský	k2eAgMnSc1d1	republikánský
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
zastával	zastávat	k5eAaImAgInS	zastávat
úřad	úřad	k1gInSc1	úřad
guvernéra	guvernér	k1gMnSc4	guvernér
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
kulturistů	kulturista	k1gMnPc2	kulturista
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
nespočet	nespočet	k1gInSc1	nespočet
soutěží	soutěž	k1gFnPc2	soutěž
v	v	k7c6	v
kulturistice	kulturistika	k1gFnSc6	kulturistika
a	a	k8xC	a
silovém	silový	k2eAgInSc6d1	silový
trojboji	trojboj	k1gInSc6	trojboj
–	–	k?	–
nejcennější	cenný	k2eAgInPc1d3	nejcennější
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
vítězství	vítězství	k1gNnSc4	vítězství
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
hraje	hrát	k5eAaImIp3nS	hrát
zejména	zejména	k9	zejména
postavy	postava	k1gFnPc1	postava
tvrdých	tvrdý	k2eAgMnPc2d1	tvrdý
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
akčních	akční	k2eAgInPc6d1	akční
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
zúročuje	zúročovat	k5eAaImIp3nS	zúročovat
svoji	svůj	k3xOyFgFnSc4	svůj
vypracovanou	vypracovaný	k2eAgFnSc4d1	vypracovaná
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
označen	označen	k2eAgMnSc1d1	označen
jako	jako	k8xS	jako
nejdokonaleji	dokonale	k6eAd3	dokonale
vyvinutý	vyvinutý	k2eAgMnSc1d1	vyvinutý
muž	muž	k1gMnSc1	muž
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
ve	v	k7c6	v
filmovém	filmový	k2eAgInSc6d1	filmový
průmyslu	průmysl	k1gInSc6	průmysl
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgMnS	mít
herecké	herecký	k2eAgNnSc4d1	herecké
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
nejznámější	známý	k2eAgFnPc1d3	nejznámější
filmové	filmový	k2eAgFnPc1d1	filmová
role	role	k1gFnPc1	role
jsou	být	k5eAaImIp3nP	být
Terminátor	terminátor	k1gMnSc1	terminátor
a	a	k8xC	a
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
knihách	kniha	k1gFnPc6	kniha
"	"	kIx"	"
<g/>
Arnoldovo	Arnoldův	k2eAgNnSc4d1	Arnoldovo
formování	formování	k1gNnSc4	formování
postavy	postava	k1gFnSc2	postava
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Arnoldova	Arnoldův	k2eAgFnSc1d1	Arnoldova
kulturistika	kulturistika	k1gFnSc1	kulturistika
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vydal	vydat	k5eAaPmAgInS	vydat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
nespočet	nespočet	k1gInSc1	nespočet
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
moderní	moderní	k2eAgFnSc2d1	moderní
kulturistiky	kulturistika	k1gFnSc2	kulturistika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
po	po	k7c6	po
mimořádných	mimořádný	k2eAgFnPc6d1	mimořádná
volbách	volba	k1gFnPc6	volba
38	[number]	k4	38
<g/>
.	.	kIx.	.
guvernérem	guvernér	k1gMnSc7	guvernér
státu	stát	k1gInSc2	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
Maria	Maria	k1gFnSc1	Maria
Shriverová	Shriverová	k1gFnSc1	Shriverová
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kennedyovské	Kennedyovský	k2eAgFnSc2d1	Kennedyovský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
je	být	k5eAaImIp3nS	být
Eunice	Eunice	k1gFnSc1	Eunice
Kennedy	Kenneda	k1gMnSc2	Kenneda
Shriver	Shriver	k1gInSc4	Shriver
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Johna	John	k1gMnSc2	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
Roberta	Robert	k1gMnSc2	Robert
Francise	Francise	k1gFnSc2	Francise
a	a	k8xC	a
senátora	senátor	k1gMnSc2	senátor
Edwarda	Edward	k1gMnSc2	Edward
Moorea	Mooreus	k1gMnSc2	Mooreus
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
manželé	manžel	k1gMnPc1	manžel
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
a	a	k8xC	a
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Shriverová	Shriverová	k1gFnSc1	Shriverová
opustí	opustit	k5eAaPmIp3nS	opustit
rodinné	rodinný	k2eAgNnSc4d1	rodinné
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Brentwoodu	Brentwood	k1gInSc6	Brentwood
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
XYZ	XYZ	kA	XYZ
kniha	kniha	k1gFnSc1	kniha
pamětí	paměť	k1gFnPc2	paměť
Arnolda	Arnold	k1gMnSc2	Arnold
Schwarzenneggera	Schwarzenneggero	k1gNnSc2	Schwarzenneggero
Total	totat	k5eAaImAgMnS	totat
Recall	Recall	k1gInSc4	Recall
-	-	kIx~	-
můj	můj	k3xOp1gInSc1	můj
neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
životní	životní	k2eAgInSc1d1	životní
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Herkules	Herkules	k1gMnSc1	Herkules
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
Hercules	Hercules	k1gInSc1	Hercules
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
–	–	k?	–
Dlouhé	Dlouhé	k2eAgNnSc2d1	Dlouhé
loučení	loučení	k1gNnSc2	loučení
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Long	Long	k1gMnSc1	Long
Goodbye	Goodbye	k1gInSc1	Goodbye
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
–	–	k?	–
Zůstaň	zůstat	k5eAaPmRp2nS	zůstat
hladový	hladový	k2eAgMnSc1d1	hladový
(	(	kIx(	(
<g/>
Stay	Sta	k2eAgFnPc1d1	Sta
Hungry	Hungra	k1gFnPc1	Hungra
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
Pumping	Pumping	k1gInSc1	Pumping
Iron	iron	k1gInSc1	iron
(	(	kIx(	(
<g/>
Pumping	Pumping	k1gInSc1	Pumping
Iron	iron	k1gInSc1	iron
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
–	–	k?	–
Kaktusový	kaktusový	k2eAgMnSc1d1	kaktusový
Jack	Jack	k1gMnSc1	Jack
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Villain	Villain	k1gMnSc1	Villain
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
–	–	k?	–
Hledání	hledání	k1gNnSc2	hledání
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
Scavenger	Scavenger	k1gInSc1	Scavenger
Hunt	hunt	k1gInSc1	hunt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
Příběh	příběh	k1gInSc1	příběh
Jayne	Jayn	k1gInSc5	Jayn
Mansfieldové	Mansfielda	k1gMnPc1	Mansfielda
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Jayne	Jayn	k1gMnSc5	Jayn
Mansfield	Mansfield	k1gInSc4	Mansfield
Story	story	k1gFnPc4	story
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
Návrat	návrat	k1gInSc1	návrat
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Comeback	Comeback	k1gMnSc1	Comeback
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Barbarian	Barbarian	k1gMnSc1	Barbarian
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
Karneval	karneval	k1gInSc1	karneval
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
(	(	kIx(	(
<g/>
Carneval	Carneval	k1gFnSc1	Carneval
in	in	k?	in
Rio	Rio	k1gFnSc1	Rio
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Ničitel	ničitel	k1gMnSc1	ničitel
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
Conan	Conan	k1gMnSc1	Conan
the	the	k?	the
Destroyer	Destroyer	k1gMnSc1	Destroyer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Terminátor	terminátor	k1gMnSc1	terminátor
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Terminator	Terminator	k1gMnSc1	Terminator
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Komando	komando	k1gNnSc1	komando
(	(	kIx(	(
<g/>
Commando	Commanda	k1gFnSc5	Commanda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Rudá	rudý	k2eAgFnSc1d1	rudá
Sonja	Sonja	k1gFnSc1	Sonja
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
Sonja	Sonja	k1gMnSc1	Sonja
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Špinavá	špinavý	k2eAgFnSc1d1	špinavá
dohoda	dohoda	k1gFnSc1	dohoda
(	(	kIx(	(
<g/>
Raw	Raw	k1gMnSc1	Raw
Deal	Deal	k1gMnSc1	Deal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Běžící	běžící	k2eAgMnSc1d1	běžící
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Running	Running	k1gInSc1	Running
Man	Man	k1gMnSc1	Man
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Predátor	predátor	k1gMnSc1	predátor
(	(	kIx(	(
<g/>
Predator	Predator	k1gMnSc1	Predator
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Dvojčata	dvojče	k1gNnPc1	dvojče
(	(	kIx(	(
<g/>
Twins	Twins	k1gInSc1	Twins
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Rudé	rudý	k2eAgNnSc1d1	Rudé
horko	horko	k1gNnSc1	horko
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
Heat	Heat	k1gMnSc1	Heat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Policajt	Policajt	k?	Policajt
ze	z	k7c2	z
školky	školka	k1gFnSc2	školka
(	(	kIx(	(
<g/>
Kindergarten	Kindergarten	k2eAgInSc1d1	Kindergarten
Cop	cop	k1gInSc1	cop
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Total	totat	k5eAaImAgMnS	totat
Recall	Recall	k1gMnSc1	Recall
(	(	kIx(	(
<g/>
Total	totat	k5eAaImAgMnS	totat
Recall	Recall	k1gMnSc1	Recall
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Terminátor	terminátor	k1gInSc1	terminátor
2	[number]	k4	2
<g/>
:	:	kIx,	:
Den	den	k1gInSc1	den
zúčtování	zúčtování	k1gNnSc1	zúčtování
(	(	kIx(	(
<g/>
Terminator	Terminator	k1gInSc1	Terminator
2	[number]	k4	2
<g/>
:	:	kIx,	:
Judgement	Judgement	k1gMnSc1	Judgement
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Vánoce	Vánoce	k1gFnPc1	Vánoce
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
(	(	kIx(	(
<g/>
Christmas	Christmas	k1gMnSc1	Christmas
in	in	k?	in
Connecticut	Connecticut	k1gMnSc1	Connecticut
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Dave	Dav	k1gInSc5	Dav
(	(	kIx(	(
<g/>
Dave	Dav	k1gInSc5	Dav
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Poslední	poslední	k2eAgMnSc1d1	poslední
akční	akční	k2eAgMnSc1d1	akční
hrdina	hrdina	k1gMnSc1	hrdina
(	(	kIx(	(
<g/>
Last	Last	k2eAgInSc1d1	Last
Action	Action	k1gInSc1	Action
Hero	Hero	k6eAd1	Hero
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Berettův	Berettův	k2eAgInSc4d1	Berettův
ostrov	ostrov	k1gInSc4	ostrov
(	(	kIx(	(
<g/>
Berreta	Berret	k2eAgFnSc1d1	Berret
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Junior	junior	k1gMnSc1	junior
(	(	kIx(	(
<g/>
Junior	junior	k1gMnSc1	junior
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Pravdivé	pravdivý	k2eAgFnSc2d1	pravdivá
lži	lež	k1gFnSc2	lež
(	(	kIx(	(
<g/>
True	True	k1gInSc1	True
Lies	Lies	k1gInSc1	Lies
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
:	:	kIx,	:
Battle	Battle	k1gFnSc1	Battle
Across	Across	k1gInSc1	Across
Time	Time	k1gFnSc1	Time
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
:	:	kIx,	:
Battle	Battle	k1gFnSc1	Battle
Across	Across	k1gInSc1	Across
Time	Time	k1gFnSc1	Time
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Rolničky	rolnička	k1gFnSc2	rolnička
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
podíváš	podívat	k5eAaPmIp2nS	podívat
(	(	kIx(	(
<g/>
Jingle	Jingle	k1gInSc1	Jingle
All	All	k1gMnSc2	All
the	the	k?	the
Way	Way	k1gMnSc2	Way
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Likvidátor	likvidátor	k1gMnSc1	likvidátor
(	(	kIx(	(
<g/>
Eraser	Eraser	k1gMnSc1	Eraser
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Batman	Batman	k1gMnSc1	Batman
a	a	k8xC	a
Robin	robin	k2eAgMnSc1d1	robin
(	(	kIx(	(
<g/>
Batman	Batman	k1gMnSc1	Batman
&	&	k?	&
Robin	Robina	k1gFnPc2	Robina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
End	End	k1gFnSc1	End
Of	Of	k1gMnSc1	Of
Days	Days	k1gInSc1	Days
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Hollywodský	Hollywodský	k2eAgMnSc1d1	Hollywodský
hrdina	hrdina	k1gMnSc1	hrdina
(	(	kIx(	(
<g/>
Hollywood	Hollywood	k1gInSc1	Hollywood
Hero	Hero	k1gNnSc1	Hero
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
6	[number]	k4	6
<g/>
th	th	k?	th
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Protiúder	protiúder	k1gInSc1	protiúder
(	(	kIx(	(
<g/>
Collateral	Collateral	k1gFnSc1	Collateral
Damage	Damage	k1gFnSc1	Damage
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Vítejte	vítat	k5eAaImRp2nP	vítat
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Rundown	Rundown	k1gMnSc1	Rundown
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Terminátor	terminátor	k1gInSc1	terminátor
3	[number]	k4	3
<g/>
:	:	kIx,	:
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
Terminator	Terminator	k1gInSc1	Terminator
3	[number]	k4	3
<g/>
:	:	kIx,	:
Rise	Ris	k1gInSc2	Ris
of	of	k?	of
the	the	k?	the
Machines	Machines	k1gInSc1	Machines
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
80	[number]	k4	80
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
Around	Around	k1gInSc1	Around
the	the	k?	the
World	World	k1gInSc1	World
in	in	k?	in
80	[number]	k4	80
Days	Days	k1gInSc1	Days
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Expendables	Expendables	k1gInSc1	Expendables
<g/>
:	:	kIx,	:
Postradatelní	postradatelný	k2eAgMnPc1d1	postradatelný
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Expendables	Expendables	k1gMnSc1	Expendables
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Expendables	Expendables	k1gInSc1	Expendables
<g/>
:	:	kIx,	:
Postradatelní	postradatelný	k2eAgMnPc1d1	postradatelný
2	[number]	k4	2
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Expendables	Expendables	k1gInSc1	Expendables
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Plan	plan	k1gInSc1	plan
útěku	útěk	k1gInSc2	útěk
(	(	kIx(	(
<g/>
Escape	Escap	k1gInSc5	Escap
plan	plan	k1gInSc4	plan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Konečná	Konečná	k1gFnSc1	Konečná
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
stand	stand	k?	stand
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
The	The	k1gMnSc1	The
Expendables	Expendables	k1gMnSc1	Expendables
3	[number]	k4	3
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Maggie	Maggie	k1gFnSc2	Maggie
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Sabotage	Sabotage	k1gFnPc2	Sabotage
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Terminátor	terminátor	k1gInSc1	terminátor
Genisys	Genisys	k1gInSc1	Genisys
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Aftermath	Aftermath	k1gMnSc1	Aftermath
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
–	–	k?	–
Terminátor	terminátor	k1gMnSc1	terminátor
6	[number]	k4	6
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturistická	kulturistický	k2eAgFnSc1d1	kulturistická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
postav	postava	k1gFnPc2	postava
kulturistiky	kulturistika	k1gFnSc2	kulturistika
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
7	[number]	k4	7
krát	krát	k6eAd1	krát
Mr	Mr	k1gMnPc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
nejprestižnější	prestižní	k2eAgFnSc7d3	nejprestižnější
soutěží	soutěž	k1gFnSc7	soutěž
v	v	k7c6	v
kulturistice	kulturistika	k1gFnSc6	kulturistika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
komu	kdo	k3yRnSc3	kdo
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
tolikrát	tolikrát	k6eAd1	tolikrát
vyhrát	vyhrát	k5eAaPmF	vyhrát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
co	co	k9	co
přijel	přijet	k5eAaPmAgInS	přijet
do	do	k7c2	do
USA	USA	kA	USA
začal	začít	k5eAaPmAgInS	začít
cvičit	cvičit	k5eAaImF	cvičit
ve	v	k7c4	v
fitness	fitness	k1gInSc4	fitness
centru	centr	k1gInSc2	centr
Gold	Gold	k1gInSc1	Gold
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
gym	gym	k?	gym
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
proslaveno	proslavit	k5eAaPmNgNnS	proslavit
tím	ten	k3xDgNnSc7	ten
že	že	k9	že
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
chodila	chodit	k5eAaImAgFnS	chodit
cvičit	cvičit	k5eAaImF	cvičit
kulturistická	kulturistický	k2eAgFnSc1d1	kulturistická
elita	elita	k1gFnSc1	elita
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kulturistické	kulturistický	k2eAgFnSc2d1	kulturistická
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Předsedal	předsedat	k5eAaImAgMnS	předsedat
mnoha	mnoho	k4c3	mnoho
soutěžím	soutěž	k1gFnPc3	soutěž
a	a	k8xC	a
přehlídkám	přehlídka	k1gFnPc3	přehlídka
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
do	do	k7c2	do
sportovních	sportovní	k2eAgInPc2d1	sportovní
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
magazínů	magazín	k1gInPc2	magazín
(	(	kIx(	(
<g/>
Muscle	Muscle	k1gInSc1	Muscle
&	&	k?	&
Fitness	Fitness	k1gInSc1	Fitness
<g/>
,	,	kIx,	,
Flex	Flex	k1gInSc1	Flex
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
i	i	k9	i
několik	několik	k4yIc4	několik
posiloven	posilovna	k1gFnPc2	posilovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Míry	mír	k1gInPc4	mír
===	===	k?	===
</s>
</p>
<p>
<s>
Váha	váha	k1gFnSc1	váha
v	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
<g/>
:	:	kIx,	:
107	[number]	k4	107
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Váha	váha	k1gFnSc1	váha
mimo	mimo	k7c4	mimo
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
:	:	kIx,	:
120	[number]	k4	120
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
188	[number]	k4	188
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
bicepsu	biceps	k1gInSc2	biceps
<g/>
:	:	kIx,	:
56	[number]	k4	56
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
:	:	kIx,	:
140	[number]	k4	140
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
v	v	k7c6	v
pase	pas	k1gInSc6	pas
<g/>
:	:	kIx,	:
86	[number]	k4	86
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
stehna	stehno	k1gNnSc2	stehno
<g/>
:	:	kIx,	:
72	[number]	k4	72
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
lýtka	lýtko	k1gNnSc2	lýtko
<g/>
:	:	kIx,	:
51	[number]	k4	51
cm	cm	kA	cm
</s>
</p>
<p>
<s>
===	===	k?	===
Silové	silový	k2eAgFnSc2d1	silová
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
Schwarzenegger	Schwarzenegger	k1gInSc1	Schwarzenegger
účastnil	účastnit	k5eAaImAgInS	účastnit
také	také	k9	také
mnoha	mnoho	k4c6	mnoho
silových	silový	k2eAgFnPc6d1	silová
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvě	dva	k4xCgFnPc4	dva
soutěže	soutěž	k1gFnPc4	soutěž
ve	v	k7c6	v
vzpírání	vzpírání	k1gNnSc6	vzpírání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
a	a	k8xC	a
1965	[number]	k4	1965
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
soutěže	soutěž	k1gFnPc1	soutěž
v	v	k7c6	v
trojboji	trojboj	k1gInSc6	trojboj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
a	a	k8xC	a
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
soutěž	soutěž	k1gFnSc4	soutěž
ve	v	k7c6	v
zdvihu	zdvih	k1gInSc6	zdvih
kamene	kámen	k1gInSc2	kámen
vážícího	vážící	k2eAgInSc2d1	vážící
254	[number]	k4	254
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
====	====	k?	====
</s>
</p>
<p>
<s>
Nadhoz	nadhoz	k1gInSc1	nadhoz
<g/>
:	:	kIx,	:
135	[number]	k4	135
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Trh	trh	k1gInSc1	trh
<g/>
:	:	kIx,	:
110	[number]	k4	110
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Dřep	dřep	k1gInSc1	dřep
<g/>
:	:	kIx,	:
247	[number]	k4	247
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Bench	Bench	k1gInSc1	Bench
press	press	k1gInSc1	press
<g/>
:	:	kIx,	:
240	[number]	k4	240
kg	kg	kA	kg
Mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
tah	tah	k1gInSc4	tah
<g/>
:	:	kIx,	:
320	[number]	k4	320
kg	kg	kA	kg
</s>
</p>
<p>
<s>
===	===	k?	===
Umístění	umístění	k1gNnSc1	umístění
na	na	k7c6	na
soutěžích	soutěž	k1gFnPc6	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
Contest	Contest	k1gMnSc1	Contest
in	in	k?	in
Graz	Graz	k1gMnSc1	Graz
<g/>
,	,	kIx,	,
Steirer	Steirer	k1gMnSc1	Steirer
Hof	Hof	k1gFnSc2	Hof
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
Krajské	krajský	k2eAgNnSc1d1	krajské
mistrovství	mistrovství	k1gNnSc1	mistrovství
-	-	kIx~	-
třída	třída	k1gFnSc1	třída
junioři	junior	k1gMnPc1	junior
<g/>
,	,	kIx,	,
Curlene	Curlen	k1gInSc5	Curlen
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
třída	třída	k1gFnSc1	třída
amatéři	amatér	k1gMnPc1	amatér
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Nejlépe	dobře	k6eAd3	dobře
stavěný	stavěný	k2eAgMnSc1d1	stavěný
muž	muž	k1gMnSc1	muž
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
mistrovství	mistrovství	k1gNnSc1	mistrovství
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
trojboji	trojboj	k1gInSc6	trojboj
-	-	kIx~	-
těžká	těžký	k2eAgFnSc1d1	těžká
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
NABBA	NABBA	kA	NABBA
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
-	-	kIx~	-
třída	třída	k1gFnSc1	třída
amatéři	amatér	k1gMnPc1	amatér
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
NABBA	NABBA	kA	NABBA
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
-	-	kIx~	-
třída	třída	k1gFnSc1	třída
amatéři	amatér	k1gMnPc1	amatér
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
NABBA	NABBA	kA	NABBA
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
-	-	kIx~	-
třída	třída	k1gFnSc1	třída
profi	prof	k1gFnSc2	prof
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc1	Miami
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třídě	třída	k1gFnSc6	třída
i	i	k9	i
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
International	Internationat	k5eAaImAgMnS	Internationat
<g/>
,	,	kIx,	,
Tijuana	Tijuana	k1gFnSc1	Tijuana
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
NABBA	NABBA	kA	NABBA
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
-	-	kIx~	-
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třídě	třída	k1gFnSc6	třída
i	i	k9	i
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Essen	Essen	k1gInSc1	Essen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třídě	třída	k1gFnSc6	třída
i	i	k9	i
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
NABBA	NABBA	kA	NABBA
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
-	-	kIx~	-
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třídě	třída	k1gFnSc6	třída
i	i	k9	i
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
AAU	AAU	kA	AAU
Pro	pro	k7c4	pro
Mr	Mr	k1gFnSc4	Mr
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k1gInSc1	World
<g/>
,	,	kIx,	,
Columbus	Columbus	k1gInSc1	Columbus
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
-	-	kIx~	-
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Essen	Essen	k1gInSc1	Essen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Pretorie	Pretorie	k1gFnSc1	Pretorie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
IFBB	IFBB	kA	IFBB
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Sydney	Sydney	k1gNnSc1	Sydney
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzeneggra	k1gFnPc2	Schwarzeneggra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
kancelář	kancelář	k1gFnSc1	kancelář
guvernéra	guvernér	k1gMnSc2	guvernér
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
fan	fana	k1gFnPc2	fana
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
