<p>
<s>
Dubňany	Dubňan	k1gMnPc4	Dubňan
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Dubnian	Dubniany	k1gInPc2	Dubniany
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
8	[number]	k4	8
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Hodonína	Hodonín	k1gInSc2	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Dubňanech	Dubňan	k1gMnPc6	Dubňan
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1222	[number]	k4	1222
je	být	k5eAaImIp3nS	být
doložena	doložen	k2eAgFnSc1d1	doložena
existence	existence	k1gFnSc1	existence
osady	osada	k1gFnSc2	osada
Jarohněvice	Jarohněvice	k1gFnSc2	Jarohněvice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
Dubňany	Dubňan	k1gMnPc4	Dubňan
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obecní	obecní	k2eAgFnSc1d1	obecní
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
starostou	starosta	k1gMnSc7	starosta
František	František	k1gMnSc1	František
Tříska	Tříska	k1gMnSc1	Tříska
(	(	kIx(	(
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
občanská	občanský	k2eAgFnSc1d1	občanská
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ustavujícím	ustavující	k2eAgNnSc6d1	ustavující
zasedání	zasedání	k1gNnSc6	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
opětovně	opětovně	k6eAd1	opětovně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
místostarostkou	místostarostka	k1gFnSc7	místostarostka
Renata	Renata	k1gFnSc1	Renata
Prokopenková	Prokopenkový	k2eAgFnSc1d1	Prokopenková
(	(	kIx(	(
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
občanská	občanský	k2eAgFnSc1d1	občanská
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
funkcí	funkce	k1gFnPc2	funkce
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
i	i	k9	i
po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Znak	znak	k1gInSc1	znak
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
povýšení	povýšení	k1gNnSc6	povýšení
na	na	k7c4	na
město	město	k1gNnSc4	město
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1964	[number]	k4	1964
si	se	k3xPyFc3	se
Dubňany	Dubňan	k1gMnPc4	Dubňan
vzaly	vzít	k5eAaPmAgFnP	vzít
na	na	k7c4	na
znak	znak	k1gInSc4	znak
symbol	symbol	k1gInSc4	symbol
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
pečetí	pečeť	k1gFnPc2	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
znaku	znak	k1gInSc2	znak
<g/>
:	:	kIx,	:
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
štítě	štít	k1gInSc6	štít
kolmo	kolmo	k6eAd1	kolmo
postavený	postavený	k2eAgInSc1d1	postavený
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
list	list	k1gInSc1	list
dubu	dub	k1gInSc2	dub
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
dubová	dubový	k2eAgFnSc1d1	dubová
ratolest	ratolest	k1gFnSc1	ratolest
s	s	k7c7	s
pěti	pět	k4xCc7	pět
žaludy	žalud	k1gInPc7	žalud
v	v	k7c6	v
přirozené	přirozený	k2eAgFnSc6d1	přirozená
(	(	kIx(	(
<g/>
hnědé	hnědý	k2eAgFnSc6d1	hnědá
<g/>
)	)	kIx)	)
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
požádaly	požádat	k5eAaPmAgFnP	požádat
Dubňany	Dubňan	k1gMnPc4	Dubňan
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
praporu	prapor	k1gInSc2	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
heraldik	heraldik	k1gMnSc1	heraldik
Miroslav	Miroslav	k1gMnSc1	Miroslav
J.	J.	kA	J.
V.	V.	kA	V.
Pavlů	Pavlů	k1gMnSc1	Pavlů
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
praporu	prapor	k1gInSc2	prapor
tvoří	tvořit	k5eAaImIp3nS	tvořit
tři	tři	k4xCgInPc4	tři
svislé	svislý	k2eAgInPc4d1	svislý
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc4d1	zelený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
zelený	zelený	k2eAgInSc4d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
bílého	bílý	k2eAgNnSc2d1	bílé
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
větévka	větévka	k1gFnSc1	větévka
s	s	k7c7	s
pěti	pět	k4xCc7	pět
žaludy	žalud	k1gInPc7	žalud
z	z	k7c2	z
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Dubňanech	Dubňan	k1gMnPc6	Dubňan
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
solárních	solární	k2eAgFnPc2d1	solární
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
okolo	okolo	k7c2	okolo
12	[number]	k4	12
000	[number]	k4	000
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
s	s	k7c7	s
instalovaným	instalovaný	k2eAgInSc7d1	instalovaný
výkonem	výkon	k1gInSc7	výkon
2,1	[number]	k4	2,1
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
sedmihektarového	sedmihektarový	k2eAgInSc2d1	sedmihektarový
areálu	areál	k1gInSc2	areál
stálo	stát	k5eAaImAgNnS	stát
230	[number]	k4	230
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
kostela	kostel	k1gInSc2	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1720	[number]	k4	1720
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
fary	fara	k1gFnSc2	fara
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc6d1	stojící
v	v	k7c6	v
parku	park	k1gInSc6	park
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Vendelína	Vendelína	k1gFnSc1	Vendelína
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc6d1	stojící
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
paní	paní	k1gFnSc2	paní
Křížové	Křížové	k2eAgFnSc2d1	Křížové
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Hodonínská	hodonínský	k2eAgFnSc1d1	hodonínská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kříž	kříž	k1gInSc1	kříž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc6d1	stojící
při	při	k7c6	při
silnici	silnice	k1gFnSc6	silnice
poblíž	poblíž	k7c2	poblíž
benzinového	benzinový	k2eAgNnSc2d1	benzinové
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kyjov	Kyjov	k1gInSc4	Kyjov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prastarý	prastarý	k2eAgInSc1d1	prastarý
dub	dub	k1gInSc1	dub
u	u	k7c2	u
Jarohněvského	Jarohněvský	k2eAgInSc2d1	Jarohněvský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
asi	asi	k9	asi
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Breu	Breus	k1gInSc2	Breus
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iluzionista	iluzionista	k1gMnSc1	iluzionista
a	a	k8xC	a
eskapolog	eskapolog	k1gMnSc1	eskapolog
</s>
</p>
<p>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Bobr	bobr	k1gMnSc1	bobr
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
</s>
</p>
<p>
<s>
Helena	Helena	k1gFnSc1	Helena
Malotová-Jošková	Malotová-Jošková	k1gFnSc1	Malotová-Jošková
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basketbalistka	basketbalistka	k1gFnSc1	basketbalistka
<g/>
,	,	kIx,	,
olympionička	olympionička	k1gFnSc1	olympionička
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Podéšť	Podéšť	k1gMnSc1	Podéšť
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnPc1	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc4	jihozápad
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
severojižně	severojižně	k6eAd1	severojižně
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
řetězec	řetězec	k1gInSc1	řetězec
rybníků	rybník	k1gInPc2	rybník
sahající	sahající	k2eAgMnSc1d1	sahající
až	až	k8xS	až
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
Hodonína	Hodonín	k1gInSc2	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
od	od	k7c2	od
bývalé	bývalý	k2eAgFnSc2d1	bývalá
budovy	budova	k1gFnSc2	budova
nádraží	nádraží	k1gNnSc2	nádraží
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kyjov	Kyjov	k1gInSc4	Kyjov
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
cenná	cenný	k2eAgFnSc1d1	cenná
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
základy	základ	k1gInPc1	základ
vyhořelého	vyhořelý	k2eAgInSc2d1	vyhořelý
románského	románský	k2eAgInSc2d1	románský
kostela	kostel	k1gInSc2	kostel
postaveného	postavený	k2eAgInSc2d1	postavený
ve	v	k7c4	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odkryté	odkrytý	k2eAgFnPc1d1	odkrytá
v	v	k7c6	v
září	září	k1gNnSc6	září
1927	[number]	k4	1927
a	a	k8xC	a
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
hřbitova	hřbitov	k1gInSc2	hřbitov
(	(	kIx(	(
<g/>
nalezeny	nalezen	k2eAgInPc1d1	nalezen
zbytky	zbytek	k1gInPc1	zbytek
lidských	lidský	k2eAgFnPc2d1	lidská
koster	kostra	k1gFnPc2	kostra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Dubňany	Dubňan	k1gMnPc4	Dubňan
</s>
</p>
<p>
<s>
FK	FK	kA	FK
Baník	baník	k1gMnSc1	baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dubňany	Dubňan	k1gMnPc4	Dubňan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Dubňany	Dubňan	k1gMnPc4	Dubňan
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
