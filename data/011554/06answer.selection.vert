<s>
Asi	asi	k9	asi
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
gastronomickým	gastronomický	k2eAgInSc7d1	gastronomický
produktem	produkt	k1gInSc7	produkt
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
je	být	k5eAaImIp3nS	být
portské	portský	k2eAgNnSc4d1	portské
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
dolihované	dolihovaný	k2eAgNnSc4d1	dolihované
víno	víno	k1gNnSc4	víno
z	z	k7c2	z
vinic	vinice	k1gFnPc2	vinice
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Douro	Douro	k1gNnSc1	Douro
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
