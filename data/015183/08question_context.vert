<s>
Vila	vila	k1gFnSc1
Josefa	Josef	k1gMnSc2
Filipa	Filip	k1gMnSc2
je	být	k5eAaImIp3nS
rodinný	rodinný	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
5	#num#	k4
<g/>
-Hlubočepích	-Hlubočepí	k1gNnPc6
ve	v	k7c6
vilové	vilový	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Barrandov	Barrandov	k1gInSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Barrandovská	barrandovský	k2eAgFnSc1d1
v	v	k7c6
její	její	k3xOp3gFnSc6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>