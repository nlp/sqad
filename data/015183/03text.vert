<s>
Vila	vila	k1gFnSc1
Josefa	Josef	k1gMnSc2
Filipa	Filip	k1gMnSc2
</s>
<s>
Vila	vila	k1gFnSc1
Josefa	Josef	k1gMnSc4
Filipa	Filip	k1gMnSc4
vila	vít	k5eAaImAgFnS
Barrandovská	barrandovský	k2eAgFnSc1d1
13	#num#	k4
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
funkcionalismus	funkcionalismus	k1gInSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Alois	Alois	k1gMnSc1
Houba	houba	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1930	#num#	k4
Stavebník	stavebník	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Filip	Filip	k1gMnSc1
<g/>
,	,	kIx,
Ludmila	Ludmila	k1gFnSc1
Filipová	Filipová	k1gFnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Barrandovská	barrandovský	k2eAgFnSc1d1
160	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
5	#num#	k4
-	-	kIx~
Hlubočepy	Hlubočepa	k1gFnPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Barrandovská	barrandovský	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
7,5	7,5	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
3,7	3,7	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vila	vila	k1gFnSc1
Josefa	Josef	k1gMnSc2
Filipa	Filip	k1gMnSc2
je	být	k5eAaImIp3nS
rodinný	rodinný	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
5	#num#	k4
<g/>
-Hlubočepích	-Hlubočepí	k1gNnPc6
ve	v	k7c6
vilové	vilový	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Barrandov	Barrandov	k1gInSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Barrandovská	barrandovský	k2eAgFnSc1d1
v	v	k7c6
její	její	k3xOp3gFnSc6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1929	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
si	se	k3xPyFc3
manželé	manžel	k1gMnPc1
Josef	Josef	k1gMnSc1
a	a	k8xC
Ludmila	Ludmila	k1gFnSc1
Filipovi	Filipův	k2eAgMnPc1d1
na	na	k7c6
Barrandově	Barrandov	k1gInSc6
postavili	postavit	k5eAaPmAgMnP
vilu	vila	k1gFnSc4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc4
Aloise	Alois	k1gMnSc4
Houby	houby	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
1937	#num#	k4
zde	zde	k6eAd1
měl	mít	k5eAaImAgMnS
adresu	adresa	k1gFnSc4
bratr	bratr	k1gMnSc1
Josefa	Josef	k1gMnSc2
Oldřich	Oldřich	k1gMnSc1
Filip	Filip	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Filip	Filip	k1gMnSc1
se	se	k3xPyFc4
s	s	k7c7
Václavem	Václav	k1gMnSc7
M.	M.	kA
Havlem	Havel	k1gMnSc7
přátelil	přátelit	k5eAaImAgMnS
již	již	k6eAd1
od	od	k7c2
dob	doba	k1gFnPc2
studií	studio	k1gNnPc2
na	na	k7c6
obchodní	obchodní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
bance	banka	k1gFnSc6
stal	stát	k5eAaPmAgInS
ředitelem	ředitel	k1gMnSc7
devizového	devizový	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
činnost	činnost	k1gFnSc4
v	v	k7c6
zednářské	zednářský	k2eAgFnSc6d1
lóži	lóže	k1gFnSc6
jej	on	k3xPp3gInSc2
roku	rok	k1gInSc2
1944	#num#	k4
zatklo	zatknout	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
aktivní	aktivní	k2eAgMnSc1d1
v	v	k7c6
odboji	odboj	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zůstalo	zůstat	k5eAaPmAgNnS
utajeno	utajit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
návštěvě	návštěva	k1gFnSc6
v	v	k7c6
Petschkově	Petschkův	k2eAgInSc6d1
paláci	palác	k1gInSc6
byla	být	k5eAaImAgFnS
zadržena	zadržen	k2eAgFnSc1d1
také	také	k9
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
dvě	dva	k4xCgFnPc1
dcery	dcera	k1gFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
války	válka	k1gFnSc2
ve	v	k7c6
vile	vila	k1gFnSc6
samy	sám	k3xTgInPc1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
babičkou	babička	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Filip	Filip	k1gMnSc1
z	z	k7c2
Národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
do	do	k7c2
invalidního	invalidní	k2eAgInSc2d1
důchodu	důchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracoval	spolupracovat	k5eAaImAgInS
se	s	k7c7
švýcarskou	švýcarský	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
Longines	Longinesa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyráběla	vyrábět	k5eAaImAgFnS
hodinky	hodinka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nuceném	nucený	k2eAgNnSc6d1
vystěhování	vystěhování	k1gNnSc6
z	z	k7c2
Barrandova	Barrandov	k1gInSc2
v	v	k7c6
září	září	k1gNnSc6
1952	#num#	k4
žila	žít	k5eAaImAgFnS
rodina	rodina	k1gFnSc1
nejdřív	dříve	k6eAd3
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
v	v	k7c6
Černošicích	Černošice	k1gFnPc6
v	v	k7c6
domku	domek	k1gInSc6
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
hausbótu	hausbót	k1gInSc6
postaveném	postavený	k2eAgInSc6d1
otcem	otec	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Filipová	Filipová	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Syllabová	Syllabová	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
MUDr.	MUDr.	kA
Ladislava	Ladislav	k1gMnSc2
Syllaby	Syllaba	k1gMnSc2
<g/>
,	,	kIx,
osobního	osobní	k2eAgMnSc2d1
lékaře	lékař	k1gMnSc2
a	a	k8xC
přítele	přítel	k1gMnSc2
prezidenta	prezident	k1gMnSc2
Masaryka	Masaryk	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
sestrou	sestra	k1gFnSc7
lékaře	lékař	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
Syllaby	Syllaba	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1952	#num#	k4
byla	být	k5eAaImAgFnS
rodina	rodina	k1gFnSc1
Filipových	Filipových	k2eAgFnSc1d1
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
domu	dům	k1gInSc2
vystěhována	vystěhován	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
do	do	k7c2
vily	vila	k1gFnSc2
nastěhoval	nastěhovat	k5eAaPmAgMnS
Bedřich	Bedřich	k1gMnSc1
Kozelka	Kozelek	k1gMnSc2
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
ve	v	k7c6
vile	vila	k1gFnSc6
sídlila	sídlit	k5eAaImAgFnS
Správa	správa	k1gFnSc1
Svazu	svaz	k1gInSc2
uranových	uranový	k2eAgInPc2d1
dolů	dol	k1gInPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
ji	on	k3xPp3gFnSc4
získala	získat	k5eAaPmAgFnS
Správa	správa	k1gFnSc1
služeb	služba	k1gFnPc2
diplomatického	diplomatický	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
byla	být	k5eAaImAgFnS
vila	vila	k1gFnSc1
vrácena	vrátit	k5eAaPmNgFnS
původním	původní	k2eAgMnPc3d1
majitelům	majitel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2000	#num#	k4
prošla	projít	k5eAaPmAgFnS
kompletní	kompletní	k2eAgFnSc7d1
rekonstrukcí	rekonstrukce	k1gFnSc7
včetně	včetně	k7c2
zateplení	zateplení	k1gNnSc2
a	a	k8xC
výměny	výměna	k1gFnSc2
okenních	okenní	k2eAgFnPc2d1
a	a	k8xC
dveřních	dveřní	k2eAgFnPc2d1
výplní	výplň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
prvorepubliková	prvorepublikový	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
zásahem	zásah	k1gInSc7
změnila	změnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Dům	dům	k1gInSc1
na	na	k7c6
půdorysu	půdorys	k1gInSc6
písmene	písmeno	k1gNnSc2
„	„	k?
<g/>
L	L	kA
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
plochou	plochý	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
místech	místo	k1gNnPc6
setkání	setkání	k1gNnSc2
obou	dva	k4xCgNnPc2
ramen	rameno	k1gNnPc2
je	být	k5eAaImIp3nS
oblé	oblý	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
schodišťový	schodišťový	k2eAgInSc1d1
rizalit	rizalit	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
prosvětlen	prosvětlit	k5eAaPmNgInS
oblou	oblý	k2eAgFnSc7d1
prosklenou	prosklený	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
uličního	uliční	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
vystupuje	vystupovat	k5eAaImIp3nS
přístavba	přístavba	k1gFnSc1
garáže	garáž	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
navazuje	navazovat	k5eAaImIp3nS
vstup	vstup	k1gInSc1
do	do	k7c2
domu	dům	k1gInSc2
krytý	krytý	k2eAgInSc1d1
portikem	portikus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
ulici	ulice	k1gFnSc6
je	být	k5eAaImIp3nS
vila	vila	k1gFnSc1
dvoupodlažní	dvoupodlažní	k2eAgFnSc1d1
s	s	k7c7
minimem	minimum	k1gNnSc7
oken	okno	k1gNnPc2
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
do	do	k7c2
zahrady	zahrada	k1gFnSc2
třípodlažní	třípodlažní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zahradní	zahradní	k2eAgFnSc6d1
části	část	k1gFnSc6
jsou	být	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnPc4d1
obytné	obytný	k2eAgFnPc4d1
místnosti	místnost	k1gFnPc4
a	a	k8xC
je	být	k5eAaImIp3nS
sem	sem	k6eAd1
orientováno	orientován	k2eAgNnSc1d1
východní	východní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průčelí	průčelí	k1gNnSc1
původně	původně	k6eAd1
symetricky	symetricky	k6eAd1
členila	členit	k5eAaImAgFnS
sestava	sestava	k1gFnSc1
špaletových	špaletový	k2eAgNnPc2d1
oken	okno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravoúhlý	pravoúhlý	k2eAgInSc4d1
arkýř	arkýř	k1gInSc4
v	v	k7c6
prvním	první	k4xOgNnSc6
patře	patro	k1gNnSc6
jižní	jižní	k2eAgFnSc2d1
části	část	k1gFnSc2
průčelí	průčelí	k1gNnPc2
byl	být	k5eAaImAgInS
prosklený	prosklený	k2eAgInSc1d1
pásovým	pásový	k2eAgNnSc7d1
oknem	okno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
střechu	střecha	k1gFnSc4
tvořila	tvořit	k5eAaImAgFnS
pochozí	pochozí	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Svojsíkův	Svojsíkův	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
<g/>
:	:	kIx,
Za	za	k7c7
Evou	Eva	k1gFnSc7
Průšovou	Průšová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekrolog	nekrolog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Obadálek	Obadálek	k1gMnSc1
<g/>
,	,	kIx,
13.9	13.9	k4
<g/>
.2017	.2017	k4
<g/>
.	.	kIx.
pdf	pdf	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DOLEJSKÁ	DOLEJSKÁ	kA
<g/>
,	,	kIx,
Pavlína	Pavlína	k1gFnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
Prahy	Praha	k1gFnSc2
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barrandov	Barrandov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Foibos	Foibos	k1gMnSc1
Books	Books	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
186	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88258	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
77	#num#	k4
-	-	kIx~
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VILINSKIJ	VILINSKIJ	kA
<g/>
,	,	kIx,
Valerij	Valerij	k1gMnSc1
Sergejevič	Sergejevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc1d1
adresář	adresář	k1gInSc1
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
-	-	kIx~
III	III	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
.	.	kIx.
28	#num#	k4
<g/>
,	,	kIx,
1685	#num#	k4
<g/>
,	,	kIx,
83	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
255	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VLČEK	Vlček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
část	část	k1gFnSc1
<g/>
,	,	kIx,
A-	A-	k1gFnSc1
<g/>
l.	l.	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
1077	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2107	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
407	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vilová	vilový	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
na	na	k7c6
Barrandově	Barrandov	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vila	vít	k5eAaImAgFnS
Josefa	Josefa	k1gFnSc1
Filipa	Filip	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Orientační	orientační	k2eAgInSc1d1
plán	plán	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
s	s	k7c7
okolím	okolí	k1gNnSc7
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
list	list	k1gInSc1
č.	č.	k?
65	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Barrandovská	barrandovský	k2eAgFnSc1d1
13	#num#	k4
<g/>
/	/	kIx~
<g/>
160	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barrandov	Barrandov	k1gInSc1
1928	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Nejedlý	Nejedlý	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Zdeněk	Zdeněk	k1gMnSc1
Černovský	Černovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
2000	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vila	vila	k1gFnSc1
Filip	Filip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Prázdné	prázdný	k2eAgInPc1d1
domy	dům	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Databáze	databáze	k1gFnSc1
domů	dům	k1gInPc2
s	s	k7c7
historií	historie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
