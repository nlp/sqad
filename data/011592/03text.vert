<p>
<s>
Házená	házená	k1gFnSc1	házená
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
míčový	míčový	k2eAgInSc4d1	míčový
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dva	dva	k4xCgInPc1	dva
sedmičlenné	sedmičlenný	k2eAgInPc1d1	sedmičlenný
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
mají	mít	k5eAaImIp3nP	mít
prapůvod	prapůvod	k1gInSc4	prapůvod
už	už	k9	už
v	v	k7c6	v
míčových	míčový	k2eAgInPc6d1	míčový
sportech	sport	k1gInPc6	sport
starého	starý	k2eAgNnSc2d1	staré
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
tehdy	tehdy	k6eAd1	tehdy
sloužily	sloužit	k5eAaImAgFnP	sloužit
dospělým	dospělí	k1gMnPc3	dospělí
jako	jako	k8xS	jako
forma	forma	k1gFnSc1	forma
zábavy	zábava	k1gFnSc2	zábava
nebo	nebo	k8xC	nebo
obřadu	obřad	k1gInSc2	obřad
a	a	k8xC	a
dětem	dítě	k1gFnPc3	dítě
jako	jako	k9	jako
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
dospělost	dospělost	k1gFnSc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
provozovali	provozovat	k5eAaImAgMnP	provozovat
míčové	míčový	k2eAgFnPc4d1	Míčová
hry	hra	k1gFnPc4	hra
dokonce	dokonce	k9	dokonce
jako	jako	k8xS	jako
léčebný	léčebný	k2eAgInSc4d1	léčebný
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Házenou	házená	k1gFnSc4	házená
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Handbold	Handbold	k1gMnSc1	Handbold
<g/>
)	)	kIx)	)
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
dánský	dánský	k2eAgMnSc1d1	dánský
učitel	učitel	k1gMnSc1	učitel
Holger	Holger	k1gMnSc1	Holger
Nielsen	Nielsen	k2eAgMnSc1d1	Nielsen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
pravidla	pravidlo	k1gNnPc1	pravidlo
vyšla	vyjít	k5eAaPmAgNnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
objevila	objevit	k5eAaPmAgFnS	objevit
varianta	varianta	k1gFnSc1	varianta
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dva	dva	k4xCgInPc1	dva
jedenáctičlenné	jedenáctičlenný	k2eAgInPc1d1	jedenáctičlenný
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
velká	velký	k2eAgFnSc1d1	velká
házená	házená	k1gFnSc1	házená
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
na	na	k7c6	na
programu	program	k1gInSc6	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
házené	házená	k1gFnSc2	házená
(	(	kIx(	(
<g/>
IHF	IHF	kA	IHF
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
házená	házená	k1gFnSc1	házená
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
program	program	k1gInSc4	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
československé	československý	k2eAgInPc4d1	československý
úspěchy	úspěch	k1gInPc4	úspěch
patří	patřit	k5eAaImIp3nP	patřit
titul	titul	k1gInSc4	titul
mistryň	mistryně	k1gFnPc2	mistryně
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
ČSSR	ČSSR	kA	ČSSR
z	z	k7c2	z
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
a	a	k8xC	a
titul	titul	k1gInSc1	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
výrazným	výrazný	k2eAgInSc7d1	výrazný
příspěvkem	příspěvek	k1gInSc7	příspěvek
ČSSR	ČSSR	kA	ČSSR
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
házené	házená	k1gFnSc2	házená
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zrod	zrod	k1gInSc1	zrod
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
tzv.	tzv.	kA	tzv.
Národní	národní	k2eAgFnSc2d1	národní
házené	házená	k1gFnSc2	házená
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
česká	český	k2eAgFnSc1d1	Česká
házená	házená	k1gFnSc1	házená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgFnSc2d1	klasická
<g/>
"	"	kIx"	"
házené	házená	k1gFnSc2	házená
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
nezdá	zdát	k5eNaPmIp3nS	zdát
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
několika	několik	k4yIc6	několik
zásadních	zásadní	k2eAgNnPc6d1	zásadní
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
členění	členění	k1gNnSc1	členění
hřiště	hřiště	k1gNnSc2	hřiště
na	na	k7c6	na
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
-	-	kIx~	-
podle	podle	k7c2	podle
postu	post	k1gInSc2	post
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
obránce	obránce	k1gMnSc1	obránce
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
obranné	obranný	k2eAgFnSc6d1	obranná
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
třetině	třetina	k1gFnSc6	třetina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgInPc7d1	jiný
rozdíly	rozdíl	k1gInPc7	rozdíl
jsou	být	k5eAaImIp3nP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
rozměry	rozměr	k1gInPc1	rozměr
hřiště	hřiště	k1gNnSc2	hřiště
a	a	k8xC	a
branek	branka	k1gFnPc2	branka
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
měla	mít	k5eAaImAgFnS	mít
Národní	národní	k2eAgFnSc1d1	národní
házená	házená	k1gFnSc1	házená
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
úspěchu	úspěch	k1gInSc6	úspěch
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgFnSc6d1	klasická
házené	házená	k1gFnSc6	házená
<g/>
"	"	kIx"	"
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c1	mnoho
vyspělých	vyspělý	k2eAgMnPc2d1	vyspělý
hráčů	hráč	k1gMnPc2	hráč
přecházelo	přecházet	k5eAaImAgNnS	přecházet
právě	právě	k6eAd1	právě
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
házené	házená	k1gFnSc2	házená
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgFnSc2d1	klasická
házené	házená	k1gFnSc2	házená
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
i	i	k9	i
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
omezuje	omezovat	k5eAaImIp3nS	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
a	a	k8xC	a
i	i	k9	i
zde	zde	k6eAd1	zde
pozbývá	pozbývat	k5eAaImIp3nS	pozbývat
svého	svůj	k3xOyFgInSc2	svůj
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravidla	pravidlo	k1gNnPc1	pravidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Házená	házená	k1gFnSc1	házená
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
20	[number]	k4	20
<g/>
×	×	k?	×
<g/>
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Čáry	čára	k1gFnPc1	čára
vymezující	vymezující	k2eAgFnSc4d1	vymezující
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
na	na	k7c6	na
delší	dlouhý	k2eAgFnSc6d2	delší
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
postranní	postranní	k2eAgFnPc1d1	postranní
čáry	čára	k1gFnPc1	čára
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnPc1d2	kratší
strany	strana	k1gFnPc1	strana
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
brankové	brankový	k2eAgFnPc1d1	branková
čáry	čára	k1gFnPc1	čára
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
tyčemi	tyč	k1gFnPc7	tyč
branky	branka	k1gFnSc2	branka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnější	vnější	k2eAgFnPc4d1	vnější
brankové	brankový	k2eAgFnPc4d1	branková
čáry	čára	k1gFnPc4	čára
(	(	kIx(	(
<g/>
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
branky	branka	k1gFnSc2	branka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
celé	celý	k2eAgFnSc2d1	celá
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ochranná	ochranný	k2eAgFnSc1d1	ochranná
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
vedle	vedle	k7c2	vedle
postranních	postranní	k2eAgFnPc2d1	postranní
čar	čára	k1gFnPc2	čára
nejméně	málo	k6eAd3	málo
1	[number]	k4	1
metr	metr	k1gInSc4	metr
a	a	k8xC	a
za	za	k7c7	za
brankovými	brankový	k2eAgFnPc7d1	branková
i	i	k8xC	i
brankovými-autovými	brankovýmiutův	k2eAgFnPc7d1	brankovými-autův
čarami	čára	k1gFnPc7	čára
nejméně	málo	k6eAd3	málo
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
během	během	k7c2	během
utkání	utkání	k1gNnSc2	utkání
měnit	měnit	k5eAaImF	měnit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hřiště	hřiště	k1gNnSc1	hřiště
je	být	k5eAaImIp3nS	být
rozdělené	rozdělený	k2eAgNnSc1d1	rozdělené
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
v	v	k7c4	v
každé	každý	k3xTgNnSc4	každý
je	být	k5eAaImIp3nS	být
vyznačené	vyznačený	k2eAgNnSc4d1	vyznačené
brankoviště	brankoviště	k1gNnSc4	brankoviště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brankovišti	brankoviště	k1gNnSc6	brankoviště
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
pohybovat	pohybovat	k5eAaImF	pohybovat
pouze	pouze	k6eAd1	pouze
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hájí	hájit	k5eAaImIp3nS	hájit
branku	branka	k1gFnSc4	branka
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
družstvo	družstvo	k1gNnSc1	družstvo
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
14	[number]	k4	14
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
účastní	účastnit	k5eAaImIp3nS	účastnit
pouze	pouze	k6eAd1	pouze
7	[number]	k4	7
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
6	[number]	k4	6
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nově	nově	k6eAd1	nově
7	[number]	k4	7
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
dopravit	dopravit	k5eAaPmF	dopravit
míč	míč	k1gInSc4	míč
do	do	k7c2	do
branky	branka	k1gFnSc2	branka
protivníka	protivník	k1gMnSc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
poločasy	poločas	k1gInPc4	poločas
po	po	k7c6	po
30	[number]	k4	30
minutách	minuta	k1gFnPc6	minuta
(	(	kIx(	(
<g/>
u	u	k7c2	u
hráčů	hráč	k1gMnPc2	hráč
12-14	[number]	k4	12-14
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
po	po	k7c6	po
25	[number]	k4	25
minutách	minuta	k1gFnPc6	minuta
a	a	k8xC	a
u	u	k7c2	u
hráčů	hráč	k1gMnPc2	hráč
8-12	[number]	k4	8-12
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
po	po	k7c6	po
20	[number]	k4	20
minutách	minuta	k1gFnPc6	minuta
<g/>
)	)	kIx)	)
oddělených	oddělený	k2eAgFnPc2d1	oddělená
desetiminutovou	desetiminutový	k2eAgFnSc7d1	desetiminutová
přestávkou	přestávka	k1gFnSc7	přestávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brankoviště	brankoviště	k1gNnSc1	brankoviště
má	mít	k5eAaImIp3nS	mít
poloměr	poloměr	k1gInSc4	poloměr
6	[number]	k4	6
m.	m.	k?	m.
Za	za	k7c4	za
zabránění	zabránění	k1gNnSc4	zabránění
v	v	k7c6	v
brankové	brankový	k2eAgFnSc6d1	branková
příležitosti	příležitost	k1gFnSc6	příležitost
(	(	kIx(	(
<g/>
hrubý	hrubý	k2eAgInSc1d1	hrubý
faul	faul	k1gInSc1	faul
<g/>
)	)	kIx)	)
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
brankoviště	brankoviště	k1gNnSc2	brankoviště
se	se	k3xPyFc4	se
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
trestný	trestný	k2eAgInSc1d1	trestný
hod	hod	k1gInSc1	hod
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
7	[number]	k4	7
m	m	kA	m
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sedmička	sedmička	k1gFnSc1	sedmička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
faul	faul	k1gInSc1	faul
a	a	k8xC	a
jiné	jiný	k2eAgNnSc1d1	jiné
porušení	porušení	k1gNnSc1	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
trestáno	trestat	k5eAaImNgNnS	trestat
9	[number]	k4	9
<g/>
m	m	kA	m
hodem	hod	k1gInSc7	hod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
devítka	devítka	k1gFnSc1	devítka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
libovolným	libovolný	k2eAgInSc7d1	libovolný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hrubý	hrubý	k2eAgInSc4d1	hrubý
faul	faul	k1gInSc4	faul
následuje	následovat	k5eAaImIp3nS	následovat
varování	varování	k1gNnSc4	varování
žlutou	žlutý	k2eAgFnSc7d1	žlutá
kartou	karta	k1gFnSc7	karta
<g/>
,	,	kIx,	,
při	při	k7c6	při
opakovaném	opakovaný	k2eAgInSc6d1	opakovaný
hrubém	hrubý	k2eAgInSc6d1	hrubý
faulu	faul	k1gInSc6	faul
vyloučení	vyloučení	k1gNnSc2	vyloučení
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
hráče	hráč	k1gMnPc4	hráč
vyloučit	vyloučit	k5eAaPmF	vyloučit
i	i	k9	i
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgNnSc2d1	předchozí
varování	varování	k1gNnSc2	varování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
vyloučení	vyloučení	k1gNnSc6	vyloučení
je	být	k5eAaImIp3nS	být
hráč	hráč	k1gMnSc1	hráč
současně	současně	k6eAd1	současně
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
zápasu	zápas	k1gInSc2	zápas
-	-	kIx~	-
červená	červený	k2eAgFnSc1d1	červená
karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
velikost	velikost	k1gFnSc4	velikost
hracího	hrací	k2eAgInSc2d1	hrací
míče	míč	k1gInSc2	míč
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kategorie	kategorie	k1gFnSc1	kategorie
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minižáci	Minižák	k1gMnPc1	Minižák
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
0	[number]	k4	0
</s>
</p>
<p>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
žáci	žák	k1gMnPc1	žák
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
1	[number]	k4	1
</s>
</p>
<p>
<s>
Starší	starý	k2eAgFnPc1d2	starší
žačky	žačka	k1gFnPc1	žačka
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
1	[number]	k4	1
</s>
</p>
<p>
<s>
Starší	starý	k2eAgMnPc1d2	starší
žáci	žák	k1gMnPc1	žák
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
2	[number]	k4	2
</s>
</p>
<p>
<s>
Mladší	mladý	k2eAgInSc1d2	mladší
dorost	dorost	k1gInSc1	dorost
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
2	[number]	k4	2
</s>
</p>
<p>
<s>
Starší	starý	k2eAgInSc1d2	starší
dorost	dorost	k1gInSc1	dorost
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgFnPc1d1	dospělá
kategorie	kategorie	k1gFnPc1	kategorie
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
2	[number]	k4	2
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
míče	míč	k1gInSc2	míč
54	[number]	k4	54
až	až	k9	až
56	[number]	k4	56
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
325	[number]	k4	325
až	až	k9	až
400	[number]	k4	400
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Starší	starý	k2eAgInSc1d2	starší
dorost	dorost	k1gInSc1	dorost
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgFnPc1d1	dospělá
kategorie	kategorie	k1gFnPc1	kategorie
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
-	-	kIx~	-
Velikost	velikost	k1gFnSc1	velikost
č.	č.	k?	č.
3	[number]	k4	3
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
míče	míč	k1gInSc2	míč
56	[number]	k4	56
až	až	k9	až
60	[number]	k4	60
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
425	[number]	k4	425
až	až	k9	až
475	[number]	k4	475
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Házená	házená	k1gFnSc1	házená
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
mužská	mužský	k2eAgFnSc1d1	mužská
soutěž	soutěž	k1gFnSc1	soutěž
nazývá	nazývat	k5eAaImIp3nS	nazývat
Strabag	Strabag	k1gMnSc1	Strabag
Rail	Rail	k1gMnSc1	Rail
Extraliga	extraliga	k1gFnSc1	extraliga
házené	házená	k1gFnSc2	házená
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Tipgames	Tipgames	k1gInSc4	Tipgames
extraliga	extraliga	k1gFnSc1	extraliga
a	a	k8xC	a
před	před	k7c7	před
tím	ten	k3xDgInSc7	ten
ZUBR	zubr	k1gMnSc1	zubr
extraliga	extraliga	k1gFnSc1	extraliga
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
hrají	hrát	k5eAaImIp3nP	hrát
společně	společně	k6eAd1	společně
se	s	k7c7	s
slovenskými	slovenský	k2eAgNnPc7d1	slovenské
družstvy	družstvo	k1gNnPc7	družstvo
Interligu	Interlig	k1gInSc2	Interlig
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
WHIL	WHIL	kA	WHIL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
mužském	mužský	k2eAgNnSc6d1	mužské
provedení	provedení	k1gNnSc6	provedení
už	už	k6eAd1	už
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
vítězem	vítěz	k1gMnSc7	vítěz
extraligy	extraliga	k1gFnSc2	extraliga
(	(	kIx(	(
<g/>
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2017-	[number]	k4	2017-
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
družstvo	družstvo	k1gNnSc1	družstvo
HCB	HCB	kA	HCB
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
play-off	playff	k1gInSc1	play-off
zdolalo	zdolat	k5eAaPmAgNnS	zdolat
celek	celek	k1gInSc1	celek
M.	M.	kA	M.
<g/>
A.T.	A.T.	k1gFnSc1	A.T.
Plzeň	Plzeň	k1gFnSc1	Plzeň
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2017-2018	[number]	k4	2017-2018
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
družstvo	družstvo	k1gNnSc1	družstvo
Baníku	Baník	k1gInSc2	Baník
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
play-off	playff	k1gInSc4	play-off
porazilo	porazit	k5eAaPmAgNnS	porazit
Slavii	slavie	k1gFnSc4	slavie
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
historii	historie	k1gFnSc6	historie
české	český	k2eAgFnSc6d1	Česká
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
československé	československý	k2eAgFnSc2d1	Československá
klubové	klubový	k2eAgFnSc2d1	klubová
mužské	mužský	k2eAgFnSc2d1	mužská
házené	házená	k1gFnSc2	házená
je	být	k5eAaImIp3nS	být
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
klubem	klub	k1gInSc7	klub
HC	HC	kA	HC
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
jednatřicetkrát	jednatřicetkrát	k6eAd1	jednatřicetkrát
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
šestkrát	šestkrát	k6eAd1	šestkrát
Český	český	k2eAgInSc4d1	český
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
a	a	k8xC	a
1984	[number]	k4	1984
se	se	k3xPyFc4	se
Dukla	Dukla	k1gFnSc1	Dukla
stala	stát	k5eAaPmAgFnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
Poháru	pohár	k1gInSc6	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
československé	československý	k2eAgFnSc2d1	Československá
reprezentace	reprezentace	k1gFnSc2	reprezentace
bylo	být	k5eAaImAgNnS	být
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
na	na	k7c6	na
MS	MS	kA	MS
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958	[number]	k4	1958
a	a	k8xC	a
1961	[number]	k4	1961
a	a	k8xC	a
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
a	a	k8xC	a
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnSc3d1	Československá
reprezentaci	reprezentace	k1gFnSc3	reprezentace
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
i	i	k8xC	i
autorství	autorství	k1gNnSc1	autorství
tzv.	tzv.	kA	tzv.
trháku	trhák	k1gInSc2	trhák
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
rychlého	rychlý	k2eAgInSc2d1	rychlý
protiútoku	protiútok	k1gInSc2	protiútok
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
klasické	klasický	k2eAgFnSc2d1	klasická
házené	házená	k1gFnSc2	házená
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
bezpochyby	bezpochyby	k6eAd1	bezpochyby
také	také	k9	také
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
Filipa	Filip	k1gMnSc2	Filip
Jíchy	jícha	k1gFnSc2	jícha
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
světovým	světový	k2eAgMnSc7d1	světový
hráčem	hráč	k1gMnSc7	hráč
házené	házená	k1gFnSc2	házená
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
vzorem	vzor	k1gInSc7	vzor
malých	malá	k1gFnPc2	malá
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
i	i	k9	i
velkých	velký	k2eAgMnPc2d1	velký
házenkářů	házenkář	k1gMnPc2	házenkář
<g/>
...	...	k?	...
<g/>
nedávno	nedávno	k6eAd1	nedávno
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
<g/>
který	který	k3yIgInSc1	který
hrál	hrát	k5eAaImAgInS	hrát
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
Bundes	Bundes	k1gMnSc1	Bundes
Ligu	liga	k1gFnSc4	liga
[	[	kIx(	[
<g/>
Kiel	Kiel	k1gInSc4	Kiel
<g/>
]	]	kIx)	]
<g/>
do	do	k7c2	do
Barcelony	Barcelona	k1gFnSc2	Barcelona
kde	kde	k6eAd1	kde
i	i	k8xC	i
přesto	přesto	k8xC	přesto
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
zraněný	zraněný	k1gMnSc1	zraněný
hraje	hrát	k5eAaImIp3nS	hrát
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
týmové	týmový	k2eAgFnSc6d1	týmová
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Letos	letos	k6eAd1	letos
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
ukončil	ukončit	k5eAaPmAgMnS	ukončit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodné	rodný	k2eAgFnSc6d1	rodná
Plzni	Plzeň	k1gFnSc6	Plzeň
na	na	k7c6	na
zimním	zimní	k2eAgInSc6d1	zimní
stadioně	stadion	k1gInSc6	stadion
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Kielu	Kiel	k1gInSc6	Kiel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
i	i	k9	i
značnými	značný	k2eAgInPc7d1	značný
úspěchy	úspěch	k1gInPc7	úspěch
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
především	především	k6eAd1	především
dvojice	dvojice	k1gFnSc1	dvojice
Václav	Václav	k1gMnSc1	Václav
Horáček	Horáček	k1gMnSc1	Horáček
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
IHF	IHF	kA	IHF
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
nominování	nominování	k1gNnSc4	nominování
na	na	k7c4	na
Olympiádu	olympiáda	k1gFnSc4	olympiáda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
utkání	utkání	k1gNnSc4	utkání
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
úspěchům	úspěch	k1gInPc3	úspěch
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
tito	tento	k3xDgMnPc1	tento
sudí	sudí	k1gMnPc1	sudí
pyšnit	pyšnit	k5eAaImF	pyšnit
označením	označení	k1gNnSc7	označení
TOP	topit	k5eAaImRp2nS	topit
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc1	druh
turnajů	turnaj	k1gInPc2	turnaj
(	(	kIx(	(
<g/>
zápasů	zápas	k1gInPc2	zápas
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
házená	házená	k1gFnSc1	házená
a	a	k8xC	a
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
-	-	kIx~	-
velká	velký	k2eAgFnSc1d1	velká
házená	házená	k1gFnSc1	házená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
mužů	muž	k1gMnPc2	muž
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
mužů	muž	k1gMnPc2	muž
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
Házená	házená	k1gFnSc1	házená
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
Plážová	plážový	k2eAgFnSc1d1	plážová
házená	házená	k1gFnSc1	házená
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
házená	házená	k1gFnSc1	házená
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
házená	házená	k1gFnSc1	házená
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
