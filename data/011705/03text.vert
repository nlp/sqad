<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2002	[number]	k4	2002
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
od	od	k7c2	od
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
sobotu	sobota	k1gFnSc4	sobota
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
200	[number]	k4	200
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
se	se	k3xPyFc4	se
ucházelo	ucházet	k5eAaImAgNnS	ucházet
29	[number]	k4	29
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ČSSD	ČSSD	kA	ČSSD
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
následně	následně	k6eAd1	následně
sestavila	sestavit	k5eAaPmAgFnS	sestavit
vládu	vláda	k1gFnSc4	vláda
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
US-DEU	US-DEU	k1gFnSc2	US-DEU
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
celé	celý	k2eAgNnSc1d1	celé
své	svůj	k3xOyFgNnSc4	svůj
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
jen	jen	k6eAd1	jen
nepatrnou	patrný	k2eNgFnSc7d1	patrný
většinou	většina	k1gFnSc7	většina
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
58	[number]	k4	58
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
99,56	[number]	k4	99,56
%	%	kIx~	%
(	(	kIx(	(
<g/>
4	[number]	k4	4
768	[number]	k4	768
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
platně	platně	k6eAd1	platně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
stran	strana	k1gFnPc2	strana
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zvolení	zvolený	k2eAgMnPc1d1	zvolený
poslanci	poslanec	k1gMnPc1	poslanec
podle	podle	k7c2	podle
politické	politický	k2eAgFnSc2d1	politická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Výsledky	výsledek	k1gInPc4	výsledek
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Rozdělení	rozdělení	k1gNnSc1	rozdělení
mandátů	mandát	k1gInPc2	mandát
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Mapy	mapa	k1gFnPc4	mapa
výsledků	výsledek	k1gInPc2	výsledek
====	====	k?	====
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
výsledků	výsledek	k1gInPc2	výsledek
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
