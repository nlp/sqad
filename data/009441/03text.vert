<p>
<s>
Pieria	Pierium	k1gNnPc1	Pierium
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Π	Π	k?	Π
<g/>
–	–	k?	–
<g/>
Pieria	Pierium	k1gNnSc2	Pierium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přímořský	přímořský	k2eAgInSc1d1	přímořský
řecký	řecký	k2eAgInSc1d1	řecký
region	region	k1gInSc1	region
a	a	k8xC	a
administrativní	administrativní	k2eAgFnSc1d1	administrativní
krajská	krajský	k2eAgFnSc1d1	krajská
jednotka	jednotka	k1gFnSc1	jednotka
v	v	k7c4	v
jižní	jižní	k2eAgNnSc4d1	jižní
Makedónií	Makedónie	k1gFnSc7	Makedónie
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hora	hora	k1gFnSc1	hora
Olymp	Olymp	k1gInSc1	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Pieria	Pierium	k1gNnPc1	Pierium
hraničí	hraničit	k5eAaImIp3nP	hraničit
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
s	s	k7c7	s
makedonským	makedonský	k2eAgInSc7d1	makedonský
krajem	kraj	k1gInSc7	kraj
Imathia	Imathium	k1gNnSc2	Imathium
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
s	s	k7c7	s
Thesálií	Thesálie	k1gFnSc7	Thesálie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Katerini	Katerin	k1gMnPc1	Katerin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Kraj	kraj	k1gInSc1	kraj
Pieria	Pierium	k1gNnSc2	Pierium
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
obydlený	obydlený	k2eAgInSc1d1	obydlený
trackým	tracký	k2eAgInSc7d1	tracký
kmenem	kmen	k1gInSc7	kmen
Pierů	pier	k1gInPc2	pier
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
řečtí	řecký	k2eAgMnPc1d1	řecký
Makedonci	Makedonec	k1gMnPc1	Makedonec
a	a	k8xC	a
kraj	kraj	k1gInSc1	kraj
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Makedonci	Makedonec	k1gMnPc1	Makedonec
zde	zde	k6eAd1	zde
postavili	postavit	k5eAaPmAgMnP	postavit
město	město	k1gNnSc4	město
Dion	Diona	k1gFnPc2	Diona
(	(	kIx(	(
<g/>
od	od	k7c2	od
Dias	Diasa	k1gFnPc2	Diasa
=	=	kIx~	=
Zeus	Zeus	k1gInSc1	Zeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
město	město	k1gNnSc1	město
zde	zde	k6eAd1	zde
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Pydna	Pydna	k1gFnSc1	Pydna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
7	[number]	k4	7
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
a	a	k8xC	a
osídlili	osídlit	k5eAaPmAgMnP	osídlit
Eubojčané	Eubojčan	k1gMnPc1	Eubojčan
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
z	z	k7c2	z
Euboie	Euboie	k1gFnSc2	Euboie
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
i	i	k8xC	i
město	město	k1gNnSc4	město
Methot	Methota	k1gFnPc2	Methota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
století	století	k1gNnSc6	století
však	však	k9	však
tato	tento	k3xDgNnPc4	tento
města	město	k1gNnPc4	město
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Makedonci	Makedonec	k1gMnPc1	Makedonec
<g/>
.	.	kIx.	.
</s>
<s>
Pieria	Pierium	k1gNnPc4	Pierium
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Řeky	Řek	k1gMnPc4	Řek
bájný	bájný	k2eAgInSc1d1	bájný
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
bájí	báj	k1gFnPc2	báj
právě	právě	k9	právě
zde	zde	k6eAd1	zde
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Olymp	Olymp	k1gInSc4	Olymp
měli	mít	k5eAaImAgMnP	mít
sídlo	sídlo	k1gNnSc4	sídlo
řečtí	řecký	k2eAgMnPc1d1	řecký
bohové	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Pydna	Pydna	k1gFnSc1	Pydna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důležitým	důležitý	k2eAgNnSc7d1	důležité
městem	město	k1gNnSc7	město
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
kraj	kraj	k1gInSc1	kraj
po	po	k7c6	po
Římsko-makedonských	římskoakedonský	k2eAgFnPc6d1	římsko-makedonský
válkách	válka	k1gFnPc6	válka
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začalo	začít	k5eAaPmAgNnS	začít
šířit	šířit	k5eAaImF	šířit
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pohanské	pohanský	k2eAgNnSc1d1	pohanské
náboženství	náboženství	k1gNnSc1	náboženství
zde	zde	k6eAd1	zde
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
až	až	k9	až
do	do	k7c2	do
4	[number]	k4	4
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
Pieria	Pierium	k1gNnSc2	Pierium
nehrál	hrát	k5eNaImAgInS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ani	ani	k8xC	ani
během	během	k7c2	během
Římského	římský	k2eAgInSc2d1	římský
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
během	během	k7c2	během
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
(	(	kIx(	(
<g/>
Byzantského	byzantský	k2eAgNnSc2d1	byzantské
<g/>
)	)	kIx)	)
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
invaze	invaze	k1gFnSc1	invaze
Řecka	Řecko	k1gNnSc2	Řecko
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Pierie	Pierie	k1gFnSc1	Pierie
dotkla	dotknout	k5eAaPmAgFnS	dotknout
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
kontinuálně	kontinuálně	k6eAd1	kontinuálně
a	a	k8xC	a
od	od	k7c2	od
7	[number]	k4	7
století	století	k1gNnSc2	století
Pieria	Pierium	k1gNnSc2	Pierium
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
řecké	řecký	k2eAgFnSc2d1	řecká
části	část	k1gFnSc2	část
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
sever	sever	k1gInSc1	sever
byl	být	k5eAaImAgInS	být
osídlen	osídlit	k5eAaPmNgMnS	osídlit
Slovany	Slovan	k1gMnPc7	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
vládli	vládnout	k5eAaImAgMnP	vládnout
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Pieria	Pierium	k1gNnSc2	Pierium
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Řecka	Řecko	k1gNnSc2	Řecko
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Makedonii	Makedonie	k1gFnSc4	Makedonie
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
řecký	řecký	k2eAgMnSc1d1	řecký
premiér	premiér	k1gMnSc1	premiér
Venizelos	Venizelos	k1gMnSc1	Venizelos
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
byl	být	k5eAaImAgInS	být
modernizován	modernizovat	k5eAaBmNgInS	modernizovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
částí	část	k1gFnSc7	část
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
po	po	k7c6	po
Řecko-turecké	řeckourecký	k2eAgFnSc6d1	řecko-turecká
výměně	výměna	k1gFnSc6	výměna
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
mnohé	mnohé	k1gNnSc4	mnohé
řecké	řecký	k2eAgFnSc2d1	řecká
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
z	z	k7c2	z
Efezu	Efez	k1gInSc2	Efez
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
Nea	Nea	k1gMnSc1	Nea
Efesos	Efesos	k1gMnSc1	Efesos
a	a	k8xC	a
Pontští	pontský	k2eAgMnPc1d1	pontský
Řekové	Řek	k1gMnPc1	Řek
z	z	k7c2	z
Trapezuntu	Trapezunt	k1gInSc2	Trapezunt
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
Nea	Nea	k1gMnSc1	Nea
Trapezunta	Trapezunta	k1gMnSc1	Trapezunta
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pierie	Pierie	k1gFnSc2	Pierie
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
Soluně	Soluň	k1gFnSc2	Soluň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Pieria	Pierium	k1gNnSc2	Pierium
dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
135	[number]	k4	135
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgMnSc1d1	důležitý
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
četné	četný	k2eAgFnPc1d1	četná
starořecké	starořecký	k2eAgFnPc1d1	starořecká
památky	památka	k1gFnPc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
provádět	provádět	k5eAaImF	provádět
turistika	turistika	k1gFnSc1	turistika
u	u	k7c2	u
hory	hora	k1gFnSc2	hora
Olymp	Olymp	k1gInSc1	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
dovolenková	dovolenkový	k2eAgFnSc1d1	dovolenková
zóna	zóna	k1gFnSc1	zóna
Olympská	olympský	k2eAgFnSc1d1	Olympská
riviéra	riviéra	k1gFnSc1	riviéra
v	v	k7c6	v
Katerini	Katerin	k2eAgMnPc1d1	Katerin
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
Katerini	Katerin	k1gMnPc1	Katerin
<g/>
,	,	kIx,	,
Aiginio	Aiginio	k1gMnSc1	Aiginio
<g/>
,	,	kIx,	,
Dio	Dio	k1gMnSc1	Dio
(	(	kIx(	(
<g/>
starořecký	starořecký	k2eAgInSc1d1	starořecký
Dion	Dion	k1gInSc1	Dion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Elafina	Elafino	k1gNnSc2	Elafino
<g/>
,	,	kIx,	,
Kolindros	Kolindrosa	k1gFnPc2	Kolindrosa
<g/>
,	,	kIx,	,
Litochoro	Litochora	k1gFnSc5	Litochora
<g/>
,	,	kIx,	,
Methoni	Methoň	k1gFnSc3	Methoň
a	a	k8xC	a
Pydna	Pydn	k1gInSc2	Pydn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pieria	Pierium	k1gNnSc2	Pierium
(	(	kIx(	(
<g/>
územie	územie	k1gFnSc1	územie
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pieria	Pierium	k1gNnSc2	Pierium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
