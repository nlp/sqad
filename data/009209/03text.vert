<p>
<s>
Arrondissement	Arrondissement	k1gMnSc1	Arrondissement
Diksmuide	Diksmuid	k1gInSc5	Diksmuid
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
arrondissementů	arrondissement	k1gInPc2	arrondissement
(	(	kIx(	(
<g/>
okresů	okres	k1gInPc2	okres
<g/>
)	)	kIx)	)
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Západní	západní	k2eAgInPc4d1	západní
Flandry	Flandry	k1gInPc4	Flandry
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
činil	činit	k5eAaImAgInS	činit
51	[number]	k4	51
250	[number]	k4	250
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
okresu	okres	k1gInSc2	okres
činí	činit	k5eAaImIp3nS	činit
362,42	[number]	k4	362,42
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Okres	okres	k1gInSc1	okres
Diksmuide	Diksmuid	k1gInSc5	Diksmuid
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
obcí	obec	k1gFnPc2	obec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Diksmuide	Diksmuid	k1gMnSc5	Diksmuid
</s>
</p>
<p>
<s>
Houthulst	Houthulst	k1gFnSc1	Houthulst
</s>
</p>
<p>
<s>
Koekelare	Koekelar	k1gMnSc5	Koekelar
</s>
</p>
<p>
<s>
Kortemark	Kortemark	k1gInSc1	Kortemark
</s>
</p>
<p>
<s>
Lo-Reninge	Lo-Reninge	k6eAd1	Lo-Reninge
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bezirk	Bezirk	k1gInSc1	Bezirk
Diksmuide	Diksmuid	k1gInSc5	Diksmuid
na	na	k7c6	na
německé	německý	k2eAgFnSc3d1	německá
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
