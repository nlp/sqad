<s>
BMW	BMW	kA
</s>
<s>
Bayerische	Bayerische	k1gNnSc1
Motoren	Motorna	k1gFnPc2
Werke	Werke	k1gNnSc2
AG	AG	kA
Logo	logo	k1gNnSc1
Základní	základní	k2eAgFnSc7d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1916	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Franz	Franz	k1gMnSc1
Josef	Josef	k1gMnSc1
Popp	Popp	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Bavorsko	Bavorsko	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
BMW	BMW	kA
Headquarters	Headquarters	k1gInSc1
<g/>
)	)	kIx)
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
Norbert	Norbert	k1gMnSc1
Reithofer	Reithofer	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Předseda	předseda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Harald	Harald	k1gMnSc1
Krüger	Krüger	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Chief	Chief	k1gMnSc1
executive	executiv	k1gInSc5
officer	officer	k1gInSc1
<g/>
)	)	kIx)
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Strojírenský	strojírenský	k2eAgMnSc1d1
Produkty	produkt	k1gInPc1
</s>
<s>
Automobily	automobil	k1gInPc1
<g/>
,	,	kIx,
motocykly	motocykl	k1gInPc1
a	a	k8xC
jízdní	jízdní	k2eAgNnPc1d1
kola	kolo	k1gNnPc1
Obrat	obrat	k1gInSc1
</s>
<s>
€	€	k?
<g/>
10.655	10.655	k4
mld	mld	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
€	€	k?
<g/>
8.706	8.706	k4
mld	mld	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
129,932	129,932	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Majitelé	majitel	k1gMnPc1
</s>
<s>
Stefan	Stefan	k1gMnSc1
Quandt	Quandt	k1gMnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
Susanne	Susann	k1gMnSc5
Klatten	Klattno	k1gNnPc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
Minoritní	minoritní	k2eAgMnPc1d1
akcionáři	akcionář	k1gMnPc1
(	(	kIx(
<g/>
50	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
Dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Rolls-RoyceBMW	Rolls-RoyceBMW	k?
Group	Group	k1gInSc1
Plant	planta	k1gFnPc2
DingolfingRolls-Royce	DingolfingRolls-Royce	k1gMnSc1
DeutschlandBMW	DeutschlandBMW	k1gMnSc1
Group	Group	k1gMnSc1
ClassicBMW	ClassicBMW	k1gMnSc1
divize	divize	k1gFnSc2
MLand	MLand	k1gMnSc1
Rover	rover	k1gMnSc1
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
bmw	bmw	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
ISIN	ISIN	kA
</s>
<s>
DE0005190003	DE0005190003	k4
a	a	k8xC
DE0005190037	DE0005190037	k1gMnSc1
LEI	lei	k1gInSc2
</s>
<s>
YEH5ZCD6E441RHVHD759	YEH5ZCD6E441RHVHD759	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
„	„	k?
<g/>
Čtyřválec	čtyřválec	k1gInSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
sídlem	sídlo	k1gNnSc7
společnosti	společnost	k1gFnSc2
BMW	BMW	kA
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
popředí	popředí	k1gNnSc6
budova	budova	k1gFnSc1
BMW	BMW	kA
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
BMW	BMW	kA
Dixi	Dixi	k1gNnSc1
</s>
<s>
BMW	BMW	kA
303	#num#	k4
1933	#num#	k4
</s>
<s>
BMW	BMW	kA
328	#num#	k4
Roadster	roadster	k1gInSc1
1938	#num#	k4
</s>
<s>
BMW	BMW	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
Bayerische	Bayerische	k1gInSc4
Motoren	Motorna	k1gFnPc2
Werke	Werk	k1gInSc2
AG	AG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německý	německý	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
automobilů	automobil	k1gInPc2
<g/>
,	,	kIx,
motocyklů	motocykl	k1gInPc2
a	a	k8xC
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
sídlo	sídlo	k1gNnSc4
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMW	BMW	kA
je	být	k5eAaImIp3nS
mateřskou	mateřský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
firem	firma	k1gFnPc2
Mini	mini	k2eAgNnSc2d1
a	a	k8xC
Rolls-Royce	Rolls-Royec	k1gInPc4
a	a	k8xC
v	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
minulosti	minulost	k1gFnSc6
i	i	k9
bývalé	bývalý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Rover	rover	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
zaměstnával	zaměstnávat	k5eAaImAgInS
koncern	koncern	k1gInSc1
přes	přes	k7c4
105	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modro-bílý	Modro-bílý	k2eAgInSc1d1
vzor	vzor	k1gInSc1
znaku	znak	k1gInSc2
firmy	firma	k1gFnSc2
je	být	k5eAaImIp3nS
stylizací	stylizace	k1gFnSc7
bavorské	bavorský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
symbol	symbol	k1gInSc4
výrobce	výrobce	k1gMnSc2
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
se	se	k3xPyFc4
spojily	spojit	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
německé	německý	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
<g/>
,	,	kIx,
Rapp	Rapp	k1gInSc1
Motorenwerke	Motorenwerk	k1gFnSc2
a	a	k8xC
Gustav	Gustav	k1gMnSc1
Otto	Otto	k1gMnSc1
Flugzeugwerke	Flugzeugwerke	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
vznikla	vzniknout	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Bayerische	Bayerische	k1gFnSc1
Flugzeugwerke	Flugzeugwerke	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c6
Bayerische	Bayerische	k1gFnSc6
Motoren	Motorna	k1gFnPc2
Werke	Werk	k1gFnSc2
(	(	kIx(
<g/>
Bavorské	bavorský	k2eAgInPc1d1
Motorové	motorový	k2eAgInPc1d1
Závody	závod	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
BMW	BMW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
se	se	k3xPyFc4
firma	firma	k1gFnSc1
specializovala	specializovat	k5eAaBmAgFnS
pouze	pouze	k6eAd1
na	na	k7c4
výrobu	výroba	k1gFnSc4
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jedním	jeden	k4xCgMnSc7
z	z	k7c2
motorů	motor	k1gInPc2
dokonce	dokonce	k9
získala	získat	k5eAaPmAgFnS
světový	světový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
Franz	Franz	k1gMnSc1
Diemer	Diemer	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
s	s	k7c7
letadlem	letadlo	k1gNnSc7
poháněným	poháněný	k2eAgNnSc7d1
motorem	motor	k1gInSc7
BMW	BMW	kA
IV	IV	kA
rekordní	rekordní	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
9	#num#	k4
759	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
místě	místo	k1gNnSc6
kde	kde	k6eAd1
automobilka	automobilka	k1gFnSc1
sídlí	sídlet	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
sestrojen	sestrojen	k2eAgInSc4d1
první	první	k4xOgInSc4
motocykl	motocykl	k1gInSc4
BMW	BMW	kA
R	R	kA
<g/>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
do	do	k7c2
automobilového	automobilový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
odkoupila	odkoupit	k5eAaPmAgFnS
závod	závod	k1gInSc4
v	v	k7c6
Eisenachu	Eisenach	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
vyráběla	vyrábět	k5eAaImAgFnS
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc4d1
model	model	k1gInSc4
na	na	k7c6
základě	základ	k1gInSc6
vozu	vůz	k1gInSc2
Austin	Austina	k1gFnPc2
Seven	Seven	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
vyráběla	vyrábět	k5eAaImAgFnS
na	na	k7c6
základě	základ	k1gInSc6
licence	licence	k1gFnSc2
pod	pod	k7c7
označením	označení	k1gNnSc7
Dixi	Dix	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
vyrábět	vyrábět	k5eAaImF
automobil	automobil	k1gInSc4
vlastní	vlastní	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
–	–	k?
Dixi	Dixi	k1gNnSc1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
byl	být	k5eAaImAgInS
velice	velice	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
a	a	k8xC
prodalo	prodat	k5eAaPmAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc2
kolem	kolem	k7c2
16	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
model	model	k1gInSc1
měl	mít	k5eAaImAgInS
motor	motor	k1gInSc4
o	o	k7c6
objemu	objem	k1gInSc6
750	#num#	k4
cm³	cm³	k?
a	a	k8xC
výkonem	výkon	k1gInSc7
11	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
14	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgFnSc1d2
verze	verze	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1932	#num#	k4
nazývaná	nazývaný	k2eAgFnSc1d1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
PS	PS	kA
měla	mít	k5eAaImAgFnS
vyšší	vysoký	k2eAgInSc4d2
výkon	výkon	k1gInSc4
a	a	k8xC
modernější	moderní	k2eAgFnSc6d2
konstrukci	konstrukce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Předválečné	předválečný	k2eAgInPc1d1
modely	model	k1gInPc1
</s>
<s>
Začátkem	začátkem	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
již	již	k6eAd1
měly	mít	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
modely	model	k1gInPc1
charakteristickou	charakteristický	k2eAgFnSc4d1
masku	maska	k1gFnSc4
chladiče	chladič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějším	důležitý	k2eAgInSc7d3
modelem	model	k1gInSc7
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
303	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
s	s	k7c7
1,2	1,2	k4
<g/>
litrovým	litrový	k2eAgInSc7d1
šestiválcem	šestiválec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
nabízel	nabízet	k5eAaImAgInS
skvělé	skvělý	k2eAgFnPc4d1
jízdní	jízdní	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
a	a	k8xC
navíc	navíc	k6eAd1
se	se	k3xPyFc4
dodávaly	dodávat	k5eAaImAgFnP
různé	různý	k2eAgFnPc1d1
karoserie	karoserie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
následovaly	následovat	k5eAaImAgInP
modely	model	k1gInPc1
309	#num#	k4
<g/>
,	,	kIx,
315	#num#	k4
a	a	k8xC
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
stále	stále	k6eAd1
malá	malý	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
se	se	k3xPyFc4
zasloužila	zasloužit	k5eAaPmAgNnP
o	o	k7c4
zvětšení	zvětšení	k1gNnSc4
produkce	produkce	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
zvyšování	zvyšování	k1gNnSc4
zisku	zisk	k1gInSc2
automobilky	automobilka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
nejznámějším	známý	k2eAgInSc7d3
předválečným	předválečný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
značky	značka	k1gFnSc2
je	být	k5eAaImIp3nS
model	model	k1gInSc4
328	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lehký	lehký	k2eAgInSc1d1
elegantní	elegantní	k2eAgInSc1d1
roadster	roadster	k1gInSc1
se	se	k3xPyFc4
vyráběl	vyrábět	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
poháněn	pohánět	k5eAaImNgMnS
šestiválcem	šestiválec	k1gInSc7
se	s	k7c7
třemi	tři	k4xCgInPc7
karburátory	karburátor	k1gInPc7
Solex	Solex	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
971	#num#	k4
cm³	cm³	k?
<g/>
)	)	kIx)
a	a	k8xC
výkonem	výkon	k1gInSc7
57	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
77	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
dosahoval	dosahovat	k5eAaImAgInS
maximální	maximální	k2eAgFnPc4d1
rychlosti	rychlost	k1gFnPc4
150	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k8xC,k8xS
nejrychlejším	rychlý	k2eAgInSc7d3
sériově	sériově	k6eAd1
vyráběným	vyráběný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typ	typ	k1gInSc1
328	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
velmi	velmi	k6eAd1
moderní	moderní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmizely	zmizet	k5eAaPmAgFnP
stupačky	stupačka	k1gFnPc1
a	a	k8xC
přední	přední	k2eAgInPc1d1
světlomety	světlomet	k1gInPc1
byly	být	k5eAaImAgInP
zapuštěny	zapustit	k5eAaPmNgInP
do	do	k7c2
karoserie	karoserie	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
u	u	k7c2
předválečných	předválečný	k2eAgInPc2d1
vozů	vůz	k1gInPc2
naprosto	naprosto	k6eAd1
výjimečné	výjimečný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
velice	velice	k6eAd1
oblíbeným	oblíbený	k2eAgInPc3d1
hlavně	hlavně	k6eAd1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zakladatelem	zakladatel	k1gMnSc7
tradice	tradice	k1gFnSc2
anglických	anglický	k2eAgInPc2d1
roadsterů	roadster	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyvinuta	vyvinout	k5eAaPmNgFnS
i	i	k9
závodní	závodní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
dosahovala	dosahovat	k5eAaImAgFnS
nevídaných	vídaný	k2eNgNnPc6d1
</s>
<s>
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
největší	veliký	k2eAgInPc4d3
úspěchy	úspěch	k1gInPc4
patřilo	patřit	k5eAaImAgNnS
vítězství	vítězství	k1gNnSc4
v	v	k7c6
závodě	závod	k1gInSc6
Mille	Mille	k1gNnSc2
Miglia	Miglium	k1gNnSc2
a	a	k8xC
24	#num#	k4
<g/>
hodinovém	hodinový	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Le	Le	k1gFnSc6
Mans	Mansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
tento	tento	k3xDgInSc4
závod	závod	k1gInSc4
byla	být	k5eAaImAgFnS
zkonstruována	zkonstruován	k2eAgFnSc1d1
speciální	speciální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
kupé	kupé	k1gNnSc2
od	od	k7c2
italské	italský	k2eAgFnSc2d1
návrhářské	návrhářský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Corrozzeria	Corrozzerium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledními	poslední	k2eAgInPc7d1
předválečnými	předválečný	k2eAgInPc7d1
modely	model	k1gInPc7
byly	být	k5eAaImAgFnP
321	#num#	k4
a	a	k8xC
335	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgInPc1d1
modely	model	k1gInPc1
</s>
<s>
BMW	BMW	kA
502	#num#	k4
Coupe	coup	k1gInSc5
</s>
<s>
BMW	BMW	kA
507	#num#	k4
</s>
<s>
BMW	BMW	kA
1600	#num#	k4
kabriolet	kabriolet	k1gInSc4
</s>
<s>
BMW	BMW	kA
2002	#num#	k4
Touring	Touring	k1gInSc4
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
výroba	výroba	k1gFnSc1
natrvalo	natrvalo	k6eAd1
přesunula	přesunout	k5eAaPmAgFnS
z	z	k7c2
okupovaného	okupovaný	k2eAgInSc2d1
Eisenachu	Eisenach	k1gInSc2
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
motocyklů	motocykl	k1gInPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
výroba	výroba	k1gFnSc1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
poválečným	poválečný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
byl	být	k5eAaImAgInS
model	model	k1gInSc1
501	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ještě	ještě	k6eAd1
vycházel	vycházet	k5eAaImAgInS
z	z	k7c2
předválečných	předválečný	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
BMW	BMW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřdveřový	čtyřdveřový	k2eAgInSc4d1
vůz	vůz	k1gInSc4
poháněný	poháněný	k2eAgInSc4d1
šestiválcem	šestiválec	k1gInSc7
(	(	kIx(
<g/>
2	#num#	k4
<g/>
l	l	kA
<g/>
)	)	kIx)
podpořil	podpořit	k5eAaPmAgInS
znovuvzkříšení	znovuvzkříšení	k1gNnSc4
značky	značka	k1gFnSc2
a	a	k8xC
</s>
<s>
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
postaral	postarat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
firma	firma	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
seznam	seznam	k1gInSc4
výrobců	výrobce	k1gMnPc2
luxusních	luxusní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následoval	následovat	k5eAaImAgMnS
typ	typ	k1gInSc4
502	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmiválcový	osmiválcový	k2eAgInSc1d1
motor	motor	k1gInSc1
V8	V8	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
600	#num#	k4
cm³	cm³	k?
<g/>
)	)	kIx)
poháněl	pohánět	k5eAaImAgMnS
mnoho	mnoho	k4c1
modelů	model	k1gInPc2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejhezčí	hezký	k2eAgFnSc1d3
z	z	k7c2
nich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
bezpochyby	bezpochyby	k6eAd1
kupé	kupé	k1gNnSc1
507	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ovšem	ovšem	k9
neslavilo	slavit	k5eNaImAgNnS
očekávaný	očekávaný	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
luxusních	luxusní	k2eAgInPc6d1
automobilech	automobil	k1gInPc6
totiž	totiž	k9
nebyla	být	k5eNaImAgFnS
v	v	k7c6
poválečné	poválečný	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
velká	velký	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodělávající	prodělávající	k2eAgFnSc4d1
automobilku	automobilka	k1gFnSc4
zachránila	zachránit	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
(	(	kIx(
<g/>
pod	pod	k7c7
licencí	licence	k1gFnSc7
<g/>
)	)	kIx)
italských	italský	k2eAgInPc2d1
vozů	vůz	k1gInPc2
Isetta	Isett	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miniaturní	miniaturní	k2eAgNnSc1d1
vozidlo	vozidlo	k1gNnSc4
poháněl	pohánět	k5eAaImAgInS
vzduchem	vzduch	k1gInSc7
chlazený	chlazený	k2eAgInSc4d1
jednoválec	jednoválec	k1gInSc4
s	s	k7c7
výkonem	výkon	k1gInSc7
8,8	8,8	k4
kW	kW	kA
(	(	kIx(
<g/>
12	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
úspěšnými	úspěšný	k2eAgInPc7d1
modely	model	k1gInPc7
zvyšující	zvyšující	k2eAgFnSc2d1
zisk	zisk	k1gInSc4
automobilce	automobilka	k1gFnSc3
byly	být	k5eAaImAgInP
500	#num#	k4
a	a	k8xC
600	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
zaznamenaly	zaznamenat	k5eAaPmAgFnP
úspěchy	úspěch	k1gInPc4
hlavně	hlavně	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
vyrábět	vyrábět	k5eAaImF
model	model	k1gInSc1
1500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
sedan	sedan	k1gInSc4
s	s	k7c7
motorem	motor	k1gInSc7
vpředu	vpředu	k6eAd1
a	a	k8xC
poháněnými	poháněný	k2eAgFnPc7d1
zadními	zadní	k2eAgFnPc7d1
koly	kolo	k1gNnPc7
předznamenal	předznamenat	k5eAaPmAgInS
další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
značky	značka	k1gFnSc2
BMW	BMW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
automobilka	automobilka	k1gFnSc1
nabízela	nabízet	k5eAaImAgFnS
modely	model	k1gInPc1
1600	#num#	k4
<g/>
,	,	kIx,
1800	#num#	k4
a	a	k8xC
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoudveřové	dvoudveřový	k2eAgFnPc1d1
verze	verze	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
označení	označení	k1gNnSc4
</s>
<s>
s	s	k7c7
dvojkou	dvojka	k1gFnSc7
na	na	k7c6
konci	konec	k1gInSc6
<g/>
,	,	kIx,
tedy	tedy	k9
1502	#num#	k4
<g/>
,	,	kIx,
1602	#num#	k4
<g/>
,	,	kIx,
1802	#num#	k4
a	a	k8xC
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
koupilo	koupit	k5eAaPmAgNnS
BMW	BMW	kA
společnost	společnost	k1gFnSc1
Hans	hansa	k1gFnPc2
Glas	Glas	k1gInSc4
BmbH	BmbH	k1gFnSc4
v	v	k7c6
Dingolfingu	Dingolfing	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
postavila	postavit	k5eAaPmAgFnS
svojí	svojit	k5eAaImIp3nP
druhou	druhý	k4xOgFnSc4
továrnu	továrna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
čekal	čekat	k5eAaImAgInS
automobilku	automobilka	k1gFnSc4
rozmach	rozmach	k1gInSc1
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
v	v	k7c6
Dingolfingu	Dingolfing	k1gInSc6
postavena	postaven	k2eAgFnSc1d1
druhá	druhý	k4xOgFnSc1
továrna	továrna	k1gFnSc1
a	a	k8xC
celkem	celkem	k6eAd1
čtvrtá	čtvrtý	k4xOgFnSc1
továrna	továrna	k1gFnSc1
v	v	k7c6
Landshutu	Landshut	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
první	první	k4xOgInSc4
sériově	sériově	k6eAd1
vyráběný	vyráběný	k2eAgInSc4d1
vůz	vůz	k1gInSc4
s	s	k7c7
turbo	turba	k1gFnSc5
motorem	motor	k1gInSc7
–	–	k?
BMW	BMW	kA
2002	#num#	k4
Turbo	turba	k1gFnSc5
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
ale	ale	k9
nedočkal	dočkat	k5eNaPmAgMnS
úspěchu	úspěch	k1gInSc2
kvůli	kvůli	k7c3
ropné	ropný	k2eAgFnSc3d1
krizi	krize	k1gFnSc3
(	(	kIx(
<g/>
vůz	vůz	k1gInSc1
měl	mít	k5eAaImAgInS
totiž	totiž	k9
spotřebu	spotřeba	k1gFnSc4
22	#num#	k4
l	l	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
výroby	výroba	k1gFnSc2
motocyklů	motocykl	k1gInPc2
slaví	slavit	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
padesáté	padesátý	k4xOgFnSc2
výročí	výročí	k1gNnPc2
a	a	k8xC
po	po	k7c4
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
500	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomtéž	týž	k3xTgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
první	první	k4xOgFnSc1
generace	generace	k1gFnSc1
řady	řada	k1gFnSc2
5	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
modely	model	k1gInPc7
1500	#num#	k4
až	až	k9
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
se	se	k3xPyFc4
představila	představit	k5eAaPmAgFnS
„	„	k?
<g/>
trojková	trojkový	k2eAgFnSc1d1
<g/>
“	“	k?
řada	řada	k1gFnSc1
nahrazující	nahrazující	k2eAgInSc4d1
modely	model	k1gInPc4
typu	typ	k1gInSc2
1502	#num#	k4
až	až	k9
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgInP
představeny	představen	k2eAgInPc1d1
vozy	vůz	k1gInPc1
řady	řada	k1gFnSc2
6	#num#	k4
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
výrobní	výrobní	k2eAgInSc1d1
program	program	k1gInSc1
motocyklů	motocykl	k1gInPc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
představila	představit	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
„	„	k?
<g/>
trojkové	trojkový	k2eAgFnSc2d1
<g/>
“	“	k?
řady	řada	k1gFnSc2
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
nejlepším	dobrý	k2eAgInSc7d3
modelem	model	k1gInSc7
označeným	označený	k2eAgInSc7d1
M	M	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
se	se	k3xPyFc4
prodává	prodávat	k5eAaImIp3nS
řada	řada	k1gFnSc1
5	#num#	k4
se	s	k7c7
vznětovým	vznětový	k2eAgInSc7d1
motorem	motor	k1gInSc7
(	(	kIx(
<g/>
2,4	2,4	k4
l	l	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
třetí	třetí	k4xOgFnSc1
generace	generace	k1gFnSc1
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
prodávat	prodávat	k5eAaImF
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
první	první	k4xOgFnSc1
evropská	evropský	k2eAgFnSc1d1
automobilka	automobilka	k1gFnSc1
BMW	BMW	kA
vznesla	vznést	k5eAaPmAgFnS
požadavek	požadavek	k1gInSc4
na	na	k7c4
používání	používání	k1gNnSc4
bezolovnatého	bezolovnatý	k2eAgInSc2d1
benzínu	benzín	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
představila	představit	k5eAaPmAgFnS
jedny	jeden	k4xCgFnPc4
z	z	k7c2
prvních	první	k4xOgInPc2
evropských	evropský	k2eAgInPc2d1
vozů	vůz	k1gInPc2
s	s	k7c7
katalyzátory	katalyzátor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
koupilo	koupit	k5eAaPmAgNnS
BMW	BMW	kA
společnost	společnost	k1gFnSc1
Rover	rover	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
spadaly	spadat	k5eAaImAgInP,k5eAaPmAgInP
vozy	vůz	k1gInPc1
Rover	rover	k1gMnSc1
<g/>
,	,	kIx,
MG	mg	kA
(	(	kIx(
<g/>
tyto	tento	k3xDgFnPc4
dvě	dva	k4xCgFnPc4
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
zaniklé	zaniklý	k2eAgInPc1d1
<g/>
)	)	kIx)
a	a	k8xC
Land	Land	k1gMnSc1
Rover	rover	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
začala	začít	k5eAaPmAgFnS
firma	firma	k1gFnSc1
dodávat	dodávat	k5eAaImF
své	svůj	k3xOyFgInPc4
motory	motor	k1gInPc4
BMW	BMW	kA
Rolls-Royce	Rolls-Royce	k1gFnSc1
pro	pro	k7c4
letadlo	letadlo	k1gNnSc4
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
koncernu	koncern	k1gInSc2
BMW	BMW	kA
jsou	být	k5eAaImIp3nP
i	i	k9
dceřiné	dceřiný	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
jako	jako	k8xC,k8xS
BMW	BMW	kA
Technik	technik	k1gMnSc1
GmbH	GmbH	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
vývojem	vývoj	k1gInSc7
nových	nový	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
a	a	k8xC
celkovým	celkový	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
celé	celý	k2eAgFnSc2d1
automobilové	automobilový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
BMW	BMW	kA
M	M	kA
GmbH	GmbH	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
výrobou	výroba	k1gFnSc7
sportovních	sportovní	k2eAgInPc2d1
a	a	k8xC
závodních	závodní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
okruhových	okruhový	k2eAgInPc2d1
speciálů	speciál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
BMW	BMW	kA
v	v	k7c6
datech	datum	k1gNnPc6
</s>
<s>
BMW	BMW	kA
M3	M3	k1gFnSc1
</s>
<s>
1916	#num#	k4
<g/>
:	:	kIx,
Založena	založen	k2eAgFnSc1d1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
společnost	společnost	k1gFnSc1
BMW	BMW	kA
na	na	k7c4
výrobu	výroba	k1gFnSc4
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
<g/>
:	:	kIx,
BMW	BMW	kA
dosáhlo	dosáhnout	k5eAaPmAgNnS
světového	světový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
Franz	Franz	k1gMnSc1
Diemer	Diemer	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
s	s	k7c7
motorem	motor	k1gInSc7
BMW	BMW	kA
IV	IV	kA
výšky	výška	k1gFnSc2
9759	#num#	k4
m.	m.	k?
</s>
<s>
1922	#num#	k4
<g/>
:	:	kIx,
Postavena	postaven	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1923	#num#	k4
<g/>
:	:	kIx,
V	v	k7c6
Mnichově	Mnichov	k1gInSc6
byl	být	k5eAaImAgInS
sestrojen	sestrojit	k5eAaPmNgInS
první	první	k4xOgInSc1
motocykl	motocykl	k1gInSc1
BMW	BMW	kA
R	R	kA
<g/>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
<g/>
:	:	kIx,
BMW	BMW	kA
získalo	získat	k5eAaPmAgNnS
29	#num#	k4
světových	světový	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
díky	díky	k7c3
svým	svůj	k3xOyFgInPc3
leteckým	letecký	k2eAgInPc3d1
motorům	motor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
:	:	kIx,
BMW	BMW	kA
odkoupilo	odkoupit	k5eAaPmAgNnS
závod	závod	k1gInSc4
v	v	k7c6
Eisenachu	Eisenach	k1gInSc6
a	a	k8xC
začíná	začínat	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
první	první	k4xOgInSc1
automobil	automobil	k1gInSc1
Dixi	Dix	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
licence	licence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
:	:	kIx,
Rozbíhá	rozbíhat	k5eAaImIp3nS
se	se	k3xPyFc4
výroba	výroba	k1gFnSc1
prvního	první	k4xOgInSc2
automobilu	automobil	k1gInSc2
konstrukce	konstrukce	k1gFnSc2
BMW	BMW	kA
–	–	k?
Dixi	Dixi	k1gNnSc1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
BMW	BMW	kA
3	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
1933	#num#	k4
<g/>
:	:	kIx,
Model	model	k1gInSc1
303	#num#	k4
</s>
<s>
1934	#num#	k4
<g/>
:	:	kIx,
Výroba	výroba	k1gFnSc1
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
samostatného	samostatný	k2eAgInSc2d1
závodu	závod	k1gInSc2
Flugmotorenbau	Flugmotorenbaa	k1gMnSc4
GmbH	GmbH	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
:	:	kIx,
Byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
nejslavnější	slavný	k2eAgInSc1d3
předválečný	předválečný	k2eAgInSc1d1
model	model	k1gInSc1
značky	značka	k1gFnSc2
model	model	k1gInSc4
328	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
<g/>
:	:	kIx,
Byl	být	k5eAaImAgInS
překonán	překonat	k5eAaPmNgInS
rychlostní	rychlostní	k2eAgInSc1d1
rekord	rekord	k1gInSc1
na	na	k7c6
motocyklu	motocykl	k1gInSc6
značky	značka	k1gFnSc2
BMW	BMW	kA
280	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
1938	#num#	k4
<g/>
:	:	kIx,
Model	model	k1gInSc4
328	#num#	k4
roadster	roadster	k1gInSc1
vítězí	vítězit	k5eAaImIp3nS
v	v	k7c6
závodě	závod	k1gInSc6
Mille	Mill	k1gMnSc2
Miglia	Miglius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
1945	#num#	k4
<g/>
:	:	kIx,
Na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
továren	továrna	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
Východního	východní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
proto	proto	k8xC
zabrány	zabrána	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgFnSc1
výroba	výroba	k1gFnSc1
byla	být	k5eAaImAgFnS
přesunuta	přesunout	k5eAaPmNgFnS
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
spojenci	spojenec	k1gMnPc1
nařídili	nařídit	k5eAaPmAgMnP
její	její	k3xOp3gNnSc4
zastavení	zastavení	k1gNnSc4
na	na	k7c4
3	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
:	:	kIx,
Z	z	k7c2
Mnichova	Mnichov	k1gInSc2
vyjíždí	vyjíždět	k5eAaImIp3nS
první	první	k4xOgInSc4
motocykl	motocykl	k1gInSc4
po	po	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
–	–	k?
BMW	BMW	kA
R	R	kA
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
:	:	kIx,
Brány	brána	k1gFnPc4
mnichovské	mnichovský	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
opouští	opouštět	k5eAaImIp3nS
první	první	k4xOgInSc4
poválečný	poválečný	k2eAgInSc4d1
automobil	automobil	k1gInSc4
<g/>
,	,	kIx,
model	model	k1gInSc4
BMW	BMW	kA
501	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
:	:	kIx,
Postaveny	postaven	k2eAgInPc4d1
v	v	k7c6
současnosti	současnost	k1gFnSc6
nejcennější	cenný	k2eAgInPc4d3
modely	model	k1gInPc4
značky	značka	k1gFnSc2
503	#num#	k4
a	a	k8xC
507	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
:	:	kIx,
Luxusní	luxusní	k2eAgInSc1d1
sedan	sedan	k1gInSc1
1500	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
:	:	kIx,
Odkoupena	odkoupen	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
Dingolfingu	Dingolfing	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
druhá	druhý	k4xOgFnSc1
továrna	továrna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
Představena	představen	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založení	založení	k1gNnSc1
společnosti	společnost	k1gFnSc2
BMW	BMW	kA
M	M	kA
GmbH	GmbH	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
:	:	kIx,
Výroba	výroba	k1gFnSc1
prvního	první	k4xOgNnSc2
sériově	sériově	k6eAd1
vyráběného	vyráběný	k2eAgNnSc2d1
automobilem	automobil	k1gInSc7
s	s	k7c7
turbo	turba	k1gFnSc5
motorem	motor	k1gInSc7
–	–	k?
BMW	BMW	kA
2002	#num#	k4
Turbo	turba	k1gFnSc5
</s>
<s>
1975	#num#	k4
<g/>
:	:	kIx,
Představena	představen	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
trojková	trojkový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
:	:	kIx,
Vyvinut	vyvinout	k5eAaPmNgInS
první	první	k4xOgInSc1
motor	motor	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
s	s	k7c7
elektronickým	elektronický	k2eAgInSc7d1
řídícím	řídící	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahájen	zahájen	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
</s>
<s>
vodíkového	vodíkový	k2eAgInSc2d1
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
Do	do	k7c2
výroby	výroba	k1gFnSc2
byly	být	k5eAaImAgInP
začleněny	začleněn	k2eAgInPc1d1
dieselové	dieselový	k2eAgInPc1d1
motory	motor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
První	první	k4xOgInPc4
evropské	evropský	k2eAgInPc4d1
vozy	vůz	k1gInPc4
s	s	k7c7
katalyzátory	katalyzátor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
Vozy	vůz	k1gInPc1
mnichovské	mnichovský	k2eAgFnSc2d1
značky	značka	k1gFnSc2
BMW	BMW	kA
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
nejoblíbenějšími	oblíbený	k2eAgFnPc7d3
v	v	k7c6
nabídce	nabídka	k1gFnSc6
luxusních	luxusní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
:	:	kIx,
Byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
pětimilióntý	pětimilióntý	k2eAgInSc1d1
vůz	vůz	k1gInSc1
BMW	BMW	kA
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgFnPc1d1
modelové	modelový	k2eAgFnPc1d1
řady	řada	k1gFnPc1
</s>
<s>
BMW	BMW	kA
X3	X3	k1gFnSc1
(	(	kIx(
<g/>
interní	interní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
G	G	kA
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
řada	řada	k1gFnSc1
1	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
2	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
3	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
4	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
5	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
6	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
7	#num#	k4
</s>
<s>
řada	řada	k1gFnSc1
8	#num#	k4
</s>
<s>
X1	X1	k4
</s>
<s>
X2	X2	k4
</s>
<s>
X3	X3	k4
</s>
<s>
X4	X4	k4
</s>
<s>
X5	X5	k4
</s>
<s>
X6	X6	k4
</s>
<s>
X7	X7	k4
</s>
<s>
Z4	Z4	k4
</s>
<s>
i	i	k9
<g/>
3	#num#	k4
</s>
<s>
i	i	k9
<g/>
8	#num#	k4
</s>
<s>
Prodeje	prodej	k1gInPc1
</s>
<s>
Enduro	Endura	k1gFnSc5
motocykl	motocykl	k1gInSc1
<g/>
:	:	kIx,
BMW	BMW	kA
F	F	kA
650	#num#	k4
GS	GS	kA
Dakar	Dakar	k1gInSc4
</s>
<s>
RokProdeje	RokProdej	k1gInPc1
skupiny	skupina	k1gFnSc2
BMW	BMW	kA
(	(	kIx(
<g/>
celosvětově	celosvětově	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
Prodeje	prodej	k1gInPc1
značky	značka	k1gFnSc2
BMW	BMW	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000822.181	2000822.181	k4
<g/>
?	?	kIx.
</s>
<s>
2002913.225	2002913.225	k4
<g/>
?	?	kIx.
</s>
<s>
2003928.151	2003928.151	k4
<g/>
?	?	kIx.
</s>
<s>
20041.023	20041.023	k4
<g/>
.583908	.583908	k4
</s>
<s>
20051.126	20051.126	k4
<g/>
.7681	.7681	k4
<g/>
.254	.254	k4
</s>
<s>
20061.185	20061.185	k4
<g/>
.0881	.0881	k4
<g/>
.105	.105	k4
</s>
<s>
20071.276	20071.276	k4
<g/>
.7931	.7931	k4
<g/>
.355	.355	k4
</s>
<s>
20081.202	20081.202	k4
<g/>
.2391	.2391	k4
<g/>
.575	.575	k4
</s>
<s>
20091.068	20091.068	k4
<g/>
.7702	.7702	k4
<g/>
.628	.628	k4
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Automobilka	automobilka	k1gFnSc1
se	se	k3xPyFc4
také	také	k9
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
motorsportu	motorsport	k1gInSc6
<g/>
,	,	kIx,
startovala	startovat	k5eAaBmAgFnS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
v	v	k7c6
seriálu	seriál	k1gInSc6
DTM	DTM	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
získal	získat	k5eAaPmAgInS
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
vozem	vůz	k1gInSc7
titul	titul	k1gInSc1
Bruno	Bruno	k1gMnSc1
Spengler	Spengler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
startovali	startovat	k5eAaBmAgMnP
ve	v	k7c6
WTCC	WTCC	kA
a	a	k8xC
ETCC	ETCC	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
získali	získat	k5eAaPmAgMnP
několik	několik	k4yIc4
titulů	titul	k1gInPc2
díky	díky	k7c3
Andymu	Andymum	k1gNnSc3
Priaulxovi	Priaulxa	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
také	také	k6eAd1
zkusil	zkusit	k5eAaPmAgInS
WRC	WRC	kA
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
vozem	vůz	k1gInSc7
startoval	startovat	k5eAaBmAgInS
například	například	k6eAd1
Bernard	Bernard	k1gMnSc1
Béguin	Béguin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
a	a	k8xC
2012	#num#	k4
startovali	startovat	k5eAaBmAgMnP
se	s	k7c7
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
jako	jako	k9
Mini	mini	k2eAgMnSc1d1
–	–	k?
Mini	mini	k2eAgInSc1d1
WRC	WRC	kA
Team	team	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tým	tým	k1gInSc1
se	se	k3xPyFc4
také	také	k9
objevil	objevit	k5eAaPmAgMnS
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
koncem	koncem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
a	a	k8xC
začátkem	začátek	k1gInSc7
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
působili	působit	k5eAaImAgMnP
mnoho	mnoho	k4c1
let	léto	k1gNnPc2
jako	jako	k9
dodavatelé	dodavatel	k1gMnPc1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
u	u	k7c2
týmu	tým	k1gInSc2
Williams	Williamsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Formule	formule	k1gFnSc2
1	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
BMW	BMW	kA
Sauber	Sauber	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnPc4
hlavní	hlavní	k2eAgFnSc7d1
závodnickou	závodnický	k2eAgFnSc7d1
dvojicí	dvojice	k1gFnSc7
byli	být	k5eAaImAgMnP
Nick	Nick	k1gMnSc1
Heidfeld	Heidfeld	k1gMnSc1
a	a	k8xC
Polák	Polák	k1gMnSc1
Robert	Robert	k1gMnSc1
Kubica	Kubica	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
pro	pro	k7c4
BMW	BMW	kA
vyhrál	vyhrát	k5eAaPmAgInS
Velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Kanady	Kanada	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2009	#num#	k4
F1	F1	k1gFnSc2
opustil	opustit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Geschäftsbericht	Geschäftsberichta	k1gFnPc2
2017	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Annual	Annual	k1gInSc1
Report	report	k1gInSc1
2009	#num#	k4
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMW	BMW	kA
Group	Group	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
BMW	BMW	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
BMW	BMW	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
firmy	firma	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
BMW	BMW	kA
klub	klub	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
BMW	BMW	kA
Group	Group	k1gMnSc1
</s>
<s>
BMW	BMW	kA
•	•	k?
Mini	mini	k2eAgFnSc1d1
•	•	k?
Rolls-Royce	Rolls-Royce	k1gFnSc1
</s>
<s>
Německé	německý	k2eAgFnSc2d1
automobilové	automobilový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
1918	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
AAA	AAA	kA
•	•	k?
ABC	ABC	kA
•	•	k?
Adler	Adler	k1gMnSc1
•	•	k?
AGA	aga	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
•	•	k?
Alfi	Alf	k1gFnSc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alfi	Alfi	k1gNnSc4
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AMBAG	AMBAG	kA
•	•	k?
Amor	Amor	k1gMnSc1
•	•	k?
Anker	Anker	k1gMnSc1
•	•	k?
Apollo	Apollo	k1gMnSc1
•	•	k?
Argeo	Argeo	k1gNnSc4
•	•	k?
Arimofa	Arimof	k1gMnSc2
•	•	k?
Atlantic	Atlantice	k1gFnPc2
•	•	k?
Audi	Audi	k1gNnSc4
•	•	k?
Auto-Ell	Auto-Ell	k1gInSc1
•	•	k?
Badenia	Badenium	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Baer	Baer	k1gMnSc1
•	•	k?
BAW	BAW	kA
•	•	k?
BEB	BEB	kA
•	•	k?
Beckmann	Beckmann	k1gMnSc1
•	•	k?
Benz	Benz	k1gMnSc1
•	•	k?
Bergmann	Bergmann	k1gMnSc1
•	•	k?
Bergo	Bergo	k1gMnSc1
•	•	k?
BF	BF	kA
•	•	k?
Biene	Bien	k1gInSc5
•	•	k?
Bleichert	Bleichert	k1gInSc1
•	•	k?
BMW	BMW	kA
•	•	k?
Bob	Bob	k1gMnSc1
•	•	k?
Borcharding	Borcharding	k1gInSc1
•	•	k?
Borgward	Borgward	k1gMnSc1
•	•	k?
Bravo	bravo	k1gMnSc1
•	•	k?
Brennabor	Brennabor	k1gMnSc1
•	•	k?
Bufag	Bufag	k1gMnSc1
•	•	k?
Bully	bulla	k1gFnSc2
•	•	k?
Butz	Butz	k1gMnSc1
•	•	k?
BZ	bz	k0
•	•	k?
C.	C.	kA
Benz	Benz	k1gInSc1
Söhne	Söhn	k1gInSc5
•	•	k?
Certus	Certus	k1gInSc1
•	•	k?
Club	club	k1gInSc1
•	•	k?
Cockerell	Cockerell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Combi	Comb	k1gFnSc2
•	•	k?
Cyklon	cyklon	k1gInSc1
•	•	k?
Davidl	Davidl	k1gFnSc2
•	•	k?
Dehn	Dehn	k1gInSc1
•	•	k?
DEW	DEW	kA
•	•	k?
Diabolo	diabolo	k1gNnSc1
•	•	k?
Diana	Diana	k1gFnSc1
•	•	k?
Dinos	Dinos	k1gInSc1
•	•	k?
Dixi	Dix	k1gFnSc2
•	•	k?
DKW	DKW	kA
•	•	k?
Dorner	Dorner	k1gMnSc1
•	•	k?
Dürkopp	Dürkopp	k1gMnSc1
•	•	k?
Dux	Dux	k1gMnSc1
•	•	k?
D-Wagen	D-Wagen	k1gInSc1
•	•	k?
EBS	EBS	kA
•	•	k?
Ego	ego	k1gNnSc2
•	•	k?
Ehrhardt	Ehrhardt	k1gMnSc1
•	•	k?
Ehrhardt-Szawe	Ehrhardt-Szawe	k1gInSc1
•	•	k?
Eibach	Eibach	k1gInSc1
•	•	k?
Electra	Electr	k1gMnSc2
•	•	k?
Elektric	Elektric	k1gMnSc1
•	•	k?
Elite	Elit	k1gInSc5
•	•	k?
Elitewagen	Elitewagen	k1gInSc1
•	•	k?
Eos	Eos	k1gFnSc2
•	•	k?
Erco	Erco	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Espenlaub	Espenlaub	k1gInSc1
•	•	k?
Eubu	Eubus	k1gInSc2
•	•	k?
Exor	Exor	k1gMnSc1
•	•	k?
Fadag	Fadag	k1gMnSc1
•	•	k?
Fafag	Fafag	k1gMnSc1
•	•	k?
Fafnir	Fafnir	k1gMnSc1
•	•	k?
Falcon	Falcon	k1gMnSc1
•	•	k?
Fama	Fama	k?
•	•	k?
Faun	fauna	k1gFnPc2
•	•	k?
Ferbedo	Ferbedo	k1gNnSc4
•	•	k?
Ford	ford	k1gInSc1
•	•	k?
Fox	fox	k1gInSc1
•	•	k?
Framo	Frama	k1gFnSc5
•	•	k?
Freia	Freium	k1gNnSc2
•	•	k?
Fulmina	Fulmin	k2eAgFnSc1d1
•	•	k?
Garbaty	Garbat	k1gInPc4
•	•	k?
Gasi	Gas	k1gFnSc2
•	•	k?
Goliath	Goliath	k1gInSc1
•	•	k?
Görke	Görke	k1gInSc1
•	•	k?
Grade	grad	k1gInSc5
•	•	k?
Gridi	Grid	k1gMnPc1
•	•	k?
Gries	Gries	k1gMnSc1
•	•	k?
Habag	Habag	k1gMnSc1
•	•	k?
HAG	HAG	kA
•	•	k?
HAG-Gastell	HAG-Gastell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Hagea-Moto	Hagea-Mota	k1gFnSc5
•	•	k?
Hanomag	Hanomag	k1gInSc1
•	•	k?
Hansa	hansa	k1gFnSc1
•	•	k?
Hansa-Lloyd	Hansa-Lloyd	k1gInSc1
•	•	k?
Hascho	Hascha	k1gFnSc5
•	•	k?
Hataz	Hataz	k1gInSc1
•	•	k?
Hawa	Haw	k1gInSc2
•	•	k?
Heim	Heim	k1gMnSc1
•	•	k?
Helios	Helios	k1gMnSc1
•	•	k?
Helo	Hela	k1gFnSc5
•	•	k?
Hercules	Hercules	k1gMnSc1
•	•	k?
Hero	Hero	k1gMnSc1
•	•	k?
Hildebrand	Hildebrand	k1gInSc1
•	•	k?
Hiller	Hiller	k1gInSc1
•	•	k?
Horch	Horch	k1gInSc1
•	•	k?
HT	HT	kA
•	•	k?
Imperia	Imperium	k1gNnSc2
•	•	k?
Induhag	Induhag	k1gMnSc1
•	•	k?
Ipe	Ipe	k1gMnSc1
•	•	k?
Joswin	Joswin	k1gMnSc1
•	•	k?
Juhö	Juhö	k1gMnSc1
•	•	k?
Kaha	Kaha	k1gMnSc1
•	•	k?
Kaiser	Kaiser	k1gMnSc1
•	•	k?
Keitel	Keitel	k1gMnSc1
•	•	k?
Kenter	Kenter	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Kico	Kico	k1gMnSc1
•	•	k?
Kieling	Kieling	k1gInSc1
•	•	k?
Knöllner	Knöllner	k1gMnSc1
•	•	k?
Kobold	kobold	k1gMnSc1
•	•	k?
Koco	Koco	k6eAd1
•	•	k?
Komet	kometa	k1gFnPc2
•	•	k?
Komnick	Komnick	k1gMnSc1
•	•	k?
Körting	Körting	k1gInSc1
•	•	k?
Kühn	Kühn	k1gInSc1
•	•	k?
Landgrebe	Landgreb	k1gInSc5
•	•	k?
Lauer	Lauer	k1gMnSc1
•	•	k?
Leichtauto	Leichtaut	k2eAgNnSc1d1
•	•	k?
Leifa	Leif	k1gMnSc2
•	•	k?
Lesshaft	Lesshaft	k1gMnSc1
•	•	k?
Ley	Lea	k1gFnSc2
•	•	k?
Libelle	Libelle	k1gFnSc2
•	•	k?
Lindcar	Lindcar	k1gMnSc1
•	•	k?
Lipsia	Lipsia	k1gFnSc1
•	•	k?
Loeb	Loeb	k1gInSc1
•	•	k?
Luther	Luthra	k1gFnPc2
&	&	k?
Heyer	Heyer	k1gMnSc1
•	•	k?
LuWe	LuW	k1gFnSc2
•	•	k?
Luwo	Luwo	k1gMnSc1
•	•	k?
Lux	Lux	k1gMnSc1
•	•	k?
Macu	Maca	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
MAF	MAF	kA
•	•	k?
Magnet	magnet	k1gInSc1
•	•	k?
Maier	Maier	k1gInSc1
•	•	k?
Maja	Maja	k1gFnSc1
•	•	k?
Mannesmann	Mannesmann	k1gInSc1
•	•	k?
Martinette	Martinett	k1gInSc5
•	•	k?
Maurer	Maurer	k1gMnSc1
•	•	k?
Mauser	Mauser	k1gMnSc1
•	•	k?
Maybach	Maybach	k1gMnSc1
•	•	k?
Mayrette	Mayrett	k1gInSc5
•	•	k?
Mercedes	mercedes	k1gInSc1
•	•	k?
Mercedes-Benz	Mercedes-Benz	k1gInSc1
•	•	k?
MFB	MFB	kA
•	•	k?
Mikromobil	Mikromobil	k1gMnSc1
•	•	k?
Minimus	Minimus	k1gMnSc1
•	•	k?
Möckwagen	Möckwagen	k1gInSc1
•	•	k?
Mölkamp	Mölkamp	k1gInSc1
•	•	k?
Moll	moll	k1gNnSc2
•	•	k?
Monos	Monos	k1gMnSc1
•	•	k?
Mops	mops	k1gMnSc1
•	•	k?
Morgan	morgan	k1gMnSc1
•	•	k?
Motobil	Motobil	k1gMnSc1
•	•	k?
Motrix	Motrix	k1gInSc1
•	•	k?
Muvo	Muvo	k6eAd1
•	•	k?
Nafa	Nafa	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
NAG	NAG	kA
•	•	k?
NAG-Presto	NAG-Presta	k1gMnSc5
•	•	k?
NAG-Protos	NAG-Protos	k1gMnSc1
•	•	k?
Nawa	Nawa	k1gMnSc1
•	•	k?
Neander	Neander	k1gMnSc1
•	•	k?
Neiman	Neiman	k1gMnSc1
•	•	k?
Nemalette	Nemalett	k1gInSc5
•	•	k?
Nenndorf	Nenndorf	k1gMnSc1
•	•	k?
Nowa	Nowa	k1gMnSc1
•	•	k?
NSU	NSU	kA
•	•	k?
NSU-Fiat	NSU-Fiat	k1gInSc1
•	•	k?
Nufmobil	Nufmobil	k1gFnSc2
•	•	k?
Nug	Nug	k1gMnSc1
•	•	k?
Omega	omega	k1gFnSc1
•	•	k?
Omikron	omikron	k1gInSc1
•	•	k?
Omnobil	Omnobil	k1gFnSc2
•	•	k?
Onnasch	Onnasch	k1gInSc1
•	•	k?
Opel	opel	k1gInSc1
•	•	k?
Otto	Otto	k1gMnSc1
•	•	k?
Pawi	Paw	k1gFnSc2
•	•	k?
Pe-Ka	Pe-Ka	k1gMnSc1
•	•	k?
Peer	peer	k1gMnSc1
Gynt	Gynt	k1gMnSc1
•	•	k?
Pelikan	Pelikan	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Moritz	moritz	k1gInSc1
•	•	k?
Pfeil	Pfeil	k1gInSc1
•	•	k?
Phänomen	Phänomen	k2eAgInSc1d1
•	•	k?
Pilot	pilot	k1gInSc1
•	•	k?
Pluto	Pluto	k1gMnSc1
•	•	k?
Presto	presto	k6eAd1
•	•	k?
Priamus	Priamus	k1gMnSc1
•	•	k?
Protos	Protos	k1gMnSc1
•	•	k?
Rabag	Rabag	k1gMnSc1
•	•	k?
Remag	Remag	k1gMnSc1
•	•	k?
Renfert	Renfert	k1gMnSc1
•	•	k?
Rex-Simplex	Rex-Simplex	k1gInSc1
•	•	k?
Rhemag	Rhemag	k1gMnSc1
•	•	k?
Rikas	Rikas	k1gMnSc1
•	•	k?
Rivo	Rivo	k1gMnSc1
•	•	k?
Roland	Roland	k1gInSc1
•	•	k?
Röhr	Röhr	k1gInSc1
•	•	k?
Rollfix	Rollfix	k1gInSc1
•	•	k?
Rumpler	Rumpler	k1gInSc1
•	•	k?
Rüttger	Rüttger	k1gInSc1
•	•	k?
RWN	RWN	kA
•	•	k?
Sablatnig-Beuchelt	Sablatnig-Beuchelt	k1gInSc1
•	•	k?
Sauer	Sauer	k1gInSc1
•	•	k?
SB	sb	kA
•	•	k?
Schebera	Schebera	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Schönnagel	Schönnagel	k1gMnSc1
•	•	k?
Schuricht	Schuricht	k1gMnSc1
•	•	k?
Schütte-Lanz	Schütte-Lanz	k1gMnSc1
•	•	k?
Seidel-Arop	Seidel-Arop	k1gInSc1
•	•	k?
Selve	Selev	k1gFnSc2
•	•	k?
SHW	SHW	kA
•	•	k?
Simson	Simson	k1gMnSc1
•	•	k?
Slaby-Beringer	Slaby-Beringer	k1gMnSc1
•	•	k?
Slevogt	Slevogt	k1gMnSc1
•	•	k?
Solomobil	Solomobil	k1gMnSc1
•	•	k?
Sperber	Sperber	k1gMnSc1
•	•	k?
Sphinx	Sphinx	k1gInSc1
•	•	k?
Spinell	Spinell	k1gInSc1
•	•	k?
Staiger	Staiger	k1gInSc1
•	•	k?
Standard	standard	k1gInSc1
•	•	k?
Steiger	Steiger	k1gInSc1
•	•	k?
Stoewer	Stoewer	k1gInSc1
•	•	k?
Stolle	Stolle	k1gFnSc2
•	•	k?
Sun	Sun	kA
•	•	k?
Szawe	Szawe	k1gInSc1
•	•	k?
Tamag	Tamag	k1gInSc1
•	•	k?
Tamm	Tamm	k1gInSc1
•	•	k?
Tatra	Tatra	k1gFnSc1
•	•	k?
Teco	Teco	k6eAd1
•	•	k?
Tempo	tempo	k1gNnSc4
•	•	k?
Theis	Theis	k1gInSc1
•	•	k?
Tornax	Tornax	k1gInSc1
•	•	k?
Tourist	Tourist	k1gInSc1
•	•	k?
Traeger	Traeger	k1gInSc1
•	•	k?
Trinks	Trinks	k1gInSc1
•	•	k?
Trippel	Trippel	k1gInSc1
•	•	k?
Triumph	Triumph	k1gInSc1
•	•	k?
Turbo	turba	k1gFnSc5
Utilitas	Utilitas	k1gInSc4
•	•	k?
VL	VL	kA
•	•	k?
Voran	Voran	k1gInSc1
•	•	k?
Volkswagen	volkswagen	k1gInSc1
•	•	k?
Walmobil	Walmobil	k1gFnSc2
•	•	k?
Wanderer	Wanderer	k1gMnSc1
•	•	k?
Wegmann	Wegmann	k1gMnSc1
•	•	k?
Weise	Weise	k1gFnSc2
•	•	k?
Wesnigk	Wesnigk	k1gInSc1
•	•	k?
Westfalia	Westfalius	k1gMnSc2
•	•	k?
Winkler	Winkler	k1gMnSc1
•	•	k?
Wittekind	Wittekind	k1gMnSc1
•	•	k?
York	York	k1gInSc1
•	•	k?
Zetgelette	Zetgelett	k1gInSc5
•	•	k?
Zündapp	Zündapp	k1gMnSc1
•	•	k?
Zwerg	Zwerg	k1gMnSc1
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
Automobily	automobil	k1gInPc1
značky	značka	k1gFnSc2
BMW	BMW	kA
1980	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
199020002010	#num#	k4
</s>
<s>
0123456789012345678901234567890123456789	#num#	k4
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
třída	třída	k1gFnSc1
</s>
<s>
řady	řada	k1gFnPc1
1	#num#	k4
</s>
<s>
E82	E82	k4
/	/	kIx~
E88	E88	k1gFnSc1
</s>
<s>
E81	E81	k4
/	/	kIx~
E87	E87	k1gFnSc1
</s>
<s>
F20	F20	k4
/	/	kIx~
F21	F21	k1gFnSc1
</s>
<s>
F40	F40	k4
</s>
<s>
řady	řada	k1gFnPc1
2	#num#	k4
</s>
<s>
F22	F22	k4
/	/	kIx~
F23	F23	k1gFnSc1
</s>
<s>
F45	F45	k4
/	/	kIx~
F46	F46	k1gFnSc1
</s>
<s>
F44	F44	k4
</s>
<s>
řady	řada	k1gFnPc1
3	#num#	k4
Compact	Compacta	k1gFnPc2
</s>
<s>
E	E	kA
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
</s>
<s>
E	E	kA
<g/>
46	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
</s>
<s>
Střední	střední	k2eAgFnSc1d1
třída	třída	k1gFnSc1
</s>
<s>
řady	řada	k1gFnPc1
3	#num#	k4
</s>
<s>
E21	E21	k4
</s>
<s>
E30	E30	k4
</s>
<s>
E36	E36	k4
</s>
<s>
E46	E46	k4
</s>
<s>
E90	E90	k4
/	/	kIx~
E91	E91	k1gMnSc1
/	/	kIx~
E92	E92	k1gMnSc1
/	/	kIx~
E93	E93	k1gMnSc1
</s>
<s>
F30	F30	k4
/	/	kIx~
F31	F31	k1gFnSc1
/	/	kIx~
F35	F35	k1gFnSc1
</s>
<s>
G20	G20	k4
</s>
<s>
řady	řada	k1gFnPc1
4	#num#	k4
</s>
<s>
F32	F32	k4
/	/	kIx~
F33	F33	k1gFnSc1
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
třída	třída	k1gFnSc1
</s>
<s>
řady	řada	k1gFnPc1
5	#num#	k4
</s>
<s>
E12	E12	k4
</s>
<s>
E28	E28	k4
</s>
<s>
E34	E34	k4
</s>
<s>
E39	E39	k4
</s>
<s>
E60	E60	k4
/	/	kIx~
E61	E61	k1gFnSc1
</s>
<s>
F10	F10	k4
/	/	kIx~
F11	F11	k1gFnSc1
</s>
<s>
G30	G30	k4
</s>
<s>
Luxusní	luxusní	k2eAgFnSc1d1
</s>
<s>
řady	řada	k1gFnPc1
6	#num#	k4
</s>
<s>
E24	E24	k4
</s>
<s>
E63	E63	k4
/	/	kIx~
E64	E64	k1gFnSc1
</s>
<s>
F12	F12	k4
/	/	kIx~
F13	F13	k1gFnSc1
</s>
<s>
G32	G32	k4
</s>
<s>
řady	řada	k1gFnPc1
7	#num#	k4
</s>
<s>
E23	E23	k4
</s>
<s>
E32	E32	k4
</s>
<s>
E38	E38	k4
</s>
<s>
E65	E65	k4
/	/	kIx~
E66	E66	k1gMnSc1
/	/	kIx~
E67	E67	k1gMnSc1
/	/	kIx~
E68	E68	k1gMnSc1
</s>
<s>
F01	F01	k4
/	/	kIx~
F02	F02	k1gMnSc1
/	/	kIx~
F03	F03	k1gMnSc1
/	/	kIx~
F04	F04	k1gMnSc1
</s>
<s>
G11	G11	k4
/	/	kIx~
G12	G12	k1gFnSc1
</s>
<s>
GT	GT	kA
</s>
<s>
řady	řada	k1gFnPc1
8	#num#	k4
</s>
<s>
E31	E31	k4
</s>
<s>
G15	G15	k4
</s>
<s>
Roadstery	roadster	k1gInPc1
</s>
<s>
Z	z	k7c2
</s>
<s>
E30	E30	k4
(	(	kIx(
<g/>
Z	z	k7c2
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
E	E	kA
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
/	/	kIx~
E	E	kA
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
(	(	kIx(
<g/>
Z	z	k7c2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
E85	E85	k4
/	/	kIx~
E86	E86	k1gMnPc2
(	(	kIx(
<g/>
Z	z	k7c2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
E89	E89	k4
(	(	kIx(
<g/>
Z	z	k7c2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
G29	G29	k4
(	(	kIx(
<g/>
Z	z	k7c2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
automobily	automobil	k1gInPc1
</s>
<s>
E26	E26	k4
(	(	kIx(
<g/>
M	M	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
E52	E52	k4
(	(	kIx(
<g/>
Z	z	k7c2
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SUV	SUV	kA
</s>
<s>
X1	X1	k4
</s>
<s>
E84	E84	k4
</s>
<s>
F48	F48	k4
</s>
<s>
X2	X2	k4
</s>
<s>
F39	F39	k4
</s>
<s>
X3	X3	k4
</s>
<s>
E83	E83	k4
</s>
<s>
F25	F25	k4
</s>
<s>
G01	G01	k4
</s>
<s>
X4	X4	k4
</s>
<s>
F26	F26	k4
</s>
<s>
G02	G02	k4
</s>
<s>
X5	X5	k4
</s>
<s>
E53	E53	k4
</s>
<s>
E70	E70	k4
</s>
<s>
F15	F15	k4
</s>
<s>
G05	G05	k4
</s>
<s>
X6	X6	k4
</s>
<s>
E71	E71	k4
/	/	kIx~
E72	E72	k1gFnSc1
</s>
<s>
F16	F16	k4
</s>
<s>
G06	G06	k4
</s>
<s>
X7	X7	k4
</s>
<s>
G07	G07	k4
</s>
<s>
BMW	BMW	kA
GT	GT	kA
</s>
<s>
3	#num#	k4
GT	GT	kA
</s>
<s>
F34	F34	k4
</s>
<s>
5	#num#	k4
GT	GT	kA
</s>
<s>
F07	F07	k4
</s>
<s>
6	#num#	k4
GT	GT	kA
</s>
<s>
G32	G32	k4
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
<g/>
3	#num#	k4
</s>
<s>
I01	I01	k4
</s>
<s>
i	i	k9
<g/>
8	#num#	k4
</s>
<s>
I12	I12	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
osa	osa	k1gFnSc1
<g/>
2011641868	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2005475-0	2005475-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2308	#num#	k4
257X	257X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81052383	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
129013645	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81052383	#num#	k4
</s>
