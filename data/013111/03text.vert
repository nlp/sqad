<p>
<s>
Wangari	Wangari	k6eAd1	Wangari
Muta	Mut	k2eAgNnPc1d1	Mut
Maathai	Maathai	k1gNnPc1	Maathai
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Nyeri	Nyeri	k1gNnSc1	Nyeri
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Nairobi	Nairobi	k1gNnSc2	Nairobi
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
keňská	keňský	k2eAgFnSc1d1	keňská
environmentální	environmentální	k2eAgFnSc1d1	environmentální
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
aktivistka	aktivistka	k1gFnSc1	aktivistka
a	a	k8xC	a
ekofeministka	ekofeministka	k1gFnSc1	ekofeministka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wangari	Wangari	k1gNnSc1	Wangari
Maathai	Maatha	k1gFnSc2	Maatha
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
veterinární	veterinární	k2eAgFnSc2d1	veterinární
medicíny	medicína	k1gFnSc2	medicína
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
profesorkou	profesorka	k1gFnSc7	profesorka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vedoucí	vedoucí	k1gFnSc1	vedoucí
Ústavu	ústav	k1gInSc2	ústav
veterinární	veterinární	k2eAgFnSc1d1	veterinární
anatomie	anatomie	k1gFnSc1	anatomie
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
členka	členka	k1gFnSc1	členka
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
myšlenky	myšlenka	k1gFnSc2	myšlenka
zlepšení	zlepšení	k1gNnSc2	zlepšení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
díky	díky	k7c3	díky
výsadbě	výsadba	k1gFnSc3	výsadba
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Hnutí	hnutí	k1gNnSc1	hnutí
zeleného	zelený	k2eAgInSc2d1	zelený
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zasloužilo	zasloužit	k5eAaPmAgNnS	zasloužit
o	o	k7c6	o
vysazení	vysazení	k1gNnSc6	vysazení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
stromků	stromek	k1gInPc2	stromek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Hnutí	hnutí	k1gNnSc2	hnutí
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
v	v	k7c4	v
panafrickou	panafrický	k2eAgFnSc4d1	panafrická
sít	sít	k5eAaImF	sít
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
pomáhající	pomáhající	k2eAgMnPc1d1	pomáhající
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wangari	Wangari	k6eAd1	Wangari
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
rezistentní	rezistentní	k2eAgInSc4d1	rezistentní
boj	boj	k1gInSc4	boj
za	za	k7c4	za
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
udržitelný	udržitelný	k2eAgInSc4d1	udržitelný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
promlouvala	promlouvat	k5eAaImAgFnS	promlouvat
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
OSN	OSN	kA	OSN
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ženských	ženský	k2eAgNnPc2d1	ženské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
zasedala	zasedat	k5eAaImAgFnS	zasedat
v	v	k7c6	v
Komisi	komise	k1gFnSc6	komise
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
i	i	k9	i
v	v	k7c6	v
Komisi	komise	k1gFnSc6	komise
pro	pro	k7c4	pro
globální	globální	k2eAgNnSc4d1	globální
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
s	s	k7c7	s
jednoznačnou	jednoznačný	k2eAgFnSc7d1	jednoznačná
podporou	podpora	k1gFnSc7	podpora
98	[number]	k4	98
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
náměstkyní	náměstkyně	k1gFnSc7	náměstkyně
ministra	ministr	k1gMnSc2	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
její	její	k3xOp3gInSc4	její
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
trvale	trvale	k6eAd1	trvale
udržitelnému	udržitelný	k2eAgInSc3d1	udržitelný
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
,	,	kIx,	,
demokracii	demokracie	k1gFnSc6	demokracie
a	a	k8xC	a
míru	mír	k1gInSc6	mír
<g/>
"	"	kIx"	"
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
Afričankou	Afričanka	k1gFnSc7	Afričanka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
získala	získat	k5eAaPmAgFnS	získat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
a	a	k8xC	a
světovému	světový	k2eAgNnSc3d1	světové
využívání	využívání	k1gNnSc3	využívání
termínu	termín	k1gInSc2	termín
mottainai	mottaina	k1gFnSc2	mottaina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wangari	Wangar	k1gFnSc2	Wangar
Maathaiová	Maathaiový	k2eAgFnSc1d1	Maathaiová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Wangari	Wangar	k1gFnSc2	Wangar
Muta	Mutum	k1gNnSc2	Mutum
Maathai	Maatha	k1gFnSc2	Maatha
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
