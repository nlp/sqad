<s>
William	William	k6eAd1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeatsa	k1gFnPc2	Yeatsa
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Sandymount	Sandymount	k1gInSc1	Sandymount
u	u	k7c2	u
Dublinu	Dublin	k1gInSc2	Dublin
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Roquebrune-Cap-Martin	Roquebrune-Cap-Martin	k1gInSc1	Roquebrune-Cap-Martin
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglicky	anglicky	k6eAd1	anglicky
píšící	píšící	k2eAgMnSc1d1	píšící
irský	irský	k2eAgMnSc1d1	irský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
