<p>
<s>
Painit	Painit	k1gInSc1	Painit
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc4d1	vzácný
minerál	minerál	k1gInSc4	minerál
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
borátů	borát	k1gInPc2	borát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
britským	britský	k2eAgMnSc7d1	britský
mineralogem	mineralog	k1gMnSc7	mineralog
a	a	k8xC	a
obchodníkem	obchodník	k1gMnSc7	obchodník
s	s	k7c7	s
drahokamy	drahokam	k1gInPc1	drahokam
Arthur	Arthura	k1gFnPc2	Arthura
C.	C.	kA	C.
D.	D.	kA	D.
Painem	Pain	k1gInSc7	Pain
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
byl	být	k5eAaImAgInS	být
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
jako	jako	k8xC	jako
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
něm.	něm.	k?	něm.
Painit	Painit	k1gInSc1	Painit
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
zirkon	zirkon	k1gInSc1	zirkon
<g/>
,	,	kIx,	,
bór	bór	k1gInSc1	bór
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
stopové	stopový	k2eAgNnSc1d1	stopové
množství	množství	k1gNnSc1	množství
chromu	chrom	k1gInSc2	chrom
a	a	k8xC	a
vanadu	vanad	k1gInSc2	vanad
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
oranžovo-červenou	oranžovo-červený	k2eAgFnSc4d1	oranžovo-červená
až	až	k8xS	až
hnědavě-červenou	hnědavě-červený	k2eAgFnSc4d1	hnědavě-červený
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
topaz	topaz	k1gInSc4	topaz
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
stopovému	stopový	k2eAgNnSc3d1	stopové
množství	množství	k1gNnSc3	množství
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
krystaly	krystal	k1gInPc1	krystal
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
šestiúhelníkovém	šestiúhelníkový	k2eAgInSc6d1	šestiúhelníkový
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
minerálu	minerál	k1gInSc2	minerál
momentálně	momentálně	k6eAd1	momentálně
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
zatím	zatím	k6eAd1	zatím
nalezen	nalézt	k5eAaBmNgInS	nalézt
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
štěrkových	štěrkový	k2eAgInPc6d1	štěrkový
rozsypech	rozsyp	k1gInPc6	rozsyp
bohatých	bohatý	k2eAgInPc6d1	bohatý
na	na	k7c4	na
bór	bór	k1gInSc4	bór
a	a	k8xC	a
zirkon	zirkon	k1gInSc4	zirkon
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
drahokamy	drahokam	k1gInPc7	drahokam
podobného	podobný	k2eAgNnSc2d1	podobné
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgFnPc1d1	známa
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc1	tři
malé	malý	k2eAgInPc1d1	malý
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
časem	časem	k6eAd1	časem
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
narostl	narůst	k5eAaPmAgInS	narůst
na	na	k7c4	na
25	[number]	k4	25
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
vzorky	vzorek	k1gInPc1	vzorek
painitu	painit	k1gInSc2	painit
nalezeny	nalezen	k2eAgInPc1d1	nalezen
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Barmě	Barma	k1gFnSc6	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInPc1d1	další
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
přinesou	přinést	k5eAaPmIp3nP	přinést
další	další	k2eAgInPc1d1	další
nové	nový	k2eAgInPc1d1	nový
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mogok	Mogok	k1gInSc1	Mogok
určil	určit	k5eAaPmAgInS	určit
několik	několik	k4yIc4	několik
nových	nový	k2eAgNnPc2d1	nové
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
painitu	painit	k1gInSc2	painit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
důsledně	důsledně	k6eAd1	důsledně
prozkoumány	prozkoumán	k2eAgFnPc1d1	prozkoumána
a	a	k8xC	a
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
známých	známý	k2eAgInPc2d1	známý
vzorků	vzorek	k1gInPc2	vzorek
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
kamenů	kámen	k1gInPc2	kámen
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c7	mezi
British	Britisha	k1gFnPc2	Britisha
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
<g/>
,	,	kIx,	,
Gemological	Gemological	k1gMnPc7	Gemological
Institute	institut	k1gInSc5	institut
of	of	k?	of
America	Americum	k1gNnSc2	Americum
<g/>
,	,	kIx,	,
California	Californium	k1gNnSc2	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc4	technolog
a	a	k8xC	a
GRS	GRS	kA	GRS
Gem	gem	k1gInSc4	gem
Research	Researcha	k1gFnPc2	Researcha
Laboratory	Laborator	k1gInPc1	Laborator
v	v	k7c4	v
Lucernu	lucerna	k1gFnSc4	lucerna
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Painit	Painit	k1gInSc1	Painit
se	se	k3xPyFc4	se
brousí	brousit	k5eAaImIp3nS	brousit
jako	jako	k9	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
jeho	jeho	k3xOp3gFnSc1	jeho
tržní	tržní	k2eAgFnSc1d1	tržní
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
50	[number]	k4	50
000	[number]	k4	000
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
karát	karát	k1gInSc4	karát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Painite	Painit	k1gInSc5	Painit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
painite	painit	k1gInSc5	painit
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaImF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Painite	Painit	k1gInSc5	Painit
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
