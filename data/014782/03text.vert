<s>
Angel	angel	k1gMnSc1
Station	station	k1gInSc4
</s>
<s>
Angel	angel	k1gMnSc1
StationInterpretManfred	StationInterpretManfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
BandDruh	BandDruha	k1gFnPc2
albaStudiové	albaStudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1979	#num#	k4
<g/>
Nahránosrpen	Nahránosrpen	k2eAgInSc4d1
1978	#num#	k4
<g/>
-leden	-ledna	k1gFnPc2
1979	#num#	k4
<g/>
Žánryprogresivní	Žánryprogresivní	k2eAgInSc1d1
rock	rock	k1gInSc1
<g/>
,	,	kIx,
hard	hard	k1gInSc1
rockDélka	rockDélka	k1gFnSc1
<g/>
46	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
JazykangličtinaVydavatelstvíBronze	JazykangličtinaVydavatelstvíBronze	k1gFnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Warner	Warner	k1gMnSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
US	US	kA
<g/>
)	)	kIx)
<g/>
ProducentiAnthony	ProducentiAnthon	k1gInPc1
Moore	Moor	k1gInSc5
<g/>
,	,	kIx,
Manfred	Manfred	k1gInSc4
MannProfesionální	MannProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
Band	banda	k1gFnPc2
chronologicky	chronologicky	k6eAd1
</s>
<s>
Watch	Watch	k1gInSc1
<g/>
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Angel	angel	k1gMnSc1
Station	station	k1gInSc1
<g/>
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Chance	Chanec	k1gInPc1
<g/>
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Angel	angel	k1gMnSc1
Station	station	k1gInSc4
je	být	k5eAaImIp3nS
deváté	devátý	k4xOgNnSc4
studiové	studiový	k2eAgNnSc4d1
album	album	k1gNnSc4
skupiny	skupina	k1gFnSc2
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
Band	banda	k1gFnPc2
<g/>
,	,	kIx,
vydané	vydaný	k2eAgFnPc4d1
v	v	k7c6
březnu	březen	k1gInSc6
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
produkovali	produkovat	k5eAaImAgMnP
Anthony	Anthon	k1gInPc1
Moore	Moor	k1gInSc5
a	a	k8xC
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Strana	strana	k1gFnSc1
1	#num#	k4
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevAutor	NázevAutor	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Kill	Kill	k1gInSc1
It	It	k1gMnSc1
Carol	Carola	k1gFnPc2
<g/>
“	“	k?
Heron	Heron	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
You	You	k1gMnSc1
Angel	angel	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
Dylan	Dylan	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Hollywood	Hollywood	k1gInSc1
Town	Town	k1gMnSc1
<g/>
“	“	k?
Schock	Schock	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
"	"	kIx"
<g/>
Belle	bell	k1gInSc5
<g/>
"	"	kIx"
of	of	k?
the	the	k?
Earth	Earth	k1gMnSc1
<g/>
“	“	k?
Mann	Mann	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Platform	Platform	k1gInSc1
End	End	k1gMnSc1
<g/>
“	“	k?
Mann	Mann	k1gMnSc1
<g/>
,	,	kIx,
Britton	Britton	k1gInSc1
<g/>
,	,	kIx,
King	King	k1gMnSc1
<g/>
,	,	kIx,
Waller	Waller	k1gMnSc1
<g/>
,	,	kIx,
Thompson	Thompson	k1gMnSc1
<g/>
,	,	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
Neill	Neill	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
</s>
<s>
Strana	strana	k1gFnSc1
2	#num#	k4
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevAutor	NázevAutor	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Angels	Angels	k1gInSc1
At	At	k1gMnSc1
My	my	k3xPp1nPc1
Gate	Gate	k1gNnPc7
<g/>
“	“	k?
Mann	Mann	k1gMnSc1
<g/>
,	,	kIx,
Martinez	Martinez	k1gMnSc1
<g/>
,	,	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
Neill	Neill	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
You	You	k1gFnSc1
Are	ar	k1gInSc5
-	-	kIx~
I	i	k9
Am	Am	k1gMnSc1
<g/>
“	“	k?
Mann	Mann	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Waiting	Waiting	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Rain	Rain	k1gMnSc1
<g/>
“	“	k?
Falcon	Falcon	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Resurrection	Resurrection	k1gInSc1
<g/>
“	“	k?
Mann	Mann	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
</s>
<s>
Bonusy	bonus	k1gInPc1
na	na	k7c6
reedici	reedice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevAutor	NázevAutor	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Kill	Kill	k1gInSc1
It	It	k1gMnSc1
Carol	Carola	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
singlová	singlový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Heron	Heron	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
You	You	k1gMnSc1
Angel	angel	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
singlová	singlový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Dylan	Dylan	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
</s>
<s>
Sestava	sestava	k1gFnSc1
</s>
<s>
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
–	–	k?
klávesy	klávesa	k1gFnSc2
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc4
</s>
<s>
Geoff	Geoff	k1gInSc1
Britton	Britton	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
Pat	pat	k1gInSc1
King	King	k1gMnSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Steve	Steve	k1gMnSc1
Waller	Waller	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
v	v	k7c6
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Kill	Kill	k1gInSc1
it	it	k?
Carol	Carol	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Angelz	Angelz	k1gMnSc1
At	At	k1gMnSc1
My	my	k3xPp1nPc1
Gate	Gate	k1gFnSc5
<g/>
“	“	k?
</s>
<s>
Chris	Chris	k1gInSc1
Thompson	Thompson	k1gInSc1
–	–	k?
zpěv	zpěv	k1gInSc1
v	v	k7c6
„	„	k?
<g/>
You	You	k1gMnSc1
Angel	angel	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Hollywood	Hollywood	k1gInSc1
Town	Town	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
"	"	kIx"
<g/>
Belle	bell	k1gInSc5
<g/>
"	"	kIx"
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Waiting	Waiting	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Rain	Rain	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
&	&	k?
</s>
<s>
Jimmy	Jimm	k1gInPc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Neill	Neill	k1gMnSc1
–	–	k?
rytmická	rytmický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
</s>
<s>
Dyan	Dyan	k1gInSc1
Birch	Birch	k1gInSc1
–	–	k?
doprovodný	doprovodný	k2eAgInSc1d1
zpěv	zpěv	k1gInSc1
</s>
<s>
Anthony	Anthona	k1gFnPc1
Moore	Moor	k1gInSc5
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
sekvencer	sekvencer	k1gInSc1
<g/>
,	,	kIx,
syntezátor	syntezátor	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Recenze	recenze	k1gFnSc1
na	na	k7c6
Allmusic	Allmusic	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
Band	banda	k1gFnPc2
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
•	•	k?
Mick	Mick	k1gInSc1
Rogers	Rogers	k1gInSc1
•	•	k?
Robert	Robert	k1gMnSc1
Hart	Hart	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Lingwood	Lingwooda	k1gFnPc2
•	•	k?
Steve	Steve	k1gMnSc1
KinchChris	KinchChris	k1gFnSc2
Thompson	Thompson	k1gMnSc1
•	•	k?
Steve	Steve	k1gMnSc1
Waller	Waller	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Slade	slad	k1gInSc5
•	•	k?
Colin	Colin	k1gInSc4
Pattenden	Pattendna	k1gFnPc2
•	•	k?
Pat	pata	k1gFnPc2
King	King	k1gMnSc1
•	•	k?
Clive	Cliev	k1gFnSc2
Bunker	Bunker	k1gMnSc1
•	•	k?
Dave	Dav	k1gInSc5
Flett	Flett	k2eAgInSc1d1
•	•	k?
Matt	Matt	k2eAgInSc1d1
Irving	Irving	k1gInSc1
•	•	k?
Shona	Shon	k1gInSc2
Laing	Laing	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Marcangelo	Marcangela	k1gFnSc5
•	•	k?
Geoff	Geoff	k1gInSc1
Britton	Britton	k1gInSc1
•	•	k?
Noel	Noel	k1gMnSc1
McCalla	McCalla	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Trotter	Trotter	k1gMnSc1
•	•	k?
Pete	Pete	k1gInSc1
May	May	k1gMnSc1
•	•	k?
Geoff	Geoff	k1gMnSc1
Dunn	Dunn	k1gMnSc1
•	•	k?
Jimmy	Jimma	k1gFnSc2
Copley	Coplea	k1gFnSc2
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
Band	banda	k1gFnPc2
•	•	k?
Glorified	Glorified	k1gMnSc1
Magnified	Magnified	k1gMnSc1
•	•	k?
Messin	Messin	k1gMnSc1
<g/>
'	'	kIx"
•	•	k?
Solar	Solar	k1gMnSc1
Fire	Fir	k1gFnSc2
•	•	k?
The	The	k1gMnSc1
Good	Good	k1gMnSc1
Earth	Earth	k1gMnSc1
•	•	k?
Nightingales	Nightingales	k1gMnSc1
and	and	k?
Bombers	Bombers	k1gInSc1
•	•	k?
The	The	k1gFnSc1
Roaring	Roaring	k1gInSc1
Silence	silenka	k1gFnSc3
•	•	k?
Watch	Watch	k1gMnSc1
•	•	k?
Angel	angel	k1gMnSc1
Station	station	k1gInSc1
•	•	k?
Chance	Chanec	k1gInSc2
•	•	k?
Somewhere	Somewher	k1gInSc5
in	in	k?
Afrika	Afrika	k1gFnSc1
•	•	k?
Criminal	Criminal	k1gMnSc1
Tango	tango	k1gNnSc1
•	•	k?
Masque	Masqu	k1gInSc2
•	•	k?
Plains	Plains	k1gInSc1
Music	Music	k1gMnSc1
•	•	k?
Soft	Soft	k?
Vengeance	Vengeanec	k1gInSc2
•	•	k?
2006	#num#	k4
Koncertní	koncertní	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Budapest	Budapest	k1gMnSc1
Live	Liv	k1gFnSc2
•	•	k?
Mann	Mann	k1gMnSc1
Alive	Aliev	k1gFnSc2
Kompilační	kompilační	k2eAgFnSc2d1
alba	album	k1gNnPc4
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
Band	banda	k1gFnPc2
Re-Mastered	Re-Mastered	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
Manfred	Manfred	k1gMnSc1
Mann	Mann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earth	Earth	k1gInSc1
Band	banda	k1gFnPc2
Re-Mastered	Re-Mastered	k1gInSc1
Volume	volum	k1gInSc5
II	II	kA
•	•	k?
Odds	Odds	k1gInSc1
&	&	k?
Sods	Sods	k1gInSc1
-	-	kIx~
Mis-takes	Mis-takes	k1gInSc1
&	&	k?
Out-takes	Out-takes	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
