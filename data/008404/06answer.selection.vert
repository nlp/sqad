<s>
Rutherforův	Rutherforův	k2eAgInSc1d1	Rutherforův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
též	též	k9	též
planetární	planetární	k2eAgInSc1d1	planetární
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
experimentů	experiment	k1gInPc2	experiment
Geigera	Geigero	k1gNnSc2	Geigero
a	a	k8xC	a
Marsdena	Marsdeno	k1gNnSc2	Marsdeno
<g/>
.	.	kIx.	.
</s>
