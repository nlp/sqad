<p>
<s>
Rutherforův	Rutherforův	k2eAgInSc1d1	Rutherforův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
též	též	k9	též
planetární	planetární	k2eAgInSc1d1	planetární
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
experimentů	experiment	k1gInPc2	experiment
Geigera	Geigero	k1gNnSc2	Geigero
a	a	k8xC	a
Marsdena	Marsdeno	k1gNnSc2	Marsdeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
modelu	model	k1gInSc6	model
je	být	k5eAaImIp3nS	být
atom	atom	k1gInSc1	atom
složen	složen	k2eAgInSc1d1	složen
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
malého	malý	k2eAgNnSc2d1	malé
hmotného	hmotný	k2eAgNnSc2d1	hmotné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
nějž	jenž	k3xRgInSc2	jenž
obíhají	obíhat	k5eAaImIp3nP	obíhat
elektrony	elektron	k1gInPc4	elektron
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
cca	cca	kA	cca
10000	[number]	k4	10000
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Experiment	experiment	k1gInSc1	experiment
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
prováděli	provádět	k5eAaImAgMnP	provádět
Hans	Hans	k1gMnSc1	Hans
Geiger	Geiger	k1gMnSc1	Geiger
a	a	k8xC	a
Ernest	Ernest	k1gMnSc1	Ernest
Marsden	Marsdna	k1gFnPc2	Marsdna
rozptylové	rozptylový	k2eAgInPc1d1	rozptylový
experimenty	experiment	k1gInPc1	experiment
částic	částice	k1gFnPc2	částice
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
ozařovali	ozařovat	k5eAaImAgMnP	ozařovat
alfa	alfa	k1gNnSc1	alfa
částicemi	částice	k1gFnPc7	částice
zlatou	zlatá	k1gFnSc7	zlatá
a	a	k8xC	a
platinovou	platinový	k2eAgFnSc4d1	platinová
fólii	fólie	k1gFnSc4	fólie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
tloušťka	tloušťka	k1gFnSc1	tloušťka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10000	[number]	k4	10000
atomových	atomový	k2eAgFnPc2d1	atomová
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
experimentech	experiment	k1gInPc6	experiment
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
alfa	alfa	k1gNnSc2	alfa
částic	částice	k1gFnPc2	částice
není	být	k5eNaImIp3nS	být
atomy	atom	k1gInPc4	atom
fólie	fólie	k1gFnSc2	fólie
příliš	příliš	k6eAd1	příliš
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
a	a	k8xC	a
že	že	k8xS	že
střední	střední	k2eAgFnSc1d1	střední
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
směru	směr	k1gInSc2	směr
pohybu	pohyb	k1gInSc2	pohyb
alfa	alfa	k1gNnSc2	alfa
částic	částice	k1gFnPc2	částice
způsobená	způsobený	k2eAgFnSc1d1	způsobená
fólií	fólie	k1gFnSc7	fólie
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
si	se	k3xPyFc3	se
Geiger	Geiger	k1gInSc4	Geiger
a	a	k8xC	a
Marsden	Marsdna	k1gFnPc2	Marsdna
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
alfa	alfa	k1gNnSc2	alfa
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1	[number]	k4	1
z	z	k7c2	z
20000	[number]	k4	20000
pro	pro	k7c4	pro
zlatou	zlatý	k2eAgFnSc4d1	zlatá
fólii	fólie	k1gFnSc4	fólie
<g/>
)	)	kIx)	)
neprošlo	projít	k5eNaPmAgNnS	projít
fólií	fólie	k1gFnPc2	fólie
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
o	o	k7c4	o
úhel	úhel	k1gInSc4	úhel
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poznatek	poznatek	k1gInSc1	poznatek
byl	být	k5eAaImAgInS	být
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgFnSc4d2	pozdější
Rutherfordovu	Rutherfordův	k2eAgFnSc4d1	Rutherfordova
interpretaci	interpretace	k1gFnSc4	interpretace
experimentu	experiment	k1gInSc2	experiment
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
planetárního	planetární	k2eAgInSc2d1	planetární
modelu	model	k1gInSc2	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Analýza	analýza	k1gFnSc1	analýza
experimentu	experiment	k1gInSc2	experiment
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
rozptyl	rozptyl	k1gInSc1	rozptyl
===	===	k?	===
</s>
</p>
<p>
<s>
Rutherford	Rutherford	k1gMnSc1	Rutherford
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
alfa	alfa	k1gNnSc1	alfa
částice	částice	k1gFnSc2	částice
mající	mající	k2eAgInSc1d1	mající
náboj	náboj	k1gInSc1	náboj
+2	+2	k4	+2
<g/>
e	e	k0	e
interagují	interagovat	k5eAaPmIp3nP	interagovat
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
fólie	fólie	k1gFnSc2	fólie
pouze	pouze	k6eAd1	pouze
elektricky	elektricky	k6eAd1	elektricky
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Coulombovým	Coulombův	k2eAgInSc7d1	Coulombův
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
předpokladu	předpoklad	k1gInSc2	předpoklad
odvodil	odvodit	k5eAaPmAgMnS	odvodit
vzorec	vzorec	k1gInSc4	vzorec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
nalétávajících	nalétávající	k2eAgNnPc2d1	nalétávající
alfa	alfa	k1gNnPc2	alfa
částic	částice	k1gFnPc2	částice
určí	určit	k5eAaPmIp3nS	určit
počet	počet	k1gInSc1	počet
alfa	alfa	k1gNnSc1	alfa
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
atom	atom	k1gInSc4	atom
fólie	fólie	k1gFnSc2	fólie
odchýlí	odchýlit	k5eAaPmIp3nS	odchýlit
o	o	k7c4	o
úhel	úhel	k1gInSc4	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
θ	θ	k?	θ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
theta	theto	k1gNnSc2	theto
}	}	kIx)	}
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
tvar	tvar	k1gInSc4	tvar
tohoto	tento	k3xDgInSc2	tento
vzorce	vzorec	k1gInSc2	vzorec
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
veličinu	veličina	k1gFnSc4	veličina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
srážkový	srážkový	k2eAgInSc1d1	srážkový
průřez	průřez	k1gInSc1	průřez
Rutherfordova	Rutherfordův	k2eAgInSc2d1	Rutherfordův
rozptylu	rozptyl	k1gInSc2	rozptyl
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
θ	θ	k?	θ
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
theta	theta	k1gFnSc1	theta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
Z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
/	/	kIx~	/
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
coulombovský	coulombovský	k2eAgInSc4d1	coulombovský
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
nalétávající	nalétávající	k2eAgFnSc2d1	nalétávající
částice	částice	k1gFnSc2	částice
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hybnost	hybnost	k1gFnSc1	hybnost
částice	částice	k1gFnSc2	částice
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
θ	θ	k?	θ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
theta	theto	k1gNnSc2	theto
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
rozptylu	rozptyl	k1gInSc2	rozptyl
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Omega	omega	k1gNnPc3	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prostorový	prostorový	k2eAgInSc4d1	prostorový
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koincidence	koincidence	k1gFnSc2	koincidence
úspěchu	úspěch	k1gInSc2	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
objevení	objevení	k1gNnSc3	objevení
malého	malý	k2eAgNnSc2d1	malé
hmotného	hmotný	k2eAgNnSc2d1	hmotné
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
správné	správný	k2eAgFnSc6d1	správná
interpretaci	interpretace	k1gFnSc6	interpretace
rozptylových	rozptylový	k2eAgInPc2d1	rozptylový
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
přispěly	přispět	k5eAaPmAgInP	přispět
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
následující	následující	k2eAgFnPc4d1	následující
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rutherford	Rutherford	k1gMnSc1	Rutherford
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Coulombův	Coulombův	k2eAgInSc1d1	Coulombův
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
ověřen	ověřit	k5eAaPmNgInS	ověřit
pro	pro	k7c4	pro
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
platný	platný	k2eAgInSc1d1	platný
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
atomárních	atomární	k2eAgFnPc2d1	atomární
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
odvozený	odvozený	k2eAgMnSc1d1	odvozený
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
dává	dávat	k5eAaImIp3nS	dávat
stejný	stejný	k2eAgInSc1d1	stejný
vzorec	vzorec	k1gInSc1	vzorec
jako	jako	k8xS	jako
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
vzorec	vzorec	k1gInSc1	vzorec
odvozený	odvozený	k2eAgInSc1d1	odvozený
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
<g/>
Kdyby	kdyby	k9	kdyby
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
skutečností	skutečnost	k1gFnPc2	skutečnost
neplatila	platit	k5eNaImAgNnP	platit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
interpretace	interpretace	k1gFnSc1	interpretace
Geigerových	Geigerová	k1gFnPc2	Geigerová
a	a	k8xC	a
Marsdenových	Marsdenův	k2eAgInPc2d1	Marsdenův
experimentů	experiment	k1gInPc2	experiment
byla	být	k5eAaImAgFnS	být
významně	významně	k6eAd1	významně
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
Rutherfordova	Rutherfordův	k2eAgInSc2d1	Rutherfordův
modelu	model	k1gInSc2	model
je	být	k5eAaImIp3nS	být
stabilita	stabilita	k1gFnSc1	stabilita
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
elektrony	elektron	k1gInPc1	elektron
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
kolem	kolem	k7c2	kolem
hmotného	hmotný	k2eAgNnSc2d1	hmotné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
planety	planeta	k1gFnSc2	planeta
kolem	kolem	k7c2	kolem
centra	centrum	k1gNnSc2	centrum
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
dostředivě	dostředivě	k6eAd1	dostředivě
urychlovány	urychlován	k2eAgFnPc1d1	urychlována
a	a	k8xC	a
podle	podle	k7c2	podle
klasické	klasický	k2eAgFnSc2d1	klasická
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
teorie	teorie	k1gFnSc2	teorie
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařování	vyzařování	k1gNnSc1	vyzařování
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
snižovat	snižovat	k5eAaImF	snižovat
energii	energie	k1gFnSc4	energie
elektronu	elektron	k1gInSc2	elektron
a	a	k8xC	a
tedy	tedy	k9	tedy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
elektron	elektron	k1gInSc1	elektron
měl	mít	k5eAaImAgInS	mít
postupně	postupně	k6eAd1	postupně
přibližovat	přibližovat	k5eAaImF	přibližovat
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
atomu	atom	k1gInSc2	atom
až	až	k8xS	až
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
spadnout	spadnout	k5eAaPmF	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
by	by	kYmCp3nS	by
stačilo	stačit	k5eAaBmAgNnS	stačit
pouhých	pouhý	k2eAgInPc2d1	pouhý
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
16	[number]	k4	16
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
elektron	elektron	k1gInSc1	elektron
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
spadl	spadnout	k5eAaPmAgMnS	spadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
experimentu	experiment	k1gInSc2	experiment
ale	ale	k8xC	ale
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
až	až	k9	až
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
</s>
</p>
<p>
<s>
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
rozptyl	rozptyl	k1gInSc1	rozptyl
</s>
</p>
<p>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
</s>
</p>
<p>
<s>
Kvantově-mechanický	Kvantověechanický	k2eAgInSc1d1	Kvantově-mechanický
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
</s>
</p>
