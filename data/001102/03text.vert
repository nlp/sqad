<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
suverénně	suverénně	k6eAd1	suverénně
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc4	ski
i	i	k8xC	i
celkové	celkový	k2eAgNnSc4d1	celkové
pořadí	pořadí	k1gNnSc4	pořadí
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
triumf	triumf	k1gInSc4	triumf
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
pak	pak	k6eAd1	pak
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
i	i	k9	i
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ZOH	ZOH	kA	ZOH
2006	[number]	k4	2006
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
a	a	k8xC	a
na	na	k7c4	na
MS	MS	kA	MS
2009	[number]	k4	2009
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mistrem	mistr	k1gMnSc7	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
skiatlonu	skiatlon	k1gInSc2	skiatlon
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
cyklistice	cyklistika	k1gFnSc6	cyklistika
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
startoval	startovat	k5eAaBmAgInS	startovat
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
MTB	MTB	kA	MTB
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
cross	cross	k6eAd1	cross
country	country	k2eAgMnPc2d1	country
juniorů	junior	k1gMnPc2	junior
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
bude	být	k5eAaImBp3nS	být
závodit	závodit	k5eAaImF	závodit
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
nebo	nebo	k8xC	nebo
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Bauerová	Bauerová	k1gFnSc1	Bauerová
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
lyžařka	lyžařka	k1gFnSc1	lyžařka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
Matyáše	Matyáš	k1gMnSc4	Matyáš
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Anetu	Aneta	k1gFnSc4	Aneta
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
je	být	k5eAaImIp3nS	být
zetěm	zeť	k1gMnSc7	zeť
bývalé	bývalý	k2eAgFnSc2d1	bývalá
běžkyně	běžkyně	k1gFnSc2	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Heleny	Helena	k1gFnSc2	Helena
Balatkové-Šikolové	Balatkové-Šikolový	k2eAgFnSc2d1	Balatkové-Šikolový
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
na	na	k7c6	na
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Sapporu	Sappor	k1gInSc6	Sappor
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Božím	boží	k2eAgInSc6d1	boží
Daru	dar	k1gInSc6	dar
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sportem	sport	k1gInSc7	sport
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
TJ	tj	kA	tj
Škoda	škoda	k1gFnSc1	škoda
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
LK	LK	kA	LK
Mattoni	Mattoň	k1gFnSc6	Mattoň
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgMnS	začít
trénovat	trénovat	k5eAaImF	trénovat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Petrásek	Petrásek	k1gMnSc1	Petrásek
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
trenér	trenér	k1gMnSc1	trenér
reprezentačního	reprezentační	k2eAgInSc2d1	reprezentační
týmu	tým	k1gInSc2	tým
běžců	běžec	k1gMnPc2	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
poprvé	poprvé	k6eAd1	poprvé
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
na	na	k7c6	na
republikových	republikový	k2eAgInPc6d1	republikový
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
poději	podě	k6eAd2	podě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
juniorským	juniorský	k2eAgMnSc7d1	juniorský
mistrem	mistr	k1gMnSc7	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nominoval	nominovat	k5eAaBmAgInS	nominovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
juniorské	juniorský	k2eAgFnPc4d1	juniorská
MS	MS	kA	MS
v	v	k7c6	v
Asiagu	Asiag	k1gInSc6	Asiag
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gInSc7	jeho
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výsledkem	výsledek	k1gInSc7	výsledek
17	[number]	k4	17
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
získal	získat	k5eAaPmAgInS	získat
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
juniorské	juniorský	k2eAgInPc4d1	juniorský
republikové	republikový	k2eAgInPc4d1	republikový
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
MS	MS	kA	MS
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Canmore	Canmor	k1gInSc5	Canmor
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
také	také	k9	také
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
MS	MS	kA	MS
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
Trondheimu	Trondheim	k1gInSc6	Trondheim
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
47	[number]	k4	47
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
závodu	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
však	však	k9	však
pomohl	pomoct	k5eAaPmAgInS	pomoct
k	k	k7c3	k
osmému	osmý	k4xOgNnSc3	osmý
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
<s>
Začátky	začátek	k1gInPc1	začátek
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
Do	do	k7c2	do
závodu	závod	k1gInSc2	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Val	val	k1gInSc1	val
di	di	k?	di
Fiemme	Fiemme	k1gMnPc3	Fiemme
účastnil	účastnit	k5eAaImAgInS	účastnit
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
Doběhl	doběhnout	k5eAaPmAgInS	doběhnout
na	na	k7c6	na
nebodovaném	bodovaný	k2eNgMnSc6d1	nebodovaný
54	[number]	k4	54
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
zimní	zimní	k2eAgFnSc2d1	zimní
sezóny	sezóna	k1gFnSc2	sezóna
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
závodu	závod	k1gInSc2	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
však	však	k9	však
neskončil	skončit	k5eNaPmAgInS	skončit
na	na	k7c6	na
bodovaných	bodovaný	k2eAgFnPc6d1	bodovaná
příčkách	příčka	k1gFnPc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěšným	úspěšný	k2eAgInPc3d1	úspěšný
startům	start	k1gInPc3	start
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
FIS	fis	k1gNnSc2	fis
nižší	nízký	k2eAgFnSc2d2	nižší
kategorie	kategorie	k1gFnSc2	kategorie
byl	být	k5eAaImAgInS	být
však	však	k9	však
nominován	nominován	k2eAgInSc1d1	nominován
na	na	k7c4	na
Zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
1998	[number]	k4	1998
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
Naganu	Nagano	k1gNnSc6	Nagano
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1998	[number]	k4	1998
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
Při	při	k7c6	při
prvním	první	k4xOgInSc6	první
startu	start	k1gInSc6	start
obsadil	obsadit	k5eAaPmAgMnS	obsadit
33	[number]	k4	33
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
na	na	k7c4	na
45	[number]	k4	45
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
startovalo	startovat	k5eAaBmAgNnS	startovat
s	s	k7c7	s
odstupy	odstup	k1gInPc7	odstup
dle	dle	k7c2	dle
výsledků	výsledek	k1gInPc2	výsledek
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
polepšil	polepšit	k5eAaPmAgMnS	polepšit
na	na	k7c4	na
32	[number]	k4	32
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
štafetou	štafeta	k1gFnSc7	štafeta
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Naganem	Nagano	k1gNnSc7	Nagano
a	a	k8xC	a
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc2	Lake
City	city	k1gNnSc1	city
Prvního	první	k4xOgNnSc2	první
bodovaného	bodovaný	k2eAgNnSc2d1	bodované
umístění	umístění	k1gNnSc2	umístění
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
1999	[number]	k4	1999
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Ramsau	Ramsaus	k1gInSc6	Ramsaus
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dokázal	dokázat	k5eAaPmAgMnS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
dvacítky	dvacítka	k1gFnSc2	dvacítka
-	-	kIx~	-
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
polepšil	polepšit	k5eAaPmAgMnS	polepšit
o	o	k7c4	o
několik	několik	k4yIc4	několik
příček	příčka	k1gFnPc2	příčka
na	na	k7c4	na
solidní	solidní	k2eAgFnPc4d1	solidní
13	[number]	k4	13
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2001	[number]	k4	2001
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Lahti	Lahti	k1gNnSc6	Lahti
si	se	k3xPyFc3	se
pohoršil	pohoršit	k5eAaPmAgMnS	pohoršit
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
stíhačce	stíhačka	k1gFnSc6	stíhačka
si	se	k3xPyFc3	se
polepšil	polepšit	k5eAaPmAgMnS	polepšit
jen	jen	k9	jen
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
skončil	skončit	k5eAaPmAgInS	skončit
26	[number]	k4	26
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
volně	volně	k6eAd1	volně
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
dlouho	dlouho	k6eAd1	dlouho
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
až	až	k9	až
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Kuopiu	Kuopium	k1gNnSc6	Kuopium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
2002	[number]	k4	2002
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
Na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2002	[number]	k4	2002
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gMnSc2	Lak
City	City	k1gFnSc2	City
zahájil	zahájit	k5eAaPmAgInS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
6	[number]	k4	6
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
volně	volně	k6eAd1	volně
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
(	(	kIx(	(
<g/>
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
sice	sice	k8xC	sice
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vítězný	vítězný	k2eAgMnSc1d1	vítězný
Johann	Johann	k1gMnSc1	Johann
Mühlegg	Mühlegg	k1gMnSc1	Mühlegg
byl	být	k5eAaImAgMnS	být
poději	podě	k6eAd2	podě
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
kvůli	kvůli	k7c3	kvůli
dopingu	doping	k1gInSc3	doping
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
20	[number]	k4	20
km	km	kA	km
(	(	kIx(	(
<g/>
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
+	+	kIx~	+
10	[number]	k4	10
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
)	)	kIx)	)
skončil	skončit	k5eAaPmAgInS	skončit
dvanáctý	dvanáctý	k4xOgMnSc1	dvanáctý
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
diskvalifikaci	diskvalifikace	k1gFnSc6	diskvalifikace
vítězného	vítězný	k2eAgMnSc2d1	vítězný
Mühlegga	Mühlegg	k1gMnSc2	Mühlegg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
běžel	běžet	k5eAaImAgMnS	běžet
třetí	třetí	k4xOgInSc4	třetí
úsek	úsek	k1gInSc4	úsek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
nejrychlejšího	rychlý	k2eAgInSc2d3	nejrychlejší
času	čas	k1gInSc2	čas
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
startujících	startující	k2eAgMnPc2d1	startující
<g/>
.	.	kIx.	.
</s>
<s>
Štafeta	štafeta	k1gFnSc1	štafeta
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
sedmá	sedmý	k4xOgFnSc1	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
pak	pak	k6eAd1	pak
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
osmý	osmý	k4xOgMnSc1	osmý
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
pódiová	pódiový	k2eAgNnPc1d1	pódiové
umístění	umístění	k1gNnPc1	umístění
v	v	k7c6	v
SP	SP	kA	SP
Výsledky	výsledek	k1gInPc1	výsledek
ze	z	k7c2	z
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gMnSc2	Lak
City	City	k1gFnSc2	City
jej	on	k3xPp3gNnSc4	on
povzbudily	povzbudit	k5eAaPmAgFnP	povzbudit
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
čtvrtá	čtvrtý	k4xOgNnPc4	čtvrtý
místa	místo	k1gNnPc4	místo
z	z	k7c2	z
individuálních	individuální	k2eAgInPc2d1	individuální
závodů	závod	k1gInPc2	závod
v	v	k7c6	v
Lahti	Lahť	k1gFnSc6	Lahť
a	a	k8xC	a
Falunu	Falun	k1gInSc6	Falun
a	a	k8xC	a
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
dokonce	dokonce	k9	dokonce
první	první	k4xOgNnSc4	první
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
když	když	k8xS	když
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
dojeli	dojet	k5eAaPmAgMnP	dojet
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Lahti	Lahti	k1gNnSc6	Lahti
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Koukalem	Koukal	k1gMnSc7	Koukal
ve	v	k7c6	v
sprintu	sprint	k1gInSc6	sprint
dvojic	dvojice	k1gFnPc2	dvojice
třetí	třetí	k4xOgFnSc3	třetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
stál	stát	k5eAaImAgMnS	stát
již	již	k9	již
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
hned	hned	k6eAd1	hned
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
Kuusamu	Kuusam	k1gInSc6	Kuusam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
doma	doma	k6eAd1	doma
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
dočkal	dočkat	k5eAaPmAgMnS	dočkat
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
vítězství	vítězství	k1gNnSc2	vítězství
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
,	,	kIx,	,
Rakušana	Rakušan	k1gMnSc2	Rakušan
Christiana	Christian	k1gMnSc4	Christian
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
za	za	k7c4	za
sebou	se	k3xPyFc7	se
nechal	nechat	k5eAaPmAgInS	nechat
o	o	k7c4	o
půl	půl	k1xP	půl
minuty	minuta	k1gFnSc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
přidal	přidat	k5eAaPmAgMnS	přidat
další	další	k2eAgNnSc4d1	další
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Lahti	Lahti	k1gNnSc6	Lahti
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
poprvé	poprvé	k6eAd1	poprvé
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
když	když	k8xS	když
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
výborném	výborný	k2eAgNnSc6d1	výborné
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Val	val	k1gInSc4	val
di	di	k?	di
Fiemme	Fiemme	k1gFnSc2	Fiemme
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
podle	podle	k7c2	podle
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
skončil	skončit	k5eAaPmAgInS	skončit
sedmnáctý	sedmnáctý	k4xOgInSc1	sedmnáctý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
dvacátý	dvacátý	k4xOgInSc4	dvacátý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
tyto	tento	k3xDgFnPc4	tento
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgFnP	být
značným	značný	k2eAgNnSc7d1	značné
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gInSc7	jeho
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výsledkem	výsledek	k1gInSc7	výsledek
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
dvě	dva	k4xCgNnPc1	dva
třetí	třetí	k4xOgNnPc1	třetí
místa	místo	k1gNnPc1	místo
z	z	k7c2	z
Oberstdorfu	Oberstdorf	k1gInSc2	Oberstdorf
a	a	k8xC	a
Osla	Oslo	k1gNnSc2	Oslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
si	se	k3xPyFc3	se
také	také	k9	také
pohoršil	pohoršit	k5eAaPmAgMnS	pohoršit
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgMnS	skončit
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
ještě	ještě	k9	ještě
o	o	k7c4	o
příčku	příčka	k1gFnSc4	příčka
níže	nízce	k6eAd2	nízce
(	(	kIx(	(
<g/>
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
závodech	závod	k1gInPc6	závod
vystoupal	vystoupat	k5eAaPmAgInS	vystoupat
na	na	k7c4	na
stupeň	stupeň	k1gInSc4	stupeň
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
si	se	k3xPyFc3	se
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
tratích	trať	k1gFnPc6	trať
v	v	k7c6	v
Pragelatu	Pragelat	k1gInSc6	Pragelat
ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
druhé	druhý	k4xOgMnPc4	druhý
pak	pak	k6eAd1	pak
začátkem	začátek	k1gInSc7	začátek
března	březen	k1gInSc2	březen
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Lahti	Lahti	k1gNnSc6	Lahti
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Oberstdorfu	Oberstdorf	k1gInSc6	Oberstdorf
se	se	k3xPyFc4	se
medaile	medaile	k1gFnSc1	medaile
opět	opět	k6eAd1	opět
nedočkal	dočkat	k5eNaPmAgInS	dočkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
doposud	doposud	k6eAd1	doposud
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výsledkem	výsledek	k1gInSc7	výsledek
na	na	k7c6	na
šampionátech	šampionát	k1gInPc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
skončil	skončit	k5eAaPmAgInS	skončit
34	[number]	k4	34
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
štafetou	štafeta	k1gFnSc7	štafeta
pak	pak	k6eAd1	pak
osmý	osmý	k4xOgMnSc1	osmý
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
sezóna	sezóna	k1gFnSc1	sezóna
byla	být	k5eAaImAgFnS	být
olympijská	olympijský	k2eAgFnSc1d1	olympijská
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
olympijskými	olympijský	k2eAgFnPc7d1	olympijská
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
si	se	k3xPyFc3	se
držel	držet	k5eAaImAgMnS	držet
dobrou	dobrý	k2eAgFnSc4d1	dobrá
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
mezi	mezi	k7c7	mezi
Vánocemi	Vánoce	k1gFnPc7	Vánoce
2005	[number]	k4	2005
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
olympiády	olympiáda	k1gFnSc2	olympiáda
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
dvě	dva	k4xCgNnPc4	dva
druhá	druhý	k4xOgNnPc4	druhý
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
individuálních	individuální	k2eAgInPc6d1	individuální
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
2006	[number]	k4	2006
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
-	-	kIx~	-
konečně	konečně	k6eAd1	konečně
velká	velký	k2eAgFnSc1d1	velká
medaile	medaile	k1gFnSc1	medaile
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2006	[number]	k4	2006
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Turíně	Turín	k1gInSc6	Turín
zahájil	zahájit	k5eAaPmAgInS	zahájit
10	[number]	k4	10
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
trochu	trochu	k6eAd1	trochu
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
zde	zde	k6eAd1	zde
právě	právě	k9	právě
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závod	závod	k1gInSc4	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
na	na	k7c6	na
špici	špice	k1gFnSc6	špice
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
pole	pole	k1gFnPc1	pole
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
roztrhat	roztrhat	k5eAaPmF	roztrhat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
a	a	k8xC	a
ve	v	k7c6	v
finiši	finiš	k1gInSc6	finiš
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
chyběly	chybět	k5eAaImAgInP	chybět
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
si	se	k3xPyFc3	se
však	však	k9	však
vynahradil	vynahradit	k5eAaPmAgMnS	vynahradit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
oblíbené	oblíbený	k2eAgFnSc6d1	oblíbená
trati	trať	k1gFnSc6	trať
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
Rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
Estonec	Estonec	k1gMnSc1	Estonec
Andrus	Andrus	k1gMnSc1	Andrus
Veerpalu	Veerpal	k1gInSc2	Veerpal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vynechal	vynechat	k5eAaPmAgInS	vynechat
úvodní	úvodní	k2eAgInSc4d1	úvodní
skiatlon	skiatlon	k1gInSc4	skiatlon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Bauera	Bauer	k1gMnSc4	Bauer
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
stříbro	stříbro	k1gNnSc1	stříbro
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgFnSc4	první
mužskou	mužský	k2eAgFnSc4d1	mužská
individuální	individuální	k2eAgFnSc4d1	individuální
běžeckou	běžecký	k2eAgFnSc4d1	běžecká
medailí	medaile	k1gFnSc7	medaile
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
(	(	kIx(	(
<g/>
či	či	k8xC	či
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
v	v	k7c6	v
olympijské	olympijský	k2eAgFnSc6d1	olympijská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
skončil	skončit	k5eAaPmAgMnS	skončit
vedoucí	vedoucí	k1gMnSc1	vedoucí
muž	muž	k1gMnSc1	muž
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
Tobias	Tobias	k1gMnSc1	Tobias
Angerer	Angerer	k1gMnSc1	Angerer
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
bronzem	bronz	k1gInSc7	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
medailový	medailový	k2eAgInSc4d1	medailový
závod	závod	k1gInSc4	závod
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jasné	jasný	k2eAgNnSc1d1	jasné
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
až	až	k9	až
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věřit	věřit	k5eAaImF	věřit
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgInS	začít
při	při	k7c6	při
nájezdu	nájezd	k1gInSc6	nájezd
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
prvním	první	k4xOgNnSc6	první
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
jet	jet	k5eAaImF	jet
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
věřil	věřit	k5eAaImAgMnS	věřit
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mi	já	k3xPp1nSc3	já
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
nedojdou	dojít	k5eNaPmIp3nP	dojít
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
dojel	dojet	k5eAaPmAgMnS	dojet
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgMnS	dívat
na	na	k7c6	na
tabuli	tabule	k1gFnSc6	tabule
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jsem	být	k5eAaImIp1nS	být
před	před	k7c7	před
Ročevem	Ročev	k1gInSc7	Ročev
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
zbýval	zbývat	k5eAaImAgInS	zbývat
jenom	jenom	k9	jenom
Angerer	Angerer	k1gInSc1	Angerer
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ten	ten	k3xDgMnSc1	ten
naštěstí	naštěstí	k6eAd1	naštěstí
zůstal	zůstat	k5eAaPmAgMnS	zůstat
za	za	k7c7	za
mnou	já	k3xPp1nSc7	já
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
napínavý	napínavý	k2eAgInSc1d1	napínavý
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Se	s	k7c7	s
štafetou	štafeta	k1gFnSc7	štafeta
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
až	až	k6eAd1	až
devátý	devátý	k4xOgMnSc1	devátý
<g/>
,	,	kIx,	,
když	když	k8xS	když
manažer	manažer	k1gMnSc1	manažer
týmu	tým	k1gInSc2	tým
nahlásil	nahlásit	k5eAaPmAgMnS	nahlásit
omylem	omylem	k6eAd1	omylem
špatnou	špatný	k2eAgFnSc4d1	špatná
sestavu	sestava	k1gFnSc4	sestava
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
Milana	Milan	k1gMnSc2	Milan
Šperla	Šperl	k1gMnSc2	Šperl
musel	muset	k5eAaImAgMnS	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
poslední	poslední	k2eAgInSc4d1	poslední
úsek	úsek	k1gInSc4	úsek
původní	původní	k2eAgMnSc1d1	původní
náhradník	náhradník	k1gMnSc1	náhradník
-	-	kIx~	-
sprinter	sprinter	k1gMnSc1	sprinter
Dušan	Dušan	k1gMnSc1	Dušan
Kožíšek	kožíšek	k1gInSc4	kožíšek
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
běžel	běžet	k5eAaImAgMnS	běžet
druhý	druhý	k4xOgInSc4	druhý
úsek	úsek	k1gInSc4	úsek
a	a	k8xC	a
předával	předávat	k5eAaImAgMnS	předávat
štafetu	štafeta	k1gFnSc4	štafeta
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Magál	Magál	k1gMnSc1	Magál
předával	předávat	k5eAaImAgMnS	předávat
po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
úseku	úsek	k1gInSc6	úsek
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kožíšek	kožíšek	k1gInSc1	kožíšek
se	se	k3xPyFc4	se
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
propadal	propadat	k5eAaPmAgMnS	propadat
a	a	k8xC	a
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
až	až	k9	až
na	na	k7c6	na
deváté	devátý	k4xOgFnSc6	devátý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
závodu	závod	k1gInSc2	závod
dvakrát	dvakrát	k6eAd1	dvakrát
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
Po	po	k7c6	po
olympijské	olympijský	k2eAgFnSc6d1	olympijská
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
způsob	způsob	k1gInSc4	způsob
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
trénoval	trénovat	k5eAaImAgInS	trénovat
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
reprezentanty	reprezentant	k1gMnPc7	reprezentant
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
René	René	k1gFnPc4	René
Sommerfeldtem	Sommerfeldt	k1gInSc7	Sommerfeldt
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
pak	pak	k6eAd1	pak
volil	volit	k5eAaImAgInS	volit
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
začal	začít	k5eAaPmAgMnS	začít
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
místem	místo	k1gNnSc7	místo
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
a	a	k8xC	a
třetím	třetí	k4xOgNnSc7	třetí
místem	místo	k1gNnSc7	místo
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
Gällivare	Gällivar	k1gInSc5	Gällivar
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
jej	on	k3xPp3gNnSc4	on
však	však	k9	však
postihlo	postihnout	k5eAaPmAgNnS	postihnout
silné	silný	k2eAgNnSc1d1	silné
nachlazení	nachlazení	k1gNnSc1	nachlazení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
zánět	zánět	k1gInSc1	zánět
krčních	krční	k2eAgFnPc2d1	krční
mandlí	mandle	k1gFnPc2	mandle
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
léčit	léčit	k5eAaImF	léčit
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
a	a	k8xC	a
na	na	k7c6	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Vynechat	vynechat	k5eAaPmF	vynechat
musel	muset	k5eAaImAgMnS	muset
i	i	k9	i
úvodní	úvodní	k2eAgInSc4d1	úvodní
ročník	ročník	k1gInSc4	ročník
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
Ski	ski	k1gFnPc2	ski
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
a	a	k8xC	a
Bauer	Bauer	k1gMnSc1	Bauer
začal	začít	k5eAaPmAgMnS	začít
znovu	znovu	k6eAd1	znovu
trénovat	trénovat	k5eAaImF	trénovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
znovu	znovu	k6eAd1	znovu
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
opět	opět	k6eAd1	opět
nasadit	nasadit	k5eAaPmF	nasadit
antibiotika	antibiotikum	k1gNnPc4	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
začátkem	začátek	k1gInSc7	začátek
února	únor	k1gInSc2	únor
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Davosu	Davos	k1gInSc6	Davos
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2007	[number]	k4	2007
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Sapporu	Sappor	k1gInSc6	Sappor
se	se	k3xPyFc4	se
však	však	k9	však
připravit	připravit	k5eAaPmF	připravit
stihl	stihnout	k5eAaPmAgMnS	stihnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgInS	skončit
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
,	,	kIx,	,
když	když	k8xS	když
dva	dva	k4xCgInPc4	dva
kilometry	kilometr	k1gInPc4	kilometr
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
zlomil	zlomit	k5eAaPmAgMnS	zlomit
hůlku	hůlka	k1gFnSc4	hůlka
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
kontakt	kontakt	k1gInSc4	kontakt
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
závod	závod	k1gInSc1	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
vynechal	vynechat	k5eAaPmAgInS	vynechat
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
až	až	k6eAd1	až
do	do	k7c2	do
štafety	štafeta	k1gFnSc2	štafeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
osmá	osmý	k4xOgFnSc1	osmý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
se	se	k3xPyFc4	se
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
kilometrů	kilometr	k1gInPc2	kilometr
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
roztrhat	roztrhat	k5eAaPmF	roztrhat
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zúžila	zúžit	k5eAaPmAgFnS	zúžit
na	na	k7c4	na
pět	pět	k4xCc4	pět
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
nejméně	málo	k6eAd3	málo
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
tak	tak	k6eAd1	tak
z	z	k7c2	z
čelní	čelní	k2eAgFnSc2d1	čelní
skupiny	skupina	k1gFnSc2	skupina
poslední	poslední	k2eAgFnSc2d1	poslední
<g/>
,	,	kIx,	,
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
skončil	skončit	k5eAaPmAgInS	skončit
kvůli	kvůli	k7c3	kvůli
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
výpadku	výpadek	k1gInSc3	výpadek
až	až	k9	až
na	na	k7c4	na
37	[number]	k4	37
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
dvě	dva	k4xCgFnPc4	dva
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
odstranění	odstranění	k1gNnSc2	odstranění
krčních	krční	k2eAgFnPc2d1	krční
mandlí	mandle	k1gFnPc2	mandle
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yIgMnPc3	který
promarodil	promarodit	k5eAaImAgInS	promarodit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
závodní	závodní	k2eAgFnSc2d1	závodní
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
pak	pak	k8xC	pak
artroskopii	artroskopie	k1gFnSc6	artroskopie
kolene	kolen	k1gMnSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Vládcem	vládce	k1gMnSc7	vládce
bílé	bílý	k2eAgFnSc2d1	bílá
stopy	stopa	k1gFnSc2	stopa
Po	po	k7c6	po
prodělaných	prodělaný	k2eAgFnPc6d1	prodělaná
operacích	operace	k1gFnPc6	operace
neměl	mít	k5eNaImAgMnS	mít
Bauer	Bauer	k1gMnSc1	Bauer
natrénováno	natrénován	k2eAgNnSc4d1	natrénováno
tolik	tolik	k6eAd1	tolik
jako	jako	k8xS	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
může	moct	k5eAaImIp3nS	moct
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
v	v	k7c6	v
norském	norský	k2eAgMnSc6d1	norský
Beitostø	Beitostø	k1gMnSc6	Beitostø
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
Kuusamu	Kuusam	k1gInSc6	Kuusam
závod	závod	k1gInSc4	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Davosu	Davos	k1gInSc6	Davos
dojel	dojet	k5eAaPmAgMnS	dojet
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
a	a	k8xC	a
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
jej	on	k3xPp3gMnSc4	on
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
Axel	Axel	k1gMnSc1	Axel
Teichmann	Teichmann	k1gMnSc1	Teichmann
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
štafetě	štafeta	k1gFnSc3	štafeta
pomohl	pomoct	k5eAaPmAgMnS	pomoct
k	k	k7c3	k
historicky	historicky	k6eAd1	historicky
prvnímu	první	k4xOgNnSc3	první
vítězství	vítězství	k1gNnSc3	vítězství
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Štafetu	štafeta	k1gFnSc4	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
rozbíhal	rozbíhat	k5eAaImAgMnS	rozbíhat
Martin	Martin	k1gMnSc1	Martin
Jakš	Jakš	k1gMnSc1	Jakš
a	a	k8xC	a
předával	předávat	k5eAaImAgMnS	předávat
Bauerovi	Bauer	k1gMnSc3	Bauer
ve	v	k7c6	v
vedoucí	vedoucí	k1gFnSc6	vedoucí
skupině	skupina	k1gFnSc6	skupina
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
úseku	úsek	k1gInSc6	úsek
držel	držet	k5eAaImAgInS	držet
bezpečně	bezpečně	k6eAd1	bezpečně
v	v	k7c6	v
čelní	čelní	k2eAgFnSc6d1	čelní
skupině	skupina	k1gFnSc6	skupina
a	a	k8xC	a
předával	předávat	k5eAaImAgMnS	předávat
rovněž	rovněž	k9	rovněž
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
úseku	úsek	k1gInSc6	úsek
běžel	běžet	k5eAaImAgMnS	běžet
Milan	Milan	k1gMnSc1	Milan
Šperl	Šperl	k1gMnSc1	Šperl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
předával	předávat	k5eAaImAgMnS	předávat
finišmanovi	finišman	k1gMnSc3	finišman
Martinu	Martin	k1gMnSc3	Martin
Koukalovi	Koukal	k1gMnSc3	Koukal
štafetu	štafeta	k1gFnSc4	štafeta
na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
2,5	[number]	k4	2,5
sekundy	sekunda	k1gFnSc2	sekunda
na	na	k7c4	na
vedoucí	vedoucí	k2eAgMnPc4d1	vedoucí
Italy	Ital	k1gMnPc4	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Koukal	Koukal	k1gMnSc1	Koukal
předvedl	předvést	k5eAaPmAgMnS	předvést
skvělý	skvělý	k2eAgInSc4d1	skvělý
finiš	finiš	k1gInSc4	finiš
<g/>
,	,	kIx,	,
o	o	k7c6	o
českém	český	k2eAgNnSc6d1	české
prvenství	prvenství	k1gNnSc6	prvenství
před	před	k7c4	před
Italy	Ital	k1gMnPc4	Ital
nakonec	nakonec	k6eAd1	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
cílová	cílový	k2eAgFnSc1d1	cílová
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
závody	závod	k1gInPc1	závod
SP	SP	kA	SP
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
Rybinsku	Rybinsko	k1gNnSc6	Rybinsko
vynechal	vynechat	k5eAaPmAgMnS	vynechat
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nekonalo	konat	k5eNaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ani	ani	k8xC	ani
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
pro	pro	k7c4	pro
lyžaře	lyžař	k1gMnPc4	lyžař
hlavní	hlavní	k2eAgInSc4d1	hlavní
vrchol	vrchol	k1gInSc4	vrchol
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
úvod	úvod	k1gInSc1	úvod
Tour	Toura	k1gFnPc2	Toura
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bauer	Bauer	k1gMnSc1	Bauer
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
úvodní	úvodní	k2eAgInSc4d1	úvodní
prolog	prolog	k1gInSc4	prolog
na	na	k7c4	na
4,5	[number]	k4	4,5
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
pak	pak	k6eAd1	pak
doslova	doslova	k6eAd1	doslova
převálcoval	převálcovat	k5eAaPmAgMnS	převálcovat
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
,	,	kIx,	,
když	když	k8xS	když
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
Švéd	Švéd	k1gMnSc1	Švéd
Marcus	Marcus	k1gMnSc1	Marcus
Hellner	Hellner	k1gMnSc1	Hellner
<g/>
,	,	kIx,	,
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
ztratil	ztratit	k5eAaPmAgInS	ztratit
47	[number]	k4	47
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
již	již	k6eAd1	již
nezakrýval	zakrývat	k5eNaImAgMnS	zakrývat
ambice	ambice	k1gFnPc4	ambice
vyhrát	vyhrát	k5eAaPmF	vyhrát
celou	celá	k1gFnSc4	celá
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sprintu	sprint	k1gInSc6	sprint
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
však	však	k9	však
skončil	skončit	k5eAaPmAgMnS	skončit
až	až	k9	až
na	na	k7c4	na
45	[number]	k4	45
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
vedení	vedení	k1gNnSc4	vedení
v	v	k7c4	v
Tour	Tour	k1gInSc4	Tour
přebral	přebrat	k5eAaPmAgMnS	přebrat
Nor	Nor	k1gMnSc1	Nor
Ø	Ø	k?	Ø
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
klesl	klesnout	k5eAaPmAgMnS	klesnout
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
12,1	[number]	k4	12,1
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
však	však	k9	však
Bauer	Bauer	k1gMnSc1	Bauer
opět	opět	k6eAd1	opět
dominoval	dominovat	k5eAaImAgMnS	dominovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
nenašel	najít	k5eNaPmAgMnS	najít
přemožitele	přemožitel	k1gMnPc4	přemožitel
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
intervalovém	intervalový	k2eAgInSc6d1	intervalový
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
Bauerův	Bauerův	k2eAgInSc4d1	Bauerův
náskok	náskok	k1gInSc4	náskok
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
Tour	Toura	k1gFnPc2	Toura
již	již	k9	již
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sprintu	sprint	k1gInSc6	sprint
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Asiagu	Asiag	k1gInSc6	Asiag
sice	sice	k8xC	sice
skončil	skončit	k5eAaPmAgInS	skončit
opět	opět	k6eAd1	opět
až	až	k9	až
na	na	k7c4	na
42	[number]	k4	42
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
však	však	k9	však
držel	držet	k5eAaImAgMnS	držet
vedení	vedení	k1gNnSc4	vedení
s	s	k7c7	s
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
náskokem	náskok	k1gInSc7	náskok
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
17,1	[number]	k4	17,1
minuty	minuta	k1gFnPc4	minuta
na	na	k7c4	na
druhého	druhý	k4xOgMnSc4	druhý
Nora	Nor	k1gMnSc4	Nor
Gjerdalena	Gjerdalen	k2eAgMnSc4d1	Gjerdalen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předposledním	předposlední	k2eAgInSc6d1	předposlední
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
20	[number]	k4	20
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
ve	v	k7c4	v
Val	val	k1gInSc4	val
di	di	k?	di
Fiemme	Fiemme	k1gFnSc2	Fiemme
dojel	dojet	k5eAaPmAgInS	dojet
sedmý	sedmý	k4xOgInSc4	sedmý
jen	jen	k6eAd1	jen
s	s	k7c7	s
několikasekundovým	několikasekundový	k2eAgInSc7d1	několikasekundový
odstupem	odstup	k1gInSc7	odstup
na	na	k7c4	na
vítěze	vítěz	k1gMnSc4	vítěz
a	a	k8xC	a
díky	díky	k7c3	díky
vítězství	vítězství	k1gNnSc3	vítězství
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
bodovaných	bodovaný	k2eAgInPc6d1	bodovaný
sprintech	sprint	k1gInPc6	sprint
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
svůj	svůj	k3xOyFgInSc4	svůj
náskok	náskok	k1gInSc4	náskok
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
Tour	Toura	k1gFnPc2	Toura
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
půlminutu	půlminuta	k1gFnSc4	půlminuta
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
49,8	[number]	k4	49,8
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
volně	volně	k6eAd1	volně
se	s	k7c7	s
stoupáním	stoupání	k1gNnSc7	stoupání
do	do	k7c2	do
sjezdovky	sjezdovka	k1gFnSc2	sjezdovka
v	v	k7c6	v
Alpe	Alpe	k1gFnSc6	Alpe
Cermis	Cermis	k1gFnPc2	Cermis
již	již	k6eAd1	již
své	svůj	k3xOyFgNnSc4	svůj
vedení	vedení	k1gNnSc4	vedení
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
udržel	udržet	k5eAaPmAgInS	udržet
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jej	on	k3xPp3gMnSc4	on
ještě	ještě	k6eAd1	ještě
navýšil	navýšit	k5eAaPmAgMnS	navýšit
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
René	René	k1gMnSc1	René
Sommerfeldt	Sommerfeldt	k1gMnSc1	Sommerfeldt
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
dojel	dojet	k5eAaPmAgMnS	dojet
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
s	s	k7c7	s
propastnou	propastný	k2eAgFnSc7d1	propastná
ztrátou	ztráta	k1gFnSc7	ztráta
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
47,3	[number]	k4	47,3
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
triumfu	triumf	k1gInSc6	triumf
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
hlavní	hlavní	k2eAgFnSc1d1	hlavní
motivace	motivace	k1gFnSc1	motivace
a	a	k8xC	a
jsem	být	k5eAaImIp1nS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
to	ten	k3xDgNnSc1	ten
nevypadalo	vypadat	k5eNaImAgNnS	vypadat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
strašně	strašně	k6eAd1	strašně
unavený	unavený	k2eAgMnSc1d1	unavený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sjezdovce	sjezdovka	k1gFnSc6	sjezdovka
jsem	být	k5eAaImIp1nS	být
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
metrem	metro	k1gNnSc7	metro
bojoval	bojovat	k5eAaImAgInS	bojovat
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
otočit	otočit	k5eAaPmF	otočit
to	ten	k3xDgNnSc1	ten
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c4	v
Tour	Tour	k1gInSc4	Tour
ho	on	k3xPp3gNnSc2	on
také	také	k6eAd1	také
vyneslo	vynést	k5eAaPmAgNnS	vynést
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
celkového	celkový	k2eAgNnSc2d1	celkové
pořadí	pořadí	k1gNnSc2	pořadí
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
celých	celý	k2eAgInPc2d1	celý
301	[number]	k4	301
bodů	bod	k1gInPc2	bod
před	před	k7c7	před
druhým	druhý	k4xOgMnSc7	druhý
Norem	Nor	k1gMnSc7	Nor
Hetlandem	Hetland	k1gInSc7	Hetland
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
závody	závod	k1gInPc1	závod
SP	SP	kA	SP
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
sice	sice	k8xC	sice
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
vynechal	vynechat	k5eAaPmAgMnS	vynechat
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
náskok	náskok	k1gInSc4	náskok
se	se	k3xPyFc4	se
však	však	k9	však
snížil	snížit	k5eAaPmAgInS	snížit
jen	jen	k9	jen
o	o	k7c6	o
necelých	celý	k2eNgInPc6d1	necelý
sto	sto	k4xCgNnSc1	sto
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
SP	SP	kA	SP
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závod	závod	k1gInSc4	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
v	v	k7c6	v
estonském	estonský	k2eAgMnSc6d1	estonský
Otepää	Otepää	k1gMnSc6	Otepää
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
generálce	generálka	k1gFnSc6	generálka
na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
11,4	[number]	k4	11,4
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
)	)	kIx)	)
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
historické	historický	k2eAgNnSc4d1	historické
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
žádnou	žádný	k3yNgFnSc7	žádný
utopií	utopie	k1gFnSc7	utopie
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
držel	držet	k5eAaImAgMnS	držet
výtečnou	výtečný	k2eAgFnSc4d1	výtečná
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
Falunu	Falun	k1gInSc6	Falun
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
skiatlon	skiatlon	k1gInSc1	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
a	a	k8xC	a
štafetě	štafeta	k1gFnSc6	štafeta
pomohl	pomoct	k5eAaPmAgInS	pomoct
ke	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Lahti	Lahti	k1gNnSc6	Lahti
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
a	a	k8xC	a
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
Holmenkollenu	Holmenkollen	k1gInSc6	Holmenkollen
dojel	dojet	k5eAaPmAgMnS	dojet
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
závěrečnými	závěrečný	k2eAgInPc7d1	závěrečný
závody	závod	k1gInPc7	závod
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
Bormiu	Bormium	k1gNnSc6	Bormium
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
jistotu	jistota	k1gFnSc4	jistota
celkového	celkový	k2eAgNnSc2d1	celkové
prvenství	prvenství	k1gNnSc2	prvenství
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bormiu	Bormium	k1gNnSc6	Bormium
ještě	ještě	k9	ještě
přidal	přidat	k5eAaPmAgMnS	přidat
dvě	dva	k4xCgNnPc4	dva
druhá	druhý	k4xOgNnPc4	druhý
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
20	[number]	k4	20
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
a	a	k8xC	a
ve	v	k7c6	v
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
už	už	k6eAd1	už
mohl	moct	k5eAaImAgMnS	moct
zvednout	zvednout	k5eAaPmF	zvednout
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
velký	velký	k2eAgInSc4d1	velký
křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
glóbus	glóbus	k1gInSc4	glóbus
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k6eAd1	ještě
přidal	přidat	k5eAaPmAgMnS	přidat
malý	malý	k2eAgInSc4d1	malý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
distančních	distanční	k2eAgInPc2d1	distanční
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
hodnocení	hodnocení	k1gNnSc1	hodnocení
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
před	před	k7c7	před
druhým	druhý	k4xOgMnSc7	druhý
René	René	k1gMnSc1	René
Sommerfeldtem	Sommerfeldt	k1gInSc7	Sommerfeldt
s	s	k7c7	s
rekordním	rekordní	k2eAgInSc7d1	rekordní
náskokem	náskok	k1gInSc7	náskok
633	[number]	k4	633
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
získal	získat	k5eAaPmAgMnS	získat
ještě	ještě	k9	ještě
další	další	k2eAgNnSc4d1	další
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
tradiční	tradiční	k2eAgFnSc4d1	tradiční
anketu	anketa	k1gFnSc4	anketa
Král	Král	k1gMnSc1	Král
bílé	bílý	k2eAgFnSc2d1	bílá
stopy	stopa	k1gFnSc2	stopa
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
českého	český	k2eAgMnSc4d1	český
lyžaře	lyžař	k1gMnSc4	lyžař
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc4	stříbro
z	z	k7c2	z
MS	MS	kA	MS
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Další	další	k2eAgFnSc1d1	další
sezóna	sezóna	k1gFnSc1	sezóna
nebyla	být	k5eNaImAgFnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
vydařená	vydařený	k2eAgFnSc1d1	vydařená
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
Kuusamu	Kuusam	k1gInSc6	Kuusam
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
medailovým	medailový	k2eAgFnPc3d1	medailová
příčkám	příčka	k1gFnPc3	příčka
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
se	s	k7c7	s
zánětem	zánět	k1gInSc7	zánět
průdušek	průduška	k1gFnPc2	průduška
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
až	až	k6eAd1	až
desátý	desátý	k4xOgMnSc1	desátý
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
devátý	devátý	k4xOgMnSc1	devátý
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
Rus	Rus	k1gMnSc1	Rus
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Dementěv	Dementěv	k1gMnSc1	Dementěv
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
z	z	k7c2	z
pořadí	pořadí	k1gNnSc2	pořadí
kvůli	kvůli	k7c3	kvůli
prokázanému	prokázaný	k2eAgInSc3d1	prokázaný
dopingu	doping	k1gInSc3	doping
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
znamenalo	znamenat	k5eAaImAgNnS	znamenat
konec	konec	k1gInSc4	konec
nadějí	naděje	k1gFnSc7	naděje
na	na	k7c4	na
obhajobu	obhajoba	k1gFnSc4	obhajoba
celkového	celkový	k2eAgNnSc2d1	celkové
vítězství	vítězství	k1gNnSc2	vítězství
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
závod	závod	k1gInSc1	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
ve	v	k7c6	v
estonském	estonský	k2eAgMnSc6d1	estonský
Otepää	Otepää	k1gMnSc6	Otepää
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
opět	opět	k6eAd1	opět
začaly	začít	k5eAaPmAgFnP	začít
trápit	trápit	k5eAaImF	trápit
průdušky	průduška	k1gFnPc1	průduška
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
omezit	omezit	k5eAaPmF	omezit
přípravu	příprava	k1gFnSc4	příprava
a	a	k8xC	a
nasadit	nasadit	k5eAaPmF	nasadit
antibiotika	antibiotikum	k1gNnPc4	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
získal	získat	k5eAaPmAgMnS	získat
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
mezičase	mezičas	k1gInSc6	mezičas
kilometr	kilometr	k1gInSc1	kilometr
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
vedl	vést	k5eAaImAgInS	vést
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgInSc1d2	lepší
finiš	finiš	k1gInSc1	finiš
však	však	k9	však
měl	mít	k5eAaImAgInS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
2006	[number]	k4	2006
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
Estonec	Estonec	k1gMnSc1	Estonec
Andrus	Andrus	k1gMnSc1	Andrus
Veerpalu	Veerpala	k1gFnSc4	Veerpala
<g/>
.	.	kIx.	.
</s>
<s>
Bauerova	Bauerův	k2eAgFnSc1d1	Bauerova
ztráta	ztráta	k1gFnSc1	ztráta
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
činila	činit	k5eAaImAgFnS	činit
6,3	[number]	k4	6,3
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
závodě	závod	k1gInSc6	závod
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Andrus	Andrus	k1gMnSc1	Andrus
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
víc	hodně	k6eAd2	hodně
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zkušenější	zkušený	k2eAgMnSc1d2	zkušenější
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
poslední	poslední	k2eAgInSc1d1	poslední
kilometr	kilometr	k1gInSc1	kilometr
byl	být	k5eAaImAgInS	být
hodně	hodně	k6eAd1	hodně
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jsem	být	k5eAaImIp1nS	být
neustál	ustát	k5eNaPmAgInS	ustát
ten	ten	k3xDgInSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
nástup	nástup	k1gInSc1	nástup
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
závodech	závod	k1gInPc6	závod
MS	MS	kA	MS
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
štafetou	štafeta	k1gFnSc7	štafeta
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
(	(	kIx(	(
<g/>
předposlední	předposlední	k2eAgInSc4d1	předposlední
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
maratónu	maratón	k1gInSc2	maratón
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
volně	volně	k6eAd1	volně
už	už	k6eAd1	už
ani	ani	k8xC	ani
nenastoupil	nastoupit	k5eNaPmAgMnS	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
sezóny	sezóna	k1gFnSc2	sezóna
nezářil	zářit	k5eNaImAgInS	zářit
a	a	k8xC	a
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
devátý	devátý	k4xOgMnSc1	devátý
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
medaile	medaile	k1gFnPc4	medaile
z	z	k7c2	z
Vancouveru	Vancouver	k1gInSc2	Vancouver
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
olympijské	olympijský	k2eAgFnSc2d1	olympijská
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
ulehl	ulehnout	k5eAaPmAgMnS	ulehnout
s	s	k7c7	s
chřipkou	chřipka	k1gFnSc7	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
tréninku	trénink	k1gInSc2	trénink
vynechal	vynechat	k5eAaPmAgInS	vynechat
úvodní	úvodní	k2eAgInPc4d1	úvodní
závody	závod	k1gInPc4	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
až	až	k9	až
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
Kuusamu	Kuusam	k1gInSc6	Kuusam
do	do	k7c2	do
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dojel	dojet	k5eAaPmAgMnS	dojet
devátý	devátý	k4xOgMnSc1	devátý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
start	start	k1gInSc4	start
následujících	následující	k2eAgInPc2d1	následující
závodů	závod	k1gInPc2	závod
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Davosu	Davos	k1gInSc6	Davos
jej	on	k3xPp3gNnSc2	on
nepustil	pustit	k5eNaPmAgInS	pustit
zánět	zánět	k1gInSc1	zánět
nosohltanu	nosohltan	k1gInSc2	nosohltan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
už	už	k9	už
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
zdráv	zdráv	k2eAgMnSc1d1	zdráv
a	a	k8xC	a
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
tréninku	trénink	k1gInSc6	trénink
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
zahájil	zahájit	k5eAaPmAgInS	zahájit
7	[number]	k4	7
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
novoročním	novoroční	k2eAgInSc6d1	novoroční
prologu	prolog	k1gInSc6	prolog
na	na	k7c4	na
3,7	[number]	k4	3,7
km	km	kA	km
volně	volně	k6eAd1	volně
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Oberhofu	Oberhof	k1gInSc6	Oberhof
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
si	se	k3xPyFc3	se
polepšil	polepšit	k5eAaPmAgMnS	polepšit
na	na	k7c4	na
celkové	celkový	k2eAgNnSc4d1	celkové
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
sprintu	sprint	k1gInSc6	sprint
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
43	[number]	k4	43
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pád	pád	k1gInSc4	pád
14	[number]	k4	14
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
celkového	celkový	k2eAgNnSc2d1	celkové
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
Nora	Nor	k1gMnSc4	Nor
Pettera	Petter	k1gMnSc4	Petter
Northuga	Northug	k1gMnSc4	Northug
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
minuty	minuta	k1gFnPc1	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sprintu	sprint	k1gInSc6	sprint
volnou	volnou	k6eAd1	volnou
technikou	technika	k1gFnSc7	technika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
skončil	skončit	k5eAaPmAgMnS	skončit
až	až	k9	až
šedesátý	šedesátý	k4xOgInSc4	šedesátý
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
čtyři	čtyři	k4xCgFnPc4	čtyři
příčky	příčka	k1gFnPc4	příčka
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
na	na	k7c4	na
vítěze	vítěz	k1gMnSc4	vítěz
sprintu	sprint	k1gInSc2	sprint
a	a	k8xC	a
nového	nový	k2eAgMnSc2d1	nový
lídra	lídr	k1gMnSc2	lídr
Tour	Toura	k1gFnPc2	Toura
Emila	Emil	k1gMnSc2	Emil
Jönssona	Jönsson	k1gMnSc2	Jönsson
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
narostla	narůst	k5eAaPmAgFnS	narůst
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jönsson	Jönsson	k1gInSc1	Jönsson
ovšem	ovšem	k9	ovšem
v	v	k7c4	v
Tour	Tour	k1gInSc4	Tour
dále	daleko	k6eAd2	daleko
nepokračoval	pokračovat	k5eNaImAgMnS	pokračovat
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
přípravě	příprava	k1gFnSc3	příprava
na	na	k7c4	na
olympiádu	olympiáda	k1gFnSc4	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
na	na	k7c4	na
36	[number]	k4	36
km	km	kA	km
volně	volně	k6eAd1	volně
z	z	k7c2	z
Cortiny	Cortina	k1gFnSc2	Cortina
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ampezzo	Ampezza	k1gFnSc5	Ampezza
do	do	k7c2	do
Toblachu	Toblach	k1gInSc2	Toblach
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
dojel	dojet	k5eAaPmAgMnS	dojet
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
ztrátu	ztráta	k1gFnSc4	ztráta
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc2	vedení
opět	opět	k6eAd1	opět
ujal	ujmout	k5eAaPmAgMnS	ujmout
Nor	Nor	k1gMnSc1	Nor
Petter	Petter	k1gMnSc1	Petter
Northug	Northug	k1gMnSc1	Northug
<g/>
,	,	kIx,	,
na	na	k7c4	na
39,3	[number]	k4	39,3
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Toblachu	Toblach	k1gInSc6	Toblach
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
intervalovým	intervalový	k2eAgInSc7d1	intervalový
startem	start	k1gInSc7	start
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
si	se	k3xPyFc3	se
polepšil	polepšit	k5eAaPmAgInS	polepšit
již	již	k9	již
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
na	na	k7c4	na
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
Northuga	Northug	k1gMnSc4	Northug
umazal	umazat	k5eAaPmAgMnS	umazat
dalších	další	k2eAgFnPc2d1	další
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgInSc1d1	vynikající
výkon	výkon	k1gInSc1	výkon
podal	podat	k5eAaPmAgInS	podat
v	v	k7c6	v
předposledním	předposlední	k2eAgInSc6d1	předposlední
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
20	[number]	k4	20
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
ve	v	k7c4	v
Val	val	k1gInSc4	val
di	di	k?	di
Fiemme	Fiemme	k1gFnSc2	Fiemme
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
o	o	k7c4	o
půl	půl	k1xP	půl
minuty	minuta	k1gFnSc2	minuta
před	před	k7c7	před
lídrem	lídr	k1gMnSc7	lídr
celkového	celkový	k2eAgInSc2d1	celkový
pořadí	pořadí	k1gNnSc6	pořadí
Northugem	Northug	k1gMnSc7	Northug
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
utrhli	utrhnout	k5eAaPmAgMnP	utrhnout
zbytku	zbytek	k1gInSc2	zbytek
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
kilometru	kilometr	k1gInSc2	kilometr
setřásl	setřást	k5eAaPmAgMnS	setřást
Bauer	Bauer	k1gMnSc1	Bauer
i	i	k8xC	i
Nora	Nora	k1gFnSc1	Nora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
započtení	započtení	k1gNnSc6	započtení
bonifikací	bonifikace	k1gFnPc2	bonifikace
z	z	k7c2	z
prémiových	prémiový	k2eAgInPc2d1	prémiový
sprintů	sprint	k1gInPc2	sprint
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
Bauera	Bauer	k1gMnSc4	Bauer
znamenalo	znamenat	k5eAaImAgNnS	znamenat
posun	posun	k1gInSc4	posun
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
celkového	celkový	k2eAgNnSc2d1	celkové
pořadí	pořadí	k1gNnSc2	pořadí
se	se	k3xPyFc4	se
ztátou	ztáíst	k5eAaPmIp3nP	ztáíst
8,3	[number]	k4	8,3
sekundy	sekund	k1gInPc7	sekund
na	na	k7c4	na
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
Northuga	Northug	k1gMnSc4	Northug
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
minuty	minuta	k1gFnPc4	minuta
před	před	k7c7	před
třetím	třetí	k4xOgMnSc7	třetí
Axelem	Axel	k1gMnSc7	Axel
Teichmannem	Teichmann	k1gInSc7	Teichmann
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
se	s	k7c7	s
stoupáním	stoupání	k1gNnSc7	stoupání
do	do	k7c2	do
sjezdovky	sjezdovka	k1gFnSc2	sjezdovka
v	v	k7c6	v
Alpe	Alpe	k1gFnSc6	Alpe
Cermis	Cermis	k1gFnSc2	Cermis
nedal	dát	k5eNaPmAgMnS	dát
Bauer	Bauer	k1gMnSc1	Bauer
doposud	doposud	k6eAd1	doposud
vedoucímu	vedoucí	k1gMnSc3	vedoucí
Northugovi	Northug	k1gMnSc3	Northug
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
v	v	k7c6	v
prudkém	prudký	k2eAgNnSc6d1	prudké
stoupání	stoupání	k1gNnSc6	stoupání
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
km	km	kA	km
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc4	on
utrhl	utrhnout	k5eAaPmAgMnS	utrhnout
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
celkový	celkový	k2eAgInSc4d1	celkový
triumf	triumf	k1gInSc4	triumf
v	v	k7c4	v
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
.	.	kIx.	.
</s>
<s>
Nor	Nor	k1gMnSc1	Nor
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
zcela	zcela	k6eAd1	zcela
odpadl	odpadnout	k5eAaPmAgMnS	odpadnout
<g/>
,	,	kIx,	,
uhájil	uhájit	k5eAaPmAgMnS	uhájit
sice	sice	k8xC	sice
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
vítězného	vítězný	k2eAgMnSc4d1	vítězný
Bauera	Bauer	k1gMnSc4	Bauer
ztratil	ztratit	k5eAaPmAgMnS	ztratit
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
16,4	[number]	k4	16,4
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jdou	jít	k5eAaImIp3nP	jít
srovnat	srovnat	k5eAaPmF	srovnat
oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gFnSc1	jeho
triumfy	triumf	k1gInPc4	triumf
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokaždé	pokaždé	k6eAd1	pokaždé
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
letos	letos	k6eAd1	letos
jsem	být	k5eAaImIp1nS	být
šel	jít	k5eAaImAgMnS	jít
zezadu	zezadu	k6eAd1	zezadu
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
den	den	k1gInSc1	den
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
mohl	moct	k5eAaImAgMnS	moct
jsem	být	k5eAaImIp1nS	být
jen	jen	k9	jen
překvapit	překvapit	k5eAaPmF	překvapit
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
jsem	být	k5eAaImIp1nS	být
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
prolog	prolog	k1gInSc4	prolog
a	a	k8xC	a
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něčem	něco	k3yInSc6	něco
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
letos	letos	k6eAd1	letos
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
,	,	kIx,	,
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
těžší	těžký	k2eAgFnSc1d2	těžší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
konec	konec	k1gInSc1	konec
byl	být	k5eAaImAgInS	být
stejnej	stejnej	k?	stejnej
-	-	kIx~	-
perfektně	perfektně	k6eAd1	perfektně
zajetý	zajetý	k2eAgInSc1d1	zajetý
poslední	poslední	k2eAgFnPc4d1	poslední
dvě	dva	k4xCgFnPc4	dva
etapy	etapa	k1gFnPc4	etapa
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
radost	radost	k1gFnSc1	radost
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Týden	týden	k1gInSc1	týden
po	po	k7c4	po
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
závod	závod	k1gInSc1	závod
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
v	v	k7c6	v
estonském	estonský	k2eAgMnSc6d1	estonský
Otepää	Otepää	k1gMnSc6	Otepää
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
již	již	k6eAd1	již
třetí	třetí	k4xOgInSc4	třetí
rok	rok	k1gInSc4	rok
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Úvodním	úvodní	k2eAgInSc7d1	úvodní
závodem	závod	k1gInSc7	závod
pro	pro	k7c4	pro
běžce	běžec	k1gMnSc4	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2010	[number]	k4	2010
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Vancouveru	Vancouvero	k1gNnSc6	Vancouvero
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
km	km	kA	km
volnou	volný	k2eAgFnSc7d1	volná
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
o	o	k7c4	o
pouhých	pouhý	k2eAgNnPc2d1	pouhé
1,5	[number]	k4	1,5
sekundy	sekunda	k1gFnPc4	sekunda
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
Švéd	Švéd	k1gMnSc1	Švéd
Marcus	Marcus	k1gMnSc1	Marcus
Hellner	Hellner	k1gMnSc1	Hellner
<g/>
.	.	kIx.	.
</s>
<s>
Suverénním	suverénní	k2eAgMnSc7d1	suverénní
vítězem	vítěz	k1gMnSc7	vítěz
závodu	závod	k1gInSc2	závod
byl	být	k5eAaImAgMnS	být
Švýcar	Švýcar	k1gMnSc1	Švýcar
Dario	Daria	k1gFnSc5	Daria
Cologna	Cologn	k1gMnSc4	Cologn
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
Ital	Ital	k1gMnSc1	Ital
Pietro	Pietro	k1gNnSc1	Pietro
Piller-Cottrer	Piller-Cottrer	k1gMnSc1	Piller-Cottrer
ztratil	ztratit	k5eAaPmAgMnS	ztratit
24,6	[number]	k4	24,6
sekundy	sekund	k1gInPc7	sekund
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
Bauer	Bauer	k1gMnSc1	Bauer
35,7	[number]	k4	35,7
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
skončil	skončit	k5eAaPmAgInS	skončit
sedmý	sedmý	k4xOgInSc1	sedmý
<g/>
,	,	kIx,	,
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
medaili	medaile	k1gFnSc4	medaile
ho	on	k3xPp3gMnSc2	on
zbrdil	zbrdit	k5eAaImAgInS	zbrdit
pád	pád	k1gInSc4	pád
kolo	kolo	k1gNnSc4	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Štafetě	štafeta	k1gFnSc3	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
pomohl	pomoct	k5eAaPmAgInS	pomoct
k	k	k7c3	k
bronzové	bronzový	k2eAgFnSc3d1	bronzová
medaili	medaile	k1gFnSc3	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
úseku	úsek	k1gInSc6	úsek
běžící	běžící	k2eAgMnSc1d1	běžící
Martin	Martin	k1gMnSc1	Martin
Jakš	Jakš	k1gMnSc1	Jakš
předával	předávat	k5eAaImAgMnS	předávat
Bauerovi	Bauer	k1gMnSc3	Bauer
štafetu	štafeta	k1gFnSc4	štafeta
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
26,8	[number]	k4	26,8
sekundy	sekunda	k1gFnPc4	sekunda
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
ztrátu	ztráta	k1gFnSc4	ztráta
smazal	smazat	k5eAaPmAgMnS	smazat
ještě	ještě	k9	ještě
před	před	k7c7	před
polovinou	polovina	k1gFnSc7	polovina
svého	svůj	k3xOyFgInSc2	svůj
úseku	úsek	k1gInSc2	úsek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
u	u	k7c2	u
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pole	pole	k1gNnSc1	pole
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
způsobem	způsob	k1gInSc7	způsob
roztrhalo	roztrhat	k5eAaPmAgNnS	roztrhat
<g/>
.	.	kIx.	.
</s>
<s>
Předával	předávat	k5eAaImAgMnS	předávat
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
5,7	[number]	k4	5,7
sekundy	sekunda	k1gFnPc4	sekunda
na	na	k7c4	na
vedoucí	vedoucí	k2eAgMnPc4d1	vedoucí
Švédy	Švéd	k1gMnPc4	Švéd
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Francouzi	Francouz	k1gMnPc7	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
Německo	Německo	k1gNnSc4	Německo
už	už	k6eAd1	už
ovšem	ovšem	k9	ovšem
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
skoro	skoro	k6eAd1	skoro
půl	půl	k1xP	půl
minuty	minuta	k1gFnSc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
trojice	trojice	k1gFnSc1	trojice
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
sjela	sjet	k5eAaPmAgFnS	sjet
a	a	k8xC	a
udržovala	udržovat	k5eAaImAgFnS	udržovat
odstup	odstup	k1gInSc4	odstup
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Magál	Magál	k1gMnSc1	Magál
předával	předávat	k5eAaImAgMnS	předávat
finišmanovi	finišman	k1gMnSc3	finišman
Martinu	Martin	k1gMnSc3	Martin
Koukalovi	Koukal	k1gMnSc3	Koukal
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
5,3	[number]	k4	5,3
sekundy	sekunda	k1gFnPc4	sekunda
na	na	k7c4	na
vedoucí	vedoucí	k2eAgMnPc4d1	vedoucí
Švédy	Švéd	k1gMnPc4	Švéd
<g/>
,	,	kIx,	,
odstup	odstup	k1gInSc4	odstup
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
trojice	trojice	k1gFnSc2	trojice
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
štafet	štafeta	k1gFnPc2	štafeta
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půlminutový	půlminutový	k2eAgInSc1d1	půlminutový
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
medaile	medaile	k1gFnPc4	medaile
již	již	k6eAd1	již
nikdo	nikdo	k3yNnSc1	nikdo
další	další	k2eAgMnSc1d1	další
nemůže	moct	k5eNaImIp3nS	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zezadu	zezadu	k6eAd1	zezadu
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
přibližovat	přibližovat	k5eAaImF	přibližovat
Nor	Nor	k1gMnSc1	Nor
Petter	Petter	k1gMnSc1	Petter
Northug	Northug	k1gMnSc1	Northug
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Hellner	Hellner	k1gMnSc1	Hellner
se	se	k3xPyFc4	se
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
utrhl	utrhnout	k5eAaPmAgMnS	utrhnout
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
Švédům	Švéd	k1gMnPc3	Švéd
pro	pro	k7c4	pro
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
nakonec	nakonec	k6eAd1	nakonec
vybojovalo	vybojovat	k5eAaPmAgNnS	vybojovat
zásluhou	zásluhou	k7c2	zásluhou
výborně	výborně	k6eAd1	výborně
finišujícího	finišující	k2eAgMnSc2d1	finišující
Northuga	Northug	k1gMnSc2	Northug
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Koukal	Koukal	k1gMnSc1	Koukal
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
přesprintoval	přesprintovat	k5eAaBmAgInS	přesprintovat
Francouze	Francouz	k1gMnSc4	Francouz
Emmanuela	Emmanuel	k1gMnSc4	Emmanuel
Jonniera	Jonnier	k1gMnSc4	Jonnier
a	a	k8xC	a
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
Česku	Česko	k1gNnSc6	Česko
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Bauera	Bauer	k1gMnSc4	Bauer
vrcholem	vrchol	k1gInSc7	vrchol
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
neměl	mít	k5eNaImAgMnS	mít
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
ideální	ideální	k2eAgInSc4d1	ideální
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
se	se	k3xPyFc4	se
nikomu	nikdo	k3yNnSc3	nikdo
nepodařilo	podařit	k5eNaPmAgNnS	podařit
roztrhat	roztrhat	k5eAaPmF	roztrhat
a	a	k8xC	a
tak	tak	k6eAd1	tak
do	do	k7c2	do
finiše	finiš	k1gInSc2	finiš
přijela	přijet	k5eAaPmAgFnS	přijet
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
14	[number]	k4	14
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
nikdy	nikdy	k6eAd1	nikdy
nepatřil	patřit	k5eNaImAgMnS	patřit
mezi	mezi	k7c4	mezi
skvělé	skvělý	k2eAgMnPc4d1	skvělý
sprintery	sprinter	k1gMnPc4	sprinter
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
nestačil	stačit	k5eNaBmAgMnS	stačit
<g/>
,	,	kIx,	,
dojel	dojet	k5eAaPmAgMnS	dojet
dvanáctý	dvanáctý	k4xOgMnSc1	dvanáctý
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Nor	Nor	k1gMnSc1	Nor
Petter	Petter	k1gMnSc1	Petter
Northug	Northug	k1gMnSc1	Northug
<g/>
.	.	kIx.	.
</s>
<s>
Bauer	Bauer	k1gMnSc1	Bauer
uhájil	uhájit	k5eAaPmAgMnS	uhájit
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
mu	on	k3xPp3gNnSc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
i	i	k9	i
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Lahti	Lahti	k1gNnSc6	Lahti
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ze	z	k7c2	z
skiatlonu	skiatlon	k1gInSc2	skiatlon
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
Falunu	Falun	k1gInSc6	Falun
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Finále	finále	k1gNnSc2	finále
SP	SP	kA	SP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Král	Král	k1gMnSc1	Král
bílé	bílý	k2eAgFnSc2d1	bílá
stopy	stopa	k1gFnSc2	stopa
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
českého	český	k2eAgMnSc4d1	český
lyžaře	lyžař	k1gMnSc4	lyžař
sezóny	sezóna	k1gFnSc2	sezóna
jej	on	k3xPp3gMnSc4	on
poněkud	poněkud	k6eAd1	poněkud
překvapivě	překvapivě	k6eAd1	překvapivě
porazila	porazit	k5eAaPmAgFnS	porazit
alpská	alpský	k2eAgFnSc1d1	alpská
lyžařka	lyžařka	k1gFnSc1	lyžařka
Šárka	Šárka	k1gFnSc1	Šárka
Záhrobská	Záhrobský	k2eAgFnSc1d1	Záhrobská
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
u	u	k7c2	u
MS	MS	kA	MS
a	a	k8xC	a
OH	OH	kA	OH
jsou	být	k5eAaImIp3nP	být
uvedena	uveden	k2eAgNnPc4d1	uvedeno
umístění	umístění	k1gNnPc4	umístění
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
individuálních	individuální	k2eAgInPc6d1	individuální
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
celkového	celkový	k2eAgNnSc2d1	celkové
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
pódiová	pódiový	k2eAgNnPc4d1	pódiové
umístění	umístění	k1gNnPc4	umístění
(	(	kIx(	(
<g/>
do	do	k7c2	do
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
místa	místo	k1gNnPc1	místo
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
-	-	kIx~	-
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc2	Lake
City	City	k1gFnPc2	City
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
volně	volně	k6eAd1	volně
hromadný	hromadný	k2eAgInSc4d1	hromadný
start	start	k1gInSc4	start
2002	[number]	k4	2002
-	-	kIx~	-
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc2	Lake
City	City	k1gFnPc2	City
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2006	[number]	k4	2006
-	-	kIx~	-
Turín	Turín	k1gInSc1	Turín
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2010	[number]	k4	2010
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2010	[number]	k4	2010
-	-	kIx~	-
Vancouver	Vancouver	k1gInSc1	Vancouver
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2010	[number]	k4	2010
-	-	kIx~	-
Vancouver	Vancouver	k1gInSc1	Vancouver
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
(	(	kIx(	(
<g/>
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Jakš	Jakš	k1gMnSc1	Jakš
/	/	kIx~	/
Bauer	Bauer	k1gMnSc1	Bauer
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
Magál	Magál	k1gMnSc1	Magál
/	/	kIx~	/
Koukal	Koukal	k1gMnSc1	Koukal
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Oberstdorf	Oberstdorf	k1gInSc1	Oberstdorf
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2007	[number]	k4	2007
-	-	kIx~	-
Sapporo	Sappora	k1gFnSc5	Sappora
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2007	[number]	k4	2007
-	-	kIx~	-
Sapporo	Sappora	k1gFnSc5	Sappora
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
hromadný	hromadný	k2eAgInSc4d1	hromadný
<g />
.	.	kIx.	.
</s>
<s>
start	start	k1gInSc1	start
2009	[number]	k4	2009
-	-	kIx~	-
Liberec	Liberec	k1gInSc1	Liberec
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2011	[number]	k4	2011
-	-	kIx~	-
Oslo	Oslo	k1gNnSc6	Oslo
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2015	[number]	k4	2015
-	-	kIx~	-
Falun	Faluna	k1gFnPc2	Faluna
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2015	[number]	k4	2015
-	-	kIx~	-
Falun	Faluna	k1gFnPc2	Faluna
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g />
.	.	kIx.	.
</s>
<s>
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
hromadný	hromadný	k2eAgInSc4d1	hromadný
start	start	k1gInSc4	start
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
-	-	kIx~	-
celkové	celkový	k2eAgNnSc4d1	celkové
pořadí	pořadí	k1gNnSc4	pořadí
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
-	-	kIx~	-
42	[number]	k4	42
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
-	-	kIx~	-
61	[number]	k4	61
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
-	-	kIx~	-
70	[number]	k4	70
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
-	-	kIx~	-
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
<g/>
místo	místo	k7c2	místo
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
-	-	kIx~	-
37	[number]	k4	37
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
Ski	ski	k1gFnSc2	ski
-	-	kIx~	-
celkové	celkový	k2eAgNnSc1d1	celkové
pořadí	pořadí	k1gNnSc1	pořadí
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
10	[number]	k4	10
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k6eAd1	místo
Závody	závod	k1gInPc4	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2002	[number]	k4	2002
-	-	kIx~	-
Lahti	Lahti	k1gNnSc1	Lahti
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
sprint	sprint	k1gInSc4	sprint
dvojic	dvojice	k1gFnPc2	dvojice
volně	volně	k6eAd1	volně
2002	[number]	k4	2002
-	-	kIx~	-
Kuusamo	Kuusama	k1gFnSc5	Kuusama
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2003	[number]	k4	2003
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2003	[number]	k4	2003
-	-	kIx~	-
Lahti	Lahti	k1gNnSc1	Lahti
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2004	[number]	k4	2004
-	-	kIx~	-
Oberstdorf	Oberstdorf	k1gInSc1	Oberstdorf
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2004	[number]	k4	2004
-	-	kIx~	-
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
50	[number]	k4	50
km	km	kA	km
volně	volně	k6eAd1	volně
2005	[number]	k4	2005
-	-	kIx~	-
Pragelato	Pragelat	k2eAgNnSc1d1	Pragelato
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2005	[number]	k4	2005
-	-	kIx~	-
Lahti	Lahti	k1gNnSc1	Lahti
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2005	[number]	k4	2005
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
2006	[number]	k4	2006
-	-	kIx~	-
Otepää	Otepää	k1gMnSc1	Otepää
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2006	[number]	k4	2006
-	-	kIx~	-
Gällivare	Gällivar	k1gInSc5	Gällivar
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
2007	[number]	k4	2007
-	-	kIx~	-
Beitostø	Beitostø	k1gMnSc1	Beitostø
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g />
.	.	kIx.	.
</s>
<s>
km	km	kA	km
volně	volně	k6eAd1	volně
2007	[number]	k4	2007
-	-	kIx~	-
Kuusamo	Kuusama	k1gFnSc5	Kuusama
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2007	[number]	k4	2007
-	-	kIx~	-
Davos	Davos	k1gMnSc1	Davos
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
2008	[number]	k4	2008
-	-	kIx~	-
Otepää	Otepää	k1gMnSc1	Otepää
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2008	[number]	k4	2008
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
11,4	[number]	k4	11,4
km	km	kA	km
volně	volně	k6eAd1	volně
2008	[number]	k4	2008
-	-	kIx~	-
Falun	Falun	k1gNnSc1	Falun
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2008	[number]	k4	2008
-	-	kIx~	-
Falun	Falun	k1gNnSc1	Falun
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
km	km	kA	km
2008	[number]	k4	2008
-	-	kIx~	-
Lahti	Lahti	k1gNnSc1	Lahti
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2008	[number]	k4	2008
-	-	kIx~	-
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
50	[number]	k4	50
km	km	kA	km
volně	volně	k6eAd1	volně
2008	[number]	k4	2008
-	-	kIx~	-
Bormio	Bormio	k1gNnSc1	Bormio
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2008	[number]	k4	2008
-	-	kIx~	-
Kuusamo	Kuusama	k1gFnSc5	Kuusama
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Otepää	Otepää	k1gMnSc1	Otepää
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2010	[number]	k4	2010
-	-	kIx~	-
Otepää	Otepää	k1gMnSc1	Otepää
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2010	[number]	k4	2010
-	-	kIx~	-
Lahti	Lahti	k1gNnSc1	Lahti
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
skiatlon	skiatlon	k1gInSc4	skiatlon
2010	[number]	k4	2010
-	-	kIx~	-
Davos	Davos	k1gMnSc1	Davos
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2011	[number]	k4	2011
-	-	kIx~	-
Davos	Davos	k1gMnSc1	Davos
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
volně	volně	k6eAd1	volně
2012	[number]	k4	2012
-	-	kIx~	-
Otepää	Otepää	k1gMnSc1	Otepää
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
2013	[number]	k4	2013
-	-	kIx~	-
La	la	k1gNnSc1	la
Clusaz	Clusaz	k1gInSc1	Clusaz
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
<g/>
×	×	k?	×
<g />
.	.	kIx.	.
</s>
<s>
<g/>
7,5	[number]	k4	7,5
km	km	kA	km
Etapy	etapa	k1gFnSc2	etapa
závodů	závod	k1gInPc2	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2007	[number]	k4	2007
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
<g />
.	.	kIx.	.
</s>
<s>
Ski	ski	k1gFnSc1	ski
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Cortina-Toblach	Cortina-Toblach	k1gMnSc1	Cortina-Toblach
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Val	val	k1gInSc1	val
di	di	k?	di
<g />
.	.	kIx.	.
</s>
<s>
Fiemme	Fiemit	k5eAaPmRp1nP	Fiemit
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
hromadný	hromadný	k2eAgInSc4d1	hromadný
start	start	k1gInSc4	start
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Val	val	k1gInSc1	val
di	di	k?	di
Fiemme	Fiemme	k1gFnSc2	Fiemme
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
volně	volně	k6eAd1	volně
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Falun	Falun	k1gInSc1	Falun
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
stíhací	stíhací	k2eAgInSc4d1	stíhací
závod	závod	k1gInSc4	závod
(	(	kIx(	(
<g/>
Finále	finále	k1gNnSc4	finále
SP	SP	kA	SP
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Kuusamo	Kuusama	k1gFnSc5	Kuusama
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
Ruka	ruka	k1gFnSc1	ruka
Triple	tripl	k1gInSc5	tripl
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
Val	val	k1gInSc1	val
di	di	k?	di
Fiemme	Fiemme	k1gFnSc2	Fiemme
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
9	[number]	k4	9
km	km	kA	km
volně	volně	k6eAd1	volně
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
Falun	Falun	k1gNnSc1	Falun
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
volně	volně	k6eAd1	volně
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
Finále	finále	k1gNnSc1	finále
SP	SP	kA	SP
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Kuusamo	Kuusama	k1gFnSc5	Kuusama
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Ruka	ruka	k1gFnSc1	ruka
Triple	tripl	k1gInSc5	tripl
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
L.	L.	kA	L.
Bauera	Bauer	k1gMnSc2	Bauer
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
fis-ski	fiskit	k5eAaPmRp2nS	fis-skit
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
sports-reference	sportseference	k1gFnSc1	sports-reference
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
