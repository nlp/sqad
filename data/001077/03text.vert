<s>
Frank	Frank	k1gMnSc1	Frank
James	James	k1gMnSc1	James
Lampard	Lampard	k1gMnSc1	Lampard
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
v	v	k7c6	v
Romfordu	Romford	k1gInSc6	Romford
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
West	Westa	k1gFnPc2	Westa
Ham	ham	k0	ham
United	United	k1gMnSc1	United
a	a	k8xC	a
Swansea	Swansea	k1gMnSc1	Swansea
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
hostoval	hostovat	k5eAaImAgInS	hostovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
zařídil	zařídit	k5eAaPmAgMnS	zařídit
dvěma	dva	k4xCgFnPc7	dva
góly	gól	k1gInPc1	gól
vítězství	vítězství	k1gNnSc1	vítězství
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Premier	Premier	k1gMnSc1	Premier
League	League	k1gInSc1	League
proti	proti	k7c3	proti
domácí	domácí	k1gFnSc3	domácí
Aston	Astona	k1gFnPc2	Astona
Ville	Ville	k1gNnSc2	Ville
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
s	s	k7c7	s
203	[number]	k4	203
nastřílenými	nastřílený	k2eAgFnPc7d1	nastřílená
brankami	branka	k1gFnPc7	branka
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soutěžích	soutěž	k1gFnPc6	soutěž
stal	stát	k5eAaPmAgInS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kanonýrem	kanonýr	k1gMnSc7	kanonýr
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Chelsea	Chelsea	k1gMnSc1	Chelsea
(	(	kIx(	(
<g/>
překonal	překonat	k5eAaPmAgMnS	překonat
202	[number]	k4	202
gólů	gól	k1gInPc2	gól
Bobbyho	Bobby	k1gMnSc2	Bobby
Tamblinga	Tambling	k1gMnSc2	Tambling
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
respektu	respekt	k1gInSc2	respekt
k	k	k7c3	k
nemocnému	mocný	k2eNgMnSc3d1	nemocný
bývalému	bývalý	k2eAgMnSc3d1	bývalý
fotbalistovi	fotbalista	k1gMnSc3	fotbalista
Tamblingovi	Tambling	k1gMnSc3	Tambling
tyto	tento	k3xDgInPc4	tento
góly	gól	k1gInPc4	gól
neslavil	slavit	k5eNaImAgInS	slavit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
IQ	iq	kA	iq
157	[number]	k4	157
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Reprezentační	reprezentační	k2eAgFnSc4d1	reprezentační
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Anglie	Anglie	k1gFnSc2	Anglie
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
za	za	k7c4	za
anglický	anglický	k2eAgInSc4d1	anglický
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
106	[number]	k4	106
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
29	[number]	k4	29
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Romfordu	Romford	k1gInSc6	Romford
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
otec	otec	k1gMnSc1	otec
Frank	Frank	k1gMnSc1	Frank
Richard	Richard	k1gMnSc1	Richard
George	Georg	k1gMnSc2	Georg
Lampard	Lampard	k1gMnSc1	Lampard
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
anglický	anglický	k2eAgMnSc1d1	anglický
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
s	s	k7c7	s
West	West	k1gInSc1	West
Ham	ham	k0	ham
United	United	k1gInSc1	United
FA	fa	k1gNnSc6	fa
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Harry	Harra	k1gFnSc2	Harra
Redknapp	Redknapp	k1gMnSc1	Redknapp
trénuje	trénovat	k5eAaImIp3nS	trénovat
tým	tým	k1gInSc4	tým
Queens	Queens	k1gInSc1	Queens
Park	park	k1gInSc1	park
Rangers	Rangers	k1gInSc1	Rangers
a	a	k8xC	a
bratranec	bratranec	k1gMnSc1	bratranec
Jamie	Jamie	k1gFnSc2	Jamie
Redknapp	Redknapp	k1gInSc1	Redknapp
hrál	hrát	k5eAaImAgInS	hrát
12	[number]	k4	12
let	léto	k1gNnPc2	léto
za	za	k7c4	za
Liverpool	Liverpool	k1gInSc4	Liverpool
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
týmu	tým	k1gInSc3	tým
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1994	[number]	k4	1994
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc4	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
profesionální	profesionální	k2eAgFnSc4d1	profesionální
smlouvu	smlouva	k1gFnSc4	smlouva
podepsal	podepsat	k5eAaPmAgMnS	podepsat
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
do	do	k7c2	do
druholigového	druholigový	k2eAgInSc2d1	druholigový
týmu	tým	k1gInSc2	tým
Swansea	Swanseum	k1gNnSc2	Swanseum
City	City	k1gFnSc2	City
AFC	AFC	kA	AFC
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
tým	tým	k1gInSc4	tým
odehrál	odehrát	k5eAaPmAgMnS	odehrát
devět	devět	k4xCc1	devět
utkání	utkání	k1gNnPc2	utkání
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
opět	opět	k6eAd1	opět
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
West	Westa	k1gFnPc2	Westa
Ham	ham	k0	ham
United	United	k1gInSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1997	[number]	k4	1997
si	se	k3xPyFc3	se
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Aston	Aston	k1gMnSc1	Aston
Ville	Ville	k1gNnSc4	Ville
zlomil	zlomit	k5eAaPmAgMnS	zlomit
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
zranění	zranění	k1gNnSc4	zranění
mu	on	k3xPp3gMnSc3	on
předčasně	předčasně	k6eAd1	předčasně
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Lampardův	Lampardův	k2eAgInSc4d1	Lampardův
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
West	Westa	k1gFnPc2	Westa
Hamu	Hamus	k1gInSc2	Hamus
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Barnsley	Barnsley	k1gInPc7	Barnsley
<g/>
.	.	kIx.	.
</s>
<s>
Lampard	Lampard	k1gMnSc1	Lampard
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
kapitánem	kapitán	k1gMnSc7	kapitán
mládežnického	mládežnický	k2eAgInSc2d1	mládežnický
týmu	tým	k1gInSc2	tým
West	Westa	k1gFnPc2	Westa
Hamu	Hama	k1gFnSc4	Hama
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
finále	finále	k1gNnSc1	finále
FA	fa	kA	fa
cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
přelomová	přelomový	k2eAgNnPc1d1	přelomové
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
stálou	stálý	k2eAgFnSc7d1	stálá
součástí	součást	k1gFnSc7	součást
A	a	k8xC	a
týmu	tým	k1gInSc3	tým
West	Westa	k1gFnPc2	Westa
Hamu	Hamus	k1gInSc2	Hamus
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
nechyběl	chybět	k5eNaImAgMnS	chybět
ani	ani	k8xC	ani
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
utkání	utkání	k1gNnSc6	utkání
<g/>
,	,	kIx,	,
i	i	k8xC	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
West	West	k1gInSc1	West
Ham	ham	k0	ham
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
pátém	pátý	k4xOgInSc6	pátý
místě	místo	k1gNnSc6	místo
Premier	Premira	k1gFnPc2	Premira
League	League	k1gNnPc2	League
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tým	tým	k1gInSc4	tým
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nepostradatelným	postradatelný	k2eNgMnSc7d1	nepostradatelný
a	a	k8xC	a
během	během	k7c2	během
187	[number]	k4	187
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
West	West	k1gMnSc1	West
Hamu	Hama	k1gMnSc4	Hama
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
39	[number]	k4	39
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každému	každý	k3xTgMnSc3	každý
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
pozice	pozice	k1gFnSc1	pozice
Franka	Frank	k1gMnSc2	Frank
líbila	líbit	k5eAaImAgFnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Frank	Frank	k1gMnSc1	Frank
senior	senior	k1gMnSc1	senior
coby	coby	k?	coby
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
protežování	protežování	k1gNnSc2	protežování
vlastního	vlastní	k2eAgMnSc2d1	vlastní
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Joe	Joe	k1gMnSc7	Joe
Colem	Col	k1gMnSc7	Col
<g/>
,	,	kIx,	,
Michaelem	Michael	k1gMnSc7	Michael
Carrickem	Carricko	k1gNnSc7	Carricko
a	a	k8xC	a
Rio	Rio	k1gMnSc7	Rio
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
tvořil	tvořit	k5eAaImAgMnS	tvořit
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
jádro	jádro	k1gNnSc1	jádro
West	Westa	k1gFnPc2	Westa
Hamu	Hamus	k1gInSc2	Hamus
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
prodán	prodat	k5eAaPmNgMnS	prodat
do	do	k7c2	do
Leedsu	Leeds	k1gInSc2	Leeds
United	United	k1gMnSc1	United
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
také	také	k9	také
pro	pro	k7c4	pro
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgFnPc3	všecek
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c6	o
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
Aston	Astona	k1gFnPc2	Astona
Villy	Villa	k1gFnSc2	Villa
namířil	namířit	k5eAaPmAgMnS	namířit
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
londýnské	londýnský	k2eAgFnSc2d1	londýnská
Chelsea	Chelse	k2eAgNnPc1d1	Chelse
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
od	od	k7c2	od
týmu	tým	k1gInSc2	tým
odešel	odejít	k5eAaPmAgInS	odejít
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Harry	Harra	k1gFnSc2	Harra
Redknapp	Redknapp	k1gMnSc1	Redknapp
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Chelsea	Chelseum	k1gNnSc2	Chelseum
podepsal	podepsat	k5eAaPmAgInS	podepsat
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgMnSc1d1	někdejší
trenér	trenér	k1gMnSc1	trenér
Claudio	Claudio	k6eAd1	Claudio
Ranieri	Raniere	k1gFnSc4	Raniere
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zlepšování	zlepšování	k1gNnSc1	zlepšování
v	v	k7c4	v
Chelsea	Chelseus	k1gMnSc4	Chelseus
bylo	být	k5eAaImAgNnS	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
ale	ale	k8xC	ale
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
sezónách	sezóna	k1gFnPc6	sezóna
odehrál	odehrát	k5eAaPmAgInS	odehrát
jen	jen	k6eAd1	jen
jedenáct	jedenáct	k4xCc4	jedenáct
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výpadky	výpadek	k1gInPc1	výpadek
hráčů	hráč	k1gMnPc2	hráč
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
mu	on	k3xPp3gMnSc3	on
umožnily	umožnit	k5eAaPmAgFnP	umožnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
třetí	třetí	k4xOgFnSc4	třetí
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
týmu	tým	k1gInSc2	tým
ujal	ujmout	k5eAaPmAgInS	ujmout
co	co	k9	co
by	by	kYmCp3nS	by
majitel	majitel	k1gMnSc1	majitel
Roman	Roman	k1gMnSc1	Roman
Abramovič	Abramovič	k1gMnSc1	Abramovič
naplno	naplno	k6eAd1	naplno
prosadil	prosadit	k5eAaPmAgMnS	prosadit
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
záložníků	záložník	k1gMnPc2	záložník
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
záložníkem	záložník	k1gMnSc7	záložník
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
novináři	novinář	k1gMnPc7	novinář
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
i	i	k9	i
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
záložníkem	záložník	k1gMnSc7	záložník
anglické	anglický	k2eAgFnSc2d1	anglická
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
trenér	trenér	k1gMnSc1	trenér
Chelsea	Chelseus	k1gMnSc2	Chelseus
José	José	k1gNnSc2	José
Mourinho	Mourin	k1gMnSc2	Mourin
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
rozhovorů	rozhovor	k1gInPc2	rozhovor
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
prohrál	prohrát	k5eAaPmAgMnS	prohrát
moskevské	moskevský	k2eAgFnSc3d1	Moskevská
finále	finála	k1gFnSc3	finála
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
na	na	k7c4	na
penalty	penalta	k1gFnPc4	penalta
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
dal	dát	k5eAaPmAgInS	dát
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
penaltu	penalta	k1gFnSc4	penalta
také	také	k9	také
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
pod	pod	k7c7	pod
izraelským	izraelský	k2eAgMnSc7d1	izraelský
trenérem	trenér	k1gMnSc7	trenér
Avramem	Avram	k1gInSc7	Avram
Grantem	grant	k1gInSc7	grant
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
s	s	k7c7	s
Chelsea	Chelseum	k1gNnSc2	Chelseum
finále	finále	k1gNnSc7	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
proti	proti	k7c3	proti
Bayernu	Bayern	k1gInSc3	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
vedl	vést	k5eAaImAgMnS	vést
mužstvo	mužstvo	k1gNnSc4	mužstvo
jako	jako	k8xC	jako
kapitán	kapitán	k1gMnSc1	kapitán
a	a	k8xC	a
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
skóroval	skórovat	k5eAaBmAgMnS	skórovat
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
Chelsea	Chelse	k1gInSc2	Chelse
k	k	k7c3	k
utkání	utkání	k1gNnSc3	utkání
o	o	k7c4	o
evropský	evropský	k2eAgInSc4d1	evropský
Superpohár	superpohár	k1gInSc4	superpohár
proti	proti	k7c3	proti
španělskému	španělský	k2eAgMnSc3d1	španělský
Atléticu	Atlétic	k1gMnSc3	Atlétic
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Chelsea	Chelsea	k1gMnSc1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Atlético	Atlético	k1gNnSc1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lampard	Lampard	k1gMnSc1	Lampard
odehrál	odehrát	k5eAaPmAgMnS	odehrát
celý	celý	k2eAgInSc4d1	celý
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
porážce	porážka	k1gFnSc6	porážka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
zabránit	zabránit	k5eAaPmF	zabránit
nedokázal	dokázat	k5eNaPmAgInS	dokázat
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
FA	fa	kA	fa
Cupu	cupat	k5eAaImIp1nS	cupat
proti	proti	k7c3	proti
domácímu	domácí	k2eAgInSc3d1	domácí
Southamptonu	Southampton	k1gInSc3	Southampton
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
gól	gól	k1gInSc4	gól
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
Chelsea	Chelse	k1gInSc2	Chelse
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gInSc2	Leagu
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
svým	svůj	k3xOyFgInSc7	svůj
gólem	gól	k1gInSc7	gól
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
nad	nad	k7c7	nad
domácím	domácí	k2eAgInSc7d1	domácí
West	West	k1gInSc1	West
Hamem	Ham	k1gInSc7	Ham
United	United	k1gMnSc1	United
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gMnSc1	Chelsea
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
před	před	k7c4	před
Tottenham	Tottenham	k1gInSc4	Tottenham
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Franka	Frank	k1gMnSc4	Frank
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
jubilejní	jubilejní	k2eAgMnSc1d1	jubilejní
200	[number]	k4	200
<g/>
.	.	kIx.	.
gól	gól	k1gInSc4	gól
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Chelsea	Chelse	k1gInSc2	Chelse
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
tři	tři	k4xCgInPc4	tři
góly	gól	k1gInPc4	gól
mu	on	k3xPp3gMnSc3	on
chyběly	chybět	k5eAaImAgInP	chybět
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
střelce	střelec	k1gMnSc2	střelec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Chelsea	Chelseus	k1gMnSc2	Chelseus
Bobbyho	Bobby	k1gMnSc2	Bobby
Tamblinga	Tambling	k1gMnSc2	Tambling
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rekord	rekord	k1gInSc1	rekord
překonal	překonat	k5eAaPmAgInS	překonat
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
v	v	k7c6	v
ligovém	ligový	k2eAgNnSc6d1	ligové
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Aston	Aston	k1gInSc4	Aston
Ville	Ville	k1gFnPc4	Ville
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
skóroval	skórovat	k5eAaBmAgMnS	skórovat
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gFnSc1	Chelsea
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnSc4	jeho
střeleckou	střelecký	k2eAgFnSc4d1	střelecká
potenci	potence	k1gFnSc4	potence
v	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
části	část	k1gFnSc6	část
ročníku	ročník	k1gInSc2	ročník
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
vedení	vedení	k1gNnSc1	vedení
Chelsea	Chelse	k1gInSc2	Chelse
dlouho	dlouho	k6eAd1	dlouho
otálelo	otálet	k5eAaImAgNnS	otálet
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
prodloužení	prodloužení	k1gNnSc2	prodloužení
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Frankovi	Frank	k1gMnSc3	Frank
končila	končit	k5eAaImAgFnS	končit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
předložilo	předložit	k5eAaPmAgNnS	předložit
jednoroční	jednoroční	k2eAgNnSc4d1	jednoroční
prodloužení	prodloužení	k1gNnSc4	prodloužení
kontraktu	kontrakt	k1gInSc2	kontrakt
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
proti	proti	k7c3	proti
portugalskému	portugalský	k2eAgInSc3d1	portugalský
týmu	tým	k1gInSc3	tým
Benfica	Benfic	k1gInSc2	Benfic
Lisabon	Lisabon	k1gInSc4	Lisabon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Chelsea	Chelsea	k1gFnSc1	Chelsea
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
Lampard	Lampard	k1gMnSc1	Lampard
mohl	moct	k5eAaImAgMnS	moct
zvednout	zvednout	k5eAaPmF	zvednout
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnPc4	vítěz
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k6eAd1	Chelsea
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
rok	rok	k1gInSc4	rok
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
Chelsea	Chelse	k1gInSc2	Chelse
nedostal	dostat	k5eNaPmAgMnS	dostat
adekvátní	adekvátní	k2eAgFnSc4d1	adekvátní
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2014	[number]	k4	2014
do	do	k7c2	do
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
tvořícího	tvořící	k2eAgInSc2d1	tvořící
amerického	americký	k2eAgInSc2d1	americký
klubu	klub	k1gInSc2	klub
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
silné	silný	k2eAgFnSc3d1	silná
vazbě	vazba	k1gFnSc3	vazba
na	na	k7c4	na
Chelsea	Chelseus	k1gMnSc4	Chelseus
nebude	být	k5eNaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
žádný	žádný	k3yNgInSc4	žádný
klub	klub	k1gInSc4	klub
Premier	Premier	k1gInSc1	Premier
League	League	k1gNnSc1	League
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgMnSc1d1	anglický
ani	ani	k8xC	ani
velšský	velšský	k2eAgMnSc1d1	velšský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
New	New	k1gFnSc1	New
Yorku	York	k1gInSc2	York
začíná	začínat	k5eAaImIp3nS	začínat
sezóna	sezóna	k1gFnSc1	sezóna
až	až	k9	až
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
překvapivě	překvapivě	k6eAd1	překvapivě
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
půlroční	půlroční	k2eAgNnSc4d1	půlroční
hostování	hostování	k1gNnSc4	hostování
v	v	k7c4	v
Manchester	Manchester	k1gInSc4	Manchester
City	City	k1gFnSc2	City
FC	FC	kA	FC
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
znechutil	znechutit	k5eAaPmAgMnS	znechutit
některé	některý	k3yIgMnPc4	některý
fanoušky	fanoušek	k1gMnPc4	fanoušek
Chelsea	Chelseus	k1gMnSc4	Chelseus
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
zrádce	zrádce	k1gMnPc4	zrádce
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
pálit	pálit	k5eAaImF	pálit
jeho	jeho	k3xOp3gInPc4	jeho
dresy	dres	k1gInPc4	dres
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
jeho	jeho	k3xOp3gNnSc4	jeho
hostování	hostování	k1gNnSc4	hostování
v	v	k7c6	v
City	city	k1gNnSc6	city
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
ho	on	k3xPp3gMnSc4	on
trenér	trenér	k1gMnSc1	trenér
Manuel	Manuel	k1gMnSc1	Manuel
Pellegrini	Pellegrin	k2eAgMnPc1d1	Pellegrin
vyslal	vyslat	k5eAaPmAgMnS	vyslat
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
78	[number]	k4	78
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Chelsea	Chelsea	k1gFnSc1	Chelsea
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Lampard	Lampard	k1gInSc1	Lampard
svým	svůj	k3xOyFgInSc7	svůj
gólem	gól	k1gInSc7	gól
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
na	na	k7c4	na
konečných	konečný	k2eAgInPc2d1	konečný
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
obral	obrat	k5eAaPmAgMnS	obrat
svůj	svůj	k3xOyFgInSc4	svůj
bývalý	bývalý	k2eAgInSc4d1	bývalý
klub	klub	k1gInSc4	klub
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
gól	gól	k1gInSc4	gól
neoslavoval	oslavovat	k5eNaImAgMnS	oslavovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lampard	Lampard	k1gMnSc1	Lampard
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
hostovat	hostovat	k5eAaImF	hostovat
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
trenéra	trenér	k1gMnSc2	trenér
Manuela	Manuel	k1gMnSc2	Manuel
Pellegriniho	Pellegrini	k1gMnSc2	Pellegrini
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
reprezentační	reprezentační	k2eAgInPc4d1	reprezentační
týmy	tým	k1gInPc4	tým
Anglie	Anglie	k1gFnSc2	Anglie
nastupoval	nastupovat	k5eAaImAgInS	nastupovat
už	už	k6eAd1	už
v	v	k7c6	v
mladých	mladý	k2eAgNnPc6d1	mladé
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
kapitánem	kapitán	k1gMnSc7	kapitán
anglického	anglický	k2eAgInSc2d1	anglický
týmu	tým	k1gInSc2	tým
U	u	k7c2	u
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obsadil	obsadit	k5eAaPmAgMnS	obsadit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
konaném	konaný	k2eAgNnSc6d1	konané
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nepostupové	postupový	k2eNgFnSc2d1	nepostupová
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
B.	B.	kA	B.
Lampard	Lampard	k1gInSc1	Lampard
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
gól	gól	k1gInSc4	gól
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
tak	tak	k9	tak
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
výhře	výhra	k1gFnSc3	výhra
Anglie	Anglie	k1gFnSc1	Anglie
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
seniorský	seniorský	k2eAgInSc4d1	seniorský
tým	tým	k1gInSc4	tým
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
poprvé	poprvé	k6eAd1	poprvé
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nebyl	být	k5eNaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
šampionát	šampionát	k1gInSc4	šampionát
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
EURO	euro	k1gNnSc1	euro
2000	[number]	k4	2000
ani	ani	k8xC	ani
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pořádalo	pořádat	k5eAaImAgNnS	pořádat
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
gól	gól	k1gInSc1	gól
za	za	k7c4	za
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
tým	tým	k1gInSc4	tým
vsřelil	vsřelit	k5eAaImAgMnS	vsřelit
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Anglie	Anglie	k1gFnSc1	Anglie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
forma	forma	k1gFnSc1	forma
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
prudce	prudko	k6eAd1	prudko
stoupat	stoupat	k5eAaImF	stoupat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
i	i	k9	i
pozvánku	pozvánka	k1gFnSc4	pozvánka
na	na	k7c4	na
EURO	euro	k1gNnSc4	euro
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Anglie	Anglie	k1gFnSc1	Anglie
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
proti	proti	k7c3	proti
domácímu	domácí	k2eAgInSc3d1	domácí
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejčastějším	častý	k2eAgMnSc7d3	nejčastější
střelcem	střelec	k1gMnSc7	střelec
na	na	k7c4	na
bránu	brána	k1gFnSc4	brána
soupeře	soupeř	k1gMnSc2	soupeř
s	s	k7c7	s
24	[number]	k4	24
zásahy	zásah	k1gInPc7	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Anglie	Anglie	k1gFnSc1	Anglie
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
proti	proti	k7c3	proti
Portugalsku	Portugalsko	k1gNnSc3	Portugalsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
Lampard	Lamparda	k1gFnPc2	Lamparda
neproměnil	proměnit	k5eNaPmAgMnS	proměnit
svůj	svůj	k3xOyFgInSc4	svůj
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
anglická	anglický	k2eAgFnSc1d1	anglická
reprezentace	reprezentace	k1gFnSc1	reprezentace
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
v	v	k7c6	v
osmifinálovém	osmifinálový	k2eAgInSc6d1	osmifinálový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Lampardovi	Lampardův	k2eAgMnPc1d1	Lampardův
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
utkání	utkání	k1gNnSc2	utkání
nebyl	být	k5eNaImAgMnS	být
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
pro	pro	k7c4	pro
Němce	Němec	k1gMnPc4	Němec
uznán	uznán	k2eAgInSc4d1	uznán
regulérní	regulérní	k2eAgInSc4d1	regulérní
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
domácímu	domácí	k1gMnSc3	domácí
San	San	k1gFnSc2	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
drtivým	drtivý	k2eAgNnSc7d1	drtivé
vítězstvím	vítězství	k1gNnSc7	vítězství
Anglie	Anglie	k1gFnSc2	Anglie
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Lampard	Lampard	k1gMnSc1	Lampard
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Roy	Roy	k1gMnSc1	Roy
Hodgson	Hodgson	k1gMnSc1	Hodgson
jej	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Angličané	Angličan	k1gMnPc1	Angličan
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
již	již	k6eAd1	již
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
D	D	kA	D
<g/>
,	,	kIx,	,
obsadili	obsadit	k5eAaPmAgMnP	obsadit
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
bodem	bod	k1gInSc7	bod
poslední	poslední	k2eAgFnSc4d1	poslední
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
po	po	k7c6	po
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
ukončil	ukončit	k5eAaPmAgInS	ukončit
reprezentační	reprezentační	k2eAgFnSc4d1	reprezentační
kariéru	kariéra	k1gFnSc4	kariéra
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
106	[number]	k4	106
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
29	[number]	k4	29
vstřelených	vstřelený	k2eAgInPc2d1	vstřelený
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgInPc1d1	reprezentační
zápasy	zápas	k1gInPc1	zápas
</s>
