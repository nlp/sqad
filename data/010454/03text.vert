<p>
<s>
İ	İ	k1gMnSc1	İ
Köybaşı	Köybaşı	k1gMnSc1	Köybaşı
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
turecký	turecký	k2eAgMnSc1d1	turecký
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Beşiktaş	Beşiktaş	k1gFnSc2	Beşiktaş
JK	JK	kA	JK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
İ	İ	k?	İ
Köybaşı	Köybaşı	k1gMnSc1	Köybaşı
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
za	za	k7c4	za
turecké	turecký	k2eAgInPc4d1	turecký
mládežnické	mládežnický	k2eAgInPc4d1	mládežnický
výběry	výběr	k1gInPc4	výběr
od	od	k7c2	od
kategorie	kategorie	k1gFnSc2	kategorie
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
A-týmu	Aýmo	k1gNnSc6	A-týmo
Turecka	Turecko	k1gNnSc2	Turecko
debutoval	debutovat	k5eAaBmAgInS	debutovat
12	[number]	k4	12
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
s	s	k7c7	s
domácím	domácí	k2eAgInSc7d1	domácí
týmem	tým	k1gInSc7	tým
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
İ	İ	k?	İ
Köybaşı	Köybaşı	k1gFnSc2	Köybaşı
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
Transfermarkt	Transfermarkt	k1gInSc4	Transfermarkt
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
National	National	k1gFnSc6	National
Football	Footballa	k1gFnPc2	Footballa
Teams	Teamsa	k1gFnPc2	Teamsa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
