<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
ptáček	ptáček	k1gMnSc1	ptáček
je	být	k5eAaImIp3nS	být
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
české	český	k2eAgNnSc4d1	české
jídlo	jídlo	k1gNnSc4	jídlo
připravované	připravovaný	k2eAgNnSc4d1	připravované
z	z	k7c2	z
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
jménu	jméno	k1gNnSc3	jméno
recept	recept	k1gInSc1	recept
nepochází	pocházet	k5eNaImIp3nP	pocházet
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dušený	dušený	k2eAgInSc1d1	dušený
závitek	závitek	k1gInSc1	závitek
z	z	k7c2	z
roštěnky	roštěnka	k1gFnSc2	roštěnka
v	v	k7c6	v
omáčce	omáčka	k1gFnSc6	omáčka
<g/>
,	,	kIx,	,
naplněný	naplněný	k2eAgInSc1d1	naplněný
anglickou	anglický	k2eAgFnSc7d1	anglická
slaninou	slanina	k1gFnSc7	slanina
<g/>
,	,	kIx,	,
sterilovanou	sterilovaný	k2eAgFnSc7d1	sterilovaná
okurkou	okurka	k1gFnSc7	okurka
<g/>
,	,	kIx,	,
párkem	párek	k1gInSc7	párek
a	a	k8xC	a
vajíčkem	vajíčko	k1gNnSc7	vajíčko
a	a	k8xC	a
podobající	podobající	k2eAgNnSc4d1	podobající
se	se	k3xPyFc4	se
ptáčku	ptáček	k1gInSc2	ptáček
<g/>
.	.	kIx.	.
</s>
