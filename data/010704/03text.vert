<p>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
ptáček	ptáček	k1gMnSc1	ptáček
je	být	k5eAaImIp3nS	být
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
české	český	k2eAgNnSc4d1	české
jídlo	jídlo	k1gNnSc4	jídlo
připravované	připravovaný	k2eAgNnSc4d1	připravované
z	z	k7c2	z
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
jménu	jméno	k1gNnSc3	jméno
recept	recept	k1gInSc1	recept
nepochází	pocházet	k5eNaImIp3nP	pocházet
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dušený	dušený	k2eAgInSc1d1	dušený
závitek	závitek	k1gInSc1	závitek
z	z	k7c2	z
roštěnky	roštěnka	k1gFnSc2	roštěnka
v	v	k7c6	v
omáčce	omáčka	k1gFnSc6	omáčka
<g/>
,	,	kIx,	,
naplněný	naplněný	k2eAgInSc1d1	naplněný
anglickou	anglický	k2eAgFnSc7d1	anglická
slaninou	slanina	k1gFnSc7	slanina
<g/>
,	,	kIx,	,
sterilovanou	sterilovaný	k2eAgFnSc7d1	sterilovaná
okurkou	okurka	k1gFnSc7	okurka
<g/>
,	,	kIx,	,
párkem	párek	k1gInSc7	párek
a	a	k8xC	a
vajíčkem	vajíčko	k1gNnSc7	vajíčko
a	a	k8xC	a
podobající	podobající	k2eAgNnSc4d1	podobající
se	se	k3xPyFc4	se
ptáčku	ptáček	k1gInSc2	ptáček
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
s	s	k7c7	s
kynutými	kynutý	k2eAgMnPc7d1	kynutý
nebo	nebo	k8xC	nebo
s	s	k7c7	s
houskovými	houskový	k2eAgInPc7d1	houskový
knedlíky	knedlík	k1gInPc7	knedlík
nebo	nebo	k8xC	nebo
s	s	k7c7	s
rýží	rýže	k1gFnSc7	rýže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
pokrmu	pokrm	k1gInSc2	pokrm
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
dvora	dvůr	k1gInSc2	dvůr
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Španělská	španělský	k2eAgFnSc1d1	španělská
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c6	na
císařském	císařský	k2eAgInSc6d1	císařský
dvoře	dvůr	k1gInSc6	dvůr
vařili	vařit	k5eAaImAgMnP	vařit
také	také	k9	také
španělští	španělský	k2eAgMnPc1d1	španělský
kuchaři	kuchař	k1gMnPc1	kuchař
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
také	také	k9	také
připravovali	připravovat	k5eAaImAgMnP	připravovat
telecí	telecí	k2eAgInPc4d1	telecí
závitky	závitek	k1gInPc4	závitek
s	s	k7c7	s
nádivkou	nádivka	k1gFnSc7	nádivka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
španělskou	španělský	k2eAgFnSc4d1	španělská
specialitu	specialita	k1gFnSc4	specialita
<g/>
.	.	kIx.	.
</s>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
používala	používat	k5eAaImAgFnS	používat
název	název	k1gInSc4	název
španělské	španělský	k2eAgInPc1d1	španělský
ptáčky	ptáček	k1gInPc1	ptáček
pro	pro	k7c4	pro
závitek	závitek	k1gInSc4	závitek
ze	z	k7c2	z
zeleného	zelený	k2eAgInSc2d1	zelený
listu	list	k1gInSc2	list
plněný	plněný	k2eAgInSc1d1	plněný
vepřovou	vepřový	k2eAgFnSc7d1	vepřová
sekaninou	sekanina	k1gFnSc7	sekanina
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
uvádí	uvádět	k5eAaImIp3nS	uvádět
závitek	závitek	k1gInSc1	závitek
z	z	k7c2	z
telecího	telecí	k2eAgNnSc2d1	telecí
masa	maso	k1gNnSc2	maso
plněný	plněný	k2eAgInSc1d1	plněný
sardelkou	sardelka	k1gFnSc7	sardelka
a	a	k8xC	a
žemlí	žemle	k1gFnSc7	žemle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
česká	český	k2eAgFnSc1d1	Česká
Kuchařka	kuchařka	k1gFnSc1	kuchařka
Františky	Františka	k1gFnSc2	Františka
Hansgirgové	Hansgirgový	k2eAgFnSc2d1	Hansgirgová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xC	jako
náplň	náplň	k1gFnSc1	náplň
sardelkou	sardelka	k1gFnSc7	sardelka
potřeného	potřený	k2eAgNnSc2d1	potřené
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
pouze	pouze	k6eAd1	pouze
slaninu	slanina	k1gFnSc4	slanina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
ptáček	ptáček	k1gMnSc1	ptáček
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
v	v	k7c6	v
nově	nově	k6eAd1	nově
vytvářených	vytvářený	k2eAgFnPc6d1	vytvářená
normách	norma	k1gFnPc6	norma
veřejného	veřejný	k2eAgNnSc2d1	veřejné
stravování	stravování	k1gNnSc2	stravování
<g/>
,	,	kIx,	,
použit	použit	k2eAgInSc1d1	použit
právě	právě	k9	právě
pro	pro	k7c4	pro
hovězí	hovězí	k2eAgInSc4d1	hovězí
závitek	závitek	k1gInSc4	závitek
<g/>
.	.	kIx.	.
</s>
</p>
