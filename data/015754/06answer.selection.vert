<s>
Negace	negace	k1gFnSc1
výroku	výrok	k1gInSc2
(	(	kIx(
<g/>
graficky	graficky	k6eAd1
¬	¬	k?
<g/>
,	,	kIx,
′	′	k?
<g/>
;	;	kIx,
textově	textově	k6eAd1
non	non	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
matematické	matematický	k2eAgFnSc6d1
logice	logika	k1gFnSc6
opačná	opačný	k2eAgFnSc1d1
pravdivostní	pravdivostní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
k	k	k7c3
výroku	výrok	k1gInSc3
<g/>
.	.	kIx.
</s>