<s>
Městský	městský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
</s>
<s>
Platební	platební	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
vyúčtovaná	vyúčtovaný	k2eAgFnSc1d1
teplickým	teplický	k2eAgInSc7d1
národním	národní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
</s>
<s>
Městský	městský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
MěNV	MěNV	k1gFnSc1
nebo	nebo	k8xC
MěstNV	MěstNV	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vykonávaly	vykonávat	k5eAaImAgInP
v	v	k7c6
Československu	Československo	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
správu	správa	k1gFnSc4
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
podřízené	podřízená	k1gFnPc4
okresním	okresní	k2eAgInPc3d1
národním	národní	k2eAgInPc3d1
výborům	výbor	k1gInPc3
(	(	kIx(
<g/>
ONV	ONV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Praze	Praha	k1gFnSc6
však	však	k9
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
Národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
NVP	NVP	kA
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgInS
postavený	postavený	k2eAgInSc1d1
na	na	k7c4
úroveň	úroveň	k1gFnSc4
krajským	krajský	k2eAgInPc3d1
národním	národní	k2eAgInPc3d1
výborům	výbor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
i	i	k9
v	v	k7c6
některých	některý	k3yIgNnPc6
dalších	další	k2eAgNnPc6d1
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
měly	mít	k5eAaImAgInP
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
vyšší	vysoký	k2eAgFnSc4d2
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
se	se	k3xPyFc4
městské	městský	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
transformovaly	transformovat	k5eAaBmAgInP
na	na	k7c4
městské	městský	k2eAgInPc4d1
úřady	úřad	k1gInPc4
a	a	k8xC
městská	městský	k2eAgNnPc4d1
zastupitelstva	zastupitelstvo	k1gNnPc4
<g/>
,	,	kIx,
u	u	k7c2
statutárních	statutární	k2eAgNnPc2d1
měst	město	k1gNnPc2
na	na	k7c4
úřady	úřad	k1gInPc4
města	město	k1gNnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
magistráty	magistrát	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
</s>
<s>
Městské	městský	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1983	#num#	k4
podle	podle	k7c2
nového	nový	k2eAgNnSc2d1
vymezení	vymezení	k1gNnSc2
měst	město	k1gNnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
městského	městský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
v	v	k7c6
§	§	k?
10	#num#	k4
novelizovaného	novelizovaný	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
69	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
národních	národní	k2eAgInPc6d1
výborech	výbor	k1gInPc6
<g/>
,	,	kIx,
rozdělovaly	rozdělovat	k5eAaImAgFnP
do	do	k7c2
několika	několik	k4yIc2
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
první	první	k4xOgFnSc6
kategorii	kategorie	k1gFnSc6
<g/>
,	,	kIx,
mimo	mimo	k7c4
měst	město	k1gNnPc2
se	s	k7c7
zvláštním	zvláštní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
krajská	krajský	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
,	,	kIx,
další	další	k2eAgNnPc1d1
významná	významný	k2eAgNnPc1d1
velká	velký	k2eAgNnPc1d1
města	město	k1gNnPc1
se	se	k3xPyFc4
zvlášť	zvlášť	k6eAd1
rozvinutou	rozvinutý	k2eAgFnSc7d1
hospodářskou	hospodářský	k2eAgFnSc7d1
<g/>
,	,	kIx,
politickou	politický	k2eAgFnSc7d1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgFnSc7d1
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc7d1
a	a	k8xC
správní	správní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
a	a	k8xC
významná	významný	k2eAgFnSc1d1
lázeňská	lázeňská	k1gFnSc1
města	město	k1gNnSc2
(	(	kIx(
<g/>
stanovila	stanovit	k5eAaPmAgFnS
je	být	k5eAaImIp3nS
nařízením	nařízení	k1gNnSc7
vláda	vláda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
kategorii	kategorie	k1gFnSc6
okresní	okresní	k2eAgFnSc1d1
města	město	k1gNnSc2
a	a	k8xC
další	další	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
byla	být	k5eAaImAgNnP
významnými	významný	k2eAgMnPc7d1
středisky	středisko	k1gNnPc7
osídlení	osídlení	k1gNnSc2
a	a	k8xC
byla	být	k5eAaImAgNnP
vybavena	vybavit	k5eAaPmNgNnP
zařízeními	zařízení	k1gNnPc7
a	a	k8xC
provozovnami	provozovna	k1gFnPc7
pro	pro	k7c4
zabezpečování	zabezpečování	k1gNnSc4
služeb	služba	k1gFnPc2
a	a	k8xC
potřeb	potřeba	k1gFnPc2
svých	svůj	k3xOyFgMnPc2
obyvatel	obyvatel	k1gMnPc2
i	i	k8xC
jejich	jejich	k3xOp3gNnSc2
spádového	spádový	k2eAgNnSc2d1
území	území	k1gNnSc2
(	(	kIx(
<g/>
určoval	určovat	k5eAaImAgInS
je	on	k3xPp3gMnPc4
krajský	krajský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnPc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
ČSR	ČSR	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c6
SSR	SSR	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
hl.	hl.	k?
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
hl.	hl.	k?
město	město	k1gNnSc1
SSR	SSR	kA
Bratislava	Bratislava	k1gFnSc1
<g/>
11	#num#	k4
</s>
<s>
ostatní	ostatní	k2eAgNnPc1d1
města	město	k1gNnPc1
se	se	k3xPyFc4
zvláštním	zvláštní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
<g/>
31	#num#	k4
</s>
<s>
města	město	k1gNnPc1
s	s	k7c7
MěNV	MěNV	k1gFnSc7
1	#num#	k4
<g/>
.	.	kIx.
kategorie	kategorie	k1gFnSc2
<g/>
239	#num#	k4
</s>
<s>
města	město	k1gNnPc1
s	s	k7c7
MěNV	MěNV	k1gFnSc7
2	#num#	k4
<g/>
.	.	kIx.
kategorie	kategorie	k1gFnSc2
<g/>
18183	#num#	k4
</s>
<s>
obce	obec	k1gFnPc1
městského	městský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
s	s	k7c7
MěNV	MěNV	k1gFnSc7
3	#num#	k4
<g/>
.	.	kIx.
kategorie	kategorie	k1gFnSc2
<g/>
22430	#num#	k4
</s>
<s>
celkem	celkem	k6eAd1
<g/>
432124	#num#	k4
</s>
<s>
Počty	počet	k1gInPc1
MěNV	MěNV	k1gMnPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1983	#num#	k4
(	(	kIx(
<g/>
mezi	mezi	k7c4
1	#num#	k4
<g/>
.	.	kIx.
lednem	leden	k1gInSc7
1983	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
lednem	leden	k1gInSc7
1988	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc2
změnám	změna	k1gFnPc3
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
u	u	k7c2
několika	několik	k4yIc2
měst	město	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Městy	město	k1gNnPc7
v	v	k7c6
první	první	k4xOgFnSc6
kategorii	kategorie	k1gFnSc6
byly	být	k5eAaImAgFnP
<g/>
:	:	kIx,
Kladno	Kladno	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Chomutov	Chomutov	k1gInSc1
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Most	most	k1gInSc1
<g/>
,	,	kIx,
Teplice	Teplice	k1gFnPc1
<g/>
,	,	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Pardubice	Pardubice	k1gInPc1
<g/>
,	,	kIx,
Gottwaldov	Gottwaldov	k1gInSc1
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
<g/>
,	,	kIx,
Prostějov	Prostějov	k1gInSc1
<g/>
,	,	kIx,
Frýdek-Místek	Frýdek-Místek	k1gInSc1
<g/>
,	,	kIx,
Havířov	Havířov	k1gInSc1
<g/>
,	,	kIx,
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Opava	Opava	k1gFnSc1
<g/>
,	,	kIx,
Přerov	Přerov	k1gInSc1
a	a	k8xC
Třinec	Třinec	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
§	§	k?
27	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
a	a	k8xC
§	§	k?
58	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
4	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
367	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
obcích	obec	k1gFnPc6
(	(	kIx(
<g/>
obecní	obecní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.1	.1	k4
2	#num#	k4
Sborník	sborník	k1gInSc1
ČGS	ČGS	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
108	#num#	k4
<g/>
–	–	k?
<g/>
1091	#num#	k4
2	#num#	k4
Sborník	sborník	k1gInSc1
ČGS	ČGS	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
110	#num#	k4
<g/>
↑	↑	k?
Sborník	sborník	k1gInSc1
ČGS	ČGS	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
113	#num#	k4
<g/>
↑	↑	k?
Nařízení	nařízení	k1gNnSc4
vlády	vláda	k1gFnSc2
č.	č.	k?
152	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
ANDRLE	Andrle	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
;	;	kIx,
SRP	srp	k1gInSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	Nová	k1gFnSc1
koncepce	koncepce	k1gFnSc2
pojmů	pojem	k1gInPc2
město	město	k1gNnSc1
a	a	k8xC
venkov	venkov	k1gInSc1
a	a	k8xC
její	její	k3xOp3gInSc4
význam	význam	k1gInSc4
pro	pro	k7c4
geografii	geografie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
Československé	československý	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
