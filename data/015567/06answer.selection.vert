<s>
Městský	městský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
MěNV	MěNV	k1gFnSc1
nebo	nebo	k8xC
MěstNV	MěstNV	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vykonávaly	vykonávat	k5eAaImAgInP
v	v	k7c6
Československu	Československo	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
správu	správa	k1gFnSc4
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>