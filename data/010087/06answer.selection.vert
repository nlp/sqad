<s>
Prstové	prstový	k2eAgFnPc1d1	prstová
abecedy	abeceda	k1gFnPc1	abeceda
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
i	i	k9	i
pro	pro	k7c4	pro
jiná	jiný	k2eAgNnPc4d1	jiné
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
než	než	k8xS	než
latinku	latinka	k1gFnSc4	latinka
(	(	kIx(	(
<g/>
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
japonština	japonština	k1gFnSc1	japonština
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
