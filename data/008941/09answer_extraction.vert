Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
Ladakem	Ladak	k1gInSc7	Ladak
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
státě	stát	k1gInSc6	stát
Džammú	Džammú	k1gFnSc1	Džammú
a	a	k8xC	a
Kašmír	Kašmír	k1gInSc1	Kašmír
přes	přes	k7c4	přes
pákistánskou	pákistánský	k2eAgFnSc4d1	pákistánská
část	část	k1gFnSc4	část
Kašmíru	Kašmír	k1gInSc2	Kašmír
Gilgit-Baltistán	Gilgit-Baltistán	k1gInSc1	Gilgit-Baltistán
a	a	k8xC	a
provincie	provincie	k1gFnSc1	provincie
Chajbar	Chajbar	k1gInSc1	Chajbar
Paštúnchwá	Paštúnchwá	k1gFnSc1	Paštúnchwá
<g/>
,	,	kIx,	,
Paňdžáb	Paňdžáb	k1gInSc1	Paňdžáb
a	a	k8xC	a
Sindh	Sindh	k1gInSc1	Sindh
