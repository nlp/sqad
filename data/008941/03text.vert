<p>
<s>
Indus	Indus	k1gInSc1	Indus
(	(	kIx(	(
<g/>
tibetsky	tibetsky	k6eAd1	tibetsky
ས	ས	k?	ས
<g/>
ེ	ེ	k?	ེ
<g/>
ང	ང	k?	ང
<g/>
་	་	k?	་
<g/>
ག	ག	k?	ག
<g/>
ེ	ེ	k?	ེ
<g/>
་	་	k?	་
<g/>
ཁ	ཁ	k?	ཁ
<g/>
་	་	k?	་
<g/>
འ	འ	k?	འ
<g/>
,	,	kIx,	,
wylie	wylie	k1gFnSc1	wylie
Seng-ge	Senge	k1gFnSc1	Seng-ge
kha-	kha-	k?	kha-
<g/>
'	'	kIx"	'
<g/>
abb	abb	k?	abb
<g/>
,	,	kIx,	,
Sê	Sê	k1gMnSc5	Sê
Zangbo	Zangba	k1gMnSc5	Zangba
(	(	kIx(	(
<g/>
Lví	lví	k2eAgFnSc1d1	lví
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Jin-tu-che	Jinuh	k1gFnSc2	Jin-tu-ch
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
pinyin	pinyin	k1gMnSc1	pinyin
Yì	Yì	k1gMnSc1	Yì
Hé	hé	k0	hé
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
印	印	k?	印
<g/>
,	,	kIx,	,
urdsky	urdsky	k6eAd1	urdsky
د	د	k?	د
س	س	k?	س
[	[	kIx(	[
<g/>
Daryā	Daryā	k1gMnSc4	Daryā
Sindhu	Sindha	k1gMnSc4	Sindha
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
paňdžábsky	paňdžábsky	k6eAd1	paňdžábsky
ਸ	ਸ	k?	ਸ
<g/>
ਿ	ਿ	k?	ਿ
<g/>
ੰ	ੰ	k?	ੰ
<g/>
ਧ	ਧ	k?	ਧ
ਦ	ਦ	k?	ਦ
<g/>
ਿ	ਿ	k?	ਿ
<g/>
ਆ	ਆ	k?	ਆ
[	[	kIx(	[
<g/>
Siñ	Siñ	k1gMnSc1	Siñ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
س	س	k?	س
[	[	kIx(	[
<g/>
Sindhu	Sindh	k1gInSc2	Sindh
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
paštunsky	paštunsky	k6eAd1	paštunsky
ا	ا	k?	ا
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
Ā	Ā	k?	Ā
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
sindhsky	sindhsky	k6eAd1	sindhsky
س	س	k?	س
[	[	kIx(	[
<g/>
Sindhū	Sindhū	k1gMnSc1	Sindhū
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
स	स	k?	स
<g/>
ि	ि	k?	ि
<g/>
न	न	k?	न
<g/>
्	्	k?	्
<g/>
ध	ध	k?	ध
<g/>
ू	ू	k?	ू
<g/>
न	न	k?	न
<g/>
ी	ी	k?	ी
[	[	kIx(	[
<g/>
Sindhū	Sindhū	k1gMnSc1	Sindhū
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
hindsky	hindsky	k6eAd1	hindsky
स	स	k?	स
<g/>
ि	ि	k?	ि
<g/>
न	न	k?	न
<g/>
्	्	k?	्
<g/>
ध	ध	k?	ध
<g/>
ु	ु	k?	ु
न	न	k?	न
<g/>
ी	ी	k?	ी
[	[	kIx(	[
<g/>
Sindhu	Sindha	k1gMnSc4	Sindha
nadī	nadī	k?	nadī
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
persky	persky	k6eAd1	persky
س	س	k?	س
[	[	kIx(	[
<g/>
Sind	Sind	k1gMnSc1	Sind
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Ι	Ι	k?	Ι
[	[	kIx(	[
<g/>
Indos	Indos	k1gMnSc1	Indos
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
Ladakem	Ladak	k1gInSc7	Ladak
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
státě	stát	k1gInSc6	stát
Džammú	Džammú	k1gFnSc1	Džammú
a	a	k8xC	a
Kašmír	Kašmír	k1gInSc1	Kašmír
přes	přes	k7c4	přes
pákistánskou	pákistánský	k2eAgFnSc4d1	pákistánská
část	část	k1gFnSc4	část
Kašmíru	Kašmír	k1gInSc2	Kašmír
Gilgit-Baltistán	Gilgit-Baltistán	k1gInSc1	Gilgit-Baltistán
a	a	k8xC	a
provincie	provincie	k1gFnSc1	provincie
Chajbar	Chajbar	k1gInSc1	Chajbar
Paštúnchwá	Paštúnchwá	k1gFnSc1	Paštúnchwá
<g/>
,	,	kIx,	,
Paňdžáb	Paňdžáb	k1gInSc1	Paňdžáb
a	a	k8xC	a
Sindh	Sindh	k1gInSc1	Sindh
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Indii	Indie	k1gFnSc4	Indie
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
řecké	řecký	k2eAgInPc4d1	řecký
Sinthos	Sinthos	k1gInSc4	Sinthos
a	a	k8xC	a
latinské	latinský	k2eAgInPc4d1	latinský
Sindus	Sindus	k1gInSc4	Sindus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozdělením	rozdělení	k1gNnSc7	rozdělení
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
na	na	k7c4	na
Pákistán	Pákistán	k1gInSc4	Pákistán
a	a	k8xC	a
Indii	Indie	k1gFnSc4	Indie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Ganze	Ganga	k1gFnSc6	Ganga
druhou	druhý	k4xOgFnSc7	druhý
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
řekou	řeka	k1gFnSc7	řeka
této	tento	k3xDgFnSc2	tento
britské	britský	k2eAgFnSc2d1	britská
kolonie	kolonie	k1gFnSc2	kolonie
–	–	k?	–
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
geografického	geografický	k2eAgNnSc2d1	geografické
<g/>
,	,	kIx,	,
kulturního	kulturní	k2eAgNnSc2d1	kulturní
i	i	k8xC	i
obchodního	obchodní	k2eAgNnSc2d1	obchodní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
3	[number]	k4	3
180	[number]	k4	180
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
980	[number]	k4	980
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Indus	Indus	k1gInSc1	Indus
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Transhimálaji	Transhimálaj	k1gInSc6	Transhimálaj
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
posvátné	posvátný	k2eAgFnSc2d1	posvátná
hory	hora	k1gFnSc2	hora
Kailás	Kailása	k1gFnPc2	Kailása
(	(	kIx(	(
<g/>
asi	asi	k9	asi
12	[number]	k4	12
km	km	kA	km
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
5	[number]	k4	5
600	[number]	k4	600
m.	m.	k?	m.
Tibeťané	Tibeťan	k1gMnPc1	Tibeťan
této	tento	k3xDgFnSc3	tento
řece	řeka	k1gFnSc3	řeka
říkají	říkat	k5eAaImIp3nP	říkat
Sê	Sê	k1gMnSc5	Sê
Zangbo	Zangba	k1gMnSc5	Zangba
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sindhu	Sindh	k1gInSc2	Sindh
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
až	až	k9	až
na	na	k7c6	na
indicko-pákistánském	indickoákistánský	k2eAgNnSc6d1	indicko-pákistánský
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
Sê	Sê	k1gFnSc1	Sê
Zangbo	Zangba	k1gFnSc5	Zangba
ztotožňována	ztotožňován	k2eAgMnSc4d1	ztotožňován
s	s	k7c7	s
Indem	Indus	k1gInSc7	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Garem	Garum	k1gNnSc7	Garum
řeka	řeka	k1gFnSc1	řeka
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Kašmíru	Kašmír	k1gInSc2	Kašmír
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Ladaku	Ladak	k1gInSc2	Ladak
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
dnem	dnem	k7c2	dnem
hluboké	hluboký	k2eAgFnSc2d1	hluboká
tektonické	tektonický	k2eAgFnSc2d1	tektonická
doliny	dolina	k1gFnSc2	dolina
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zde	zde	k6eAd1	zde
mnohé	mnohý	k2eAgFnPc4d1	mnohá
skalní	skalní	k2eAgFnPc4d1	skalní
soutěsky	soutěska	k1gFnPc4	soutěska
a	a	k8xC	a
kaňony	kaňon	k1gInPc4	kaňon
<g/>
.	.	kIx.	.
</s>
<s>
Nejdivočejší	divoký	k2eAgMnPc1d3	nejdivočejší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
kaňon	kaňon	k1gInSc4	kaňon
Rondu	rond	k1gInSc2	rond
v	v	k7c6	v
Baltistánu	Baltistán	k1gInSc6	Baltistán
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Gilgitu	Gilgit	k1gInSc2	Gilgit
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
a	a	k8xC	a
západně	západně	k6eAd1	západně
od	od	k7c2	od
Nanga	Nang	k1gMnSc2	Nang
Parbatu	Parbat	k1gInSc2	Parbat
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
západní	západní	k2eAgFnSc1d1	západní
Himálaj	Himálaj	k1gFnSc1	Himálaj
od	od	k7c2	od
pohoří	pohoří	k1gNnSc2	pohoří
Karákóram	Karákóram	k1gInSc1	Karákóram
a	a	k8xC	a
Hindúkuš	Hindúkuš	k1gInSc1	Hindúkuš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
velký	velký	k2eAgInSc1d1	velký
přítok	přítok	k1gInSc1	přítok
Kábul	Kábul	k1gInSc1	Kábul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
Indu	Ind	k1gMnSc3	Ind
téměř	téměř	k6eAd1	téměř
vyrovná	vyrovnat	k5eAaPmIp3nS	vyrovnat
vodnatostí	vodnatost	k1gFnSc7	vodnatost
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
Kalabagskou	Kalabagský	k2eAgFnSc7d1	Kalabagský
soutěskou	soutěska	k1gFnSc7	soutěska
mezi	mezi	k7c4	mezi
výběžky	výběžek	k1gInPc4	výběžek
Sulejmanových	Sulejmanův	k2eAgFnPc2d1	Sulejmanova
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Solného	solný	k2eAgInSc2d1	solný
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
Indoganžské	Indoganžský	k2eAgFnSc2d1	Indoganžská
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
po	po	k7c6	po
dně	dno	k1gNnSc6	dno
široké	široký	k2eAgFnSc2d1	široká
doliny	dolina	k1gFnSc2	dolina
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
Pákistánu	Pákistán	k1gInSc2	Pákistán
planinami	planina	k1gFnPc7	planina
Paňdžábu	Paňdžáb	k1gInSc2	Paňdžáb
a	a	k8xC	a
Sindhu	Sindh	k1gInSc2	Sindh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
tok	tok	k1gInSc1	tok
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
a	a	k8xC	a
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
průtoků	průtok	k1gInPc2	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
posledním	poslední	k2eAgInSc7d1	poslední
velkým	velký	k2eAgInSc7d1	velký
přítokem	přítok	k1gInSc7	přítok
řekou	řeka	k1gFnSc7	řeka
Paňdžáb	Paňdžáb	k1gInSc4	Paňdžáb
šířka	šířka	k1gFnSc1	šířka
koryta	koryto	k1gNnSc2	koryto
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
ze	z	k7c2	z
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
m	m	kA	m
na	na	k7c4	na
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
záplav	záplava	k1gFnPc2	záplava
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
km	km	kA	km
a	a	k8xC	a
u	u	k7c2	u
města	město	k1gNnSc2	město
Deraismailchán	Deraismailchán	k2eAgInSc1d1	Deraismailchán
20	[number]	k4	20
až	až	k8xS	až
22	[number]	k4	22
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
hrázemi	hráz	k1gFnPc7	hráz
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
okolní	okolní	k2eAgNnSc4d1	okolní
území	území	k1gNnSc4	území
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
řeka	řeka	k1gFnSc1	řeka
protíná	protínat	k5eAaImIp3nS	protínat
západní	západní	k2eAgInSc4d1	západní
okraj	okraj	k1gInSc4	okraj
pouště	poušť	k1gFnSc2	poušť
Thár	Thára	k1gFnPc2	Thára
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Hajdarábádem	Hajdarábád	k1gInSc7	Hajdarábád
začíná	začínat	k5eAaImIp3nS	začínat
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
000	[number]	k4	000
km2	km2	k4	km2
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
nesplavná	splavný	k2eNgFnSc1d1	nesplavná
říční	říční	k2eAgFnSc1d1	říční
delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
11	[number]	k4	11
základních	základní	k2eAgNnPc2d1	základní
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Karáčí	Karáčí	k1gNnSc2	Karáčí
do	do	k7c2	do
Arabského	arabský	k2eAgNnSc2d1	arabské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přítoky	přítok	k1gInPc4	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
P	P	kA	P
Šajók	Šajók	k1gMnSc1	Šajók
</s>
</p>
<p>
<s>
P	P	kA	P
Šigar	Šigar	k1gMnSc1	Šigar
</s>
</p>
<p>
<s>
P	P	kA	P
Gilgit	Gilgit	k1gMnSc1	Gilgit
</s>
</p>
<p>
<s>
P	P	kA	P
Kábul	Kábul	k1gInSc1	Kábul
</s>
</p>
<p>
<s>
L	L	kA	L
Soán	Soán	k1gMnSc1	Soán
</s>
</p>
<p>
<s>
P	P	kA	P
Kurra	Kurra	k1gFnSc1	Kurra
</s>
</p>
<p>
<s>
L	L	kA	L
Pandžnad	Pandžnad	k1gInSc1	Pandžnad
(	(	kIx(	(
<g/>
Satledž	Satledž	k1gFnSc1	Satledž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
především	především	k6eAd1	především
tající	tající	k2eAgInSc1d1	tající
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
zbývající	zbývající	k2eAgFnSc6d1	zbývající
části	část	k1gFnSc6	část
povodí	povodí	k1gNnSc2	povodí
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
monzunové	monzunový	k2eAgInPc1d1	monzunový
deště	dešť	k1gInPc1	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
léto	léto	k1gNnSc4	léto
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
stoupá	stoupat	k5eAaImIp3nS	stoupat
o	o	k7c4	o
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
m	m	kA	m
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
a	a	k8xC	a
o	o	k7c4	o
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
m	m	kA	m
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
Hajdarábádu	Hajdarábád	k1gInSc2	Hajdarábád
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
850	[number]	k4	850
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
nejvodnatějších	vodnatý	k2eAgNnPc6d3	nejvodnatější
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenán	k2eAgInPc4d1	zaznamenán
průtoky	průtok	k1gInPc4	průtok
až	až	k9	až
30	[number]	k4	30
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
suchých	suchý	k2eAgNnPc6d1	suché
obdobích	období	k1gNnPc6	období
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
zcela	zcela	k6eAd1	zcela
vysychá	vysychat	k5eAaImIp3nS	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
řeka	řeka	k1gFnSc1	řeka
přinese	přinést	k5eAaPmIp3nS	přinést
450	[number]	k4	450
Mt	Mt	k1gFnPc2	Mt
nánosů	nános	k1gInPc2	nános
<g/>
.	.	kIx.	.
</s>
<s>
Indus	Indus	k1gInSc1	Indus
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
(	(	kIx(	(
<g/>
slapová	slapový	k2eAgFnSc1d1	slapová
<g/>
,	,	kIx,	,
ne	ne	k9	ne
tzv.	tzv.	kA	tzv.
tsunami	tsunami	k1gNnSc4	tsunami
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Indu	Indus	k1gInSc2	Indus
je	být	k5eAaImIp3nS	být
zavlažováno	zavlažovat	k5eAaImNgNnS	zavlažovat
110	[number]	k4	110
000	[number]	k4	000
km2	km2	k4	km2
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Paňdžábu	Paňdžáb	k1gInSc6	Paňdžáb
a	a	k8xC	a
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
14	[number]	k4	14
nevelkých	velký	k2eNgFnPc2d1	nevelká
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
Sukkurská	Sukkurský	k2eAgFnSc1d1	Sukkurský
přehrada	přehrada	k1gFnSc1	přehrada
u	u	k7c2	u
města	město	k1gNnSc2	město
Sukkur	Sukkura	k1gFnPc2	Sukkura
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
systémů	systém	k1gInPc2	systém
zavlažuje	zavlažovat	k5eAaImIp3nS	zavlažovat
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
zavlažovacích	zavlažovací	k2eAgInPc2d1	zavlažovací
kanálů	kanál	k1gInPc2	kanál
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
běžného	běžný	k2eAgInSc2d1	běžný
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
splavná	splavný	k2eAgFnSc1d1	splavná
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
ke	k	k7c3	k
městu	město	k1gNnSc3	město
Deraismailchán	Deraismailchán	k2eAgInSc4d1	Deraismailchán
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
lodě	loď	k1gFnPc1	loď
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
ponorem	ponor	k1gInSc7	ponor
plují	plout	k5eAaImIp3nP	plout
výše	vysoce	k6eAd2	vysoce
až	až	k9	až
do	do	k7c2	do
města	město	k1gNnSc2	město
Attok	Attok	k1gInSc1	Attok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Kábul	Kábul	k1gInSc1	Kábul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
osídlení	osídlení	k1gNnSc2	osídlení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
významná	významný	k2eAgFnSc1d1	významná
lidská	lidský	k2eAgFnSc1d1	lidská
civilizace	civilizace	k1gFnSc1	civilizace
s	s	k7c7	s
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
civilizace	civilizace	k1gFnSc1	civilizace
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
mezopotámské	mezopotámský	k2eAgFnSc2d1	mezopotámská
<g/>
,	,	kIx,	,
egyptské	egyptský	k2eAgFnSc2d1	egyptská
a	a	k8xC	a
čínské	čínský	k2eAgFnSc2d1	čínská
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
starověkých	starověký	k2eAgFnPc2d1	starověká
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
města	město	k1gNnPc1	město
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Harappa	Harappa	k1gFnSc1	Harappa
a	a	k8xC	a
Mohendžodaro	Mohendžodara	k1gFnSc5	Mohendžodara
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
sídlištím	sídliště	k1gNnPc3	sídliště
starověku	starověk	k1gInSc2	starověk
období	období	k1gNnSc4	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
3	[number]	k4	3
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
И	И	k?	И
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
řek	řeka	k1gFnPc2	řeka
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Indus	Indus	k1gInSc1	Indus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Indus	Indus	k1gInSc1	Indus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
