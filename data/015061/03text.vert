<s>
HM	HM	k?
Cancri	Cancri	k1gNnSc1
</s>
<s>
HM	HM	k?
Cancri	Cancri	k1gNnSc1
Pozorovací	pozorovací	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
dvojhvězda	dvojhvězda	k1gFnSc1
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Rak	rak	k1gMnSc1
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
21,2	21,2	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
21,206	21,206	k4
8	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
20,7	20,7	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
20,74	20,74	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
21,221	21,221	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
20,334	20,334	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
21,539	21,539	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
21,679	21,679	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Označení	označení	k1gNnSc1
v	v	k7c6
katalozích	katalog	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
RX	RX	kA
J	J	kA
<g/>
0	#num#	k4
<g/>
806.3	806.3	k4
<g/>
+	+	kIx~
<g/>
1527	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
podle	podle	k7c2
katalogu	katalog	k1gInSc2
proměnných	proměnný	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
HM	HM	k?
Cancri	Cancri	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejtěsněji	těsně	k6eAd3
obíhající	obíhající	k2eAgFnSc1d1
známá	známý	k2eAgFnSc1d1
dvojhvězda	dvojhvězda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
dvojicí	dvojice	k1gFnSc7
bílých	bílý	k2eAgMnPc2d1
trpaslíků	trpaslík	k1gMnPc2
o	o	k7c6
hmotnostech	hmotnost	k1gFnPc6
0,55	0,55	k4
a	a	k8xC
0,27	0,27	k4
hmotnosti	hmotnost	k1gFnSc2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
obíhají	obíhat	k5eAaImIp3nP
kolem	kolem	k7c2
společného	společný	k2eAgNnSc2d1
těžiště	těžiště	k1gNnSc2
s	s	k7c7
periodou	perioda	k1gFnSc7
321,5	321,5	k4
sekund	sekunda	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
21,5	21,5	k4
sekundy	sekunda	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
hvězd	hvězda	k1gFnPc2
zhruba	zhruba	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
velikosti	velikost	k1gFnPc4
Země	zem	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
vzdálenost	vzdálenost	k1gFnSc4
–	–	k?
80	#num#	k4
000	#num#	k4
km	km	kA
–	–	k?
zhruba	zhruba	k6eAd1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
vzdálenosti	vzdálenost	k1gFnPc4
Země	zem	k1gFnSc2
a	a	k8xC
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
1	#num#	k4
600	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
od	od	k7c2
Sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rotace	rotace	k1gFnSc1
systému	systém	k1gInSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zpomaluje	zpomalovat	k5eAaImIp3nS
–	–	k?
o	o	k7c4
zhruba	zhruba	k6eAd1
1,2	1,2	k4
ms	ms	k?
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
ztrátě	ztráta	k1gFnSc3
energie	energie	k1gFnSc2
vyzařováním	vyzařování	k1gNnSc7
gravitačních	gravitační	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
<g/>
,	,	kIx,
spočítané	spočítaný	k2eAgFnSc2d1
podle	podle	k7c2
obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
relativity	relativita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgInPc2
předpokladů	předpoklad	k1gInPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
nejsilnější	silný	k2eAgInSc4d3
zdroj	zdroj	k1gInSc4
gravitačních	gravitační	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
galaxii	galaxie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
78	#num#	k4
<g/>
th	th	k?
name-list	name-list	k1gMnSc1
of	of	k?
variable	variable	k6eAd1
stars	stars	k6eAd1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gaia	Gaia	k1gFnSc1
Data	datum	k1gNnSc2
Release	Releasa	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SIMBAD	SIMBAD	kA
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
The	The	k1gFnPc2
SDSS	SDSS	kA
Photometric	Photometric	k1gMnSc1
Catalog	Catalog	k1gMnSc1
<g/>
,	,	kIx,
Release	Releas	k1gInSc6
7	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.astrovm.cz/cz/pro-navstevniky/novinky_obr/zbesily-tanec-dvojhvezdy/364.html	http://www.astrovm.cz/cz/pro-navstevniky/novinky_obr/zbesily-tanec-dvojhvezdy/364.html	k1gMnSc1
</s>
<s>
http://www.nasa.gov/centers/marshall/news/news/releases/2005/05-078.html	http://www.nasa.gov/centers/marshall/news/news/releases/2005/05-078.html	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
