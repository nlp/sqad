<s>
Meitnerium	Meitnerium	k1gNnSc1
</s>
<s>
Meitnerium	Meitnerium	k1gNnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
7	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
276	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mt	Mt	kA
</s>
<s>
109	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Meitnerium	Meitnerium	k1gNnSc1
<g/>
,	,	kIx,
Mt	Mt	k1gFnSc1
<g/>
,	,	kIx,
109	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Meitnerium	Meitnerium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54038-01-6	54038-01-6	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
276,15	276,15	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
předpokládaný	předpokládaný	k2eAgInSc1d1
122	#num#	k4
pm	pm	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
předpokládaný	předpokládaný	k2eAgInSc1d1
129	#num#	k4
pm	pm	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
7	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
předpokládané	předpokládaný	k2eAgInPc1d1
IX	IX	kA
<g/>
,	,	kIx,
VIII	VIII	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
I	i	k9
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
800,8	800,8	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
1	#num#	k4
823,6	823,6	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
2	#num#	k4
904,2	904,2	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
krychlová	krychlový	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
37,4	37,4	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgInPc4d1
pevné	pevný	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgInPc4d1
paramagnetické	paramagnetický	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ir	Ir	k1gMnSc1
<g/>
⋏	⋏	k?
</s>
<s>
Hassium	Hassium	k1gNnSc1
≺	≺	k?
<g/>
Mt	Mt	k1gMnSc2
<g/>
≻	≻	k?
Darmstadtium	Darmstadtium	k1gNnSc1
</s>
<s>
Meitnerium	Meitnerium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Mt	Mt	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sedmnáctým	sedmnáctý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
nebo	nebo	k8xC
urychlovači	urychlovač	k1gInSc6
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Meitnerium	Meitnerium	k1gNnSc1
doposud	doposud	k6eAd1
nebylo	být	k5eNaImAgNnS
izolováno	izolovat	k5eAaBmNgNnS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
poloze	poloha	k1gFnSc6
v	v	k7c6
Periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
prvků	prvek	k1gInPc2
by	by	k9
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
mělo	mít	k5eAaImAgNnS
připomínat	připomínat	k5eAaImF
iridium	iridium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Lise	Lisa	k1gFnSc3
Meitnerová	Meitnerová	k1gFnSc1
a	a	k8xC
Otto	Otto	k1gMnSc1
Hahn	Hahn	k1gMnSc1
</s>
<s>
První	první	k4xOgFnSc4
přípravu	příprava	k1gFnSc4
prvku	prvek	k1gInSc2
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
109	#num#	k4
oznámili	oznámit	k5eAaPmAgMnP
němečtí	německý	k2eAgMnPc1d1
fyzici	fyzik	k1gMnPc1
Peter	Peter	k1gMnSc1
Armbruster	Armbruster	k1gMnSc1
a	a	k8xC
Gottfried	Gottfried	k1gMnSc1
Münzenberg	Münzenberg	k1gMnSc1
z	z	k7c2
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
těžkých	těžký	k2eAgInPc2d1
iontů	ion	k1gInPc2
v	v	k7c6
německém	německý	k2eAgInSc6d1
Darmstadtu	Darmstadt	k1gInSc6
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Bombardováním	bombardování	k1gNnSc7
izotopu	izotop	k1gInSc2
bismutu	bismut	k1gInSc2
jádry	jádro	k1gNnPc7
atomu	atom	k1gInSc6
železa	železo	k1gNnSc2
získali	získat	k5eAaPmAgMnP
izotop	izotop	k1gInSc4
266	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
přibližně	přibližně	k6eAd1
1,7	1,7	k4
ms.	ms.	k?
</s>
<s>
20983	#num#	k4
Bi	Bi	k1gFnSc1
+	+	kIx~
5826	#num#	k4
Fe	Fe	k1gFnSc2
→	→	k?
266109	#num#	k4
Mt	Mt	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
Prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
po	po	k7c6
rakouské	rakouský	k2eAgFnSc6d1
matematičce	matematička	k1gFnSc6
a	a	k8xC
fyzičce	fyzička	k1gFnSc3
Lise	Lisa	k1gFnSc3
Meitnerové	Meitnerová	k1gFnSc2
a	a	k8xC
zasedání	zasedání	k1gNnSc2
IUPAC	IUPAC	kA
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
toto	tento	k3xDgNnSc1
pojmenování	pojmenování	k1gNnSc1
schválilo	schválit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Doposud	doposud	k6eAd1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
následujících	následující	k2eAgInPc2d1
15	#num#	k4
izotopů	izotop	k1gInPc2
meitneria	meitnerium	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
IzotopRok	IzotopRok	k1gInSc1
objevuReakcePoločas	objevuReakcePoločasa	k1gFnPc2
rozpadu	rozpad	k1gInSc2
</s>
<s>
265	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
266	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
1982209	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
<g/>
(	(	kIx(
<g/>
58	#num#	k4
<g/>
Fe	Fe	k1gMnSc2
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
1,7	1,7	k4
ms	ms	k?
</s>
<s>
267	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
268	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
1994209	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
21	#num#	k4
ms	ms	k?
</s>
<s>
269	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
270	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
2004209	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
<g/>
(	(	kIx(
<g/>
70	#num#	k4
<g/>
Zn	zn	kA
<g/>
,	,	kIx,
<g/>
n	n	k0
)	)	kIx)
<g/>
5,0	5,0	k4
ms	ms	k?
</s>
<s>
271	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
272	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
273	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
274	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
2006237	#num#	k4
<g/>
Np	Np	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,44	0,44	k4
s	s	k7c7
</s>
<s>
275	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
9,7	9,7	k4
ms	ms	k?
</s>
<s>
276	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,72	0,72	k4
s	s	k7c7
</s>
<s>
277	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
5	#num#	k4
s	s	k7c7
</s>
<s>
278	#num#	k4
<g/>
Mt	Mt	k1gFnSc1
<g/>
2009294	#num#	k4
<g/>
Ts	ts	k0
<g/>
(	(	kIx(
<g/>
—	—	k?
<g/>
,4	,4	k4
<g/>
α	α	k?
<g/>
)	)	kIx)
<g/>
8	#num#	k4
s	s	k7c7
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
279	#num#	k4
<g/>
Mt	Mt	k1gFnPc2
?	?	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
HAIRE	HAIRE	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
G.	G.	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Chemistry	Chemistr	k1gMnPc4
of	of	k?
the	the	k?
Actinide	Actinid	k1gInSc5
and	and	k?
Transactinide	Transactinid	k1gInSc5
Elements	Elements	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Morss	Morssa	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
rd	rd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dordrecht	Dordrecht	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Netherlands	Netherlands	k1gInSc1
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
Science	Science	k1gFnSc1
<g/>
+	+	kIx~
<g/>
Business	business	k1gInSc1
Media	medium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4020	#num#	k4
<g/>
-	-	kIx~
<g/>
3555	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Transactinides	Transactinidesa	k1gFnPc2
and	and	k?
the	the	k?
future	futur	k1gMnSc5
elements	elements	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Thierfelder	Thierfelder	k1gInSc1
<g/>
,	,	kIx,
C.	C.	kA
<g/>
;	;	kIx,
SCHWERDTFEGER	SCHWERDTFEGER	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
HEßBERGER	HEßBERGER	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
P.	P.	kA
<g/>
;	;	kIx,
HOFMANN	HOFMANN	kA
<g/>
,	,	kIx,
S.	S.	kA
Dirac-Hartree-Fock	Dirac-Hartree-Fock	k1gMnSc1
studies	studies	k1gMnSc1
of	of	k?
X-ray	X-raa	k1gFnSc2
transitions	transitionsa	k1gFnPc2
in	in	k?
meitnerium	meitnerium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
European	European	k1gMnSc1
Physical	Physical	k1gMnSc1
Journal	Journal	k1gMnSc1
A.	A.	kA
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
227	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
epja	epj	k1gInSc2
<g/>
/	/	kIx~
<g/>
i	i	k9
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10584	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2008	#num#	k4
<g/>
EPJA	EPJA	kA
<g/>
..	..	k?
<g/>
.36	.36	k4
<g/>
.	.	kIx.
<g/>
.227	.227	k4
<g/>
T.	T.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chemical	Chemical	k1gMnPc1
Data	datum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meitnerium	Meitnerium	k1gNnSc1
-	-	kIx~
Mt	Mt	k1gMnSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Chemical	Chemical	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
↑	↑	k?
Thierfelder	Thierfelder	k1gMnSc1
<g/>
,	,	kIx,
C.	C.	kA
<g/>
;	;	kIx,
SCHWERDTFEGER	SCHWERDTFEGER	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
HEßBERGER	HEßBERGER	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
P.	P.	kA
<g/>
;	;	kIx,
HOFMANN	HOFMANN	kA
<g/>
,	,	kIx,
S.	S.	kA
Dirac-Hartree-Fock	Dirac-Hartree-Fock	k1gMnSc1
studies	studies	k1gMnSc1
of	of	k?
X-ray	X-raa	k1gFnSc2
transitions	transitionsa	k1gFnPc2
in	in	k?
meitnerium	meitnerium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
European	European	k1gMnSc1
Physical	Physical	k1gMnSc1
Journal	Journal	k1gMnSc1
A.	A.	kA
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
227	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
epja	epj	k1gInSc2
<g/>
/	/	kIx~
<g/>
i	i	k9
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10584	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2008	#num#	k4
<g/>
EPJA	EPJA	kA
<g/>
..	..	k?
<g/>
.36	.36	k4
<g/>
.	.	kIx.
<g/>
.227	.227	k4
<g/>
T.	T.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
ÖSTLIN	ÖSTLIN	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
VITOS	VITOS	kA
<g/>
,	,	kIx,
L.	L.	kA
First-principles	First-principles	k1gInSc1
calculation	calculation	k1gInSc1
of	of	k?
the	the	k?
structural	structurat	k5eAaPmAgMnS,k5eAaImAgMnS
stability	stabilita	k1gFnSc2
of	of	k?
6	#num#	k4
<g/>
d	d	k?
transition	transition	k1gInSc1
metals	metals	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
B.	B.	kA
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevB	PhysRevB	k1gFnSc2
<g/>
.84	.84	k4
<g/>
.113104	.113104	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2011	#num#	k4
<g/>
PhRvB	PhRvB	k1gFnPc2
<g/>
.	.	kIx.
<g/>
.84	.84	k4
<g/>
k	k	k7c3
<g/>
3104	#num#	k4
<g/>
O.	O.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SAITO	SAITO	kA
<g/>
,	,	kIx,
Shiro	Shiro	k1gNnSc1
L.	L.	kA
Hartree	Hartre	k1gInSc2
<g/>
–	–	k?
<g/>
Fock	Fock	k1gMnSc1
<g/>
–	–	k?
<g/>
Roothaan	Roothaan	k1gMnSc1
energies	energies	k1gMnSc1
and	and	k?
expectation	expectation	k1gInSc1
values	valuesa	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
neutral	utrat	k5eNaPmAgMnS,k5eNaImAgMnS
atoms	atoms	k6eAd1
He	he	k0
to	ten	k3xDgNnSc4
Uuo	Uuo	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
B-spline	B-splin	k1gInSc5
expansion	expansion	k1gInSc4
method	methoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atomic	Atomice	k1gInPc2
Data	datum	k1gNnSc2
and	and	k?
Nuclear	Nuclear	k1gMnSc1
Data	datum	k1gNnSc2
Tables	Tablesa	k1gFnPc2
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
836	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
adt	adt	k?
<g/>
.2009	.2009	k4
<g/>
.06	.06	k4
<g/>
.001	.001	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2009	#num#	k4
<g/>
ADNDT	ADNDT	kA
<g/>
.	.	kIx.
<g/>
.95	.95	k4
<g/>
.	.	kIx.
<g/>
.836	.836	k4
<g/>
S.	S.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Münzenberg	Münzenberg	k1gInSc1
<g/>
,	,	kIx,
G.	G.	kA
Observation	Observation	k1gInSc1
of	of	k?
one	one	k?
correlated	correlated	k1gMnSc1
α	α	k1gFnSc2
in	in	k?
the	the	k?
reaction	reaction	k1gInSc4
58	#num#	k4
<g/>
Fe	Fe	k1gFnPc2
on	on	k3xPp3gMnSc1
209	#num#	k4
<g/>
Bi	Bi	k1gFnSc2
<g/>
→	→	k?
<g/>
267109	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeitschrift	Zeitschrift	k2eAgInSc1d1
für	für	k?
Physik	Physik	k1gInSc1
a	a	k8xC
Atoms	Atoms	k1gInSc1
and	and	k?
Nuclei	Nucle	k1gFnSc2
<g/>
.	.	kIx.
1982	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
309	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
BF	BF	kA
<g/>
0	#num#	k4
<g/>
1420157	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Interactive	Interactiv	k1gInSc5
Chart	charta	k1gFnPc2
of	of	k?
Nuclides	Nuclidesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meitnerium	Meitnerium	k1gNnSc1
278	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Nuclear	Nuclear	k1gMnSc1
Data	datum	k1gNnSc2
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
Brookhaven	Brookhavna	k1gFnPc2
National	National	k1gFnSc2
Laboratory	Laborator	k1gInPc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
meitnerium	meitnerium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
meitnerium	meitnerium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4518308-9	4518308-9	k4
</s>
