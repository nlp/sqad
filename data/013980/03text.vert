<s>
Ethereum	Ethereum	k1gInSc1
</s>
<s>
Ethereum	Ethereum	k1gInSc1
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Vitalik	Vitalik	k1gMnSc1
Buterin	Buterin	k1gInSc4
<g/>
,	,	kIx,
Gavin	Gavin	k2eAgInSc4d1
Wood	Wood	k1gInSc4
První	první	k4xOgNnPc4
vydání	vydání	k1gNnPc4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2015	#num#	k4
Aktuální	aktuální	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
Frontier	Frontier	k1gInSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2015	#num#	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Linux	Linux	kA
<g/>
,	,	kIx,
Windows	Windows	kA
<g/>
,	,	kIx,
macOS	macOS	k?
<g/>
,	,	kIx,
POSIX	POSIX	kA
compliant	compliant	k1gInSc4
Vyvíjeno	vyvíjen	k2eAgNnSc1d1
v	v	k7c6
</s>
<s>
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
Go	Go	k1gMnSc1
<g/>
,	,	kIx,
JavaScript	JavaScript	k1gMnSc1
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
<g/>
,	,	kIx,
Java	Java	k1gMnSc1
<g/>
,	,	kIx,
node	node	k1gFnSc1
<g/>
.	.	kIx.
<g/>
js	js	k?
Licence	licence	k1gFnSc1
</s>
<s>
GPL	GPL	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
MIT	MIT	kA
<g/>
,	,	kIx,
LGPL	LGPL	kA
<g/>
,	,	kIx,
etc	etc	k?
<g/>
..	..	k?
Web	web	k1gInSc1
</s>
<s>
www.ethereum.org	www.ethereum.org	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ethereum	Ethereum	k1gInSc1
je	být	k5eAaImIp3nS
kryptoměna	kryptoměn	k2eAgFnSc1d1
(	(	kIx(
<g/>
ETH	ETH	kA
<g/>
)	)	kIx)
a	a	k8xC
open-source	open-sourka	k1gFnSc6
platforma	platforma	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
decentralizované	decentralizovaný	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
blockchainu	blockchain	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
chrání	chránit	k5eAaImIp3nS
před	před	k7c7
neoprávněným	oprávněný	k2eNgInSc7d1
zásahem	zásah	k1gInSc7
z	z	k7c2
vnější	vnější	k2eAgFnSc2d1
i	i	k8xC
z	z	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
však	však	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
decentralizovaný	decentralizovaný	k2eAgInSc4d1
<g/>
,	,	kIx,
turingovsky	turingovsky	k6eAd1
kompletní	kompletní	k2eAgInSc1d1
virtuální	virtuální	k2eAgInSc1d1
stroj	stroj	k1gInSc1
Ethereum	Ethereum	k1gNnSc1
Virtual	Virtual	k1gInSc1
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
EVM	EVM	kA
<g/>
)	)	kIx)
pro	pro	k7c4
běh	běh	k1gInSc4
takzvaných	takzvaný	k2eAgMnPc2d1
„	„	k?
<g/>
smart	smart	k1gInSc1
contracts	contracts	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
chytrých	chytrý	k2eAgInPc2d1
kontraktů	kontrakt	k1gInPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
zajišťují	zajišťovat	k5eAaImIp3nP
hladké	hladký	k2eAgNnSc4d1
a	a	k8xC
nezmanipulovatelné	zmanipulovatelný	k2eNgNnSc4d1
fungování	fungování	k1gNnSc4
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Autorem	autor	k1gMnSc7
platformy	platforma	k1gFnSc2
a	a	k8xC
kryptoměny	kryptoměn	k2eAgInPc1d1
Ethereum	Ethereum	k1gInSc1
je	být	k5eAaImIp3nS
Vitalik	Vitalik	k1gMnSc1
Buterin	Buterin	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
koncept	koncept	k1gInSc1
poprvé	poprvé	k6eAd1
představil	představit	k5eAaPmAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Gavinem	Gavin	k1gMnSc7
Woodem	Wood	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
koncept	koncept	k1gInSc4
formalizoval	formalizovat	k5eAaBmAgMnS
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
v	v	k7c6
takzvaném	takzvaný	k2eAgMnSc6d1
Yellow	Yellow	k1gMnSc6
Paperu	Paper	k1gMnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Síť	síť	k1gFnSc1
byla	být	k5eAaImAgFnS
spuštěna	spuštěn	k2eAgFnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2015	#num#	k4
v	v	k7c6
režimu	režim	k1gInSc6
Frontier	Frontira	k1gFnPc2
(	(	kIx(
<g/>
veřejný	veřejný	k2eAgInSc1d1
beta	beta	k1gNnSc1
test	test	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncept	koncept	k1gInSc1
Etherea	Ethere	k1gInSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
mezi	mezi	k7c4
next-generation	next-generation	k1gInSc4
kryptoměny	kryptoměn	k2eAgFnPc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
nazývané	nazývaný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Bitcoin	Bitcoin	k1gInSc1
2.0	2.0	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Hard	Hard	k6eAd1
fork	fork	k1gInSc1
a	a	k8xC
zcizení	zcizení	k1gNnSc1
etherů	ether	k1gInPc2
</s>
<s>
Rok	rok	k1gInSc1
po	po	k7c6
spuštění	spuštění	k1gNnSc6
sítě	síť	k1gFnSc2
Ethereum	Ethereum	k1gInSc4
již	již	k6eAd1
cena	cena	k1gFnSc1
kryptoměny	kryptoměn	k2eAgFnPc1d1
vzrostla	vzrůst	k5eAaPmAgFnS
na	na	k7c4
20	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
za	za	k7c4
jeden	jeden	k4xCgInSc4
ether	ether	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
pořádku	pořádek	k1gInSc6
dne	den	k1gInSc2
byl	být	k5eAaImAgInS
velký	velký	k2eAgInSc1d1
propad	propad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvisel	souviset	k5eAaImAgInS
s	s	k7c7
kryptoměnovým	kryptoměnův	k2eAgInSc7d1
systémem	systém	k1gInSc7
DAO	DAO	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
decentralized	decentralized	k1gInSc1
autonomous	autonomous	k1gMnSc1
organization	organization	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
a	a	k8xC
byl	být	k5eAaImAgInS
v	v	k7c6
podstatě	podstata	k1gFnSc6
naprosto	naprosto	k6eAd1
transparentní	transparentní	k2eAgInSc1d1
a	a	k8xC
flexibilní	flexibilní	k2eAgInSc1d1
decentralizovaný	decentralizovaný	k2eAgInSc1d1
fond	fond	k1gInSc1
rizikového	rizikový	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
účel	účel	k1gInSc4
bylo	být	k5eAaImAgNnS
financování	financování	k1gNnSc1
všech	všecek	k3xTgFnPc2
budoucích	budoucí	k2eAgFnPc2d1
decentralizovaných	decentralizovaný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
května	květen	k1gInSc2
až	až	k8xS
června	červen	k1gInSc2
2016	#num#	k4
narostla	narůst	k5eAaPmAgFnS
hodnota	hodnota	k1gFnSc1
DAO	DAO	kA
v	v	k7c6
přepočtu	přepočet	k1gInSc6
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
150	#num#	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
v	v	k7c6
etheru	ether	k1gInSc6
<g/>
,	,	kIx,
tj.	tj.	kA
14	#num#	k4
%	%	kIx~
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
vydaných	vydaný	k2eAgInPc2d1
etherů	ether	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
koncepci	koncepce	k1gFnSc6
blockchainu	blockchainout	k5eAaPmIp1nS
Ethereum	Ethereum	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gInPc4
tokeny	token	k1gInPc4
mohl	moct	k5eAaImAgInS
získat	získat	k5eAaPmF
každý	každý	k3xTgInSc1
výměnou	výměna	k1gFnSc7
za	za	k7c4
ethery	ether	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
také	také	k9
získal	získat	k5eAaPmAgMnS
přístup	přístup	k1gInSc4
na	na	k7c6
DAPPS	DAPPS	kA
(	(	kIx(
<g/>
tzv.	tzv.	kA
decentralizované	decentralizovaný	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
DAO	DAO	kA
ale	ale	k8xC
umožňoval	umožňovat	k5eAaImAgInS
i	i	k9
opačnou	opačný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
klientovi	klient	k1gMnSc3
aplikace	aplikace	k1gFnPc1
DAPPS	DAPPS	kA
nevyhovovaly	vyhovovat	k5eNaImAgFnP
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgMnS
znovu	znovu	k6eAd1
získat	získat	k5eAaPmF
funkcí	funkce	k1gFnSc7
„	„	k?
<g/>
split	split	k1gMnSc1
<g/>
“	“	k?
zpět	zpět	k6eAd1
své	svůj	k3xOyFgInPc4
ethery	ether	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnPc4
také	také	k6eAd1
každému	každý	k3xTgMnSc3
umožňovala	umožňovat	k5eAaImAgFnS
tvorbu	tvorba	k1gFnSc4
vlastní	vlastní	k2eAgFnSc4d1
„	„	k?
<g/>
child	childa	k1gFnPc2
DAO	DAO	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
dceřiné	dceřiný	k2eAgFnSc2d1
DAO	DAO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
zajištěný	zajištěný	k2eAgInSc4d1
hladký	hladký	k2eAgInSc4d1
průběh	průběh	k1gInSc4
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
získané	získaný	k2eAgInPc4d1
ethery	ether	k1gInPc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
utratit	utratit	k5eAaPmF
až	až	k9
28	#num#	k4
dní	den	k1gInPc2
od	od	k7c2
oddělení	oddělení	k1gNnSc2
se	se	k3xPyFc4
od	od	k7c2
systému	systém	k1gInSc2
DAO	DAO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
samotném	samotný	k2eAgInSc6d1
systému	systém	k1gInSc6
DAO	DAO	kA
se	se	k3xPyFc4
však	však	k9
nachází	nacházet	k5eAaImIp3nS
chyba	chyba	k1gFnSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
tj.	tj.	kA
po	po	k7c6
zhruba	zhruba	k6eAd1
měsíci	měsíc	k1gInSc3
od	od	k7c2
vzniku	vznik	k1gInSc2
DAO	DAO	kA
<g/>
,	,	kIx,
využili	využít	k5eAaPmAgMnP
hackeři	hacker	k1gMnPc1
chyby	chyba	k1gFnSc2
v	v	k7c6
systému	systém	k1gInSc6
a	a	k8xC
odčerpali	odčerpat	k5eAaPmAgMnP
pomocí	pomocí	k7c2
funkce	funkce	k1gFnSc2
„	„	k?
<g/>
split	split	k1gMnSc1
<g/>
“	“	k?
třetinu	třetina	k1gFnSc4
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
DAO	DAO	kA
<g/>
,	,	kIx,
tedy	tedy	k9
cca	cca	kA
50	#num#	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
přepočtu	přepočet	k1gInSc6
3,7	3,7	k4
milionů	milion	k4xCgInPc2
tokenů	token	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utratit	utratit	k5eAaPmF
odčerpané	odčerpaný	k2eAgInPc4d1
peníze	peníz	k1gInPc4
mohli	moct	k5eAaImAgMnP
však	však	k9
až	až	k9
po	po	k7c6
uplynutí	uplynutí	k1gNnSc6
28	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Chyba	chyba	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
straně	strana	k1gFnSc6
DAO	DAO	kA
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
Etherea	Etherea	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přesto	přesto	k8xC
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
důvěry	důvěra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
etheru	ether	k1gInSc2
klesla	klesnout	k5eAaPmAgFnS
z	z	k7c2
20	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
za	za	k7c4
token	token	k1gInSc4
na	na	k7c6
cca	cca	kA
12	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
reakci	reakce	k1gFnSc3
Nadace	nadace	k1gFnSc2
Ethereum	Ethereum	k1gNnSc1
Foundation	Foundation	k1gInSc4
z	z	k7c2
možných	možný	k2eAgNnPc2d1
řešení	řešení	k1gNnPc2
zvolila	zvolit	k5eAaPmAgFnS
„	„	k?
<g/>
hard	hardo	k1gNnPc2
fork	forko	k1gNnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
nenávratné	návratný	k2eNgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
platnosti	platnost	k1gFnSc2
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
bloku	blok	k1gInSc2
1920	#num#	k4
000	#num#	k4
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
blockchain	blockchain	k1gInSc4
novou	nový	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
a	a	k8xC
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
tyto	tento	k3xDgFnPc4
změny	změna	k1gFnPc4
akceptoval	akceptovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
touto	tento	k3xDgFnSc7
cestou	cesta	k1gFnSc7
vydá	vydat	k5eAaPmIp3nS
také	také	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatním	ostatní	k1gNnSc7
byl	být	k5eAaImAgInS
přístup	přístup	k1gInSc1
k	k	k7c3
dalším	další	k2eAgFnPc3d1
aktualizacím	aktualizace	k1gFnPc3
odepřen	odepřen	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
„	„	k?
<g/>
hard	hard	k6eAd1
forku	forka	k1gFnSc4
<g/>
“	“	k?
vyjádřila	vyjádřit	k5eAaPmAgFnS
podporu	podpora	k1gFnSc4
většina	většina	k1gFnSc1
investorů	investor	k1gMnPc2
a	a	k8xC
významných	významný	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
zachoval	zachovat	k5eAaPmAgMnS
si	se	k3xPyFc3
název	název	k1gInSc4
Ethereum	Ethereum	k1gInSc4
(	(	kIx(
<g/>
ETH	ETH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
kryptoměna	kryptoměn	k2eAgFnSc1d1
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nesouhlasila	souhlasit	k5eNaImAgFnS
s	s	k7c7
radikálním	radikální	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
a	a	k8xC
odmítla	odmítnout	k5eAaPmAgFnS
přejít	přejít	k5eAaPmF
na	na	k7c4
nový	nový	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
tj.	tj.	kA
zůstala	zůstat	k5eAaPmAgFnS
u	u	k7c2
starého	starý	k2eAgInSc2d1
blockchainu	blockchain	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
Ethereum	Ethereum	k1gInSc4
Classic	Classice	k1gInPc2
(	(	kIx(
<g/>
ETC	ETC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zásah	zásah	k1gInSc1
do	do	k7c2
podstaty	podstata	k1gFnSc2
blockchainu	blockchaina	k1gMnSc4
Etherea	Ethereus	k1gMnSc4
představoval	představovat	k5eAaImAgMnS
pouze	pouze	k6eAd1
zavedení	zavedení	k1gNnSc4
chytrého	chytrý	k2eAgInSc2d1
kontrakt	kontrakt	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
jedinou	jediný	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
bylo	být	k5eAaImAgNnS
umožnit	umožnit	k5eAaPmF
obětem	oběť	k1gFnPc3
získat	získat	k5eAaPmF
své	svůj	k3xOyFgInPc4
peníze	peníz	k1gInPc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
krok	krok	k1gInSc1
však	však	k9
narušil	narušit	k5eAaPmAgInS
ideu	idea	k1gFnSc4
spravedlivého	spravedlivý	k2eAgMnSc2d1
a	a	k8xC
transparentního	transparentní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nebude	být	k5eNaImBp3nS
měnit	měnit	k5eAaImF
a	a	k8xC
zůstane	zůstat	k5eAaPmIp3nS
stejný	stejný	k2eAgInSc1d1
na	na	k7c4
věky	věk	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
naznačoval	naznačovat	k5eAaImAgMnS
i	i	k9
další	další	k2eAgFnPc4d1
možné	možný	k2eAgFnPc4d1
budoucí	budoucí	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
ostatně	ostatně	k6eAd1
odehrály	odehrát	k5eAaPmAgInP
hned	hned	k6eAd1
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
proběhl	proběhnout	k5eAaPmAgMnS
další	další	k2eAgMnSc1d1
„	„	k?
<g/>
hard	hard	k1gMnSc1
fork	fork	k1gMnSc1
<g/>
“	“	k?
Etherea	Etherea	k1gMnSc1
na	na	k7c6
řešení	řešení	k1gNnSc6
s	s	k7c7
názvem	název	k1gInSc7
Byzantium	Byzantium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
„	„	k?
<g/>
hard	hard	k1gInSc1
fork	fork	k1gInSc1
<g/>
“	“	k?
s	s	k7c7
názve	název	k1gInSc5
Constantinopole	Constantinopole	k1gFnSc2
má	můj	k3xOp1gFnSc1
následovat	následovat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
kapitalizace	kapitalizace	k1gFnSc2
představuje	představovat	k5eAaImIp3nS
Ethereum	Ethereum	k1gInSc4
druhou	druhý	k4xOgFnSc4
největší	veliký	k2eAgFnSc4d3
kryptoměnu	kryptoměn	k2eAgFnSc4d1
současnosti	současnost	k1gFnPc4
(	(	kIx(
<g/>
Etherum	Etherum	k1gInSc1
Classic	Classic	k1gMnSc1
je	být	k5eAaImIp3nS
osmnáctou	osmnáctý	k4xOgFnSc7
největší	veliký	k2eAgFnSc1d3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tržní	tržní	k2eAgFnSc1d1
kapitalizace	kapitalizace	k1gFnSc1
ETH	ETH	kA
činila	činit	k5eAaImAgFnS
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
roku	rok	k1gInSc2
2018	#num#	k4
49	#num#	k4
789	#num#	k4
840	#num#	k4
777	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
při	při	k7c6
ceně	cena	k1gFnSc6
497,15	497,15	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
za	za	k7c4
jeden	jeden	k4xCgInSc4
ether	ether	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tokenů	Token	k1gInPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
oběhu	oběh	k1gInSc6
100	#num#	k4
150	#num#	k4
943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tržní	tržní	k2eAgFnSc1d1
kapitalizace	kapitalizace	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
110	#num#	k4
752	#num#	k4
419	#num#	k4
254	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dnes	dnes	k6eAd1
tedy	tedy	k9
existují	existovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
Ethereové	Ethereové	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
provádění	provádění	k1gNnSc4
transakce	transakce	k1gFnSc2
a	a	k8xC
chytré	chytrý	k2eAgInPc4d1
kontrakty	kontrakt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
velmi	velmi	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
však	však	k9
držel	držet	k5eAaImAgMnS
měnu	měna	k1gFnSc4
ether	ether	k1gInSc4
v	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
rozdělením	rozdělení	k1gNnSc7
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
nárok	nárok	k1gInSc1
na	na	k7c4
stejný	stejný	k2eAgInSc4d1
objem	objem	k1gInSc4
jednotek	jednotka	k1gFnPc2
v	v	k7c6
obou	dva	k4xCgFnPc6
kryptoměnách	kryptoměna	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Enterprise	Enterprise	k1gFnSc1
Ethereum	Ethereum	k1gInSc1
Alliance	Alliance	k1gFnSc1
(	(	kIx(
<g/>
EEA	EEA	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
různé	různý	k2eAgInPc4d1
blockchain	blockchain	k2eAgInSc4d1
startupy	startup	k1gInPc4
<g/>
,	,	kIx,
výzkumné	výzkumný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
a	a	k8xC
společnosti	společnost	k1gFnSc2
Fortune	Fortun	k1gInSc5
500	#num#	k4
oznámili	oznámit	k5eAaPmAgMnP
vytvoření	vytvoření	k1gNnSc4
Enterprise	Enterprise	k1gFnSc2
Ethereum	Ethereum	k1gInSc4
Alliance	Alliance	k1gFnSc2
(	(	kIx(
<g/>
EEA	EEA	kA
<g/>
)	)	kIx)
s	s	k7c7
30	#num#	k4
zakládajícími	zakládající	k2eAgInPc7d1
členy	člen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
května	květen	k1gInSc2
měla	mít	k5eAaImAgFnS
organizace	organizace	k1gFnSc1
116	#num#	k4
členů	člen	k1gInPc2
<g/>
,	,	kIx,
vč.	vč.	k?
ConsenSys	ConsenSys	k1gInSc1
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
Intel	Intel	kA
<g/>
,	,	kIx,
ING	ing	kA
a	a	k8xC
dalších	další	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s>
Účelem	účel	k1gInSc7
EEA	EEA	kA
je	být	k5eAaImIp3nS
koordinace	koordinace	k1gFnSc1
vývoje	vývoj	k1gInSc2
open-source	open-sourka	k1gFnSc6
verze	verze	k1gFnPc4
<g/>
,	,	kIx,
i	i	k9
neveřejné	veřejný	k2eNgFnPc4d1
části	část	k1gFnPc4
blockchainu	blockchainout	k5eAaPmIp1nS
určené	určený	k2eAgNnSc1d1
k	k	k7c3
řešení	řešení	k1gNnSc3
korporátních	korporátní	k2eAgInPc2d1
problémů	problém	k1gInPc2
v	v	k7c6
bankovnictví	bankovnictví	k1gNnSc6
<g/>
,	,	kIx,
managementu	management	k1gInSc6
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc6
<g/>
,	,	kIx,
aj.	aj.	kA
za	za	k7c2
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
vývojáři	vývojář	k1gMnPc7
z	z	k7c2
EEA	EEA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
členové	člen	k1gMnPc1
aliance	aliance	k1gFnSc2
projevili	projevit	k5eAaPmAgMnP
zájem	zájem	k1gInSc4
o	o	k7c4
implementaci	implementace	k1gFnSc4
nepřístupných	přístupný	k2eNgNnPc2d1
části	část	k1gFnSc6
do	do	k7c2
veřejného	veřejný	k2eAgMnSc2d1
Etheruem	Etheruem	k1gInSc1
blockchain	blockchain	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
ale	ale	k8xC
velké	velký	k2eAgFnPc4d1
pochybnosti	pochybnost	k1gFnPc4
v	v	k7c6
podobě	podoba	k1gFnSc6
možných	možný	k2eAgInPc2d1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
a	a	k8xC
regulačních	regulační	k2eAgInPc2d1
problémů	problém	k1gInPc2
spojené	spojený	k2eAgNnSc1d1
se	s	k7c7
vzájemným	vzájemný	k2eAgNnSc7d1
přemostěním	přemostění	k1gNnSc7
blockchainů	blockchain	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
červencem	červenec	k1gInSc7
2017	#num#	k4
má	mít	k5eAaImIp3nS
EEA	EEA	kA
již	již	k6eAd1
150	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
nově	nově	k6eAd1
např.	např.	kA
MasterCard	MasterCarda	k1gFnPc2
<g/>
,	,	kIx,
Cisco	Cisco	k6eAd1
Systems	Systems	k1gInSc1
nebo	nebo	k8xC
Scotia	Scotia	k1gFnSc1
Bank	banka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Tvůrcům	tvůrce	k1gMnPc3
kryptoměny	kryptoměn	k2eAgFnPc1d1
Ethereum	Ethereum	k1gInSc1
šlo	jít	k5eAaImAgNnS
zejména	zejména	k9
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
sdílené	sdílený	k2eAgFnSc2d1
výpočetní	výpočetní	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
využít	využít	k5eAaPmF
blockchain	blockchain	k1gInSc4
k	k	k7c3
provozu	provoz	k1gInSc3
decentralizovaných	decentralizovaný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platforma	platforma	k1gFnSc1
Ethereum	Ethereum	k1gNnSc1
proto	proto	k8xC
slouží	sloužit	k5eAaImIp3nS
ke	k	k7c3
zdokonalení	zdokonalení	k1gNnSc3
chytrých	chytrý	k2eAgInPc2d1
kontraktů	kontrakt	k1gInPc2
a	a	k8xC
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
uvedení	uvedení	k1gNnSc3
do	do	k7c2
praxe	praxe	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
nahradit	nahradit	k5eAaPmF
a	a	k8xC
decentralizovat	decentralizovat	k5eAaBmF
všechny	všechen	k3xTgFnPc4
klasické	klasický	k2eAgFnPc4d1
smlouvy	smlouva	k1gFnPc4
a	a	k8xC
dohody	dohoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Chytrý	chytrý	k2eAgInSc1d1
kontrakt	kontrakt	k1gInSc1
je	být	k5eAaImIp3nS
jakýkoliv	jakýkoliv	k3yIgInSc4
protokol	protokol	k1gInSc4
nebo	nebo	k8xC
software	software	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ověřuje	ověřovat	k5eAaImIp3nS
anebo	anebo	k8xC
vynucuje	vynucovat	k5eAaImIp3nS
vyjednání	vyjednání	k1gNnSc1
nebo	nebo	k8xC
provedení	provedení	k1gNnSc1
kontraktu	kontrakt	k1gInSc2
(	(	kIx(
<g/>
smlouvy	smlouva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
standardní	standardní	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dodržení	dodržení	k1gNnSc1
závazku	závazek	k1gInSc2
vynucuje	vynucovat	k5eAaImIp3nS
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
chytré	chytrý	k2eAgInPc1d1
kontrakty	kontrakt	k1gInPc1
závazky	závazek	k1gInPc4
vynucují	vynucovat	k5eAaImIp3nP
pomocí	pomocí	k7c2
kryptografického	kryptografický	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fungování	fungování	k1gNnSc1
této	tento	k3xDgFnSc2
sítě	síť	k1gFnSc2
tedy	tedy	k9
není	být	k5eNaImIp3nS
jako	jako	k9
u	u	k7c2
Bitcoinu	Bitcoin	k1gInSc2
omezeno	omezen	k2eAgNnSc1d1
pouze	pouze	k6eAd1
na	na	k7c6
transakci	transakce	k1gFnSc6
měny	měna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
možné	možný	k2eAgFnSc6d1
psát	psát	k5eAaImF
své	svůj	k3xOyFgInPc4
vlastní	vlastní	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
,	,	kIx,
tj.	tj.	kA
chytré	chytrý	k2eAgInPc4d1
kontrakty	kontrakt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
provádějí	provádět	k5eAaImIp3nP
přesně	přesně	k6eAd1
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
jejich	jejich	k3xOp3gMnSc1
tvůrce	tvůrce	k1gMnSc1
nastaví	nastavět	k5eAaBmIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrakty	kontrakt	k1gInPc7
tak	tak	k6eAd1
klientům	klient	k1gMnPc3
pomohou	pomoct	k5eAaPmIp3nP
transparentním	transparentní	k2eAgInSc7d1
a	a	k8xC
nekonfliktním	konfliktní	k2eNgInSc7d1
procesem	proces	k1gInSc7
vyměnit	vyměnit	k5eAaPmF
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
majetek	majetek	k1gInSc4
<g/>
,	,	kIx,
akcie	akcie	k1gFnPc1
nebo	nebo	k8xC
cokoliv	cokoliv	k3yInSc1
jiného	jiné	k1gNnSc2
bez	bez	k7c2
zásahů	zásah	k1gInPc2
třetích	třetí	k4xOgFnPc2
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
právníků	právník	k1gMnPc2
<g/>
,	,	kIx,
notářů	notář	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chytré	chytrý	k2eAgInPc1d1
kontrakty	kontrakt	k1gInPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
přirovnávány	přirovnávat	k5eAaImNgFnP
k	k	k7c3
digitálnímu	digitální	k2eAgInSc3d1
prodejnímu	prodejní	k2eAgInSc3d1
automatu	automat	k1gInSc3
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
se	se	k3xPyFc4
vloží	vložit	k5eAaPmIp3nS
data	datum	k1gNnSc2
nebo	nebo	k8xC
něco	něco	k3yInSc1
hodnotného	hodnotný	k2eAgNnSc2d1
a	a	k8xC
zákazník	zákazník	k1gMnSc1
za	za	k7c4
to	ten	k3xDgNnSc4
získá	získat	k5eAaPmIp3nS
finální	finální	k2eAgFnSc4d1
položku	položka	k1gFnSc4
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
nápoj	nápoj	k1gInSc4
nebo	nebo	k8xC
dům	dům	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
akce	akce	k1gFnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
automatu	automat	k1gInSc6
<g/>
,	,	kIx,
síti	sít	k5eAaImF
Ethereum	Ethereum	k1gNnSc4
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
určité	určitý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
potřebného	potřebný	k2eAgInSc2d1
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
a	a	k8xC
délky	délka	k1gFnSc2
běhu	běh	k1gInSc2
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kryptoměna	Kryptoměn	k2eAgFnSc1d1
ether	ether	k1gInSc4
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
právě	právě	k6eAd1
jako	jako	k8xS,k8xC
digitální	digitální	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
této	tento	k3xDgFnSc2
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
získávají	získávat	k5eAaImIp3nP
od	od	k7c2
správce	správce	k1gMnSc2
chytrého	chytrý	k2eAgInSc2d1
kontraktu	kontrakt	k1gInSc2
těžaři	těžař	k1gMnSc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
popud	popud	k1gInSc1
spustí	spustit	k5eAaPmIp3nS
část	část	k1gFnSc4
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ether	ether	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dále	daleko	k6eAd2
doplňován	doplňovat	k5eAaImNgInS
zasláním	zaslání	k1gNnSc7
od	od	k7c2
uživatelů	uživatel	k1gMnPc2
nebo	nebo	k8xC
jiných	jiný	k2eAgInPc2d1
chytrých	chytrý	k2eAgInPc2d1
kontraktů	kontrakt	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Těžba	těžba	k1gFnSc1
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Bitcoinu	Bitcoin	k1gInSc2
lze	lze	k6eAd1
ether	ether	k1gInSc4
relativně	relativně	k6eAd1
snadno	snadno	k6eAd1
těžit	těžit	k5eAaImF
grafickými	grafický	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
místo	místo	k7c2
specializovaných	specializovaný	k2eAgInPc2d1
minerů	miner	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
ale	ale	k8xC
začínají	začínat	k5eAaImIp3nP
programátoři	programátor	k1gMnPc1
testovat	testovat	k5eAaImF
pro	pro	k7c4
ověřování	ověřování	k1gNnSc4
transakcí	transakce	k1gFnPc2
a	a	k8xC
zapisování	zapisování	k1gNnPc2
do	do	k7c2
blockchainu	blockchain	k1gInSc2
koncept	koncept	k1gInSc1
proof-of-stake	proof-of-stakat	k5eAaPmIp3nS
namísto	namísto	k7c2
proof-of-work	proof-of-work	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloky	blok	k1gInPc1
by	by	kYmCp3nP
tak	tak	k6eAd1
do	do	k7c2
řetězce	řetězec	k1gInSc2
transakcí	transakce	k1gFnPc2
nepřidávali	přidávat	k5eNaImAgMnP
těžaři	těžař	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
přímo	přímo	k6eAd1
majitelé	majitel	k1gMnPc1
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
etherů	ether	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
</s>
<s>
Ethereum	Ethereum	k1gInSc4
lze	lze	k6eAd1
směnit	směnit	k5eAaPmF
za	za	k7c4
Bitcoin	Bitcoin	k1gInSc4
na	na	k7c6
některé	některý	k3yIgFnSc6
ze	z	k7c2
světových	světový	k2eAgFnPc2d1
burz	burza	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
obchodováno	obchodován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmi	ten	k3xDgInPc7
můžou	můžou	k?
být	být	k5eAaImF
například	například	k6eAd1
Poloniex	Poloniex	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
nebo	nebo	k8xC
Kraken	Krakno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s>
Ethereum	Ethereum	k1gInSc4
lze	lze	k6eAd1
zakoupit	zakoupit	k5eAaPmF
i	i	k9
např.	např.	kA
pomocí	pomocí	k7c2
SEPA	SEPA	kA
převodu	převod	k1gInSc2
na	na	k7c6
burze	burza	k1gFnSc6
coinbase	coinbase	k6eAd1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
českých	český	k2eAgFnPc2d1
směnáren	směnárna	k1gFnPc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
ale	ale	k8xC
při	při	k7c6
vyšších	vysoký	k2eAgFnPc6d2
částkách	částka	k1gFnPc6
nutné	nutný	k2eAgNnSc1d1
podstoupit	podstoupit	k5eAaPmF
proces	proces	k1gInSc4
ověření	ověření	k1gNnSc2
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2017	#num#	k4
umožňuje	umožňovat	k5eAaImIp3nS
Ethereum	Ethereum	k1gInSc4
obchodovat	obchodovat	k5eAaImF
i	i	k9
burza	burza	k1gFnSc1
bitstamp	bitstamp	k1gInSc1
(	(	kIx(
<g/>
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
burz	burza	k1gFnPc2
obchodujících	obchodující	k2eAgFnPc2d1
Bitcoin	Bitcoina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Vitalik	Vitalik	k1gMnSc1
Buterin	Buterin	k1gInSc4
přiznal	přiznat	k5eAaPmAgMnS
kolik	kolik	k9
vlastní	vlastnit	k5eAaImIp3nS
Etheru	ether	k1gInSc2
a	a	k8xC
další	další	k2eAgInPc4d1
kryptoměny	kryptoměn	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ethereum	Ethereum	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ethereum	Ethereum	k1gInSc1
-	-	kIx~
Kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
wallet	wallet	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k6eAd1
koupit	koupit	k5eAaPmF
a	a	k8xC
další	další	k2eAgInSc4d1
|	|	kIx~
Finex	Finex	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
finex	finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BUTERIN	BUTERIN	kA
<g/>
,	,	kIx,
Vitalik	Vitalik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ethereum	Ethereum	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Next-Generation	Next-Generation	k1gInSc1
Cryptocurrency	Cryptocurrenca	k1gFnSc2
and	and	k?
Decentralized	Decentralized	k1gInSc1
Application	Application	k1gInSc1
Platform	Platform	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-01-23	2014-01-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WOOD	WOOD	kA
<g/>
,	,	kIx,
Gavin	Gavin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ethereum	Ethereum	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Secure	Secur	k1gMnSc5
Decentralised	Decentralised	k1gInSc4
Generalised	Generalised	k1gInSc1
Transaction	Transaction	k1gInSc1
Ledger	Ledger	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-06	2014-04-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ethereum	Ethereum	k1gInSc1
Classic	Classic	k1gMnSc1
-	-	kIx~
Kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
graf	graf	k1gInSc1
a	a	k8xC
další	další	k2eAgInSc1d1
|	|	kIx~
Finex	Finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
finex	finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ethereum	ethereum	k1gInSc1
<g/>
/	/	kIx~
<g/>
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GitHub	GitHub	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cryptocurrency	Cryptocurrenca	k1gFnSc2
Market	market	k1gInSc1
Capitalizations	Capitalizations	k1gInSc1
|	|	kIx~
CoinMarketCap	CoinMarketCap	k1gInSc1
<g/>
.	.	kIx.
coinmarketcap	coinmarketcap	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MIKSA	MIKSA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
je	být	k5eAaImIp3nS
Ethereum	Ethereum	k1gInSc1
<g/>
:	:	kIx,
vše	všechen	k3xTgNnSc1
co	co	k9
byste	by	kYmCp2nP
měli	mít	k5eAaImAgMnP
vědět	vědět	k5eAaImF
o	o	k7c4
tak	tak	k6eAd1
trochu	trochu	k6eAd1
jiné	jiný	k2eAgFnSc3d1
kryptoměně	kryptoměna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A.S.	A.S.	k1gMnSc1
<g/>
,	,	kIx,
Alza	Alza	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ethereum	Ethereum	k1gNnSc1
(	(	kIx(
<g/>
VŠE	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
CO	co	k3yRnSc4,k3yInSc4,k3yQnSc4
CHCETE	chtít	k5eAaImIp2nP
VĚDĚT	vědět	k5eAaImF
<g/>
)	)	kIx)
|	|	kIx~
Alza	Alza	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.alza.cz	www.alza.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ethereum	Ethereum	k1gInSc1
-	-	kIx~
těžba	těžba	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
ceny	cena	k1gFnSc2
a	a	k8xC
jak	jak	k6eAd1
ho	on	k3xPp3gInSc4
koupit	koupit	k5eAaPmF
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Vitalik	Vitalik	k1gMnSc1
Buterin	Buterin	k1gInSc1
se	se	k3xPyFc4
přiznal	přiznat	k5eAaPmAgInS
v	v	k7c6
AMA	AMA	kA
na	na	k7c4
redditu	reddita	k1gFnSc4
kolik	kolik	k9
vlastní	vlastnit	k5eAaImIp3nS
Etheru	ether	k1gInSc2
a	a	k8xC
další	další	k2eAgFnPc4d1
kryptoměny	kryptoměn	k2eAgFnPc4d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CryptoSvet	CryptoSvet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-02-21	2019-02-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chytrý	chytrý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
</s>
<s>
Litecoin	Litecoin	k1gMnSc1
</s>
<s>
Dogecoin	Dogecoin	k1gMnSc1
</s>
<s>
Dash	Dash	k1gMnSc1
</s>
<s>
Kryptoměna	Kryptoměn	k2eAgFnSc1d1
</s>
<s>
Monero	Monero	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
White	Whiit	k5eAaImRp2nP,k5eAaPmRp2nP,k5eAaBmRp2nP
paper	paper	k1gInSc4
popisující	popisující	k2eAgInSc4d1
princip	princip	k1gInSc4
fungování	fungování	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ethereum	Ethereum	k1gInSc1
</s>
<s>
Ethereum	Ethereum	k1gInSc1
prakticky	prakticky	k6eAd1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kryptoměny	Kryptoměn	k2eAgInPc1d1
založené	založený	k2eAgInPc1d1
na	na	k7c4
SHA-256	SHA-256	k1gFnSc4
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
•	•	k?
Bitcoin	Bitcoin	k1gMnSc1
cash	cash	k1gFnSc2
•	•	k?
Peercoin	Peercoin	k1gInSc1
•	•	k?
Namecoin	Namecoina	k1gFnPc2
založené	založený	k2eAgFnPc4d1
na	na	k7c6
Scryptu	Scrypt	k1gInSc6
</s>
<s>
Auroracoin	Auroracoin	k1gMnSc1
•	•	k?
Dogecoin	Dogecoin	k1gMnSc1
•	•	k?
Litecoin	Litecoin	k1gMnSc1
•	•	k?
PotCoin	PotCoin	k1gMnSc1
založené	založený	k2eAgInPc4d1
na	na	k7c4
Zerocoinu	Zerocoina	k1gFnSc4
</s>
<s>
Zcoin	Zcoin	k1gMnSc1
•	•	k?
ZeroVert	ZeroVert	k1gMnSc1
•	•	k?
Anoncoin	Anoncoin	k1gMnSc1
•	•	k?
SmartCash	SmartCash	k1gMnSc1
•	•	k?
PIVX	PIVX	kA
•	•	k?
Zcash	Zcash	k1gInSc1
•	•	k?
Komodo	komoda	k1gFnSc5
založené	založený	k2eAgInPc1d1
na	na	k7c4
CryptoNote	CryptoNot	k1gInSc5
</s>
<s>
Bytecoin	Bytecoin	k1gMnSc1
•	•	k?
Monero	Monero	k1gNnSc4
•	•	k?
DigitalNote	DigitalNot	k1gInSc5
•	•	k?
Boolberry	Boolberr	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c4
Ethash	Ethash	k1gInSc4
</s>
<s>
Ethereum	Ethereum	k1gInSc1
•	•	k?
Ethereum	Ethereum	k1gInSc1
Classic	Classic	k1gMnSc1
•	•	k?
Ubiq	Ubiq	k1gMnSc1
Jiné	jiná	k1gFnSc2
proof-of-work	proof-of-work	k1gInSc1
skripty	skript	k1gInPc4
</s>
<s>
Dash	Dash	k1gMnSc1
•	•	k?
Primecoin	Primecoin	k1gMnSc1
•	•	k?
Digibyte	Digibyt	k1gInSc5
Bez	bez	k1gInSc4
proof-of-work	proof-of-work	k1gInSc1
skriptu	skript	k1gInSc2
</s>
<s>
BlackCoin	BlackCoin	k1gMnSc1
•	•	k?
Burstcoin	Burstcoin	k1gMnSc1
•	•	k?
Counterparty	Counterparta	k1gFnSc2
•	•	k?
Enigma	enigma	k1gFnSc1
•	•	k?
FunFair	FunFair	k1gMnSc1
•	•	k?
Gridcoin	Gridcoin	k1gMnSc1
•	•	k?
Lisk	Lisk	k1gMnSc1
•	•	k?
Melonport	Melonport	k1gInSc1
•	•	k?
NEM	NEM	kA
•	•	k?
NEO	NEO	kA
•	•	k?
Nxt	Nxt	k1gMnSc1
•	•	k?
OmiseGO	OmiseGO	k1gMnSc1
•	•	k?
Polkadot	Polkadot	k1gMnSc1
•	•	k?
Qtum	Qtum	k1gMnSc1
•	•	k?
RChain	RChain	k1gMnSc1
•	•	k?
Ripple	Ripple	k1gMnSc1
•	•	k?
Simple	Simple	k1gMnSc1
Token	Token	k2eAgMnSc1d1
•	•	k?
Stellar	Stellar	k1gMnSc1
•	•	k?
Shadow	Shadow	k1gMnSc1
Technologie	technologie	k1gFnSc2
</s>
<s>
Blockchain	Blockchain	k2eAgInSc1d1
•	•	k?
Proof-of-stake	Proof-of-stake	k1gInSc1
•	•	k?
Proof-of-work	Proof-of-work	k1gInSc1
system	syst	k1gInSc7
•	•	k?
Zerocash	Zerocash	k1gInSc1
•	•	k?
Zerocoin	Zerocoin	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Anonymní	anonymní	k2eAgFnSc1d1
internetové	internetový	k2eAgNnSc4d1
bankovnictví	bankovnictví	k1gNnSc4
•	•	k?
Kryptoanarchismus	Kryptoanarchismus	k1gInSc1
•	•	k?
Digitální	digitální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Digital	Digital	kA
currency	currenca	k1gFnPc1
exchanger	exchanger	k1gMnSc1
•	•	k?
Double-spending	Double-spending	k1gInSc1
•	•	k?
Virtuální	virtuální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
</s>
