<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Les	les	k1gInSc1	les
Schtroumpfs	Schtroumpfs	k1gInSc1	Schtroumpfs
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
komiksu	komiks	k1gInSc2	komiks
belgického	belgický	k2eAgMnSc2d1	belgický
kreslíře	kreslíř	k1gMnSc2	kreslíř
Peya	Peyus	k1gMnSc2	Peyus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
i	i	k8xC	i
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
animovaný	animovaný	k2eAgInSc1d1	animovaný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
zasazeny	zasazen	k2eAgInPc1d1	zasazen
do	do	k7c2	do
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
krajiny	krajina	k1gFnSc2	krajina
nejspíš	nejspíš	k9	nejspíš
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pobřeží	pobřeží	k1gNnSc2	pobřeží
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
Walesu	Wales	k1gInSc2	Wales
nebo	nebo	k8xC	nebo
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
blízko	blízko	k7c2	blízko
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
hor.	hor.	k?	hor.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kreslíř	kreslíř	k1gMnSc1	kreslíř
Peyo	Peyo	k1gMnSc1	Peyo
přimaloval	přimalovat	k5eAaPmAgMnS	přimalovat
do	do	k7c2	do
frankofonního	frankofonní	k2eAgInSc2d1	frankofonní
belgického	belgický	k2eAgInSc2d1	belgický
komiksu	komiks	k1gInSc2	komiks
Johan	Johan	k1gMnSc1	Johan
&	&	k?	&
Pirlouit	Pirlouit	k1gMnSc1	Pirlouit
malé	malá	k1gFnSc2	malá
modré	modrý	k2eAgFnSc2d1	modrá
trpaslíky	trpaslík	k1gMnPc4	trpaslík
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
po	po	k7c6	po
zahrání	zahrání	k1gNnSc6	zahrání
na	na	k7c4	na
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
flétnu	flétna	k1gFnSc4	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Skřítkové	skřítek	k1gMnPc1	skřítek
si	se	k3xPyFc3	se
získali	získat	k5eAaPmAgMnP	získat
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
dostali	dostat	k5eAaPmAgMnP	dostat
vlastní	vlastní	k2eAgInSc4d1	vlastní
příběh	příběh	k1gInSc4	příběh
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Le	Le	k1gMnSc4	Le
Journal	Journal	k1gMnPc2	Journal
de	de	k?	de
Spirou	Spira	k1gFnSc7	Spira
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
modely	model	k1gInPc1	model
a	a	k8xC	a
hračky	hračka	k1gFnPc1	hračka
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
kluby	klub	k1gInPc1	klub
sběratelů	sběratel	k1gMnPc2	sběratel
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
figurky	figurka	k1gFnPc4	figurka
šmoulů	šmoula	k1gMnPc2	šmoula
z	z	k7c2	z
PVC	PVC	kA	PVC
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
a	a	k8xC	a
trval	trvat	k5eAaImAgInS	trvat
91	[number]	k4	91
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
další	další	k2eAgInSc1d1	další
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
Šmoulové	šmoula	k1gMnPc1	šmoula
a	a	k8xC	a
kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
74	[number]	k4	74
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
běžel	běžet	k5eAaImAgMnS	běžet
v	v	k7c6	v
USA	USA	kA	USA
americko-belgický	americkoelgický	k2eAgInSc1d1	americko-belgický
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
společnosti	společnost	k1gFnSc2	společnost
Hanna-Barbera	Hanna-Barbero	k1gNnSc2	Hanna-Barbero
Productions	Productions	k1gInSc1	Productions
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc4d1	obrovský
hit	hit	k1gInSc4	hit
a	a	k8xC	a
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
popkulturu	popkulturu	k?	popkulturu
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gNnSc4	on
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Television	Television	k1gInSc1	Television
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
videohry	videohra	k1gFnPc1	videohra
pro	pro	k7c4	pro
herní	herní	k2eAgNnSc4d1	herní
konzole	konzola	k1gFnSc3	konzola
NES	nést	k5eAaImRp2nS	nést
<g/>
,	,	kIx,	,
Super	super	k6eAd1	super
NES	nést	k5eAaImRp2nS	nést
<g/>
,	,	kIx,	,
Game	game	k1gInSc4	game
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
Atari	Atari	k1gNnSc1	Atari
<g/>
,	,	kIx,	,
Colecovision	Colecovision	k1gInSc1	Colecovision
<g/>
,	,	kIx,	,
Game	game	k1gInSc1	game
Gear	Gear	k1gInSc1	Gear
<g/>
,	,	kIx,	,
Master	master	k1gMnSc1	master
System	Syst	k1gMnSc7	Syst
<g/>
,	,	kIx,	,
Mega	mega	k1gNnSc1	mega
Drive	drive	k1gInSc1	drive
<g/>
,	,	kIx,	,
Mega	mega	k1gNnSc1	mega
CD	CD	kA	CD
PlayStation	PlayStation	k1gInSc4	PlayStation
a	a	k8xC	a
také	také	k9	také
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
PC	PC	kA	PC
a	a	k8xC	a
Amigu	Amig	k1gInSc2	Amig
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
podobní	podobný	k2eAgMnPc1d1	podobný
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
modří	modrý	k2eAgMnPc1d1	modrý
<g/>
,	,	kIx,	,
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
Šmoulinku	Šmoulinka	k1gFnSc4	Šmoulinka
<g/>
,	,	kIx,	,
Sašetku	Sašetka	k1gFnSc4	Sašetka
a	a	k8xC	a
Babču	Babču	k?	Babču
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc4	oblečení
do	do	k7c2	do
bílých	bílý	k2eAgFnPc2d1	bílá
kalhot	kalhoty	k1gFnPc2	kalhoty
(	(	kIx(	(
<g/>
jen	jen	k9	jen
Taťka	taťka	k1gMnSc1	taťka
Šmoula	šmoula	k1gMnSc1	šmoula
nosí	nosit	k5eAaImIp3nS	nosit
červené	červený	k2eAgNnSc4d1	červené
<g/>
,	,	kIx,	,
Děda	děda	k1gMnSc1	děda
Šmoula	šmoula	k1gMnSc1	šmoula
žluté	žlutý	k2eAgFnPc4d1	žlutá
s	s	k7c7	s
kšandami	kšanda	k1gFnPc7	kšanda
<g/>
,	,	kIx,	,
Šmoulíček	Šmoulíček	k1gInSc4	Šmoulíček
dupačky	dupačka	k1gFnSc2	dupačka
<g/>
,	,	kIx,	,
Farmář	farmář	k1gMnSc1	farmář
nosí	nosit	k5eAaImIp3nS	nosit
zahradnické	zahradnický	k2eAgNnSc4d1	zahradnické
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
Šmoulata	Šmoule	k1gNnPc1	Šmoule
mají	mít	k5eAaImIp3nP	mít
různá	různý	k2eAgNnPc1d1	různé
trička	tričko	k1gNnPc1	tričko
a	a	k8xC	a
Divoušek	Divoušek	k1gMnSc1	Divoušek
je	být	k5eAaImIp3nS	být
oděn	oděn	k2eAgMnSc1d1	oděn
do	do	k7c2	do
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	)
vzadu	vzadu	k6eAd1	vzadu
s	s	k7c7	s
dírou	díra	k1gFnSc7	díra
na	na	k7c4	na
malý	malý	k2eAgInSc4d1	malý
ocásek	ocásek	k1gInSc4	ocásek
a	a	k8xC	a
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
nosí	nosit	k5eAaImIp3nP	nosit
frygickou	frygický	k2eAgFnSc4d1	frygická
čapku	čapka	k1gFnSc4	čapka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
komixech	komix	k1gInPc6	komix
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc4	jejich
oblečení	oblečení	k1gNnSc4	oblečení
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
různorodější	různorodý	k2eAgMnSc1d2	různorodější
a	a	k8xC	a
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
různobarevné	různobarevný	k2eAgInPc1d1	různobarevný
kraťasy	kraťas	k1gInPc1	kraťas
<g/>
,	,	kIx,	,
trika	triko	k1gNnSc2	triko
atd.	atd.	kA	atd.
Oblečení	oblečení	k1gNnSc1	oblečení
šije	šít	k5eAaImIp3nS	šít
Šmoula	šmoula	k1gMnSc1	šmoula
Nitka	nitka	k1gFnSc1	nitka
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
seriále	seriál	k1gInSc6	seriál
jiné	jiný	k2eAgFnPc4d1	jiná
než	než	k8xS	než
bílé	bílý	k2eAgFnPc4d1	bílá
kalhoty	kalhoty	k1gFnPc4	kalhoty
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
čepici	čepice	k1gFnSc4	čepice
často	často	k6eAd1	často
odmítá	odmítat	k5eAaImIp3nS	odmítat
šít	šít	k5eAaImF	šít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
bežné	bežný	k2eAgNnSc4d1	bežný
oblečení	oblečení	k1gNnSc4	oblečení
na	na	k7c4	na
denní	denní	k2eAgNnSc4d1	denní
nošení	nošení	k1gNnSc4	nošení
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc4	kalhoty
a	a	k8xC	a
čepice	čepice	k1gFnPc4	čepice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
vyhovět	vyhovět	k5eAaPmF	vyhovět
různým	různý	k2eAgFnPc3d1	různá
úpravám	úprava	k1gFnPc3	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
chce	chtít	k5eAaImIp3nS	chtít
jiné	jiný	k2eAgNnSc4d1	jiné
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
často	často	k6eAd1	často
musí	muset	k5eAaImIp3nS	muset
ušít	ušít	k5eAaPmF	ušít
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Šmoulata	Šmoule	k1gNnPc4	Šmoule
nebo	nebo	k8xC	nebo
Šprýmař	šprýmař	k1gMnSc1	šprýmař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vydával	vydávat	k5eAaImAgInS	vydávat
za	za	k7c4	za
Dona	Don	k1gMnSc4	Don
Šmouliče	Šmoulič	k1gMnSc4	Šmoulič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komiksech	komiks	k1gInPc6	komiks
ale	ale	k8xC	ale
požadavkům	požadavek	k1gInPc3	požadavek
na	na	k7c4	na
různé	různý	k2eAgNnSc4d1	různé
oblečení	oblečení	k1gNnSc4	oblečení
často	často	k6eAd1	často
vyhoví	vyhovit	k5eAaPmIp3nP	vyhovit
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
hrdinské	hrdinský	k2eAgNnSc1d1	hrdinské
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
říkají	říkat	k5eAaImIp3nP	říkat
cokoli	cokoli	k3yInSc1	cokoli
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
šmoula	šmoula	k1gMnSc1	šmoula
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
přesně	přesně	k6eAd1	přesně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k8xS	co
tím	ten	k3xDgNnSc7	ten
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jdeme	jít	k5eAaImIp1nP	jít
šmoulovat	šmoulovat	k5eAaPmF	šmoulovat
ke	k	k7c3	k
Šmoulí	Šmoule	k1gFnPc2	Šmoule
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozumějí	rozumět	k5eAaImIp3nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
jsou	být	k5eAaImIp3nP	být
vytvořeni	vytvořit	k5eAaPmNgMnP	vytvořit
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
díl	díl	k1gInSc4	díl
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
už	už	k6eAd1	už
neobjevují	objevovat	k5eNaImIp3nP	objevovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Šmoula	šmoula	k1gMnSc1	šmoula
Slaboch	slaboch	k1gMnSc1	slaboch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
dospělých	dospělý	k2eAgMnPc2d1	dospělý
šmoulů	šmoula	k1gMnPc2	šmoula
(	(	kIx(	(
<g/>
bez	bez	k1gInSc4	bez
šmoulat	šmoulat	k5eAaPmF	šmoulat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
,	,	kIx,	,
při	při	k7c6	při
velkém	velký	k2eAgNnSc6d1	velké
šmoulím	šmoulit	k5eAaPmIp1nS	šmoulit
tancování	tancování	k1gNnSc4	tancování
se	se	k3xPyFc4	se
stavějí	stavět	k5eAaImIp3nP	stavět
do	do	k7c2	do
formace	formace	k1gFnSc2	formace
9	[number]	k4	9
<g/>
×	×	k?	×
<g/>
11	[number]	k4	11
s	s	k7c7	s
dirigujícím	dirigující	k2eAgMnSc7d1	dirigující
Taťkou	taťka	k1gMnSc7	taťka
Šmoulou	šmoula	k1gMnSc7	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
jsou	být	k5eAaImIp3nP	být
vegetariáni	vegetarián	k1gMnPc1	vegetarián
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
především	především	k6eAd1	především
sladkou	sladký	k2eAgFnSc4d1	sladká
stravu	strava	k1gFnSc4	strava
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
zeleninou	zelenina	k1gFnSc7	zelenina
a	a	k8xC	a
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
jejich	jejich	k3xOp3gInSc2	jejich
jídelníčku	jídelníček	k1gInSc2	jídelníček
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
bobule	bobule	k1gFnPc1	bobule
šmorůvky	šmorůvka	k1gFnSc2	šmorůvka
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
si	se	k3xPyFc3	se
staví	stavit	k5eAaImIp3nP	stavit
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
domky	domek	k1gInPc4	domek
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
houby	houba	k1gFnSc2	houba
a	a	k8xC	a
se	s	k7c7	s
střechou	střecha	k1gFnSc7	střecha
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
muchomůrky	muchomůrky	k?	muchomůrky
červené	červená	k1gFnSc2	červená
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
šmoula	šmoula	k1gMnSc1	šmoula
obývá	obývat	k5eAaImIp3nS	obývat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
vesnici	vesnice	k1gFnSc4	vesnice
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Šmoulice	Šmoulice	k1gFnSc1	Šmoulice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Šmoulici	Šmoulice	k1gFnSc4	Šmoulice
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
šmoula	šmoula	k1gMnSc1	šmoula
většinou	většinou	k6eAd1	většinou
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
celá	celý	k2eAgFnSc1d1	celá
vesnice	vesnice	k1gFnSc1	vesnice
dobře	dobře	k6eAd1	dobře
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Fungování	fungování	k1gNnPc1	fungování
vesnice	vesnice	k1gFnSc2	vesnice
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
nejpracovitější	pracovitý	k2eAgMnPc1d3	nejpracovitější
šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
věnují	věnovat	k5eAaImIp3nP	věnovat
svoji	svůj	k3xOyFgFnSc4	svůj
určité	určitý	k2eAgFnSc3d1	určitá
a	a	k8xC	a
důležité	důležitý	k2eAgFnSc3d1	důležitá
práci	práce	k1gFnSc3	práce
(	(	kIx(	(
<g/>
Mlsoun	mlsoun	k1gMnSc1	mlsoun
vaří	vařit	k5eAaImIp3nS	vařit
<g/>
,	,	kIx,	,
Kutil	kutit	k5eAaImAgMnS	kutit
staví	stavit	k5eAaImIp3nS	stavit
a	a	k8xC	a
opravuje	opravovat	k5eAaImIp3nS	opravovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
ti	ten	k3xDgMnPc1	ten
užiteční	užitečný	k2eAgMnPc1d1	užitečný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
nezbytní	zbytnět	k5eNaPmIp3nS	zbytnět
(	(	kIx(	(
<g/>
Malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
Silák	silák	k1gMnSc1	silák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
občas	občas	k6eAd1	občas
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
také	také	k9	také
kategorie	kategorie	k1gFnPc4	kategorie
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
užiteční	užitečný	k2eAgMnPc1d1	užitečný
(	(	kIx(	(
<g/>
Lenoch	lenoch	k1gMnSc1	lenoch
stále	stále	k6eAd1	stále
spí	spát	k5eAaImIp3nS	spát
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
,	,	kIx,	,
Nešika	nešika	k1gFnSc1	nešika
zkazí	zkazit	k5eAaPmIp3nS	zkazit
na	na	k7c4	na
co	co	k3yQnSc4	co
sáhne	sáhnout	k5eAaPmIp3nS	sáhnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Šmoulici	Šmoulice	k1gFnSc6	Šmoulice
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
časté	častý	k2eAgFnPc1d1	častá
různé	různý	k2eAgFnPc1d1	různá
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
skrytém	skrytý	k2eAgNnSc6d1	skryté
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
málokdo	málokdo	k3yInSc1	málokdo
objeví	objevit	k5eAaPmIp3nS	objevit
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
ji	on	k3xPp3gFnSc4	on
objeví	objevit	k5eAaPmIp3nS	objevit
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
tam	tam	k6eAd1	tam
dovede	dovést	k5eAaPmIp3nS	dovést
nějaký	nějaký	k3yIgMnSc1	nějaký
šmoula	šmoula	k1gMnSc1	šmoula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Šmoulice	Šmoulice	k1gFnSc1	Šmoulice
a	a	k8xC	a
nad	nad	k7c7	nad
vesnicí	vesnice	k1gFnSc7	vesnice
je	být	k5eAaImIp3nS	být
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
udržovat	udržovat	k5eAaImF	udržovat
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
protrhla	protrhnout	k5eAaPmAgFnS	protrhnout
<g/>
,	,	kIx,	,
tak	tak	k9	tak
voda	voda	k1gFnSc1	voda
celou	celý	k2eAgFnSc4d1	celá
vesnici	vesnice	k1gFnSc4	vesnice
zaplaví	zaplavit	k5eAaPmIp3nP	zaplavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okolních	okolní	k2eAgNnPc6d1	okolní
polích	pole	k1gNnPc6	pole
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
okolo	okolo	k7c2	okolo
Šmoulice	Šmoulice	k1gFnSc2	Šmoulice
jsou	být	k5eAaImIp3nP	být
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
vesnice	vesnice	k1gFnSc2	vesnice
je	být	k5eAaImIp3nS	být
Taťka	taťka	k1gMnSc1	taťka
Šmoula	šmoula	k1gMnSc1	šmoula
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ostatním	ostatní	k2eAgMnPc3d1	ostatní
šmoulům	šmoula	k1gMnPc3	šmoula
radí	radit	k5eAaImIp3nS	radit
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
pokud	pokud	k8xS	pokud
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
odjede	odjet	k5eAaPmIp3nS	odjet
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
samotní	samotný	k2eAgMnPc1d1	samotný
šmoulové	šmoula	k1gMnPc1	šmoula
nedovedou	dovést	k5eNaPmIp3nP	dovést
všechno	všechen	k3xTgNnSc4	všechen
správně	správně	k6eAd1	správně
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
a	a	k8xC	a
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
Šmoulici	Šmoulice	k1gFnSc6	Šmoulice
často	často	k6eAd1	často
obrovský	obrovský	k2eAgInSc4d1	obrovský
zmatek	zmatek	k1gInSc4	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ze	z	k7c2	z
šmoulů	šmoula	k1gMnPc2	šmoula
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
udělat	udělat	k5eAaPmF	udělat
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
neúspěšně	úspěšně	k6eNd1	úspěšně
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
největší	veliký	k2eAgMnPc1d3	veliký
nepřátelé	nepřítel	k1gMnPc1	nepřítel
jsou	být	k5eAaImIp3nP	být
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gargamel	Gargamel	k1gMnSc1	Gargamel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
kocour	kocour	k1gMnSc1	kocour
Azrael	Azrael	k1gMnSc1	Azrael
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
jdou	jít	k5eAaImIp3nP	jít
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
polapit	polapit	k5eAaPmF	polapit
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulí	Šmoulit	k5eAaPmIp3nS	Šmoulit
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
šmoulové	šmoula	k1gMnPc1	šmoula
měli	mít	k5eAaImAgMnP	mít
řídit	řídit	k5eAaImF	řídit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
šmoulózní	šmoulózní	k2eAgMnSc1d1	šmoulózní
(	(	kIx(	(
<g/>
poctivý	poctivý	k2eAgMnSc1d1	poctivý
<g/>
,	,	kIx,	,
dobrácký	dobrácký	k2eAgMnSc1d1	dobrácký
<g/>
,	,	kIx,	,
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
pomáhat	pomáhat	k5eAaImF	pomáhat
a	a	k8xC	a
k	k	k7c3	k
nikomu	nikdo	k3yNnSc3	nikdo
se	se	k3xPyFc4	se
nechovat	chovat	k5eNaImF	chovat
špatně	špatně	k6eAd1	špatně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Šmoulici	Šmoulice	k1gFnSc6	Šmoulice
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
přibližně	přibližně	k6eAd1	přibližně
rovni	roven	k2eAgMnPc1d1	roven
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdo	někdo	k3yInSc1	někdo
je	být	k5eAaImIp3nS	být
oblíbenější	oblíbený	k2eAgNnSc1d2	oblíbenější
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
někdo	někdo	k3yInSc1	někdo
míň	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejméně	málo	k6eAd3	málo
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Koumák	Koumák	k?	Koumák
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
nápadům	nápad	k1gInPc3	nápad
a	a	k8xC	a
řečem	řeč	k1gFnPc3	řeč
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
je	být	k5eAaImIp3nS	být
Taťka	taťka	k1gMnSc1	taťka
Šmoula	šmoula	k1gMnSc1	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
vlastnost	vlastnost	k1gFnSc1	vlastnost
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
každému	každý	k3xTgMnSc3	každý
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
se	se	k3xPyFc4	se
konájí	konájet	k5eAaPmIp3nS	konájet
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
karnevaly	karneval	k1gInPc1	karneval
atd.	atd.	kA	atd.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
poprvé	poprvé	k6eAd1	poprvé
seriál	seriál	k1gInSc1	seriál
Šmoulové	šmoula	k1gMnPc1	šmoula
vysílala	vysílat	k5eAaImAgFnS	vysílat
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Magion	Magion	k1gInSc1	Magion
a	a	k8xC	a
ve	v	k7c6	v
Studiu	studio	k1gNnSc6	studio
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
52	[number]	k4	52
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
okamžitě	okamžitě	k6eAd1	okamžitě
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
prodávat	prodávat	k5eAaImF	prodávat
různé	různý	k2eAgInPc4d1	různý
reklamní	reklamní	k2eAgInPc4d1	reklamní
předměty	předmět	k1gInPc4	předmět
jako	jako	k8xC	jako
například	například	k6eAd1	například
trička	tričko	k1gNnSc2	tričko
<g/>
,	,	kIx,	,
omalovánky	omalovánka	k1gFnSc2	omalovánka
a	a	k8xC	a
samolepky	samolepka	k1gFnSc2	samolepka
se	se	k3xPyFc4	se
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Oblibu	obliba	k1gFnSc4	obliba
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
díky	díky	k7c3	díky
vynikajícímu	vynikající	k2eAgInSc3d1	vynikající
dabingu	dabing	k1gInSc3	dabing
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Vlastimilem	Vlastimil	k1gMnSc7	Vlastimil
Brodským	Brodský	k1gMnSc7	Brodský
<g/>
,	,	kIx,	,
Jitkou	Jitka	k1gFnSc7	Jitka
Molavcovou	Molavcův	k2eAgFnSc7d1	Molavcův
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Císlerem	Císler	k1gMnSc7	Císler
<g/>
,	,	kIx,	,
Pavlem	Pavel	k1gMnSc7	Pavel
Trávníčkem	Trávníček	k1gMnSc7	Trávníček
<g/>
,	,	kIx,	,
Evou	Eva	k1gFnSc7	Eva
Klepáčovou	Klepáčův	k2eAgFnSc7d1	Klepáčova
<g/>
,	,	kIx,	,
Danielou	Daniela	k1gFnSc7	Daniela
Bartákovou	Bartáková	k1gFnSc7	Bartáková
<g/>
,	,	kIx,	,
Pavlem	Pavel	k1gMnSc7	Pavel
Zedníčkem	Zedníček	k1gMnSc7	Zedníček
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Dvořákem	Dvořák	k1gMnSc7	Dvořák
<g/>
,	,	kIx,	,
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Havelkou	Havelka	k1gMnSc7	Havelka
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Pragerem	Prager	k1gMnSc7	Prager
<g/>
,	,	kIx,	,
Václavem	Václav	k1gMnSc7	Václav
Postráneckým	Postránecký	k2eAgMnSc7d1	Postránecký
<g/>
,	,	kIx,	,
Inkou	Inka	k1gFnSc7	Inka
Šecovou	Šecová	k1gFnSc7	Šecová
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
i	i	k9	i
komiksové	komiksový	k2eAgFnPc1d1	komiksová
knihy	kniha	k1gFnPc1	kniha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Šmoulí	Šmoule	k1gFnPc2	Šmoule
vynálezy	vynález	k1gInPc7	vynález
a	a	k8xC	a
Černí	černit	k5eAaImIp3nP	černit
šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
VHS	VHS	kA	VHS
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
jich	on	k3xPp3gMnPc2	on
5	[number]	k4	5
vydal	vydat	k5eAaPmAgInS	vydat
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
6	[number]	k4	6
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
jich	on	k3xPp3gMnPc2	on
17	[number]	k4	17
vydal	vydat	k5eAaPmAgMnS	vydat
i	i	k9	i
Davay	Davaa	k1gFnSc2	Davaa
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
4	[number]	k4	4
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cukrárnách	cukrárna	k1gFnPc6	cukrárna
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
objevuje	objevovat	k5eAaImIp3nS	objevovat
"	"	kIx"	"
<g/>
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
zmrzlina	zmrzlina	k1gFnSc1	zmrzlina
<g/>
"	"	kIx"	"
bledě	bledě	k6eAd1	bledě
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
předem	předem	k6eAd1	předem
neodhadnutelné	odhadnutelný	k2eNgFnSc2d1	neodhadnutelná
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
vysílán	vysílat	k5eAaImNgInS	vysílat
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
epizodami	epizoda	k1gFnPc7	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
112	[number]	k4	112
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
přeobsazený	přeobsazený	k2eAgInSc4d1	přeobsazený
dabing	dabing	k1gInSc4	dabing
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
Šmoulové	šmoula	k1gMnPc1	šmoula
vysílal	vysílat	k5eAaImAgInS	vysílat
na	na	k7c6	na
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
všechny	všechen	k3xTgInPc4	všechen
poslední	poslední	k2eAgInPc4d1	poslední
díly	díl	k1gInPc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
266	[number]	k4	266
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
nový	nový	k2eAgInSc4d1	nový
dabing	dabing	k1gInSc4	dabing
<g/>
.	.	kIx.	.
</s>
<s>
Odvysíláno	odvysílán	k2eAgNnSc1d1	odvysíláno
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
kompletních	kompletní	k2eAgInPc2d1	kompletní
429	[number]	k4	429
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Šmoulové	šmoula	k1gMnPc5	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
34	[number]	k4	34
předabováno	předabovat	k5eAaBmNgNnS	předabovat
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
vysílala	vysílat	k5eAaImAgFnS	vysílat
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
důvod	důvod	k1gInSc1	důvod
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc4	Barrandov
uvedla	uvést	k5eAaPmAgFnS	uvést
jinou	jiný	k2eAgFnSc4d1	jiná
stopáž	stopáž	k1gFnSc4	stopáž
a	a	k8xC	a
drahá	drahý	k2eAgNnPc4d1	drahé
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
všech	všecek	k3xTgInPc2	všecek
18	[number]	k4	18
posledních	poslední	k2eAgInPc2d1	poslední
dílů	díl	k1gInPc2	díl
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
52	[number]	k4	52
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
dabingem	dabing	k1gInSc7	dabing
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
vysílá	vysílat	k5eAaImIp3nS	vysílat
dětská	dětský	k2eAgFnSc1d1	dětská
stanice	stanice	k1gFnSc1	stanice
ČT	ČT	kA	ČT
:	:	kIx,	:
<g/>
D	D	kA	D
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
v	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
epizodou	epizoda	k1gFnSc7	epizoda
byla	být	k5eAaImAgFnS	být
Šmoulinka	Šmoulinka	k1gFnSc1	Šmoulinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
Gargamel	Gargamel	k1gMnSc1	Gargamel
stvoří	stvořit	k5eAaPmIp3nS	stvořit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
několik	několik	k4yIc4	několik
alb	album	k1gNnPc2	album
se	se	k3xPyFc4	se
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
populární	populární	k2eAgFnSc7d1	populární
zpěvácí	zpěvácet	k5eAaPmIp3nS	zpěvácet
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
albech	album	k1gNnPc6	album
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
známé	známý	k2eAgFnPc4d1	známá
melodie	melodie	k1gFnPc4	melodie
a	a	k8xC	a
texty	text	k1gInPc4	text
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
složil	složit	k5eAaPmAgMnS	složit
Lou	Lou	k1gMnSc2	Lou
Fanánek	Fanánek	k1gMnSc1	Fanánek
Hagen	Hagen	k2eAgMnSc1d1	Hagen
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Šmoulí	Šmoule	k1gFnPc2	Šmoule
super	super	k1gInSc2	super
disko	disko	k2eAgInSc2d1	disko
šou	šou	k?	šou
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
315	[number]	k4	315
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
bylo	být	k5eAaImAgNnS	být
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
českým	český	k2eAgNnSc7d1	české
albem	album	k1gNnSc7	album
v	v	k7c6	v
období	období	k1gNnSc6	období
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Šmoulové	šmoula	k1gMnPc1	šmoula
a	a	k8xC	a
Gargamel	Gargamel	k1gInSc1	Gargamel
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Šmoulí	Šmoule	k1gFnPc2	Šmoule
super	super	k2eAgNnSc2d1	super
disko	disko	k1gNnSc2	disko
šou	šou	k?	šou
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Velká	velká	k1gFnSc1	velká
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
prázdninová	prázdninový	k2eAgFnSc1d1	prázdninová
párty	párty	k1gFnSc1	párty
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
První	první	k4xOgFnSc1	první
zimní	zimní	k2eAgFnSc1d1	zimní
šmoulympiáda	šmoulympiáda	k1gFnSc1	šmoulympiáda
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Mistrovská	mistrovský	k2eAgFnSc1d1	mistrovská
světová	světový	k2eAgFnSc1d1	světová
šmoulovaná	šmoulovaný	k2eAgFnSc1d1	šmoulovaný
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Ufouni	Ufouň	k1gMnPc7	Ufouň
a	a	k8xC	a
Marťani	marťan	k1gMnPc1	marťan
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Šmouléro	Šmouléro	k1gNnSc4	Šmouléro
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Nejslavnější	slavný	k2eAgInPc1d3	nejslavnější
šmouledy	šmouled	k1gInPc1	šmouled
a	a	k8xC	a
šmoulandy	šmouland	k1gInPc1	šmouland
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Šmoulénium	Šmoulénium	k1gNnSc4	Šmoulénium
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Velká	velká	k1gFnSc1	velká
šmoulí	šmoule	k1gFnPc2	šmoule
párty	párty	k1gFnPc2	párty
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Eurošmouliáda	Eurošmouliáda	k1gFnSc1	Eurošmouliáda
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Šmoulí	Šmoule	k1gFnPc2	Šmoule
párty	párty	k1gFnPc2	párty
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
Šmoulí	Šmoule	k1gFnPc2	Šmoule
oslava	oslava	k1gFnSc1	oslava
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988-1990	[number]	k4	1988-1990
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997-1999	[number]	k4	1997-1999
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
celkem	celek	k1gInSc7	celek
22	[number]	k4	22
VHS	VHS	kA	VHS
se	se	k3xPyFc4	se
šmoulímy	šmoulíma	k1gFnPc1	šmoulíma
díly	díl	k1gInPc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
1	[number]	k4	1
Šmoulové	šmoula	k1gMnPc1	šmoula
2	[number]	k4	2
Šmoulové	šmoula	k1gMnPc1	šmoula
3	[number]	k4	3
Šmoulové	šmoula	k1gMnPc1	šmoula
4	[number]	k4	4
Šmoulové	šmoula	k1gMnPc1	šmoula
5	[number]	k4	5
Délka	délka	k1gFnSc1	délka
jedné	jeden	k4xCgFnSc2	jeden
VHS	VHS	kA	VHS
byla	být	k5eAaImAgFnS	být
80	[number]	k4	80
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
VHS	VHS	kA	VHS
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
6	[number]	k4	6
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulí	Šmoulit	k5eAaPmIp3nP	Šmoulit
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
zázraky	zázrak	k1gInPc4	zázrak
Vánoce	Vánoce	k1gFnPc1	Vánoce
u	u	k7c2	u
šmoulů	šmoula	k1gMnPc2	šmoula
Velký	velký	k2eAgInSc1d1	velký
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
výlet	výlet	k1gInSc1	výlet
Šmoulí	Šmoule	k1gFnPc2	Šmoule
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Šmoulí	Šmoule	k1gFnPc2	Šmoule
den	dna	k1gFnPc2	dna
přátelství	přátelství	k1gNnSc2	přátelství
Šmoulí	Šmoule	k1gFnPc2	Šmoule
překvapení	překvapení	k1gNnSc2	překvapení
Prázdniny	prázdniny	k1gFnPc4	prázdniny
se	s	k7c7	s
šmouly	šmoula	k1gMnPc7	šmoula
Šmoulí	Šmoule	k1gFnPc2	Šmoule
velikonoce	velikonoce	k1gFnPc1	velikonoce
Šmoulí	Šmoule	k1gFnPc2	Šmoule
trampoty	trampota	k1gFnSc2	trampota
Šmoulí	Šmoule	k1gFnSc7	Šmoule
vynálezy	vynález	k1gInPc4	vynález
Na	na	k7c4	na
šmouly	šmoula	k1gMnPc4	šmoula
je	být	k5eAaImIp3nS	být
spolehnutí	spolehnutý	k2eAgMnPc1d1	spolehnutý
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
:	:	kIx,	:
Báječný	báječný	k2eAgInSc4d1	báječný
silvestr	silvestr	k1gInSc4	silvestr
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
:	:	kIx,	:
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
kouzel	kouzlo	k1gNnPc2	kouzlo
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
:	:	kIx,	:
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
:	:	kIx,	:
Příběhy	příběh	k1gInPc1	příběh
taťky	taťka	k1gMnSc2	taťka
šmouly	šmoula	k1gMnSc2	šmoula
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
:	:	kIx,	:
Vánoční	vánoční	k2eAgFnPc4d1	vánoční
radovánky	radovánka	k1gFnPc4	radovánka
Šmoulové	šmoula	k1gMnPc1	šmoula
ze	z	k7c2	z
školních	školní	k2eAgFnPc2d1	školní
lavic	lavice	k1gFnPc2	lavice
Délka	délka	k1gFnSc1	délka
jedné	jeden	k4xCgFnSc2	jeden
VHS	VHS	kA	VHS
byla	být	k5eAaImAgFnS	být
100	[number]	k4	100
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
VHS	VHS	kA	VHS
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
4	[number]	k4	4
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
postav	postav	k1gInSc1	postav
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
hraný	hraný	k2eAgInSc1d1	hraný
3D	[number]	k4	3D
film	film	k1gInSc4	film
Šmoulové	šmoula	k1gMnPc1	šmoula
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Smurfs	Smurfsa	k1gFnPc2	Smurfsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
šmoulové	šmoula	k1gMnPc1	šmoula
jsou	být	k5eAaImIp3nP	být
animovaní	animovaný	k2eAgMnPc1d1	animovaný
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
klasické	klasický	k2eAgFnPc1d1	klasická
loutky	loutka	k1gFnPc1	loutka
<g/>
)	)	kIx)	)
s	s	k7c7	s
hraným	hraný	k2eAgInSc7d1	hraný
filmem	film	k1gInSc7	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
navázalo	navázat	k5eAaPmAgNnS	navázat
pokračování	pokračování	k1gNnSc1	pokračování
Šmoulové	šmoula	k1gMnPc1	šmoula
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Gargamelem	Gargamel	k1gInSc7	Gargamel
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
vesničky	vesnička	k1gFnSc2	vesnička
a	a	k8xC	a
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
projdou	projít	k5eAaPmIp3nP	projít
kouzelným	kouzelný	k2eAgInSc7d1	kouzelný
vodopádem	vodopád	k1gInSc7	vodopád
<g/>
,	,	kIx,	,
ocitnou	ocitnout	k5eAaPmIp3nP	ocitnout
uprostřed	uprostřed	k7c2	uprostřed
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
vodopádem	vodopád	k1gInSc7	vodopád
projdou	projít	k5eAaPmIp3nP	projít
jen	jen	k9	jen
Nešika	nešika	k1gFnSc1	nešika
<g/>
,	,	kIx,	,
Šmoulinka	Šmoulinka	k1gFnSc1	Šmoulinka
<g/>
,	,	kIx,	,
Směloun	Směloun	k1gMnSc1	Směloun
<g/>
,	,	kIx,	,
Taťka	taťka	k1gMnSc1	taťka
Šmoula	šmoula	k1gMnSc1	šmoula
<g/>
,	,	kIx,	,
Koumák	Koumák	k?	Koumák
<g/>
,	,	kIx,	,
Mrzout	mrzout	k1gMnSc1	mrzout
<g/>
,	,	kIx,	,
Azrael	Azrael	k1gMnSc1	Azrael
a	a	k8xC	a
Gargamel	Gargamel	k1gMnSc1	Gargamel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gMnPc4	on
skoro	skoro	k6eAd1	skoro
dohnali	dohnat	k5eAaPmAgMnP	dohnat
skočí	skočit	k5eAaPmIp3nP	skočit
hned	hned	k6eAd1	hned
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
Šmoulové	šmoula	k1gMnPc1	šmoula
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Šmoulové	šmoula	k1gMnPc1	šmoula
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
jí	on	k3xPp3gFnSc7	on
doma	doma	k6eAd1	doma
dělat	dělat	k5eAaImF	dělat
nepořádek	nepořádek	k1gInSc4	nepořádek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nevědí	vědět	k5eNaImIp3nP	vědět
co	co	k9	co
je	on	k3xPp3gNnSc4	on
třeba	třeba	k6eAd1	třeba
záchod	záchod	k1gInSc1	záchod
nebo	nebo	k8xC	nebo
topinkovač	topinkovač	k1gInSc1	topinkovač
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
si	se	k3xPyFc3	se
na	na	k7c4	na
šmouly	šmoula	k1gMnPc4	šmoula
ale	ale	k8xC	ale
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
zvykne	zvyknout	k5eAaPmIp3nS	zvyknout
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
jim	on	k3xPp3gMnPc3	on
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Gargamelovi	Gargamel	k1gMnSc3	Gargamel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
šmoulové	šmoula	k1gMnPc1	šmoula
vrací	vracet	k5eAaImIp3nP	vracet
toutéž	týž	k3xTgFnSc7	týž
cestou	cesta	k1gFnSc7	cesta
zpět	zpět	k6eAd1	zpět
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2017	[number]	k4	2017
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
animovaný	animovaný	k2eAgInSc4d1	animovaný
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
Šmoulové	šmoula	k1gMnPc1	šmoula
<g/>
:	:	kIx,	:
Zapomenutá	zapomenutý	k2eAgFnSc1d1	zapomenutá
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Kellyho	Kelly	k1gMnSc2	Kelly
Asburyho	Asbury	k1gMnSc2	Asbury
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Šmoulové	šmoula	k1gMnPc1	šmoula
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Šmoulové	šmoula	k1gMnPc1	šmoula
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šmoulové	šmoula	k1gMnPc1	šmoula
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Šmoulové	šmoula	k1gMnPc1	šmoula
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc5	Databas
Šmoulové	šmoula	k1gMnPc1	šmoula
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
vydaných	vydaný	k2eAgInPc2d1	vydaný
VHS	VHS	kA	VHS
apod.	apod.	kA	apod.
Fanklub	fanklub	k1gInSc1	fanklub
Šmoulů	šmoula	k1gMnPc2	šmoula
</s>
