<s>
Miroslava	Miroslava	k1gFnSc1	Miroslava
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Miroslava	Miroslava	k1gFnSc1	Miroslava
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
už	už	k6eAd1	už
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
obdobou	obdoba	k1gFnSc7	obdoba
Slavomíry	Slavomíra	k1gFnSc2	Slavomíra
<g/>
,	,	kIx,	,
Bedřišky	Bedřiška	k1gFnSc2	Bedřiška
či	či	k8xC	či
Frederiky	Frederika	k1gFnSc2	Frederika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývá	bývat	k5eAaImIp3nS	bývat
pokládána	pokládat	k5eAaImNgFnS	pokládat
i	i	k9	i
za	za	k7c4	za
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
Ireny	Irena	k1gFnSc2	Irena
<g/>
.	.	kIx.	.
</s>
<s>
Lidově	lidově	k6eAd1	lidově
též	též	k9	též
Mirka	Mirka	k1gFnSc1	Mirka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
jménu	jméno	k1gNnSc3	jméno
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnSc1	jeho
mužská	mužský	k2eAgFnSc1d1	mužská
varianta	varianta	k1gFnSc1	varianta
Miroslav	Miroslava	k1gFnPc2	Miroslava
<g/>
.	.	kIx.	.
</s>
<s>
Zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
Mira	Mira	k1gFnSc1	Mira
znamená	znamenat	k5eAaImIp3nS	znamenat
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
oceán	oceán	k1gInSc1	oceán
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
<g/>
.	.	kIx.	.
</s>
<s>
Miruš	Miruš	k1gInSc1	Miruš
<g/>
,	,	kIx,	,
Míra	Míra	k1gFnSc1	Míra
<g/>
,	,	kIx,	,
Mirda	Mirda	k1gFnSc1	Mirda
<g/>
,	,	kIx,	,
Miruna	Miruna	k1gFnSc1	Miruna
<g/>
,	,	kIx,	,
Miri	Miri	k1gNnSc1	Miri
<g/>
,	,	kIx,	,
Mira	Mira	k1gFnSc1	Mira
<g/>
,	,	kIx,	,
Mirča	Mirča	k1gFnSc1	Mirča
<g/>
,	,	kIx,	,
Milouš	Milouš	k1gMnSc1	Milouš
<g/>
,	,	kIx,	,
Mirečka	Mirečka	k1gFnSc1	Mirečka
<g/>
,	,	kIx,	,
Mirous	Mirous	k1gMnSc1	Mirous
<g/>
,	,	kIx,	,
Miroušek	Miroušek	k1gMnSc1	Miroušek
<g/>
,	,	kIx,	,
Mirka	Mirka	k1gFnSc1	Mirka
<g/>
,	,	kIx,	,
Mirinka	Mirinka	k1gFnSc1	Mirinka
<g/>
,	,	kIx,	,
Miruška	Miruška	k1gFnSc1	Miruška
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
−	−	k?	−
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Miroslava	Miroslava	k1gFnSc1	Miroslava
Šafránková	Šafránková	k1gFnSc1	Šafránková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
Miroslava	Miroslava	k1gFnSc1	Miroslava
Kopicová	Kopicová	k1gFnSc1	Kopicová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
ministryně	ministryně	k1gFnSc1	ministryně
školství	školství	k1gNnSc2	školství
Miroslava	Miroslava	k1gFnSc1	Miroslava
Knapková	Knapková	k1gFnSc1	Knapková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
<g/>
,	,	kIx,	,
veslařka	veslařka	k1gFnSc1	veslařka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Šternová	Šternová	k1gFnSc1	Šternová
–	–	k?	–
česko-mexická	českoexický	k2eAgFnSc1d1	česko-mexický
herečka	herečka	k1gFnSc1	herečka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Vavrincová	Vavrincový	k2eAgFnSc1d1	Vavrincová
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
tenistka	tenistka	k1gFnSc1	tenistka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Pleštilová	Pleštilová	k1gFnSc1	Pleštilová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
dabérka	dabérka	k1gFnSc1	dabérka
Četnost	četnost	k1gFnSc1	četnost
jmen	jméno	k1gNnPc2	jméno
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
MV	MV	kA	MV
ČR	ČR	kA	ČR
</s>
