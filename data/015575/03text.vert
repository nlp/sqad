<s>
Druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Zničené	zničený	k2eAgNnSc1d1
čínské	čínský	k2eAgNnSc1d1
město	město	k1gNnSc1
Tan-jang	Tan-jang	k1gMnSc1
<g/>
,	,	kIx,
Čen-ťiang	Čen-ťiang	k1gMnSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1937	#num#	k4
(	(	kIx(
<g/>
de	de	k?
facto	facto	k1gNnSc4
od	od	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
)	)	kIx)
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1945	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Vítězství	vítězství	k1gNnSc1
Číny	Čína	k1gFnSc2
a	a	k8xC
spojenců	spojenec	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
Kapitulace	kapitulace	k1gFnSc1
japonských	japonský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
pevninské	pevninský	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Mandžuska	Mandžusko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Formose	Formosa	k1gFnSc3
a	a	k8xC
Francouzské	francouzský	k2eAgFnSc3d1
Indočíně	Indočína	k1gFnSc3
po	po	k7c6
16	#num#	k4
<g/>
°	°	k?
rovnoběžku	rovnoběžka	k1gFnSc4
Čínské	čínský	k2eAgFnSc3d1
republice	republika	k1gFnSc3
</s>
<s>
Čína	Čína	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
permanentním	permanentní	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
</s>
<s>
Obnovení	obnovení	k1gNnSc1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
územími	území	k1gNnPc7
získanými	získaný	k2eAgNnPc7d1
Šimonoseckou	Šimonosecký	k2eAgFnSc4d1
smlouvou	smlouva	k1gFnSc7
<g/>
,	,	kIx,
tato	tento	k3xDgNnPc1
území	území	k1gNnPc1
připadla	připadnout	k5eAaPmAgNnP
zpět	zpět	k6eAd1
Čínské	čínský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
:	:	kIx,
Kuomintang	Kuomintang	k1gInSc1
i	i	k8xC
KS	ks	kA
Číny	Čína	k1gFnSc2
Zahraniční	zahraniční	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
<g/>
:	:	kIx,
USA	USA	kA
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
SSSR	SSSR	kA
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
1941	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Mandžukuo	Mandžukuo	k6eAd1
MengkukuoNankinská	MengkukuoNankinský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
někteří	některý	k3yIgMnPc1
čínští	čínský	k2eAgMnPc1d1
vojenští	vojenský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
a	a	k8xC
váleční	váleční	k2eAgMnPc1d1
magnáti	magnát	k1gMnPc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Čankajšek	Čankajšek	k1gInSc1
Čchen	Čchen	k2eAgInSc4d1
Čcheng	Čcheng	k1gInSc4
Jen	jen	k9
Si-šan	Si-šany	k1gInPc2
Li	li	k8xS
Cung-žen	Cung-žna	k1gFnPc2
Paj	Paj	k1gMnSc2
Čchung-si	Čchung-se	k1gFnSc4
Tu	tu	k6eAd1
Jü-ming	Jü-ming	k1gInSc1
Wej	Wej	k1gFnSc2
Li-chuang	Li-chuang	k1gMnSc1
Mao	Mao	k1gMnSc1
Ce-tung	Ce-tung	k1gMnSc1
Pcheng	Pcheng	k1gMnSc1
Te-chuaj	Te-chuaj	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Stilwell	Stilwell	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Vasilevsky	Vasilevsko	k1gNnPc7
</s>
<s>
Hirohito	Hirohit	k2eAgNnSc1d1
Hideki	Hideki	k1gNnSc1
Tódžó	Tódžó	k1gMnSc2
Korečika	Korečik	k1gMnSc2
Anami	Ana	k1gFnPc7
princ	princ	k1gMnSc1
Jasuhiko	Jasuhika	k1gFnSc5
Asaka	Asako	k1gNnSc2
Šunroku	Šunrok	k1gInSc3
Hata	Hatum	k1gNnSc2
Iwane	Iwan	k1gInSc5
Macui	Macu	k1gMnPc1
Otozó	Otozó	k1gFnSc1
Jamada	Jamada	k1gFnSc1
</s>
<s>
Pchu	Pchu	k5eAaPmIp1nS
I	i	k9
Čang	Čang	k1gInSc1
Ťing-chuej	Ťing-chuej	k1gInSc1
Čang	Čang	k1gInSc1
Chaj	Chaj	k1gInSc1
-pcheng	-pchenga	k1gFnPc2
Demčigdonrov	Demčigdonrov	k1gInSc1
Wang	Wang	k1gMnSc1
Ťing-wej	Ťing-wej	k1gInSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
5	#num#	k4
600	#num#	k4
000	#num#	k4
</s>
<s>
3	#num#	k4
600	#num#	k4
sovětských	sovětský	k2eAgMnPc2d1
dobrovolníků	dobrovolník	k1gMnPc2
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
900	#num#	k4
amerických	americký	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
900	#num#	k4
000	#num#	k4
</s>
<s>
900	#num#	k4
000	#num#	k4
čínských	čínský	k2eAgMnPc2d1
kolaborantů	kolaborant	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
Nacionalisté	nacionalista	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
320	#num#	k4
000	#num#	k4
zabito	zabít	k5eAaPmNgNnS
<g/>
1	#num#	k4
797	#num#	k4
000	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
<g/>
120	#num#	k4
000	#num#	k4
ztraceno	ztraceno	k1gNnSc4
v	v	k7c6
boji	boj	k1gInSc6
</s>
<s>
Komunisté	komunista	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
500	#num#	k4
000	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
a	a	k8xC
zabito	zabít	k5eAaPmNgNnS
</s>
<s>
1	#num#	k4
055	#num#	k4
000	#num#	k4
zabito	zabít	k5eAaPmNgNnS
<g/>
1	#num#	k4
172	#num#	k4
200	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
</s>
<s>
17	#num#	k4
000	#num#	k4
000	#num#	k4
–	–	k?
22	#num#	k4
000	#num#	k4
000	#num#	k4
čínských	čínský	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
období	období	k1gNnSc6
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
První	první	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1937	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1945	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
válečným	válečný	k2eAgInSc7d1
konfliktem	konflikt	k1gInSc7
mezi	mezi	k7c7
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
a	a	k8xC
Japonským	japonský	k2eAgNnSc7d1
císařstvím	císařství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
většiny	většina	k1gFnSc2
historiků	historik	k1gMnPc2
předcházela	předcházet	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
válka	válka	k1gFnSc1
datu	datum	k1gNnSc6
vzniku	vznik	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
ovšem	ovšem	k9
počátek	počátek	k1gInSc4
této	tento	k3xDgFnSc2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
den	den	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1937	#num#	k4
(	(	kIx(
<g/>
incident	incident	k1gInSc4
na	na	k7c6
mostě	most	k1gInSc6
Marca	Marc	k2eAgFnSc1d1
Pola	pola	k1gFnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
považují	považovat	k5eAaImIp3nP
za	za	k7c4
počátek	počátek	k1gInSc4
celosvětové	celosvětový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
však	však	k9
o	o	k7c4
menšinový	menšinový	k2eAgInSc4d1
názor	názor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
představovala	představovat	k5eAaImAgFnS
vyvrcholení	vyvrcholení	k1gNnSc4
dlouholetého	dlouholetý	k2eAgNnSc2d1
nepřátelství	nepřátelství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
projevovalo	projevovat	k5eAaImAgNnS
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
rozsáhlých	rozsáhlý	k2eAgInPc2d1
ozbrojených	ozbrojený	k2eAgInPc2d1
incidentů	incident	k1gInPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
vyprovokovaných	vyprovokovaný	k2eAgFnPc2d1
Japonskem	Japonsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
realizovat	realizovat	k5eAaBmF
imperialistické	imperialistický	k2eAgInPc4d1
plány	plán	k1gInPc4
Japonského	japonský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
na	na	k7c4
získání	získání	k1gNnSc4
pozice	pozice	k1gFnSc2
první	první	k4xOgFnSc2
velmoci	velmoc	k1gFnSc2
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozrostla	rozrůst	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
ve	v	k7c4
světový	světový	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
Japonsko	Japonsko	k1gNnSc1
napadlo	napadnout	k5eAaPmAgNnS
i	i	k9
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
a	a	k8xC
britské	britský	k2eAgFnSc2d1
a	a	k8xC
nizozemské	nizozemský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončila	skončit	k5eAaPmAgFnS
až	až	k9
kapitulací	kapitulace	k1gFnSc7
japonských	japonský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
září	září	k1gNnSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Invaze	invaze	k1gFnSc1
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
Číny	Čína	k1gFnSc2
okupované	okupovaný	k2eAgNnSc4d1
japonskými	japonský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
</s>
<s>
Ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
získali	získat	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
vliv	vliv	k1gInSc4
na	na	k7c4
čínského	čínský	k2eAgMnSc4d1
generála	generál	k1gMnSc4
Čang	Čang	k1gMnSc1
Cuo-lina	Cuo-lina	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
faktickým	faktický	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
Mandžuska	Mandžusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
posílalo	posílat	k5eAaImAgNnS
Čangovi	Čangův	k2eAgMnPc1d1
zbraně	zbraň	k1gFnSc2
<g/>
,	,	kIx,
munici	munice	k1gFnSc4
<g/>
,	,	kIx,
podpůrný	podpůrný	k2eAgInSc4d1
materiál	materiál	k1gInSc4
a	a	k8xC
vojenské	vojenský	k2eAgMnPc4d1
poradce	poradce	k1gMnPc4
ve	v	k7c6
snaze	snaha	k1gFnSc6
upevnit	upevnit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
vztah	vztah	k1gInSc1
k	k	k7c3
Japonsku	Japonsko	k1gNnSc3
však	však	k9
nebyl	být	k5eNaImAgInS
jednoznačný	jednoznačný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
byl	být	k5eAaImAgMnS
Čang	Čang	k1gMnSc1
poražen	porazit	k5eAaPmNgMnS
generálem	generál	k1gMnSc7
Čankajškem	Čankajšek	k1gInSc7
a	a	k8xC
japonští	japonský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
Čanga	Čang	k1gMnSc4
zavraždili	zavraždit	k5eAaPmAgMnP
výbuchem	výbuch	k1gInSc7
nálože	nálož	k1gFnSc2
na	na	k7c6
trati	trať	k1gFnSc6
vlaku	vlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
Mandžuska	Mandžusko	k1gNnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
Čang	Čang	k1gMnSc1
Süe-liang	Süe-liang	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
však	však	k9
vymanil	vymanit	k5eAaPmAgInS
z	z	k7c2
vlivu	vliv	k1gInSc2
Japonska	Japonsko	k1gNnSc2
a	a	k8xC
přísahal	přísahat	k5eAaImAgMnS
věrnost	věrnost	k1gFnSc4
Čankajškově	Čankajškův	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
Kuantungská	Kuantungský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
400	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
dostala	dostat	k5eAaPmAgFnS
rozkazy	rozkaz	k1gInPc4
na	na	k7c4
přípravu	příprava	k1gFnSc4
invaze	invaze	k1gFnSc2
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
bylo	být	k5eAaImAgNnS
bohaté	bohatý	k2eAgInPc4d1
na	na	k7c4
nerostné	nerostný	k2eAgFnPc4d1
suroviny	surovina	k1gFnPc4
a	a	k8xC
úrodnou	úrodný	k2eAgFnSc4d1
zemědělskou	zemědělský	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1931	#num#	k4
provedli	provést	k5eAaPmAgMnP
japonští	japonský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
zinscenovaný	zinscenovaný	k2eAgInSc4d1
bombový	bombový	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
jihomandžuskou	jihomandžuský	k2eAgFnSc4d1
železniční	železniční	k2eAgFnSc4d1
trať	trať	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mukdenský	Mukdenský	k2eAgInSc1d1
incident	incident	k1gInSc1
započal	započnout	k5eAaPmAgInS
okolo	okolo	k7c2
22	#num#	k4
<g/>
.	.	kIx.
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
explodovala	explodovat	k5eAaBmAgFnS
na	na	k7c6
kolejích	kolej	k1gFnPc6
nálož	nálož	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
po	po	k7c6
nich	on	k3xPp3gFnPc6
přejel	přejet	k5eAaPmAgInS
pravidelný	pravidelný	k2eAgInSc1d1
expres	expres	k1gInSc1
spojující	spojující	k2eAgInSc1d1
Mukden	Mukdna	k1gFnPc2
s	s	k7c7
městem	město	k1gNnSc7
Sin-ting	Sin-ting	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlak	vlak	k1gInSc1
dojel	dojet	k5eAaPmAgInS
v	v	k7c6
pořádku	pořádek	k1gInSc6
na	na	k7c6
nádraží	nádraží	k1gNnSc6
v	v	k7c6
Mukdenu	Mukden	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
cestujících	cestující	k1gMnPc2
netuší	tušit	k5eNaImIp3nS
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
jen	jen	k9
hrstka	hrstka	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
vlak	vlak	k1gInSc4
zaútočili	zaútočit	k5eAaPmAgMnP
„	„	k?
<g/>
čínští	čínský	k2eAgMnPc1d1
bandité	bandita	k1gMnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
souprava	souprava	k1gFnSc1
neutrpěla	utrpět	k5eNaPmAgFnS
ani	ani	k8xC
škrábnutí	škrábnutí	k1gNnPc1
<g/>
,	,	kIx,
znělo	znět	k5eAaImAgNnS
nepřesvědčivě	přesvědčivě	k6eNd1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
hlášení	hlášení	k1gNnSc1
jistého	jistý	k2eAgMnSc2d1
japonského	japonský	k2eAgMnSc2d1
poddůstojníka	poddůstojník	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
krátce	krátce	k6eAd1
nato	nato	k6eAd1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
hlídka	hlídka	k1gFnSc1
zažila	zažít	k5eAaPmAgFnS
„	„	k?
<g/>
bombový	bombový	k2eAgInSc1d1
přepad	přepad	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
vzápětí	vzápětí	k6eAd1
na	na	k7c4
ně	on	k3xPp3gMnPc4
„	„	k?
<g/>
teroristé	terorista	k1gMnPc1
<g/>
“	“	k?
zahájili	zahájit	k5eAaPmAgMnP
kulometnou	kulometný	k2eAgFnSc4d1
palbu	palba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1
poté	poté	k6eAd1
zahájily	zahájit	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
Kuantungské	Kuantungský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
plukovníka	plukovník	k1gMnSc2
Igatakiho	Igataki	k1gMnSc2
<g/>
,	,	kIx,
mající	mající	k2eAgFnSc4d1
za	za	k7c4
úkol	úkol	k1gInSc4
střežit	střežit	k5eAaImF
bezpečnost	bezpečnost	k1gFnSc4
železnice	železnice	k1gFnSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
odvetnou	odvetný	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
k	k	k7c3
posílení	posílení	k1gNnSc3
bezpečnosti	bezpečnost	k1gFnSc2
japonských	japonský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc2
majetku	majetek	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
oficiální	oficiální	k2eAgNnSc1d1
zdůvodnění	zdůvodnění	k1gNnSc1
agrese	agrese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
dopadly	dopadnout	k5eAaPmAgFnP
dělostřelecké	dělostřelecký	k2eAgInPc4d1
granáty	granát	k1gInPc4
na	na	k7c4
severní	severní	k2eAgFnPc4d1
kasárny	kasárny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc7
zde	zde	k6eAd1
dosáhli	dosáhnout	k5eAaPmAgMnP
momentu	moment	k1gInSc2
překvapení	překvapený	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
přestože	přestože	k8xS
na	na	k7c4
kasárny	kasárny	k1gFnPc4
útočilo	útočit	k5eAaImAgNnS
necelých	celý	k2eNgNnPc2d1
500	#num#	k4
japonských	japonský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
jádro	jádro	k1gNnSc1
zhruba	zhruba	k6eAd1
desetitisícové	desetitisícový	k2eAgNnSc4d1
7	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
generála	generál	k1gMnSc2
Wang-I-čua	Wang-I-čuus	k1gMnSc2
se	se	k3xPyFc4
v	v	k7c6
panice	panika	k1gFnSc6
rozuteklo	rozutéct	k5eAaPmAgNnS
a	a	k8xC
sám	sám	k3xTgMnSc1
generál	generál	k1gMnSc1
unikl	uniknout	k5eAaPmAgMnS
zajetí	zajetí	k1gNnSc4
jen	jen	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
opustil	opustit	k5eAaPmAgMnS
město	město	k1gNnSc4
v	v	k7c6
přestrojení	přestrojení	k1gNnSc6
za	za	k7c4
dělníka	dělník	k1gMnSc4
(	(	kIx(
<g/>
kuli	kuli	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
scénář	scénář	k1gInSc1
měl	mít	k5eAaImAgInS
i	i	k9
japonský	japonský	k2eAgInSc1d1
útok	útok	k1gInSc1
v	v	k7c6
městě	město	k1gNnSc6
samotném	samotný	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
pod	pod	k7c7
záminkou	záminka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
postaví	postavit	k5eAaPmIp3nS
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
plavecký	plavecký	k2eAgInSc4d1
bazén	bazén	k1gInSc4
<g/>
,	,	kIx,
umístili	umístit	k5eAaPmAgMnP
v	v	k7c6
betonových	betonový	k2eAgInPc6d1
valech	val	k1gInPc6
dva	dva	k4xCgInPc1
moždíře	moždíř	k1gInPc1
ráže	ráže	k1gFnSc2
240	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
ukořistili	ukořistit	k5eAaPmAgMnP
Rusům	Rus	k1gMnPc3
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
a	a	k8xC
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mířily	mířit	k5eAaImAgInP
na	na	k7c4
policejní	policejní	k2eAgNnPc4d1
kasárna	kasárna	k1gNnPc4
a	a	k8xC
letiště	letiště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	dík	k1gInPc4
tomu	ten	k3xDgMnSc3
dobyly	dobýt	k5eAaPmAgInP
početně	početně	k6eAd1
slabé	slabý	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
Kanto	Kanto	k1gNnSc4
Gun	Gun	k1gFnSc2
oba	dva	k4xCgInPc4
cíle	cíl	k1gInPc4
bez	bez	k7c2
většího	veliký	k2eAgNnSc2d2
úsilí	úsilí	k1gNnSc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
ve	v	k7c4
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
byl	být	k5eAaImAgInS
obsazen	obsadit	k5eAaPmNgInS
Mukden	Mukdno	k1gNnPc2
(	(	kIx(
<g/>
Šen-jang	Šen-jang	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
obsazeného	obsazený	k2eAgNnSc2d1
města	město	k1gNnSc2
přijel	přijet	k5eAaPmAgMnS
japonský	japonský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Hondžo	Hondžo	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
převzal	převzít	k5eAaPmAgMnS
zodpovědnost	zodpovědnost	k1gFnSc4
za	za	k7c4
pokračování	pokračování	k1gNnSc4
operace	operace	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
okamžitě	okamžitě	k6eAd1
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
velitelem	velitel	k1gMnSc7
japonských	japonský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
Koreji	Korea	k1gFnSc6
<g/>
,	,	kIx,
generálporučíkem	generálporučík	k1gMnSc7
Hajašim	Hajašima	k1gFnPc2
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
leteckou	letecký	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
a	a	k8xC
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Hajaši	Hajaše	k1gFnSc4
podporu	podpora	k1gFnSc4
slíbil	slíbit	k5eAaPmAgMnS
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
překročila	překročit	k5eAaPmAgNnP
japonská	japonský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
hranice	hranice	k1gFnSc2
a	a	k8xC
vtrhla	vtrhnout	k5eAaPmAgFnS
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
byl	být	k5eAaImAgMnS
dobyt	dobyt	k2eAgMnSc1d1
Cicikar	Cicikar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
Kuantungská	Kuantungský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
posílená	posílená	k1gFnSc1
počtem	počet	k1gInSc7
65	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
obsadila	obsadit	k5eAaPmAgFnS
za	za	k7c2
minimálního	minimální	k2eAgInSc2d1
odporu	odpor	k1gInSc2
ustupujících	ustupující	k2eAgNnPc2d1
čínských	čínský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
a	a	k8xC
za	za	k7c2
nečinného	činný	k2eNgNnSc2d1
přihlížení	přihlížení	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
přes	přes	k7c4
bezmocné	bezmocný	k2eAgNnSc4d1
protesty	protest	k1gInPc1
čínského	čínský	k2eAgMnSc2d1
delegáta	delegát	k1gMnSc2
u	u	k7c2
Společnosti	společnost	k1gFnSc2
národů	národ	k1gInPc2
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
do	do	k7c2
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1932	#num#	k4
celé	celý	k2eAgNnSc4d1
mandžuské	mandžuský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
Japonsko	Japonsko	k1gNnSc1
vyslalo	vyslat	k5eAaPmAgNnS
na	na	k7c6
zajištění	zajištění	k1gNnSc6
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
téměř	téměř	k6eAd1
700	#num#	k4
000	#num#	k4
kolonistů	kolonista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odporu	odpor	k1gInSc6
proti	proti	k7c3
japonské	japonský	k2eAgFnSc3d1
okupaci	okupace	k1gFnSc3
pokračovalo	pokračovat	k5eAaImAgNnS
asi	asi	k9
200	#num#	k4
000	#num#	k4
partyzánů	partyzán	k1gMnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
většinu	většina	k1gFnSc4
těchto	tento	k3xDgFnPc2
jednotek	jednotka	k1gFnPc2
se	se	k3xPyFc4
Japoncům	Japonec	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
rozprášit	rozprášit	k5eAaPmF
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
loutkový	loutkový	k2eAgInSc1d1
stát	stát	k1gInSc1
Mandžukuo	Mandžukuo	k6eAd1
<g/>
,	,	kIx,
do	do	k7c2
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
čela	čelo	k1gNnSc2
byl	být	k5eAaImAgMnS
jako	jako	k9
císař	císař	k1gMnSc1
dosazen	dosazen	k2eAgMnSc1d1
Pchu	Pch	k2eAgFnSc4d1
I	I	kA
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgMnSc1d1
poslední	poslední	k2eAgMnSc1d1
čínský	čínský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Přípravy	příprava	k1gFnPc1
na	na	k7c6
invazi	invaze	k1gFnSc6
do	do	k7c2
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Japoncům	Japonec	k1gMnPc3
obsazení	obsazení	k1gNnSc4
Mandžuska	Mandžusko	k1gNnSc2
nestačilo	stačit	k5eNaBmAgNnS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
usilovali	usilovat	k5eAaImAgMnP
o	o	k7c4
podmanění	podmanění	k1gNnPc4
dalších	další	k2eAgNnPc2d1
čínských	čínský	k2eAgNnPc2d1
území	území	k1gNnPc2
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
Mandžukua	Mandžuku	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Číně	Čína	k1gFnSc6
spolu	spolu	k6eAd1
soupeřili	soupeřit	k5eAaImAgMnP
a	a	k8xC
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
sobě	se	k3xPyFc3
především	především	k6eAd1
Čajkajškovi	Čajkajškův	k2eAgMnPc1d1
nacionalisté	nacionalista	k1gMnPc1
a	a	k8xC
komunisté	komunista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vylodění	vylodění	k1gNnSc6
čtyř	čtyři	k4xCgFnPc2
pěších	pěší	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
brigády	brigáda	k1gFnSc2
námořního	námořní	k2eAgInSc2d1
výsadku	výsadek	k1gInSc2
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
a	a	k8xC
ostřelování	ostřelování	k1gNnSc6
Nankingu	Nanking	k1gInSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
čínských	čínský	k2eAgMnPc2d1
nacionalistů	nacionalista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozšířené	rozšířený	k2eAgFnSc3d1
okupaci	okupace	k1gFnSc3
Mandžuska	Mandžusko	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
území	území	k1gNnSc2
dříve	dříve	k6eAd2
ovládaných	ovládaný	k2eAgInPc2d1
Ruským	ruský	k2eAgNnSc7d1
impériem	impérium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
Japonsko	Japonsko	k1gNnSc1
odmítlo	odmítnout	k5eAaPmAgNnS
mezinárodní	mezinárodní	k2eAgNnSc1d1
odsouzení	odsouzení	k1gNnSc1
své	svůj	k3xOyFgFnSc2
agrese	agrese	k1gFnSc2
vůči	vůči	k7c3
Číně	Čína	k1gFnSc3
a	a	k8xC
vystoupilo	vystoupit	k5eAaPmAgNnS
ze	z	k7c2
Společnosti	společnost	k1gFnSc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgFnP
jeho	jeho	k3xOp3gFnSc4
další	další	k2eAgFnPc1d1
akce	akce	k1gFnPc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Vnitřním	vnitřní	k2eAgNnSc6d1
Mongolsku	Mongolsko	k1gNnSc6
a	a	k8xC
Mandžusku	Mandžusko	k1gNnSc6
za	za	k7c7
účelem	účel	k1gInSc7
destabilizace	destabilizace	k1gFnSc2
čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
Japonsko	Japonsko	k1gNnSc1
prohlásilo	prohlásit	k5eAaPmAgNnS
Washingtonskou	washingtonský	k2eAgFnSc4d1
úmluvu	úmluva	k1gFnSc4
za	za	k7c7
neplatnou	platný	k2eNgFnSc7d1
a	a	k8xC
s	s	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
Itálií	Itálie	k1gFnSc7
podepsalo	podepsat	k5eAaPmAgNnS
Pakt	pakt	k1gInSc1
proti	proti	k7c3
Kominterně	Kominterna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Invaze	invaze	k1gFnSc1
do	do	k7c2
Číny	Čína	k1gFnSc2
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
Mukdenu	Mukden	k1gInSc2
<g/>
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
v	v	k7c6
červnu	červen	k1gInSc6
1941	#num#	k4
během	během	k7c2
japonského	japonský	k2eAgNnSc2d1
bombardování	bombardování	k1gNnSc2
města	město	k1gNnSc2
Čchung-čching	Čchung-čching	k1gInSc4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
vyčlenilo	vyčlenit	k5eAaPmAgNnS
pro	pro	k7c4
tažení	tažení	k1gNnSc4
na	na	k7c4
300	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
dobře	dobře	k6eAd1
vyzbrojení	vyzbrojený	k2eAgMnPc1d1
i	i	k8xC
vycvičení	vycvičený	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
Kuomintangu	Kuomintang	k1gInSc2
a	a	k8xC
čínských	čínský	k2eAgMnPc2d1
komunistů	komunista	k1gMnPc2
měli	mít	k5eAaImAgMnP
sice	sice	k8xC
početní	početní	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
špatný	špatný	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
<g/>
,	,	kIx,
nekvalitní	kvalitní	k2eNgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
a	a	k8xC
absence	absence	k1gFnSc1
námořních	námořní	k2eAgFnPc2d1
i	i	k8xC
leteckých	letecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
je	být	k5eAaImIp3nS
stavěla	stavět	k5eAaImAgFnS
do	do	k7c2
velké	velký	k2eAgFnSc2d1
nevýhody	nevýhoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kuomintang	Kuomintang	k1gInSc4
a	a	k8xC
komunisté	komunista	k1gMnPc1
navíc	navíc	k6eAd1
byli	být	k5eAaImAgMnP
ve	v	k7c6
stavu	stav	k1gInSc6
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1937	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
incidentu	incident	k1gInSc3
na	na	k7c6
mostě	most	k1gInSc6
Marca	Marc	k2eAgFnSc1d1
Pola	pola	k1gFnSc1
–	–	k?
Japonci	Japonec	k1gMnPc1
uměle	uměle	k6eAd1
vyvolané	vyvolaný	k2eAgFnSc6d1
potyčce	potyčka	k1gFnSc6
na	na	k7c6
předměstí	předměstí	k1gNnSc6
Pekingu	Peking	k1gInSc2
–	–	k?
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
záminkou	záminka	k1gFnSc7
pro	pro	k7c4
vypuknutí	vypuknutí	k1gNnSc4
Druhé	druhý	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
rychle	rychle	k6eAd1
obsadila	obsadit	k5eAaPmAgFnS
Tchien-ťin	Tchien-ťin	k1gInSc4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
)	)	kIx)
i	i	k9
Peking	Peking	k1gInSc1
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
postoupila	postoupit	k5eAaPmAgFnS
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
se	se	k3xPyFc4
však	však	k9
postup	postup	k1gInSc1
začal	začít	k5eAaPmAgInS
zpomalovat	zpomalovat	k5eAaImF
kvůli	kvůli	k7c3
prodloužení	prodloužení	k1gNnSc3
zásobovacích	zásobovací	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
vylodilo	vylodit	k5eAaPmAgNnS
10	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
trvající	trvající	k2eAgFnSc1d1
92	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
za	za	k7c4
letecké	letecký	k2eAgFnPc4d1
podpory	podpora	k1gFnPc4
konečně	konečně	k6eAd1
dobyto	dobýt	k5eAaPmNgNnS
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
čínští	čínský	k2eAgMnPc1d1
komunističtí	komunistický	k2eAgMnPc1d1
partyzáni	partyzán	k1gMnPc1
porazili	porazit	k5eAaPmAgMnP
japonské	japonský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
v	v	k7c6
provincii	provincie	k1gFnSc6
Šan-si	Šan-se	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
velmi	velmi	k6eAd1
pozvedlo	pozvednout	k5eAaPmAgNnS
morálku	morálka	k1gFnSc4
Číňanů	Číňan	k1gMnPc2
a	a	k8xC
zesílilo	zesílit	k5eAaPmAgNnS
odpor	odpor	k1gInSc4
proti	proti	k7c3
okupantům	okupant	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
nespokojení	spokojený	k2eNgMnPc1d1
a	a	k8xC
rozhněvaní	rozhněvaný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
nezabránilo	zabránit	k5eNaPmAgNnS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
listopadu	listopad	k1gInSc6
tři	tři	k4xCgFnPc4
japonské	japonský	k2eAgFnPc4d1
armády	armáda	k1gFnPc4
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
Nanking	Nanking	k1gInSc4
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
nacionalistů	nacionalista	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
jej	on	k3xPp3gNnSc4
dobyly	dobýt	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
bylo	být	k5eAaImAgNnS
obyvatelstvo	obyvatelstvo	k1gNnSc1
vystaveno	vystaven	k2eAgNnSc1d1
teroru	teror	k1gInSc3
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
popravám	poprava	k1gFnPc3
<g/>
,	,	kIx,
mučení	mučení	k1gNnSc6
a	a	k8xC
hromadnému	hromadný	k2eAgNnSc3d1
znásilňování	znásilňování	k1gNnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jej	on	k3xPp3gInSc4
zaznamenala	zaznamenat	k5eAaPmAgFnS
řada	řada	k1gFnSc1
zahraničních	zahraniční	k2eAgMnPc2d1
pozorovatelů	pozorovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
až	až	k9
na	na	k7c4
300	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
je	být	k5eAaImIp3nS
uváděno	uvádět	k5eAaImNgNnS
něco	něco	k6eAd1
kolem	kolem	k7c2
42	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
akt	akt	k1gInSc1
známý	známý	k2eAgInSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Znásilnění	znásilnění	k1gNnSc1
Nankingu	Nanking	k1gInSc2
<g/>
“	“	k?
představuje	představovat	k5eAaImIp3nS
vážný	vážný	k2eAgInSc1d1
problém	problém	k1gInSc1
v	v	k7c6
čínsko-japonských	čínsko-japonský	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
zahájila	zahájit	k5eAaPmAgFnS
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
ofenzívu	ofenzíva	k1gFnSc4
na	na	k7c6
Žluté	žlutý	k2eAgFnSc6d1
řece	řeka	k1gFnSc6
a	a	k8xC
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
dobývání	dobývání	k1gNnSc6
čínských	čínský	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
března	březen	k1gInSc2
a	a	k8xC
dubna	duben	k1gInSc2
porazili	porazit	k5eAaPmAgMnP
Číňané	Číňan	k1gMnPc1
vedení	vedení	k1gNnPc2
generálem	generál	k1gMnSc7
Li	li	k8xS
Cung-ženem	Cung-ženo	k1gNnSc7
Japonce	Japonec	k1gMnSc2
v	v	k7c6
krvavé	krvavý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Š	Š	kA
<g/>
’	’	k?
<g/>
-ťia-čuangu	-ťia-čuang	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
mezitím	mezitím	k6eAd1
dobyla	dobýt	k5eAaPmAgFnS
přístavy	přístav	k1gInPc4
Sia-men	Sia-men	k1gInSc4
a	a	k8xC
Fu-čou	Fu-čá	k1gFnSc4
na	na	k7c6
jihu	jih	k1gInSc6
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
započala	započnout	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
proti	proti	k7c3
Chan-kchou	Chan-kcha	k1gMnSc7
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
část	část	k1gFnSc1
města	město	k1gNnSc2
Wu-chan	Wu-chany	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prozatímnímu	prozatímní	k2eAgNnSc3d1
hlavnímu	hlavní	k2eAgNnSc3d1
městu	město	k1gNnSc3
nacionalistů	nacionalista	k1gMnPc2
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
Japonci	Japonec	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
Nan-jang	Nan-jang	k1gInSc4
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
první	první	k4xOgFnSc3
bitvě	bitva	k1gFnSc3
o	o	k7c6
Čchang-ša	Čchang-šum	k1gNnPc4
v	v	k7c6
provincii	provincie	k1gFnSc6
Chu-nan	Chu-nany	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
utrpěla	utrpět	k5eAaPmAgFnS
strašlivé	strašlivý	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1940	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Chu-pej	Chu-pej	k1gFnSc2
<g/>
,	,	kIx,
namířená	namířený	k2eAgNnPc1d1
proti	proti	k7c3
nové	nový	k2eAgFnSc3d1
baště	bašta	k1gFnSc3
nacionalistů	nacionalista	k1gMnPc2
–	–	k?
Čchung-čchingu	Čchung-čching	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
však	však	k9
již	již	k6eAd1
byli	být	k5eAaImAgMnP
vyčerpaní	vyčerpaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
unavení	unavený	k2eAgMnPc1d1
a	a	k8xC
potřebovali	potřebovat	k5eAaImAgMnP
doplnit	doplnit	k5eAaPmF
stavy	stav	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
proto	proto	k6eAd1
rozhodnuto	rozhodnut	k2eAgNnSc1d1
vyhlásit	vyhlásit	k5eAaPmF
mobilizaci	mobilizace	k1gFnSc4
a	a	k8xC
v	v	k7c6
červenci	červenec	k1gInSc6
armáda	armáda	k1gFnSc1
povolala	povolat	k5eAaPmAgFnS
do	do	k7c2
zbraně	zbraň	k1gFnSc2
jeden	jeden	k4xCgMnSc1
milion	milion	k4xCgInSc4
mužů	muž	k1gMnPc2
(	(	kIx(
<g/>
34	#num#	k4
divizí	divize	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
masivní	masivní	k2eAgNnSc1d1
posílení	posílení	k1gNnSc1
bojových	bojový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
hlavně	hlavně	k9
Kuantungské	Kuantungský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kapitulaci	kapitulace	k1gFnSc3
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
odevzdala	odevzdat	k5eAaPmAgFnS
Japonsku	Japonsko	k1gNnSc3
po	po	k7c6
německém	německý	k2eAgInSc6d1
nátlaku	nátlak	k1gInSc6
území	území	k1gNnSc2
Indočíny	Indočína	k1gFnSc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
leteckými	letecký	k2eAgFnPc7d1
a	a	k8xC
námořními	námořní	k2eAgFnPc7d1
základnami	základna	k1gFnPc7
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
vstoupila	vstoupit	k5eAaPmAgFnS
5	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
do	do	k7c2
Hanoje	Hanoj	k1gFnSc2
a	a	k8xC
o	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
uzavřen	uzavřen	k2eAgInSc1d1
pakt	pakt	k1gInSc1
–	–	k?
Osa	osa	k1gFnSc1
Berlín	Berlín	k1gInSc1
<g/>
–	–	k?
<g/>
Řím	Řím	k1gInSc1
<g/>
–	–	k?
<g/>
Tokio	Tokio	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
snaze	snaha	k1gFnSc6
zastavit	zastavit	k5eAaPmF
japonskou	japonský	k2eAgFnSc4d1
agresi	agrese	k1gFnSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
vyvíjely	vyvíjet	k5eAaImAgFnP
USA	USA	kA
na	na	k7c4
Japonsko	Japonsko	k1gNnSc4
nátlak	nátlak	k1gInSc1
prostřednictvím	prostřednictvím	k7c2
ekonomických	ekonomický	k2eAgNnPc2d1
embarg	embargo	k1gNnPc2
<g/>
,	,	kIx,
uvalených	uvalený	k2eAgMnPc2d1
na	na	k7c4
export	export	k1gInSc4
železné	železný	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
<g/>
,	,	kIx,
mědi	měď	k1gFnSc2
<g/>
,	,	kIx,
niklu	nikl	k1gInSc2
<g/>
,	,	kIx,
strojů	stroj	k1gInPc2
a	a	k8xC
zařízení	zařízení	k1gNnSc4
k	k	k7c3
těžbě	těžba	k1gFnSc3
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
tyto	tento	k3xDgFnPc4
sankce	sankce	k1gFnPc4
ignorovali	ignorovat	k5eAaImAgMnP
a	a	k8xC
dále	daleko	k6eAd2
pokračovali	pokračovat	k5eAaImAgMnP
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
postupu	postup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1941	#num#	k4
Číňané	Číňan	k1gMnPc1
porazili	porazit	k5eAaPmAgMnP
japonská	japonský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Šang-žao	Šang-žao	k1gNnSc4
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
uzavřeli	uzavřít	k5eAaPmAgMnP
Japonsko	Japonsko	k1gNnSc4
a	a	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
dohodu	dohoda	k1gFnSc4
o	o	k7c6
neútočení	neútočení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
zvýšily	zvýšit	k5eAaPmAgInP
tlak	tlak	k1gInSc4
a	a	k8xC
uvalily	uvalit	k5eAaPmAgFnP
na	na	k7c4
Japonsko	Japonsko	k1gNnSc4
plíživé	plíživý	k2eAgNnSc4d1
ropné	ropný	k2eAgNnSc4d1
embargo	embargo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodávky	dodávka	k1gFnPc1
ropy	ropa	k1gFnSc2
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
postupně	postupně	k6eAd1
snižovány	snižován	k2eAgInPc4d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
byl	být	k5eAaImAgInS
okupován	okupován	k2eAgInSc1d1
Saigon	Saigon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
druhé	druhý	k4xOgFnSc3
bitvě	bitva	k1gFnSc3
o	o	k7c6
Čchang-ša	Čchang-ša	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
skončila	skončit	k5eAaPmAgFnS
vítězstvím	vítězství	k1gNnSc7
Číňanů	Číňan	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
oficiálnímu	oficiální	k2eAgNnSc3d1
schválení	schválení	k1gNnSc3
ropného	ropný	k2eAgNnSc2d1
embarga	embargo	k1gNnSc2
vládou	vláda	k1gFnSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Agresivní	agresivní	k2eAgInSc1d1
a	a	k8xC
bezohledný	bezohledný	k2eAgInSc1d1
postup	postup	k1gInSc1
nevedl	vést	k5eNaImAgInS
k	k	k7c3
porážce	porážka	k1gFnSc3
Kuomintangu	Kuomintang	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	s	k7c7
„	„	k?
<g/>
čínský	čínský	k2eAgInSc4d1
incident	incident	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
jak	jak	k8xS,k8xC
tuto	tento	k3xDgFnSc4
válku	válka	k1gFnSc4
nazývali	nazývat	k5eAaImAgMnP
Japonci	Japonec	k1gMnPc1
<g/>
)	)	kIx)
protáhl	protáhnout	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
podporovaly	podporovat	k5eAaImAgInP
generála	generál	k1gMnSc2
Čankajška	Čankajšek	k1gInSc2
a	a	k8xC
dodávali	dodávat	k5eAaImAgMnP
mu	on	k3xPp3gMnSc3
zbraně	zbraň	k1gFnPc4
a	a	k8xC
po	po	k7c6
vstupu	vstup	k1gInSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
do	do	k7c2
války	válka	k1gFnSc2
pak	pak	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
neokupované	okupovaný	k2eNgFnSc2d1
Číny	Čína	k1gFnSc2
postavili	postavit	k5eAaPmAgMnP
Američané	Američan	k1gMnPc1
několik	několik	k4yIc4
svých	svůj	k3xOyFgFnPc2
základen	základna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
„	„	k?
<g/>
čínský	čínský	k2eAgInSc1d1
incident	incident	k1gInSc1
<g/>
“	“	k?
pak	pak	k6eAd1
nutil	nutit	k5eAaImAgMnS
japonské	japonský	k2eAgNnSc4d1
velení	velení	k1gNnSc4
shromažďovat	shromažďovat	k5eAaImF
zde	zde	k6eAd1
stále	stále	k6eAd1
větší	veliký	k2eAgFnPc4d2
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
nemohli	moct	k5eNaImAgMnP
použít	použít	k5eAaPmF
ani	ani	k8xC
v	v	k7c6
Barmě	Barma	k1gFnSc6
<g/>
,	,	kIx,
ani	ani	k8xC
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vleklá	vleklý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
polykající	polykající	k2eAgInPc4d1
obrovské	obrovský	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
měla	mít	k5eAaImAgFnS
za	za	k7c4
následek	následek	k1gInSc4
také	také	k9
značné	značný	k2eAgFnPc4d1
hospodářské	hospodářský	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
na	na	k7c6
domácích	domácí	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JOWETT	JOWETT	kA
<g/>
,	,	kIx,
Philip	Philip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
1931	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1888	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Ospreye	Osprey	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Causa	causa	k1gFnSc1
Dohihara	Dohihara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Laser	laser	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85601	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PAINE	PAINE	kA
<g/>
,	,	kIx,
S.	S.	kA
C.	C.	kA
M.	M.	kA
The	The	k1gFnPc2
Wars	Warsa	k1gFnPc2
For	forum	k1gNnPc2
Asia	Asia	k1gFnSc1
1911	#num#	k4
<g/>
-	-	kIx~
<g/>
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
107	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2069	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ŠAJTAR	ŠAJTAR	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
„	„	k?
<g/>
čínského	čínský	k2eAgInSc2d1
incidentu	incident	k1gInSc2
<g/>
“	“	k?
ke	k	k7c3
druhé	druhý	k4xOgFnSc3
světové	světový	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenec	červenec	k1gInSc1
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
44	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
64	#num#	k4
<g/>
-	-	kIx~
<g/>
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VEJŘÍK	VEJŘÍK	kA
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzestup	vzestup	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
orlů	orel	k1gMnPc2
Nipponu	Nippon	k1gInSc2
1931-1941	1931-1941	k4
(	(	kIx(
<g/>
Prolog	prolog	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheb	Cheb	k1gInSc1
<g/>
:	:	kIx,
Svět	svět	k1gInSc1
křídel	křídlo	k1gNnPc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85280	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
GUSTAVSSON	GUSTAVSSON	kA
<g/>
,	,	kIx,
Hå	Hå	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sino-Japanese	Sino-Japanese	k1gFnSc1
Air	Air	k1gFnSc1
War	War	k1gFnSc1
1937-45	1937-45	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
surfcity	surfcit	k1gInPc1
<g/>
.	.	kIx.
<g/>
kund	kund	k1gInSc1
<g/>
.	.	kIx.
<g/>
dalnet	dalnet	k1gInSc1
<g/>
.	.	kIx.
<g/>
se	s	k7c7
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2003-03-16	2003-03-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
TULLY	TULLY	kA
<g/>
,	,	kIx,
Anthony	Anthon	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
RISING	RISING	kA
STORM	STORM	kA
-	-	kIx~
THE	THE	kA
IMPERIAL	IMPERIAL	kA
JAPANESE	JAPANESE	kA
NAVY	NAVY	k?
AND	Anda	k1gFnPc2
CHINA	China	k1gFnSc1
1931-1941	1931-1941	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Velké	velký	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
a	a	k8xC
střetnutí	střetnutí	k1gNnSc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Invaze	invaze	k1gFnSc2
do	do	k7c2
Polska	Polsko	k1gNnSc2
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
o	o	k7c4
Atlantik	Atlantik	k1gInSc4
•	•	k?
Zimní	zimní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Obsazení	obsazení	k1gNnSc1
Norska	Norsko	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Francii	Francie	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Británii	Británie	k1gFnSc6
•	•	k?
Balkánské	balkánský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
•	•	k?
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Africe	Afrika	k1gFnSc6
•	•	k?
Východní	východní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
•	•	k?
Pokračovací	pokračovací	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
•	•	k?
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
•	•	k?
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
•	•	k?
Italské	italský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
•	•	k?
Západní	západní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
•	•	k?
Sovětsko-japonská	sovětsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
128195	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4073002-5	4073002-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85122898	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85122898	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
