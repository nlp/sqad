<s>
Přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
nebo	nebo	k8xC	nebo
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
umožnit	umožnit	k5eAaPmF	umožnit
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
kotvení	kotvení	k1gNnSc4	kotvení
<g/>
,	,	kIx,	,
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
plavidly	plavidlo	k1gNnPc7	plavidlo
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc1	zajištění
snadné	snadný	k2eAgFnSc2d1	snadná
nakládky	nakládka	k1gFnSc2	nakládka
a	a	k8xC	a
vykládky	vykládka	k1gFnSc2	vykládka
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
naloďování	naloďování	k1gNnSc2	naloďování
a	a	k8xC	a
vyloďování	vyloďování	k1gNnSc2	vyloďování
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
dopravu	doprava	k1gFnSc4	doprava
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
stavbou	stavba	k1gFnSc7	stavba
přístavu	přístav	k1gInSc2	přístav
je	být	k5eAaImIp3nS	být
přístavní	přístavní	k2eAgNnSc1d1	přístavní
molo	molo	k1gNnSc1	molo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
umístění	umístění	k1gNnSc2	umístění
dělíme	dělit	k5eAaImIp1nP	dělit
přístavy	přístav	k1gInPc4	přístav
na	na	k7c6	na
říční	říční	k2eAgFnSc6d1	říční
a	a	k8xC	a
námořní	námořní	k2eAgFnSc6d1	námořní
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgInPc1d1	námořní
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
větších	veliký	k2eAgFnPc2d2	veliký
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgInPc1d1	říční
přístavy	přístav	k1gInPc1	přístav
přijímají	přijímat	k5eAaImIp3nP	přijímat
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
velikost	velikost	k1gFnSc1	velikost
i	i	k8xC	i
ponor	ponor	k1gInSc1	ponor
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgInP	omezit
ostatními	ostatní	k2eAgFnPc7d1	ostatní
stavbami	stavba	k1gFnPc7	stavba
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Přístavy	přístav	k1gInPc4	přístav
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jsou	být	k5eAaImIp3nP	být
přístavy	přístav	k1gInPc1	přístav
v	v	k7c6	v
Libni	Libeň	k1gFnSc6	Libeň
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
a	a	k8xC	a
v	v	k7c6	v
Radotíně	Radotína	k1gFnSc6	Radotína
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
mezi	mezi	k7c7	mezi
Mělníkem	Mělník	k1gInSc7	Mělník
a	a	k8xC	a
Hřenskem	Hřensko	k1gNnSc7	Hřensko
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
ČR	ČR	kA	ČR
provozuje	provozovat	k5eAaImIp3nS	provozovat
jen	jen	k9	jen
říční	říční	k2eAgFnSc1d1	říční
plavba	plavba	k1gFnSc1	plavba
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
význam	význam	k1gInSc4	význam
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1	námořní
základna	základna	k1gFnSc1	základna
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
Přístaviště	přístaviště	k1gNnSc2	přístaviště
Vývaziště	Vývaziště	k1gNnSc2	Vývaziště
Kotviště	kotviště	k1gNnSc2	kotviště
Marina	Marina	k1gFnSc1	Marina
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přístav	přístav	k1gInSc1	přístav
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přístav	přístav	k1gInSc1	přístav
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
