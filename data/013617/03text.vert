<s>
Skillet	Skillet	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Skillet	Skillet	k1gInSc1
Skillet	Skillet	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Původ	původ	k1gInSc1
</s>
<s>
Memphis	Memphis	k1gFnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
Žánry	žánr	k1gInPc7
</s>
<s>
Industrialní	Industrialný	k2eAgMnPc1d1
rock	rock	k1gInSc4
<g/>
<	<	kIx(
<g/>
br	br	k0
hard	harda	k1gFnPc2
rocknu	rocknout	k5eAaPmIp1nS
Metal	metal	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
1996	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc1
Web	web	k1gInSc1
</s>
<s>
www.skillet.com	www.skillet.com	k1gInSc4
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
John	John	k1gMnSc1
CooperKorey	CooperKorea	k1gFnSc2
CooperJennifer	CooperJennifer	k1gMnSc1
LedgerSeth	LedgerSeth	k1gMnSc1
Morrison	Morrison	k1gInSc4
Dřívější	dřívější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Ken	Ken	k?
SteortsTrey	SteortsTrea	k1gFnPc1
McClurkinKevin	McClurkinKevina	k1gFnPc2
HaalandLori	HaalandLor	k1gFnSc2
PetersBen	PetersBen	k1gInSc1
Kasica	Kasic	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skillet	Skillet	k1gInSc1
je	být	k5eAaImIp3nS
rocková	rockový	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1996	#num#	k4
v	v	k7c6
Tennessee	Tennessee	k1gFnSc6
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrají	hrát	k5eAaImIp3nP
různé	různý	k2eAgInPc4d1
žánry	žánr	k1gInPc4
–	–	k?
nejen	nejen	k6eAd1
rock	rock	k1gInSc4
<g/>
,	,	kIx,
alternativní	alternativní	k2eAgInSc4d1
rock	rock	k1gInSc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc4
muzika	muzika	k1gFnSc1
sklouzává	sklouzávat	k5eAaImIp3nS
k	k	k7c3
industrial	industriat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
rocku	rock	k1gInSc3
a	a	k8xC
nu	nu	k9
metalu	metal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kapela	kapela	k1gFnSc1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
nahrála	nahrát	k5eAaBmAgFnS,k5eAaPmAgFnS
9	#num#	k4
desek	deska	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
DVD	DVD	kA
a	a	k8xC
mnoho	mnoho	k4c1
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
písní	píseň	k1gFnPc2
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
v	v	k7c6
rádiích	rádio	k1gNnPc6
a	a	k8xC
ve	v	k7c6
filmech	film	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
muzika	muzika	k1gFnSc1
je	být	k5eAaImIp3nS
energická	energický	k2eAgFnSc1d1
s	s	k7c7
aktuálními	aktuální	k2eAgFnPc7d1
<g/>
,	,	kIx,
trefnými	trefný	k2eAgInPc7d1
texty	text	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlas	hlas	k1gInSc1
zpěváka	zpěvák	k1gMnSc2
Johna	John	k1gMnSc2
Coopera	Cooper	k1gMnSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
kapelu	kapela	k1gFnSc4
typický	typický	k2eAgInSc1d1
nejen	nejen	k6eAd1
naléhavostí	naléhavost	k1gFnSc7
<g/>
,	,	kIx,
živostí	živost	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
surovostí	surovost	k1gFnSc7
–	–	k?
zase	zase	k9
hlavně	hlavně	k9
jedné	jeden	k4xCgFnSc2
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
desky	deska	k1gFnPc4
Comatose	Comatosa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnPc4
dvě	dva	k4xCgNnPc4
alba	album	k1gNnPc4
nahráli	nahrát	k5eAaBmAgMnP,k5eAaPmAgMnP
pouze	pouze	k6eAd1
tři	tři	k4xCgMnPc1
(	(	kIx(
<g/>
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
basa	basa	k1gFnSc1
<g/>
,	,	kIx,
Trey	Trea	k1gFnPc1
McClurkin	McClurkin	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
Steorts	Steorts	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
v	v	k7c6
nynější	nynější	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
zůstal	zůstat	k5eAaPmAgMnS
jen	jen	k9
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
má	mít	k5eAaImIp3nS
kapela	kapela	k1gFnSc1
čtyři	čtyři	k4xCgMnPc4
členy	člen	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dnes	dnes	k6eAd1
byla	být	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
"	"	kIx"
<g/>
Skillet	Skillet	k1gInSc1
<g/>
"	"	kIx"
obdarována	obdarován	k2eAgFnSc1d1
dvěma	dva	k4xCgFnPc7
cenami	cena	k1gFnPc7
Grammy	Gramm	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
Nynější	nynější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
basová	basový	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
členem	člen	k1gInSc7
od	od	k7c2
r.	r.	kA
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Korey	Korea	k1gFnPc1
Cooper	Cooper	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
keyboard	keyboard	k1gInSc1
(	(	kIx(
<g/>
členem	člen	k1gInSc7
od	od	k7c2
r.	r.	kA
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jennifer	Jennifer	k1gInSc1
Ledger	Ledger	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
<g/>
,	,	kIx,
druhé	druhý	k4xOgInPc1
hlasy	hlas	k1gInPc1
(	(	kIx(
<g/>
členem	člen	k1gInSc7
od	od	k7c2
r.	r.	kA
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Seth	Seth	k1gMnSc1
Morrison	Morrison	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
členem	člen	k1gInSc7
od	od	k7c2
r.	r.	kA
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Ken	Ken	k?
Steorts	Steorts	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
–	–	k?
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trey	Trey	k1gInPc1
McClurkin	McClurkina	k1gFnPc2
–	–	k?
basová	basový	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
–	–	k?
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kevin	Kevin	k2eAgInSc1d1
Haaland	Haaland	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lori	Lori	k1gFnSc1
Peters	Petersa	k1gFnPc2
–	–	k?
bicí	bicí	k2eAgNnSc1d1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ben	Ben	k1gInSc1
Kasica	Kasic	k1gInSc2
–	–	k?
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
–	–	k?
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
Skillet	Skillet	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
Skillet	Skillet	k1gInSc1
navrhl	navrhnout	k5eAaPmAgMnS
zakládajícím	zakládající	k2eAgMnSc7d1
členům	člen	k1gMnPc3
jejich	jejich	k3xOp3gMnPc4
pastor	pastor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
předtím	předtím	k6eAd1
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
kapele	kapela	k1gFnSc6
Seraph	Seraph	k1gMnSc1
a	a	k8xC
Ken	Ken	k1gMnSc1
Steorts	Steorts	k1gInSc4
v	v	k7c6
kapele	kapela	k1gFnSc6
Urgent	urgent	k1gMnSc1
Cry	Cry	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trey	Trea	k1gFnSc2
McClurkin	McClurkina	k1gFnPc2
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
přidal	přidat	k5eAaPmAgInS
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
kapely	kapela	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
rozpadly	rozpadnout	k5eAaPmAgFnP
a	a	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
založit	založit	k5eAaPmF
spolu	spolu	k6eAd1
novou	nový	k2eAgFnSc4d1
kapelu	kapela	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
protože	protože	k8xS
každý	každý	k3xTgInSc4
z	z	k7c2
nich	on	k3xPp3gInPc2
pocházel	pocházet	k5eAaImAgInS
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
ten	ten	k3xDgInSc1
nápad	nápad	k1gInSc1
nazvat	nazvat	k5eAaBmF,k5eAaPmF
novou	nový	k2eAgFnSc4d1
kapelu	kapela	k1gFnSc4
Skillet	Skillet	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Skillet	Skillet	k1gInSc1
<g/>
"	"	kIx"
znamená	znamenat	k5eAaImIp3nS
v	v	k7c6
češtině	čeština	k1gFnSc6
"	"	kIx"
<g/>
pánvička	pánvička	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
přirovnal	přirovnat	k5eAaPmAgMnS
skupinu	skupina	k1gFnSc4
k	k	k7c3
omeletě	omeleta	k1gFnSc3
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Všechno	všechen	k3xTgNnSc4
jsme	být	k5eAaImIp1nP
hodili	hodit	k5eAaPmAgMnP,k5eAaImAgMnP
dohromady	dohromady	k6eAd1
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
bychom	by	kYmCp1nP
věděli	vědět	k5eAaImAgMnP
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
z	z	k7c2
toho	ten	k3xDgNnSc2
-bude	-bude	k6eAd1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Vývoj	vývoj	k1gInSc1
kapely	kapela	k1gFnSc2
</s>
<s>
1996-	1996-	k4
založena	založen	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
<g/>
,	,	kIx,
členy	člen	k1gInPc7
byli	být	k5eAaImAgMnP
Ken	Ken	k1gMnPc1
Steorts	Steortsa	k1gFnPc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
<g/>
,	,	kIx,
Trey	Trea	k1gFnPc1
McClurkin	McClurkin	k1gInSc1
<g/>
;	;	kIx,
vydáno	vydán	k2eAgNnSc1d1
album	album	k1gNnSc1
Skillet	Skillet	k1gInSc1
<g/>
;	;	kIx,
první	první	k4xOgNnPc4
turné	turné	k1gNnPc4
<g/>
;	;	kIx,
</s>
<s>
1998	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
druhé	druhý	k4xOgNnSc4
album	album	k1gNnSc4
<g/>
:	:	kIx,
Hey	Hey	k1gMnSc1
You	You	k1gMnSc1
<g/>
,	,	kIx,
I	i	k9
Love	lov	k1gInSc5
Your	Your	k1gMnSc1
Soul	Soul	k1gInSc1
<g/>
;	;	kIx,
kapela	kapela	k1gFnSc1
změnila	změnit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
styl	styl	k1gInSc4
z	z	k7c2
tvrdšího	tvrdý	k2eAgInSc2d2
rocku	rock	k1gInSc2
na	na	k7c6
jemnější	jemný	k2eAgFnSc6d2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
figurovaly	figurovat	k5eAaImAgFnP
hlavně	hlavně	k9
Johnovy	Johnův	k2eAgFnPc4d1
klávesy	klávesa	k1gFnPc4
<g/>
;	;	kIx,
John	John	k1gMnSc1
Cooper	Cooper	k1gMnSc1
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Korey	Korea	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
vzali	vzít	k5eAaPmAgMnP
a	a	k8xC
Korey	Korea	k1gFnSc2
se	se	k3xPyFc4
přidala	přidat	k5eAaPmAgFnS
ke	k	k7c3
kapele	kapela	k1gFnSc3
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
však	však	k9
nebyla	být	k5eNaImAgFnS
členkou	členka	k1gFnSc7
kapely	kapela	k1gFnSc2
až	až	k6eAd1
do	do	k7c2
nahrávání	nahrávání	k1gNnSc2
dalšího	další	k2eAgNnSc2d1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1999	#num#	k4
–	–	k?
nový	nový	k2eAgMnSc1d1
člen	člen	k1gMnSc1
<g/>
:	:	kIx,
Kevin	Kevin	k1gMnSc1
Haaland	Haalanda	k1gFnPc2
</s>
<s>
2000	#num#	k4
–	–	k?
Ken	Ken	k1gMnSc4
Steorts	Steorts	k1gInSc1
opustil	opustit	k5eAaPmAgInS
kapelu	kapela	k1gFnSc4
<g/>
;	;	kIx,
vydáno	vydat	k5eAaPmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
album	album	k1gNnSc4
<g/>
:	:	kIx,
Invincible	Invincible	k1gMnPc5
<g/>
;	;	kIx,
i	i	k8xC
kvůli	kvůli	k7c3
odchodu	odchod	k1gInSc3
Kena	Ken	k2eAgFnSc1d1
Steortse	Steortse	k1gFnSc1
je	být	k5eAaImIp3nS
kompletně	kompletně	k6eAd1
změněné	změněný	k2eAgInPc1d1
<g/>
,	,	kIx,
Skillet	Skillet	k1gInSc4
začali	začít	k5eAaPmAgMnP
používat	používat	k5eAaImF
industriální	industriální	k2eAgInPc4d1
zvuky	zvuk	k1gInPc4
a	a	k8xC
hudebně	hudebně	k6eAd1
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
kritik	kritika	k1gFnPc2
přirovnávalo	přirovnávat	k5eAaImAgNnS
k	k	k7c3
Marilyn	Marilyn	k1gFnSc3
Mansonovi	Manson	k1gMnSc6
nebo	nebo	k8xC
Nine	Nin	k1gInPc4
Inch	Incha	k1gFnPc2
Nails	Nailsa	k1gFnPc2
<g/>
;	;	kIx,
Trey	Trea	k1gMnSc2
McClurkin	McClurkin	k1gMnSc1
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
kapely	kapela	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
přibyla	přibýt	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
členka	členka	k1gFnSc1
<g/>
:	:	kIx,
Lori	Lori	k1gFnSc1
Peters	Peters	k1gInSc1
<g/>
;	;	kIx,
vydáno	vydán	k2eAgNnSc1d1
album	album	k1gNnSc1
Ardent	Ardent	k1gInSc1
Worship	Worship	k1gMnSc1
–	–	k?
sbírka	sbírka	k1gFnSc1
chval	chvála	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
pět	pět	k4xCc1
bylo	být	k5eAaImAgNnS
složeno	složit	k5eAaPmNgNnS
přímo	přímo	k6eAd1
kapelou	kapela	k1gFnSc7
Skillet	Skillet	k1gInSc4
</s>
<s>
2001	#num#	k4
–	–	k?
z	z	k7c2
kapely	kapela	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
Kevin	Kevin	k2eAgInSc4d1
Haaland	Haaland	k1gInSc4
<g/>
,	,	kIx,
kytaru	kytara	k1gFnSc4
po	po	k7c6
něm	on	k3xPp3gNnSc6
převzal	převzít	k5eAaPmAgInS
Ben	Ben	k1gInSc1
Kasica	Kasicum	k1gNnSc2
<g/>
;	;	kIx,
vydáno	vydán	k2eAgNnSc1d1
album	album	k1gNnSc1
Alien	Alina	k1gFnPc2
Youth	Youtha	k1gFnPc2
<g/>
;	;	kIx,
po	po	k7c6
zvukové	zvukový	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
album	album	k1gNnSc1
mnohem	mnohem	k6eAd1
agresivnější	agresivní	k2eAgMnSc1d2
a	a	k8xC
útočnější	útoční	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
původ	původ	k1gInSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
pohlíží	pohlížet	k5eAaImIp3nS
na	na	k7c4
Ježíše	Ježíš	k1gMnPc4
jako	jako	k8xC,k8xS
na	na	k7c4
cizince	cizinec	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
v	v	k7c6
písních	píseň	k1gFnPc6
Alien	Alien	k2eAgInSc4d1
Youth	Youth	k1gInSc4
a	a	k8xC
Earth	Earth	k1gInSc4
Invasion	Invasion	k1gInSc1
</s>
<s>
2002	#num#	k4
–	–	k?
vzniklo	vzniknout	k5eAaPmAgNnS
DVD	DVD	kA
Alien	Alien	k2eAgInSc4d1
Youth	Youth	k1gInSc4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
The	The	k1gFnSc1
Unplugged	Unplugged	k1gMnSc1
Invasion	Invasion	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
2003	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
album	album	k1gNnSc4
Collide	Collid	k1gInSc5
<g/>
,	,	kIx,
nominované	nominovaný	k2eAgInPc4d1
na	na	k7c4
Grammy	Gramm	k1gInPc4
<g/>
;	;	kIx,
třetí	třetí	k4xOgInSc1
posun	posun	k1gInSc1
kapely	kapela	k1gFnSc2
z	z	k7c2
hudebního	hudební	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
industriálních	industriální	k2eAgInPc2d1
zvuků	zvuk	k1gInPc2
přes	přes	k7c4
hard-rock	hard-rock	k1gInSc4
ke	k	k7c3
kombinaci	kombinace	k1gFnSc3
smyčcových	smyčcový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
a	a	k8xC
elektrické	elektrický	k2eAgFnSc2d1
kytary	kytara	k1gFnSc2
</s>
<s>
2006	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
album	album	k1gNnSc4
Comatose	Comatosa	k1gFnSc3
<g/>
;	;	kIx,
velice	velice	k6eAd1
úspěšné	úspěšný	k2eAgNnSc4d1
album	album	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
získalo	získat	k5eAaPmAgNnS
nominaci	nominace	k1gFnSc4
na	na	k7c4
Grammy	Gramma	k1gFnPc4
<g/>
,	,	kIx,
cenu	cena	k1gFnSc4
Dove	Dove	k1gNnSc2
Awards	Awardsa	k1gFnPc2
a	a	k8xC
další	další	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
</s>
<s>
2007	#num#	k4
–	–	k?
vydána	vydán	k2eAgFnSc1d1
speciální	speciální	k2eAgFnSc1d1
edice	edice	k1gFnSc1
alba	album	k1gNnSc2
Comatose	Comatosa	k1gFnSc3
CD	CD	kA
<g/>
/	/	kIx~
<g/>
DVD	DVD	kA
<g/>
,	,	kIx,
z	z	k7c2
kapely	kapela	k1gFnSc2
odešla	odejít	k5eAaPmAgFnS
Lori	Lori	k1gFnSc1
Peters	Petersa	k1gFnPc2
</s>
<s>
2008	#num#	k4
–	–	k?
nová	nový	k2eAgFnSc1d1
členka	členka	k1gFnSc1
<g/>
:	:	kIx,
Jen	jen	k9
Ledger	Ledger	k1gInSc1
</s>
<s>
2009	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
album	album	k1gNnSc4
Awake	Awak	k1gFnSc2
</s>
<s>
2011	#num#	k4
–	–	k?
Odešel	odejít	k5eAaPmAgMnS
kytarista	kytarista	k1gMnSc1
Ben	Ben	k1gInSc4
Kasica	Kasic	k1gInSc2
<g/>
,	,	kIx,
<g/>
nový	nový	k2eAgMnSc1d1
člen	člen	k1gMnSc1
<g/>
:	:	kIx,
Seth	Seth	k1gMnSc1
Morrison	Morrison	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
EP	EP	kA
s	s	k7c7
názvem	název	k1gInSc7
Awake	Awak	k1gInSc2
and	and	k?
Remixed	Remixed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
album	album	k1gNnSc4
Rise	Ris	k1gFnSc2
</s>
<s>
2016	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
album	album	k1gNnSc4
Unleashed	Unleashed	k1gMnSc1
</s>
<s>
2019	#num#	k4
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
album	album	k1gNnSc4
Victorious	Victorious	k1gMnSc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Alba	alba	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Skillet	Skillet	k1gInSc1
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Hey	Hey	k1gMnSc1
You	You	k1gMnSc1
<g/>
,	,	kIx,
I	i	k9
Love	lov	k1gInSc5
Your	Your	k1gInSc1
Soul	Soul	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
Invincible	Invincible	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
Ardent	Ardent	k1gMnSc1
Worship	Worship	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Alien	Alien	k2eAgInSc1d1
Youth	Youth	k1gInSc1
</s>
<s>
2003	#num#	k4
<g/>
:	:	kIx,
Collide	Collid	k1gInSc5
–	–	k?
nominace	nominace	k1gFnSc2
Best	Best	k2eAgInSc4d1
Rock	rock	k1gInSc4
Gospel	gospel	k1gInSc1
Album	album	k1gNnSc1
Grammy	Gramma	k1gFnSc2
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
Comatose	Comatosa	k1gFnSc6
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Awake	Awak	k1gInSc2
</s>
<s>
2013	#num#	k4
<g/>
:	:	kIx,
Rise	Ris	k1gInSc2
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Unleashed	Unleashed	k1gInSc1
</s>
<s>
2019	#num#	k4
<g/>
:	:	kIx,
Victorious	Victorious	k1gInSc1
</s>
<s>
Živá	živý	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
Comatose	Comatosa	k1gFnSc6
Comes	Comesa	k1gFnPc2
Alive	Aliev	k1gFnSc2
</s>
<s>
EP	EP	kA
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Older	Oldra	k1gFnPc2
I	i	k8xC
Get	Get	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
:	:	kIx,
Awake	Awak	k1gInSc2
and	and	k?
Remixed	Remixed	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Skillet	Skillet	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.skillet.com	http://www.skillet.com	k1gInSc1
</s>
<s>
http://skillet-fan-page.blog.cz/	http://skillet-fan-page.blog.cz/	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Skillet	Skillet	k1gInSc1
(	(	kIx(
<g/>
band	band	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
16050754-6	16050754-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0725	#num#	k4
6453	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2007036636	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146486152	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2007036636	#num#	k4
</s>
