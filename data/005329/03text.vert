<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
kysličník	kysličník	k1gInSc1	kysličník
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
;	;	kIx,	;
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
slabě	slabě	k6eAd1	slabě
nakyslou	nakyslý	k2eAgFnSc4d1	nakyslá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
také	také	k9	také
jako	jako	k8xS	jako
suchý	suchý	k2eAgInSc4d1	suchý
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
molekula	molekula	k1gFnSc1	molekula
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jedním	jeden	k4xCgInSc7	jeden
atomem	atom	k1gInSc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
atomy	atom	k1gInPc7	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
(	(	kIx(	(
<g/>
spalováním	spalování	k1gNnSc7	spalování
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
C	C	kA	C
+	+	kIx~	+
O2	O2	k1gMnSc1	O2
→	→	k?	→
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
hořením	hoření	k1gNnSc7	hoření
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
svítiplynu	svítiplyn	k1gInSc3	svítiplyn
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2	[number]	k4	2
CO	co	k8xS	co
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2	[number]	k4	2
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
methanu	methan	k1gInSc2	methan
<g/>
:	:	kIx,	:
CH4	CH4	k1gFnSc1	CH4
+	+	kIx~	+
2	[number]	k4	2
O2	O2	k1gMnSc1	O2
→	→	k?	→
CO2	CO2	k1gMnSc1	CO2
+	+	kIx~	+
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
za	za	k7c2	za
vývinu	vývin	k1gInSc2	vývin
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Podobnými	podobný	k2eAgFnPc7d1	podobná
reakcemi	reakce	k1gFnPc7	reakce
můžeme	moct	k5eAaImIp1nP	moct
popsat	popsat	k5eAaPmF	popsat
i	i	k9	i
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
a	a	k8xC	a
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
produktem	produkt	k1gInSc7	produkt
dýchání	dýchání	k1gNnSc2	dýchání
většiny	většina	k1gFnSc2	většina
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
konečným	konečný	k2eAgInSc7d1	konečný
produktem	produkt	k1gInSc7	produkt
metabolické	metabolický	k2eAgFnSc2d1	metabolická
přeměny	přeměna	k1gFnSc2	přeměna
živin	živina	k1gFnPc2	živina
obsažených	obsažený	k2eAgFnPc2d1	obsažená
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
uhličitanů	uhličitan	k1gInPc2	uhličitan
<g/>
,	,	kIx,	,
především	především	k9	především
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
kyselinami	kyselina	k1gFnPc7	kyselina
například	například	k6eAd1	například
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
:	:	kIx,	:
CaCO	CaCO	k1gFnPc2	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
2	[number]	k4	2
HCl	HCl	k1gMnSc1	HCl
→	→	k?	→
CO2	CO2	k1gMnSc1	CO2
+	+	kIx~	+
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
(	(	kIx(	(
<g/>
žíháním	žíhání	k1gNnPc3	žíhání
<g/>
)	)	kIx)	)
vápence	vápenec	k1gInSc2	vápenec
(	(	kIx(	(
<g/>
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
→	→	k?	→
CaO	CaO	k1gFnPc2	CaO
+	+	kIx~	+
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chemické	chemický	k2eAgFnSc6d1	chemická
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
velice	velice	k6eAd1	velice
stálý	stálý	k2eAgInSc1d1	stálý
a	a	k8xC	a
ani	ani	k8xC	ani
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
2	[number]	k4	2
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
znatelně	znatelně	k6eAd1	znatelně
nerozkládá	rozkládat	k5eNaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
uhličitou	uhličitý	k2eAgFnSc4d1	uhličitá
<g/>
:	:	kIx,	:
CO2	CO2	k1gFnSc4	CO2
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
↔	↔	k?	↔
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
hydroxidy	hydroxid	k1gInPc7	hydroxid
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
uhličitany	uhličitan	k1gInPc7	uhličitan
a	a	k8xC	a
hydrogenuhličitany	hydrogenuhličitan	k1gInPc7	hydrogenuhličitan
(	(	kIx(	(
<g/>
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
kyselé	kyselý	k2eAgInPc4d1	kyselý
uhličitany	uhličitan	k1gInPc4	uhličitan
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
sodným	sodný	k2eAgInSc7d1	sodný
vzniká	vznikat	k5eAaImIp3nS	vznikat
buď	buď	k8xC	buď
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
:	:	kIx,	:
CO2	CO2	k1gFnSc1	CO2
+	+	kIx~	+
NaOH	NaOH	k1gFnSc1	NaOH
→	→	k?	→
NaHCO	NaHCO	k1gFnSc1	NaHCO
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
hydroxidu	hydroxid	k1gInSc2	hydroxid
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
:	:	kIx,	:
CO2	CO2	k1gFnSc1	CO2
+	+	kIx~	+
2	[number]	k4	2
NaOH	NaOH	k1gFnPc4	NaOH
→	→	k?	→
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
V	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
rostlinách	rostlina	k1gFnPc6	rostlina
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
asimilován	asimilován	k2eAgInSc4d1	asimilován
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
za	za	k7c2	za
katalytického	katalytický	k2eAgNnSc2d1	katalytické
působení	působení	k1gNnSc2	působení
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
a	a	k8xC	a
dodávky	dodávka	k1gFnSc2	dodávka
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
světelných	světelný	k2eAgNnPc2d1	světelné
kvant	kvantum	k1gNnPc2	kvantum
na	na	k7c4	na
monosacharidy	monosacharid	k1gInPc4	monosacharid
podle	podle	k7c2	podle
celkové	celkový	k2eAgFnSc2d1	celková
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
6	[number]	k4	6
CO2	CO2	k1gFnPc2	CO2
+	+	kIx~	+
6	[number]	k4	6
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
C6H12O6	C6H12O6	k1gMnSc1	C6H12O6
+	+	kIx~	+
6	[number]	k4	6
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Spalování	spalování	k1gNnSc1	spalování
sacharidů	sacharid	k1gInPc2	sacharid
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
i	i	k9	i
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemickým	chemický	k2eAgInSc7d1	chemický
procesem	proces	k1gInSc7	proces
právě	právě	k6eAd1	právě
opačným	opačný	k2eAgInSc7d1	opačný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
kvašení	kvašení	k1gNnSc4	kvašení
cukrů	cukr	k1gInPc2	cukr
působením	působení	k1gNnSc7	působení
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
geologickém	geologický	k2eAgInSc6d1	geologický
vývoji	vývoj	k1gInSc6	vývoj
planetárních	planetární	k2eAgNnPc2d1	planetární
těles	těleso	k1gNnPc2	těleso
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
chemismu	chemismus	k1gInSc6	chemismus
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
rovnováha	rovnováha	k1gFnSc1	rovnováha
mezi	mezi	k7c7	mezi
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
křemičitým	křemičitý	k2eAgInSc7d1	křemičitý
v	v	k7c6	v
kompetici	kompetik	k1gMnPc1	kompetik
o	o	k7c4	o
vápník	vápník	k1gInSc4	vápník
podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
<g/>
:	:	kIx,	:
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
↔	↔	k?	↔
CO2	CO2	k1gMnSc1	CO2
+	+	kIx~	+
CaSiO	CaSiO	k1gMnSc1	CaSiO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
teplot	teplota	k1gFnPc2	teplota
panujících	panující	k2eAgFnPc2d1	panující
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
rovnováha	rovnováha	k1gFnSc1	rovnováha
posunuta	posunout	k5eAaPmNgFnS	posunout
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
v	v	k7c6	v
uhličitanových	uhličitanový	k2eAgFnPc6d1	uhličitanová
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
<g/>
-li	i	k?	-li
teplota	teplota	k1gFnSc1	teplota
zhruba	zhruba	k6eAd1	zhruba
nad	nad	k7c7	nad
350	[number]	k4	350
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
rovnováha	rovnováha	k1gFnSc1	rovnováha
se	se	k3xPyFc4	se
vychýlí	vychýlit	k5eAaPmIp3nS	vychýlit
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
vytěsní	vytěsnit	k5eAaPmIp3nS	vytěsnit
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
mechanismem	mechanismus	k1gInSc7	mechanismus
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
velmi	velmi	k6eAd1	velmi
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ochlazení	ochlazení	k1gNnSc6	ochlazení
pod	pod	k7c7	pod
-80	-80	k4	-80
°	°	k?	°
<g/>
C	C	kA	C
mění	měnit	k5eAaImIp3nS	měnit
plynný	plynný	k2eAgInSc1d1	plynný
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
svoje	svůj	k3xOyFgNnPc4	svůj
skupenství	skupenství	k1gNnPc4	skupenství
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
(	(	kIx(	(
<g/>
desublimuje	desublimovat	k5eAaBmIp3nS	desublimovat
<g/>
)	)	kIx)	)
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
bezbarvé	bezbarvý	k2eAgFnSc2d1	bezbarvá
tuhé	tuhý	k2eAgFnSc2d1	tuhá
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
suchý	suchý	k2eAgInSc4d1	suchý
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
nedýchatelný	dýchatelný	k2eNgInSc1d1	nedýchatelný
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
ztrátu	ztráta	k1gFnSc4	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Naruší	narušit	k5eAaPmIp3nS	narušit
totiž	totiž	k9	totiž
uhličitanovou	uhličitanový	k2eAgFnSc4d1	uhličitanová
rovnováhu	rovnováha	k1gFnSc4	rovnováha
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
tak	tak	k6eAd1	tak
acidosu	acidosa	k1gFnSc4	acidosa
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
ovšem	ovšem	k9	ovšem
pokles	pokles	k1gInSc4	pokles
pod	pod	k7c4	pod
jeho	jeho	k3xOp3gFnSc4	jeho
normální	normální	k2eAgFnSc4d1	normální
koncentraci	koncentrace	k1gFnSc4	koncentrace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
alkalosu	alkalosa	k1gFnSc4	alkalosa
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
0,040	[number]	k4	0,040
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
na	na	k7c6	na
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
relativní	relativní	k2eAgFnSc2d1	relativní
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zejména	zejména	k9	zejména
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
emisí	emise	k1gFnPc2	emise
jeho	jeho	k3xOp3gFnSc4	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
koncentrace	koncentrace	k1gFnSc1	koncentrace
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
odstavec	odstavec	k1gInSc1	odstavec
"	"	kIx"	"
<g/>
Vliv	vliv	k1gInSc1	vliv
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
na	na	k7c6	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
půdě	půda	k1gFnSc6	půda
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gNnSc2	on
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
<g/>
x	x	k?	x
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
<g/>
x	x	k?	x
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
jeho	jeho	k3xOp3gFnSc2	jeho
výronu	výron	k1gInSc3	výron
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
ve	v	k7c6	v
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivních	aktivní	k2eAgFnPc6d1	aktivní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
přírodních	přírodní	k2eAgFnPc6d1	přírodní
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
v	v	k7c6	v
takových	takový	k3xDgNnPc6	takový
místech	místo	k1gNnPc6	místo
hromadit	hromadit	k5eAaImF	hromadit
a	a	k8xC	a
představovat	představovat	k5eAaImF	představovat
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
past	past	k1gFnSc4	past
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
i	i	k8xC	i
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
tak	tak	k6eAd1	tak
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
aktivity	aktivita	k1gFnPc1	aktivita
dodávají	dodávat	k5eAaImIp3nP	dodávat
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
Země	zem	k1gFnSc2	zem
přibližně	přibližně	k6eAd1	přibližně
130	[number]	k4	130
až	až	k9	až
230	[number]	k4	230
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
řádově	řádově	k6eAd1	řádově
jen	jen	k9	jen
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
%	%	kIx~	%
produkce	produkce	k1gFnSc1	produkce
CO2	CO2	k1gFnSc2	CO2
lidstvem	lidstvo	k1gNnSc7	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
byl	být	k5eAaImAgInS	být
také	také	k9	také
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
atmosfér	atmosféra	k1gFnPc2	atmosféra
planet	planeta	k1gFnPc2	planeta
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Spektroskopicky	spektroskopicky	k6eAd1	spektroskopicky
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
i	i	k9	i
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
průmyslově	průmyslově	k6eAd1	průmyslově
lehce	lehko	k6eAd1	lehko
dostupný	dostupný	k2eAgInSc1d1	dostupný
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
<g/>
:	:	kIx,	:
chemická	chemický	k2eAgFnSc1d1	chemická
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
<g/>
:	:	kIx,	:
anorganických	anorganický	k2eAgInPc2d1	anorganický
uhličitanů	uhličitan	k1gInPc2	uhličitan
polykarbonátů	polykarbonát	k1gInPc2	polykarbonát
polyuretanů	polyuretan	k1gInPc2	polyuretan
karbamátů	karbamát	k1gInPc2	karbamát
isokyanátů	isokyanát	k1gInPc2	isokyanát
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
hnací	hnací	k2eAgInSc1d1	hnací
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
ochranná	ochranný	k2eAgFnSc1d1	ochranná
atmosféra	atmosféra	k1gFnSc1	atmosféra
pro	pro	k7c4	pro
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
konzervant	konzervant	k1gInSc1	konzervant
E290	E290	k1gFnSc2	E290
součást	součást	k1gFnSc4	součást
perlivých	perlivý	k2eAgInPc2d1	perlivý
<g/>
/	/	kIx~	/
<g/>
sycených	sycený	k2eAgInPc2d1	sycený
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
vnímání	vnímání	k1gNnSc1	vnímání
jejich	jejich	k3xOp3gFnSc2	jejich
chuti	chuť	k1gFnSc2	chuť
náplň	náplň	k1gFnSc4	náplň
sněhových	sněhový	k2eAgInPc2d1	sněhový
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
chladicí	chladicí	k2eAgNnSc1d1	chladicí
médium	médium	k1gNnSc1	médium
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
(	(	kIx(	(
<g/>
do	do	k7c2	do
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
efektivity	efektivita	k1gFnSc2	efektivita
dýchání	dýchání	k1gNnSc2	dýchání
hnojivo	hnojivo	k1gNnSc4	hnojivo
v	v	k7c6	v
akvaristice	akvaristika	k1gFnSc6	akvaristika
Oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
na	na	k7c6	na
skleníkovém	skleníkový	k2eAgInSc6d1	skleníkový
efektu	efekt	k1gInSc6	efekt
nižší	nízký	k2eAgInSc4d2	nižší
vliv	vliv	k1gInSc4	vliv
než	než	k8xS	než
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
podílí	podílet	k5eAaImIp3nP	podílet
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
opačné	opačný	k2eAgFnPc1d1	opačná
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
naopak	naopak	k6eAd1	naopak
nárůst	nárůst	k1gInSc4	nárůst
obsahu	obsah	k1gInSc2	obsah
CO2	CO2	k1gFnSc2	CO2
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
zejména	zejména	k9	zejména
nárůstem	nárůst	k1gInSc7	nárůst
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
že	že	k8xS	že
změny	změna	k1gFnPc1	změna
obsahu	obsah	k1gInSc2	obsah
CO2	CO2	k1gFnPc1	CO2
jsou	být	k5eAaImIp3nP	být
zpožděny	zpožděn	k2eAgInPc1d1	zpožděn
za	za	k7c7	za
změnami	změna	k1gFnPc7	změna
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
podporuje	podporovat	k5eAaImIp3nS	podporovat
bujení	bujení	k1gNnSc4	bujení
pozemské	pozemský	k2eAgFnSc2d1	pozemská
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
nárůstu	nárůst	k1gInSc6	nárůst
zelenání	zelenání	k1gNnSc2	zelenání
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
stereotypicky	stereotypicky	k6eAd1	stereotypicky
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
špatný	špatný	k2eAgInSc4d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
mapuje	mapovat	k5eAaImIp3nS	mapovat
výskyt	výskyt	k1gInSc1	výskyt
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
družice	družice	k1gFnPc4	družice
NASA	NASA	kA	NASA
(	(	kIx(	(
<g/>
Orbiting	Orbiting	k1gInSc1	Orbiting
Carbon	Carbon	k1gInSc1	Carbon
Observatory-	Observatory-	k1gFnSc1	Observatory-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Změřený	změřený	k2eAgInSc1d1	změřený
výskyt	výskyt	k1gInSc1	výskyt
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
se	s	k7c7	s
staršími	starý	k2eAgInPc7d2	starší
modely	model	k1gInPc7	model
šíření	šíření	k1gNnSc2	šíření
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozpuštěno	rozpustit	k5eAaPmNgNnS	rozpustit
ve	v	k7c6	v
světových	světový	k2eAgInPc6d1	světový
mořích	moře	k1gNnPc6	moře
a	a	k8xC	a
oceánech	oceán	k1gInPc6	oceán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tak	tak	k6eAd1	tak
regulují	regulovat	k5eAaImIp3nP	regulovat
jeho	jeho	k3xOp3gNnSc4	jeho
množství	množství	k1gNnSc4	množství
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
nárůst	nárůst	k1gInSc4	nárůst
globální	globální	k2eAgFnSc2d1	globální
teploty	teplota	k1gFnSc2	teplota
však	však	k9	však
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
CO2	CO2	k1gFnPc2	CO2
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
vazbou	vazba	k1gFnSc7	vazba
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostává	dostávat	k5eAaImIp3nS	dostávat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
další	další	k2eAgNnSc1d1	další
dodatečné	dodatečný	k2eAgNnSc1d1	dodatečné
množství	množství	k1gNnSc1	množství
tohoto	tento	k3xDgInSc2	tento
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
vázána	vázat	k5eAaImNgFnS	vázat
chemicky	chemicky	k6eAd1	chemicky
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
uhličitanových	uhličitanový	k2eAgInPc2d1	uhličitanový
a	a	k8xC	a
hydrogenuhličitanových	hydrogenuhličitanový	k2eAgInPc2d1	hydrogenuhličitanový
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsoben	k2eAgNnSc1d1	způsobeno
jeho	jeho	k3xOp3gFnSc4	jeho
reakci	reakce	k1gFnSc4	reakce
s	s	k7c7	s
vápenatými	vápenatý	k2eAgInPc7d1	vápenatý
minerály	minerál	k1gInPc7	minerál
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
←	←	k?	←
<g/>
→	→	k?	→
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
HCO3-	HCO3-	k1gFnSc2	HCO3-
Tato	tento	k3xDgFnSc1	tento
rovnováha	rovnováha	k1gFnSc1	rovnováha
se	se	k3xPyFc4	se
však	však	k9	však
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
teplotou	teplota	k1gFnSc7	teplota
posunuje	posunovat	k5eAaImIp3nS	posunovat
doleva	doleva	k6eAd1	doleva
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
tedy	tedy	k9	tedy
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
z	z	k7c2	z
ovzduší	ovzduší	k1gNnSc2	ovzduší
poutá	poutat	k5eAaImIp3nS	poutat
mořský	mořský	k2eAgInSc1d1	mořský
fytoplankton	fytoplankton	k1gInSc1	fytoplankton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
živiny	živina	k1gFnPc4	živina
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
optimální	optimální	k2eAgFnSc6d1	optimální
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
růstem	růst	k1gInSc7	růst
nad	nad	k7c4	nad
optimum	optimum	k1gNnSc4	optimum
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
nejvýkonnější	výkonný	k2eAgInSc1d3	nejvýkonnější
ekosystém	ekosystém	k1gInSc1	ekosystém
poutající	poutající	k2eAgInSc1d1	poutající
vzdušný	vzdušný	k2eAgInSc1d1	vzdušný
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
příliš	příliš	k6eAd1	příliš
narušen	narušen	k2eAgMnSc1d1	narušen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kromě	kromě	k7c2	kromě
spalování	spalování	k1gNnSc2	spalování
biomasy	biomasa	k1gFnSc2	biomasa
či	či	k8xC	či
bioplynu	bioplyn	k1gInSc2	bioplyn
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
také	také	k9	také
během	během	k7c2	během
kompostování	kompostování	k1gNnSc2	kompostování
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
část	část	k1gFnSc1	část
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
jako	jako	k8xC	jako
posklizňové	posklizňový	k2eAgInPc4d1	posklizňový
(	(	kIx(	(
<g/>
potěžební	potěžební	k2eAgInPc4d1	potěžební
<g/>
)	)	kIx)	)
zbytky	zbytek	k1gInPc4	zbytek
<g/>
,	,	kIx,	,
a	a	k8xC	a
kořenový	kořenový	k2eAgInSc4d1	kořenový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
anaerobní	anaerobní	k2eAgFnSc2d1	anaerobní
digesce	digesce	k1gFnSc2	digesce
a	a	k8xC	a
kompostování	kompostování	k1gNnSc2	kompostování
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
stabilizované	stabilizovaný	k2eAgNnSc4d1	stabilizované
organo-minerální	organoinerální	k2eAgNnSc4d1	organo-minerální
hnojivo	hnojivo	k1gNnSc4	hnojivo
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
humusových	humusový	k2eAgFnPc2d1	humusová
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
uhlíku	uhlík	k1gInSc2	uhlík
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
fixován	fixovat	k5eAaImNgInS	fixovat
v	v	k7c6	v
humusu	humus	k1gInSc6	humus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
půd	půda	k1gFnPc2	půda
(	(	kIx(	(
<g/>
vododržnost	vododržnost	k1gFnSc4	vododržnost
<g/>
,	,	kIx,	,
pufrační	pufrační	k2eAgFnSc4d1	pufrační
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
zlepšené	zlepšený	k2eAgFnPc1d1	zlepšená
vlastnosti	vlastnost	k1gFnPc1	vlastnost
půdy	půda	k1gFnSc2	půda
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vyšší	vysoký	k2eAgInPc1d2	vyšší
výnosy	výnos	k1gInPc1	výnos
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
intenzivnější	intenzivní	k2eAgFnSc4d2	intenzivnější
asimilaci	asimilace	k1gFnSc4	asimilace
CO2	CO2	k1gFnSc2	CO2
během	během	k7c2	během
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
chemickou	chemický	k2eAgFnSc7d1	chemická
sloučeninou	sloučenina	k1gFnSc7	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
jako	jako	k9	jako
plyn	plyn	k1gInSc1	plyn
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vlámský	vlámský	k2eAgMnSc1d1	vlámský
chemik	chemik	k1gMnSc1	chemik
Jan	Jan	k1gMnSc1	Jan
Baptist	Baptist	k1gMnSc1	Baptist
van	vana	k1gFnPc2	vana
Helmont	Helmont	k1gMnSc1	Helmont
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
nádobě	nádoba	k1gFnSc6	nádoba
je	být	k5eAaImIp3nS	být
váha	váha	k1gFnSc1	váha
zbylého	zbylý	k2eAgInSc2d1	zbylý
popele	popel	k1gInSc2	popel
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
původního	původní	k2eAgNnSc2d1	původní
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
to	ten	k3xDgNnSc4	ten
přeměnou	přeměna	k1gFnSc7	přeměna
části	část	k1gFnSc2	část
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c4	na
neviditelnou	viditelný	k2eNgFnSc4d1	neviditelná
substanci	substance	k1gFnSc4	substance
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazval	nazvat	k5eAaBmAgInS	nazvat
plyn	plyn	k1gInSc1	plyn
spiritus	spiritus	k1gInSc1	spiritus
sylvestre	sylvestr	k1gInSc5	sylvestr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
studoval	studovat	k5eAaImAgMnS	studovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
tohoto	tento	k3xDgInSc2	tento
plynu	plyn	k1gInSc2	plyn
podrobněji	podrobně	k6eAd2	podrobně
skotský	skotský	k2eAgMnSc1d1	skotský
lékař	lékař	k1gMnSc1	lékař
Joseph	Joseph	k1gMnSc1	Joseph
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahříváním	zahřívání	k1gNnSc7	zahřívání
vápence	vápenec	k1gInSc2	vápenec
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc2	jeho
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
vzniká	vznikat	k5eAaImIp3nS	vznikat
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
"	"	kIx"	"
<g/>
fixovatelný	fixovatelný	k2eAgInSc1d1	fixovatelný
vzduch	vzduch	k1gInSc1	vzduch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
fixed	fixed	k1gInSc1	fixed
air	air	k?	air
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vázat	vázat	k5eAaImF	vázat
silnými	silný	k2eAgFnPc7d1	silná
zásadami	zásada	k1gFnPc7	zásada
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
hydroxidem	hydroxid	k1gInSc7	hydroxid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
normálního	normální	k2eAgInSc2d1	normální
vzduchu	vzduch	k1gInSc2	vzduch
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
hoření	hoření	k2eAgFnSc1d1	hoření
a	a	k8xC	a
zvířata	zvíře	k1gNnPc4	zvíře
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Vázání	vázání	k1gNnSc1	vázání
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
použil	použít	k5eAaPmAgInS	použít
k	k	k7c3	k
důkazu	důkaz	k1gInSc3	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vydechovaném	vydechovaný	k2eAgInSc6d1	vydechovaný
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
také	také	k9	také
že	že	k8xS	že
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
kvašení	kvašení	k1gNnSc2	kvašení
(	(	kIx(	(
<g/>
fermentace	fermentace	k1gFnSc1	fermentace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
3	[number]	k4	3
%	%	kIx~	%
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
člověk	člověk	k1gMnSc1	člověk
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
5	[number]	k4	5
%	%	kIx~	%
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
závratě	závrať	k1gFnPc4	závrať
<g/>
,	,	kIx,	,
nouzi	nouze	k1gFnSc4	nouze
o	o	k7c4	o
dech	dech	k1gInSc4	dech
<g/>
,	,	kIx,	,
ospalost	ospalost	k1gFnSc4	ospalost
<g/>
.	.	kIx.	.
8-10	[number]	k4	8-10
%	%	kIx~	%
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
20	[number]	k4	20
%	%	kIx~	%
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
náhle	náhle	k6eAd1	náhle
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
nastává	nastávat	k5eAaImIp3nS	nastávat
do	do	k7c2	do
5-10	[number]	k4	5-10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
hrozí	hrozit	k5eAaImIp3nS	hrozit
například	například	k6eAd1	například
v	v	k7c6	v
silážních	silážní	k2eAgInPc6d1	silážní
či	či	k8xC	či
kanalizačních	kanalizační	k2eAgInPc6d1	kanalizační
prostorech	prostor	k1gInPc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Neftel	Neftel	k1gMnSc1	Neftel
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Friedli	Friedli	k1gFnSc4	Friedli
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Lotscher	Lotschra	k1gFnPc2	Lotschra
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Oeschger	Oeschger	k1gInSc1	Oeschger
<g/>
,	,	kIx,	,
U.	U.	kA	U.
Siegenthaler	Siegenthaler	k1gInSc1	Siegenthaler
<g/>
,	,	kIx,	,
and	and	k?	and
B.	B.	kA	B.
Stauffer	Stauffer	k1gInSc1	Stauffer
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Historical	Historicat	k5eAaPmAgMnS	Historicat
carbon	carbon	k1gMnSc1	carbon
dioxide	dioxid	k1gInSc5	dioxid
record	recordo	k1gNnPc2	recordo
from	fromo	k1gNnPc2	fromo
the	the	k?	the
Siple	Sipl	k1gInSc6	Sipl
Station	station	k1gInSc1	station
ice	ice	k?	ice
core	core	k1gInSc1	core
<g/>
.	.	kIx.	.
pp	pp	k?	pp
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
T.A.	T.A.	k1gMnSc1	T.A.
Boden	bodnout	k5eAaPmNgMnS	bodnout
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
P.	P.	kA	P.
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
J.	J.	kA	J.
Sepanski	Sepanski	k1gNnPc4	Sepanski
<g/>
,	,	kIx,	,
and	and	k?	and
F.	F.	kA	F.
<g/>
W.	W.	kA	W.
Stoss	Stossa	k1gFnPc2	Stossa
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Trends	Trends	k1gInSc1	Trends
<g/>
'	'	kIx"	'
<g/>
93	[number]	k4	93
<g/>
:	:	kIx,	:
A	a	k8xC	a
Compendium	Compendium	k1gNnSc4	Compendium
of	of	k?	of
Data	datum	k1gNnSc2	datum
on	on	k3xPp3gMnSc1	on
Global	globat	k5eAaImAgMnS	globat
Change	change	k1gFnSc2	change
<g/>
.	.	kIx.	.
</s>
<s>
ORNL	ORNL	kA	ORNL
<g/>
/	/	kIx~	/
<g/>
CDIAC-	CDIAC-	k1gMnSc1	CDIAC-
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
<s>
Carbon	Carbon	k1gMnSc1	Carbon
Dioxide	dioxid	k1gInSc5	dioxid
Information	Information	k1gInSc1	Information
Analysis	Analysis	k1gFnSc4	Analysis
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Oak	Oak	k1gFnPc2	Oak
Ridge	Ridg	k1gFnSc2	Ridg
National	National	k1gFnSc7	National
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
,	,	kIx,	,
Oak	Oak	k1gFnPc4	Oak
Ridge	Ridge	k1gFnPc2	Ridge
<g/>
,	,	kIx,	,
Tenn	Tenna	k1gFnPc2	Tenna
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
Keeling	Keeling	k1gInSc1	Keeling
<g/>
,	,	kIx,	,
C.D.	C.D.	k1gFnSc1	C.D.
<g/>
,	,	kIx,	,
and	and	k?	and
T.	T.	kA	T.
<g/>
P.	P.	kA	P.
Whorf	Whorf	k1gMnSc1	Whorf
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Atmospheric	Atmospheric	k1gMnSc1	Atmospheric
carbon	carbon	k1gMnSc1	carbon
dioxide	dioxid	k1gInSc5	dioxid
records	recordsit	k5eAaPmRp2nS	recordsit
from	from	k1gMnSc1	from
sites	sites	k1gMnSc1	sites
in	in	k?	in
the	the	k?	the
SIO	SIO	kA	SIO
air	air	k?	air
sampling	sampling	k1gInSc1	sampling
network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
pp	pp	k?	pp
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
T.A.	T.A.	k1gMnSc1	T.A.
Boden	bůst	k5eAaImNgMnS	bůst
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
P.	P.	kA	P.
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
J.	J.	kA	J.
Sepanski	Sepanski	k1gNnPc4	Sepanski
<g/>
,	,	kIx,	,
and	and	k?	and
F.	F.	kA	F.
<g/>
W.	W.	kA	W.
Stoss	Stossa	k1gFnPc2	Stossa
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Trends	Trends	k1gInSc1	Trends
<g/>
'	'	kIx"	'
<g/>
93	[number]	k4	93
<g/>
:	:	kIx,	:
A	a	k8xC	a
Compendium	Compendium	k1gNnSc4	Compendium
of	of	k?	of
Data	datum	k1gNnSc2	datum
on	on	k3xPp3gMnSc1	on
Global	globat	k5eAaImAgMnS	globat
Change	change	k1gFnSc2	change
<g/>
.	.	kIx.	.
</s>
<s>
ORNL	ORNL	kA	ORNL
<g/>
/	/	kIx~	/
<g/>
CDIAC-	CDIAC-	k1gMnSc1	CDIAC-
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
<s>
Carbon	Carbon	k1gMnSc1	Carbon
Dioxide	dioxid	k1gInSc5	dioxid
Information	Information	k1gInSc1	Information
Analysis	Analysis	k1gFnSc4	Analysis
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Oak	Oak	k1gFnPc2	Oak
Ridge	Ridg	k1gFnSc2	Ridg
National	National	k1gFnSc7	National
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
,	,	kIx,	,
Oak	Oak	k1gFnPc4	Oak
Ridge	Ridge	k1gFnPc2	Ridge
<g/>
,	,	kIx,	,
Tenn	Tenna	k1gFnPc2	Tenna
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
WALTER	Walter	k1gMnSc1	Walter
<g/>
,	,	kIx,	,
Carsten	Carsten	k2eAgMnSc1d1	Carsten
<g/>
,	,	kIx,	,
STUPAVSKÝ	STUPAVSKÝ	kA	STUPAVSKÝ
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
:	:	kIx,	:
Velký	velký	k2eAgMnSc1d1	velký
CO2	CO2	k1gMnSc1	CO2
podvod	podvod	k1gInSc1	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Biom	Biom	k1gInSc1	Biom
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2008-12-15	[number]	k4	2008-12-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
WWW	WWW	kA	WWW
<g/>
:	:	kIx,	:
<	<	kIx(	<
<g/>
http://biom.cz/cz/odborne-clanky/velky-co2-podvod	[url]	k1gInSc1	http://biom.cz/cz/odborne-clanky/velky-co2-podvod
<g/>
>	>	kIx)	>
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
<g/>
:	:	kIx,	:
1801	[number]	k4	1801
<g/>
–	–	k?	–
<g/>
2655	[number]	k4	2655
<g/>
.	.	kIx.	.
</s>
<s>
NÁTR	NÁTR	kA	NÁTR
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
jako	jako	k8xS	jako
skleník	skleník	k1gInSc1	skleník
:	:	kIx,	:
proč	proč	k6eAd1	proč
s	s	k7c7	s
bát	bát	k5eAaImF	bát
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
vydala	vydat	k5eAaPmAgFnS	vydat
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-1362-8	[number]	k4	80-200-1362-8
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Emise	emise	k1gFnSc2	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
absolutních	absolutní	k2eAgNnPc6d1	absolutní
číslech	číslo	k1gNnPc6	číslo
<g/>
,	,	kIx,	,
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
</s>
