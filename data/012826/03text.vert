<p>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
je	být	k5eAaImIp3nS	být
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělý	k2eAgMnPc2d1	dospělý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
respektuje	respektovat	k5eAaImIp3nS	respektovat
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
socializací	socializace	k1gFnSc7	socializace
<g/>
,	,	kIx,	,
personalizací	personalizace	k1gFnSc7	personalizace
<g/>
,	,	kIx,	,
akulturací	akulturace	k1gFnSc7	akulturace
a	a	k8xC	a
enkulturací	enkulturace	k1gFnSc7	enkulturace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
termínu	termín	k1gInSc2	termín
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
"	"	kIx"	"
<g/>
pedagogika	pedagogika	k1gFnSc1	pedagogika
<g/>
"	"	kIx"	"
obdobným	obdobný	k2eAgNnSc7d1	obdobné
pojmovým	pojmový	k2eAgNnSc7d1	pojmové
rozšířením	rozšíření	k1gNnSc7	rozšíření
pojmu	pojmout	k5eAaPmIp1nS	pojmout
anér	anér	k1gMnSc1	anér
<g/>
,	,	kIx,	,
andr-os	andrs	k1gMnSc1	andr-os
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
na	na	k7c6	na
dospělý	dospělý	k1gMnSc1	dospělý
člověk	člověk	k1gMnSc1	člověk
+	+	kIx~	+
agogé	agogé	k1gNnSc1	agogé
(	(	kIx(	(
<g/>
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
výchova	výchova	k1gFnSc1	výchova
z	z	k7c2	z
agó	agó	k?	agó
<g/>
;	;	kIx,	;
agein	agein	k1gMnSc1	agein
=	=	kIx~	=
vésti	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
říditi	řídit	k5eAaImF	řídit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
andragogika	andragogika	k1gFnSc1	andragogika
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
metody	metoda	k1gFnPc1	metoda
a	a	k8xC	a
techniky	technika	k1gFnPc1	technika
učení	učení	k1gNnSc2	učení
dětí	dítě	k1gFnPc2	dítě
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
aplikovat	aplikovat	k5eAaBmF	aplikovat
při	při	k7c6	při
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
andragogika	andragogika	k1gFnSc1	andragogika
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
pedagogika	pedagogika	k1gFnSc1	pedagogika
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
řeckého	řecký	k2eAgInSc2d1	řecký
významu	význam	k1gInSc2	význam
znamená	znamenat	k5eAaImIp3nS	znamenat
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
a	a	k8xC	a
výchova	výchova	k1gFnSc1	výchova
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
tedy	tedy	k9	tedy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
termín	termín	k1gInSc4	termín
andragogika	andragogika	k1gFnSc1	andragogika
použil	použít	k5eAaPmAgInS	použít
německý	německý	k2eAgMnSc1d1	německý
učitel	učitel	k1gMnSc1	učitel
Alexandr	Alexandr	k1gMnSc1	Alexandr
Kapp	Kapp	k1gMnSc1	Kapp
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Andragogika	andragogika	k1gFnSc1	andragogika
neboli	neboli	k8xC	neboli
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
Platónových	Platónův	k2eAgFnPc6d1	Platónova
a	a	k8xC	a
Aristotelových	Aristotelův	k2eAgFnPc6d1	Aristotelova
vzdělávacích	vzdělávací	k2eAgFnPc6d1	vzdělávací
teoriích	teorie	k1gFnPc6	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
však	však	k9	však
nepoužíval	používat	k5eNaImAgMnS	používat
a	a	k8xC	a
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
jej	on	k3xPp3gMnSc4	on
obnovil	obnovit	k5eAaPmAgInS	obnovit
Rosenstock-Huessy	Rosenstock-Huessa	k1gFnSc2	Rosenstock-Huessa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
andragogiku	andragogika	k1gFnSc4	andragogika
od	od	k7c2	od
pedagogiky	pedagogika	k1gFnSc2	pedagogika
a	a	k8xC	a
demagogiky	demagogika	k1gFnSc2	demagogika
a	a	k8xC	a
rozumí	rozumět	k5eAaImIp3nS	rozumět
jí	on	k3xPp3gFnSc3	on
všechny	všechen	k3xTgFnPc1	všechen
školské	školský	k2eAgFnPc1d1	školská
formy	forma	k1gFnPc1	forma
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
nechápe	chápat	k5eNaImIp3nS	chápat
však	však	k9	však
andragogiku	andragogika	k1gFnSc4	andragogika
jako	jako	k8xS	jako
vědní	vědní	k2eAgFnSc4d1	vědní
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
zaváděné	zaváděný	k2eAgFnPc1d1	zaváděná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepoužívané	používaný	k2eNgInPc4d1	nepoužívaný
pojmy	pojem	k1gInPc4	pojem
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
andragogie	andragogie	k1gFnSc1	andragogie
–	–	k?	–
používané	používaný	k2eAgInPc1d1	používaný
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
metodiky	metodika	k1gFnSc2	metodika
a	a	k8xC	a
ideologie	ideologie	k1gFnSc2	ideologie
andragogiky	andragogika	k1gFnSc2	andragogika
jako	jako	k8xS	jako
praktické	praktický	k2eAgFnSc2d1	praktická
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
Ten	ten	k3xDgInSc1	ten
Have	Have	k1gInSc1	Have
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
andragologie	andragologie	k1gFnSc1	andragologie
–	–	k?	–
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
andragogice	andragogika	k1gFnSc6	andragogika
<g/>
,	,	kIx,	,
výchově	výchova	k1gFnSc3	výchova
dospělých	dospělí	k1gMnPc2	dospělí
(	(	kIx(	(
<g/>
Ten	ten	k3xDgInSc1	ten
Have	Have	k1gInSc1	Have
<g/>
,	,	kIx,	,
Gottschalk	Gottschalk	k1gInSc1	Gottschalk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
antropogogika	antropogogika	k1gFnSc1	antropogogika
–	–	k?	–
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
zastřešující	zastřešující	k2eAgInSc4d1	zastřešující
pojem	pojem	k1gInSc4	pojem
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
jak	jak	k6eAd1	jak
andragogiku	andragogika	k1gFnSc4	andragogika
tak	tak	k8xS	tak
pedagogiku	pedagogika	k1gFnSc4	pedagogika
(	(	kIx(	(
<g/>
Medynský	Medynský	k2eAgInSc1d1	Medynský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hélikagogika	hélikagogika	k1gFnSc1	hélikagogika
–	–	k?	–
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
hélikia	hélikium	k1gNnSc2	hélikium
či	či	k8xC	či
hélikié	hélikié	k1gNnSc2	hélikié
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
nejplodnější	plodný	k2eAgNnSc4d3	nejplodnější
období	období	k1gNnSc4	období
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
enélikagogika	enélikagogika	k1gFnSc1	enélikagogika
–	–	k?	–
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
enélikos	enélikos	k1gMnSc1	enélikos
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
označuje	označovat	k5eAaImIp3nS	označovat
dospělého	dospělý	k1gMnSc4	dospělý
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
mužského	mužský	k2eAgInSc2d1	mužský
<g/>
,	,	kIx,	,
ženského	ženský	k2eAgInSc2d1	ženský
i	i	k8xC	i
středního	střední	k2eAgInSc2d1	střední
rodu	rod	k1gInSc2	rod
</s>
</p>
<p>
<s>
megalagogika	megalagogika	k1gFnSc1	megalagogika
nebo	nebo	k8xC	nebo
megagogika	megagogika	k1gFnSc1	megagogika
–	–	k?	–
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
megalos	megalos	k1gMnSc1	megalos
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
významu	význam	k1gInSc3	význam
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
dospělý	dospělý	k2eAgInSc1d1	dospělý
či	či	k8xC	či
zralý	zralý	k2eAgInSc1d1	zralý
</s>
</p>
<p>
<s>
==	==	k?	==
Charakter	charakter	k1gInSc1	charakter
==	==	k?	==
</s>
</p>
<p>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
mezi	mezi	k7c7	mezi
interdisciplinaritou	interdisciplinarita	k1gFnSc7	interdisciplinarita
a	a	k8xC	a
transdisciplinaritou	transdisciplinarita	k1gFnSc7	transdisciplinarita
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
aplikovanou	aplikovaný	k2eAgFnSc7d1	aplikovaná
vědou	věda	k1gFnSc7	věda
–	–	k?	–
její	její	k3xOp3gInPc4	její
poznatky	poznatek	k1gInPc4	poznatek
slouží	sloužit	k5eAaImIp3nS	sloužit
praktickým	praktický	k2eAgNnSc7d1	praktické
opatřením	opatření	k1gNnSc7	opatření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
společenských	společenský	k2eAgInPc2d1	společenský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozvíjeny	rozvíjet	k5eAaImNgFnP	rozvíjet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
formulovaných	formulovaný	k2eAgFnPc2d1	formulovaná
společenských	společenský	k2eAgFnPc2d1	společenská
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
vědou	věda	k1gFnSc7	věda
induktivní	induktivní	k2eAgFnSc7d1	induktivní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
předmět	předmět	k1gInSc1	předmět
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
identifikovaných	identifikovaný	k2eAgInPc2d1	identifikovaný
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
objevují	objevovat	k5eAaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
i	i	k9	i
vědou	věda	k1gFnSc7	věda
normativní	normativní	k2eAgFnSc7d1	normativní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
vlivy	vliv	k1gInPc7	vliv
sociotechnických	sociotechnický	k2eAgInPc2d1	sociotechnický
přístupů	přístup	k1gInPc2	přístup
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
formulují	formulovat	k5eAaImIp3nP	formulovat
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
techniky	technika	k1gFnPc4	technika
jejich	jejich	k3xOp3gNnSc4	jejich
dosažení	dosažení	k1gNnSc4	dosažení
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
stávají	stávat	k5eAaImIp3nP	stávat
normy	norma	k1gFnPc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
andragogika	andragogika	k1gFnSc1	andragogika
problémově	problémově	k6eAd1	problémově
a	a	k8xC	a
systematicky	systematicky	k6eAd1	systematicky
orientována	orientovat	k5eAaBmNgFnS	orientovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jí	on	k3xPp3gFnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vidět	vidět	k5eAaImF	vidět
jeden	jeden	k4xCgInSc1	jeden
problém	problém	k1gInSc1	problém
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgInPc2d1	různý
úhlů	úhel	k1gInPc2	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předvědecké	předvědecký	k2eAgNnSc1d1	předvědecké
stádium	stádium	k1gNnSc1	stádium
konstituování	konstituování	k1gNnSc2	konstituování
andragogiky	andragogika	k1gFnSc2	andragogika
===	===	k?	===
</s>
</p>
<p>
<s>
období	období	k1gNnSc1	období
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
/	/	kIx~	/
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
prvních	první	k4xOgInPc6	první
znacích	znak	k1gInPc6	znak
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
postrádá	postrádat	k5eAaImIp3nS	postrádat
vlastní	vlastní	k2eAgInSc4d1	vlastní
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
metodologii	metodologie	k1gFnSc4	metodologie
a	a	k8xC	a
pojmoslovný	pojmoslovný	k2eAgInSc4d1	pojmoslovný
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
první	první	k4xOgFnPc4	první
andragogické	andragogický	k2eAgFnPc4d1	andragogická
myšlenky	myšlenka	k1gFnPc4	myšlenka
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
návrhů	návrh	k1gInPc2	návrh
či	či	k8xC	či
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
nalézt	nalézt	k5eAaPmF	nalézt
u	u	k7c2	u
filozofů	filozof	k1gMnPc2	filozof
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
Konfucia	Konfucium	k1gNnSc2	Konfucium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Deskriptivně	deskriptivně	k6eAd1	deskriptivně
deduktivní	deduktivní	k2eAgNnSc1d1	deduktivní
stádium	stádium	k1gNnSc1	stádium
konstituování	konstituování	k1gNnSc2	konstituování
andragogiky	andragogika	k1gFnSc2	andragogika
===	===	k?	===
</s>
</p>
<p>
<s>
období	období	k1gNnSc1	období
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
/	/	kIx~	/
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
I.	I.	kA	I.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
systematické	systematický	k2eAgNnSc4d1	systematické
popisování	popisování	k1gNnSc4	popisování
projevů	projev	k1gInPc2	projev
znamenajících	znamenající	k2eAgInPc2d1	znamenající
souvislost	souvislost	k1gFnSc4	souvislost
se	s	k7c7	s
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
o	o	k7c4	o
první	první	k4xOgInPc4	první
výklady	výklad	k1gInPc4	výklad
takto	takto	k6eAd1	takto
fixovaných	fixovaný	k2eAgInPc2d1	fixovaný
projevů	projev	k1gInPc2	projev
vyplývajících	vyplývající	k2eAgInPc2d1	vyplývající
z	z	k7c2	z
podoby	podoba	k1gFnSc2	podoba
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
zde	zde	k6eAd1	zde
řazeni	řazen	k2eAgMnPc1d1	řazen
i	i	k9	i
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
nespadají	spadat	k5eNaImIp3nP	spadat
časově	časově	k6eAd1	časově
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
úroveň	úroveň	k1gFnSc4	úroveň
rozvinutosti	rozvinutost	k1gFnSc2	rozvinutost
svých	svůj	k3xOyFgInPc2	svůj
konceptů	koncept	k1gInPc2	koncept
a	a	k8xC	a
andragogických	andragogický	k2eAgFnPc2d1	andragogická
myšlenek	myšlenka	k1gFnPc2	myšlenka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Erasmus	Erasmus	k1gMnSc1	Erasmus
Rotterdamský	rotterdamský	k2eAgMnSc1d1	rotterdamský
či	či	k8xC	či
Jan	Jan	k1gMnSc1	Jan
Ámos	Ámos	k1gMnSc1	Ámos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stadium	stadium	k1gNnSc1	stadium
relativně	relativně	k6eAd1	relativně
samostatné	samostatný	k2eAgFnPc4d1	samostatná
vědní	vědní	k2eAgFnPc4d1	vědní
disciplíny	disciplína	k1gFnPc4	disciplína
===	===	k?	===
</s>
</p>
<p>
<s>
období	období	k1gNnSc1	období
<g/>
:	:	kIx,	:
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
andragogika	andragogika	k1gFnSc1	andragogika
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
jako	jako	k8xS	jako
pole	pole	k1gNnSc2	pole
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
jako	jako	k9	jako
studijní	studijní	k2eAgInSc4d1	studijní
obor	obor	k1gInSc4	obor
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vědních	vědní	k2eAgFnPc2d1	vědní
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
Adult	Adult	k2eAgInSc1d1	Adult
Education	Education	k1gInSc1	Education
učit	učit	k5eAaImF	učit
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
znovuoživení	znovuoživení	k1gNnSc6	znovuoživení
termínu	termín	k1gInSc2	termín
andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Zařadit	zařadit	k5eAaPmF	zařadit
sem	sem	k6eAd1	sem
můžeme	moct	k5eAaImIp1nP	moct
autory	autor	k1gMnPc4	autor
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Rosenstock-Huessy	Rosenstock-Huessa	k1gFnSc2	Rosenstock-Huessa
nebo	nebo	k8xC	nebo
Ten	ten	k3xDgInSc1	ten
Have	Have	k1gInSc1	Have
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stadium	stadium	k1gNnSc1	stadium
relativně	relativně	k6eAd1	relativně
rozvinuté	rozvinutý	k2eAgFnPc4d1	rozvinutá
vědní	vědní	k2eAgFnPc4d1	vědní
disciplíny	disciplína	k1gFnPc4	disciplína
===	===	k?	===
</s>
</p>
<p>
<s>
období	období	k1gNnSc1	období
<g/>
:	:	kIx,	:
II	II	kA	II
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
–	–	k?	–
současnost	současnost	k1gFnSc4	současnost
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
postupné	postupný	k2eAgNnSc4d1	postupné
ujasňování	ujasňování	k1gNnSc4	ujasňování
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
kurikulárního	kurikulární	k2eAgNnSc2d1	kurikulární
jádra	jádro	k1gNnSc2	jádro
andragogiky	andragogika	k1gFnSc2	andragogika
v	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
i	i	k8xC	i
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Těžištěm	těžiště	k1gNnSc7	těžiště
tohoto	tento	k3xDgNnSc2	tento
stádia	stádium	k1gNnSc2	stádium
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
legitimizační	legitimizační	k2eAgFnPc1d1	legitimizační
snahy	snaha	k1gFnPc1	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Hanselmann	Hanselmann	k1gMnSc1	Hanselmann
<g/>
,	,	kIx,	,
Pöggeler	Pöggeler	k1gMnSc1	Pöggeler
<g/>
,	,	kIx,	,
Savičevič	Savičevič	k1gMnSc1	Savičevič
<g/>
,	,	kIx,	,
Turose	Turosa	k1gFnSc6	Turosa
<g/>
,	,	kIx,	,
Knowles	Knowles	k1gInSc1	Knowles
<g/>
,	,	kIx,	,
Jarvis	Jarvis	k1gInSc1	Jarvis
<g/>
,	,	kIx,	,
Jochmann	Jochmann	k1gMnSc1	Jochmann
<g/>
,	,	kIx,	,
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Kotásek	Kotásek	k1gMnSc1	Kotásek
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
Livečka	Livečka	k1gFnSc1	Livečka
<g/>
,	,	kIx,	,
Kubálek	Kubálek	k1gMnSc1	Kubálek
<g/>
,	,	kIx,	,
Kulich	Kulich	k1gMnSc1	Kulich
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Kalnický	Kalnický	k2eAgMnSc1d1	Kalnický
<g/>
,	,	kIx,	,
Palán	Palán	k1gMnSc1	Palán
<g/>
,	,	kIx,	,
Mužík	Mužík	k1gMnSc1	Mužík
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předmět	předmět	k1gInSc1	předmět
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
nemá	mít	k5eNaImIp3nS	mít
andragogika	andragogika	k1gFnSc1	andragogika
vlastní	vlastní	k2eAgFnSc1d1	vlastní
předmět	předmět	k1gInSc1	předmět
<g/>
/	/	kIx~	/
<g/>
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
předmět	předmět	k1gInSc4	předmět
má	mít	k5eAaImIp3nS	mít
společný	společný	k2eAgInSc1d1	společný
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
společenskými	společenský	k2eAgFnPc7d1	společenská
vědami	věda	k1gFnPc7	věda
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
učícího	učící	k2eAgMnSc2d1	učící
se	se	k3xPyFc4	se
dospělého	dospělý	k2eAgInSc2d1	dospělý
–	–	k?	–
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
však	však	k9	však
jiný	jiný	k2eAgInSc4d1	jiný
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
nemá	mít	k5eNaImIp3nS	mít
jednotné	jednotný	k2eAgNnSc4d1	jednotné
vymezení	vymezení	k1gNnSc4	vymezení
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
vymezení	vymezení	k1gNnPc1	vymezení
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc1d1	různé
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Celoživotní	celoživotní	k2eAgNnSc1d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
a	a	k8xC	a
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
dospělých	dospělý	k2eAgMnPc2d1	dospělý
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
užší	úzký	k2eAgNnSc4d2	užší
pojetí	pojetí	k1gNnSc4	pojetí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
např.	např.	kA	např.
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
předmět	předmět	k1gInSc1	předmět
andragogiky	andragogika	k1gFnSc2	andragogika
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
učení	učení	k1gNnSc4	učení
se	se	k3xPyFc4	se
dospělých	dospělý	k2eAgInPc2d1	dospělý
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
šíři	šíř	k1gFnSc6	šíř
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
intencionální	intencionální	k2eAgNnSc1d1	intencionální
a	a	k8xC	a
incidentní	incidentní	k2eAgNnSc1d1	incidentní
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
dalších	další	k2eAgFnPc2d1	další
kategorií	kategorie	k1gFnPc2	kategorie
na	na	k7c4	na
organizované	organizovaný	k2eAgInPc4d1	organizovaný
někým	někdo	k3yInSc7	někdo
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
organizované	organizovaný	k2eAgFnSc3d1	organizovaná
dospělým	dospělý	k2eAgMnSc7d1	dospělý
samým	samý	k3xTgMnSc7	samý
<g/>
,	,	kIx,	,
plánované	plánovaný	k2eAgNnSc1d1	plánované
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
incidentní	incidentní	k2eAgInPc1d1	incidentní
<g/>
,	,	kIx,	,
incidentní	incidentní	k2eAgInPc1d1	incidentní
během	během	k7c2	během
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
incidentní	incidentní	k2eAgFnSc1d1	incidentní
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
životní	životní	k2eAgFnSc2d1	životní
rutiny	rutina	k1gFnSc2	rutina
<g/>
.	.	kIx.	.
<g/>
Vyrovnávání	vyrovnávání	k1gNnSc1	vyrovnávání
se	se	k3xPyFc4	se
dospělého	dospělý	k1gMnSc2	dospělý
člověka	člověk	k1gMnSc2	člověk
se	s	k7c7	s
sociálními	sociální	k2eAgFnPc7d1	sociální
institucemi	instituce	k1gFnPc7	instituce
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
se	s	k7c7	s
vznikajícími	vznikající	k2eAgFnPc7d1	vznikající
institucemi	instituce	k1gFnPc7	instituce
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
institucionalizace	institucionalizace	k1gFnSc2	institucionalizace
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
vytváření	vytváření	k1gNnSc1	vytváření
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
s	s	k7c7	s
institucí	instituce	k1gFnSc7	instituce
již	již	k6eAd1	již
existující	existující	k2eAgMnSc1d1	existující
(	(	kIx(	(
<g/>
jedinec	jedinec	k1gMnSc1	jedinec
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
pravidla	pravidlo	k1gNnPc1	pravidlo
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mobilizace	mobilizace	k1gFnSc1	mobilizace
lidského	lidský	k2eAgInSc2d1	lidský
kapitálu	kapitál	k1gInSc2	kapitál
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
sociální	sociální	k2eAgFnSc2d1	sociální
změny	změna	k1gFnSc2	změna
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejobecnější	obecní	k2eAgNnSc4d3	obecní
pojetí	pojetí	k1gNnSc4	pojetí
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
postindustriální	postindustriální	k2eAgFnSc4d1	postindustriální
společnost	společnost	k1gFnSc4	společnost
charakterizující	charakterizující	k2eAgFnSc2d1	charakterizující
neustálé	neustálý	k2eAgFnSc2d1	neustálá
změny	změna	k1gFnSc2	změna
lidský	lidský	k2eAgInSc1d1	lidský
<g />
.	.	kIx.	.
</s>
<s>
kapitál	kapitál	k1gInSc1	kapitál
představuje	představovat	k5eAaImIp3nS	představovat
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
<g/>
,	,	kIx,	,
nejcitlivější	citlivý	k2eAgFnSc4d3	nejcitlivější
a	a	k8xC	a
nejzranitelnější	zranitelný	k2eAgFnSc4d3	nejzranitelnější
determinantu	determinanta	k1gFnSc4	determinanta
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
<g/>
Orientace	orientace	k1gFnSc1	orientace
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
kritických	kritický	k2eAgInPc6d1	kritický
uzlech	uzel	k1gInPc6	uzel
jeho	jeho	k3xOp3gFnSc2	jeho
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
či	či	k8xC	či
při	při	k7c6	při
problémovém	problémový	k2eAgInSc6d1	problémový
průběhu	průběh	k1gInSc6	průběh
jeho	jeho	k3xOp3gFnSc2	jeho
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
již	již	k6eAd1	již
v	v	k7c6	v
konceptu	koncept	k1gInSc6	koncept
předchozím	předchozí	k2eAgInSc6d1	předchozí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
individuum	individuum	k1gNnSc4	individuum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jako	jako	k9	jako
opěrnou	opěrný	k2eAgFnSc4d1	opěrná
vědu	věda	k1gFnSc4	věda
využívá	využívat	k5eAaPmIp3nS	využívat
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
<g/>
Individuální	individuální	k2eAgFnPc4d1	individuální
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
souvislosti	souvislost	k1gFnPc4	souvislost
změn	změna	k1gFnPc2	změna
syntetického	syntetický	k2eAgInSc2d1	syntetický
statusu	status	k1gInSc2	status
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sociologizující	sociologizující	k2eAgNnSc4d1	sociologizující
a	a	k8xC	a
nenormativní	normativní	k2eNgNnSc4d1	nenormativní
chápání	chápání	k1gNnSc4	chápání
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
;	;	kIx,	;
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
změna	změna	k1gFnSc1	změna
jedné	jeden	k4xCgFnSc2	jeden
složky	složka	k1gFnSc2	složka
syntetického	syntetický	k2eAgInSc2d1	syntetický
statusu	status	k1gInSc2	status
se	se	k3xPyFc4	se
nutně	nutně	k6eAd1	nutně
promítne	promítnout	k5eAaPmIp3nS	promítnout
do	do	k7c2	do
složek	složka	k1gFnPc2	složka
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
,	,	kIx,	,
integrální	integrální	k2eAgFnSc1d1	integrální
andragogika	andragogika	k1gFnSc1	andragogika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
odraz	odraz	k1gInSc4	odraz
<g />
.	.	kIx.	.
</s>
<s>
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
dimenzích	dimenze	k1gFnPc6	dimenze
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
minimalizování	minimalizování	k1gNnSc3	minimalizování
negativních	negativní	k2eAgInPc2d1	negativní
důsledků	důsledek	k1gInPc2	důsledek
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
<g/>
Animace	animace	k1gFnSc1	animace
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejširší	široký	k2eAgNnSc4d3	nejširší
vymezení	vymezení	k1gNnSc4	vymezení
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
části	část	k1gFnSc2	část
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ostatních	ostatní	k2eAgNnPc6d1	ostatní
vymezeních	vymezení	k1gNnPc6	vymezení
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nekončící	končící	k2eNgFnSc4d1	nekončící
humanizaci	humanizace	k1gFnSc4	humanizace
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
záměrná	záměrný	k2eAgFnSc1d1	záměrná
i	i	k8xC	i
nezáměrná	záměrný	k2eNgFnSc1d1	nezáměrná
(	(	kIx(	(
<g/>
formování	formování	k1gNnPc2	formování
vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vztažena	vztáhnout	k5eAaPmNgFnS	vztáhnout
k	k	k7c3	k
následujícím	následující	k2eAgInPc3d1	následující
procesům	proces	k1gInPc3	proces
<g/>
:	:	kIx,	:
enkulturaci	enkulturace	k1gFnSc6	enkulturace
<g/>
,	,	kIx,	,
socializaci	socializace	k1gFnSc6	socializace
a	a	k8xC	a
resocializaci	resocializace	k1gFnSc6	resocializace
<g/>
,	,	kIx,	,
profesionalizaci	profesionalizace	k1gFnSc6	profesionalizace
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
edukaci	edukace	k1gFnSc4	edukace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objekt	objekt	k1gInSc1	objekt
==	==	k?	==
</s>
</p>
<p>
<s>
Objektem	objekt	k1gInSc7	objekt
andragogiky	andragogika	k1gFnSc2	andragogika
je	být	k5eAaImIp3nS	být
dospělý	dospělý	k1gMnSc1	dospělý
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
biologicky	biologicky	k6eAd1	biologicky
<g/>
,	,	kIx,	,
psychicky	psychicky	k6eAd1	psychicky
<g/>
,	,	kIx,	,
sociálně	sociálně	k6eAd1	sociálně
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
zralý	zralý	k2eAgMnSc1d1	zralý
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
)	)	kIx)	)
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Životní	životní	k2eAgFnSc4d1	životní
dráhu	dráha	k1gFnSc4	dráha
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čtyř	čtyři	k4xCgFnPc2	čtyři
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
biografický	biografický	k2eAgInSc4d1	biografický
(	(	kIx(	(
<g/>
potenciál	potenciál	k1gInSc4	potenciál
délky	délka	k1gFnSc2	délka
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
historický	historický	k2eAgInSc1d1	historický
(	(	kIx(	(
<g/>
procházení	procházení	k1gNnSc1	procházení
časovými	časový	k2eAgInPc7d1	časový
cykly	cyklus	k1gInPc7	cyklus
a	a	k8xC	a
společenskými	společenský	k2eAgFnPc7d1	společenská
změnami	změna	k1gFnPc7	změna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Horizontální	horizontální	k2eAgFnSc1d1	horizontální
dimenze	dimenze	k1gFnSc1	dimenze
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
sociálně	sociálně	k6eAd1	sociálně
historický	historický	k2eAgInSc4d1	historický
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
jedinec	jedinec	k1gMnSc1	jedinec
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
který	který	k3yIgMnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
syntetickým	syntetický	k2eAgInSc7d1	syntetický
statusem	status	k1gInSc7	status
</s>
</p>
<p>
<s>
Vertikální	vertikální	k2eAgFnSc1d1	vertikální
dimenze	dimenze	k1gFnSc1	dimenze
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c4	o
postupy	postup	k1gInPc4	postup
či	či	k8xC	či
propady	propad	k1gInPc4	propad
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgInPc1d1	pracovní
úspěchy	úspěch	k1gInPc1	úspěch
a	a	k8xC	a
neúspěchy	neúspěch	k1gInPc1	neúspěch
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
hodnocení	hodnocení	k1gNnSc1	hodnocení
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedinec	jedinec	k1gMnSc1	jedinec
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
jej	on	k3xPp3gMnSc4	on
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jeho	jeho	k3xOp3gNnSc1	jeho
sociální	sociální	k2eAgNnSc1d1	sociální
okolí	okolí	k1gNnSc1	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístupy	přístup	k1gInPc1	přístup
k	k	k7c3	k
objektu	objekt	k1gInSc3	objekt
==	==	k?	==
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
operacionalizací	operacionalizace	k1gFnPc2	operacionalizace
andragogiky	andragogika	k1gFnSc2	andragogika
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
jiný	jiný	k2eAgInSc4d1	jiný
z	z	k7c2	z
přístupů	přístup	k1gInPc2	přístup
<g/>
,	,	kIx,	,
sociotechnické	sociotechnický	k2eAgNnSc1d1	sociotechnický
paradigma	paradigma	k1gNnSc1	paradigma
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zakotveno	zakotven	k2eAgNnSc1d1	zakotveno
v	v	k7c6	v
základech	základ	k1gInPc6	základ
andragogického	andragogický	k2eAgNnSc2d1	andragogický
působení	působení	k1gNnSc2	působení
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
určuje	určovat	k5eAaImIp3nS	určovat
charakter	charakter	k1gInSc4	charakter
andragogiky	andragogika	k1gFnSc2	andragogika
jako	jako	k8xC	jako
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sociotechnický	sociotechnický	k2eAgInSc4d1	sociotechnický
přístup	přístup	k1gInSc4	přístup
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
poučený	poučený	k2eAgMnSc1d1	poučený
a	a	k8xC	a
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
věci	věc	k1gFnPc1	věc
mají	mít	k5eAaImIp3nP	mít
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
vypadat	vypadat	k5eAaImF	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
situaci	situace	k1gFnSc4	situace
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
změny	změna	k1gFnSc2	změna
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
moc	moc	k6eAd1	moc
i	i	k9	i
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
stojí	stát	k5eAaImIp3nS	stát
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
veden	vést	k5eAaImNgInS	vést
a	a	k8xC	a
poučován	poučován	k2eAgInSc1d1	poučován
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
své	svůj	k3xOyFgInPc4	svůj
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
rozvojem	rozvoj	k1gInSc7	rozvoj
toto	tento	k3xDgNnSc4	tento
paradigma	paradigma	k1gNnSc1	paradigma
pocházelo	pocházet	k5eAaImAgNnS	pocházet
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tzv.	tzv.	kA	tzv.
sociální	sociální	k2eAgNnSc1d1	sociální
inženýrství	inženýrství	k1gNnSc1	inženýrství
bylo	být	k5eAaImAgNnS	být
prostředkem	prostředek	k1gInSc7	prostředek
řešení	řešení	k1gNnSc4	řešení
všech	všecek	k3xTgInPc2	všecek
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
dodnes	dodnes	k6eAd1	dodnes
především	především	k9	především
v	v	k7c6	v
animačních	animační	k2eAgNnPc6d1	animační
pojetích	pojetí	k1gNnPc6	pojetí
andragogiky	andragogika	k1gFnSc2	andragogika
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
přístupech	přístup	k1gInPc6	přístup
socialozačních	socialozační	k2eAgInPc6d1	socialozační
a	a	k8xC	a
resocializačních	resocializační	k2eAgInPc6d1	resocializační
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Emancipační	emancipační	k2eAgInSc4d1	emancipační
přístup	přístup	k1gInSc4	přístup
===	===	k?	===
</s>
</p>
<p>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
autonomii	autonomie	k1gFnSc4	autonomie
jedince	jedinec	k1gMnSc2	jedinec
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnPc1	skupina
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
cestu	cesta	k1gFnSc4	cesta
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc1	cíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
dán	dán	k2eAgMnSc1d1	dán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cestu	cesta	k1gFnSc4	cesta
si	se	k3xPyFc3	se
učící	učící	k2eAgMnPc1d1	učící
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komunikační	komunikační	k2eAgInSc4d1	komunikační
přístup	přístup	k1gInSc4	přístup
===	===	k?	===
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
paradigma	paradigma	k1gNnSc1	paradigma
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
nezbytnost	nezbytnost	k1gFnSc4	nezbytnost
poznání	poznání	k1gNnSc2	poznání
komunikačního	komunikační	k2eAgNnSc2d1	komunikační
jednání	jednání	k1gNnSc2	jednání
pro	pro	k7c4	pro
optimalizaci	optimalizace	k1gFnSc4	optimalizace
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
předvídat	předvídat	k5eAaImF	předvídat
společenský	společenský	k2eAgInSc4d1	společenský
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
nelze	lze	k6eNd1	lze
tak	tak	k6eAd1	tak
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
koho	kdo	k3yRnSc4	kdo
a	a	k8xC	a
kam	kam	k6eAd1	kam
vést	vést	k5eAaImF	vést
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
poznávat	poznávat	k5eAaImF	poznávat
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
významy	význam	k1gInPc4	význam
lidské	lidský	k2eAgFnSc2d1	lidská
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
poznání	poznání	k1gNnSc1	poznání
následně	následně	k6eAd1	následně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
podstaty	podstata	k1gFnSc2	podstata
sociálního	sociální	k2eAgNnSc2d1	sociální
jednání	jednání	k1gNnSc2	jednání
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
podstaty	podstata	k1gFnSc2	podstata
fungování	fungování	k1gNnSc2	fungování
společenských	společenský	k2eAgInPc2d1	společenský
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objekt	objekt	k1gInSc1	objekt
a	a	k8xC	a
teoretikové	teoretik	k1gMnPc1	teoretik
andragogiky	andragogika	k1gFnSc2	andragogika
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
teoretiků	teoretik	k1gMnPc2	teoretik
andrgogiky	andrgogika	k1gFnSc2	andrgogika
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
tendence	tendence	k1gFnPc1	tendence
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
nahlížejí	nahlížet	k5eAaImIp3nP	nahlížet
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
objekt	objekt	k1gInSc1	objekt
andragogiky	andragogika	k1gFnSc2	andragogika
striktně	striktně	k6eAd1	striktně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
objektu	objekt	k1gInSc2	objekt
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
objekt	objekt	k1gInSc1	objekt
pedagogiky	pedagogika	k1gFnSc2	pedagogika
kontinuálně	kontinuálně	k6eAd1	kontinuálně
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
objekt	objekt	k1gInSc4	objekt
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgMnPc4d1	základní
teoretiky	teoretik	k1gMnPc4	teoretik
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Malcolm	Malcolm	k1gMnSc1	Malcolm
Sheperd	Sheperd	k1gMnSc1	Sheperd
Knowles	Knowles	k1gMnSc1	Knowles
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Jarvis	Jarvis	k1gInSc1	Jarvis
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgInSc1d1	Stephen
Brookfield	Brookfield	k1gInSc1	Brookfield
<g/>
,	,	kIx,	,
Paulo	Paula	k1gFnSc5	Paula
Freire	Freir	k1gInSc5	Freir
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Illich	Illich	k1gMnSc1	Illich
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
může	moct	k5eAaImIp3nS	moct
andragogika	andragogika	k1gFnSc1	andragogika
vycházet	vycházet	k5eAaImF	vycházet
i	i	k9	i
z	z	k7c2	z
myšlenek	myšlenka	k1gFnPc2	myšlenka
Martina	Martin	k1gMnSc2	Martin
Bubera	Buber	k1gMnSc2	Buber
a	a	k8xC	a
Michela	Michel	k1gMnSc2	Michel
Foucaulta	Foucault	k1gMnSc2	Foucault
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Integrální	integrální	k2eAgFnSc1d1	integrální
andragogika	andragogika	k1gFnSc1	andragogika
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nová	nový	k2eAgFnSc1d1	nová
koncepce	koncepce	k1gFnSc1	koncepce
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
různé	různý	k2eAgInPc1d1	různý
podněty	podnět	k1gInPc1	podnět
uvedených	uvedený	k2eAgNnPc2d1	uvedené
pojetí	pojetí	k1gNnPc2	pojetí
a	a	k8xC	a
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
je	on	k3xPp3gMnPc4	on
pod	pod	k7c4	pod
označení	označení	k1gNnSc4	označení
integrální	integrální	k2eAgFnSc1d1	integrální
andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
byla	být	k5eAaImAgFnS	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Jochmannem	Jochmann	k1gMnSc7	Jochmann
tato	tento	k3xDgFnSc1	tento
širší	široký	k2eAgFnSc1d2	širší
koncepce	koncepce	k1gFnSc1	koncepce
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
personální	personální	k2eAgInSc1d1	personální
management	management	k1gInSc1	management
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
i	i	k8xC	i
kulturní	kulturní	k2eAgFnSc4d1	kulturní
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jochmann	Jochmann	k1gMnSc1	Jochmann
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
koncepce	koncepce	k1gFnSc2	koncepce
výchovy	výchova	k1gFnSc2	výchova
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
sociologii	sociologie	k1gFnSc6	sociologie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
ideami	idea	k1gFnPc7	idea
antropologické	antropologický	k2eAgFnSc2d1	antropologická
pedagogiky	pedagogika	k1gFnSc2	pedagogika
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
odrážet	odrážet	k5eAaImF	odrážet
civilizační	civilizační	k2eAgInPc4d1	civilizační
trendy	trend	k1gInPc4	trend
přelomu	přelom	k1gInSc2	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Integrální	integrální	k2eAgFnSc1d1	integrální
andragogika	andragogika	k1gFnSc1	andragogika
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
širokou	široký	k2eAgFnSc4d1	široká
oblast	oblast	k1gFnSc4	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
</s>
</p>
<p>
<s>
oblast	oblast	k1gFnSc1	oblast
edukace	edukace	k1gFnSc2	edukace
</s>
</p>
<p>
<s>
oblast	oblast	k1gFnSc1	oblast
péče	péče	k1gFnSc2	péče
</s>
</p>
<p>
<s>
funkcionální	funkcionální	k2eAgNnSc1d1	funkcionální
působení	působení	k1gNnSc1	působení
</s>
</p>
<p>
<s>
==	==	k?	==
Problém	problém	k1gInSc1	problém
legitimizace	legitimizace	k1gFnSc2	legitimizace
==	==	k?	==
</s>
</p>
<p>
<s>
Otázka	otázka	k1gFnSc1	otázka
andragogiky	andragogika	k1gFnSc2	andragogika
jako	jako	k8xC	jako
vědní	vědní	k2eAgFnSc2d1	vědní
disciplíny	disciplína	k1gFnSc2	disciplína
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
řešit	řešit	k5eAaImF	řešit
až	až	k9	až
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
andragogice	andragogika	k1gFnSc3	andragogika
stále	stále	k6eAd1	stále
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
existenci	existence	k1gFnSc4	existence
jakožto	jakožto	k8xS	jakožto
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
uznána	uznán	k2eAgFnSc1d1	uznána
vědami	věda	k1gFnPc7	věda
ostatními	ostatní	k2eAgFnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc1d1	základní
legitimizační	legitimizační	k2eAgInPc1d1	legitimizační
problémy	problém	k1gInPc1	problém
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pochybnost	pochybnost	k1gFnSc1	pochybnost
o	o	k7c6	o
odlišnosti	odlišnost	k1gFnSc6	odlišnost
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
mládeže	mládež	k1gFnSc2	mládež
</s>
</p>
<p>
<s>
Nevole	nevole	k1gFnSc1	nevole
aplikovat	aplikovat	k5eAaBmF	aplikovat
pojem	pojem	k1gInSc4	pojem
výchovy	výchova	k1gFnSc2	výchova
na	na	k7c4	na
dospělého	dospělý	k2eAgMnSc4d1	dospělý
jedince	jedinec	k1gMnSc4	jedinec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zastávány	zastáván	k2eAgInPc1d1	zastáván
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
dospělý	dospělý	k1gMnSc1	dospělý
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nevychovatelný	vychovatelný	k2eNgInSc1d1	nevychovatelný
</s>
</p>
<p>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
andragogika	andragogika	k1gFnSc1	andragogika
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
ideologií	ideologie	k1gFnSc7	ideologie
nebo	nebo	k8xC	nebo
souhrnem	souhrn	k1gInSc7	souhrn
pokynů	pokyn	k1gInPc2	pokyn
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
</s>
</p>
<p>
<s>
Spory	spor	k1gInPc1	spor
ohledně	ohledně	k7c2	ohledně
vymezení	vymezení	k1gNnSc2	vymezení
náplně	náplň	k1gFnSc2	náplň
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
používat	používat	k5eAaImF	používat
úzké	úzký	k2eAgNnSc4d1	úzké
či	či	k8xC	či
širší	široký	k2eAgNnSc4d2	širší
pojetí	pojetí	k1gNnSc4	pojetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Katedry	katedra	k1gFnSc2	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
a	a	k8xC	a
personálního	personální	k2eAgNnSc2d1	personální
řízení	řízení	k1gNnSc2	řízení
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
jako	jako	k8xS	jako
Katedra	katedra	k1gFnSc1	katedra
lidovýchovy	lidovýchova	k1gFnSc2	lidovýchova
při	při	k7c6	při
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Působit	působit	k5eAaImF	působit
začala	začít	k5eAaPmAgNnP	začít
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
studijního	studijní	k2eAgInSc2d1	studijní
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
/	/	kIx~	/
<g/>
1948	[number]	k4	1948
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
T.	T.	kA	T.
Trnky	trnka	k1gFnSc2	trnka
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
katedra	katedra	k1gFnSc1	katedra
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studijním	studijní	k2eAgInSc6d1	studijní
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
obor	obor	k1gInSc4	obor
osvěta	osvěta	k1gFnSc1	osvěta
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
fakultě	fakulta	k1gFnSc6	fakulta
osvěty	osvěta	k1gFnSc2	osvěta
a	a	k8xC	a
novinářství	novinářství	k1gNnSc2	novinářství
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
zaniká	zanikat	k5eAaImIp3nS	zanikat
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
obory	obor	k1gInPc1	obor
předcházejí	předcházet	k5eAaImIp3nP	předcházet
na	na	k7c4	na
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
obsah	obsah	k1gInSc1	obsah
i	i	k8xC	i
název	název	k1gInSc1	název
katedry	katedra	k1gFnSc2	katedra
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
změnila	změnit	k5eAaPmAgFnS	změnit
své	svůj	k3xOyFgNnSc4	svůj
zaměření	zaměření	k1gNnSc4	zaměření
a	a	k8xC	a
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Katedru	katedra	k1gFnSc4	katedra
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyčlenění	vyčlenění	k1gNnSc6	vyčlenění
sociální	sociální	k2eAgFnSc2d1	sociální
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stala	stát	k5eAaPmAgFnS	stát
samostatná	samostatný	k2eAgFnSc1d1	samostatná
katedra	katedra	k1gFnSc1	katedra
s	s	k7c7	s
názvem	název	k1gInSc7	název
Katedra	katedra	k1gFnSc1	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
a	a	k8xC	a
personálního	personální	k2eAgNnSc2d1	personální
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
sociologie	sociologie	k1gFnSc2	sociologie
a	a	k8xC	a
andragogiky	andragogika	k1gFnSc2	andragogika
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
ve	v	k7c6	v
studijním	studijní	k2eAgInSc6d1	studijní
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
studovat	studovat	k5eAaImF	studovat
pedagogiku	pedagogika	k1gFnSc4	pedagogika
dospělých	dospělí	k1gMnPc2	dospělí
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
sociologií	sociologie	k1gFnSc7	sociologie
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
větvích	větev	k1gFnPc6	větev
–	–	k?	–
podniková	podnikový	k2eAgFnSc1d1	podniková
pedagogika	pedagogika	k1gFnSc1	pedagogika
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
pedagogika	pedagogika	k1gFnSc1	pedagogika
–	–	k?	–
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
uměnovědnými	uměnovědný	k2eAgInPc7d1	uměnovědný
obory	obor	k1gInPc7	obor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
studium	studium	k1gNnSc1	studium
muselo	muset	k5eAaImAgNnS	muset
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
celostátnímu	celostátní	k2eAgInSc3d1	celostátní
studijnímu	studijní	k2eAgInSc3d1	studijní
plánu	plán	k1gInSc3	plán
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
samostatná	samostatný	k2eAgFnSc1d1	samostatná
katedra	katedra	k1gFnSc1	katedra
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Akreditační	akreditační	k2eAgFnSc1d1	akreditační
komise	komise	k1gFnSc1	komise
MŠMT	MŠMT	kA	MŠMT
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
andragogiku	andragogika	k1gFnSc4	andragogika
jako	jako	k8xS	jako
studijní	studijní	k2eAgInSc4d1	studijní
obor	obor	k1gInSc4	obor
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
od	od	k7c2	od
studijního	studijní	k2eAgInSc2d1	studijní
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
se	se	k3xPyFc4	se
obor	obor	k1gInSc4	obor
andragogika	andragogika	k1gFnSc1	andragogika
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
koncepci	koncepce	k1gFnSc6	koncepce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
probíhá	probíhat	k5eAaImIp3nS	probíhat
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xS	jako
dvouoborové	dvouoborový	k2eAgInPc1d1	dvouoborový
s	s	k7c7	s
možnými	možný	k2eAgFnPc7d1	možná
kombinacemi	kombinace	k1gFnPc7	kombinace
<g/>
:	:	kIx,	:
Andragogika-Sociologie	Andragogika-Sociologie	k1gFnPc1	Andragogika-Sociologie
nebo	nebo	k8xC	nebo
Andragogika-Kulturní	Andragogika-Kulturní	k2eAgFnPc1d1	Andragogika-Kulturní
Antropologie	antropologie	k1gFnPc1	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
tohoto	tento	k3xDgNnSc2	tento
pojetí	pojetí	k1gNnSc2	pojetí
je	být	k5eAaImIp3nS	být
doc.	doc.	kA	doc.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Jochmann	Jochmann	k1gMnSc1	Jochmann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinované	kombinovaný	k2eAgFnSc6d1	kombinovaná
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
studuje	studovat	k5eAaImIp3nS	studovat
andragogika	andragogika	k1gFnSc1	andragogika
jednooborově	jednooborově	k6eAd1	jednooborově
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
Andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
habilitace	habilitace	k1gFnSc1	habilitace
a	a	k8xC	a
jmenování	jmenování	k1gNnSc4	jmenování
profesorem	profesor	k1gMnSc7	profesor
pro	pro	k7c4	pro
obor	obor	k1gInSc4	obor
andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Katedra	katedra	k1gFnSc1	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
soukromé	soukromý	k2eAgFnSc6d1	soukromá
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
nabízí	nabízet	k5eAaImIp3nS	nabízet
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
programu	program	k1gInSc6	program
obor	obor	k1gInSc1	obor
Vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
programu	program	k1gInSc6	program
obor	obor	k1gInSc1	obor
Andragogika	andragogika	k1gFnSc1	andragogika
a	a	k8xC	a
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
Andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
je	být	k5eAaImIp3nS	být
pracovištěm	pracoviště	k1gNnSc7	pracoviště
orientovaným	orientovaný	k2eAgNnSc7d1	orientované
na	na	k7c4	na
pedagogické	pedagogický	k2eAgFnPc4d1	pedagogická
vědy	věda	k1gFnPc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
šíři	šíř	k1gFnSc6	šíř
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
odkaz	odkaz	k1gInSc4	odkaz
Pedagogického	pedagogický	k2eAgInSc2d1	pedagogický
semináře	seminář	k1gInSc2	seminář
zřízeného	zřízený	k2eAgInSc2d1	zřízený
na	na	k7c6	na
Filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
studium	studium	k1gNnSc4	studium
Andragogiky	andragogika	k1gFnSc2	andragogika
jako	jako	k8xC	jako
jedooborového	jedooborový	k2eAgInSc2d1	jedooborový
navazujícího	navazující	k2eAgInSc2d1	navazující
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
i	i	k8xC	i
kombinované	kombinovaný	k2eAgFnSc6d1	kombinovaná
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
aktivity	aktivita	k1gFnPc4	aktivita
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
andragogiky	andragogika	k1gFnSc2	andragogika
zřídil	zřídit	k5eAaPmAgInS	zřídit
Ústav	ústav	k1gInSc1	ústav
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
jako	jako	k8xC	jako
podpůrné	podpůrný	k2eAgNnSc1d1	podpůrné
pracoviště	pracoviště	k1gNnSc1	pracoviště
Centrum	centrum	k1gNnSc1	centrum
inovace	inovace	k1gFnSc2	inovace
andragogických	andragogický	k2eAgFnPc2d1	andragogická
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
studia	studio	k1gNnSc2	studio
andragogiky	andragogika	k1gFnSc2	andragogika
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgInS	připravit
pro	pro	k7c4	pro
odbornou	odborný	k2eAgFnSc4d1	odborná
a	a	k8xC	a
koncepční	koncepční	k2eAgFnSc4d1	koncepční
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
samostatně	samostatně	k6eAd1	samostatně
plánovat	plánovat	k5eAaImF	plánovat
<g/>
,	,	kIx,	,
řídit	řídit	k5eAaImF	řídit
<g/>
,	,	kIx,	,
realizovat	realizovat	k5eAaBmF	realizovat
a	a	k8xC	a
hodnotit	hodnotit	k5eAaImF	hodnotit
mnohostranné	mnohostranný	k2eAgFnPc4d1	mnohostranná
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
aktivity	aktivita	k1gFnPc4	aktivita
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c4	na
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
adekvátní	adekvátní	k2eAgInPc4d1	adekvátní
didaktické	didaktický	k2eAgInPc4d1	didaktický
a	a	k8xC	a
metodické	metodický	k2eAgInPc4d1	metodický
přístupy	přístup	k1gInPc4	přístup
ke	k	k7c3	k
vzdělávání	vzdělávání	k1gNnSc3	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
specifických	specifický	k2eAgInPc2d1	specifický
přístupů	přístup	k1gInPc2	přístup
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
moderace	moderace	k1gFnPc1	moderace
a	a	k8xC	a
facilitace	facilitace	k1gFnPc1	facilitace
<g/>
,	,	kIx,	,
mentoring	mentoring	k1gInSc1	mentoring
<g/>
,	,	kIx,	,
zkušenostní	zkušenostní	k2eAgNnSc1d1	zkušenostní
a	a	k8xC	a
reflektivní	reflektivní	k2eAgNnSc1d1	reflektivní
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
pedagogika	pedagogika	k1gFnSc1	pedagogika
osobnostního	osobnostní	k2eAgInSc2d1	osobnostní
a	a	k8xC	a
sociálního	sociální	k2eAgInSc2d1	sociální
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
e-learning	eearning	k1gInSc1	e-learning
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
pedagogiky	pedagogika	k1gFnSc2	pedagogika
a	a	k8xC	a
andragogiky	andragogika	k1gFnSc2	andragogika
poprvé	poprvé	k6eAd1	poprvé
přijala	přijmout	k5eAaPmAgFnS	přijmout
studenty	student	k1gMnPc4	student
do	do	k7c2	do
kombinovaného	kombinovaný	k2eAgNnSc2d1	kombinované
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
Andragogiky	andragogika	k1gFnSc2	andragogika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
humanitních	humanitní	k2eAgFnPc2d1	humanitní
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
otevřel	otevřít	k5eAaPmAgInS	otevřít
kombinované	kombinovaný	k2eAgNnSc4d1	kombinované
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
studium	studium	k1gNnSc4	studium
Andragogiky	andragogika	k1gFnSc2	andragogika
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
profesní	profesní	k2eAgFnSc4d1	profesní
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc4d1	kulturní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
andragogiku	andragogika	k1gFnSc4	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Metodologicky	metodologicky	k6eAd1	metodologicky
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
systémovou	systémový	k2eAgFnSc4d1	systémová
andragogiku	andragogika	k1gFnSc4	andragogika
i	i	k8xC	i
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
oblasti	oblast	k1gFnPc4	oblast
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
teoretické	teoretický	k2eAgInPc1d1	teoretický
základy	základ	k1gInPc1	základ
a	a	k8xC	a
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c6	na
teorii	teorie	k1gFnSc6	teorie
a	a	k8xC	a
praxi	praxe	k1gFnSc6	praxe
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
srov.	srov.	kA	srov.
</s>
</p>
<p>
<s>
Prešovská	prešovský	k2eAgFnSc1d1	Prešovská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
sociálnej	sociálnat	k5eAaPmRp2nS	sociálnat
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
andragogiky	andragogika	k1gFnPc4	andragogika
a	a	k8xC	a
Inštitút	Inštitút	k1gInSc4	Inštitút
edukológie	edukológie	k1gFnSc2	edukológie
a	a	k8xC	a
sociálnej	sociálnat	k5eAaImRp2nS	sociálnat
práce	práce	k1gFnPc4	práce
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
poznatkové	poznatkový	k2eAgFnSc2d1	poznatková
základny	základna	k1gFnSc2	základna
profilujících	profilující	k2eAgFnPc2d1	profilující
disciplín	disciplína	k1gFnPc2	disciplína
andragogiky	andragogika	k1gFnSc2	andragogika
a	a	k8xC	a
na	na	k7c6	na
prezentování	prezentování	k1gNnSc6	prezentování
výsledků	výsledek	k1gInPc2	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
ve	v	k7c6	v
vyučovacím	vyučovací	k2eAgInSc6d1	vyučovací
procesu	proces	k1gInSc6	proces
a	a	k8xC	a
společenské	společenský	k2eAgFnSc3d1	společenská
praxi	praxe	k1gFnSc3	praxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Mateja	Mateja	k1gFnSc1	Mateja
Bela	Bela	k1gFnSc1	Bela
v	v	k7c6	v
Bánské	bánský	k2eAgFnSc6d1	Bánská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
byla	být	k5eAaImAgFnS	být
konstituována	konstituovat	k5eAaBmNgFnS	konstituovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
s	s	k7c7	s
posláním	poslání	k1gNnSc7	poslání
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
výuky	výuka	k1gFnSc2	výuka
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
a	a	k8xC	a
psychologických	psychologický	k2eAgFnPc2d1	psychologická
disciplín	disciplína	k1gFnPc2	disciplína
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
základě	základ	k1gInSc6	základ
učitelských	učitelský	k2eAgFnPc2d1	učitelská
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
katedra	katedra	k1gFnSc1	katedra
vyprofilovala	vyprofilovat	k5eAaPmAgFnS	vyprofilovat
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
akreditaci	akreditace	k1gFnSc4	akreditace
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
působila	působit	k5eAaImAgFnS	působit
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
jako	jako	k8xS	jako
Katedra	katedra	k1gFnSc1	katedra
andragogiky	andragogika	k1gFnSc2	andragogika
a	a	k8xC	a
psychológie	psychológie	k1gFnSc2	psychológie
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
katedry	katedra	k1gFnPc4	katedra
a	a	k8xC	a
převedena	převést	k5eAaPmNgFnS	převést
pod	pod	k7c4	pod
Pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
andragogiky	andragogika	k1gFnSc2	andragogika
==	==	k?	==
</s>
</p>
<p>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
je	být	k5eAaImIp3nS	být
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
každá	každý	k3xTgFnSc1	každý
jiná	jiný	k2eAgFnSc1d1	jiná
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
teoretický	teoretický	k2eAgInSc4d1	teoretický
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teoretické	teoretický	k2eAgFnPc1d1	teoretická
disciplíny	disciplína	k1gFnPc1	disciplína
</s>
</p>
<p>
<s>
historie	historie	k1gFnSc1	historie
andragogického	andragogický	k2eAgNnSc2d1	andragogický
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc2	vzdělání
dospělých	dospělý	k2eAgMnPc2d1	dospělý
</s>
</p>
<p>
<s>
teorie	teorie	k1gFnSc1	teorie
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
(	(	kIx(	(
<g/>
androdidaktika	androdidaktika	k1gFnSc1	androdidaktika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
teorie	teorie	k1gFnPc1	teorie
výchovy	výchova	k1gFnSc2	výchova
dospělých	dospělí	k1gMnPc2	dospělí
</s>
</p>
<p>
<s>
komparativní	komparativní	k2eAgFnSc1d1	komparativní
andragogika	andragogika	k1gFnSc1	andragogika
</s>
</p>
<p>
<s>
Praktické	praktický	k2eAgFnPc1d1	praktická
disciplíny	disciplína	k1gFnPc1	disciplína
</s>
</p>
<p>
<s>
profesní	profesní	k2eAgFnSc1d1	profesní
andragogika	andragogika	k1gFnSc1	andragogika
</s>
</p>
<p>
<s>
sociální	sociální	k2eAgFnSc1d1	sociální
andragogika	andragogika	k1gFnSc1	andragogika
</s>
</p>
<p>
<s>
kulturně-vzdělávací	kulturnězdělávací	k2eAgFnSc1d1	kulturně-vzdělávací
andragogika	andragogika	k1gFnSc1	andragogika
</s>
</p>
<p>
<s>
==	==	k?	==
Filosofie	filosofie	k1gFnSc1	filosofie
andragogiky	andragogika	k1gFnSc2	andragogika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Teorie	teorie	k1gFnSc2	teorie
rozvinuté	rozvinutý	k2eAgFnSc2d1	rozvinutá
v	v	k7c6	v
andragogice	andragogika	k1gFnSc6	andragogika
===	===	k?	===
</s>
</p>
<p>
<s>
behaviorální	behaviorální	k2eAgFnSc1d1	behaviorální
–	–	k?	–
Thorndike	Thorndike	k1gFnSc1	Thorndike
<g/>
,	,	kIx,	,
Skinner	Skinner	k1gInSc1	Skinner
</s>
</p>
<p>
<s>
pragmatické	pragmatický	k2eAgFnPc1d1	pragmatická
–	–	k?	–
Dewey	Dewea	k1gFnPc1	Dewea
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
</s>
</p>
<p>
<s>
psychoanalytické	psychoanalytický	k2eAgInPc1d1	psychoanalytický
–	–	k?	–
S.	S.	kA	S.
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
Adler	Adler	k1gMnSc1	Adler
</s>
</p>
<p>
<s>
existencialistické	existencialistický	k2eAgInPc1d1	existencialistický
–	–	k?	–
Schaller	Schaller	k1gMnSc1	Schaller
<g/>
,	,	kIx,	,
Bolnov	Bolnov	k1gInSc1	Bolnov
</s>
</p>
<p>
<s>
komunikativní	komunikativní	k2eAgMnSc1d1	komunikativní
–	–	k?	–
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Klafki	Klafk	k1gFnSc2	Klafk
</s>
</p>
<p>
<s>
humanistické	humanistický	k2eAgInPc1d1	humanistický
–	–	k?	–
Rogers	Rogersa	k1gFnPc2	Rogersa
<g/>
,	,	kIx,	,
Selby	Selba	k1gFnSc2	Selba
</s>
</p>
<p>
<s>
kritické	kritický	k2eAgFnPc1d1	kritická
</s>
</p>
<p>
<s>
===	===	k?	===
Nové	Nové	k2eAgInPc1d1	Nové
směry	směr	k1gInPc1	směr
v	v	k7c6	v
andragogice	andragogika	k1gFnSc6	andragogika
===	===	k?	===
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
nových	nový	k2eAgInPc2d1	nový
směrů	směr	k1gInPc2	směr
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
alespoň	alespoň	k9	alespoň
ty	ten	k3xDgMnPc4	ten
nejznámější	známý	k2eAgMnPc4d3	nejznámější
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
teologická	teologický	k2eAgFnSc1d1	teologická
pedagogika	pedagogika	k1gFnSc1	pedagogika
</s>
</p>
<p>
<s>
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
</s>
</p>
<p>
<s>
==	==	k?	==
Oblasti	oblast	k1gFnSc3	oblast
působení	působení	k1gNnSc2	působení
<g/>
(	(	kIx(	(
<g/>
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
)	)	kIx)	)
andragogiky	andragogika	k1gFnSc2	andragogika
==	==	k?	==
</s>
</p>
<p>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
světě	svět	k1gInSc6	svět
široké	široký	k2eAgNnSc4d1	široké
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
široké	široký	k2eAgNnSc1d1	široké
pole	pole	k1gNnSc1	pole
působnosti	působnost	k1gFnSc2	působnost
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
shrnout	shrnout	k5eAaPmF	shrnout
do	do	k7c2	do
třech	tři	k4xCgFnPc2	tři
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
oblast	oblast	k1gFnSc1	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
podnikové	podnikový	k2eAgInPc4d1	podnikový
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
systémy	systém	k1gInPc4	systém
</s>
</p>
<p>
<s>
školské	školský	k2eAgInPc4d1	školský
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
systémy	systém	k1gInPc4	systém
</s>
</p>
<p>
<s>
zájmové	zájmový	k2eAgInPc4d1	zájmový
(	(	kIx(	(
<g/>
mimoškolské	mimoškolský	k2eAgInPc4d1	mimoškolský
<g/>
)	)	kIx)	)
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
systémy	systém	k1gInPc4	systém
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
se	s	k7c7	s
specifickým	specifický	k2eAgNnSc7d1	specifické
zaměřením	zaměření	k1gNnSc7	zaměření
===	===	k?	===
</s>
</p>
<p>
<s>
ekologická	ekologický	k2eAgFnSc1d1	ekologická
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
kulturní	kulturní	k2eAgFnSc1d1	kulturní
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
politická	politický	k2eAgFnSc1d1	politická
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
občanská	občanský	k2eAgFnSc1d1	občanská
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
prevence	prevence	k1gFnSc1	prevence
patologických	patologický	k2eAgInPc2d1	patologický
jevů	jev	k1gInPc2	jev
v	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
</s>
</p>
<p>
<s>
sexuální	sexuální	k2eAgFnSc1d1	sexuální
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
osvěta	osvěta	k1gFnSc1	osvěta
</s>
</p>
<p>
<s>
lékařská	lékařský	k2eAgFnSc1d1	lékařská
andragogika	andragogika	k1gFnSc1	andragogika
</s>
</p>
<p>
<s>
kriminalistická	kriminalistický	k2eAgFnSc1d1	kriminalistická
andragogika	andragogika	k1gFnSc1	andragogika
</s>
</p>
<p>
<s>
penologická	penologický	k2eAgFnSc1d1	penologický
andragogika	andragogika	k1gFnSc1	andragogika
(	(	kIx(	(
<g/>
trestné	trestný	k2eAgNnSc1d1	trestné
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
věznice	věznice	k1gFnSc1	věznice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podniková	podnikový	k2eAgFnSc1d1	podniková
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
vojenská	vojenský	k2eAgFnSc1d1	vojenská
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
tělesná	tělesný	k2eAgFnSc1d1	tělesná
výchova	výchova	k1gFnSc1	výchova
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
občanům	občan	k1gMnPc3	občan
===	===	k?	===
</s>
</p>
<p>
<s>
kulturně-výchovná	kulturněýchovný	k2eAgFnSc1d1	kulturně-výchovný
činnost	činnost	k1gFnSc1	činnost
</s>
</p>
<p>
<s>
muzejnictví	muzejnictví	k1gNnSc1	muzejnictví
</s>
</p>
<p>
<s>
památková	památkový	k2eAgFnSc1d1	památková
péče	péče	k1gFnSc1	péče
</s>
</p>
<p>
<s>
personalistika	personalistika	k1gFnSc1	personalistika
</s>
</p>
<p>
<s>
rozvoj	rozvoj	k1gInSc4	rozvoj
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
</s>
</p>
<p>
<s>
personální	personální	k2eAgInSc1d1	personální
management	management	k1gInSc1	management
</s>
</p>
<p>
<s>
sociální	sociální	k2eAgFnSc1d1	sociální
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
péče	péče	k1gFnSc1	péče
</s>
</p>
<p>
<s>
postpenitenciární	postpenitenciární	k2eAgFnSc1d1	postpenitenciární
péče	péče	k1gFnSc1	péče
(	(	kIx(	(
<g/>
po	po	k7c6	po
výkonu	výkon	k1gInSc6	výkon
trestu	trest	k1gInSc2	trest
-	-	kIx~	-
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poradenství	poradenství	k1gNnPc1	poradenství
</s>
</p>
<p>
<s>
volnočasové	volnočasová	k1gFnPc1	volnočasová
<g/>
,	,	kIx,	,
odpočinkové	odpočinkový	k2eAgFnPc1d1	odpočinková
a	a	k8xC	a
seberealizační	seberealizační	k2eAgFnPc1d1	seberealizační
aktivity	aktivita	k1gFnPc1	aktivita
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
projekty	projekt	k1gInPc4	projekt
o	o	k7c4	o
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
projektu	projekt	k1gInSc2	projekt
<g/>
:	:	kIx,	:
Vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
:	:	kIx,	:
priority	priorita	k1gFnPc1	priorita
<g/>
,	,	kIx,	,
příležitosti	příležitost	k1gFnPc1	příležitost
a	a	k8xC	a
možnosti	možnost	k1gFnPc1	možnost
rozvoje	rozvoj	k1gInSc2	rozvoj
</s>
</p>
<p>
<s>
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
jako	jako	k8xS	jako
součásti	součást	k1gFnSc2	součást
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
obecnému	obecný	k2eAgInSc3d1	obecný
přehledu	přehled	k1gInSc3	přehled
o	o	k7c4	o
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
dimenzích	dimenze	k1gFnPc6	dimenze
<g/>
:	:	kIx,	:
jaké	jaký	k3yIgNnSc1	jaký
jsou	být	k5eAaImIp3nP	být
vzdělávací	vzdělávací	k2eAgFnPc1d1	vzdělávací
potřeby	potřeba	k1gFnPc1	potřeba
dospělé	dospělý	k2eAgFnPc1d1	dospělá
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
stávající	stávající	k2eAgFnPc4d1	stávající
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
možnosti	možnost	k1gFnPc4	možnost
ve	v	k7c6	v
formálním	formální	k2eAgInSc6d1	formální
a	a	k8xC	a
neformálním	formální	k2eNgInSc6d1	neformální
vzdělávacím	vzdělávací	k2eAgInSc6d1	vzdělávací
sektoru	sektor	k1gInSc6	sektor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vzdělávací	vzdělávací	k2eAgFnSc1d1	vzdělávací
"	"	kIx"	"
<g/>
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
"	"	kIx"	"
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
poptávkou	poptávka	k1gFnSc7	poptávka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc1	jaký
existují	existovat	k5eAaImIp3nP	existovat
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
bariéry	bariéra	k1gFnPc4	bariéra
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
cílovým	cílový	k2eAgFnPc3d1	cílová
skupinám	skupina	k1gFnPc3	skupina
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
přístupy	přístup	k1gInPc7	přístup
a	a	k8xC	a
řešeními	řešení	k1gNnPc7	řešení
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
podporovat	podporovat	k5eAaImF	podporovat
větší	veliký	k2eAgNnSc4d2	veliký
a	a	k8xC	a
smysluplnější	smysluplný	k2eAgNnSc4d2	smysluplnější
zapojení	zapojení	k1gNnSc4	zapojení
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
celou	celý	k2eAgFnSc4d1	celá
situaci	situace	k1gFnSc4	situace
vnímat	vnímat	k5eAaImF	vnímat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
politiky	politika	k1gFnSc2	politika
státu	stát	k1gInSc2	stát
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
relevantních	relevantní	k2eAgInPc2d1	relevantní
subjektů	subjekt	k1gInPc2	subjekt
angažujících	angažující	k2eAgFnPc2d1	angažující
se	se	k3xPyFc4	se
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
jak	jak	k6eAd1	jak
ji	on	k3xPp3gFnSc4	on
hodnotit	hodnotit	k5eAaImF	hodnotit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
poznáním	poznání	k1gNnSc7	poznání
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
projekt	projekt	k1gInSc1	projekt
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
několik	několik	k4yIc4	několik
dílčích	dílčí	k2eAgFnPc2d1	dílčí
tematických	tematický	k2eAgFnPc2d1	tematická
studií	studie	k1gFnPc2	studie
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
daných	daný	k2eAgFnPc2d1	daná
dominantními	dominantní	k2eAgFnPc7d1	dominantní
životními	životní	k2eAgFnPc7d1	životní
rolemi	role	k1gFnPc7	role
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
předseniorském	předseniorský	k2eAgInSc6d1	předseniorský
a	a	k8xC	a
seniorském	seniorský	k2eAgInSc6d1	seniorský
věku	věk	k1gInSc6	věk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
funkční	funkční	k2eAgFnSc2d1	funkční
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
rodičovské	rodičovský	k2eAgFnSc6d1	rodičovská
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
v	v	k7c6	v
profesním	profesní	k2eAgInSc6d1	profesní
a	a	k8xC	a
kariérním	kariérní	k2eAgInSc6d1	kariérní
rozvoji	rozvoj	k1gInSc6	rozvoj
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
možností	možnost	k1gFnPc2	možnost
využívání	využívání	k1gNnSc2	využívání
ICT	ICT	kA	ICT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
podpory	podpora	k1gFnSc2	podpora
zdravotně	zdravotně	k6eAd1	zdravotně
handicapovaných	handicapovaný	k2eAgMnPc2d1	handicapovaný
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rozvoje	rozvoj	k1gInSc2	rozvoj
zájmů	zájem	k1gInPc2	zájem
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
naplňování	naplňování	k1gNnSc2	naplňování
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
projektu	projekt	k1gInSc2	projekt
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgInP	využívat
jak	jak	k8xS	jak
kvantitativní	kvantitativní	k2eAgInPc1d1	kvantitativní
(	(	kIx(	(
<g/>
survey	survey	k1gInPc1	survey
na	na	k7c6	na
reprezentativním	reprezentativní	k2eAgInSc6d1	reprezentativní
vzorku	vzorek	k1gInSc6	vzorek
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
kategorii	kategorie	k1gFnSc6	kategorie
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
kvalitativní	kvalitativní	k2eAgFnSc3d1	kvalitativní
(	(	kIx(	(
<g/>
biografická	biografický	k2eAgFnSc1d1	biografická
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
hloubkové	hloubkový	k2eAgInPc1d1	hloubkový
rozhovory	rozhovor	k1gInPc1	rozhovor
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zakotvené	zakotvený	k2eAgFnSc2d1	zakotvená
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
skupinové	skupinový	k2eAgInPc1d1	skupinový
rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
,	,	kIx,	,
obsahová	obsahový	k2eAgFnSc1d1	obsahová
analýza	analýza	k1gFnSc1	analýza
dokumentů	dokument	k1gInPc2	dokument
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zainteresovaných	zainteresovaný	k2eAgFnPc2d1	zainteresovaná
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
)	)	kIx)	)
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gNnSc4	jejich
dominantní	dominantní	k2eAgNnSc4d1	dominantní
uplatnění	uplatnění	k1gNnSc4	uplatnění
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dílčím	dílčí	k2eAgInPc3d1	dílčí
výzkumným	výzkumný	k2eAgInPc3d1	výzkumný
záměrům	záměr	k1gInPc3	záměr
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
fáze	fáze	k1gFnPc4	fáze
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Řešitelem	řešitel	k1gMnSc7	řešitel
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
Ústav	ústav	k1gInSc1	ústav
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
FF	ff	kA	ff
MU	MU	kA	MU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BARTOŇKOVÁ	Bartoňková	k1gFnSc1	Bartoňková
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Foucaultovo	Foucaultův	k2eAgNnSc1d1	Foucaultovo
andragogické	andragogický	k2eAgNnSc1d1	andragogický
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
MJF	MJF	kA	MJF
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
292	[number]	k4	292
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86284	[number]	k4	86284
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BENEŠ	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eurolex	Eurolex	k1gInSc1	Eurolex
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
216	[number]	k4	216
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86432	[number]	k4	86432
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BENEŠ	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
129	[number]	k4	129
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
381	[number]	k4	381
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DVOŘÁKOVÁ	Dvořáková	k1gFnSc1	Dvořáková
<g/>
,	,	kIx,	,
Miroslava	Miroslava	k1gFnSc1	Miroslava
-	-	kIx~	-
ŠERÁK	Šerák	k1gMnSc1	Šerák
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Andragogika	andragogika	k1gFnSc1	andragogika
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
kapitoly	kapitola	k1gFnPc1	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
170	[number]	k4	170
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7308-694-7	[number]	k4	978-80-7308-694-7
</s>
</p>
<p>
<s>
KALNICKÝ	KALNICKÝ	kA	KALNICKÝ
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Efektivnost	efektivnost	k1gFnSc1	efektivnost
a	a	k8xC	a
ekonomika	ekonomika	k1gFnSc1	ekonomika
edukace	edukace	k1gFnSc2	edukace
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Repronis	Repronis	k1gInSc1	Repronis
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
148	[number]	k4	148
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7329	[number]	k4	7329
<g/>
-	-	kIx~	-
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KALNICKÝ	KALNICKÝ	kA	KALNICKÝ
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
<g/>
.	.	kIx.	.
</s>
<s>
Systémová	systémový	k2eAgFnSc1d1	systémová
andragogika	andragogika	k1gFnSc1	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
150	[number]	k4	150
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7368	[number]	k4	7368
<g/>
-	-	kIx~	-
<g/>
489	[number]	k4	489
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VETEŠKA	VETEŠKA	kA	VETEŠKA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
andragogiky	andragogika	k1gFnSc2	andragogika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-262-1026-9	[number]	k4	978-80-262-1026-9
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
andragogika	andragogika	k1gFnSc1	andragogika
ve	v	k7c6	v
WikislovníkuPROJEKT	WikislovníkuPROJEKT	k1gFnSc6	WikislovníkuPROJEKT
"	"	kIx"	"
<g/>
Vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
:	:	kIx,	:
priority	priorita	k1gFnPc1	priorita
<g/>
,	,	kIx,	,
příležitosti	příležitost	k1gFnPc1	příležitost
a	a	k8xC	a
možnosti	možnost	k1gFnPc1	možnost
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Andragogika	andragogika	k1gFnSc1	andragogika
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRŮCHA	Průcha	k1gMnSc1	Průcha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
VETEŠKA	VETEŠKA	kA	VETEŠKA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Andragogický	Andragogický	k2eAgInSc1d1	Andragogický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aktualiz	aktualiz	k1gMnSc1	aktualiz
<g/>
.	.	kIx.	.
a	a	k8xC	a
rozš	rozš	k1gInSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
4748	[number]	k4	4748
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
