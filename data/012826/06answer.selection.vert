<s>
Andragogika	andragogika	k1gFnSc1	andragogika
je	být	k5eAaImIp3nS	být
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
dospělých	dospělý	k2eAgMnPc2d1	dospělý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
respektuje	respektovat	k5eAaImIp3nS	respektovat
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
<g/>
.	.	kIx.	.
</s>
