<s>
V	v	k7c6	v
Saturnově	Saturnův	k2eAgFnSc6d1	Saturnova
atmosféře	atmosféra	k1gFnSc6	atmosféra
vanou	vanout	k5eAaImIp3nP	vanout
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
400	[number]	k4	400
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnPc1	rychlost
až	až	k9	až
1	[number]	k4	1
800	[number]	k4	800
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pětkrát	pětkrát	k6eAd1	pětkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
nejrychlejší	rychlý	k2eAgInPc1d3	nejrychlejší
větry	vítr	k1gInPc1	vítr
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
<g/>
.	.	kIx.	.
</s>
