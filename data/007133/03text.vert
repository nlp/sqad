<s>
Rys	Rys	k1gMnSc1	Rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
(	(	kIx(	(
<g/>
Lynx	Lynx	k1gInSc1	Lynx
lynx	lynx	k1gInSc1	lynx
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Felis	Felis	k1gInSc1	Felis
lynx	lynx	k1gInSc1	lynx
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
v	v	k7c6	v
Eurasii	Eurasie	k1gFnSc6	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čtyřdruhového	čtyřdruhový	k2eAgInSc2d1	čtyřdruhový
rodu	rod	k1gInSc2	rod
rys	rys	k1gInSc1	rys
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
kočkovitou	kočkovitý	k2eAgFnSc7d1	kočkovitá
šelmou	šelma	k1gFnSc7	šelma
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
druhy	druh	k1gInPc4	druh
chráněné	chráněný	k2eAgInPc4d1	chráněný
Bernskou	bernský	k2eAgFnSc7d1	Bernská
konvencí	konvence	k1gFnSc7	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
zákonů	zákon	k1gInPc2	zákon
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
a	a	k8xC	a
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nelze	lze	k6eNd1	lze
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
byl	být	k5eAaImAgInS	být
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úspěšné	úspěšný	k2eAgFnSc3d1	úspěšná
reintrodukci	reintrodukce	k1gFnSc3	reintrodukce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
i	i	k9	i
případ	případ	k1gInSc1	případ
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nyní	nyní	k6eAd1	nyní
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
malých	malý	k2eAgFnPc2d1	malá
nepočetných	početný	k2eNgFnPc2d1	nepočetná
populací	populace	k1gFnPc2	populace
náležících	náležící	k2eAgFnPc2d1	náležící
ke	k	k7c3	k
karpatské	karpatský	k2eAgFnSc3d1	Karpatská
populaci	populace	k1gFnSc3	populace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
někteří	některý	k3yIgMnPc1	některý
zoologové	zoolog	k1gMnPc1	zoolog
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
poddruh	poddruh	k1gInSc4	poddruh
Lynx	Lynx	k1gInSc1	Lynx
lynx	lynx	k1gInSc1	lynx
carpathica	carpathica	k1gFnSc1	carpathica
<g/>
.	.	kIx.	.
</s>
<s>
Rys	Rys	k1gMnSc1	Rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
evropskou	evropský	k2eAgFnSc7d1	Evropská
kočkovitou	kočkovitý	k2eAgFnSc7d1	kočkovitá
šelmou	šelma	k1gFnSc7	šelma
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
těla	tělo	k1gNnSc2	tělo
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délkou	délka	k1gFnSc7	délka
ocasu	ocas	k1gInSc2	ocas
až	až	k9	až
25	[number]	k4	25
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výškou	výška	k1gFnSc7	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
až	až	k9	až
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
až	až	k9	až
35	[number]	k4	35
kg	kg	kA	kg
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
všech	všecek	k3xTgInPc2	všecek
rysů	rys	k1gInPc2	rys
jsou	být	k5eAaImIp3nP	být
trojúhelníkovité	trojúhelníkovitý	k2eAgFnPc1d1	trojúhelníkovitá
uši	ucho	k1gNnPc4	ucho
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
chomáčky	chomáček	k1gInPc7	chomáček
chlupů	chlup	k1gInPc2	chlup
na	na	k7c6	na
konci	konec	k1gInSc6	konec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
chvostky	chvostek	k1gInPc1	chvostek
<g/>
)	)	kIx)	)
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
konec	konec	k1gInSc1	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jedinců	jedinec	k1gMnPc2	jedinec
má	mít	k5eAaImIp3nS	mít
lícní	lícní	k2eAgInPc4d1	lícní
chlupy	chlup	k1gInPc4	chlup
prodloužené	prodloužený	k2eAgInPc4d1	prodloužený
a	a	k8xC	a
utvářející	utvářející	k2eAgInPc4d1	utvářející
licousy	licousy	k1gInPc4	licousy
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yInSc7	co
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
rys	rys	k1gInSc1	rys
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
světlejší	světlý	k2eAgFnSc1d2	světlejší
má	mít	k5eAaImIp3nS	mít
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
co	co	k3yInSc4	co
nejlépe	dobře	k6eAd3	dobře
maskován	maskovat	k5eAaBmNgInS	maskovat
v	v	k7c6	v
zasněžené	zasněžený	k2eAgFnSc6d1	zasněžená
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
jeho	jeho	k3xOp3gFnSc2	jeho
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
šedá	šedá	k1gFnSc1	šedá
s	s	k7c7	s
žlutavým	žlutavý	k2eAgNnSc7d1	žlutavé
až	až	k8xS	až
rezavým	rezavý	k2eAgNnSc7d1	rezavé
zabarvením	zabarvení	k1gNnSc7	zabarvení
a	a	k8xC	a
s	s	k7c7	s
hnědými	hnědý	k2eAgFnPc7d1	hnědá
až	až	k8xS	až
červenohnědými	červenohnědý	k2eAgFnPc7d1	červenohnědá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
hustší	hustý	k2eAgMnSc1d2	hustší
<g/>
,	,	kIx,	,
s	s	k7c7	s
méně	málo	k6eAd2	málo
výraznou	výrazný	k2eAgFnSc7d1	výrazná
skvrnitostí	skvrnitost	k1gFnSc7	skvrnitost
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
hřbetu	hřbet	k1gInSc2	hřbet
se	se	k3xPyFc4	se
často	často	k6eAd1	často
táhne	táhnout	k5eAaImIp3nS	táhnout
tmavý	tmavý	k2eAgInSc4d1	tmavý
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc4	břicho
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
světlejší	světlý	k2eAgNnSc1d2	světlejší
až	až	k8xS	až
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Karyotyp	Karyotyp	k1gInSc1	Karyotyp
somatických	somatický	k2eAgFnPc2d1	somatická
buněk	buňka	k1gFnPc2	buňka
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
38	[number]	k4	38
chromozomů	chromozom	k1gInPc2	chromozom
se	s	k7c7	s
72	[number]	k4	72
rameny	rameno	k1gNnPc7	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
rysa	rys	k1gMnSc2	rys
ostrovida	ostrovid	k1gMnSc2	ostrovid
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
-	-	kIx~	-
18	[number]	k4	18
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
až	až	k9	až
24	[number]	k4	24
let	léto	k1gNnPc2	léto
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
druhu	druh	k1gInSc2	druh
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
lesy	les	k1gInPc4	les
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Eurasii	Eurasie	k1gFnSc6	Eurasie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
systematický	systematický	k2eAgInSc1d1	systematický
lov	lov	k1gInSc1	lov
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
likvidace	likvidace	k1gFnSc2	likvidace
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zmenšení	zmenšení	k1gNnSc3	zmenšení
a	a	k8xC	a
roztříštěnosti	roztříštěnost	k1gFnSc3	roztříštěnost
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
početnosti	početnost	k1gFnSc2	početnost
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
relativně	relativně	k6eAd1	relativně
souvislá	souvislý	k2eAgFnSc1d1	souvislá
část	část	k1gFnSc1	část
areálu	areál	k1gInSc2	areál
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
až	až	k8xS	až
Fennoskandinávie	Fennoskandinávie	k1gFnSc2	Fennoskandinávie
a	a	k8xC	a
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
rozšíření	rozšíření	k1gNnSc1	rozšíření
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nesouvislé	souvislý	k2eNgNnSc1d1	nesouvislé
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnSc4d2	veliký
území	území	k1gNnSc4	území
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
silnými	silný	k2eAgFnPc7d1	silná
populacemi	populace	k1gFnPc7	populace
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
rys	rys	k1gInSc4	rys
až	až	k9	až
na	na	k7c4	na
pár	pár	k4xCyI	pár
lokálních	lokální	k2eAgFnPc2d1	lokální
přežívajících	přežívající	k2eAgFnPc2d1	přežívající
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc1d1	malá
lokální	lokální	k2eAgFnPc1d1	lokální
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
reintrodukcí	reintrodukce	k1gFnSc7	reintrodukce
nebo	nebo	k8xC	nebo
migrací	migrace	k1gFnSc7	migrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
o	o	k7c6	o
reintrodukci	reintrodukce	k1gFnSc6	reintrodukce
a	a	k8xC	a
obnovení	obnovení	k1gNnSc6	obnovení
populací	populace	k1gFnSc7	populace
rysa	rys	k1gMnSc2	rys
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
a	a	k8xC	a
pomalý	pomalý	k2eAgInSc4d1	pomalý
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
brzděný	brzděný	k2eAgMnSc1d1	brzděný
až	až	k8xS	až
mařený	mařený	k2eAgMnSc1d1	mařený
nepřátelským	přátelský	k2eNgInSc7d1	nepřátelský
postojem	postoj	k1gInSc7	postoj
myslivců	myslivec	k1gMnPc2	myslivec
a	a	k8xC	a
pytláctvím	pytláctví	k1gNnSc7	pytláctví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
7	[number]	k4	7
500	[number]	k4	500
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
500	[number]	k4	500
ve	v	k7c6	v
Fennoskandinávii	Fennoskandinávie	k1gFnSc6	Fennoskandinávie
<g/>
,	,	kIx,	,
2	[number]	k4	2
000	[number]	k4	000
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
a	a	k8xC	a
2	[number]	k4	2
200	[number]	k4	200
v	v	k7c6	v
Karpatské	karpatský	k2eAgFnSc6d1	Karpatská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
docházelo	docházet	k5eAaImAgNnS	docházet
od	od	k7c2	od
konce	konec	k1gInSc2	konec
středověku	středověk	k1gInSc2	středověk
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
hubení	hubení	k1gNnSc3	hubení
a	a	k8xC	a
vytlačování	vytlačování	k1gNnSc3	vytlačování
rysa	rys	k1gMnSc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
až	až	k9	až
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vybit	vybit	k2eAgMnSc1d1	vybit
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
osídlení	osídlení	k1gNnSc2	osídlení
(	(	kIx(	(
<g/>
Polabí	Polabí	k1gNnSc1	Polabí
a	a	k8xC	a
dolní	dolní	k2eAgNnPc1d1	dolní
Povltaví	Povltaví	k1gNnPc1	Povltaví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
lokální	lokální	k2eAgFnPc1d1	lokální
a	a	k8xC	a
roztříštěné	roztříštěný	k2eAgFnPc1d1	roztříštěná
enklávy	enkláva	k1gFnPc1	enkláva
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
životaschopné	životaschopný	k2eAgFnPc1d1	životaschopná
populace	populace	k1gFnPc1	populace
přežívaly	přežívat	k5eAaImAgFnP	přežívat
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
lese	les	k1gInSc6	les
a	a	k8xC	a
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
potvrzené	potvrzený	k2eAgInPc4d1	potvrzený
výskyty	výskyt	k1gInPc4	výskyt
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udávají	udávat	k5eAaImIp3nP	udávat
se	se	k3xPyFc4	se
i	i	k9	i
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
jednoznačně	jednoznačně	k6eAd1	jednoznačně
doložený	doložený	k2eAgInSc1d1	doložený
odstřelený	odstřelený	k2eAgInSc1d1	odstřelený
rys	rys	k1gInSc1	rys
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
na	na	k7c6	na
Táborsku	Táborsek	k1gInSc6	Táborsek
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
i	i	k9	i
odstřel	odstřel	k1gInSc1	odstřel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
v	v	k7c6	v
Dolním	dolní	k2eAgInSc6d1	dolní
Hvozdu	hvozd	k1gInSc6	hvozd
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
přežíval	přežívat	k5eAaImAgMnS	přežívat
rys	rys	k1gMnSc1	rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
díky	díky	k7c3	díky
migraci	migrace	k1gFnSc3	migrace
nových	nový	k2eAgMnPc2d1	nový
jedinců	jedinec	k1gMnPc2	jedinec
ze	z	k7c2	z
západokarpatské	západokarpatský	k2eAgFnSc2d1	západokarpatská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
migraci	migrace	k1gFnSc3	migrace
též	též	k9	též
nelze	lze	k6eNd1	lze
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
původní	původní	k2eAgFnSc1d1	původní
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
že	že	k8xS	že
zastřelení	zastřelený	k2eAgMnPc1d1	zastřelený
jedinci	jedinec	k1gMnPc1	jedinec
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
byli	být	k5eAaImAgMnP	být
zatoulanci	zatoulanec	k1gMnPc1	zatoulanec
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnPc1	jejich
potomci	potomek	k1gMnPc1	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
v	v	k7c6	v
karpatských	karpatský	k2eAgNnPc6d1	Karpatské
pohořích	pohoří	k1gNnPc6	pohoří
zachoval	zachovat	k5eAaPmAgInS	zachovat
a	a	k8xC	a
zásluhou	zásluhou	k7c2	zásluhou
úplné	úplný	k2eAgFnSc2d1	úplná
ochrany	ochrana	k1gFnSc2	ochrana
se	se	k3xPyFc4	se
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
jeho	jeho	k3xOp3gFnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
tak	tak	k6eAd1	tak
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
pronikat	pronikat	k5eAaImF	pronikat
i	i	k9	i
více	hodně	k6eAd2	hodně
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
na	na	k7c4	na
moravské	moravský	k2eAgNnSc4d1	Moravské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Rys	rys	k1gInSc1	rys
je	být	k5eAaImIp3nS	být
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
velkou	velký	k2eAgFnSc7d1	velká
šelmou	šelma	k1gFnSc7	šelma
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
zakládá	zakládat	k5eAaImIp3nS	zakládat
stálé	stálý	k2eAgFnSc2d1	stálá
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
migrace	migrace	k1gFnSc1	migrace
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
částečnému	částečný	k2eAgNnSc3d1	částečné
obnovení	obnovení	k1gNnSc3	obnovení
rysích	rysí	k2eAgFnPc2d1	rysí
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jeseníků	Jeseník	k1gInPc2	Jeseník
a	a	k8xC	a
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1	Moravskoslezská
Beskyd	Beskydy	k1gFnPc2	Beskydy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
až	až	k6eAd1	až
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudké	prudký	k2eAgFnSc3d1	prudká
decimaci	decimace	k1gFnSc3	decimace
nejdříve	dříve	k6eAd3	dříve
beskydské	beskydský	k2eAgFnPc1d1	Beskydská
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
jesenické	jesenický	k2eAgFnSc2d1	Jesenická
populace	populace	k1gFnSc2	populace
pytláky	pytlák	k1gMnPc4	pytlák
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gNnSc3	jeho
omezení	omezení	k1gNnSc3	omezení
a	a	k8xC	a
posílení	posílení	k1gNnSc3	posílení
migrace	migrace	k1gFnSc2	migrace
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
přežily	přežít	k5eAaPmAgFnP	přežít
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
vzpamatovaly	vzpamatovat	k5eAaPmAgFnP	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
díky	díky	k7c3	díky
reintrodukčním	reintrodukční	k2eAgInPc3d1	reintrodukční
programům	program	k1gInPc3	program
v	v	k7c6	v
Bavorském	bavorský	k2eAgInSc6d1	bavorský
lese	les	k1gInSc6	les
a	a	k8xC	a
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
rysa	rys	k1gMnSc2	rys
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Rysi	rys	k1gMnPc1	rys
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
těchto	tento	k3xDgFnPc2	tento
nových	nový	k2eAgFnPc2d1	nová
populací	populace	k1gFnPc2	populace
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dalších	další	k2eAgFnPc2d1	další
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
reintrodukováni	reintrodukovat	k5eAaPmNgMnP	reintrodukovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ZOO	zoo	k1gFnSc2	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
n.	n.	k?	n.
L	L	kA	L
(	(	kIx(	(
<g/>
SRN	SRN	kA	SRN
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tedy	tedy	k9	tedy
existují	existovat	k5eAaImIp3nP	existovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
tři	tři	k4xCgFnPc1	tři
izolované	izolovaný	k2eAgFnPc1d1	izolovaná
populace	populace	k1gFnPc1	populace
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
,	,	kIx,	,
Javorníky	Javorník	k1gInPc1	Javorník
<g/>
,	,	kIx,	,
Vsetínské	vsetínský	k2eAgInPc1d1	vsetínský
vrchy	vrch	k1gInPc1	vrch
a	a	k8xC	a
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
asi	asi	k9	asi
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vytrvalé	vytrvalý	k2eAgFnSc3d1	vytrvalá
migraci	migrace	k1gFnSc3	migrace
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g />
.	.	kIx.	.
</s>
<s>
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
stabilní	stabilní	k2eAgInSc4d1	stabilní
stav	stav	k1gInSc4	stav
2	[number]	k4	2
<g/>
)	)	kIx)	)
Jeseníky	Jeseník	k1gInPc1	Jeseník
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
pytláctví	pytláctví	k1gNnSc3	pytláctví
vymírá	vymírat	k5eAaImIp3nS	vymírat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možná	možný	k2eAgFnSc1d1	možná
občasná	občasný	k2eAgFnSc1d1	občasná
migrace	migrace	k1gFnSc1	migrace
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
3	[number]	k4	3
<g/>
)	)	kIx)	)
jižní	jižní	k2eAgFnPc1d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnPc1d1	západní
Čechy	Čechy	k1gFnPc1	Čechy
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
Blanský	blanský	k2eAgInSc1d1	blanský
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Novohradské	novohradský	k2eAgFnPc1d1	Novohradská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
52	[number]	k4	52
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
odstřelu	odstřel	k1gInSc3	odstřel
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
zřetelnému	zřetelný	k2eAgInSc3d1	zřetelný
poklesu	pokles	k1gInSc3	pokles
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
populaci	populace	k1gFnSc3	populace
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
i	i	k9	i
okrajové	okrajový	k2eAgFnPc4d1	okrajová
minipopulace	minipopulace	k1gFnPc4	minipopulace
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
v	v	k7c6	v
Brdech	Brdy	k1gInPc6	Brdy
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
asi	asi	k9	asi
2	[number]	k4	2
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Labských	labský	k2eAgInPc6d1	labský
pískovcích	pískovec	k1gInPc6	pískovec
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
hlášen	hlásit	k5eAaImNgInS	hlásit
i	i	k9	i
NP	NP	kA	NP
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lužických	lužický	k2eAgFnPc6d1	Lužická
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
z	z	k7c2	z
Jihlavských	jihlavský	k2eAgInPc2d1	jihlavský
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
populační	populační	k2eAgFnSc6d1	populační
dynamice	dynamika	k1gFnSc6	dynamika
rysa	rys	k1gMnSc2	rys
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
určité	určitý	k2eAgInPc4d1	určitý
dlouhodobější	dlouhodobý	k2eAgInPc4d2	dlouhodobější
cykly	cyklus	k1gInPc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byly	být	k5eAaImAgInP	být
objeveny	objeven	k2eAgInPc1d1	objeven
stopy	stop	k1gInPc1	stop
rysa	rys	k1gMnSc2	rys
i	i	k9	i
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
a	a	k8xC	a
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
necelých	celý	k2eNgNnPc6d1	necelé
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
rys	rys	k1gInSc1	rys
ostrovid	ostrovid	k1gInSc4	ostrovid
také	také	k9	také
zachycen	zachycen	k2eAgMnSc1d1	zachycen
fotopastí	fotopastit	k5eAaPmIp3nS	fotopastit
u	u	k7c2	u
nedalekých	daleký	k2eNgFnPc2d1	nedaleká
Adršpašských	adršpašský	k2eAgFnPc2d1	Adršpašská
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Duha	duha	k1gFnSc1	duha
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
rysa	rys	k1gMnSc2	rys
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
na	na	k7c4	na
100	[number]	k4	100
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnPc1	populace
jsou	být	k5eAaImIp3nP	být
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
pytláctvím	pytláctví	k1gNnSc7	pytláctví
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
ochranářů	ochranář	k1gMnPc2	ochranář
zabito	zabít	k5eAaPmNgNnS	zabít
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgNnSc7d1	typické
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
rysa	rys	k1gMnSc2	rys
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
oblasti	oblast	k1gFnSc2	oblast
smíšených	smíšený	k2eAgInPc2d1	smíšený
a	a	k8xC	a
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
lesů	les	k1gInPc2	les
středních	střední	k2eAgFnPc2d1	střední
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
poloh	poloha	k1gFnPc2	poloha
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
podrostem	podrost	k1gInSc7	podrost
a	a	k8xC	a
skalními	skalní	k2eAgInPc7d1	skalní
útvary	útvar	k1gInPc7	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvyšování	zvyšování	k1gNnSc6	zvyšování
početnosti	početnost	k1gFnSc2	početnost
však	však	k9	však
rys	rys	k1gInSc1	rys
proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
jak	jak	k8xC	jak
horských	horský	k2eAgNnPc2d1	horské
bezlesí	bezlesí	k1gNnPc2	bezlesí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
konkurentem	konkurent	k1gMnSc7	konkurent
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
v	v	k7c6	v
kulturních	kulturní	k2eAgFnPc6d1	kulturní
smrčinách	smrčina	k1gFnPc6	smrčina
a	a	k8xC	a
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívané	využívaný	k2eAgFnSc6d1	využívaná
krajině	krajina	k1gFnSc6	krajina
(	(	kIx(	(
<g/>
pakliže	pakliže	k8xS	pakliže
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
větší	veliký	k2eAgInPc4d2	veliký
lesní	lesní	k2eAgInPc4d1	lesní
celky	celek	k1gInPc4	celek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rys	Rys	k1gMnSc1	Rys
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
hlavně	hlavně	k9	hlavně
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
na	na	k7c6	na
tichých	tichý	k2eAgFnPc6d1	tichá
lokalitách	lokalita	k1gFnPc6	lokalita
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
i	i	k8xC	i
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
sluní	slunit	k5eAaImIp3nS	slunit
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
však	však	k9	však
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
ve	v	k7c6	v
skalních	skalní	k2eAgInPc6d1	skalní
úkrytech	úkryt	k1gInPc6	úkryt
nebo	nebo	k8xC	nebo
v	v	k7c6	v
houštinách	houština	k1gFnPc6	houština
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc1	období
říje	říje	k1gFnSc2	říje
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
dne	den	k1gInSc2	den
aktivní	aktivní	k2eAgNnSc1d1	aktivní
běžně	běžně	k6eAd1	běžně
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
denních	denní	k2eAgInPc2d1	denní
přesunů	přesun	k1gInPc2	přesun
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jedinec	jedinec	k1gMnSc1	jedinec
od	od	k7c2	od
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
i	i	k9	i
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
25	[number]	k4	25
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
žije	žít	k5eAaImIp3nS	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
,	,	kIx,	,
jen	jen	k9	jen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	s	k7c7	s
samicí	samice	k1gFnSc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
samici	samice	k1gFnSc4	samice
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
více	hodně	k6eAd2	hodně
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spolu	spolu	k6eAd1	spolu
bojují	bojovat	k5eAaImIp3nP	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Říje	říje	k1gFnSc1	říje
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červnu	červen	k1gInSc6	červen
rodí	rodit	k5eAaImIp3nP	rodit
samice	samice	k1gFnPc1	samice
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
houštinách	houština	k1gFnPc6	houština
<g/>
,	,	kIx,	,
skalních	skalní	k2eAgFnPc6d1	skalní
dutinách	dutina	k1gFnPc6	dutina
či	či	k8xC	či
pod	pod	k7c7	pod
vývratem	vývrat	k1gInSc7	vývrat
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
začnou	začít	k5eAaPmIp3nP	začít
tak	tak	k6eAd1	tak
po	po	k7c6	po
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
dnech	den	k1gInPc6	den
a	a	k8xC	a
kojení	kojení	k1gNnSc2	kojení
trvá	trvat	k5eAaImIp3nS	trvat
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
matky	matka	k1gFnSc2	matka
až	až	k9	až
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
říje	říje	k1gFnSc2	říje
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
zprvu	zprvu	k6eAd1	zprvu
krmí	krmě	k1gFnSc7	krmě
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
učí	učit	k5eAaImIp3nP	učit
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
mláďata	mládě	k1gNnPc1	mládě
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mezi	mezi	k7c7	mezi
21	[number]	k4	21
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
až	až	k8xS	až
33	[number]	k4	33
měsíci	měsíc	k1gInPc7	měsíc
(	(	kIx(	(
<g/>
samci	samec	k1gInPc7	samec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
rysové	rys	k1gMnPc1	rys
si	se	k3xPyFc3	se
vytyčují	vytyčovat	k5eAaImIp3nP	vytyčovat
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
celková	celkový	k2eAgFnSc1d1	celková
velikost	velikost	k1gFnSc1	velikost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
úživnosti	úživnost	k1gFnSc6	úživnost
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
po	po	k7c4	po
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Teritorium	teritorium	k1gNnSc1	teritorium
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
domovský	domovský	k2eAgInSc4d1	domovský
okrsek	okrsek	k1gInSc4	okrsek
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
si	se	k3xPyFc3	se
jedinec	jedinec	k1gMnSc1	jedinec
značkuje	značkovat	k5eAaImIp3nS	značkovat
trusem	trus	k1gInSc7	trus
a	a	k8xC	a
močí	moč	k1gFnSc7	moč
a	a	k8xC	a
urputně	urputně	k6eAd1	urputně
je	on	k3xPp3gInPc4	on
brání	bránit	k5eAaImIp3nS	bránit
proti	proti	k7c3	proti
vetřelcům	vetřelec	k1gMnPc3	vetřelec
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
obvykle	obvykle	k6eAd1	obvykle
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
)	)	kIx)	)
a	a	k8xC	a
okrajový	okrajový	k2eAgInSc4d1	okrajový
okrsek	okrsek	k1gInSc4	okrsek
<g/>
.	.	kIx.	.
</s>
<s>
Teritoria	teritorium	k1gNnPc4	teritorium
dvou	dva	k4xCgMnPc2	dva
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nepřekrývají	překrývat	k5eNaImIp3nP	překrývat
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
překrývají	překrývat	k5eAaImIp3nP	překrývat
jen	jen	k9	jen
nepatrně	patrně	k6eNd1	patrně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
samec	samec	k1gInSc4	samec
strpí	strpět	k5eAaPmIp3nP	strpět
překryv	překrýt	k5eAaPmDgInS	překrýt
svého	svůj	k3xOyFgNnSc2	svůj
teritoria	teritorium	k1gNnSc2	teritorium
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
i	i	k9	i
několika	několik	k4yIc7	několik
samičími	samičí	k2eAgNnPc7d1	samičí
teritorii	teritorium	k1gNnPc7	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Rys	Rys	k1gMnSc1	Rys
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgMnSc1	žádný
vytrvalý	vytrvalý	k2eAgMnSc1d1	vytrvalý
pronásledovatel	pronásledovatel	k1gMnSc1	pronásledovatel
<g/>
,	,	kIx,	,
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
číhá	číhat	k5eAaImIp3nS	číhat
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
z	z	k7c2	z
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ji	on	k3xPp3gFnSc4	on
nedostihne	dostihnout	k5eNaPmIp3nS	dostihnout
několika	několik	k4yIc7	několik
skoky	skok	k1gInPc7	skok
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
ji	on	k3xPp3gFnSc4	on
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
již	již	k9	již
po	po	k7c6	po
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
metrů	metr	k1gMnPc2	metr
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
po	po	k7c6	po
cca	cca	kA	cca
100	[number]	k4	100
metrech	metr	k1gInPc6	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
číhání	číhání	k1gNnSc3	číhání
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
vyvýšená	vyvýšený	k2eAgNnPc4d1	vyvýšené
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
kořist	kořist	k1gFnSc1	kořist
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
houštin	houština	k1gFnPc2	houština
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
rozeznat	rozeznat	k5eAaPmF	rozeznat
hlodavce	hlodavec	k1gMnPc4	hlodavec
na	na	k7c4	na
75	[number]	k4	75
m	m	kA	m
<g/>
,	,	kIx,	,
zajíce	zajíc	k1gMnSc2	zajíc
na	na	k7c4	na
300	[number]	k4	300
m	m	kA	m
a	a	k8xC	a
srnce	srnka	k1gFnSc6	srnka
na	na	k7c4	na
500	[number]	k4	500
m.	m.	k?	m.
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
lovu	lov	k1gInSc2	lov
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kořist	kořist	k1gFnSc4	kořist
na	na	k7c4	na
rysa	rys	k1gMnSc4	rys
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
faktorech	faktor	k1gInPc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
kopytníky	kopytník	k1gInPc4	kopytník
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
kořist	kořist	k1gFnSc1	kořist
je	být	k5eAaImIp3nS	být
zabíjena	zabíjet	k5eAaImNgFnS	zabíjet
kousnutím	kousnutí	k1gNnSc7	kousnutí
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kopytníci	kopytník	k1gMnPc1	kopytník
zakousnutím	zakousnutí	k1gNnPc3	zakousnutí
do	do	k7c2	do
hrdla	hrdlo	k1gNnSc2	hrdlo
nebo	nebo	k8xC	nebo
týla	týlo	k1gNnSc2	týlo
a	a	k8xC	a
zadušením	zadušení	k1gNnSc7	zadušení
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
spárkatá	spárkatý	k2eAgFnSc1d1	spárkatá
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
srnec	srnec	k1gMnSc1	srnec
<g/>
,	,	kIx,	,
muflon	muflon	k1gMnSc1	muflon
či	či	k8xC	či
kamzík	kamzík	k1gMnSc1	kamzík
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
již	již	k6eAd1	již
jelen	jelena	k1gFnPc2	jelena
či	či	k8xC	či
prase	prase	k1gNnSc1	prase
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
významný	významný	k2eAgInSc4d1	významný
lze	lze	k6eAd1	lze
ještě	ještě	k9	ještě
považovat	považovat	k5eAaImF	považovat
podíl	podíl	k1gInSc4	podíl
drobných	drobný	k2eAgMnPc2d1	drobný
hlodavců	hlodavec	k1gMnPc2	hlodavec
a	a	k8xC	a
zajíců	zajíc	k1gMnPc2	zajíc
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
pak	pak	k6eAd1	pak
jídelníček	jídelníček	k1gInSc4	jídelníček
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
zvířectvo	zvířectvo	k1gNnSc1	zvířectvo
<g/>
.	.	kIx.	.
</s>
<s>
Rys	rys	k1gInSc1	rys
zpravidla	zpravidla	k6eAd1	zpravidla
nezačne	začít	k5eNaPmIp3nS	začít
žrát	žrát	k5eAaImF	žrát
hned	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
lov	lov	k1gInSc1	lov
ho	on	k3xPp3gInSc4	on
často	často	k6eAd1	často
vzruší	vzrušit	k5eAaPmIp3nS	vzrušit
a	a	k8xC	a
než	než	k8xS	než
vzrušení	vzrušení	k1gNnSc1	vzrušení
pomine	pominout	k5eAaPmIp3nS	pominout
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
si	se	k3xPyFc3	se
s	s	k7c7	s
mrtvou	mrtvý	k2eAgFnSc7d1	mrtvá
kořistí	kořist	k1gFnSc7	kořist
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
pustí	pustit	k5eAaPmIp3nS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
jedlík	jedlík	k1gMnSc1	jedlík
<g/>
,	,	kIx,	,
na	na	k7c4	na
posezení	posezení	k1gNnSc4	posezení
spořádá	spořádat	k5eAaPmIp3nS	spořádat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
kg	kg	kA	kg
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
3,5	[number]	k4	3,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
kořist	kořist	k1gFnSc1	kořist
většinou	většinou	k6eAd1	většinou
odtáhne	odtáhnout	k5eAaPmIp3nS	odtáhnout
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
přehrne	přehrnout	k5eAaPmIp3nS	přehrnout
listím	listí	k1gNnSc7	listí
a	a	k8xC	a
větvičkami	větvička	k1gFnPc7	větvička
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
ochota	ochota	k1gFnSc1	ochota
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
vrátit	vrátit	k5eAaPmF	vrátit
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
míře	míra	k1gFnSc6	míra
hladu	hlad	k1gInSc2	hlad
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc2	dostupnost
dalších	další	k2eAgInPc2d1	další
úlovků	úlovek	k1gInPc2	úlovek
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dostatek	dostatek	k1gInSc1	dostatek
neopatrné	opatrný	k2eNgFnSc2d1	neopatrná
kořisti	kořist	k1gFnSc2	kořist
(	(	kIx(	(
<g/>
rys	rys	k1gInSc1	rys
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
kopytníci	kopytník	k1gMnPc1	kopytník
přemnoženi	přemnožen	k2eAgMnPc1d1	přemnožen
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nezvykli	zvyknout	k5eNaPmAgMnP	zvyknout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
jde	jít	k5eAaImIp3nS	jít
lovit	lovit	k5eAaImF	lovit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pokud	pokud	k8xS	pokud
je	on	k3xPp3gFnPc4	on
kořisti	kořist	k1gFnPc4	kořist
málo	málo	k6eAd1	málo
a	a	k8xC	a
lov	lov	k1gInSc1	lov
je	být	k5eAaImIp3nS	být
namáhavý	namáhavý	k2eAgInSc1d1	namáhavý
<g/>
,	,	kIx,	,
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
zdechliny	zdechlina	k1gFnPc4	zdechlina
<g/>
,	,	kIx,	,
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
kořist	kořist	k1gFnSc4	kořist
jiných	jiný	k2eAgMnPc2d1	jiný
lovců	lovec	k1gMnPc2	lovec
občas	občas	k6eAd1	občas
požírá	požírat	k5eAaImIp3nS	požírat
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
dost	dost	k6eAd1	dost
hustá	hustý	k2eAgFnSc1d1	hustá
populace	populace	k1gFnSc1	populace
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
někdy	někdy	k6eAd1	někdy
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
NP	NP	kA	NP
Šumava	Šumava	k1gFnSc1	Šumava
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
rysy	rys	k1gInPc4	rys
pomocí	pomocí	k7c2	pomocí
vysílaček	vysílačka	k1gFnPc2	vysílačka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
připevňují	připevňovat	k5eAaImIp3nP	připevňovat
na	na	k7c4	na
krk	krk	k1gInSc4	krk
a	a	k8xC	a
potom	potom	k6eAd1	potom
díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
sledují	sledovat	k5eAaImIp3nP	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
rysové	rys	k1gMnPc1	rys
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
je	být	k5eAaImIp3nS	být
rys	rys	k1gInSc1	rys
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
málo	málo	k6eAd1	málo
dotčené	dotčený	k2eAgInPc4d1	dotčený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Bernská	bernský	k2eAgFnSc1d1	Bernská
konvence	konvence	k1gFnSc1	konvence
jej	on	k3xPp3gMnSc4	on
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
mezi	mezi	k7c4	mezi
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směrnici	směrnice	k1gFnSc6	směrnice
92	[number]	k4	92
<g/>
/	/	kIx~	/
<g/>
43	[number]	k4	43
<g/>
/	/	kIx~	/
<g/>
EEC	EEC	kA	EEC
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
druhy	druh	k1gInPc4	druh
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
územní	územní	k2eAgFnSc4d1	územní
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
přísnou	přísný	k2eAgFnSc4d1	přísná
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
CITES	CITES	kA	CITES
je	být	k5eAaImIp3nS	být
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
nelze	lze	k6eNd1	lze
obchodovat	obchodovat	k5eAaImF	obchodovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
rys	rys	k1gInSc1	rys
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
celoročně	celoročně	k6eAd1	celoročně
hájený	hájený	k2eAgInSc1d1	hájený
<g/>
,	,	kIx,	,
případné	případný	k2eAgFnSc2d1	případná
škody	škoda	k1gFnSc2	škoda
jím	jíst	k5eAaImIp1nS	jíst
způsobené	způsobený	k2eAgNnSc1d1	způsobené
hradí	hradit	k5eAaImIp3nS	hradit
stát	stát	k5eAaImF	stát
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
krajské	krajský	k2eAgInPc1d1	krajský
úřady	úřad	k1gInPc1	úřad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
legislativní	legislativní	k2eAgFnSc1d1	legislativní
ochrana	ochrana	k1gFnSc1	ochrana
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
přílohy	příloha	k1gFnSc2	příloha
I	i	k8xC	i
prováděcí	prováděcí	k2eAgFnSc2d1	prováděcí
vyhlášky	vyhláška	k1gFnSc2	vyhláška
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
zákonu	zákon	k1gInSc3	zákon
č.	č.	k?	č.
<g/>
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Rys	Rys	k1gMnSc1	Rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
národním	národní	k2eAgInSc6d1	národní
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Praktickou	praktický	k2eAgFnSc7d1	praktická
ochranou	ochrana	k1gFnSc7	ochrana
rysa	rys	k1gMnSc2	rys
ostrovida	ostrovid	k1gMnSc2	ostrovid
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
nevládní	vládní	k2eNgFnSc2d1	nevládní
organizace	organizace	k1gFnSc2	organizace
Hnutí	hnutí	k1gNnSc2	hnutí
DUHA	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
Beskydčan	Beskydčan	k1gMnSc1	Beskydčan
a	a	k8xC	a
ČSOP	ČSOP	kA	ČSOP
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
účinným	účinný	k2eAgInSc7d1	účinný
prostředkem	prostředek	k1gInSc7	prostředek
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
vlčí	vlčí	k2eAgFnPc4d1	vlčí
hlídky	hlídka	k1gFnPc4	hlídka
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
a	a	k8xC	a
rysí	rysí	k2eAgFnPc4d1	rysí
hlídky	hlídka	k1gFnPc4	hlídka
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgMnPc4d1	přirozený
nepřátele	nepřítel	k1gMnPc4	nepřítel
rys	rys	k1gInSc1	rys
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
přísné	přísný	k2eAgFnSc3d1	přísná
ochraně	ochrana	k1gFnSc3	ochrana
je	být	k5eAaImIp3nS	být
však	však	k9	však
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
pytláctvím	pytláctví	k1gNnSc7	pytláctví
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumy	průzkum	k1gInPc1	průzkum
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
myslivci	myslivec	k1gMnPc7	myslivec
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pytlačení	pytlačení	k1gNnSc4	pytlačení
rysa	rys	k1gMnSc2	rys
nejenže	nejenže	k6eAd1	nejenže
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
řadách	řada	k1gFnPc6	řada
silnou	silný	k2eAgFnSc4d1	silná
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
též	též	k9	též
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
praktikováno	praktikován	k2eAgNnSc1d1	praktikováno
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
upytlačeno	upytlačit	k5eAaPmNgNnS	upytlačit
asi	asi	k9	asi
60	[number]	k4	60
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pytláctví	pytláctví	k1gNnSc1	pytláctví
představuje	představovat	k5eAaImIp3nS	představovat
80	[number]	k4	80
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
mortalitě	mortalita	k1gFnSc6	mortalita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
šumavské	šumavský	k2eAgFnSc2d1	Šumavská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
anonymních	anonymní	k2eAgFnPc2d1	anonymní
anket	anketa	k1gFnPc2	anketa
mezi	mezi	k7c7	mezi
myslivci	myslivec	k1gMnPc7	myslivec
z	z	k7c2	z
daných	daný	k2eAgFnPc2d1	daná
oblastí	oblast	k1gFnPc2	oblast
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
37	[number]	k4	37
%	%	kIx~	%
ví	vědět	k5eAaImIp3nS	vědět
o	o	k7c6	o
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
případě	případ	k1gInSc6	případ
upytlačení	upytlačení	k1gNnSc2	upytlačení
rysa	rys	k1gMnSc2	rys
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
se	se	k3xPyFc4	se
k	k	k7c3	k
pytlačení	pytlačení	k1gNnSc3	pytlačení
na	na	k7c6	na
rysech	rys	k1gInPc6	rys
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
nebyl	být	k5eNaImAgMnS	být
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
ani	ani	k8xC	ani
jediný	jediný	k2eAgMnSc1d1	jediný
pytlák	pytlák	k1gMnSc1	pytlák
<g/>
.	.	kIx.	.
</s>
