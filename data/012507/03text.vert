<p>
<s>
Rumba	rumba	k1gFnSc1	rumba
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc4d1	tradiční
kubánský	kubánský	k2eAgInSc4d1	kubánský
romantický	romantický	k2eAgInSc4d1	romantický
tanec	tanec	k1gInSc4	tanec
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
blízký	blízký	k2eAgInSc1d1	blízký
tanci	tanec	k1gInSc3	tanec
cha-cha	chaha	k1gMnSc1	cha-cha
<g/>
.	.	kIx.	.
</s>
<s>
Partner	partner	k1gMnSc1	partner
s	s	k7c7	s
partnerkou	partnerka	k1gFnSc7	partnerka
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
v	v	k7c6	v
neustálém	neustálý	k2eAgNnSc6d1	neustálé
napětí	napětí	k1gNnSc6	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
rumby	rumba	k1gFnSc2	rumba
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Americká	americký	k2eAgFnSc1d1	americká
rumba	rumba	k1gFnSc1	rumba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Rumba	rumba	k1gFnSc1	rumba
Square	square	k1gInSc1	square
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Mambo	Mamba	k1gFnSc5	Mamba
Bolero	bolero	k1gNnSc5	bolero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
americká	americký	k2eAgFnSc1d1	americká
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
rumba	rumba	k1gFnSc1	rumba
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
tanečních	taneční	k2eAgInPc6d1	taneční
kurzech	kurz	k1gInPc6	kurz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
pohybu	pohyb	k1gInSc6	pohyb
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
po	po	k7c6	po
čtvercové	čtvercový	k2eAgFnSc6d1	čtvercová
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
čtverec	čtverec	k1gInSc1	čtverec
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
takty	takt	k1gInPc4	takt
<g/>
,	,	kIx,	,
v	v	k7c6	v
rytmizaci	rytmizace	k1gFnSc6	rytmizace
volně	volně	k6eAd1	volně
rychle	rychle	k6eAd1	rychle
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
rychle	rychle	k6eAd1	rychle
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
slow	slow	k?	slow
quick	quick	k1gMnSc1	quick
quick	quick	k1gMnSc1	quick
<g/>
,	,	kIx,	,
slow	slow	k?	slow
quick	quick	k1gMnSc1	quick
quick	quick	k1gMnSc1	quick
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
je	být	k5eAaImIp3nS	být
27-33	[number]	k4	27-33
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
ve	v	k7c6	v
čtyřčtvrťovém	čtyřčtvrťový	k2eAgInSc6d1	čtyřčtvrťový
taktu	takt	k1gInSc6	takt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mambo	Mamba	k1gMnSc5	Mamba
Bolero	bolero	k1gNnSc4	bolero
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rumba	rumba	k1gFnSc1	rumba
kubánská	kubánský	k2eAgFnSc1d1	kubánská
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
variantou	varianta	k1gFnSc7	varianta
rumby	rumba	k1gFnSc2	rumba
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
v	v	k7c6	v
pokračovacích	pokračovací	k2eAgFnPc6d1	pokračovací
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc6d2	vyšší
úrovních	úroveň	k1gFnPc6	úroveň
kurzů	kurz	k1gInPc2	kurz
a	a	k8xC	a
tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
cha-cha	chaha	k1gFnSc1	cha-cha
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
cha-cha	chaha	k1gFnSc1	cha-cha
přeměna	přeměna	k1gFnSc1	přeměna
se	se	k3xPyFc4	se
nahradí	nahradit	k5eAaPmIp3nS	nahradit
jedním	jeden	k4xCgInSc7	jeden
krokem	krok	k1gInSc7	krok
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
krok	krok	k1gInSc1	krok
se	se	k3xPyFc4	se
vynechá	vynechat	k5eAaPmIp3nS	vynechat
-	-	kIx~	-
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
taktu	takt	k1gInSc2	takt
tedy	tedy	k9	tedy
pár	pár	k4xCyI	pár
učiní	učinit	k5eAaPmIp3nS	učinit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
kroky	krok	k1gInPc4	krok
na	na	k7c4	na
doby	doba	k1gFnPc4	doba
2,3	[number]	k4	2,3
<g/>
,4	,4	k4	,4
<g/>
,	,	kIx,	,
na	na	k7c4	na
první	první	k4xOgFnSc4	první
dobu	doba	k1gFnSc4	doba
pouze	pouze	k6eAd1	pouze
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
krok	krok	k1gInSc1	krok
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
figur	figura	k1gFnPc2	figura
z	z	k7c2	z
rumby	rumba	k1gFnSc2	rumba
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
cha-cha	chaha	k1gFnSc1	cha-cha
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
Mamba	Mamb	k1gMnSc2	Mamb
bolera	bolero	k1gNnSc2	bolero
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
24-26	[number]	k4	24-26
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
ve	v	k7c6	v
čtyřčtvrťovém	čtyřčtvrťový	k2eAgInSc6d1	čtyřčtvrťový
taktu	takt	k1gInSc6	takt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumba	rumba	k1gFnSc1	rumba
je	být	k5eAaImIp3nS	být
tanec	tanec	k1gInSc1	tanec
poměrně	poměrně	k6eAd1	poměrně
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náročný	náročný	k2eAgInSc1d1	náročný
na	na	k7c4	na
rytmus	rytmus	k1gInSc4	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
a	a	k8xC	a
sedmou	sedmý	k4xOgFnSc4	sedmý
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pomalý	pomalý	k2eAgInSc1d1	pomalý
rytmus	rytmus	k1gInSc1	rytmus
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
</s>
</p>
<p>
<s>
se	s	k7c7	s
střídáním	střídání	k1gNnSc7	střídání
rychlého	rychlý	k2eAgInSc2d1	rychlý
a	a	k8xC	a
pomalého	pomalý	k2eAgInSc2d1	pomalý
pohybu	pohyb	k1gInSc2	pohyb
až	až	k8xS	až
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
pár	pár	k1gInSc1	pár
představuje	představovat	k5eAaImIp3nS	představovat
milostnou	milostný	k2eAgFnSc4d1	milostná
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
namlouvání	namlouvání	k1gNnSc2	namlouvání
a	a	k8xC	a
vášeň	vášeň	k1gFnSc4	vášeň
vztahu	vztah	k1gInSc2	vztah
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Rumbu	rumba	k1gFnSc4	rumba
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pohyb	pohyb	k1gInSc1	pohyb
kyčlí	kyčel	k1gFnPc2	kyčel
<g/>
,	,	kIx,	,
předvádění	předvádění	k1gNnSc1	předvádění
linie	linie	k1gFnSc2	linie
nohou	noha	k1gFnSc7	noha
a	a	k8xC	a
pohyby	pohyb	k1gInPc7	pohyb
paží	paže	k1gFnPc2	paže
podporují	podporovat	k5eAaImIp3nP	podporovat
smyslnost	smyslnost	k1gFnSc4	smyslnost
<g/>
.	.	kIx.	.
</s>
<s>
Rumba	rumba	k1gFnSc1	rumba
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
hluboký	hluboký	k2eAgInSc1d1	hluboký
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prožitek	prožitek	k1gInSc1	prožitek
a	a	k8xC	a
cit	cit	k1gInSc1	cit
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dostupných	dostupný	k2eAgInPc6d1	dostupný
pramenech	pramen	k1gInPc6	pramen
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
verzí	verze	k1gFnSc7	verze
vzniku	vznik	k1gInSc2	vznik
tohoto	tento	k3xDgInSc2	tento
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Rumby	rumba	k1gFnSc2	rumba
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
tanec	tanec	k1gInSc4	tanec
tančený	tančený	k2eAgInSc4d1	tančený
při	při	k7c6	při
lidových	lidový	k2eAgFnPc6d1	lidová
slavnostech	slavnost	k1gFnPc6	slavnost
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
sklizně	sklizeň	k1gFnSc2	sklizeň
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
Rumba	rumba	k1gFnSc1	rumba
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
těchto	tento	k3xDgMnPc2	tento
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Rumba	rumba	k1gFnSc1	rumba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
hýření	hýření	k1gNnSc4	hýření
<g/>
,	,	kIx,	,
směr	směr	k1gInSc4	směr
či	či	k8xC	či
neuspořádanou	uspořádaný	k2eNgFnSc4d1	neuspořádaná
hromadu	hromada	k1gFnSc4	hromada
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
Rumba	rumba	k1gFnSc1	rumba
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
formována	formovat	k5eAaImNgFnS	formovat
vlivy	vliv	k1gInPc7	vliv
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
i	i	k8xC	i
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
Rumba	rumba	k1gFnSc1	rumba
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
až	až	k9	až
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Skutečného	skutečný	k2eAgNnSc2d1	skutečné
rozšíření	rozšíření	k1gNnSc2	rozšíření
ale	ale	k8xC	ale
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Rumba	rumba	k1gFnSc1	rumba
až	až	k9	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
tancovala	tancovat	k5eAaImAgFnS	tancovat
hlavně	hlavně	k9	hlavně
Americká	americký	k2eAgFnSc1d1	americká
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
rumba	rumba	k1gFnSc1	rumba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
tančí	tančit	k5eAaImIp3nS	tančit
kubánská	kubánský	k2eAgFnSc1d1	kubánská
rumba	rumba	k1gFnSc1	rumba
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
krok	krok	k1gInSc1	krok
tancuje	tancovat	k5eAaImIp3nS	tancovat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumba	rumba	k1gFnSc1	rumba
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
latinskoamerické	latinskoamerický	k2eAgInPc4d1	latinskoamerický
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Figury	figura	k1gFnSc2	figura
==	==	k?	==
</s>
</p>
<p>
<s>
Figury	figura	k1gFnPc1	figura
podle	podle	k7c2	podle
ČSTS	ČSTS	kA	ČSTS
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
–	–	k?	–
Basic	Basic	kA	Basic
Movement	Movement	k1gInSc1	Movement
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgInSc1d1	alternativní
základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
–	–	k?	–
Alternative	Alternativ	k1gInSc5	Alternativ
Basic	Basic	kA	Basic
Movement	Movement	k1gInSc1	Movement
</s>
</p>
<p>
<s>
Vějíř	vějíř	k1gInSc1	vějíř
–	–	k?	–
Fan	Fana	k1gFnPc2	Fana
</s>
</p>
<p>
<s>
Alemana	Aleman	k1gMnSc4	Aleman
–	–	k?	–
Alemana	Aleman	k1gMnSc4	Aleman
</s>
</p>
<p>
<s>
Postupová	postupový	k2eAgFnSc1d1	postupová
chůze	chůze	k1gFnSc1	chůze
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
vzad	vzad	k6eAd1	vzad
–	–	k?	–
Progressive	Progressiev	k1gFnSc2	Progressiev
Walks	Walksa	k1gFnPc2	Walksa
Forward	Forward	k1gMnSc1	Forward
and	and	k?	and
Backward	Backward	k1gMnSc1	Backward
</s>
</p>
<p>
<s>
Káča	Káča	k1gFnSc1	Káča
vpravo	vpravo	k6eAd1	vpravo
–	–	k?	–
Natural	Natural	k?	Natural
Top	topit	k5eAaImRp2nS	topit
</s>
</p>
<p>
<s>
Otevření	otevření	k1gNnSc1	otevření
ven	ven	k6eAd1	ven
vpravo	vpravo	k6eAd1	vpravo
–	–	k?	–
Natural	Natural	k?	Natural
Opening	Opening	k1gInSc1	Opening
Out	Out	k1gMnSc1	Out
Movement	Movement	k1gMnSc1	Movement
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
–	–	k?	–
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
Kroky	krok	k1gInPc4	krok
stranou	stranou	k6eAd1	stranou
–	–	k?	–
Side	Side	k1gNnPc1	Side
Steps	Steps	k1gInSc1	Steps
</s>
</p>
<p>
<s>
Kukarači	Kukarač	k1gMnPc1	Kukarač
–	–	k?	–
Cucarachas	Cucarachas	k1gInSc1	Cucarachas
</s>
</p>
<p>
<s>
Hokejka	hokejka	k1gFnSc1	hokejka
–	–	k?	–
Hockey	Hockea	k1gFnSc2	Hockea
Stick	Stick	k1gMnSc1	Stick
</s>
</p>
<p>
<s>
Ruka	ruka	k1gFnSc1	ruka
k	k	k7c3	k
ruce	ruka	k1gFnSc3	ruka
–	–	k?	–
Hand	Hand	k1gInSc1	Hand
to	ten	k3xDgNnSc1	ten
Hand	Hand	k1gInSc1	Hand
</s>
</p>
<p>
<s>
Otáčky	otáčka	k1gFnPc1	otáčka
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
–	–	k?	–
Spot	spot	k1gInSc1	spot
Turns	Turns	k1gInSc1	Turns
</s>
</p>
<p>
<s>
Podtáčky	Podtáček	k1gInPc1	Podtáček
pod	pod	k7c7	pod
rukou	ruka	k1gFnSc7	ruka
–	–	k?	–
Under	Under	k1gInSc4	Under
Arms	Armsa	k1gFnPc2	Armsa
Turns	Turnsa	k1gFnPc2	Turnsa
</s>
</p>
<p>
<s>
Zavřený	zavřený	k2eAgInSc1d1	zavřený
švih	švih	k1gInSc1	švih
kyčle	kyčel	k1gFnSc2	kyčel
–	–	k?	–
Closed	Closed	k1gMnSc1	Closed
Hip	hip	k0	hip
Twist	twist	k1gInSc4	twist
</s>
</p>
<p>
<s>
Káča	Káča	k1gFnSc1	Káča
vlevo	vlevo	k6eAd1	vlevo
–	–	k?	–
Reverse	reverse	k1gFnSc1	reverse
Top	topit	k5eAaImRp2nS	topit
</s>
</p>
<p>
<s>
Otevření	otevření	k1gNnSc1	otevření
ven	ven	k6eAd1	ven
z	z	k7c2	z
káči	káča	k1gFnSc2	káča
vlevo	vlevo	k6eAd1	vlevo
–	–	k?	–
Opening	Opening	k1gInSc4	Opening
Out	Out	k1gFnSc2	Out
from	from	k6eAd1	from
Reverse	reverse	k1gFnSc1	reverse
Top	topit	k5eAaImRp2nS	topit
</s>
</p>
<p>
<s>
Aida	Aida	k1gFnSc1	Aida
–	–	k?	–
Aida	Aida	k1gFnSc1	Aida
</s>
</p>
<p>
<s>
Spirála	spirála	k1gFnSc1	spirála
–	–	k?	–
Spiral	Spiral	k1gFnSc2	Spiral
</s>
</p>
<p>
<s>
Otevřený	otevřený	k2eAgInSc1d1	otevřený
švih	švih	k1gInSc1	švih
kyčle	kyčel	k1gFnSc2	kyčel
–	–	k?	–
Open	Open	k1gMnSc1	Open
Hip	hip	k0	hip
Twist	twist	k1gInSc1	twist
</s>
</p>
<p>
<s>
Pokročilý	pokročilý	k2eAgInSc1d1	pokročilý
švih	švih	k1gInSc1	švih
kyčle	kyčel	k1gFnSc2	kyčel
–	–	k?	–
Advanced	Advanced	k1gMnSc1	Advanced
Hip	hip	k0	hip
Twist	twist	k1gInSc1	twist
</s>
</p>
<p>
<s>
Pokračující	pokračující	k2eAgInSc1d1	pokračující
švih	švih	k1gInSc1	švih
kyčle	kyčel	k1gFnSc2	kyčel
–	–	k?	–
Continuous	Continuous	k1gMnSc1	Continuous
Hip	hip	k0	hip
Twist	twist	k1gInSc4	twist
</s>
</p>
<p>
<s>
Pokračující	pokračující	k2eAgInSc1d1	pokračující
kruhový	kruhový	k2eAgInSc1d1	kruhový
švih	švih	k1gInSc1	švih
kyčle	kyčel	k1gFnSc2	kyčel
–	–	k?	–
Continuous	Continuous	k1gMnSc1	Continuous
Circular	Circular	k1gMnSc1	Circular
Hip	hip	k0	hip
Twist	twist	k1gInSc4	twist
</s>
</p>
<p>
<s>
Postupová	postupový	k2eAgFnSc1d1	postupová
chůze	chůze	k1gFnSc1	chůze
ve	v	k7c6	v
stínovém	stínový	k2eAgNnSc6d1	stínové
postavení	postavení	k1gNnSc6	postavení
–	–	k?	–
Kiki	Kik	k1gFnSc2	Kik
Walks	Walksa	k1gFnPc2	Walksa
</s>
</p>
<p>
<s>
Posuvné	posuvný	k2eAgFnPc1d1	posuvná
dveře	dveře	k1gFnPc1	dveře
–	–	k?	–
Sliding	Sliding	k1gInSc1	Sliding
Doors	Doors	k1gInSc1	Doors
</s>
</p>
<p>
<s>
Laso	laso	k1gNnSc1	laso
–	–	k?	–
Rope	Rop	k1gInSc2	Rop
Spinning	Spinning	k1gInSc1	Spinning
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
trojky	trojka	k1gFnPc1	trojka
–	–	k?	–
Three	Three	k1gNnSc1	Three
Threes	Threes	k1gMnSc1	Threes
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc4	tři
Alemany	Aleman	k1gMnPc4	Aleman
–	–	k?	–
Three	Thre	k1gInSc2	Thre
Alemanas	Alemanasa	k1gFnPc2	Alemanasa
</s>
</p>
<p>
<s>
Kadeř	kadeř	k1gFnSc1	kadeř
–	–	k?	–
Curl	Curl	k1gInSc1	Curl
</s>
</p>
<p>
<s>
Kubánské	kubánský	k2eAgFnSc2d1	kubánská
kolébky	kolébka	k1gFnSc2	kolébka
–	–	k?	–
Cuban	Cuban	k1gInSc1	Cuban
Rocks	Rocks	k1gInSc1	Rocks
</s>
</p>
<p>
<s>
Šerm	šerm	k1gInSc1	šerm
–	–	k?	–
Fencing	Fencing	k1gInSc1	Fencing
</s>
</p>
<p>
<s>
Otevření	otevření	k1gNnSc1	otevření
ven	ven	k6eAd1	ven
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
–	–	k?	–
Opening	Opening	k1gInSc4	Opening
Out	Out	k1gFnSc2	Out
to	ten	k3xDgNnSc4	ten
Right	Right	k1gMnSc1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
Rameno	rameno	k1gNnSc4	rameno
k	k	k7c3	k
rameni	rameno	k1gNnSc3	rameno
–	–	k?	–
Shoulder	Shoulder	k1gInSc1	Shoulder
to	ten	k3xDgNnSc1	ten
Shoulder	Shoulder	k1gInSc4	Shoulder
</s>
</p>
<p>
<s>
Spádná	spádný	k2eAgFnSc1d1	spádná
–	–	k?	–
Fallaway	Fallaway	k1gInPc4	Fallaway
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Taneční	taneční	k2eAgFnSc1d1	taneční
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rumba	rumba	k1gFnSc1	rumba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rumba	rumba	k1gFnSc1	rumba
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
IDSF	IDSF	kA	IDSF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
rada	rada	k1gFnSc1	rada
tanečních	taneční	k2eAgMnPc2d1	taneční
profesionálů	profesionál	k1gMnPc2	profesionál
(	(	kIx(	(
<g/>
WDC	WDC	kA	WDC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rumba	rumba	k1gFnSc1	rumba
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
