<p>
<s>
Strakapoud	strakapoud	k1gMnSc1	strakapoud
jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
Dendrocopos	Dendrocopos	k1gMnSc1	Dendrocopos
syriacus	syriacus	k1gMnSc1	syriacus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
datlovitých	datlovitý	k2eAgMnPc2d1	datlovitý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
strakapoudu	strakapoud	k1gMnSc3	strakapoud
velkému	velký	k2eAgMnSc3d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
se	s	k7c7	s
smíšenými	smíšený	k2eAgInPc7d1	smíšený
páry	pár	k1gInPc7	pár
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
mláďata	mládě	k1gNnPc1	mládě
připomínají	připomínat	k5eAaImIp3nP	připomínat
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k8xC	i
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
oba	dva	k4xCgInPc1	dva
rodičovské	rodičovský	k2eAgInPc1d1	rodičovský
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Černobílý	černobílý	k2eAgMnSc1d1	černobílý
strakatý	strakatý	k2eAgMnSc1d1	strakatý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
strakapouda	strakapoud	k1gMnSc2	strakapoud
velkého	velký	k2eAgMnSc2d1	velký
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
lícemi	líc	k1gFnPc7	líc
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bílém	bílý	k2eAgInSc6d1	bílý
krku	krk	k1gInSc6	krk
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
vous	vous	k1gInSc1	vous
<g/>
,	,	kIx,	,
<g/>
nikoliv	nikoliv	k9	nikoliv
kříž	kříž	k1gInSc4	kříž
jak	jak	k8xS	jak
u	u	k7c2	u
strakapouda	strakapoud	k1gMnSc2	strakapoud
velkého	velký	k2eAgMnSc2d1	velký
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
všechny	všechen	k3xTgInPc1	všechen
červené	červený	k2eAgInPc1d1	červený
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
růžovější	růžový	k2eAgInPc1d2	růžovější
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
strakapoud	strakapoud	k1gMnSc1	strakapoud
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
74-82	[number]	k4	74-82
g	g	kA	g
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
asi	asi	k9	asi
40	[number]	k4	40
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kňouravý	kňouravý	k2eAgInSc4d1	kňouravý
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
ptákem	pták	k1gMnSc7	pták
otevřené	otevřený	k2eAgFnSc2d1	otevřená
krajiny	krajina	k1gFnSc2	krajina
s	s	k7c7	s
vtroušenými	vtroušený	k2eAgInPc7d1	vtroušený
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
obývá	obývat	k5eAaImIp3nS	obývat
okolí	okolí	k1gNnSc1	okolí
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
i	i	k8xC	i
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
původně	původně	k6eAd1	původně
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
Balkánský	balkánský	k2eAgInSc1d1	balkánský
poloostrov	poloostrov	k1gInSc1	poloostrov
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
a	a	k8xC	a
potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
2-3	[number]	k4	2-3
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
vletový	vletový	k2eAgInSc1d1	vletový
otvor	otvor	k1gInSc1	otvor
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
kolem	kolem	k7c2	kolem
45	[number]	k4	45
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
páření	páření	k1gNnSc3	páření
dochází	docházet	k5eAaImIp3nS	docházet
vsedě	vsedě	k6eAd1	vsedě
na	na	k7c6	na
šikmé	šikmý	k2eAgFnSc6d1	šikmá
nebo	nebo	k8xC	nebo
horizontální	horizontální	k2eAgFnSc6d1	horizontální
větvi	větev	k1gFnSc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Snůšku	snůška	k1gFnSc4	snůška
tvoří	tvořit	k5eAaImIp3nS	tvořit
4-7	[number]	k4	4-7
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
partneři	partner	k1gMnPc1	partner
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
vysezení	vysezení	k1gNnSc2	vysezení
je	být	k5eAaImIp3nS	být
12-13	[number]	k4	12-13
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
krmeni	krmit	k5eAaImNgMnP	krmit
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
<g/>
Proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
řídký	řídký	k2eAgInSc4d1	řídký
trus	trus	k1gInSc4	trus
<g/>
,	,	kIx,	,
<g/>
který	který	k3yQgInSc4	který
nelze	lze	k6eNd1	lze
odklidit	odklidit	k5eAaPmF	odklidit
a	a	k8xC	a
tak	tak	k6eAd1	tak
musejí	muset	k5eAaImIp3nP	muset
kálet	kálet	k5eAaImF	kálet
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
<g/>
která	který	k3yQgFnSc1	který
páchne	páchnout	k5eAaImIp3nS	páchnout
<g/>
.	.	kIx.	.
<g/>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vylétnutí	vylétnutí	k1gNnSc6	vylétnutí
si	se	k3xPyFc3	se
mláďata	mládě	k1gNnPc1	mládě
dopřejí	dopřát	k5eAaPmIp3nP	dopřát
vytrvalou	vytrvalý	k2eAgFnSc4d1	vytrvalá
a	a	k8xC	a
důkladnou	důkladný	k2eAgFnSc4d1	důkladná
koupel	koupel	k1gFnSc4	koupel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Sauer	Sauer	k1gMnSc1	Sauer
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Ikar	Ikar	k1gInSc1	Ikar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
strakapoud	strakapoud	k1gMnSc1	strakapoud
jižní	jižní	k2eAgMnSc1d1	jižní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Přírodainfo	Přírodainfo	k1gNnSc1	Přírodainfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Strakapoud	strakapoud	k1gMnSc1	strakapoud
jižní	jižní	k2eAgFnSc2d1	jižní
</s>
</p>
