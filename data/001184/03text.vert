<s>
Titanic	Titanic	k1gInSc1	Titanic
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
americký	americký	k2eAgInSc1d1	americký
velkofilm	velkofilm	k1gInSc1	velkofilm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
11	[number]	k4	11
Oscarů	Oscar	k1gMnPc2	Oscar
včetně	včetně	k7c2	včetně
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
katastrofu	katastrofa	k1gFnSc4	katastrofa
zaoceánské	zaoceánský	k2eAgFnSc2d1	zaoceánská
lodi	loď	k1gFnSc2	loď
Titanic	Titanic	k1gInSc1	Titanic
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejnákladnější	nákladný	k2eAgInPc4d3	nejnákladnější
filmy	film	k1gInPc4	film
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
světové	světový	k2eAgFnSc2d1	světová
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celosvětovými	celosvětový	k2eAgFnPc7d1	celosvětová
tržbami	tržba	k1gFnPc7	tržba
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,8	[number]	k4	1,8
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
uvedení	uvedení	k1gNnSc2	uvedení
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
ve	v	k7c4	v
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
drží	držet	k5eAaImIp3nS	držet
zároveň	zároveň	k6eAd1	zároveň
titul	titul	k1gInSc1	titul
druhého	druhý	k4xOgInSc2	druhý
nejúspěšnějšího	úspěšný	k2eAgInSc2d3	nejúspěšnější
filmu	film	k1gInSc2	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
nejromantičtější	romantický	k2eAgInSc1d3	nejromantičtější
film	film	k1gInSc1	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zápletka	zápletka	k1gFnSc1	zápletka
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
překonávající	překonávající	k2eAgFnPc1d1	překonávající
i	i	k8xC	i
hrůzy	hrůza	k1gFnPc1	hrůza
katastrofy	katastrofa	k1gFnSc2	katastrofa
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
schopni	schopen	k2eAgMnPc1d1	schopen
se	se	k3xPyFc4	se
i	i	k9	i
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
.	.	kIx.	.
</s>
<s>
Výrazná	výrazný	k2eAgFnSc1d1	výrazná
filmová	filmový	k2eAgFnSc1d1	filmová
hudba	hudba	k1gFnSc1	hudba
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
celkový	celkový	k2eAgInSc4d1	celkový
dojem	dojem	k1gInSc4	dojem
a	a	k8xC	a
vyznění	vyznění	k1gNnSc1	vyznění
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
časových	časový	k2eAgNnPc6d1	časové
obdobích	období	k1gNnPc6	období
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
hledač	hledač	k1gInSc4	hledač
pokladů	poklad	k1gInPc2	poklad
Brock	Brock	k1gMnSc1	Brock
Lovett	Lovett	k1gMnSc1	Lovett
hledá	hledat	k5eAaImIp3nS	hledat
ve	v	k7c6	v
vraku	vrak	k1gInSc6	vrak
Titanicu	Titanicus	k1gInSc2	Titanicus
známý	známý	k2eAgInSc1d1	známý
šperk	šperk	k1gInSc1	šperk
Srdce	srdce	k1gNnSc2	srdce
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
Caledona	Caledon	k1gMnSc2	Caledon
Hockleyho	Hockley	k1gMnSc2	Hockley
speciální	speciální	k2eAgFnSc1d1	speciální
průzkumná	průzkumný	k2eAgFnSc1d1	průzkumná
ponorka	ponorka	k1gFnSc1	ponorka
nalezne	naleznout	k5eAaPmIp3nS	naleznout
kresbu	kresba	k1gFnSc4	kresba
nahé	nahý	k2eAgFnSc2d1	nahá
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
nálezu	nález	k1gInSc6	nález
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
vidí	vidět	k5eAaImIp3nP	vidět
stoletá	stoletý	k2eAgFnSc1d1	stoletá
Rose	Rosa	k1gFnSc3	Rosa
Calvertová	Calvertový	k2eAgFnSc1d1	Calvertový
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
vnučkou	vnučka	k1gFnSc7	vnučka
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Lovetta	Lovetta	k1gFnSc1	Lovetta
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
později	pozdě	k6eAd2	pozdě
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prožila	prožít	k5eAaPmAgFnS	prožít
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
Rose	Rosa	k1gFnSc6	Rosa
sedmnáct	sedmnáct	k4xCc4	sedmnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zasnoubená	zasnoubená	k1gFnSc1	zasnoubená
s	s	k7c7	s
bohatým	bohatý	k2eAgMnSc7d1	bohatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesympatickým	sympatický	k2eNgMnSc7d1	nesympatický
Calem	Cal	k1gMnSc7	Cal
Hockleyem	Hockley	k1gMnSc7	Hockley
(	(	kIx(	(
<g/>
Billy	Bill	k1gMnPc7	Bill
Zane	Zan	k1gFnSc2	Zan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
to	ten	k3xDgNnSc4	ten
tak	tak	k6eAd1	tak
chtěla	chtít	k5eAaImAgFnS	chtít
a	a	k8xC	a
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
rodina	rodina	k1gFnSc1	rodina
chudla	chudnout	k5eAaImAgFnS	chudnout
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
nebyla	být	k5eNaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
nic	nic	k6eAd1	nic
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Ros	k1gMnSc4	Ros
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
pronajato	pronajmout	k5eAaPmNgNnS	pronajmout
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejluxusnějších	luxusní	k2eAgNnPc2d3	nejluxusnější
apartmá	apartmá	k1gNnPc2	apartmá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
<g/>
,	,	kIx,	,
cítila	cítit	k5eAaImAgFnS	cítit
prázdnotu	prázdnota	k1gFnSc4	prázdnota
a	a	k8xC	a
nepochopení	nepochopení	k1gNnSc4	nepochopení
a	a	k8xC	a
chtěla	chtít	k5eAaImAgFnS	chtít
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skok	k1gInSc7	skok
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
poznala	poznat	k5eAaPmAgFnS	poznat
chudého	chudý	k1gMnSc4	chudý
mladíka	mladík	k1gMnSc4	mladík
a	a	k8xC	a
malíře	malíř	k1gMnSc4	malíř
Jacka	Jacek	k1gMnSc4	Jacek
Dawsona	Dawson	k1gMnSc4	Dawson
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
lístek	lístek	k1gInSc4	lístek
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
pokeru	poker	k1gInSc6	poker
<g/>
,	,	kIx,	,
rozmyslela	rozmyslet	k5eAaPmAgFnS	rozmyslet
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k6eAd1	Jack
jí	on	k3xPp3gFnSc3	on
pomohl	pomoct	k5eAaPmAgInS	pomoct
překonat	překonat	k5eAaPmF	překonat
duševní	duševní	k2eAgNnSc4d1	duševní
strádání	strádání	k1gNnSc4	strádání
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
společnosti	společnost	k1gFnSc2	společnost
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
získal	získat	k5eAaPmAgMnS	získat
svým	svůj	k3xOyFgInSc7	svůj
humorem	humor	k1gInSc7	humor
a	a	k8xC	a
svobodomyslným	svobodomyslný	k2eAgNnSc7d1	svobodomyslné
smýšlením	smýšlení	k1gNnSc7	smýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
prožívali	prožívat	k5eAaImAgMnP	prožívat
nádherné	nádherný	k2eAgFnPc4d1	nádherná
chvíle	chvíle	k1gFnPc4	chvíle
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
romantiky	romantika	k1gFnSc2	romantika
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
také	také	k9	také
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Rosin	Rosin	k2eAgInSc4d1	Rosin
obrázek	obrázek	k1gInSc4	obrázek
(	(	kIx(	(
<g/>
nalezený	nalezený	k2eAgMnSc1d1	nalezený
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Titanic	Titanic	k1gInSc1	Titanic
narazil	narazit	k5eAaPmAgInS	narazit
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
do	do	k7c2	do
plovoucího	plovoucí	k2eAgInSc2d1	plovoucí
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
zmatek	zmatek	k1gInSc1	zmatek
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc4	jenž
nebyli	být	k5eNaImAgMnP	být
příliš	příliš	k6eAd1	příliš
oblíbení	oblíbený	k2eAgMnPc1d1	oblíbený
jsou	být	k5eAaImIp3nP	být
hrdinové	hrdina	k1gMnPc1	hrdina
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ohromný	ohromný	k2eAgMnSc1d1	ohromný
zbabělec	zbabělec	k1gMnSc1	zbabělec
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
Rosin	Rosin	k2eAgMnSc1d1	Rosin
snoubenec	snoubenec	k1gMnSc1	snoubenec
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
Jacka	Jacek	k1gMnSc4	Jacek
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
potápí	potápět	k5eAaImIp3nS	potápět
<g/>
,	,	kIx,	,
hodí	hodit	k5eAaImIp3nS	hodit
loupež	loupež	k1gFnSc1	loupež
Srdce	srdce	k1gNnSc2	srdce
oceánu	oceán	k1gInSc2	oceán
-	-	kIx~	-
drahokamu	drahokam	k1gInSc2	drahokam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Rose	Rose	k1gMnSc1	Rose
daroval	darovat	k5eAaPmAgMnS	darovat
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
podpalubí	podpalubí	k1gNnSc6	podpalubí
zavřít	zavřít	k5eAaPmF	zavřít
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
by	by	kYmCp3nS	by
býval	bývat	k5eAaImAgMnS	bývat
Jack	Jack	k1gMnSc1	Jack
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
statečná	statečný	k2eAgFnSc1d1	statečná
Rose	Rosa	k1gFnSc6	Rosa
ho	on	k3xPp3gMnSc4	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
záchranného	záchranný	k2eAgInSc2d1	záchranný
člunu	člun	k1gInSc2	člun
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
díky	díky	k7c3	díky
Jackově	Jackův	k2eAgFnSc3d1	Jackova
pohotovosti	pohotovost	k1gFnSc3	pohotovost
podaří	podařit	k5eAaPmIp3nS	podařit
přežít	přežít	k5eAaPmF	přežít
samotné	samotný	k2eAgNnSc4d1	samotné
potopení	potopení	k1gNnSc4	potopení
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
riziko	riziko	k1gNnSc1	riziko
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ledová	ledový	k2eAgFnSc1d1	ledová
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
nemožné	nemožná	k1gFnSc2	nemožná
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
Jack	Jack	k1gMnSc1	Jack
Rose	Rose	k1gMnSc1	Rose
zachrání	zachránit	k5eAaPmIp3nS	zachránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
vysadí	vysadit	k5eAaPmIp3nS	vysadit
na	na	k7c4	na
plovoucí	plovoucí	k2eAgInSc4d1	plovoucí
kus	kus	k1gInSc4	kus
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
mu	on	k3xPp3gNnSc3	on
ale	ale	k9	ale
Rose	Rose	k1gMnSc1	Rose
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
přání	přání	k1gNnSc4	přání
slíbí	slíbit	k5eAaPmIp3nP	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
šťastná	šťastný	k2eAgFnSc1d1	šťastná
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
žít	žít	k5eAaImF	žít
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
pak	pak	k6eAd1	pak
Jack	Jack	k1gMnSc1	Jack
zmizí	zmizet	k5eAaPmIp3nS	zmizet
v	v	k7c6	v
hlubině	hlubina	k1gFnSc6	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
pár	pár	k4xCyI	pár
dalšími	další	k1gNnPc7	další
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
žít	žít	k5eAaImF	žít
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
pod	pod	k7c7	pod
jiným	jiný	k2eAgNnSc7d1	jiné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
jejím	její	k3xOp3gInSc7	její
příběhem	příběh	k1gInSc7	příběh
dojati	dojmout	k5eAaPmNgMnP	dojmout
a	a	k8xC	a
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
loď	loď	k1gFnSc1	loď
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
Rose	Rose	k1gMnSc1	Rose
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
Lovettovy	Lovettův	k2eAgFnSc2d1	Lovettův
lodi	loď	k1gFnSc2	loď
hází	házet	k5eAaImIp3nS	házet
Srdce	srdce	k1gNnSc1	srdce
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
měla	mít	k5eAaImAgFnS	mít
až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ulehá	ulehat	k5eAaImIp3nS	ulehat
do	do	k7c2	do
přidělené	přidělený	k2eAgFnSc2d1	přidělená
kajuty	kajuta	k1gFnSc2	kajuta
a	a	k8xC	a
z	z	k7c2	z
fotek	fotka	k1gFnPc2	fotka
na	na	k7c6	na
polici	police	k1gFnSc6	police
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
Jacku	Jacek	k1gMnSc3	Jacek
Dawsonovi	Dawson	k1gMnSc3	Dawson
splnila-	splnila-	k?	splnila-
žila	žít	k5eAaImAgFnS	žít
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
tak	tak	k8xS	tak
okem	oke	k1gNnSc7	oke
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
příběhu	příběh	k1gInSc2	příběh
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
potopení	potopení	k1gNnSc4	potopení
lodě	loď	k1gFnSc2	loď
Titanic	Titanic	k1gInSc4	Titanic
patřící	patřící	k2eAgFnSc1d1	patřící
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
severního	severní	k2eAgInSc2d1	severní
Atlantiku	Atlantik	k1gInSc2	Atlantik
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
-	-	kIx~	-
Jack	Jack	k1gMnSc1	Jack
Dawson	Dawson	k1gMnSc1	Dawson
Kate	kat	k1gInSc5	kat
Winslet	Winslet	k1gInSc1	Winslet
-	-	kIx~	-
Rose	Rose	k1gMnSc1	Rose
DeWitt	DeWitt	k1gMnSc1	DeWitt
Bukater	Bukater	k1gMnSc1	Bukater
Billy	Bill	k1gMnPc4	Bill
Zane	Zane	k1gFnPc2	Zane
-	-	kIx~	-
Caledon	Caledon	k1gInSc1	Caledon
'	'	kIx"	'
<g/>
Cal	Cal	k1gFnSc1	Cal
<g/>
'	'	kIx"	'
Hockley	Hockle	k2eAgInPc1d1	Hockle
Kathy	Kath	k1gInPc1	Kath
Bates	Batesa	k1gFnPc2	Batesa
-	-	kIx~	-
Margareth	Margareth	k1gInSc1	Margareth
'	'	kIx"	'
<g/>
Molly	Molla	k1gFnPc1	Molla
<g/>
'	'	kIx"	'
Brown	Brown	k1gInSc1	Brown
Bill	Bill	k1gMnSc1	Bill
Paxton	Paxton	k1gInSc1	Paxton
-	-	kIx~	-
Brock	Brocko	k1gNnPc2	Brocko
Lovett	Lovetta	k1gFnPc2	Lovetta
Gloria	Gloria	k1gFnSc1	Gloria
Stuart	Stuart	k1gInSc1	Stuart
-	-	kIx~	-
Rose	Rose	k1gMnSc1	Rose
Dawson	Dawson	k1gMnSc1	Dawson
Calvert	Calvert	k1gMnSc1	Calvert
(	(	kIx(	(
<g/>
stará	starat	k5eAaImIp3nS	starat
Rose	Rose	k1gMnSc1	Rose
<g/>
)	)	kIx)	)
Frances	Frances	k1gMnSc1	Frances
Fisher	Fishra	k1gFnPc2	Fishra
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Ruth	Ruth	k1gFnSc6	Ruth
DeWitt	DeWitt	k2eAgMnSc1d1	DeWitt
Bukater	Bukater	k1gMnSc1	Bukater
Bernard	Bernard	k1gMnSc1	Bernard
Hill	Hill	k1gMnSc1	Hill
-	-	kIx~	-
Kapitán	kapitán	k1gMnSc1	kapitán
Edward	Edward	k1gMnSc1	Edward
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
Jonathan	Jonathan	k1gMnSc1	Jonathan
Hyde	Hyde	k1gFnSc1	Hyde
-	-	kIx~	-
Bruce	Bruce	k1gFnSc1	Bruce
Ismay	Ismaa	k1gFnSc2	Ismaa
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
společnosti	společnost	k1gFnSc2	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
David	David	k1gMnSc1	David
Warner	Warner	k1gMnSc1	Warner
-	-	kIx~	-
Spicer	Spicer	k1gMnSc1	Spicer
Lovejoy	Lovejoa	k1gFnSc2	Lovejoa
Victor	Victor	k1gMnSc1	Victor
Garber	Garber	k1gMnSc1	Garber
-	-	kIx~	-
Thomas	Thomas	k1gMnSc1	Thomas
Andrews	Andrewsa	k1gFnPc2	Andrewsa
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
konstruktér	konstruktér	k1gMnSc1	konstruktér
Danny	Danna	k1gFnSc2	Danna
Nucci	Nucce	k1gFnSc4	Nucce
-	-	kIx~	-
Fabrizio	Fabrizio	k6eAd1	Fabrizio
De	De	k?	De
Rossi	Rosse	k1gFnSc4	Rosse
Lewis	Lewis	k1gFnSc2	Lewis
Abernathy	Abernatha	k1gFnSc2	Abernatha
-	-	kIx~	-
Lewis	Lewis	k1gInSc1	Lewis
Bodine	Bodin	k1gInSc5	Bodin
Suzy	Suzy	k1gInPc4	Suzy
Amis	Amis	k1gInSc1	Amis
-	-	kIx~	-
Lizzy	Lizza	k1gFnSc2	Lizza
Calvert	Calvert	k1gMnSc1	Calvert
Nicholas	Nicholas	k1gMnSc1	Nicholas
Cascone	Cascon	k1gInSc5	Cascon
-	-	kIx~	-
Bobby	Bobba	k1gMnSc2	Bobba
Buell	Buell	k1gMnSc1	Buell
Podle	podle	k7c2	podle
původní	původní	k2eAgFnSc2d1	původní
představy	představa	k1gFnSc2	představa
režiséra	režisér	k1gMnSc2	režisér
Jamese	Jamese	k1gFnSc2	Jamese
Camerona	Camerona	k1gFnSc1	Camerona
měl	mít	k5eAaImAgInS	mít
hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
pár	pár	k1gInSc1	pár
ztvárnit	ztvárnit	k5eAaPmF	ztvárnit
úplně	úplně	k6eAd1	úplně
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Jacka	Jacka	k1gFnSc1	Jacka
Dawsona	Dawsona	k1gFnSc1	Dawsona
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
zahrát	zahrát	k5eAaPmF	zahrát
Matthew	Matthew	k1gFnSc4	Matthew
McConaughey	McConaughea	k1gFnSc2	McConaughea
nebo	nebo	k8xC	nebo
Chris	Chris	k1gFnSc2	Chris
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Donnell	Donnell	k1gInSc4	Donnell
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
potom	potom	k6eAd1	potom
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
herci	herec	k1gMnPc1	herec
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
roli	role	k1gFnSc4	role
příliš	příliš	k6eAd1	příliš
staří	starý	k2eAgMnPc1d1	starý
<g/>
,	,	kIx,	,
a	a	k8xC	a
úlohu	úloha	k1gFnSc4	úloha
svobodomyslného	svobodomyslný	k2eAgMnSc2d1	svobodomyslný
mladíka	mladík	k1gMnSc2	mladík
udělil	udělit	k5eAaPmAgInS	udělit
Leonardu	Leonardo	k1gMnSc3	Leonardo
DiCapriovi	DiCaprius	k1gMnSc3	DiCaprius
<g/>
.	.	kIx.	.
</s>
<s>
Kate	kat	k1gMnSc5	kat
Winslet	Winslet	k1gInSc4	Winslet
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
úlohy	úloha	k1gFnSc2	úloha
již	již	k9	již
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
vžila	vžít	k5eAaPmAgFnS	vžít
a	a	k8xC	a
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
role	role	k1gFnSc2	role
udělala	udělat	k5eAaPmAgFnS	udělat
vše	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Porazila	porazit	k5eAaPmAgFnS	porazit
tak	tak	k6eAd1	tak
své	svůj	k3xOyFgFnPc4	svůj
rivalky	rivalka	k1gFnPc4	rivalka
Gwyneth	Gwynetha	k1gFnPc2	Gwynetha
Paltrow	Paltrow	k1gFnPc4	Paltrow
i	i	k8xC	i
Claire	Clair	k1gInSc5	Clair
Danes	Danes	k1gInSc4	Danes
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
herců	herc	k1gInPc2	herc
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
natočení	natočení	k1gNnSc4	natočení
filmu	film	k1gInSc2	film
nechal	nechat	k5eAaPmAgMnS	nechat
Cameron	Cameron	k1gMnSc1	Cameron
postavit	postavit	k5eAaPmF	postavit
téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
kopii	kopie	k1gFnSc4	kopie
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
detailu	detail	k1gInSc3	detail
věnoval	věnovat	k5eAaPmAgMnS	věnovat
maximální	maximální	k2eAgFnSc4d1	maximální
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
i	i	k9	i
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
nádrž	nádrž	k1gFnSc4	nádrž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
ji	on	k3xPp3gFnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
potopit	potopit	k5eAaPmF	potopit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
podbarvený	podbarvený	k2eAgInSc1d1	podbarvený
filmovou	filmový	k2eAgFnSc7d1	filmová
hudbou	hudba	k1gFnSc7	hudba
Jamese	Jamese	k1gFnSc2	Jamese
Hornera	Horner	k1gMnSc2	Horner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Horner	Horner	k1gMnSc1	Horner
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
soundtracku	soundtrack	k1gInSc2	soundtrack
Back	Back	k1gInSc1	Back
to	ten	k3xDgNnSc4	ten
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
skladba	skladba	k1gFnSc1	skladba
Céline	Célin	k1gInSc5	Célin
Dion	Diona	k1gFnPc2	Diona
My	my	k3xPp1nPc1	my
Heart	Hearta	k1gFnPc2	Hearta
Will	Will	k1gInSc4	Will
Go	Go	k1gFnSc4	Go
On	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
-	-	kIx~	-
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Landau	Landaus	k1gInSc2	Landaus
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
-	-	kIx~	-
James	James	k1gInSc1	James
Cameron	Cameron	k1gInSc1	Cameron
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
-	-	kIx~	-
James	James	k1gMnSc1	James
Horner	Horner	k1gMnSc1	Horner
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
-	-	kIx~	-
Hudba	hudba	k1gFnSc1	hudba
James	James	k1gMnSc1	James
Horner	Horner	k1gMnSc1	Horner
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
Will	Will	k1gInSc1	Will
Jennings	Jennings	k1gInSc4	Jennings
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Heart	Hearta	k1gFnPc2	Hearta
Will	Will	k1gMnSc1	Will
Go	Go	k1gMnSc1	Go
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
-	-	kIx~	-
Russell	Russell	k1gInSc1	Russell
Carpenter	Carpentra	k1gFnPc2	Carpentra
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
výprava	výprava	k1gMnSc1	výprava
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
<g />
.	.	kIx.	.
</s>
<s>
Lamont	Lamont	k1gMnSc1	Lamont
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Ford	ford	k1gInSc1	ford
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
kostýmy	kostým	k1gInPc7	kostým
-	-	kIx~	-
Deborah	Deborah	k1gMnSc1	Deborah
Lynn	Lynn	k1gMnSc1	Lynn
Scott	Scott	k1gMnSc1	Scott
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
-	-	kIx~	-
Robert	Robert	k1gMnSc1	Robert
Legato	legato	k6eAd1	legato
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
A.	A.	kA	A.
Lasoff	Lasoff	k1gMnSc1	Lasoff
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
L.	L.	kA	L.
Fisher	Fishra	k1gFnPc2	Fishra
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Kanfer	Kanfer	k1gMnSc1	Kanfer
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zvuk	zvuk	k1gInSc4	zvuk
-	-	kIx~	-
Gary	Gara	k1gFnSc2	Gara
Rydstrom	Rydstrom	k1gInSc1	Rydstrom
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Gary	Gara	k1gFnPc1	Gara
Summers	Summers	k1gInSc1	Summers
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Ulano	Ulano	k6eAd1	Ulano
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
<g />
.	.	kIx.	.
</s>
<s>
zvuku	zvuk	k1gInSc2	zvuk
-	-	kIx~	-
Tom	Tom	k1gMnSc1	Tom
Bellfort	Bellfort	k1gInSc1	Bellfort
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Boyes	Boyesa	k1gFnPc2	Boyesa
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
-	-	kIx~	-
Conrad	Conrad	k1gInSc1	Conrad
Buff	buffa	k1gFnPc2	buffa
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
A.	A.	kA	A.
Harris	Harris	k1gFnSc1	Harris
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c4	v
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
-	-	kIx~	-
Kate	kat	k1gMnSc5	kat
Winslet	Winslet	k1gInSc1	Winslet
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
-	-	kIx~	-
Gloria	Gloria	k1gFnSc1	Gloria
Stuart	Stuarta	k1gFnPc2	Stuarta
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
masky	maska	k1gFnSc2	maska
-	-	kIx~	-
Tina	Tina	k1gFnSc1	Tina
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Greg	Greg	k1gInSc1	Greg
Cannom	Cannom	k1gInSc1	Cannom
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
Thompson	Thompson	k1gMnSc1	Thompson
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
-	-	kIx~	-
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Landau	Landaus	k1gInSc2	Landaus
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
-	-	kIx~	-
James	James	k1gInSc1	James
Cameron	Cameron	k1gInSc1	Cameron
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
-	-	kIx~	-
James	James	k1gMnSc1	James
Horner	Horner	k1gMnSc1	Horner
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
-	-	kIx~	-
Hudba	hudba	k1gFnSc1	hudba
James	James	k1gMnSc1	James
Horner	Horner	k1gMnSc1	Horner
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
Will	Will	k1gInSc1	Will
Jennings	Jennings	k1gInSc4	Jennings
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Heart	Hearta	k1gFnPc2	Hearta
Will	Will	k1gMnSc1	Will
Go	Go	k1gMnSc1	Go
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
drama	drama	k1gNnSc1	drama
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kate	kat	k1gMnSc5	kat
Winslet	Winslet	k1gInSc1	Winslet
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
(	(	kIx(	(
<g/>
drama	drama	k1gNnSc1	drama
<g/>
)	)	kIx)	)
-	-	kIx~	-
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k6eAd1	DiCaprio
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
-	-	kIx~	-
Gloria	Gloria	k1gFnSc1	Gloria
Stuart	Stuart	k1gInSc4	Stuart
Nejlepší	dobrý	k2eAgInSc1d3	nejlepší
scénář	scénář	k1gInSc1	scénář
-	-	kIx~	-
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
Cena	cena	k1gFnSc1	cena
Davida	David	k1gMnSc2	David
Leana	Lean	k1gMnSc2	Lean
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
Cena	cena	k1gFnSc1	cena
Anthonyho	Anthony	k1gMnSc2	Anthony
Asquitha	Asquith	k1gMnSc2	Asquith
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudbu	hudba	k1gFnSc4	hudba
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kostýmy	kostým	k1gInPc4	kostým
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
masky	maska	k1gFnSc2	maska
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zvuk	zvuk	k1gInSc4	zvuk
Nejlepší	dobrý	k2eAgInPc4d3	nejlepší
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
výprava	výprava	k1gFnSc1	výprava
Divácky	divácky	k6eAd1	divácky
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
film	film	k1gInSc4	film
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
nominace	nominace	k1gFnSc1	nominace
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
filmová	filmový	k2eAgFnSc1d1	filmová
nebo	nebo	k8xC	nebo
televizní	televizní	k2eAgFnSc1d1	televizní
píseň	píseň	k1gFnSc1	píseň
</s>
