<p>
<s>
Historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
Proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
vydal	vydat	k5eAaPmAgMnS	vydat
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Zachytil	zachytit	k5eAaPmAgMnS	zachytit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
proplétající	proplétající	k2eAgInPc1d1	proplétající
osudy	osud	k1gInPc1	osud
několika	několik	k4yIc2	několik
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
významných	významný	k2eAgFnPc2d1	významná
dějinných	dějinný	k2eAgFnPc2d1	dějinná
událostí	událost	k1gFnPc2	událost
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mistrně	mistrně	k6eAd1	mistrně
zde	zde	k6eAd1	zde
zachytil	zachytit	k5eAaPmAgMnS	zachytit
náladu	nálada	k1gFnSc4	nálada
uvedené	uvedený	k2eAgFnSc2d1	uvedená
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
postavy	postava	k1gFnSc2	postava
nechal	nechat	k5eAaPmAgMnS	nechat
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
a	a	k8xC	a
spoluvytvářet	spoluvytvářet	k5eAaImF	spoluvytvářet
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
považované	považovaný	k2eAgFnPc4d1	považovaná
dnes	dnes	k6eAd1	dnes
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
doby	doba	k1gFnSc2	doba
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
díla	dílo	k1gNnSc2	dílo
má	mít	k5eAaImIp3nS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
boj	boj	k1gInSc4	boj
husitů	husita	k1gMnPc2	husita
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
jejich	jejich	k3xOp3gMnPc3	jejich
odpůrcům	odpůrce	k1gMnPc3	odpůrce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
psána	psán	k2eAgFnSc1d1	psána
velmi	velmi	k6eAd1	velmi
epickým	epický	k2eAgInSc7d1	epický
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
autora	autor	k1gMnSc2	autor
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
vtěsnat	vtěsnat	k5eAaPmF	vtěsnat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
maximální	maximální	k2eAgInSc1d1	maximální
množství	množství	k1gNnSc4	množství
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Popisy	popis	k1gInPc1	popis
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
prostředí	prostředí	k1gNnSc2	prostředí
mají	mít	k5eAaImIp3nP	mít
podtrhnout	podtrhnout	k5eAaPmF	podtrhnout
atmosféru	atmosféra	k1gFnSc4	atmosféra
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
husitských	husitský	k2eAgFnPc2d1	husitská
písní	píseň	k1gFnPc2	píseň
vkládané	vkládaný	k2eAgMnPc4d1	vkládaný
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
vystupujícím	vystupující	k2eAgFnPc3d1	vystupující
postavám	postava	k1gFnPc3	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dějinné	dějinný	k2eAgFnPc1d1	dějinná
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
popisovány	popisován	k2eAgInPc1d1	popisován
hodnověrným	hodnověrný	k2eAgInSc7d1	hodnověrný
a	a	k8xC	a
vyrovnaným	vyrovnaný	k2eAgInSc7d1	vyrovnaný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
dobří	dobrý	k2eAgMnPc1d1	dobrý
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
jim	on	k3xPp3gInPc3	on
pomáhající	pomáhající	k2eAgInSc1d1	pomáhající
národy	národ	k1gInPc7	národ
ti	ten	k3xDgMnPc1	ten
špatní	špatný	k2eAgMnPc1d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
toho	ten	k3xDgNnSc2	ten
dělení	dělení	k1gNnSc2	dělení
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
podnítit	podnítit	k5eAaPmF	podnítit
národní	národní	k2eAgFnSc4d1	národní
hrdost	hrdost	k1gFnSc4	hrdost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ani	ani	k8xC	ani
husité	husita	k1gMnPc1	husita
nejsou	být	k5eNaImIp3nP	být
idealizováni	idealizovat	k5eAaBmNgMnP	idealizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
otevřeně	otevřeně	k6eAd1	otevřeně
zmíněno	zmíněn	k2eAgNnSc1d1	zmíněno
jejich	jejich	k3xOp3gNnSc1	jejich
loupení	loupení	k1gNnSc1	loupení
a	a	k8xC	a
drancování	drancování	k1gNnSc1	drancování
zvláště	zvláště	k6eAd1	zvláště
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odmítaly	odmítat	k5eAaImAgFnP	odmítat
přijímat	přijímat	k5eAaImF	přijímat
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
tři	tři	k4xCgFnPc1	tři
významné	významný	k2eAgFnPc1d1	významná
historické	historický	k2eAgFnPc1d1	historická
události	událost	k1gFnPc1	událost
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Skonání	skonání	k1gNnSc1	skonání
věku	věk	k1gInSc2	věk
–	–	k?	–
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
období	období	k1gNnSc4	období
budování	budování	k1gNnSc2	budování
a	a	k8xC	a
stavby	stavba	k1gFnSc2	stavba
husitského	husitský	k2eAgNnSc2d1	husitské
města	město	k1gNnSc2	město
Tábora	Tábor	k1gInSc2	Tábor
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
majetkem	majetek	k1gInSc7	majetek
ukládaným	ukládaný	k2eAgInSc7d1	ukládaný
do	do	k7c2	do
kádí	káď	k1gFnPc2	káď
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc2	budování
jeho	jeho	k3xOp3gNnPc2	jeho
opevnění	opevnění	k1gNnPc2	opevnění
i	i	k8xC	i
domů	dům	k1gInPc2	dům
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
osidlování	osidlování	k1gNnSc4	osidlování
města	město	k1gNnSc2	město
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
často	často	k6eAd1	často
opouští	opouštět	k5eAaImIp3nP	opouštět
své	svůj	k3xOyFgFnPc4	svůj
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
pálí	pálit	k5eAaImIp3nS	pálit
své	svůj	k3xOyFgFnPc4	svůj
chalupy	chalupa	k1gFnPc4	chalupa
<g/>
,	,	kIx,	,
berou	brát	k5eAaImIp3nP	brát
svůj	svůj	k3xOyFgInSc4	svůj
chudý	chudý	k2eAgInSc4d1	chudý
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
vrchnost	vrchnost	k1gFnSc1	vrchnost
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
bratry	bratr	k1gMnPc7	bratr
a	a	k8xC	a
sestrami	sestra	k1gFnPc7	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kruciata	Kruciata	k1gFnSc1	Kruciata
–	–	k?	–
popisuje	popisovat	k5eAaImIp3nS	popisovat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
do	do	k7c2	do
kacířských	kacířský	k2eAgFnPc2d1	kacířská
Čech	Čechy	k1gFnPc2	Čechy
přes	přes	k7c4	přes
Hradec	Hradec	k1gInSc4	Hradec
<g/>
,	,	kIx,	,
Kutnou	kutný	k2eAgFnSc4d1	Kutná
Horu	hora	k1gFnSc4	hora
až	až	k9	až
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
loupení	loupení	k1gNnSc3	loupení
<g/>
,	,	kIx,	,
drancování	drancování	k1gNnSc3	drancování
a	a	k8xC	a
vypalování	vypalování	k1gNnSc4	vypalování
vesnic	vesnice	k1gFnPc2	vesnice
s	s	k7c7	s
českým	český	k2eAgNnSc7d1	české
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
a	a	k8xC	a
boje	boj	k1gInPc1	boj
císařových	císařův	k2eAgMnPc2d1	císařův
křižáků	křižák	k1gMnPc2	křižák
a	a	k8xC	a
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
s	s	k7c7	s
husity	husita	k1gMnPc7	husita
a	a	k8xC	a
Pražany	Pražan	k1gMnPc7	Pražan
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
boje	boj	k1gInPc4	boj
o	o	k7c4	o
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
památnou	památný	k2eAgFnSc4d1	památná
bitvu	bitva	k1gFnSc4	bitva
na	na	k7c6	na
Vítkově	Vítkův	k2eAgFnSc6d1	Vítkova
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
naprostým	naprostý	k2eAgInSc7d1	naprostý
Zikmundovým	Zikmundův	k2eAgInSc7d1	Zikmundův
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
i	i	k9	i
tažení	tažení	k1gNnSc1	tažení
husitů	husita	k1gMnPc2	husita
od	od	k7c2	od
Tábora	Tábor	k1gInSc2	Tábor
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Praze	Praha	k1gFnSc3	Praha
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
boje	boj	k1gInPc1	boj
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boží	boží	k2eAgInSc1d1	boží
zástup	zástup	k1gInSc1	zástup
–	–	k?	–
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
sekta	sekta	k1gFnSc1	sekta
Adamitů	adamita	k1gMnPc2	adamita
hlásajících	hlásající	k2eAgInPc2d1	hlásající
naprosté	naprostý	k2eAgNnSc4d1	naprosté
odloučení	odloučení	k1gNnSc4	odloučení
od	od	k7c2	od
všeho	všecek	k3xTgInSc2	všecek
světského	světský	k2eAgInSc2d1	světský
a	a	k8xC	a
pouhé	pouhý	k2eAgNnSc4d1	pouhé
čekání	čekání	k1gNnSc4	čekání
na	na	k7c4	na
vykoupení	vykoupení	k1gNnSc4	vykoupení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
již	již	k6eAd1	již
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zobrazeno	zobrazen	k2eAgNnSc1d1	zobrazeno
jejich	jejich	k3xOp3gNnSc1	jejich
vyhnání	vyhnání	k1gNnSc1	vyhnání
z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
umístění	umístění	k1gNnSc2	umístění
na	na	k7c4	na
nedaleký	daleký	k2eNgInSc4d1	nedaleký
hrad	hrad	k1gInSc4	hrad
Příběnice	Příběnice	k1gFnSc2	Příběnice
<g/>
.	.	kIx.	.
<g/>
Román	román	k1gInSc1	román
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
trilogii	trilogie	k1gFnSc4	trilogie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
menšímu	malý	k2eAgInSc3d2	menší
rozsahu	rozsah	k1gInSc3	rozsah
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
bývá	bývat	k5eAaImIp3nS	bývat
vydáván	vydávat	k5eAaImNgInS	vydávat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Probošt	probošt	k1gMnSc1	probošt
vypáleného	vypálený	k2eAgInSc2d1	vypálený
Louňovického	Louňovický	k2eAgInSc2d1	Louňovický
kláštera	klášter	k1gInSc2	klášter
společně	společně	k6eAd1	společně
s	s	k7c7	s
novickou	novicka	k1gFnSc7	novicka
Martou	Marta	k1gFnSc7	Marta
a	a	k8xC	a
sakristiánem	sakristián	k1gMnSc7	sakristián
prchá	prchat	k5eAaImIp3nS	prchat
před	před	k7c4	před
oddíly	oddíl	k1gInPc4	oddíl
husitů	husita	k1gMnPc2	husita
putujícími	putující	k2eAgFnPc7d1	putující
jižními	jižní	k2eAgFnPc7d1	jižní
Čechami	Čechy	k1gFnPc7	Čechy
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
je	on	k3xPp3gMnPc4	on
chytili	chytit	k5eAaPmAgMnP	chytit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k8xC	jako
odpůrce	odpůrce	k1gMnSc1	odpůrce
přijímání	přijímání	k1gNnSc4	přijímání
pod	pod	k7c4	pod
obojí	obojí	k4xRgInSc4	obojí
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
čekala	čekat	k5eAaImAgFnS	čekat
pouze	pouze	k6eAd1	pouze
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vesnice	vesnice	k1gFnSc2	vesnice
Bukovska	Bukovsko	k1gNnSc2	Bukovsko
se	se	k3xPyFc4	se
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
takovým	takový	k3xDgInSc7	takový
oddílem	oddíl	k1gInSc7	oddíl
setkají	setkat	k5eAaPmIp3nP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ale	ale	k8xC	ale
prchnout	prchnout	k5eAaPmF	prchnout
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
vsi	ves	k1gFnSc2	ves
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
najdou	najít	k5eAaPmIp3nP	najít
útočiště	útočiště	k1gNnSc4	útočiště
na	na	k7c6	na
statku	statek	k1gInSc6	statek
zemana	zeman	k1gMnSc2	zeman
Ctibora	Ctibor	k1gMnSc2	Ctibor
<g/>
.	.	kIx.	.
</s>
<s>
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
je	být	k5eAaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gInSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
táborských	táborští	k1gMnPc2	táborští
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
u	u	k7c2	u
něj	on	k3xPp3gMnSc4	on
probošta	probošt	k1gMnSc4	probošt
nalezli	nalézt	k5eAaBmAgMnP	nalézt
<g/>
,	,	kIx,	,
dopřeje	dopřát	k5eAaPmIp3nS	dopřát
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
odpočinek	odpočinek	k1gInSc4	odpočinek
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
při	při	k7c6	při
obhlídce	obhlídka	k1gFnSc6	obhlídka
sousedního	sousední	k2eAgNnSc2d1	sousední
Bukovska	Bukovsko	k1gNnSc2	Bukovsko
je	být	k5eAaImIp3nS	být
svědkem	svědek	k1gMnSc7	svědek
vypálení	vypálení	k1gNnSc2	vypálení
a	a	k8xC	a
zničení	zničení	k1gNnSc2	zničení
tamního	tamní	k2eAgInSc2d1	tamní
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
dobytí	dobytí	k1gNnSc4	dobytí
tvrze	tvrz	k1gFnSc2	tvrz
táborskými	táborský	k2eAgInPc7d1	táborský
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
pak	pak	k6eAd1	pak
odchází	odcházet	k5eAaImIp3nS	odcházet
i	i	k9	i
většina	většina	k1gFnSc1	většina
vesničanů	vesničan	k1gMnPc2	vesničan
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
Tábora	Tábor	k1gInSc2	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
zemanova	zemanův	k2eAgFnSc1d1	zemanova
dcera	dcera	k1gFnSc1	dcera
Zdena	Zdena	k1gFnSc1	Zdena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nadšena	nadchnout	k5eAaPmNgFnS	nadchnout
náboženstvím	náboženství	k1gNnSc7	náboženství
podle	podle	k7c2	podle
výkladu	výklad	k1gInSc2	výklad
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
následovníků	následovník	k1gMnPc2	následovník
i	i	k8xC	i
ideálem	ideál	k1gInSc7	ideál
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
rovni	roveň	k1gMnPc1	roveň
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
probošt	probošt	k1gMnSc1	probošt
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
uprchlíci	uprchlík	k1gMnPc1	uprchlík
postranními	postranní	k2eAgFnPc7d1	postranní
cestami	cesta	k1gFnPc7	cesta
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
zemanova	zemanův	k2eAgMnSc2d1	zemanův
pacholka	pacholek	k1gMnSc2	pacholek
a	a	k8xC	a
synovce	synovec	k1gMnSc2	synovec
Ondřeje	Ondřej	k1gMnSc2	Ondřej
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
radonické	radonický	k2eAgFnSc2d1	radonický
tvrze	tvrz	k1gFnSc2	tvrz
patřící	patřící	k2eAgFnSc2d1	patřící
Oldřichu	Oldřich	k1gMnSc3	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
pevný	pevný	k2eAgInSc4d1	pevný
hrad	hrad	k1gInSc4	hrad
Příběnice	Příběnice	k1gFnSc2	Příběnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
se	se	k3xPyFc4	se
Zdena	Zdena	k1gFnSc1	Zdena
ihned	ihned	k6eAd1	ihned
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
aktivně	aktivně	k6eAd1	aktivně
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
špitále	špitál	k1gInSc6	špitál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
raněné	raněný	k1gMnPc4	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
knězem	kněz	k1gMnSc7	kněz
Bydlínským	Bydlínský	k2eAgMnSc7d1	Bydlínský
a	a	k8xC	a
knězem	kněz	k1gMnSc7	kněz
Kánišem	Kániš	k1gMnSc7	Kániš
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dochází	docházet	k5eAaImIp3nP	docházet
do	do	k7c2	do
obydlí	obydlí	k1gNnSc2	obydlí
k	k	k7c3	k
Pytlům	pytel	k1gInPc3	pytel
a	a	k8xC	a
tam	tam	k6eAd1	tam
aktivně	aktivně	k6eAd1	aktivně
káží	kážit	k5eAaImIp3nS	kážit
proti	proti	k7c3	proti
všemu	všecek	k3xTgNnSc3	všecek
světskému	světský	k2eAgNnSc3d1	světské
a	a	k8xC	a
proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
církevním	církevní	k2eAgFnPc3d1	církevní
modlám	modla	k1gFnPc3	modla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
kázání	kázání	k1gNnSc6	kázání
ale	ale	k9	ale
zaznívají	zaznívat	k5eAaImIp3nP	zaznívat
rozpory	rozpor	k1gInPc4	rozpor
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
táborských	táborský	k2eAgMnPc2d1	táborský
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odmítají	odmítat	k5eAaImIp3nP	odmítat
jít	jít	k5eAaImF	jít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
táhne	táhnout	k5eAaImIp3nS	táhnout
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
s	s	k7c7	s
početným	početný	k2eAgNnSc7d1	početné
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Tábora	Tábor	k1gInSc2	Tábor
nakonec	nakonec	k6eAd1	nakonec
přichází	přicházet	k5eAaImIp3nS	přicházet
i	i	k9	i
zeman	zeman	k1gMnSc1	zeman
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
i	i	k9	i
s	s	k7c7	s
Ondřejem	Ondřej	k1gMnSc7	Ondřej
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
spíše	spíše	k9	spíše
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
nablízku	nablízku	k6eAd1	nablízku
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
potkají	potkat	k5eAaPmIp3nP	potkat
zachráněného	zachráněný	k2eAgMnSc4d1	zachráněný
probošta	probošt	k1gMnSc4	probošt
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
vlastnící	vlastnící	k2eAgNnSc1d1	vlastnící
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
císaři	císař	k1gMnSc3	císař
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
jako	jako	k8xS	jako
posla	posel	k1gMnSc2	posel
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
rozmáhajícím	rozmáhající	k2eAgInPc3d1	rozmáhající
se	se	k3xPyFc4	se
táborským	táborský	k2eAgMnSc7d1	táborský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Probošt	probošt	k1gMnSc1	probošt
jako	jako	k8xS	jako
posel	posel	k1gMnSc1	posel
cestuje	cestovat	k5eAaImIp3nS	cestovat
za	za	k7c7	za
postupujícími	postupující	k2eAgFnPc7d1	postupující
Zikmundovými	Zikmundová	k1gFnPc7	Zikmundová
vojsky	vojsky	k6eAd1	vojsky
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgInS	uniknout
hrozbě	hrozba	k1gFnSc3	hrozba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vesničanů	vesničan	k1gMnPc2	vesničan
podporujících	podporující	k2eAgMnPc2d1	podporující
táborské	táborský	k2eAgInPc4d1	táborský
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
duchovním	duchovní	k2eAgInSc6d1	duchovní
oděvu	oděv	k1gInSc6	oděv
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgMnS	uniknout
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
křižáků	křižák	k1gInPc2	křižák
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
svědkem	svědek	k1gMnSc7	svědek
řádění	řádění	k1gNnSc2	řádění
cizích	cizí	k2eAgNnPc2d1	cizí
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
bezbranné	bezbranný	k2eAgFnPc4d1	bezbranná
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
pálí	pálit	k5eAaImIp3nS	pálit
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Probošt	probošt	k1gMnSc1	probošt
se	se	k3xPyFc4	se
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Zikmundem	Zikmund	k1gMnSc7	Zikmund
setkává	setkávat	k5eAaImIp3nS	setkávat
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ochotně	ochotně	k6eAd1	ochotně
jako	jako	k9	jako
posel	posít	k5eAaPmAgInS	posít
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
společně	společně	k6eAd1	společně
domluvili	domluvit	k5eAaPmAgMnP	domluvit
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc4	který
Zikmund	Zikmund	k1gMnSc1	Zikmund
napadne	napadnout	k5eAaPmIp3nS	napadnout
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
napadne	napadnout	k5eAaPmIp3nS	napadnout
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
přichází	přicházet	k5eAaImIp3nS	přicházet
k	k	k7c3	k
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
i	i	k9	i
poselstvo	poselstvo	k1gNnSc4	poselstvo
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
snažící	snažící	k2eAgFnSc2d1	snažící
se	se	k3xPyFc4	se
odvrátit	odvrátit	k5eAaPmF	odvrátit
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poslové	posel	k1gMnPc1	posel
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
císařem	císař	k1gMnSc7	císař
ponižováni	ponižován	k2eAgMnPc1d1	ponižován
<g/>
,	,	kIx,	,
uráženi	urážen	k2eAgMnPc1d1	urážen
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
jim	on	k3xPp3gFnPc3	on
diktuje	diktovat	k5eAaImIp3nS	diktovat
nepřijatelné	přijatelný	k2eNgFnPc4d1	nepřijatelná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Poslové	posel	k1gMnPc1	posel
se	se	k3xPyFc4	se
s	s	k7c7	s
nepořízenou	nepořízená	k1gFnSc7	nepořízená
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
kněží	kněz	k1gMnPc2	kněz
Kániše	Kániš	k1gInSc2	Kániš
s	s	k7c7	s
Bydlínským	Bydlínský	k2eAgMnSc7d1	Bydlínský
se	se	k3xPyFc4	se
vypraví	vypravit	k5eAaPmIp3nS	vypravit
z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
Žižka	Žižka	k1gMnSc1	Žižka
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
i	i	k9	i
Ondřej	Ondřej	k1gMnSc1	Ondřej
v	v	k7c6	v
předním	přední	k2eAgInSc6d1	přední
jízdním	jízdní	k2eAgInSc6d1	jízdní
oddíle	oddíl	k1gInSc6	oddíl
a	a	k8xC	a
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
v	v	k7c6	v
zadním	zadní	k2eAgInSc6d1	zadní
záložním	záložní	k2eAgInSc6d1	záložní
jízdním	jízdní	k2eAgInSc6d1	jízdní
oddíle	oddíl	k1gInSc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
účastní	účastnit	k5eAaImIp3nS	účastnit
dobývání	dobývání	k1gNnSc4	dobývání
Benešova	Benešův	k2eAgInSc2d1	Benešův
i	i	k8xC	i
následného	následný	k2eAgInSc2d1	následný
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
křižáky	křižák	k1gMnPc7	křižák
u	u	k7c2	u
brodu	brod	k1gInSc2	brod
přes	přes	k7c4	přes
Sázavu	Sázava	k1gFnSc4	Sázava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
raněn	ranit	k5eAaPmNgMnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Žižkovým	Žižkův	k2eAgNnSc7d1	Žižkovo
vojskem	vojsko	k1gNnSc7	vojsko
dojde	dojít	k5eAaPmIp3nS	dojít
až	až	k9	až
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nehojící	hojící	k2eNgFnSc1d1	nehojící
se	se	k3xPyFc4	se
zranění	zranění	k1gNnPc2	zranění
ošetřováno	ošetřovat	k5eAaImNgNnS	ošetřovat
místním	místní	k2eAgMnSc7d1	místní
ranhojičem	ranhojič	k1gMnSc7	ranhojič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
neustále	neustále	k6eAd1	neustále
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Zikmundova	Zikmundův	k2eAgNnPc1d1	Zikmundovo
vojska	vojsko	k1gNnPc1	vojsko
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
blíží	blížit	k5eAaImIp3nS	blížit
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
vojsko	vojsko	k1gNnSc1	vojsko
obklíčí	obklíčit	k5eAaPmIp3nS	obklíčit
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
posádka	posádka	k1gFnSc1	posádka
vzdala	vzdát	k5eAaPmAgFnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Ježe	Jež	k1gMnSc4	Jež
nepříteli	nepřítel	k1gMnSc3	nepřítel
se	s	k7c7	s
lstí	lest	k1gFnSc7	lest
podaří	podařit	k5eAaPmIp3nS	podařit
dovézt	dovézt	k5eAaPmF	dovézt
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pomine	pominout	k5eAaPmIp3nS	pominout
smysl	smysl	k1gInSc1	smysl
obléhání	obléhání	k1gNnSc2	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Táborští	Táborský	k1gMnPc1	Táborský
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dojdou	dojít	k5eAaPmIp3nP	dojít
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
obklíčil	obklíčit	k5eAaPmAgMnS	obklíčit
město	město	k1gNnSc4	město
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
vysílá	vysílat	k5eAaImIp3nS	vysílat
svoji	svůj	k3xOyFgFnSc4	svůj
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
jezdci	jezdec	k1gMnPc1	jezdec
překvapí	překvapit	k5eAaPmIp3nS	překvapit
jednoho	jeden	k4xCgMnSc2	jeden
rána	ráno	k1gNnSc2	ráno
obléhatele	obléhatel	k1gMnSc2	obléhatel
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
rozpráší	rozprášit	k5eAaPmIp3nP	rozprášit
vojska	vojsko	k1gNnPc1	vojsko
Oldřicha	Oldřich	k1gMnSc2	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
boji	boj	k1gInSc6	boj
je	být	k5eAaImIp3nS	být
však	však	k9	však
těžce	těžce	k6eAd1	těžce
raněn	raněn	k2eAgMnSc1d1	raněn
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
a	a	k8xC	a
upálen	upálen	k2eAgMnSc1d1	upálen
louňovický	louňovický	k2eAgMnSc1d1	louňovický
probošt	probošt	k1gMnSc1	probošt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ctibor	Ctibor	k1gMnSc1	Ctibor
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
špitálu	špitál	k1gInSc2	špitál
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
pečuje	pečovat	k5eAaImIp3nS	pečovat
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Zdena	Zdena	k1gFnSc1	Zdena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
dovídá	dovídat	k5eAaImIp3nS	dovídat
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
rozkol	rozkol	k1gInSc1	rozkol
mezi	mezi	k7c7	mezi
kněžími	kněz	k1gMnPc7	kněz
se	se	k3xPyFc4	se
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
děje	děj	k1gInSc2	děj
a	a	k8xC	a
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
manželství	manželství	k1gNnSc4	manželství
knězi	kněz	k1gMnSc3	kněz
Bydlínskému	Bydlínský	k2eAgMnSc3d1	Bydlínský
<g/>
.	.	kIx.	.
</s>
<s>
Ctibor	Ctibor	k1gMnSc1	Ctibor
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zřekne	zřeknout	k5eAaPmIp3nS	zřeknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
učením	učení	k1gNnSc7	učení
kněží	kněz	k1gMnPc2	kněz
Kániše	Kániš	k1gMnSc2	Kániš
a	a	k8xC	a
Bydlínského	Bydlínský	k2eAgNnSc2d1	Bydlínský
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozpor	rozpor	k1gInSc1	rozpor
způsobený	způsobený	k2eAgInSc1d1	způsobený
žárlivostí	žárlivost	k1gFnSc7	žárlivost
Kániše	Kániše	k1gFnPc4	Kániše
kvůli	kvůli	k7c3	kvůli
Zdeně	Zdena	k1gFnSc3	Zdena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
zatím	zatím	k6eAd1	zatím
spěchá	spěchat	k5eAaImIp3nS	spěchat
s	s	k7c7	s
ostatní	ostatní	k2eAgFnSc7d1	ostatní
jízdou	jízda	k1gFnSc7	jízda
zpět	zpět	k6eAd1	zpět
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
již	již	k6eAd1	již
mezitím	mezitím	k6eAd1	mezitím
dorazil	dorazit	k5eAaPmAgMnS	dorazit
i	i	k9	i
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
hlavním	hlavní	k2eAgNnSc7d1	hlavní
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Táborští	Táborský	k1gMnPc1	Táborský
staví	stavit	k5eAaPmIp3nP	stavit
opevnění	opevnění	k1gNnSc4	opevnění
na	na	k7c6	na
Vítkově	Vítkův	k2eAgFnSc6d1	Vítkova
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
Praha	Praha	k1gFnSc1	Praha
není	být	k5eNaImIp3nS	být
obklíčena	obklíčit	k5eAaPmNgFnS	obklíčit
císařským	císařský	k2eAgNnSc7d1	císařské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jízdní	jízdní	k2eAgMnSc1d1	jízdní
posel	posít	k5eAaPmAgMnS	posít
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
oddíly	oddíl	k1gInPc7	oddíl
Táborských	Táborská	k1gFnPc2	Táborská
a	a	k8xC	a
Pražanů	Pražan	k1gMnPc2	Pražan
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
rána	ráno	k1gNnPc4	ráno
početná	početný	k2eAgNnPc4d1	početné
vojska	vojsko	k1gNnPc4	vojsko
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
přebrodí	přebrodit	k5eAaPmIp3nS	přebrodit
Vltavu	Vltava	k1gFnSc4	Vltava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c6	na
Žižkou	Žižka	k1gMnSc7	Žižka
obsazený	obsazený	k2eAgInSc1d1	obsazený
Vítkov	Vítkov	k1gInSc4	Vítkov
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Táborští	Táborský	k1gMnPc1	Táborský
zdatně	zdatně	k6eAd1	zdatně
odolají	odolat	k5eAaPmIp3nP	odolat
v	v	k7c6	v
opevnění	opevnění	k1gNnSc6	opevnění
na	na	k7c6	na
Vítkově	Vítkův	k2eAgFnSc6d1	Vítkova
hoře	hora	k1gFnSc6	hora
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pěšího	pěší	k2eAgInSc2d1	pěší
oddílu	oddíl	k1gInSc2	oddíl
Pražanů	Pražan	k1gMnPc2	Pražan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jim	on	k3xPp3gMnPc3	on
včas	včas	k6eAd1	včas
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
výpadu	výpad	k1gInSc3	výpad
jízdy	jízda	k1gFnSc2	jízda
na	na	k7c4	na
oslabená	oslabený	k2eAgNnPc4d1	oslabené
vojska	vojsko	k1gNnPc4	vojsko
císaře	císař	k1gMnSc2	císař
na	na	k7c6	na
Špitálském	špitálský	k2eAgNnSc6d1	Špitálské
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
ženou	žena	k1gFnSc7	žena
císařova	císařův	k2eAgNnSc2d1	císařovo
vojska	vojsko	k1gNnSc2	vojsko
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
slavného	slavný	k2eAgNnSc2d1	slavné
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
bitvě	bitva	k1gFnSc6	bitva
se	se	k3xPyFc4	se
Ondřej	Ondřej	k1gMnSc1	Ondřej
s	s	k7c7	s
ostatním	ostatní	k2eAgNnSc7d1	ostatní
vojskem	vojsko	k1gNnSc7	vojsko
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
se	se	k3xPyFc4	se
uzdravuje	uzdravovat	k5eAaImIp3nS	uzdravovat
<g/>
.	.	kIx.	.
</s>
<s>
Novicka	novicka	k1gFnSc1	novicka
Marta	Marta	k1gFnSc1	Marta
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
za	za	k7c7	za
zdmi	zeď	k1gFnPc7	zeď
hradu	hrad	k1gInSc2	hrad
Příběnice	Příběnice	k1gFnSc2	Příběnice
často	často	k6eAd1	často
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
setkání	setkání	k1gNnSc6	setkání
kdysi	kdysi	k6eAd1	kdysi
s	s	k7c7	s
Ondřejem	Ondřej	k1gMnSc7	Ondřej
ve	v	k7c6	v
Hvozdně	hvozdně	k6eAd1	hvozdně
<g/>
.	.	kIx.	.
</s>
<s>
Přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
táborských	táborští	k1gMnPc6	táborští
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
několik	několik	k4yIc4	několik
je	být	k5eAaImIp3nS	být
vězněno	věznit	k5eAaImNgNnS	věznit
a	a	k8xC	a
mučeno	mučit	k5eAaImNgNnS	mučit
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
hradu	hrad	k1gInSc2	hrad
Příběnice	Příběnice	k1gFnSc2	Příběnice
<g/>
.	.	kIx.	.
</s>
<s>
Přijde	přijít	k5eAaPmIp3nS	přijít
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
hrozní	hrozný	k2eAgMnPc1d1	hrozný
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
povídá	povídat	k5eAaImIp3nS	povídat
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
vězňům	vězeň	k1gMnPc3	vězeň
vzbouřit	vzbouřit	k5eAaPmF	vzbouřit
se	se	k3xPyFc4	se
a	a	k8xC	a
odeslat	odeslat	k5eAaPmF	odeslat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
dorazí	dorazit	k5eAaPmIp3nS	dorazit
k	k	k7c3	k
Příběnicím	Příběnice	k1gFnPc3	Příběnice
husitské	husitský	k2eAgFnSc2d1	husitská
vojsko	vojsko	k1gNnSc1	vojsko
a	a	k8xC	a
hrad	hrad	k1gInSc1	hrad
snadno	snadno	k6eAd1	snadno
dobude	dobýt	k5eAaPmIp3nS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
správcem	správce	k1gMnSc7	správce
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
určen	určen	k2eAgMnSc1d1	určen
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
<g/>
.	.	kIx.	.
</s>
<s>
Marta	Marta	k1gFnSc1	Marta
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Ondřejem	Ondřej	k1gMnSc7	Ondřej
<g/>
,	,	kIx,	,
když	když	k8xS	když
ten	ten	k3xDgMnSc1	ten
jí	on	k3xPp3gFnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jako	jako	k9	jako
novicka	novicka	k1gFnSc1	novicka
nestala	stát	k5eNaPmAgFnS	stát
obětí	oběť	k1gFnSc7	oběť
jiného	jiný	k2eAgNnSc2d1	jiné
náboženského	náboženský	k2eAgNnSc2d1	náboženské
smýšlení	smýšlení	k1gNnSc2	smýšlení
táborských	táborští	k1gMnPc2	táborští
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kněží	kněz	k1gMnPc1	kněz
Kániš	Kániš	k1gMnSc1	Kániš
a	a	k8xC	a
Bydlínský	Bydlínský	k2eAgMnSc1d1	Bydlínský
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
shromáždí	shromáždit	k5eAaPmIp3nS	shromáždit
věrné	věrný	k2eAgMnPc4d1	věrný
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
i	i	k9	i
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
Táborským	Táborská	k1gFnPc3	Táborská
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
spory	spor	k1gInPc1	spor
vyústí	vyústit	k5eAaPmIp3nP	vyústit
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
vyhnání	vyhnání	k1gNnSc6	vyhnání
z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
Příběnic	Příběnice	k1gFnPc2	Příběnice
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
Bydlínským	Bydlínský	k2eAgFnPc3d1	Bydlínský
i	i	k9	i
Zdena	Zdena	k1gFnSc1	Zdena
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
manželství	manželství	k1gNnSc6	manželství
však	však	k9	však
žárlí	žárlit	k5eAaImIp3nS	žárlit
Kániš	Kániš	k1gInSc4	Kániš
a	a	k8xC	a
tak	tak	k6eAd1	tak
neustále	neustále	k6eAd1	neustále
podporuje	podporovat	k5eAaImIp3nS	podporovat
ostatní	ostatní	k2eAgMnSc1d1	ostatní
proti	proti	k7c3	proti
Bydlínskému	Bydlínský	k2eAgInSc3d1	Bydlínský
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc3	jeden
noci	noc	k1gFnSc3	noc
Kánišem	Kániš	k1gInSc7	Kániš
zfanatizovaný	zfanatizovaný	k2eAgInSc1d1	zfanatizovaný
dav	dav	k1gInSc1	dav
obklíčí	obklíčit	k5eAaPmIp3nS	obklíčit
Zdenu	Zdena	k1gFnSc4	Zdena
a	a	k8xC	a
Bydlínského	Bydlínský	k2eAgNnSc2d1	Bydlínský
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
chalupě	chalupa	k1gFnSc6	chalupa
a	a	k8xC	a
upálí	upálit	k5eAaPmIp3nP	upálit
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tam	tam	k6eAd1	tam
pošle	poslat	k5eAaPmIp3nS	poslat
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Hvozdna	Hvozdno	k1gNnSc2	Hvozdno
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
Adamité	adamita	k1gMnPc1	adamita
kolem	kolem	k7c2	kolem
Kániše	Kániš	k1gInSc2	Kániš
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
i	i	k9	i
z	z	k7c2	z
Příběnic	Příběnice	k1gFnPc2	Příběnice
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
potulují	potulovat	k5eAaImIp3nP	potulovat
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
vojsko	vojsko	k1gNnSc1	vojsko
definitivně	definitivně	k6eAd1	definitivně
nerozežene	rozehnat	k5eNaPmIp3nS	rozehnat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
tažení	tažení	k1gNnSc2	tažení
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mezitím	mezitím	k6eAd1	mezitím
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
na	na	k7c4	na
táborskou	táborský	k2eAgFnSc4d1	táborská
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
Janou	Jana	k1gFnSc7	Jana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc4	její
pravé	pravý	k2eAgNnSc4d1	pravé
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
a	a	k8xC	a
radost	radost	k1gFnSc1	radost
ze	z	k7c2	z
setkání	setkání	k1gNnSc2	setkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
vycházelo	vycházet	k5eAaImAgNnS	vycházet
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
X.	X.	kA	X.
ročníku	ročník	k1gInSc2	ročník
časopisu	časopis	k1gInSc2	časopis
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
od	od	k7c2	od
čísla	číslo	k1gNnSc2	číslo
15	[number]	k4	15
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
24	[number]	k4	24
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
knižně	knižně	k6eAd1	knižně
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
předchozí	předchozí	k2eAgFnSc2d1	předchozí
Jiráskovy	Jiráskův	k2eAgFnSc2d1	Jiráskova
trilogie	trilogie	k1gFnSc2	trilogie
Mezi	mezi	k7c7	mezi
proudy	proud	k1gInPc7	proud
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
předhusitském	předhusitský	k2eAgNnSc6d1	předhusitské
údobí	údobí	k1gNnSc6	údobí
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
vydáním	vydání	k1gNnSc7	vydání
Dekretu	dekret	k1gInSc2	dekret
kutnohorského	kutnohorský	k2eAgInSc2d1	kutnohorský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
próze	próza	k1gFnSc6	próza
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
se	se	k3xPyFc4	se
Jirásek	Jirásek	k1gMnSc1	Jirásek
přenáší	přenášet	k5eAaImIp3nS	přenášet
do	do	k7c2	do
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
již	již	k6eAd1	již
jednou	jednou	k6eAd1	jednou
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
v	v	k7c6	v
románu	román	k1gInSc6	román
Slavný	slavný	k2eAgInSc4d1	slavný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
Jirásek	Jirásek	k1gMnSc1	Jirásek
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc3	období
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
provázel	provázet	k5eAaImAgMnS	provázet
bezpečného	bezpečný	k2eAgMnSc4d1	bezpečný
znalce	znalec	k1gMnSc4	znalec
dějin	dějiny	k1gFnPc2	dějiny
velký	velký	k2eAgMnSc1d1	velký
epik	epik	k1gMnSc1	epik
<g/>
,	,	kIx,	,
dovedoucí	dovedoucí	k2eAgMnSc1d1	dovedoucí
i	i	k9	i
kompozičně	kompozičně	k6eAd1	kompozičně
zvládnouti	zvládnout	k5eAaPmF	zvládnout
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
a	a	k8xC	a
ideově	ideově	k6eAd1	ideově
významnou	významný	k2eAgFnSc4d1	významná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Důležitým	důležitý	k2eAgInSc7d1	důležitý
historickým	historický	k2eAgInSc7d1	historický
pramenem	pramen	k1gInSc7	pramen
byla	být	k5eAaImAgFnS	být
Jiráskovi	Jirásek	k1gMnSc3	Jirásek
Husitská	husitský	k2eAgFnSc1d1	husitská
kronika	kronika	k1gFnSc1	kronika
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Próza	próza	k1gFnSc1	próza
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
slovenštiny	slovenština	k1gFnSc2	slovenština
(	(	kIx(	(
<g/>
Proti	proti	k7c3	proti
všetkým	všetký	k1gMnPc3	všetký
<g/>
:	:	kIx,	:
list	list	k1gInSc1	list
z	z	k7c2	z
českej	českej	k?	českej
epopeje	epopej	k1gFnSc2	epopej
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnPc1	maďarština
(	(	kIx(	(
<g/>
Mindenki	Mindenki	k1gNnPc1	Mindenki
ellen	ellna	k1gFnPc2	ellna
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bulharštiny	bulharština	k1gFnSc2	bulharština
(	(	kIx(	(
<g/>
Protiv	protiv	k1gInSc1	protiv
vsički	vsičk	k1gFnSc2	vsičk
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
Wider	Wider	k1gInSc1	Wider
alle	alle	k1gNnSc1	alle
Welt	Welt	k1gInSc1	Welt
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
(	(	kIx(	(
<g/>
Protiv	protivit	k5eAaImRp2nS	protivit
vsech	vsech	k1gInSc1	vsech
[	[	kIx(	[
<g/>
П	П	k?	П
в	в	k?	в
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rumunštiny	rumunština	k1gFnSc2	rumunština
(	(	kIx(	(
<g/>
Împortiva	Împortiva	k1gFnSc1	Împortiva
tuturor	tuturor	k1gMnSc1	tuturor
<g/>
:	:	kIx,	:
O	o	k7c6	o
pagină	pagină	k?	pagină
din	din	k1gInSc1	din
epopeea	epopeea	k1gFnSc1	epopeea
cechă	cechă	k?	cechă
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
litevštiny	litevština	k1gFnSc2	litevština
(	(	kIx(	(
<g/>
Prieš	Prieš	k1gMnSc1	Prieš
visus	visus	k1gMnSc1	visus
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovinštiny	slovinština	k1gFnPc4	slovinština
(	(	kIx(	(
<g/>
Proti	proti	k7c3	proti
vsem	vsem	k1gInPc3	vsem
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
též	též	k9	též
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
→	→	k?	→
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
(	(	kIx(	(
<g/>
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Vybíráme	vybírat	k5eAaImIp1nP	vybírat
32	[number]	k4	32
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
a	a	k8xC	a
vydání	vydání	k1gNnPc4	vydání
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Rada	Rada	k1gMnSc1	Rada
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
<g/>
:	:	kIx,	:
list	list	k1gInSc1	list
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
epopeje	epopej	k1gFnSc2	epopej
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
v	v	k7c6	v
Práci	práce	k1gFnSc6	práce
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
471	[number]	k4	471
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Rada	Rada	k1gMnSc1	Rada
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
476	[number]	k4	476
s.	s.	k?	s.
<g/>
Dále	daleko	k6eAd2	daleko
uvádíme	uvádět	k5eAaImIp1nP	uvádět
vydání	vydání	k1gNnSc4	vydání
s	s	k7c7	s
podrobným	podrobný	k2eAgInSc7d1	podrobný
poznámkovým	poznámkový	k2eAgInSc7d1	poznámkový
aparátem	aparát	k1gInSc7	aparát
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
mimočítankové	mimočítankový	k2eAgFnSc2d1	mimočítanková
četby	četba	k1gFnSc2	četba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
<g/>
:	:	kIx,	:
list	list	k1gInSc1	list
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
epopeje	epopej	k1gFnSc2	epopej
<g/>
:	:	kIx,	:
mimočítanková	mimočítankový	k2eAgFnSc1d1	mimočítanková
četba	četba	k1gFnSc1	četba
pro	pro	k7c4	pro
10	[number]	k4	10
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
ročník	ročník	k1gInSc1	ročník
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Edičně	edičně	k6eAd1	edičně
připravil	připravit	k5eAaPmAgMnS	připravit
a	a	k8xC	a
doslov	doslov	k1gInSc4	doslov
i	i	k8xC	i
vysvětlivky	vysvětlivka	k1gFnPc4	vysvětlivka
napsal	napsat	k5eAaPmAgMnS	napsat
Ludvík	Ludvík	k1gMnSc1	Ludvík
Páleníček	Páleníčko	k1gNnPc2	Páleníčko
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
507	[number]	k4	507
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
je	být	k5eAaImIp3nS	být
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
řada	řada	k1gFnSc1	řada
děl	dělo	k1gNnPc2	dělo
Al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Uvádíme	uvádět	k5eAaImIp1nP	uvádět
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
románu	román	k1gInSc2	román
Proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
<g/>
:	:	kIx,	:
list	list	k1gInSc1	list
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
epopeje	epopej	k1gFnSc2	epopej
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
713	[number]	k4	713
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
JANÁČKOVÁ	Janáčková	k1gFnSc1	Janáčková
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
monografie	monografie	k1gFnPc1	monografie
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
581	[number]	k4	581
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
kapitolu	kapitola	k1gFnSc4	kapitola
"	"	kIx"	"
<g/>
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
<g/>
"	"	kIx"	"
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
299	[number]	k4	299
<g/>
–	–	k?	–
<g/>
321	[number]	k4	321
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
