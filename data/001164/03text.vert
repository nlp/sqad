<s>
Josef	Josef	k1gMnSc1	Josef
Kemr	Kemr	k1gMnSc1	Kemr
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
ukončil	ukončit	k5eAaPmAgInS	ukončit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
letech	let	k1gInPc6	let
jako	jako	k8xC	jako
komparsista	komparsista	k1gMnSc1	komparsista
v	v	k7c6	v
představení	představení	k1gNnSc6	představení
Polská	polský	k2eAgFnSc1d1	polská
krev	krev	k1gFnSc1	krev
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
režisér	režisér	k1gMnSc1	režisér
Přemysl	Přemysl	k1gMnSc1	Přemysl
Pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgMnS	začít
obsazovat	obsazovat	k5eAaImF	obsazovat
do	do	k7c2	do
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Profesionálně	profesionálně	k6eAd1	profesionálně
se	se	k3xPyFc4	se
herectví	herectví	k1gNnSc1	herectví
věnoval	věnovat	k5eAaImAgInS	věnovat
od	od	k7c2	od
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
angažmá	angažmá	k1gNnSc1	angažmá
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
společnosti	společnost	k1gFnSc6	společnost
A.	A.	kA	A.
Budínské-Červíčkové	Budínské-Červíčková	k1gFnPc1	Budínské-Červíčková
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
angažmá	angažmá	k1gNnSc6	angažmá
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1947	[number]	k4	1947
<g/>
/	/	kIx~	/
<g/>
1948	[number]	k4	1948
pak	pak	k6eAd1	pak
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Akropolis	Akropolis	k1gFnSc1	Akropolis
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgNnSc1d2	pozdější
Divadlo	divadlo	k1gNnSc1	divadlo
města	město	k1gNnSc2	město
Žižkova	Žižkov	k1gInSc2	Žižkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
S.	S.	kA	S.
K.	K.	kA	K.
Neumanna	Neumann	k1gMnSc4	Neumann
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
v	v	k7c6	v
Městských	městský	k2eAgNnPc6d1	Městské
divadlech	divadlo	k1gNnPc6	divadlo
pražských	pražský	k2eAgNnPc6d1	Pražské
<g/>
..	..	k?	..
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
činohry	činohra	k1gFnSc2	činohra
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
stovky	stovka	k1gFnPc4	stovka
divadelních	divadelní	k2eAgMnPc2d1	divadelní
(	(	kIx(	(
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgInPc2d1	filmový
(	(	kIx(	(
<g/>
150	[number]	k4	150
<g/>
)	)	kIx)	)
a	a	k8xC	a
televizních	televizní	k2eAgInPc2d1	televizní
(	(	kIx(	(
<g/>
170	[number]	k4	170
<g/>
)	)	kIx)	)
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gMnSc7	jeho
velikým	veliký	k2eAgMnSc7d1	veliký
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
kamarádem	kamarád	k1gMnSc7	kamarád
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
,	,	kIx,	,
než	než	k8xS	než
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
přátelství	přátelství	k1gNnSc4	přátelství
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
přesně	přesně	k6eAd1	přesně
neznámo	neznámo	k6eAd1	neznámo
proč	proč	k6eAd1	proč
<g/>
)	)	kIx)	)
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
podepsal	podepsat	k5eAaPmAgMnS	podepsat
petici	petice	k1gFnSc4	petice
Několik	několik	k4yIc1	několik
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
manifestaci	manifestace	k1gFnSc6	manifestace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
na	na	k7c6	na
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
manželem	manžel	k1gMnSc7	manžel
herečky	herečka	k1gFnSc2	herečka
Evy	Eva	k1gFnSc2	Eva
Foustkové	Foustková	k1gFnSc2	Foustková
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
N.	N.	kA	N.
Richard	Richard	k1gMnSc1	Richard
Nash	Nash	k1gMnSc1	Nash
<g/>
:	:	kIx,	:
Obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
Jonáš	Jonáš	k1gMnSc1	Jonáš
Curry	Curra	k1gFnSc2	Curra
<g/>
,	,	kIx,	,
Komorní	komorní	k2eAgNnSc1d1	komorní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
z	z	k7c2	z
boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vázán	vázat	k5eAaImNgInS	vázat
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
vladařů	vladař	k1gMnPc2	vladař
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdánlivě	zdánlivě	k6eAd1	zdánlivě
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
k	k	k7c3	k
povolání	povolání	k1gNnSc3	povolání
tak	tak	k6eAd1	tak
světskému	světský	k2eAgMnSc3d1	světský
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
herectví	herectví	k1gNnSc1	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
neváhám	váhat	k5eNaImIp1nS	váhat
ho	on	k3xPp3gMnSc4	on
připojit	připojit	k5eAaPmF	připojit
ke	k	k7c3	k
jménu	jméno	k1gNnSc3	jméno
Josefa	Josef	k1gMnSc2	Josef
Kemra	Kemr	k1gMnSc2	Kemr
<g/>
...	...	k?	...
<g/>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
osobou	osoba	k1gFnSc7	osoba
bylo	být	k5eAaImAgNnS	být
vázáno	vázat	k5eAaImNgNnS	vázat
jakési	jakýsi	k3yIgNnSc1	jakýsi
charisma	charisma	k1gNnSc1	charisma
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
byl	být	k5eAaImAgInS	být
obklopován	obklopován	k2eAgInSc4d1	obklopován
a	a	k8xC	a
které	který	k3yRgFnPc1	který
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
vyzařovalo	vyzařovat	k5eAaImAgNnS	vyzařovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
z	z	k7c2	z
boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
více	hodně	k6eAd2	hodně
hodil	hodit	k5eAaPmAgMnS	hodit
než	než	k8xS	než
ke	k	k7c3	k
jménům	jméno	k1gNnPc3	jméno
některých	některý	k3yIgMnPc2	některý
králů	král	k1gMnPc2	král
a	a	k8xC	a
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
to	ten	k3xDgNnSc4	ten
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
milost	milost	k1gFnSc1	milost
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
jako	jako	k8xS	jako
titul	titul	k1gInSc1	titul
nebo	nebo	k8xC	nebo
výsada	výsada	k1gFnSc1	výsada
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Sovák	Sovák	k1gMnSc1	Sovák
Pepíček	Pepíček	k1gMnSc1	Pepíček
Kemr	Kemr	k1gMnSc1	Kemr
byl	být	k5eAaImAgMnS	být
nejen	nejen	k6eAd1	nejen
kolega	kolega	k1gMnSc1	kolega
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
spolutvůrce	spolutvůrce	k1gMnSc1	spolutvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nádherné	nádherný	k2eAgNnSc1d1	nádherné
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
Elišce	Eliška	k1gFnSc6	Eliška
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Chalupářích	chalupář	k1gMnPc6	chalupář
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
tu	ten	k3xDgFnSc4	ten
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
laskavost	laskavost	k1gFnSc4	laskavost
<g/>
,	,	kIx,	,
snahu	snaha	k1gFnSc4	snaha
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
prostě	prostě	k9	prostě
hluboké	hluboký	k2eAgNnSc4d1	hluboké
člověčenství	člověčenství	k1gNnSc4	člověčenství
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
nemusel	muset	k5eNaImAgMnS	muset
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
patřilo	patřit	k5eAaImAgNnS	patřit
bytostně	bytostně	k6eAd1	bytostně
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
nátuře	nátura	k1gFnSc3	nátura
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gFnSc2	jeho
samého	samý	k3xTgNnSc2	samý
<g/>
...	...	k?	...
<g/>
Pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
je	být	k5eAaImIp3nS	být
ctí	čest	k1gFnSc7	čest
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Vinklář	vinklář	k1gMnSc1	vinklář
Kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgMnS	mít
o	o	k7c6	o
některém	některý	k3yIgNnSc6	některý
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
v	v	k7c6	v
životě	život	k1gInSc6	život
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
,	,	kIx,	,
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
pracovitý	pracovitý	k2eAgMnSc1d1	pracovitý
<g/>
,	,	kIx,	,
pilný	pilný	k2eAgMnSc1d1	pilný
<g/>
,	,	kIx,	,
poctivý	poctivý	k2eAgMnSc1d1	poctivý
<g/>
,	,	kIx,	,
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
,	,	kIx,	,
podnětný	podnětný	k2eAgInSc1d1	podnětný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
posedlý	posedlý	k2eAgInSc4d1	posedlý
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
bych	by	kYmCp1nS	by
vyslovit	vyslovit	k5eAaPmF	vyslovit
jméno	jméno	k1gNnSc1	jméno
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Kemr	Kemr	k1gMnSc1	Kemr
<g/>
.	.	kIx.	.
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Navrátil	Navrátil	k1gMnSc1	Navrátil
On	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
především	především	k6eAd1	především
"	"	kIx"	"
<g/>
herec	herec	k1gMnSc1	herec
od	od	k7c2	od
paty	pata	k1gFnSc2	pata
až	až	k9	až
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neznal	znát	k5eNaImAgMnS	znát
oddychu	oddych	k1gInSc3	oddych
<g/>
.	.	kIx.	.
</s>
<s>
Nepamatuji	pamatovat	k5eNaImIp1nS	pamatovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
pilně	pilně	k6eAd1	pilně
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
radostné	radostný	k2eAgFnSc2d1	radostná
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
...	...	k?	...
Jana	Jana	k1gFnSc1	Jana
Boušková	Boušková	k1gFnSc1	Boušková
Měla	mít	k5eAaImAgFnS	mít
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jak	jak	k6eAd1	jak
dcerou	dcera	k1gFnSc7	dcera
pana	pan	k1gMnSc2	pan
Kemra	Kemr	k1gMnSc2	Kemr
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Tvrdohlavá	tvrdohlavý	k2eAgFnSc1d1	tvrdohlavá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
tak	tak	k9	tak
-	-	kIx~	-
jak	jak	k8xS	jak
šel	jít	k5eAaImAgMnS	jít
čas	čas	k1gInSc4	čas
-	-	kIx~	-
i	i	k9	i
nakonec	nakonec	k6eAd1	nakonec
jeho	jeho	k3xOp3gFnSc7	jeho
milenkou	milenka	k1gFnSc7	milenka
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Tajné	tajný	k2eAgInPc1d1	tajný
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
tajná	tajný	k2eAgFnSc1d1	tajná
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Prostě	prostě	k9	prostě
pan	pan	k1gMnSc1	pan
Kemr	Kemr	k1gMnSc1	Kemr
nestárnul	stárnout	k5eNaPmAgMnS	stárnout
<g/>
...	...	k?	...
Byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
pořád	pořád	k6eAd1	pořád
stejný	stejný	k2eAgInSc4d1	stejný
-	-	kIx~	-
milý	milý	k2eAgInSc4d1	milý
<g/>
,	,	kIx,	,
dobrý	dobrý	k2eAgInSc4d1	dobrý
<g/>
,	,	kIx,	,
vzdělaný	vzdělaný	k2eAgInSc4d1	vzdělaný
<g/>
,	,	kIx,	,
oduševněný	oduševněný	k2eAgInSc4d1	oduševněný
<g/>
,	,	kIx,	,
podivný	podivný	k2eAgInSc4d1	podivný
i	i	k8xC	i
laskavý	laskavý	k2eAgInSc4d1	laskavý
-	-	kIx~	-
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nezacelitelná	zacelitelný	k2eNgFnSc1d1	nezacelitelná
mezera	mezera	k1gFnSc1	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
mém	můj	k3xOp1gNnSc6	můj
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Brabec	Brabec	k1gMnSc1	Brabec
Pepík	Pepík	k1gMnSc1	Pepík
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
vstřícný	vstřícný	k2eAgMnSc1d1	vstřícný
kolega	kolega	k1gMnSc1	kolega
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
pan	pan	k1gMnSc1	pan
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
řekne	říct	k5eAaPmIp3nS	říct
kolega	kolega	k1gMnSc1	kolega
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
partnerovi	partner	k1gMnSc6	partner
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
,	,	kIx,	,
myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejkrásnější	krásný	k2eAgNnSc1d3	nejkrásnější
vyznání	vyznání	k1gNnSc1	vyznání
obdivu	obdiv	k1gInSc2	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Pepík	Pepík	k1gMnSc1	Pepík
Kemr	Kemr	k1gMnSc1	Kemr
byl	být	k5eAaImAgMnS	být
pan	pan	k1gMnSc1	pan
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
