<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
А	А	k?	А
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přejmenováním	přejmenování	k1gNnSc7	přejmenování
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
reorganizací	reorganizace	k1gFnSc7	reorganizace
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
rozpadu	rozpad	k1gInSc3	rozpad
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
existovaly	existovat	k5eAaImAgFnP	existovat
sovětské	sovětský	k2eAgFnPc1d1	sovětská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
fakticky	fakticky	k6eAd1	fakticky
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Spojených	spojený	k2eAgFnPc2d1	spojená
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
SNS	SNS	kA	SNS
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
formálně	formálně	k6eAd1	formálně
přestaly	přestat	k5eAaPmAgFnP	přestat
existovat	existovat	k5eAaImF	existovat
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
7	[number]	k4	7
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
Spojených	spojený	k2eAgFnPc2d1	spojená
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
SNS	SNS	kA	SNS
nacházelo	nacházet	k5eAaImAgNnS	nacházet
2	[number]	k4	2
822	[number]	k4	822
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
měly	mít	k5eAaImAgFnP	mít
pozemní	pozemní	k2eAgFnPc1d1	pozemní
složky	složka	k1gFnPc1	složka
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
:	:	kIx,	:
55	[number]	k4	55
000	[number]	k4	000
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
4	[number]	k4	4
000	[number]	k4	000
T-	T-	k1gFnPc2	T-
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
10	[number]	k4	10
000	[number]	k4	000
T-	T-	k1gFnPc2	T-
<g/>
72	[number]	k4	72
<g/>
,	,	kIx,	,
9	[number]	k4	9
700	[number]	k4	700
T-	T-	k1gFnPc2	T-
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
11	[number]	k4	11
300	[number]	k4	300
T-	T-	k1gFnPc2	T-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
19	[number]	k4	19
000	[number]	k4	000
T-	T-	k1gFnPc2	T-
<g/>
54	[number]	k4	54
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
55	[number]	k4	55
a	a	k8xC	a
1	[number]	k4	1
000	[number]	k4	000
PT-	PT-	k1gFnPc2	PT-
<g/>
76	[number]	k4	76
<g/>
.	.	kIx.	.
70	[number]	k4	70
000	[number]	k4	000
obrněných	obrněný	k2eAgNnPc2d1	obrněné
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
BTR-	BTR-	k1gFnSc1	BTR-
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
BTR-	BTR-	k1gFnSc1	BTR-
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
BTR-	BTR-	k1gFnSc1	BTR-
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
BTR-D	BTR-D	k1gFnSc1	BTR-D
<g/>
,	,	kIx,	,
BTR-	BTR-	k1gFnSc1	BTR-
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
BTR-152	BTR-152	k1gFnSc1	BTR-152
a	a	k8xC	a
MT-LB	MT-LB	k1gFnSc1	MT-LB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
24	[number]	k4	24
000	[number]	k4	000
bojových	bojový	k2eAgMnPc2d1	bojový
<g />
.	.	kIx.	.
</s>
<s>
vozidel	vozidlo	k1gNnPc2	vozidlo
pěchoty	pěchota	k1gFnSc2	pěchota
(	(	kIx(	(
<g/>
BMP-	BMP-	k1gFnSc1	BMP-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
BMP-	BMP-	k1gFnSc1	BMP-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
BMP-	BMP-	k1gFnSc1	BMP-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
BMD-	BMD-	k1gFnSc1	BMD-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
BMD-2	BMD-2	k1gFnSc1	BMD-2
a	a	k8xC	a
BMD-	BMD-	k1gFnSc1	BMD-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
500	[number]	k4	500
průzkumných	průzkumný	k2eAgNnPc2d1	průzkumné
vozidel	vozidlo	k1gNnPc2	vozidlo
BRDM-2	BRDM-2	k1gMnSc7	BRDM-2
a	a	k8xC	a
BRDM-	BRDM-	k1gFnSc7	BRDM-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
33	[number]	k4	33
000	[number]	k4	000
tažených	tažený	k2eAgFnPc2d1	tažená
dělostřeleckých	dělostřelecký	k2eAgFnPc2d1	dělostřelecká
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
4	[number]	k4	4
379	[number]	k4	379
D-	D-	k1gFnPc2	D-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
1	[number]	k4	1
175	[number]	k4	175
M-	M-	k1gFnPc2	M-
<g/>
46	[number]	k4	46
<g/>
,	,	kIx,	,
1	[number]	k4	1
700	[number]	k4	700
D-	D-	k1gFnPc2	D-
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
598	[number]	k4	598
2	[number]	k4	2
<g/>
A	a	k9	a
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
1	[number]	k4	1
007	[number]	k4	007
2	[number]	k4	2
<g/>
A	a	k9	a
<g/>
36	[number]	k4	36
<g/>
,	,	kIx,	,
857	[number]	k4	857
D-	D-	k1gFnPc2	D-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
693	[number]	k4	693
ML-	ML-	k1gFnPc2	ML-
<g/>
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1	[number]	k4	1
200	[number]	k4	200
M-	M-	k1gFnPc2	M-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
478	[number]	k4	478
B-	B-	k1gFnPc2	B-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
D-	D-	k1gFnSc1	D-
<g/>
74	[number]	k4	74
<g/>
,	,	kIx,	,
D-	D-	k1gFnSc1	D-
<g/>
48	[number]	k4	48
<g/>
,	,	kIx,	,
D-	D-	k1gFnSc1	D-
<g/>
44	[number]	k4	44
<g/>
,	,	kIx,	,
T-12	T-12	k1gFnSc1	T-12
a	a	k8xC	a
BS-	BS-	k1gFnSc1	BS-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
9	[number]	k4	9
000	[number]	k4	000
samohybných	samohybný	k2eAgFnPc2d1	samohybná
houfnic	houfnice	k1gFnPc2	houfnice
(	(	kIx(	(
<g/>
2,751	[number]	k4	2,751
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
325	[number]	k4	325
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
507	[number]	k4	507
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
347	[number]	k4	347
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
430	[number]	k4	430
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
20	[number]	k4	20
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
108	[number]	k4	108
SkH	SkH	k1gFnSc1	SkH
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
ASU-85	ASU-85	k1gFnSc1	ASU-85
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
8	[number]	k4	8
000	[number]	k4	000
raketových	raketový	k2eAgInPc2d1	raketový
dělostreleckých	dělostrelecký	k2eAgInPc2d1	dělostrelecký
komplexů	komplex	k1gInPc2	komplex
(	(	kIx(	(
<g/>
BM-	BM-	k1gFnPc2	BM-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
818	[number]	k4	818
BM-	BM-	k1gFnPc2	BM-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
123	[number]	k4	123
BM-	BM-	k1gFnPc2	BM-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
18	[number]	k4	18
BM-	BM-	k1gFnPc2	BM-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
TOS-	TOS-	k1gFnSc1	TOS-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
BM-25	BM-25	k1gFnSc1	BM-25
a	a	k8xC	a
BM-	BM-	k1gFnSc1	BM-
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Balistické	balistický	k2eAgFnPc1d1	balistická
taktické	taktický	k2eAgFnPc1d1	taktická
rakety	raketa	k1gFnPc1	raketa
SS-1	SS-1	k1gMnSc1	SS-1
Scud	Scud	k1gMnSc1	Scud
<g/>
,	,	kIx,	,
SS-	SS-	k1gFnSc1	SS-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
SS-23	SS-23	k1gFnSc1	SS-23
a	a	k8xC	a
FROG-	FROG-	k1gFnSc1	FROG-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
350	[number]	k4	350
protiletadlových	protiletadlový	k2eAgNnPc2d1	protiletadlové
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
SA-	SA-	k1gFnSc1	SA-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
850	[number]	k4	850
SA-	SA-	k1gFnPc2	SA-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
950	[number]	k4	950
SA-	SA-	k1gFnPc2	SA-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
430	[number]	k4	430
SA-	SA-	k1gFnPc2	SA-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
300	[number]	k4	300
SA-	SA-	k1gFnSc1	SA-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
70	[number]	k4	70
SA-	SA-	k1gFnPc2	SA-
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
860	[number]	k4	860
SA-	SA-	k1gFnPc2	SA-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
20	[number]	k4	20
SA-	SA-	k1gFnPc2	SA-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
130	[number]	k4	130
SA-	SA-	k1gFnPc2	SA-
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
ZSU-23-4	ZSU-23-4	k1gFnSc1	ZSU-23-4
a	a	k8xC	a
ZSU-	ZSU-	k1gFnSc1	ZSU-
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
12	[number]	k4	12
000	[number]	k4	000
tažených	tažený	k2eAgNnPc2d1	tažené
protiletadlových	protiletadlový	k2eAgNnPc2d1	protiletadlové
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
ZU-	ZU-	k1gFnSc1	ZU-
<g/>
23	[number]	k4	23
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ZPU-	ZPU-	k1gFnSc1	ZPU-
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
S-	S-	k1gFnSc1	S-
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
72	[number]	k4	72
<g/>
-K	-K	k?	-K
<g/>
,	,	kIx,	,
61	[number]	k4	61
<g/>
-K	-K	k?	-K
<g/>
,	,	kIx,	,
52-K	[number]	k4	52-K
a	a	k8xC	a
KS-	KS-	k1gMnSc3	KS-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
300	[number]	k4	300
vrtulníků	vrtulník	k1gInPc2	vrtulník
(	(	kIx(	(
<g/>
1	[number]	k4	1
420	[number]	k4	420
Mi-	Mi-	k1gMnPc2	Mi-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
600	[number]	k4	600
Mi-	Mi-	k1gFnPc2	Mi-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
620	[number]	k4	620
Mi-	Mi-	k1gFnPc2	Mi-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
290	[number]	k4	290
Mi-	Mi-	k1gFnPc2	Mi-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
450	[number]	k4	450
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
6	[number]	k4	6
a	a	k8xC	a
50	[number]	k4	50
Mi-	Mi-	k1gFnPc2	Mi-
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
Mužstvo	mužstvo	k1gNnSc1	mužstvo
Vojín	vojín	k1gMnSc1	vojín
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Pя	Pя	k1gFnSc1	Pя
в	в	k?	в
<g/>
)	)	kIx)	)
Svobodník	svobodník	k1gMnSc1	svobodník
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
<g/>
Е	Е	k?	Е
-	-	kIx~	-
z	z	k7c2	z
něm.	něm.	k?	něm.
Gefreiter	Gefreiter	k1gInSc1	Gefreiter
<g/>
)	)	kIx)	)
Poddůstojníci	poddůstojník	k1gMnPc1	poddůstojník
Mladší	mladý	k2eAgMnSc1d2	mladší
seržant	seržant	k1gMnSc1	seržant
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
М	М	k?	М
с	с	k?	с
<g/>
)	)	kIx)	)
Seržant	seržant	k1gMnSc1	seržant
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
С	С	k?	С
<g/>
)	)	kIx)	)
Starší	starší	k1gMnSc1	starší
seržant	seržant	k1gMnSc1	seržant
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
С	С	k?	С
с	с	k?	с
<g/>
)	)	kIx)	)
Staršina	staršina	k1gMnSc1	staršina
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
С	С	k?	С
<g/>
)	)	kIx)	)
Praporčíci	praporčík	k1gMnPc1	praporčík
Praporčík	praporčík	k1gMnSc1	praporčík
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
Starší	starší	k1gMnSc1	starší
praporčík	praporčík	k1gMnSc1	praporčík
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
С	С	k?	С
п	п	k?	п
<g/>
)	)	kIx)	)
Nižší	nízký	k2eAgMnPc1d2	nižší
důstojníci	důstojník	k1gMnPc1	důstojník
Podporučík	podporučík	k1gMnSc1	podporučík
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
М	М	k?	М
л	л	k?	л
<g/>
)	)	kIx)	)
Poručík	poručík	k1gMnSc1	poručík
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
Л	Л	k?	Л
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Nadporučík	nadporučík	k1gMnSc1	nadporučík
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
С	С	k?	С
л	л	k?	л
<g/>
)	)	kIx)	)
Kapitán	kapitán	k1gMnSc1	kapitán
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
К	К	k?	К
<g/>
)	)	kIx)	)
Vyšší	vysoký	k2eAgMnPc1d2	vyšší
důstojníci	důstojník	k1gMnPc1	důstojník
Major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
М	М	k?	М
<g/>
)	)	kIx)	)
Podplukovník	podplukovník	k1gMnSc1	podplukovník
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
Plukovník	plukovník	k1gMnSc1	plukovník
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
П	П	k?	П
<g/>
)	)	kIx)	)
Generálové	generálová	k1gFnSc2	generálová
Generálmajor	generálmajor	k1gMnSc1	generálmajor
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
Г	Г	k?	Г
<g/>
)	)	kIx)	)
Generálporučík	generálporučík	k1gMnSc1	generálporučík
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
Г	Г	k?	Г
<g/>
)	)	kIx)	)
Generálplukovník	generálplukovník	k1gMnSc1	generálplukovník
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
Г	Г	k?	Г
<g/>
)	)	kIx)	)
Armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
Г	Г	k?	Г
а	а	k?	а
<g/>
)	)	kIx)	)
Maršálové	maršál	k1gMnPc1	maršál
Maršál	maršál	k1gMnSc1	maršál
druhu	druh	k1gInSc6	druh
vojsk	vojsko	k1gNnPc2	vojsko
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Maršál	maršál	k1gMnSc1	maršál
výsadkových	výsadkový	k2eAgNnPc2d1	výsadkové
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
М	М	k?	М
в	в	k?	в
в	в	k?	в
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
maršál	maršál	k1gMnSc1	maršál
druhu	druh	k1gInSc2	druh
vojsk	vojsko	k1gNnPc2	vojsko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
maršál	maršál	k1gMnSc1	maršál
tankových	tankový	k2eAgNnPc2d1	tankové
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Г	Г	k?	Г
м	м	k?	м
т	т	k?	т
в	в	k?	в
<g/>
)	)	kIx)	)
Maršál	maršál	k1gMnSc1	maršál
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
М	М	k?	М
С	С	k?	С
С	С	k?	С
<g/>
)	)	kIx)	)
1944-1956	[number]	k4	1944-1956
Intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
1950-1953	[number]	k4	1950-1953
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
1953	[number]	k4	1953
Východoněmecké	východoněmecký	k2eAgInPc1d1	východoněmecký
povstání	povstání	k1gNnSc2	povstání
1955-1975	[number]	k4	1955-1975
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
1956	[number]	k4	1956
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
povstání	povstání	k1gNnSc1	povstání
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Sandinistická	Sandinistický	k2eAgFnSc1d1	Sandinistická
revoluce	revoluce	k1gFnSc1	revoluce
1962	[number]	k4	1962
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
Angolská	angolský	k2eAgFnSc1d1	angolská
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
1964	[number]	k4	1964
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
podpora	podpora	k1gFnSc1	podpora
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
1967	[number]	k4	1967
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1970	[number]	k4	1970
Opotřebovací	opotřebovací	k2eAgFnSc1d1	opotřebovací
válka	válka	k1gFnSc1	válka
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
Nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
1968	[number]	k4	1968
Invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
1969	[number]	k4	1969
Sovětsko-čínské	sovětsko-čínský	k2eAgInPc1d1	sovětsko-čínský
konflikty	konflikt	k1gInPc1	konflikt
1969	[number]	k4	1969
1969	[number]	k4	1969
Komunistické	komunistický	k2eAgInPc1d1	komunistický
povstání	povstání	k1gNnSc4	povstání
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
1969-1970	[number]	k4	1969-1970
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
podpora	podpora	k1gFnSc1	podpora
egyptských	egyptský	k2eAgMnPc2d1	egyptský
islamistů	islamista	k1gMnPc2	islamista
1971	[number]	k4	1971
Indicko-pákistánská	indickoákistánský	k2eAgFnSc1d1	indicko-pákistánská
válka	válka	k1gFnSc1	válka
1973	[number]	k4	1973
Jomkipurská	Jomkipurský	k2eAgFnSc1d1	Jomkipurská
válka	válka	k1gFnSc1	válka
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
1974-1990	[number]	k4	1974-1990
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Eritrei	Eritrei	k6eAd1	Eritrei
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
Angolská	angolský	k2eAgFnSc1d1	angolská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Mosambiku	Mosambik	k1gInSc6	Mosambik
1977-1978	[number]	k4	1977-1978
Etiopsko-somálská	etiopskoomálský	k2eAgFnSc1d1	etiopsko-somálský
válka	válka	k1gFnSc1	válka
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
Salvadorská	salvadorský	k2eAgFnSc1d1	Salvadorská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Náhorní	náhorní	k2eAgInSc4d1	náhorní
Karabach	Karabach	k1gInSc4	Karabach
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
1992	[number]	k4	1992
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Podněstří	Podněstří	k1gFnSc6	Podněstří
1992	[number]	k4	1992
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
1992-1997	[number]	k4	1992-1997
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
1993	[number]	k4	1993
Gruzínsko-abchazský	gruzínskobchazský	k2eAgInSc4d1	gruzínsko-abchazský
konflikt	konflikt	k1gInSc4	konflikt
1993	[number]	k4	1993
Ruská	ruský	k2eAgFnSc1d1	ruská
ústavní	ústavní	k2eAgFnSc1d1	ústavní
krize	krize	k1gFnSc1	krize
1993	[number]	k4	1993
PECKA	Pecka	k1gMnSc1	Pecka
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Chronologický	chronologický	k2eAgInSc4d1	chronologický
přehled	přehled	k1gInSc4	přehled
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
231	[number]	k4	231
s.	s.	k?	s.
PECKA	Pecka	k1gMnSc1	Pecka
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Odsun	odsun	k1gInSc1	odsun
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
351	[number]	k4	351
s.	s.	k?	s.
Ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
SSSR	SSSR	kA	SSSR
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
letectvo	letectvo	k1gNnSc1	letectvo
Vojska	vojsko	k1gNnSc2	vojsko
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
Ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
