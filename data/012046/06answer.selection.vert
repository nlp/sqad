<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
moci	moct	k5eAaImF	moct
výkonné	výkonný	k2eAgFnPc4d1	výkonná
stojí	stát	k5eAaImIp3nS	stát
vláda	vláda	k1gFnSc1	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
