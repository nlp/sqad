<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Belfast	Belfast	k1gInSc1	Belfast
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnPc2	Lewis
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Jack	Jack	k1gMnSc1	Jack
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
autorů	autor	k1gMnPc2	autor
moderní	moderní	k2eAgFnSc2d1	moderní
britské	britský	k2eAgFnSc2d1	britská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
dílu	dílo	k1gNnSc3	dílo
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
středověké	středověký	k2eAgFnSc2d1	středověká
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
apologetiky	apologetika	k1gFnSc2	apologetika
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc2d1	literární
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
knih	kniha	k1gFnPc2	kniha
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
Hobita	hobit	k1gMnSc2	hobit
a	a	k8xC	a
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
autoři	autor	k1gMnPc1	autor
byli	být	k5eAaImAgMnP	být
vůdčími	vůdčí	k2eAgFnPc7d1	vůdčí
postavami	postava	k1gFnPc7	postava
mezi	mezi	k7c4	mezi
profesory	profesor	k1gMnPc4	profesor
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
a	a	k8xC	a
neformální	formální	k2eNgFnPc1d1	neformální
oxfordské	oxfordský	k2eAgFnPc1d1	Oxfordská
literární	literární	k2eAgFnPc1d1	literární
skupiny	skupina	k1gFnPc1	skupina
zvané	zvaný	k2eAgFnSc2d1	zvaná
Inklings	Inklings	k1gInSc4	Inklings
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgInPc2	svůj
memoárů	memoáry	k1gInPc2	memoáry
Zaskočen	zaskočen	k2eAgInSc4d1	zaskočen
radostí	radost	k1gFnSc7	radost
byl	být	k5eAaImAgInS	být
Lewis	Lewis	k1gInSc1	Lewis
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
narození	narození	k1gNnSc6	narození
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
v	v	k7c6	v
irské	irský	k2eAgFnSc6d1	irská
anglikánské	anglikánský	k2eAgFnSc6d1	anglikánská
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
dospívání	dospívání	k1gNnSc2	dospívání
od	od	k7c2	od
víry	víra	k1gFnSc2	víra
odpadl	odpadnout	k5eAaPmAgMnS	odpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Tolkiena	Tolkien	k1gMnSc2	Tolkien
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
přátel	přítel	k1gMnPc2	přítel
se	se	k3xPyFc4	se
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
kolem	kolem	k7c2	kolem
30	[number]	k4	30
let	léto	k1gNnPc2	léto
znovu	znovu	k6eAd1	znovu
obrátil	obrátit	k5eAaPmAgInS	obrátit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
laikem	laik	k1gMnSc7	laik
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Obrácení	obrácení	k1gNnSc1	obrácení
významně	významně	k6eAd1	významně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
pořad	pořad	k1gInSc1	pořad
o	o	k7c4	o
křesťanství	křesťanství	k1gNnSc4	křesťanství
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
za	za	k7c2	za
války	válka	k1gFnSc2	válka
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
vlnu	vlna	k1gFnSc4	vlna
nadšení	nadšení	k1gNnSc2	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
Joy	Joy	k1gFnSc2	Joy
Greshamovou	Greshamový	k2eAgFnSc4d1	Greshamová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
45	[number]	k4	45
let	léto	k1gNnPc2	léto
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Lewisova	Lewisův	k2eAgNnPc1d1	Lewisovo
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
přeložena	přeložit	k5eAaPmNgNnP	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
jazyků	jazyk	k1gInPc2	jazyk
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc4	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Knih	kniha	k1gFnPc2	kniha
ze	z	k7c2	z
série	série	k1gFnSc2	série
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
Lewisova	Lewisův	k2eAgNnPc1d1	Lewisovo
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
zpracována	zpracovat	k5eAaPmNgNnP	zpracovat
jako	jako	k9	jako
divadelní	divadelní	k2eAgFnSc2d1	divadelní
nebo	nebo	k8xC	nebo
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	on	k3xPp3gNnSc4	on
filmové	filmový	k2eAgNnSc4d1	filmové
zpracování	zpracování	k1gNnSc4	zpracování
Lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
a	a	k8xC	a
skříně	skříň	k1gFnSc2	skříň
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
The	The	k1gMnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
děl	dělo	k1gNnPc2	dělo
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
a	a	k8xC	a
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
popularity	popularita	k1gFnSc2	popularita
např.	např.	kA	např.
Rady	Rada	k1gMnSc2	Rada
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
desítek	desítka	k1gFnPc2	desítka
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
převedení	převedení	k1gNnSc6	převedení
na	na	k7c4	na
filmové	filmový	k2eAgNnSc4d1	filmové
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
či	či	k8xC	či
kniha	kniha	k1gFnSc1	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
laických	laický	k2eAgMnPc2d1	laický
apologetů	apologet	k1gMnPc2	apologet
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
advokáta	advokát	k1gMnSc2	advokát
Alberta	Albert	k1gMnSc2	Albert
Jamese	Jamese	k1gFnSc1	Jamese
Lewise	Lewise	k1gFnSc1	Lewise
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
a	a	k8xC	a
Flory	Flora	k1gFnPc1	Flora
Augusty	Augusta	k1gMnSc2	Augusta
Lewisové	Lewisový	k2eAgFnSc2d1	Lewisová
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
irského	irský	k2eAgMnSc2d1	irský
anglikánského	anglikánský	k2eAgMnSc2d1	anglikánský
pastora	pastor	k1gMnSc2	pastor
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
Warrena	Warren	k1gMnSc2	Warren
Hamiltona	Hamilton	k1gMnSc2	Hamilton
Lewise	Lewise	k1gFnSc2	Lewise
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k9	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jeho	jeho	k3xOp3gMnSc4	jeho
psa	pes	k1gMnSc4	pes
jménem	jméno	k1gNnSc7	jméno
Jacksie	Jacksie	k1gFnSc2	Jacksie
srazil	srazit	k5eAaPmAgMnS	srazit
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
říkat	říkat	k5eAaImF	říkat
Jacksie	Jacksie	k1gFnPc4	Jacksie
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
na	na	k7c4	na
žádné	žádný	k3yNgNnSc4	žádný
jiné	jiný	k2eAgNnSc4d1	jiné
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
přijal	přijmout	k5eAaPmAgInS	přijmout
jméno	jméno	k1gNnSc4	jméno
Jacks	Jacksa	k1gFnPc2	Jacksa
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
stal	stát	k5eAaPmAgInS	stát
Jack	Jack	k1gInSc1	Jack
-	-	kIx~	-
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgMnPc3	který
ho	on	k3xPp3gNnSc4	on
znali	znát	k5eAaImAgMnP	znát
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc2	Little
Lee	Lea	k1gFnSc6	Lea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
postavil	postavit	k5eAaPmAgMnS	postavit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Strandtown	Strandtowna	k1gFnPc2	Strandtowna
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Belfastu	Belfast	k1gInSc2	Belfast
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
vyučován	vyučovat	k5eAaImNgInS	vyučovat
soukromými	soukromý	k2eAgFnPc7d1	soukromá
učiteli	učitel	k1gMnSc6	učitel
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
Wynyard	Wynyard	k1gInSc1	Wynyard
School	School	k1gInSc1	School
ve	v	k7c6	v
Watfordu	Watford	k1gInSc6	Watford
v	v	k7c6	v
Hertfordshire	Hertfordshir	k1gInSc5	Hertfordshir
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
tam	tam	k6eAd1	tam
začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
už	už	k9	už
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
škola	škola	k1gFnSc1	škola
zrušena	zrušen	k2eAgFnSc1d1	zrušena
kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
počtu	počet	k1gInSc3	počet
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Robert	Robert	k1gMnSc1	Robert
"	"	kIx"	"
<g/>
Oldie	Oldie	k1gFnSc1	Oldie
<g/>
"	"	kIx"	"
Capron	Capron	k1gInSc1	Capron
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
choré	chorý	k2eAgFnPc4d1	chorá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zaskočen	zaskočen	k2eAgMnSc1d1	zaskočen
radostí	radost	k1gFnSc7	radost
Lewis	Lewis	k1gFnSc2	Lewis
později	pozdě	k6eAd2	pozdě
tuto	tento	k3xDgFnSc4	tento
školu	škola	k1gFnSc4	škola
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
Belsen	Belsen	k2eAgMnSc1d1	Belsen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
tábor	tábor	k1gInSc1	tábor
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Životopisec	životopisec	k1gMnSc1	životopisec
Alan	Alan	k1gMnSc1	Alan
Jacobs	Jacobsa	k1gFnPc2	Jacobsa
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
na	na	k7c4	na
Wynyard	Wynyard	k1gInSc4	Wynyard
School	School	k1gInSc4	School
představovala	představovat	k5eAaImAgFnS	představovat
pro	pro	k7c4	pro
Lewise	Lewise	k1gFnPc4	Lewise
velké	velký	k2eAgNnSc4d1	velké
trauma	trauma	k1gNnSc4	trauma
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
rozvoj	rozvoj	k1gInSc4	rozvoj
"	"	kIx"	"
<g/>
mírně	mírně	k6eAd1	mírně
sadomasochistických	sadomasochistický	k2eAgFnPc2d1	sadomasochistická
představ	představa	k1gFnPc2	představa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavření	zavření	k1gNnSc6	zavření
Wynyardu	Wynyard	k1gInSc2	Wynyard
studoval	studovat	k5eAaImAgMnS	studovat
Lewis	Lewis	k1gFnSc4	Lewis
na	na	k7c4	na
Campbell	Campbell	k1gInSc4	Campbell
College	Colleg	k1gFnSc2	Colleg
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Belfastu	Belfast	k1gInSc2	Belfast
<g/>
,	,	kIx,	,
1	[number]	k4	1
míli	míle	k1gFnSc6	míle
od	od	k7c2	od
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
brzy	brzy	k6eAd1	brzy
odešel	odejít	k5eAaPmAgMnS	odejít
kvůli	kvůli	k7c3	kvůli
dýchacím	dýchací	k2eAgFnPc3d1	dýchací
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
lázeňského	lázeňský	k2eAgNnSc2d1	lázeňské
města	město	k1gNnSc2	město
Malvern	Malverna	k1gFnPc2	Malverna
ve	v	k7c6	v
Worcestershire	Worcestershir	k1gMnSc5	Worcestershir
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
přípravné	přípravný	k2eAgFnSc2d1	přípravná
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
žáka	žák	k1gMnSc4	žák
připravit	připravit	k5eAaPmF	připravit
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
)	)	kIx)	)
Cherbourg	Cherbourg	k1gInSc1	Cherbourg
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
v	v	k7c6	v
Lewisově	Lewisův	k2eAgFnSc6d1	Lewisova
autobiografii	autobiografie	k1gFnSc6	autobiografie
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Charters	Charters	k1gInSc1	Charters
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1913	[number]	k4	1913
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Malvern	Malvern	k1gNnSc4	Malvern
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
následujícího	následující	k2eAgInSc2d1	následující
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
opustil	opustit	k5eAaPmAgMnS	opustit
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ateistou	ateista	k1gMnSc7	ateista
a	a	k8xC	a
zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
mytologií	mytologie	k1gFnSc7	mytologie
a	a	k8xC	a
okultismem	okultismus	k1gInSc7	okultismus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
označil	označit	k5eAaPmAgMnS	označit
"	"	kIx"	"
<g/>
Wyvernu	Wyverna	k1gFnSc4	Wyverna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
wyverna	wyverna	k1gFnSc1	wyverna
je	být	k5eAaImIp3nS	být
mytologický	mytologický	k2eAgInSc4d1	mytologický
okřídlený	okřídlený	k2eAgInSc4d1	okřídlený
drak	drak	k1gInSc4	drak
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
nazval	nazvat	k5eAaBmAgMnS	nazvat
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
<g/>
)	)	kIx)	)
za	za	k7c4	za
tak	tak	k9	tak
jednostranně	jednostranně	k6eAd1	jednostranně
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
růst	růst	k1gInSc4	růst
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
že	že	k8xS	že
považoval	považovat	k5eAaImAgMnS	považovat
homosexuální	homosexuální	k2eAgInPc4d1	homosexuální
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
staršími	starý	k2eAgMnPc7d2	starší
i	i	k8xC	i
mladšími	mladý	k2eAgMnPc7d2	mladší
studenty	student	k1gMnPc7	student
za	za	k7c4	za
"	"	kIx"	"
<g/>
jedinou	jediný	k2eAgFnSc4d1	jediná
oázu	oáza	k1gFnSc4	oáza
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
zelenající	zelenající	k2eAgMnSc1d1	zelenající
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
plevelem	plevel	k1gInSc7	plevel
a	a	k8xC	a
zavlažovanou	zavlažovaný	k2eAgFnSc7d1	zavlažovaná
páchnoucí	páchnoucí	k2eAgFnSc7d1	páchnoucí
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
uprostřed	uprostřed	k7c2	uprostřed
horké	horký	k2eAgFnSc2d1	horká
pouště	poušť	k1gFnSc2	poušť
soutěživých	soutěživý	k2eAgFnPc2d1	soutěživá
ambicí	ambice	k1gFnPc2	ambice
<g/>
.	.	kIx.	.
...	...	k?	...
Zvrácenost	zvrácenost	k1gFnSc1	zvrácenost
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
věcí	věc	k1gFnSc7	věc
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
kterou	který	k3yRgFnSc4	který
mohlo	moct	k5eAaImAgNnS	moct
přijít	přijít	k5eAaPmF	přijít
něco	něco	k3yInSc1	něco
spontánního	spontánní	k2eAgMnSc2d1	spontánní
a	a	k8xC	a
nevypočítaného	vypočítaný	k2eNgMnSc2d1	vypočítaný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Malvernu	Malverna	k1gFnSc4	Malverna
studoval	studovat	k5eAaImAgMnS	studovat
soukromě	soukromě	k6eAd1	soukromě
u	u	k7c2	u
Williama	William	k1gMnSc2	William
T.	T.	kA	T.
Kirkpatricka	Kirkpatricko	k1gNnPc4	Kirkpatricko
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
učitele	učitel	k1gMnSc2	učitel
svého	svůj	k1gMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ředitele	ředitel	k1gMnSc2	ředitel
Lurgan	Lurgana	k1gFnPc2	Lurgana
College	Colleg	k1gMnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
chlapec	chlapec	k1gMnSc1	chlapec
byl	být	k5eAaImAgMnS	být
Lewis	Lewis	k1gFnSc4	Lewis
fascinován	fascinován	k2eAgMnSc1d1	fascinován
antropomorfními	antropomorfní	k2eAgNnPc7d1	antropomorfní
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgInS	zamilovat
si	se	k3xPyFc3	se
příběhy	příběh	k1gInPc4	příběh
Beatrix	Beatrix	k1gInSc1	Beatrix
Potterové	Potterové	k2eAgMnSc1d1	Potterové
a	a	k8xC	a
často	často	k6eAd1	často
sám	sám	k3xTgMnSc1	sám
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
svět	svět	k1gInSc4	svět
zvaný	zvaný	k2eAgInSc1d1	zvaný
Boxen	Boxen	k1gInSc1	Boxen
<g/>
,	,	kIx,	,
obývaný	obývaný	k2eAgInSc1d1	obývaný
a	a	k8xC	a
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
velmi	velmi	k6eAd1	velmi
rád	rád	k6eAd1	rád
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
dům	dům	k1gInSc1	dům
jeho	on	k3xPp3gMnSc4	on
otce	otec	k1gMnSc4	otec
byl	být	k5eAaImAgInS	být
plný	plný	k2eAgInSc1d1	plný
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
najít	najít	k5eAaPmF	najít
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ještě	ještě	k6eAd1	ještě
nečetl	číst	k5eNaImAgInS	číst
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
snadné	snadný	k2eAgNnSc1d1	snadné
jako	jako	k8xS	jako
najít	najít	k5eAaPmF	najít
stéblo	stéblo	k1gNnSc4	stéblo
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dospívající	dospívající	k2eAgMnSc1d1	dospívající
byl	být	k5eAaImAgInS	být
fascinován	fascinovat	k5eAaBmNgInS	fascinovat
severskými	severský	k2eAgFnPc7d1	severská
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
legendami	legenda	k1gFnPc7	legenda
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
islandských	islandský	k2eAgFnPc6d1	islandská
ságách	sága	k1gFnPc6	sága
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
legendy	legenda	k1gFnPc1	legenda
zesílily	zesílit	k5eAaPmAgFnP	zesílit
touhu	touha	k1gFnSc4	touha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
a	a	k8xC	a
kterou	který	k3yIgFnSc4	který
později	pozdě	k6eAd2	pozdě
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
radostí	radost	k1gFnSc7	radost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
stále	stále	k6eAd1	stále
raději	rád	k6eAd2	rád
přírodu	příroda	k1gFnSc4	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
krása	krása	k1gFnSc1	krása
mu	on	k3xPp3gMnSc3	on
připomínala	připomínat	k5eAaImAgFnS	připomínat
severské	severský	k2eAgInPc4d1	severský
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
severské	severský	k2eAgInPc4d1	severský
příběhy	příběh	k1gInPc4	příběh
mu	on	k3xPp3gMnSc3	on
připomínaly	připomínat	k5eAaImAgInP	připomínat
krásu	krása	k1gFnSc4	krása
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nezabývá	zabývat	k5eNaImIp3nS	zabývat
Boxenem	Boxen	k1gInSc7	Boxen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívá	využívat	k5eAaPmIp3nS	využívat
jiné	jiný	k2eAgFnPc4d1	jiná
umělecké	umělecký	k2eAgFnPc4d1	umělecká
formy	forma	k1gFnPc4	forma
(	(	kIx(	(
<g/>
epickou	epický	k2eAgFnSc4d1	epická
báseň	báseň	k1gFnSc4	báseň
a	a	k8xC	a
operu	opera	k1gFnSc4	opera
<g/>
)	)	kIx)	)
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zachytit	zachytit	k5eAaPmF	zachytit
novou	nový	k2eAgFnSc4d1	nová
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
a	a	k8xC	a
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
u	u	k7c2	u
Kirkpatricka	Kirkpatricko	k1gNnSc2	Kirkpatricko
mu	on	k3xPp3gMnSc3	on
vštípilo	vštípit	k5eAaPmAgNnS	vštípit
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
řecké	řecký	k2eAgFnSc3d1	řecká
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
mytologii	mytologie	k1gFnSc3	mytologie
a	a	k8xC	a
vytříbilo	vytříbit	k5eAaPmAgNnS	vytříbit
schopnost	schopnost	k1gFnSc4	schopnost
debaty	debata	k1gFnSc2	debata
a	a	k8xC	a
logického	logický	k2eAgNnSc2d1	logické
uvažování	uvažování	k1gNnSc2	uvažování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
získal	získat	k5eAaPmAgMnS	získat
stipendium	stipendium	k1gNnSc4	stipendium
na	na	k7c4	na
oxfordské	oxfordský	k2eAgFnPc4d1	Oxfordská
University	universita	k1gFnPc4	universita
College	Colleg	k1gFnSc2	Colleg
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
důstojníkem	důstojník	k1gMnSc7	důstojník
třetího	třetí	k4xOgInSc2	třetí
praporu	prapor	k1gInSc2	prapor
somersetské	somersetský	k2eAgFnSc2d1	somersetský
lehké	lehký	k2eAgFnSc2d1	lehká
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Somme	Somm	k1gInSc5	Somm
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgMnPc2	svůj
19	[number]	k4	19
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
a	a	k8xC	a
zažil	zažít	k5eAaPmAgMnS	zažít
zde	zde	k6eAd1	zde
zákopovou	zákopový	k2eAgFnSc4d1	zákopová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
Lewis	Lewis	k1gInSc1	Lewis
raněn	ranit	k5eAaPmNgInS	ranit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Arrasu	Arras	k1gInSc2	Arras
a	a	k8xC	a
během	během	k7c2	během
zotavování	zotavování	k1gNnSc2	zotavování
trpěl	trpět	k5eAaImAgMnS	trpět
depresemi	deprese	k1gFnPc7	deprese
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
stesku	stesk	k1gInSc3	stesk
po	po	k7c6	po
domově	domov	k1gInSc6	domov
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zotavení	zotavení	k1gNnSc6	zotavení
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
převelen	převelet	k5eAaPmNgMnS	převelet
do	do	k7c2	do
Andoveru	Andover	k1gInSc2	Andover
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
ke	k	k7c3	k
svým	svůj	k3xOyFgNnPc3	svůj
studiím	studio	k1gNnPc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
získal	získat	k5eAaPmAgInS	získat
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
hodnocení	hodnocení	k1gNnPc4	hodnocení
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
Honour	Honoura	k1gFnPc2	Honoura
Moderations	Moderationsa	k1gFnPc2	Moderationsa
(	(	kIx(	(
<g/>
řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
latinské	latinský	k2eAgFnSc2d1	Latinská
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
hodnocení	hodnocení	k1gNnSc1	hodnocení
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
z	z	k7c2	z
Greats	Greatsa	k1gFnPc2	Greatsa
(	(	kIx(	(
<g/>
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
starověku	starověk	k1gInSc2	starověk
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
a	a	k8xC	a
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
hodnocení	hodnocení	k1gNnSc4	hodnocení
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výcviku	výcvik	k1gInSc2	výcvik
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
bydlel	bydlet	k5eAaImAgMnS	bydlet
Lewis	Lewis	k1gInSc4	Lewis
na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
praporčíkem	praporčík	k1gMnSc7	praporčík
"	"	kIx"	"
<g/>
Paddym	Paddymum	k1gNnPc2	Paddymum
<g/>
"	"	kIx"	"
Moorem	Moorma	k1gFnPc2	Moorma
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
spolu	spolu	k6eAd1	spolu
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
padne	padnout	k5eAaImIp3nS	padnout
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
postará	postarat	k5eAaPmIp3nS	postarat
o	o	k7c4	o
obě	dva	k4xCgFnPc4	dva
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Paddy	Paddy	k6eAd1	Paddy
byl	být	k5eAaImAgInS	být
zabit	zabit	k2eAgInSc1d1	zabit
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
a	a	k8xC	a
Lewis	Lewis	k1gInSc1	Lewis
dodržel	dodržet	k5eAaPmAgInS	dodržet
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Paddy	Paddy	k6eAd1	Paddy
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
představil	představit	k5eAaPmAgMnS	představit
Lewise	Lewis	k1gInSc6	Lewis
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
Jane	Jan	k1gMnSc5	Jan
Mooreové	Mooreové	k2eAgNnSc7d1	Mooreové
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
s	s	k7c7	s
Lewisem	Lewis	k1gInSc7	Lewis
brzy	brzy	k6eAd1	brzy
spřátelila	spřátelit	k5eAaPmAgFnS	spřátelit
<g/>
.	.	kIx.	.
</s>
<s>
Lewisovi	Lewis	k1gMnSc3	Lewis
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
Jane	Jan	k1gMnSc5	Jan
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
paní	paní	k1gFnSc7	paní
Mooreovou	Mooreův	k2eAgFnSc7d1	Mooreova
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Lewise	Lewise	k1gFnPc4	Lewise
zvlášť	zvlášť	k6eAd1	zvlášť
důležité	důležitý	k2eAgNnSc1d1	důležité
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
uzdravoval	uzdravovat	k5eAaImAgInS	uzdravovat
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
trpěl	trpět	k5eAaImAgMnS	trpět
téměř	téměř	k6eAd1	téměř
patologickou	patologický	k2eAgFnSc7d1	patologická
nechutí	nechuť	k1gFnSc7	nechuť
byť	byť	k8xS	byť
jen	jen	k9	jen
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
odejít	odejít	k5eAaPmF	odejít
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ho	on	k3xPp3gNnSc2	on
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
žil	žít	k5eAaImAgInS	žít
u	u	k7c2	u
paní	paní	k1gFnSc2	paní
Mooreové	Mooreová	k1gFnSc2	Mooreová
do	do	k7c2	do
konce	konec	k1gInSc2	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
hospitalizována	hospitalizován	k2eAgFnSc1d1	hospitalizována
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluvil	mluvit	k5eAaImAgMnS	mluvit
jako	jako	k8xC	jako
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
a	a	k8xC	a
také	také	k9	také
ji	on	k3xPp3gFnSc4	on
tak	tak	k6eAd1	tak
oslovoval	oslovovat	k5eAaImAgMnS	oslovovat
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
a	a	k8xC	a
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
a	a	k8xC	a
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
k	k	k7c3	k
paní	paní	k1gFnSc3	paní
Mooreové	Mooreová	k1gFnSc2	Mooreová
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
hluboký	hluboký	k2eAgInSc1d1	hluboký
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mohu	moct	k5eAaImIp1nS	moct
nebo	nebo	k8xC	nebo
mám	mít	k5eAaImIp1nS	mít
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mé	můj	k3xOp1gNnSc4	můj
dřívější	dřívější	k2eAgNnSc4d1	dřívější
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
k	k	k7c3	k
emocím	emoce	k1gFnPc3	emoce
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
a	a	k8xC	a
různorodě	různorodě	k6eAd1	různorodě
pomstěno	pomstěn	k2eAgNnSc1d1	pomstěn
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Georgi	Georg	k1gMnSc3	Georg
Sayerovi	Sayer	k1gMnSc3	Sayer
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
štědrá	štědrý	k2eAgFnSc1d1	štědrá
a	a	k8xC	a
mě	já	k3xPp1nSc4	já
také	také	k9	také
naučila	naučit	k5eAaPmAgFnS	naučit
být	být	k5eAaImF	být
štědrý	štědrý	k2eAgInSc4d1	štědrý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1917	[number]	k4	1917
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Arthuru	Arthur	k1gMnSc3	Arthur
Greevesovi	Greeves	k1gMnSc3	Greeves
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Jane	Jan	k1gMnSc5	Jan
jsou	být	k5eAaImIp3nP	být
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nejvíc	hodně	k6eAd3	hodně
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc1	paní
Mooreová	Mooreová	k1gFnSc1	Mooreová
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
Maureen	Maurena	k1gFnPc2	Maurena
a	a	k8xC	a
Warnie	Warnie	k1gFnPc4	Warnie
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Kilns	Kilnsa	k1gFnPc2	Kilnsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
domu	dům	k1gInSc2	dům
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Headington	Headington	k1gInSc4	Headington
Quarry	Quarra	k1gFnPc1	Quarra
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
Oxfordu	Oxford	k1gInSc2	Oxford
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
předměstí	předměstí	k1gNnSc2	předměstí
Risinghurstu	Risinghurst	k1gInSc2	Risinghurst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
finančně	finančně	k6eAd1	finančně
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
koupi	koupě	k1gFnSc4	koupě
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Warrena	Warreno	k1gNnSc2	Warreno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
zdědila	zdědit	k5eAaPmAgFnS	zdědit
Maureen	Maureen	k1gInSc4	Maureen
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Mooreová	Mooreový	k2eAgFnSc1d1	Mooreová
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
trpěla	trpět	k5eAaImAgFnS	trpět
demencí	demence	k1gFnSc7	demence
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
umístěna	umístit	k5eAaPmNgFnS	umístit
do	do	k7c2	do
sanatoria	sanatorium	k1gNnSc2	sanatorium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Lewis	Lewis	k1gInSc1	Lewis
poprvé	poprvé	k6eAd1	poprvé
přijel	přijet	k5eAaPmAgInS	přijet
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
zažil	zažít	k5eAaPmAgMnS	zažít
určitý	určitý	k2eAgInSc4d1	určitý
kulturní	kulturní	k2eAgInSc4d1	kulturní
šok	šok	k1gInSc4	šok
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Žádný	žádný	k3yNgMnSc1	žádný
Angličan	Angličan	k1gMnSc1	Angličan
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
pochopit	pochopit	k5eAaPmF	pochopit
mé	můj	k3xOp1gInPc4	můj
první	první	k4xOgInPc4	první
dojmy	dojem	k1gInPc4	dojem
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zaskočen	zaskočen	k2eAgInSc1d1	zaskočen
radostí	radost	k1gFnSc7	radost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ta	ten	k3xDgNnPc1	ten
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mě	já	k3xPp1nSc4	já
obklopovala	obklopovat	k5eAaImAgFnS	obklopovat
<g/>
,	,	kIx,	,
mi	já	k3xPp1nSc3	já
připomínala	připomínat	k5eAaImAgFnS	připomínat
hlasy	hlas	k1gInPc7	hlas
démonů	démon	k1gMnPc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tím	ten	k3xDgNnSc7	ten
nejhorším	zlý	k2eAgNnSc7d3	nejhorší
byla	být	k5eAaImAgFnS	být
krajina	krajina	k1gFnSc1	krajina
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
smířil	smířit	k5eAaPmAgInS	smířit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tenkrát	tenkrát	k6eAd1	tenkrát
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
odstraňování	odstraňování	k1gNnSc1	odstraňování
trvalo	trvat	k5eAaImAgNnS	trvat
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lewis	Lewis	k1gFnSc1	Lewis
se	se	k3xPyFc4	se
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
mládí	mládí	k1gNnSc2	mládí
zabýval	zabývat	k5eAaImAgInS	zabývat
severskou	severský	k2eAgFnSc4d1	severská
a	a	k8xC	a
řeckou	řecký	k2eAgFnSc4d1	řecká
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
ještě	ještě	k9	ještě
irská	irský	k2eAgFnSc1d1	irská
mytologie	mytologie	k1gFnSc1	mytologie
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Projevil	projevit	k5eAaPmAgMnS	projevit
také	také	k9	také
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
irský	irský	k2eAgInSc4d1	irský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
jen	jen	k9	jen
malé	malý	k2eAgNnSc4d1	malé
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
ho	on	k3xPp3gNnSc4	on
W.	W.	kA	W.
B.	B.	kA	B.
Yeats	Yeatsa	k1gFnPc2	Yeatsa
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poezii	poezie	k1gFnSc6	poezie
využíval	využívat	k5eAaPmAgMnS	využívat
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
irských	irský	k2eAgMnPc2d1	irský
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Lewis	Lewis	k1gFnSc2	Lewis
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Objevil	objevit	k5eAaPmAgMnS	objevit
jsem	být	k5eAaImIp1nS	být
zde	zde	k6eAd1	zde
autora	autor	k1gMnSc4	autor
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
jistě	jistě	k6eAd1	jistě
najdeš	najít	k5eAaPmIp2nS	najít
zalíbení	zalíbení	k1gNnSc4	zalíbení
<g/>
,	,	kIx,	,
W.	W.	kA	W.
B.	B.	kA	B.
Yeatse	Yeats	k1gMnSc2	Yeats
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
básně	báseň	k1gFnPc1	báseň
v	v	k7c6	v
tom	ten	k3xDgMnSc6	ten
vzácném	vzácný	k2eAgMnSc6d1	vzácný
duchu	duch	k1gMnSc6	duch
a	a	k8xC	a
kráse	krása	k1gFnSc6	krása
naší	náš	k3xOp1gFnSc2	náš
dávné	dávný	k2eAgFnSc2d1	dávná
irské	irský	k2eAgFnSc2d1	irská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
měl	mít	k5eAaImAgInS	mít
Lewis	Lewis	k1gInSc1	Lewis
dokonce	dokonce	k9	dokonce
dvojí	dvojí	k4xRgFnSc4	dvojí
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
s	s	k7c7	s
Yeatsem	Yeats	k1gInSc7	Yeats
setkat	setkat	k5eAaPmF	setkat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
básník	básník	k1gMnSc1	básník
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Překvapen	překvapit	k5eAaPmNgMnS	překvapit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
angličtí	anglický	k2eAgMnPc1d1	anglický
vrstevníci	vrstevník	k1gMnPc1	vrstevník
byli	být	k5eAaImAgMnP	být
vůči	vůči	k7c3	vůči
Yeatsovi	Yeats	k1gMnSc3	Yeats
a	a	k8xC	a
keltskému	keltský	k2eAgNnSc3d1	keltské
obrozeneckému	obrozenecký	k2eAgNnSc3d1	obrozenecké
hnutí	hnutí	k1gNnSc3	hnutí
lhostejní	lhostejnět	k5eAaImIp3nP	lhostejnět
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gInSc1	Lewis
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Často	často	k6eAd1	často
překvapeně	překvapeně	k6eAd1	překvapeně
zjišťuji	zjišťovat	k5eAaImIp1nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Yeats	Yeats	k1gInSc1	Yeats
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
ignorován	ignorovat	k5eAaImNgInS	ignorovat
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yQgFnPc7	který
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
jen	jen	k9	jen
Iry	Ir	k1gMnPc4	Ir
-	-	kIx~	-
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
děkuji	děkovat	k5eAaImIp1nS	děkovat
bohům	bůh	k1gMnPc3	bůh
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
Ir	Ir	k1gMnSc1	Ir
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
zaslání	zaslání	k1gNnSc4	zaslání
svých	svůj	k3xOyFgMnPc2	svůj
děl	dělo	k1gNnPc2	dělo
předním	přední	k2eAgMnSc7d1	přední
dublinským	dublinský	k2eAgMnSc7d1	dublinský
vydavatelům	vydavatel	k1gMnPc3	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
někdy	někdy	k6eAd1	někdy
pošlu	poslat	k5eAaPmIp1nS	poslat
své	svůj	k3xOyFgFnPc4	svůj
práce	práce	k1gFnPc4	práce
nějakému	nějaký	k3yIgMnSc3	nějaký
vydavateli	vydavatel	k1gMnSc3	vydavatel
<g/>
,	,	kIx,	,
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bych	by	kYmCp1nS	by
měl	mít	k5eAaImAgMnS	mít
zkusit	zkusit	k5eAaPmF	zkusit
to	ten	k3xDgNnSc4	ten
dublinské	dublinský	k2eAgNnSc4d1	Dublinské
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Maunsel	Maunsela	k1gFnPc2	Maunsela
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
zařadit	zařadit	k5eAaPmF	zařadit
k	k	k7c3	k
irské	irský	k2eAgFnSc3d1	irská
škole	škola	k1gFnSc3	škola
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
tíhly	tíhnout	k5eAaImAgInP	tíhnout
jeho	jeho	k3xOp3gInPc1	jeho
zájmy	zájem	k1gInPc1	zájem
ke	k	k7c3	k
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
spiritualitě	spiritualita	k1gFnSc3	spiritualita
a	a	k8xC	a
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
keltsky	keltsky	k6eAd1	keltsky
pohanského	pohanský	k2eAgInSc2d1	pohanský
mysticismu	mysticismus	k1gInSc2	mysticismus
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
tu	tu	k6eAd1	tu
a	a	k8xC	a
tam	tam	k6eAd1	tam
projevoval	projevovat	k5eAaImAgMnS	projevovat
lehce	lehko	k6eAd1	lehko
ironický	ironický	k2eAgInSc4d1	ironický
šovinismus	šovinismus	k1gInSc4	šovinismus
vůči	vůči	k7c3	vůči
Angličanům	Angličan	k1gMnPc3	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnPc1	setkání
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
irských	irský	k2eAgMnPc2d1	irský
přátel	přítel	k1gMnPc2	přítel
popsal	popsat	k5eAaPmAgMnS	popsat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
Irové	Ir	k1gMnPc1	Ir
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
potkají	potkat	k5eAaPmIp3nP	potkat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
skončili	skončit	k5eAaPmAgMnP	skončit
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
neuctivosti	neuctivost	k1gFnSc2	neuctivost
a	a	k8xC	a
zabedněnosti	zabedněnost	k1gFnSc2	zabedněnost
anglosaské	anglosaský	k2eAgFnSc2d1	anglosaská
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Irové	Ir	k1gMnPc1	Ir
bezpochyby	bezpochyby	k6eAd1	bezpochyby
jedinými	jediný	k2eAgMnPc7d1	jediný
skutečnými	skutečný	k2eAgMnPc7d1	skutečný
lidmi	člověk	k1gMnPc7	člověk
<g/>
...	...	k?	...
Nechtěl	chtít	k5eNaImAgMnS	chtít
bych	by	kYmCp1nS	by
žít	žít	k5eAaImF	žít
ani	ani	k8xC	ani
zemřít	zemřít	k5eAaPmF	zemřít
uprostřed	uprostřed	k7c2	uprostřed
jiného	jiný	k2eAgInSc2d1	jiný
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
kariéře	kariéra	k1gFnSc3	kariéra
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
však	však	k9	však
Lewis	Lewis	k1gInSc4	Lewis
skutečně	skutečně	k6eAd1	skutečně
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
uprostřed	uprostřed	k7c2	uprostřed
jiného	jiný	k2eAgInSc2d1	jiný
národa	národ	k1gInSc2	národ
a	a	k8xC	a
často	často	k6eAd1	často
litoval	litovat	k5eAaImAgMnS	litovat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
musel	muset	k5eAaImAgMnS	muset
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
společnost	společnost	k1gFnSc4	společnost
svých	svůj	k3xOyFgMnPc2	svůj
irských	irský	k2eAgMnPc2d1	irský
krajanů	krajan	k1gMnPc2	krajan
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
jezdil	jezdit	k5eAaImAgMnS	jezdit
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
i	i	k9	i
na	na	k7c6	na
svatební	svatební	k2eAgFnSc6d1	svatební
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
svým	svůj	k3xOyFgInSc7	svůj
irským	irský	k2eAgInSc7d1	irský
životem	život	k1gInSc7	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chodila	chodit	k5eAaImAgFnS	chodit
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
irské	irský	k2eAgFnSc3d1	irská
anglikánské	anglikánský	k2eAgFnSc3d1	anglikánská
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
Lewis	Lewis	k1gInSc1	Lewis
v	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
ateistou	ateista	k1gMnSc7	ateista
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zůstal	zůstat	k5eAaPmAgInS	zůstat
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
odcizení	odcizení	k1gNnSc1	odcizení
od	od	k7c2	od
křesťanství	křesťanství	k1gNnSc2	křesťanství
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
začal	začít	k5eAaPmAgMnS	začít
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
okultismus	okultismus	k1gInSc4	okultismus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
citoval	citovat	k5eAaBmAgMnS	citovat
Lucretia	Lucretius	k1gMnSc4	Lucretius
jako	jako	k9	jako
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
disponoval	disponovat	k5eAaBmAgInS	disponovat
těmi	ten	k3xDgInPc7	ten
nejlepšími	dobrý	k2eAgInPc7d3	nejlepší
argumenty	argument	k1gInPc7	argument
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ateismu	ateismus	k1gInSc2	ateismus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgMnS	být
Bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
křehký	křehký	k2eAgInSc1d1	křehký
a	a	k8xC	a
plný	plný	k2eAgInSc1d1	plný
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sám	sám	k3xTgMnSc1	sám
označoval	označovat	k5eAaImAgInS	označovat
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
ateistu	ateista	k1gMnSc4	ateista
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zaskočen	zaskočen	k2eAgInSc4d1	zaskočen
radostí	radost	k1gFnSc7	radost
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
rozzlobený	rozzlobený	k2eAgMnSc1d1	rozzlobený
na	na	k7c4	na
Boha	bůh	k1gMnSc4	bůh
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gFnSc1	jeho
záliba	záliba	k1gFnSc1	záliba
ve	v	k7c6	v
fantasy	fantas	k1gInPc1	fantas
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
mytologii	mytologie	k1gFnSc3	mytologie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
George	Georg	k1gMnSc2	Georg
MacDonalda	Macdonald	k1gMnSc2	Macdonald
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ho	on	k3xPp3gMnSc4	on
odvedlo	odvést	k5eAaPmAgNnS	odvést
od	od	k7c2	od
ateismu	ateismus	k1gInSc2	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mělo	mít	k5eAaImAgNnS	mít
postavení	postavení	k1gNnSc1	postavení
MacDonalda	Macdonald	k1gMnSc2	Macdonald
jako	jako	k8xC	jako
autora	autor	k1gMnSc2	autor
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
fantasy	fantas	k1gInPc1	fantas
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
Lewise	Lewis	k1gInSc6	Lewis
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
zejména	zejména	k9	zejména
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
kapitole	kapitola	k1gFnSc6	kapitola
knihy	kniha	k1gFnSc2	kniha
Velký	velký	k2eAgInSc1d1	velký
rozvod	rozvod	k1gInSc1	rozvod
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napůl	napůl	k6eAd1	napůl
autobiografický	autobiografický	k2eAgMnSc1d1	autobiografický
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
MacDonaldem	Macdonald	k1gMnSc7	Macdonald
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
mu	on	k3xPp3gMnSc3	on
třesoucím	třesoucí	k2eAgInSc7d1	třesoucí
hlasem	hlas	k1gInSc7	hlas
líčil	líčit	k5eAaImAgInS	líčit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
znamenaly	znamenat	k5eAaImAgFnP	znamenat
jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
všechno	všechen	k3xTgNnSc1	všechen
mi	já	k3xPp1nSc3	já
daly	dát	k5eAaPmAgInP	dát
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
mrazivé	mrazivý	k2eAgNnSc4d1	mrazivé
odpoledne	odpoledne	k1gNnSc4	odpoledne
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c4	v
Letterhead	Letterhead	k1gInSc4	Letterhead
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
výtisk	výtisk	k1gInSc1	výtisk
jeho	jeho	k3xOp3gMnPc2	jeho
Fantastů	fantast	k1gMnPc2	fantast
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc4	ten
mi	já	k3xPp1nSc3	já
tenkrát	tenkrát	k6eAd1	tenkrát
bylo	být	k5eAaImAgNnS	být
šestnáct	šestnáct	k4xCc1	šestnáct
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
bylo	být	k5eAaImAgNnS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	čí	k3xOyQgNnSc7	čí
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Danteho	Dante	k1gMnSc4	Dante
první	první	k4xOgInSc1	první
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Beatrice	Beatrice	k1gFnPc4	Beatrice
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
začíná	začínat	k5eAaImIp3nS	začínat
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nový	nový	k2eAgInSc1d1	nový
život	život	k1gInSc1	život
však	však	k9	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
dlouho	dlouho	k6eAd1	dlouho
jen	jen	k9	jen
v	v	k7c6	v
mých	můj	k3xOp1gFnPc6	můj
představách	představa	k1gFnPc6	představa
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
zdráhavě	zdráhavě	k6eAd1	zdráhavě
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgMnS	začít
připouštět	připouštět	k5eAaImF	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
víra	víra	k1gFnSc1	víra
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
představy	představa	k1gFnSc2	představa
neoddělitelná	oddělitelný	k2eNgFnSc1d1	neoddělitelná
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
jsem	být	k5eAaImIp1nS	být
zastíral	zastírat	k5eAaImAgMnS	zastírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mě	já	k3xPp1nSc4	já
v	v	k7c6	v
MacDonaldových	Macdonaldových	k2eAgFnPc6d1	Macdonaldových
knihách	kniha	k1gFnPc6	kniha
oslovilo	oslovit	k5eAaPmAgNnS	oslovit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svatost	svatost	k1gFnSc1	svatost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
54	[number]	k4	54
<g/>
)	)	kIx)	)
Ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
hádkami	hádka	k1gFnPc7	hádka
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
oxfordským	oxfordský	k2eAgMnSc7d1	oxfordský
kolegou	kolega	k1gMnSc7	kolega
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkienem	Tolkien	k1gInSc7	Tolkien
a	a	k8xC	a
knihou	kniha	k1gFnSc7	kniha
Nesmrtelný	nesmrtelný	k1gMnSc1	nesmrtelný
člověk	člověk	k1gMnSc1	člověk
od	od	k7c2	od
obráceného	obrácený	k2eAgMnSc2d1	obrácený
katolického	katolický	k2eAgMnSc2d1	katolický
autora	autor	k1gMnSc2	autor
G.	G.	kA	G.
K.	K.	kA	K.
Chestertona	Chestertona	k1gFnSc1	Chestertona
začal	začít	k5eAaPmAgInS	začít
znovu	znovu	k6eAd1	znovu
objevovat	objevovat	k5eAaImF	objevovat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Vzdoroval	vzdorovat	k5eAaImAgMnS	vzdorovat
urputně	urputně	k6eAd1	urputně
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
obrácení	obrácení	k1gNnSc2	obrácení
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stal	stát	k5eAaPmAgMnS	stát
jsem	být	k5eAaImIp1nS	být
se	s	k7c7	s
křesťanem	křesťan	k1gMnSc7	křesťan
kopaje	kopat	k5eAaImSgInS	kopat
a	a	k8xC	a
křiče	křičet	k5eAaImSgInS	křičet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Svůj	svůj	k3xOyFgInSc1	svůj
poslední	poslední	k2eAgInSc1d1	poslední
boj	boj	k1gInSc1	boj
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zaskočen	zaskočen	k2eAgInSc4d1	zaskočen
radostí	radost	k1gFnSc7	radost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
stal	stát	k5eAaPmAgMnS	stát
teistou	teista	k1gMnSc7	teista
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Lewis	Lewis	k1gFnSc2	Lewis
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
diskuzi	diskuze	k1gFnSc6	diskuze
a	a	k8xC	a
pozdní	pozdní	k2eAgFnSc6d1	pozdní
vycházce	vycházka	k1gFnSc6	vycházka
s	s	k7c7	s
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
Tolkienem	Tolkien	k1gMnSc7	Tolkien
a	a	k8xC	a
Hugem	Hugo	k1gMnSc7	Hugo
Dysonem	Dyson	k1gMnSc7	Dyson
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
zavázal	zavázat	k5eAaPmAgInS	zavázat
ke	k	k7c3	k
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
když	když	k8xS	když
šel	jít	k5eAaImAgMnS	jít
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
do	do	k7c2	do
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poněkud	poněkud	k6eAd1	poněkud
zklamalo	zklamat	k5eAaPmAgNnS	zklamat
Tolkiena	Tolkieno	k1gNnPc4	Tolkieno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Lewis	Lewis	k1gFnSc1	Lewis
stane	stanout	k5eAaPmIp3nS	stanout
katolíkem	katolík	k1gMnSc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
oddaný	oddaný	k2eAgMnSc1d1	oddaný
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
věřící	věřící	k1gMnSc1	věřící
zastával	zastávat	k5eAaImAgMnS	zastávat
Lewis	Lewis	k1gFnSc4	Lewis
pravověrnou	pravověrný	k2eAgFnSc4d1	pravověrná
teologii	teologie	k1gFnSc4	teologie
své	svůj	k3xOyFgFnSc2	svůj
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
apologetických	apologetický	k2eAgInPc6d1	apologetický
spisech	spis	k1gInPc6	spis
snažil	snažit	k5eAaImAgMnS	snažit
nehlásit	hlásit	k5eNaImF	hlásit
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
denominaci	denominace	k1gFnSc3	denominace
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
dílech	dílo	k1gNnPc6	dílo
zastával	zastávat	k5eAaImAgMnS	zastávat
myšlenky	myšlenka	k1gFnSc2	myšlenka
jako	jako	k8xC	jako
posmrtné	posmrtný	k2eAgNnSc1d1	posmrtné
očištění	očištění	k1gNnSc1	očištění
od	od	k7c2	od
lehkých	lehký	k2eAgInPc2d1	lehký
hříchů	hřích	k1gInPc2	hřích
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc4d1	velký
rozvod	rozvod	k1gInSc4	rozvod
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
<g/>
)	)	kIx)	)
a	a	k8xC	a
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
hřích	hřích	k1gInSc1	hřích
(	(	kIx(	(
<g/>
Rady	rada	k1gFnSc2	rada
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
katolické	katolický	k2eAgFnSc2d1	katolická
věrouky	věrouka	k1gFnSc2	věrouka
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
Lewis	Lewis	k1gInSc4	Lewis
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
pravověrného	pravověrný	k2eAgMnSc4d1	pravověrný
anglikána	anglikán	k1gMnSc4	anglikán
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
a	a	k8xC	a
odradily	odradit	k5eAaPmAgFnP	odradit
ho	on	k3xPp3gNnSc4	on
chvalozpěvy	chvalozpěv	k1gInPc1	chvalozpěv
a	a	k8xC	a
kázání	kázání	k1gNnSc1	kázání
nevalné	valný	k2eNgFnSc2d1	nevalná
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
poctu	pocta	k1gFnSc4	pocta
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
účastnit	účastnit	k5eAaImF	účastnit
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
s	s	k7c7	s
věřícími	věřící	k2eAgMnPc7d1	věřící
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
ošumělém	ošumělý	k2eAgNnSc6d1	ošumělé
oblečení	oblečení	k1gNnSc6	oblečení
a	a	k8xC	a
pracovních	pracovní	k2eAgFnPc6d1	pracovní
botách	bota	k1gFnPc6	bota
a	a	k8xC	a
zpívali	zpívat	k5eAaImAgMnP	zpívat
všechny	všechen	k3xTgInPc4	všechen
verše	verš	k1gInPc4	verš
chvalozpěvů	chvalozpěv	k1gInPc2	chvalozpěv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
si	se	k3xPyFc3	se
Lewis	Lewis	k1gInSc1	Lewis
dopisoval	dopisovat	k5eAaImAgInS	dopisovat
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
i	i	k9	i
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Joy	Joy	k1gFnSc7	Joy
Greshamovou	Greshamový	k2eAgFnSc7d1	Greshamová
<g/>
,	,	kIx,	,
americkou	americký	k2eAgFnSc7d1	americká
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
od	od	k7c2	od
ateismu	ateismus	k1gInSc2	ateismus
obrátila	obrátit	k5eAaPmAgFnS	obrátit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedla	rozvést	k5eAaPmAgFnS	rozvést
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
a	a	k8xC	a
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
přicestovala	přicestovat	k5eAaPmAgFnS	přicestovat
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dvěma	dva	k4xCgFnPc7	dva
syny	syn	k1gMnPc7	syn
Davidem	David	k1gMnSc7	David
a	a	k8xC	a
Douglasem	Douglas	k1gMnSc7	Douglas
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
ji	on	k3xPp3gFnSc4	on
nejprve	nejprve	k6eAd1	nejprve
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
společnici	společnice	k1gFnSc4	společnice
a	a	k8xC	a
osobní	osobní	k2eAgFnSc4d1	osobní
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
a	a	k8xC	a
zjevně	zjevně	k6eAd1	zjevně
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
občanský	občanský	k2eAgInSc1d1	občanský
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
i	i	k9	i
nadále	nadále	k6eAd1	nadále
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Lewisův	Lewisův	k2eAgMnSc1d1	Lewisův
bratr	bratr	k1gMnSc1	bratr
Warnie	Warnie	k1gFnSc2	Warnie
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
Jacka	Jacek	k1gMnSc4	Jacek
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
bezpochyby	bezpochyby	k6eAd1	bezpochyby
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
intelektuálně	intelektuálně	k6eAd1	intelektuálně
<g/>
.	.	kIx.	.
</s>
<s>
Joy	Joy	k?	Joy
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
...	...	k?	...
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
hodil	hodit	k5eAaImAgInS	hodit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jeho	jeho	k3xOp3gFnSc7	jeho
pružností	pružnost	k1gFnSc7	pružnost
<g/>
,	,	kIx,	,
šíří	šíř	k1gFnSc7	šíř
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
analytickým	analytický	k2eAgNnSc7d1	analytické
myšlením	myšlení	k1gNnSc7	myšlení
a	a	k8xC	a
především	především	k9	především
také	také	k9	také
vtipem	vtip	k1gInSc7	vtip
a	a	k8xC	a
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
ale	ale	k9	ale
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
na	na	k7c4	na
bolest	bolest	k1gFnSc4	bolest
v	v	k7c6	v
boku	bok	k1gInSc6	bok
a	a	k8xC	a
lékaři	lékař	k1gMnPc1	lékař
jí	on	k3xPp3gFnSc2	on
diagnostikovali	diagnostikovat	k5eAaBmAgMnP	diagnostikovat
nevyléčitelný	vyléčitelný	k2eNgInSc4d1	nevyléčitelný
nádor	nádor	k1gInSc4	nádor
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěli	chtít	k5eAaImAgMnP	chtít
uzavřít	uzavřít	k5eAaPmF	uzavřít
církevní	církevní	k2eAgInSc4d1	církevní
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
Joy	Joy	k1gFnSc1	Joy
byla	být	k5eAaImAgFnS	být
rozvedená	rozvedený	k2eAgFnSc1d1	rozvedená
<g/>
,	,	kIx,	,
nešlo	jít	k5eNaImAgNnS	jít
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
anglikánské	anglikánský	k2eAgFnSc6d1	anglikánská
církvi	církev	k1gFnSc6	církev
jednoduše	jednoduše	k6eAd1	jednoduše
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gMnSc1	jejich
přítel	přítel	k1gMnSc1	přítel
reverend	reverend	k1gMnSc1	reverend
Peter	Peter	k1gMnSc1	Peter
Bide	Bide	k1gInSc4	Bide
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
obřad	obřad	k1gInSc4	obřad
u	u	k7c2	u
jejího	její	k3xOp3gNnSc2	její
nemocničního	nemocniční	k2eAgNnSc2d1	nemocniční
lůžka	lůžko	k1gNnSc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
Greshamové	Greshamový	k2eAgFnSc2d1	Greshamová
nakrátko	nakrátko	k6eAd1	nakrátko
polevila	polevit	k5eAaPmAgFnS	polevit
a	a	k8xC	a
s	s	k7c7	s
Lewisem	Lewis	k1gInSc7	Lewis
žili	žít	k5eAaImAgMnP	žít
jako	jako	k8xS	jako
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
společně	společně	k6eAd1	společně
s	s	k7c7	s
Warrenem	Warren	k1gMnSc7	Warren
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
)	)	kIx)	)
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
zase	zase	k9	zase
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
a	a	k8xC	a
Greshamová	Greshamový	k2eAgFnSc1d1	Greshamová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
strávili	strávit	k5eAaPmAgMnP	strávit
krátkou	krátký	k2eAgFnSc4d1	krátká
dovolenou	dovolená	k1gFnSc4	dovolená
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
u	u	k7c2	u
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
miloval	milovat	k5eAaImAgInS	milovat
procházky	procházka	k1gFnPc4	procházka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
cestování	cestování	k1gNnSc1	cestování
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
jediná	jediný	k2eAgFnSc1d1	jediná
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
La	la	k1gNnSc4	la
Manche	Manch	k1gFnSc2	Manch
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Svědectví	svědectví	k1gNnSc2	svědectví
o	o	k7c6	o
zármutku	zármutek	k1gInSc6	zármutek
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gFnSc1	jeho
zkušenost	zkušenost	k1gFnSc1	zkušenost
s	s	k7c7	s
manželčinou	manželčin	k2eAgFnSc7d1	manželčina
smrtí	smrt	k1gFnSc7	smrt
tak	tak	k6eAd1	tak
osobně	osobně	k6eAd1	osobně
a	a	k8xC	a
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
otevřeností	otevřenost	k1gFnSc7	otevřenost
<g/>
,	,	kIx,	,
že	že	k8xS	že
knihu	kniha	k1gFnSc4	kniha
vydal	vydat	k5eAaPmAgInS	vydat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
N.	N.	kA	N.
W.	W.	kA	W.
Clerk	Clerk	k1gInSc4	Clerk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
čtenáři	čtenář	k1gMnPc1	čtenář
nespojovali	spojovat	k5eNaImAgMnP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
však	však	k9	však
Lewisovi	Lewis	k1gMnSc3	Lewis
doporučilo	doporučit	k5eAaPmAgNnS	doporučit
tolik	tolik	k6eAd1	tolik
přátel	přítel	k1gMnPc2	přítel
k	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
jeho	jeho	k3xOp3gInSc2	jeho
zármutku	zármutek	k1gInSc2	zármutek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
své	svůj	k3xOyFgNnSc4	svůj
autorství	autorství	k1gNnSc4	autorství
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
její	její	k3xOp3gMnPc4	její
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Douglas	Douglas	k1gInSc1	Douglas
Gresham	Gresham	k1gInSc1	Gresham
je	být	k5eAaImIp3nS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
křesťanem	křesťan	k1gMnSc7	křesťan
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
událostí	událost	k1gFnPc2	událost
okolo	okolo	k7c2	okolo
Lewisovy	Lewisův	k2eAgFnSc2d1	Lewisova
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
David	David	k1gMnSc1	David
Gresham	Gresham	k1gInSc1	Gresham
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
k	k	k7c3	k
židovské	židovský	k2eAgFnSc3d1	židovská
víře	víra	k1gFnSc3	víra
svých	svůj	k3xOyFgInPc2	svůj
předků	předek	k1gInPc2	předek
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odcizeni	odcizen	k2eAgMnPc1d1	odcizen
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
začaly	začít	k5eAaPmAgFnP	začít
Lewise	Lewise	k1gFnPc1	Lewise
sužovat	sužovat	k5eAaImF	sužovat
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
mu	on	k3xPp3gMnSc3	on
diagnostikován	diagnostikovat	k5eAaBmNgInS	diagnostikovat
zánět	zánět	k1gInSc1	zánět
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
otravu	otrava	k1gFnSc4	otrava
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
nemoci	nemoc	k1gFnSc3	nemoc
zameškal	zameškat	k5eAaPmAgInS	zameškat
podzimní	podzimní	k2eAgInSc1d1	podzimní
semestr	semestr	k1gInSc1	semestr
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Lewisův	Lewisův	k2eAgInSc1d1	Lewisův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
zlepšoval	zlepšovat	k5eAaImAgInS	zlepšovat
a	a	k8xC	a
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
svého	svůj	k1gMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
George	Georg	k1gMnSc2	Georg
Sayera	Sayer	k1gMnSc2	Sayer
byl	být	k5eAaImAgInS	být
Lewis	Lewis	k1gInSc4	Lewis
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
zcela	zcela	k6eAd1	zcela
zdráv	zdráv	k2eAgMnSc1d1	zdráv
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
však	však	k9	však
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
v	v	k7c4	v
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
infarkt	infarkt	k1gInSc4	infarkt
a	a	k8xC	a
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
ve	v	k7c4	v
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
odpoledne	odpoledne	k1gNnSc2	odpoledne
se	se	k3xPyFc4	se
nečekaně	nečekaně	k6eAd1	nečekaně
probral	probrat	k5eAaPmAgMnS	probrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
se	se	k3xPyFc4	se
Lewis	Lewis	k1gInSc1	Lewis
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Kilns	Kilnsa	k1gFnPc2	Kilnsa
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nedovolil	dovolit	k5eNaPmAgInS	dovolit
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Lewisův	Lewisův	k2eAgInSc1d1	Lewisův
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
mu	on	k3xPp3gMnSc3	on
lékaři	lékař	k1gMnPc1	lékař
diagnostikovali	diagnostikovat	k5eAaBmAgMnP	diagnostikovat
konečnou	konečný	k2eAgFnSc4d1	konečná
fázi	fáze	k1gFnSc3	fáze
selhání	selhání	k1gNnSc6	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
v	v	k7c4	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
Lewis	Lewis	k1gInSc1	Lewis
zkolaboval	zkolabovat	k5eAaPmAgInS	zkolabovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ložnici	ložnice	k1gFnSc6	ložnice
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
týden	týden	k1gInSc4	týden
před	před	k7c7	před
svými	svůj	k3xOyFgNnPc7	svůj
nedožitými	dožitý	k2eNgNnPc7d1	nedožité
65	[number]	k4	65
<g/>
.	.	kIx.	.
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Holy	hola	k1gFnSc2	hola
Trinity	Trinita	k1gFnSc2	Trinita
Church	Churcha	k1gFnPc2	Churcha
v	v	k7c6	v
Headingtonu	Headington	k1gInSc6	Headington
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
byly	být	k5eAaImAgFnP	být
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zastíněny	zastíněn	k2eAgFnPc1d1	zastíněna
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
vraždě	vražda	k1gFnSc6	vražda
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
J.	J.	kA	J.
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
Aldouse	Aldouse	k1gFnSc2	Aldouse
Huxleyho	Huxley	k1gMnSc2	Huxley
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
knihy	kniha	k1gFnSc2	kniha
Konec	konec	k1gInSc4	konec
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
shoda	shoda	k1gFnSc1	shoda
okolností	okolnost	k1gFnPc2	okolnost
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Petera	Peter	k1gMnSc4	Peter
Kreefta	Kreeft	k1gMnSc4	Kreeft
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
knihy	kniha	k1gFnSc2	kniha
Mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
peklem	peklo	k1gNnSc7	peklo
<g/>
:	:	kIx,	:
Rozhovor	rozhovor	k1gInSc1	rozhovor
někde	někde	k6eAd1	někde
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gInSc4	Lewis
<g/>
,	,	kIx,	,
Aldous	Aldous	k1gInSc4	Aldous
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
.	.	kIx.	.
</s>
<s>
Památka	památka	k1gFnSc1	památka
C.	C.	kA	C.
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
je	být	k5eAaImIp3nS	být
připomínána	připomínat	k5eAaImNgFnS	připomínat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
kalendáři	kalendář	k1gInSc6	kalendář
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
na	na	k7c4	na
Magdalene	Magdalen	k1gInSc5	Magdalen
College	College	k1gNnPc3	College
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
téměř	téměř	k6eAd1	téměř
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
profesorem	profesor	k1gMnSc7	profesor
středověké	středověký	k2eAgFnSc2d1	středověká
a	a	k8xC	a
renesanční	renesanční	k2eAgFnSc2d1	renesanční
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
a	a	k8xC	a
členem	člen	k1gInSc7	člen
Magdalene	Magdalen	k1gInSc5	Magdalen
College	College	k1gFnPc2	College
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgNnSc2	takový
jako	jako	k8xS	jako
anglická	anglický	k2eAgFnSc1d1	anglická
renesance	renesance	k1gFnSc1	renesance
nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
pozdní	pozdní	k2eAgInSc4d1	pozdní
středověk	středověk	k1gInSc4	středověk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
potom	potom	k6eAd1	potom
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
alegorie	alegorie	k1gFnSc2	alegorie
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
The	The	k1gFnSc2	The
Allegory	Allegora	k1gFnSc2	Allegora
of	of	k?	of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Alegorie	alegorie	k1gFnSc2	alegorie
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
pomohla	pomoct	k5eAaPmAgFnS	pomoct
oživit	oživit	k5eAaPmF	oživit
vážné	vážný	k2eAgNnSc4d1	vážné
studium	studium	k1gNnSc4	studium
pozdně	pozdně	k6eAd1	pozdně
středověkých	středověký	k2eAgInPc2d1	středověký
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
francouzská	francouzský	k2eAgFnSc1d1	francouzská
báseň	báseň	k1gFnSc1	báseň
Roman	Roman	k1gMnSc1	Roman
de	de	k?	de
la	la	k1gNnSc2	la
Rosa	Rosa	k1gFnSc1	Rosa
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
napsal	napsat	k5eAaPmAgInS	napsat
několik	několik	k4yIc4	několik
předmluv	předmluva	k1gFnPc2	předmluva
ke	k	k7c3	k
starým	starý	k2eAgInPc3d1	starý
literárním	literární	k2eAgInPc3d1	literární
dílům	díl	k1gInPc3	díl
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
Layamonově	Layamonův	k2eAgMnSc6d1	Layamonův
Brutovi	Brut	k1gMnSc6	Brut
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
A	a	k8xC	a
Preface	preface	k1gFnSc1	preface
to	ten	k3xDgNnSc4	ten
Paradise	Paradise	k1gFnPc1	Paradise
Lost	Losta	k1gFnPc2	Losta
(	(	kIx(	(
<g/>
Předmluva	předmluva	k1gFnSc1	předmluva
ke	k	k7c3	k
Ztracenému	ztracený	k2eAgInSc3d1	ztracený
ráji	ráj	k1gInSc3	ráj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejhodnotnějších	hodnotný	k2eAgFnPc2d3	nejhodnotnější
kritik	kritika	k1gFnPc2	kritika
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
poslední	poslední	k2eAgNnSc4d1	poslední
akademické	akademický	k2eAgNnSc4d1	akademické
dílo	dílo	k1gNnSc4	dílo
The	The	k1gMnSc2	The
Discarded	Discarded	k1gMnSc1	Discarded
Image	image	k1gFnSc1	image
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
Medieval	medieval	k1gInSc1	medieval
and	and	k?	and
Renaissance	Renaissance	k1gFnSc2	Renaissance
Literature	Literatur	k1gMnSc5	Literatur
(	(	kIx(	(
<g/>
Vyřazený	vyřazený	k2eAgInSc1d1	vyřazený
obraz	obraz	k1gInSc1	obraz
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
středověké	středověký	k2eAgFnSc2d1	středověká
a	a	k8xC	a
renesanční	renesanční	k2eAgFnSc2d1	renesanční
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
shrnutím	shrnutí	k1gNnSc7	shrnutí
středověkého	středověký	k2eAgInSc2d1	středověký
světonázoru	světonázor	k1gInSc2	světonázor
"	"	kIx"	"
<g/>
vyřazeného	vyřazený	k2eAgInSc2d1	vyřazený
obrazu	obraz	k1gInSc2	obraz
<g/>
"	"	kIx"	"
kosmu	kosmos	k1gInSc2	kosmos
z	z	k7c2	z
názvu	název	k1gInSc2	název
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
byl	být	k5eAaImAgMnS	být
plodným	plodný	k2eAgMnSc7d1	plodný
autorem	autor	k1gMnSc7	autor
a	a	k8xC	a
okruh	okruh	k1gInSc4	okruh
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
spisovatelů	spisovatel	k1gMnPc2	spisovatel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
neformální	formální	k2eNgFnSc7d1	neformální
diskuzní	diskuzní	k2eAgFnSc7d1	diskuzní
společností	společnost	k1gFnSc7	společnost
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xS	jako
Inklings	Inklings	k1gInSc1	Inklings
(	(	kIx(	(
<g/>
Tušitelé	Tušitel	k1gMnPc1	Tušitel
nebo	nebo	k8xC	nebo
Inkousťata	Inkoustě	k1gNnPc1	Inkoustě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
sem	sem	k6eAd1	sem
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Owen	Owen	k1gMnSc1	Owen
Barfield	Barfield	k1gMnSc1	Barfield
a	a	k8xC	a
Lewisův	Lewisův	k2eAgMnSc1d1	Lewisův
bratr	bratr	k1gMnSc1	bratr
Warnie	Warnie	k1gFnSc2	Warnie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
byl	být	k5eAaImAgInS	být
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
učitelem	učitel	k1gMnSc7	učitel
básníka	básník	k1gMnSc2	básník
Johna	John	k1gMnSc2	John
Betjemana	Betjeman	k1gMnSc2	Betjeman
<g/>
,	,	kIx,	,
kritika	kritik	k1gMnSc2	kritik
Kennetha	Kenneth	k1gMnSc2	Kenneth
Tynana	Tynan	k1gMnSc2	Tynan
<g/>
,	,	kIx,	,
mystika	mystik	k1gMnSc2	mystik
Bedeho	Bede	k1gMnSc2	Bede
Griffithse	Griffiths	k1gMnSc2	Griffiths
a	a	k8xC	a
sufistického	sufistický	k2eAgMnSc2d1	sufistický
vědce	vědec	k1gMnSc2	vědec
Martina	Martin	k1gMnSc2	Martin
Lingse	Lings	k1gMnSc2	Lings
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
věřící	věřící	k2eAgMnSc1d1	věřící
a	a	k8xC	a
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
Betjeman	Betjeman	k1gMnSc1	Betjeman
Lewise	Lewise	k1gFnSc2	Lewise
nesnášel	snášet	k5eNaImAgMnS	snášet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Tynan	Tynana	k1gFnPc2	Tynana
stojící	stojící	k2eAgFnSc1d1	stojící
proti	proti	k7c3	proti
zavedenému	zavedený	k2eAgInSc3d1	zavedený
řádu	řád	k1gInSc3	řád
ho	on	k3xPp3gInSc4	on
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Tolkienovi	Tolkien	k1gMnSc6	Tolkien
píše	psát	k5eAaImIp3nS	psát
Lewis	Lewis	k1gInSc1	Lewis
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zaskočen	zaskočen	k2eAgInSc4d1	zaskočen
radostí	radost	k1gFnSc7	radost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
svých	svůj	k3xOyFgFnPc2	svůj
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
napsal	napsat	k5eAaBmAgMnS	napsat
Lewis	Lewis	k1gInSc4	Lewis
také	také	k9	také
množství	množství	k1gNnSc4	množství
populárních	populární	k2eAgInPc2d1	populární
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
Kosmické	kosmický	k2eAgFnSc2d1	kosmická
trilogie	trilogie	k1gFnSc2	trilogie
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
knih	kniha	k1gFnPc2	kniha
o	o	k7c4	o
Narnii	Narnie	k1gFnSc4	Narnie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
implicitně	implicitně	k6eAd1	implicitně
zabývá	zabývat	k5eAaImIp3nS	zabývat
křesťanskými	křesťanský	k2eAgNnPc7d1	křesťanské
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
pád	pád	k1gInSc4	pád
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
vykoupení	vykoupení	k1gNnSc6	vykoupení
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
Lewisovým	Lewisový	k2eAgInSc7d1	Lewisový
románem	román	k1gInSc7	román
napsaným	napsaný	k2eAgInSc7d1	napsaný
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
je	být	k5eAaImIp3nS	být
Poutníkův	poutníkův	k2eAgInSc4d1	poutníkův
návrat	návrat	k1gInSc4	návrat
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
tematicky	tematicky	k6eAd1	tematicky
podobné	podobný	k2eAgFnSc6d1	podobná
Poutníkově	poutníkův	k2eAgFnSc6d1	Poutníkova
cestě	cesta	k1gFnSc6	cesta
Johna	John	k1gMnSc2	John
Bunyana	Bunyan	k1gMnSc2	Bunyan
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
autorovu	autorův	k2eAgFnSc4d1	autorova
osobní	osobní	k2eAgFnSc4d1	osobní
zkušenost	zkušenost	k1gFnSc4	zkušenost
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vydání	vydání	k1gNnSc2	vydání
ostře	ostro	k6eAd1	ostro
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Lewisovy	Lewisův	k2eAgInPc1d1	Lewisův
romány	román	k1gInPc1	román
z	z	k7c2	z
Kosmické	kosmický	k2eAgFnSc2d1	kosmická
trilogie	trilogie	k1gFnSc2	trilogie
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Lewis	Lewis	k1gInSc4	Lewis
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
odlidšťující	odlidšťující	k2eAgInPc4d1	odlidšťující
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
vědeckofantastické	vědeckofantastický	k2eAgFnSc6d1	vědeckofantastická
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
mlčící	mlčící	k2eAgFnSc2d1	mlčící
planety	planeta	k1gFnSc2	planeta
Lewis	Lewis	k1gFnSc2	Lewis
očividně	očividně	k6eAd1	očividně
napsal	napsat	k5eAaBmAgMnS	napsat
podle	podle	k7c2	podle
rozhovoru	rozhovor	k1gInSc2	rozhovor
o	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
trendech	trend	k1gInPc6	trend
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkienem	Tolkien	k1gInSc7	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Domluvili	domluvit	k5eAaPmAgMnP	domluvit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lewis	Lewis	k1gFnSc1	Lewis
napíše	napsat	k5eAaPmIp3nS	napsat
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
cestování	cestování	k1gNnSc6	cestování
kosmem	kosmos	k1gInSc7	kosmos
a	a	k8xC	a
Tolkien	Tolkien	k1gInSc1	Tolkien
o	o	k7c4	o
cestování	cestování	k1gNnSc4	cestování
časem	časem	k6eAd1	časem
<g/>
.	.	kIx.	.
</s>
<s>
Tolkienův	Tolkienův	k2eAgInSc1d1	Tolkienův
příběh	příběh	k1gInSc1	příběh
The	The	k1gFnSc2	The
Lost	Lost	k2eAgInSc1d1	Lost
Road	Road	k1gInSc1	Road
(	(	kIx(	(
<g/>
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
spojující	spojující	k2eAgFnSc1d1	spojující
mytologii	mytologie	k1gFnSc4	mytologie
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
moderní	moderní	k2eAgInSc4d1	moderní
svět	svět	k1gInSc4	svět
autor	autor	k1gMnSc1	autor
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Lewisova	Lewisův	k2eAgFnSc1d1	Lewisova
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
Ransom	Ransom	k1gInSc1	Ransom
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
Tolkienovi	Tolkien	k1gMnSc6	Tolkien
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
sám	sám	k3xTgMnSc1	sám
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
naráží	narážet	k5eAaPmIp3nS	narážet
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
The	The	k1gFnPc6	The
Letters	Letters	k1gInSc4	Letters
of	of	k?	of
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
(	(	kIx(	(
<g/>
Dopisy	dopis	k1gInPc1	dopis
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
román	román	k1gInSc1	román
Perelandra	Perelandr	k1gMnSc2	Perelandr
líčí	líčit	k5eAaImIp3nS	líčit
novou	nový	k2eAgFnSc4d1	nová
zahradu	zahrada	k1gFnSc4	zahrada
v	v	k7c6	v
Edenu	Eden	k1gInSc6	Eden
<g/>
,	,	kIx,	,
nového	nový	k2eAgMnSc4d1	nový
Adama	Adam	k1gMnSc4	Adam
a	a	k8xC	a
Evu	Eva	k1gFnSc4	Eva
a	a	k8xC	a
nového	nový	k2eAgMnSc2d1	nový
"	"	kIx"	"
<g/>
hada	had	k1gMnSc2	had
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	on	k3xPp3gNnSc4	on
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nahlížet	nahlížet	k5eAaImF	nahlížet
jako	jako	k9	jako
na	na	k7c4	na
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
"	"	kIx"	"
<g/>
naše	náš	k3xOp1gFnSc1	náš
Eva	Eva	k1gFnSc1	Eva
<g/>
"	"	kIx"	"
lépe	dobře	k6eAd2	dobře
odolávala	odolávat	k5eAaImAgFnS	odolávat
hadovu	hadův	k2eAgFnSc4d1	Hadova
pokušení	pokušení	k1gNnSc4	pokušení
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
román	román	k1gInSc1	román
trilogie	trilogie	k1gFnSc2	trilogie
Ta	ten	k3xDgFnSc1	ten
obludná	obludný	k2eAgFnSc1d1	obludná
síla	síla	k1gFnSc1	síla
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
množství	množství	k1gNnSc1	množství
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
Tolkienův	Tolkienův	k2eAgInSc4d1	Tolkienův
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
svět	svět	k1gInSc4	svět
Středozemi	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
prezentovány	prezentovat	k5eAaBmNgFnP	prezentovat
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
Té	ten	k3xDgFnSc6	ten
obludné	obludný	k2eAgFnSc6d1	obludná
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dramatizovanými	dramatizovaný	k2eAgInPc7d1	dramatizovaný
argumenty	argument	k1gInPc7	argument
mnohem	mnohem	k6eAd1	mnohem
formálněji	formálně	k6eAd2	formálně
uvedenými	uvedený	k2eAgInPc7d1	uvedený
v	v	k7c6	v
Lewisově	Lewisův	k2eAgFnSc6d1	Lewisova
knize	kniha	k1gFnSc6	kniha
Zničení	zničení	k1gNnPc2	zničení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
sérii	série	k1gFnSc4	série
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měl	mít	k5eAaImAgInS	mít
Lewis	Lewis	k1gInSc1	Lewis
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Durhamu	Durham	k1gInSc6	Durham
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
sestavené	sestavený	k2eAgFnSc6d1	sestavená
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
vůči	vůči	k7c3	vůči
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
hnutí	hnutí	k1gNnSc4	hnutí
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odlidšťuje	odlidšťovat	k5eAaImIp3nS	odlidšťovat
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
bydlel	bydlet	k5eAaImAgInS	bydlet
v	v	k7c6	v
Durhamu	Durham	k1gInSc6	Durham
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gNnSc4	on
uchvátila	uchvátit	k5eAaPmAgFnS	uchvátit
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
obludná	obludný	k2eAgFnSc1d1	obludná
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
situována	situovat	k5eAaBmNgFnS	situovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Durhamské	Durhamský	k2eAgFnSc2d1	Durhamská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
další	další	k2eAgInSc1d1	další
vědeckofantastický	vědeckofantastický	k2eAgInSc1d1	vědeckofantastický
román	román	k1gInSc1	román
Temná	temnat	k5eAaImIp3nS	temnat
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
ho	on	k3xPp3gMnSc4	on
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
měl	mít	k5eAaImAgMnS	mít
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
série	série	k1gFnSc2	série
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgFnPc4d1	předchozí
tři	tři	k4xCgFnPc4	tři
dokončené	dokončený	k2eAgFnPc4d1	dokončená
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc3	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
potom	potom	k6eAd1	potom
vyvstaly	vyvstat	k5eAaPmAgInP	vyvstat
spory	spor	k1gInPc1	spor
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
autentičnost	autentičnost	k1gFnSc4	autentičnost
<g/>
.	.	kIx.	.
</s>
<s>
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
jsou	být	k5eAaImIp3nP	být
cyklem	cyklus	k1gInSc7	cyklus
sedmi	sedm	k4xCc2	sedm
fantasy	fantas	k1gInPc4	fantas
románů	román	k1gInPc2	román
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
klasiku	klasika	k1gFnSc4	klasika
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949-1954	[number]	k4	1949-1954
a	a	k8xC	a
ilustrován	ilustrován	k2eAgInSc1d1	ilustrován
Pauline	Paulin	k1gInSc5	Paulin
Baynesovou	Baynesový	k2eAgFnSc4d1	Baynesová
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
je	být	k5eAaImIp3nS	být
Lewisovým	Lewisový	k2eAgNnSc7d1	Lewisové
nejpopulárnějším	populární	k2eAgNnSc7d3	nejpopulárnější
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
v	v	k7c6	v
41	[number]	k4	41
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
několik	několik	k4yIc1	několik
úplných	úplný	k2eAgFnPc2d1	úplná
nebo	nebo	k8xC	nebo
částečných	částečný	k2eAgFnPc2d1	částečná
adaptací	adaptace	k1gFnPc2	adaptace
pro	pro	k7c4	pro
rozhlas	rozhlas	k1gInSc4	rozhlas
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnSc4d1	divadelní
jeviště	jeviště	k1gNnSc4	jeviště
a	a	k8xC	a
kinosály	kinosál	k1gInPc4	kinosál
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
o	o	k7c6	o
doporučeném	doporučený	k2eAgNnSc6d1	Doporučené
pořadí	pořadí	k1gNnSc6	pořadí
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
celé	celý	k2eAgFnSc2d1	celá
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
často	často	k6eAd1	často
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Douglas	Douglas	k1gInSc1	Douglas
Gresham	Gresham	k1gInSc4	Gresham
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lewis	Lewis	k1gFnSc4	Lewis
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
knihy	kniha	k1gFnPc1	kniha
četly	číst	k5eAaImAgFnP	číst
podle	podle	k7c2	podle
"	"	kIx"	"
<g/>
narnijské	narnijský	k2eAgFnSc2d1	narnijská
chronologie	chronologie	k1gFnSc2	chronologie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
množství	množství	k1gNnSc1	množství
narážek	narážka	k1gFnPc2	narážka
na	na	k7c4	na
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
snadno	snadno	k6eAd1	snadno
dostupné	dostupný	k2eAgFnPc1d1	dostupná
pro	pro	k7c4	pro
mladší	mladý	k2eAgMnPc4d2	mladší
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
hutné	hutný	k2eAgInPc1d1	hutný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
číst	číst	k5eAaImF	číst
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
dobrodružnost	dobrodružnost	k1gFnSc4	dobrodružnost
<g/>
,	,	kIx,	,
barevnost	barevnost	k1gFnSc4	barevnost
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
mohou	moct	k5eAaImIp3nP	moct
oblíbit	oblíbit	k5eAaPmF	oblíbit
děti	dítě	k1gFnPc4	dítě
i	i	k8xC	i
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
křesťané	křesťan	k1gMnPc1	křesťan
i	i	k8xC	i
nekřesťané	nekřesťan	k1gMnPc1	nekřesťan
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
myšlenek	myšlenka	k1gFnPc2	myšlenka
využívá	využívat	k5eAaPmIp3nS	využívat
Lewis	Lewis	k1gFnPc4	Lewis
také	také	k9	také
postavy	postava	k1gFnPc4	postava
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
římské	římský	k2eAgFnSc2d1	římská
mytologie	mytologie	k1gFnSc2	mytologie
i	i	k9	i
britských	britský	k2eAgFnPc2d1	britská
a	a	k8xC	a
irských	irský	k2eAgFnPc2d1	irská
lidových	lidový	k2eAgFnPc2d1	lidová
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
údajně	údajně	k6eAd1	údajně
zasadil	zasadit	k5eAaPmAgInS	zasadit
své	svůj	k3xOyFgNnSc4	svůj
zobrazení	zobrazení	k1gNnSc4	zobrazení
Narnie	Narnie	k1gFnSc1	Narnie
do	do	k7c2	do
hornaté	hornatý	k2eAgFnSc2d1	hornatá
krajiny	krajina	k1gFnSc2	krajina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
části	část	k1gFnSc2	část
Rostrevoru	Rostrevor	k1gInSc2	Rostrevor
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
Carlingfordské	Carlingfordský	k2eAgNnSc4d1	Carlingfordský
jezero	jezero	k1gNnSc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc4	Lewis
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
této	tento	k3xDgFnSc2	tento
série	série	k1gFnSc2	série
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
pohádkami	pohádka	k1gFnPc7	pohádka
George	Georg	k1gMnSc2	Georg
MacDonalda	Macdonald	k1gMnSc2	Macdonald
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
napsal	napsat	k5eAaPmAgInS	napsat
množství	množství	k1gNnSc4	množství
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
peklem	peklo	k1gNnSc7	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
rozvod	rozvod	k1gInSc1	rozvod
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
vydaná	vydaný	k2eAgFnSc1d1	vydaná
jako	jako	k8xS	jako
Velký	velký	k2eAgInSc1d1	velký
rozvod	rozvod	k1gInSc1	rozvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krátkou	krátký	k2eAgFnSc7d1	krátká
novelou	novela	k1gFnSc7	novela
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
obyvatel	obyvatel	k1gMnPc2	obyvatel
pekla	peklo	k1gNnSc2	peklo
účastní	účastnit	k5eAaImIp3nS	účastnit
autobusového	autobusový	k2eAgInSc2d1	autobusový
zájezdu	zájezd	k1gInSc2	zájezd
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
zůstat	zůstat	k5eAaPmF	zůstat
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
nazvat	nazvat	k5eAaPmF	nazvat
namísto	namísto	k7c2	namísto
pekla	peklo	k1gNnSc2	peklo
očistcem	očistec	k1gInSc7	očistec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většině	většina	k1gFnSc3	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
Snoubení	snoubení	k1gNnSc4	snoubení
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
od	od	k7c2	od
Williama	Williamum	k1gNnSc2	Williamum
Blakea	Blakeum	k1gNnSc2	Blakeum
<g/>
,	,	kIx,	,
koncept	koncept	k1gInSc1	koncept
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Lewis	Lewis	k1gInSc1	Lewis
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
katastrofální	katastrofální	k2eAgInSc4d1	katastrofální
omyl	omyl	k1gInSc4	omyl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vědomě	vědomě	k6eAd1	vědomě
odráží	odrážet	k5eAaImIp3nS	odrážet
dvě	dva	k4xCgNnPc4	dva
jiná	jiný	k2eAgNnPc4d1	jiné
známá	známý	k2eAgNnPc4d1	známé
díla	dílo	k1gNnPc4	dílo
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
Božskou	božský	k2eAgFnSc4d1	božská
komedii	komedie	k1gFnSc4	komedie
Danta	Dante	k1gMnSc4	Dante
Alighieriho	Alighieri	k1gMnSc4	Alighieri
a	a	k8xC	a
Bunyanovu	Bunyanův	k2eAgFnSc4d1	Bunyanův
Poutníkovu	poutníkův	k2eAgFnSc4d1	Poutníkova
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
krátké	krátký	k2eAgNnSc1d1	krátké
dílo	dílo	k1gNnSc1	dílo
Rady	rada	k1gFnSc2	rada
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
ďábla	ďábel	k1gMnSc2	ďábel
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
zkušený	zkušený	k2eAgMnSc1d1	zkušený
démon	démon	k1gMnSc1	démon
Zmarchrob	Zmarchroba	k1gFnPc2	Zmarchroba
radí	radit	k5eAaImIp3nS	radit
svému	svůj	k3xOyFgMnSc3	svůj
synovci	synovec	k1gMnSc3	synovec
Tasemníkovi	Tasemník	k1gMnSc3	Tasemník
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nejlépe	dobře	k6eAd3	dobře
pokoušet	pokoušet	k5eAaImF	pokoušet
určitého	určitý	k2eAgMnSc4d1	určitý
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
postarat	postarat	k5eAaPmF	postarat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zatracení	zatracení	k1gNnSc6	zatracení
<g/>
.	.	kIx.	.
</s>
<s>
Lewisovým	Lewisový	k2eAgInSc7d1	Lewisový
posledním	poslední	k2eAgInSc7d1	poslední
románem	román	k1gInSc7	román
je	být	k5eAaImIp3nS	být
Dokud	dokud	k8xS	dokud
nemáme	mít	k5eNaImIp1nP	mít
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
sám	sám	k3xTgMnSc1	sám
knihu	kniha	k1gFnSc4	kniha
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
a	a	k8xC	a
nejvyspělejší	vyspělý	k2eAgNnSc4d3	nejvyspělejší
prozaické	prozaický	k2eAgNnSc4d1	prozaické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
u	u	k7c2	u
čtenářů	čtenář	k1gMnPc2	čtenář
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
převyprávění	převyprávění	k1gNnSc6	převyprávění
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
Erótovi	Erós	k1gMnSc6	Erós
a	a	k8xC	a
Psýché	Psýché	k1gFnSc6	Psýché
z	z	k7c2	z
neobvyklého	obvyklý	k2eNgInSc2d1	neobvyklý
pohledu	pohled	k1gInSc2	pohled
sestry	sestra	k1gFnPc4	sestra
Psýché	Psýché	k1gFnPc2	Psýché
<g/>
.	.	kIx.	.
</s>
<s>
Zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
myšlenkami	myšlenka	k1gFnPc7	myšlenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zasazení	zasazení	k1gNnSc1	zasazení
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
pohanské	pohanský	k2eAgInPc4d1	pohanský
a	a	k8xC	a
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
doktrínami	doktrína	k1gFnPc7	doktrína
je	být	k5eAaImIp3nS	být
ponecháno	ponechán	k2eAgNnSc1d1	ponecháno
pouze	pouze	k6eAd1	pouze
implicitně	implicitně	k6eAd1	implicitně
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
Lewis	Lewis	k1gInSc1	Lewis
konvertoval	konvertovat	k5eAaBmAgInS	konvertovat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
dvě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
básní	básnit	k5eAaImIp3nS	básnit
Spirits	Spirits	k1gInSc1	Spirits
in	in	k?	in
Bondage	Bondage	k1gInSc1	Bondage
(	(	kIx(	(
<g/>
Duchové	duch	k1gMnPc1	duch
v	v	k7c6	v
otroctví	otroctví	k1gNnSc6	otroctví
<g/>
)	)	kIx)	)
a	a	k8xC	a
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výpravnou	výpravný	k2eAgFnSc4d1	výpravná
báseň	báseň	k1gFnSc4	báseň
Dymer	Dymra	k1gFnPc2	Dymra
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
vydal	vydat	k5eAaPmAgInS	vydat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Clive	Cliev	k1gFnSc2	Cliev
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kariéry	kariéra	k1gFnSc2	kariéra
profesora	profesor	k1gMnSc2	profesor
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
prozaika	prozaik	k1gMnSc2	prozaik
je	být	k5eAaImIp3nS	být
Lewis	Lewis	k1gFnSc4	Lewis
mnohými	mnohý	k2eAgFnPc7d1	mnohá
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
apologetů	apologet	k1gMnPc2	apologet
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
zvolena	zvolen	k2eAgFnSc1d1	zvolena
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Christianity	Christianita	k1gFnSc2	Christianita
Today	Todaa	k1gFnSc2	Todaa
(	(	kIx(	(
<g/>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
knihou	kniha	k1gFnSc7	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
skeptickému	skeptický	k2eAgInSc3d1	skeptický
pohledu	pohled	k1gInSc3	pohled
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
obrácení	obrácení	k1gNnSc3	obrácení
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
apoštola	apoštol	k1gMnSc4	apoštol
skeptiků	skeptik	k1gMnPc2	skeptik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
zaujat	zaujmout	k5eAaPmNgInS	zaujmout
představou	představa	k1gFnSc7	představa
přinést	přinést	k5eAaPmF	přinést
rozumný	rozumný	k2eAgInSc4d1	rozumný
argument	argument	k1gInSc4	argument
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pravdivosti	pravdivost	k1gFnSc2	pravdivost
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
Problém	problém	k1gInSc1	problém
bolesti	bolest	k1gFnPc1	bolest
a	a	k8xC	a
Zázraky	zázrak	k1gInPc1	zázrak
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc4	všechen
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
argumenty	argument	k1gInPc1	argument
proti	proti	k7c3	proti
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Jak	jak	k6eAd1	jak
mohl	moct	k5eAaImAgMnS	moct
dobrý	dobrý	k2eAgMnSc1d1	dobrý
Bůh	bůh	k1gMnSc1	bůh
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
světě	svět	k1gInSc6	svět
existovala	existovat	k5eAaImAgFnS	existovat
bolest	bolest	k1gFnSc1	bolest
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
známým	známý	k1gMnSc7	známý
také	také	k9	také
díky	díky	k7c3	díky
přednáškové	přednáškový	k2eAgFnSc3d1	přednášková
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
účinkování	účinkování	k1gNnSc4	účinkování
v	v	k7c6	v
rozhlasových	rozhlasový	k2eAgInPc6d1	rozhlasový
pořadech	pořad	k1gInPc6	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
knihy	kniha	k1gFnSc2	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
předlohy	předloha	k1gFnPc4	předloha
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
promluvy	promluva	k1gFnPc4	promluva
nebo	nebo	k8xC	nebo
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Debata	debata	k1gFnSc1	debata
s	s	k7c7	s
filosofkou	filosofka	k1gFnSc7	filosofka
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Anscombovou	Anscombová	k1gFnSc7	Anscombová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Lewis	Lewis	k1gFnSc4	Lewis
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
přátelé	přítel	k1gMnPc1	přítel
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k9	jako
těžkou	těžký	k2eAgFnSc4d1	těžká
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
značné	značný	k2eAgNnSc4d1	značné
ponížení	ponížení	k1gNnSc4	ponížení
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
podryla	podrýt	k5eAaPmAgFnS	podrýt
jeho	jeho	k3xOp3gFnSc4	jeho
sebedůvěru	sebedůvěra	k1gFnSc4	sebedůvěra
coby	coby	k?	coby
křesťanského	křesťanský	k2eAgMnSc2d1	křesťanský
apologeta	apologet	k1gMnSc2	apologet
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dalším	další	k2eAgNnSc6d1	další
díle	dílo	k1gNnSc6	dílo
soustředil	soustředit	k5eAaPmAgInS	soustředit
své	svůj	k3xOyFgFnPc4	svůj
úsilí	úsilí	k1gNnSc1	úsilí
na	na	k7c4	na
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
beletrii	beletrie	k1gFnSc4	beletrie
a	a	k8xC	a
knihy	kniha	k1gFnPc4	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
značně	značně	k6eAd1	značně
paradoxní	paradoxní	k2eAgNnSc1d1	paradoxní
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotná	samotný	k2eAgFnSc1d1	samotná
Anscombová	Anscombová	k1gFnSc1	Anscombová
neměla	mít	k5eNaImAgFnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
Lewis	Lewis	k1gInSc1	Lewis
v	v	k7c6	v
debatě	debata	k1gFnSc6	debata
nějak	nějak	k6eAd1	nějak
výrazně	výrazně	k6eAd1	výrazně
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
považovala	považovat	k5eAaImAgFnS	považovat
ji	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
za	za	k7c4	za
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hlouběji	hluboko	k6eAd2	hluboko
probrala	probrat	k5eAaPmAgFnS	probrat
dané	daný	k2eAgNnSc4d1	dané
téma	téma	k1gNnSc4	téma
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Lewisovi	Lewisův	k2eAgMnPc1d1	Lewisův
dodatečně	dodatečně	k6eAd1	dodatečně
opravit	opravit	k5eAaPmF	opravit
některé	některý	k3yIgFnPc1	některý
slabší	slabý	k2eAgFnPc1d2	slabší
části	část	k1gFnPc1	část
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
napsal	napsat	k5eAaBmAgInS	napsat
také	také	k9	také
autobiografii	autobiografie	k1gFnSc4	autobiografie
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Zaskočen	zaskočen	k2eAgInSc1d1	zaskočen
radostí	radost	k1gFnSc7	radost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
klade	klást	k5eAaImIp3nS	klást
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
obrácení	obrácení	k1gNnSc4	obrácení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
poznal	poznat	k5eAaPmAgMnS	poznat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
Joy	Joy	k1gFnSc4	Joy
Greshamovou	Greshamový	k2eAgFnSc4d1	Greshamová
<g/>
;	;	kIx,	;
název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
řádku	řádek	k1gInSc2	řádek
básně	báseň	k1gFnSc2	báseň
Williama	William	k1gMnSc2	William
Wordswortha	Wordsworth	k1gMnSc2	Wordsworth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
eseje	esej	k1gFnPc4	esej
a	a	k8xC	a
veřejné	veřejný	k2eAgFnPc4d1	veřejná
promluvy	promluva	k1gFnPc4	promluva
o	o	k7c6	o
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
byly	být	k5eAaImAgFnP	být
společně	společně	k6eAd1	společně
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Bůh	bůh	k1gMnSc1	bůh
na	na	k7c4	na
lavici	lavice	k1gFnSc4	lavice
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
a	a	k8xC	a
The	The	k1gMnPc2	The
Weight	Weighta	k1gFnPc2	Weighta
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
and	and	k?	and
Other	Other	k1gMnSc1	Other
Addresses	Addresses	k1gMnSc1	Addresses
(	(	kIx(	(
<g/>
Váha	váha	k1gFnSc1	váha
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
proslovy	proslov	k1gInPc1	proslov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgFnPc1d1	populární
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nejznámější	známý	k2eAgNnSc1d3	nejznámější
dílo	dílo	k1gNnSc1	dílo
Letopisy	letopis	k1gInPc7	letopis
Narnie	Narnie	k1gFnSc2	Narnie
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
silné	silný	k2eAgNnSc4d1	silné
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
poselství	poselství	k1gNnSc4	poselství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
alegorii	alegorie	k1gFnSc4	alegorie
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
alegorie	alegorie	k1gFnSc2	alegorie
<g/>
,	,	kIx,	,
však	však	k9	však
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
knihy	kniha	k1gFnPc1	kniha
nejsou	být	k5eNaImIp3nP	být
alegorické	alegorický	k2eAgFnPc1d1	alegorická
<g/>
,	,	kIx,	,
a	a	k8xC	a
raději	rád	k6eAd2	rád
označoval	označovat	k5eAaImAgInS	označovat
jejich	jejich	k3xOp3gFnSc4	jejich
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
stránku	stránka	k1gFnSc4	stránka
za	za	k7c4	za
hypotetickou	hypotetický	k2eAgFnSc4d1	hypotetická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
často	často	k6eAd1	často
citované	citovaný	k2eAgFnSc6d1	citovaná
pasáži	pasáž	k1gFnSc6	pasáž
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnPc2	křesťanství
se	se	k3xPyFc4	se
Lewis	Lewis	k1gInSc1	Lewis
postavil	postavit	k5eAaPmAgInS	postavit
stále	stále	k6eAd1	stále
populárnějšímu	populární	k2eAgInSc3d2	populárnější
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakkoli	jakkoli	k6eAd1	jakkoli
byl	být	k5eAaImAgMnS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
velkým	velký	k2eAgMnSc7d1	velký
morálním	morální	k2eAgMnSc7d1	morální
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
to	ten	k3xDgNnSc1	ten
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
několik	několik	k4yIc4	několik
tvrzení	tvrzení	k1gNnPc2	tvrzení
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
božství	božství	k1gNnSc6	božství
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
logicky	logicky	k6eAd1	logicky
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
tvrzením	tvrzení	k1gNnSc7	tvrzení
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teologové	teolog	k1gMnPc1	teolog
jako	jako	k9	jako
Albert	Albert	k1gMnSc1	Albert
Schweitzer	Schweitzer	k1gMnSc1	Schweitzer
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Bultmann	Bultmann	k1gMnSc1	Bultmann
označovali	označovat	k5eAaImAgMnP	označovat
Ježíšovy	Ježíšův	k2eAgInPc4d1	Ježíšův
zázraky	zázrak	k1gInPc4	zázrak
a	a	k8xC	a
vzkříšení	vzkříšení	k1gNnPc4	vzkříšení
za	za	k7c4	za
mýtus	mýtus	k1gInSc4	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
nebyl	být	k5eNaImAgMnS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
moudrý	moudrý	k2eAgMnSc1d1	moudrý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
akademických	akademický	k2eAgInPc6d1	akademický
kruzích	kruh	k1gInPc6	kruh
zakořenila	zakořenit	k5eAaPmAgNnP	zakořenit
<g/>
.	.	kIx.	.
</s>
<s>
Přijetím	přijetí	k1gNnSc7	přijetí
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
tvrzení	tvrzení	k1gNnSc1	tvrzení
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
božství	božství	k1gNnSc6	božství
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gInSc4	Lewis
popíral	popírat	k5eAaImAgInS	popírat
pohled	pohled	k1gInSc1	pohled
zpopularizovaný	zpopularizovaný	k2eAgInSc1d1	zpopularizovaný
H.	H.	kA	H.
G.	G.	kA	G.
Wellsem	Wells	k1gInSc7	Wells
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Dějiny	dějiny	k1gFnPc1	dějiny
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	ježit	k5eAaImIp2nS	ježit
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
netvrdil	tvrdit	k5eNaImAgMnS	tvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
argument	argument	k1gInSc1	argument
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Lewis	Lewis	k1gInSc4	Lewis
nevymyslel	vymyslet	k5eNaPmAgMnS	vymyslet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
a	a	k8xC	a
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Lewisovo	Lewisův	k2eAgNnSc1d1	Lewisovo
trilema	trilema	k1gNnSc1	trilema
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
apologeta	apologeta	k1gMnSc1	apologeta
Josh	Josh	k1gMnSc1	Josh
McDowell	McDowell	k1gMnSc1	McDowell
ho	on	k3xPp3gInSc4	on
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
tesař	tesař	k1gMnSc1	tesař
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
apologetické	apologetický	k2eAgFnSc6d1	apologetická
literatuře	literatura	k1gFnSc6	literatura
často	často	k6eAd1	často
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
liberální	liberální	k2eAgMnPc1d1	liberální
teologové	teolog	k1gMnPc1	teolog
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
řad	řada	k1gFnPc2	řada
byla	být	k5eAaImAgFnS	být
Lewisova	Lewisův	k2eAgFnSc1d1	Lewisova
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
apologetika	apologetika	k1gFnSc1	apologetika
často	často	k6eAd1	často
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
tento	tento	k3xDgInSc1	tento
argument	argument	k1gInSc1	argument
<g/>
.	.	kIx.	.
</s>
<s>
Filozof	filozof	k1gMnSc1	filozof
John	John	k1gMnSc1	John
Beversluis	Beversluis	k1gInSc4	Beversluis
označil	označit	k5eAaPmAgMnS	označit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnSc1	Lewis
and	and	k?	and
the	the	k?	the
Search	Search	k1gInSc1	Search
for	forum	k1gNnPc2	forum
Rational	Rational	k1gFnSc2	Rational
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gInSc1	Lewis
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
rozumného	rozumný	k2eAgNnSc2d1	rozumné
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Lewisovy	Lewisův	k2eAgInPc1d1	Lewisův
argumenty	argument	k1gInPc1	argument
za	za	k7c2	za
"	"	kIx"	"
<g/>
bezohledně	bezohledně	k6eAd1	bezohledně
trvající	trvající	k2eAgInSc1d1	trvající
na	na	k7c6	na
textu	text	k1gInSc6	text
Bible	bible	k1gFnSc2	bible
a	a	k8xC	a
teologicky	teologicky	k6eAd1	teologicky
nespolehlivé	spolehlivý	k2eNgNnSc1d1	nespolehlivé
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Hick	Hick	k1gMnSc1	Hick
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
znalci	znalec	k1gMnPc1	znalec
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
již	již	k6eAd1	již
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nezastávají	zastávat	k5eNaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
biskup	biskup	k1gMnSc1	biskup
N.	N.	kA	N.
T.	T.	kA	T.
Wright	Wright	k1gMnSc1	Wright
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
argument	argument	k1gInSc1	argument
trilematu	trilemat	k1gInSc2	trilemat
nefunguje	fungovat	k5eNaImIp3nS	fungovat
jako	jako	k9	jako
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
že	že	k8xS	že
nebezpečně	bezpečně	k6eNd1	bezpečně
selhává	selhávat	k5eAaImIp3nS	selhávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
historičtí	historický	k2eAgMnPc1d1	historický
kritikové	kritik	k1gMnPc1	kritik
postaví	postavit	k5eAaPmIp3nP	postavit
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInSc3	jeho
způsobu	způsob	k1gInSc3	způsob
čtení	čtení	k1gNnSc3	čtení
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
strukturu	struktura	k1gFnSc4	struktura
použil	použít	k5eAaPmAgInS	použít
Lewis	Lewis	k1gFnSc4	Lewis
i	i	k9	i
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
profesor	profesor	k1gMnSc1	profesor
Kirke	Kirk	k1gFnSc2	Kirk
radí	radit	k5eAaImIp3nS	radit
mladým	mladý	k2eAgMnPc3d1	mladý
hrdinům	hrdina	k1gMnPc3	hrdina
<g/>
,	,	kIx,	,
že	že	k8xS	že
výpovědi	výpověď	k1gFnSc3	výpověď
jejich	jejich	k3xOp3gFnSc2	jejich
sestry	sestra	k1gFnSc2	sestra
o	o	k7c6	o
kouzelném	kouzelný	k2eAgInSc6d1	kouzelný
světě	svět	k1gInSc6	svět
musí	muset	k5eAaImIp3nS	muset
logicky	logicky	k6eAd1	logicky
považovat	považovat	k5eAaImF	považovat
buď	buď	k8xC	buď
za	za	k7c4	za
lež	lež	k1gFnSc4	lež
<g/>
,	,	kIx,	,
bláznovství	bláznovství	k1gNnSc4	bláznovství
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
myšlenek	myšlenka	k1gFnPc2	myšlenka
Lewisovy	Lewisův	k2eAgFnSc2d1	Lewisova
apologetiky	apologetika	k1gFnSc2	apologetika
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
etická	etický	k2eAgNnPc4d1	etické
pravidla	pravidlo	k1gNnPc4	pravidlo
společná	společný	k2eAgNnPc4d1	společné
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
pěti	pět	k4xCc6	pět
kapitolách	kapitola	k1gFnPc6	kapitola
knihy	kniha	k1gFnPc1	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnPc2	křesťanství
se	se	k3xPyFc4	se
Lewis	Lewis	k1gFnSc1	Lewis
zabývá	zabývat	k5eAaImIp3nS	zabývat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
mají	mít	k5eAaImIp3nP	mít
určité	určitý	k2eAgFnPc4d1	určitá
normy	norma	k1gFnPc4	norma
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
očekávají	očekávat	k5eAaImIp3nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
budou	být	k5eAaImBp3nP	být
ostatní	ostatní	k2eAgMnPc1d1	ostatní
chovat	chovat	k5eAaImF	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
normy	norma	k1gFnPc1	norma
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
univerzální	univerzální	k2eAgFnSc1d1	univerzální
morálka	morálka	k1gFnSc1	morálka
nebo	nebo	k8xC	nebo
přirozený	přirozený	k2eAgInSc1d1	přirozený
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc4	Lewis
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
porušili	porušit	k5eAaPmAgMnP	porušit
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
takovouto	takovýto	k3xDgFnSc7	takovýto
univerzální	univerzální	k2eAgFnSc7d1	univerzální
sadou	sada	k1gFnSc7	sada
pravidel	pravidlo	k1gNnPc2	pravidlo
musí	muset	k5eAaImIp3nS	muset
někdo	někdo	k3yInSc1	někdo
nebo	nebo	k8xC	nebo
něco	něco	k3yInSc1	něco
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
univerzální	univerzální	k2eAgFnSc4d1	univerzální
morálku	morálka	k1gFnSc4	morálka
také	také	k9	také
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prozaickém	prozaický	k2eAgNnSc6d1	prozaické
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Letopisech	letopis	k1gInPc6	letopis
Narnie	Narnie	k1gFnSc1	Narnie
ji	on	k3xPp3gFnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
dávnou	dávný	k2eAgFnSc4d1	dávná
magii	magie	k1gFnSc4	magie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
každý	každý	k3xTgMnSc1	každý
zná	znát	k5eAaImIp3nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
kapitole	kapitola	k1gFnSc6	kapitola
knihy	kniha	k1gFnSc2	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
Lewis	Lewis	k1gFnSc2	Lewis
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
lidi	člověk	k1gMnPc4	člověk
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
zákonem	zákon	k1gInSc7	zákon
lidské	lidský	k2eAgFnSc2d1	lidská
přirozenosti	přirozenost	k1gFnSc2	přirozenost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
morální	morální	k2eAgInSc1d1	morální
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
prostě	prostě	k9	prostě
náš	náš	k3xOp1gInSc1	náš
davový	davový	k2eAgInSc1d1	davový
instinkt	instinkt	k1gInSc1	instinkt
<g/>
"	"	kIx"	"
a	a	k8xC	a
potom	potom	k6eAd1	potom
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
morální	morální	k2eAgInSc1d1	morální
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
prostě	prostě	k9	prostě
společenská	společenský	k2eAgFnSc1d1	společenská
konvence	konvence	k1gFnSc1	konvence
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
myšlenku	myšlenka	k1gFnSc4	myšlenka
Lewis	Lewis	k1gFnSc2	Lewis
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
často	často	k6eAd1	často
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
sada	sada	k1gFnSc1	sada
morálních	morální	k2eAgNnPc2d1	morální
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
"	"	kIx"	"
<g/>
skutečné	skutečný	k2eAgFnSc2d1	skutečná
morálky	morálka	k1gFnSc2	morálka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yIgFnSc7	který
ostatní	ostatní	k2eAgFnPc4d1	ostatní
morální	morální	k2eAgNnPc4d1	morální
pravidla	pravidlo	k1gNnPc4	pravidlo
porovnávají	porovnávat	k5eAaImIp3nP	porovnávat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
morálce	morálka	k1gFnSc6	morálka
jsou	být	k5eAaImIp3nP	být
zveličené	zveličený	k2eAgFnPc4d1	zveličená
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
pletou	plést	k5eAaImIp3nP	plést
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
o	o	k7c6	o
morálce	morálka	k1gFnSc6	morálka
s	s	k7c7	s
rozdíly	rozdíl	k1gInPc7	rozdíl
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
o	o	k7c6	o
faktech	fakt	k1gInPc6	fakt
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc1	Lewis
dodnes	dodnes	k6eAd1	dodnes
získává	získávat	k5eAaImIp3nS	získávat
pozornost	pozornost	k1gFnSc4	pozornost
širokého	široký	k2eAgInSc2d1	široký
okruhu	okruh	k1gInSc2	okruh
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáři	čtenář	k1gMnPc1	čtenář
jeho	jeho	k3xOp3gFnSc2	jeho
fikce	fikce	k1gFnSc2	fikce
často	často	k6eAd1	často
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
Lewis	Lewis	k1gInSc4	Lewis
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
křesťanská	křesťanský	k2eAgNnPc4d1	křesťanské
témata	téma	k1gNnPc4	téma
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
apologetiky	apologetika	k1gFnSc2	apologetika
čtou	číst	k5eAaImIp3nP	číst
a	a	k8xC	a
citují	citovat	k5eAaBmIp3nP	citovat
stoupenci	stoupenec	k1gMnPc1	stoupenec
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
denominací	denominace	k1gFnPc2	denominace
včetně	včetně	k7c2	včetně
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
mormonů	mormon	k1gMnPc2	mormon
<g/>
.	.	kIx.	.
</s>
<s>
Lewisem	Lewis	k1gInSc7	Lewis
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
několik	několik	k4yIc1	několik
biografických	biografický	k2eAgNnPc2d1	biografické
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některá	některý	k3yIgFnSc1	některý
napsali	napsat	k5eAaBmAgMnP	napsat
jeho	jeho	k3xOp3gMnPc1	jeho
blízcí	blízký	k2eAgMnPc1d1	blízký
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Roger	Roger	k1gInSc1	Roger
Lancelyn	Lancelyna	k1gFnPc2	Lancelyna
Green	Grena	k1gFnPc2	Grena
a	a	k8xC	a
George	George	k1gFnPc2	George
Sayer	Sayra	k1gFnPc2	Sayra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vysílaly	vysílat	k5eAaImAgFnP	vysílat
britské	britský	k2eAgFnPc1d1	britská
televizní	televizní	k2eAgFnPc4d1	televizní
stanice	stanice	k1gFnPc4	stanice
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
s	s	k7c7	s
Jossem	Joss	k1gMnSc7	Joss
Acklandem	Acklando	k1gNnSc7	Acklando
a	a	k8xC	a
Claire	Clair	k1gInSc5	Clair
Bloomovou	Bloomový	k2eAgFnSc7d1	Bloomová
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
<g/>
)	)	kIx)	)
Shadowlands	Shadowlandsa	k1gFnPc2	Shadowlandsa
Williama	William	k1gMnSc4	William
Nicholsona	Nicholson	k1gMnSc4	Nicholson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dramaticky	dramaticky	k6eAd1	dramaticky
líčí	líčit	k5eAaImIp3nS	líčit
Lewisův	Lewisův	k2eAgInSc4d1	Lewisův
život	život	k1gInSc4	život
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Joy	Joy	k1gFnSc7	Joy
Greshamovou	Greshamový	k2eAgFnSc7d1	Greshamová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
scénář	scénář	k1gInSc1	scénář
zinscenován	zinscenovat	k5eAaPmNgInS	zinscenovat
jako	jako	k8xS	jako
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
s	s	k7c7	s
Nigelem	Nigel	k1gMnSc7	Nigel
Hawthornem	Hawthorn	k1gInSc7	Hawthorn
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
potom	potom	k6eAd1	potom
natočen	natočit	k5eAaBmNgInS	natočit
celovečerní	celovečerní	k2eAgInSc1d1	celovečerní
film	film	k1gInSc1	film
Shadowlands	Shadowlandsa	k1gFnPc2	Shadowlandsa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
hráli	hrát	k5eAaImAgMnP	hrát
Anthony	Anthon	k1gInPc4	Anthon
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
a	a	k8xC	a
Debra	Debra	k1gFnSc1	Debra
Wingerová	Wingerová	k1gFnSc1	Wingerová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
hodinový	hodinový	k2eAgInSc1d1	hodinový
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
s	s	k7c7	s
Antonem	Anton	k1gMnSc7	Anton
Rodgersem	Rodgers	k1gMnSc7	Rodgers
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
:	:	kIx,	:
Beyond	Beyond	k1gInSc1	Beyond
Narnia	Narnium	k1gNnSc2	Narnium
(	(	kIx(	(
<g/>
Za	za	k7c2	za
Narnií	Narnie	k1gFnPc2	Narnie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přinesl	přinést	k5eAaPmAgInS	přinést
stručný	stručný	k2eAgInSc4d1	stručný
přehled	přehled	k1gInSc4	přehled
Lewisova	Lewisův	k2eAgInSc2d1	Lewisův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
nepřímo	přímo	k6eNd1	přímo
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
i	i	k9	i
dílo	dílo	k1gNnSc4	dílo
svého	své	k1gNnSc2	své
přítele	přítel	k1gMnSc2	přítel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
postava	postava	k1gFnSc1	postava
enta	enta	k1gFnSc1	enta
Stromovouse	Stromovouse	k1gFnSc1	Stromovouse
z	z	k7c2	z
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
je	být	k5eAaImIp3nS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
přátelskou	přátelský	k2eAgFnSc7d1	přátelská
karikaturou	karikatura	k1gFnSc7	karikatura
Lewise	Lewise	k1gFnSc2	Lewise
<g/>
.	.	kIx.	.
</s>
<s>
Lewisem	Lewis	k1gInSc7	Lewis
bylo	být	k5eAaImAgNnS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
mnoho	mnoho	k4c1	mnoho
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Přísné	přísný	k2eAgNnSc4d1	přísné
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
Lewisova	Lewisův	k2eAgMnSc2d1	Lewisův
současníka	současník	k1gMnSc2	současník
a	a	k8xC	a
přítele	přítel	k1gMnSc2	přítel
Sheldona	Sheldon	k1gMnSc2	Sheldon
Vanaukena	Vanauken	k1gMnSc2	Vanauken
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
měly	mít	k5eAaImAgFnP	mít
Letopisy	letopis	k1gInPc4	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
dětská	dětský	k2eAgFnSc1d1	dětská
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Řady	řada	k1gFnSc2	řada
nešťastných	šťastný	k2eNgFnPc2d1	nešťastná
příhod	příhoda	k1gFnPc2	příhoda
Daniela	Daniel	k1gMnSc2	Daniel
Handlera	Handler	k1gMnSc2	Handler
<g/>
,	,	kIx,	,
Colferovy	Colferův	k2eAgFnSc2d1	Colferův
série	série	k1gFnSc2	série
Artemis	Artemis	k1gFnSc1	Artemis
Fowl	Fowl	k1gInSc1	Fowl
<g/>
,	,	kIx,	,
Jeho	jeho	k3xOp3gInSc1	jeho
temných	temný	k2eAgFnPc2d1	temná
esencí	esence	k1gFnPc2	esence
Philipa	Philip	k1gMnSc2	Philip
Pullmana	Pullman	k1gMnSc2	Pullman
nebo	nebo	k8xC	nebo
série	série	k1gFnSc2	série
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
řadou	řada	k1gFnSc7	řada
Lewisových	Lewisový	k2eAgFnPc2d1	Lewisová
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Lewisův	Lewisův	k2eAgMnSc1d1	Lewisův
kritik	kritik	k1gMnSc1	kritik
Pullman	Pullman	k1gMnSc1	Pullman
považoval	považovat	k5eAaImAgMnS	považovat
Lewise	Lewise	k1gFnPc4	Lewise
za	za	k7c4	za
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgMnS	obvinit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
využívá	využívat	k5eAaImIp3nS	využívat
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
propagandu	propaganda	k1gFnSc4	propaganda
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc1d1	mužský
šovinismus	šovinismus	k1gInSc1	šovinismus
<g/>
,	,	kIx,	,
rasismus	rasismus	k1gInSc1	rasismus
a	a	k8xC	a
emocionální	emocionální	k2eAgInSc1d1	emocionální
sadismus	sadismus	k1gInSc1	sadismus
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
fantasy	fantas	k1gInPc7	fantas
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
jako	jako	k8xC	jako
například	například	k6eAd1	například
Tim	Tim	k?	Tim
Powers	Powersa	k1gFnPc2	Powersa
také	také	k9	také
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
Lewis	Lewis	k1gInSc1	Lewis
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
posmrtně	posmrtně	k6eAd1	posmrtně
vydaného	vydaný	k2eAgNnSc2d1	vydané
díla	dílo	k1gNnSc2	dílo
editoval	editovat	k5eAaImAgMnS	editovat
správce	správce	k1gMnSc1	správce
jeho	jeho	k3xOp3gFnSc2	jeho
literární	literární	k2eAgFnSc2d1	literární
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
Walter	Walter	k1gMnSc1	Walter
Hooper	Hooper	k1gMnSc1	Hooper
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
odbornice	odbornice	k1gFnSc1	odbornice
na	na	k7c6	na
Lewise	Lewis	k1gInSc6	Lewis
Kathryn	Kathryna	k1gFnPc2	Kathryna
Lindskoogová	Lindskoogový	k2eAgFnSc1d1	Lindskoogový
však	však	k8xC	však
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hooperovo	Hooperův	k2eAgNnSc1d1	Hooperův
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
nespolehlivé	spolehlivý	k2eNgNnSc1d1	nespolehlivé
a	a	k8xC	a
že	že	k8xS	že
Hooper	Hooper	k1gInSc1	Hooper
učinil	učinit	k5eAaPmAgInS	učinit
nepravdivá	pravdivý	k2eNgNnPc4d1	nepravdivé
prohlášení	prohlášení	k1gNnSc4	prohlášení
a	a	k8xC	a
přisoudil	přisoudit	k5eAaPmAgMnS	přisoudit
Lewisovi	Lewis	k1gMnSc3	Lewis
nepravá	pravý	k2eNgNnPc4d1	nepravé
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
Lindskoogové	Lindskoogový	k2eAgFnSc2d1	Lindskoogový
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
Hooper	Hooper	k1gInSc1	Hooper
po	po	k7c6	po
Lewisově	Lewisův	k2eAgFnSc6d1	Lewisova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
stavět	stavět	k5eAaImF	stavět
do	do	k7c2	do
role	role	k1gFnSc2	role
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
sekretáře	sekretář	k1gMnSc2	sekretář
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
Lewisem	Lewis	k1gInSc7	Lewis
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
začátkem	začátek	k1gInSc7	začátek
června	červen	k1gInSc2	červen
a	a	k8xC	a
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
úvody	úvod	k1gInPc4	úvod
k	k	k7c3	k
Lewisovým	Lewisový	k2eAgInPc3d1	Lewisový
dílům	díl	k1gInPc3	díl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
,	,	kIx,	,
vzbuzovaly	vzbuzovat	k5eAaImAgFnP	vzbuzovat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
znal	znát	k5eAaImAgMnS	znát
Lewise	Lewise	k1gFnPc4	Lewise
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
velmi	velmi	k6eAd1	velmi
úzký	úzký	k2eAgInSc1d1	úzký
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Hooper	Hooper	k1gMnSc1	Hooper
však	však	k9	však
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Lewisem	Lewis	k1gInSc7	Lewis
trval	trvat	k5eAaImAgInS	trvat
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
otevřeně	otevřeně	k6eAd1	otevřeně
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
úvodu	úvod	k1gInSc6	úvod
k	k	k7c3	k
Lewisově	Lewisův	k2eAgFnSc3d1	Lewisova
knize	kniha	k1gFnSc3	kniha
The	The	k1gMnSc1	The
Weight	Weight	k1gMnSc1	Weight
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
(	(	kIx(	(
<g/>
Váha	váha	k1gFnSc1	váha
slávy	sláva	k1gFnSc2	sláva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
argumenty	argument	k1gInPc1	argument
Kathryn	Kathryna	k1gFnPc2	Kathryna
Lindskoogové	Lindskoog	k1gMnPc1	Lindskoog
jsou	být	k5eAaImIp3nP	být
vyloženy	vyložen	k2eAgFnPc1d1	vyložena
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
knize	kniha	k1gFnSc6	kniha
Sleuthing	Sleuthing	k1gInSc1	Sleuthing
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
:	:	kIx,	:
More	mor	k1gInSc5	mor
Light	Light	k2eAgInSc4d1	Light
in	in	k?	in
the	the	k?	the
Shadowlands	Shadowlands	k1gInSc1	Shadowlands
(	(	kIx(	(
<g/>
Slídění	slídění	k1gNnSc1	slídění
po	po	k7c6	po
C.	C.	kA	C.
S.	S.	kA	S.
Lewisovi	Lewis	k1gMnSc3	Lewis
<g/>
:	:	kIx,	:
Více	hodně	k6eAd2	hodně
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
stínů	stín	k1gInPc2	stín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
Lewisovy	Lewisův	k2eAgFnSc2d1	Lewisova
postavy	postava	k1gFnSc2	postava
Digoryho	Digory	k1gMnSc2	Digory
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c4	v
Holywood	Holywood	k1gInSc4	Holywood
Arches	Arches	k1gInSc4	Arches
před	před	k7c7	před
knihovnou	knihovna	k1gFnSc7	knihovna
Holywood	Holywooda	k1gFnPc2	Holywooda
Road	Roada	k1gFnPc2	Roada
Library	Librara	k1gFnSc2	Librara
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
proti	proti	k7c3	proti
filmovým	filmový	k2eAgInPc3d1	filmový
zpracováním	zpracování	k1gNnSc7	zpracování
svých	svůj	k3xOyFgFnPc2	svůj
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Obával	obávat	k5eAaImAgInS	obávat
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
postavy	postava	k1gFnPc1	postava
antropomorfních	antropomorfní	k2eAgNnPc2d1	antropomorfní
zvířat	zvíře	k1gNnPc2	zvíře
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
komedií	komedie	k1gFnSc7	komedie
nebo	nebo	k8xC	nebo
noční	noční	k2eAgFnSc7d1	noční
můrou	můra	k1gFnSc7	můra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
převedeny	převést	k5eAaPmNgInP	převést
z	z	k7c2	z
vyprávění	vyprávění	k1gNnSc2	vyprávění
do	do	k7c2	do
viditelné	viditelný	k2eAgFnSc2d1	viditelná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebyly	být	k5eNaImAgInP	být
technicky	technicky	k6eAd1	technicky
možné	možný	k2eAgInPc1d1	možný
filmové	filmový	k2eAgInPc1d1	filmový
triky	trik	k1gInPc1	trik
potřebné	potřebný	k2eAgInPc1d1	potřebný
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
souvislého	souvislý	k2eAgNnSc2d1	souvislé
a	a	k8xC	a
kompaktního	kompaktní	k2eAgNnSc2d1	kompaktní
filmového	filmový	k2eAgNnSc2d1	filmové
zpracování	zpracování	k1gNnSc2	zpracování
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
The	The	k1gMnSc1	The
Earth	Earth	k1gMnSc1	Earth
Will	Will	k1gMnSc1	Will
Shake	Shake	k1gFnSc1	Shake
zpívaná	zpívaná	k1gFnSc1	zpívaná
skupinou	skupina	k1gFnSc7	skupina
Thrice	Thric	k1gMnSc2	Thric
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
Lewisových	Lewisový	k2eAgFnPc2d1	Lewisová
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Sixpence	Sixpence	k1gFnSc1	Sixpence
None	None	k1gFnSc1	None
the	the	k?	the
Richer	Richra	k1gFnPc2	Richra
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
oddílu	oddíl	k1gInSc2	oddíl
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Velký	velký	k2eAgInSc1d1	velký
rozvod	rozvod	k1gInSc1	rozvod
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
zase	zase	k9	zase
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgNnPc4	tři
hudební	hudební	k2eAgNnPc4d1	hudební
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
pro	pro	k7c4	pro
smyčcové	smyčcový	k2eAgNnSc4d1	smyčcové
kvarteto	kvarteto	k1gNnSc4	kvarteto
s	s	k7c7	s
názvem	název	k1gInSc7	název
Velký	velký	k2eAgInSc1d1	velký
rozvod	rozvod	k1gInSc1	rozvod
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Matta	Matt	k1gMnSc2	Matt
Slocuma	Slocum	k1gMnSc2	Slocum
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Sixpence	Sixpence	k1gFnPc1	Sixpence
None	None	k1gNnSc1	None
the	the	k?	the
Richer	Richra	k1gFnPc2	Richra
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
The	The	k1gMnSc1	The
High	High	k1gMnSc1	High
Countries	Countries	k1gMnSc1	Countries
z	z	k7c2	z
alba	album	k1gNnSc2	album
Back	Backa	k1gFnPc2	Backa
Home	Hom	k1gFnSc2	Hom
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Caedmon	Caedmon	k1gInSc1	Caedmon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Call	Call	k1gInSc1	Call
a	a	k8xC	a
rockové	rockový	k2eAgNnSc1d1	rockové
album	album	k1gNnSc1	album
Ghosts	Ghosts	k1gInSc1	Ghosts
and	and	k?	and
Spirits	Spirits	k1gInSc1	Spirits
Phila	Phil	k1gMnSc2	Phil
Woodwarda	Woodward	k1gMnSc2	Woodward
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
písničkářka	písničkářka	k1gFnSc1	písničkářka
Brooke	Brooke	k1gFnSc1	Brooke
Fraserová	Fraserová	k1gFnSc1	Fraserová
zařadila	zařadit	k5eAaPmAgFnS	zařadit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
album	album	k1gNnSc4	album
Albertine	Albertin	k1gInSc5	Albertin
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnSc1	Lewis
Song	song	k1gInSc1	song
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pasáže	pasáž	k1gFnPc4	pasáž
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
alternativní	alternativní	k2eAgFnSc1d1	alternativní
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
Poor	Poora	k1gFnPc2	Poora
Old	Olda	k1gFnPc2	Olda
Lu	Lu	k1gFnSc2	Lu
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
podle	podle	k7c2	podle
věty	věta	k1gFnSc2	věta
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
alternativní	alternativní	k2eAgFnSc1d1	alternativní
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
Future	Futur	k1gMnSc5	Futur
of	of	k?	of
Forestry	Forestr	k1gInPc7	Forestr
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
Lewisovy	Lewisův	k2eAgFnSc2d1	Lewisova
básně	báseň	k1gFnSc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
2	[number]	k4	2
<g/>
nd	nd	k?	nd
Chapter	Chapter	k1gMnSc1	Chapter
of	of	k?	of
Acts	Acts	k1gInSc1	Acts
nahrála	nahrát	k5eAaBmAgFnS	nahrát
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Roar	Roar	k1gMnSc1	Roar
of	of	k?	of
Love	lov	k1gInSc5	lov
inspirované	inspirovaný	k2eAgFnPc1d1	inspirovaná
první	první	k4xOgFnPc4	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Narnii	Narnie	k1gFnSc6	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
kapela	kapela	k1gFnSc1	kapela
The	The	k1gMnPc2	The
Waterboys	Waterboysa	k1gFnPc2	Waterboysa
citovala	citovat	k5eAaBmAgFnS	citovat
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
dílu	díl	k1gInSc2	díl
série	série	k1gFnSc2	série
o	o	k7c6	o
Narnii	Narnie	k1gFnSc6	Narnie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
Church	Church	k1gMnSc1	Church
Not	nota	k1gFnPc2	nota
Made	Made	k1gInSc1	Made
with	with	k1gMnSc1	with
Hands	Hands	k1gInSc1	Hands
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
album	album	k1gNnSc4	album
Room	Room	k1gInPc3	Room
to	ten	k3xDgNnSc1	ten
Roam	Roam	k1gInSc1	Roam
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Waterboys	Waterboysa	k1gFnPc2	Waterboysa
zařadili	zařadit	k5eAaPmAgMnP	zařadit
píseň	píseň	k1gFnSc4	píseň
Further	Furthra	k1gFnPc2	Furthra
Up	Up	k1gFnPc2	Up
<g/>
,	,	kIx,	,
Further	Furthra	k1gFnPc2	Furthra
In	In	k1gFnPc2	In
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
podle	podle	k7c2	podle
poslední	poslední	k2eAgFnSc2d1	poslední
kapitoly	kapitola	k1gFnSc2	kapitola
Poslední	poslední	k2eAgFnSc2d1	poslední
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
Lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
a	a	k8xC	a
skříně	skříň	k1gFnSc2	skříň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
série	série	k1gFnSc2	série
o	o	k7c6	o
Narnii	Narnie	k1gFnSc6	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Tržby	tržba	k1gFnPc1	tržba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
744	[number]	k4	744
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1	filmová
adaptace	adaptace	k1gFnPc1	adaptace
se	se	k3xPyFc4	se
natáčí	natáčet	k5eAaImIp3nP	natáčet
také	také	k9	také
podle	podle	k7c2	podle
tří	tři	k4xCgFnPc2	tři
dalších	další	k2eAgFnPc2d1	další
Lewisových	Lewisový	k2eAgFnPc2d1	Lewisová
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Kaspian	Kaspian	k1gMnSc1	Kaspian
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
uveden	uveden	k2eAgInSc4d1	uveden
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Plavba	plavba	k1gFnSc1	plavba
jitřního	jitřní	k2eAgMnSc2d1	jitřní
poutníka	poutník	k1gMnSc2	poutník
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
a	a	k8xC	a
Rady	rada	k1gFnPc1	rada
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
ďábla	ďábel	k1gMnSc2	ďábel
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
C.	C.	kA	C.
S.	S.	kA	S.
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jedné	jeden	k4xCgFnSc2	jeden
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
diskusí	diskuse	k1gFnPc2	diskuse
o	o	k7c6	o
pracích	prak	k1gInPc6	prak
zabývajících	zabývající	k2eAgInPc6d1	zabývající
se	s	k7c7	s
životem	život	k1gInSc7	život
a	a	k8xC	a
dílem	díl	k1gInSc7	díl
C.	C.	kA	C.
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc2	Lewise
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
Inklings	Inklingsa	k1gFnPc2	Inklingsa
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc2	hodnocení
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
s	s	k7c7	s
Lewisem	Lewis	k1gInSc7	Lewis
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
hodnot	hodnota	k1gFnPc2	hodnota
ve	v	k7c6	v
vzdělání	vzdělání	k1gNnSc6	vzdělání
a	a	k8xC	a
studiu	studio	k1gNnSc6	studio
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
Cliva	Cliv	k1gMnSc2	Cliv
Staplese	Staplese	k1gFnSc2	Staplese
Lewise	Lewise	k1gFnSc2	Lewise
<g/>
.	.	kIx.	.
</s>
<s>
Poutníkův	poutníkův	k2eAgInSc4d1	poutníkův
návrat	návrat	k1gInSc4	návrat
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
trilogie	trilogie	k1gFnSc1	trilogie
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
mlčící	mlčící	k2eAgFnSc2d1	mlčící
planety	planeta	k1gFnSc2	planeta
Perelandra	Perelandra	k1gFnSc1	Perelandra
Ta	ten	k3xDgFnSc1	ten
obludná	obludný	k2eAgFnSc1d1	obludná
síla	síla	k1gFnSc1	síla
Letopisy	letopis	k1gInPc4	letopis
Narnie	Narnie	k1gFnSc2	Narnie
Lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
Princ	princa	k1gFnPc2	princa
Kaspian	Kaspiany	k1gInPc2	Kaspiany
Plavba	plavba	k1gFnSc1	plavba
Jitřního	jitřní	k2eAgMnSc2d1	jitřní
poutníka	poutník	k1gMnSc2	poutník
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
židle	židle	k1gFnSc1	židle
Kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
chlapec	chlapec	k1gMnSc1	chlapec
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
křesťanství	křesťanství	k1gNnSc2	křesťanství
Problém	problém	k1gInSc1	problém
bolesti	bolest	k1gFnSc2	bolest
Svědectví	svědectví	k1gNnSc2	svědectví
o	o	k7c6	o
zármutku	zármutek	k1gInSc6	zármutek
Zázraky	zázrak	k1gInPc1	zázrak
Dokud	dokud	k6eAd1	dokud
nemáme	mít	k5eNaImIp1nP	mít
tvář	tvář	k1gFnSc4	tvář
Rady	rada	k1gFnSc2	rada
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
ďábla	ďábel	k1gMnSc2	ďábel
<g />
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozvod	rozvod	k1gInSc1	rozvod
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	péct	k5eAaImAgFnS	péct
Přípitek	přípitek	k1gInSc4	přípitek
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
ďábla	ďábel	k1gMnSc2	ďábel
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
eseje	esej	k1gInPc4	esej
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
esejí	esej	k1gFnPc2	esej
<g/>
)	)	kIx)	)
Sloni	sloň	k1gFnSc2wR	sloň
a	a	k8xC	a
kapradí	kapradí	k1gNnSc2	kapradí
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
esejí	esej	k1gFnPc2	esej
<g/>
)	)	kIx)	)
Bůh	bůh	k1gMnSc1	bůh
na	na	k7c6	na
lavici	lavice	k1gFnSc6	lavice
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
esejí	esej	k1gFnPc2	esej
<g/>
)	)	kIx)	)
Zničení	zničení	k1gNnSc1	zničení
člověka	člověk	k1gMnSc2	člověk
Zaskočen	zaskočen	k2eAgInSc1d1	zaskočen
Radostí	radost	k1gFnSc7	radost
Čtyři	čtyři	k4xCgFnPc1	čtyři
lásky	láska	k1gFnPc1	láska
Průvodce	průvodce	k1gMnSc2	průvodce
modlitbou	modlitba	k1gFnSc7	modlitba
Úvahy	úvaha	k1gFnSc2	úvaha
nad	nad	k7c4	nad
žalmy	žalm	k1gInPc4	žalm
Sam	Sam	k1gMnSc1	Sam
Wellman	Wellman	k1gMnSc1	Wellman
<g/>
,	,	kIx,	,
Poutník	poutník	k1gMnSc1	poutník
krajinou	krajina	k1gFnSc7	krajina
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7255-018-7	[number]	k4	80-7255-018-7
(	(	kIx(	(
<g/>
životopis	životopis	k1gInSc1	životopis
<g/>
)	)	kIx)	)
FLORYK	FLORYK	kA	FLORYK
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Zasuté	zasutý	k2eAgInPc1d1	zasutý
kameny	kámen	k1gInPc1	kámen
:	:	kIx,	:
deset	deset	k4xCc4	deset
postav	postava	k1gFnPc2	postava
konzervativního	konzervativní	k2eAgNnSc2d1	konzervativní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
:	:	kIx,	:
Konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
klub	klub	k1gInSc1	klub
o.s.	o.s.	k?	o.s.
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
3007	[number]	k4	3007
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Clive	Cliev	k1gFnSc2	Cliev
Staples	Staplesa	k1gFnPc2	Staplesa
Lewis	Lewis	k1gFnSc7	Lewis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Clive	Cliev	k1gFnSc2	Cliev
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc4	Lewis
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Clive	Cliev	k1gFnSc2	Cliev
Staples	Staples	k1gInSc4	Staples
Lewis	Lewis	k1gFnSc2	Lewis
(	(	kIx(	(
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
Lewisovi	Lewis	k1gMnSc6	Lewis
a	a	k8xC	a
dalších	další	k2eAgMnPc6d1	další
spisovatelích	spisovatel	k1gMnPc6	spisovatel
"	"	kIx"	"
<g/>
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
romantismu	romantismus	k1gInSc2	romantismus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Databáze	databáze	k1gFnSc2	databáze
ČBDB	ČBDB	kA	ČBDB
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
Into	Into	k1gMnSc1	Into
the	the	k?	the
Wardrobe	Wardrob	k1gMnSc5	Wardrob
<g/>
,	,	kIx,	,
a	a	k8xC	a
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnSc1	Lewis
web	web	k1gInSc1	web
site	site	k1gFnSc1	site
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnSc2	stránka
americké	americký	k2eAgFnSc2d1	americká
Nadace	nadace	k1gFnSc2	nadace
C.	C.	kA	C.
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
</s>
