<s>
Počátky	počátek	k1gInPc1	počátek
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
přelomu	přelom	k1gInSc3	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
odvodil	odvodit	k5eAaPmAgMnS	odvodit
vztah	vztah	k1gInSc4	vztah
pro	pro	k7c4	pro
frekvenční	frekvenční	k2eAgNnSc4d1	frekvenční
rozdělení	rozdělení	k1gNnSc4	rozdělení
energie	energie	k1gFnSc2	energie
záření	záření	k1gNnSc2	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vyzařováno	vyzařovat	k5eAaImNgNnS	vyzařovat
po	po	k7c6	po
malých	malý	k2eAgInPc6d1	malý
kvantech	kvantum	k1gNnPc6	kvantum
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
frekvenci	frekvence	k1gFnSc4	frekvence
(	(	kIx(	(
<g/>
konstanta	konstanta	k1gFnSc1	konstanta
úměrnosti	úměrnost	k1gFnSc2	úměrnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Planckovou	Planckův	k2eAgFnSc7d1	Planckova
konstantou	konstanta	k1gFnSc7	konstanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
