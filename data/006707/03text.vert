<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
oborů	obor	k1gInPc2	obor
postavených	postavený	k2eAgInPc2d1	postavený
na	na	k7c6	na
teoriích	teorie	k1gFnPc6	teorie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
ústředním	ústřední	k2eAgInSc7d1	ústřední
pojmem	pojem	k1gInSc7	pojem
je	být	k5eAaImIp3nS	být
kvantování	kvantování	k1gNnSc1	kvantování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
se	se	k3xPyFc4	se
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
fyzice	fyzika	k1gFnSc6	fyzika
stav	stav	k1gInSc1	stav
systému	systém	k1gInSc3	systém
popisuje	popisovat	k5eAaImIp3nS	popisovat
nikoli	nikoli	k9	nikoli
přímo	přímo	k6eAd1	přímo
měřitelnými	měřitelný	k2eAgFnPc7d1	měřitelná
veličinami	veličina	k1gFnPc7	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
poloha	poloha	k1gFnSc1	poloha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
hybnost	hybnost	k1gFnSc1	hybnost
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
speciální	speciální	k2eAgFnSc7d1	speciální
funkcí	funkce	k1gFnSc7	funkce
-	-	kIx~	-
stavovým	stavový	k2eAgInSc7d1	stavový
vektorem	vektor	k1gInSc7	vektor
(	(	kIx(	(
<g/>
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ψ	Ψ	k?	Ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
}	}	kIx)	}
,	,	kIx,	,
nejobecněji	obecně	k6eAd3	obecně
matice	matice	k1gFnSc2	matice
hustoty	hustota	k1gFnSc2	hustota
určující	určující	k2eAgInSc4d1	určující
soubor	soubor	k1gInSc4	soubor
vlnových	vlnový	k2eAgFnPc2d1	vlnová
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ψ	Ψ	k?	Ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
}	}	kIx)	}
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
pravděpodobnostmi	pravděpodobnost	k1gFnPc7	pravděpodobnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
klasická	klasický	k2eAgFnSc1d1	klasická
fyzika	fyzika	k1gFnSc1	fyzika
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měření	měření	k1gNnSc1	měření
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	on	k3xPp3gInSc4	on
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
měřený	měřený	k2eAgInSc4d1	měřený
objekt	objekt	k1gInSc4	objekt
byl	být	k5eAaImAgInS	být
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
,	,	kIx,	,
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Měřitelným	měřitelný	k2eAgFnPc3d1	měřitelná
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
veličinám	veličina	k1gFnPc3	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
}	}	kIx)	}
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
operátory	operátor	k1gMnPc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}}	}}}	k?	}}}
působící	působící	k2eAgInSc1d1	působící
na	na	k7c4	na
stavový	stavový	k2eAgInSc4d1	stavový
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ψ	Ψ	k?	Ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
měření	měření	k1gNnSc1	měření
je	být	k5eAaImIp3nS	být
zásahem	zásah	k1gInSc7	zásah
měnícím	měnící	k2eAgInSc7d1	měnící
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
redukce	redukce	k1gFnSc1	redukce
vlnového	vlnový	k2eAgNnSc2d1	vlnové
klubka	klubko	k1gNnSc2	klubko
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ψ	Ψ	k?	Ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
}	}	kIx)	}
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
změna	změna	k1gFnSc1	změna
na	na	k7c4	na
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
operátoru	operátor	k1gInSc3	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
}	}	kIx)	}
)	)	kIx)	)
a	a	k8xC	a
dávající	dávající	k2eAgNnSc1d1	dávající
jen	jen	k6eAd1	jen
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
možných	možný	k2eAgFnPc2d1	možná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hodnotu	hodnota	k1gFnSc4	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l_	l_	k?	l_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
operátoru	operátor	k1gInSc3	operátor
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
l_	l_	k?	l_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ψ	Ψ	k?	Ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
spočítat	spočítat	k5eAaPmF	spočítat
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
hodnoty	hodnota	k1gFnPc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l_	l_	k?	l_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
nemusí	muset	k5eNaImIp3nP	muset
tvořit	tvořit	k5eAaImF	tvořit
spojitou	spojitý	k2eAgFnSc4d1	spojitá
řadu	řada	k1gFnSc4	řada
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
název	název	k1gInSc1	název
kvantování	kvantování	k1gNnSc2	kvantování
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
energie	energie	k1gFnSc1	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
nejen	nejen	k6eAd1	nejen
spojitě	spojitě	k6eAd1	spojitě
(	(	kIx(	(
<g/>
např.	např.	kA	např.
elektron	elektron	k1gInSc1	elektron
s	s	k7c7	s
kladnou	kladný	k2eAgFnSc7d1	kladná
energií	energie	k1gFnSc7	energie
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nespojitě	spojitě	k6eNd1	spojitě
(	(	kIx(	(
<g/>
elektron	elektron	k1gInSc1	elektron
se	s	k7c7	s
zápornou	záporný	k2eAgFnSc7d1	záporná
energií	energie	k1gFnSc7	energie
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
Δ	Δ	k?	Δ
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gFnPc7	E_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
E	E	kA	E
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
kvanta	kvantum	k1gNnPc1	kvantum
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
realizují	realizovat	k5eAaBmIp3nP	realizovat
se	se	k3xPyFc4	se
elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
energií	energie	k1gFnSc7	energie
-	-	kIx~	-
fotonem	foton	k1gInSc7	foton
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
E	E	kA	E
<g/>
}	}	kIx)	}
a	a	k8xC	a
frekvencí	frekvence	k1gFnSc7	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
=	=	kIx~	=
Δ	Δ	k?	Δ
E	E	kA	E
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc4	hbar
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
mechaniku	mechanika	k1gFnSc4	mechanika
a	a	k8xC	a
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
teorii	teorie	k1gFnSc4	teorie
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
stejných	stejný	k2eAgInPc6d1	stejný
postulátech	postulát	k1gInPc6	postulát
<g/>
,	,	kIx,	,
užívají	užívat	k5eAaImIp3nP	užívat
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
metody	metoda	k1gFnPc1	metoda
při	při	k7c6	při
praktických	praktický	k2eAgInPc6d1	praktický
výpočtech	výpočet	k1gInPc6	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
přelomu	přelom	k1gInSc3	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
odvodil	odvodit	k5eAaPmAgMnS	odvodit
vztah	vztah	k1gInSc4	vztah
pro	pro	k7c4	pro
frekvenční	frekvenční	k2eAgNnSc4d1	frekvenční
rozdělení	rozdělení	k1gNnSc4	rozdělení
energie	energie	k1gFnSc2	energie
záření	záření	k1gNnSc2	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vyzařováno	vyzařovat	k5eAaImNgNnS	vyzařovat
po	po	k7c6	po
malých	malý	k2eAgInPc6d1	malý
kvantech	kvantum	k1gNnPc6	kvantum
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
frekvenci	frekvence	k1gFnSc4	frekvence
(	(	kIx(	(
<g/>
konstanta	konstanta	k1gFnSc1	konstanta
úměrnosti	úměrnost	k1gFnSc2	úměrnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Planckovou	Planckův	k2eAgFnSc7d1	Planckova
konstantou	konstanta	k1gFnSc7	konstanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
teorie	teorie	k1gFnSc1	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
čistě	čistě	k6eAd1	čistě
vlnový	vlnový	k2eAgInSc4d1	vlnový
charakter	charakter	k1gInSc4	charakter
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
rozložení	rozložení	k1gNnSc4	rozložení
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
moment	moment	k1gInSc1	moment
hybnosti	hybnost	k1gFnSc2	hybnost
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
celým	celý	k2eAgInSc7d1	celý
násobkem	násobek	k1gInSc7	násobek
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
pak	pak	k8xC	pak
vysvětlil	vysvětlit	k5eAaPmAgInS	vysvětlit
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
pouze	pouze	k6eAd1	pouze
mikroskopické	mikroskopický	k2eAgInPc4d1	mikroskopický
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Broglie	Broglie	k1gFnSc1	Broglie
tedy	tedy	k8xC	tedy
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
uvažovat	uvažovat	k5eAaImF	uvažovat
u	u	k7c2	u
veškeré	veškerý	k3xTgFnSc2	veškerý
látky	látka	k1gFnSc2	látka
dvojí	dvojit	k5eAaImIp3nS	dvojit
podstatu	podstata	k1gFnSc4	podstata
<g/>
,	,	kIx,	,
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
a	a	k8xC	a
částicovou	částicový	k2eAgFnSc4d1	částicová
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
vysvětleny	vysvětlen	k2eAgInPc4d1	vysvětlen
interferenční	interferenční	k2eAgInPc4d1	interferenční
jevy	jev	k1gInPc4	jev
při	při	k7c6	při
rozptylu	rozptyl	k1gInSc6	rozptyl
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Youngův	Youngův	k2eAgInSc1d1	Youngův
experiment	experiment	k1gInSc1	experiment
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
E.	E.	kA	E.
Schrödinger	Schrödinger	k1gInSc1	Schrödinger
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
rovnici	rovnice	k1gFnSc4	rovnice
.	.	kIx.	.
</s>
<s>
W.	W.	kA	W.
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
zobecnil	zobecnit	k5eAaPmAgMnS	zobecnit
Hamiltonovy	Hamiltonův	k2eAgFnPc4d1	Hamiltonova
rovnice	rovnice	k1gFnPc4	rovnice
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klasická	klasický	k2eAgFnSc1d1	klasická
mechanika	mechanika	k1gFnSc1	mechanika
je	být	k5eAaImIp3nS	být
limitním	limitní	k2eAgInSc7d1	limitní
případem	případ	k1gInSc7	případ
mechaniky	mechanika	k1gFnSc2	mechanika
kvantové	kvantový	k2eAgFnSc2d1	kvantová
<g/>
.	.	kIx.	.
</s>
<s>
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
vlnová	vlnový	k2eAgFnSc1d1	vlnová
teorie	teorie	k1gFnSc1	teorie
i	i	k9	i
Heisenbergův	Heisenbergův	k2eAgInSc4d1	Heisenbergův
maticový	maticový	k2eAgInSc4d1	maticový
počet	počet	k1gInSc4	počet
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgMnPc1d1	ekvivalentní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
různými	různý	k2eAgFnPc7d1	různá
reprezentacemi	reprezentace	k1gFnPc7	reprezentace
stejné	stejný	k2eAgFnSc2d1	stejná
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
narážela	narážet	k5eAaImAgFnS	narážet
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
zásadní	zásadní	k2eAgInPc4d1	zásadní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
<g/>
:	:	kIx,	:
mechanikou	mechanika	k1gFnSc7	mechanika
s	s	k7c7	s
konečným	konečný	k2eAgNnSc7d1	konečné
a	a	k8xC	a
předem	předem	k6eAd1	předem
daným	daný	k2eAgInSc7d1	daný
počtem	počet	k1gInSc7	počet
stupňů	stupeň	k1gInPc2	stupeň
volnosti	volnost	k1gFnSc2	volnost
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
uvažovat	uvažovat	k5eAaImF	uvažovat
proměnlivé	proměnlivý	k2eAgNnSc4d1	proměnlivé
množství	množství	k1gNnSc4	množství
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
rozpady	rozpad	k1gInPc1	rozpad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
neslučitelná	slučitelný	k2eNgFnSc1d1	neslučitelná
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
relativitou	relativita	k1gFnSc7	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
pořadí	pořadí	k1gNnSc4	pořadí
operátorů	operátor	k1gInPc2	operátor
souřadnice	souřadnice	k1gFnSc2	souřadnice
a	a	k8xC	a
generátorů	generátor	k1gInPc2	generátor
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
neudržitelnost	neudržitelnost	k1gFnSc1	neudržitelnost
pojmu	pojem	k1gInSc2	pojem
lokalizovaného	lokalizovaný	k2eAgInSc2d1	lokalizovaný
stavu	stav	k1gInSc2	stav
v	v	k7c6	v
Lorentzovsky	Lorentzovsko	k1gNnPc7	Lorentzovsko
kovariantních	kovariantní	k2eAgFnPc6d1	kovariantní
teoriích	teorie	k1gFnPc6	teorie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
důsledcích	důsledek	k1gInPc6	důsledek
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
interpretačním	interpretační	k2eAgFnPc3d1	interpretační
těžkostem	těžkost	k1gFnPc3	těžkost
u	u	k7c2	u
Diracovy	Diracův	k2eAgFnSc2d1	Diracova
a	a	k8xC	a
Klein-Gordonovy	Klein-Gordonův	k2eAgFnSc2d1	Klein-Gordonova
rovnice	rovnice	k1gFnSc2	rovnice
(	(	kIx(	(
<g/>
Kleinův	Kleinův	k2eAgInSc1d1	Kleinův
paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
třaslavý	třaslavý	k2eAgInSc1d1	třaslavý
pohyb	pohyb	k1gInSc1	pohyb
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgInPc1	tento
problémy	problém	k1gInPc1	problém
řeší	řešit	k5eAaImIp3nP	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
je	být	k5eAaImIp3nS	být
budována	budován	k2eAgFnSc1d1	budována
pro	pro	k7c4	pro
systémy	systém	k1gInPc4	systém
s	s	k7c7	s
nekonečným	konečný	k2eNgInSc7d1	nekonečný
počtem	počet	k1gInSc7	počet
stupňů	stupeň	k1gInPc2	stupeň
volnosti	volnost	k1gFnSc2	volnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
stavu	stav	k1gInSc6	stav
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
částic	částice	k1gFnPc2	částice
pevně	pevně	k6eAd1	pevně
daný	daný	k2eAgInSc1d1	daný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
problém	problém	k1gInSc4	problém
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
pole	pole	k1gNnSc2	pole
také	také	k9	také
nenarážíme	narážet	k5eNaImIp1nP	narážet
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
prostorová	prostorový	k2eAgFnSc1d1	prostorová
souřadnice	souřadnice	k1gFnSc1	souřadnice
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
časem	čas	k1gInSc7	čas
jako	jako	k8xS	jako
parametr	parametr	k1gInSc1	parametr
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
dynamická	dynamický	k2eAgFnSc1d1	dynamická
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přirozeně	přirozeně	k6eAd1	přirozeně
zahrnout	zahrnout	k5eAaPmF	zahrnout
speciální	speciální	k2eAgFnSc4d1	speciální
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
problému	problém	k1gInSc2	problém
s	s	k7c7	s
interpretací	interpretace	k1gFnSc7	interpretace
lokalizovaných	lokalizovaný	k2eAgInPc2d1	lokalizovaný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Korektním	korektní	k2eAgNnSc7d1	korektní
zahrnutím	zahrnutí	k1gNnSc7	zahrnutí
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
přesnějšího	přesný	k2eAgNnSc2d2	přesnější
popisu	popis	k1gInSc3	popis
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
některých	některý	k3yIgFnPc2	některý
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
klidovou	klidový	k2eAgFnSc7d1	klidová
energií	energie	k1gFnSc7	energie
či	či	k8xC	či
vyšší	vysoký	k2eAgFnSc7d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
především	především	k9	především
o	o	k7c4	o
β	β	k?	β
neutronu	neutron	k1gInSc2	neutron
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
uvolněnému	uvolněný	k2eAgNnSc3d1	uvolněné
lehkému	lehký	k2eAgNnSc3d1	lehké
neutrinu	neutrino	k1gNnSc3	neutrino
<g/>
)	)	kIx)	)
a	a	k8xC	a
pochopitelně	pochopitelně	k6eAd1	pochopitelně
všech	všecek	k3xTgInPc2	všecek
procesů	proces	k1gInPc2	proces
s	s	k7c7	s
fotony	foton	k1gInPc7	foton
<g/>
.	.	kIx.	.
</s>
<s>
Relativistická	relativistický	k2eAgFnSc1d1	relativistická
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Fermiho	Fermi	k1gMnSc2	Fermi
teorie	teorie	k1gFnSc2	teorie
β	β	k?	β
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
kvantové	kvantový	k2eAgFnSc2d1	kvantová
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímž	jejíž	k3xOyRp3gMnPc3	jejíž
hlavním	hlavní	k2eAgMnPc3d1	hlavní
autorům	autor	k1gMnPc3	autor
patří	patřit	k5eAaImIp3nS	patřit
P.	P.	kA	P.
Dirac	Dirac	k1gInSc1	Dirac
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Dyson	Dyson	k1gInSc1	Dyson
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Schwinger	Schwinger	k1gInSc1	Schwinger
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Feynman	Feynman	k1gMnSc1	Feynman
a	a	k8xC	a
Š.	Š.	kA	Š.
Tomonaga	Tomonaga	k1gFnSc1	Tomonaga
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
v	v	k7c6	v
poruchové	poruchový	k2eAgFnSc6d1	poruchová
formulaci	formulace	k1gFnSc6	formulace
teorie	teorie	k1gFnSc2	teorie
pole	pole	k1gFnSc2	pole
byl	být	k5eAaImAgInS	být
výskyt	výskyt	k1gInSc1	výskyt
nekonečných	konečný	k2eNgInPc2d1	nekonečný
koeficientů	koeficient	k1gInPc2	koeficient
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
korekcích	korekce	k1gFnPc6	korekce
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
aproximaci	aproximace	k1gFnSc3	aproximace
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
procedura	procedura	k1gFnSc1	procedura
zvaná	zvaný	k2eAgFnSc1d1	zvaná
renormalizace	renormalizace	k1gFnSc1	renormalizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
nekonečné	konečný	k2eNgFnPc4d1	nekonečná
hodnoty	hodnota	k1gFnPc4	hodnota
pomocí	pomocí	k7c2	pomocí
předefinování	předefinování	k1gNnSc2	předefinování
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
vystupujících	vystupující	k2eAgFnPc2d1	vystupující
v	v	k7c6	v
rovnicích	rovnice	k1gFnPc6	rovnice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
náboje	náboj	k1gInPc1	náboj
či	či	k8xC	či
hmotnosti	hmotnost	k1gFnPc1	hmotnost
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
matematická	matematický	k2eAgFnSc1d1	matematická
konzistence	konzistence	k1gFnSc1	konzistence
této	tento	k3xDgFnSc2	tento
procedury	procedura	k1gFnSc2	procedura
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
uspokojivá	uspokojivý	k2eAgFnSc1d1	uspokojivá
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnSc3d1	dobrá
shodě	shoda	k1gFnSc3	shoda
s	s	k7c7	s
experimentem	experiment	k1gInSc7	experiment
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
současné	současný	k2eAgFnSc2d1	současná
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Dirac	Dirac	k1gInSc1	Dirac
a	a	k8xC	a
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
,	,	kIx,	,
vyjadřovalo	vyjadřovat	k5eAaImAgNnS	vyjadřovat
ovšem	ovšem	k9	ovšem
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
stavem	stav	k1gInSc7	stav
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
renormalizace	renormalizace	k1gFnSc1	renormalizace
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
nejdivnějších	divný	k2eAgInPc2d3	Nejdivnější
<g/>
"	"	kIx"	"
míst	místo	k1gNnPc2	místo
současné	současný	k2eAgFnSc2d1	současná
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Renormalizace	Renormalizace	k1gFnSc1	Renormalizace
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
aplikovatelná	aplikovatelný	k2eAgFnSc1d1	aplikovatelná
jen	jen	k9	jen
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
třídu	třída	k1gFnSc4	třída
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgFnPc2d1	zvaná
renormalizovatelné	renormalizovatelný	k2eAgMnPc4d1	renormalizovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Renormalizovatelnost	Renormalizovatelnost	k1gFnSc1	Renormalizovatelnost
je	být	k5eAaImIp3nS	být
užitečným	užitečný	k2eAgNnSc7d1	užitečné
kritériem	kritérion	k1gNnSc7	kritérion
pro	pro	k7c4	pro
rozhodování	rozhodování	k1gNnSc4	rozhodování
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
určitá	určitý	k2eAgFnSc1d1	určitá
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
přípustná	přípustný	k2eAgFnSc1d1	přípustná
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgInPc2d1	vyskytující
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Nerenormalizovatelnost	Nerenormalizovatelnost	k1gFnSc1	Nerenormalizovatelnost
staré	stará	k1gFnSc2	stará
Fermiho	Fermi	k1gMnSc2	Fermi
teorie	teorie	k1gFnSc2	teorie
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
předpovědi	předpověď	k1gFnSc3	předpověď
objevu	objev	k1gInSc2	objev
existence	existence	k1gFnSc2	existence
intermediálních	intermediální	k2eAgInPc2d1	intermediální
bosonů	boson	k1gInPc2	boson
W	W	kA	W
a	a	k8xC	a
Z	Z	kA	Z
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
slabých	slabý	k2eAgFnPc2d1	slabá
interakcí	interakce	k1gFnPc2	interakce
do	do	k7c2	do
jednotné	jednotný	k2eAgFnSc2d1	jednotná
teorie	teorie	k1gFnSc2	teorie
elektroslabých	elektroslabý	k2eAgFnPc2d1	elektroslabá
interakcí	interakce	k1gFnPc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
teorie	teorie	k1gFnSc2	teorie
silných	silný	k2eAgFnPc2d1	silná
interakcí	interakce	k1gFnPc2	interakce
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgFnPc1d1	zodpovědná
za	za	k7c4	za
síly	síla	k1gFnPc4	síla
držící	držící	k2eAgFnPc4d1	držící
dohromady	dohromady	k6eAd1	dohromady
hadrony	hadron	k1gInPc1	hadron
a	a	k8xC	a
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
složitější	složitý	k2eAgInSc1d2	složitější
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
nepřebernému	přeberný	k2eNgNnSc3d1	nepřeberné
množství	množství	k1gNnSc3	množství
nově	nově	k6eAd1	nově
objevených	objevený	k2eAgInPc2d1	objevený
hadronů	hadron	k1gInPc2	hadron
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
dokonce	dokonce	k9	dokonce
převažoval	převažovat	k5eAaImAgMnS	převažovat
mezi	mezi	k7c7	mezi
fyziky	fyzik	k1gMnPc7	fyzik
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
částicových	částicový	k2eAgInPc2d1	částicový
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
předpovědi	předpověď	k1gFnPc1	předpověď
byly	být	k5eAaImAgFnP	být
získávány	získávat	k5eAaImNgFnP	získávat
vyšetřováním	vyšetřování	k1gNnSc7	vyšetřování
analytických	analytický	k2eAgFnPc2d1	analytická
vlastností	vlastnost	k1gFnPc2	vlastnost
S-matice	Satice	k1gFnSc2	S-matice
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
zažila	zažít	k5eAaPmAgFnS	zažít
návrat	návrat	k1gInSc4	návrat
koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
kvarkového	kvarkový	k2eAgInSc2d1	kvarkový
modelu	model	k1gInSc2	model
a	a	k8xC	a
asymptotické	asymptotický	k2eAgFnSc2d1	asymptotická
volnosti	volnost	k1gFnSc2	volnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
kvantové	kvantový	k2eAgFnSc2d1	kvantová
chromodynamiky	chromodynamika	k1gFnSc2	chromodynamika
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc2d1	hlavní
teorie	teorie	k1gFnSc2	teorie
silných	silný	k2eAgFnPc2d1	silná
interakcí	interakce	k1gFnPc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
asymptotické	asymptotický	k2eAgFnSc2d1	asymptotická
volnosti	volnost	k1gFnSc2	volnost
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
F.	F.	kA	F.
Wilczekovi	Wilczekův	k2eAgMnPc1d1	Wilczekův
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Grossovi	Gross	k1gMnSc3	Gross
a	a	k8xC	a
H.	H.	kA	H.
D.	D.	kA	D.
Politzerovi	Politzer	k1gMnSc3	Politzer
nobelova	nobelův	k2eAgInSc2d1	nobelův
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
fyzika	fyzika	k1gFnSc1	fyzika
popisuje	popisovat	k5eAaImIp3nS	popisovat
stav	stav	k1gInSc4	stav
systému	systém	k1gInSc2	systém
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
čase	čas	k1gInSc6	čas
pomocí	pomocí	k7c2	pomocí
sady	sada	k1gFnSc2	sada
hodnot	hodnota	k1gFnPc2	hodnota
vybraných	vybraný	k2eAgFnPc2d1	vybraná
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
veličin	veličina	k1gFnPc2	veličina
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
fyzice	fyzika	k1gFnSc6	fyzika
obvykle	obvykle	k6eAd1	obvykle
nazývány	nazývat	k5eAaImNgFnP	nazývat
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
stav	stav	k1gInSc1	stav
bodové	bodový	k2eAgFnSc2d1	bodová
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
určen	určit	k5eAaPmNgInS	určit
<g/>
,	,	kIx,	,
zadáme	zadat	k5eAaPmIp1nP	zadat
<g/>
-li	i	k?	-li
jeho	on	k3xPp3gInSc4	on
polohový	polohový	k2eAgInSc4d1	polohový
vektor	vektor	k1gInSc4	vektor
a	a	k8xC	a
vektor	vektor	k1gInSc4	vektor
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
hybnost	hybnost	k1gFnSc1	hybnost
jsou	být	k5eAaImIp3nP	být
úplným	úplný	k2eAgInSc7d1	úplný
systémem	systém	k1gInSc7	systém
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
(	(	kIx(	(
<g/>
ÚSP	ÚSP	kA	ÚSP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Alternativně	alternativně	k6eAd1	alternativně
lze	lze	k6eAd1	lze
udat	udat	k5eAaPmF	udat
místo	místo	k7c2	místo
hybnosti	hybnost	k1gFnSc2	hybnost
například	například	k6eAd1	například
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
<g/>
;	;	kIx,	;
výběr	výběr	k1gInSc4	výběr
ÚSP	ÚSP	kA	ÚSP
použitých	použitý	k2eAgInPc2d1	použitý
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
libovolný	libovolný	k2eAgInSc4d1	libovolný
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
sada	sada	k1gFnSc1	sada
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
co	co	k8xS	co
nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
hodící	hodící	k2eAgFnPc1d1	hodící
k	k	k7c3	k
výpočtům	výpočet	k1gInPc3	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
funkcí	funkce	k1gFnPc2	funkce
těch	ten	k3xDgInPc2	ten
z	z	k7c2	z
ÚSP	ÚSP	kA	ÚSP
-	-	kIx~	-
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
volného	volný	k2eAgInSc2d1	volný
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
kvadrátem	kvadrát	k1gInSc7	kvadrát
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Řešením	řešení	k1gNnSc7	řešení
pohybové	pohybový	k2eAgFnSc2d1	pohybová
rovnice	rovnice	k1gFnSc2	rovnice
pak	pak	k6eAd1	pak
principiálně	principiálně	k6eAd1	principiálně
můžeme	moct	k5eAaImIp1nP	moct
spočíst	spočíst	k5eAaPmF	spočíst
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
bude	být	k5eAaImBp3nS	být
stav	stav	k1gInSc1	stav
systému	systém	k1gInSc2	systém
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
stavu	stav	k1gInSc2	stav
systému	systém	k1gInSc2	systém
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
rozdílem	rozdíl	k1gInSc7	rozdíl
oproti	oproti	k7c3	oproti
klasické	klasický	k2eAgFnSc3d1	klasická
fyzice	fyzika	k1gFnSc3	fyzika
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vybraná	vybraná	k1gFnSc1	vybraná
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
stavu	stav	k1gInSc6	stav
nemá	mít	k5eNaImIp3nS	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
této	tento	k3xDgFnSc2	tento
veličiny	veličina	k1gFnSc2	veličina
můžeme	moct	k5eAaImIp1nP	moct
dostat	dostat	k5eAaPmF	dostat
různé	různý	k2eAgInPc4d1	různý
výsledky	výsledek	k1gInPc4	výsledek
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
naměření	naměření	k1gNnSc2	naměření
hodnoty	hodnota	k1gFnSc2	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
veličiny	veličina	k1gFnPc1	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
můžeme	moct	k5eAaImIp1nP	moct
označit	označit	k5eAaPmF	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
místo	místo	k7c2	místo
jedné	jeden	k4xCgFnSc2	jeden
hodnoty	hodnota	k1gFnSc2	hodnota
polohy	poloha	k1gFnSc2	poloha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
určující	určující	k2eAgFnSc1d1	určující
pozici	pozice	k1gFnSc4	pozice
částice	částice	k1gFnSc2	částice
tak	tak	k6eAd1	tak
musíme	muset	k5eAaImIp1nP	muset
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
systému	systém	k1gInSc2	systém
udat	udat	k5eAaPmF	udat
pravděpodobnosti	pravděpodobnost	k1gFnPc4	pravděpodobnost
nalezení	nalezení	k1gNnSc2	nalezení
částice	částice	k1gFnSc2	částice
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Protože	protože	k8xS	protože
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
spojitý	spojitý	k2eAgInSc1d1	spojitý
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
udávat	udávat	k5eAaImF	udávat
hustotu	hustota	k1gFnSc4	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
rozlišení	rozlišení	k1gNnSc4	rozlišení
budeme	být	k5eAaImBp1nP	být
dále	daleko	k6eAd2	daleko
vynechávat	vynechávat	k5eAaImF	vynechávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
podstaty	podstata	k1gFnSc2	podstata
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
zásadní	zásadní	k2eAgInPc4d1	zásadní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
ovšem	ovšem	k9	ovšem
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
speciální	speciální	k2eAgInPc4d1	speciální
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měřením	měření	k1gNnSc7	měření
vybrané	vybraná	k1gFnSc2	vybraná
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
(	(	kIx(	(
<g/>
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
polohy	poloha	k1gFnSc2	poloha
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
získat	získat	k5eAaPmF	získat
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc4d1	jediná
hodnotu	hodnota	k1gFnSc4	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
≠	≠	k?	≠
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
stavům	stav	k1gInPc3	stav
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
stavy	stav	k1gInPc1	stav
vlastní	vlastní	k2eAgInPc1d1	vlastní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stavy	stav	k1gInPc1	stav
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
hodnotou	hodnota	k1gFnSc7	hodnota
pozorovatelné	pozorovatelný	k2eAgFnSc3d1	pozorovatelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
tyto	tento	k3xDgInPc4	tento
stavy	stav	k1gInPc4	stav
jen	jen	k9	jen
malou	malý	k2eAgFnSc7d1	malá
podmnožinou	podmnožina	k1gFnSc7	podmnožina
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
stavů	stav	k1gInPc2	stav
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
interference	interference	k1gFnSc2	interference
nestačí	stačit	k5eNaBmIp3nS	stačit
udávat	udávat	k5eAaImF	udávat
pravděpodobnostní	pravděpodobnostní	k2eAgFnSc4d1	pravděpodobnostní
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
ale	ale	k8xC	ale
komplexní	komplexní	k2eAgFnSc4d1	komplexní
amplitudu	amplituda	k1gFnSc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
ψ	ψ	k?	ψ
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Amplituda	amplituda	k1gFnSc1	amplituda
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
funkcí	funkce	k1gFnSc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
možné	možný	k2eAgNnSc1d1	možné
udávat	udávat	k5eAaImF	udávat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
a	a	k8xC	a
fázový	fázový	k2eAgInSc1d1	fázový
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
parametrizace	parametrizace	k1gFnSc1	parametrizace
pomocí	pomocí	k7c2	pomocí
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
elegantnější	elegantní	k2eAgMnSc1d2	elegantnější
a	a	k8xC	a
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
v	v	k7c6	v
ÚSP	ÚSP	kA	ÚSP
pozorovatelných	pozorovatelný	k2eAgInPc2d1	pozorovatelný
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
označme	označit	k5eAaPmRp1nP	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
,	,	kIx,	,
C	C	kA	C
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
znát	znát	k5eAaImF	znát
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
naměření	naměření	k1gNnSc2	naměření
<g />
.	.	kIx.	.
</s>
<s hack="1">
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
kombinaci	kombinace	k1gFnSc4	kombinace
hodnot	hodnota	k1gFnPc2	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
<g />
.	.	kIx.	.
</s>
<s hack="1">
také	také	k6eAd1	také
popisujeme	popisovat	k5eAaImIp1nP	popisovat
systém	systém	k1gInSc4	systém
pomocí	pomocí	k7c2	pomocí
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
více	hodně	k6eAd2	hodně
proměnných	proměnný	k2eAgFnPc2d1	proměnná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
procesem	proces	k1gInSc7	proces
měření	měření	k1gNnPc1	měření
realizována	realizovat	k5eAaBmNgFnS	realizovat
konceptem	koncept	k1gInSc7	koncept
kolapsu	kolaps	k1gInSc2	kolaps
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Měříme	měřit	k5eAaImIp1nP	měřit
<g/>
-li	i	k?	-li
pozorovatelnou	pozorovatelna	k1gFnSc7	pozorovatelna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
a	a	k8xC	a
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
výsledek	výsledek	k1gInSc4	výsledek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
systém	systém	k1gInSc1	systém
"	"	kIx"	"
<g/>
zkolabuje	zkolabovat	k5eAaPmIp3nS	zkolabovat
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s hack="1">
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
hodnotou	hodnota	k1gFnSc7	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
=	=	kIx~	=
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tj.	tj.	kA	tj.
jeho	jeho	k3xOp3gFnSc1	jeho
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
změní	změnit	k5eAaPmIp3nS	změnit
-	-	kIx~	-
nikoliv	nikoliv	k9	nikoliv
vývojem	vývoj	k1gInSc7	vývoj
daným	daný	k2eAgInSc7d1	daný
Schrödingerovou	Schrödingerův	k2eAgFnSc7d1	Schrödingerova
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
procesem	proces	k1gInSc7	proces
měření	měření	k1gNnSc2	měření
jako	jako	k8xC	jako
takovým	takový	k3xDgFnPc3	takový
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
bezprostředně	bezprostředně	k6eAd1	bezprostředně
následující	následující	k2eAgNnSc4d1	následující
měření	měření	k1gNnSc4	měření
téže	týž	k3xTgFnSc2	týž
veličiny	veličina	k1gFnSc2	veličina
pak	pak	k6eAd1	pak
dá	dát	k5eAaPmIp3nS	dát
týž	týž	k3xTgInSc4	týž
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Provedeme	provést	k5eAaPmIp1nP	provést
<g/>
-li	i	k?	-li
měření	měření	k1gNnSc4	měření
téže	týž	k3xTgFnSc2	týž
veličiny	veličina	k1gFnSc2	veličina
na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
systému	systém	k1gInSc6	systém
dvakrát	dvakrát	k6eAd1	dvakrát
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
získáme	získat	k5eAaPmIp1nP	získat
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
dané	daný	k2eAgFnSc2d1	daná
veličiny	veličina	k1gFnSc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
pozorovatelných	pozorovatelný	k2eAgFnPc6d1	pozorovatelná
-	-	kIx~	-
pokud	pokud	k8xS	pokud
měříme	měřit	k5eAaImIp1nP	měřit
veličinu	veličina	k1gFnSc4	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
poté	poté	k6eAd1	poté
veličinu	veličina	k1gFnSc4	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
následujícím	následující	k2eAgNnSc7d1	následující
měřením	měření	k1gNnSc7	měření
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
získat	získat	k5eAaPmF	získat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
různé	různý	k2eAgInPc1d1	různý
od	od	k7c2	od
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
přirozeně	přirozeně	k6eAd1	přirozeně
interpretujeme	interpretovat	k5eAaBmIp1nP	interpretovat
jako	jako	k9	jako
nemožnost	nemožnost	k1gFnSc4	nemožnost
současného	současný	k2eAgNnSc2d1	současné
měření	měření	k1gNnSc2	měření
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
nekompatibilní	kompatibilní	k2eNgFnPc1d1	nekompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
ÚSP	ÚSP	kA	ÚSP
pak	pak	k6eAd1	pak
zřejmě	zřejmě	k6eAd1	zřejmě
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jen	jen	k9	jen
kompatibilními	kompatibilní	k2eAgFnPc7d1	kompatibilní
pozorovatelnými	pozorovatelný	k2eAgFnPc7d1	pozorovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Praktickým	praktický	k2eAgInSc7d1	praktický
příkladem	příklad	k1gInSc7	příklad
nekompatibilních	kompatibilní	k2eNgInPc2d1	nekompatibilní
pozorovatelných	pozorovatelný	k2eAgInPc2d1	pozorovatelný
je	být	k5eAaImIp3nS	být
hybnost	hybnost	k1gFnSc1	hybnost
a	a	k8xC	a
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nekompatibilita	nekompatibilita	k1gFnSc1	nekompatibilita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
dokonce	dokonce	k9	dokonce
maximální	maximální	k2eAgMnSc1d1	maximální
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
totiž	totiž	k9	totiž
žádný	žádný	k3yNgInSc1	žádný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
ostrou	ostrý	k2eAgFnSc4d1	ostrá
hodnotu	hodnota	k1gFnSc4	hodnota
obou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgFnPc2	tento
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
stavu	stav	k1gInSc6	stav
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
hodnotou	hodnota	k1gFnSc7	hodnota
souřadnice	souřadnice	k1gFnSc2	souřadnice
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hybnost	hybnost	k1gFnSc1	hybnost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
)	)	kIx)	)
libovolná	libovolný	k2eAgFnSc1d1	libovolná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
rovnoměrným	rovnoměrný	k2eAgNnSc7d1	rovnoměrné
rozdělením	rozdělení	k1gNnSc7	rozdělení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
bodové	bodový	k2eAgFnSc2d1	bodová
částice	částice	k1gFnSc2	částice
nemůže	moct	k5eNaImIp3nS	moct
hybnost	hybnost	k1gFnSc1	hybnost
a	a	k8xC	a
souřadnice	souřadnice	k1gFnSc1	souřadnice
tvořit	tvořit	k5eAaImF	tvořit
ÚSP	ÚSP	kA	ÚSP
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
kvantové	kvantový	k2eAgNnSc1d1	kvantové
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ÚSP	ÚSP	kA	ÚSP
tvořen	tvořit	k5eAaImNgInS	tvořit
jen	jen	k9	jen
hybností	hybnost	k1gFnSc7	hybnost
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
souřadnicí	souřadnice	k1gFnSc7	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
princip	princip	k1gInSc1	princip
superpozice	superpozice	k1gFnSc2	superpozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
otázka	otázka	k1gFnSc1	otázka
přípravy	příprava	k1gFnSc2	příprava
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
klasická	klasický	k2eAgFnSc1d1	klasická
fyzika	fyzika	k1gFnSc1	fyzika
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
neřeší	řešit	k5eNaImIp3nS	řešit
(	(	kIx(	(
<g/>
připravit	připravit	k5eAaPmF	připravit
systém	systém	k1gInSc4	systém
do	do	k7c2	do
nějakého	nějaký	k3yIgInSc2	nějaký
výchozího	výchozí	k2eAgInSc2d1	výchozí
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
problém	problém	k1gInSc4	problém
ryze	ryze	k6eAd1	ryze
technický	technický	k2eAgMnSc1d1	technický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
kvantové	kvantový	k2eAgFnSc2d1	kvantová
narážíme	narážet	k5eAaImIp1nP	narážet
na	na	k7c4	na
koncepční	koncepční	k2eAgInSc4d1	koncepční
problém	problém	k1gInSc4	problém
<g/>
:	:	kIx,	:
abychom	aby	kYmCp1nP	aby
vůbec	vůbec	k9	vůbec
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgNnSc6	jaký
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
provádět	provádět	k5eAaImF	provádět
měření	měření	k1gNnSc4	měření
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
systém	systém	k1gInSc1	systém
nechová	chovat	k5eNaImIp3nS	chovat
deterministicky	deterministicky	k6eAd1	deterministicky
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
příprava	příprava	k1gFnSc1	příprava
stavu	stav	k1gInSc2	stav
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
procedurou	procedura	k1gFnSc7	procedura
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
měření	měření	k1gNnSc2	měření
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gInSc1	jehož
výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
náhodný	náhodný	k2eAgMnSc1d1	náhodný
<g/>
)	)	kIx)	)
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
zkolaboval	zkolabovat	k5eAaPmAgInS	zkolabovat
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsme	být	k5eAaImIp1nP	být
naměřili	naměřit	k5eAaBmAgMnP	naměřit
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
připravili	připravit	k5eAaPmAgMnP	připravit
systém	systém	k1gInSc4	systém
v	v	k7c6	v
onom	onen	k3xDgInSc6	onen
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
měříme	měřit	k5eAaImIp1nP	měřit
například	například	k6eAd1	například
polohu	poloha	k1gFnSc4	poloha
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
svítíme	svítit	k5eAaImIp1nP	svítit
a	a	k8xC	a
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
odražené	odražený	k2eAgInPc4d1	odražený
fotony	foton	k1gInPc4	foton
<g/>
)	)	kIx)	)
a	a	k8xC	a
naměříme	naměřit	k5eAaBmIp1nP	naměřit
nějakou	nějaký	k3yIgFnSc4	nějaký
hodnotu	hodnota	k1gFnSc4	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
připravili	připravit	k5eAaPmAgMnP	připravit
částici	částice	k1gFnSc4	částice
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Povšimněme	povšimnout	k5eAaPmRp1nP	povšimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prakticky	prakticky	k6eAd1	prakticky
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
stavu	stav	k1gInSc6	stav
byl	být	k5eAaImAgMnS	být
systém	systém	k1gInSc4	systém
před	před	k7c7	před
prvním	první	k4xOgNnSc7	první
měřením	měření	k1gNnSc7	měření
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
je	být	k5eAaImIp3nS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
neschopnost	neschopnost	k1gFnSc1	neschopnost
určit	určit	k5eAaPmF	určit
onen	onen	k3xDgInSc4	onen
bod	bod	k1gInSc4	bod
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
trochu	trochu	k6eAd1	trochu
modifikovaná	modifikovaný	k2eAgFnSc1d1	modifikovaná
procedura	procedura	k1gFnSc1	procedura
-	-	kIx~	-
měřit	měřit	k5eAaImF	měřit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
částice	částice	k1gFnSc1	částice
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
získáme	získat	k5eAaPmIp1nP	získat
ano	ano	k9	ano
<g/>
/	/	kIx~	/
<g/>
ne	ne	k9	ne
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kladného	kladný	k2eAgInSc2d1	kladný
výsledku	výsledek	k1gInSc2	výsledek
měření	měření	k1gNnSc2	měření
máme	mít	k5eAaImIp1nP	mít
systém	systém	k1gInSc4	systém
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
jsme	být	k5eAaImIp1nP	být
usilovali	usilovat	k5eAaImAgMnP	usilovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
můžeme	moct	k5eAaImIp1nP	moct
provést	provést	k5eAaPmF	provést
třeba	třeba	k6eAd1	třeba
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
svítíme	svítit	k5eAaImIp1nP	svítit
jen	jen	k9	jen
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Technické	technický	k2eAgInPc1d1	technický
aspekty	aspekt	k1gInPc1	aspekt
měření	měření	k1gNnSc2	měření
můžeme	moct	k5eAaImIp1nP	moct
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
,	,	kIx,	,
říkejme	říkat	k5eAaImRp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
do	do	k7c2	do
daného	daný	k2eAgInSc2d1	daný
bodu	bod	k1gInSc2	bod
umístili	umístit	k5eAaPmAgMnP	umístit
detektor	detektor	k1gInSc4	detektor
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ignorujme	ignorovat	k5eAaImRp1nP	ignorovat
také	také	k9	také
konečné	konečný	k2eAgInPc1d1	konečný
rozměry	rozměr	k1gInPc1	rozměr
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nenulovou	nulový	k2eNgFnSc4d1	nenulová
systematickou	systematický	k2eAgFnSc4d1	systematická
chybu	chyba	k1gFnSc4	chyba
měření	měření	k1gNnSc2	měření
detektoru	detektor	k1gInSc2	detektor
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
když	když	k8xS	když
umístíme	umístit	k5eAaPmIp1nP	umístit
dva	dva	k4xCgInPc4	dva
detektory	detektor	k1gInPc4	detektor
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
bodů	bod	k1gInPc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
?	?	kIx.	?
</s>
<s>
Pokud	pokud	k8xS	pokud
budeme	být	k5eAaImBp1nP	být
detekovat	detekovat	k5eAaImF	detekovat
signál	signál	k1gInSc4	signál
zvlášť	zvlášť	k6eAd1	zvlášť
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
detektorů	detektor	k1gInPc2	detektor
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
podstatně	podstatně	k6eAd1	podstatně
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
případu	případ	k1gInSc2	případ
-	-	kIx~	-
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kladného	kladný	k2eAgInSc2d1	kladný
výsledku	výsledek	k1gInSc2	výsledek
měření	měření	k1gNnSc2	měření
získáme	získat	k5eAaPmIp1nP	získat
částici	částice	k1gFnSc4	částice
umístěnou	umístěný	k2eAgFnSc4d1	umístěná
buď	buď	k8xC	buď
v	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
když	když	k8xS	když
ale	ale	k8xC	ale
budeme	být	k5eAaImBp1nP	být
detekovat	detekovat	k5eAaImF	detekovat
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
ignorujíce	ignorovat	k5eAaImSgFnP	ignorovat
z	z	k7c2	z
jakého	jaký	k3yQgInSc2	jaký
detektoru	detektor	k1gInSc2	detektor
vyšel	vyjít	k5eAaPmAgInS	vyjít
<g/>
?	?	kIx.	?
</s>
<s>
Naivně	naivně	k6eAd1	naivně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stav	stav	k1gInSc1	stav
bude	být	k5eAaImBp3nS	být
buď	budit	k5eAaImRp2nS	budit
částice	částice	k1gFnSc2	částice
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
nebo	nebo	k8xC	nebo
částice	částice	k1gFnPc1	částice
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
nebudeme	být	k5eNaImBp1nP	být
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
možnost	možnost	k1gFnSc4	možnost
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bude	být	k5eAaImBp3nS	být
ale	ale	k9	ale
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
výsledného	výsledný	k2eAgInSc2d1	výsledný
stavu	stav	k1gInSc2	stav
lineární	lineární	k2eAgFnSc7d1	lineární
kombinací	kombinace	k1gFnSc7	kombinace
vlnových	vlnový	k2eAgFnPc2d1	vlnová
funkcí	funkce	k1gFnPc2	funkce
stavů	stav	k1gInPc2	stav
soustředěných	soustředěný	k2eAgInPc2d1	soustředěný
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
koeficienty	koeficient	k1gInPc1	koeficient
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lineární	lineární	k2eAgFnSc6d1	lineární
kombinaci	kombinace	k1gFnSc6	kombinace
budou	být	k5eAaImBp3nP	být
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
systému	systém	k1gInSc2	systém
před	před	k7c7	před
měřením	měření	k1gNnSc7	měření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
podivuhodná	podivuhodný	k2eAgFnSc1d1	podivuhodná
vlastnost	vlastnost	k1gFnSc1	vlastnost
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
(	(	kIx(	(
<g/>
všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
stav	stav	k1gInSc1	stav
systému	systém	k1gInSc2	systém
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
informace	informace	k1gFnSc2	informace
experimentátor	experimentátor	k1gMnSc1	experimentátor
extrahuje	extrahovat	k5eAaBmIp3nS	extrahovat
z	z	k7c2	z
měřicí	měřicí	k2eAgFnSc2d1	měřicí
aparatury	aparatura	k1gFnSc2	aparatura
<g/>
)	)	kIx)	)
a	a	k8xC	a
sčítání	sčítání	k1gNnSc1	sčítání
vlnových	vlnový	k2eAgFnPc2d1	vlnová
funkcí	funkce	k1gFnPc2	funkce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
principem	princip	k1gInSc7	princip
superpozice	superpozice	k1gFnSc2	superpozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
klasické	klasický	k2eAgFnSc2d1	klasická
pohybové	pohybový	k2eAgFnSc2d1	pohybová
rovnice	rovnice	k1gFnSc2	rovnice
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rovnice	rovnice	k1gFnSc1	rovnice
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
rovnice	rovnice	k1gFnSc1	rovnice
řídící	řídící	k2eAgFnSc2d1	řídící
časový	časový	k2eAgInSc4d1	časový
vývoj	vývoj	k1gInSc4	vývoj
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rovnice	rovnice	k1gFnSc1	rovnice
popisuje	popisovat	k5eAaImIp3nS	popisovat
vývoj	vývoj	k1gInSc4	vývoj
systému	systém	k1gInSc2	systém
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
žádné	žádný	k3yNgNnSc1	žádný
měření	měření	k1gNnSc1	měření
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
systém	systém	k1gInSc1	systém
dostatečně	dostatečně	k6eAd1	dostatečně
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
řízením	řízení	k1gNnSc7	řízení
Schrödingerovy	Schrödingerův	k2eAgFnSc2d1	Schrödingerova
rovnice	rovnice	k1gFnSc2	rovnice
v	v	k7c6	v
čase	čas	k1gInSc6	čas
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
spojitě	spojitě	k6eAd1	spojitě
<g/>
.	.	kIx.	.
</s>
<s>
Tzn.	tzn.	kA	tzn.
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
časového	časový	k2eAgInSc2d1	časový
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
<g/>
:	:	kIx,	:
Hladký	hladký	k2eAgInSc1d1	hladký
vývoj	vývoj	k1gInSc1	vývoj
daný	daný	k2eAgInSc1d1	daný
Schrödingerovou	Schrödingerův	k2eAgFnSc7d1	Schrödingerova
rovnicí	rovnice	k1gFnSc7	rovnice
jsoucí	jsoucí	k2eAgFnSc7d1	jsoucí
obdobou	obdoba	k1gFnSc7	obdoba
vývoje	vývoj	k1gInSc2	vývoj
klasického	klasický	k2eAgInSc2d1	klasický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ryze	ryze	k6eAd1	ryze
kvantový	kvantový	k2eAgInSc4d1	kvantový
kolaps	kolaps	k1gInSc4	kolaps
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
k	k	k7c3	k
symbolice	symbolika	k1gFnSc3	symbolika
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
jsou	být	k5eAaImIp3nP	být
vektory	vektor	k1gInPc1	vektor
označovány	označovat	k5eAaImNgInP	označovat
malými	malý	k2eAgInPc7d1	malý
latinskými	latinský	k2eAgNnPc7d1	latinské
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
operátory	operátor	k1gInPc1	operátor
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
označovány	označovat	k5eAaImNgInP	označovat
velkými	velký	k2eAgFnPc7d1	velká
písmeny	písmeno	k1gNnPc7	písmeno
se	se	k3xPyFc4	se
stříškou	stříška	k1gFnSc7	stříška
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
notace	notace	k1gFnSc1	notace
je	být	k5eAaImIp3nS	být
jistým	jistý	k2eAgInSc7d1	jistý
kompromisem	kompromis	k1gInSc7	kompromis
mezi	mezi	k7c7	mezi
notací	notace	k1gFnSc7	notace
užívanou	užívaný	k2eAgFnSc7d1	užívaná
matematickými	matematický	k2eAgInPc7d1	matematický
fyziky	fyzik	k1gMnPc4	fyzik
a	a	k8xC	a
matematiky	matematik	k1gMnPc4	matematik
-	-	kIx~	-
kde	kde	k6eAd1	kde
chybí	chybět	k5eAaImIp3nS	chybět
i	i	k9	i
stříška	stříška	k1gFnSc1	stříška
u	u	k7c2	u
operátorů	operátor	k1gInPc2	operátor
-	-	kIx~	-
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
Diracovou	Diracová	k1gFnSc7	Diracová
(	(	kIx(	(
<g/>
braketovou	braketový	k2eAgFnSc7d1	braketový
<g/>
)	)	kIx)	)
notací	notace	k1gFnSc7	notace
<g/>
.	.	kIx.	.
</s>
<s>
Diracova	Diracův	k2eAgFnSc1d1	Diracova
notace	notace	k1gFnSc1	notace
je	být	k5eAaImIp3nS	být
přehlednější	přehlední	k2eAgMnSc1d2	přehlední
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
typograficky	typograficky	k6eAd1	typograficky
náročnější	náročný	k2eAgMnSc1d2	náročnější
a	a	k8xC	a
pro	pro	k7c4	pro
konzistentní	konzistentní	k2eAgNnSc4d1	konzistentní
zavedení	zavedení	k1gNnSc4	zavedení
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zavádět	zavádět	k5eAaImF	zavádět
duální	duální	k2eAgInPc4d1	duální
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
detailnější	detailní	k2eAgNnSc4d2	detailnější
porozumění	porozumění	k1gNnSc4	porozumění
kvantové	kvantový	k2eAgFnSc3d1	kvantová
teorii	teorie	k1gFnSc3	teorie
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
uvažovat	uvažovat	k5eAaImF	uvažovat
obecnější	obecní	k2eAgInSc4d2	obecní
a	a	k8xC	a
abstraktnější	abstraktní	k2eAgInSc4d2	abstraktnější
aparát	aparát	k1gInSc4	aparát
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
lineární	lineární	k2eAgFnSc6d1	lineární
algebře	algebra	k1gFnSc6	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
Hilbertův	Hilbertův	k2eAgInSc4d1	Hilbertův
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
úplný	úplný	k2eAgInSc1d1	úplný
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
nenulový	nulový	k2eNgInSc1d1	nenulový
vektor	vektor	k1gInSc1	vektor
představuje	představovat	k5eAaImIp3nS	představovat
možný	možný	k2eAgInSc1d1	možný
stav	stav	k1gInSc1	stav
systému	systém	k1gInSc2	systém
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
sama	sám	k3xTgFnSc1	sám
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
reprezentaci	reprezentace	k1gFnSc3	reprezentace
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
vektoru	vektor	k1gInSc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
popisu	popis	k1gInSc2	popis
je	být	k5eAaImIp3nS	být
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
vzorců	vzorec	k1gInPc2	vzorec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
místo	místo	k7c2	místo
nepřehledných	přehledný	k2eNgFnPc2d1	nepřehledná
integrálních	integrální	k2eAgFnPc2d1	integrální
formulí	formule	k1gFnPc2	formule
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
stavových	stavový	k2eAgInPc2d1	stavový
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
jsme	být	k5eAaImIp1nP	být
pouze	pouze	k6eAd1	pouze
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
naměření	naměření	k1gNnSc4	naměření
určité	určitý	k2eAgFnSc2d1	určitá
hodnoty	hodnota	k1gFnSc2	hodnota
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
argumentu	argument	k1gInSc2	argument
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
výběr	výběr	k1gInSc4	výběr
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
použijeme	použít	k5eAaPmIp1nP	použít
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
libovolný	libovolný	k2eAgInSc4d1	libovolný
<g/>
.	.	kIx.	.
</s>
<s>
Jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
naměření	naměření	k1gNnSc2	naměření
hodnoty	hodnota	k1gFnSc2	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
funkcí	funkce	k1gFnSc7	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
?	?	kIx.	?
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
tuto	tento	k3xDgFnSc4	tento
amplitudu	amplituda	k1gFnSc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
s	s	k7c7	s
vektorem	vektor	k1gInSc7	vektor
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
stavu	stav	k1gInSc6	stav
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
hodnotou	hodnota	k1gFnSc7	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
(	(	kIx(	(
ψ	ψ	k?	ψ
,	,	kIx,	,
:	:	kIx,	:
v	v	k7c4	v
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
,	,	kIx,	,
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Disponujeme	disponovat	k5eAaBmIp1nP	disponovat
<g/>
-li	i	k?	-li
systémem	systém	k1gInSc7	systém
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
kopiích	kopie	k1gFnPc6	kopie
a	a	k8xC	a
jsme	být	k5eAaImIp1nP	být
<g/>
-li	i	k?	-li
teoreticky	teoreticky	k6eAd1	teoreticky
schopni	schopen	k2eAgMnPc1d1	schopen
všechny	všechen	k3xTgFnPc4	všechen
kopie	kopie	k1gFnPc4	kopie
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
stavu	stav	k1gInSc2	stav
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
ptát	ptát	k5eAaImF	ptát
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
bude	být	k5eAaImBp3nS	být
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
X	X	kA	X
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}}	}}}	k?	}}}
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
,	,	kIx,	,
tj.	tj.	kA	tj.
aritmetický	aritmetický	k2eAgInSc1d1	aritmetický
průměr	průměr	k1gInSc1	průměr
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kopiích	kopie	k1gFnPc6	kopie
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
váženým	vážený	k2eAgInSc7d1	vážený
aritmetickým	aritmetický	k2eAgInSc7d1	aritmetický
průměrem	průměr	k1gInSc7	průměr
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
naměřené	naměřený	k2eAgFnPc4d1	naměřená
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
váhy	váha	k1gFnPc1	váha
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
určeny	určit	k5eAaPmNgFnP	určit
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
naměření	naměření	k1gNnSc2	naměření
příslušné	příslušný	k2eAgFnSc2d1	příslušná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
ψ	ψ	k?	ψ
,	,	kIx,	,
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
,	,	kIx,	,
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
operátor	operátor	k1gInSc1	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnSc3	každý
pozorovatelné	pozorovatelný	k2eAgFnSc3d1	pozorovatelná
veličině	veličina	k1gFnSc3	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
přiřazen	přiřazen	k2eAgInSc4d1	přiřazen
hermitovský	hermitovský	k2eAgInSc4d1	hermitovský
operátor	operátor	k1gInSc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
Y	Y	kA	Y
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInPc1d1	vlastní
vektory	vektor	k1gInPc1	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
tohoto	tento	k3xDgInSc2	tento
operátoru	operátor	k1gInSc2	operátor
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
stavy	stav	k1gInPc1	stav
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
hodnotou	hodnota	k1gFnSc7	hodnota
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rovna	roven	k2eAgFnSc1d1	rovna
příslušnému	příslušný	k2eAgNnSc3d1	příslušné
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
číslu	číslo	k1gNnSc3	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
reprezentace	reprezentace	k1gFnSc1	reprezentace
pozorovatelných	pozorovatelný	k2eAgMnPc2d1	pozorovatelný
má	mít	k5eAaImIp3nS	mít
nezanedbatelné	zanedbatelný	k2eNgFnPc4d1	nezanedbatelná
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
díky	díky	k7c3	díky
ortogonalitě	ortogonalita	k1gFnSc3	ortogonalita
vlastních	vlastní	k2eAgInPc2d1	vlastní
vektorů	vektor	k1gInPc2	vektor
samosdruženého	samosdružený	k2eAgInSc2d1	samosdružený
operátoru	operátor	k1gInSc2	operátor
můžeme	moct	k5eAaImIp1nP	moct
operátor	operátor	k1gInSc4	operátor
rozložit	rozložit	k5eAaPmF	rozložit
podle	podle	k7c2	podle
následujícího	následující	k2eAgInSc2d1	následující
přepisu	přepis	k1gInSc2	přepis
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
ψ	ψ	k?	ψ
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
hodnotu	hodnota	k1gFnSc4	hodnota
se	se	k3xPyFc4	se
zjednoduší	zjednodušit	k5eAaPmIp3nP	zjednodušit
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
̄	̄	k?	̄
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
ψ	ψ	k?	ψ
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Operátory	operátor	k1gInPc1	operátor
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
kompatibilním	kompatibilní	k2eAgFnPc3d1	kompatibilní
veličinám	veličina	k1gFnPc3	veličina
navzájem	navzájem	k6eAd1	navzájem
komutují	komutovat	k5eAaBmIp3nP	komutovat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nekomutující	komutující	k2eNgInPc1d1	komutující
operátory	operátor	k1gInPc1	operátor
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
nekompatibilní	kompatibilní	k2eNgInPc1d1	nekompatibilní
(	(	kIx(	(
<g/>
komplementární	komplementární	k2eAgFnPc1d1	komplementární
<g/>
)	)	kIx)	)
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
veličiny	veličina	k1gFnPc1	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Operátory	operátor	k1gInPc1	operátor
také	také	k9	také
hrají	hrát	k5eAaImIp3nP	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
kvantování	kvantování	k1gNnSc2	kvantování
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kvantování	kvantování	k1gNnSc2	kvantování
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
operátorů	operátor	k1gInPc2	operátor
je	být	k5eAaImIp3nS	být
klíčové	klíčový	k2eAgNnSc1d1	klíčové
pro	pro	k7c4	pro
proceduru	procedura	k1gFnSc4	procedura
zvanou	zvaný	k2eAgFnSc4d1	zvaná
kvantování	kvantování	k1gNnSc4	kvantování
<g/>
.	.	kIx.	.
</s>
<s>
Kvantováním	kvantování	k1gNnSc7	kvantování
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
postup	postup	k1gInSc1	postup
vedoucí	vedoucí	k1gMnSc1	vedoucí
k	k	k7c3	k
"	"	kIx"	"
<g/>
odvození	odvození	k1gNnSc3	odvození
<g/>
"	"	kIx"	"
kvantových	kvantový	k2eAgFnPc2d1	kvantová
pohybových	pohybový	k2eAgFnPc2d1	pohybová
rovnic	rovnice	k1gFnPc2	rovnice
ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
příslušného	příslušný	k2eAgInSc2d1	příslušný
klasického	klasický	k2eAgInSc2d1	klasický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Určíme	určit	k5eAaPmIp1nP	určit
základní	základní	k2eAgFnPc4d1	základní
pozorovatelné	pozorovatelný	k2eAgFnPc4d1	pozorovatelná
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
souřadnici	souřadnice	k1gFnSc4	souřadnice
a	a	k8xC	a
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
přiřadíme	přiřadit	k5eAaPmIp1nP	přiřadit
jim	on	k3xPp3gMnPc3	on
operátory	operátor	k1gMnPc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
konjugované	konjugovaný	k2eAgFnPc1d1	konjugovaná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
klasicky	klasicky	k6eAd1	klasicky
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
Poissonova	Poissonův	k2eAgFnSc1d1	Poissonova
závorka	závorka	k1gFnSc1	závorka
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
jedné	jeden	k4xCgFnSc2	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Přiřazené	přiřazený	k2eAgInPc1d1	přiřazený
kvantové	kvantový	k2eAgInPc1d1	kvantový
operátory	operátor	k1gInPc1	operátor
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nekomutující	komutující	k2eNgMnPc1d1	komutující
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
komutátor	komutátor	k1gInSc1	komutátor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
roven	roven	k2eAgInSc1d1	roven
imaginární	imaginární	k2eAgFnSc3d1	imaginární
jednotce	jednotka	k1gFnSc3	jednotka
násobené	násobený	k2eAgInPc1d1	násobený
redukovanou	redukovaný	k2eAgFnSc7d1	redukovaná
Planckovou	Planckův	k2eAgFnSc7d1	Planckova
konstantou	konstanta	k1gFnSc7	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Komutátor	komutátor	k1gInSc1	komutátor
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
klasické	klasický	k2eAgFnSc3d1	klasická
Poissonově	Poissonův	k2eAgFnSc3d1	Poissonova
závorce	závorka	k1gFnSc3	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
volba	volba	k1gFnSc1	volba
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
operátorů	operátor	k1gMnPc2	operátor
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
násobením	násobení	k1gNnSc7	násobení
argumentem	argument	k1gInSc7	argument
vlnové	vlnový	k2eAgFnPc4d1	vlnová
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
ψ	ψ	k?	ψ
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
zatímco	zatímco	k8xS	zatímco
druhý	druhý	k4xOgInSc4	druhý
operátor	operátor	k1gInSc4	operátor
(	(	kIx(	(
<g/>
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
i-násobkem	iásobek	k1gInSc7	i-násobek
operátoru	operátor	k1gInSc2	operátor
derivace	derivace	k1gFnSc2	derivace
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
^	^	kIx~	^
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
i	i	k9	i
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gMnSc1	hbar
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
operátor	operátor	k1gInSc4	operátor
násobení	násobení	k1gNnSc2	násobení
zvolíme	zvolit	k5eAaPmIp1nP	zvolit
jako	jako	k9	jako
souřadnici	souřadnice	k1gFnSc4	souřadnice
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
souřadnicové	souřadnicový	k2eAgFnSc6d1	souřadnicová
reprezentaci	reprezentace	k1gFnSc6	reprezentace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
násobícím	násobící	k2eAgInSc7d1	násobící
operátorem	operátor	k1gInSc7	operátor
je	být	k5eAaImIp3nS	být
hybnost	hybnost	k1gFnSc1	hybnost
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
reprezentaci	reprezentace	k1gFnSc6	reprezentace
hybnostní	hybnostní	k2eAgFnSc6d1	hybnostní
(	(	kIx(	(
<g/>
impulsové	impulsový	k2eAgFnSc6d1	impulsová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
komutátor	komutátor	k1gInSc1	komutátor
těchto	tento	k3xDgMnPc2	tento
operátorů	operátor	k1gMnPc2	operátor
je	být	k5eAaImIp3nS	být
jednotkový	jednotkový	k2eAgInSc4d1	jednotkový
operátor	operátor	k1gInSc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vyjádřeny	vyjádřen	k2eAgFnPc1d1	vyjádřena
jako	jako	k9	jako
funkce	funkce	k1gFnSc1	funkce
těchto	tento	k3xDgNnPc2	tento
základních	základní	k2eAgNnPc2d1	základní
pozorovatelných	pozorovatelný	k2eAgNnPc2d1	pozorovatelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
stejné	stejné	k1gNnSc1	stejné
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
hamiltonián	hamiltonián	k1gMnSc1	hamiltonián
(	(	kIx(	(
<g/>
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
bodové	bodový	k2eAgFnSc2d1	bodová
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
+	+	kIx~	+
V	V	kA	V
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
+	+	kIx~	+
<g/>
V	V	kA	V
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
v	v	k7c6	v
kvantovém	kvantový	k2eAgInSc6d1	kvantový
případě	případ	k1gInSc6	případ
pouze	pouze	k6eAd1	pouze
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
symboly	symbol	k1gInPc4	symbol
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
příslušnými	příslušný	k2eAgInPc7d1	příslušný
operátory	operátor	k1gInPc7	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
musíme	muset	k5eAaImIp1nP	muset
řešit	řešit	k5eAaImF	řešit
určité	určitý	k2eAgFnPc4d1	určitá
nejednoznačnosti	nejednoznačnost	k1gFnPc4	nejednoznačnost
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
definičním	definiční	k2eAgInSc6d1	definiční
oboru	obor	k1gInSc6	obor
neomezených	omezený	k2eNgMnPc2d1	neomezený
operátorů	operátor	k1gMnPc2	operátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
v	v	k7c6	v
interpretaci	interpretace	k1gFnSc6	interpretace
součinů	součin	k1gInPc2	součin
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
souřadnice	souřadnice	k1gFnSc2	souřadnice
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
klasických	klasický	k2eAgInPc6d1	klasický
vztazích	vztah	k1gInPc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kvantové	kvantový	k2eAgFnSc6d1	kvantová
úrovni	úroveň	k1gFnSc6	úroveň
totiž	totiž	k9	totiž
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
pořadí	pořadí	k1gNnSc6	pořadí
v	v	k7c6	v
součinu	součin	k1gInSc6	součin
nekomutujících	komutující	k2eNgInPc2d1	komutující
operátorů	operátor	k1gInPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
u	u	k7c2	u
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
systémů	systém	k1gInPc2	systém
problém	problém	k1gInSc1	problém
s	s	k7c7	s
nejednoznačností	nejednoznačnost	k1gFnSc7	nejednoznačnost
pořadí	pořadí	k1gNnSc2	pořadí
nevzniká	vznikat	k5eNaImIp3nS	vznikat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hamiltonián	hamiltonián	k1gMnSc1	hamiltonián
má	mít	k5eAaImIp3nS	mít
standardně	standardně	k6eAd1	standardně
tvar	tvar	k1gInSc4	tvar
součtu	součet	k1gInSc2	součet
kinetického	kinetický	k2eAgInSc2d1	kinetický
členu	člen	k1gInSc2	člen
(	(	kIx(	(
<g/>
jsoucího	jsoucí	k2eAgInSc2d1	jsoucí
funkcí	funkce	k1gFnPc2	funkce
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
funkcí	funkce	k1gFnSc7	funkce
souřadnice	souřadnice	k1gFnSc2	souřadnice
-	-	kIx~	-
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
hamiltoniánu	hamiltonián	k1gInSc6	hamiltonián
součiny	součin	k1gInPc4	součin
nekompatibilních	kompatibilní	k2eNgInPc2d1	nekompatibilní
pozorovatelných	pozorovatelný	k2eAgInPc2d1	pozorovatelný
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
volba	volba	k1gFnSc1	volba
definičního	definiční	k2eAgInSc2d1	definiční
oboru	obor	k1gInSc2	obor
operátoru	operátor	k1gInSc2	operátor
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
nejednoznačnosti	nejednoznačnost	k1gFnPc4	nejednoznačnost
lze	lze	k6eAd1	lze
rozřešit	rozřešit	k5eAaPmF	rozřešit
jen	jen	k9	jen
experimentálně	experimentálně	k6eAd1	experimentálně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvantování	kvantování	k1gNnSc1	kvantování
není	být	k5eNaImIp3nS	být
exaktní	exaktní	k2eAgFnSc1d1	exaktní
procedura	procedura	k1gFnSc1	procedura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíš	spíš	k9	spíš
umění	umění	k1gNnSc4	umění
najít	najít	k5eAaPmF	najít
ke	k	k7c3	k
klasickému	klasický	k2eAgInSc3d1	klasický
systému	systém	k1gInSc2	systém
jeho	jeho	k3xOp3gInSc4	jeho
protějšek	protějšek	k1gInSc4	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
dost	dost	k6eAd1	dost
systémů	systém	k1gInPc2	systém
nemajících	mající	k2eNgMnPc2d1	nemající
klasickou	klasický	k2eAgFnSc4d1	klasická
analogii	analogie	k1gFnSc4	analogie
<g/>
,	,	kIx,	,
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
spin	spin	k1gInSc1	spin
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
kvantování	kvantování	k1gNnSc2	kvantování
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgNnSc1d1	zásadní
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
interagujících	interagující	k2eAgFnPc2d1	interagující
polí	pole	k1gFnPc2	pole
matematicky	matematicky	k6eAd1	matematicky
rigorózně	rigorózně	k6eAd1	rigorózně
systém	systém	k1gInSc4	systém
definovat	definovat	k5eAaBmF	definovat
a	a	k8xC	a
řešit	řešit	k5eAaImF	řešit
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
určit	určit	k5eAaPmF	určit
Hilbertův	Hilbertův	k2eAgInSc4d1	Hilbertův
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
definiční	definiční	k2eAgInPc4d1	definiční
obory	obor	k1gInPc4	obor
operátorů	operátor	k1gMnPc2	operátor
a	a	k8xC	a
exaktně	exaktně	k6eAd1	exaktně
stanovit	stanovit	k5eAaPmF	stanovit
platnost	platnost	k1gFnSc4	platnost
použitých	použitý	k2eAgFnPc2d1	použitá
metod	metoda	k1gFnPc2	metoda
řešení	řešení	k1gNnSc2	řešení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
standardního	standardní	k2eAgInSc2d1	standardní
postupu	postup	k1gInSc2	postup
užívajícího	užívající	k2eAgInSc2d1	užívající
Hilbertova	Hilbertův	k2eAgInSc2d1	Hilbertův
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
operátorů	operátor	k1gInPc2	operátor
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
populární	populární	k2eAgInSc1d1	populární
postup	postup	k1gInSc1	postup
využívající	využívající	k2eAgFnSc1d1	využívající
dráhového	dráhový	k2eAgInSc2d1	dráhový
integrálu	integrál	k1gInSc2	integrál
<g/>
,	,	kIx,	,
zavedený	zavedený	k2eAgInSc4d1	zavedený
R.	R.	kA	R.
P.	P.	kA	P.
Feynmanem	Feynman	k1gInSc7	Feynman
<g/>
.	.	kIx.	.
</s>
<s>
Opírá	opírat	k5eAaImIp3nS	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
výpočty	výpočet	k1gInPc4	výpočet
integrálů	integrál	k1gInPc2	integrál
přes	přes	k7c4	přes
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgFnPc2	všecek
myslitelných	myslitelný	k2eAgFnPc2d1	myslitelná
trajektorií	trajektorie	k1gFnPc2	trajektorie
ve	v	k7c6	v
fázovém	fázový	k2eAgInSc6d1	fázový
nebo	nebo	k8xC	nebo
konfiguračním	konfigurační	k2eAgInSc6d1	konfigurační
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
(	(	kIx(	(
<g/>
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
)	)	kIx)	)
předpovídat	předpovídat	k5eAaImF	předpovídat
výsledky	výsledek	k1gInPc4	výsledek
měření	měření	k1gNnSc2	měření
na	na	k7c6	na
kvantovém	kvantový	k2eAgInSc6d1	kvantový
systému	systém	k1gInSc6	systém
užívaje	užívat	k5eAaImSgMnS	užívat
klasického	klasický	k2eAgInSc2d1	klasický
hamiltoniánu	hamiltonián	k1gInSc2	hamiltonián
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
jednoduchému	jednoduchý	k2eAgNnSc3d1	jednoduché
zavedení	zavedení	k1gNnSc3	zavedení
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
operuje	operovat	k5eAaImIp3nS	operovat
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
přehlednému	přehledný	k2eAgNnSc3d1	přehledné
odvození	odvození	k1gNnSc3	odvození
poruchových	poruchový	k2eAgFnPc2d1	poruchová
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Feynman	Feynman	k1gMnSc1	Feynman
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
této	tento	k3xDgFnSc2	tento
formulace	formulace	k1gFnSc2	formulace
ho	on	k3xPp3gMnSc4	on
vedla	vést	k5eAaImAgFnS	vést
snaha	snaha	k1gFnSc1	snaha
pochopit	pochopit	k5eAaPmF	pochopit
vztah	vztah	k1gInSc4	vztah
klasického	klasický	k2eAgInSc2d1	klasický
a	a	k8xC	a
kvantového	kvantový	k2eAgInSc2d1	kvantový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
ve	v	k7c6	v
formalismu	formalismus	k1gInSc6	formalismus
dráhového	dráhový	k2eAgInSc2d1	dráhový
integrálu	integrál	k1gInSc2	integrál
zřetelnější	zřetelný	k2eAgMnSc1d2	zřetelnější
(	(	kIx(	(
<g/>
k	k	k7c3	k
hodnotě	hodnota	k1gFnSc3	hodnota
integrálu	integrál	k1gInSc2	integrál
přispívají	přispívat	k5eAaImIp3nP	přispívat
nejvíce	hodně	k6eAd3	hodně
trajektorie	trajektorie	k1gFnPc1	trajektorie
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
minima	minimum	k1gNnSc2	minimum
akce	akce	k1gFnSc2	akce
-	-	kIx~	-
tj.	tj.	kA	tj.
klasické	klasický	k2eAgFnPc4d1	klasická
trajektorie	trajektorie	k1gFnPc4	trajektorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
není	být	k5eNaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
formulace	formulace	k1gFnSc1	formulace
užívána	užívat	k5eAaImNgFnS	užívat
dominantně	dominantně	k6eAd1	dominantně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
prakticky	prakticky	k6eAd1	prakticky
snazší	snadný	k2eAgNnSc4d2	snazší
počítání	počítání	k1gNnSc4	počítání
s	s	k7c7	s
operátory	operátor	k1gMnPc7	operátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
především	především	k9	především
matematická	matematický	k2eAgFnSc1d1	matematická
obtížnost	obtížnost	k1gFnSc1	obtížnost
pojmu	pojem	k1gInSc2	pojem
dráhového	dráhový	k2eAgInSc2d1	dráhový
integrálu	integrál	k1gInSc2	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Dráhový	dráhový	k2eAgInSc4d1	dráhový
integrál	integrál	k1gInSc4	integrál
nelze	lze	k6eNd1	lze
definovat	definovat	k5eAaBmF	definovat
přímo	přímo	k6eAd1	přímo
dle	dle	k7c2	dle
teorie	teorie	k1gFnSc2	teorie
míry	míra	k1gFnSc2	míra
a	a	k8xC	a
integrálu	integrál	k1gInSc2	integrál
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Wienerova	Wienerův	k2eAgFnSc1d1	Wienerova
míra	míra	k1gFnSc1	míra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
přiblížení	přiblížení	k1gNnSc1	přiblížení
pomocí	pomocí	k7c2	pomocí
limitní	limitní	k2eAgFnSc2d1	limitní
diskretizace	diskretizace	k1gFnSc2	diskretizace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ožívají	ožívat	k5eAaImIp3nP	ožívat
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
řazení	řazení	k1gNnSc2	řazení
operátorů	operátor	k1gInPc2	operátor
v	v	k7c6	v
součinech	součin	k1gInPc6	součin
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
problémy	problém	k1gInPc1	problém
(	(	kIx(	(
<g/>
limita	limita	k1gFnSc1	limita
přísně	přísně	k6eAd1	přísně
vzato	vzat	k2eAgNnSc1d1	vzato
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
paradoxně	paradoxně	k6eAd1	paradoxně
tato	tento	k3xDgFnSc1	tento
formulace	formulace	k1gFnSc1	formulace
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
plodným	plodný	k2eAgInPc3d1	plodný
výsledkům	výsledek	k1gInPc3	výsledek
v	v	k7c6	v
částicové	částicový	k2eAgFnSc6d1	částicová
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Nemožnost	nemožnost	k1gFnSc1	nemožnost
pozorovat	pozorovat	k5eAaImF	pozorovat
mikrosvět	mikrosvět	k1gInSc4	mikrosvět
atomů	atom	k1gInPc2	atom
přímo	přímo	k6eAd1	přímo
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
subjektivní	subjektivní	k2eAgFnPc4d1	subjektivní
interpretace	interpretace	k1gFnPc4	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
kolapsu	kolaps	k1gInSc2	kolaps
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
vedla	vést	k5eAaImAgFnS	vést
již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
odmítání	odmítání	k1gNnSc3	odmítání
či	či	k8xC	či
minimálně	minimálně	k6eAd1	minimálně
kritickému	kritický	k2eAgInSc3d1	kritický
postoji	postoj	k1gInSc3	postoj
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
význačných	význačný	k2eAgMnPc2d1	význačný
vědců	vědec	k1gMnPc2	vědec
(	(	kIx(	(
<g/>
jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
za	za	k7c4	za
všechny	všechen	k3xTgInPc4	všechen
E.	E.	kA	E.
Schrödingera	Schrödingero	k1gNnSc2	Schrödingero
a	a	k8xC	a
A.	A.	kA	A.
Einsteina	Einstein	k1gMnSc2	Einstein
-	-	kIx~	-
paradoxně	paradoxně	k6eAd1	paradoxně
dva	dva	k4xCgMnPc4	dva
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
zásadní	zásadní	k2eAgFnSc7d1	zásadní
měrou	míra	k1gFnSc7wR	míra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgFnPc2d1	různá
příčin	příčina	k1gFnPc2	příčina
<g/>
:	:	kIx,	:
Pojem	pojem	k1gInSc1	pojem
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
principiálního	principiální	k2eAgNnSc2d1	principiální
hlediska	hledisko	k1gNnSc2	hledisko
definován	definovat	k5eAaBmNgInS	definovat
vágně	vágně	k6eAd1	vágně
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kolaps	kolaps	k1gInSc1	kolaps
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
ale	ale	k9	ale
o	o	k7c4	o
systém	systém	k1gInSc4	systém
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
prostorovým	prostorový	k2eAgInSc7d1	prostorový
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
,	,	kIx,	,
měření	měřený	k2eAgMnPc1d1	měřený
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgNnPc6d1	vzdálené
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
tak	tak	k9	tak
okamžitě	okamžitě	k6eAd1	okamžitě
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
v	v	k7c6	v
principu	princip	k1gInSc6	princip
celý	celý	k2eAgInSc4d1	celý
vesmír	vesmír	k1gInSc4	vesmír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
v	v	k7c6	v
příkrém	příkrý	k2eAgInSc6d1	příkrý
protikladu	protiklad	k1gInSc6	protiklad
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
tvrzením	tvrzení	k1gNnSc7	tvrzení
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
o	o	k7c6	o
nemožnosti	nemožnost	k1gFnSc6	nemožnost
nadsvětelné	nadsvětelný	k2eAgFnSc3d1	nadsvětelná
rychlosti	rychlost	k1gFnSc3	rychlost
šíření	šíření	k1gNnSc2	šíření
signálů	signál	k1gInPc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
nastává	nastávat	k5eAaImIp3nS	nastávat
podle	podle	k7c2	podle
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
obvykle	obvykle	k6eAd1	obvykle
nejdou	jít	k5eNaImIp3nP	jít
příliš	příliš	k6eAd1	příliš
dál	daleko	k6eAd2	daleko
ve	v	k7c6	v
výkladu	výklad	k1gInSc6	výklad
o	o	k7c6	o
bližším	blízký	k2eAgNnSc6d2	bližší
určení	určení	k1gNnSc6	určení
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
mikroskopický	mikroskopický	k2eAgInSc1d1	mikroskopický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
provádíme	provádět	k5eAaImIp1nP	provádět
měření	měření	k1gNnPc1	měření
pomocí	pomocí	k7c2	pomocí
makroskopické	makroskopický	k2eAgFnSc2d1	makroskopická
aparatury	aparatura	k1gFnSc2	aparatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praktickém	praktický	k2eAgInSc6d1	praktický
případě	případ	k1gInSc6	případ
nedělá	dělat	k5eNaImIp3nS	dělat
takové	takový	k3xDgNnSc1	takový
rozlišení	rozlišení	k1gNnSc1	rozlišení
žádné	žádný	k3yNgFnSc2	žádný
potíže	potíž	k1gFnSc2	potíž
-	-	kIx~	-
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tvořen	tvořit	k5eAaImNgInS	tvořit
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
atomy	atom	k1gInPc7	atom
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
aparatura	aparatura	k1gFnSc1	aparatura
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
rozměrů	rozměr	k1gInPc2	rozměr
továrny	továrna	k1gFnSc2	továrna
(	(	kIx(	(
<g/>
nejextrémnějším	extrémní	k2eAgInSc7d3	nejextrémnější
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
obří	obří	k2eAgInPc1d1	obří
urychlovače	urychlovač	k1gInPc1	urychlovač
sloužící	sloužící	k2eAgInPc1d1	sloužící
často	často	k6eAd1	často
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
struktury	struktura	k1gFnSc2	struktura
protonů	proton	k1gInPc2	proton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
ale	ale	k9	ale
můžeme	moct	k5eAaImIp1nP	moct
libovolně	libovolně	k6eAd1	libovolně
velkou	velký	k2eAgFnSc4d1	velká
aparaturu	aparatura	k1gFnSc4	aparatura
zahrnout	zahrnout	k5eAaPmF	zahrnout
do	do	k7c2	do
zkoumaného	zkoumaný	k2eAgInSc2d1	zkoumaný
systému	systém	k1gInSc2	systém
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pohledu	pohled	k1gInSc6	pohled
se	se	k3xPyFc4	se
kolaps	kolaps	k1gInSc1	kolaps
odsouvá	odsouvat	k5eAaImIp3nS	odsouvat
až	až	k9	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výzkumník	výzkumník	k1gMnSc1	výzkumník
začne	začít	k5eAaPmIp3nS	začít
pozorovat	pozorovat	k5eAaImF	pozorovat
aparaturu	aparatura	k1gFnSc4	aparatura
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
ale	ale	k9	ale
do	do	k7c2	do
systému	systém	k1gInSc2	systém
zahrnout	zahrnout	k5eAaPmF	zahrnout
i	i	k9	i
mozek	mozek	k1gInSc1	mozek
výzkumníka	výzkumník	k1gMnSc2	výzkumník
atd.	atd.	kA	atd.
Tudíž	tudíž	k8xC	tudíž
už	už	k6eAd1	už
samo	sám	k3xTgNnSc4	sám
určení	určení	k1gNnSc4	určení
okamžiku	okamžik	k1gInSc2	okamžik
kolapsu	kolaps	k1gInSc2	kolaps
je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
příklad	příklad	k1gInSc4	příklad
Schrödingerovy	Schrödingerův	k2eAgFnSc2d1	Schrödingerova
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
existují	existovat	k5eAaImIp3nP	existovat
tyto	tento	k3xDgFnPc4	tento
zásadní	zásadní	k2eAgFnPc4d1	zásadní
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
věc	věc	k1gFnSc4	věc
řešit	řešit	k5eAaImF	řešit
<g/>
:	:	kIx,	:
Problém	problém	k1gInSc4	problém
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
dává	dávat	k5eAaImIp3nS	dávat
dobré	dobrý	k2eAgFnPc4d1	dobrá
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
o	o	k7c6	o
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
výsledcích	výsledek	k1gInPc6	výsledek
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
dívat	dívat	k5eAaImF	dívat
jako	jako	k9	jako
na	na	k7c4	na
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
předpovídání	předpovídání	k1gNnSc4	předpovídání
výsledků	výsledek	k1gInPc2	výsledek
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
na	na	k7c4	na
kolaps	kolaps	k1gInSc4	kolaps
jako	jako	k8xS	jako
na	na	k7c4	na
pomocnou	pomocný	k2eAgFnSc4d1	pomocná
myšlenkovou	myšlenkový	k2eAgFnSc4d1	myšlenková
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nám	my	k3xPp1nPc3	my
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
o	o	k7c4	o
teorii	teorie	k1gFnSc4	teorie
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
kolaps	kolaps	k1gInSc1	kolaps
skutečný	skutečný	k2eAgInSc1d1	skutečný
(	(	kIx(	(
<g/>
či	či	k8xC	či
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
skutečná	skutečný	k2eAgFnSc1d1	skutečná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
kolapsu	kolaps	k1gInSc3	kolaps
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
ne	ne	k9	ne
do	do	k7c2	do
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
originále	originál	k1gInSc6	originál
tzv.	tzv.	kA	tzv.
shut-up-and-calculate	shutpndalculat	k1gInSc5	shut-up-and-calculat
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Problém	problém	k1gInSc1	problém
kolapsu	kolaps	k1gInSc2	kolaps
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepozorované	pozorovaný	k2eNgInPc1d1	nepozorovaný
stavy	stav	k1gInPc1	stav
přestaneme	přestat	k5eAaPmIp1nP	přestat
uvažovat	uvažovat	k5eAaImF	uvažovat
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
jim	on	k3xPp3gMnPc3	on
nepřisuzujeme	přisuzovat	k5eNaImIp1nP	přisuzovat
žádnou	žádný	k3yNgFnSc4	žádný
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kolapsu	kolaps	k1gInSc3	kolaps
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
superpozice	superpozice	k1gFnSc2	superpozice
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgInPc1d1	velký
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
kolaps	kolaps	k1gInSc1	kolaps
je	být	k5eAaImIp3nS	být
objektivní	objektivní	k2eAgFnSc4d1	objektivní
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
teorii	teorie	k1gFnSc4	teorie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kodaňská	kodaňský	k2eAgFnSc1d1	Kodaňská
interpretace	interpretace	k1gFnSc1	interpretace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zastáván	zastávat	k5eAaImNgMnS	zastávat
většinou	většina	k1gFnSc7	většina
fyziků	fyzik	k1gMnPc2	fyzik
dneška	dnešek	k1gInSc2	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
kolapsu	kolaps	k1gInSc3	kolaps
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
provázáním	provázání	k1gNnSc7	provázání
stavu	stav	k1gInSc2	stav
systému	systém	k1gInSc2	systém
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
vědomí	vědomí	k1gNnSc2	vědomí
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgFnSc1d1	globální
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
celého	celý	k2eAgInSc2d1	celý
vesmíru	vesmír	k1gInSc2	vesmír
stavem	stav	k1gInSc7	stav
nikdy	nikdy	k6eAd1	nikdy
neprochází	procházet	k5eNaImIp3nS	procházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
superpozici	superpozice	k1gFnSc4	superpozice
mnoha	mnoho	k4c2	mnoho
větví	větev	k1gFnPc2	větev
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
všem	všecek	k3xTgInPc3	všecek
možným	možný	k2eAgInPc3d1	možný
výstupům	výstup	k1gInPc3	výstup
experimentu	experiment	k1gInSc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
mnohosvětová	mnohosvětový	k2eAgFnSc1d1	mnohosvětový
interpretace	interpretace	k1gFnSc1	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
zdání	zdání	k1gNnSc1	zdání
vznikající	vznikající	k2eAgNnSc1d1	vznikající
díky	díky	k7c3	díky
naší	náš	k3xOp1gFnSc3	náš
neschopnosti	neschopnost	k1gFnSc3	neschopnost
zjistit	zjistit	k5eAaPmF	zjistit
všechny	všechen	k3xTgFnPc4	všechen
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
skrytých	skrytý	k2eAgInPc2d1	skrytý
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Podolsky	podolsky	k6eAd1	podolsky
a	a	k8xC	a
N.	N.	kA	N.
Rosen	rosen	k2eAgMnSc1d1	rosen
publikovali	publikovat	k5eAaBmAgMnP	publikovat
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
experiment	experiment	k1gInSc4	experiment
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
hodlali	hodlat	k5eAaImAgMnP	hodlat
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
úplnost	úplnost	k1gFnSc4	úplnost
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
(	(	kIx(	(
<g/>
EPR	EPR	kA	EPR
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
principu	princip	k1gInSc6	princip
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgNnPc2	který
měření	měření	k1gNnPc2	měření
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
možných	možný	k2eAgInPc2d1	možný
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
a	a	k8xC	a
tak	tak	k6eAd1	tak
k	k	k7c3	k
popření	popření	k1gNnSc3	popření
principů	princip	k1gInPc2	princip
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
experimentátor	experimentátor	k1gMnSc1	experimentátor
nemůže	moct	k5eNaImIp3nS	moct
"	"	kIx"	"
<g/>
zařídit	zařídit	k5eAaPmF	zařídit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
výsledek	výsledek	k1gInSc4	výsledek
jeho	on	k3xPp3gNnSc2	on
měření	měření	k1gNnSc2	měření
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
kam	kam	k6eAd1	kam
má	mít	k5eAaImIp3nS	mít
systém	systém	k1gInSc1	systém
zkolabovat	zkolabovat	k5eAaPmF	zkolabovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
informace	informace	k1gFnSc2	informace
nadsvětelnou	nadsvětelna	k1gFnSc7	nadsvětelna
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
pokus	pokus	k1gInSc1	pokus
spíše	spíše	k9	spíše
argumentem	argument	k1gInSc7	argument
filozofickým	filozofický	k2eAgInSc7d1	filozofický
<g/>
,	,	kIx,	,
opírajícím	opírající	k2eAgMnSc7d1	opírající
se	se	k3xPyFc4	se
o	o	k7c6	o
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
reálný	reálný	k2eAgInSc4d1	reálný
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc4	autor
byl	být	k5eAaImAgMnS	být
zamýšlen	zamýšlen	k2eAgMnSc1d1	zamýšlen
jako	jako	k8xC	jako
podpora	podpora	k1gFnSc1	podpora
teorií	teorie	k1gFnPc2	teorie
se	s	k7c7	s
skrytými	skrytý	k2eAgInPc7d1	skrytý
parametry	parametr	k1gInPc7	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvantový	kvantový	k2eAgInSc1d1	kvantový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
klasický	klasický	k2eAgInSc1d1	klasický
<g/>
,	,	kIx,	,
určen	určen	k2eAgInSc1d1	určen
jednoznačně	jednoznačně	k6eAd1	jednoznačně
hodnotami	hodnota	k1gFnPc7	hodnota
sady	sada	k1gFnSc2	sada
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
skryté	skrytý	k2eAgInPc1d1	skrytý
parametry	parametr	k1gInPc1	parametr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
neklasického	klasický	k2eNgNnSc2d1	neklasické
chování	chování	k1gNnSc2	chování
kvantových	kvantový	k2eAgInPc2d1	kvantový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
měření	měření	k1gNnSc2	měření
systému	systém	k1gInSc2	systém
určen	určit	k5eAaPmNgInS	určit
předem	předem	k6eAd1	předem
a	a	k8xC	a
výběr	výběr	k1gInSc1	výběr
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
systém	systém	k1gInSc1	systém
zkolabuje	zkolabovat	k5eAaPmIp3nS	zkolabovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
právě	právě	k9	právě
skrytými	skrytý	k2eAgInPc7d1	skrytý
parametry	parametr	k1gInPc7	parametr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
publikování	publikování	k1gNnSc2	publikování
EPR	EPR	kA	EPR
článku	článek	k1gInSc2	článek
se	se	k3xPyFc4	se
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
skryté	skrytý	k2eAgInPc1d1	skrytý
parametry	parametr	k1gInPc1	parametr
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
test	test	k1gInSc4	test
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
John	John	k1gMnSc1	John
Bell	bell	k1gInSc4	bell
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
Bellových	Bellův	k2eAgFnPc2d1	Bellova
nerovností	nerovnost	k1gFnPc2	nerovnost
byla	být	k5eAaImAgFnS	být
existence	existence	k1gFnSc1	existence
skrytých	skrytý	k2eAgInPc2d1	skrytý
parametrů	parametr	k1gInPc2	parametr
experimentálně	experimentálně	k6eAd1	experimentálně
vyvracena	vyvracen	k2eAgFnSc1d1	vyvracena
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
však	však	k9	však
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
kvantové	kvantový	k2eAgFnPc4d1	kvantová
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
neslučitelnost	neslučitelnost	k1gFnSc1	neslučitelnost
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
není	být	k5eNaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
paradoxu	paradox	k1gInSc2	paradox
s	s	k7c7	s
kolapsem	kolaps	k1gInSc7	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
při	při	k7c6	při
slučování	slučování	k1gNnSc6	slučování
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
technického	technický	k2eAgInSc2d1	technický
rázu	ráz	k1gInSc2	ráz
a	a	k8xC	a
týkají	týkat	k5eAaImIp3nP	týkat
se	se	k3xPyFc4	se
nerenormalizovatelnosti	nerenormalizovatelnost	k1gFnPc1	nerenormalizovatelnost
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
teorie	teorie	k1gFnSc2	teorie
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jisté	jistý	k2eAgFnPc1d1	jistá
naděje	naděje	k1gFnPc1	naděje
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
přechodu	přechod	k1gInSc2	přechod
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
strun	struna	k1gFnPc2	struna
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
základního	základní	k2eAgInSc2d1	základní
postulátu	postulát	k1gInSc2	postulát
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
kolapsu	kolaps	k1gInSc6	kolaps
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
netýká	týkat	k5eNaImIp3nS	týkat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
hraje	hrát	k5eAaImIp3nS	hrát
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
fyzice	fyzika	k1gFnSc3	fyzika
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
atomové	atomový	k2eAgFnSc6d1	atomová
a	a	k8xC	a
molekulové	molekulový	k2eAgFnSc6d1	molekulová
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
i	i	k9	i
v	v	k7c6	v
astrofyzice	astrofyzika	k1gFnSc6	astrofyzika
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
oborech	obor	k1gInPc6	obor
včetně	včetně	k7c2	včetně
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
by	by	kYmCp3nP	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebyly	být	k5eNaImAgInP	být
zkonstruovány	zkonstruován	k2eAgInPc1d1	zkonstruován
polovodiče	polovodič	k1gInPc1	polovodič
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
neexistovala	existovat	k5eNaImAgFnS	existovat
by	by	kYmCp3nS	by
jaderná	jaderný	k2eAgFnSc1d1	jaderná
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xC	jako
uhlíková	uhlíkový	k2eAgNnPc1d1	uhlíkové
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
lasery	laser	k1gInPc1	laser
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
by	by	kYmCp3nP	by
také	také	k9	také
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc6d2	nižší
úrovni	úroveň	k1gFnSc6	úroveň
mnohé	mnohý	k2eAgInPc4d1	mnohý
diagnostické	diagnostický	k2eAgInPc4d1	diagnostický
a	a	k8xC	a
léčebné	léčebný	k2eAgFnPc4d1	léčebná
metody	metoda	k1gFnPc4	metoda
využívající	využívající	k2eAgFnPc4d1	využívající
radiofarmak	radiofarmak	k6eAd1	radiofarmak
a	a	k8xC	a
zářičů	zářič	k1gInPc2	zářič
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc2	teorie
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
smýšlení	smýšlení	k1gNnSc4	smýšlení
lidí	člověk	k1gMnPc2	člověk
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnostní	pravděpodobnostní	k2eAgInSc1d1	pravděpodobnostní
charakter	charakter	k1gInSc1	charakter
jejích	její	k3xOp3gFnPc2	její
předpovědí	předpověď	k1gFnPc2	předpověď
zasadil	zasadit	k5eAaPmAgInS	zasadit
silnou	silný	k2eAgFnSc4d1	silná
ránu	rána	k1gFnSc4	rána
mechanistickému	mechanistický	k2eAgInSc3d1	mechanistický
determinismu	determinismus	k1gInSc3	determinismus
<g/>
,	,	kIx,	,
silnému	silný	k2eAgInSc3d1	silný
ve	v	k7c6	v
filozofii	filozofie	k1gFnSc6	filozofie
osmnáctého	osmnáctý	k4xOgMnSc2	osmnáctý
a	a	k8xC	a
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
