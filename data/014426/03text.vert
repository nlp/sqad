<s>
Okresy	okres	k1gInPc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Mapa	mapa	k1gFnSc1
současných	současný	k2eAgInPc2d1
slovenských	slovenský	k2eAgInPc2d1
okresů	okres	k1gInPc2
</s>
<s>
Okresy	okres	k1gInPc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
jsou	být	k5eAaImIp3nP
administrativní	administrativní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresy	okres	k1gInPc7
nemají	mít	k5eNaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
krajů	kraj	k1gInPc2
správní	správní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnPc1
území	území	k1gNnPc1
existují	existovat	k5eAaImIp3nP
už	už	k6eAd1
jen	jen	k9
jako	jako	k9
statistické	statistický	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
79	#num#	k4
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Bratislava	Bratislava	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělené	rozdělená	k1gFnPc4
na	na	k7c4
pět	pět	k4xCc4
okresů	okres	k1gInPc2
a	a	k8xC
město	město	k1gNnSc1
Košice	Košice	k1gInPc1
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
okresy	okres	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Okresy	okres	k1gInPc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
</s>
<s>
Okresy	okres	k1gInPc1
jako	jako	k8xC,k8xS
správní	správní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
Slovenska	Slovensko	k1gNnSc2
vyskytují	vyskytovat	k5eAaImIp3nP
už	už	k9
od	od	k7c2
dob	doba	k1gFnPc2
Uherska	Uhersko	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc2
vyšší	vysoký	k2eAgFnSc2d2
správní	správní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
župy	župa	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
dále	daleko	k6eAd2
členěny	členěn	k2eAgInPc1d1
právě	právě	k9
na	na	k7c4
okresy	okres	k1gInPc4
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
járás	járás	k6eAd1
<g/>
(	(	kIx(
<g/>
ok	oka	k1gFnPc2
<g/>
))	))	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
rozčlenění	rozčlenění	k1gNnSc1
zhruba	zhruba	k6eAd1
zachováno	zachovat	k5eAaPmNgNnS
(	(	kIx(
<g/>
89	#num#	k4
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
91	#num#	k4
okresů	okres	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
vyšší	vysoký	k2eAgInPc1d2
celky	celek	k1gInPc1
(	(	kIx(
<g/>
župy	župa	k1gFnPc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
svůj	svůj	k3xOyFgInSc4
rozsah	rozsah	k1gInSc4
výrazně	výrazně	k6eAd1
měnily	měnit	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Podstatná	podstatný	k2eAgFnSc1d1
změna	změna	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
teprve	teprve	k6eAd1
se	s	k7c7
správní	správní	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
okresů	okres	k1gInPc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
radikálně	radikálně	k6eAd1
snížen	snížit	k5eAaPmNgInS
na	na	k7c4
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
získala	získat	k5eAaPmAgFnS
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
dosavadní	dosavadní	k2eAgInSc1d1
okres	okres	k1gInSc1
Bratislava-mesto	Bratislava-mesta	k1gMnSc5
<g/>
)	)	kIx)
zvláštní	zvláštní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g/>
,	,	kIx,
nadále	nadále	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
nejednalo	jednat	k5eNaImAgNnS
o	o	k7c4
okres	okres	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
územní	územní	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
obdobnou	obdobný	k2eAgFnSc7d1
okresu	okres	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
úrovni	úroveň	k1gFnSc6
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
vznikly	vzniknout	k5eAaPmAgInP
čtyři	čtyři	k4xCgInPc1
nové	nový	k2eAgInPc1d1
okresy	okres	k1gInPc1
(	(	kIx(
<g/>
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
<g/>
,	,	kIx,
Svidník	Svidník	k1gInSc1
<g/>
,	,	kIx,
Veľký	Veľký	k2eAgMnSc1d1
Krtíš	Krtíš	k1gMnSc1
a	a	k8xC
Vranov	Vranov	k1gInSc1
<g/>
)	)	kIx)
vyčleněním	vyčlenění	k1gNnSc7
z	z	k7c2
území	území	k1gNnSc2
stávajících	stávající	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1968	#num#	k4
byl	být	k5eAaImAgInS
dosavadní	dosavadní	k2eAgInSc1d1
okres	okres	k1gInSc1
Košice	Košice	k1gInPc1
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
okresy	okres	k1gInPc4
Košice-mesto	Košice-mesta	k1gMnSc5
a	a	k8xC
Košice-vidiek	Košice-vidiko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
1969	#num#	k4
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
název	název	k1gInSc1
okresu	okres	k1gInSc2
Vranov	Vranov	k1gInSc4
podle	podle	k7c2
celého	celý	k2eAgInSc2d1
názvu	název	k1gInSc2
okresního	okresní	k2eAgNnSc2d1
města	město	k1gNnSc2
na	na	k7c4
Vranov	Vranov	k1gInSc4
nad	nad	k7c7
Topľou	Topľa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Těchto	tento	k3xDgInPc2
37	#num#	k4
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
a	a	k8xC
Bratislava	Bratislava	k1gFnSc1
jako	jako	k8xC,k8xS
samostatná	samostatný	k2eAgFnSc1d1
územní	územní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
existovalo	existovat	k5eAaImAgNnS
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Okresy	okres	k1gInPc1
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
(	(	kIx(
<g/>
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
RZ	RZ	kA
<g/>
)	)	kIx)
</s>
<s>
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
(	(	kIx(
<g/>
BB	BB	kA
<g/>
,	,	kIx,
BC	BC	kA
<g/>
)	)	kIx)
</s>
<s>
Bardejov	Bardejov	k1gInSc1
(	(	kIx(
<g/>
BJ	BJ	kA
<g/>
)	)	kIx)
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
BA	ba	k9
<g/>
,	,	kIx,
BL	BL	kA
<g/>
,	,	kIx,
BT	BT	kA
<g/>
)	)	kIx)
</s>
<s>
Bratislava-vidiek	Bratislava-vidiek	k1gMnSc1
(	(	kIx(
<g/>
BH	BH	kA
<g/>
,	,	kIx,
BY	by	kYmCp3nS
<g/>
)	)	kIx)
</s>
<s>
Čadca	Čadca	k1gFnSc1
(	(	kIx(
<g/>
CA	ca	kA
<g/>
)	)	kIx)
</s>
<s>
Dolný	Dolný	k1gMnSc1
Kubín	Kubín	k1gMnSc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
</s>
<s>
Dunajská	dunajský	k2eAgFnSc1d1
Streda	Streda	k1gMnSc1
(	(	kIx(
<g/>
DS	DS	kA
<g/>
)	)	kIx)
</s>
<s>
Galanta	Galanta	k1gFnSc1
(	(	kIx(
<g/>
GA	GA	kA
<g/>
)	)	kIx)
</s>
<s>
Humenné	Humenné	k1gNnSc1
(	(	kIx(
<g/>
HN	HN	kA
<g/>
)	)	kIx)
</s>
<s>
Komárno	Komárno	k1gNnSc1
(	(	kIx(
<g/>
KN	KN	kA
<g/>
)	)	kIx)
</s>
<s>
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
KE	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Košice-okolie	Košice-okolie	k1gFnSc1
(	(	kIx(
<g/>
KS	ks	kA
<g/>
)	)	kIx)
</s>
<s>
Levice	levice	k1gFnSc1
(	(	kIx(
<g/>
LV	LV	kA
<g/>
)	)	kIx)
</s>
<s>
Liptovský	liptovský	k2eAgMnSc1d1
Mikuláš	Mikuláš	k1gMnSc1
(	(	kIx(
<g/>
LM	LM	kA
<g/>
)	)	kIx)
</s>
<s>
Lučenec	Lučenec	k1gInSc1
(	(	kIx(
<g/>
LC	LC	kA
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
(	(	kIx(
<g/>
MT	MT	kA
<g/>
)	)	kIx)
</s>
<s>
Michalovce	Michalovce	k1gInPc1
(	(	kIx(
<g/>
MI	já	k3xPp1nSc3
<g/>
)	)	kIx)
</s>
<s>
Nitra	Nitra	k1gFnSc1
(	(	kIx(
<g/>
NR	NR	kA
<g/>
)	)	kIx)
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
(	(	kIx(
<g/>
NZ	NZ	kA
<g/>
)	)	kIx)
</s>
<s>
Poprad	Poprad	k1gInSc1
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
</s>
<s>
Považská	Považský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
(	(	kIx(
<g/>
PX	PX	kA
<g/>
)	)	kIx)
</s>
<s>
Prešov	Prešov	k1gInSc1
(	(	kIx(
<g/>
PO	Po	kA
<g/>
)	)	kIx)
</s>
<s>
Prievidza	Prievidza	k1gFnSc1
(	(	kIx(
<g/>
PD	PD	kA
<g/>
)	)	kIx)
</s>
<s>
Rimavská	rimavský	k2eAgFnSc1d1
Sobota	sobota	k1gFnSc1
(	(	kIx(
<g/>
RS	RS	kA
<g/>
)	)	kIx)
</s>
<s>
Rožňava	Rožňava	k1gFnSc1
(	(	kIx(
<g/>
RV	RV	kA
<g/>
)	)	kIx)
</s>
<s>
Senica	Senica	k1gFnSc1
(	(	kIx(
<g/>
SE	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
Spišská	spišský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
(	(	kIx(
<g/>
SN	SN	kA
<g/>
)	)	kIx)
</s>
<s>
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
(	(	kIx(
<g/>
SL	SL	kA
<g/>
)	)	kIx)
</s>
<s>
Svidník	Svidník	k1gInSc1
(	(	kIx(
<g/>
SK	Sk	kA
<g/>
)	)	kIx)
</s>
<s>
Topoľčany	Topoľčan	k1gMnPc4
(	(	kIx(
<g/>
TO	to	k9
<g/>
)	)	kIx)
</s>
<s>
Trebišov	Trebišov	k1gInSc1
(	(	kIx(
<g/>
TV	TV	kA
<g/>
)	)	kIx)
</s>
<s>
Trenčín	Trenčín	k1gInSc1
(	(	kIx(
<g/>
TN	TN	kA
<g/>
)	)	kIx)
</s>
<s>
Trnava	Trnava	k1gFnSc1
(	(	kIx(
<g/>
TT	TT	kA
<g/>
)	)	kIx)
</s>
<s>
Veľký	Veľký	k2eAgMnSc1d1
Krtíš	Krtíš	k1gMnSc1
(	(	kIx(
<g/>
VK	VK	kA
<g/>
)	)	kIx)
</s>
<s>
Vranov	Vranov	k1gInSc1
nad	nad	k7c7
Topľou	Topľa	k1gMnSc7
(	(	kIx(
<g/>
VV	VV	kA
<g/>
)	)	kIx)
</s>
<s>
Zvolen	Zvolen	k1gInSc1
(	(	kIx(
<g/>
ZV	ZV	kA
<g/>
)	)	kIx)
</s>
<s>
Žiar	Žiar	k1gInSc1
nad	nad	k7c4
Hronom	Hronom	k1gInSc4
(	(	kIx(
<g/>
ZH	ZH	kA
<g/>
)	)	kIx)
</s>
<s>
Žilina	Žilina	k1gFnSc1
(	(	kIx(
<g/>
ZA	za	k7c7
<g/>
,	,	kIx,
ZI	ZI	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Slovenska	Slovensko	k1gNnSc2
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c6
dnešních	dnešní	k2eAgInPc6d1
79	#num#	k4
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
více	hodně	k6eAd2
než	než	k8xS
na	na	k7c4
dvojnásobek	dvojnásobek	k1gInSc4
předešlého	předešlý	k2eAgInSc2d1
počtu	počet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
množství	množství	k1gNnSc1
se	se	k3xPyFc4
blížilo	blížit	k5eAaImAgNnS
stavu	stav	k1gInSc3
před	před	k7c7
rokem	rok	k1gInSc7
1960	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
nové	nový	k2eAgNnSc1d1
rozčlenění	rozčlenění	k1gNnSc1
bylo	být	k5eAaImAgNnS
méně	málo	k6eAd2
rovnoměrné	rovnoměrný	k2eAgNnSc1d1
–	–	k?
zatímco	zatímco	k8xS
většina	většina	k1gFnSc1
dosavadních	dosavadní	k2eAgInPc2d1
okresů	okres	k1gInPc2
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
až	až	k9
tři	tři	k4xCgInPc4
menší	malý	k2eAgInPc4d2
<g/>
,	,	kIx,
okresů	okres	k1gInPc2
podél	podél	k7c2
jižní	jižní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
(	(	kIx(
<g/>
se	s	k7c7
silnou	silný	k2eAgFnSc7d1
maďarskou	maďarský	k2eAgFnSc7d1
menšinou	menšina	k1gFnSc7
až	až	k8xS
většinou	většina	k1gFnSc7
<g/>
)	)	kIx)
se	se	k3xPyFc4
změna	změna	k1gFnSc1
téměř	téměř	k6eAd1
nedotkla	dotknout	k5eNaPmAgFnS
a	a	k8xC
zůstaly	zůstat	k5eAaPmAgInP
v	v	k7c6
původním	původní	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
když	když	k8xS
měly	mít	k5eAaImAgFnP
nápadně	nápadně	k6eAd1
nekompaktní	kompaktní	k2eNgInSc4d1
tvar	tvar	k1gInSc4
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
nebo	nebo	k8xC
Trebišov	Trebišov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
okresy	okres	k1gInPc1
tak	tak	k6eAd1
byly	být	k5eAaImAgInP
mnohonásobně	mnohonásobně	k6eAd1
větší	veliký	k2eAgInPc1d2
a	a	k8xC
lidnatější	lidnatý	k2eAgInPc1d2
než	než	k8xS
okresy	okres	k1gInPc1
jiné	jiný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
byl	být	k5eAaImAgInS
každý	každý	k3xTgInSc1
tento	tento	k3xDgInSc1
okres	okres	k1gInSc1
spravovaný	spravovaný	k2eAgInSc1d1
okresním	okresní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vykonával	vykonávat	k5eAaImAgInS
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
úřady	úřad	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
zrušeny	zrušit	k5eAaPmNgInP
a	a	k8xC
počínaje	počínaje	k7c7
1	#num#	k4
<g/>
.	.	kIx.
lednem	leden	k1gInSc7
2004	#num#	k4
nahrazeny	nahradit	k5eAaPmNgFnP
tzv.	tzv.	kA
obvodními	obvodní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
je	být	k5eAaImIp3nS
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
úřady	úřad	k1gInPc1
jsou	být	k5eAaImIp3nP
místními	místní	k2eAgInPc7d1
orgány	orgán	k1gInPc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
oblast	oblast	k1gFnSc4
všeobecné	všeobecný	k2eAgFnSc2d1
vnitřní	vnitřní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
živností	živnost	k1gFnPc2
<g/>
,	,	kIx,
civilní	civilní	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
a	a	k8xC
řízení	řízení	k1gNnSc2
státu	stát	k1gInSc2
v	v	k7c6
krizových	krizový	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
kromě	kromě	k7c2
období	období	k1gNnSc2
válečného	válečný	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
obvodních	obvodní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
než	než	k8xS
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pro	pro	k7c4
některé	některý	k3yIgInPc4
okresy	okres	k1gInPc4
jeden	jeden	k4xCgMnSc1
společný	společný	k2eAgInSc1d1
obvodní	obvodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
skupinu	skupina	k1gFnSc4
sousedících	sousedící	k2eAgInPc2d1
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
rozsah	rozsah	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
zákonem	zákon	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
naopak	naopak	k6eAd1
v	v	k7c6
okrese	okres	k1gInSc6
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
jsou	být	k5eAaImIp3nP
obvodní	obvodní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
jak	jak	k8xC,k8xS
ve	v	k7c6
městě	město	k1gNnSc6
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
ve	v	k7c6
městě	město	k1gNnSc6
Štúrovo	Štúrův	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozložení	rozložení	k1gNnSc1
obvodních	obvodní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
je	být	k5eAaImIp3nS
rovnoměrnější	rovnoměrný	k2eAgFnSc1d2
než	než	k8xS
dosavadních	dosavadní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
okresních	okresní	k2eAgInPc2d1
a	a	k8xC
je	být	k5eAaImIp3nS
určitým	určitý	k2eAgInSc7d1
kompromisem	kompromis	k1gInSc7
mezi	mezi	k7c7
okresy	okres	k1gInPc7
starými	starý	k2eAgInPc7d1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
novými	nový	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
současných	současný	k2eAgInPc2d1
okresů	okres	k1gInPc2
</s>
<s>
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
Banská	banský	k2eAgFnSc1d1
Štiavnica	Štiavnica	k1gFnSc1
</s>
<s>
Bardejov	Bardejov	k1gInSc1
</s>
<s>
Bánovce	Bánovec	k1gInPc1
nad	nad	k7c7
Bebravou	Bebrava	k1gFnSc7
</s>
<s>
Brezno	Brezna	k1gFnSc5
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
I	i	k9
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
II	II	kA
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
III	III	kA
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
IV	Iva	k1gFnPc2
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
V	v	k7c6
</s>
<s>
Bytča	Bytča	k6eAd1
</s>
<s>
Čadca	Čadca	k1gFnSc1
</s>
<s>
Detva	Detva	k1gFnSc1
</s>
<s>
Dolný	Dolný	k1gMnSc1
Kubín	Kubín	k1gMnSc1
</s>
<s>
Dunajská	dunajský	k2eAgFnSc1d1
Streda	Stred	k1gMnSc2
</s>
<s>
Galanta	Galanta	k1gFnSc1
</s>
<s>
Gelnica	Gelnica	k6eAd1
</s>
<s>
Hlohovec	Hlohovec	k1gInSc1
</s>
<s>
Humenné	Humenné	k1gNnSc1
</s>
<s>
Ilava	Ilava	k6eAd1
</s>
<s>
Kežmarok	Kežmarok	k1gInSc1
</s>
<s>
Komárno	Komárno	k1gNnSc1
</s>
<s>
Košice	Košice	k1gInPc1
I	i	k9
</s>
<s>
Košice	Košice	k1gInPc1
II	II	kA
</s>
<s>
Košice	Košice	k1gInPc1
III	III	kA
</s>
<s>
Košice	Košice	k1gInPc1
IV	Iva	k1gFnPc2
</s>
<s>
Košice-okolie	Košice-okolie	k1gFnSc1
</s>
<s>
Krupina	Krupina	k1gFnSc1
</s>
<s>
Kysucké	Kysucký	k2eAgNnSc1d1
Nové	Nové	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
</s>
<s>
Levice	levice	k1gFnSc1
</s>
<s>
Levoča	Levoča	k1gFnSc1
</s>
<s>
Liptovský	liptovský	k2eAgMnSc1d1
Mikuláš	Mikuláš	k1gMnSc1
</s>
<s>
Lučenec	Lučenec	k1gInSc1
</s>
<s>
Malacky	malacky	k6eAd1
</s>
<s>
Martin	Martin	k1gMnSc1
</s>
<s>
Medzilaborce	Medzilaborka	k1gFnSc3
</s>
<s>
Michalovce	Michalovce	k1gInPc1
</s>
<s>
Myjava	Myjava	k1gFnSc1
</s>
<s>
Námestovo	Námestův	k2eAgNnSc1d1
</s>
<s>
Nitra	Nitra	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
nad	nad	k7c4
Váhom	Váhom	k1gInSc4
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
</s>
<s>
Partizánske	Partizánske	k6eAd1
</s>
<s>
Pezinok	Pezinok	k1gInSc1
</s>
<s>
Piešťany	Piešťany	k1gInPc1
</s>
<s>
Poltár	Poltár	k1gMnSc1
</s>
<s>
Poprad	Poprad	k1gInSc1
</s>
<s>
Považská	Považský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
Prešov	Prešov	k1gInSc1
</s>
<s>
Prievidza	Prievidza	k1gFnSc1
</s>
<s>
Púchov	Púchov	k1gInSc1
</s>
<s>
Revúca	Revúca	k6eAd1
</s>
<s>
Rimavská	rimavský	k2eAgFnSc1d1
Sobota	sobota	k1gFnSc1
</s>
<s>
Rožňava	Rožňava	k1gFnSc1
</s>
<s>
Ružomberok	Ružomberok	k1gInSc1
</s>
<s>
Sabinov	Sabinov	k1gInSc1
</s>
<s>
Senec	Senec	k1gInSc1
</s>
<s>
Senica	Senica	k6eAd1
</s>
<s>
Skalica	Skalica	k6eAd1
</s>
<s>
Snina	Snina	k6eAd1
</s>
<s>
Sobrance	Sobrance	k1gFnSc1
</s>
<s>
Spišská	spišský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
</s>
<s>
Stropkov	Stropkov	k1gInSc1
</s>
<s>
Svidník	Svidník	k1gInSc1
</s>
<s>
Šaľa	Šaľa	k6eAd1
</s>
<s>
Topoľčany	Topoľčan	k1gMnPc4
</s>
<s>
Trebišov	Trebišov	k1gInSc1
</s>
<s>
Trenčín	Trenčín	k1gInSc1
</s>
<s>
Trnava	Trnava	k1gFnSc1
</s>
<s>
Turčianske	Turčianske	k6eAd1
Teplice	Teplice	k1gFnPc1
</s>
<s>
Tvrdošín	Tvrdošín	k1gMnSc1
</s>
<s>
Veľký	Veľký	k2eAgMnSc1d1
Krtíš	Krtíš	k1gMnSc1
</s>
<s>
Vranov	Vranov	k1gInSc1
nad	nad	k7c7
Topľou	Topľa	k1gFnSc7
</s>
<s>
Zlaté	zlatý	k2eAgFnSc3d1
Moravce	Moravka	k1gFnSc3
</s>
<s>
Zvolen	Zvolen	k1gInSc1
</s>
<s>
Žarnovica	Žarnovica	k6eAd1
</s>
<s>
Žiar	Žiar	k1gInSc1
nad	nad	k7c4
Hronom	Hronom	k1gInSc4
</s>
<s>
Žilina	Žilina	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Slovenska	Slovensko	k1gNnSc2
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Administratívne	Administratívne	k1gFnSc6
členenie	členenie	k1gFnSc2
Slovenska	Slovensko	k1gNnSc2
v	v	k7c4
rokoch	rokoch	k1gInSc4
1990-1996	1990-1996	k4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
Evropy	Evropa	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Athos	Athos	k1gMnSc1
(	(	kIx(
<g/>
GR	GR	kA
<g/>
)	)	kIx)
</s>
<s>
Alandy	Alanda	k1gFnPc1
(	(	kIx(
<g/>
FI	fi	k0
<g/>
)	)	kIx)
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Guernsey	Guernsea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Mayen	Mayna	k1gFnPc2
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
</s>
<s>
Jersey	Jersea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Špicberky	Špicberky	k1gFnPc1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
Území	území	k1gNnSc1
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
