<s>
Jóko	Jóko	k6eAd1	Jóko
Kanno	Kanno	k6eAd1	Kanno
(	(	kIx(	(
<g/>
菅	菅	k?	菅
よ	よ	k?	よ
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Mijagi	Mijagi	k1gNnSc1	Mijagi
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
hudební	hudební	k2eAgFnSc1d1	hudební
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
.	.	kIx.	.
</s>
<s>
Složila	složit	k5eAaPmAgFnS	složit
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
počítačovým	počítačový	k2eAgFnPc3d1	počítačová
hrám	hra	k1gFnPc3	hra
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
seriálům	seriál	k1gInPc3	seriál
a	a	k8xC	a
reklamám	reklama	k1gFnPc3	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
nejznámější	známý	k2eAgInPc4d3	nejznámější
počiny	počin	k1gInPc4	počin
patří	patřit	k5eAaImIp3nS	patřit
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
seriálům	seriál	k1gInPc3	seriál
Cowboy	Cowboa	k1gFnSc2	Cowboa
Bebop	bebop	k1gInSc1	bebop
<g/>
,	,	kIx,	,
Macross	Macross	k1gInSc1	Macross
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
či	či	k8xC	či
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc4	Complex
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
frontmankou	frontmanka	k1gFnSc7	frontmanka
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnPc2	The
Seatbelts	Seatbelts	k1gInSc1	Seatbelts
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
nahrála	nahrát	k5eAaBmAgFnS	nahrát
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
soundtrackům	soundtrack	k1gInPc3	soundtrack
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
pracovala	pracovat	k5eAaImAgFnS	pracovat
<g/>
.	.	kIx.	.
</s>
