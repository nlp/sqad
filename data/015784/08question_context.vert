<s>
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
byl	být	k5eAaImAgMnS
výsledkem	výsledek	k1gInSc7
japonské	japonský	k2eAgFnPc1d1
invaze	invaze	k1gFnPc1
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
v	v	k7c6
letech	léto	k1gNnPc6
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bránily	bránit	k5eAaImAgFnP
spojené	spojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
USA	USA	kA
a	a	k8xC
Filipínců	Filipínec	k1gMnPc2
<g/>
.	.	kIx.
</s>