<s>
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
invazi	invaze	k1gFnSc6
Japonců	Japonec	k1gMnPc2
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
znovudobytí	znovudobytí	k1gNnSc6
Filipín	Filipíny	k1gFnPc2
Američany	Američan	k1gMnPc4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
některé	některý	k3yIgFnPc1
části	část	k1gFnPc1
potřebují	potřebovat	k5eAaImIp3nP
upravit	upravit	k5eAaPmF
(	(	kIx(
<g/>
strojový	strojový	k2eAgInSc4d1
překlad	překlad	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Japonské	japonský	k2eAgInPc1d1
střední	střední	k2eAgInPc1d1
tanky	tank	k1gInPc1
typu	typ	k1gInSc2
89	#num#	k4
I-gó	I-gó	k1gMnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc2
(	(	kIx(
<g/>
中	中	k?
~	~	kIx~
rota	rota	k1gFnSc1
<g/>
)	)	kIx)
7	#num#	k4
<g/>
.	.	kIx.
senša	senš	k1gInSc2
rentai	renta	k1gFnSc2
(	(	kIx(
<g/>
戦	戦	k?
~	~	kIx~
tankový	tankový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
)	)	kIx)
postupují	postupovat	k5eAaImIp3nP
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
po	po	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
6	#num#	k4
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
USA	USA	kA
Filipínské	filipínský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Douglas	Douglas	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
Jonathan	Jonathan	k1gMnSc1
M.	M.	kA
Wainwright	Wainwright	k1gMnSc1
George	Georg	k1gMnSc2
F.	F.	kA
Moore	Moor	k1gInSc5
Lewis	Lewis	k1gFnPc6
H.	H.	kA
Brereton	Brereton	k1gInSc4
Vincente	Vincent	k1gMnSc5
Lim	Lima	k1gFnPc2
</s>
<s>
Masaharu	Masahara	k1gFnSc4
HommaKjúdži	HommaKjúdž	k1gFnSc3
KuboSueto	KuboSueta	k1gFnSc5
HiroseKenzaburo	HiroseKenzabura	k1gFnSc5
HaraŠódži	HaraŠódž	k1gFnSc6
NišimuraRaizó	NišimuraRaizó	k1gFnSc4
TanakaHidejoši	TanakaHidejoš	k1gMnSc3
ObataIbó	ObataIbó	k1gMnSc3
TakahašiNišizo	TakahašiNišiza	k1gFnSc5
Cukahara	Cukahar	k1gMnSc2
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
31	#num#	k4
095	#num#	k4
americká	americký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
asi	asi	k9
120	#num#	k4
000	#num#	k4
Filipínská	filipínský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
+	+	kIx~
námořnictvo	námořnictvo	k1gNnSc1
<g/>
+	+	kIx~
námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
</s>
<s>
129	#num#	k4
435	#num#	k4
armáda	armáda	k1gFnSc1
<g/>
12	#num#	k4
752	#num#	k4
armádní	armádní	k2eAgInSc4d1
letectvo	letectvo	k1gNnSc1
<g/>
49	#num#	k4
752	#num#	k4
námořnictvo	námořnictvo	k1gNnSc1
a	a	k8xC
námořní	námořní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
nejméně	málo	k6eAd3
10	#num#	k4
800	#num#	k4
mrtvýchnejméně	mrtvýchnejméně	k6eAd1
30	#num#	k4
000	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
100	#num#	k4
000	#num#	k4
zajatých	zajatá	k1gFnPc2
</s>
<s>
nejméně	málo	k6eAd3
7900	#num#	k4
mrtvýchnejméně	mrtvýchnejméně	k6eAd1
13	#num#	k4
200	#num#	k4
zraněných	zraněný	k1gMnPc2
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
byl	být	k5eAaImAgMnS
výsledkem	výsledek	k1gInSc7
japonské	japonský	k2eAgFnPc1d1
invaze	invaze	k1gFnPc1
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
v	v	k7c6
letech	léto	k1gNnPc6
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bránily	bránit	k5eAaImAgFnP
spojené	spojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
USA	USA	kA
a	a	k8xC
Filipínců	Filipínec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
japonské	japonský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
a	a	k8xC
největší	veliký	k2eAgFnSc1d3
kapitulace	kapitulace	k1gFnSc1
v	v	k7c6
amerických	americký	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInPc1d1
boje	boj	k1gInPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgInP
na	na	k7c6
největších	veliký	k2eAgInPc6d3
ostrovech	ostrov	k1gInPc6
Luzon	Luzona	k1gFnPc2
(	(	kIx(
<g/>
na	na	k7c6
severu	sever	k1gInSc6
Filipín	Filipíny	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Mindanao	Mindanao	k6eAd1
(	(	kIx(
<g/>
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
obránci	obránce	k1gMnPc1
měli	mít	k5eAaImAgMnP
početní	početní	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
převážně	převážně	k6eAd1
o	o	k7c4
nedostatečně	dostatečně	k6eNd1
vycvičené	vycvičený	k2eAgFnPc4d1
a	a	k8xC
nedostatečně	dostatečně	k6eNd1
vybavené	vybavený	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
sehrálo	sehrát	k5eAaPmAgNnS
i	i	k9
americké	americký	k2eAgNnSc1d1
podcenění	podcenění	k1gNnSc1
Japonců	Japonec	k1gMnPc2
<g/>
,	,	kIx,
neschopnost	neschopnost	k1gFnSc4
americko-filipínského	americko-filipínský	k2eAgNnSc2d1
velení	velení	k1gNnSc2
a	a	k8xC
japonský	japonský	k2eAgInSc4d1
překvapivý	překvapivý	k2eAgInSc4d1
úder	úder	k1gInSc4
na	na	k7c4
filipínská	filipínský	k2eAgNnPc4d1
letiště	letiště	k1gNnPc4
během	během	k7c2
prvního	první	k4xOgMnSc2
dne	den	k1gInSc2
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
leteckými	letecký	k2eAgInPc7d1
údery	úder	k1gInPc7
v	v	k7c6
následujících	následující	k2eAgInPc6d1
dnech	den	k1gInPc6
se	se	k3xPyFc4
císařskému	císařský	k2eAgNnSc3d1
námořnímu	námořní	k2eAgNnSc3d1
a	a	k8xC
armádnímu	armádní	k2eAgNnSc3d1
letectvu	letectvo	k1gNnSc3
podařilo	podařit	k5eAaPmAgNnS
prakticky	prakticky	k6eAd1
eliminovat	eliminovat	k5eAaBmF
americké	americký	k2eAgFnPc4d1
a	a	k8xC
filipínské	filipínský	k2eAgFnPc4d1
letecké	letecký	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
získat	získat	k5eAaPmF
nadvládu	nadvláda	k1gFnSc4
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnPc1
japonská	japonský	k2eAgNnPc1d1
vylodění	vylodění	k1gNnPc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
u	u	k7c2
Aparri	Aparr	k1gFnSc2
a	a	k8xC
Viganu	Vigan	k1gInSc2
na	na	k7c6
severu	sever	k1gInSc6
Luzonu	Luzon	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
následným	následný	k2eAgInSc7d1
postupem	postup	k1gInSc7
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
další	další	k2eAgFnPc1d1
japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
vylodily	vylodit	k5eAaPmAgFnP
u	u	k7c2
Legazpi	Legazp	k1gFnSc2
na	na	k7c6
jihu	jih	k1gInSc6
Luzonu	Luzona	k1gFnSc4
a	a	k8xC
zahájily	zahájit	k5eAaPmAgInP
postup	postup	k1gInSc4
na	na	k7c4
sever	sever	k1gInSc4
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
vylodily	vylodit	k5eAaPmAgFnP
v	v	k7c6
Davau	Davaum	k1gNnSc6
na	na	k7c6
Mindanau	Mindanaus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vylodění	vylodění	k1gNnSc3
hlavních	hlavní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
generálporučíka	generálporučík	k1gMnSc2
(	(	kIx(
<g/>
中	中	k?
čúdžó	čúdžó	k?
<g/>
)	)	kIx)
Masaharu	Masahar	k1gInSc2
Hommy	Homma	k1gFnSc2
na	na	k7c4
Luzonu	Luzona	k1gFnSc4
došlo	dojít	k5eAaPmAgNnS
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
vedl	vést	k5eAaImAgMnS
Homma	Homma	k1gNnSc4
úder	úder	k1gInSc4
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vyhlášena	vyhlásit	k5eAaPmNgFnS
otevřeným	otevřený	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americko-filipínské	americko-filipínský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
se	se	k3xPyFc4
stáhly	stáhnout	k5eAaPmAgFnP
na	na	k7c4
poloostrov	poloostrov	k1gInSc4
Bataan	Bataana	k1gFnPc2
a	a	k8xC
ostrov	ostrov	k1gInSc1
Corregidor	Corregidora	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dokázaly	dokázat	k5eAaPmAgFnP
po	po	k7c4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
klást	klást	k5eAaImF
odpor	odpor	k1gInSc4
až	až	k9
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1942	#num#	k4
na	na	k7c6
Bataanu	Bataan	k1gInSc6
<g/>
,	,	kIx,
či	či	k8xC
o	o	k7c4
měsíc	měsíc	k1gInSc4
déle	dlouho	k6eAd2
na	na	k7c6
Corregidoru	Corregidor	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajatce	zajatec	k1gMnSc2
poté	poté	k6eAd1
čekal	čekat	k5eAaImAgInS
takzvaný	takzvaný	k2eAgInSc1d1
Bataanský	Bataanský	k2eAgInSc1d1
pochod	pochod	k1gInSc1
smrti	smrt	k1gFnSc2
do	do	k7c2
zajateckých	zajatecký	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zahynulo	zahynout	k5eAaPmAgNnS
přes	přes	k7c4
8000	#num#	k4
Američanů	Američan	k1gMnPc2
a	a	k8xC
Filipínců	Filipínec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
ostatních	ostatní	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
se	se	k3xPyFc4
ale	ale	k9
americko-filipínské	americko-filipínský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
držely	držet	k5eAaImAgFnP
ještě	ještě	k9
o	o	k7c4
něco	něco	k3yInSc4
déle	dlouho	k6eAd2
a	a	k8xC
teprve	teprve	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
kapitulovala	kapitulovat	k5eAaBmAgFnS
poslední	poslední	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Samar	Samara	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
obránci	obránce	k1gMnPc1
odešli	odejít	k5eAaPmAgMnP
jako	jako	k9
partyzáni	partyzán	k1gMnPc1
do	do	k7c2
hor	hora	k1gFnPc2
a	a	k8xC
vedli	vést	k5eAaImAgMnP
záškodnickou	záškodnický	k2eAgFnSc4d1
válku	válka	k1gFnSc4
až	až	k9
do	do	k7c2
osvobození	osvobození	k1gNnSc2
Filipín	Filipíny	k1gFnPc2
<g/>
,	,	kIx,
respektive	respektive	k9
do	do	k7c2
kapitulace	kapitulace	k1gFnSc2
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
japonských	japonský	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
je	být	k5eAaImIp3nS
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
uváděno	uvádět	k5eAaImNgNnS
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
a	a	k8xC
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
na	na	k7c6
druhém	druhý	k4xOgNnSc6
</s>
<s>
Při	při	k7c6
přepisu	přepis	k1gInSc6
japonštiny	japonština	k1gFnSc2
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
česká	český	k2eAgFnSc1d1
transkripce	transkripce	k1gFnSc1
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Japonské	japonský	k2eAgInPc1d1
plány	plán	k1gInPc1
</s>
<s>
Cíle	cíl	k1gInPc1
</s>
<s>
Generál	generál	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Hisaiči	Hisaič	k1gInSc6
Terauči	Terauč	k1gInSc6
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
Jižní	jižní	k2eAgFnSc2d1
expediční	expediční	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
armád	armáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
japonské	japonský	k2eAgFnSc6d1
kapitulaci	kapitulace	k1gFnSc6
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
mrtvici	mrtvice	k1gFnSc4
v	v	k7c6
zajateckém	zajatecký	k2eAgInSc6d1
táboře	tábor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1
Masaharu	Masahar	k1gInSc2
Homma	Hommum	k1gNnSc2
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
byl	být	k5eAaImAgMnS
za	za	k7c4
válečné	válečný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
spáchané	spáchaný	k2eAgInPc4d1
jeho	jeho	k3xOp3gFnSc1
vojáky	voják	k1gMnPc4
na	na	k7c4
Filipínách	Filipíny	k1gFnPc6
postaven	postavit	k5eAaPmNgInS
před	před	k7c4
válečný	válečný	k2eAgInSc4d1
soud	soud	k1gInSc4
o	o	k7c6
jehož	jehož	k3xOyRp3gFnSc6
nestrannosti	nestrannost	k1gFnSc6
se	se	k3xPyFc4
vedou	vést	k5eAaImIp3nP
spory	spor	k1gInPc1
a	a	k8xC
následně	následně	k6eAd1
zastřelen	zastřelit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
osobní	osobní	k2eAgFnSc4d1
MacArthurovu	MacArthurův	k2eAgFnSc4d1
pomstu	pomsta	k1gFnSc4
za	za	k7c4
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
mu	on	k3xPp3gMnSc3
Homma	Homm	k1gMnSc2
uštědřil	uštědřit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
plán	plán	k1gInSc1
na	na	k7c6
obsazení	obsazení	k1gNnSc6
Filipín	Filipíny	k1gFnPc2
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
rozsáhlejšího	rozsáhlý	k2eAgInSc2d2
plánu	plán	k1gInSc2
expanze	expanze	k1gFnSc2
do	do	k7c2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
Pacifiku	Pacifik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plán	plán	k1gInSc1
expanze	expanze	k1gFnSc2
na	na	k7c4
jih	jih	k1gInSc4
získal	získat	k5eAaPmAgInS
mezi	mezi	k7c7
japonskými	japonský	k2eAgMnPc7d1
militaristy	militarista	k1gMnPc7
podporu	podpor	k1gInSc2
zejména	zejména	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byla	být	k5eAaImAgFnS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
poražena	porazit	k5eAaPmNgFnS
Sověty	Sovět	k1gMnPc7
v	v	k7c6
bitvách	bitva	k1gFnPc6
u	u	k7c2
Chalchyn	Chalchyna	k1gFnPc2
<g/>
–	–	k?
<g/>
golu	golu	k5eAaPmIp1nS
v	v	k7c6
létě	léto	k1gNnSc6
1939	#num#	k4
a	a	k8xC
po	po	k7c6
uzavření	uzavření	k1gNnSc6
sovětsko	sovětsko	k6eAd1
<g/>
–	–	k?
<g/>
japonské	japonský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
neútočení	neútočení	k1gNnSc6
v	v	k7c6
dubnu	duben	k1gInSc6
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
sehrál	sehrát	k5eAaPmAgInS
i	i	k9
neuspokojivý	uspokojivý	k2eNgInSc1d1
vývoj	vývoj	k1gInSc1
„	„	k?
<g/>
čínského	čínský	k2eAgInSc2d1
incidentu	incident	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
nakonec	nakonec	k6eAd1
nátlak	nátlak	k1gInSc1
a	a	k8xC
embargo	embargo	k1gNnSc1
USA	USA	kA
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Holandska	Holandsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Invazí	invaze	k1gFnPc2
na	na	k7c4
jih	jih	k1gInSc4
si	se	k3xPyFc3
Japonsko	Japonsko	k1gNnSc1
slibovalo	slibovat	k5eAaImAgNnS
získat	získat	k5eAaPmF
přístup	přístup	k1gInSc4
ke	k	k7c3
strategickým	strategický	k2eAgFnPc3d1
surovinám	surovina	k1gFnPc3
v	v	k7c6
Britské	britský	k2eAgFnSc6d1
Malajsii	Malajsie	k1gFnSc6
a	a	k8xC
Holandské	holandský	k2eAgFnSc6d1
východní	východní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc1
„	„	k?
<g/>
omezené	omezený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
“	“	k?
pouze	pouze	k6eAd1
proti	proti	k7c3
Británii	Británie	k1gFnSc3
a	a	k8xC
Holandsku	Holandsko	k1gNnSc6
<g/>
,	,	kIx,
bez	bez	k7c2
zatahování	zatahování	k1gNnSc2
USA	USA	kA
do	do	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Japonci	Japonec	k1gMnPc1
vyhodnocena	vyhodnotit	k5eAaPmNgFnS
jako	jako	k9
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
invaze	invaze	k1gFnSc1
na	na	k7c4
jih	jih	k1gInSc4
proto	proto	k8xC
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
i	i	k9
eliminování	eliminování	k1gNnSc1
amerického	americký	k2eAgNnSc2d1
tichomořského	tichomořský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
japonským	japonský	k2eAgNnSc7d1
Spojeným	spojený	k2eAgNnSc7d1
loďstvem	loďstvo	k1gNnSc7
a	a	k8xC
obsazení	obsazení	k1gNnSc1
Filipín	Filipíny	k1gFnPc2
Jižní	jižní	k2eAgFnSc1d1
expediční	expediční	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
armád	armáda	k1gFnPc2
(	(	kIx(
<g/>
南	南	k?
nanpo	nanpa	k1gFnSc5
gun	gun	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
expediční	expediční	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
vznikla	vzniknout	k5eAaPmAgFnS
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1934	#num#	k4
pod	pod	k7c7
velením	velení	k1gNnSc7
generála	generál	k1gMnSc2
(	(	kIx(
<g/>
大	大	k?
taišó	taišó	k?
<g/>
)	)	kIx)
hraběte	hrabě	k1gMnSc2
(	(	kIx(
<g/>
伯	伯	k?
hakušaku	hakušak	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
bývalého	bývalý	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
války	válka	k1gFnSc2
Hisaiči	Hisaič	k1gMnPc1
Teraučiho	Terauči	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
vypracovat	vypracovat	k5eAaPmF
plán	plán	k1gInSc4
války	válka	k1gFnSc2
pro	pro	k7c4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
jednání	jednání	k1gNnSc1
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
selhalo	selhat	k5eAaPmAgNnS
a	a	k8xC
Japonsko	Japonsko	k1gNnSc1
nedosáhlo	dosáhnout	k5eNaPmAgNnS
svých	svůj	k3xOyFgInPc2
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
Teraučiho	Terauči	k1gMnSc4
velením	velení	k1gNnSc7
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc1
armády	armáda	k1gFnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
<g/>
)	)	kIx)
s	s	k7c7
celkem	celkem	k6eAd1
deseti	deset	k4xCc7
divizemi	divize	k1gFnPc7
a	a	k8xC
třemi	tři	k4xCgFnPc7
smíšenými	smíšený	k2eAgFnPc7d1
brigádami	brigáda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
proti	proti	k7c3
Filipínám	Filipíny	k1gFnPc3
a	a	k8xC
Malajsii	Malajsie	k1gFnSc3
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
zahájeny	zahájit	k5eAaPmNgFnP
současně	současně	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
rozkazu	rozkaz	k1gInSc2
císařského	císařský	k2eAgInSc2d1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
(	(	kIx(
<g/>
大	大	k?
daihonei	daihone	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
samy	sám	k3xTgInPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
neměly	mít	k5eNaImAgFnP
příliš	příliš	k6eAd1
surovin	surovina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
Japonsko	Japonsko	k1gNnSc1
potřebovalo	potřebovat	k5eAaImAgNnS
a	a	k8xC
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
expanze	expanze	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
měly	mít	k5eAaImAgInP
strategickou	strategický	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Invaze	invaze	k1gFnSc1
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
proto	proto	k8xC
měla	mít	k5eAaImAgFnS
tři	tři	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zabránit	zabránit	k5eAaPmF
Američanům	Američan	k1gMnPc3
ve	v	k7c6
využití	využití	k1gNnSc6
Filipín	Filipíny	k1gFnPc2
jako	jako	k8xS,k8xC
předsunuté	předsunutý	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Získat	získat	k5eAaPmF
nástupní	nástupní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
a	a	k8xC
zásobovací	zásobovací	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
pro	pro	k7c4
operace	operace	k1gFnPc4
proti	proti	k7c3
Holandské	holandský	k2eAgFnSc3d1
východní	východní	k2eAgFnSc3d1
Indii	Indie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Zabezpečit	zabezpečit	k5eAaPmF
zásobovací	zásobovací	k2eAgFnSc4d1
linii	linie	k1gFnSc4
mezi	mezi	k7c7
okupovaným	okupovaný	k2eAgNnSc7d1
územím	území	k1gNnSc7
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
mateřskými	mateřský	k2eAgInPc7d1
ostrovy	ostrov	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
podniknout	podniknout	k5eAaPmF
invazi	invaze	k1gFnSc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
také	také	k9
třeba	třeba	k6eAd1
získat	získat	k5eAaPmF
informace	informace	k1gFnPc4
o	o	k7c6
plánovaných	plánovaný	k2eAgFnPc6d1
vyloďovacích	vyloďovací	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
a	a	k8xC
o	o	k7c6
prostředcích	prostředek	k1gInPc6
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1941	#num#	k4
byla	být	k5eAaImAgFnS
proto	proto	k6eAd1
císařským	císařský	k2eAgNnSc7d1
námořnictvem	námořnictvo	k1gNnSc7
zřízena	zřízen	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k1gNnSc2
(	(	kIx(
<g/>
航	航	k?
~	~	kIx~
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
/	/	kIx~
<g/>
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
bombardéry	bombardér	k1gInPc4
G3M2	G3M2	k1gMnSc1
„	„	k?
<g/>
Nell	Nell	k1gMnSc1
<g/>
“	“	k?
byly	být	k5eAaImAgInP
upraveny	upravit	k5eAaPmNgInP
pro	pro	k7c4
dálkové	dálkový	k2eAgFnPc4d1
fotoprůzkumné	fotoprůzkumný	k2eAgFnPc4d1
mise	mise	k1gFnPc4
a	a	k8xC
zpočátku	zpočátku	k6eAd1
působily	působit	k5eAaImAgInP
z	z	k7c2
letiště	letiště	k1gNnSc2
Takao	Takao	k6eAd1
na	na	k7c6
Formose	Formosa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
průzkumný	průzkumný	k2eAgInSc4d1
let	let	k1gInSc4
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
měl	mít	k5eAaImAgMnS
za	za	k7c4
cíl	cíl	k1gInSc4
Legazpi	Legazp	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c6
zpátečním	zpáteční	k2eAgInSc6d1
letu	let	k1gInSc6
bombardér	bombardér	k1gMnSc1
přelétal	přelétat	k5eAaPmAgMnS,k5eAaImAgMnS
přes	přes	k7c4
Luzon	Luzon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
byla	být	k5eAaImAgFnS
jednotka	jednotka	k1gFnSc1
přeložena	přeložit	k5eAaPmNgFnS
na	na	k7c4
Palau	Palaa	k1gFnSc4
a	a	k8xC
jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
americké	americký	k2eAgInPc1d1
mandátní	mandátní	k2eAgInPc1d1
tichomořské	tichomořský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plán	plán	k1gInSc1
operace	operace	k1gFnSc2
M	M	kA
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
plán	plán	k1gInSc1
operace	operace	k1gFnSc2
„	„	k?
<g/>
M	M	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
M作	M作	k1gMnSc3
M	M	kA
sakusen	sakusen	k1gInSc1
<g/>
)	)	kIx)
počítal	počítat	k5eAaImAgInS
v	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
s	s	k7c7
leteckými	letecký	k2eAgInPc7d1
útoky	útok	k1gInPc7
proti	proti	k7c3
cílům	cíl	k1gInPc3
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovat	následovat	k5eAaImF
měly	mít	k5eAaImAgFnP
tři	tři	k4xCgFnPc4
výsadky	výsadka	k1gFnPc4
na	na	k7c4
Luzonu	Luzona	k1gFnSc4
<g/>
:	:	kIx,
v	v	k7c6
Aparri	Aparr	k1gFnSc6
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
Viganu	Vigana	k1gFnSc4
na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
Legazpi	Legazp	k1gFnSc6
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplnit	doplnit	k5eAaPmF
je	on	k3xPp3gNnSc4
měl	mít	k5eAaImAgInS
jeden	jeden	k4xCgInSc1
výsadek	výsadek	k1gInSc1
na	na	k7c4
Mindanau	Mindanaa	k1gFnSc4
v	v	k7c4
Davao	Davao	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsadky	výsadek	k1gInPc7
měly	mít	k5eAaImAgFnP
jako	jako	k9
primární	primární	k2eAgInSc4d1
cíl	cíl	k1gInSc4
zajištění	zajištění	k1gNnSc2
letišť	letiště	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
pak	pak	k6eAd1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k8xC,k8xS
předsunuté	předsunutý	k2eAgFnSc2d1
základny	základna	k1gFnSc2
jednak	jednak	k8xC
pro	pro	k7c4
podporu	podpora	k1gFnSc4
dalšího	další	k2eAgInSc2d1
postupu	postup	k1gInSc2
na	na	k7c6
Luzonu	Luzon	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Borneo	Borneo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
fázi	fáze	k1gFnSc6
mělo	mít	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
vylodění	vylodění	k1gNnSc3
na	na	k7c4
Luzonu	Luzona	k1gFnSc4
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
a	a	k8xC
v	v	k7c4
Lamon	Lamon	k1gNnSc4
Bay	Bay	k1gFnSc2
s	s	k7c7
následným	následný	k2eAgInSc7d1
postupem	postup	k1gInSc7
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
císařský	císařský	k2eAgInSc1d1
generální	generální	k2eAgInSc1d1
štáb	štáb	k1gInSc1
a	a	k8xC
velitelství	velitelství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
expediční	expediční	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
předpokládaly	předpokládat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
dobytí	dobytí	k1gNnSc1
Luzonu	Luzon	k1gInSc2
bude	být	k5eAaImBp3nS
trvat	trvat	k5eAaImF
přibližně	přibližně	k6eAd1
50	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
poté	poté	k6eAd1
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
síly	síla	k1gFnPc1
uvolněny	uvolněn	k2eAgFnPc1d1
pro	pro	k7c4
další	další	k2eAgInSc4d1
postup	postup	k1gInSc4
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponechané	ponechaný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
pak	pak	k6eAd1
měly	mít	k5eAaImAgInP
dobýt	dobýt	k5eAaPmF
zbývající	zbývající	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
a	a	k8xC
celá	celý	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
měla	mít	k5eAaImAgFnS
skončit	skončit	k5eAaPmF
před	před	k7c7
koncem	konec	k1gInSc7
března	březen	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operace	operace	k1gFnSc1
„	„	k?
<g/>
M	M	kA
<g/>
“	“	k?
ale	ale	k8xC
nebyla	být	k5eNaImAgFnS
prvním	první	k4xOgInSc7
japonským	japonský	k2eAgInSc7d1
plánem	plán	k1gInSc7
na	na	k7c4
obsazení	obsazení	k1gNnSc4
Filipín	Filipíny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plány	plán	k1gInPc4
invaze	invaze	k1gFnSc1
na	na	k7c4
toto	tento	k3xDgNnSc4
souostroví	souostroví	k1gNnSc4
se	se	k3xPyFc4
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
i	i	k8xC
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
zabývaly	zabývat	k5eAaImAgFnP
jíž	jenž	k3xRgFnSc3
od	od	k7c2
pozdního	pozdní	k2eAgInSc2d1
období	období	k1gNnSc6
Meidži	Meidž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Invazní	invazní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
</s>
<s>
Letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Rjúdžó	Rjúdžó	k1gFnPc2
byla	být	k5eAaImAgFnS
jedinou	jediný	k2eAgFnSc7d1
letadlovou	letadlový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
ve	v	k7c6
filipínském	filipínský	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
stíhací	stíhací	k2eAgFnSc1d1
A6M2	A6M2	k1gFnSc1
„	„	k?
<g/>
Zero	Zero	k6eAd1
<g/>
“	“	k?
operovala	operovat	k5eAaImAgFnS
z	z	k7c2
pozemních	pozemní	k2eAgFnPc2d1
základen	základna	k1gFnPc2
na	na	k7c6
Formose	Formosa	k1gFnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
Tainan	Tainan	k1gMnSc1
kókútai	kókúta	k1gFnSc2
</s>
<s>
Armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
nasadilo	nasadit	k5eAaPmAgNnS
starší	starý	k2eAgFnSc7d2
Ki-	Ki-	k1gFnSc7
<g/>
27	#num#	k4
<g/>
,	,	kIx,
neboť	neboť	k8xC
moderních	moderní	k2eAgFnPc2d1
Ki-	Ki-	k1gFnPc2
<g/>
43	#num#	k4
bylo	být	k5eAaImAgNnS
málo	málo	k1gNnSc1
a	a	k8xC
byly	být	k5eAaImAgFnP
přednostně	přednostně	k6eAd1
nasazeny	nasadit	k5eAaPmNgFnP
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ki-	Ki-	k1gFnSc1
<g/>
27	#num#	k4
byly	být	k5eAaImAgInP
vybaveny	vybaven	k2eAgInPc1d1
24	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
</s>
<s>
Podporu	podpora	k1gFnSc4
vyloďovaným	vyloďovaný	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
poskytovaly	poskytovat	k5eAaImAgInP
i	i	k9
pozorovací	pozorovací	k2eAgInPc1d1
hydroplány	hydroplán	k1gInPc1
F1M	F1M	k1gFnSc2
„	„	k?
<g/>
Pete	Pet	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
Generál	generál	k1gMnSc1
Terauči	Terauč	k1gFnSc3
pověřil	pověřit	k5eAaPmAgInS
dobytím	dobytí	k1gNnSc7
Filipín	Filipíny	k1gFnPc2
14	#num#	k4
<g/>
.	.	kIx.
armádu	armáda	k1gFnSc4
generálporučíka	generálporučík	k1gMnSc2
(	(	kIx(
<g/>
中	中	k?
čúdžó	čúdžó	k?
<g/>
)	)	kIx)
Masaharu	Masahar	k1gInSc2
Hommy	Homma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leteckou	letecký	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
měla	mít	k5eAaImAgFnS
poskytnout	poskytnout	k5eAaPmF
5	#num#	k4
<g/>
.	.	kIx.
letecká	letecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
(	(	kIx(
<g/>
飛	飛	k?
hikóšidan	hikóšidan	k1gInSc1
<g/>
)	)	kIx)
generálporučíka	generálporučík	k1gMnSc2
(	(	kIx(
<g/>
飛	飛	k?
hikó	hikó	k?
čúdžó	čúdžó	k?
<g/>
)	)	kIx)
Hidejoši	Hidejoš	k1gMnPc1
Obaty	Obata	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
Formosu	Formos	k1gInSc6
převelena	převelen	k2eAgFnSc1d1
z	z	k7c2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylodění	vylodění	k1gNnSc1
měl	mít	k5eAaImAgInS
zajistit	zajistit	k5eAaPmF
Filipínský	filipínský	k2eAgInSc1d1
úderný	úderný	k2eAgInSc1d1
svaz	svaz	k1gInSc1
viceadmirála	viceadmirál	k1gMnSc2
(	(	kIx(
<g/>
中	中	k?
čúdžó	čúdžó	k?
<g/>
)	)	kIx)
Ibó	Ibó	k1gMnSc2
Takahašiho	Takahaši	k1gMnSc2
pro	pro	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
vyčlenilo	vyčlenit	k5eAaPmAgNnS
jednotky	jednotka	k1gFnPc4
3	#num#	k4
<g/>
.	.	kIx.
loďstva	loďstvo	k1gNnSc2
(	(	kIx(
<g/>
艦	艦	k?
kantai	kanta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgInSc4d1
letectvo	letectvo	k1gNnSc1
přispělo	přispět	k5eAaPmAgNnS
11	#num#	k4
<g/>
.	.	kIx.
leteckou	letecký	k2eAgFnSc4d1
flotou	flotá	k1gFnSc4
(	(	kIx(
<g/>
航	航	k?
kókú	kókú	k?
kantai	kanta	k1gFnSc2
<g/>
)	)	kIx)
viceadmirála	viceadmirál	k1gMnSc2
Nišizo	Nišiza	k1gFnSc5
Cukahary	Cukahara	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
operovala	operovat	k5eAaImAgFnS
z	z	k7c2
pozemních	pozemní	k2eAgFnPc2d1
základen	základna	k1gFnPc2
na	na	k7c6
Formose	Formosa	k1gFnSc6
a	a	k8xC
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hommova	Hommův	k2eAgFnSc1d1
14	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
mohla	moct	k5eAaImAgFnS
nasadit	nasadit	k5eAaPmF
do	do	k7c2
první	první	k4xOgFnSc2
linie	linie	k1gFnSc2
při	při	k7c6
dobývání	dobývání	k1gNnSc6
Luzonu	Luzona	k1gFnSc4
dvě	dva	k4xCgFnPc4
divize	divize	k1gFnPc4
<g/>
:	:	kIx,
16	#num#	k4
<g/>
.	.	kIx.
z	z	k7c2
Palau	Palaus	k1gInSc2
a	a	k8xC
Rjúkjú	Rjúkjú	k1gNnSc2
a	a	k8xC
48	#num#	k4
<g/>
.	.	kIx.
z	z	k7c2
Formosy	Formosa	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
65	#num#	k4
<g/>
.	.	kIx.
samostatná	samostatný	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
k	k	k7c3
posádkové	posádkový	k2eAgFnSc3d1
službě	služba	k1gFnSc3
<g/>
.	.	kIx.
48	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
sice	sice	k8xC
neměla	mít	k5eNaImAgFnS
bojové	bojový	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgFnPc2d3
jednotek	jednotka	k1gFnPc2
japonské	japonský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vycvičena	vycvičit	k5eAaPmNgFnS
pro	pro	k7c4
provádění	provádění	k1gNnSc4
obojživelných	obojživelný	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pověřena	pověřen	k2eAgFnSc1d1
provést	provést	k5eAaPmF
vylodění	vylodění	k1gNnSc4
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šestnáctá	šestnáctý	k4xOgFnSc1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
vybrána	vybrat	k5eAaPmNgFnS
jako	jako	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejlepších	dobrý	k2eAgFnPc2d3
divizí	divize	k1gFnPc2
dostupných	dostupný	k2eAgFnPc2d1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
pověřena	pověřit	k5eAaPmNgFnS
vyloděním	vylodění	k1gNnSc7
v	v	k7c4
Lamon	Lamon	k1gNnSc4
Bay	Bay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
disponovala	disponovat	k5eAaBmAgFnS
14	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
ještě	ještě	k6eAd1
dvěma	dva	k4xCgInPc7
tankovými	tankový	k2eAgInPc7d1
pluky	pluk	k1gInPc7
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dvěma	dva	k4xCgInPc7
pluky	pluk	k1gInPc7
těžkého	těžký	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
nezávislým	závislý	k2eNgInSc7d1
praporem	prapor	k1gInSc7
těžkého	těžký	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
čtyřmi	čtyři	k4xCgInPc7
prapory	prapor	k1gInPc7
(	(	kIx(
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
45	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
47	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
48	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
dvěma	dva	k4xCgFnPc7
nezávislými	závislý	k2eNgFnPc7d1
jednotkami	jednotka	k1gFnPc7
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
31	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
polního	polní	k2eAgNnSc2d1
protiletadlového	protiletadlový	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
,	,	kIx,
čtyřmi	čtyři	k4xCgFnPc7
nezávislými	závislý	k2eNgFnPc7d1
protitankovými	protitankový	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
nezávislým	závislý	k2eNgInSc7d1
praporem	prapor	k1gInSc7
minometů	minomet	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Invazní	invazní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
dále	daleko	k6eAd2
doplňovalo	doplňovat	k5eAaImAgNnS
několik	několik	k4yIc1
podpůrných	podpůrný	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
ženistů	ženista	k1gMnPc2
<g/>
,	,	kIx,
spojařů	spojař	k1gMnPc2
<g/>
,	,	kIx,
zásobovačů	zásobovač	k1gMnPc2
a	a	k8xC
vojenské	vojenský	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
samotné	samotný	k2eAgFnSc3d1
invazi	invaze	k1gFnSc3
bylo	být	k5eAaImAgNnS
3	#num#	k4
<g/>
.	.	kIx.
loďstvo	loďstvo	k1gNnSc4
posíleno	posílen	k2eAgNnSc4d1
o	o	k7c4
dvě	dva	k4xCgFnPc4
eskadry	eskadra	k1gFnPc4
torpédoborců	torpédoborec	k1gInPc2
(	(	kIx(
<g/>
水	水	k?
suirai	suirai	k6eAd1
sentai	sentai	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
těžký	těžký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Maja	Maja	k1gFnSc1
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
loďstva	loďstvo	k1gNnSc2
a	a	k8xC
lehkou	lehký	k2eAgFnSc4d1
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Rjúdžó	Rjúdžó	k1gFnSc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
letecké	letecký	k2eAgFnSc2d1
floty	flota	k1gFnSc2
(	(	kIx(
<g/>
航	航	k?
kókú	kókú	k?
kantai	kanta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipínský	filipínský	k2eAgInSc1d1
úderný	úderný	k2eAgInSc1d1
svaz	svaz	k1gInSc1
tak	tak	k6eAd1
sestával	sestávat	k5eAaImAgInS
vedle	vedle	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
z	z	k7c2
celkem	celkem	k6eAd1
pěti	pět	k4xCc2
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
(	(	kIx(
<g/>
Mjókó	Mjókó	k1gMnSc1
<g/>
,	,	kIx,
Nači	Nač	k1gFnSc5
<g/>
,	,	kIx,
Haguro	Hagura	k1gFnSc5
<g/>
,	,	kIx,
Ašigara	Ašigara	k1gFnSc1
a	a	k8xC
Maja	Maja	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pěti	pět	k4xCc2
lehkých	lehký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
(	(	kIx(
<g/>
Kuma	kuma	k1gFnSc1
<g/>
,	,	kIx,
Nagara	Nagara	k1gFnSc1
<g/>
,	,	kIx,
Natori	Natori	k1gNnSc1
<g/>
,	,	kIx,
Džincú	Džincú	k1gFnSc1
a	a	k8xC
Naka	Naka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
29	#num#	k4
torpédoborců	torpédoborec	k1gMnPc2
<g/>
,	,	kIx,
čtyř	čtyři	k4xCgFnPc2
mateřských	mateřský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
pro	pro	k7c4
hydroplány	hydroplán	k1gInPc4
(	(	kIx(
<g/>
Čitose	Čitosa	k1gFnSc6
<g/>
,	,	kIx,
Mizuho	Mizu	k1gMnSc4
<g/>
,	,	kIx,
Sanuki	Sanuki	k1gNnPc7
Maru	Maru	k1gFnSc2
a	a	k8xC
Sanjó	Sanjó	k1gFnSc2
Maru	Maru	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
17	#num#	k4
minolovek	minolovka	k1gFnPc2
a	a	k8xC
minonosek	minonoska	k1gFnPc2
a	a	k8xC
čtyř	čtyři	k4xCgFnPc2
torpédovek	torpédovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
vylodění	vylodění	k1gNnSc6
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
podporoval	podporovat	k5eAaImAgInS
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
hlavní	hlavní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
loďstva	loďstvo	k1gNnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
bitevními	bitevní	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
Kongó	Kongó	k1gFnSc2
a	a	k8xC
Haruna	Haruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Armádní	armádní	k2eAgMnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
letecká	letecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
koncem	koncem	k7c2
listopadu	listopad	k1gInSc2
přeletěla	přeletět	k5eAaPmAgFnS
na	na	k7c4
Formosu	Formosa	k1gFnSc4
z	z	k7c2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
mohla	moct	k5eAaImAgFnS
do	do	k7c2
boje	boj	k1gInSc2
nasadit	nasadit	k5eAaPmF
4	#num#	k4
<g/>
.	.	kIx.
sentai	sentai	k1gNnSc2
(	(	kIx(
<g/>
戦	戦	k?
~	~	kIx~
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
)	)	kIx)
se	s	k7c7
72	#num#	k4
stíhacími	stíhací	k2eAgInPc7d1
Ki-	Ki-	k1gMnSc4
<g/>
27	#num#	k4
„	„	k?
<g/>
Nate	Nate	k1gInSc1
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
27	#num#	k4
bitevními	bitevní	k2eAgFnPc7d1
Ki-	Ki-	k1gMnSc4
<g/>
30	#num#	k4
„	„	k?
<g/>
Ann	Ann	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
54	#num#	k4
bombardéry	bombardér	k1gMnPc7
Ki-	Ki-	k1gFnSc2
<g/>
15	#num#	k4
„	„	k?
<g/>
Babs	Babs	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
Ki-	Ki-	k1gFnSc1
<g/>
21	#num#	k4
„	„	k?
<g/>
Sally	Salla	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
Ki-	Ki-	k1gMnPc1
<g/>
48	#num#	k4
Sokei	Sokei	k1gNnSc2
<g/>
/	/	kIx~
<g/>
„	„	k?
<g/>
Lily	lít	k5eAaImAgInP
<g/>
“	“	k?
v	v	k7c4
pěti	pět	k4xCc7
čútai	čúta	k1gInPc7
(	(	kIx(
<g/>
中	中	k?
~	~	kIx~
perutích	peruť	k1gFnPc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
ještě	ještě	k9
třeba	třeba	k6eAd1
přičíst	přičíst	k5eAaPmF
34	#num#	k4
průzkumných	průzkumný	k2eAgInPc2d1
letounů	letoun	k1gInPc2
10	#num#	k4
<g/>
.	.	kIx.
dokuricu	dokuricus	k1gInSc2
čútai	čúta	k1gFnSc2
(	(	kIx(
<g/>
独	独	k?
~	~	kIx~
nezávislá	závislý	k2eNgFnSc1d1
peruť	peruť	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedenáctou	jedenáctý	k4xOgFnSc4
námořní	námořní	k2eAgFnSc4d1
flotu	flota	k1gFnSc4
tvořily	tvořit	k5eAaImAgFnP
21	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
letecká	letecký	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
(	(	kIx(
<g/>
航	航	k?
kókú	kókú	k?
sentai	senta	k1gFnSc2
<g/>
)	)	kIx)
s	s	k7c7
celkem	celek	k1gInSc7
pěti	pět	k4xCc2
a	a	k8xC
půl	půl	k1xP
kókútai	kókútai	k1gNnSc2
(	(	kIx(
<g/>
航	航	k?
~	~	kIx~
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
či	či	k8xC
letecká	letecký	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Tainan	Tainan	k1gInSc1
<g/>
,	,	kIx,
Takao	Takaa	k1gFnSc5
<g/>
,	,	kIx,
Toko	toka	k1gFnSc5
a	a	k8xC
polovina	polovina	k1gFnSc1
Kanoja	Kanoj	k1gInSc2
kókútai	kókúta	k1gFnSc2
(	(	kIx(
<g/>
proti	proti	k7c3
Filipínám	Filipíny	k1gFnPc3
byla	být	k5eAaImAgFnS
nasazena	nasadit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
polovina	polovina	k1gFnSc1
Kanoja	Kanoj	k1gInSc2
kókútai	kókútai	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
zbytek	zbytek	k1gInSc1
jednotky	jednotka	k1gFnSc2
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
na	na	k7c6
základně	základna	k1gFnSc6
Thu	Thu	k1gMnSc1
Dai	Dai	k1gMnSc1
Moi	Moi	k1gMnSc1
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
kókútai	kókúta	k1gFnPc1
disponovaly	disponovat	k5eAaBmAgFnP
celkem	celkem	k6eAd1
90	#num#	k4
A6M2	A6M2	k1gMnPc2
„	„	k?
<g/>
Zero	Zero	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
24	#num#	k4
A5M	A5M	k1gFnSc1
„	„	k?
<g/>
Claude	Claud	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
81	#num#	k4
G4M1	G4M1	k1gFnSc1
Model	modla	k1gFnPc2
11	#num#	k4
„	„	k?
<g/>
Betty	Betty	k1gFnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
36	#num#	k4
G3M	G3M	k1gMnPc2
„	„	k?
<g/>
Nell	Nellum	k1gNnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
24	#num#	k4
létajícími	létající	k2eAgInPc7d1
čluny	člun	k1gInPc7
H6K	H6K	k1gFnSc2
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Mavis	Mavis	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
12	#num#	k4
navigačními	navigační	k2eAgInPc7d1
C5M	C5M	k1gFnPc6
„	„	k?
<g/>
Babs	Babs	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Letecké	letecký	k2eAgFnPc1d1
síly	síla	k1gFnPc1
námořnictva	námořnictvo	k1gNnSc2
doplňovalo	doplňovat	k5eAaImAgNnS
ještě	ještě	k6eAd1
16	#num#	k4
stíhacích	stíhací	k2eAgFnPc2d1
A5M4	A5M4	k1gFnPc2
„	„	k?
<g/>
Claude	Claud	k1gInSc5
<g/>
“	“	k?
a	a	k8xC
18	#num#	k4
bombardovacích	bombardovací	k2eAgFnPc2d1
B5N1	B5N1	k1gFnPc2
„	„	k?
<g/>
Kate	kat	k1gInSc5
<g/>
“	“	k?
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
pozorovacích	pozorovací	k2eAgInPc2d1
a	a	k8xC
průzkumných	průzkumný	k2eAgInPc2d1
hydroplánů	hydroplán	k1gInPc2
F1M	F1M	k1gFnSc2
„	„	k?
<g/>
Pete	Pet	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
E8N	E8N	k1gFnSc1
„	„	k?
<g/>
Dave	Dav	k1gInSc5
<g/>
“	“	k?
a	a	k8xC
E13A	E13A	k1gMnSc1
„	„	k?
<g/>
Jake	Jake	k1gInSc1
<g/>
“	“	k?
na	na	k7c6
mateřských	mateřský	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
pro	pro	k7c4
hydroplány	hydroplán	k1gInPc4
a	a	k8xC
křižnících	křižník	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američané	Američan	k1gMnPc1
se	se	k3xPyFc4
rovněž	rovněž	k9
obávali	obávat	k5eAaImAgMnP
„	„	k?
<g/>
páté	pátá	k1gFnPc4
kolony	kolona	k1gFnSc2
<g/>
“	“	k?
v	v	k7c6
podobě	podoba	k1gFnSc6
30	#num#	k4
000	#num#	k4
Japonců	Japonec	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
nejvíce	nejvíce	k6eAd1,k6eAd3
jich	on	k3xPp3gMnPc2
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Davao	Davao	k6eAd1
na	na	k7c6
Mindanau	Mindanaus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
války	válka	k1gFnSc2
byli	být	k5eAaImAgMnP
odvlečeni	odvléct	k5eAaPmNgMnP
do	do	k7c2
zajateckých	zajatecký	k2eAgInPc2d1
táborů	tábor	k1gInPc2
a	a	k8xC
někteří	některý	k3yIgMnPc1
zabiti	zabít	k5eAaPmNgMnP
ustupujícími	ustupující	k2eAgMnPc7d1
obránci	obránce	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obrana	obrana	k1gFnSc1
</s>
<s>
Plány	plán	k1gInPc1
obrany	obrana	k1gFnSc2
<g/>
:	:	kIx,
ORANGE-3	ORANGE-3	k1gFnSc1
a	a	k8xC
RAINBOW	RAINBOW	kA
5	#num#	k4
</s>
<s>
Zbytky	zbytek	k1gInPc1
baterie	baterie	k1gFnSc2
Cheney	Chenea	k1gFnSc2
na	na	k7c6
západě	západ	k1gInSc6
Corregidoru	Corregidor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baterie	baterie	k1gFnSc1
střežila	střežit	k5eAaImAgFnS
vjezd	vjezd	k1gInSc4
do	do	k7c2
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
svými	svůj	k3xOyFgMnPc7
dvěma	dva	k4xCgFnPc7
šestipalcovými	šestipalcový	k2eAgFnPc7d1
(	(	kIx(
<g/>
152,4	152,4	k4
mm	mm	kA
<g/>
)	)	kIx)
děly	dít	k5eAaBmAgFnP,k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Jižně	jižně	k6eAd1
od	od	k7c2
Corregidoru	Corregidor	k1gInSc2
střežila	střežit	k5eAaImAgFnS
vjezd	vjezd	k1gInSc4
do	do	k7c2
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
Fort	Fort	k?
Drum	Drum	k1gMnSc1
–	–	k?
„	„	k?
<g/>
betonová	betonový	k2eAgFnSc1d1
bitevní	bitevní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
“	“	k?
vybavená	vybavený	k2eAgFnSc1d1
čtyřmi	čtyři	k4xCgNnPc7
čtrnáctipalcovými	čtrnáctipalcový	k2eAgFnPc7d1
(	(	kIx(
<g/>
355,6	355,6	k4
mm	mm	kA
<g/>
)	)	kIx)
děly	dít	k5eAaImAgFnP,k5eAaBmAgFnP
ve	v	k7c6
dvou	dva	k4xCgFnPc6
dvouhlavňových	dvouhlavňový	k2eAgFnPc6d1
věžích	věž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Aktuálním	aktuální	k2eAgInSc7d1
plánem	plán	k1gInSc7
pro	pro	k7c4
obranu	obrana	k1gFnSc4
Filipín	Filipíny	k1gFnPc2
byl	být	k5eAaImAgInS
válečný	válečný	k2eAgInSc1d1
plán	plán	k1gInSc1
ORANGE-3	ORANGE-3	k1gFnSc2
z	z	k7c2
dubna	duben	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vycházel	vycházet	k5eAaImAgInS
ze	z	k7c2
společného	společný	k2eAgInSc2d1
plánu	plán	k1gInSc2
armády	armáda	k1gFnSc2
a	a	k8xC
námořnictva	námořnictvo	k1gNnSc2
ORANGE	ORANGE	kA
z	z	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plán	plán	k1gInSc1
počítal	počítat	k5eAaImAgInS
s	s	k7c7
překvapivým	překvapivý	k2eAgInSc7d1
útokem	útok	k1gInSc7
Japonska	Japonsko	k1gNnSc2
bez	bez	k7c2
vyhlášení	vyhlášení	k1gNnSc2
války	válka	k1gFnSc2
a	a	k8xC
s	s	k7c7
invazí	invaze	k1gFnSc7
přibližně	přibližně	k6eAd1
100	#num#	k4
000	#num#	k4
japonských	japonský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nP
dobýt	dobýt	k5eAaPmF
Manilu	Manila	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládalo	předpokládat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
varování	varování	k1gNnSc1
přijde	přijít	k5eAaPmIp3nS
(	(	kIx(
<g/>
pokud	pokud	k8xS
vůbec	vůbec	k9
<g/>
)	)	kIx)
nejvýše	nejvýše	k6eAd1,k6eAd3
48	#num#	k4
hodin	hodina	k1gFnPc2
předem	předem	k6eAd1
a	a	k8xC
Filipínská	filipínský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
spolehnout	spolehnout	k5eAaPmF
pouze	pouze	k6eAd1
sama	sám	k3xTgFnSc1
na	na	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
z	z	k7c2
vnějšího	vnější	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejlepším	dobrý	k2eAgNnSc7d3
obdobím	období	k1gNnSc7
k	k	k7c3
útoku	útok	k1gInSc3
bylo	být	k5eAaImAgNnS
vyhodnoceno	vyhodnocen	k2eAgNnSc1d1
období	období	k1gNnSc1
sucha	sucho	k1gNnSc2
<g/>
,	,	kIx,
nejpravděpodobněji	pravděpodobně	k6eAd3
v	v	k7c6
prosinci	prosinec	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
rovněž	rovněž	k9
vcelku	vcelku	k6eAd1
správně	správně	k6eAd1
předpokládali	předpokládat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
hlavní	hlavní	k2eAgInSc1d1
útok	útok	k1gInSc1
bude	být	k5eAaImBp3nS
veden	vést	k5eAaImNgInS
na	na	k7c4
Luzon	Luzon	k1gNnSc4
a	a	k8xC
bude	být	k5eAaImBp3nS
proveden	provést	k5eAaPmNgInS
sérií	série	k1gFnSc7
několika	několik	k4yIc3
vylodění	vylodění	k1gNnPc2
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
kvůli	kvůli	k7c3
rozptýlení	rozptýlení	k1gNnSc3
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
předpokládán	předpokládán	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
úder	úder	k1gInSc1
bez	bez	k7c2
varování	varování	k1gNnSc2
a	a	k8xC
snaha	snaha	k1gFnSc1
Japonců	Japonec	k1gMnPc2
zajistit	zajistit	k5eAaPmF
co	co	k9
nejdříve	dříve	k6eAd3
předsunutá	předsunutý	k2eAgNnPc1d1
letiště	letiště	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
těmto	tento	k3xDgFnPc3
všem	všecek	k3xTgInPc3
předpokladům	předpoklad	k1gInPc3
se	se	k3xPyFc4
ani	ani	k9
nepočítalo	počítat	k5eNaImAgNnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
odrazit	odrazit	k5eAaPmF
japonský	japonský	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obránci	obránce	k1gMnPc1
se	se	k3xPyFc4
měli	mít	k5eAaImAgMnP
stáhnout	stáhnout	k5eAaPmF
na	na	k7c4
poloostrov	poloostrov	k1gInSc4
Bataan	Bataany	k1gInPc2
a	a	k8xC
ostrov	ostrov	k1gInSc4
Corregidor	Corregidora	k1gFnPc2
a	a	k8xC
tam	tam	k6eAd1
klást	klást	k5eAaImF
odpor	odpor	k1gInSc4
po	po	k7c4
dobu	doba	k1gFnSc4
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
blokovat	blokovat	k5eAaImF
Manilskou	manilský	k2eAgFnSc4d1
zátoku	zátoka	k1gFnSc4
a	a	k8xC
znemožnit	znemožnit	k5eAaPmF
tak	tak	k6eAd1
využití	využití	k1gNnSc4
Manily	Manila	k1gFnSc2
jako	jako	k8xS,k8xC
námořní	námořní	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ORANGE-3	ORANGE-3	k4
ale	ale	k8xC
již	již	k6eAd1
neříkal	říkat	k5eNaImAgMnS
nic	nic	k3yNnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
stát	stát	k5eAaImF,k5eAaPmF
po	po	k7c6
šesti	šest	k4xCc6
měsících	měsíc	k1gInPc6
<g/>
,	,	kIx,
až	až	k6eAd1
se	se	k3xPyFc4
obrana	obrana	k1gFnSc1
Bataanu	Bataan	k1gInSc2
zhroutí	zhroutit	k5eAaPmIp3nS
<g/>
,	,	kIx,
takže	takže	k8xS
neexistoval	existovat	k5eNaImAgInS
žádný	žádný	k3yNgInSc1
plán	plán	k1gInSc1
na	na	k7c4
evakuaci	evakuace	k1gFnSc4
Bataanu	Bataan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejspíš	nejspíš	k9
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
Tichomořské	tichomořský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
z	z	k7c2
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
svede	svést	k5eAaPmIp3nS
rozhodující	rozhodující	k2eAgFnSc4d1
vítěznou	vítězný	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
s	s	k7c7
japonským	japonský	k2eAgNnSc7d1
loďstvem	loďstvo	k1gNnSc7
a	a	k8xC
obnoví	obnovit	k5eAaPmIp3nS
se	se	k3xPyFc4
přísun	přísun	k1gInSc1
zásob	zásoba	k1gFnPc2
a	a	k8xC
posil	posila	k1gFnPc2
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
bude	být	k5eAaImBp3nS
možné	možný	k2eAgNnSc1d1
přejít	přejít	k5eAaPmF
do	do	k7c2
protiútoku	protiútok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nic	nic	k3yNnSc1
takového	takový	k3xDgNnSc2
se	se	k3xPyFc4
ale	ale	k9
nestalo	stát	k5eNaPmAgNnS
a	a	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
tomuto	tento	k3xDgInSc3
scénáři	scénář	k1gInSc3
nikdo	nikdo	k3yNnSc1
z	z	k7c2
velitelů	velitel	k1gMnPc2
v	v	k7c6
dubnu	duben	k1gInSc6
1941	#num#	k4
nevěřil	věřit	k5eNaImAgMnS
a	a	k8xC
námořní	námořní	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
byli	být	k5eAaImAgMnP
přesvědčeni	přesvědčit	k5eAaPmNgMnP
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
trvat	trvat	k5eAaImF
nejméně	málo	k6eAd3
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
loďstvo	loďstvo	k1gNnSc1
probojuje	probojovat	k5eAaPmIp3nS
k	k	k7c3
Filipínám	Filipíny	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Filipínská	filipínský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
tak	tak	k8xC,k8xS
de	de	k?
facto	facto	k1gNnSc1
byla	být	k5eAaImAgFnS
odsouzena	odsoudit	k5eAaPmNgFnS,k5eAaImNgFnS
k	k	k7c3
záhubě	záhuba	k1gFnSc3
v	v	k7c6
samotném	samotný	k2eAgInSc6d1
počátku	počátek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
1941	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
MacArthur	MacArthur	k1gMnSc1
nový	nový	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
plán	plán	k1gInSc1
RAINBOW	RAINBOW	kA
5	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
již	již	k6eAd1
rovnou	rovnou	k6eAd1
počítal	počítat	k5eAaImAgInS
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
Filipín	Filipíny	k1gFnPc2
a	a	k8xC
mandátních	mandátní	k2eAgNnPc2d1
území	území	k1gNnPc2
Guam	Gua	k1gNnSc7
a	a	k8xC
Wake	Wake	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prioritou	priorita	k1gFnSc7
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
válka	válka	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
se	se	k3xPyFc4
počítalo	počítat	k5eAaImAgNnS
jenom	jenom	k9
s	s	k7c7
obranou	obrana	k1gFnSc7
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
ale	ale	k9
MacArthur	MacArthur	k1gMnSc1
nesouhlasil	souhlasit	k5eNaImAgMnS
a	a	k8xC
dožadoval	dožadovat	k5eAaImAgMnS
se	se	k3xPyFc4
revize	revize	k1gFnSc2
válečných	válečná	k1gFnPc2
plánů	plán	k1gInPc2
a	a	k8xC
generál	generál	k1gMnSc1
George	Georg	k1gMnSc2
C.	C.	kA
Marshall	Marshall	k1gInSc1
mu	on	k3xPp3gMnSc3
počátkem	počátkem	k7c2
listopadu	listopad	k1gInSc2
vyhověl	vyhovět	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obráncům	obránce	k1gMnPc3
Filipín	Filipíny	k1gFnPc2
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
poskytnuto	poskytnout	k5eAaPmNgNnS
vše	všechen	k3xTgNnSc1
potřebné	potřebné	k1gNnSc1
k	k	k7c3
obraně	obrana	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
MacArthur	MacArthur	k1gMnSc1
měl	mít	k5eAaImAgMnS
v	v	k7c6
plánu	plán	k1gInSc6
vést	vést	k5eAaImF
aktivní	aktivní	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
a	a	k8xC
zejména	zejména	k9
s	s	k7c7
pomocí	pomoc	k1gFnSc7
silného	silný	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
napadat	napadat	k5eAaPmF,k5eAaImF,k5eAaBmF
zázemí	zázemí	k1gNnSc4
postupujících	postupující	k2eAgMnPc2d1
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
ani	ani	k8xC
MacArthur	MacArthur	k1gMnSc1
nepočítal	počítat	k5eNaImAgMnS
s	s	k7c7
obranou	obrana	k1gFnSc7
delší	dlouhý	k2eAgFnSc7d2
než	než	k8xS
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USAFFE	USAFFE	kA
</s>
<s>
Vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
USAFFE	USAFFE	kA
generál	generál	k1gMnSc1
Douglas	Douglas	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
velitel	velitel	k1gMnSc1
Filipínské	filipínský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
generálmajor	generálmajor	k1gMnSc1
Jonathan	Jonathan	k1gMnSc1
M.	M.	kA
Wainwright	Wainwright	k1gMnSc1
</s>
<s>
Velitel	velitel	k1gMnSc1
Přístavní	přístavní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
generálmajor	generálmajor	k1gMnSc1
George	Georg	k1gMnSc2
F.	F.	kA
Moore	Moor	k1gInSc5
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
začaly	začít	k5eAaPmAgFnP
USA	USA	kA
–	–	k?
pod	pod	k7c7
vlivem	vliv	k1gInSc7
stupňujícího	stupňující	k2eAgMnSc2d1
se	se	k3xPyFc4
mezinárodního	mezinárodní	k2eAgNnSc2d1
napětí	napětí	k1gNnSc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
–	–	k?
pomalu	pomalu	k6eAd1
posilovat	posilovat	k5eAaImF
obranu	obrana	k1gFnSc4
Filipín	Filipíny	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
svěřena	svěřit	k5eAaPmNgFnS
americké	americký	k2eAgFnSc3d1
dálněvýchodní	dálněvýchodní	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
(	(	kIx(
<g/>
US	US	kA
Army	Arma	k1gFnPc1
Forces	Forces	k1gMnSc1
in	in	k?
the	the	k?
Far	fara	k1gFnPc2
East	East	k1gMnSc1
~	~	kIx~
USAFFE	USAFFE	kA
<g/>
)	)	kIx)
generála	generál	k1gMnSc2
Douglas	Douglas	k1gMnSc1
MacArthura	MacArthura	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
létě	léto	k1gNnSc6
1941	#num#	k4
opět	opět	k6eAd1
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Samotná	samotný	k2eAgFnSc1d1
USAFFE	USAFFE	kA
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
ministerstvem	ministerstvo	k1gNnSc7
války	válka	k1gFnSc2
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1941	#num#	k4
a	a	k8xC
skládala	skládat	k5eAaImAgFnS
se	se	k3xPyFc4
z	z	k7c2
Filipínského	filipínský	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
(	(	kIx(
<g/>
Philippine	Philippin	k1gInSc5
Department	department	k1gInSc1
<g/>
)	)	kIx)
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
filipínského	filipínský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
složky	složka	k1gFnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
poprvé	poprvé	k6eAd1
dostaly	dostat	k5eAaPmAgFnP
pod	pod	k7c4
jedno	jeden	k4xCgNnSc4
velení	velení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
21	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc6
1941	#num#	k4
–	–	k?
tedy	tedy	k9
před	před	k7c7
zařazením	zařazení	k1gNnSc7
do	do	k7c2
USAFFE	USAFFE	kA
–	–	k?
měl	mít	k5eAaImAgMnS
Filipínský	filipínský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
22	#num#	k4
532	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
ale	ale	k8xC
pouze	pouze	k6eAd1
10	#num#	k4
560	#num#	k4
byli	být	k5eAaImAgMnP
Američané	Američan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývajících	zbývající	k2eAgInPc2d1
11	#num#	k4
972	#num#	k4
mužů	muž	k1gMnPc2
tvořili	tvořit	k5eAaImAgMnP
Filipínští	filipínský	k2eAgMnPc1d1
skauti	skaut	k1gMnPc1
(	(	kIx(
<g/>
Philippine	Philippin	k1gInSc5
Scouts	Scouts	k1gInSc1
~	~	kIx~
PS	PS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
navýšení	navýšení	k1gNnSc1
na	na	k7c4
12	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
schválil	schválit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Roosevelt	Roosevelt	k1gMnSc1
v	v	k7c6
lednu	leden	k1gInSc6
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
síla	síla	k1gFnSc1
pouhých	pouhý	k2eAgMnPc2d1
6500	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MacArthur	MacArthur	k1gMnSc1
doporučil	doporučit	k5eAaPmAgMnS
nahradit	nahradit	k5eAaPmF
dosavadního	dosavadní	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
Filipínského	filipínský	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
generálmajora	generálmajor	k1gMnSc2
George	Georg	k1gMnSc2
Grunerta	Grunert	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
před	před	k7c7
odchodem	odchod	k1gInSc7
ze	z	k7c2
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
války	válka	k1gFnSc2
proto	proto	k8xC
Grunerta	Grunert	k1gMnSc4
odvolalo	odvolat	k5eAaPmAgNnS
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
místo	místo	k1gNnSc1
jmenovalo	jmenovat	k5eAaImAgNnS,k5eAaBmAgNnS
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
MacArthura	MacArthura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgFnSc7d3
vojenskou	vojenský	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
byla	být	k5eAaImAgFnS
Filipínské	filipínský	k2eAgFnPc4d1
divize	divize	k1gFnPc4
generálmajora	generálmajor	k1gMnSc2
Jonathan	Jonathan	k1gMnSc1
M.	M.	kA
Wainwrighta	Wainwrighta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
divizí	divize	k1gFnPc2
byla	být	k5eAaImAgNnP
pouze	pouze	k6eAd1
podle	podle	k7c2
jména	jméno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chybělo	chybět	k5eAaImAgNnS
vybavení	vybavení	k1gNnSc1
a	a	k8xC
ani	ani	k8xC
organizace	organizace	k1gFnSc1
neodpovídala	odpovídat	k5eNaImAgFnS
běžné	běžný	k2eAgFnSc3d1
divizi	divize	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
i	i	k9
31	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
jediným	jediný	k2eAgInSc7d1
pěším	pěší	k2eAgInSc7d1
plukem	pluk	k1gInSc7
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
složeným	složený	k2eAgInPc3d1
výhradně	výhradně	k6eAd1
z	z	k7c2
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
31	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
příslušníků	příslušník	k1gMnPc2
štábu	štáb	k1gInSc2
a	a	k8xC
několika	několik	k4yIc2
vojenských	vojenský	k2eAgMnPc2d1
policistů	policista	k1gMnPc2
byl	být	k5eAaImAgInS
zbytek	zbytek	k1gInSc4
příslušníků	příslušník	k1gMnPc2
Filipínské	filipínský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
tvořen	tvořit	k5eAaImNgInS
Filipínskými	filipínský	k2eAgMnPc7d1
skauty	skaut	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
byli	být	k5eAaImAgMnP
organizováni	organizovat	k5eAaBmNgMnP
v	v	k7c4
45	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
57	#num#	k4
<g/>
.	.	kIx.
pěším	pěší	k2eAgInSc6d1
pluku	pluk	k1gInSc6
filipínských	filipínský	k2eAgMnPc2d1
skautů	skaut	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělostřelectvo	dělostřelectvo	k1gNnSc1
Filipínské	filipínský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
představovaly	představovat	k5eAaImAgFnP
23	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc4
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
vyzbrojené	vyzbrojený	k2eAgFnSc2d1
75	#num#	k4
<g/>
mm	mm	kA
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vytvoření	vytvoření	k1gNnSc3
třetího	třetí	k4xOgInSc2
pluku	pluk	k1gInSc2
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
praporu	prapor	k1gInSc2
155	#num#	k4
<g/>
mm	mm	kA
houfnic	houfnice	k1gFnPc2
již	již	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc6
1941	#num#	k4
měla	mít	k5eAaImAgFnS
divize	divize	k1gFnSc1
10	#num#	k4
233	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
Luzonu	Luzona	k1gFnSc4
v	v	k7c6
Manile	Manila	k1gFnSc6
a	a	k8xC
nejbližším	blízký	k2eAgNnSc6d3
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
Filipínské	filipínský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
červenci	červenec	k1gInSc6
1941	#num#	k4
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
nacházely	nacházet	k5eAaImAgFnP
další	další	k2eAgFnPc1d1
menší	malý	k2eAgFnPc1d2
jednotky	jednotka	k1gFnPc1
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
5360	#num#	k4
mužů	muž	k1gMnPc2
Přístavní	přístavní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
Harbor	Harbor	k1gMnSc1
Defenses	Defenses	k1gMnSc1
<g/>
)	)	kIx)
generálmajora	generálmajor	k1gMnSc4
George	Georg	k1gMnSc4
F.	F.	kA
Moora	Moor	k1gMnSc4
střežilo	střežit	k5eAaImAgNnS
vjezdy	vjezd	k1gInPc4
do	do	k7c2
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
(	(	kIx(
<g/>
Corregidor	Corregidor	k1gInSc1
a	a	k8xC
pevnosti	pevnost	k1gFnSc3
Hughes	Hughesa	k1gFnPc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Caballo	Caballo	k1gNnSc1
<g/>
,	,	kIx,
Fort	Fort	k?
Drum	Drum	k1gInSc4
na	na	k7c4
El	Ela	k1gFnPc2
Fraile	Fraila	k1gFnSc3
a	a	k8xC
Fort	Fort	k?
Frank	Frank	k1gMnSc1
na	na	k7c4
Carabao	Carabao	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
Subické	Subický	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
(	(	kIx(
<g/>
Fort	Fort	k?
Wint	Wint	k1gMnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Grande	grand	k1gMnSc5
<g/>
)	)	kIx)
na	na	k7c6
západě	západ	k1gInSc6
Bataanu	Bataan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
tu	tu	k6eAd1
byly	být	k5eAaImAgFnP
26	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
kavalerie	kavalerie	k1gFnSc2
PS	PS	kA
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
pluky	pluk	k1gInPc1
(	(	kIx(
<g/>
86	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
88	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
PS	PS	kA
a	a	k8xC
zásobovací	zásobovací	k2eAgFnSc2d1
<g/>
,	,	kIx,
signální	signální	k2eAgFnSc2d1
a	a	k8xC
policejní	policejní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MacArthur	MacArthur	k1gMnSc1
volal	volat	k5eAaImAgMnS
po	po	k7c6
posilách	posila	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
letech	léto	k1gNnPc6
izolacionismu	izolacionismus	k1gInSc2
<g/>
,	,	kIx,
omezování	omezování	k1gNnSc1
výdajů	výdaj	k1gInPc2
na	na	k7c4
armádu	armáda	k1gFnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
i	i	k9
produkci	produkce	k1gFnSc4
určené	určený	k2eAgInPc4d1
pro	pro	k7c4
armády	armáda	k1gFnPc4
spojenců	spojenec	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
zákona	zákon	k1gInSc2
o	o	k7c6
půjčce	půjčka	k1gFnSc6
a	a	k8xC
pronájmu	pronájem	k1gInSc6
nebylo	být	k5eNaImAgNnS
kde	kde	k6eAd1
brát	brát	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Proto	proto	k8xC
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
aspoň	aspoň	k9
něco	něco	k3yInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1941	#num#	k4
dorazily	dorazit	k5eAaPmAgInP
ze	z	k7c2
Států	stát	k1gInPc2
200	#num#	k4
<g/>
.	.	kIx.
pobřežní	pobřežní	k2eAgInSc1d1
dělostřelecký	dělostřelecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
s	s	k7c7
dvanácti	dvanáct	k4xCc7
76,2	76,2	k4
<g/>
mm	mm	kA
a	a	k8xC
dvaceti	dvacet	k4xCc7
čtyřmi	čtyři	k4xCgInPc7
37	#num#	k4
<g/>
mm	mm	kA
protiletadlovými	protiletadlový	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
a	a	k8xC
192	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
194	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
s	s	k7c7
celkem	celek	k1gInSc7
108	#num#	k4
lehkými	lehký	k2eAgInPc7d1
tanky	tank	k1gInPc7
M-3	M-3	k1gFnSc2
Stuart	Stuarta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dorazilo	dorazit	k5eAaPmAgNnS
také	také	k6eAd1
deset	deset	k4xCc1
75	#num#	k4
<g/>
mm	mm	kA
houfnic	houfnice	k1gFnPc2
<g/>
,	,	kIx,
lehké	lehký	k2eAgInPc4d1
kulomety	kulomet	k1gInPc4
<g/>
,	,	kIx,
jeepy	jeep	k1gInPc4
<g/>
,	,	kIx,
ambulance	ambulance	k1gFnPc4
a	a	k8xC
další	další	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipíny	Filipíny	k1gFnPc4
dokonce	dokonce	k9
dostaly	dostat	k5eAaPmAgFnP
v	v	k7c6
zásobování	zásobování	k1gNnSc6
přednost	přednost	k1gFnSc4
před	před	k7c7
Havají	Havaj	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
stejnému	stejný	k2eAgNnSc3d1
datu	datum	k1gNnSc3
měl	mít	k5eAaImAgInS
Filipínský	filipínský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
31	#num#	k4
095	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
19	#num#	k4
116	#num#	k4
Američanů	Američan	k1gMnPc2
a	a	k8xC
11	#num#	k4
988	#num#	k4
Filipínských	filipínský	k2eAgMnPc2d1
skautů	skaut	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozmístění	rozmístění	k1gNnSc1
USAFFE	USAFFE	kA
</s>
<s>
Filipínští	filipínský	k2eAgMnPc1d1
skauti	skaut	k1gMnPc1
nacvičují	nacvičovat	k5eAaImIp3nP
ve	v	k7c6
Fort	Fort	k?
William	William	k1gInSc1
McKinley	McKinlea	k1gMnSc2
(	(	kIx(
<g/>
jižně	jižně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
<g/>
)	)	kIx)
střelbu	střelba	k1gFnSc4
z	z	k7c2
37	#num#	k4
<g/>
mm	mm	kA
protitankového	protitankový	k2eAgInSc2d1
kanónu	kanón	k1gInSc2
M3	M3	k1gFnSc2
</s>
<s>
MacArthur	MacArthur	k1gMnSc1
rozdělil	rozdělit	k5eAaPmAgMnS
USAFFE	USAFFE	kA
na	na	k7c4
čtyři	čtyři	k4xCgNnPc4
taktická	taktický	k2eAgNnPc4d1
velitelství	velitelství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severoluzonské	Severoluzonský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
(	(	kIx(
<g/>
North	North	k1gInSc1
Luzon	Luzon	k1gNnSc1
Force	force	k1gFnPc2
<g/>
)	)	kIx)
měly	mít	k5eAaImAgFnP
na	na	k7c4
starost	starost	k1gFnSc4
nejohroženější	ohrožený	k2eAgFnSc1d3
oblast	oblast	k1gFnSc1
Luzonu	Luzon	k1gInSc2
severně	severně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Bataanského	Bataanský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
velení	velení	k1gNnSc2
převzal	převzít	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
generálmajor	generálmajor	k1gMnSc1
Wainwright	Wainwright	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
mu	on	k3xPp3gMnSc3
podřízeny	podřízen	k2eAgFnPc4d1
11	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
31	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgFnSc2d1
divize	divize	k1gFnSc2
Filipínské	filipínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
FA	fa	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
Filipínských	filipínský	k2eAgMnPc2d1
skautů	skaut	k1gMnPc2
(	(	kIx(
<g/>
FS	FS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
prapor	prapor	k1gInSc1
45	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
FS	FS	kA
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
baterie	baterie	k1gFnPc4
155	#num#	k4
<g/>
mm	mm	kA
děl	dělo	k1gNnPc2
FS	FS	kA
a	a	k8xC
baterie	baterie	k1gFnSc1
74,9	74,9	k4
<g/>
mm	mm	kA
horských	horský	k2eAgInPc2d1
děl	dělo	k1gNnPc2
FS	FS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
mu	on	k3xPp3gMnSc3
podléhala	podléhat	k5eAaImAgFnS
i	i	k9
71	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
o	o	k7c6
jejím	její	k3xOp3gNnSc6
nasazení	nasazení	k1gNnSc6
mohlo	moct	k5eAaImAgNnS
rozhodnout	rozhodnout	k5eAaPmF
pouze	pouze	k6eAd1
USAFFE	USAFFE	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiholuzonské	Jiholuzonský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
(	(	kIx(
<g/>
South	South	k1gInSc1
Luzon	Luzon	k1gNnSc1
Force	force	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
podstatně	podstatně	k6eAd1
slabší	slabý	k2eAgInPc1d2
než	než	k8xS
Severoluzonské	Severoluzonský	k2eAgInPc1d1
<g/>
,	,	kIx,
měly	mít	k5eAaImAgInP
na	na	k7c6
starosti	starost	k1gFnSc6
obranu	obrana	k1gFnSc4
Luzonu	Luzon	k1gInSc2
jižně	jižně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
a	a	k8xC
velel	velet	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
brigádní	brigádní	k2eAgMnPc1d1
generál	generál	k1gMnSc1
George	Georg	k1gMnSc2
M.	M.	kA
Parker	Parker	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládaly	skládat	k5eAaImAgInP
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
divizí	divize	k1gFnPc2
(	(	kIx(
<g/>
41	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
51	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
FA	fa	kA
a	a	k8xC
86	#num#	k4
<g/>
.	.	kIx.
baterie	baterie	k1gFnSc1
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
FS	FS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obranu	obrana	k1gFnSc4
zbývajících	zbývající	k2eAgInPc2d1
filipínských	filipínský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
měly	mít	k5eAaImAgInP
na	na	k7c4
starosti	starost	k1gFnPc4
síly	síla	k1gFnSc2
Visayan	Visayana	k1gFnPc2
<g/>
–	–	k?
<g/>
Mindanao	Mindanao	k1gMnSc1
(	(	kIx(
<g/>
Visayan	Visayan	k1gMnSc1
<g/>
–	–	k?
<g/>
Mindanao	Mindanao	k1gMnSc1
Force	force	k1gFnSc2
<g/>
)	)	kIx)
pod	pod	k7c7
velením	velení	k1gNnSc7
brigádního	brigádní	k2eAgMnSc2d1
generála	generál	k1gMnSc2
William	William	k1gInSc4
F.	F.	kA
Sharpa	sharp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
velení	velení	k1gNnSc2
se	se	k3xPyFc4
skládaly	skládat	k5eAaImAgFnP
výhradně	výhradně	k6eAd1
z	z	k7c2
příslušníků	příslušník	k1gMnPc2
FA	fa	kA
organizovaných	organizovaný	k2eAgInPc2d1
do	do	k7c2
61	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
81	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
101	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
61	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
Panay	Panaa	k1gFnPc4
<g/>
,	,	kIx,
81	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
Cebu	Cebum	k1gNnSc6
a	a	k8xC
Negros	Negrosa	k1gFnPc2
a	a	k8xC
101	#num#	k4
<g/>
.	.	kIx.
měla	mít	k5eAaImAgFnS
bránit	bránit	k5eAaImF
Mindanao	Mindanao	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
těchto	tento	k3xDgFnPc2
divizí	divize	k1gFnPc2
byla	být	k5eAaImAgFnS
především	především	k9
ochrana	ochrana	k1gFnSc1
letišť	letiště	k1gNnPc2
a	a	k8xC
nepředpokládalo	předpokládat	k5eNaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
bránit	bránit	k5eAaImF
pobřeží	pobřeží	k1gNnSc4
před	před	k7c7
vyloděním	vylodění	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
obraně	obrana	k1gFnSc3
průlivů	průliv	k1gInPc2
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
ostrovy	ostrov	k1gInPc7
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
ze	z	k7c2
Států	stát	k1gInPc2
dodána	dodán	k2eAgFnSc1d1
155	#num#	k4
<g/>
mm	mm	kA
děla	dělo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
slíbena	slíbit	k5eAaPmNgFnS
MacArthurovi	MacArthur	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
dodávka	dodávka	k1gFnSc1
byla	být	k5eAaImAgFnS
naplánována	naplánovat	k5eAaBmNgFnS
na	na	k7c4
duben	duben	k1gInSc4
1942	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
už	už	k6eAd1
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtou	čtvrtá	k1gFnSc7
skupinou	skupina	k1gFnSc7
byly	být	k5eAaImAgFnP
rezervy	rezerva	k1gFnPc1
(	(	kIx(
<g/>
Reserve	Reserev	k1gFnPc1
Force	force	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
v	v	k7c6
Manile	Manila	k1gFnSc6
a	a	k8xC
nejbližším	blízký	k2eAgNnSc6d3
okolí	okolí	k1gNnSc6
severně	severně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podléhaly	podléhat	k5eAaImAgInP
přímo	přímo	k6eAd1
MacArthurovu	MacArthurův	k2eAgInSc3d1
štábu	štáb	k1gInSc3
a	a	k8xC
skládaly	skládat	k5eAaImAgInP
se	se	k3xPyFc4
z	z	k7c2
téměř	téměř	k6eAd1
celé	celý	k2eAgFnSc2d1
Filipínské	filipínský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
(	(	kIx(
<g/>
bez	bez	k7c2
jednoho	jeden	k4xCgInSc2
praporu	prapor	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
91	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
,	,	kIx,
86	#num#	k4
<g/>
.	.	kIx.
jednotky	jednotka	k1gFnSc2
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
FS	FS	kA
<g/>
,	,	kIx,
dálněvýchodního	dálněvýchodní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
dislokováno	dislokovat	k5eAaBmNgNnS
i	i	k9
jinde	jinde	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
velitelství	velitelství	k1gNnSc1
Filipínského	filipínský	k2eAgInSc2d1
departmentu	department	k1gInSc2
a	a	k8xC
FA	fa	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pátou	pátý	k4xOgFnSc4
skupinu	skupina	k1gFnSc4
tvořila	tvořit	k5eAaImAgFnS
Přístavní	přístavní	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
generálmajora	generálmajor	k1gMnSc2
Moora	Moor	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgNnSc1d1
armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
pro	pro	k7c4
dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
</s>
<s>
Generálmajor	generálmajor	k1gMnSc1
Lewis	Lewis	k1gFnSc2
H.	H.	kA
Brereton	Brereton	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1940	#num#	k4
velitelem	velitel	k1gMnSc7
amerických	americký	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
</s>
<s>
B-17C	B-17C	k4
Flying	Flying	k1gInSc1
Fortress	Fortress	k1gInSc4
představovaly	představovat	k5eAaImAgInP
spolu	spolu	k6eAd1
se	s	k7c7
stroji	stroj	k1gInPc7
ve	v	k7c6
verzi	verze	k1gFnSc6
D	D	kA
výzbroj	výzbroj	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
z	z	k7c2
pěti	pět	k4xCc2
BS	BS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
pozdějším	pozdní	k2eAgMnPc3d2
modelům	model	k1gInPc3
postrádaly	postrádat	k5eAaImAgFnP
zadní	zadní	k2eAgNnSc4d1
střeliště	střeliště	k1gNnSc4
a	a	k8xC
hřbetní	hřbetní	k2eAgFnSc2d1
a	a	k8xC
spodní	spodní	k2eAgFnSc2d1
střelecké	střelecký	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zastaralé	zastaralý	k2eAgFnPc1d1
B-10B	B-10B	k1gFnPc1
byly	být	k5eAaImAgFnP
předány	předat	k5eAaPmNgFnP
Filipínskému	filipínský	k2eAgMnSc3d1
letectvu	letectvo	k1gNnSc3
</s>
<s>
P-35A	P-35A	k4
byl	být	k5eAaImAgMnS
hlavním	hlavní	k2eAgMnSc7d1
stíhačem	stíhač	k1gMnSc7
FEAF	FEAF	kA
až	až	k6eAd1
do	do	k7c2
příchodu	příchod	k1gInSc2
modernějších	moderní	k2eAgFnPc2d2
P-40	P-40	k1gFnPc2
</s>
<s>
Dva	dva	k4xCgMnPc1
A-27	A-27	k1gMnPc1
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
</s>
<s>
Americké	americký	k2eAgNnSc1d1
armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
pro	pro	k7c4
dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
(	(	kIx(
<g/>
US	US	kA
Army	Arma	k1gFnPc1
Forces	Forces	k1gMnSc1
in	in	k?
the	the	k?
Far	fara	k1gFnPc2
East	East	k1gMnSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
~	~	kIx~
USAFFEAF	USAFFEAF	kA
<g/>
,	,	kIx,
též	též	k9
jenom	jenom	k9
Dálněvýchodní	dálněvýchodní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
Far	fara	k1gFnPc2
East	Easta	k1gFnPc2
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
~	~	kIx~
FEAF	FEAF	kA
<g/>
)	)	kIx)
dostalo	dostat	k5eAaPmAgNnS
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1940	#num#	k4
nového	nový	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
generálmajor	generálmajor	k1gMnSc1
Lewis	Lewis	k1gFnSc2
H.	H.	kA
Brereton	Brereton	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1941	#num#	k4
bylo	být	k5eAaImAgNnS
FEAF	FEAF	kA
podřízeno	podřídit	k5eAaPmNgNnS
MacArthurovi	MacArthurův	k2eAgMnPc1d1
a	a	k8xC
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
štábu	štáb	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
bylo	být	k5eAaImAgNnS
rovněž	rovněž	k9
dlouho	dlouho	k6eAd1
zanedbáváno	zanedbáván	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
počátkem	počátek	k1gInSc7
40	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
dělalo	dělat	k5eAaImAgNnS
vše	všechen	k3xTgNnSc1
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
posílení	posílení	k1gNnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
zabavování	zabavování	k1gNnSc2
strojů	stroj	k1gInPc2
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
jiná	jiné	k1gNnPc4
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
tak	tak	k6eAd1
byly	být	k5eAaImAgFnP
Filipíny	Filipíny	k1gFnPc1
posíleny	posílit	k5eAaPmNgFnP
o	o	k7c4
část	část	k1gFnSc4
ze	z	k7c2
60	#num#	k4
zabavených	zabavený	k2eAgInPc2d1
stíhacích	stíhací	k2eAgInPc2d1
Seversky	seversky	k6eAd1
EP-106	EP-106	k1gMnPc7
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
Švédsko	Švédsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
potom	potom	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
48	#num#	k4
P-35A	P-35A	k1gMnPc7
a	a	k8xC
prvními	první	k4xOgInPc7
modely	model	k1gInPc7
P-40	P-40	k1gFnSc2
tvořily	tvořit	k5eAaImAgFnP
páteř	páteř	k1gFnSc4
stíhacího	stíhací	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
FEAF	FEAF	kA
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
Filipínského	filipínský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
byla	být	k5eAaImAgFnS
ještě	ještě	k6eAd1
horší	zlý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
muselo	muset	k5eAaImAgNnS
spokojit	spokojit	k5eAaPmF
se	s	k7c7
zastaralými	zastaralý	k2eAgFnPc7d1
P-26A	P-26A	k1gFnPc7
Peashooter	Peashootrum	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
mu	on	k3xPp3gMnSc3
přenechalo	přenechat	k5eAaPmAgNnS
FEAF	FEAF	kA
po	po	k7c6
obdržení	obdržení	k1gNnSc6
P-	P-	k1gFnSc2
<g/>
35	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Situace	situace	k1gFnSc1
u	u	k7c2
bombardovacího	bombardovací	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
vypadala	vypadat	k5eAaImAgFnS,k5eAaPmAgFnS
obdobně	obdobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastaralé	zastaralý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
B-10B	B-10B	k1gFnSc1
byly	být	k5eAaImAgInP
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1940	#num#	k4
a	a	k8xC
1941	#num#	k4
nahrazeny	nahradit	k5eAaPmNgFnP
za	za	k7c7
jenom	jenom	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
málo	málo	k6eAd1
lepší	dobrý	k2eAgFnSc1d2
B-18A	B-18A	k1gFnSc1
Bolo	bola	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Vyřazené	vyřazený	k2eAgFnPc1d1
B-10B	B-10B	k1gFnPc1
pak	pak	k6eAd1
putovaly	putovat	k5eAaImAgFnP
k	k	k7c3
Filipínskému	filipínský	k2eAgNnSc3d1
letectvu	letectvo	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velitel	velitel	k1gMnSc1
USAAF	USAAF	kA
generálmajor	generálmajor	k1gMnSc1
Henry	Henry	k1gMnSc1
H.	H.	kA
Arnold	Arnold	k1gMnSc1
zdůraznil	zdůraznit	k5eAaPmAgMnS
v	v	k7c6
červenci	červenec	k1gInSc6
1941	#num#	k4
nutnost	nutnost	k1gFnSc4
posílit	posílit	k5eAaPmF
letectvo	letectvo	k1gNnSc4
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
skutečně	skutečně	k6eAd1
moderní	moderní	k2eAgInPc1d1
stroje	stroj	k1gInPc1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
představovala	představovat	k5eAaImAgFnS
dodávka	dodávka	k1gFnSc1
prvních	první	k4xOgInPc2
31	#num#	k4
kusů	kus	k1gInPc2
P-40B	P-40B	k1gFnSc1
Tomahawk	tomahawk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I.	I.	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
březnu	březen	k1gInSc6
1941	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
podle	podle	k7c2
Mortona	Morton	k1gMnSc2
k	k	k7c3
tomu	ten	k3xDgNnSc3
došlo	dojít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
srpnu	srpen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
dorazilo	dorazit	k5eAaPmAgNnS
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
celkem	celkem	k6eAd1
107	#num#	k4
P-40	P-40	k1gFnPc2
ve	v	k7c6
verzích	verze	k1gFnPc6
B	B	kA
a	a	k8xC
E	E	kA
a	a	k8xC
byly	být	k5eAaImAgFnP
přiděleny	přidělit	k5eAaPmNgInP
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
perutím	peruť	k1gFnPc3
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PG	PG	kA
(	(	kIx(
<g/>
Pursuit	Pursuit	k1gMnSc1
Group	Group	k1gMnSc1
~	~	kIx~
stíhací	stíhací	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
(	(	kIx(
<g/>
Pursuit	Pursuit	k1gMnSc1
Squadron	Squadron	k1gMnSc1
~	~	kIx~
stíhací	stíhací	k2eAgFnSc1d1
peruť	peruť	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c4
Iba	iba	k6eAd1
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
P-40	P-40	k4
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
jako	jako	k8xC,k8xS
přepadový	přepadový	k2eAgMnSc1d1
stíhač	stíhač	k1gMnSc1
a	a	k8xC
jako	jako	k8xC,k8xS
takový	takový	k3xDgMnSc1
se	se	k3xPyFc4
v	v	k7c6
manévrovém	manévrový	k2eAgInSc6d1
boji	boj	k1gInSc6
nemohl	moct	k5eNaImAgMnS
měřit	měřit	k5eAaImF
s	s	k7c7
japonskými	japonský	k2eAgFnPc7d1
stíhačkami	stíhačka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
poukazoval	poukazovat	k5eAaImAgMnS
Čankajškův	Čankajškův	k2eAgMnSc1d1
americký	americký	k2eAgMnSc1d1
letecký	letecký	k2eAgMnSc1d1
poradce	poradce	k1gMnSc1
plukovník	plukovník	k1gMnSc1
Claire	Clair	k1gInSc5
Chennault	Chennault	k2eAgInSc4d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
v	v	k7c6
souboji	souboj	k1gInSc6
s	s	k7c7
obratnějšími	obratní	k2eAgInPc7d2
japonskými	japonský	k2eAgInPc7d1
stroji	stroj	k1gInPc7
třeba	třeba	k6eAd1
použít	použít	k5eAaPmF
taktiku	taktika	k1gFnSc4
„	„	k?
<g/>
udeř	udeřit	k5eAaPmRp2nS
a	a	k8xC
uteč	utéct	k5eAaPmRp2nS
<g/>
“	“	k?
a	a	k8xC
využít	využít	k5eAaPmF
tak	tak	k6eAd1
vyšší	vysoký	k2eAgFnSc4d2
rychlost	rychlost	k1gFnSc4
P-	P-	k1gFnSc2
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
rady	rada	k1gFnPc1
ale	ale	k9
zůstaly	zůstat	k5eAaPmAgFnP
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
nevyslyšeny	vyslyšen	k2eNgInPc4d1
(	(	kIx(
<g/>
pokud	pokud	k8xS
se	se	k3xPyFc4
k	k	k7c3
pilotům	pilot	k1gInPc3
vůbec	vůbec	k9
dostaly	dostat	k5eAaPmAgInP
<g/>
)	)	kIx)
a	a	k8xC
piloti	pilot	k1gMnPc1
se	se	k3xPyFc4
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
P-40	P-40	k1gMnPc7
pouštěli	pouštět	k5eAaImAgMnP
do	do	k7c2
manévrového	manévrový	k2eAgInSc2d1
vzdušného	vzdušný	k2eAgInSc2d1
boje	boj	k1gInSc2
s	s	k7c7
mnohem	mnohem	k6eAd1
obratnějším	obratní	k2eAgMnSc7d2
protivníkem	protivník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
nedostatkem	nedostatek	k1gInSc7
bylo	být	k5eAaImAgNnS
používání	používání	k1gNnSc1
neohrabaných	neohrabaný	k2eAgFnPc2d1,k2eNgFnPc2d1
trojčlenných	trojčlenný	k2eAgFnPc2d1
formací	formace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
pak	pak	k6eAd1
hrálo	hrát	k5eAaImAgNnS
roli	role	k1gFnSc4
i	i	k8xC
hrubé	hrubý	k2eAgNnSc4d1
podcenění	podcenění	k1gNnSc4
letadel	letadlo	k1gNnPc2
a	a	k8xC
pilotů	pilot	k1gMnPc2
protivníka	protivník	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
–	–	k?
na	na	k7c6
základě	základ	k1gInSc6
článku	článek	k1gInSc2
JAPAN	japan	k1gInSc1
<g/>
'	'	kIx"
<g/>
S	s	k7c7
BUSH	Bush	k1gMnSc1
LEAGUE	LEAGUE	kA
AIR	AIR	kA
FORCE	force	k1gFnSc1
(	(	kIx(
<g/>
Letecká	letecký	k2eAgFnSc1d1
liga	liga	k1gFnSc1
japonských	japonský	k2eAgMnPc2d1
křováků	křovák	k1gMnPc2
<g/>
)	)	kIx)
v	v	k7c4
American	American	k1gInSc4
aviation	aviation	k1gInSc1
magazine	magazinout	k5eAaPmIp3nS
–	–	k?
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
neschopné	schopný	k2eNgMnPc4d1
a	a	k8xC
létající	létající	k2eAgMnPc4d1
na	na	k7c6
špatných	špatný	k2eAgFnPc6d1
kopiích	kopie	k1gFnPc6
západních	západní	k2eAgFnPc6d1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
to	ten	k3xDgNnSc4
větší	veliký	k2eAgInSc1d2
byl	být	k5eAaImAgInS
pro	pro	k7c4
Američany	Američan	k1gMnPc4
šok	šok	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
boji	boj	k1gInSc6
setkali	setkat	k5eAaPmAgMnP
se	s	k7c7
Zerem	Zer	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
piloti	pilot	k1gMnPc1
P-40	P-40	k1gFnSc2
později	pozdě	k6eAd2
přiznali	přiznat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
raději	rád	k6eAd2
vyčerpali	vyčerpat	k5eAaPmAgMnP
palivo	palivo	k1gNnSc4
někde	někde	k6eAd1
v	v	k7c6
bezpečí	bezpečí	k1gNnSc6
a	a	k8xC
poté	poté	k6eAd1
svůj	svůj	k3xOyFgInSc4
stroj	stroj	k1gInSc4
opustili	opustit	k5eAaPmAgMnP
na	na	k7c6
padáku	padák	k1gInSc6
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nS
se	se	k3xPyFc4
svojí	svůj	k3xOyFgFnSc7
P-40	P-40	k1gFnSc7
riskovali	riskovat	k5eAaBmAgMnP
střetnutí	střetnutí	k1gNnSc4
se	s	k7c7
Zerem	Zer	k1gInSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Situace	situace	k1gFnSc1
u	u	k7c2
bombardovacího	bombardovací	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
se	se	k3xPyFc4
zlepšila	zlepšit	k5eAaPmAgFnS
v	v	k7c6
září	září	k1gNnSc6
1941	#num#	k4
s	s	k7c7
dodávkou	dodávka	k1gFnSc7
prvních	první	k4xOgFnPc2
devíti	devět	k4xCc3
„	„	k?
<g/>
létajících	létající	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
<g/>
“	“	k?
B-17D	B-17D	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BS	BS	kA
(	(	kIx(
<g/>
Bombing	Bombing	k1gInSc1
Squadron	Squadron	k1gInSc1
~	~	kIx~
bombardovací	bombardovací	k2eAgFnSc1d1
peruť	peruť	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
dorazilo	dorazit	k5eAaPmAgNnS
celkem	celkem	k6eAd1
35	#num#	k4
strojů	stroj	k1gInPc2
ve	v	k7c6
verzích	verze	k1gFnPc6
C	C	kA
a	a	k8xC
D	D	kA
v	v	k7c6
celkem	celkem	k6eAd1
třech	tři	k4xCgFnPc6
vlnách	vlna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
vlna	vlna	k1gFnSc1
letěla	letět	k5eAaImAgFnS
z	z	k7c2
Pearl	Pearl	k1gInSc4
Harboru	Harbora	k1gFnSc4
přes	přes	k7c4
Midway	Midwaa	k1gFnPc4
na	na	k7c6
Wake	Wake	k1gFnSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
s	s	k7c7
narušením	narušení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
japonských	japonský	k2eAgNnPc2d1
mandátních	mandátní	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
centrálním	centrální	k2eAgInSc6d1
Pacifiku	Pacifik	k1gInSc6
přeletěla	přeletět	k5eAaPmAgFnS
do	do	k7c2
Port	porta	k1gFnPc2
Moresby	Moresba	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
přes	přes	k7c4
Darwin	Darwin	k1gMnSc1
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
B-17	B-17	k1gFnPc1
tvořily	tvořit	k5eAaImAgFnP
hlavní	hlavní	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BG	BG	kA
(	(	kIx(
<g/>
Bombing	Bombing	k1gInSc1
Group	Group	k1gInSc1
~	~	kIx~
bombardovací	bombardovací	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc1
čtyři	čtyři	k4xCgMnPc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
93	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
Del	Del	k1gFnSc6
Monte	Mont	k1gInSc5
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gInSc4
<g/>
)	)	kIx)
z	z	k7c2
pěti	pět	k4xCc3
perutí	peruť	k1gFnPc2
byly	být	k5eAaImAgInP
vybaveny	vybavit	k5eAaPmNgInP
právě	právě	k6eAd1
létajícími	létající	k2eAgFnPc7d1
pevnostmi	pevnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgInPc2d1
12	#num#	k4
strojů	stroj	k1gInPc2
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BG	BG	kA
bylo	být	k5eAaImAgNnS
během	během	k7c2
mezipřistání	mezipřistání	k1gNnSc2
v	v	k7c4
Pearl	Pearl	k1gInSc4
Harboru	Harbor	k1gInSc2
zaskočeno	zaskočit	k5eAaPmNgNnS
japonským	japonský	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
B-17	B-17	k1gFnSc1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
byla	být	k5eAaImAgFnS
ponechána	ponechán	k2eAgFnSc1d1
v	v	k7c6
barvě	barva	k1gFnSc6
kovu	kov	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
značný	značný	k2eAgInSc4d1
demaskující	demaskující	k2eAgInSc4d1
efekt	efekt	k1gInSc4
a	a	k8xC
pouze	pouze	k6eAd1
několik	několik	k4yIc4
z	z	k7c2
ních	nícha	k1gFnPc2
bylo	být	k5eAaImAgNnS
kamuflováno	kamuflovat	k5eAaImNgNnS
olivověšedohnědou	olivověšedohnědý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
(	(	kIx(
<g/>
Olive	Oliev	k1gFnSc2
Drab	Drab	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
horních	horní	k2eAgFnPc6d1
a	a	k8xC
bočních	boční	k2eAgFnPc6d1
plochách	plocha	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
52	#num#	k4
střemhlavých	střemhlavý	k2eAgMnPc2d1
bombardérů	bombardér	k1gMnPc2
A-24	A-24	k1gMnSc2
Banshee	Banshe	k1gMnSc2
pro	pro	k7c4
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BG	BG	kA
nebylo	být	k5eNaImAgNnS
nikdy	nikdy	k6eAd1
dodáno	dodat	k5eAaPmNgNnS
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
její	její	k3xOp3gInSc1
personál	personál	k1gInSc1
a	a	k8xC
piloti	pilot	k1gMnPc1
již	již	k6eAd1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
byli	být	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Situaci	situace	k1gFnSc4
(	(	kIx(
<g/>
nejenom	nejenom	k6eAd1
<g/>
)	)	kIx)
letectva	letectvo	k1gNnSc2
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
–	–	k?
zejména	zejména	k9
stíhačů	stíhač	k1gMnPc2
–	–	k?
nezlepšoval	zlepšovat	k5eNaImAgInS
ani	ani	k8xC
nedostatek	nedostatek	k1gInSc1
munice	munice	k1gFnSc2
pro	pro	k7c4
palubní	palubní	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
požadovaných	požadovaný	k2eAgInPc2d1
šesti	šest	k4xCc2
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
půlpalcové	půlpalcový	k2eAgFnSc2d1
munice	munice	k1gFnSc2
pro	pro	k7c4
palubní	palubní	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
letadel	letadlo	k1gNnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
13,3	13,3	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
pro	pro	k7c4
pozemní	pozemní	k2eAgFnPc4d1
instalace	instalace	k1gFnPc4
bylo	být	k5eAaImAgNnS
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
dodáno	dodat	k5eAaPmNgNnS
pouhých	pouhý	k2eAgInPc2d1
3	#num#	k4
800	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
musely	muset	k5eAaImAgInP
být	být	k5eAaImF
navíc	navíc	k6eAd1
do	do	k7c2
nábojových	nábojový	k2eAgInPc2d1
pásů	pás	k1gInPc2
zasouvány	zasouván	k2eAgInPc1d1
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
si	se	k3xPyFc3
piloti	pilot	k1gMnPc1
P-40	P-40	k1gFnSc4
mohli	moct	k5eAaImAgMnP
nechat	nechat	k5eAaPmF
o	o	k7c6
ostrých	ostrý	k2eAgFnPc6d1
střelbách	střelba	k1gFnPc6
jenom	jenom	k9
zdát	zdát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
rovněž	rovněž	k9
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
kulomety	kulomet	k1gInPc7
Browning	browning	k1gInSc1
velice	velice	k6eAd1
často	často	k6eAd1
zasekávají	zasekávat	k5eAaImIp3nP
a	a	k8xC
tak	tak	k9
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
americkým	americký	k2eAgMnSc7d1
stíhačům	stíhač	k1gMnPc3
přece	přece	k9
jen	jen	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
Zero	Zero	k6eAd1
(	(	kIx(
<g/>
či	či	k8xC
jiný	jiný	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
)	)	kIx)
do	do	k7c2
zaměřovače	zaměřovač	k1gInSc2
<g/>
,	,	kIx,
často	často	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
ze	z	k7c2
šesti	šest	k4xCc2
kulometů	kulomet	k1gInPc2
P-40E	P-40E	k1gFnSc2
ani	ani	k8xC
jeden	jeden	k4xCgMnSc1
nevystřelil	vystřelit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letiště	letiště	k1gNnSc1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
měla	mít	k5eAaImAgFnS
vesměs	vesměs	k6eAd1
nezpevněný	zpevněný	k2eNgInSc4d1
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
se	se	k3xPyFc4
stávala	stávat	k5eAaImAgFnS
nepoužitelnými	použitelný	k2eNgFnPc7d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
měnila	měnit	k5eAaImAgFnS
v	v	k7c4
bažiny	bažina	k1gFnPc4
a	a	k8xC
v	v	k7c6
období	období	k1gNnSc6
sucha	sucho	k1gNnSc2
zase	zase	k9
operace	operace	k1gFnSc1
znesnadňoval	znesnadňovat	k5eAaImAgInS
zvířený	zvířený	k2eAgInSc1d1
prach	prach	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
byl	být	k5eAaImAgInS
Clark	Clark	k1gInSc1
Field	Field	k1gInSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
9	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
120	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
měly	mít	k5eAaImAgFnP
základnu	základna	k1gFnSc4
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BS	BS	kA
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OS	osa	k1gFnPc2
(	(	kIx(
<g/>
Observation	Observation	k1gInSc1
Squadron	Squadron	k1gInSc1
~	~	kIx~
pozorovací	pozorovací	k2eAgFnSc1d1
peruť	peruť	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházel	nacházet	k5eAaImAgMnS
se	se	k3xPyFc4
asi	asi	k9
60	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
96,6	96,6	k4
km	km	kA
<g/>
)	)	kIx)
severně	severně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
a	a	k8xC
jižně	jižně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gNnSc2
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
Del	Del	k1gFnSc3
Carmen	Carmen	k1gInSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
120	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
měla	mít	k5eAaImAgFnS
základnu	základna	k1gFnSc4
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
s	s	k7c7
P-	P-	k1gFnSc7
<g/>
35	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Jižně	jižně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
ležel	ležet	k5eAaImAgInS
Nichols	Nichols	k1gInSc1
Field	Field	k1gInSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
121	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
měli	mít	k5eAaImAgMnP
základnu	základna	k1gFnSc4
stíhači	stíhač	k1gMnSc3
17	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Nielson	Nielson	k1gMnSc1
Field	Field	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
121	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
se	se	k3xPyFc4
pro	pro	k7c4
operační	operační	k2eAgNnSc4d1
létání	létání	k1gNnSc4
nepoužívalo	používat	k5eNaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
tu	tu	k6eAd1
umístěno	umístěn	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
dálněvýchodního	dálněvýchodní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Mindanaa	Mindanaum	k1gNnSc2
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
Del	Del	k1gMnSc5
Monte	Mont	k1gMnSc5
(	(	kIx(
<g/>
8	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
124	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
druhým	druhý	k4xOgNnSc7
a	a	k8xC
posledním	poslední	k2eAgNnSc7d1
letištěm	letiště	k1gNnSc7
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yQgMnSc2,k3yRgMnSc2,k3yIgMnSc2
mohly	moct	k5eAaImAgFnP
operovat	operovat	k5eAaImF
B-	B-	k1gFnSc7
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
těchto	tento	k3xDgNnPc2
letišť	letiště	k1gNnPc2
se	se	k3xPyFc4
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
nacházela	nacházet	k5eAaImAgFnS
řada	řada	k1gFnSc1
záložních	záložní	k2eAgFnPc2d1
a	a	k8xC
nouzových	nouzový	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
letiště	letiště	k1gNnPc4
ale	ale	k9
trpěla	trpět	k5eAaImAgFnS
nedostatkem	nedostatek	k1gInSc7
místa	místo	k1gNnSc2
pro	pro	k7c4
rozptýlení	rozptýlení	k1gNnSc4
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
letouny	letoun	k1gInPc1
se	se	k3xPyFc4
na	na	k7c6
zemi	zem	k1gFnSc6
stávaly	stávat	k5eAaImAgInP
snadným	snadný	k2eAgInSc7d1
terčem	terč	k1gInSc7
–	–	k?
jak	jak	k6eAd1
se	se	k3xPyFc4
později	pozdě	k6eAd2
potvrdilo	potvrdit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jediný	jediný	k2eAgInSc1d1
funkční	funkční	k2eAgInSc1d1
radar	radar	k1gInSc1
byl	být	k5eAaImAgInS
SCR-270B	SCR-270B	k1gFnSc4
v	v	k7c6
Iba	iba	k6eAd1
na	na	k7c6
západě	západ	k1gInSc6
Luzonu	Luzon	k1gInSc2
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Morton	Morton	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
radar	radar	k1gInSc1
u	u	k7c2
Manily	Manila	k1gFnSc2
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Včasné	včasný	k2eAgNnSc1d1
varování	varování	k1gNnSc1
proto	proto	k8xC
záviselo	záviset	k5eAaImAgNnS
hlavně	hlavně	k9
na	na	k7c6
síti	síť	k1gFnSc6
200	#num#	k4
pozemních	pozemní	k2eAgMnPc2d1
pozorovatelů	pozorovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
byl	být	k5eAaImAgInS
nedostatek	nedostatek	k1gInSc1
protiletadlových	protiletadlový	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
chránila	chránit	k5eAaImAgFnS
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celkem	celkem	k6eAd1
mělo	mít	k5eAaImAgNnS
americké	americký	k2eAgNnSc4d1
armádní	armádní	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
k	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1941	#num#	k4
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
5609	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
669	#num#	k4
důstojníků	důstojník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1
a	a	k8xC
námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
</s>
<s>
Velitel	velitel	k1gMnSc1
asijského	asijský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
Thomas	Thomas	k1gMnSc1
C.	C.	kA
Hart	Hart	k1gMnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
prezidentem	prezident	k1gMnSc7
Filipínského	filipínský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
Manuelem	Manuel	k1gMnSc7
Quezonem	Quezon	k1gMnSc7
a	a	k8xC
novinářka	novinářka	k1gFnSc1
Clare	Clar	k1gMnSc5
Boothe	Boothus	k1gMnSc5
Luce	Lucus	k1gMnSc5
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
1941	#num#	k4
</s>
<s>
Cavite	Cavit	k1gMnSc5
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
severního	severní	k2eAgNnSc2d1
mola	molo	k1gNnSc2
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zakotveno	zakotven	k2eAgNnSc1d1
šest	šest	k4xCc1
torpédových	torpédový	k2eAgInPc2d1
člunů	člun	k1gInPc2
PT	PT	kA
a	a	k8xC
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
bazénu	bazén	k1gInSc6
jsou	být	k5eAaImIp3nP
zakotveny	zakotven	k2eAgInPc1d1
zásobovací	zásobovací	k2eAgFnSc4d1
loď	loď	k1gFnSc4
ponorek	ponorka	k1gFnPc2
USS	USS	kA
Canopus	Canopus	k1gInSc1
a	a	k8xC
dva	dva	k4xCgInPc4
torpédoborce	torpédoborec	k1gInPc4
</s>
<s>
Asijské	asijský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
USA	USA	kA
mělo	mít	k5eAaImAgNnS
velitelství	velitelství	k1gNnSc1
v	v	k7c6
Manile	Manila	k1gFnSc6
a	a	k8xC
velitelství	velitelství	k1gNnSc2
16	#num#	k4
<g/>
.	.	kIx.
námořní	námořní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
(	(	kIx(
<g/>
Naval	navalit	k5eAaPmRp2nS
District	District	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
v	v	k7c6
Cavite	Cavit	k1gMnSc5
<g/>
,	,	kIx,
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelem	velitel	k1gMnSc7
asijského	asijský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
byl	být	k5eAaImAgMnS
admirál	admirál	k1gMnSc1
Thomas	Thomas	k1gMnSc1
C.	C.	kA
Hart	Hart	k1gMnSc1
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
velení	velení	k1gNnSc2
podléhaly	podléhat	k5eAaImAgInP
v	v	k7c4
předvečer	předvečer	k1gInSc4
vypuknutí	vypuknutí	k1gNnSc2
války	válka	k1gFnSc2
tyto	tento	k3xDgFnPc4
jednotky	jednotka	k1gFnPc4
<g/>
:	:	kIx,
těžký	těžký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
USS	USS	kA
Houston	Houston	k1gInSc1
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
USS	USS	kA
Marblehead	Marblehead	k1gInSc1
<g/>
,	,	kIx,
13	#num#	k4
čtyřkomínových	čtyřkomínový	k2eAgInPc2d1
torpédoborců	torpédoborec	k1gInPc2
třídy	třída	k1gFnSc2
Clemson	Clemsona	k1gFnPc2
a	a	k8xC
celkem	celkem	k6eAd1
29	#num#	k4
ponorek	ponorka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Ty	ten	k3xDgMnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
doplňovalo	doplňovat	k5eAaImAgNnS
sedm	sedm	k4xCc1
dělových	dělový	k2eAgInPc2d1
člunů	člun	k1gInPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
jachta	jachta	k1gFnSc1
USS	USS	kA
Isabel	Isabela	k1gFnPc2
<g/>
,	,	kIx,
šest	šest	k4xCc4
velkých	velký	k2eAgFnPc2d1
minolovek	minolovka	k1gFnPc2
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
tankery	tanker	k1gInPc1
(	(	kIx(
<g/>
USS	USS	kA
Pecos	Pecos	k1gInSc1
a	a	k8xC
USS	USS	kA
Trinity	Trinit	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
oceánský	oceánský	k2eAgInSc1d1
remorkér	remorkér	k1gInSc1
USS	USS	kA
Napa	napa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Ne	ne	k9
všechny	všechen	k3xTgFnPc1
jednotky	jednotka	k1gFnPc1
se	se	k3xPyFc4
ale	ale	k9
nacházely	nacházet	k5eAaImAgFnP
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Marblehead	Marblehead	k1gInSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
pěti	pět	k4xCc7
torpédoborci	torpédoborec	k1gInPc7
nacházel	nacházet	k5eAaImAgMnS
v	v	k7c6
Tarakanu	Tarakan	k1gInSc6
na	na	k7c6
Borneu	Borneo	k1gNnSc6
a	a	k8xC
další	další	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
torpédoborce	torpédoborec	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
Balikpapanu	Balikpapan	k1gInSc6
na	na	k7c6
Borneu	Borneo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Pět	pět	k4xCc1
ze	z	k7c2
sedmi	sedm	k4xCc2
dělových	dělový	k2eAgInPc2d1
člunů	člun	k1gInPc2
Asijského	asijský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
působících	působící	k2eAgMnPc2d1
v	v	k7c6
Číně	Čína	k1gFnSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
listopadu	listopad	k1gInSc6
převeleno	převelen	k2eAgNnSc1d1
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
USS	USS	kA
Asheville	Asheville	k1gNnSc1
<g/>
,	,	kIx,
USS	USS	kA
Tulsa	Tulsa	k1gFnSc1
<g/>
,	,	kIx,
USS	USS	kA
Oahu	Oaha	k1gFnSc4
<g/>
,	,	kIx,
USS	USS	kA
Luzon	Luzon	k1gInSc1
a	a	k8xC
USS	USS	kA
Mindanao	Mindanao	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgInSc4d1
USS	USS	kA
Wake	Wake	k1gInSc1
byl	být	k5eAaImAgInS
odstrojen	odstrojit	k5eAaPmNgInS
a	a	k8xC
ponechán	ponechat	k5eAaPmNgInS
v	v	k7c6
Číně	Čína	k1gFnSc6
k	k	k7c3
demolici	demolice	k1gFnSc3
a	a	k8xC
USS	USS	kA
Tutuila	Tutuila	k1gFnSc1
byla	být	k5eAaImAgFnS
předána	předán	k2eAgFnSc1d1
Číňanům	Číňan	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
se	se	k3xPyFc4
ve	v	k7c6
filipínských	filipínský	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
na	na	k7c6
Cebu	Cebus	k1gInSc6
nacházel	nacházet	k5eAaImAgMnS
lehký	lehký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
USS	USS	kA
Boise	Bois	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
Pacifickému	pacifický	k2eAgNnSc3d1
loďstvu	loďstvo	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
zajišťovalo	zajišťovat	k5eAaImAgNnS
celkem	celkem	k6eAd1
28	#num#	k4
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
létajících	létající	k2eAgInPc2d1
člunů	člun	k1gInPc2
PBY-4	PBY-4	k1gFnSc1
Catalina	Catalina	k1gFnSc1
od	od	k7c2
Patrol	Patrol	k?
Wing	Wing	k1gInSc1
10	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
organizovány	organizovat	k5eAaBmNgInP
do	do	k7c2
perutí	peruť	k1gFnPc2
VP-101	VP-101	k1gFnSc2
a	a	k8xC
VP-	VP-	k1gFnSc7
<g/>
102	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
jim	on	k3xPp3gMnPc3
zajišťovaly	zajišťovat	k5eAaImAgInP
čtyři	čtyři	k4xCgFnPc4
mateřské	mateřský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
pro	pro	k7c4
hydroplány	hydroplán	k1gInPc4
<g/>
:	:	kIx,
USS	USS	kA
Langley	Langlea	k1gFnSc2
<g/>
,	,	kIx,
USS	USS	kA
Childs	Childs	k1gInSc1
<g/>
,	,	kIx,
USS	USS	kA
William	William	k1gInSc1
B.	B.	kA
Preston	Preston	k1gInSc1
a	a	k8xC
USS	USS	kA
Heron	Heron	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1
vybudovalo	vybudovat	k5eAaPmAgNnS
novou	nový	k2eAgFnSc4d1
námořní	námořní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
v	v	k7c4
Mariveles	Mariveles	k1gInSc4
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
poloostrova	poloostrov	k1gInSc2
Bataan	Bataan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
základnu	základna	k1gFnSc4
Olongapo	Olongapa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
další	další	k2eAgFnSc7d1
námořní	námořní	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
Cavite	Cavit	k1gInSc5
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
Manilská	manilský	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
Subická	Subický	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
byly	být	k5eAaImAgFnP
do	do	k7c2
konce	konec	k1gInSc2
srpna	srpen	k1gInSc2
1941	#num#	k4
zaminovány	zaminovat	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedle	vedle	k7c2
ponorek	ponorka	k1gFnPc2
bylo	být	k5eAaImAgNnS
asijské	asijský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
posíleno	posílen	k2eAgNnSc1d1
šesti	šest	k4xCc7
torpédovými	torpédový	k2eAgInPc7d1
čluny	člun	k1gInPc7
PT	PT	kA
(	(	kIx(
<g/>
čluny	člun	k1gInPc1
PT-31	PT-31	k1gFnSc1
až	až	k9
-35	-35	k4
a	a	k8xC
PT-	PT-	k1gMnSc3
<g/>
41	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
zformována	zformován	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
eskadra	eskadra	k1gFnSc1
torpédových	torpédový	k2eAgInPc2d1
člunů	člun	k1gInPc2
se	s	k7c7
základnou	základna	k1gFnSc7
v	v	k7c6
Cavite	Cavit	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Zbývajících	zbývající	k2eAgInPc2d1
šest	šest	k4xCc1
objednaných	objednaný	k2eAgInPc2d1
člunů	člun	k1gInPc2
nikdy	nikdy	k6eAd1
nedorazilo	dorazit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hart	Hart	k1gMnSc1
ale	ale	k9
mohl	moct	k5eAaImAgMnS
počítat	počítat	k5eAaImF
navíc	navíc	k6eAd1
se	s	k7c7
dvěma	dva	k4xCgInPc7
čluny	člun	k1gInPc7
Filipínské	filipínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mu	on	k3xPp3gMnSc3
přislíbil	přislíbit	k5eAaPmAgMnS
MacArthur	MacArthur	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
listopadu	listopad	k1gInSc2
a	a	k8xC
prosince	prosinec	k1gInSc2
připlulo	připlout	k5eAaPmAgNnS
z	z	k7c2
Číny	Čína	k1gFnSc2
750	#num#	k4
vycvičených	vycvičený	k2eAgMnPc2d1
a	a	k8xC
vybavených	vybavený	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
4	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc3
sboru	sbor	k1gInSc2
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
většinu	většina	k1gFnSc4
4	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
opustit	opustit	k5eAaPmF
Čchin-chuang-tao	Čchin-chuang-tao	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
do	do	k7c2
rukou	ruka	k1gFnPc2
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtý	čtvrtý	k4xOgInSc1
pluk	pluk	k1gInSc1
byl	být	k5eAaImAgInS
rozmístěn	rozmístit	k5eAaPmNgInS
v	v	k7c6
Mariveles	Mariveles	k1gMnSc1
<g/>
,	,	kIx,
Olongapo	Olongapa	k1gFnSc5
a	a	k8xC
na	na	k7c6
Corregidoru	Corregidor	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mobilizace	mobilizace	k1gFnSc1
</s>
<s>
MacArthur	MacArthur	k1gMnSc1
počítal	počítat	k5eAaImAgMnS
s	s	k7c7
mobilizací	mobilizace	k1gFnSc7
75	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
desíti	deset	k4xCc2
záložních	záložní	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
41	#num#	k4
<g/>
,	,	kIx,
51	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
61	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
71	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
81	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
91	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
101	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Filipínské	filipínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
mezi	mezi	k7c7
1	#num#	k4
<g/>
.	.	kIx.
zářím	zářit	k5eAaImIp1nS
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
prosincem	prosinec	k1gInSc7
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mobilizace	mobilizace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
včas	včas	k6eAd1
povoláním	povolání	k1gNnSc7
jednoho	jeden	k4xCgInSc2
pluku	pluk	k1gInSc2
od	od	k7c2
každé	každý	k3xTgFnSc2
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nedostatek	nedostatek	k1gInSc1
vybavení	vybavení	k1gNnSc2
zdržoval	zdržovat	k5eAaImAgInS
výcvik	výcvik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
a	a	k8xC
Filipínští	filipínský	k2eAgMnPc1d1
skauti	skaut	k1gMnPc1
sloužili	sloužit	k5eAaImAgMnP
jako	jako	k9
instruktoři	instruktor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
pluky	pluk	k1gInPc7
se	se	k3xPyFc4
k	k	k7c3
divizím	divize	k1gFnPc3
přidaly	přidat	k5eAaPmAgInP
nejdříve	dříve	k6eAd3
až	až	k9
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
a	a	k8xC
třetí	třetí	k4xOgInPc1
pluky	pluk	k1gInPc1
vznikaly	vznikat	k5eAaImAgInP
až	až	k9
po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vypuknutí	vypuknutí	k1gNnSc6
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
mobilizace	mobilizace	k1gFnSc1
dokončena	dokončit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
ze	z	k7c2
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
síly	síla	k1gFnPc4
Filipínské	filipínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
asi	asi	k9
120	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Většina	většina	k1gFnSc1
odvedenců	odvedenec	k1gMnPc2
byla	být	k5eAaImAgFnS
negramotná	gramotný	k2eNgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
nerozuměli	rozumět	k5eNaImAgMnP
jazyku	jazyk	k1gInSc6
svých	svůj	k3xOyFgMnPc2
velitelů	velitel	k1gMnPc2
a	a	k8xC
mnohdy	mnohdy	k6eAd1
ani	ani	k8xC
sobě	se	k3xPyFc3
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémem	problém	k1gInSc7
byl	být	k5eAaImAgInS
i	i	k9
nedostatek	nedostatek	k1gInSc1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Filipínců	Filipínec	k1gMnPc2
neměla	mít	k5eNaImAgFnS
ani	ani	k9
kovové	kovový	k2eAgFnPc4d1
přilby	přilba	k1gFnPc4
a	a	k8xC
jenom	jenom	k9
ti	ten	k3xDgMnPc1
šťastnější	šťastný	k2eAgMnPc1d2
měli	mít	k5eAaImAgMnP
kožené	kožený	k2eAgFnPc4d1
boty	bota	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
MacArthur	MacArthur	k1gMnSc1
sice	sice	k8xC
požadoval	požadovat	k5eAaImAgInS
dodání	dodání	k1gNnSc4
125	#num#	k4
000	#num#	k4
přileb	přilba	k1gFnPc2
a	a	k8xC
84	#num#	k4
500	#num#	k4
pušek	puška	k1gFnPc2
M1	M1	k1gFnPc2
Garand	Garand	k1gInSc1
<g/>
,	,	kIx,
ty	ty	k3xPp2nSc1
ale	ale	k9
nebylo	být	k5eNaImAgNnS
kde	kde	k6eAd1
vzít	vzít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
FA	fa	k1gNnSc2
tak	tak	k6eAd1
byly	být	k5eAaImAgFnP
staré	starý	k2eAgFnPc1d1
Enfieldky	Enfieldka	k1gFnPc1
a	a	k8xC
Springfieldky	Springfieldka	k1gFnPc1
z	z	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
protitankových	protitankový	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
nebyl	být	k5eNaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
ani	ani	k9
jeden	jeden	k4xCgInSc1
protitankový	protitankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plukovník	plukovník	k1gMnSc1
Clifford	Clifford	k1gMnSc1
Bluemel	Bluemel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgMnS
velením	velení	k1gNnSc7
31	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
Filipínské	filipínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
o	o	k7c6
filipínských	filipínský	k2eAgMnPc6d1
vojácích	voják	k1gMnPc6
své	svůj	k3xOyFgFnSc2
divize	divize	k1gFnSc2
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dobře	dobře	k6eAd1
uměli	umět	k5eAaImAgMnP
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
věci	věc	k1gFnPc4
<g/>
:	:	kIx,
„	„	k?
<g/>
první	první	k4xOgFnSc4
<g/>
:	:	kIx,
když	když	k8xS
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
křičet	křičet	k5eAaImF
nahlas	nahlas	k6eAd1
'	'	kIx"
<g/>
Pozor	pozor	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
'	'	kIx"
<g/>
,	,	kIx,
vyskočit	vyskočit	k5eAaPmF
a	a	k8xC
salutovat	salutovat	k5eAaImF
a	a	k8xC
druhou	druhý	k4xOgFnSc7
<g/>
:	:	kIx,
dožadovat	dožadovat	k5eAaImF
se	se	k3xPyFc4
tří	tři	k4xCgNnPc2
jídel	jídlo	k1gNnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslední	poslední	k2eAgInPc1d1
dny	den	k1gInPc1
před	před	k7c7
válkou	válka	k1gFnSc7
</s>
<s>
„	„	k?
</s>
<s>
Říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
dostáváme	dostávat	k5eAaImIp1nP
to	ten	k3xDgNnSc4
nejnovější	nový	k2eAgNnSc4d3
vybavení	vybavení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
my	my	k3xPp1nPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
tu	tu	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
létáme	létat	k5eAaImIp1nP
<g/>
,	,	kIx,
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
letadla	letadlo	k1gNnSc2
–	–	k?
poslední	poslední	k2eAgFnSc2d1
P-40	P-40	k1gFnSc2
–	–	k?
nejsou	být	k5eNaImIp3nP
dost	dost	k6eAd1
dobrá	dobrý	k2eAgFnSc1d1
pro	pro	k7c4
boj	boj	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Někdo	někdo	k3yInSc1
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
«	«	k?
<g/>
Jsou	být	k5eAaImIp3nP
nejlepší	dobrý	k2eAgMnPc1d3
<g/>
»	»	k?
a	a	k8xC
tak	tak	k6eAd1
je	on	k3xPp3gFnPc4
máme	mít	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angličané	Angličan	k1gMnPc1
nechtějí	chtít	k5eNaImIp3nP
naše	náš	k3xOp1gFnPc4
stíhačky	stíhačka	k1gFnPc4
<g/>
,	,	kIx,
chtějí	chtít	k5eAaImIp3nP
bombardéry	bombardér	k1gMnPc4
<g/>
…	…	k?
Víš	vědět	k5eAaImIp2nS
Dee	Dee	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
zhnusen	zhnusen	k2eAgMnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
riskuje	riskovat	k5eAaBmIp3nS
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
pro	pro	k7c4
naše	náš	k3xOp1gNnSc4
všivácké	všivácký	k2eAgNnSc4d1
zastaralé	zastaralý	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
2	#num#	k4
<g/>
d	d	k?
Lt	Lt	k1gMnSc1
(	(	kIx(
<g/>
~	~	kIx~
poručík	poručík	k1gMnSc1
<g/>
)	)	kIx)
G.	G.	kA
Max	max	kA
Louk	louka	k1gFnPc2
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
v	v	k7c6
dopisu	dopis	k1gInSc6
své	svůj	k3xOyFgFnSc3
sestře	sestra	k1gFnSc3
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
1941	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Louk	louka	k1gFnPc2
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
na	na	k7c4
Clark	Clark	k1gInSc4
Fieldu	Field	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gFnSc4
P-40B	P-40B	k1gFnSc4
při	při	k7c6
startu	start	k1gInSc6
během	během	k7c2
náletu	nálet	k1gInSc2
zasažen	zasáhnout	k5eAaPmNgMnS
pumou	puma	k1gFnSc7
a	a	k8xC
Louk	louka	k1gFnPc2
uhořel	uhořet	k5eAaPmAgInS
v	v	k7c6
kokpitu	kokpit	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USS	USS	kA
Pigeon	Pigeon	k1gMnSc1
(	(	kIx(
<g/>
ASR-	ASR-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
narazily	narazit	k5eAaPmAgInP
na	na	k7c4
japonský	japonský	k2eAgInSc4d1
konvoj	konvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pigeon	Pigeon	k1gInSc1
byl	být	k5eAaImAgInS
potopen	potopit	k5eAaPmNgInS
japonskými	japonský	k2eAgFnPc7d1
letadly	letadlo	k1gNnPc7
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1942	#num#	k4
u	u	k7c2
Corregidoru	Corregidor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c4
podzim	podzim	k1gInSc4
1941	#num#	k4
stále	stále	k6eAd1
více	hodně	k6eAd2
stupňovalo	stupňovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
varovalo	varovat	k5eAaImAgNnS
americké	americký	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
před	před	k7c7
nenadálým	nenadálý	k2eAgInSc7d1
japonským	japonský	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
nebylo	být	k5eNaImAgNnS
schopno	schopen	k2eAgNnSc1d1
předvídat	předvídat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
očekávat	očekávat	k5eAaImF
každým	každý	k3xTgInSc7
okamžikem	okamžik	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Říkalo	říkat	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gNnSc6
rovněž	rovněž	k9
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
…	…	k?
<g/>
Jestliže	jestliže	k8xS
nepřátelství	nepřátelství	k1gNnSc1
nebude	být	k5eNaImBp3nS
<g/>
,	,	kIx,
opakuji	opakovat	k5eAaImIp1nS
nebude	být	k5eNaImBp3nS
<g/>
,	,	kIx,
možno	možno	k6eAd1
zabránit	zabránit	k5eAaPmF
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
si	se	k3xPyFc3
přejí	přát	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Japonsko	Japonsko	k1gNnSc1
dopustilo	dopustit	k5eAaPmAgNnS
zjevného	zjevný	k2eAgMnSc4d1
činu	čin	k1gInSc6
jako	jako	k9
první	první	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
by	by	kYmCp3nS
neměl	mít	k5eNaImAgInS
<g/>
,	,	kIx,
opakuji	opakovat	k5eAaImIp1nS
neměl	mít	k5eNaImAgMnS
<g/>
,	,	kIx,
být	být	k5eAaImF
vykládán	vykládat	k5eAaImNgInS
jako	jako	k9
omezení	omezení	k1gNnSc2
vašich	váš	k3xOp2gNnPc2
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
ohrozila	ohrozit	k5eAaPmAgFnS
vaši	váš	k3xOp2gFnSc4
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vypuknutím	vypuknutí	k1gNnSc7
japonských	japonský	k2eAgFnPc2d1
nepřátelských	přátelský	k2eNgFnPc2d1
akcí	akce	k1gFnPc2
je	být	k5eAaImIp3nS
vám	vy	k3xPp2nPc3
nařizováno	nařizován	k2eAgNnSc1d1
provést	provést	k5eAaPmF
průzkum	průzkum	k1gInSc4
a	a	k8xC
jiná	jiný	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
uznáte	uznat	k5eAaPmIp2nP
za	za	k7c4
vhodná	vhodný	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlaste	hlásit	k5eAaImRp2nP
přijatá	přijatý	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dojde	dojít	k5eAaPmIp3nS
<g/>
-li	-li	k?
k	k	k7c3
nepřátelství	nepřátelství	k1gNnSc3
<g/>
,	,	kIx,
řiďte	řídit	k5eAaImRp2nP
se	se	k3xPyFc4
úkoly	úkol	k1gInPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
vám	vy	k3xPp2nPc3
byly	být	k5eAaImAgInP
přiděleny	přidělit	k5eAaPmNgInP
na	na	k7c6
základě	základ	k1gInSc6
RAINBOW	RAINBOW	kA
5	#num#	k4
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1
na	na	k7c6
tomto	tento	k3xDgNnSc6
sdělení	sdělení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
Filipínám	Filipíny	k1gFnPc3
a	a	k8xC
Havaji	Havaj	k1gFnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednak	jednak	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
nemluví	mluvit	k5eNaImIp3nS
o	o	k7c6
válce	válka	k1gFnSc6
(	(	kIx(
<g/>
war	war	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c6
nepřátelství	nepřátelství	k1gNnSc6
(	(	kIx(
<g/>
hostilities	hostilities	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
hlavně	hlavně	k9
instruktáž	instruktáž	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
nesmějí	smát	k5eNaImIp3nP
dopustit	dopustit	k5eAaPmF
nepřátelství	nepřátelství	k1gNnSc4
jako	jako	k9
první	první	k4xOgFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jinak	jinak	k6eAd1
mají	mít	k5eAaImIp3nP
velitelé	velitel	k1gMnPc1
povinnost	povinnost	k1gFnSc4
zajistit	zajistit	k5eAaPmF
průzkum	průzkum	k1gInSc4
a	a	k8xC
všechny	všechen	k3xTgInPc4
potřebné	potřebné	k1gNnSc4
opatření	opatření	k1gNnPc2
pro	pro	k7c4
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
MacArthur	MacArthur	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
vyslat	vyslat	k5eAaPmF
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
B-17	B-17	k1gFnPc2
na	na	k7c4
průzkum	průzkum	k1gInSc4
zátoky	zátoka	k1gFnSc2
Cam	Cam	k1gFnSc2
Ranh	Ranha	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c4
což	což	k3yRnSc4,k3yQnSc4
ho	on	k3xPp3gNnSc4
žádal	žádat	k5eAaImAgMnS
letecký	letecký	k2eAgMnSc1d1
maršál	maršál	k1gMnSc1
(	(	kIx(
<g/>
Air	Air	k1gMnSc1
Chief	Chief	k1gMnSc1
Marshal	Marshal	k1gMnSc1
<g/>
)	)	kIx)
sir	sir	k1gMnSc1
Robert	Robert	k1gMnSc1
Brooke-Popham	Brooke-Popham	k1gInSc4
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
britských	britský	k2eAgFnPc2d1
dálněvýchodních	dálněvýchodní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Singapuru	Singapur	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
MacArthur	MacArthur	k1gMnSc1
to	ten	k3xDgNnSc4
odůvodnil	odůvodnit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
takové	takový	k3xDgFnPc4
operace	operace	k1gFnPc4
zakázané	zakázaný	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Také	také	k9
ministerstvo	ministerstvo	k1gNnSc1
námořnictva	námořnictvo	k1gNnSc2
odeslalo	odeslat	k5eAaPmAgNnS
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
varování	varování	k1gNnSc2
podřízeným	podřízený	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obdržení	obdržení	k1gNnSc6
těchto	tento	k3xDgFnPc2
zpráv	zpráva	k1gFnPc2
se	se	k3xPyFc4
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
setkal	setkat	k5eAaPmAgMnS
MacArthur	MacArthur	k1gMnSc1
s	s	k7c7
Hartem	Hart	k1gMnSc7
a	a	k8xC
americkým	americký	k2eAgMnSc7d1
komisařem	komisař	k1gMnSc7
pro	pro	k7c4
Filipíny	Filipíny	k1gFnPc4
Francisem	Francis	k1gInSc7
B.	B.	kA
Sayre	Sayr	k1gInSc5
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
probrali	probrat	k5eAaPmAgMnP
nutná	nutný	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
MacArthur	MacArthur	k1gMnSc1
seznámil	seznámit	k5eAaPmAgMnS
štáb	štáb	k1gInSc4
s	s	k7c7
přijatými	přijatý	k2eAgNnPc7d1
opatřeními	opatření	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
i	i	k9
rozšíření	rozšíření	k1gNnSc1
leteckého	letecký	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Vážnost	vážnost	k1gFnSc1
situace	situace	k1gFnSc1
ale	ale	k8xC
očividně	očividně	k6eAd1
nedocházela	docházet	k5eNaImAgFnS
všem	všecek	k3xTgMnPc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
například	například	k6eAd1
velitel	velitel	k1gMnSc1
Nichols	Nicholsa	k1gFnPc2
Field	Field	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
nechat	nechat	k5eAaPmF
kopat	kopat	k5eAaImF
zákopy	zákop	k1gInPc4
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
zdevastoval	zdevastovat	k5eAaPmAgInS
trávník	trávník	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
narazila	narazit	k5eAaPmAgFnS
ve	v	k7c6
Formoské	Formoský	k2eAgFnSc6d1
úžině	úžina	k1gFnSc6
malá	malý	k2eAgFnSc1d1
skupinka	skupinka	k1gFnSc1
amerických	americký	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
–	–	k?
složená	složený	k2eAgFnSc1d1
z	z	k7c2
minolovky	minolovka	k1gFnSc2
Finch	Finch	k1gInSc4
<g/>
,	,	kIx,
záchranného	záchranný	k2eAgNnSc2d1
ponorkového	ponorkový	k2eAgNnSc2d1
plavidla	plavidlo	k1gNnSc2
Pigeon	Pigeona	k1gFnPc2
a	a	k8xC
dělových	dělový	k2eAgInPc2d1
člunů	člun	k1gInPc2
Luzon	Luzona	k1gFnPc2
a	a	k8xC
Oahu	Oahus	k1gInSc2
–	–	k?
na	na	k7c4
japonský	japonský	k2eAgInSc4d1
konvoj	konvoj	k1gInSc4
doprovázený	doprovázený	k2eAgInSc1d1
válečnými	válečný	k2eAgNnPc7d1
plavidly	plavidlo	k1gNnPc7
a	a	k8xC
směřující	směřující	k2eAgFnSc1d1
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
žádnému	žádný	k3yNgInSc3
incidentu	incident	k1gInSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
nad	nad	k7c7
americkými	americký	k2eAgNnPc7d1
plavidly	plavidlo	k1gNnPc7
pouze	pouze	k6eAd1
zakroužil	zakroužit	k5eAaPmAgMnS
hydroplán	hydroplán	k1gInSc4
a	a	k8xC
každý	každý	k3xTgInSc1
konvoj	konvoj	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
vlastní	vlastní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
rozedněním	rozednění	k1gNnSc7
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
nacvičovalo	nacvičovat	k5eAaImAgNnS
šest	šest	k4xCc1
P-40B	P-40B	k1gMnPc2
od	od	k7c2
velitelské	velitelský	k2eAgFnSc2d1
(	(	kIx(
<g/>
HS	HS	kA
<g/>
)	)	kIx)
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
noční	noční	k2eAgNnSc1d1
stíhání	stíhání	k1gNnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
B-18	B-18	k1gFnPc7
a	a	k8xC
světlomety	světlomet	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
toho	ten	k3xDgInSc2
narazila	narazit	k5eAaPmAgFnS
dvojice	dvojice	k1gFnSc1
1	#num#	k4
<g/>
st	st	kA
Lt	Lt	k1gFnPc2
(	(	kIx(
<g/>
~	~	kIx~
nadporučík	nadporučík	k1gMnSc1
<g/>
)	)	kIx)
Joseph	Joseph	k1gMnSc1
H.	H.	kA
Moore	Moor	k1gInSc5
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
1	#num#	k4
<g/>
st	st	kA
Lt	Lt	k1gFnSc1
William	William	k1gInSc1
Cummings	Cummings	k1gInSc1
(	(	kIx(
<g/>
HS	HS	kA
<g/>
)	)	kIx)
na	na	k7c4
neidentifikovaný	identifikovaný	k2eNgInSc4d1
letoun	letoun	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c7
P-	P-	k1gFnSc7
<g/>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gMnSc1
jim	on	k3xPp3gMnPc3
zmizel	zmizet	k5eAaPmAgMnS
ve	v	k7c6
tmě	tma	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
přistání	přistání	k1gNnSc6
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
žádná	žádný	k3yNgNnPc4
jiná	jiný	k2eAgNnPc4d1
vlastní	vlastní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
nebyla	být	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k9
radar	radar	k1gInSc1
SCR-270B	SCR-270B	k1gFnPc2
v	v	k7c6
Iba	iba	k6eAd1
zaregistroval	zaregistrovat	k5eAaPmAgMnS
neidentifikovaný	identifikovaný	k2eNgInSc4d1
kontakt	kontakt	k1gInSc4
letící	letící	k2eAgInPc4d1
podél	podél	k7c2
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Luzonu	Luzon	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zaznamenal	zaznamenat	k5eAaPmAgMnS
SCR-270B	SCR-270B	k1gMnSc1
opět	opět	k6eAd1
neznámý	známý	k2eNgInSc4d1
kontakt	kontakt	k1gInSc4
<g/>
,	,	kIx,
proti	proti	k7c3
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
odstartoval	odstartovat	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
P-40E	P-40E	k1gMnSc1
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
z	z	k7c2
Iba	iba	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
ztrátě	ztráta	k1gFnSc3
radiového	radiový	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
ale	ale	k9
cíl	cíl	k1gInSc4
nenašel	najít	k5eNaPmAgMnS
a	a	k8xC
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
samou	samý	k3xTgFnSc4
noc	noc	k1gFnSc4
byl	být	k5eAaImAgMnS
před	před	k7c7
úsvitem	úsvit	k1gInSc7
během	během	k7c2
dalšího	další	k2eAgInSc2d1
nácviku	nácvik	k1gInSc2
nočního	noční	k2eAgNnSc2d1
stíhání	stíhání	k1gNnSc2
opět	opět	k6eAd1
zpozorován	zpozorován	k2eAgInSc1d1
další	další	k2eAgInSc1d1
neidentifikovatelný	identifikovatelný	k2eNgInSc1d1
letoun	letoun	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
těchto	tento	k3xDgInPc2
incidentů	incident	k1gInPc2
povolil	povolit	k5eAaPmAgMnS
náčelník	náčelník	k1gMnSc1
MacArthurova	MacArthurův	k2eAgInSc2d1
štábu	štáb	k1gInSc2
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Richard	Richard	k1gMnSc1
K.	K.	kA
Sutherland	Sutherland	k1gInSc1
sestřelení	sestřelení	k1gNnSc1
vetřelců	vetřelec	k1gMnPc2
nad	nad	k7c7
Luzonem	Luzon	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
zakázal	zakázat	k5eAaPmAgInS
jejich	jejich	k3xOp3gNnSc4
stíhání	stíhání	k1gNnSc4
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c4
sobotu	sobota	k1gFnSc4
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
nařídil	nařídit	k5eAaPmAgMnS
MacArthur	MacArthura	k1gFnPc2
Severoluzonským	Severoluzonský	k2eAgFnPc3d1
silám	síla	k1gFnPc3
pohotovost	pohotovost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
připraveny	připraven	k2eAgFnPc4d1
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
přesunout	přesunout	k5eAaPmF
na	na	k7c4
pobřeží	pobřeží	k1gNnSc4
a	a	k8xC
hájit	hájit	k5eAaImF
vyloďovací	vyloďovací	k2eAgFnPc4d1
pláže	pláž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnPc1
byla	být	k5eAaImAgNnP
rozptýlena	rozptýlit	k5eAaPmNgNnP
na	na	k7c6
letištích	letiště	k1gNnPc6
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
jen	jen	k9
to	ten	k3xDgNnSc1
nedostatek	nedostatek	k1gInSc4
místa	místo	k1gNnSc2
dovoloval	dovolovat	k5eAaImAgMnS
<g/>
)	)	kIx)
a	a	k8xC
hlídána	hlídán	k2eAgFnSc1d1
kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
ze	z	k7c2
sabotáží	sabotáž	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
ze	z	k7c2
7	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
–	–	k?
ještě	ještě	k6eAd1
před	před	k7c7
napadením	napadení	k1gNnSc7
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
–	–	k?
radar	radar	k1gInSc1
v	v	k7c4
Iba	iba	k6eAd1
opět	opět	k6eAd1
po	po	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
zaznamenal	zaznamenat	k5eAaPmAgInS
neidentifikovaný	identifikovaný	k2eNgInSc4d1
kontakt	kontakt	k1gInSc4
letící	letící	k2eAgInPc4d1
podél	podél	k7c2
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
čtveřici	čtveřice	k1gFnSc6
rikkó	rikkó	k?
(	(	kIx(
<g/>
tedy	tedy	k9
buďto	buďto	k8xC
G3M	G3M	k1gFnSc1
nebo	nebo	k8xC
G	G	kA
<g/>
4	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
na	na	k7c6
meteorologickém	meteorologický	k2eAgInSc6d1
průzkumném	průzkumný	k2eAgInSc6d1
letu	let	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstartovalo	odstartovat	k5eAaPmAgNnS
proti	proti	k7c3
nim	on	k3xPp3gInPc3
šest	šest	k4xCc4
P-40E	P-40E	k1gMnPc2
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
z	z	k7c2
Iba	iba	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
opět	opět	k6eAd1
kvůli	kvůli	k7c3
ztrátě	ztráta	k1gFnSc3
spojení	spojení	k1gNnSc2
(	(	kIx(
<g/>
a	a	k8xC
neznalosti	neznalost	k1gFnSc2
letové	letový	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
Japonců	Japonec	k1gMnPc2
<g/>
)	)	kIx)
nic	nic	k6eAd1
nenašly	najít	k5eNaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
úder	úder	k1gInSc1
<g/>
:	:	kIx,
MacArthurův	MacArthurův	k2eAgInSc1d1
Pearl	Pearl	k1gInSc1
Harbor	Harbor	k1gInSc1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
</s>
<s>
Reakce	reakce	k1gFnPc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
a	a	k8xC
první	první	k4xOgInPc1
útoky	útok	k1gInPc1
</s>
<s>
Ceremonie	ceremonie	k1gFnSc1
v	v	k7c4
Camp	camp	k1gInSc4
Murphy	Murpha	k1gFnSc2
<g/>
,	,	kIx,
Rizal	Rizal	k1gMnSc1
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1941	#num#	k4
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
založení	založení	k1gNnSc1
sboru	sbor	k1gInSc2
filipínského	filipínský	k2eAgNnSc2d1
armádního	armádní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
mikrofonu	mikrofon	k1gInSc2
je	být	k5eAaImIp3nS
Douglas	Douglas	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
a	a	k8xC
v	v	k7c6
řadě	řada	k1gFnSc6
za	za	k7c7
ním	on	k3xPp3gNnSc7
(	(	kIx(
<g/>
zleva	zleva	k6eAd1
doprava	doprava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Richard	Richard	k1gMnSc1
K.	K.	kA
Sutherland	Sutherland	k1gInSc1
<g/>
,	,	kIx,
plukovník	plukovník	k1gMnSc1
Harold	Harold	k1gMnSc1
H.	H.	kA
George	Georg	k1gMnSc4
<g/>
,	,	kIx,
podplukovník	podplukovník	k1gMnSc1
William	William	k1gInSc4
F.	F.	kA
Marquat	Marquat	k2eAgMnSc1d1
a	a	k8xC
major	major	k1gMnSc1
LeGrande	LeGrand	k1gInSc5
A.	A.	kA
Diller	Diller	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Malalag	Malalag	k1gInSc1
Bay	Bay	k1gFnSc2
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
po	po	k7c6
sedmé	sedmý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
ráno	ráno	k6eAd1
<g/>
:	:	kIx,
Kankkó	Kankkó	k1gMnSc1
B5N1	B5N1	k1gMnSc1
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
přelétá	přelétat	k5eAaImIp3nS,k5eAaPmIp3nS
nad	nad	k7c7
zásobovací	zásobovací	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
hydroplánů	hydroplán	k1gInPc2
USS	USS	kA
William	William	k1gInSc1
B.	B.	kA
Preston	Preston	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pumové	pumový	k2eAgInPc1d1
závěsy	závěs	k1gInPc1
jsou	být	k5eAaImIp3nP
prázdné	prázdný	k2eAgInPc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
kankkó	kankkó	k?
již	již	k9
svůj	svůj	k3xOyFgInSc4
útok	útok	k1gInSc4
provedl	provést	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
pobřeží	pobřeží	k1gNnSc2
hoří	hořet	k5eAaImIp3nP
zbytky	zbytek	k1gInPc1
obou	dva	k4xCgFnPc2
PBY-	PBY-	k1gFnPc2
<g/>
4	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
rozstříleli	rozstřílet	k5eAaPmAgMnP
doprovodní	doprovodní	k2eAgMnPc1d1
stíhači	stíhač	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnPc1
zprávy	zpráva	k1gFnPc1
o	o	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc4
zachytila	zachytit	k5eAaPmAgFnS
stanice	stanice	k1gFnSc1
Východoasijského	východoasijský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
v	v	k7c6
Manile	Manila	k1gFnSc6
ve	v	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
v	v	k7c6
pondělí	pondělí	k1gNnSc6
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
(	(	kIx(
<g/>
na	na	k7c6
Havaji	Havaj	k1gFnSc6
bylo	být	k5eAaImAgNnS
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
se	se	k3xPyFc4
zpráva	zpráva	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
k	k	k7c3
admirálu	admirál	k1gMnSc3
Hartovi	Hart	k1gMnSc3
a	a	k8xC
ten	ten	k3xDgInSc1
rozkázal	rozkázat	k5eAaPmAgInS
vysílat	vysílat	k5eAaImF
jí	on	k3xPp3gFnSc3
dál	daleko	k6eAd2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jí	on	k3xPp3gFnSc3
zachytily	zachytit	k5eAaPmAgFnP
i	i	k9
další	další	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
se	se	k3xPyFc4
o	o	k7c6
útoku	útok	k1gInSc6
dozvěděl	dozvědět	k5eAaPmAgMnS
náčelník	náčelník	k1gMnSc1
MacArthurova	MacArthurův	k2eAgInSc2d1
štábu	štáb	k1gInSc2
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Richard	Richard	k1gMnSc1
K.	K.	kA
Sutherland	Sutherland	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
útoku	útok	k1gInSc6
slyšel	slyšet	k5eAaImAgMnS
z	z	k7c2
rozhlasu	rozhlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sutherland	Sutherland	k1gInSc1
okamžitě	okamžitě	k6eAd1
o	o	k7c6
útoku	útok	k1gInSc6
informoval	informovat	k5eAaBmAgInS
MacArthura	MacArthur	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
apartmánu	apartmán	k1gInSc6
hotelu	hotel	k1gInSc2
Manila	Manila	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
Clark	Clark	k1gInSc1
Field	Fielda	k1gFnPc2
se	se	k3xPyFc4
o	o	k7c6
útoku	útok	k1gInSc6
dozvěděl	dozvědět	k5eAaPmAgMnS
z	z	k7c2
rozhlasového	rozhlasový	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
již	již	k6eAd1
o	o	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc4
věděli	vědět	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
něco	něco	k3yInSc1
naprosto	naprosto	k6eAd1
neuvěřitelného	uvěřitelný	k2eNgMnSc4d1
<g/>
:	:	kIx,
japonský	japonský	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
americká	americký	k2eAgNnPc4d1
letiště	letiště	k1gNnPc4
zaskočil	zaskočit	k5eAaPmAgInS
Američany	Američan	k1gMnPc4
nepřipravené	připravený	k2eNgNnSc1d1
a	a	k8xC
s	s	k7c7
letouny	letoun	k1gInPc7
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
první	první	k4xOgFnPc1
zaútočily	zaútočit	k5eAaPmAgFnP
námořní	námořní	k2eAgFnPc1d1
letouny	letoun	k1gInPc4
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
na	na	k7c4
Mindanao	Mindanao	k1gNnSc4
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
armádní	armádní	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
z	z	k7c2
letišť	letiště	k1gNnPc2
Čchia-tung	Čchia-tunga	k1gFnPc2
a	a	k8xC
Ťiaji	Ťiaje	k1gFnSc4
na	na	k7c6
severu	sever	k1gInSc6
Formosy	Formosa	k1gFnSc2
na	na	k7c4
sever	sever	k1gInSc4
Luzonu	Luzon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
dorazily	dorazit	k5eAaPmAgFnP
nad	nad	k7c4
svůj	svůj	k3xOyFgInSc4
cíl	cíl	k1gInSc4
–	–	k?
Davao	Davao	k6eAd1
na	na	k7c6
Mindanau	Mindanaum	k1gNnSc6
–	–	k?
letouny	letoun	k1gInPc4
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jelikož	jelikož	k8xS
letiště	letiště	k1gNnSc1
i	i	k8xC
přístav	přístav	k1gInSc1
byly	být	k5eAaImAgInP
prázdně	prázdně	k6eAd1
<g/>
,	,	kIx,
pokračovaly	pokračovat	k5eAaImAgFnP
dál	daleko	k6eAd2
v	v	k7c6
pátrání	pátrání	k1gNnSc6
a	a	k8xC
v	v	k7c6
7	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
mateřskou	mateřský	k2eAgFnSc4d1
loď	loď	k1gFnSc4
hydroplánů	hydroplán	k1gInPc2
USS	USS	kA
William	William	k1gInSc1
B.	B.	kA
Preston	Preston	k1gInSc1
a	a	k8xC
dvě	dva	k4xCgFnPc1
zakotvené	zakotvený	k2eAgFnPc1d1
PBY-4	PBY-4	k1gFnPc1
Cataliny	Catalina	k1gFnSc2
(	(	kIx(
<g/>
101	#num#	k4
<g/>
-P-	-P-	k?
<g/>
4	#num#	k4
a	a	k8xC
101	#num#	k4
<g/>
-P-	-P-	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
od	od	k7c2
VP-	VP-	k1gFnSc2
<g/>
101	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zničily	zničit	k5eAaPmAgInP
obě	dva	k4xCgFnPc4
Cataliny	Catalina	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
William	William	k1gInSc1
B.	B.	kA
Preston	Preston	k1gInSc1
unikl	uniknout	k5eAaPmAgInS
bez	bez	k7c2
poškození	poškození	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
útok	útok	k1gInSc1
na	na	k7c4
Filipíny	Filipíny	k1gFnPc4
stál	stát	k5eAaImAgMnS
Japonce	Japonec	k1gMnPc4
pouze	pouze	k6eAd1
jeden	jeden	k4xCgMnSc1
B5N1	B5N1	k1gMnSc1
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Osmá	osmý	k4xOgNnPc4
čútai	čútai	k1gNnPc4
vyslala	vyslat	k5eAaPmAgFnS
25	#num#	k4
Ki-	Ki-	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
48	#num#	k4
<g/>
-Ib	-Ib	k?
Sokei	Soke	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
krátce	krátce	k6eAd1
po	po	k7c6
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
letiště	letiště	k1gNnSc4
u	u	k7c2
města	město	k1gNnSc2
Tuguegarao	Tuguegarao	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Čtrnáctá	čtrnáctý	k4xOgNnPc4
čútai	čútai	k1gNnPc4
vyslala	vyslat	k5eAaPmAgFnS
18	#num#	k4
Ki-	Ki-	k1gFnSc1
<g/>
21	#num#	k4
<g/>
-IIa	-IIum	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
Camp	camp	k1gInSc4
John	John	k1gMnSc1
Hay	Hay	k1gMnSc1
v	v	k7c6
Baguio	Baguio	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americká	americký	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
12	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
od	od	k7c2
východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
letištěm	letiště	k1gNnSc7
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
Fort	Fort	k?
Stotsenburg	Stotsenburg	k1gInSc4
(	(	kIx(
<g/>
obdélníkový	obdélníkový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
lemovaný	lemovaný	k2eAgInSc4d1
stromy	strom	k1gInPc7
vlevo	vlevo	k6eAd1
nahoře	nahoře	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
pohoří	pohoří	k1gNnSc4
Zambales	Zambalesa	k1gFnPc2
</s>
<s>
Jako	jako	k9
reakci	reakce	k1gFnSc4
na	na	k7c4
první	první	k4xOgInSc4
radarový	radarový	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
nad	nad	k7c7
Luzonem	Luzon	k1gInSc7
(	(	kIx(
<g/>
armádní	armádní	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
mířící	mířící	k2eAgInPc1d1
na	na	k7c6
Baguio	Baguio	k6eAd1
a	a	k8xC
Tuguegarao	Tuguegarao	k6eAd1
<g/>
)	)	kIx)
odstartovaly	odstartovat	k5eAaPmAgFnP
mezi	mezi	k7c7
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
a	a	k8xC
8	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
stíhačky	stíhačka	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radarový	radarový	k2eAgInSc1d1
kontakt	kontakt	k1gInSc1
ale	ale	k9
zmizel	zmizet	k5eAaPmAgInS
a	a	k8xC
stíhačky	stíhačka	k1gFnSc2
na	na	k7c4
žádného	žádný	k3yNgMnSc4
nepřítele	nepřítel	k1gMnSc4
nenarazily	narazit	k5eNaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
ubývajícím	ubývající	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
v	v	k7c6
10	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
vrátila	vrátit	k5eAaPmAgFnS
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gInSc4
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystřídala	vystřídat	k5eAaPmAgFnS
je	on	k3xPp3gNnSc4
část	část	k1gFnSc1
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
kroužila	kroužit	k5eAaImAgFnS
nad	nad	k7c4
Iba	iba	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obranu	obrana	k1gFnSc4
Clark	Clark	k1gInSc4
Fieldu	Field	k1gInSc2
<g/>
,	,	kIx,
Corregidoru	Corregidor	k1gInSc2
a	a	k8xC
Iba	iba	k6eAd1
kolem	kolem	k7c2
poledne	poledne	k1gNnSc2
zajišťovaly	zajišťovat	k5eAaImAgFnP
hlídky	hlídka	k1gFnPc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
ale	ale	k9
musely	muset	k5eAaImAgFnP
ve	v	k7c6
12	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
přistát	přistát	k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
ale	ale	k8xC
některé	některý	k3yIgInPc1
stroje	stroj	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
době	doba	k1gFnSc6
útoku	útok	k1gInSc2
stále	stále	k6eAd1
ještě	ještě	k6eAd1
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
dotankovat	dotankovat	k5eAaPmF
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
převzít	převzít	k5eAaPmF
hlídku	hlídka	k1gFnSc4
nad	nad	k7c4
Clark	Clark	k1gInSc4
Fieldem	Field	k1gInSc7
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
neodstartovala	odstartovat	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
americké	americký	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
mezitím	mezitím	k6eAd1
velitelé	velitel	k1gMnPc1
dohadovali	dohadovat	k5eAaImAgMnP
o	o	k7c6
následujícím	následující	k2eAgInSc6d1
postupu	postup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
přijel	přijet	k5eAaPmAgMnS
na	na	k7c6
velitelství	velitelství	k1gNnSc6
dálněvýchodního	dálněvýchodní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
generálmajor	generálmajor	k1gMnSc1
Brereton	Brereton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plánoval	plánovat	k5eAaImAgMnS
vyslat	vyslat	k5eAaPmF
své	svůj	k3xOyFgFnPc4
B-17	B-17	k1gFnPc4
z	z	k7c2
Clarku	Clarek	k1gInSc2
k	k	k7c3
dennímu	denní	k2eAgInSc3d1
náletu	nálet	k1gInSc3
na	na	k7c4
Formosu	Formosa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neodvážil	odvážit	k5eNaPmAgMnS
se	se	k3xPyFc4
ale	ale	k8xC
vyslat	vyslat	k5eAaPmF
útok	útok	k1gInSc4
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
zodpovědnost	zodpovědnost	k1gFnSc4
a	a	k8xC
nedařilo	dařit	k5eNaImAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
MacArthurem	MacArthur	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
získal	získat	k5eAaPmAgMnS
povolení	povolení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojil	spojit	k5eAaPmAgMnS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
s	s	k7c7
brigádním	brigádní	k2eAgMnSc7d1
generálem	generál	k1gMnSc7
Sutherlandem	Sutherland	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
schválil	schválit	k5eAaPmAgInS
přípravu	příprava	k1gFnSc4
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
rovněž	rovněž	k9
trval	trvat	k5eAaImAgInS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
útok	útok	k1gInSc1
musí	muset	k5eAaImIp3nS
potvrdit	potvrdit	k5eAaPmF
MacArthur	MacArthur	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
se	se	k3xPyFc4
opět	opět	k6eAd1
Brereton	Brereton	k1gInSc1
pokusil	pokusit	k5eAaPmAgInS
získat	získat	k5eAaPmF
povolení	povolení	k1gNnSc4
k	k	k7c3
útoku	útok	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
(	(	kIx(
<g/>
podle	podle	k7c2
Breretona	Brereton	k1gMnSc2
<g/>
)	)	kIx)
to	ten	k3xDgNnSc1
Sutherland	Sutherlando	k1gNnPc2
zamítl	zamítnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
ale	ale	k8xC
Breretonovi	Brereton	k1gMnSc3
volal	volat	k5eAaImAgInS
MacArthur	MacArthur	k1gMnSc1
a	a	k8xC
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
náletem	nálet	k1gInSc7
v	v	k7c6
pozdních	pozdní	k2eAgFnPc6d1
odpoledních	odpolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
vyslány	vyslat	k5eAaPmNgFnP
tři	tři	k4xCgFnPc1
B-17	B-17	k1gFnPc1
na	na	k7c4
průzkum	průzkum	k1gInSc4
japonských	japonský	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
na	na	k7c6
Formose	Formosa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgFnPc1d1
B-17	B-17	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
v	v	k7c6
11	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
vyzbrojovány	vyzbrojován	k2eAgFnPc1d1
pumami	puma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jedné	jeden	k4xCgFnSc2
Sutherlandovy	Sutherlandův	k2eAgFnSc2d1
verze	verze	k1gFnSc2
chtěl	chtít	k5eAaImAgInS
Brereton	Brereton	k1gInSc1
mít	mít	k5eAaImF
napřed	napřed	k6eAd1
fotografie	fotografia	k1gFnPc1
cílů	cíl	k1gInPc2
<g/>
,	,	kIx,
podle	podle	k7c2
druhé	druhý	k4xOgFnSc2
Sutherlandovy	Sutherlandův	k2eAgFnSc2d1
verze	verze	k1gFnSc2
to	ten	k3xDgNnSc4
naopak	naopak	k6eAd1
byl	být	k5eAaImAgInS
Sutherland	Sutherland	k1gInSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
nařídil	nařídit	k5eAaPmAgMnS
průzkumný	průzkumný	k2eAgInSc4d1
let	let	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
MacArthur	MacArthur	k1gMnSc1
popřel	popřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mu	on	k3xPp3gMnSc3
Brereton	Brereton	k1gInSc1
navrhoval	navrhovat	k5eAaImAgInS
provedení	provedení	k1gNnSc4
útoku	útok	k1gInSc2
na	na	k7c4
Formosu	Formosa	k1gFnSc4
a	a	k8xC
že	že	k8xS
naopak	naopak	k6eAd1
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
plánoval	plánovat	k5eAaImAgMnS
provedení	provedení	k1gNnSc3
útoku	útok	k1gInSc2
ráno	ráno	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
je	být	k5eAaImIp3nS
situace	situace	k1gFnSc1
v	v	k7c6
americkém	americký	k2eAgNnSc6d1
velení	velení	k1gNnSc6
v	v	k7c6
ranních	ranní	k2eAgFnPc6d1
a	a	k8xC
dopoledních	dopolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zahalena	zahalit	k5eAaPmNgFnS
mnoha	mnoho	k4c7
nejasnostmi	nejasnost	k1gFnPc7
a	a	k8xC
navzájem	navzájem	k6eAd1
si	se	k3xPyFc3
protiřečícími	protiřečící	k2eAgFnPc7d1
výpověďmi	výpověď	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situaci	situace	k1gFnSc4
neulehčuje	ulehčovat	k5eNaImIp3nS
ani	ani	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Sutherland	Sutherland	k1gInSc1
později	pozdě	k6eAd2
měnil	měnit	k5eAaImAgInS
své	svůj	k3xOyFgFnPc4
výpovědi	výpověď	k1gFnPc4
a	a	k8xC
i	i	k9
MacArthurovi	MacArthur	k1gMnSc3
výpovědi	výpověď	k1gFnSc2
si	se	k3xPyFc3
odporují	odporovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Existují	existovat	k5eAaImIp3nP
rovněž	rovněž	k9
nejasnosti	nejasnost	k1gFnPc1
<g/>
,	,	kIx,
jestli	jestli	k8xS
vůbec	vůbec	k9
letadla	letadlo	k1gNnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
B-	B-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
Clarku	Clarek	k1gInSc6
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
či	či	k8xC
nebyla	být	k5eNaImAgFnS
<g/>
,	,	kIx,
vyrovnána	vyrovnat	k5eAaPmNgFnS,k5eAaBmNgFnS
na	na	k7c6
ploše	plocha	k1gFnSc6
a	a	k8xC
představovala	představovat	k5eAaImAgFnS
snadný	snadný	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
japonských	japonský	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
se	se	k3xPyFc4
opravdu	opravdu	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
překvapit	překvapit	k5eAaPmF
Američany	Američan	k1gMnPc4
s	s	k7c7
letouny	letoun	k1gInPc7
vyrovnanými	vyrovnaný	k2eAgInPc7d1
na	na	k7c6
ploše	plocha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Útok	útok	k1gInSc1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
na	na	k7c6
Iba	iba	k6eAd1
<g/>
,	,	kIx,
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
a	a	k8xC
Del	Del	k1gMnPc2
Carmen	Carmen	k1gInSc1
</s>
<s>
„	„	k?
</s>
<s>
…	…	k?
<g/>
Když	když	k8xS
jsme	být	k5eAaImIp1nP
dorazili	dorazit	k5eAaPmAgMnP
nad	nad	k7c4
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
překvapeni	překvapit	k5eAaPmNgMnP
<g/>
,	,	kIx,
že	že	k8xS
nás	my	k3xPp1nPc4
nenapadli	napadnout	k5eNaPmAgMnP
stíhači	stíhač	k1gMnPc7
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
pod	pod	k7c7
námi	my	k3xPp1nPc7
bylo	být	k5eAaImAgNnS
pět	pět	k4xCc1
amerických	americký	k2eAgFnPc2d1
stíhaček	stíhačka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nezaútočily	zaútočit	k5eNaPmAgFnP
a	a	k8xC
my	my	k3xPp1nPc1
zaútočit	zaútočit	k5eAaPmF
nemohli	moct	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gInPc1
rozkazy	rozkaz	k1gInPc1
byly	být	k5eAaImAgInP
nenapadat	napadat	k5eNaImF
<g/>
,	,	kIx,
dokud	dokud	k8xS
všechny	všechen	k3xTgInPc1
naše	náš	k3xOp1gInPc1
bombardéry	bombardér	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
rovněž	rovněž	k9
překvapen	překvapit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgInPc1
americké	americký	k2eAgInPc1d1
letouny	letoun	k1gInPc1
byly	být	k5eAaImAgInP
perfektně	perfektně	k6eAd1
vyrovnány	vyrovnat	k5eAaBmNgInP,k5eAaPmNgInP
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
na	na	k7c4
ně	on	k3xPp3gInPc4
mohli	moct	k5eAaImAgMnP
zaútočit	zaútočit	k5eAaPmF
a	a	k8xC
my	my	k3xPp1nPc1
jsme	být	k5eAaImIp1nP
bombardovali	bombardovat	k5eAaImAgMnP
a	a	k8xC
postřelovali	postřelovat	k5eAaImAgMnP
a	a	k8xC
úplně	úplně	k6eAd1
všechny	všechen	k3xTgMnPc4
zničili	zničit	k5eAaPmAgMnP
<g/>
…	…	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Saburó	Saburó	k1gFnSc1
Sakai	Saka	k1gFnSc2
od	od	k7c2
Tainan	Tainana	k1gFnPc2
kókútai	kókúta	k1gFnSc2
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Clark	Clark	k1gInSc1
Field	Fielda	k1gFnPc2
a	a	k8xC
další	další	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Manily	Manila	k1gFnSc2
měly	mít	k5eAaImAgFnP
napadnout	napadnout	k5eAaPmF
letouny	letoun	k1gMnPc7
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
ze	z	k7c2
základen	základna	k1gFnPc2
na	na	k7c6
jihu	jih	k1gInSc6
Formosy	Formosa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
letiště	letiště	k1gNnSc1
Taiču	Taičus	k1gInSc2
<g/>
,	,	kIx,
Tainan	Tainan	k1gMnSc1
a	a	k8xC
Takao	Takao	k1gMnSc1
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
ale	ale	k8xC
zahalena	zahalen	k2eAgFnSc1d1
mlhou	mlha	k1gFnSc7
a	a	k8xC
tak	tak	k6eAd1
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
start	start	k1gInSc4
téměř	téměř	k6eAd1
dvou	dva	k4xCgNnPc2
set	sto	k4xCgNnPc2
letadel	letadlo	k1gNnPc2
odložen	odložit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
když	když	k8xS
se	se	k3xPyFc4
mlha	mlha	k1gFnSc1
zvedla	zvednout	k5eAaPmAgFnS
<g/>
,	,	kIx,
odstartovala	odstartovat	k5eAaPmAgFnS
první	první	k4xOgFnSc1
skupina	skupina	k1gFnSc1
25	#num#	k4
G3M	G3M	k1gMnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókúta	k1gFnSc6
(	(	kIx(
<g/>
jeden	jeden	k4xCgMnSc1
při	při	k7c6
startu	start	k1gInSc6
havaroval	havarovat	k5eAaPmAgMnS
<g/>
)	)	kIx)
a	a	k8xC
27	#num#	k4
G4M1	G4M1	k1gMnPc2
od	od	k7c2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
s	s	k7c7
eskortou	eskorta	k1gFnSc7
36	#num#	k4
A6M2	A6M2	k1gFnPc2
od	od	k7c2
Tainan	Tainana	k1gFnPc2
kókútai	kókúta	k1gFnSc2
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
se	se	k3xPyFc4
pro	pro	k7c4
poruchu	porucha	k1gFnSc4
vrátily	vrátit	k5eAaPmAgFnP
zpět	zpět	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skupina	skupina	k1gFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
a	a	k8xC
Del	Del	k1gFnSc2
Carmen	Carmen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovala	následovat	k5eAaImAgFnS
jí	on	k3xPp3gFnSc3
druhá	druhý	k4xOgFnSc1
skupina	skupina	k1gFnSc1
27	#num#	k4
G4M1	G4M1	k1gMnPc2
od	od	k7c2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
<g/>
,	,	kIx,
27	#num#	k4
G4M1	G4M1	k1gMnPc2
od	od	k7c2
Kanoja	Kanojum	k1gNnSc2
kókútai	kókútai	k6eAd1
a	a	k8xC
53	#num#	k4
A6M2	A6M2	k1gMnPc2
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
a	a	k8xC
Tainan	Tainan	k1gInSc4
kókútai	kókúta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skupina	skupina	k1gFnSc1
–	–	k?
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
pro	pro	k7c4
poruchy	porucha	k1gFnPc4
oddělila	oddělit	k5eAaPmAgFnS
a	a	k8xC
vrátila	vrátit	k5eAaPmAgFnS
zpět	zpět	k6eAd1
jedna	jeden	k4xCgFnSc1
G4M1	G4M1	k1gFnSc1
a	a	k8xC
dvě	dva	k4xCgFnPc4
A6M2	A6M2	k1gFnPc4
–	–	k?
měla	mít	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
Iba	iba	k6eAd1
Field	Fieldo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
první	první	k4xOgFnSc1
dorazila	dorazit	k5eAaPmAgFnS
nad	nad	k7c4
svůj	svůj	k3xOyFgInSc4
cíl	cíl	k1gInSc4
druhá	druhý	k4xOgFnSc1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
ve	v	k7c6
12	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
objevila	objevit	k5eAaPmAgFnS
nad	nad	k7c4
Iba	iba	k6eAd1
Field	Fielda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
zrovna	zrovna	k6eAd1
chystala	chystat	k5eAaImAgFnS
přistát	přistát	k5eAaImF,k5eAaPmF
jedna	jeden	k4xCgFnSc1
letka	letka	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
ale	ale	k8xC
A6M2	A6M2	k1gFnPc1
jí	on	k3xPp3gFnSc3
zmasakrovaly	zmasakrovat	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pumy	puma	k1gFnSc2
navíc	navíc	k6eAd1
zničily	zničit	k5eAaPmAgFnP
radarovou	radarový	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
12	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
nad	nad	k7c4
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stíhačky	stíhačka	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
startovaly	startovat	k5eAaBmAgInP
mezi	mezi	k7c7
vybuchujícími	vybuchující	k2eAgFnPc7d1
pumami	puma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
přišly	přijít	k5eAaPmAgFnP
na	na	k7c4
řadu	řada	k1gFnSc4
A	a	k9
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
postřelovaly	postřelovat	k5eAaImAgFnP
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vzduchu	vzduch	k1gInSc2
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
tři	tři	k4xCgFnPc4
P-	P-	k1gFnPc4
<g/>
40	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
piloti	pilot	k1gMnPc1
(	(	kIx(
<g/>
Moore	Moor	k1gMnSc5
<g/>
,	,	kIx,
Keator	Keator	k1gInSc4
a	a	k8xC
Gilmore	Gilmor	k1gInSc5
<g/>
)	)	kIx)
si	se	k3xPyFc3
nárokovali	nárokovat	k5eAaImAgMnP
čtyři	čtyři	k4xCgInPc4
sestřely	sestřel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývajících	zbývající	k2eAgMnPc2d1
20	#num#	k4
P-40B	P-40B	k1gMnPc2
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
bylo	být	k5eAaImAgNnS
zničeno	zničen	k2eAgNnSc1d1
nebo	nebo	k8xC
poškozeno	poškozen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
A-	A-	k1gFnSc2
<g/>
27	#num#	k4
<g/>
,	,	kIx,
B-10	B-10	k1gMnPc2
a	a	k8xC
B-18	B-18	k1gMnPc2
bylo	být	k5eAaImAgNnS
zničeno	zničit	k5eAaPmNgNnS
také	také	k9
14	#num#	k4
B-	B-	k1gFnPc2
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
boje	boj	k1gInSc2
zasáhlo	zasáhnout	k5eAaPmAgNnS
i	i	k9
pár	pár	k4xCyI
strojů	stroj	k1gInPc2
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
a	a	k8xC
šest	šest	k4xCc1
P-40E	P-40E	k1gMnPc2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
si	se	k3xPyFc3
nárokovaly	nárokovat	k5eAaImAgFnP
pět	pět	k4xCc4
sestřelených	sestřelený	k2eAgMnPc2d1
Japonců	Japonec	k1gMnPc2
při	při	k7c6
vlastní	vlastní	k2eAgFnSc6d1
ztrátě	ztráta	k1gFnSc6
dvou	dva	k4xCgInPc2
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Del	Del	k1gMnSc2
Carmen	Carmen	k1gInSc1
odstartovaly	odstartovat	k5eAaPmAgFnP
P-35A	P-35A	k1gFnPc1
od	od	k7c2
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeden	jeden	k4xCgInSc1
byl	být	k5eAaImAgInS
sestřelen	sestřelen	k2eAgMnSc1d1
a	a	k8xC
další	další	k2eAgMnSc1d1
musel	muset	k5eAaImAgMnS
nouzově	nouzově	k6eAd1
přistát	přistát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
P-35A	P-35A	k1gFnPc2
dokázaly	dokázat	k5eAaPmAgFnP
sestřelit	sestřelit	k5eAaPmF
dvě	dva	k4xCgFnPc1
nebo	nebo	k8xC
tři	tři	k4xCgFnPc1
A	a	k9
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
si	se	k3xPyFc3
–	–	k?
převážně	převážně	k6eAd1
nad	nad	k7c4
Clark	Clark	k1gInSc4
Fieldem	Field	k1gInSc7
–	–	k?
připsala	připsat	k5eAaPmAgFnS
sedm	sedm	k4xCc4
sestřelů	sestřel	k1gInPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
tři	tři	k4xCgFnPc1
pravděpodobné	pravděpodobný	k2eAgFnPc1d1
při	při	k7c6
vlastní	vlastní	k2eAgFnSc6d1
ztrátě	ztráta	k1gFnSc6
tří	tři	k4xCgInPc2
A	A	kA
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zemi	zem	k1gFnSc6
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k1gNnSc6
zničila	zničit	k5eAaPmAgFnS
nebo	nebo	k8xC
zapálila	zapálit	k5eAaPmAgFnS
22	#num#	k4
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
B-	B-	k1gFnPc2
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Tainan	Tainany	k1gInPc2
kókútai	kókútai	k6eAd1
útočila	útočit	k5eAaImAgFnS
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gMnSc1
a	a	k8xC
Del	Del	k1gMnSc1
Carmen	Carmen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztratila	ztratit	k5eAaPmAgFnS
při	při	k7c6
tom	ten	k3xDgNnSc6
čtyři	čtyři	k4xCgFnPc1
A6M2	A6M2	k1gFnPc1
i	i	k9
s	s	k7c7
piloty	pilot	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnPc1
piloti	pilot	k1gMnPc1
si	se	k3xPyFc3
nárokovali	nárokovat	k5eAaImAgMnP
osm	osm	k4xCc4
jistých	jistý	k2eAgInPc2d1
a	a	k8xC
čtyři	čtyři	k4xCgInPc4
pravděpodobné	pravděpodobný	k2eAgInPc4d1
sestřely	sestřel	k1gInPc4
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
letoun	letoun	k1gInSc4
havarovaný	havarovaný	k2eAgInSc4d1
při	při	k7c6
přistání	přistání	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgInPc2d1
25	#num#	k4
zničených	zničený	k2eAgInPc2d1
<g/>
,	,	kIx,
zapálených	zapálený	k2eAgInPc2d1
nebo	nebo	k8xC
poškozených	poškozený	k2eAgInPc2d1
letounů	letoun	k1gInPc2
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledek	výsledek	k1gInSc1
náletu	nálet	k1gInSc2
byl	být	k5eAaImAgInS
pro	pro	k7c4
americké	americký	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
na	na	k7c6
Luzonu	Luzon	k1gInSc6
zdrcující	zdrcující	k2eAgNnSc1d1
<g/>
:	:	kIx,
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
čtvrtinu	čtvrtina	k1gFnSc4
stíhačů	stíhač	k1gMnPc2
(	(	kIx(
<g/>
po	po	k7c6
útoku	útok	k1gInSc6
letuschopných	letuschopný	k2eAgFnPc2d1
pouze	pouze	k6eAd1
58	#num#	k4
z	z	k7c2
92	#num#	k4
operačních	operační	k2eAgFnPc2d1
P-40	P-40	k1gFnPc2
před	před	k7c7
náletem	nálet	k1gInSc7
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
pouze	pouze	k6eAd1
jediná	jediný	k2eAgFnSc1d1
B-17	B-17	k1gFnSc1
z	z	k7c2
Clark	Clark	k1gInSc4
Fieldu	Fielda	k1gFnSc4
zůstala	zůstat	k5eAaPmAgFnS
použitelná	použitelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
náletu	nálet	k1gInSc2
bylo	být	k5eAaImAgNnS
na	na	k7c6
zemi	zem	k1gFnSc6
zabito	zabít	k5eAaPmNgNnS
asi	asi	k9
100	#num#	k4
Američanů	Američan	k1gMnPc2
a	a	k8xC
Filipínců	Filipínec	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
250	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clark	Clark	k1gInSc1
Field	Field	k1gInSc4
byl	být	k5eAaImAgInS
jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgFnSc1d1
základna	základna	k1gFnSc1
opuštěn	opuštěn	k2eAgInSc4d1
a	a	k8xC
nadále	nadále	k6eAd1
sloužil	sloužit	k5eAaImAgInS
již	již	k6eAd1
jenom	jenom	k9
jako	jako	k9
předsunuté	předsunutý	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Večer	večer	k6eAd1
ještě	ještě	k6eAd1
(	(	kIx(
<g/>
podle	podle	k7c2
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	i	k9
<g/>
)	)	kIx)
devět	devět	k4xCc1
G3M	G3M	k1gFnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
zaútočilo	zaútočit	k5eAaPmAgNnS
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Field	k1gMnSc1
a	a	k8xC
Fort	Fort	k?
McKinley	McKinlea	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Bartsch	Bartscha	k1gFnPc2
I	I	kA
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
pouze	pouze	k6eAd1
o	o	k7c4
sedm	sedm	k4xCc4
G3M	G3M	k1gFnPc2
a	a	k8xC
čas	čas	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ztráta	ztráta	k1gFnSc1
Batanu	Batan	k1gInSc2
a	a	k8xC
námořní	námořní	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
</s>
<s>
CDR	CDR	kA
William	William	k1gInSc1
A.	A.	kA
Glassford	Glassford	k1gInSc4
ještě	ještě	k6eAd1
jako	jako	k8xS,k8xC
velitel	velitel	k1gMnSc1
32	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
torpédoborců	torpédoborec	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
</s>
<s>
USS	USS	kA
Houston	Houston	k1gInSc1
(	(	kIx(
<g/>
CA-	CA-	k1gFnSc1
<g/>
30	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
říjnu	říjen	k1gInSc6
1935	#num#	k4
</s>
<s>
Ráno	ráno	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
vylodilo	vylodit	k5eAaPmAgNnS
490	#num#	k4
japonských	japonský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Batan	Batana	k1gFnPc2
v	v	k7c6
souostroví	souostroví	k1gNnSc6
Batanes	Batanesa	k1gFnPc2
severně	severně	k6eAd1
od	od	k7c2
Luzonu	Luzon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylodění	vylodění	k1gNnSc2
podporovaly	podporovat	k5eAaImAgFnP
torpédoborec	torpédoborec	k1gInSc4
Jamagumo	Jamaguma	k1gFnSc5
<g/>
,	,	kIx,
torpédovky	torpédovka	k1gFnPc1
Manazuru	Manazur	k1gInSc2
a	a	k8xC
Hacukari	Hacukar	k1gFnSc2
<g/>
,	,	kIx,
mateřská	mateřský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
hydroplánů	hydroplán	k1gInPc2
Sanuki	Sanuke	k1gFnSc4
Maru	Maru	k1gFnSc2
a	a	k8xC
minolovky	minolovka	k1gFnSc2
W-13	W-13	k1gFnSc2
a	a	k8xC
W-	W-	k1gFnSc7
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Japonci	Japonec	k1gMnPc1
se	se	k3xPyFc4
nesetkali	setkat	k5eNaPmAgMnP
s	s	k7c7
odporem	odpor	k1gInSc7
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
rychle	rychle	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
letiště	letiště	k1gNnSc4
u	u	k7c2
Basco	Basco	k6eAd1
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
plánovali	plánovat	k5eAaImAgMnP
využít	využít	k5eAaPmF
pro	pro	k7c4
podporu	podpora	k1gFnSc4
vylodění	vylodění	k1gNnSc2
na	na	k7c6
severu	sever	k1gInSc6
Luzonu	Luzon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
z	z	k7c2
něj	on	k3xPp3gInSc2
začaly	začít	k5eAaPmAgInP
operovat	operovat	k5eAaImF
stíhací	stíhací	k2eAgFnSc7d1
Ki-	Ki-	k1gFnSc7
<g/>
27	#num#	k4
24	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Letiště	letiště	k1gNnSc1
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
malé	malý	k2eAgNnSc1d1
a	a	k8xC
práce	práce	k1gFnSc1
na	na	k7c6
rozšíření	rozšíření	k1gNnSc6
se	se	k3xPyFc4
záhy	záhy	k6eAd1
zastavily	zastavit	k5eAaPmAgFnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
po	po	k7c6
úspěšném	úspěšný	k2eAgInSc6d1
útoku	útok	k1gInSc6
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Fieldo	k1gNnPc2
došli	dojít	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
nebudou	být	k5eNaImBp3nP
potřebovat	potřebovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
vyvázlo	vyváznout	k5eAaPmAgNnS
z	z	k7c2
prvního	první	k4xOgInSc2
úderu	úder	k1gInSc2
–	–	k?
až	až	k9
na	na	k7c4
lehké	lehký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
způsobené	způsobený	k2eAgFnPc4d1
náletem	nálet	k1gInSc7
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
–	–	k?
nepoškozeno	poškozen	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontradmirál	kontradmirál	k1gMnSc1
William	William	k1gInSc1
A.	A.	kA
Glassford	Glassford	k1gMnSc1
soustředil	soustředit	k5eAaPmAgMnS
na	na	k7c4
Panay	Pana	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
Houston	Houston	k1gInSc1
(	(	kIx(
<g/>
vlajková	vlajkový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Task	Task	k1gInSc1
Force	force	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Boise	Boise	k1gFnSc1
spolu	spolu	k6eAd1
se	s	k7c7
zásobovací	zásobovací	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
hydroplánů	hydroplán	k1gInPc2
Langley	Langley	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozkaz	rozkaz	k1gInSc4
admirála	admirál	k1gMnSc2
Harta	Hart	k1gInSc2
s	s	k7c7
nimi	on	k3xPp3gMnPc7
večer	večer	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
odplul	odplout	k5eAaPmAgMnS
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napřed	napřed	k6eAd1
do	do	k7c2
Holandského	holandský	k2eAgNnSc2d1
Bornea	Borneo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gNnSc3
připojili	připojit	k5eAaPmAgMnP
další	další	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
a	a	k8xC
poté	poté	k6eAd1
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byly	být	k5eAaImAgFnP
námořní	námořní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
redukovány	redukován	k2eAgFnPc1d1
na	na	k7c4
několik	několik	k4yIc4
starých	starý	k2eAgInPc2d1
torpédoborců	torpédoborec	k1gInPc2
<g/>
,	,	kIx,
minolovek	minolovka	k1gFnPc2
<g/>
,	,	kIx,
torpédových	torpédový	k2eAgInPc2d1
a	a	k8xC
dělových	dělový	k2eAgInPc2d1
člunů	člun	k1gInPc2
a	a	k8xC
ponorky	ponorka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
byl	být	k5eAaImAgMnS
klid	klid	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
špatné	špatný	k2eAgNnSc1d1
počasí	počasí	k1gNnSc1
nad	nad	k7c7
Formosou	Formosa	k1gFnSc7
zabránilo	zabránit	k5eAaPmAgNnS
dalším	další	k2eAgMnPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
ale	ale	k8xC
Američané	Američan	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
tři	tři	k4xCgInPc4
stroje	stroj	k1gInPc4
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
P-40E	P-40E	k1gFnPc1
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
a	a	k8xC
jedna	jeden	k4xCgFnSc1
B-	B-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
)	)	kIx)
při	při	k7c6
letecké	letecký	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
jeden	jeden	k4xCgMnSc1
O-52	O-52	k1gMnSc1
byl	být	k5eAaImAgMnS
sestřelen	sestřelit	k5eAaPmNgMnS
„	„	k?
<g/>
přátelskou	přátelský	k2eAgFnSc7d1
palbou	palba	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
byla	být	k5eAaImAgFnS
poškozena	poškodit	k5eAaPmNgFnS
i	i	k9
Catalina	Catalina	k1gFnSc1
102-P-21	102-P-21	k4
během	během	k7c2
přiblížení	přiblížení	k1gNnSc2
na	na	k7c4
přistání	přistání	k1gNnSc4
v	v	k7c6
Subic	Subice	k1gFnPc2
Bay	Bay	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Třetí	třetí	k4xOgMnPc1
PS	PS	kA
de	de	k?
facto	facto	k1gNnSc1
zanikla	zaniknout	k5eAaPmAgFnS
předáním	předání	k1gNnSc7
posledních	poslední	k2eAgFnPc2d1
dvou	dva	k4xCgFnPc2
zbývajících	zbývající	k2eAgFnPc2d1
P-40	P-40	k1gFnPc2
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
Japoncům	Japonec	k1gMnPc3
se	se	k3xPyFc4
ztráty	ztráta	k1gFnPc1
nevyhnuly	vyhnout	k5eNaPmAgFnP
<g/>
:	:	kIx,
pravděpodobně	pravděpodobně	k6eAd1
kvůli	kvůli	k7c3
špatnému	špatný	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
se	se	k3xPyFc4
z	z	k7c2
průzkumného	průzkumný	k2eAgInSc2d1
letu	let	k1gInSc2
nevrátily	vrátit	k5eNaPmAgFnP
dvě	dva	k4xCgFnPc1
G	G	kA
<g/>
3	#num#	k4
<g/>
M.	M.	kA
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američané	Američan	k1gMnPc1
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
posílit	posílit	k5eAaPmF
protiletadlovou	protiletadlový	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
oblasti	oblast	k1gFnSc2
kolem	kolem	k7c2
Manily	Manila	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
stáhli	stáhnout	k5eAaPmAgMnP
200	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
později	pozdě	k6eAd2
přeznačen	přeznačit	k5eAaPmNgInS
na	na	k7c4
515	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
pobřežní	pobřežní	k2eAgInSc1d1
dělostřelecký	dělostřelecký	k2eAgInSc1d1
(	(	kIx(
<g/>
protiletadlový	protiletadlový	k2eAgInSc1d1
<g/>
)	)	kIx)
pluk	pluk	k1gInSc1
z	z	k7c2
Clark	Clark	k1gInSc4
Fieldu	Fielda	k1gFnSc4
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
pobřežní	pobřežní	k2eAgInSc1d1
dělostřelecký	dělostřelecký	k2eAgInSc1d1
(	(	kIx(
<g/>
protiletadlový	protiletadlový	k2eAgInSc1d1
<g/>
)	)	kIx)
pluk	pluk	k1gInSc1
z	z	k7c2
Corregidoru	Corregidor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
odstartovala	odstartovat	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
B-17	B-17	k1gFnSc1
z	z	k7c2
Clark	Clark	k1gInSc4
Fieldu	Fielda	k1gFnSc4
k	k	k7c3
průzkumnému	průzkumný	k2eAgInSc3d1
letu	let	k1gInSc3
nad	nad	k7c4
Formosu	Formosa	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
mechanické	mechanický	k2eAgFnSc3d1
závadě	závada	k1gFnSc3
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
vrátit	vrátit	k5eAaPmF
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
severním	severní	k2eAgInSc7d1
Luzonem	Luzon	k1gInSc7
hlídkovaly	hlídkovat	k5eAaImAgFnP
stíhačky	stíhačka	k1gFnPc1
a	a	k8xC
východně	východně	k6eAd1
a	a	k8xC
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Luzonu	Luzon	k1gInSc2
hlídkovaly	hlídkovat	k5eAaImAgFnP
námořní	námořní	k2eAgFnPc1d1
PBY-4	PBY-4	k1gFnPc1
PatWing-	PatWing-	k1gFnSc1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nic	nic	k6eAd1
ale	ale	k8xC
nenašly	najít	k5eNaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
dne	den	k1gInSc2
odstartovalo	odstartovat	k5eAaPmAgNnS
z	z	k7c2
Del	Del	k1gFnSc2
Monte	Mont	k1gInSc5
na	na	k7c6
Mindanau	Mindanaus	k1gInSc6
13	#num#	k4
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
17	#num#	k4
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
B-17	B-17	k1gFnPc4
a	a	k8xC
přeletěly	přeletět	k5eAaPmAgFnP
na	na	k7c4
Luzon	Luzon	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
zúčastnit	zúčastnit	k5eAaPmF
plánovaného	plánovaný	k2eAgInSc2d1
útoku	útok	k1gInSc2
na	na	k7c6
Formosu	Formos	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
naplánován	naplánovat	k5eAaBmNgInS
na	na	k7c6
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
devět	devět	k4xCc4
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
B-17	B-17	k1gMnPc2
přistálo	přistát	k5eAaImAgNnS,k5eAaPmAgNnS
na	na	k7c4
Clark	Clark	k1gInSc4
Fieldu	Field	k1gInSc2
ve	v	k7c6
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dotankování	dotankování	k1gNnSc6
opět	opět	k6eAd1
vzlétly	vzlétnout	k5eAaPmAgInP
a	a	k8xC
kroužily	kroužit	k5eAaImAgInP
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
až	až	k9
do	do	k7c2
setmění	setmění	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	k9
je	být	k5eAaImIp3nS
případný	případný	k2eAgInSc1d1
nálet	nálet	k1gInSc1
nezastihl	zastihnout	k5eNaPmAgInS
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývajících	zbývající	k2eAgMnPc2d1
sedm	sedm	k4xCc1
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
osm	osm	k4xCc4
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
B-17	B-17	k1gMnPc2
přistálo	přistát	k5eAaImAgNnS,k5eAaPmAgNnS
na	na	k7c6
záložním	záložní	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
San	San	k1gFnSc1
Marcelino	Marcelin	k2eAgNnSc4d1
na	na	k7c6
západě	západ	k1gInSc6
Luzonu	Luzon	k1gInSc2
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
ráno	ráno	k6eAd1
přeletěly	přeletět	k5eAaPmAgFnP
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
plánovaného	plánovaný	k2eAgInSc2d1
úderu	úder	k1gInSc2
na	na	k7c6
Formosu	Formos	k1gInSc6
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
ale	ale	k8xC
sešlo	sejít	k5eAaPmAgNnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
začala	začít	k5eAaPmAgFnS
invaze	invaze	k1gFnSc1
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Invaze	invaze	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
</s>
<s>
První	první	k4xOgNnSc1
vylodění	vylodění	k1gNnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Schéma	schéma	k1gNnSc1
japonského	japonský	k2eAgInSc2d1
postupu	postup	k1gInSc2
na	na	k7c6
Luzonu	Luzon	k1gInSc6
</s>
<s>
Otazníky	otazník	k1gInPc1
„	„	k?
<g/>
???	???	k?
<g/>
“	“	k?
místo	místo	k1gNnSc4
jmen	jméno	k1gNnPc2
lodí	loď	k1gFnPc2
znamenají	znamenat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
konkrétní	konkrétní	k2eAgNnSc1d1
plavidlo	plavidlo	k1gNnSc1
dohledat	dohledat	k5eAaPmF
a	a	k8xC
že	že	k8xS
se	se	k3xPyFc4
i	i	k9
Dull	Dull	k1gMnSc1
může	moct	k5eAaImIp3nS
mýlit	mýlit	k5eAaImF
v	v	k7c6
počtu	počet	k1gInSc6
(	(	kIx(
<g/>
či	či	k8xC
typu	typ	k1gInSc2
<g/>
)	)	kIx)
uváděných	uváděný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Combinedfleet	Combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
například	například	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
pohyby	pohyb	k1gInPc4
všech	všecek	k3xTgMnPc2
stíhačů	stíhač	k1gMnPc2
ponorek	ponorka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
měli	mít	k5eAaImAgMnP
Japonci	Japonec	k1gMnPc1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
a	a	k8xC
žádné	žádný	k3yNgMnPc4
další	další	k2eAgMnPc4d1
stíhače	stíhač	k1gMnPc4
se	se	k3xPyFc4
u	u	k7c2
Aparri	Aparr	k1gFnSc2
podle	podle	k7c2
combinedfleet	combinedfleeta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
nevyskytovaly	vyskytovat	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohlo	moct	k5eAaImAgNnS
se	se	k3xPyFc4
ale	ale	k9
třeba	třeba	k6eAd1
jednat	jednat	k5eAaImF
o	o	k7c4
pomocné	pomocný	k2eAgMnPc4d1
stíhače	stíhač	k1gMnPc4
ponorek	ponorka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
combinedfleet	combinedfleet	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
neuvádí	uvádět	k5eNaImIp3nS
</s>
<s>
Ráno	ráno	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
proběhla	proběhnout	k5eAaPmAgNnP
současně	současně	k6eAd1
tři	tři	k4xCgNnPc1
vylodění	vylodění	k1gNnPc1
na	na	k7c6
severu	sever	k1gInSc6
Luzonu	Luzon	k1gInSc2
u	u	k7c2
Aparri	Aparr	k1gFnSc2
a	a	k8xC
Viganu	Vigan	k1gInSc2
a	a	k8xC
na	na	k7c6
ostrově	ostrov	k1gInSc6
Gamiguin	Gamiguina	k1gFnPc2
(	(	kIx(
<g/>
18	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
121	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
vzdálené	vzdálený	k2eAgNnSc4d1
krytí	krytí	k1gNnSc4
vylodění	vylodění	k1gNnSc2
u	u	k7c2
Aparri	Aparr	k1gFnSc2
a	a	k8xC
Viganu	Vigan	k1gInSc2
sloužila	sloužit	k5eAaImAgFnS
eskadra	eskadra	k1gFnSc1
viceadmirála	viceadmirál	k1gMnSc2
Ibó	Ibó	k1gMnSc2
Takahašiho	Takahaši	k1gMnSc2
složená	složený	k2eAgFnSc1d1
z	z	k7c2
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
Ašigara	Ašigara	k1gFnSc1
a	a	k8xC
Maja	Maja	k1gFnSc1
<g/>
,	,	kIx,
lehkého	lehký	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
Kuma	kum	k1gMnSc2
a	a	k8xC
torpédoborců	torpédoborec	k1gMnPc2
Asakaze	Asakaha	k1gFnSc6
a	a	k8xC
Macukaze	Macukaha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostrov	ostrov	k1gInSc1
Gamiguin	Gamiguina	k1gFnPc2
</s>
<s>
Ráno	ráno	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
ostrov	ostrov	k1gInSc4
Batan	Batany	k1gInPc2
<g/>
,	,	kIx,
vylodili	vylodit	k5eAaPmAgMnP
na	na	k7c6
ostrově	ostrov	k1gInSc6
Gamiguin	Gamiguin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morton	Morton	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
vojáky	voják	k1gMnPc4
3	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
dělových	dělový	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
Hackett	Hackett	k2eAgInSc1d1
&	&	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
dvě	dva	k4xCgFnPc4
čety	četa	k1gFnPc4
sasebské	sasebský	k2eAgNnSc1d1
kaigun	kaigun	k1gNnSc1
tokubecu	tokubecus	k1gInSc2
rikusentai	rikusenta	k1gFnSc2
(	(	kIx(
<g/>
海	海	k?
~	~	kIx~
Speciální	speciální	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
vyloďovací	vyloďovací	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Vylodění	vylodění	k1gNnSc1
vojáků	voják	k1gMnPc2
z	z	k7c2
nosiče	nosič	k1gInSc2
hydroplánů	hydroplán	k1gInPc2
Sanuki	Sanuk	k1gFnSc2
Maru	Maru	k1gFnSc2
podporoval	podporovat	k5eAaImAgInS
torpédoborec	torpédoborec	k1gInSc1
Tačikaze	Tačikaha	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Japonské	japonský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
zřídilo	zřídit	k5eAaPmAgNnS
na	na	k7c4
Gamiguin	Gamiguin	k1gInSc4
základnu	základna	k1gFnSc4
hydroplánů	hydroplán	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
ale	ale	k9
neposkytovala	poskytovat	k5eNaImAgFnS
letounům	letoun	k1gMnPc3
dostatečnou	dostatečný	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
posléze	posléze	k6eAd1
opuštěna	opuštěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aparri	Aparri	k6eAd1
</s>
<s>
U	u	k7c2
Aparri	Aparr	k1gFnSc2
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
vylodit	vylodit	k5eAaPmF
„	„	k?
<g/>
skupina	skupina	k1gFnSc1
Tanaka	Tanak	k1gMnSc2
<g/>
“	“	k?
<g/>
:	:	kIx,
část	část	k1gFnSc1
(	(	kIx(
<g/>
štáb	štáb	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
a	a	k8xC
polovina	polovina	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
asi	asi	k9
2000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
2	#num#	k4
<g/>
.	.	kIx.
formoského	formoský	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
48	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
plukovníka	plukovník	k1gMnSc2
Toru	torus	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Tanaky	Tanak	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Vylodění	vylodění	k1gNnSc2
zajišťoval	zajišťovat	k5eAaImAgInS
svaz	svaz	k1gInSc1
kontradmirála	kontradmirál	k1gMnSc2
Kenzaburo	Kenzabura	k1gFnSc5
Hary	Hara	k1gFnSc2
složený	složený	k2eAgInSc4d1
z	z	k7c2
lehkého	lehký	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
Natori	Nator	k1gFnSc2
(	(	kIx(
<g/>
vlajkový	vlajkový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
torpédoborců	torpédoborec	k1gInPc2
Fumizuki	Fumizuki	k1gNnSc2
<g/>
,	,	kIx,
Nagacuki	Nagacuki	k1gNnSc2
<g/>
,	,	kIx,
Sacuki	Sacuki	k1gNnSc2
<g/>
,	,	kIx,
Minazuki	Minazuki	k1gNnSc2
<g/>
,	,	kIx,
Harukaze	Harukaha	k1gFnSc3
a	a	k8xC
Hatakaze	Hatakaha	k1gFnSc3
<g/>
,	,	kIx,
tří	tři	k4xCgFnPc2
minolovek	minolovka	k1gFnPc2
(	(	kIx(
<g/>
W-	W-	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
15	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
W-	W-	k1gFnSc1
<g/>
16	#num#	k4
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
W-	W-	k1gFnSc1
<g/>
19	#num#	k4
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
devíti	devět	k4xCc3
stíhačů	stíhač	k1gMnPc2
ponorek	ponorka	k1gFnPc2
(	(	kIx(
<g/>
CH-	CH-	k1gFnPc2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
CH-2	CH-2	k1gFnSc1
a	a	k8xC
CH-3	CH-3	k1gFnSc1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
protiponorkové	protiponorkový	k2eAgFnPc4d1
eskadry	eskadra	k1gFnPc4
<g/>
,	,	kIx,
CH-	CH-	k1gFnSc1
<g/>
13	#num#	k4
<g/>
,	,	kIx,
CH-14	CH-14	k1gFnSc1
a	a	k8xC
CH-15	CH-15	k1gFnSc1
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
protiponorkové	protiponorkový	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
a	a	k8xC
???	???	k?
<g/>
,	,	kIx,
???	???	k?
a	a	k8xC
???	???	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
šesti	šest	k4xCc2
transportních	transportní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
vylodění	vylodění	k1gNnSc2
bylo	být	k5eAaImAgNnS
letiště	letiště	k1gNnSc4
v	v	k7c6
Aparri	Aparr	k1gInSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
pro	pro	k7c4
postup	postup	k1gInSc4
na	na	k7c4
Manilu	Manila	k1gFnSc4
se	se	k3xPyFc4
Aparri	Aparri	k1gNnSc1
nehodilo	hodit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
v	v	k7c6
Aparri	Aparr	k1gFnSc6
prakticky	prakticky	k6eAd1
neexistoval	existovat	k5eNaImAgMnS
(	(	kIx(
<g/>
nacházela	nacházet	k5eAaImAgFnS
se	se	k3xPyFc4
tu	tu	k6eAd1
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
rota	rota	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ustoupila	ustoupit	k5eAaPmAgFnS
bez	bez	k7c2
boje	boj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
Aparri	Aparr	k1gFnSc6
vylodily	vylodit	k5eAaPmAgFnP
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
roty	rota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
špatnému	špatný	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
bylo	být	k5eAaImAgNnS
vyloďování	vyloďování	k1gNnSc4
dalších	další	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
Aparri	Aparr	k1gFnSc6
zastaveno	zastavit	k5eAaPmNgNnS
a	a	k8xC
zbytek	zbytek	k1gInSc1
vojáků	voják	k1gMnPc2
byl	být	k5eAaImAgInS
vyloděn	vyloděn	k2eAgInSc1d1
v	v	k7c4
Gonzaga	Gonzag	k1gMnSc4
východně	východně	k6eAd1
od	od	k7c2
Aparri	Aparr	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americkou	americký	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
na	na	k7c4
vylodění	vylodění	k1gNnSc4
v	v	k7c6
Aparri	Aparr	k1gFnSc6
bylo	být	k5eAaImAgNnS
pouze	pouze	k6eAd1
nařízení	nařízení	k1gNnSc1
leteckých	letecký	k2eAgInPc2d1
útoků	útok	k1gInPc2
MacArthurem	MacArthur	k1gInSc7
a	a	k8xC
Wainwright	Wainwright	k1gMnSc1
vyslal	vyslat	k5eAaPmAgMnS
několik	několik	k4yIc4
průzkumných	průzkumný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
26	#num#	k4
<g/>
.	.	kIx.
kavalérie	kavalérie	k1gFnSc2
FS	FS	kA
pro	pro	k7c4
spojení	spojení	k1gNnSc4
s	s	k7c7
11	#num#	k4
<g/>
.	.	kIx.
divizí	divize	k1gFnPc2
na	na	k7c6
severu	sever	k1gInSc6
Luzonu	Luzon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MacArthur	MacArthur	k1gMnSc1
rovněž	rovněž	k9
nařídil	nařídit	k5eAaPmAgMnS
zničit	zničit	k5eAaPmF
mosty	most	k1gInPc4
v	v	k7c6
údolí	údolí	k1gNnSc6
Cagayan	Cagayana	k1gFnPc2
a	a	k8xC
bránit	bránit	k5eAaImF
průsmyk	průsmyk	k1gInSc4
Balete	balet	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
spojoval	spojovat	k5eAaImAgInS
silnicí	silnice	k1gFnSc7
č.	č.	k?
5	#num#	k4
údolí	údolí	k1gNnPc4
Cagayan	Cagayany	k1gInPc2
s	s	k7c7
jihem	jih	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
Harovy	Harův	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
zaútočily	zaútočit	k5eAaPmAgFnP
tři	tři	k4xCgInPc1
B-17C	B-17C	k1gMnPc2
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BS	BS	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
zasáhly	zasáhnout	k5eAaPmAgFnP
minolovku	minolovka	k1gFnSc4
W-	W-	k1gFnSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
musela	muset	k5eAaImAgFnS
najet	najet	k5eAaPmF
na	na	k7c4
břeh	břeh	k1gInSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
odepsána	odepsán	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
totální	totální	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Blízkými	blízký	k2eAgInPc7d1
dopady	dopad	k1gInPc7
pum	puma	k1gFnPc2
byly	být	k5eAaImAgInP
rovněž	rovněž	k9
lehce	lehko	k6eAd1
poškozeny	poškodit	k5eAaPmNgFnP
Natori	Nator	k1gFnPc1
a	a	k8xC
Harukaze	Harukaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
leteckému	letecký	k2eAgInSc3d1
útoku	útok	k1gInSc3
se	se	k3xPyFc4
Hara	Hara	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
přerušit	přerušit	k5eAaPmF
vyloďování	vyloďování	k1gNnSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
a	a	k8xC
odplout	odplout	k5eAaPmF
do	do	k7c2
bezpečí	bezpečí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
nevyložených	vyložený	k2eNgFnPc2d1
zásob	zásoba	k1gFnPc2
byl	být	k5eAaImAgInS
hozen	hodit	k5eAaPmNgMnS
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dodriftoval	dodriftovat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
k	k	k7c3
pobřeží	pobřeží	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnPc4
tři	tři	k4xCgFnPc4
stíhačky	stíhačka	k1gFnPc4
Ki-	Ki-	k1gFnSc4
<g/>
27	#num#	k4
50	#num#	k4
<g/>
.	.	kIx.
čútai	čúta	k1gFnPc1
přistály	přistát	k5eAaPmAgFnP,k5eAaImAgFnP
na	na	k7c6
letišti	letiště	k1gNnSc6
v	v	k7c6
Aparri	Aparr	k1gFnSc6
pozdě	pozdě	k6eAd1
večer	večer	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vylodění	vylodění	k1gNnSc6
v	v	k7c6
Aparri	Aparri	k1gNnSc6
a	a	k8xC
Gonzaga	Gonzaga	k1gFnSc1
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
Tanaka	Tanak	k1gMnSc2
rozdělila	rozdělit	k5eAaPmAgFnS
<g/>
:	:	kIx,
část	část	k1gFnSc4
postupovala	postupovat	k5eAaImAgFnS
na	na	k7c4
jih	jih	k1gInSc4
po	po	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
5	#num#	k4
na	na	k7c4
Tuguegarao	Tuguegarao	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
s	s	k7c7
letištěm	letiště	k1gNnSc7
obsadila	obsadit	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
čistila	čistit	k5eAaImAgFnS
mys	mys	k1gInSc4
Engañ	Engañ	k6eAd1
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
a	a	k8xC
třetí	třetí	k4xOgFnSc1
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
západ	západ	k1gInSc4
po	po	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
3	#num#	k4
zajistit	zajistit	k5eAaPmF
letiště	letiště	k1gNnPc1
v	v	k7c6
Laoagu	Laoag	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vigan	Vigan	k1gMnSc1
</s>
<s>
U	u	k7c2
Viganu	Vigan	k1gInSc2
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
vylodit	vylodit	k5eAaPmF
„	„	k?
<g/>
skupina	skupina	k1gFnSc1
Kanno	Kanno	k6eAd1
<g/>
“	“	k?
<g/>
:	:	kIx,
polovina	polovina	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
asi	asi	k9
2000	#num#	k4
mužů	muž	k1gMnPc2
2	#num#	k4
<g/>
.	.	kIx.
formoského	formoský	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
48	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
Aparri	Aparr	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
bylo	být	k5eAaImAgNnS
i	i	k9
u	u	k7c2
Viganu	Vigan	k1gInSc2
hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
obsazení	obsazení	k1gNnSc2
letiště	letiště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Vylodění	vylodění	k1gNnSc2
zajišťoval	zajišťovat	k5eAaImAgInS
svaz	svaz	k1gInSc1
kontradmirála	kontradmirál	k1gMnSc2
Šodži	Šodž	k1gFnSc6
Nišimury	Nišimura	k1gFnSc2
složený	složený	k2eAgInSc4d1
z	z	k7c2
lehkého	lehký	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
Naka	Nak	k1gInSc2
(	(	kIx(
<g/>
vlajkový	vlajkový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
torpédoborců	torpédoborec	k1gInPc2
Murasame	Murasam	k1gInSc5
<g/>
,	,	kIx,
Júdači	Júdač	k1gMnPc5
<g/>
,	,	kIx,
Harusame	Harusam	k1gInSc5
<g/>
,	,	kIx,
Samidare	Samidar	k1gMnSc5
<g/>
,	,	kIx,
Asagumo	Asaguma	k1gFnSc5
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Minegumo	Mineguma	k1gFnSc5
a	a	k8xC
Nacugumo	Nacuguma	k1gFnSc5
<g/>
,	,	kIx,
šesti	šest	k4xCc2
minolovek	minolovka	k1gFnPc2
(	(	kIx(
<g/>
W-	W-	k1gFnPc2
<g/>
9	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
W-	W-	k1gFnPc2
<g/>
10	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
W-	W-	k1gFnPc2
<g/>
11	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
W-	W-	k1gFnPc2
<g/>
12	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
107	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
???	???	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
devíti	devět	k4xCc3
stíhačů	stíhač	k1gMnPc2
ponorek	ponorka	k1gFnPc2
(	(	kIx(
<g/>
CH-	CH-	k1gFnPc2
<g/>
10	#num#	k4
<g/>
,	,	kIx,
CH-11	CH-11	k1gFnSc1
a	a	k8xC
CH-12	CH-12	k1gFnSc1
z	z	k7c2
31	#num#	k4
<g/>
.	.	kIx.
protiponorkové	protiponorkový	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
a	a	k8xC
CH-	CH-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
,	,	kIx,
CH-	CH-	k1gFnSc1
<g/>
5	#num#	k4
<g/>
,	,	kIx,
CH-	CH-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
,	,	kIx,
CH-	CH-	k1gFnSc1
<g/>
16	#num#	k4
<g/>
,	,	kIx,
CH-17	CH-17	k1gFnSc1
a	a	k8xC
CH-18	CH-18	k1gFnSc1
z	z	k7c2
21	#num#	k4
<g/>
.	.	kIx.
protiponorkové	protiponorkový	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
šesti	šest	k4xCc2
transportních	transportní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
vyloďovat	vyloďovat	k5eAaImF
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Viganu	Vigan	k1gInSc2
u	u	k7c2
ústí	ústí	k1gNnSc2
řeky	řeka	k1gFnSc2
Abra	Abr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
rozbouřenému	rozbouřený	k2eAgNnSc3d1
moři	moře	k1gNnSc3
a	a	k8xC
americkým	americký	k2eAgInPc3d1
náletům	nálet	k1gInPc3
se	se	k3xPyFc4
na	na	k7c4
břeh	břeh	k1gInSc4
dostala	dostat	k5eAaPmAgFnS
pouze	pouze	k6eAd1
část	část	k1gFnSc1
invazních	invazní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
v	v	k7c6
10	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
zmocnily	zmocnit	k5eAaPmAgFnP
Viganu	Vigan	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obránci	obránce	k1gMnPc1
byli	být	k5eAaImAgMnP
o	o	k7c6
vylodění	vylodění	k1gNnSc6
informováni	informovat	k5eAaBmNgMnP
pilotem	pilot	k1gInSc7
P-	P-	k1gFnSc2
<g/>
40	#num#	k4
<g/>
E	E	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
ve	v	k7c6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
vyslán	vyslat	k5eAaPmNgInS
z	z	k7c2
Nichols	Nicholsa	k1gFnPc2
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ověřil	ověřit	k5eAaPmAgMnS
situaci	situace	k1gFnSc4
u	u	k7c2
Viganu	Vigan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dramatickém	dramatický	k2eAgInSc6d1
návratu	návrat	k1gInSc6
nahlásil	nahlásit	k5eAaPmAgInS
Grant	grant	k1gInSc1
Mahony	mahon	k1gInPc4
v	v	k7c6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
zpozorování	zpozorování	k1gNnSc2
invazního	invazní	k2eAgInSc2d1
konvoje	konvoj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Proti	proti	k7c3
invazním	invazní	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
odstartovalo	odstartovat	k5eAaPmAgNnS
pět	pět	k4xCc4
B-17	B-17	k1gFnPc2
se	s	k7c7
100	#num#	k4
<g/>
lb	lb	k?
(	(	kIx(
<g/>
45,4	45,4	k4
kg	kg	kA
<g/>
)	)	kIx)
pumami	puma	k1gFnPc7
<g/>
,	,	kIx,
několik	několik	k4yIc1
P-40	P-40	k1gMnPc2
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
a	a	k8xC
16	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
jich	on	k3xPp3gMnPc2
ale	ale	k9
pro	pro	k7c4
poruchu	porucha	k1gFnSc4
motoru	motor	k1gInSc2
útok	útok	k1gInSc4
vzdalo	vzdát	k5eAaPmAgNnS
<g/>
)	)	kIx)
P-35A	P-35A	k1gFnSc1
od	od	k7c2
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylodění	vylodění	k1gNnPc1
u	u	k7c2
Viganu	Vigan	k1gInSc2
podporovaly	podporovat	k5eAaImAgFnP
bombardovací	bombardovací	k2eAgFnPc1d1
Ki-	Ki-	k1gFnPc1
<g/>
48	#num#	k4
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
čútai	čúta	k1gFnPc4
a	a	k8xC
stíhací	stíhací	k2eAgFnPc4d1
Ki-	Ki-	k1gFnPc4
<g/>
27	#num#	k4
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalé	nastalý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
přišli	přijít	k5eAaPmAgMnP
Američané	Američan	k1gMnPc1
o	o	k7c4
tři	tři	k4xCgFnPc4
P-40	P-40	k1gFnPc4
(	(	kIx(
<g/>
jeden	jeden	k4xCgMnSc1
sestřelen	sestřelen	k2eAgMnSc1d1
Ki-	Ki-	k1gMnSc1
<g/>
27	#num#	k4
a	a	k8xC
dva	dva	k4xCgInPc4
sestřelené	sestřelený	k2eAgInPc4d1
palubními	palubní	k2eAgMnPc7d1
střelci	střelec	k1gMnPc7
Ki-	Ki-	k1gFnSc2
<g/>
48	#num#	k4
nebo	nebo	k8xC
uzemněné	uzemněný	k2eAgFnPc1d1
poruchou	porucha	k1gFnSc7
motoru	motor	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
dva	dva	k4xCgInPc1
P-35A	P-35A	k1gMnPc2
(	(	kIx(
<g/>
jeden	jeden	k4xCgMnSc1
pro	pro	k7c4
poruchu	porucha	k1gFnSc4
motoru	motor	k1gInSc2
a	a	k8xC
stroj	stroj	k1gInSc1
poručíka	poručík	k1gMnSc2
Samuela	Samuel	k1gMnSc2
H.	H.	kA
Marretta	Marrett	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
na	na	k7c6
letadlech	letadlo	k1gNnPc6
byly	být	k5eAaImAgFnP
jedna	jeden	k4xCgFnSc1
sestřelená	sestřelený	k2eAgFnSc1d1
a	a	k8xC
šest	šest	k4xCc1
poškozených	poškozený	k1gMnPc2
Ki-	Ki-	k1gFnSc2
<g/>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Invazní	invazní	k2eAgInSc1d1
sily	silo	k1gNnPc7
ztratili	ztratit	k5eAaPmAgMnP
minolovku	minolovka	k1gFnSc4
W-	W-	k1gFnSc2
<g/>
10	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
explodovala	explodovat	k5eAaBmAgFnS
po	po	k7c6
několika	několik	k4yIc6
Marrettových	Marrettův	k2eAgInPc6d1
náletech	nálet	k1gInPc6
a	a	k8xC
potopila	potopit	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exploze	exploze	k1gFnSc1
zničila	zničit	k5eAaPmAgFnS
i	i	k9
Marrettův	Marrettův	k2eAgMnSc1d1
P-35A	P-35A	k1gMnSc1
a	a	k8xC
Marrett	Marrett	k1gMnSc1
zahynul	zahynout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transportní	transportní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
Oigawa	Oigawum	k1gNnSc2
Maru	Maru	k1gFnSc2
a	a	k8xC
Takao	Takao	k1gNnSc4
Maru	Maru	k1gFnPc2
byly	být	k5eAaImAgFnP
poškozené	poškozený	k2eAgFnPc1d1
a	a	k8xC
musely	muset	k5eAaImAgFnP
najet	najet	k5eAaPmF
na	na	k7c4
břeh	břeh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižník	křižník	k1gInSc1
Naka	Nakum	k1gNnSc2
a	a	k8xC
torpédoborec	torpédoborec	k1gMnSc1
Murasame	Murasam	k1gInSc5
byly	být	k5eAaImAgFnP
lehce	lehko	k6eAd1
poškozeny	poškodit	k5eAaPmNgFnP
postřelováním	postřelování	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyloďování	vyloďování	k1gNnSc1
skupiny	skupina	k1gFnSc2
Kanno	Kanno	k6eAd1
pokračovalo	pokračovat	k5eAaImAgNnS
i	i	k9
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
jednotky	jednotka	k1gFnSc2
zamířila	zamířit	k5eAaPmAgFnS
na	na	k7c4
sever	sever	k1gInSc4
po	po	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
3	#num#	k4
do	do	k7c2
Laoagu	Laoag	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
spolu	spolu	k6eAd1
s	s	k7c7
letištěm	letiště	k1gNnSc7
obsazen	obsadit	k5eAaPmNgInS
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plukovník	plukovník	k1gMnSc1
Tanaka	Tanak	k1gMnSc4
byl	být	k5eAaImAgMnS
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c6
Aparri	Aparr	k1gInSc6
pověřen	pověřit	k5eAaPmNgMnS
velením	velení	k1gNnSc7
nad	nad	k7c7
oběma	dva	k4xCgInPc7
výsadky	výsadek	k1gInPc7
v	v	k7c6
Aparri	Aparr	k1gInSc6
a	a	k8xC
Viganu	Vigan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
skupiny	skupina	k1gFnSc2
Tanaka	Tanak	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
postupovala	postupovat	k5eAaImAgFnS
na	na	k7c4
Laoag	Laoag	k1gInSc4
proto	proto	k8xC
pokračovala	pokračovat	k5eAaImAgFnS
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Viganu	Vigan	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
a	a	k8xC
kde	kde	k6eAd1
se	se	k3xPyFc4
spojila	spojit	k5eAaPmAgFnS
se	s	k7c7
skupinou	skupina	k1gFnSc7
Kanno	Kanno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
Tanaka	Tanak	k1gMnSc4
vyrazil	vyrazit	k5eAaPmAgMnS
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
sjednocenou	sjednocený	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
Lingayenskému	Lingayenský	k2eAgInSc3d1
zálivu	záliv	k1gInSc3
po	po	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Viganu	Vigan	k1gInSc6
<g/>
,	,	kIx,
Aparri	Aparr	k1gInSc6
a	a	k8xC
Laoagu	Laoag	k1gInSc6
zůstaly	zůstat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
jednotky	jednotka	k1gFnPc1
potřebné	potřebný	k2eAgFnPc1d1
k	k	k7c3
zajištění	zajištění	k1gNnSc3
letišť	letiště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
začalo	začít	k5eAaPmAgNnS
vylodění	vylodění	k1gNnSc1
v	v	k7c6
Lingayenskému	Lingayenský	k2eAgInSc3d1
zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
dorazil	dorazit	k5eAaPmAgMnS
Tanaka	Tanak	k1gMnSc2
do	do	k7c2
San	San	k1gFnSc2
Fernando	Fernanda	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
nálet	nálet	k1gInSc1
na	na	k7c4
Cavite	Cavit	k1gInSc5
a	a	k8xC
filipínská	filipínský	k2eAgNnPc4d1
letiště	letiště	k1gNnPc4
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
</s>
<s>
„	„	k?
</s>
<s>
První	první	k4xOgNnSc1
varování	varování	k1gNnSc1
že	že	k8xS
Japíci	Japík	k1gMnPc1
jsou	být	k5eAaImIp3nP
poblíž	poblíž	k6eAd1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
kdokoliv	kdokoliv	k3yInSc1
z	z	k7c2
nás	my	k3xPp1nPc2
na	na	k7c6
zemi	zem	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
když	když	k8xS
jsme	být	k5eAaImIp1nP
je	on	k3xPp3gFnPc4
uviděli	uvidět	k5eAaPmAgMnP
klesat	klesat	k5eAaImF
přes	přes	k7c4
kopce	kopec	k1gInPc4
a	a	k8xC
střílet	střílet	k5eAaImF
po	po	k7c4
nás	my	k3xPp1nPc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
LT	LT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grant	grant	k1gInSc1
Mahony	mahon	k1gInPc4
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
o	o	k7c6
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
účinnosti	účinnost	k1gFnSc2
včasného	včasný	k2eAgNnSc2d1
varování	varování	k1gNnSc2
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hořící	hořící	k2eAgFnSc1d1
Cavite	Cavit	k1gInSc5
po	po	k7c6
bombardování	bombardování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlevo	vlevo	k6eAd1
je	být	k5eAaImIp3nS
zachycena	zachytit	k5eAaPmNgFnS
exploze	exploze	k1gFnSc1
munice	munice	k1gFnSc1
do	do	k7c2
ručních	ruční	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
uprostřed	uprostřed	k6eAd1
hoří	hořet	k5eAaImIp3nS
bárka	bárka	k1gFnSc1
naložená	naložený	k2eAgFnSc1d1
torpédy	torpédo	k1gNnPc7
a	a	k8xC
napravo	napravo	k6eAd1
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
příď	příď	k1gFnSc4
ponorky	ponorka	k1gFnSc2
–	–	k?
pravděpodobně	pravděpodobně	k6eAd1
USS	USS	kA
Sealion	Sealion	k1gInSc4
</s>
<s>
Kouř	kouř	k1gInSc1
stoupající	stoupající	k2eAgInSc1d1
z	z	k7c2
hořícího	hořící	k2eAgNnSc2d1
zasaženého	zasažený	k2eAgNnSc2d1
skladiště	skladiště	k1gNnSc2
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
</s>
<s>
Dopoledne	dopoledne	k1gNnSc1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vyslali	vyslat	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
z	z	k7c2
Formosy	Formosa	k1gFnSc2
další	další	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
bombardérů	bombardér	k1gInPc2
a	a	k8xC
stíhaček	stíhačka	k1gFnPc2
k	k	k7c3
útokům	útok	k1gInPc3
na	na	k7c4
cíle	cíl	k1gInPc4
na	na	k7c4
Luzonu	Luzona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgInSc4d1
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k1gNnSc6
vyslala	vyslat	k5eAaPmAgFnS
27	#num#	k4
G3M	G3M	k1gFnPc2
(	(	kIx(
<g/>
jeden	jeden	k4xCgInSc1
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
pro	pro	k7c4
poruchu	porucha	k1gFnSc4
motorů	motor	k1gInPc2
vrátit	vrátit	k5eAaPmF
<g/>
)	)	kIx)
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
námořní	námořní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
Cavite	Cavit	k1gInSc5
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
vyslala	vyslat	k5eAaPmAgFnS
24	#num#	k4
G4M1	G4M1	k1gFnSc1
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
27	#num#	k4
G4M1	G4M1	k1gFnPc2
na	na	k7c4
Del	Del	k1gFnSc4
Carmen	Carmen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doprovod	doprovod	k1gInSc1
jim	on	k3xPp3gMnPc3
zajišťovalo	zajišťovat	k5eAaImAgNnS
22	#num#	k4
A6M2	A6M2	k1gMnPc2
(	(	kIx(
<g/>
čtyři	čtyři	k4xCgMnPc1
se	se	k3xPyFc4
musely	muset	k5eAaImAgFnP
pro	pro	k7c4
poruchu	porucha	k1gFnSc4
motoru	motor	k1gInSc2
vrátit	vrátit	k5eAaPmF
<g/>
)	)	kIx)
od	od	k7c2
Tainan	Tainana	k1gFnPc2
kókútai	kókútai	k6eAd1
s	s	k7c7
jedním	jeden	k4xCgInSc7
navigačním	navigační	k2eAgInSc7d1
C	C	kA
<g/>
5	#num#	k4
<g/>
M.	M.	kA
Další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
C5M	C5M	k1gFnSc1
navigovaly	navigovat	k5eAaImAgFnP
34	#num#	k4
A6M2	A6M2	k1gFnPc2
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
do	do	k7c2
oblasti	oblast	k1gFnSc2
Manily	Manila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádní	armádní	k2eAgInSc4d1
letectvo	letectvo	k1gNnSc1
přispělo	přispět	k5eAaPmAgNnS
bombardéry	bombardér	k1gMnPc4
Ki-	Ki-	k1gFnSc2
<g/>
21	#num#	k4
4	#num#	k4
<g/>
.	.	kIx.
čůtai	čůta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
měly	mít	k5eAaImAgFnP
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Iba	iba	k6eAd1
Field	Fielda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kolem	kolem	k7c2
poledne	poledne	k1gNnSc2
se	s	k7c7
27	#num#	k4
G4M1	G4M1	k1gMnPc7
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
objevilo	objevit	k5eAaPmAgNnS
nad	nad	k7c7
Del	Del	k1gMnPc7
Carmen	Carmen	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
oblačnosti	oblačnost	k1gFnSc3
nad	nad	k7c7
cílem	cíl	k1gInSc7
zamířily	zamířit	k5eAaPmAgFnP
na	na	k7c6
Cavite	Cavit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Del	Del	k1gFnSc6
Carmen	Carmen	k1gInSc1
právě	právě	k6eAd1
přistály	přistát	k5eAaImAgFnP,k5eAaPmAgFnP
P-35A	P-35A	k1gFnPc1
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
se	se	k3xPyFc4
vrhly	vrhnout	k5eAaImAgFnP,k5eAaPmAgFnP
A6M2	A6M2	k1gFnPc1
Tainan	Tainana	k1gFnPc2
kókútai	kókútai	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejich	jejich	k3xOp3gInSc6
útoku	útok	k1gInSc6
bylo	být	k5eAaImAgNnS
dvanáct	dvanáct	k4xCc1
P-35A	P-35A	k1gMnPc2
zničeno	zničit	k5eAaPmNgNnS
a	a	k8xC
další	další	k2eAgFnPc1d1
neopravitelně	opravitelně	k6eNd1
poškozeny	poškozen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zničeny	zničen	k2eAgInPc1d1
byly	být	k5eAaImAgInP
i	i	k9
zásoby	zásoba	k1gFnSc2
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
Del	Del	k1gFnSc4
Carmen	Carmen	k1gInSc4
bylo	být	k5eAaImAgNnS
prohlášeno	prohlásit	k5eAaPmNgNnS
za	za	k7c4
nepoužitelné	použitelný	k2eNgNnSc4d1
a	a	k8xC
opuštěno	opuštěn	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nesetkala	setkat	k5eNaPmAgFnS
s	s	k7c7
žádným	žádný	k3yNgInSc7
odporem	odpor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
pum	puma	k1gFnPc2
ale	ale	k8xC
dopadla	dopadnout	k5eAaPmAgFnS
na	na	k7c4
Manilu	Manila	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
P-40	P-40	k1gFnSc1
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
se	se	k3xPyFc4
v	v	k7c6
prostoru	prostor	k1gInSc6
Manily	Manila	k1gFnSc2
a	a	k8xC
Del	Del	k1gMnSc2
Carmen	Carmen	k1gInSc1
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
souboje	souboj	k1gInSc2
s	s	k7c7
A6M2	A6M2	k1gMnPc7
Tainan	Tainana	k1gFnPc2
kókútai	kókútai	k6eAd1
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókúta	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Američané	Američan	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
11	#num#	k4
P-40	P-40	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
musely	muset	k5eAaImAgFnP
odepsat	odepsat	k5eAaPmF
kvůli	kvůli	k7c3
nehodám	nehoda	k1gFnPc3
při	při	k7c6
přistání	přistání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
jedno	jeden	k4xCgNnSc1
A6M2	A6M2	k1gFnPc6
od	od	k7c2
Tainan	Tainany	k1gInPc2
kókútai	kókútai	k6eAd1
a	a	k8xC
dvě	dva	k4xCgNnPc4
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
i	i	k9
s	s	k7c7
piloty	pilot	k1gMnPc7
a	a	k8xC
dalších	další	k2eAgFnPc2d1
19	#num#	k4
A6M2	A6M2	k1gFnPc2
bylo	být	k5eAaImAgNnS
poškozeno	poškodit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
poškozených	poškozený	k2eAgMnPc2d1
A6M2	A6M2	k1gMnPc2
musela	muset	k5eAaImAgFnS
později	pozdě	k6eAd2
nouzově	nouzově	k6eAd1
přistát	přistát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezitím	mezitím	k6eAd1
bombardéry	bombardér	k1gInPc1
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókúta	k1gFnPc1
bombardovaly	bombardovat	k5eAaImAgFnP
námořní	námořní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
Cavite	Cavit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
baterii	baterie	k1gFnSc3
devíti	devět	k4xCc3
76,2	76,2	k4
<g/>
mm	mm	kA
protiletadlových	protiletadlový	k2eAgFnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
ale	ale	k9
do	do	k7c2
letové	letový	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
bombardérů	bombardér	k1gMnPc2
nedostřelila	dostřelit	k5eNaPmAgFnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
přidala	přidat	k5eAaPmAgNnP
i	i	k9
protiletadlová	protiletadlový	k2eAgNnPc1d1
děla	dělo	k1gNnPc1
plavidel	plavidlo	k1gNnPc2
zakotvených	zakotvený	k2eAgNnPc2d1
v	v	k7c6
Cavite	Cavit	k1gInSc5
i	i	k9
těch	ten	k3xDgFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
stačily	stačit	k5eAaBmAgFnP
vyplout	vyplout	k5eAaPmF
do	do	k7c2
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odletu	odlet	k1gInSc6
bombardérů	bombardér	k1gMnPc2
se	se	k3xPyFc4
nad	nad	k7c7
arzenálem	arzenál	k1gInSc7
vznášel	vznášet	k5eAaImAgInS
kouř	kouř	k1gInSc1
ze	z	k7c2
zasažené	zasažený	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
<g/>
,	,	kIx,
dílen	dílna	k1gFnPc2
<g/>
,	,	kIx,
skladišť	skladiště	k1gNnPc2
<g/>
,	,	kIx,
kasáren	kasárny	k1gFnPc2
a	a	k8xC
radiostanice	radiostanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponorka	ponorka	k1gFnSc1
USS	USS	kA
Sealion	Sealion	k1gInSc1
byla	být	k5eAaImAgFnS
těžce	těžce	k6eAd1
poškozena	poškodit	k5eAaPmNgFnS
jedním	jeden	k4xCgInSc7
přímým	přímý	k2eAgInSc7d1
zásahem	zásah	k1gInSc7
a	a	k8xC
jednou	jeden	k4xCgFnSc7
pumou	puma	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dopadla	dopadnout	k5eAaPmAgFnS
těsně	těsně	k6eAd1
vedle	vedle	k6eAd1
a	a	k8xC
dosedla	dosednout	k5eAaPmAgFnS
na	na	k7c4
dno	dno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
se	s	k7c7
Sealionem	Sealion	k1gInSc7
byla	být	k5eAaImAgFnS
těsným	těsný	k2eAgInSc7d1
dopadem	dopad	k1gInSc7
pumy	puma	k1gFnSc2
poškozena	poškodit	k5eAaPmNgFnS
i	i	k8xC
vedle	vedle	k7c2
zakotvená	zakotvený	k2eAgFnSc1d1
ponorka	ponorka	k1gFnSc1
USS	USS	kA
Seadragon	Seadragon	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
zásahem	zásah	k1gInSc7
byl	být	k5eAaImAgInS
poškozen	poškodit	k5eAaPmNgInS
i	i	k9
torpédoborec	torpédoborec	k1gInSc1
USS	USS	kA
Peary	Pear	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
muselo	muset	k5eAaImAgNnS
odepsat	odepsat	k5eAaPmF
také	také	k9
minolovku	minolovka	k1gFnSc4
USS	USS	kA
Bittern	Bittern	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oheň	oheň	k1gInSc1
se	se	k3xPyFc4
vymkl	vymknout	k5eAaPmAgInS
kontrole	kontrola	k1gFnSc3
a	a	k8xC
arzenál	arzenál	k1gInSc1
hořel	hořet	k5eAaImAgInS
ještě	ještě	k9
několik	několik	k4yIc4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zničeno	zničen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
i	i	k9
na	na	k7c4
200	#num#	k4
ponorkových	ponorkový	k2eAgNnPc2d1
torpéd	torpédo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stíhači	stíhač	k1gMnSc3
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
zastihly	zastihnout	k5eAaPmAgInP
v	v	k7c6
Cavite	Cavit	k1gInSc5
čtyři	čtyři	k4xCgFnPc4
PBY-4	PBY-4	k1gFnPc4
PatWingu	PatWing	k1gInSc2
10	#num#	k4
vyzbrojené	vyzbrojený	k2eAgFnSc2d1
torpédy	torpédo	k1gNnPc7
a	a	k8xC
chystající	chystající	k2eAgFnSc1d1
se	se	k3xPyFc4
odstartovat	odstartovat	k5eAaPmF
proti	proti	k7c3
japonským	japonský	k2eAgFnPc3d1
invazním	invazní	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
LT	LT	kA
Harmon	Harmona	k1gFnPc2
T.	T.	kA
Utter	Utter	k1gMnSc1
(	(	kIx(
<g/>
101	#num#	k4
<g/>
-P-	-P-	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ENS	ENS	kA
James	James	k1gMnSc1
H.	H.	kA
McConnel	McConnel	k1gMnSc1
(	(	kIx(
<g/>
102	#num#	k4
<g/>
-P-	-P-	k?
<g/>
28	#num#	k4
<g/>
)	)	kIx)
dokázali	dokázat	k5eAaPmAgMnP
odstartovat	odstartovat	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInPc1
Cataliny	Catalin	k1gInPc1
byly	být	k5eAaImAgInP
natolik	natolik	k6eAd1
poškozené	poškozený	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
musely	muset	k5eAaImAgFnP
nedaleko	nedaleko	k7c2
Cavite	Cavit	k1gInSc5
nouzově	nouzově	k6eAd1
přistát	přistát	k5eAaImF,k5eAaPmF
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utterův	Utterův	k2eAgInSc1d1
střelec	střelec	k1gMnSc1
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
sestřelení	sestřelení	k1gNnSc4
jednoho	jeden	k4xCgMnSc2
A	a	k8xC
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
znamenalo	znamenat	k5eAaImAgNnS
první	první	k4xOgNnSc1
vzdušné	vzdušný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
pro	pro	k7c4
US	US	kA
Navy	Navy	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
si	se	k3xPyFc3
nárokovali	nárokovat	k5eAaImAgMnP
tři	tři	k4xCgFnPc4
sestřelené	sestřelený	k2eAgFnPc4d1
Cataliny	Catalina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Onou	onen	k3xDgFnSc7
třetí	třetí	k4xOgFnSc3
by	by	kYmCp3nS
mohla	moct	k5eAaImAgNnP
být	být	k5eAaImF
101-P-12	101-P-12	k4
(	(	kIx(
<g/>
podle	podle	k7c2
Alslebena	Alsleben	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
Knott	Knott	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zbývající	zbývající	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
Cataliny	Catalina	k1gFnPc1
vyvázly	vyváznout	k5eAaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
návratu	návrat	k1gInSc6
musely	muset	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
G4M1	G4M1	k1gFnPc4
od	od	k7c2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
nouzově	nouzově	k6eAd1
přistát	přistát	k5eAaPmF,k5eAaImF
<g/>
:	:	kIx,
jedna	jeden	k4xCgFnSc1
ve	v	k7c6
Viganu	Vigan	k1gInSc6
na	na	k7c6
čerstvě	čerstvě	k6eAd1
obsazeném	obsazený	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
a	a	k8xC
druhá	druhý	k4xOgFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
Formosy	Formosa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
G3M	G3M	k1gFnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókúta	k1gFnPc4
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc4
A6M2	A6M2	k1gFnPc2
od	od	k7c2
Tainan	Tainana	k1gFnPc2
kókútai	kókúta	k1gFnSc2
a	a	k8xC
velitel	velitel	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
taji	tát	k5eAaImIp1nS
(	(	kIx(
<g/>
poručík	poručík	k1gMnSc1
<g/>
)	)	kIx)
Tomocu	Tomocus	k1gInSc2
Jokojama	Jokojamum	k1gNnSc2
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
poškozeným	poškozený	k1gMnSc7
A6M2	A6M2	k1gMnSc7
museli	muset	k5eAaImAgMnP
nouzově	nouzově	k6eAd1
sednout	sednout	k5eAaPmF
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
japonském	japonský	k2eAgInSc6d1
náletu	nálet	k1gInSc6
z	z	k7c2
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zůstalo	zůstat	k5eAaPmAgNnS
USAFFEAF	USAFFEAF	kA
bojeschopných	bojeschopný	k2eAgNnPc2d1
pouze	pouze	k6eAd1
22	#num#	k4
P-	P-	k1gFnPc2
<g/>
40	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
B-17	B-17	k1gFnPc2
a	a	k8xC
pět	pět	k4xCc1
nebo	nebo	k8xC
osm	osm	k4xCc1
P-	P-	k1gFnPc2
<g/>
35	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
Námořnictvo	námořnictvo	k1gNnSc1
disponovalo	disponovat	k5eAaBmAgNnS
24	#num#	k4
PBY-4	PBY-4	k1gFnPc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Japoncům	Japonec	k1gMnPc3
padly	padnout	k5eAaPmAgFnP,k5eAaImAgFnP
za	za	k7c4
oběť	oběť	k1gFnSc4
pouze	pouze	k6eAd1
101-P-4	101-P-4	k4
a	a	k8xC
101-P-7	101-P-7	k4
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
a	a	k8xC
101-P-5	101-P-5	k4
a	a	k8xC
102-P-28	102-P-28	k4
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vylodění	vylodění	k1gNnSc1
u	u	k7c2
Legazpi	Legazp	k1gFnSc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Minonoska	minonoska	k1gFnSc1
Jaejama	Jaejamum	k1gNnSc2
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zaminovala	zaminovat	k5eAaPmAgFnS
úžinu	úžina	k1gFnSc4
Surigao	Surigao	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránila	zabránit	k5eAaPmAgFnS
americkým	americký	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
v	v	k7c6
přístupu	přístup	k1gInSc6
k	k	k7c3
Legazpi	Legazpi	k1gNnSc3
a	a	k8xC
případnému	případný	k2eAgNnSc3d1
narušení	narušení	k1gNnSc3
vylodění	vylodění	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
<g/>
:	:	kIx,
Dvě	dva	k4xCgFnPc1
B5N1	B5N1	k1gFnPc1
z	z	k7c2
Rjúdžó	Rjúdžó	k1gFnSc2
letí	letět	k5eAaImIp3nS
napadnout	napadnout	k5eAaPmF
cíle	cíl	k1gInPc4
u	u	k7c2
Legazpi	Legazp	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozadí	pozadí	k1gNnSc6
sopka	sopka	k1gFnSc1
Mayon	Mayon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zaminovala	zaminovat	k5eAaPmAgFnS
minonoska	minonoska	k1gFnSc1
Jaejama	Jaejama	k1gFnSc1
<g/>
,	,	kIx,
doprovázená	doprovázený	k2eAgNnPc4d1
lehkým	lehký	k2eAgInSc7d1
křižníkem	křižník	k1gInSc7
Džincu	Džincus	k1gInSc2
a	a	k8xC
torpédoborci	torpédoborec	k1gMnPc1
<g/>
,	,	kIx,
úžinu	úžina	k1gFnSc4
Surigao	Surigao	k6eAd1
(	(	kIx(
<g/>
Dull	Dull	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
zaminování	zaminování	k1gNnSc1
úžiny	úžina	k1gFnSc2
San	San	k1gFnSc2
Bernardino	Bernardin	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Časně	časně	k6eAd1
ráno	ráno	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
ze	z	k7c2
sedmi	sedm	k4xCc2
transportních	transportní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
začalo	začít	k5eAaPmAgNnS
vyloďovat	vyloďovat	k5eAaImF
asi	asi	k9
2500	#num#	k4
mužů	muž	k1gMnPc2
skupiny	skupina	k1gFnSc2
Kimura	Kimura	k1gFnSc1
a	a	k8xC
575	#num#	k4
mužů	muž	k1gMnPc2
1	#num#	k4
<g/>
.	.	kIx.
kurské	kurský	k2eAgFnSc2d1
speciální	speciální	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
vyloďovací	vyloďovací	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupině	skupina	k1gFnSc6
Kimura	Kimur	k1gMnSc2
velel	velet	k5eAaImAgMnS
generálmajor	generálmajor	k1gMnSc1
Naoki	Naok	k1gFnSc2
Kimura	Kimura	k1gFnSc1
a	a	k8xC
skládala	skládat	k5eAaImAgFnS
se	se	k3xPyFc4
ze	z	k7c2
štábu	štáb	k1gInSc2
<g/>
,	,	kIx,
33	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
(	(	kIx(
<g/>
bez	bez	k7c2
1	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
baterie	baterie	k1gFnSc1
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
a	a	k8xC
ženistů	ženista	k1gMnPc2
16	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vylodění	vylodění	k1gNnSc1
zajišťovaly	zajišťovat	k5eAaImAgInP
lehký	lehký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Nagara	Nagara	k1gFnSc1
<g/>
,	,	kIx,
torpédoborce	torpédoborec	k1gMnSc4
Jamakaze	Jamakaha	k1gFnSc6
<g/>
,	,	kIx,
Suzukaze	Suzukaha	k1gFnSc6
<g/>
,	,	kIx,
Kawakaze	Kawakaha	k1gFnSc6
<g/>
,	,	kIx,
Umikaze	Umikaha	k1gFnSc6
<g/>
,	,	kIx,
Jukikaze	Jukikaha	k1gFnSc6
a	a	k8xC
Tokicukaze	Tokicukaha	k1gFnSc6
<g/>
,	,	kIx,
nosiče	nosič	k1gInPc4
hydroplánů	hydroplán	k1gInPc2
Čitose	Čitosa	k1gFnSc3
a	a	k8xC
Mizuho	Mizu	k1gMnSc2
<g/>
,	,	kIx,
minolovky	minolovka	k1gFnSc2
W-	W-	k1gFnSc2
<g/>
7	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
W-	W-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dva	dva	k4xCgInPc1
hlídkové	hlídkový	k2eAgInPc1d1
čluny	člun	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálené	vzdálený	k2eAgFnPc4d1
krytí	krytí	k1gNnSc3
poskytovaly	poskytovat	k5eAaImAgInP
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Rjúdžó	Rjúdžó	k1gFnSc2
<g/>
,	,	kIx,
těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Haguro	Hagura	k1gFnSc5
<g/>
,	,	kIx,
Mjóko	Mjóko	k1gNnSc4
a	a	k8xC
Nači	Nači	k1gNnSc4
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Džincu	Džincus	k1gInSc2
a	a	k8xC
torpédoborce	torpédoborec	k1gInPc4
Amacukaze	Amacukaha	k1gFnSc3
<g/>
,	,	kIx,
Hajašio	Hajašio	k1gNnSc4
<g/>
,	,	kIx,
Kurošio	Kurošio	k1gNnSc4
<g/>
,	,	kIx,
Hacukaze	Hacukaha	k1gFnSc6
<g/>
,	,	kIx,
Nacušio	Nacušio	k6eAd1
<g/>
,	,	kIx,
Ojašio	Ojašio	k6eAd1
a	a	k8xC
Šiokaze	Šiokaha	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
se	se	k3xPyFc4
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
nesetkaly	setkat	k5eNaPmAgInP
s	s	k7c7
žádným	žádný	k3yNgInSc7
odporem	odpor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
letiště	letiště	k1gNnSc1
a	a	k8xC
železniční	železniční	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
v	v	k7c6
rukou	ruka	k1gFnPc6
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Vylodění	vylodění	k1gNnSc2
se	se	k3xPyFc4
pokusily	pokusit	k5eAaPmAgFnP
narušit	narušit	k5eAaPmF
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
P-	P-	k1gFnPc4
<g/>
40	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
1	#num#	k4
<g/>
st	st	kA
Lt	Lt	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
nadporučík	nadporučík	k1gMnSc1
<g/>
)	)	kIx)
Robert	Robert	k1gMnSc1
T.	T.	kA
Hanson	Hanson	k1gMnSc1
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
sestřelení	sestřelení	k1gNnSc4
jednoho	jeden	k4xCgInSc2
létajícího	létající	k2eAgInSc2d1
člunu	člun	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
nenacházely	nacházet	k5eNaImAgFnP
žádné	žádný	k3yNgFnPc1
pozemní	pozemní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
obránců	obránce	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c4
vylodění	vylodění	k1gNnSc4
Japonců	Japonec	k1gMnPc2
vyslala	vyslat	k5eAaPmAgFnS
51	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
FA	fa	k1gNnSc2
několik	několik	k4yIc1
jednotek	jednotka	k1gFnPc2
na	na	k7c4
Bicolský	Bicolský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
provést	provést	k5eAaPmF
evakuaci	evakuace	k1gFnSc4
železnice	železnice	k1gFnSc2
a	a	k8xC
zničení	zničení	k1gNnSc1
silničních	silniční	k2eAgInPc2d1
a	a	k8xC
železničních	železniční	k2eAgInPc2d1
mostů	most	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
vylodění	vylodění	k1gNnSc6
a	a	k8xC
zajištění	zajištění	k1gNnSc6
Legazpi	Legazp	k1gFnSc2
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
Kimura	Kimura	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
sever	sever	k1gInSc4
podél	podél	k7c2
silnice	silnice	k1gFnSc2
číslo	číslo	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
kontaktu	kontakt	k1gInSc3
<g/>
,	,	kIx,
když	když	k8xS
japonská	japonský	k2eAgFnSc1d1
hlídka	hlídka	k1gFnSc1
narazila	narazit	k5eAaPmAgFnS
poblíž	poblíž	k7c2
Ragay	Ragaa	k1gFnSc2
na	na	k7c4
51	#num#	k4
<g/>
.	.	kIx.
ženijní	ženijní	k2eAgInSc1d1
prapor	prapor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c4
demolici	demolice	k1gFnSc4
mostu	most	k1gInSc2
přes	přes	k7c4
řeku	řeka	k1gFnSc4
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
zničení	zničení	k1gNnSc6
se	se	k3xPyFc4
japonská	japonský	k2eAgFnSc1d1
hlídka	hlídka	k1gFnSc1
stáhla	stáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
jednotka	jednotka	k1gFnSc1
Kimura	Kimura	k1gFnSc1
obsadila	obsadit	k5eAaPmAgFnS
město	město	k1gNnSc4
Naga	Nag	k1gInSc2
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vstoupili	vstoupit	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
do	do	k7c2
Sipocot	Sipocota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dvě	dva	k4xCgFnPc4
roty	rota	k1gFnPc4
1	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
52	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
51	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
držely	držet	k5eAaImAgInP
asi	asi	k9
jedenácti	jedenáct	k4xCc2
kilometrovou	kilometrový	k2eAgFnSc4d1
pevninskou	pevninský	k2eAgFnSc4d1
šíji	šíje	k1gFnSc4
mezi	mezi	k7c7
Ragayským	Ragayský	k2eAgInSc7d1
zálivem	záliv	k1gInSc7
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
Lamon	Lamon	k1gMnSc1
Bay	Bay	k1gMnSc1
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
byly	být	k5eAaImAgFnP
roty	rota	k1gFnPc1
B	B	kA
a	a	k8xC
C	C	kA
vyslány	vyslán	k2eAgInPc1d1
k	k	k7c3
Sipocot	Sipocot	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
rotě	rota	k1gFnSc3
B	B	kA
1	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
východně	východně	k6eAd1
od	od	k7c2
Negritos	Negritos	k1gMnSc1
Camp	camp	k1gInSc4
na	na	k7c6
silnici	silnice	k1gFnSc6
číslo	číslo	k1gNnSc1
1	#num#	k4
odrazit	odrazit	k5eAaPmF
japonský	japonský	k2eAgInSc4d1
pokus	pokus	k1gInSc4
o	o	k7c4
postup	postup	k1gInSc4
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc7
ale	ale	k8xC
toto	tento	k3xDgNnSc4
střetnutí	střetnutí	k1gNnSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
vlastní	vlastní	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
filipínské	filipínský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
stahovat	stahovat	k5eAaImF
<g/>
,	,	kIx,
neboť	neboť	k8xC
jim	on	k3xPp3gMnPc3
hrozilo	hrozit	k5eAaImAgNnS
obklíčení	obklíčení	k1gNnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
japonského	japonský	k2eAgNnSc2d1
vylodění	vylodění	k1gNnSc2
v	v	k7c4
Lamon	Lamon	k1gNnSc4
Bay	Bay	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
vzduchu	vzduch	k1gInSc6
a	a	k8xC
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
…	…	k?
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
sekce	sekce	k1gFnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
námořních	námořní	k2eAgFnPc6d1
a	a	k8xC
leteckých	letecký	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
na	na	k7c6
severu	sever	k1gInSc6
Filipín	Filipíny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
operace	operace	k1gFnPc4
nad	nad	k7c7
jižními	jižní	k2eAgFnPc7d1
a	a	k8xC
středními	střední	k2eAgFnPc7d1
Filipínami	Filipíny	k1gFnPc7
viz	vidět	k5eAaImRp2nS
sekce	sekce	k1gFnSc2
Jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
centrální	centrální	k2eAgFnSc6d1
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Lt	Lt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boyd	Boyda	k1gFnPc2
D.	D.	kA
Wagner	Wagner	k1gMnSc1
(	(	kIx(
<g/>
zde	zde	k6eAd1
bez	bez	k7c2
svého	svůj	k3xOyFgInSc2
typického	typický	k2eAgInSc2d1
kníru	knír	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nosil	nosit	k5eAaImAgInS
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
stíhacím	stíhací	k2eAgMnSc7d1
esem	eso	k1gNnSc7
USAAF	USAAF	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
útoku	útok	k1gInSc2
na	na	k7c4
vyloďující	vyloďující	k2eAgInSc4d1
se	se	k3xPyFc4
Japonce	Japonka	k1gFnSc3
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
byl	být	k5eAaImAgMnS
zraněn	zraněn	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
1942	#num#	k4
evakuován	evakuovat	k5eAaBmNgInS
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dálněvýchodní	dálněvýchodní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
utrpělo	utrpět	k5eAaPmAgNnS
v	v	k7c6
předchozích	předchozí	k2eAgInPc6d1
bojích	boj	k1gInPc6
těžké	těžký	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
a	a	k8xC
následující	následující	k2eAgFnSc2d1
akce	akce	k1gFnSc2
dálněvýchodního	dálněvýchodní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
měly	mít	k5eAaImAgFnP
charakter	charakter	k1gInSc4
spíše	spíše	k9
samostatných	samostatný	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
jednotlivých	jednotlivý	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
ztrát	ztráta	k1gFnPc2
se	se	k3xPyFc4
MacArthur	MacArthur	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
ušetřit	ušetřit	k5eAaPmF
zbývající	zbývající	k2eAgFnPc4d1
stíhačky	stíhačka	k1gFnPc4
pro	pro	k7c4
plnění	plnění	k1gNnSc4
průzkumných	průzkumný	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
a	a	k8xC
stíhačům	stíhač	k1gMnPc3
„	„	k?
<g/>
bylo	být	k5eAaImAgNnS
nařízeno	nařídit	k5eAaPmNgNnS
vyhýbat	vyhýbat	k5eAaImF
se	se	k3xPyFc4
přímému	přímý	k2eAgNnSc3d1
boji	boj	k1gInSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ráno	ráno	k6eAd1
v	v	k7c4
pátek	pátek	k1gInSc4
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
vydalo	vydat	k5eAaPmAgNnS
šest	šest	k4xCc1
B-17	B-17	k1gMnPc2
z	z	k7c2
Del	Del	k1gFnSc2
Monte	Mont	k1gInSc5
(	(	kIx(
<g/>
sedmý	sedmý	k4xOgInSc4
havaroval	havarovat	k5eAaPmAgMnS
při	při	k7c6
startu	start	k1gInSc6
<g/>
)	)	kIx)
bombardovat	bombardovat	k5eAaImF
cíle	cíl	k1gInPc4
u	u	k7c2
Viganu	Vigan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odbombardování	odbombardování	k1gNnSc6
se	se	k3xPyFc4
všechny	všechen	k3xTgInPc1
stroje	stroj	k1gInPc1
vrátily	vrátit	k5eAaPmAgInP
na	na	k7c4
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
odstartovaly	odstartovat	k5eAaPmAgFnP
z	z	k7c2
Formosy	Formosa	k1gFnSc2
japonské	japonský	k2eAgInPc1d1
stroje	stroj	k1gInPc1
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
náletu	nálet	k1gInSc3
na	na	k7c4
Luzon	Luzon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
36	#num#	k4
G	G	kA
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
79	#num#	k4
G4M1	G4M1	k1gMnPc2
a	a	k8xC
63	#num#	k4
A6M2	A6M2	k1gMnSc2
byly	být	k5eAaImAgInP
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
Iba	iba	k6eAd1
Field	Field	k1gMnSc1
<g/>
,	,	kIx,
Batangas	Batangas	k1gMnSc1
<g/>
,	,	kIx,
Manila	Manila	k1gFnSc1
a	a	k8xC
Subic	Subic	k1gMnSc1
Bay	Bay	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japoncům	Japonec	k1gMnPc3
se	se	k3xPyFc4
postavili	postavit	k5eAaPmAgMnP
filipínští	filipínský	k2eAgMnPc1d1
stíhači	stíhač	k1gMnPc1
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
zastaralými	zastaralý	k2eAgFnPc7d1
P-	P-	k1gFnPc7
<g/>
26	#num#	k4
<g/>
A.	A.	kA
Na	na	k7c4
konto	konto	k1gNnSc4
Filipínců	Filipínec	k1gMnPc2
(	(	kIx(
<g/>
Jesus	Jesus	k1gMnSc1
A.	A.	kA
Villamor	Villamor	k1gMnSc1
od	od	k7c2
6	#num#	k4
<g/>
.	.	kIx.
perutě	peruť	k1gFnSc2
<g/>
)	)	kIx)
šel	jít	k5eAaImAgMnS
pravděpodobně	pravděpodobně	k6eAd1
jeden	jeden	k4xCgMnSc1
G3M	G3M	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókúta	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nouzově	nouzově	k6eAd1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
poblíž	poblíž	k6eAd1
Clark	Clark	k1gInSc1
Fieldu	Field	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jeden	jeden	k4xCgInSc1
až	až	k9
dvě	dva	k4xCgFnPc4
A	a	k8xC
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipínci	Filipínec	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
tři	tři	k4xCgFnPc4
P-26A	P-26A	k1gFnPc4
a	a	k8xC
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
byly	být	k5eAaImAgFnP
těžce	těžce	k6eAd1
poškozeny	poškodit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
Stíhači	stíhač	k1gMnPc1
Tainan	Tainana	k1gFnPc2
kókútai	kókútai	k6eAd1
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókúta	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	se	k3xPyFc3
na	na	k7c6
zakotvené	zakotvený	k2eAgFnSc6d1
PBY-4	PBY-4	k1gFnSc6
PatWingu	PatWing	k1gInSc2
10	#num#	k4
v	v	k7c6
Subic	Subice	k1gFnPc2
Bay	Bay	k1gFnPc4
a	a	k8xC
sedm	sedm	k4xCc1
jich	on	k3xPp3gMnPc2
zničili	zničit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
stroje	stroj	k1gInPc4
102	#num#	k4
<g/>
-P-	-P-	k?
<g/>
16	#num#	k4
<g/>
,	,	kIx,
102	#num#	k4
<g/>
-P-	-P-	k?
<g/>
17	#num#	k4
<g/>
,	,	kIx,
102	#num#	k4
<g/>
-P-	-P-	k?
<g/>
18	#num#	k4
<g/>
,	,	kIx,
102	#num#	k4
<g/>
-P-	-P-	k?
<g/>
19	#num#	k4
<g/>
,	,	kIx,
102	#num#	k4
<g/>
-P-	-P-	k?
<g/>
20	#num#	k4
<g/>
,	,	kIx,
102-P-21	102-P-21	k4
a	a	k8xC
101	#num#	k4
<g/>
-P-	-P-	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
provedl	provést	k5eAaPmAgMnS
velitel	velitel	k1gMnSc1
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
Lt	Lt	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boyd	Boyda	k1gFnPc2
D.	D.	kA
Wagner	Wagner	k1gMnSc1
průzkumný	průzkumný	k2eAgInSc4d1
let	let	k1gInSc4
nad	nad	k7c7
Aparri	Aparri	k1gNnSc7
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
sestřelení	sestřelení	k1gNnSc4
čtyř	čtyři	k4xCgFnPc2
Ki-	Ki-	k1gFnPc2
<g/>
27	#num#	k4
a	a	k8xC
„	„	k?
<g/>
několika	několik	k4yIc7
<g/>
“	“	k?
letounů	letoun	k1gInPc2
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgInPc1d1
záznamy	záznam	k1gInPc1
(	(	kIx(
<g/>
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
jsou	být	k5eAaImIp3nP
ale	ale	k9
neúplné	úplný	k2eNgFnPc1d1
<g/>
)	)	kIx)
potvrzují	potvrzovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
sestřel	sestřel	k1gInSc1
jedné	jeden	k4xCgFnSc2
Ki-	Ki-	k1gFnSc2
<g/>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c4
sobotu	sobota	k1gFnSc4
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zaútočily	zaútočit	k5eAaPmAgFnP
armádní	armádní	k2eAgFnPc1d1
Ki-	Ki-	k1gFnPc1
<g/>
27	#num#	k4
<g/>
,	,	kIx,
Ki-	Ki-	k1gFnSc1
<g/>
21	#num#	k4
a	a	k8xC
Ki-	Ki-	k1gFnSc1
<g/>
48	#num#	k4
na	na	k7c6
Del	Del	k1gFnSc6
Carmen	Carmen	k1gInSc4
<g/>
,	,	kIx,
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
Cabanatuan	Cabanatuana	k1gFnPc2
a	a	k8xC
na	na	k7c4
město	město	k1gNnSc4
Baguio	Baguio	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
Námořní	námořní	k2eAgNnSc1d1
Takao	Takao	k1gNnSc1
kókútai	kókúta	k1gFnSc2
vyslala	vyslat	k5eAaPmAgFnS
26	#num#	k4
G4M1	G4M1	k1gFnSc4
napadnout	napadnout	k5eAaPmF
Olongapo	Olongapa	k1gFnSc5
a	a	k8xC
Ibu	Ibu	k1gMnPc2
a	a	k8xC
26	#num#	k4
G4M1	G4M1	k1gFnPc2
na	na	k7c4
Del	Del	k1gFnSc4
Carmen	Carmen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nichols	Nichols	k1gInSc1
Field	Field	k1gInSc4
napadlo	napadnout	k5eAaPmAgNnS
26	#num#	k4
G4M1	G4M1	k1gMnPc2
z	z	k7c2
Kanoja	Kanojum	k1gNnSc2
kókútai	kókútai	k6eAd1
a	a	k8xC
26	#num#	k4
G3M1	G3M1	k1gMnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eskortu	eskorta	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
celkem	celkem	k6eAd1
33	#num#	k4
A	a	k9
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
věnovaly	věnovat	k5eAaImAgInP,k5eAaPmAgInP
hloubkovým	hloubkový	k2eAgInPc3d1
útokům	útok	k1gInPc3
na	na	k7c4
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nálety	nálet	k1gInPc7
se	se	k3xPyFc4
podle	podle	k7c2
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	i	k8xC
pokusil	pokusit	k5eAaPmAgMnS
narušit	narušit	k5eAaPmF
pouze	pouze	k6eAd1
jediný	jediný	k2eAgMnSc1d1
P-	P-	k1gMnSc1
<g/>
40	#num#	k4
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
si	se	k3xPyFc3
japonští	japonský	k2eAgMnPc1d1
stíhači	stíhač	k1gMnPc1
nárokovali	nárokovat	k5eAaImAgMnP
jako	jako	k9
sestřelený	sestřelený	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Bartsche	Bartsch	k1gFnSc2
byl	být	k5eAaImAgInS
jedinou	jediný	k2eAgFnSc4d1
P-40	P-40	k1gFnSc4
sestřelenou	sestřelený	k2eAgFnSc4d1
onoho	onen	k3xDgInSc2
dne	den	k1gInSc2
stroj	stroj	k1gInSc1
1	#num#	k4
<g/>
st	st	kA
Lt	Lt	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
L.	L.	kA
Cosse	Cosse	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
během	během	k7c2
průzkumného	průzkumný	k2eAgInSc2d1
letu	let	k1gInSc2
„	„	k?
<g/>
sundali	sundat	k5eAaPmAgMnP
<g/>
“	“	k?
japonští	japonský	k2eAgMnPc1d1
stíhači	stíhač	k1gMnPc1
nad	nad	k7c7
Aparri	Aparri	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
neděli	neděle	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
opět	opět	k6eAd1
armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
zaútočilo	zaútočit	k5eAaPmAgNnS
na	na	k7c4
luzonská	luzonský	k2eAgNnPc4d1
letiště	letiště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Del	Del	k1gFnSc4
Carmen	Carmen	k1gInSc4
zaútočilo	zaútočit	k5eAaPmAgNnS
11	#num#	k4
Ki-	Ki-	k1gFnSc1
<g/>
27	#num#	k4
50	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
(	(	kIx(
<g/>
operující	operující	k2eAgMnSc1d1
z	z	k7c2
Aparri	Aparri	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šest	šest	k4xCc1
Ki-	Ki-	k1gFnSc2
<g/>
21	#num#	k4
14	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
a	a	k8xC
několik	několik	k4yIc4
Ki-	Ki-	k1gFnPc2
<g/>
27	#num#	k4
24	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
z	z	k7c2
Viganu	Vigan	k1gInSc2
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
navíc	navíc	k6eAd1
napadla	napadnout	k5eAaPmAgFnS
Clark	Clark	k1gInSc4
Field	Fieldo	k1gNnPc2
a	a	k8xC
Cabanatuan	Cabanatuana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgInSc4d1
letectvo	letectvo	k1gNnSc1
vyslalo	vyslat	k5eAaPmAgNnS
26	#num#	k4
G4M1	G4M1	k1gMnPc2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
a	a	k8xC
devět	devět	k4xCc4
A6M2	A6M2	k1gFnPc2
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
proti	proti	k7c3
cílům	cíl	k1gInPc3
v	v	k7c6
Manilské	manilský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
také	také	k6eAd1
k	k	k7c3
přesunu	přesun	k1gInSc3
jednotek	jednotka	k1gFnPc2
na	na	k7c4
Luzon	Luzon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Aparri	Aparr	k1gFnSc2
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
část	část	k1gFnSc1
16	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
s	s	k7c7
lehkými	lehký	k2eAgMnPc7d1
bombardéry	bombardér	k1gMnPc7
Ki-	Ki-	k1gFnSc2
<g/>
30	#num#	k4
a	a	k8xC
do	do	k7c2
Legazpi	Legazpi	k1gNnSc2
se	se	k3xPyFc4
přesunulo	přesunout	k5eAaPmAgNnS
šest	šest	k4xCc1
G4M1	G4M1	k1gFnPc2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
a	a	k8xC
devět	devět	k4xCc4
A6M2	A6M2	k1gFnPc2
Tainan	Tainany	k1gInPc2
kókútai	kókútai	k6eAd1
doprovázených	doprovázený	k2eAgInPc2d1
dvěma	dva	k4xCgFnPc7
C	C	kA
<g/>
5	#num#	k4
<g/>
M.	M.	kA
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
Američané	Američan	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
odpoledne	odpoledne	k1gNnSc4
proti	proti	k7c3
lodím	loď	k1gFnPc3
u	u	k7c2
Legazpi	Legazpi	k1gNnSc2
šest	šest	k4xCc1
B-17	B-17	k1gFnPc2
z	z	k7c2
Del	Del	k1gFnSc2
Monte	Mont	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
poruchám	porucha	k1gFnPc3
k	k	k7c3
cíli	cíl	k1gInSc3
doletěly	doletět	k5eAaPmAgInP
pouze	pouze	k6eAd1
tři	tři	k4xCgInPc4
bombardéry	bombardér	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezpůsobily	způsobit	k5eNaPmAgInP
žádné	žádný	k3yNgFnPc4
vážné	vážný	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
byl	být	k5eAaImAgMnS
sestřelen	sestřelen	k2eAgMnSc1d1
stíhači	stíhač	k1gMnPc1
a	a	k8xC
druhý	druhý	k4xOgInSc1
poškozen	poškozen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Večer	večer	k6eAd1
ještě	ještě	k6eAd1
na	na	k7c4
letiště	letiště	k1gNnSc4
u	u	k7c2
Legazpi	Legazp	k1gFnSc2
zaútočil	zaútočit	k5eAaPmAgMnS
osamělý	osamělý	k2eAgMnSc1d1
P-40	P-40	k1gMnSc1
pilotovaný	pilotovaný	k2eAgInSc4d1
Mahonym	Mahonym	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
poškodil	poškodit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
A6M2	A6M2	k1gFnPc4
a	a	k8xC
pět	pět	k4xCc4
G	G	kA
<g/>
4	#num#	k4
<g/>
M	M	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
u	u	k7c2
Aparri	Aparr	k1gFnSc2
ponorka	ponorka	k1gFnSc1
USS	USS	kA
Seawolf	Seawolf	k1gMnSc1
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
japonskou	japonský	k2eAgFnSc4d1
zásobovací	zásobovací	k2eAgFnSc4d1
loď	loď	k1gFnSc4
hydroplánů	hydroplán	k1gInPc2
Sanjó	Sanjó	k1gFnSc2
Maru	Maru	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zasáhla	zasáhnout	k5eAaPmAgFnS
jedním	jeden	k4xCgNnSc7
torpédem	torpédo	k1gNnSc7
Mark	Mark	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torpédo	torpédo	k1gNnSc1
selhalo	selhat	k5eAaPmAgNnS
a	a	k8xC
Sanjó	Sanjó	k1gMnPc2
Maru	Maru	k1gFnPc2
mohla	moct	k5eAaImAgFnS
dál	daleko	k6eAd2
plnit	plnit	k5eAaImF
své	svůj	k3xOyFgFnPc4
povinnosti	povinnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jelikož	jelikož	k8xS
Japonci	Japonec	k1gMnPc1
získali	získat	k5eAaPmAgMnP
naprostou	naprostý	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
admirál	admirál	k1gMnSc1
Hart	Hart	k1gMnSc1
zachránit	zachránit	k5eAaPmF
zbytek	zbytek	k1gInSc4
jednotek	jednotka	k1gFnPc2
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
ještě	ještě	k6eAd1
zůstávaly	zůstávat	k5eAaImAgInP
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
a	a	k8xC
odeslat	odeslat	k5eAaPmF
je	on	k3xPp3gMnPc4
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
tak	tak	k9
11	#num#	k4
letuschopných	letuschopný	k2eAgFnPc2d1
Catalin	Catalina	k1gFnPc2
PatWingu	PatWing	k1gInSc2
10	#num#	k4
zamířilo	zamířit	k5eAaPmAgNnS
na	na	k7c4
Ambon	ambon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
mezipřistání	mezipřistání	k1gNnSc2
na	na	k7c6
jezeře	jezero	k1gNnSc6
Lanao	Lanao	k6eAd1
na	na	k7c6
Mindanau	Mindanaum	k1gNnSc6
byla	být	k5eAaImAgFnS
ztracena	ztratit	k5eAaPmNgFnS
Catalina	Catalina	k1gFnSc1
102-P-24	102-P-24	k4
BuNo	buna	k1gFnSc5
1217	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
najela	najet	k5eAaPmAgFnS
na	na	k7c4
skálu	skála	k1gFnSc4
a	a	k8xC
potopila	potopit	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
Ze	z	k7c2
šesti	šest	k4xCc2
<g />
.	.	kIx.
</s>
<s hack="1">
Catalin	Catalin	k1gInSc1
ponechaných	ponechaný	k2eAgMnPc2d1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
později	pozdě	k6eAd2
čtyři	čtyři	k4xCgInPc4
stroje	stroj	k1gInPc4
zprovoznit	zprovoznit	k5eAaPmF
a	a	k8xC
musely	muset	k5eAaImAgFnP
se	se	k3xPyFc4
přes	přes	k7c4
den	den	k1gInSc4
skrývat	skrývat	k5eAaImF
před	před	k7c7
japonskými	japonský	k2eAgNnPc7d1
letadly	letadlo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
Spolu	spolu	k6eAd1
s	s	k7c7
Catalinami	Catalina	k1gFnPc7
odpluly	odplout	k5eAaPmAgFnP
na	na	k7c4
jih	jih	k1gInSc4
i	i	k8xC
poslední	poslední	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
zásobovací	zásobovací	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
hydroplánů	hydroplán	k1gInPc2
Childs	Childs	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
B.	B.	kA
Preston	Preston	k1gInSc1
a	a	k8xC
Heron	Heron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jih	jih	k1gInSc4
zamířil	zamířit	k5eAaPmAgMnS
i	i	k9
štáb	štáb	k1gInSc4
asijského	asijský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Hart	Hart	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
zůstat	zůstat	k5eAaPmF
v	v	k7c6
Manile	Manila	k1gFnSc6
<g/>
,	,	kIx,
dokud	dokud	k8xS
budou	být	k5eAaImBp3nP
z	z	k7c2
Filipín	Filipíny	k1gFnPc2
operovat	operovat	k5eAaImF
ponorky	ponorka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hartovi	Hart	k1gMnSc6
tak	tak	k9
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
zůstaly	zůstat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc4
torpédoborce	torpédoborec	k1gInPc4
<g/>
,	,	kIx,
všech	všecek	k3xTgInPc2
šest	šest	k4xCc4
torpédových	torpédový	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc1
zásobovací	zásobovací	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
ponorek	ponorka	k1gFnPc2
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
dělové	dělový	k2eAgInPc1d1
čluny	člun	k1gInPc1
<g/>
,	,	kIx,
27	#num#	k4
ponorek	ponorka	k1gFnPc2
a	a	k8xC
několik	několik	k4yIc1
pomocných	pomocný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
pondělí	pondělí	k1gNnSc6
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
opakovala	opakovat	k5eAaImAgFnS
obvyklá	obvyklý	k2eAgFnSc1d1
sada	sada	k1gFnSc1
náletů	nálet	k1gInPc2
na	na	k7c4
luzonská	luzonský	k2eAgNnPc4d1
letiště	letiště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ki-	Ki-	k1gFnSc1
<g/>
27	#num#	k4
24	#num#	k4
<g/>
.	.	kIx.
čútai	čúta	k1gFnSc6
a	a	k8xC
Ki-	Ki-	k1gFnSc6
<g/>
21	#num#	k4
14	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc2
napadly	napadnout	k5eAaPmAgInP
třikrát	třikrát	k6eAd1
Del	Del	k1gFnSc4
Carmen	Carmen	k1gInSc1
<g/>
,	,	kIx,
11	#num#	k4
Ki-	Ki-	k1gFnSc1
<g/>
30	#num#	k4
16	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
a	a	k8xC
18	#num#	k4
Ki-	Ki-	k1gFnSc1
<g/>
27	#num#	k4
50	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
zaútočilo	zaútočit	k5eAaPmAgNnS
na	na	k7c4
Clark	Clark	k1gInSc4
Field	Field	k1gMnSc1
a	a	k8xC
Del	Del	k1gMnSc1
Carmen	Carmen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ki-	Ki-	k1gFnSc1
<g/>
48	#num#	k4
8	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc6
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
Tarlac	Tarlac	k1gFnSc4
a	a	k8xC
Baguio	Baguio	k1gNnSc4
<g/>
.	.	kIx.
26	#num#	k4
G4M1	G4M1	k1gFnPc2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
a	a	k8xC
15	#num#	k4
A6M2	A6M2	k1gFnPc2
3	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k1gNnSc2
napadly	napadnout	k5eAaPmAgInP
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
Del	Del	k1gFnSc4
Carmen	Carmen	k1gInSc4
a	a	k8xC
cíle	cíl	k1gInPc4
v	v	k7c6
Manilské	manilský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
na	na	k7c4
nové	nový	k2eAgNnSc4d1
tajné	tajný	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
západně	západně	k6eAd1
od	od	k7c2
Lubao	Lubao	k1gMnSc1
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
bataanského	bataanský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
úterý	úterý	k1gNnSc6
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zaútočily	zaútočit	k5eAaPmAgInP
tři	tři	k4xCgInPc1
P-40	P-40	k1gMnPc2
na	na	k7c4
viganské	viganský	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lt	Lt	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russell	Russell	k1gInSc1
M.	M.	kA
Church	Church	k1gInSc1
byl	být	k5eAaImAgInS
sestřelen	sestřelit	k5eAaPmNgInS
protiletadlovou	protiletadlový	k2eAgFnSc7d1
palbou	palba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zemi	zem	k1gFnSc6
bylo	být	k5eAaImAgNnS
zničeno	zničit	k5eAaPmNgNnS
osm	osm	k4xCc1
letounů	letoun	k1gInPc2
a	a	k8xC
sedm	sedm	k4xCc1
poškozeno	poškodit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wagner	Wagner	k1gMnSc1
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
své	svůj	k3xOyFgNnSc4
páté	pátý	k4xOgNnSc4
vzdušné	vzdušný	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
sestřelil	sestřelit	k5eAaPmAgInS
jedinou	jediný	k2eAgFnSc4d1
Ki-	Ki-	k1gFnSc4
<g/>
27	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
odstartovat	odstartovat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
útok	útok	k1gInSc1
sice	sice	k8xC
byl	být	k5eAaImAgMnS
porušením	porušení	k1gNnSc7
MacArthurova	MacArthurův	k2eAgInSc2d1
rozkazu	rozkaz	k1gInSc2
o	o	k7c6
vyhýbání	vyhýbání	k1gNnSc6
se	se	k3xPyFc4
boje	boj	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k6eAd1
byli	být	k5eAaImAgMnP
Wagner	Wagner	k1gMnSc1
a	a	k8xC
Church	Church	k1gMnSc1
vyznamenáni	vyznamenán	k2eAgMnPc1d1
DSC	DSC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
Breretonův	Breretonův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
přeletělo	přeletět	k5eAaPmAgNnS
ve	v	k7c6
středu	střed	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
šest	šest	k4xCc1
B-17	B-17	k1gMnPc2
z	z	k7c2
Filipín	Filipíny	k1gFnPc2
na	na	k7c4
Batchelor	Batchelor	k1gInSc4
Field	Fielda	k1gFnPc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
je	být	k5eAaImIp3nS
následovaly	následovat	k5eAaImAgInP
čtyři	čtyři	k4xCgFnPc4
další	další	k2eAgFnPc4d1
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
odlétla	odlétnout	k5eAaPmAgFnS
poslední	poslední	k2eAgFnSc1d1
B-	B-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
skončilo	skončit	k5eAaPmAgNnS
působení	působení	k1gNnSc3
B-17	B-17	k1gFnSc2
ze	z	k7c2
základen	základna	k1gFnPc2
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rovněž	rovněž	k9
na	na	k7c6
japonské	japonský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
k	k	k7c3
přesunům	přesun	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ki-	Ki-	k1gFnSc1
<g/>
27	#num#	k4
24	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
se	se	k3xPyFc4
přesunuly	přesunout	k5eAaPmAgInP
z	z	k7c2
Viganu	Vigan	k1gInSc2
na	na	k7c4
sever	sever	k1gInSc4
do	do	k7c2
Laoagu	Laoag	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zabránilo	zabránit	k5eAaPmAgNnS
opakování	opakování	k1gNnSc1
překvapivých	překvapivý	k2eAgInPc2d1
útoků	útok	k1gInPc2
jako	jako	k8xS,k8xC
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
a	a	k8xC
G4M1	G4M1	k1gMnSc3
Kanoja	Kanoja	k1gMnSc1
kókútai	kókúta	k1gMnSc3
se	se	k3xPyFc4
přemístilo	přemístit	k5eAaPmAgNnS
z	z	k7c2
Formosy	Formosa	k1gFnSc2
na	na	k7c6
Peleliu	Pelelium	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
Armádní	armádní	k2eAgMnPc1d1
Ki-	Ki-	k1gMnPc1
<g/>
21	#num#	k4
16	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc1
<g/>
,	,	kIx,
Ki-	Ki-	k1gFnSc1
<g/>
27	#num#	k4
50	#num#	k4
<g/>
.	.	kIx.
čútai	čúta	k1gFnSc6
a	a	k8xC
Ki-	Ki-	k1gFnSc6
<g/>
48	#num#	k4
8	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc6
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Field	k1gMnSc1
a	a	k8xC
Zablan	Zablan	k1gMnSc1
Field	Field	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
Ki-	Ki-	k1gFnSc7
<g/>
48	#num#	k4
8	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc2
napadly	napadnout	k5eAaPmAgInP
Tarlac	Tarlac	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
časných	časný	k2eAgFnPc6d1
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
vylodili	vylodit	k5eAaPmAgMnP
v	v	k7c4
Davao	Davao	k1gNnSc4
na	na	k7c6
Mindanau	Mindanaum	k1gNnSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Jižní	jižní	k2eAgFnPc4d1
a	a	k8xC
centrální	centrální	k2eAgFnPc4d1
Filipíny	Filipíny	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
Šestice	šestice	k1gFnSc2
P-35A	P-35A	k1gFnSc2
od	od	k7c2
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
napadla	napadnout	k5eAaPmAgFnS
školní	školní	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
u	u	k7c2
Viganu	Vigan	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
nacházeli	nacházet	k5eAaImAgMnP
Japonci	Japonec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
Japonské	japonský	k2eAgInPc1d1
armádní	armádní	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
4	#num#	k4
<g/>
.	.	kIx.
sentai	sentai	k1gNnSc6
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
letiště	letiště	k1gNnSc4
Iba	iba	k6eAd1
<g/>
,	,	kIx,
Clark	Clark	k1gInSc1
a	a	k8xC
Nichols	Nichols	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
sentai	sentai	k1gNnPc6
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
letiště	letiště	k1gNnSc4
Nichols	Nicholsa	k1gFnPc2
<g/>
,	,	kIx,
Cabantuan	Cabantuana	k1gFnPc2
a	a	k8xC
zálivy	záliv	k1gInPc4
Subic	Subice	k1gFnPc2
a	a	k8xC
Baler	Balra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ki-	Ki-	k1gFnSc1
<g/>
55	#num#	k4
od	od	k7c2
52	#num#	k4
<g/>
.	.	kIx.
šótai	šótai	k1gNnSc2
(	(	kIx(
<g/>
小	小	k?
~	~	kIx~
letka	letka	k1gFnSc1
<g/>
)	)	kIx)
10	#num#	k4
<g/>
.	.	kIx.
dokuricu	dokuricu	k6eAd1
čútai	čútai	k6eAd1
shodil	shodit	k5eAaPmAgMnS
nad	nad	k7c7
Manilou	Manila	k1gFnSc7
letáky	leták	k1gInPc1
s	s	k7c7
výzvou	výzva	k1gFnSc7
ke	k	k7c3
kapitulaci	kapitulace	k1gFnSc3
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc4
samý	samý	k3xTgInSc4
den	den	k1gInSc4
zřídilo	zřídit	k5eAaPmAgNnS
námořnictvo	námořnictvo	k1gNnSc1
základnu	základna	k1gFnSc4
hydroplánů	hydroplán	k1gInPc2
ve	v	k7c6
Viganu	Vigan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operovalo	operovat	k5eAaImAgNnS
odtud	odtud	k6eAd1
dvanáct	dvanáct	k4xCc1
F1M	F1M	k1gMnPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
E13A	E13A	k1gMnSc1
ze	z	k7c2
Sanuki	Sanuki	k1gNnSc2
Maru	Maru	k1gFnSc2
a	a	k8xC
Sanjó	Sanjó	k1gFnSc2
Maru	Maru	k1gFnSc2
a	a	k8xC
tři	tři	k4xCgInPc4
letouny	letoun	k1gInPc4
z	z	k7c2
Natori	Natori	k1gNnSc2
a	a	k8xC
Naka	Nakum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
Útoky	útok	k1gInPc1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
se	se	k3xPyFc4
soustředily	soustředit	k5eAaPmAgFnP
převážně	převážně	k6eAd1
na	na	k7c4
Mindanao	Mindanao	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
čtyři	čtyři	k4xCgFnPc1
A6M2	A6M2	k1gFnPc1
od	od	k7c2
Tainan	Tainany	k1gInPc2
kókútai	kókúta	k1gFnSc2
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
letiště	letiště	k1gNnSc4
Batangas	Batangasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnPc1
piloti	pilot	k1gMnPc1
si	se	k3xPyFc3
nárokovali	nárokovat	k5eAaImAgMnP
zničení	zničení	k1gNnSc4
pěti	pět	k4xCc2
stíhacích	stíhací	k2eAgInPc2d1
letounů	letoun	k1gInPc2
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
pravděpodobně	pravděpodobně	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
staly	stát	k5eAaPmAgFnP
pouze	pouze	k6eAd1
boje	boj	k1gInPc1
neschopné	schopný	k2eNgFnSc2d1
P-26A	P-26A	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
perutě	peruť	k1gFnSc2
filipínského	filipínský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
útok	útok	k1gInSc1
</s>
<s>
Dosavadní	dosavadní	k2eAgNnPc1d1
vylodění	vylodění	k1gNnPc1
měla	mít	k5eAaImAgNnP
za	za	k7c4
hlavní	hlavní	k2eAgInSc4d1
cíl	cíl	k1gInSc4
zajistit	zajistit	k5eAaPmF
letiště	letiště	k1gNnSc4
pro	pro	k7c4
námořní	námořní	k2eAgNnSc4d1
a	a	k8xC
armádní	armádní	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
této	tento	k3xDgFnSc2
fáze	fáze	k1gFnSc2
ofenzívy	ofenzíva	k1gFnSc2
nadešel	nadejít	k5eAaPmAgInS
čas	čas	k1gInSc4
pro	pro	k7c4
hlavní	hlavní	k2eAgInSc4d1
úder	úder	k1gInSc4
<g/>
:	:	kIx,
postup	postup	k1gInSc4
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
severu	sever	k1gInSc2
měly	mít	k5eAaImAgFnP
postupovat	postupovat	k5eAaImF
jednotky	jednotka	k1gFnPc4
vyloděné	vyloděný	k2eAgFnPc4d1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Luzonu	Luzon	k1gInSc2
a	a	k8xC
z	z	k7c2
jihu	jih	k1gInSc2
měly	mít	k5eAaImAgFnP
postupovat	postupovat	k5eAaImF
jednotky	jednotka	k1gFnPc4
vyloděné	vyloděný	k2eAgFnPc4d1
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c4
Lamon	Lamon	k1gNnSc4
Bay	Bay	k1gFnSc2
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Luzonu	Luzon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vylodění	vylodění	k1gNnSc1
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Invazní	invazní	k2eAgInPc1d1
konvoje	konvoj	k1gInPc1
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
i	i	k8xC
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
používaly	používat	k5eAaImAgFnP
pro	pro	k7c4
obojživelné	obojživelný	k2eAgFnPc4d1
operace	operace	k1gFnPc4
v	v	k7c6
hojné	hojný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
14,6	14,6	k4
metrů	metr	k1gInPc2
dlouhé	dlouhý	k2eAgInPc4d1
vyloďovací	vyloďovací	k2eAgInPc4d1
čluny	člun	k1gInPc4
typu	typ	k1gInSc2
daihacu	daihacus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
unesly	unést	k5eAaPmAgFnP
až	až	k9
10	#num#	k4
tun	tuna	k1gFnPc2
nákladu	náklad	k1gInSc2
</s>
<s>
Ráno	ráno	k6eAd1
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vyplul	vyplout	k5eAaPmAgMnS
z	z	k7c2
Kirungu	Kirung	k1gInSc2
na	na	k7c6
severu	sever	k1gInSc6
Formosy	Formosa	k1gFnSc2
první	první	k4xOgFnSc2
ze	z	k7c2
tří	tři	k4xCgInPc2
invazních	invazní	k2eAgInPc2d1
konvojů	konvoj	k1gInPc2
převážejících	převážející	k2eAgFnPc2d1
vojáky	voják	k1gMnPc7
a	a	k8xC
materiál	materiál	k1gInSc1
48	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
(	(	kIx(
<g/>
bez	bez	k7c2
2	#num#	k4
<g/>
.	.	kIx.
formoského	formoský	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
byly	být	k5eAaImAgFnP
sestaveny	sestaven	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
Tanaka	Tanak	k1gMnSc2
a	a	k8xC
Kanno	Kanno	k6eAd1
<g/>
)	)	kIx)
generálporučíka	generálporučík	k1gMnSc2
Juiči	Juič	k1gInPc7
Cučibašiho	Cučibaši	k1gMnSc2
pro	pro	k7c4
vylodění	vylodění	k1gNnSc4
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládal	skládat	k5eAaImAgMnS
se	se	k3xPyFc4
z	z	k7c2
21	#num#	k4
transportních	transportní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
eskortovaných	eskortovaný	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
poledne	poledne	k1gNnSc4
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vyplul	vyplout	k5eAaPmAgMnS
z	z	k7c2
Mako	mako	k1gNnSc4
na	na	k7c4
Pescadorech	Pescador	k1gMnPc6
druhý	druhý	k4xOgInSc1
konvoj	konvoj	k1gInSc1
28	#num#	k4
transportérů	transportér	k1gInPc2
a	a	k8xC
odpoledne	odpoledne	k1gNnSc4
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
opustil	opustit	k5eAaPmAgMnS
Takao	Takao	k6eAd1
na	na	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
Formose	Formosa	k1gFnSc6
třetí	třetí	k4xOgInSc4
konvoj	konvoj	k1gInSc4
složený	složený	k2eAgInSc4d1
z	z	k7c2
27	#num#	k4
transportérů	transportér	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Každý	každý	k3xTgInSc1
z	z	k7c2
konvojů	konvoj	k1gInPc2
měl	mít	k5eAaImAgInS
přiděleno	přidělen	k2eAgNnSc4d1
vlastní	vlastní	k2eAgNnSc4d1
krytí	krytí	k1gNnSc4
a	a	k8xC
vlastní	vlastní	k2eAgInSc4d1
vyloďovací	vyloďovací	k2eAgInSc4d1
úsek	úsek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
v	v	k7c6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
nejvíce	hodně	k6eAd3,k6eAd1
na	na	k7c6
jihu	jih	k1gInSc6
u	u	k7c2
Agoo	Agoo	k1gNnSc4
vylodit	vylodit	k5eAaPmF
taisa	tais	k1gMnSc4
(	(	kIx(
<g/>
大	大	k?
~	~	kIx~
plukovník	plukovník	k1gMnSc1
<g/>
)	)	kIx)
Isamu	Isama	k1gFnSc4
Janagi	Janag	k1gFnSc2
s	s	k7c7
47	#num#	k4
<g/>
.	.	kIx.
pěším	pěší	k2eAgInPc3d1
(	(	kIx(
<g/>
bez	bez	k7c2
jednoho	jeden	k4xCgInSc2
praporu	prapor	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
tankovým	tankový	k2eAgInSc7d1
plukem	pluk	k1gInSc7
(	(	kIx(
<g/>
bez	bez	k7c2
jedné	jeden	k4xCgFnSc2
roty	rota	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
přepravoval	přepravovat	k5eAaImAgInS
konvoj	konvoj	k1gInSc1
z	z	k7c2
Takao	Takao	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgInSc3
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
použito	použít	k5eAaPmNgNnS
69	#num#	k4
vyloďovacích	vyloďovací	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
:	:	kIx,
od	od	k7c2
malých	malý	k2eAgInPc2d1
šóhacu	šóhacu	k6eAd1
<g/>
,	,	kIx,
přes	přes	k7c4
větší	veliký	k2eAgFnSc4d2
daihacu	daihaca	k1gFnSc4
až	až	k9
po	po	k7c6
velké	velká	k1gFnSc6
toku	tok	k1gInSc2
daihacu	daihacus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
schopné	schopný	k2eAgFnPc1d1
převážet	převážet	k5eAaImF
jeden	jeden	k4xCgInSc4
tank	tank	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
10	#num#	k4
kilometrů	kilometr	k1gInPc2
severněji	severně	k6eAd2
a	a	k8xC
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
u	u	k7c2
Caba	Cab	k1gInSc2
vylodit	vylodit	k5eAaPmF
taisa	taisa	k1gFnSc1
Hifumi	Hifu	k1gFnPc7
Imai	Ima	k1gFnSc2
s	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
formoským	formoské	k1gNnPc3
pěším	pěší	k2eAgNnPc3d1
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
tankovým	tankový	k2eAgInSc7d1
plukem	pluk	k1gInSc7
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
přepravoval	přepravovat	k5eAaImAgInS
konvoj	konvoj	k1gInSc1
z	z	k7c2
Mako	mako	k1gNnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
měly	mít	k5eAaImAgFnP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
57	#num#	k4
vyloďovacích	vyloďovací	k2eAgMnPc2d1
člunů	člun	k1gInPc2
a	a	k8xC
19	#num#	k4
motorizovaných	motorizovaný	k2eAgInPc2d1
sampanů	sampan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
poslední	poslední	k2eAgMnSc1d1
se	se	k3xPyFc4
nejseverněji	severně	k6eAd3
u	u	k7c2
Bauangu	Bauang	k1gInSc2
měla	mít	k5eAaImAgFnS
vylodit	vylodit	k5eAaPmF
„	„	k?
<g/>
skupina	skupina	k1gFnSc1
Kamidžima	Kamidžima	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
velitel	velitel	k1gMnSc1
taisa	tais	k1gMnSc2
Kamidžima	Kamidžimum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tvořená	tvořený	k2eAgFnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
pěším	pěší	k2eAgInSc7d1
plukem	pluk	k1gInSc7
a	a	k8xC
přepravovaná	přepravovaný	k2eAgNnPc4d1
Kirungským	Kirungský	k2eAgInSc7d1
konvojem	konvoj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
měla	mít	k5eAaImAgFnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
20	#num#	k4
vyloďovacích	vyloďovací	k2eAgMnPc2d1
člunů	člun	k1gInPc2
a	a	k8xC
29	#num#	k4
motorizovaných	motorizovaný	k2eAgInPc2d1
sampanů	sampan	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celkem	celkem	k6eAd1
transportéry	transportér	k1gInPc1
přepravovaly	přepravovat	k5eAaImAgInP
43	#num#	k4
110	#num#	k4
mužů	muž	k1gMnPc2
Hommovy	Hommův	k2eAgFnSc2d1
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
konvojů	konvoj	k1gInPc2
plul	plout	k5eAaImAgMnS
i	i	k9
generálporučík	generálporučík	k1gMnSc1
Homma	Homm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Doprovod	doprovod	k1gInSc1
konvojům	konvoj	k1gInPc3
a	a	k8xC
krytí	krytí	k1gNnSc3
vylodění	vylodění	k1gNnSc2
zajišťovaly	zajišťovat	k5eAaImAgInP
Takahašiho	Takahaši	k1gMnSc2
těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Ašigara	Ašigar	k1gMnSc2
a	a	k8xC
Maja	Maja	k1gFnSc1
<g/>
,	,	kIx,
lehké	lehký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Kuma	kum	k1gMnSc2
<g/>
,	,	kIx,
Naka	Nakus	k1gMnSc2
a	a	k8xC
Natori	Nator	k1gFnSc2
<g/>
,	,	kIx,
16	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
a	a	k8xC
množství	množství	k1gNnSc2
menších	malý	k2eAgNnPc2d2
plavidel	plavidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
těm	ten	k3xDgMnPc3
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
vzdálené	vzdálený	k2eAgNnSc1d1
krytí	krytí	k1gNnSc1
přidalo	přidat	k5eAaPmAgNnS
Kondóovo	Kondóův	k2eAgNnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
loďstvo	loďstvo	k1gNnSc1
složené	složený	k2eAgNnSc1d1
z	z	k7c2
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Kongo	Kongo	k1gNnSc1
a	a	k8xC
Haruna	Haruna	k1gFnSc1
<g/>
,	,	kIx,
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
Atago	Atago	k6eAd1
a	a	k8xC
Takao	Takao	k6eAd1
a	a	k8xC
osmi	osm	k4xCc2
torpédoborců	torpédoborec	k1gInPc2
uvolněných	uvolněný	k2eAgInPc2d1
z	z	k7c2
malajské	malajský	k2eAgFnSc2d1
operace	operace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konvoje	konvoj	k1gInPc1
pluly	plout	k5eAaImAgInP
bez	bez	k7c2
letecké	letecký	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
až	až	k9
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
jim	on	k3xPp3gMnPc3
ochranu	ochrana	k1gFnSc4
poskytly	poskytnout	k5eAaPmAgInP
Ki-	Ki-	k1gFnSc4
<g/>
27	#num#	k4
24	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
21	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vplula	vplout	k5eAaPmAgFnS
japonská	japonský	k2eAgFnSc1d1
invazní	invazní	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
do	do	k7c2
Lingayenského	Lingayenský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
ze	z	k7c2
76	#num#	k4
armádních	armádní	k2eAgFnPc2d1
transportních	transportní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
tam	tam	k6eAd1
zakotvily	zakotvit	k5eAaPmAgFnP
krátce	krátce	k6eAd1
po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
navigační	navigační	k2eAgFnSc3d1
chybě	chyba	k1gFnSc3
zakotvil	zakotvit	k5eAaPmAgInS
nejjižnější	jižní	k2eAgInSc1d3
konvoj	konvoj	k1gInSc1
jižněji	jižně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k6eAd1
u	u	k7c2
Agoo	Agoo	k6eAd1
kotvil	kotvit	k5eAaImAgInS
naproti	naproti	k6eAd1
asi	asi	k9
6,5	6,5	k4
kilometru	kilometr	k1gInSc2
jižněji	jižně	k6eAd2
ležícímu	ležící	k2eAgInSc3d1
Santo	Santo	k1gNnSc1
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vylodění	vylodění	k1gNnSc1
a	a	k8xC
první	první	k4xOgFnPc1
reakce	reakce	k1gFnPc1
obránců	obránce	k1gMnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
Filipínských	filipínský	k2eAgMnPc2d1
skautů	skaut	k1gMnPc2
projíždí	projíždět	k5eAaImIp3nP
kolem	kolem	k7c2
lehkého	lehký	k2eAgInSc2d1
tanku	tank	k1gInSc2
M3	M3	k1gFnSc2
Stuart	Stuarta	k1gFnPc2
na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Pozorrubio	Pozorrubia	k1gFnSc5
</s>
<s>
Ponorka	ponorka	k1gFnSc1
USS	USS	kA
S-	S-	k1gFnSc1
<g/>
38	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ráno	ráno	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
dokázala	dokázat	k5eAaPmAgFnS
proniknout	proniknout	k5eAaPmF
do	do	k7c2
Lingayenského	Lingayenský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
a	a	k8xC
potopit	potopit	k5eAaPmF
transportér	transportér	k1gInSc4
Hajo	Hajo	k6eAd1
Maru	Maru	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
celodenním	celodenní	k2eAgNnSc6d1
pronásledování	pronásledování	k1gNnSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
nakonec	nakonec	k9
podařilo	podařit	k5eAaPmAgNnS
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
na	na	k7c4
fotografii	fotografia	k1gFnSc4
po	po	k7c6
přestavbě	přestavba	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
v	v	k7c6
noci	noc	k1gFnSc6
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
první	první	k4xOgFnPc1
jednotky	jednotka	k1gFnPc1
přesouvat	přesouvat	k5eAaImF
do	do	k7c2
vyloďovacích	vyloďovací	k2eAgInPc2d1
člunů	člun	k1gInPc2
a	a	k8xC
v	v	k7c6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
vkročili	vkročit	k5eAaPmAgMnP
první	první	k4xOgMnPc1
vojáci	voják	k1gMnPc1
na	na	k7c4
suchou	suchý	k2eAgFnSc4d1
zem	zem	k1gFnSc4
jižně	jižně	k6eAd1
od	od	k7c2
Agoo	Agoo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
5	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
dosáhly	dosáhnout	k5eAaPmAgInP
pláží	pláž	k1gFnSc7
u	u	k7c2
Aringay	Aringaa	k1gFnSc2
(	(	kIx(
<g/>
jižně	jižně	k6eAd1
od	od	k7c2
Caba	Cab	k1gInSc2
<g/>
)	)	kIx)
první	první	k4xOgInPc1
čluny	člun	k1gInPc1
prostředního	prostřední	k2eAgInSc2d1
konvoje	konvoj	k1gInSc2
z	z	k7c2
Mako	mako	k1gNnSc4
a	a	k8xC
v	v	k7c6
7	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
se	se	k3xPyFc4
na	na	k7c4
břeh	břeh	k1gInSc4
u	u	k7c2
Bauangu	Bauang	k1gInSc2
dostala	dostat	k5eAaPmAgFnS
první	první	k4xOgFnSc1
část	část	k1gFnSc1
skupiny	skupina	k1gFnSc2
Kamidžima	Kamidžima	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
skupiny	skupina	k1gFnSc2
Kamidžima	Kamidžim	k1gMnSc2
se	se	k3xPyFc4
v	v	k7c6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
vylodil	vylodit	k5eAaPmAgMnS
u	u	k7c2
jižněji	jižně	k6eAd2
ležícího	ležící	k2eAgNnSc2d1
Santiaga	Santiago	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američané	Američan	k1gMnPc1
sice	sice	k8xC
měli	mít	k5eAaImAgMnP
zprávy	zpráva	k1gFnPc4
o	o	k7c6
blížícím	blížící	k2eAgNnSc6d1
se	se	k3xPyFc4
konvoji	konvoj	k1gInPc7
již	již	k6eAd1
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
a	a	k8xC
prostor	prostor	k1gInSc4
Lingayenského	Lingayenský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
byl	být	k5eAaImAgInS
(	(	kIx(
<g/>
zcela	zcela	k6eAd1
správně	správně	k6eAd1
<g/>
)	)	kIx)
považován	považován	k2eAgMnSc1d1
za	za	k7c4
pravděpodobné	pravděpodobný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
vylodění	vylodění	k1gNnSc2
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
postupovat	postupovat	k5eAaImF
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
nebyli	být	k5eNaImAgMnP
obránci	obránce	k1gMnPc1
schopni	schopen	k2eAgMnPc1d1
zajistit	zajistit	k5eAaPmF
odpovídající	odpovídající	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
daného	daný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc4d1
prostor	prostor	k1gInSc4
Lingayenského	Lingayenský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
měly	mít	k5eAaImAgFnP
bránit	bránit	k5eAaImF
pouhé	pouhý	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
divize	divize	k1gFnPc4
FA	fa	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
stranu	strana	k1gFnSc4
zálivu	záliv	k1gInSc2
až	až	k9
po	po	k7c4
San	San	k1gFnSc4
Fernando	Fernanda	k1gFnSc5
na	na	k7c6
severu	sever	k1gInSc6
hájila	hájit	k5eAaImAgFnS
11	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
FA	fa	kA
posílená	posílený	k2eAgFnSc1d1
o	o	k7c4
71	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
LTC	LTC	kA
Donalda	Donald	k1gMnSc4
Van	vana	k1gFnPc2
N.	N.	kA
Bonnetta	Bonnetta	k1gFnSc1
71	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
a	a	k8xC
jižní	jižní	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
zálivu	záliv	k1gInSc2
mela	mlít	k5eAaImSgInS
chránit	chránit	k5eAaImF
21	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šestadvacátý	šestadvacátý	k4xOgInSc1
pluk	pluk	k1gInSc1
kavalérie	kavalérie	k1gFnSc2
FS	FS	kA
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
na	na	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
3	#num#	k4
v	v	k7c4
Pozorrubio	Pozorrubio	k1gNnSc4
a	a	k8xC
měl	mít	k5eAaImAgInS
bránit	bránit	k5eAaImF
japonskému	japonský	k2eAgInSc3d1
postupu	postup	k1gInSc3
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Těsně	těsně	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
první	první	k4xOgMnPc1
Japonci	Japonec	k1gMnPc1
vstoupili	vstoupit	k5eAaPmAgMnP
na	na	k7c4
vyloďovací	vyloďovací	k2eAgFnPc4d1
pláže	pláž	k1gFnPc4
<g/>
,	,	kIx,
se	s	k7c7
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
začala	začít	k5eAaPmAgFnS
přesunovat	přesunovat	k5eAaImF
z	z	k7c2
Pozorrubio	Pozorrubio	k6eAd1
do	do	k7c2
Rosario	Rosario	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Četa	četa	k1gFnSc1
průzkumníků	průzkumník	k1gMnPc2
pokračovala	pokračovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
Damortis	Damortis	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
na	na	k7c4
sever	sever	k1gInSc4
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
amerických	americký	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
pronikla	proniknout	k5eAaPmAgFnS
do	do	k7c2
zálivu	záliv	k1gInSc2
pouze	pouze	k6eAd1
USS	USS	kA
S-38	S-38	k1gFnPc2
a	a	k8xC
potopila	potopit	k5eAaPmAgFnS
5445	#num#	k4
<g/>
tunový	tunový	k2eAgInSc4d1
armádní	armádní	k2eAgInSc4d1
transportér	transportér	k1gInSc4
Hajo	Hajo	k1gNnSc4
Maru	Maru	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Zdecimované	zdecimovaný	k2eAgNnSc1d1
Dálněvýchodní	dálněvýchodní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
se	se	k3xPyFc4
zmohlo	zmoct	k5eAaPmAgNnS
pouze	pouze	k6eAd1
na	na	k7c4
symbolický	symbolický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
:	:	kIx,
několik	několik	k4yIc4
stíhaček	stíhačka	k1gFnPc2
vyslaných	vyslaný	k2eAgFnPc2d1
postřelovat	postřelovat	k5eAaImF
a	a	k8xC
bombardovat	bombardovat	k5eAaImF
předmostí	předmostí	k1gNnSc4
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
plavidla	plavidlo	k1gNnSc2
nemohlo	moct	k5eNaImAgNnS
vylodění	vylodění	k1gNnSc1
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
pravděpodobně	pravděpodobně	k6eAd1
o	o	k7c4
dva	dva	k4xCgMnPc4
P-35	P-35	k1gMnPc4
a	a	k8xC
dvě	dva	k4xCgFnPc4
P-	P-	k1gFnPc4
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
Burton	Burton	k1gInSc1
a	a	k8xC
Bartsch	Bartsch	k1gMnSc1
popisují	popisovat	k5eAaImIp3nP
akci	akce	k1gFnSc4
šesti	šest	k4xCc2
P-40	P-40	k1gFnPc2
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
a	a	k8xC
tří	tři	k4xCgFnPc2
P-40	P-40	k1gFnPc2
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
(	(	kIx(
<g/>
další	další	k2eAgFnPc1d1
tři	tři	k4xCgFnPc4
P-40	P-40	k1gFnPc4
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
k	k	k7c3
cíli	cíl	k1gInSc3
nedoletěly	doletět	k5eNaPmAgInP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
armádní	armádní	k2eAgFnPc1d1
Ki-	Ki-	k1gFnPc1
<g/>
48	#num#	k4
bombardovaly	bombardovat	k5eAaImAgInP
Nichols	Nichols	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
Camp	camp	k1gInSc4
Morphy	Morpha	k1gFnSc2
a	a	k8xC
Limay	Limaa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Budování	budování	k1gNnSc1
předmostí	předmostí	k1gNnSc2
</s>
<s>
Na	na	k7c6
většině	většina	k1gFnSc6
pláží	pláž	k1gFnPc2
Japonci	Japonec	k1gMnPc1
prakticky	prakticky	k6eAd1
nenarazili	narazit	k5eNaPmAgMnP
na	na	k7c4
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
u	u	k7c2
Bauangu	Bauang	k1gInSc2
se	se	k3xPyFc4
skupině	skupina	k1gFnSc3
Kamidžima	Kamidžimum	k1gNnSc2
postavil	postavit	k5eAaPmAgInS
na	na	k7c4
odpor	odpor	k1gInSc4
velitelský	velitelský	k2eAgInSc4d1
prapor	prapor	k1gInSc4
12	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jedním	jeden	k4xCgInSc7
těžkým	těžký	k2eAgInSc7d1
a	a	k8xC
několika	několik	k4yIc7
lehkými	lehký	k2eAgInPc7d1
kulomety	kulomet	k1gInPc7
dokázal	dokázat	k5eAaPmAgInS
skupině	skupina	k1gFnSc3
Kamidžima	Kamidžim	k1gMnSc4
způsobit	způsobit	k5eAaPmF
těžké	těžký	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
vytvoření	vytvoření	k1gNnSc6
předmostí	předmostí	k1gNnPc4
Japoncům	Japonec	k1gMnPc3
zabránit	zabránit	k5eAaPmF
nedokázal	dokázat	k5eNaPmAgMnS
a	a	k8xC
následně	následně	k6eAd1
ustoupil	ustoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupině	skupina	k1gFnSc6
Kamidžima	Kamidžima	k1gNnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
u	u	k7c2
Bauangu	Bauang	k1gInSc2
podařilo	podařit	k5eAaPmAgNnS
přepadnout	přepadnout	k5eAaPmF
nepřipravený	připravený	k2eNgInSc4d1
detašovaný	detašovaný	k2eAgInSc4d1
71	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
71	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
předchozího	předchozí	k2eAgInSc2d1
dne	den	k1gInSc2
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
Tanakův	Tanakův	k2eAgInSc4d1
2	#num#	k4
<g/>
.	.	kIx.
formoský	formoský	k2eAgInSc1d1
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
v	v	k7c6
San	San	k1gFnSc6
Fernando	Fernanda	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
71	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
ustoupila	ustoupit	k5eAaPmAgFnS
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
Baguio	Baguio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zbývajících	zbývající	k2eAgFnPc6d1
plážích	pláž	k1gFnPc6
bylo	být	k5eAaImAgNnS
jediným	jediný	k2eAgMnSc7d1
protivníkem	protivník	k1gMnSc7
Japonců	Japonec	k1gMnPc2
počasí	počasí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlny	vlna	k1gFnPc1
znesnadňovaly	znesnadňovat	k5eAaImAgFnP
přestupování	přestupování	k1gNnSc4
do	do	k7c2
výsadkových	výsadkový	k2eAgInPc2d1
člunů	člun	k1gInPc2
a	a	k8xC
některé	některý	k3yIgInPc1
čluny	člun	k1gInPc1
kvůli	kvůli	k7c3
vlnám	vlna	k1gFnPc3
uvázly	uváznout	k5eAaPmAgFnP
na	na	k7c6
plážích	pláž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
vlně	vlna	k1gFnSc6
měla	mít	k5eAaImAgFnS
následovat	následovat	k5eAaImF
druhá	druhý	k4xOgFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nepříznivé	příznivý	k2eNgNnSc1d1
počasí	počasí	k1gNnSc1
narušilo	narušit	k5eAaPmAgNnS
časování	časování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
stoupajícím	stoupající	k2eAgInSc7d1
přílivem	příliv	k1gInSc7
musely	muset	k5eAaImAgFnP
vyloďovací	vyloďovací	k2eAgFnPc1d1
plavidla	plavidlo	k1gNnPc1
změnit	změnit	k5eAaPmF
kotviště	kotviště	k1gNnSc2
a	a	k8xC
přesunuly	přesunout	k5eAaPmAgInP
se	se	k3xPyFc4
ještě	ještě	k9
více	hodně	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
nejjižněji	jižně	k6eAd3
umístěná	umístěný	k2eAgNnPc1d1
plavidla	plavidlo	k1gNnPc1
dostala	dostat	k5eAaPmAgNnP
na	na	k7c4
dostřel	dostřel	k1gInSc4
děl	dělo	k1gNnPc2
86	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc3
polního	polní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
FS	FS	kA
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
jedna	jeden	k4xCgFnSc1
baterie	baterie	k1gFnSc1
dvou	dva	k4xCgMnPc6
155	#num#	k4
<g/>
mm	mm	kA
děl	dělo	k1gNnPc2
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
u	u	k7c2
San	San	k1gFnSc2
Fabian	Fabian	k1gMnSc1
a	a	k8xC
další	další	k2eAgFnSc1d1
dvoudělová	dvoudělový	k2eAgFnSc1d1
baterie	baterie	k1gFnSc1
měla	mít	k5eAaImAgFnS
svůj	svůj	k3xOyFgInSc4
palpost	palpost	k1gInSc4
u	u	k7c2
Dagupanu	Dagupan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádnou	žádný	k3yNgFnSc4
škodu	škoda	k1gFnSc4
ale	ale	k8xC
invazním	invazní	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
nezpůsobily	způsobit	k5eNaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
vylodění	vylodění	k1gNnSc6
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
Kamidžima	Kamidžima	k1gFnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
<g/>
)	)	kIx)
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
sever	sever	k1gInSc4
po	po	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
se	se	k3xPyFc4
předsunuté	předsunutý	k2eAgFnPc1d1
hlídky	hlídka	k1gFnPc1
skupiny	skupina	k1gFnSc2
Kamidžima	Kamidžima	k1gFnSc1
setkaly	setkat	k5eAaPmAgFnP
s	s	k7c7
hlídkama	hlídkama	k?
Tanakova	Tanakův	k2eAgNnSc2d1
2	#num#	k4
<g/>
.	.	kIx.
formoského	formoský	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
během	během	k7c2
několika	několik	k4yIc2
posledních	poslední	k2eAgInPc2d1
dnů	den	k1gInPc2
prošel	projít	k5eAaPmAgMnS
z	z	k7c2
Viganu	Vigan	k1gInSc2
a	a	k8xC
Appari	Appar	k1gFnSc2
až	až	k9
do	do	k7c2
San	San	k1gFnSc2
Fernando	Fernanda	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
spojení	spojení	k1gNnSc4
obou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
prapor	prapor	k1gInSc1
skupiny	skupina	k1gFnSc2
Kamidžima	Kamidžimum	k1gNnSc2
obsadil	obsadit	k5eAaPmAgMnS
a	a	k8xC
zajistil	zajistit	k5eAaPmAgMnS
(	(	kIx(
<g/>
kolem	kolem	k7c2
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
město	město	k1gNnSc1
Bauang	Bauanga	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc4
prapor	prapor	k1gInSc4
téže	týž	k3xTgFnSc2,k3xDgFnSc2
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
na	na	k7c4
východ	východ	k1gInSc4
po	po	k7c6
silnici	silnice	k1gFnSc6
na	na	k7c4
Baguio	Baguio	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
obsadil	obsadit	k5eAaPmAgMnS
letiště	letiště	k1gNnSc4
v	v	k7c4
Naguilian	Naguilian	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odpoledne	odpoledne	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
stále	stále	k6eAd1
ještě	ještě	k6eAd1
hlavní	hlavní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
japonské	japonský	k2eAgFnSc2d1
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnPc1
nacházely	nacházet	k5eAaImAgFnP
na	na	k7c6
lodích	loď	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Generálporučík	generálporučík	k1gMnSc1
Homa	Homa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
na	na	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
lodí	loď	k1gFnPc2
u	u	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
de	de	k?
facto	facto	k1gNnSc4
ztratil	ztratit	k5eAaPmAgInS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
jednotkami	jednotka	k1gFnPc7
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
žádnému	žádný	k3yNgInSc3
americkému	americký	k2eAgInSc3d1
protiútoku	protiútok	k1gInSc3
v	v	k7c6
tomto	tento	k3xDgNnSc6
–	–	k?
pro	pro	k7c4
Japonce	Japonec	k1gMnPc4
kritickém	kritický	k2eAgInSc6d1
–	–	k?
okamžiku	okamžik	k1gInSc6
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
ve	v	k7c6
štábu	štáb	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
ozvaly	ozvat	k5eAaPmAgInP
opatrné	opatrný	k2eAgInPc1d1
hlasy	hlas	k1gInPc1
volající	volající	k2eAgInPc1d1
po	po	k7c6
vybudování	vybudování	k1gNnSc6
předmostí	předmostí	k1gNnSc2
u	u	k7c2
Agno	Agno	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
bude	být	k5eAaImBp3nS
zahájen	zahájit	k5eAaPmNgInS
postup	postup	k1gInSc1
k	k	k7c3
Manile	Manila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homma	Hommum	k1gNnPc1
ale	ale	k8xC
nakonec	nakonec	k6eAd1
dal	dát	k5eAaPmAgMnS
na	na	k7c4
agresivnější	agresivní	k2eAgInPc4d2
hlasy	hlas	k1gInPc4
ve	v	k7c6
štábu	štáb	k1gInSc6
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgInS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
postupu	postup	k1gInSc6
podle	podle	k7c2
plánu	plán	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
zdržování	zdržování	k1gNnSc2
se	se	k3xPyFc4
budováním	budování	k1gNnSc7
předmostí	předmostí	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozbouřené	rozbouřený	k2eAgNnSc1d1
moře	moře	k1gNnSc1
stále	stále	k6eAd1
bránilo	bránit	k5eAaImAgNnS
vylodění	vylodění	k1gNnSc1
dělostřelectva	dělostřelectvo	k1gNnSc2
a	a	k8xC
dalšího	další	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homma	Hommum	k1gNnSc2
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
přesunout	přesunout	k5eAaPmF
kotviště	kotviště	k1gNnSc4
ještě	ještě	k9
více	hodně	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
vyloďování	vyloďování	k1gNnSc6
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
ale	ale	k9
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
neutralizovat	neutralizovat	k5eAaBmF
155	#num#	k4
<g/>
mm	mm	kA
baterii	baterie	k1gFnSc4
u	u	k7c2
San	San	k1gFnSc2
Fabian	Fabian	k1gMnSc1
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
zálivu	záliv	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
jinak	jinak	k6eAd1
měla	mít	k5eAaImAgFnS
zakotvené	zakotvený	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
jako	jako	k8xC,k8xS
na	na	k7c6
dlani	dlaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
byla	být	k5eAaImAgFnS
pověřena	pověřen	k2eAgFnSc1d1
48	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Postup	postup	k1gInSc1
na	na	k7c4
jih	jih	k1gInSc4
<g/>
:	:	kIx,
Damortis	Damortis	k1gInSc4
a	a	k8xC
Rosario	Rosario	k6eAd1
</s>
<s>
Na	na	k7c6
plážích	pláž	k1gFnPc6
uprostřed	uprostřed	k7c2
vyloďovacího	vyloďovací	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
u	u	k7c2
Aringay	Aringaa	k1gFnSc2
se	se	k3xPyFc4
shromáždily	shromáždit	k5eAaPmAgFnP
vyloděné	vyloděný	k2eAgFnPc1d1
Imaiovy	Imaiův	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
a	a	k8xC
před	před	k7c7
polednem	poledne	k1gNnSc7
zahájily	zahájit	k5eAaPmAgInP
postup	postup	k1gInSc4
na	na	k7c4
jih	jih	k1gInSc4
po	po	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
na	na	k7c6
Damortis	Damortis	k1gFnSc6
(	(	kIx(
<g/>
jižně	jižně	k6eAd1
od	od	k7c2
Santo	Santo	k1gNnSc1
Thomas	Thomas	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Rosario	Rosario	k6eAd1
(	(	kIx(
<g/>
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
východně	východně	k6eAd1
od	od	k7c2
Damortis	Damortis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
Janagiho	Janagiha	k1gFnSc5
jednotkám	jednotka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylodění	vylodění	k1gNnSc1
Janagiho	Janagi	k1gMnSc2
jednotek	jednotka	k1gFnPc2
na	na	k7c6
nejjižnějších	jižní	k2eAgFnPc6d3
plážích	pláž	k1gFnPc6
mezi	mezi	k7c7
Damortis	Damortis	k1gFnSc7
a	a	k8xC
Santo	Santo	k1gNnSc1
Thomas	Thomas	k1gMnSc1
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
nesetkalo	setkat	k5eNaPmAgNnS
s	s	k7c7
odporem	odpor	k1gInSc7
a	a	k8xC
Janagi	Janagi	k1gNnSc7
okamžitě	okamžitě	k6eAd1
zahájil	zahájit	k5eAaPmAgInS
postup	postup	k1gInSc1
na	na	k7c4
jih	jih	k1gInSc4
na	na	k7c6
Damortis	Damortis	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
Damortis	Damortis	k1gFnSc2
na	na	k7c4
sever	sever	k1gInSc4
postupující	postupující	k2eAgFnSc1d1
četa	četa	k1gFnSc1
průzkumníků	průzkumník	k1gMnPc2
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc2
FS	FS	kA
narazila	narazit	k5eAaPmAgFnS
již	již	k6eAd1
vyloděné	vyloděný	k2eAgInPc4d1
japonské	japonský	k2eAgInPc4d1
pluky	pluk	k1gInPc4
<g />
.	.	kIx.
</s>
<s hack="1">
48	#num#	k4
<g/>
.	.	kIx.
průzkumný	průzkumný	k2eAgInSc4d1
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
a	a	k8xC
stáhla	stáhnout	k5eAaPmAgFnS
se	se	k3xPyFc4
do	do	k7c2
Damortis	Damortis	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
Jedenáctá	jedenáctý	k4xOgFnSc1
divize	divize	k1gFnSc1
FA	fa	kA
proti	proti	k7c3
Janagiho	Janagiha	k1gFnSc5
vyloďujícím	vyloďující	k2eAgFnPc3d1
se	se	k3xPyFc4
jednotkám	jednotka	k1gFnPc3
vyslala	vyslat	k5eAaPmAgFnS
jeden	jeden	k4xCgInSc4
pěší	pěší	k2eAgInSc4d1
prapor	prapor	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
rovněž	rovněž	k9
narazil	narazit	k5eAaPmAgMnS
na	na	k7c4
48	#num#	k4
<g/>
.	.	kIx.
průzkumný	průzkumný	k2eAgInSc4d1
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
a	a	k8xC
rovněž	rovněž	k9
ustoupil	ustoupit	k5eAaPmAgMnS
do	do	k7c2
Damortis	Damortis	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
Damortis	Damortis	k1gFnSc2
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
z	z	k7c2
Rosario	Rosario	k6eAd1
i	i	k8xC
zbytek	zbytek	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc2
FS	FS	kA
Col	cola	k1gFnPc2
(	(	kIx(
<g/>
plukovník	plukovník	k1gMnSc1
<g/>
)	)	kIx)
Clintona	Clinton	k1gMnSc2
A.	A.	kA
Pierce	Pierce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pierce	Pierec	k1gInSc2
v	v	k7c6
Damortis	Damortis	k1gFnSc6
vybudoval	vybudovat	k5eAaPmAgMnS
obranné	obranný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
71	#num#	k4
<g/>
.	.	kIx.
plukem	pluk	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
ve	v	k7c6
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
byly	být	k5eAaImAgFnP
Pierceho	Pierceha	k1gFnSc5
jednotky	jednotka	k1gFnPc1
vystaveny	vystaven	k2eAgFnPc1d1
útoku	útok	k1gInSc3
postupujících	postupující	k2eAgMnPc2d1
Japonců	Japonec	k1gMnPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
armádního	armádní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pierce	Pierec	k1gInSc2
proto	proto	k8xC
žádal	žádat	k5eAaImAgMnS
o	o	k7c4
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wainwright	Wainwright	k1gMnSc1
nařídil	nařídit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
do	do	k7c2
oblasti	oblast	k1gFnSc2
přesunula	přesunout	k5eAaPmAgFnS
rota	rota	k1gFnSc1
tanků	tank	k1gInPc2
192	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
USAFFE	USAFFE	kA
uvolnil	uvolnit	k5eAaPmAgMnS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
severoluzonského	severoluzonský	k2eAgNnSc2d1
velitelství	velitelství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
paliva	palivo	k1gNnSc2
se	se	k3xPyFc4
ale	ale	k9
do	do	k7c2
boje	boj	k1gInSc2
vydala	vydat	k5eAaPmAgFnS
pouze	pouze	k6eAd1
pět	pět	k4xCc4
tanků	tank	k1gInPc2
jedné	jeden	k4xCgFnSc2
čety	četa	k1gFnSc2
roty	rota	k1gFnSc2
C	C	kA
192	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Agoo	Agoo	k6eAd1
se	se	k3xPyFc4
americké	americký	k2eAgFnPc1d1
M3	M3	k1gFnPc1
Stuart	Stuarta	k1gFnPc2
setkaly	setkat	k5eAaPmAgFnP
se	se	k3xPyFc4
svými	svůj	k3xOyFgInPc7
japonskými	japonský	k2eAgInPc7d1
protějšky	protějšek	k1gInPc7
typu	typ	k1gInSc2
95	#num#	k4
Ha-gó	Ha-gó	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
první	první	k4xOgFnSc6
tankové	tankový	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
na	na	k7c6
tichomořském	tichomořský	k2eAgNnSc6d1
válčišti	válčiště	k1gNnSc6
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
jeden	jeden	k4xCgInSc4
americký	americký	k2eAgInSc4d1
tank	tank	k1gInSc4
zničen	zničen	k2eAgInSc4d1
a	a	k8xC
zbývající	zbývající	k2eAgFnPc4d1
čtyři	čtyři	k4xCgFnPc4
ustoupily	ustoupit	k5eAaPmAgFnP
do	do	k7c2
Rosario	Rosario	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
později	pozdě	k6eAd2
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
zničilo	zničit	k5eAaPmAgNnS
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
se	se	k3xPyFc4
Imaiova	Imaiův	k2eAgFnSc1d1
formace	formace	k1gFnSc1
postupující	postupující	k2eAgFnSc1d1
od	od	k7c2
Aringay	Aringaa	k1gFnSc2
spojila	spojit	k5eAaPmAgFnS
se	s	k7c7
48	#num#	k4
<g/>
.	.	kIx.
průzkumným	průzkumný	k2eAgNnPc3d1
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
tankovým	tankový	k2eAgInSc7d1
plukem	pluk	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
Pierce	Pierec	k1gInSc2
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
vyklidit	vyklidit	k5eAaPmF
Damortis	Damortis	k1gInSc4
a	a	k8xC
stáhl	stáhnout	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
pozici	pozice	k1gFnSc4
východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
již	již	k6eAd1
měli	mít	k5eAaImAgMnP
Japonci	Japonec	k1gMnPc1
město	město	k1gNnSc4
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
od	od	k7c2
Damortis	Damortis	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
16	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
Pierce	Pierec	k1gInSc2
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
brigádním	brigádní	k2eAgMnSc7d1
generálem	generál	k1gMnSc7
Clyde	Clyd	k1gInSc5
A.	A.	kA
Selleckem	Sellecko	k1gNnSc7
<g/>
,	,	kIx,
velitelem	velitel	k1gMnSc7
71	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
FA	fa	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
přesunovala	přesunovat	k5eAaImAgFnS
z	z	k7c2
Urdaneta	Urdaneto	k1gNnSc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Damortis	Damortis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šestadvacátá	šestadvacátý	k4xOgFnSc1
kavalerie	kavalerie	k1gFnSc1
byla	být	k5eAaImAgFnS
mezitím	mezitím	k6eAd1
podřízena	podřízen	k2eAgFnSc1d1
71	#num#	k4
<g/>
.	.	kIx.
divizi	divize	k1gFnSc6
a	a	k8xC
Selleck	Selleck	k1gInSc1
jí	on	k3xPp3gFnSc7
po	po	k7c6
zjištění	zjištění	k1gNnSc6
stavu	stav	k1gInSc2
rozkázal	rozkázat	k5eAaPmAgMnS
ustoupit	ustoupit	k5eAaPmF
do	do	k7c2
Rosario	Rosario	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
město	město	k1gNnSc1
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
stát	stát	k5eAaPmF,k5eAaImF
hlavním	hlavní	k2eAgInSc7d1
obranným	obranný	k2eAgInSc7d1
bodem	bod	k1gInSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
západu	západ	k1gInSc2
ale	ale	k8xC
Rosario	Rosario	k6eAd1
ohrožovaly	ohrožovat	k5eAaImAgFnP
japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
(	(	kIx(
<g/>
48	#num#	k4
<g/>
.	.	kIx.
průzkumný	průzkumný	k2eAgInSc4d1
</s>
<s>
a	a	k8xC
Imaiův	Imaiův	k2eAgMnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
formoský	formoský	k2eAgInSc1d1
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
a	a	k8xC
část	část	k1gFnSc1
48	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
horských	horský	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
)	)	kIx)
postupující	postupující	k2eAgFnSc1d1
z	z	k7c2
Damortis	Damortis	k1gFnSc2
a	a	k8xC
od	od	k7c2
severozápadu	severozápad	k1gInSc2
postupoval	postupovat	k5eAaImAgInS
Janagiho	Janagi	k1gMnSc2
47	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
z	z	k7c2
Agoo	Agoo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ústup	ústup	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc2
do	do	k7c2
Rosario	Rosario	k6eAd1
byl	být	k5eAaImAgInS
podporován	podporovat	k5eAaImNgInS
tanky	tank	k1gInPc7
roty	rota	k1gFnSc2
C	C	kA
192	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tankisté	tankista	k1gMnPc1
se	se	k3xPyFc4
ale	ale	k9
ve	v	k7c6
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
začaly	začít	k5eAaPmAgInP
stahovat	stahovat	k5eAaImF
do	do	k7c2
Rosario	Rosario	k6eAd1
a	a	k8xC
kavalerii	kavalerie	k1gFnSc4
nechali	nechat	k5eAaPmAgMnP
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgInPc1d1
tanky	tank	k1gInPc1
záhy	záhy	k6eAd1
nahradily	nahradit	k5eAaPmAgInP
tanky	tank	k1gInPc1
japonské	japonský	k2eAgInPc1d1
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
ve	v	k7c6
tmě	tma	k1gFnSc6
pronikly	proniknout	k5eAaPmAgFnP
přes	přes	k7c4
zadní	zadní	k2eAgInSc4d1
voj	voj	k1gInSc4
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnPc4
a	a	k8xC
způsobily	způsobit	k5eAaPmAgFnP
masakr	masakr	k1gInSc4
mezi	mezi	k7c7
jejími	její	k3xOp3gMnPc7
příslušníky	příslušník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonskému	japonský	k2eAgInSc3d1
průniku	průnik	k1gInSc3
až	až	k9
k	k	k7c3
Rosario	Rosario	k6eAd1
zabránilo	zabránit	k5eAaPmAgNnS
až	až	k9
kousek	kousek	k1gInSc4
západně	západně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
zablokování	zablokování	k1gNnSc3
jednoho	jeden	k4xCgInSc2
mostu	most	k1gInSc2
přes	přes	k7c4
říčku	říčka	k1gFnSc4
zasaženým	zasažený	k2eAgInSc7d1
tankem	tank	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc2
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ze	z	k7c2
severozápadu	severozápad	k1gInSc2
postupující	postupující	k2eAgFnSc2d1
Janagiho	Janagi	k1gMnSc2
47	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
se	se	k3xPyFc4
střetl	střetnout	k5eAaPmAgInS
se	s	k7c7
skupinou	skupina	k1gFnSc7
F	F	kA
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
donutil	donutit	k5eAaPmAgMnS
k	k	k7c3
ústupu	ústup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dosažení	dosažení	k1gNnSc6
Rosario	Rosario	k6eAd1
mu	on	k3xPp3gNnSc3
ale	ale	k9
zabránilo	zabránit	k5eAaPmAgNnS
odvolání	odvolání	k1gNnSc1
zpět	zpět	k6eAd1
do	do	k7c2
Agoo	Agoo	k6eAd1
a	a	k8xC
pověření	pověření	k1gNnSc4
úkolem	úkol	k1gInSc7
zajistit	zajistit	k5eAaPmF
San	San	k1gFnSc3
Fabian	Fabian	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tím	ten	k3xDgNnSc7
de	de	k?
facto	facto	k1gNnSc1
skončil	skončit	k5eAaPmAgInS
japonský	japonský	k2eAgInSc1d1
postup	postup	k1gInSc1
v	v	k7c4
den	den	k1gInSc4
vylodění	vylodění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
stály	stát	k5eAaImAgFnP
na	na	k7c6
jihu	jih	k1gInSc6
před	před	k7c7
Rosario	Rosario	k6eAd1
(	(	kIx(
<g/>
ze	z	k7c2
kterého	který	k3yIgNnSc2,k3yRgNnSc2,k3yQgNnSc2
se	se	k3xPyFc4
obránci	obránce	k1gMnPc1
již	již	k6eAd1
stáhli	stáhnout	k5eAaPmAgMnP
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
se	se	k3xPyFc4
blížily	blížit	k5eAaImAgFnP
k	k	k7c3
Baguio	Baguio	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
postup	postup	k1gInSc1
23	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1
Masaharu	Masahar	k1gInSc2
Homma	Hommum	k1gNnSc2
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
vstupuje	vstupovat	k5eAaImIp3nS
osobně	osobně	k6eAd1
na	na	k7c4
Luzonský	Luzonský	k2eAgInSc4d1
břeh	břeh	k1gInSc4
v	v	k7c6
Lingayenském	Lingayenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
</s>
<s>
Ráno	ráno	k6eAd1
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
nad	nad	k7c4
Lingayenský	Lingayenský	k2eAgInSc4d1
záliv	záliv	k1gInSc4
podařilo	podařit	k5eAaPmAgNnS
proniknout	proniknout	k5eAaPmF
čtyřem	čtyři	k4xCgInPc3
B-17	B-17	k1gMnPc6
od	od	k7c2
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BG	BG	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
předchozího	předchozí	k2eAgInSc2d1
dne	den	k1gInSc2
přiletěly	přiletět	k5eAaPmAgFnP
na	na	k7c4
Del	Del	k1gFnSc4
Monte	Mont	k1gInSc5
z	z	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nálet	nálet	k1gInSc4
ale	ale	k8xC
prakticky	prakticky	k6eAd1
žádné	žádný	k3yNgFnPc4
škody	škoda	k1gFnPc4
nezpůsobil	způsobit	k5eNaPmAgMnS
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
B-17	B-17	k1gMnSc4
unikly	uniknout	k5eAaPmAgFnP
sedmnácti	sedmnáct	k4xCc2
japonským	japonský	k2eAgMnPc3d1
stíhačům	stíhač	k1gMnPc3
<g/>
,	,	kIx,
přistály	přistát	k5eAaImAgInP,k5eAaPmAgInP
bombardéry	bombardér	k1gInPc1
na	na	k7c6
holandském	holandský	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
Ambon	ambona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bonnett	Bonnett	k1gInSc1
v	v	k7c4
Baguio	Baguio	k1gNnSc4
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Japonci	Japonec	k1gMnPc1
již	již	k6eAd1
odřízli	odříznout	k5eAaPmAgMnP
cestu	cesta	k1gFnSc4
na	na	k7c4
jih	jih	k1gInSc4
přes	přes	k7c4
Rosario	Rosario	k6eAd1
a	a	k8xC
se	s	k7c7
svým	svůj	k3xOyFgInSc7
71	#num#	k4
<g/>
.	.	kIx.
plukem	pluk	k1gInSc7
FA	fa	k1gNnSc2
se	se	k3xPyFc4
během	během	k7c2
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
stáhl	stáhnout	k5eAaPmAgMnS
z	z	k7c2
Baguio	Baguio	k1gNnSc4
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
přes	přes	k7c4
hory	hora	k1gFnPc4
do	do	k7c2
Cagayanského	Cagayanský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Baguio	Baguio	k1gNnSc4
zůstal	zůstat	k5eAaPmAgInS
pouze	pouze	k6eAd1
LTC	LTC	kA
John	John	k1gMnSc1
P.	P.	kA
Horan	Horan	k1gMnSc1
–	–	k?
velitel	velitel	k1gMnSc1
Camp	camp	k1gInSc4
John	John	k1gMnSc1
Hay	Hay	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
jihu	jih	k1gInSc6
zaujali	zaujmout	k5eAaPmAgMnP
obránci	obránce	k1gMnPc1
ze	z	k7c2
Selleckovy	Selleckův	k2eAgFnSc2d1
71	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
FA	fa	k1gNnSc2
pozice	pozice	k1gFnSc2
na	na	k7c6
silnici	silnice	k1gFnSc6
č.	č.	k?
3	#num#	k4
vedoucí	vedoucí	k1gFnSc1
z	z	k7c2
Rosaria	rosarium	k1gNnSc2
na	na	k7c4
jih	jih	k1gInSc4
přes	přes	k7c4
Sison	Sison	k1gNnSc4
a	a	k8xC
Pozorrubio	Pozorrubio	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
linie	linie	k1gFnPc1
obrany	obrana	k1gFnSc2
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
jižně	jižně	k6eAd1
od	od	k7c2
Sisonu	Sison	k1gInSc2
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
se	se	k3xPyFc4
mezitím	mezitím	k6eAd1
stáhla	stáhnout	k5eAaPmAgFnS
do	do	k7c2
Pozorrubio	Pozorrubio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopoledne	dopoledne	k1gNnSc1
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
na	na	k7c4
Selleckovu	Selleckův	k2eAgFnSc4d1
linii	linie	k1gFnSc4
zaútočily	zaútočit	k5eAaPmAgInP
dva	dva	k4xCgInPc1
prapory	prapor	k1gInPc1
Janagiho	Janagi	k1gMnSc2
47	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
prošly	projít	k5eAaPmAgFnP
přes	přes	k7c4
Rosario	Rosario	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seleckovi	Seleckův	k2eAgMnPc5d1
se	se	k3xPyFc4
–	–	k?
hlavně	hlavně	k9
díky	díky	k7c3
dělostřelecké	dělostřelecký	k2eAgFnSc3d1
podpoře	podpora	k1gFnSc3
–	–	k?
podařilo	podařit	k5eAaPmAgNnS
japonský	japonský	k2eAgInSc4d1
první	první	k4xOgInSc4
útok	útok	k1gInSc4
odrazit	odrazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časně	časně	k6eAd1
odpoledne	odpoledne	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
Japoncům	Japonec	k1gMnPc3
posila	posila	k1gFnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
48	#num#	k4
<g/>
.	.	kIx.
průzkumného	průzkumný	k2eAgInSc2d1
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odpoledne	odpoledne	k6eAd1
zahájily	zahájit	k5eAaPmAgFnP
japonské	japonský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
nový	nový	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Seleckovu	Seleckův	k2eAgFnSc4d1
linii	linie	k1gFnSc4
podporovaný	podporovaný	k2eAgInSc1d1
armádním	armádní	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipínští	filipínský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
71	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
se	se	k3xPyFc4
před	před	k7c7
útočícími	útočící	k2eAgMnPc7d1
Japonci	Japonec	k1gMnPc7
dali	dát	k5eAaPmAgMnP
na	na	k7c4
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pomoc	pomoc	k1gFnSc4
Seleckovi	Seleckův	k2eAgMnPc1d1
byla	být	k5eAaImAgNnP
z	z	k7c2
Cabanatuanu	Cabanatuan	k1gInSc2
vyslána	vyslán	k2eAgFnSc1d1
91	#num#	k4
<g/>
.	.	kIx.
taktická	taktický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
(	(	kIx(
<g/>
Combat	Combat	k1gFnSc1
Team	team	k1gInSc1
<g/>
)	)	kIx)
91	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgInPc1d1
letouny	letoun	k1gInPc1
ale	ale	k9
zničily	zničit	k5eAaPmAgInP
most	most	k1gInSc4
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Agno	Agno	k1gNnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
zdržely	zdržet	k5eAaPmAgFnP
postup	postup	k1gInSc4
91	#num#	k4
<g/>
.	.	kIx.
taktické	taktický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaha	snaha	k1gFnSc1
vybudovat	vybudovat	k5eAaPmF
a	a	k8xC
udržet	udržet	k5eAaPmF
novou	nový	k2eAgFnSc4d1
linii	linie	k1gFnSc4
severně	severně	k6eAd1
od	od	k7c2
Pozorrubio	Pozorrubio	k6eAd1
selhala	selhat	k5eAaPmAgFnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
očekávaná	očekávaný	k2eAgFnSc1d1
posila	posila	k1gFnSc1
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
Pozorrubio	Pozorrubio	k6eAd1
až	až	k9
večer	večer	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
s	s	k7c7
příchodem	příchod	k1gInSc7
91	#num#	k4
<g/>
.	.	kIx.
taktické	taktický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
se	s	k7c7
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
dala	dát	k5eAaPmAgFnS
na	na	k7c4
další	další	k2eAgInSc4d1
ústup	ústup	k1gInSc4
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Binalonanu	Binalonan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Wainwright	Wainwright	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
od	od	k7c2
MacArthura	MacArthur	k1gMnSc2
svolení	svolení	k1gNnSc2
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
za	za	k7c4
řeku	řeka	k1gFnSc4
Agno	Agno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
noci	noc	k1gFnSc2
na	na	k7c4
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
Pozorrubio	Pozorrubio	k6eAd1
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
do	do	k7c2
rukou	ruka	k1gFnPc2
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
bombardovalo	bombardovat	k5eAaImAgNnS
devět	devět	k4xCc1
G3M	G3M	k1gMnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kókútai	kókútai	k6eAd1
radiostanici	radiostanice	k1gFnSc4
v	v	k7c6
Cavite	Cavit	k1gMnSc5
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
armádní	armádní	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
bombardovaly	bombardovat	k5eAaImAgInP
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
,	,	kIx,
Del	Del	k1gFnSc1
Carmen	Carmen	k1gInSc1
a	a	k8xC
Batangas	Batangas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císařské	císařský	k2eAgNnSc4d1
armádní	armádní	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
na	na	k7c6
Luzonu	Luzon	k1gInSc6
bylo	být	k5eAaImAgNnS
posíleno	posílit	k5eAaPmNgNnS
příletem	přílet	k1gInSc7
14	#num#	k4
<g/>
.	.	kIx.
čútai	čútai	k6eAd1
s	s	k7c7
bombardéry	bombardér	k1gMnPc7
Ki-	Ki-	k1gFnSc2
<g/>
21	#num#	k4
z	z	k7c2
Formosy	Formosa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
svítáním	svítání	k1gNnSc7
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
další	další	k2eAgFnPc1d1
japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
vylodily	vylodit	k5eAaPmAgFnP
jižně	jižně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
v	v	k7c4
Lamon	Lamon	k1gNnSc4
Bay	Bay	k1gFnSc2
<g/>
…	…	k?
</s>
<s>
Ráno	ráno	k6eAd1
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
LTC	LTC	kA
Horan	Horan	k1gMnSc1
stáhl	stáhnout	k5eAaPmAgMnS
z	z	k7c2
Baguio	Baguio	k6eAd1
a	a	k8xC
město	město	k1gNnSc1
během	během	k7c2
dne	den	k1gInSc2
obsadila	obsadit	k5eAaPmAgFnS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
na	na	k7c6
jihu	jih	k1gInSc6
předmostí	předmostí	k1gNnSc2
obnovili	obnovit	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
svůj	svůj	k3xOyFgInSc4
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
narazily	narazit	k5eAaPmAgInP
tanky	tank	k1gInPc1
japonského	japonský	k2eAgMnSc2d1
4	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
na	na	k7c4
muže	muž	k1gMnSc4
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
bránili	bránit	k5eAaImAgMnP
přístup	přístup	k1gInSc4
do	do	k7c2
Binalonanu	Binalonan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opuštěné	opuštěný	k2eAgInPc1d1
ostatními	ostatní	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
se	s	k7c7
26	#num#	k4
<g/>
.	.	kIx.
kavalerii	kavalerie	k1gFnSc4
podařilo	podařit	k5eAaPmAgNnS
samotné	samotný	k2eAgNnSc1d1
a	a	k8xC
bez	bez	k7c2
protitankových	protitankový	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
odrazit	odrazit	k5eAaPmF
útok	útok	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
byl	být	k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
později	pozdě	k6eAd2
ráno	ráno	k6eAd1
posílen	posílit	k5eAaPmNgInS
2	#num#	k4
<g/>
.	.	kIx.
formoským	formoský	k2eAgInSc7d1
pěším	pěší	k2eAgInSc7d1
plukem	pluk	k1gInSc7
<g/>
,	,	kIx,
dokázala	dokázat	k5eAaPmAgFnS
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
klást	klást	k5eAaImF
odpor	odpor	k1gInSc4
až	až	k9
do	do	k7c2
15	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
Wainwrightův	Wainwrightův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
začala	začít	k5eAaPmAgFnS
stahovat	stahovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
večer	večer	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
formoský	formoský	k2eAgInSc1d1
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
obsadil	obsadit	k5eAaPmAgInS
Binalonan	Binalonan	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
26	#num#	k4
<g/>
.	.	kIx.
kavalerie	kavalerie	k1gFnSc1
ustoupila	ustoupit	k5eAaPmAgFnS
přes	přes	k7c4
Agno	Agno	k1gNnSc4
na	na	k7c4
jihovýchod	jihovýchod	k1gInSc4
do	do	k7c2
Tayugu	Tayug	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
noci	noc	k1gFnSc2
na	na	k7c4
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
držely	držet	k5eAaImAgFnP
americké	americký	k2eAgFnPc1d1
a	a	k8xC
filipínské	filipínský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
obranou	obraný	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
tvořila	tvořit	k5eAaImAgFnS
řeka	řeka	k1gFnSc1
Agno	Agno	k6eAd1
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
se	se	k3xPyFc4
táhla	táhnout	k5eAaImAgNnP
mezi	mezi	k7c7
městy	město	k1gNnPc7
Urdaneta	Urdaneto	k1gNnSc2
a	a	k8xC
San	San	k1gMnSc1
Carlos	Carlos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vylodění	vylodění	k1gNnSc1
v	v	k7c4
Lamon	Lamon	k1gInSc4
Bay	Bay	k1gFnSc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Ústup	ústup	k1gInSc1
na	na	k7c4
Bataan	Bataan	k1gInSc4
</s>
<s>
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
MacArthur	MacArthur	k1gMnSc1
do	do	k7c2
chodu	chod	k1gInSc2
předválečný	předválečný	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
plán	plán	k1gInSc1
WPO-3	WPO-3	k1gFnSc1
(	(	kIx(
<g/>
War	War	k1gFnSc1
Plan	plan	k1gInSc1
Orange	Orange	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vyzval	vyzvat	k5eAaPmAgMnS
k	k	k7c3
použití	použití	k1gNnSc3
pěti	pět	k4xCc3
zdržovací	zdržovací	k2eAgFnPc1d1
pozic	pozice	k1gFnPc2
v	v	k7c6
centrálním	centrální	k2eAgInSc6d1
Luzonu	Luzon	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
síly	síla	k1gFnPc1
ustoupily	ustoupit	k5eAaPmAgFnP
na	na	k7c4
poloostrov	poloostrov	k1gInSc4
Bataan	Bataany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
z	z	k7c2
části	část	k1gFnSc2
26	#num#	k4
<g/>
.	.	kIx.
kavalérií	kavalérie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MacArthur	MacArthur	k1gMnSc1
odebral	odebrat	k5eAaPmAgMnS
generálu	generál	k1gMnSc3
Parkerovi	Parker	k1gMnSc3
velení	velení	k1gNnSc2
nad	nad	k7c7
jednotkami	jednotka	k1gFnPc7
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
Luzonu	Luzon	k1gInSc6
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
připravovat	připravovat	k5eAaImF
obranná	obranný	k2eAgNnPc4d1
postavení	postavení	k1gNnPc4
na	na	k7c4
Bataanu	Bataana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
vojenské	vojenský	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
a	a	k8xC
Filipínská	filipínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
byly	být	k5eAaImAgFnP
přesunuty	přesunout	k5eAaPmNgFnP
na	na	k7c4
Bataan	Bataan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
devět	devět	k4xCc4
dnů	den	k1gInPc2
dodávek	dodávka	k1gFnPc2
na	na	k7c4
Bataan	Bataan	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
z	z	k7c2
Manily	Manila	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
snaze	snaha	k1gFnSc6
nakrmit	nakrmit	k5eAaPmF
předpokládané	předpokládaný	k2eAgInPc4d1
síly	síl	k1gInPc4
43.000	43.000	k4
vojáků	voják	k1gMnPc2
po	po	k7c4
dobu	doba	k1gFnSc4
šesti	šest	k4xCc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Nakonec	nakonec	k6eAd1
80.000	80.000	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
26.000	26.000	k4
uprchlíků	uprchlík	k1gMnPc2
zaplavilo	zaplavit	k5eAaPmAgNnS
Bataan	Bataan	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
značné	značný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
zůstaly	zůstat	k5eAaPmAgFnP
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
po	po	k7c4
dobu	doba	k1gFnSc4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jednotky	jednotka	k1gFnPc1
obou	dva	k4xCgFnPc2
obranných	obranný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
byly	být	k5eAaImAgFnP
manévrované	manévrovaný	k2eAgFnPc1d1
tak	tak	k8xS,k8xC
aby	aby	kYmCp3nP
udržely	udržet	k5eAaPmAgFnP
otevřené	otevřený	k2eAgFnPc4d1
únikové	únikový	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
na	na	k7c4
Bataan	Bataan	k1gInSc4
<g/>
,	,	kIx,
zejména	zejména	k9
San	San	k1gMnPc1
Fernando	Fernanda	k1gFnSc5
<g/>
,	,	kIx,
ocelové	ocelový	k2eAgInPc1d1
mosty	most	k1gInPc1
v	v	k7c6
Calumpit	Calumpit	k1gFnSc6
přes	přes	k7c4
hluboké	hluboký	k2eAgFnPc4d1
řeky	řeka	k1gFnPc4
Pampanga	Pampang	k1gMnSc2
na	na	k7c6
severním	severní	k2eAgInSc6d1
konci	konec	k1gInSc6
Manila	Manila	k1gFnSc1
Bay	Bay	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
Plaridel	Plaridlo	k1gNnPc2
severně	severně	k6eAd1
od	od	k7c2
Manily	Manila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gInSc1
Luzon	Luzon	k1gInSc4
Force	force	k1gFnSc2
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
své	svůj	k3xOyFgFnSc3
nezkušenosti	nezkušenost	k1gFnSc3
a	a	k8xC
nejasných	jasný	k2eNgInPc2d1
rozkazů	rozkaz	k1gInPc2
ustupovat	ustupovat	k5eAaImF
a	a	k8xC
držet	držet	k5eAaImF
pozice	pozice	k1gFnPc4
<g/>
,	,	kIx,
úspěšně	úspěšně	k6eAd1
přešly	přejít	k5eAaPmAgInP
mosty	most	k1gInPc1
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
letečtí	letecký	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
zamítli	zamítnout	k5eAaPmAgMnP
bombardovat	bombardovat	k5eAaImF
mosty	most	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
uzavřely	uzavřít	k5eAaPmAgFnP
ustupující	ustupující	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mosty	most	k1gInPc4
byly	být	k5eAaImAgInP
následně	následně	k6eAd1
zničeny	zničit	k5eAaPmNgInP
filipínskými	filipínský	k2eAgMnPc7d1
skautskými	skautský	k2eAgMnPc7d1
inženýry	inženýr	k1gMnPc7
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Japonci	Japonec	k1gMnPc1
si	se	k3xPyFc3
uvědomili	uvědomit	k5eAaPmAgMnP
plný	plný	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
MacArthurova	MacArthurův	k2eAgInSc2d1
plánu	plán	k1gInSc2
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
nařídili	nařídit	k5eAaPmAgMnP
48	#num#	k4
<g/>
.	.	kIx.
divizi	divize	k1gFnSc6
postup	postup	k1gInSc4
a	a	k8xC
obklíčení	obklíčení	k1gNnSc4
Bataanu	Bataan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sérii	série	k1gFnSc6
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
lednem	leden	k1gInSc7
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
Armády	armáda	k1gFnSc2
filipínské	filipínský	k2eAgFnSc2d1
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
kavalérie	kavalérie	k1gFnPc1
a	a	k8xC
americké	americký	k2eAgInPc1d1
tanky	tank	k1gInPc1
M3	M3	k1gFnSc2
Stuart	Stuarta	k1gFnPc2
Prozatímní	prozatímní	k2eAgFnSc2d1
tankové	tankový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
drželi	držet	k5eAaImAgMnP
otevřenou	otevřený	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
ze	z	k7c2
San	San	k1gFnSc2
Fernando	Fernanda	k1gFnSc5
na	na	k7c4
Dinalupihan	Dinalupihan	k1gInSc1
na	na	k7c6
severu	sever	k1gInSc6
poloostrova	poloostrov	k1gInSc2
pro	pro	k7c4
ustupující	ustupující	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Jižní	jižní	k2eAgFnSc2d1
Luzonské	Luzonský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
sami	sám	k3xTgMnPc1
dali	dát	k5eAaPmAgMnP
na	na	k7c4
ústup	ústup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
50	#num#	k4
<g/>
%	%	kIx~
ztrát	ztráta	k1gFnPc2
194	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
během	během	k7c2
ústupu	ústup	k1gInSc2
a	a	k8xC
podpůrných	podpůrný	k2eAgFnPc2d1
baterií	baterie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
opakovaně	opakovaně	k6eAd1
zastavily	zastavit	k5eAaPmAgFnP
japonské	japonský	k2eAgInPc4d1
výpady	výpad	k1gInPc4
a	a	k8xC
byly	být	k5eAaImAgFnP
posledními	poslední	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
vstupujícími	vstupující	k2eAgFnPc7d1
na	na	k7c4
Bataan	Bataan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Bataan	Bataan	k1gInSc4
</s>
<s>
Od	od	k7c2
7	#num#	k4
do	do	k7c2
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
soustředili	soustředit	k5eAaPmAgMnP
na	na	k7c4
průzkum	průzkum	k1gInSc4
a	a	k8xC
připravovali	připravovat	k5eAaImAgMnP
útok	útok	k1gInSc4
na	na	k7c4
hlavní	hlavní	k2eAgFnPc4d1
bitevní	bitevní	k2eAgFnPc4d1
linie	linie	k1gFnPc4
od	od	k7c2
Abucay	Abucaa	k1gFnSc2
k	k	k7c3
hoře	hora	k1gFnSc3
Natib	Natiba	k1gFnPc2
na	na	k7c4
Maubanu	Maubana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
kritickou	kritický	k2eAgFnSc7d1
chybou	chyba	k1gFnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vystřídána	vystřídán	k2eAgFnSc1d1
48	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
úspěchu	úspěch	k1gInSc2
japonských	japonský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
za	za	k7c4
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
schopnou	schopný	k2eAgFnSc4d1
65	#num#	k4
<g/>
.	.	kIx.
brigádu	brigáda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
určena	určit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
posádka	posádka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letecká	letecký	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
byla	být	k5eAaImAgFnS
stažena	stažen	k2eAgFnSc1d1
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
jako	jako	k8xC,k8xS
příprava	příprava	k1gFnSc1
na	na	k7c4
pohyb	pohyb	k1gInSc4
s	s	k7c7
48	#num#	k4
<g/>
.	.	kIx.
divizí	divize	k1gFnPc2
do	do	k7c2
Nizozemské	nizozemský	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgInPc1d1
a	a	k8xC
filipínské	filipínský	k2eAgInPc1d1
síly	síl	k1gInPc1
odrazily	odrazit	k5eAaPmAgInP
noční	noční	k2eAgInPc1d1
útoky	útok	k1gInPc1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Abucay	Abucaa	k1gFnSc2
a	a	k8xC
část	část	k1gFnSc4
divize	divize	k1gFnSc2
americko-filipínské	americko-filipínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
uskutečnili	uskutečnit	k5eAaPmAgMnP
protiútok	protiútok	k1gInSc4
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
divize	divize	k1gFnSc1
se	se	k3xPyFc4
stáhla	stáhnout	k5eAaPmAgFnS
na	na	k7c4
rezervní	rezervní	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
u	u	k7c2
Casa	Casum	k1gNnSc2
Pilar	Pilara	k1gFnPc2
do	do	k7c2
Bagac	Bagac	k1gInSc4
ve	v	k7c6
středu	střed	k1gInSc6
poloostrova	poloostrov	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
obnovila	obnovit	k5eAaPmAgFnS
útoky	útok	k1gInPc4
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
s	s	k7c7
obojživelným	obojživelný	k2eAgInSc7d1
výsadkem	výsadek	k1gInSc7
za	za	k7c7
nepřátelskou	přátelský	k2eNgFnSc7d1
linií	linie	k1gFnSc7
praporem	prapor	k1gInSc7
16	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
nasledovány	nasledován	k2eAgInPc4d1
obecnými	obecný	k2eAgInPc7d1
útoky	útok	k1gInPc7
začínající	začínající	k2eAgFnSc2d1
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
bitevní	bitevní	k2eAgFnSc3d1
linii	linie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obojživelné	obojživelný	k2eAgFnPc4d1
přistání	přistání	k1gNnSc2
byla	být	k5eAaImAgFnS
narušena	narušit	k5eAaPmNgFnS
a	a	k8xC
zadržena	zadržet	k5eAaPmNgFnS
v	v	k7c6
husté	hustý	k2eAgFnSc6d1
džungli	džungle	k1gFnSc6
jednotkami	jednotka	k1gFnPc7
ad-hoc	ad-hoc	k6eAd1
skládající	skládající	k2eAgFnSc1d1
se	se	k3xPyFc4
z	z	k7c2
amerických	americký	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Air	Air	k1gFnSc1
Corps	corps	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
filipínské	filipínský	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsadek	výsadka	k1gFnPc2
byl	být	k5eAaImAgMnS
poté	poté	k6eAd1
nucen	nutit	k5eAaImNgMnS
pomalu	pomalu	k6eAd1
ustupovat	ustupovat	k5eAaImF
zpět	zpět	k6eAd1
k	k	k7c3
útesům	útes	k1gInPc3
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
vysoké	vysoký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistání	přistání	k1gNnSc1
za	za	k7c7
účelem	účel	k1gInSc7
posílení	posílení	k1gNnSc2
přežívajícího	přežívající	k2eAgInSc2d1
výsadku	výsadek	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
byla	být	k5eAaImAgFnS
narušena	narušit	k5eAaPmNgFnS
leteckými	letecký	k2eAgInPc7d1
útoky	útok	k1gInPc7
z	z	k7c2
mála	málo	k1gNnSc2
zbývajících	zbývající	k2eAgInPc2d1
FEAF	FEAF	kA
P-	P-	k1gFnSc7
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsadek	výsadek	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
uvězněn	uvězněn	k2eAgInSc1d1
a	a	k8xC
nakonec	nakonec	k6eAd1
zničen	zničit	k5eAaPmNgInS
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Generál	generál	k1gMnSc1
Homma	Hommum	k1gNnSc2
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
nařídil	nařídit	k5eAaPmAgMnS
pozastavení	pozastavení	k1gNnSc4
ofenzivních	ofenzivní	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
reorganizoval	reorganizovat	k5eAaBmAgMnS
své	svůj	k3xOyFgFnPc4
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nemohlo	moct	k5eNaImAgNnS
být	být	k5eAaImF
provedeno	provést	k5eAaPmNgNnS
okamžitě	okamžitě	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
16	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
zapojena	zapojit	k5eAaPmNgFnS
snažíc	snažit	k5eAaImSgFnS
se	se	k3xPyFc4
vymanit	vymanit	k5eAaPmF
z	z	k7c2
svůj	svůj	k3xOyFgInSc4
prapor	prapor	k1gInSc4
20	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
z	z	k7c2
obležení	obležení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
dalším	další	k2eAgNnSc7d1
ztrátami	ztráta	k1gFnPc7
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
zbytky	zbytek	k1gInPc1
praporu	prapor	k1gInSc2
<g/>
,	,	kIx,
378	#num#	k4
důstojníků	důstojník	k1gMnPc2
a	a	k8xC
mužů	muž	k1gMnPc2
a	a	k8xC
vyproštěno	vyproštěn	k2eAgNnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
14	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
stáhla	stáhnout	k5eAaPmAgFnS
pár	pár	k4xCyI
kilometrů	kilometr	k1gInPc2
na	na	k7c4
sever	sever	k1gInSc4
a	a	k8xC
jednotky	jednotka	k1gFnPc4
USAFFE	USAFFE	kA
znovu	znovu	k6eAd1
obsadily	obsadit	k5eAaPmAgFnP
opuštěné	opuštěný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výsledku	výsledek	k1gInSc6
byly	být	k5eAaImAgInP
úplně	úplně	k6eAd1
zničeny	zničit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
tři	tři	k4xCgInPc1
prapory	prapor	k1gInPc1
japonské	japonský	k2eAgInPc1d1
20	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgMnSc1d1
a	a	k8xC
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
jasné	jasný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
obránců	obránce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
Japonci	Japonec	k1gMnPc1
<g/>
,	,	kIx,
odrazeni	odrazen	k2eAgMnPc1d1
těžkými	těžký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
a	a	k8xC
s	s	k7c7
pouhou	pouhý	k2eAgFnSc7d1
jednou	jeden	k4xCgFnSc7
brigádou	brigáda	k1gFnSc7
<g/>
,	,	kIx,
prováděli	provádět	k5eAaImAgMnP
operace	operace	k1gFnPc4
obležení	obležení	k1gNnSc2
při	při	k7c6
čekání	čekání	k1gNnSc6
na	na	k7c4
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
armády	armáda	k1gFnPc1
se	se	k3xPyFc4
setkávaly	setkávat	k5eAaImAgFnP
hlídkami	hlídka	k1gFnPc7
v	v	k7c6
omezených	omezený	k2eAgInPc6d1
lokálních	lokální	k2eAgInPc6d1
střetech	střet	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
zhoršující	zhoršující	k2eAgFnSc7d1
se	se	k3xPyFc4
spojenecké	spojenecký	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
v	v	k7c6
asijsko-tichomořském	asijsko-tichomořský	k2eAgInSc6d1
regionu	region	k1gInSc6
<g/>
,	,	kIx,
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Franklin	Franklina	k1gFnPc2
D.	D.	kA
Roosevelt	Roosevelt	k1gMnSc1
nařídil	nařídit	k5eAaPmAgInS
MacArthurovi	MacArthur	k1gMnSc3
přesunout	přesunout	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
jihu	jih	k1gInSc2
západní	západní	k2eAgFnSc2d1
Pacifické	pacifický	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wainwright	Wainwright	k1gInSc1
oficiálně	oficiálně	k6eAd1
převzal	převzít	k5eAaPmAgInS
kontrolu	kontrola	k1gFnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
(	(	kIx(
<g/>
USFIP	USFIP	kA
<g/>
)	)	kIx)
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
byly	být	k5eAaImAgFnP
části	část	k1gFnPc1
divize	divize	k1gFnSc1
americké	americký	k2eAgFnPc4d1
filipínské	filipínský	k2eAgFnPc4d1
divize	divize	k1gFnPc4
přesunuty	přesunut	k2eAgFnPc4d1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
při	při	k7c6
obraně	obrana	k1gFnSc6
jiných	jiný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Počínaje	počínaje	k7c7
28	#num#	k4
<g/>
.	.	kIx.
březnem	březen	k1gInSc7
<g/>
,	,	kIx,
nová	nový	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
japonských	japonský	k2eAgInPc2d1
leteckých	letecký	k2eAgInPc2d1
a	a	k8xC
dělostřeleckých	dělostřelecký	k2eAgInPc2d1
útoků	útok	k1gInPc2
zasáhla	zasáhnout	k5eAaPmAgFnS
spojenecké	spojenecký	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
těžce	těžce	k6eAd1
oslabené	oslabený	k2eAgNnSc1d1
podvýživou	podvýživa	k1gFnSc7
<g/>
,	,	kIx,
nemocemi	nemoc	k1gFnPc7
a	a	k8xC
dlouhotrvajícím	dlouhotrvající	k2eAgInSc7d1
bojem	boj	k1gInSc7
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc7
začali	začít	k5eAaPmAgMnP
prodírat	prodírat	k5eAaImF
podél	podél	k7c2
hory	hora	k1gFnSc2
Samat	Samat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
Filipínská	filipínský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
již	již	k6eAd1
nefungující	fungující	k2eNgInSc1d1
jako	jako	k8xS,k8xC
koordinovaný	koordinovaný	k2eAgInSc1d1
celek	celek	k1gInSc1
a	a	k8xC
vyčerpaný	vyčerpaný	k2eAgInSc1d1
pěti	pět	k4xCc7
dny	den	k1gInPc7
téměř	téměř	k6eAd1
nepřetržitého	přetržitý	k2eNgInSc2d1
boje	boj	k1gInSc2
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
protiútoku	protiútok	k1gInSc2
proti	proti	k7c3
japonským	japonský	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
byly	být	k5eAaImAgFnP
Americký	americký	k2eAgInSc4d1
57	#num#	k4
<g/>
.	.	kIx.
<g/>
Pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
a	a	k8xC
31	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
PA	Pa	kA
převálcovány	převálcován	k2eAgFnPc4d1
u	u	k7c2
řeky	řeka	k1gFnSc2
Alangan	Alangan	k1gInSc1
<g/>
.	.	kIx.
45	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
rozkazů	rozkaz	k1gInPc2
dosáhnout	dosáhnout	k5eAaPmF
Mariveles	Mariveles	k1gInSc4
a	a	k8xC
evakuovat	evakuovat	k5eAaBmF
se	se	k3xPyFc4
Corregidor	Corregidora	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
vzdal	vzdát	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
300	#num#	k4
mužů	muž	k1gMnPc2
americké	americký	k2eAgFnSc2d1
31	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgFnSc2d1
divize	divize	k1gFnSc2
úspěšně	úspěšně	k6eAd1
dosáhlo	dosáhnout	k5eAaPmAgNnS
Corregidoru	Corregidor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Corregidor	Corregidor	k1gInSc4
</s>
<s>
Corregidor	Corregidor	k1gInSc4
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
pobřeží	pobřeží	k1gNnSc6
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
základna	základna	k1gFnSc1
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
bránila	bránit	k5eAaImAgFnS
vstup	vstup	k1gInSc4
do	do	k7c2
Manilské	manilský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
dvěma	dva	k4xCgFnPc7
staršími	starý	k2eAgFnPc7d2
pobřežními	pobřežní	k2eAgFnPc7d1
dělostřeleckými	dělostřelecký	k2eAgFnPc7d1
bateriemi	baterie	k1gFnPc7
59	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
91	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
pobřeží	pobřeží	k1gNnSc2
dělostřelecké	dělostřelecký	k2eAgFnSc2d1
a	a	k8xC
protiletadlové	protiletadlový	k2eAgFnPc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CA	ca	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
umístěnaa	umístěnaa	k6eAd1
na	na	k7c6
vyšších	vysoký	k2eAgFnPc6d2
pozicích	pozice	k1gFnPc6
v	v	k7c4
Corregidor	Corregidor	k1gInSc4
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
úspěšně	úspěšně	k6eAd1
čelit	čelit	k5eAaImF
japonským	japonský	k2eAgMnPc3d1
leteckým	letecký	k2eAgMnPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgFnSc1d2
staniční	staniční	k2eAgFnSc1d1
baterie	baterie	k1gFnSc1
s	s	k7c7
nepřenosnými	přenosný	k2eNgInPc7d1
mortary	mortar	k1gInPc7
a	a	k8xC
obrovskými	obrovský	k2eAgInPc7d1
kanóny	kanón	k1gInPc7
<g/>
,	,	kIx,
pro	pro	k7c4
obranu	obrana	k1gFnSc4
před	před	k7c7
útokem	útok	k1gInSc7
z	z	k7c2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
snadno	snadno	k6eAd1
uvedeny	uvést	k5eAaPmNgInP
mimo	mimo	k7c4
provoz	provoz	k1gInSc4
japonskými	japonský	k2eAgInPc7d1
bombardéry	bombardér	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američtí	americký	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
a	a	k8xC
filipínští	filipínský	k2eAgMnPc1d1
Skauti	skaut	k1gMnPc1
bránili	bránit	k5eAaImAgMnP
malou	malý	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
japonské	japonský	k2eAgNnSc4d1
velení	velení	k1gNnSc4
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
nechalo	nechat	k5eAaPmAgNnS
instalovat	instalovat	k5eAaBmF
kyslík	kyslík	k1gInSc4
do	do	k7c2
svých	svůj	k3xOyFgInPc2
bombardérů	bombardér	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
letět	letět	k5eAaImF
výš	vysoce	k6eAd2
než	než	k8xS
je	být	k5eAaImIp3nS
dosah	dosah	k1gInSc4
protiletadlových	protiletadlový	k2eAgFnPc2d1
baterií	baterie	k1gFnPc2
v	v	k7c4
Corregidor	Corregidor	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
poté	poté	k6eAd1
začalo	začít	k5eAaPmAgNnS
masivní	masivní	k2eAgNnSc1d1
bombardování	bombardování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Corregidor	Corregidor	k1gInSc1
byl	být	k5eAaImAgInS
bráněn	bránit	k5eAaImNgInS
11,000	11,000	k4
vojáky	voják	k1gMnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
umístěné	umístěný	k2eAgInPc1d1
na	na	k7c4
Corregidor	Corregidor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
do	do	k7c2
Corregidoru	Corregidor	k1gInSc2
z	z	k7c2
poloostrova	poloostrov	k1gInSc2
Bataan	Bataana	k1gFnPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
Japonci	Japonec	k1gMnPc1
přemohli	přemoct	k5eAaPmAgMnP
tamní	tamní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
finální	finální	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Corregidor	Corregidor	k1gInSc4
dělostřeleckou	dělostřelecký	k2eAgFnSc7d1
palbou	palba	k1gFnSc7
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
dva	dva	k4xCgInPc4
prapory	prapor	k1gInPc4
61	#num#	k4
<g/>
.	.	kIx.
japonského	japonský	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
na	na	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
konci	konec	k1gInSc6
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
silný	silný	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
si	se	k3xPyFc3
japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
vytvořily	vytvořit	k5eAaPmAgFnP
předmostí	předmostí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
brzy	brzy	k6eAd1
posíleno	posílit	k5eAaPmNgNnS
tanky	tank	k1gInPc7
a	a	k8xC
dělostřelectvem	dělostřelectvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obránci	obránce	k1gMnPc1
byli	být	k5eAaImAgMnP
rychle	rychle	k6eAd1
tlačeni	tlačen	k2eAgMnPc1d1
zpět	zpět	k6eAd1
k	k	k7c3
pevnosti	pevnost	k1gFnSc3
Malinta	Malinta	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
Wainwright	Wainwright	k1gMnSc1
požádal	požádat	k5eAaPmAgMnS
Homma	Homm	k1gMnSc4
o	o	k7c6
podmínkách	podmínka	k1gFnPc6
kapitulace	kapitulace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homma	Homma	k1gFnSc1
trval	trvat	k5eAaImAgInS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
předání	předání	k1gNnSc1
bude	být	k5eAaImBp3nS
obsahovat	obsahovat	k5eAaImF
všechny	všechen	k3xTgFnPc4
spojenecké	spojenecký	k2eAgFnPc4d1
síly	síla	k1gFnPc4
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
domnění	domnění	k1gNnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
životy	život	k1gInPc1
všech	všecek	k3xTgFnPc2
těch	ten	k3xDgInPc6
<g/>
,	,	kIx,
na	na	k7c4
Corregidor	Corregidor	k1gInSc4
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
ohroženy	ohrozit	k5eAaPmNgFnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Wainwright	Wainwright	k1gMnSc1
přijat	přijmout	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
poslal	poslat	k5eAaPmAgMnS
zprávu	zpráva	k1gFnSc4
Sharpovi	Sharp	k1gMnSc6
s	s	k7c7
rozkazem	rozkaz	k1gInSc7
se	se	k3xPyFc4
vzdát	vzdát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sharp	sharp	k1gInSc1
vyhověl	vyhovět	k5eAaPmAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
mnoho	mnoho	k4c1
jedinců	jedinec	k1gMnPc2
pokračovalo	pokračovat	k5eAaImAgNnS
v	v	k7c6
boji	boj	k1gInSc6
jako	jako	k9
partyzáni	partyzán	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1
a	a	k8xC
centrální	centrální	k2eAgFnSc4d1
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pondělí	pondělí	k1gNnSc6
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
provedly	provést	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
H6K	H6K	k1gFnPc1
z	z	k7c2
Palau	Palaus	k1gInSc2
první	první	k4xOgFnSc6
nálet	nálet	k1gInSc1
na	na	k7c4
Cebu	Ceba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zaútočilo	zaútočit	k5eAaPmAgNnS
pět	pět	k4xCc1
A6M2	A6M2	k1gFnPc2
Tainan	Tainany	k1gInPc2
kókútai	kókútai	k6eAd1
z	z	k7c2
Legazpi	Legazp	k1gFnSc2
na	na	k7c4
Iloilo	Iloila	k1gFnSc5
na	na	k7c6
Panay	Panaa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zničily	zničit	k5eAaPmAgInP
tři	tři	k4xCgInPc1
letouny	letoun	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
na	na	k7c4
Iloilo	Iloila	k1gFnSc5
zaútočilo	zaútočit	k5eAaPmAgNnS
18	#num#	k4
G3M	G3M	k1gMnPc2
a	a	k8xC
39	#num#	k4
G4M1	G4M1	k1gFnPc2
Takao	Takao	k6eAd1
kókútai	kókútai	k6eAd1
z	z	k7c2
Formosy	Formosa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c4
pátek	pátek	k1gInSc4
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
Japonci	Japonec	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
letiště	letiště	k1gNnSc4
Del	Del	k1gFnSc2
Monte	Mont	k1gInSc5
a	a	k8xC
při	při	k7c6
prvním	první	k4xOgInSc6
útoku	útok	k1gInSc6
čtyř	čtyři	k4xCgFnPc2
A6M2	A6M2	k1gFnPc2
byly	být	k5eAaImAgInP
na	na	k7c6
zemi	zem	k1gFnSc6
zničeny	zničit	k5eAaPmNgFnP
tři	tři	k4xCgFnPc1
B-	B-	k1gFnPc1
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
si	se	k3xPyFc3
však	však	k9
nárokovali	nárokovat	k5eAaImAgMnP
podstatně	podstatně	k6eAd1
tučnější	tučný	k2eAgFnSc4d2
kořist	kořist	k1gFnSc4
<g/>
:	:	kIx,
dvě	dva	k4xCgFnPc4
B-	B-	k1gFnPc4
<g/>
17	#num#	k4
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
zničené	zničený	k2eAgInPc4d1
dvoumotorové	dvoumotorový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
čtyři	čtyři	k4xCgInPc4
poškozené	poškozený	k2eAgInPc4d1
dvoumotorové	dvoumotorový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
časných	časný	k2eAgFnPc6d1
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
soboty	sobota	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
v	v	k7c4
Davao	Davao	k1gNnSc4
vylodily	vylodit	k5eAaPmAgFnP
japonské	japonský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Del	Del	k1gFnSc1
Monte	Mont	k1gInSc5
bylo	být	k5eAaImAgNnS
bombardováno	bombardován	k2eAgNnSc4d1
54	#num#	k4
bombardéry	bombardér	k1gInPc4
a	a	k8xC
osmi	osm	k4xCc7
létajícími	létající	k2eAgInPc7d1
čluny	člun	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dansalang	Dansalanga	k1gFnPc2
<g/>
,	,	kIx,
Malabang	Malabanga	k1gFnPc2
a	a	k8xC
Cebu	Cebus	k1gInSc2
byly	být	k5eAaImAgInP
napadeny	napaden	k2eAgMnPc4d1
stíhači	stíhač	k1gMnPc7
z	z	k7c2
Tainan	Tainana	k1gFnPc2
kókútai	kókúta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
</s>
<s>
Filipíny	Filipíny	k1gFnPc1
byly	být	k5eAaImAgFnP
od	od	k7c2
Japonců	Japonec	k1gMnPc2
dobyty	dobýt	k5eAaPmNgFnP
zpět	zpět	k6eAd1
během	během	k7c2
let	léto	k1gNnPc2
1944	#num#	k4
a	a	k8xC
1945	#num#	k4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
))	))	k?
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Obraz	obraz	k1gInSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Battle	Battle	k1gFnSc2
of	of	k?
the	the	k?
Philippines	Philippines	k1gInSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Bataan	Bataana	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Battle	Battle	k1gFnSc2
of	of	k?
Corregidor	Corregidor	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
ZALOGA	ZALOGA	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
J.	J.	kA
Tank	tank	k1gInSc1
Battles	Battles	k1gInSc1
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
War	War	k1gMnSc1
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hong	Hong	k1gInSc1
Kong	Kongo	k1gNnPc2
<g/>
:	:	kIx,
Concord	Concord	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Armor	Armor	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
Series	Series	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
7004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
962	#num#	k4
<g/>
-	-	kIx~
<g/>
361	#num#	k4
<g/>
-	-	kIx~
<g/>
607	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZALOGA	ZALOGA	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
J.	J.	kA
Tank	tank	k1gInSc1
Battles	Battles	k1gInSc1
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
War	War	k1gMnSc1
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hong	Hong	k1gInSc1
Kong	Kongo	k1gNnPc2
<g/>
:	:	kIx,
Concord	Concord	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Armor	Armor	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
Series	Series	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
7004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
962	#num#	k4
<g/>
-	-	kIx~
<g/>
361	#num#	k4
<g/>
-	-	kIx~
<g/>
607	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
The	The	k1gMnSc2
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Campaigns	Campaignsa	k1gFnPc2
of	of	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
Philipine	Philipin	k1gInSc5
Islands	Islands	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2003	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Reports	Reports	k1gInSc1
of	of	k?
General	General	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
:	:	kIx,
THE	THE	kA
CAMPAIGNS	CAMPAIGNS	kA
OF	OF	kA
MACARTHUR	MACARTHUR	kA
IN	IN	kA
THE	THE	kA
PACIFIC	PACIFIC	kA
<g/>
,	,	kIx,
VOLUME	volum	k1gInSc5
I	i	k9
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
Japanese	Japanese	k1gFnSc1
Invasion	Invasion	k1gInSc4
of	of	k?
the	the	k?
Philippines	Philippines	k1gInSc1
and	and	k?
the	the	k?
Forces	Forces	k1gMnSc1
Employed	Employed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
147	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
↑	↑	k?
SIDES	SIDES	kA
<g/>
,	,	kIx,
Hampton	Hampton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Trial	trial	k1gInSc4
of	of	k?
General	General	k1gMnSc2
Homma	Homm	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rockville	Rockville	k1gNnSc1
<g/>
,	,	kIx,
Maryland	Maryland	k1gInSc1
<g/>
:	:	kIx,
American	American	k1gInSc1
Heritage	Heritage	k1gNnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2007-01	2007-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOYT	HOYT	kA
<g/>
,	,	kIx,
Edwin	Edwin	k1gMnSc1
P.	P.	kA
Japonsko	Japonsko	k1gNnSc1
ve	v	k7c6
válce	válka	k1gFnSc6
(	(	kIx(
<g/>
Velký	velký	k2eAgInSc1d1
pacifický	pacifický	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Oldag	Oldag	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85954	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
-	-	kIx~
<g/>
188	#num#	k4
<g/>
,	,	kIx,
193-195	193-195	k4
a	a	k8xC
197	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Reports	Reportsa	k1gFnPc2
of	of	k?
General	General	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
operations	operations	k1gInSc1
in	in	k?
the	the	k?
southwest	southwest	k1gInSc1
Pacific	Pacific	k1gMnSc1
area	area	k1gFnSc1
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
-	-	kIx~
Part	part	k1gInSc1
I	I	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHAPTER	CHAPTER	kA
VI	VI	kA
<g/>
:	:	kIx,
CONQUEST	CONQUEST	kA
OF	OF	kA
THE	THE	kA
PHILIPPINES	PHILIPPINES	kA
<g/>
:	:	kIx,
Preliminary	Preliminara	k1gFnSc2
Planning	Planning	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
431	#num#	k4
2	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Prewar	Prewara	k1gFnPc2
Plans	Plansa	k1gFnPc2
<g/>
,	,	kIx,
Japanese	Japanese	k1gFnSc2
and	and	k?
American	American	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Plan	plan	k1gInSc1
-	-	kIx~
The	The	k1gFnSc1
Plan	plan	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Philippines	Philippines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EVANS	EVANS	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
C.	C.	kA
<g/>
;	;	kIx,
PEATTIE	PEATTIE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
R.	R.	kA
Kaigun	Kaigun	k1gMnSc1
<g/>
:	:	kIx,
strategy	stratega	k1gFnPc1
<g/>
,	,	kIx,
tactics	tactics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
technology	technolog	k1gMnPc4
in	in	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
<g/>
,	,	kIx,
1887	#num#	k4
<g/>
-	-	kIx~
<g/>
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
192	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
188	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
53	#num#	k4
<g/>
↑	↑	k?
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
Japanese	Japanese	k1gFnSc1
Plan	plan	k1gInSc4
–	–	k?
Concentration	Concentration	k1gInSc1
of	of	k?
Forces	Forces	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FERKL	FERKL	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mitsubishi	mitsubishi	k1gNnSc7
G4M	G4M	k1gFnSc2
Betty	Betty	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Revi	Revi	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85957	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Bojové	bojový	k2eAgNnSc1d1
nasazení	nasazení	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	i	k9
<g/>
,	,	kIx,
tabulka	tabulka	k1gFnSc1
1	#num#	k4
a	a	k8xC
2	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
55	#num#	k4
<g/>
↑	↑	k?
NOWAK	NOWAK	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mały	Mała	k1gMnSc2
okręt	okręt	k1gInSc4
wielki	wielki	k1gNnSc2
duchem	duch	k1gMnSc7
(	(	kIx(
<g/>
Japoński	Japoński	k1gNnSc7
lotniskowiec	lotniskowiec	k1gMnSc1
Ryujo	Ryujo	k1gMnSc1
–	–	k?
geneza	geneza	k1gFnSc1
i	i	k8xC
opis	opis	k1gInSc1
konstrukcji	konstrukct	k5eAaPmIp1nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morze	Morze	k1gFnSc1
statki	statk	k1gFnSc2
i	i	k8xC
okręty	okręta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen-listopad	Říjen-listopad	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1426	#num#	k4
<g/>
-	-	kIx~
<g/>
529	#num#	k4
<g/>
X.	X.	kA
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	i	k9
<g/>
,	,	kIx,
tabulka	tabulka	k1gFnSc1
2	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
561	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Forces	Forcesa	k1gFnPc2
<g/>
,	,	kIx,
Far	fara	k1gFnPc2
East	Easta	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Plan	plan	k1gInSc1
of	of	k?
Defense	defense	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HARA	HARA	kA
<g/>
,	,	kIx,
Tameichi	Tameichi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japanese	Japanese	k1gFnSc1
Destroyer	Destroyra	k1gFnPc2
Captain	Captain	k1gMnSc1
(	(	kIx(
<g/>
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
,	,	kIx,
Guadalcanal	Guadalcanal	k1gMnSc1
<g/>
,	,	kIx,
Midway	Midwaa	k1gFnPc1
–	–	k?
The	The	k1gMnSc1
Great	Great	k2eAgMnSc1d1
Naval	navalit	k5eAaPmRp2nS
battles	battles	k1gMnSc1
as	as	k1gNnSc2
Seen	Seen	k1gMnSc1
Through	Through	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Eyes	Eyes	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1591143543	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
30	#num#	k4
a	a	k8xC
31	#num#	k4
<g/>
↑	↑	k?
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Forces	Forcesa	k1gFnPc2
<g/>
,	,	kIx,
Far	fara	k1gFnPc2
East	Easta	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Recall	Recall	k1gMnSc1
of	of	k?
General	General	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Forces	Forcesa	k1gFnPc2
<g/>
,	,	kIx,
Far	fara	k1gFnPc2
East	Easta	k1gFnPc2
<g/>
:	:	kIx,
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
the	the	k?
Philippines	Philippines	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Včetně	včetně	k7c2
poznámky	poznámka	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Reinforcement	Reinforcement	k1gInSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
<g/>
:	:	kIx,
Ground	Ground	k1gMnSc1
Forces	Forces	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
561	#num#	k4
2	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Reinforcement	Reinforcement	k1gInSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gInSc1
<g/>
:	:	kIx,
Sumary	Sumara	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Prewar	Prewara	k1gFnPc2
Plans	Plansa	k1gFnPc2
<g/>
,	,	kIx,
Japanese	Japanese	k1gFnSc1
and	and	k?
American	American	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
MacArthur	MacArthur	k1gMnSc1
Plan	plan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
401	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Reinforcement	Reinforcement	k1gInSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
<g/>
:	:	kIx,
Air	Air	k1gMnSc1
Forces	Forces	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
41	#num#	k4
a	a	k8xC
42	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
153	#num#	k4
a	a	k8xC
poznámka	poznámka	k1gFnSc1
19	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
452	#num#	k4
<g/>
↑	↑	k?
BERGERUD	BERGERUD	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
M.	M.	kA
Fire	Fir	k1gFnPc1
in	in	k?
the	the	k?
Sky	Sky	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Air	Air	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
South	South	k1gMnSc1
Pacific	Pacific	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Westview	Westview	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780813338699	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
a	a	k8xC
53	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
57	#num#	k4
a	a	k8xC
58	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
49	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
25	#num#	k4
<g/>
,	,	kIx,
28	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
42	#num#	k4
a	a	k8xC
poznámka	poznámka	k1gFnSc1
3	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
442	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
například	například	k6eAd1
str	str	kA
<g/>
.	.	kIx.
137	#num#	k4
<g/>
,	,	kIx,
144	#num#	k4
<g/>
,	,	kIx,
145	#num#	k4
a	a	k8xC
mnoho	mnoho	k6eAd1
dalších	další	k2eAgFnPc2d1
<g/>
↑	↑	k?
Del	Del	k1gFnPc2
Carmen	Carmen	k2eAgInSc4d1
Airfield	Airfield	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
pacificwrecks	pacificwrecks	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-05-09	2009-05-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nichols	Nichols	k1gInSc1
Field	Field	k1gInSc1
(	(	kIx(
<g/>
Manila	Manila	k1gFnSc1
Airport	Airport	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
pacificwrecks	pacificwrecks	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-05-11	2009-05-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
47	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
47	#num#	k4
a	a	k8xC
481	#num#	k4
2	#num#	k4
3	#num#	k4
YARNALL	YARNALL	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
R.	R.	kA
United	United	k1gMnSc1
States	States	k1gMnSc1
Asiatic	Asiatice	k1gFnPc2
Fleet	Fleet	k1gInSc4
Locations	Locations	k1gInSc1
December	December	k1gInSc1
7	#num#	k4
1941	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
navsource	navsourka	k1gFnSc3
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2005-01-20	2005-01-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Reinforcement	Reinforcement	k1gInSc1
of	of	k?
the	the	k?
Philippines	Philippinesa	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Forces	Forces	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
First	First	k1gFnSc1
Days	Days	k1gInSc4
of	of	k?
War	War	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Fleet	Fleet	k1gMnSc1
Moves	Moves	k1gMnSc1
South	South	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
41	#num#	k4
<g/>
↑	↑	k?
KNOTT	KNOTT	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
C.	C.	kA
Black	Black	k1gMnSc1
Cat	Cat	k1gMnSc1
Raiders	Raidersa	k1gFnPc2
of	of	k?
World	World	k1gMnSc1
War	War	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781557504715	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ALSLEBEN	ALSLEBEN	kA
<g/>
,	,	kIx,
Allan	Allan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
Patrol	Patrol	k?
Wing	Wing	k1gInSc1
10	#num#	k4
in	in	k?
the	the	k?
Dutch	Dutch	k1gMnSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
<g/>
,	,	kIx,
1942	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2001-08-04	2001-08-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BURTON	BURTON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortnight	Fortnight	k1gInSc4
of	of	k?
Infamy	Infam	k1gInPc4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Collapse	Collapse	k1gFnSc2
of	of	k?
Allied	Allied	k1gMnSc1
Airpower	Airpower	k1gMnSc1
West	West	k1gMnSc1
of	of	k?
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781591140962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
29	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Forces	Forcesa	k1gFnPc2
<g/>
,	,	kIx,
Far	fara	k1gFnPc2
East	Easta	k1gFnPc2
<g/>
:	:	kIx,
Philippine	Philippin	k1gInSc5
Army	Arm	k2eAgInPc1d1
<g/>
:	:	kIx,
Mobilization	Mobilization	k1gInSc1
and	and	k?
Training	Training	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
13	#num#	k4
a	a	k8xC
14	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
They	Thea	k1gFnSc2
say	say	k?
that	that	k1gMnSc1
we	we	k?
are	ar	k1gInSc5
getting	getting	k1gInSc1
the	the	k?
latest	latest	k1gMnSc1
equipment	equipment	k1gMnSc1
<g/>
,	,	kIx,
but	but	k?
we	we	k?
who	who	k?
are	ar	k1gInSc5
here	here	k1gFnPc3
flying	flying	k1gInSc1
it	it	k?
know	know	k?
that	that	k1gInSc1
it	it	k?
is	is	k?
no	no	k9
good	good	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Our	Our	k1gMnSc1
planes	planes	k1gMnSc1
–	–	k?
the	the	k?
latest	latest	k1gInSc1
P-	P-	k1gFnSc1
<g/>
40	#num#	k4
<g/>
s	s	k7c7
–	–	k?
are	ar	k1gInSc5
not	nota	k1gFnPc2
good	good	k1gMnSc1
enough	enough	k1gMnSc1
to	ten	k3xDgNnSc4
fight	fight	k2eAgInSc4d1
with	with	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Someone	Someon	k1gInSc5
in	in	k?
Washington	Washington	k1gInSc1
said	said	k1gInSc1
<g/>
,	,	kIx,
’	’	k?
<g/>
They	Thea	k1gFnSc2
are	ar	k1gInSc5
the	the	k?
best	best	k1gInSc1
<g/>
’	’	k?
and	and	k?
so	so	k?
we	we	k?
have	have	k6eAd1
them	them	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
English	English	k1gMnSc1
don	don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
want	want	k1gInSc1
our	our	k?
pursuit	pursuit	k1gInSc1
<g/>
,	,	kIx,
they	they	k1gInPc1
want	wanta	k1gFnPc2
bombers	bombersa	k1gFnPc2
<g/>
…	…	k?
You	You	k1gMnSc1
know	know	k?
<g/>
,	,	kIx,
Dee	Dee	k1gMnSc1
<g/>
,	,	kIx,
one	one	k?
gets	gets	k1gInSc1
a	a	k8xC
bit	bit	k1gInSc1
disgusted	disgusted	k1gMnSc1
when	when	k1gMnSc1
he	he	k0
lays	lays	k1gInSc1
his	his	k1gNnPc2
life	lifat	k5eAaPmIp3nS
on	on	k3xPp3gMnSc1
the	the	k?
line	linout	k5eAaImIp3nS
for	forum	k1gNnPc2
our	our	k?
lousy	lousa	k1gFnSc2
<g/>
,	,	kIx,
out-of-date	out-of-dat	k1gMnSc5
Air	Air	k1gMnSc5
Corps	corps	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
74	#num#	k4
a	a	k8xC
4381	#num#	k4
2	#num#	k4
3	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnPc2
Last	Last	k2eAgInSc1d1
Days	Days	k1gInSc1
of	of	k?
Peace	Peace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnPc2
Last	Last	k2eAgInSc1d1
Days	Days	k1gInSc1
of	of	k?
Peace	Peace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negotiations	Negotiationsa	k1gFnPc2
with	witha	k1gFnPc2
Japan	japan	k1gInSc1
appear	appear	k1gMnSc1
to	ten	k3xDgNnSc1
be	be	k?
terminated	terminated	k1gInSc1
to	ten	k3xDgNnSc1
all	all	k?
practical	practicat	k5eAaPmAgMnS
purposes	purposes	k1gInSc4
with	with	k1gMnSc1
only	onla	k1gFnSc2
the	the	k?
barest	barest	k1gMnSc1
possibility	possibilita	k1gFnSc2
that	that	k1gMnSc1
the	the	k?
Japanese	Japanese	k1gFnSc2
Government	Government	k1gMnSc1
might	might	k1gMnSc1
come	comat	k5eAaPmIp3nS
back	back	k1gMnSc1
and	and	k?
offer	offer	k1gMnSc1
to	ten	k3xDgNnSc4
continue	continue	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japanese	Japanese	k1gFnSc1
future	futur	k1gMnSc5
action	action	k1gInSc1
unpredictable	unpredictable	k6eAd1
but	but	k?
hostile	hostile	k6eAd1
action	action	k1gInSc1
possible	possible	k6eAd1
at	at	k?
any	any	k?
moment	moment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
If	If	k1gFnSc1
hostilities	hostilitiesa	k1gFnPc2
cannot	cannota	k1gFnPc2
<g/>
,	,	kIx,
repeat	repeat	k1gInSc4
cannot	cannota	k1gFnPc2
<g/>
,	,	kIx,
be	be	k?
avoided	avoided	k1gMnSc1
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
desires	desires	k1gMnSc1
that	that	k2eAgInSc4d1
Japan	japan	k1gInSc4
commit	commit	k1gInSc1
the	the	k?
first	first	k1gInSc1
overt	overt	k1gInSc1
act	act	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
This	This	k1gInSc1
policy	polica	k1gFnSc2
should	shoulda	k1gFnPc2
not	nota	k1gFnPc2
<g/>
,	,	kIx,
repeat	repeat	k1gInSc4
not	nota	k1gFnPc2
<g/>
,	,	kIx,
be	be	k?
construed	construed	k1gInSc1
as	as	k1gInSc1
restricting	restricting	k1gInSc1
you	you	k?
to	ten	k3xDgNnSc1
a	a	k8xC
course	course	k1gFnSc1
of	of	k?
action	action	k1gInSc1
that	that	k2eAgMnSc1d1
might	might	k1gMnSc1
jeopardize	jeopardize	k1gFnSc2
your	your	k1gMnSc1
defense	defense	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prior	prior	k1gMnSc1
to	ten	k3xDgNnSc4
hostile	hostile	k6eAd1
Japanese	Japanese	k1gFnSc1
action	action	k1gInSc1
you	you	k?
are	ar	k1gInSc5
directed	directed	k1gMnSc1
to	ten	k3xDgNnSc4
undertake	undertake	k1gNnSc4
such	sucho	k1gNnPc2
reconnaissance	reconnaissance	k1gFnSc2
and	and	k?
other	other	k1gMnSc1
measures	measures	k1gMnSc1
as	as	k9
you	you	k?
deem	deem	k1gInSc1
necessary	necessara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Report	report	k1gInSc1
measures	measures	k1gMnSc1
taken	taken	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Should	Should	k1gMnSc1
hostilities	hostilities	k1gMnSc1
occur	occur	k1gMnSc1
you	you	k?
will	will	k1gMnSc1
carry	carra	k1gFnSc2
out	out	k?
the	the	k?
tasks	tasks	k1gInSc1
assigned	assigned	k1gInSc1
in	in	k?
RAINBOW	RAINBOW	kA
5	#num#	k4
<g/>
…	…	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
57	#num#	k4
<g/>
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Patrol	Patrol	k?
Boat	Boat	k1gInSc1
No	no	k9
<g/>
.	.	kIx.
103	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
37	#num#	k4
až	až	k9
39	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
52	#num#	k4
<g/>
,	,	kIx,
53	#num#	k4
a	a	k8xC
poznámka	poznámka	k1gFnSc1
10	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
443	#num#	k4
s	s	k7c7
odkazem	odkaz	k1gInSc7
na	na	k7c4
Senši	Senše	k1gFnSc4
Sóšó	Sóšó	k1gFnSc2
24	#num#	k4
<g/>
:	:	kIx,
<g/>
1751	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
First	First	k1gFnSc1
Days	Days	k1gInSc4
of	of	k?
War	War	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Attack	Attack	k1gMnSc1
on	on	k3xPp3gMnSc1
Clark	Clark	k1gInSc4
Field	Fielda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
56	#num#	k4
<g/>
↑	↑	k?
MESSIMER	MESSIMER	kA
<g/>
,	,	kIx,
Dwight	Dwight	k1gMnSc1
R.	R.	kA
In	In	k1gMnSc1
the	the	k?
hands	hands	k1gInSc1
of	of	k?
fate	fate	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
story	story	k1gFnSc2
of	of	k?
Patrol	Patrol	k?
Wing	Wing	k1gInSc1
Ten	ten	k3xDgInSc1
<g/>
,	,	kIx,
8	#num#	k4
December	December	k1gInSc1
1941-11	1941-11	k4
May	May	k1gMnSc1
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781557505477	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
až	až	k9
42	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
166	#num#	k4
–	–	k?
uvádí	uvádět	k5eAaImIp3nS
ale	ale	k9
čas	čas	k1gInSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bude	být	k5eAaImBp3nS
zřejmě	zřejmě	k6eAd1
omyl	omyl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následný	následný	k2eAgInSc1d1
start	start	k1gInSc1
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
startovaly	startovat	k5eAaBmAgFnP
právě	právě	k9
kvůli	kvůli	k7c3
zjištěným	zjištěný	k2eAgInPc3d1
armádním	armádní	k2eAgInPc3d1
bombardérům	bombardér	k1gInPc3
<g/>
,	,	kIx,
totiž	totiž	k9
uvádí	uvádět	k5eAaImIp3nS
až	až	k9
v	v	k7c6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
2721	#num#	k4
2	#num#	k4
Burton	Burton	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
128	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
274	#num#	k4
<g/>
↑	↑	k?
Reports	Reportsa	k1gFnPc2
of	of	k?
General	General	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
operations	operations	k1gInSc1
in	in	k?
the	the	k?
southwest	southwest	k1gInSc1
Pacific	Pacific	k1gMnSc1
area	area	k1gFnSc1
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
-	-	kIx~
Part	part	k1gInSc1
I	I	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHAPTER	CHAPTER	kA
VI	VI	kA
<g/>
:	:	kIx,
CONQUEST	CONQUEST	kA
OF	OF	kA
THE	THE	kA
PHILIPPINES	PHILIPPINES	kA
<g/>
:	:	kIx,
Launching	Launching	k1gInSc1
of	of	k?
Operations	Operations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Burton	Burton	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
127	#num#	k4
a	a	k8xC
1281	#num#	k4
2	#num#	k4
3	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
170	#num#	k4
a	a	k8xC
171	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
166	#num#	k4
a	a	k8xC
167	#num#	k4
<g/>
↑	↑	k?
HEYLER	HEYLER	kA
<g/>
,	,	kIx,
Glenn	Glenn	k1gMnSc1
T.	T.	kA
<g/>
;	;	kIx,
FUKUTO	FUKUTO	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saburo	Sabura	k1gFnSc5
Sakai	Saka	k1gMnPc5
-	-	kIx~
The	The	k1gFnSc1
Last	Lasta	k1gFnPc2
Samurai	Samura	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acesofww	acesofww	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dull	Dull	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
167	#num#	k4
a	a	k8xC
168	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	i	k8xC
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
168	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
169	#num#	k4
a	a	k8xC
170	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
171	#num#	k4
a	a	k8xC
172	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
174	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
174	#num#	k4
a	a	k8xC
175	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1211	#num#	k4
2	#num#	k4
3	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
121	#num#	k4
a	a	k8xC
detailněji	detailně	k6eAd2
str	str	kA
<g/>
.	.	kIx.
128	#num#	k4
až	až	k9
130	#num#	k4
<g/>
↑	↑	k?
NEVITT	NEVITT	kA
<g/>
,	,	kIx,
Allyn	Allyn	k1gMnSc1
D.	D.	kA
IJN	IJN	kA
Yamagumo	Yamaguma	k1gFnSc5
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NEVITT	NEVITT	kA
<g/>
,	,	kIx,
Allyn	Allyn	k1gMnSc1
D.	D.	kA
IJN	IJN	kA
Manazuru	Manazur	k1gInSc3
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NEVITT	NEVITT	kA
<g/>
,	,	kIx,
Allyn	Allyn	k1gMnSc1
D.	D.	kA
IJN	IJN	kA
Hatsukari	Hatsukar	k1gMnPc1
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	bob	k1gInSc1
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
Kingsepp	Kingsepp	k1gMnSc1
<g/>
,	,	kIx,
Allan	Allan	k1gMnSc1
Alsleben	Alsleben	k2eAgMnSc1d1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Cundall	Cundall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Seaplane	Seaplan	k1gMnSc5
Tender	tender	k1gInSc1
SANUKI	SANUKI	kA
MARU	Maru	k1gFnSc2
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
13	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Dull	Dulla	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
301	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Batan	Batan	k1gMnSc1
Island	Island	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
130	#num#	k4
a	a	k8xC
131	#num#	k4
<g/>
↑	↑	k?
Messimer	Messimer	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
47	#num#	k4
a	a	k8xC
481	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176	#num#	k4
a	a	k8xC
1771	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
First	First	k1gFnSc1
Days	Days	k1gInSc4
of	of	k?
War	War	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Gain	Gain	k1gMnSc1
Air	Air	k1gMnSc1
and	and	k?
Naval	navalit	k5eAaPmRp2nS
Supremacy	Supremaca	k1gFnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Burton	Burton	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
174	#num#	k4
<g/>
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HIJMS	HIJMS	kA
MAYA	MAYA	k?
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1997	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HIJMS	HIJMS	kA
KUMA	kuma	k1gFnSc1
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1997	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
Landings	Landings	k1gInSc4
on	on	k3xPp3gMnSc1
North	North	k1gMnSc1
Luzon	Luzon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
15	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
16	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
19	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Subchaser	Subchaser	k1gInSc1
CH-	CH-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Subchaser	Subchaser	k1gInSc1
CH-	CH-	k1gFnSc1
<g/>
15	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
a	a	k8xC
184	#num#	k4
<g/>
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
9	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
10	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
11	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
12	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Subchaser	Subchaser	k1gInSc1
CH-	CH-	k1gFnSc1
<g/>
12	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Subchaser	Subchaser	k1gInSc1
CH-	CH-	k1gFnSc1
<g/>
16	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dull	Dull	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
30	#num#	k4
a	a	k8xC
31	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
136	#num#	k4
a	a	k8xC
137	#num#	k4
<g/>
↑	↑	k?
Laoag	Laoag	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
pacificwrecks	pacificwrecks	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2008-12-17	2008-12-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
153	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
The	The	k1gFnSc1
first	first	k1gMnSc1
warning	warning	k1gInSc1
any	any	k?
of	of	k?
us	us	k?
on	on	k3xPp3gMnSc1
the	the	k?
ground	ground	k1gInSc1
would	would	k1gInSc1
get	get	k?
that	that	k2eAgInSc1d1
Japs	Japs	k1gInSc1
were	wer	k1gFnSc2
in	in	k?
the	the	k?
vicinity	vicinita	k1gFnSc2
would	would	k1gMnSc1
come	come	k1gNnSc2
when	when	k1gMnSc1
we	we	k?
saw	saw	k?
them	them	k1gInSc1
diving	diving	k1gInSc1
over	over	k1gInSc1
the	the	k?
hills	hills	k1gInSc1
shooting	shooting	k1gInSc1
at	at	k?
us	us	k?
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
1	#num#	k4
2	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
až	až	k9
1821	#num#	k4
2	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
First	First	k1gFnSc1
Days	Days	k1gInSc4
of	of	k?
War	War	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Gain	Gain	k1gMnSc1
Air	Air	k1gMnSc1
and	and	k?
Naval	navalit	k5eAaPmRp2nS
Supremacy	Supremaca	k1gFnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Whippoorwill	Whippoorwill	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
:	:	kIx,
Department	department	k1gInSc1
of	of	k?
the	the	k?
Navy	Navy	k?
–	–	k?
Naval	navalit	k5eAaPmRp2nS
Historical	Historical	k1gFnPc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2004-01-29	2004-01-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Peary	Peara	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
:	:	kIx,
Department	department	k1gInSc1
of	of	k?
the	the	k?
Navy	Navy	k?
–	–	k?
Naval	navalit	k5eAaPmRp2nS
Historical	Historical	k1gFnPc6
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bittern	Bittern	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
:	:	kIx,
Department	department	k1gInSc1
of	of	k?
the	the	k?
Navy	Navy	k?
–	–	k?
Naval	navalit	k5eAaPmRp2nS
Historical	Historical	k1gFnPc6
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
180	#num#	k4
<g/>
↑	↑	k?
Knott	Knott	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
a	a	k8xC
23	#num#	k4
<g/>
↑	↑	k?
CRESSMAN	CRESSMAN	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
J.	J.	kA
The	The	k1gMnSc1
Official	Official	k1gMnSc1
Chronology	chronolog	k1gMnPc4
of	of	k?
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
:	:	kIx,
Department	department	k1gInSc1
of	of	k?
the	the	k?
Navy	Navy	k?
—	—	k?
Naval	navalit	k5eAaPmRp2nS
Historical	Historical	k1gFnPc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2003-10-19	2003-10-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
180	#num#	k4
a	a	k8xC
tabulka	tabulka	k1gFnSc1
16	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
1811	#num#	k4
2	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
184	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
153	#num#	k4
<g/>
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minelayer	Minelayra	k1gFnPc2
YAEYAMA	YAEYAMA	kA
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Dull	Dulla	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
311	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Legaspi	Legasp	k1gFnSc2
Landing	Landing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
CUNDALL	CUNDALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Minesweeper	Minesweeper	k1gInSc1
W-	W-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
187	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
185	#num#	k4
a	a	k8xC
186	#num#	k4
<g/>
,	,	kIx,
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
9	#num#	k4
a	a	k8xC
10	#num#	k4
a	a	k8xC
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
159	#num#	k4
<g/>
↑	↑	k?
LT	LT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
COL	cola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BOYD	BOYD	kA
"	"	kIx"
<g/>
BUZZ	BUZZ	kA
<g/>
"	"	kIx"
WAGNER	Wagner	k1gMnSc1
<g/>
:	:	kIx,
First	First	k1gMnSc1
USAAF	USAAF	kA
Ace	Ace	k1gMnSc1
of	of	k?
WW2	WW2	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acepilots	Acepilots	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
185	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1221	#num#	k4
2	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
184	#num#	k4
a	a	k8xC
185	#num#	k4
<g/>
↑	↑	k?
Knott	Knott	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
23	#num#	k4
a	a	k8xC
24	#num#	k4
<g/>
↑	↑	k?
GRAHAM	graham	k1gInSc1
<g/>
,	,	kIx,
Wynnum	Wynnum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Re	re	k9
<g/>
:	:	kIx,
PBY	PBY	kA
sunk	sunk	k1gMnSc1
at	at	k?
Subic	Subic	k1gMnSc1
Bay	Bay	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
pby	pby	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1999-11-28	1999-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
186	#num#	k4
a	a	k8xC
187	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
157	#num#	k4
a	a	k8xC
158	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
187	#num#	k4
a	a	k8xC
189	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartsch	k1gMnSc1
I	i	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
165	#num#	k4
a	a	k8xC
166	#num#	k4
popisuje	popisovat	k5eAaImIp3nS
útok	útok	k1gInSc4
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Field	k1gMnSc1
<g/>
↑	↑	k?
Bartsch	Bartsch	k1gMnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
163	#num#	k4
a	a	k8xC
164	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
189	#num#	k4
a	a	k8xC
190	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
190	#num#	k4
a	a	k8xC
191	#num#	k4
a	a	k8xC
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
166	#num#	k4
a	a	k8xC
167	#num#	k4
<g/>
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	bob	k1gInSc1
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
Kingsepp	Kingsepp	k1gMnSc1
<g/>
,	,	kIx,
Allan	Allan	k1gMnSc1
Alsleben	Alsleben	k2eAgMnSc1d1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Cundall	Cundall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
Seaplane	Seaplan	k1gMnSc5
Tender	tender	k1gInSc1
SANYO	SANYO	kA
MARU	Maru	k1gFnSc2
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
191	#num#	k4
<g/>
↑	↑	k?
Knott	Knott	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
↑	↑	k?
Re	re	k9
<g/>
:	:	kIx,
PBY	PBY	kA
sunk	sunk	k1gMnSc1
at	at	k?
Subic	Subic	k1gMnSc1
Bay	Bay	k1gMnSc1
by	by	kYmCp3nS
Allan_LeBaron@hotmail.com	Allan_LeBaron@hotmail.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
pby	pby	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1999-11-28	1999-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Knott	Knott	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
a	a	k8xC
25	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
167	#num#	k4
<g/>
,	,	kIx,
168	#num#	k4
<g/>
,	,	kIx,
179	#num#	k4
a	a	k8xC
1801	#num#	k4
2	#num#	k4
3	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
192	#num#	k4
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
168	#num#	k4
a	a	k8xC
169	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
170	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
192	#num#	k4
a	a	k8xC
193	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
173	#num#	k4
a	a	k8xC
174	#num#	k4
popisuje	popisovat	k5eAaImIp3nS
útok	útok	k1gInSc4
na	na	k7c4
Nichols	Nichols	k1gInSc4
Field	Field	k1gInSc1
<g/>
↑	↑	k?
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Landings	Landingsa	k1gFnPc2
in	in	k?
the	the	k?
South	South	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartsch	Bartsch	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1821	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
193	#num#	k4
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
193	#num#	k4
a	a	k8xC
1941	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnPc2
Lingayen	Lingayen	k2eAgInSc1d1
Landing	Landing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Burton	Burton	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
261	#num#	k4
<g/>
↑	↑	k?
HACKETT	HACKETT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
;	;	kIx,
KINGSEPP	KINGSEPP	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
<g/>
;	;	kIx,
AHLBERG	AHLBERG	kA
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJN	IJN	kA
HARUNA	HARUNA	kA
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Consolidating	Consolidating	k1gInSc1
the	the	k?
Lingayen	Lingayen	k2eAgInSc1d1
Beachhead	Beachhead	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROSCOE	ROSCOE	kA
<g/>
,	,	kIx,
Theodore	Theodor	k1gMnSc5
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
G.	G.	kA
Voge	Voge	k1gInSc1
<g/>
,	,	kIx,
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bureau	Bureaus	k1gInSc2
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Personnel	Personnel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
submarine	submarin	k1gInSc5
operations	operations	k6eAd1
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
..	..	k?
11	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ilustrované	ilustrovaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780870217319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
S-38	S-38	k1gFnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
All	All	k1gFnSc1
the	the	k?
Odds	Odds	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
36	#num#	k4
až	až	k9
38	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
194	#num#	k4
a	a	k8xC
195	#num#	k4
<g/>
,	,	kIx,
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
2	#num#	k4
Burton	Burton	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
261	#num#	k4
až	až	k9
263	#num#	k4
<g/>
↑	↑	k?
Bartsch	Bartscha	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
183	#num#	k4
až	až	k9
186	#num#	k4
<g/>
↑	↑	k?
PEJČOCH	PEJČOCH	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonský	japonský	k2eAgInSc4d1
lehký	lehký	k2eAgInSc4d1
tank	tank	k1gInSc4
Ha-go	Ha-go	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hobby	hobby	k1gNnSc7
Historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Březen-duben	Březen-duben	k2eAgInSc4d1
2010	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1804	#num#	k4
<g/>
-	-	kIx~
<g/>
2228	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Damortis	Damortis	k1gFnSc2
and	and	k?
Rosario	Rosario	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SALECKER	SALECKER	kA
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
Eric	Eric	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortress	Fortress	k1gInSc1
against	against	k1gInSc4
the	the	k?
sun	sun	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
B-17	B-17	k1gFnSc1
Flying	Flying	k1gInSc4
Fortress	Fortress	k1gInSc1
in	in	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
De	De	k?
Capo	capa	k1gFnSc5
Press	Press	k1gInSc4
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
5809	#num#	k4
<g/>
-	-	kIx~
<g/>
7049	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
84	#num#	k4
a	a	k8xC
85	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Approach	Approacha	k1gFnPc2
to	ten	k3xDgNnSc1
the	the	k?
Agno	Agno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
196	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I.	I.	kA
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DULL	DULL	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
S.	S.	kA
A	a	k9
Battle	Battle	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1591142199	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BARTSCH	BARTSCH	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
H.	H.	kA
Doomed	Doomed	k1gInSc1
at	at	k?
the	the	k?
Start	start	k1gInSc1
<g/>
:	:	kIx,
American	American	k1gInSc1
Pursuit	Pursuit	k1gMnSc1
Pilots	Pilotsa	k1gFnPc2
in	in	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1942	#num#	k4
<g/>
.	.	kIx.
???	???	k?
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
:	:	kIx,
Texas	Texas	k1gInSc1
A	A	kA
<g/>
&	&	k?
<g/>
M	M	kA
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
890966792	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
–	–	k?
odkazováno	odkazovat	k5eAaImNgNnS
jako	jako	k9
„	„	k?
<g/>
Bartsch	Bartsch	k1gInSc1
I	i	k8xC
<g/>
“	“	k?
</s>
<s>
BARTSCH	BARTSCH	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
H.	H.	kA
December	December	k1gInSc4
8	#num#	k4
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
:	:	kIx,
MacArthur	MacArthura	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
.	.	kIx.
???	???	k?
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
:	:	kIx,
Texas	Texas	k1gInSc1
A	A	kA
<g/>
&	&	k?
<g/>
M	M	kA
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781585442461	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
–	–	k?
odkazováno	odkazovat	k5eAaImNgNnS
jako	jako	k9
„	„	k?
<g/>
Bartsch	Bartscha	k1gFnPc2
II	II	kA
<g/>
“	“	k?
</s>
<s>
BURTON	BURTON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortnight	Fortnight	k1gInSc4
of	of	k?
infamy	infam	k1gInPc4
<g/>
:	:	kIx,
the	the	k?
collapse	collapse	k1gFnSc2
of	of	k?
Allied	Allied	k1gMnSc1
airpower	airpower	k1gMnSc1
west	west	k1gMnSc1
of	of	k?
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781591140962	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CALDWELL	CALDWELL	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
L.	L.	kA
Thunder	Thunder	k1gInSc4
on	on	k3xPp3gMnSc1
Bataan	Bataan	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
First	First	k1gMnSc1
American	Americana	k1gFnPc2
Tank	tank	k1gInSc1
Battles	Battles	k1gMnSc1
of	of	k?
World	World	k1gMnSc1
War	War	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guilford	Guilford	k1gInSc1
<g/>
:	:	kIx,
Stackpole	Stackpole	k1gFnSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8117	#num#	k4
<g/>
-	-	kIx~
<g/>
3771	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MORRIS	MORRIS	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Corregidor	Corregidor	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
American	American	k1gMnSc1
Alamo	Alama	k1gFnSc5
of	of	k?
World	Worldo	k1gNnPc2
War	War	k1gMnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cooper	Cooper	k1gInSc1
Square	square	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8154	#num#	k4
<g/>
-	-	kIx~
<g/>
1085	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WILLMOTT	WILLMOTT	kA
<g/>
,	,	kIx,
H.	H.	kA
P.	P.	kA
Empires	Empires	k1gMnSc1
in	in	k?
the	the	k?
Balance	balance	k1gFnSc1
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
and	and	k?
Allied	Allied	k1gMnSc1
Pacific	Pacific	k1gMnSc1
Strategies	Strategies	k1gMnSc1
To	to	k9
April	April	k1gInSc4
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
948	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WINSLOW	WINSLOW	kA
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
G.	G.	kA
The	The	k1gMnSc1
Fleet	Fleet	k1gMnSc1
The	The	k1gMnSc1
Gods	Godsa	k1gFnPc2
Forgot	Forgot	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
Asiatic	Asiatice	k1gFnPc2
Fleet	Fleet	k1gMnSc1
in	in	k?
World	World	k1gMnSc1
War	War	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
188	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
YOUNG	YOUNG	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
J.	J.	kA
The	The	k1gMnSc1
Battle	Battle	k1gFnSc2
of	of	k?
Bataan	Bataan	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Complete	Comple	k1gNnSc2
History	Histor	k1gMnPc4
<g/>
,	,	kIx,
second	second	k1gInSc1
edition	edition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jefferson	Jefferson	k1gInSc1
NC	NC	kA
&	&	k?
London	London	k1gMnSc1
<g/>
:	:	kIx,
McFarland	McFarland	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7864	#num#	k4
<g/>
-	-	kIx~
<g/>
4180	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Boj	boj	k1gInSc4
o	o	k7c6
Filipíny	Filipíny	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Rising	Rising	k1gInSc1
Sun	Sun	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Conquests	Conquests	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HyperWar	HyperWar	k1gInSc1
Foundation	Foundation	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
–	–	k?
linky	linka	k1gFnPc1
na	na	k7c4
další	další	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
a	a	k8xC
odtajněné	odtajněný	k2eAgInPc4d1
materiály	materiál	k1gInPc4
(	(	kIx(
<g/>
primární	primární	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
Campaigns	Campaignsa	k1gFnPc2
of	of	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
Philipine	Philipin	k1gInSc5
Islands	Islands	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2003	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MORTON	MORTON	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
-	-	kIx~
U.	U.	kA
<g/>
S.	S.	kA
Army	Arma	k1gMnSc2
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacifice	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Army	Army	k1gInPc4
Center	centrum	k1gNnPc2
of	of	k?
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
1953	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Reports	Reports	k6eAd1
of	of	k?
General	General	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
operations	operations	k1gInSc1
in	in	k?
the	the	k?
southwest	southwest	k1gInSc1
Pacific	Pacific	k1gMnSc1
area	area	k1gFnSc1
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
II	II	kA
-	-	kIx~
Part	part	k1gInSc1
I	I	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHAPTER	CHAPTER	kA
VI	VI	kA
<g/>
:	:	kIx,
CONQUEST	CONQUEST	kA
OF	OF	kA
THE	THE	kA
PHILIPPINES	PHILIPPINES	kA
<g/>
:	:	kIx,
Preliminary	Preliminara	k1gFnSc2
Planning	Planning	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Reports	Reports	k6eAd1
of	of	k?
General	General	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
:	:	kIx,
THE	THE	kA
CAMPAIGNS	CAMPAIGNS	kA
OF	OF	kA
MACARTHUR	MACARTHUR	kA
IN	IN	kA
THE	THE	kA
PACIFIC	PACIFIC	kA
<g/>
,	,	kIx,
VOLUME	volum	k1gInSc5
I	i	k9
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHAPTER	CHAPTER	kA
I	i	k9
<g/>
:	:	kIx,
THE	THE	kA
JAPANESE	JAPANESE	kA
OFFENSIVE	OFFENSIVE	kA
IN	IN	kA
THE	THE	kA
PACIFIC	PACIFIC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
COWAN	COWAN	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
H.	H.	kA
Barbed	Barbed	k1gMnSc1
Wire	Wir	k1gFnSc2
and	and	k?
Rice	Rice	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
whoa	whoa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
1972-05-01	1972-05-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
From	Fro	k1gNnSc7
Corregidor	Corregidora	k1gFnPc2
to	ten	k3xDgNnSc1
the	the	k?
Philippines	Philippines	k1gInSc1
photos	photos	k1gInSc1
and	and	k?
text	text	k1gInSc1
related	related	k1gInSc1
to	ten	k3xDgNnSc4
The	The	k1gFnSc1
Edge	Edg	k1gInSc2
by	by	kYmCp3nS
First	First	k1gFnSc1
Lieutenant	Lieutenanta	k1gFnPc2
George	Georg	k1gMnSc2
Francis	Francis	k1gFnSc2
<g/>
,	,	kIx,
USMC	USMC	kA
<g/>
,	,	kIx,
Retired	Retired	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fourthmarinesband	fourthmarinesband	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2007-09-03	2007-09-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HINDS	HINDS	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
America	America	k1gFnSc1
in	in	k?
WWII	WWII	kA
magazine	magazinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
Motor	motor	k1gInSc1
Torpedo	Torpedo	k1gNnSc1
Squadron	Squadron	k1gInSc1
3	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
americainwwii	americainwwie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-05-31	2009-05-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WHITMAN	WHITMAN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
W.	W.	kA
Manila	Manila	k1gFnSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
Open	Open	k1gMnSc1
Was	Was	k1gMnSc1
This	Thisa	k1gFnPc2
Open	Open	k1gMnSc1
City	City	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
historynet	historynet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
VAN	van	k1gInSc1
DEN	den	k1gInSc1
BERGH	BERGH	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
J.	J.	kA
<g/>
;	;	kIx,
ANDERSON	Anderson	k1gMnSc1
<g/>
,	,	kIx,
Burton	Burton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
California	Californium	k1gNnPc1
Militia	Militius	k1gMnSc2
and	and	k?
National	National	k1gMnSc1
Guard	Guard	k1gMnSc1
Unit	Unit	k2eAgInSc4d1
Histories	Histories	k1gInSc4
<g/>
:	:	kIx,
Company	Compana	k1gFnSc2
C	C	kA
<g/>
,	,	kIx,
194	#num#	k4
<g/>
th	th	k?
Tank	tank	k1gInSc1
Battalion	Battalion	k1gInSc1
in	in	k?
the	the	k?
Philippines	Philippines	k1gMnSc1
<g/>
,	,	kIx,
1941-42	1941-42	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
militarymuseum	militarymuseum	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-06-23	2017-06-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Philippines	Philippines	k1gMnSc1
Campaign	Campaign	k1gMnSc1
<g/>
:	:	kIx,
Prelude	Prelud	k1gInSc5
to	ten	k3xDgNnSc4
The	The	k1gFnSc7
Philippines	Philippines	k1gMnSc1
Campaign	Campaign	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
