<s>
Okulár	okulár	k1gInSc1	okulár
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
oculus	oculus	k1gMnSc1	oculus
<g/>
,	,	kIx,	,
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
a	a	k8xC	a
ocularis	ocularis	k1gInSc1	ocularis
<g/>
,	,	kIx,	,
oční	oční	k2eAgMnSc1d1	oční
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čočka	čočka	k1gFnSc1	čočka
nebo	nebo	k8xC	nebo
soustava	soustava	k1gFnSc1	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
obraz	obraz	k1gInSc1	obraz
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
v	v	k7c6	v
ohniskové	ohniskový	k2eAgFnSc6d1	ohnisková
rovině	rovina	k1gFnSc6	rovina
objektivu	objektiv	k1gInSc2	objektiv
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
mikroskopu	mikroskop	k1gInSc2	mikroskop
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Okulár	okulár	k1gInSc1	okulár
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
oku	oka	k1gFnSc4	oka
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
obraz	obraz	k1gInSc1	obraz
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
ohniskové	ohniskový	k2eAgFnSc6d1	ohnisková
rovině	rovina	k1gFnSc6	rovina
objektivu	objektiv	k1gInSc2	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Výstupní	výstupní	k2eAgFnSc1d1	výstupní
pupila	pupila	k1gFnSc1	pupila
okuláru	okulár	k1gInSc2	okulár
má	mít	k5eAaImIp3nS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
vstupní	vstupní	k2eAgFnSc3d1	vstupní
pupile	pupila	k1gFnSc3	pupila
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Posouváním	posouvání	k1gNnSc7	posouvání
okuláru	okulár	k1gInSc2	okulár
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
optické	optický	k2eAgFnSc2d1	optická
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
zaostřuje	zaostřovat	k5eAaImIp3nS	zaostřovat
<g/>
.	.	kIx.	.
</s>
<s>
Galileiho	Galileize	k6eAd1	Galileize
okulár	okulár	k1gInSc1	okulár
je	být	k5eAaImIp3nS	být
dvojvydutá	dvojvydutý	k2eAgFnSc1d1	dvojvydutý
rozptylka	rozptylka	k1gFnSc1	rozptylka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neobrací	obracet	k5eNaImIp3nS	obracet
obraz	obraz	k1gInSc4	obraz
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
však	však	k9	však
jen	jen	k9	jen
malé	malý	k2eAgNnSc4d1	malé
zvětšení	zvětšení	k1gNnSc4	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Keplerův	Keplerův	k2eAgInSc1d1	Keplerův
okulár	okulár	k1gInSc1	okulár
je	být	k5eAaImIp3nS	být
spojná	spojný	k2eAgFnSc1d1	spojná
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
větší	veliký	k2eAgNnSc4d2	veliký
zvětšení	zvětšení	k1gNnSc4	zvětšení
<g/>
,	,	kIx,	,
obrací	obracet	k5eAaImIp3nS	obracet
však	však	k9	však
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Huygensův	Huygensův	k2eAgInSc1d1	Huygensův
okulár	okulár	k1gInSc1	okulár
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
plankonvexních	plankonvexní	k2eAgFnPc2d1	plankonvexní
spojných	spojný	k2eAgFnPc2d1	spojná
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
clona	clona	k1gFnSc1	clona
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
korekci	korekce	k1gFnSc4	korekce
barevné	barevný	k2eAgFnSc2d1	barevná
chyby	chyba	k1gFnSc2	chyba
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
u	u	k7c2	u
laciných	laciný	k2eAgInPc2d1	laciný
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Ortoskopický	ortoskopický	k2eAgInSc1d1	ortoskopický
okulár	okulár	k1gInSc1	okulár
podle	podle	k7c2	podle
Ernsta	Ernst	k1gMnSc2	Ernst
Abbe	Abb	k1gMnSc2	Abb
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
vstupní	vstupní	k2eAgFnSc2d1	vstupní
clony	clona	k1gFnSc2	clona
<g/>
,	,	kIx,	,
tmelené	tmelený	k2eAgFnSc2d1	tmelená
spojné	spojný	k2eAgFnSc2d1	spojná
skupiny	skupina	k1gFnSc2	skupina
tří	tři	k4xCgFnPc2	tři
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
z	z	k7c2	z
výstupní	výstupní	k2eAgFnSc2d1	výstupní
konvex-konkávní	konvexonkávní	k2eAgFnSc2d1	konvex-konkávní
spojky	spojka	k1gFnSc2	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
čtyřem	čtyři	k4xCgFnPc3	čtyři
čočkám	čočka	k1gFnPc3	čočka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
korekci	korekce	k1gFnSc4	korekce
barevné	barevný	k2eAgFnSc2d1	barevná
chyby	chyba	k1gFnSc2	chyba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
standard	standard	k1gInSc1	standard
u	u	k7c2	u
astronomických	astronomický	k2eAgInPc2d1	astronomický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Okulár	okulár	k1gInSc1	okulár
Ploessel-Steinheilův	Ploessel-Steinheilův	k2eAgMnSc1d1	Ploessel-Steinheilův
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
tmelených	tmelený	k2eAgInPc2d1	tmelený
dvojčlenných	dvojčlenný	k2eAgInPc2d1	dvojčlenný
achromatů	achromat	k1gInPc2	achromat
<g/>
,	,	kIx,	,
obrácených	obrácený	k2eAgInPc2d1	obrácený
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Koriguje	korigovat	k5eAaBmIp3nS	korigovat
barevnou	barevný	k2eAgFnSc4d1	barevná
chybu	chyba	k1gFnSc4	chyba
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
jako	jako	k8xC	jako
Abbeův	Abbeův	k2eAgInSc1d1	Abbeův
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
výrobně	výrobně	k6eAd1	výrobně
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
širokoúhlé	širokoúhlý	k2eAgFnPc4d1	širokoúhlá
soustavy	soustava	k1gFnPc4	soustava
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
okuláry	okulár	k1gInPc1	okulár
složitější	složitý	k2eAgInPc1d2	složitější
<g/>
,	,	kIx,	,
se	s	k7c7	s
šesti	šest	k4xCc7	šest
a	a	k8xC	a
více	hodně	k6eAd2	hodně
čočkami	čočka	k1gFnPc7	čočka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
nesférickými	sférický	k2eNgInPc7d1	sférický
povrchy	povrch	k1gInPc7	povrch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
výrobně	výrobně	k6eAd1	výrobně
drahé	drahý	k2eAgNnSc1d1	drahé
<g/>
.	.	kIx.	.
</s>
