<s>
Maseru	maser	k1gInSc3	maser
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Lesotha	Lesotha	k1gFnSc1	Lesotha
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Caledon	Caledona	k1gFnPc2	Caledona
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
