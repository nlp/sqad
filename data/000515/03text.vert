<s>
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Leitomischl	Leitomischl	k1gFnSc1	Leitomischl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
východě	východ	k1gInSc6	východ
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Svitav	Svitava	k1gFnPc2	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
981	[number]	k4	981
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
české	český	k2eAgFnPc1d1	Česká
(	(	kIx(	(
<g/>
Lutomisl	Lutomisl	k1gInSc1	Lutomisl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k6eAd1	až
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
na	na	k7c6	na
litomyšlském	litomyšlský	k2eAgNnSc6d1	litomyšlské
návrší	návrší	k1gNnSc6	návrší
stával	stávat	k5eAaImAgInS	stávat
strážní	strážní	k2eAgInSc4d1	strážní
hrad	hrad	k1gInSc4	hrad
střežící	střežící	k2eAgNnSc1d1	střežící
panství	panství	k1gNnSc1	panství
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
procházející	procházející	k2eAgFnSc4d1	procházející
obchodní	obchodní	k2eAgFnSc4d1	obchodní
cestu	cesta	k1gFnSc4	cesta
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Trstěnickou	Trstěnický	k2eAgFnSc4d1	Trstěnická
stezku	stezka	k1gFnSc4	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyvraždění	vyvraždění	k1gNnSc6	vyvraždění
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
roku	rok	k1gInSc2	rok
995	[number]	k4	995
přešlo	přejít	k5eAaPmAgNnS	přejít
území	území	k1gNnSc1	území
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
řád	řád	k1gInSc4	řád
premonstrátů	premonstrát	k1gMnPc2	premonstrát
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1145	[number]	k4	1145
premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
Olivetská	olivetský	k2eAgFnSc1d1	Olivetská
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Mons	Mons	k1gInSc4	Mons
oliveti	olivet	k5eAaPmF	olivet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Premonstráti	premonstrát	k1gMnPc1	premonstrát
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
úrovně	úroveň	k1gFnPc4	úroveň
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc4d1	kulturní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
povýšena	povýšen	k2eAgFnSc1d1	povýšena
roku	rok	k1gInSc2	rok
1259	[number]	k4	1259
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
lilie	lilie	k1gFnSc1	lilie
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
převzata	převzít	k5eAaPmNgNnP	převzít
právě	právě	k6eAd1	právě
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
premonstrátů	premonstrát	k1gMnPc2	premonstrát
(	(	kIx(	(
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
lilie	lilie	k1gFnSc1	lilie
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěšné	úspěšný	k2eAgFnSc3d1	úspěšná
premonstrátské	premonstrátský	k2eAgFnSc3d1	premonstrátská
kolonizaci	kolonizace	k1gFnSc3	kolonizace
a	a	k8xC	a
umístění	umístění	k1gNnSc4	umístění
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Olomoucí	Olomouc	k1gFnSc7	Olomouc
založil	založit	k5eAaPmAgMnS	založit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1344	[number]	k4	1344
biskupství	biskupství	k1gNnSc4	biskupství
litomyšlské	litomyšlský	k2eAgNnSc4d1	litomyšlské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
litomyšlských	litomyšlský	k2eAgMnPc2d1	litomyšlský
biskupů	biskup	k1gMnPc2	biskup
působili	působit	k5eAaImAgMnP	působit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
ze	z	k7c2	z
Středy	středa	k1gFnSc2	středa
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Železný	Železný	k1gMnSc1	Železný
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
město	město	k1gNnSc4	město
napadli	napadnout	k5eAaPmAgMnP	napadnout
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vlnách	vlna	k1gFnPc6	vlna
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1421	[number]	k4	1421
a	a	k8xC	a
1425	[number]	k4	1425
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
velice	velice	k6eAd1	velice
poškodili	poškodit	k5eAaPmAgMnP	poškodit
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
účinkování	účinkování	k1gNnSc3	účinkování
Jana	Jan	k1gMnSc2	Jan
Železného	Železný	k1gMnSc2	Železný
na	na	k7c6	na
Kostnickém	kostnický	k2eAgInSc6d1	kostnický
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Zikmund	Zikmund	k1gMnSc1	Zikmund
svěřil	svěřit	k5eAaPmAgMnS	svěřit
panství	panství	k1gNnSc4	panství
Kostkům	Kostka	k1gMnPc3	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
<g/>
.	.	kIx.	.
</s>
<s>
Kostkové	Kostková	k1gFnPc1	Kostková
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
panství	panství	k1gNnSc6	panství
podporovali	podporovat	k5eAaImAgMnP	podporovat
Jednotu	jednota	k1gFnSc4	jednota
bratrskou	bratrský	k2eAgFnSc4d1	bratrská
a	a	k8xC	a
i	i	k9	i
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
ohrazením	ohrazení	k1gNnSc7	ohrazení
<g/>
,	,	kIx,	,
náměstím	náměstí	k1gNnSc7	náměstí
a	a	k8xC	a
radnicí	radnice	k1gFnSc7	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Kostkům	Kostka	k1gMnPc3	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gInPc2	Postupice
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
stavovském	stavovský	k2eAgInSc6d1	stavovský
odboji	odboj	k1gInSc6	odboj
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
zabaveno	zabavit	k5eAaPmNgNnS	zabavit
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
majiteli	majitel	k1gMnPc7	majitel
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
kancléř	kancléř	k1gMnSc1	kancléř
Království	království	k1gNnSc1	království
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nositelem	nositel	k1gMnSc7	nositel
Řádu	řád	k1gInSc2	řád
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
rouna	rouno	k1gNnSc2	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
litomyšlský	litomyšlský	k2eAgInSc4d1	litomyšlský
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
stavitelem	stavitel	k1gMnSc7	stavitel
byl	být	k5eAaImAgMnS	být
hlavně	hlavně	k6eAd1	hlavně
Giovanni	Giovanň	k1gMnSc3	Giovanň
Battista	Battista	k1gMnSc1	Battista
Aostalli	Aostall	k1gMnSc3	Aostall
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1649	[number]	k4	1649
prodali	prodat	k5eAaPmAgMnP	prodat
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
panství	panství	k1gNnPc2	panství
Trauttmansdorffům	Trauttmansdorff	k1gMnPc3	Trauttmansdorff
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sňatku	sňatek	k1gInSc2	sňatek
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Valdštejn-Vartemberkům	Valdštejn-Vartemberk	k1gInPc3	Valdštejn-Vartemberk
a	a	k8xC	a
posledními	poslední	k2eAgMnPc7d1	poslední
majiteli	majitel	k1gMnPc7	majitel
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
Thurn-Taxisové	Thurn-Taxisový	k2eAgInPc1d1	Thurn-Taxisový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc4	město
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vedení	vedení	k1gNnSc2	vedení
významných	významný	k2eAgFnPc2d1	významná
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k1gMnPc3	známý
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
litomyšlské	litomyšlský	k2eAgNnSc1d1	litomyšlské
piaristické	piaristický	k2eAgNnSc1d1	Piaristické
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pobývala	pobývat	k5eAaImAgFnS	pobývat
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
nebo	nebo	k8xC	nebo
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
dnem	den	k1gInSc7	den
pro	pro	k7c4	pro
Litomyšl	Litomyšl	k1gFnSc4	Litomyšl
dodnes	dodnes	k6eAd1	dodnes
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
2	[number]	k4	2
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
sládka	sládek	k1gMnSc2	sládek
zámeckého	zámecký	k2eAgInSc2d1	zámecký
pivovaru	pivovar	k1gInSc2	pivovar
Františka	František	k1gMnSc2	František
Smetany	Smetana	k1gMnSc2	Smetana
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Barbory	Barbora	k1gFnSc2	Barbora
jako	jako	k8xS	jako
jejich	jejich	k3xOp3gInSc1	jejich
11	[number]	k4	11
<g/>
.	.	kIx.	.
dítě	dítě	k1gNnSc1	dítě
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
narodil	narodit	k5eAaPmAgMnS	narodit
dnes	dnes	k6eAd1	dnes
světově	světově	k6eAd1	světově
známý	známý	k2eAgMnSc1d1	známý
skladatel	skladatel	k1gMnSc1	skladatel
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
odkaz	odkaz	k1gInSc1	odkaz
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
udržuje	udržovat	k5eAaImIp3nS	udržovat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
operní	operní	k2eAgInSc1d1	operní
festival	festival	k1gInSc1	festival
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1929	[number]	k4	1929
město	město	k1gNnSc1	město
oficiálně	oficiálně	k6eAd1	oficiálně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
T.G.	T.G.	k1gMnSc1	T.G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgMnS	navštívit
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
nejprve	nejprve	k6eAd1	nejprve
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
o	o	k7c4	o
vilové	vilový	k2eAgFnPc4d1	vilová
čtvrtě	čtvrt	k1gFnPc4	čtvrt
(	(	kIx(	(
<g/>
Husova	Husův	k2eAgFnSc1d1	Husova
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
také	také	k9	také
o	o	k7c6	o
sídliště	sídliště	k1gNnPc4	sídliště
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
Vertex	vertex	k1gInSc1	vertex
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
průtahem	průtah	k1gInSc7	průtah
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
známým	známý	k2eAgNnSc7d1	známé
podporou	podpora	k1gFnSc7	podpora
výstavby	výstavba	k1gFnSc2	výstavba
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
obytný	obytný	k2eAgInSc4d1	obytný
komplex	komplex	k1gInSc4	komplex
Za	za	k7c7	za
nemocnicí	nemocnice	k1gFnSc7	nemocnice
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
zámeckého	zámecký	k2eAgInSc2d1	zámecký
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
půvab	půvab	k1gInSc4	půvab
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
se	se	k3xPyFc4	se
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
stala	stát	k5eAaPmAgFnS	stát
známým	známý	k2eAgInSc7d1	známý
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1568	[number]	k4	1568
<g/>
-	-	kIx~	-
<g/>
1581	[number]	k4	1581
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
<g/>
,	,	kIx,	,
barokním	barokní	k2eAgNnSc7d1	barokní
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
parkem	park	k1gInSc7	park
a	a	k8xC	a
pivovarem	pivovar	k1gInSc7	pivovar
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
náměstí	náměstí	k1gNnSc1	náměstí
s	s	k7c7	s
úplně	úplně	k6eAd1	úplně
zachovaným	zachovaný	k2eAgNnSc7d1	zachované
podloubím	podloubí	k1gNnSc7	podloubí
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
historických	historický	k2eAgInPc2d1	historický
domů	dům	k1gInPc2	dům
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1418	[number]	k4	1418
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Rytířů	Rytíř	k1gMnPc2	Rytíř
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
110	[number]	k4	110
<g/>
)	)	kIx)	)
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
zdobenou	zdobený	k2eAgFnSc7d1	zdobená
kamennou	kamenný	k2eAgFnSc7d1	kamenná
fasádou	fasáda	k1gFnSc7	fasáda
a	a	k8xC	a
podloubím	podloubí	k1gNnSc7	podloubí
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
mistra	mistr	k1gMnSc2	mistr
Blažka	Blažek	k1gMnSc2	Blažek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
1550	[number]	k4	1550
Bývalý	bývalý	k2eAgInSc1d1	bývalý
Pernštejnský	pernštejnský	k2eAgInSc1d1	pernštejnský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
Smetanův	Smetanův	k2eAgInSc1d1	Smetanův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
s	s	k7c7	s
divadelním	divadelní	k2eAgInSc7d1	divadelní
sálem	sál	k1gInSc7	sál
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
náměstí	náměstí	k1gNnSc2	náměstí
Proboštský	proboštský	k2eAgMnSc1d1	proboštský
a	a	k8xC	a
kapitulní	kapitulní	k2eAgInSc1d1	kapitulní
kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
Šantově	Šantův	k2eAgNnSc6d1	Šantovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
součást	součást	k1gFnSc1	součást
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
1356	[number]	k4	1356
<g/>
,	,	kIx,	,
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
době	doba	k1gFnSc6	doba
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
gotická	gotický	k2eAgFnSc1d1	gotická
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Josefa	Josefa	k1gFnSc1	Josefa
a	a	k8xC	a
gotická	gotický	k2eAgFnSc1d1	gotická
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
budova	budova	k1gFnSc1	budova
proboštství	proboštství	k1gNnSc2	proboštství
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1758	[number]	k4	1758
<g/>
-	-	kIx~	-
<g/>
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
Šantovo	Šantův	k2eAgNnSc4d1	Šantovo
náměstí	náměstí	k1gNnSc4	náměstí
183	[number]	k4	183
Bývalý	bývalý	k2eAgInSc1d1	bývalý
piaristický	piaristický	k2eAgInSc1d1	piaristický
kostel	kostel	k1gInSc1	kostel
Nalezení	nalezení	k1gNnSc2	nalezení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
zámku	zámek	k1gInSc3	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
příčnou	příčný	k2eAgFnSc7d1	příčná
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
bočními	boční	k2eAgFnPc7d1	boční
kaplemi	kaple	k1gFnPc7	kaple
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1716	[number]	k4	1716
<g/>
-	-	kIx~	-
<g/>
1730	[number]	k4	1730
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Battisty	Battista	k1gMnSc2	Battista
Alliprandiho	Alliprandi	k1gMnSc2	Alliprandi
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
Kaňky	Kaňka	k1gMnSc2	Kaňka
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
piaristická	piaristický	k2eAgFnSc1d1	piaristická
kolej	kolej	k1gFnSc1	kolej
a	a	k8xC	a
gymnázium	gymnázium	k1gNnSc1	gymnázium
z	z	k7c2	z
téže	tenže	k3xDgFnSc2	tenže
doby	doba	k1gFnSc2	doba
Kostel	kostel	k1gInSc1	kostel
Rozeslání	rozeslání	k1gNnSc2	rozeslání
svatých	svatý	k2eAgMnPc2d1	svatý
apoštolů	apoštol	k1gMnPc2	apoštol
na	na	k7c6	na
Toulovcově	Toulovcův	k2eAgNnSc6d1	Toulovcovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Nový	nový	k2eAgInSc1d1	nový
kostel	kostel	k1gInSc1	kostel
-	-	kIx~	-
modlitebna	modlitebna	k1gFnSc1	modlitebna
Církve	církev	k1gFnSc2	církev
bratrské	bratrský	k2eAgFnSc2d1	bratrská
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Fránka	Fránek	k1gMnSc2	Fránek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc2	ulice
Komenského	Komenského	k2eAgFnSc1d1	Komenského
<g/>
,	,	kIx,	,
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1696	[number]	k4	1696
Hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1670-1672	[number]	k4	1670-1672
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
pomníky	pomník	k1gInPc4	pomník
rodičů	rodič	k1gMnPc2	rodič
Braunerových	Braunerových	k2eAgFnSc2d1	Braunerových
<g/>
,	,	kIx,	,
Magdaleny	Magdalena	k1gFnPc4	Magdalena
Dobromily	Dobromila	k1gFnSc2	Dobromila
Rettigové	Rettigový	k2eAgFnSc2d1	Rettigová
<g/>
,	,	kIx,	,
A.	A.	kA	A.
V.	V.	kA	V.
Šmilovského	Šmilovský	k2eAgNnSc2d1	Šmilovský
<g/>
,	,	kIx,	,
hrobka	hrobka	k1gFnSc1	hrobka
rodiny	rodina	k1gFnSc2	rodina
Fünfkirchenů	Fünfkirchen	k1gInPc2	Fünfkirchen
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
náměstí	náměstí	k1gNnSc6	náměstí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
Socha	Socha	k1gMnSc1	Socha
<g />
.	.	kIx.	.
</s>
<s>
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1764	[number]	k4	1764
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnSc1	silnice
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
Třebovou	Třebová	k1gFnSc4	Třebová
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
J.	J.	kA	J.
K.	K.	kA	K.
Jeřábka	Jeřábek	k1gMnSc2	Jeřábek
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
Pernštejnskému	pernštejnský	k2eAgInSc3d1	pernštejnský
dvoru	dvůr	k1gInSc3	dvůr
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Rektora	rektor	k1gMnSc2	rektor
Stříteského	Stříteský	k2eAgNnSc2d1	Stříteský
Sousoší	sousoší	k1gNnSc2	sousoší
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
od	od	k7c2	od
J.	J.	kA	J.
Dvořáka	Dvořák	k1gMnSc4	Dvořák
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
hřbitova	hřbitov	k1gInSc2	hřbitov
Pomník	pomník	k1gInSc1	pomník
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
od	od	k7c2	od
J.	J.	kA	J.
Štursy	Štursa	k1gFnSc2	Štursa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
a	a	k8xC	a
pomník	pomník	k1gInSc4	pomník
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
na	na	k7c6	na
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
náměstí	náměstí	k1gNnSc6	náměstí
Sochy	socha	k1gFnSc2	socha
Minervy	Minerva	k1gFnSc2	Minerva
<g/>
,	,	kIx,	,
Marty	Marta	k1gFnSc2	Marta
<g/>
,	,	kIx,	,
Apollona	Apollo	k1gMnSc2	Apollo
<g/>
,	,	kIx,	,
Ceres	ceres	k1gInSc4	ceres
<g/>
,	,	kIx,	,
Plutona	Pluton	k1gMnSc4	Pluton
a	a	k8xC	a
Diany	Diana	k1gFnPc4	Diana
<g/>
,	,	kIx,	,
zámecký	zámecký	k2eAgInSc4d1	zámecký
francouzský	francouzský	k2eAgInSc4d1	francouzský
park	park	k1gInSc4	park
Socha	socha	k1gFnSc1	socha
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
od	od	k7c2	od
V.	V.	kA	V.
Makovského	Makovského	k2eAgFnSc2d1	Makovského
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
Socha	Socha	k1gMnSc1	Socha
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
<g/>
,	,	kIx,	,
u	u	k7c2	u
1	[number]	k4	1
<g/>
.	.	kIx.	.
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
Pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Pomník	pomník	k1gInSc4	pomník
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
odboje	odboj	k1gInSc2	odboj
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
náměstí	náměstí	k1gNnSc4	náměstí
22	[number]	k4	22
Vodní	vodní	k2eAgInSc1d1	vodní
mlýn	mlýn	k1gInSc1	mlýn
konšelský	konšelský	k2eAgInSc1d1	konšelský
<g/>
,	,	kIx,	,
Mařákova	Mařákův	k2eAgFnSc1d1	Mařákova
283	[number]	k4	283
Zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
dvůr	dvůr	k1gInSc1	dvůr
Hraběnčin	hraběnčin	k2eAgInSc1d1	hraběnčin
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Partyzánská	partyzánský	k2eAgFnSc1d1	Partyzánská
322	[number]	k4	322
Kaplička	kaplička	k1gFnSc1	kaplička
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
v	v	k7c6	v
Nedošínském	Nedošínský	k2eAgInSc6d1	Nedošínský
háji	háj	k1gInSc6	háj
Židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
Městská	městský	k2eAgFnSc1d1	městská
galerie	galerie	k1gFnSc1	galerie
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
-	-	kIx~	-
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Rytířů	Rytíř	k1gMnPc2	Rytíř
110	[number]	k4	110
Regionální	regionální	k2eAgInPc1d1	regionální
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
-	-	kIx~	-
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
9	[number]	k4	9
Rodný	rodný	k2eAgInSc4d1	rodný
byt	byt	k1gInSc4	byt
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
-	-	kIx~	-
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
133	[number]	k4	133
Portmoneum	Portmoneum	k1gNnSc1	Portmoneum
-	-	kIx~	-
Museum	museum	k1gNnSc1	museum
Josefa	Josef	k1gMnSc2	Josef
Váchala	Váchal	k1gMnSc2	Váchal
-	-	kIx~	-
T.	T.	kA	T.
Novákové	Nováková	k1gFnSc2	Nováková
75	[number]	k4	75
Fakulta	fakulta	k1gFnSc1	fakulta
restaurování	restaurování	k1gNnSc2	restaurování
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
-	-	kIx~	-
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
3	[number]	k4	3
Muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Červené	Červené	k2eAgFnSc6d1	Červené
věži	věž	k1gFnSc6	věž
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
mezi	mezi	k7c7	mezi
Hradcem	Hradec	k1gInSc7	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
Svitavami	Svitava	k1gFnPc7	Svitava
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
jezdí	jezdit	k5eAaImIp3nP	jezdit
četné	četný	k2eAgFnPc1d1	četná
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
včetně	včetně	k7c2	včetně
dálkových	dálkový	k2eAgFnPc2d1	dálková
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
autobusové	autobusový	k2eAgNnSc1d1	autobusové
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
také	také	k9	také
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
Třebovou	Třebová	k1gFnSc7	Třebová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
dálkové	dálkový	k2eAgInPc4d1	dálkový
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc4d2	menší
význam	význam	k1gInSc4	význam
<g/>
;	;	kIx,	;
končí	končit	k5eAaImIp3nS	končit
zde	zde	k6eAd1	zde
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Choceň	Choceň	k1gFnSc1	Choceň
-	-	kIx~	-
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
kraje	kraj	k1gInSc2	kraj
zastavena	zastavit	k5eAaPmNgFnS	zastavit
osobní	osobní	k2eAgFnSc1d1	osobní
doprava	doprava	k1gFnSc1	doprava
<g/>
;	;	kIx,	;
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
veřejnosti	veřejnost	k1gFnSc2	veřejnost
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
zredukována	zredukován	k2eAgFnSc1d1	zredukována
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgNnSc4d1	sportovní
vyžití	vyžití	k1gNnSc4	vyžití
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
občanům	občan	k1gMnPc3	občan
města	město	k1gNnSc2	město
příspěvkové	příspěvkový	k2eAgFnSc2d1	příspěvková
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
Regionální	regionální	k2eAgNnSc1d1	regionální
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Smetanův	Smetanův	k2eAgInSc1d1	Smetanův
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
jsou	být	k5eAaImIp3nP	být
organizovány	organizován	k2eAgInPc1d1	organizován
programy	program	k1gInPc1	program
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
Domu	dům	k1gInSc6wR	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
k.	k.	k?	k.
ú.	ú.	k?	ú.
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Litomyšl-Město	Litomyšl-Města	k1gMnSc5	Litomyšl-Města
a	a	k8xC	a
Zahájí	zahájit	k5eAaPmIp3nS	zahájit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Zahájí	zahájit	k5eAaPmIp3nS	zahájit
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
část	část	k1gFnSc1	část
Litomyšl-Město	Litomyšl-Města	k1gMnSc5	Litomyšl-Města
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
<g/>
.	.	kIx.	.
k.	k.	k?	k.
ú.	ú.	k?	ú.
Záhraď	Záhraď	k1gFnSc1	Záhraď
<g/>
:	:	kIx,	:
Záhradí	Záhradý	k2eAgMnPc1d1	Záhradý
a	a	k8xC	a
Suchá	Suchá	k1gFnSc1	Suchá
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Pazucha	pazucha	k1gFnSc1	pazucha
(	(	kIx(	(
<g/>
též	též	k9	též
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nP	ležet
východně	východně	k6eAd1	východně
od	od	k7c2	od
Suché	Suché	k2eAgFnPc2d1	Suché
Nedošín	Nedošína	k1gFnPc2	Nedošína
(	(	kIx(	(
<g/>
též	též	k9	též
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nP	ležet
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g />
.	.	kIx.	.
</s>
<s>
Kornice	Kornice	k1gFnSc1	Kornice
(	(	kIx(	(
<g/>
též	též	k9	též
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nP	ležet
severoseverozápadně	severoseverozápadně	k6eAd1	severoseverozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Lány	lán	k1gInPc4	lán
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgInSc4d1	katastrální
území	území	k1gNnSc6	území
Lány	lán	k1gInPc4	lán
u	u	k7c2	u
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
od	od	k7c2	od
Zahájí	zahájit	k5eAaPmIp3nS	zahájit
Exklávu	Exkláv	k1gInSc3	Exkláv
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
oddělenou	oddělený	k2eAgFnSc7d1	oddělená
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
územím	území	k1gNnSc7	území
obcí	obec	k1gFnPc2	obec
Benátky	Benátky	k1gFnPc4	Benátky
a	a	k8xC	a
Osík	Osík	k1gInSc4	Osík
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvojice	dvojice	k1gFnSc1	dvojice
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
u	u	k7c2	u
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
(	(	kIx(	(
<g/>
též	též	k9	též
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
<g/>
)	)	kIx)	)
Pohodlí	pohodlí	k1gNnSc1	pohodlí
(	(	kIx(	(
<g/>
též	též	k9	též
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
Exkláva	Exkláva	k1gFnSc1	Exkláva
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
hlavní	hlavní	k2eAgFnSc2d1	hlavní
části	část	k1gFnSc2	část
města	město	k1gNnPc1	město
vzdálena	vzdálit	k5eAaPmNgNnP	vzdálit
asi	asi	k9	asi
0,3	[number]	k4	0,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnPc1	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc4	Jiří
u	u	k7c2	u
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velmi	velmi	k6eAd1	velmi
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
prodat	prodat	k5eAaPmF	prodat
její	její	k3xOp3gInSc4	její
zvon	zvon	k1gInSc4	zvon
do	do	k7c2	do
Lochovic	Lochovice	k1gFnPc2	Lochovice
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Lochovic	Lochovice	k1gFnPc2	Lochovice
prohrála	prohrát	k5eAaPmAgFnS	prohrát
zvon	zvon	k1gInSc4	zvon
v	v	k7c6	v
kartách	karta	k1gFnPc6	karta
s	s	k7c7	s
krumlovskými	krumlovský	k2eAgInPc7d1	krumlovský
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
zvon	zvon	k1gInSc1	zvon
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
k	k	k7c3	k
zajímavým	zajímavý	k2eAgNnPc3d1	zajímavé
místům	místo	k1gNnPc3	místo
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
okolí	okolí	k1gNnSc2	okolí
patří	patřit	k5eAaImIp3nS	patřit
Nedošínský	Nedošínský	k2eAgInSc1d1	Nedošínský
háj	háj	k1gInSc1	háj
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
díky	díky	k7c3	díky
novele	novela	k1gFnSc3	novela
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pískovcové	pískovcový	k2eAgNnSc4d1	pískovcové
skalní	skalní	k2eAgNnSc4d1	skalní
město	město	k1gNnSc4	město
Toulovcovy	Toulovcův	k2eAgFnSc2d1	Toulovcova
maštale	maštale	k?	maštale
Poutní	poutní	k2eAgFnSc3d1	poutní
soše	socha	k1gFnSc3	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Litomyšlské	litomyšlský	k2eAgFnSc2d1	Litomyšlská
z	z	k7c2	z
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
22	[number]	k4	22
<g/>
.	.	kIx.	.
kaple	kaple	k1gFnSc2	kaple
Svaté	svatý	k2eAgFnSc2d1	svatá
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Staré	Stará	k1gFnSc2	Stará
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1674	[number]	k4	1674
-	-	kIx~	-
1690	[number]	k4	1690
<g/>
.	.	kIx.	.
</s>
<s>
Donátorem	donátor	k1gMnSc7	donátor
této	tento	k3xDgFnSc2	tento
kaple	kaple	k1gFnSc2	kaple
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Trauttmansdorffu	Trauttmansdorff	k1gInSc2	Trauttmansdorff
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
komorník	komorník	k1gMnSc1	komorník
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
August	August	k1gMnSc1	August
Brauner	Brauner	k1gMnSc1	Brauner
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1880	[number]	k4	1880
Roztoky	roztoka	k1gFnSc2	roztoka
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
nám.	nám.	k?	nám.
čp.	čp.	k?	čp.
27	[number]	k4	27
-	-	kIx~	-
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Faltejsek	Faltejsek	k1gMnSc1	Faltejsek
<g/>
)	)	kIx)	)
a	a	k8xC	a
textem	text	k1gInSc7	text
Zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1839	[number]	k4	1839
až	až	k9	až
1840	[number]	k4	1840
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
1820	[number]	k4	1820
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1884	[number]	k4	1884
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Antonín	Antonín	k1gMnSc1	Antonín
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
1866	[number]	k4	1866
USA	USA	kA	USA
<g/>
,	,	kIx,	,
tiskař	tiskař	k1gMnSc1	tiskař
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
spisů	spis	k1gInPc2	spis
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgMnSc1d1	Němcové
<g/>
)	)	kIx)	)
Julius	Julius	k1gMnSc1	Julius
Mařák	Mařák	k1gMnSc1	Mařák
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
Alois	Alois	k1gMnSc1	Alois
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šmilovský	Šmilovský	k2eAgMnSc1d1	Šmilovský
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
gymnazijní	gymnazijní	k2eAgMnSc1d1	gymnazijní
profesor	profesor	k1gMnSc1	profesor
Karel	Karel	k1gMnSc1	Karel
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
zahradnický	zahradnický	k2eAgMnSc1d1	zahradnický
odborník	odborník	k1gMnSc1	odborník
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
ovocnář	ovocnář	k1gMnSc1	ovocnář
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
Pomologického	pomologický	k2eAgInSc2d1	pomologický
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kořenský	Kořenský	k2eAgMnSc1d1	Kořenský
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karel	Karel	k1gMnSc1	Karel
Zahradník	Zahradník	k1gMnSc1	Zahradník
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
Ladislav	Ladislav	k1gMnSc1	Ladislav
Prokop	Prokop	k1gMnSc1	Prokop
Procházka	Procházka	k1gMnSc1	Procházka
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
muzikolog	muzikolog	k1gMnSc1	muzikolog
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
veřejný	veřejný	k2eAgMnSc1d1	veřejný
činitel	činitel	k1gMnSc1	činitel
Kvido	Kvido	k1gMnSc1	Kvido
Hodura	Hodura	k1gFnSc1	Hodura
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g />
.	.	kIx.	.
</s>
<s>
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
a	a	k8xC	a
germanista	germanista	k1gMnSc1	germanista
Eliška	Eliška	k1gFnSc1	Eliška
Klimková-Deutschová	Klimková-Deutschová	k1gFnSc1	Klimková-Deutschová
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
lékařka	lékařka	k1gFnSc1	lékařka
a	a	k8xC	a
profesorka	profesorka	k1gFnSc1	profesorka
neurologie	neurologie	k1gFnSc2	neurologie
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kopal	Kopal	k1gMnSc1	Kopal
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
František	František	k1gMnSc1	František
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
(	(	kIx(	(
<g/>
*	*	kIx~	*
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
archivář	archivář	k1gMnSc1	archivář
Olbram	Olbram	k1gInSc1	Olbram
Zoubek	Zoubek	k1gMnSc1	Zoubek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Josef	Josef	k1gMnSc1	Josef
Špak	Špak	k1gMnSc1	Špak
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup-patriarcha	biskupatriarcha	k1gMnSc1	biskup-patriarcha
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
I.	I.	kA	I.
<g />
.	.	kIx.	.
</s>
<s>
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
II	II	kA	II
<g/>
.	.	kIx.	.
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
III	III	kA	III
<g/>
.	.	kIx.	.
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
496	[number]	k4	496
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
U	u	k7c2	u
Školek	školka	k1gFnPc2	školka
1117	[number]	k4	1117
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
1145	[number]	k4	1145
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Mozaika	mozaika	k1gFnSc1	mozaika
<g/>
,	,	kIx,	,
o.p.s.	o.p.s.	k?	o.p.s.
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
pracoviště	pracoviště	k1gNnSc4	pracoviště
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
Speciální	speciální	k2eAgFnSc1d1	speciální
<g />
.	.	kIx.	.
</s>
<s>
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
Gymnázium	gymnázium	k1gNnSc1	gymnázium
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
a	a	k8xC	a
jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
Trading	Trading	k1gInSc1	Trading
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
škola	škola	k1gFnSc1	škola
Fakulta	fakulta	k1gFnSc1	fakulta
restaurování	restaurování	k1gNnSc1	restaurování
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
Levoča	Levoča	k1gFnSc1	Levoča
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Roden	Rodna	k1gFnPc2	Rodna
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
San	San	k1gFnPc2	San
Polo	polo	k6eAd1	polo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Enza	Enza	k1gMnSc1	Enza
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
