<s>
Významným	významný	k2eAgNnSc7d1	významné
datem	datum	k1gNnSc7	datum
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
1339	[number]	k4	1339
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Telč	Telč	k1gFnSc4	Telč
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Oldřich	Oldřich	k1gMnSc1	Oldřich
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
<g/>
.	.	kIx.	.
</s>
