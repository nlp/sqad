<s>
Nivchština	Nivchština	k1gFnSc1	Nivchština
nebo	nebo	k8xC	nebo
též	též	k9	též
giljačtina	giljačtina	k1gFnSc1	giljačtina
(	(	kIx(	(
<g/>
nivchsky	nivchsky	k6eAd1	nivchsky
<g/>
:	:	kIx,	:
Н	Н	k?	Н
д	д	k?	д
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
ニ	ニ	k?	ニ
<g/>
/	/	kIx~	/
<g/>
ギ	ギ	k?	ギ
<g/>
,	,	kIx,	,
nivufu-go	nivufuo	k1gMnSc1	nivufu-go
<g/>
/	/	kIx~	/
<g/>
girijaku-go	girijakuo	k1gMnSc1	girijaku-go
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osamocený	osamocený	k2eAgInSc1d1	osamocený
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
mluví	mluvit	k5eAaImIp3nS	mluvit
zhruba	zhruba	k6eAd1	zhruba
1000	[number]	k4	1000
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sachalin	Sachalin	k1gInSc1	Sachalin
a	a	k8xC	a
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
řeky	řeka	k1gFnSc2	řeka
Amur	Amur	k1gInSc1	Amur
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
psán	psát	k5eAaImNgInS	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
500	[number]	k4	500
Nivchů	Nivcha	k1gMnPc2	Nivcha
mluví	mluvit	k5eAaImIp3nS	mluvit
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
přejali	přejmout	k5eAaPmAgMnP	přejmout
jako	jako	k8xC	jako
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
ruštinu	ruština	k1gFnSc4	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řazen	k2eAgInSc1d1	řazen
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
paleoasijské	paleoasijský	k2eAgInPc4d1	paleoasijský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
bez	bez	k7c2	bez
prokázané	prokázaný	k2eAgFnSc2d1	prokázaná
genetické	genetický	k2eAgFnSc2d1	genetická
příbuznosti	příbuznost	k1gFnSc2	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
jazyky	jazyk	k1gInPc7	jazyk
nebyl	být	k5eNaImAgMnS	být
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
doložen	doložen	k2eAgInSc4d1	doložen
a	a	k8xC	a
nivchštinu	nivchština	k1gFnSc4	nivchština
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutno	nutno	k6eAd1	nutno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
příbuznost	příbuznost	k1gFnSc1	příbuznost
byla	být	k5eAaImAgFnS	být
hledána	hledat	k5eAaImNgFnS	hledat
například	například	k6eAd1	například
u	u	k7c2	u
ainštiny	ainština	k1gFnSc2	ainština
<g/>
,	,	kIx,	,
japonštiny	japonština	k1gFnSc2	japonština
<g/>
,	,	kIx,	,
altajských	altajský	k2eAgInPc2d1	altajský
jazyků	jazyk	k1gInPc2	jazyk
či	či	k8xC	či
ostatních	ostatní	k2eAgInPc2d1	ostatní
paleoasijských	paleoasijský	k2eAgInPc2d1	paleoasijský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Lingvista	lingvista	k1gMnSc1	lingvista
Joseph	Joseph	k1gMnSc1	Joseph
Greenberg	Greenberg	k1gMnSc1	Greenberg
řadil	řadit	k5eAaImAgMnS	řadit
nivchštinu	nivchština	k1gFnSc4	nivchština
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
euroasijské	euroasijský	k2eAgInPc4d1	euroasijský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
pojatá	pojatý	k2eAgFnSc1d1	pojatá
rodina	rodina	k1gFnSc1	rodina
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
evropské	evropský	k2eAgFnSc2d1	Evropská
a	a	k8xC	a
asijské	asijský	k2eAgInPc1d1	asijský
jazyky	jazyk	k1gInPc1	jazyk
od	od	k7c2	od
indoevropských	indoevropský	k2eAgFnPc2d1	indoevropská
po	po	k7c4	po
čukotsko-kamčatské	čukotskoamčatský	k2eAgFnPc4d1	čukotsko-kamčatský
<g/>
.	.	kIx.	.
</s>
