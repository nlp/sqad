<s>
Měsíční	měsíční	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
Kontrastní	kontrastní	k2eAgInPc1d1
barevné	barevný	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
měsíčními	měsíční	k2eAgNnPc7d1
moři	moře	k1gNnPc7
(	(	kIx(
<g/>
tmavé	tmavý	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
ostatním	ostatní	k2eAgInSc7d1
terénem	terén	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Měsíční	měsíční	k2eAgNnSc4d1
moře	moře	k1gNnSc4
či	či	k8xC
latinsky	latinsky	k6eAd1
mare	mare	k6eAd1
jsou	být	k5eAaImIp3nP
opticky	opticky	k6eAd1
tmavší	tmavý	k2eAgFnPc1d2
oblasti	oblast	k1gFnPc1
na	na	k7c6
povrchu	povrch	k1gInSc6
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
převážně	převážně	k6eAd1
na	na	k7c6
přivrácené	přivrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
mladší	mladý	k2eAgFnPc4d2
hladké	hladký	k2eAgFnPc4d1
planiny	planina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
nejspíše	nejspíše	k9
vlivem	vlivem	k7c2
impaktů	impakt	k1gInPc2
obřích	obří	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
a	a	k8xC
proražením	proražení	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
kůry	kůra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
povrch	povrch	k1gInSc4
vyvalilo	vyvalit	k5eAaPmAgNnS
obrovské	obrovský	k2eAgNnSc1d1
množství	množství	k1gNnSc1
lávy	láva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zalila	zalít	k5eAaPmAgFnS
starší	starý	k2eAgInPc4d2
morfologické	morfologický	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
utuhla	utuhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horniny	hornina	k1gFnSc2
v	v	k7c6
těchto	tento	k3xDgFnPc6
oblastech	oblast	k1gFnPc6
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
bazalty	bazalt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Měsíční	měsíční	k2eAgNnSc4d1
moře	moře	k1gNnSc4
jsou	být	k5eAaImIp3nP
rovinatá	rovinatý	k2eAgFnSc1d1
s	s	k7c7
občasnými	občasný	k2eAgInPc7d1
zvlněnými	zvlněný	k2eAgInPc7d1
nízkými	nízký	k2eAgInPc7d1
hřbety	hřbet	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zdát	zdát	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
mořích	moře	k1gNnPc6
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
mnoho	mnoho	k4c1
kráterů	kráter	k1gInPc2
<g/>
,	,	kIx,
opak	opak	k1gInSc1
je	být	k5eAaImIp3nS
pravdou	pravda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
jejich	jejich	k3xOp3gInSc1
povrch	povrch	k1gInSc1
je	být	k5eAaImIp3nS
poset	poset	k2eAgInSc4d1
krátery	kráter	k1gInPc4
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
nevelkých	velký	k2eNgInPc2d1
rozměrů	rozměr	k1gInPc2
(	(	kIx(
<g/>
řádově	řádově	k6eAd1
metry	metr	k1gInPc4
i	i	k8xC
méně	málo	k6eAd2
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
jsou	být	k5eAaImIp3nP
mikroskopické	mikroskopický	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moře	moře	k1gNnSc2
pokrývají	pokrývat	k5eAaImIp3nP
31,2	31,2	k4
%	%	kIx~
povrchu	povrch	k1gInSc2
přivrácené	přivrácený	k2eAgInPc4d1
a	a	k8xC
pouze	pouze	k6eAd1
2,6	2,6	k4
%	%	kIx~
povrchu	povrch	k1gInSc2
odvrácené	odvrácený	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
okrajů	okraj	k1gInPc2
některých	některý	k3yIgInPc2
moří	mořit	k5eAaImIp3nS
byly	být	k5eAaImAgFnP
zaznamenány	zaznamenat	k5eAaPmNgInP
tzv.	tzv.	kA
měsíční	měsíční	k2eAgInPc4d1
přechodné	přechodný	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tmavá	tmavý	k2eAgNnPc1d1
moře	moře	k1gNnPc1
jsou	být	k5eAaImIp3nP
pozorovatelná	pozorovatelný	k2eAgNnPc1d1
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
již	již	k6eAd1
od	od	k7c2
pradávna	pradávno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
si	se	k3xPyFc3
původně	původně	k6eAd1
mysleli	myslet	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
Zemi	zem	k1gFnSc6
o	o	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
kapalná	kapalný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byly	být	k5eAaImAgInP
pojmenovány	pojmenovat	k5eAaPmNgInP
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k8xS,k8xC
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgInPc1d2
průzkumy	průzkum	k1gInPc1
teleskopy	teleskop	k1gInPc7
a	a	k8xC
sondami	sonda	k1gFnPc7
definitivně	definitivně	k6eAd1
tuto	tento	k3xDgFnSc4
chybnou	chybný	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
vyvrátily	vyvrátit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nP
dvousloví	dvousloví	k1gNnSc1
jako	jako	k8xS,k8xC
např.	např.	kA
Mare	Mare	k1gFnSc1
Tranquillitatis	Tranquillitatis	k1gFnSc1
(	(	kIx(
<g/>
Moře	moře	k1gNnSc1
klidu	klid	k1gInSc2
<g/>
)	)	kIx)
či	či	k8xC
Sinus	sinus	k1gInSc1
Roris	Roris	k1gFnSc2
(	(	kIx(
<g/>
Záliv	záliv	k1gInSc1
rosy	rosa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Povrch	povrch	k7c2wR
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Rükl	Rükl	k1gMnSc1
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
Aventinum	Aventinum	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
Pozorování	pozorování	k1gNnSc2
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
211	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85277-10-7	80-85277-10-7	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
moří	moře	k1gNnPc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
měsíční	měsíční	k2eAgFnSc2d1
moře	moře	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měsíční	měsíční	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Moře	moře	k1gNnSc2
</s>
<s>
Mare	Mare	k1gFnSc1
Anguis	Anguis	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Marus	k1gMnSc5
Australe	Austral	k1gMnSc5
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Cognitum	Cognitum	k1gNnSc1
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Crisium	Crisium	k1gNnSc1
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Fecunditatis	Fecunditatis	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Frigoris	Frigoris	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Humboldtianum	Humboldtianum	k1gInSc1
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Humorum	Humorum	k1gInSc1
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Imbrium	Imbrium	k1gNnSc1
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Ingenii	ingenium	k1gNnPc7
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Insularum	Insularum	k1gInSc1
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Marginis	Marginis	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Moscoviense	Moscoviense	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Nectaris	Nectaris	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Nubium	Nubium	k1gNnSc1
•	•	k?
</s>
<s>
Mare	Marus	k1gMnSc5
Orientale	Oriental	k1gMnSc5
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Serenitatis	Serenitatis	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Marat	k5eAaPmIp3nS
Smythii	Smythie	k1gFnSc4
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Spumans	Spumans	k1gInSc1
•	•	k?
</s>
<s>
Mare	Mare	k1gFnSc1
Tranquillitatis	Tranquillitatis	k1gFnSc2
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Undarum	Undarum	k1gInSc1
•	•	k?
</s>
<s>
Mare	Mare	k6eAd1
Vaporum	Vaporum	k1gInSc1
</s>
<s>
Oceán	oceán	k1gInSc1
</s>
<s>
Oceanus	Oceanus	k1gInSc1
Procellarum	Procellarum	k1gInSc1
</s>
<s>
Jezera	jezero	k1gNnPc1
</s>
<s>
Lacus	Lacus	k1gMnSc1
Aestatis	Aestatis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Autumni	Autumni	k1gMnSc1
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Bonitatis	Bonitatis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Doloris	Doloris	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Excellentiae	Excellentia	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Felicitatis	Felicitatis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Gaudii	gaudium	k1gNnPc7
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Hiemalis	Hiemalis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Lenitatis	Lenitatis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Luxuriae	Luxuria	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Mortis	Mortis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Oblivionis	Oblivionis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Odii	Odi	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Perseverantiae	Perseverantia	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Solitudinis	Solitudinis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gInSc1
Somniorum	Somniorum	k1gInSc1
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Spei	Spe	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Temporis	Temporis	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gMnSc1
Timoris	Timoris	k1gFnSc2
•	•	k?
</s>
<s>
Lacus	Lacus	k1gInSc1
Veris	Veris	k1gFnSc2
</s>
<s>
Zálivy	záliv	k1gInPc1
</s>
<s>
Sinus	sinus	k1gInSc1
Aestuum	Aestuum	k1gInSc1
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Amoris	Amoris	k1gFnSc2
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Asperitatis	Asperitatis	k1gFnSc2
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Concordiae	Concordia	k1gFnSc2
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Fidei	Fide	k1gFnSc2
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Honoris	Honoris	k1gFnSc2
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Iridum	Iridum	k1gInSc1
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Lunicus	Lunicus	k1gInSc1
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Medii	medium	k1gNnPc7
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Roris	Roris	k1gFnSc2
•	•	k?
</s>
<s>
Sinus	sinus	k1gInSc1
Successus	Successus	k1gInSc1
</s>
<s>
Bažiny	bažina	k1gFnPc1
</s>
<s>
Palus	Palus	k1gInSc1
Epidemiarum	Epidemiarum	k1gInSc1
•	•	k?
</s>
<s>
Palus	Palus	k1gMnSc1
Putredinis	Putredinis	k1gFnSc2
•	•	k?
</s>
<s>
Palus	Palus	k1gInSc4
Somni	Somn	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4371662-3	4371662-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85078852	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85078852	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Měsíc	měsíc	k1gInSc1
</s>
