<s>
Měsíční	měsíční	k2eAgNnSc4d1
moře	moře	k1gNnSc4
či	či	k8xC
latinsky	latinsky	k6eAd1
mare	mare	k6eAd1
jsou	být	k5eAaImIp3nP
opticky	opticky	k6eAd1
tmavší	tmavý	k2eAgFnPc1d2
oblasti	oblast	k1gFnPc1
na	na	k7c6
povrchu	povrch	k1gInSc6
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
převážně	převážně	k6eAd1
na	na	k7c6
přivrácené	přivrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>