<s>
Událost	událost	k1gFnSc1	událost
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k9	jako
Velký	velký	k2eAgInSc4d1	velký
mor	mor	k1gInSc4	mor
byla	být	k5eAaImAgFnS	být
epidemie	epidemie	k1gFnSc1	epidemie
moru	mora	k1gFnSc4	mora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
až	až	k9	až
1666	[number]	k4	1666
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
zahubila	zahubit	k5eAaPmAgFnS	zahubit
asi	asi	k9	asi
70	[number]	k4	70
000	[number]	k4	000
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
jednu	jeden	k4xCgFnSc4	jeden
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
