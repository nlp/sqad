<s>
Událost	událost	k1gFnSc1	událost
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k9	jako
Velký	velký	k2eAgInSc4d1	velký
mor	mor	k1gInSc4	mor
byla	být	k5eAaImAgFnS	být
epidemie	epidemie	k1gFnSc1	epidemie
moru	mora	k1gFnSc4	mora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
až	až	k9	až
1666	[number]	k4	1666
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
zahubila	zahubit	k5eAaPmAgFnS	zahubit
asi	asi	k9	asi
70	[number]	k4	70
000	[number]	k4	000
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
jednu	jeden	k4xCgFnSc4	jeden
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
menšího	malý	k2eAgInSc2d2	menší
rozsahu	rozsah	k1gInSc2	rozsah
než	než	k8xS	než
dřívější	dřívější	k2eAgFnSc1d1	dřívější
černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
masivní	masivní	k2eAgFnSc1d1	masivní
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
Evropu	Evropa	k1gFnSc4	Evropa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1347	[number]	k4	1347
až	až	k9	až
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
přívlastkem	přívlastek	k1gInSc7	přívlastek
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
výskytu	výskyt	k1gInSc2	výskyt
moru	mor	k1gInSc2	mor
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
dostala	dostat	k5eAaPmAgFnS	dostat
holandskou	holandský	k2eAgFnSc7d1	holandská
obchodní	obchodní	k2eAgFnSc7d1	obchodní
lodí	loď	k1gFnSc7	loď
dopravující	dopravující	k2eAgFnSc4d1	dopravující
bavlnu	bavlna	k1gFnSc4	bavlna
z	z	k7c2	z
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výskyt	výskyt	k1gInSc1	výskyt
moru	mor	k1gInSc2	mor
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
doků	dok	k1gInPc2	dok
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
1664	[number]	k4	1664
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
několik	několik	k4yIc1	několik
případů	případ	k1gInPc2	případ
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
studená	studený	k2eAgFnSc1d1	studená
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nákaza	nákaza	k1gFnSc1	nákaza
příliš	příliš	k6eAd1	příliš
nešířila	šířit	k5eNaImAgFnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jaro	jaro	k1gNnSc1	jaro
a	a	k8xC	a
léto	léto	k1gNnSc1	léto
bylo	být	k5eAaImAgNnS	být
neobvykle	obvykle	k6eNd1	obvykle
horké	horký	k2eAgNnSc1d1	horké
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
sílu	síla	k1gFnSc4	síla
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zaznamenanou	zaznamenaný	k2eAgFnSc7d1	zaznamenaná
obětí	oběť	k1gFnSc7	oběť
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1665	[number]	k4	1665
Margaret	Margareta	k1gFnPc2	Margareta
Porteousová	Porteousový	k2eAgFnSc1d1	Porteousový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1665	[number]	k4	1665
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
mor	mor	k1gInSc4	mor
vlastní	vlastní	k2eAgNnPc4d1	vlastní
město	město	k1gNnSc4	město
a	a	k8xC	a
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
dvorem	dvůr	k1gInSc7	dvůr
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
a	a	k8xC	a
konšelé	konšel	k1gMnPc1	konšel
setrvali	setrvat	k5eAaPmAgMnP	setrvat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
funkcích	funkce	k1gFnPc6	funkce
a	a	k8xC	a
řídili	řídit	k5eAaImAgMnP	řídit
chod	chod	k1gInSc4	chod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
řemeslníci	řemeslník	k1gMnPc1	řemeslník
utekli	utéct	k5eAaPmAgMnP	utéct
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zavřeny	zavřít	k5eAaPmNgInP	zavřít
i	i	k9	i
obchody	obchod	k1gInPc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
lékárníků	lékárník	k1gMnPc2	lékárník
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podniknuta	podniknout	k5eAaPmNgFnS	podniknout
některá	některý	k3yIgNnPc4	některý
opatření	opatření	k1gNnPc4	opatření
pro	pro	k7c4	pro
zmírnění	zmírnění	k1gNnSc4	zmírnění
důsledků	důsledek	k1gInPc2	důsledek
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
byli	být	k5eAaImAgMnP	být
placeni	platit	k5eAaImNgMnP	platit
z	z	k7c2	z
městských	městský	k2eAgInPc2d1	městský
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
pohřby	pohřeb	k1gInPc1	pohřeb
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
pečlivě	pečlivě	k6eAd1	pečlivě
organizovány	organizovat	k5eAaBmNgInP	organizovat
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
nařídily	nařídit	k5eAaPmAgInP	nařídit
trvale	trvale	k6eAd1	trvale
udržovat	udržovat	k5eAaImF	udržovat
ohně	oheň	k1gInPc4	oheň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dne	den	k1gInSc2	den
i	i	k8xC	i
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
povětří	povětří	k1gNnSc1	povětří
pročistilo	pročistit	k5eAaPmAgNnS	pročistit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
byly	být	k5eAaImAgFnP	být
páleny	pálen	k2eAgFnPc1d1	pálena
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vydávají	vydávat	k5eAaImIp3nP	vydávat
silnou	silný	k2eAgFnSc4d1	silná
vůni	vůně	k1gFnSc4	vůně
(	(	kIx(	(
<g/>
pepř	pepř	k1gInSc1	pepř
<g/>
,	,	kIx,	,
chmel	chmel	k1gInSc1	chmel
nebo	nebo	k8xC	nebo
kadidlo	kadidlo	k1gNnSc1	kadidlo
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
nákazou	nákaza	k1gFnSc7	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Londýňanům	Londýňan	k1gMnPc3	Londýňan
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
důrazně	důrazně	k6eAd1	důrazně
doporučováno	doporučován	k2eAgNnSc1d1	doporučováno
kouření	kouření	k1gNnSc1	kouření
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
největší	veliký	k2eAgInSc1d3	veliký
výskyt	výskyt	k1gInSc1	výskyt
moru	mor	k1gInSc2	mor
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
epidemie	epidemie	k1gFnSc1	epidemie
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
části	část	k1gFnPc4	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
byla	být	k5eAaImAgFnS	být
vesnice	vesnice	k1gFnSc1	vesnice
Eyam	Eyama	k1gFnPc2	Eyama
v	v	k7c6	v
Derbyshire	Derbyshir	k1gInSc5	Derbyshir
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
z	z	k7c2	z
balíku	balík	k1gInSc2	balík
se	s	k7c7	s
šatstvem	šatstvo	k1gNnSc7	šatstvo
poslaným	poslaný	k2eAgFnPc3d1	poslaná
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vesničané	vesničan	k1gMnPc1	vesničan
udržovali	udržovat	k5eAaImAgMnP	udržovat
karanténu	karanténa	k1gFnSc4	karanténa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neroznášeli	roznášet	k5eNaImAgMnP	roznášet
nákazu	nákaza	k1gFnSc4	nákaza
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mimo	mimo	k7c4	mimo
vesnici	vesnice	k1gFnSc4	vesnice
nerozšířil	rozšířit	k5eNaPmAgMnS	rozšířit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
úmrtí	úmrtí	k1gNnSc2	úmrtí
poloviny	polovina	k1gFnSc2	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
stoupal	stoupat	k5eAaImAgInS	stoupat
z	z	k7c2	z
1	[number]	k4	1
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
k	k	k7c3	k
2	[number]	k4	2
000	[number]	k4	000
za	za	k7c4	za
týden	týden	k1gInSc4	týden
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
1665	[number]	k4	1665
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počtu	počet	k1gInSc2	počet
7	[number]	k4	7
000	[number]	k4	000
úmrtí	úmrtí	k1gNnPc2	úmrtí
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
podzimu	podzim	k1gInSc2	podzim
se	se	k3xPyFc4	se
síla	síla	k1gFnSc1	síla
epidemie	epidemie	k1gFnSc1	epidemie
snížila	snížit	k5eAaPmAgFnS	snížit
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1666	[number]	k4	1666
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
natolik	natolik	k6eAd1	natolik
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
blízkými	blízký	k2eAgMnPc7d1	blízký
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
obnovovat	obnovovat	k5eAaImF	obnovovat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nákaza	nákaza	k1gFnSc1	nákaza
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
následující	následující	k2eAgFnSc4d1	následující
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
moru	mor	k1gInSc2	mor
postupně	postupně	k6eAd1	postupně
slábl	slábnout	k5eAaImAgInS	slábnout
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
1666	[number]	k4	1666
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
září	září	k1gNnSc6	září
1666	[number]	k4	1666
zničil	zničit	k5eAaPmAgMnS	zničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
hustě	hustě	k6eAd1	hustě
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
zastavila	zastavit	k5eAaPmAgFnS	zastavit
šíření	šíření	k1gNnSc4	šíření
nákazy	nákaza	k1gFnSc2	nákaza
zřejmě	zřejmě	k6eAd1	zřejmě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
usmrceny	usmrcen	k2eAgFnPc1d1	usmrcena
krysy	krysa	k1gFnPc1	krysa
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
blechy	blecha	k1gFnPc1	blecha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
roznášely	roznášet	k5eAaImAgFnP	roznášet
mor	mor	k1gInSc4	mor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
rekonstruován	rekonstruovat	k5eAaBmNgInS	rekonstruovat
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
širší	široký	k2eAgFnPc1d2	širší
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
snížena	snížen	k2eAgFnSc1d1	snížena
hustota	hustota	k1gFnSc1	hustota
obydlení	obydlení	k1gNnSc2	obydlení
a	a	k8xC	a
položeny	položen	k2eAgInPc4d1	položen
základy	základ	k1gInPc4	základ
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Doškové	Doškové	k2eAgFnPc1d1	Doškové
střechy	střecha	k1gFnPc1	střecha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
skvělé	skvělý	k2eAgFnPc4d1	skvělá
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
krysy	krysa	k1gFnPc4	krysa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zakázány	zakázat	k5eAaPmNgInP	zakázat
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
zákaz	zákaz	k1gInSc1	zákaz
platí	platit	k5eAaImIp3nS	platit
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
použití	použití	k1gNnSc3	použití
doškové	doškový	k2eAgFnSc2d1	došková
střechy	střecha	k1gFnSc2	střecha
pří	přít	k5eAaImIp3nS	přít
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
divadla	divadlo	k1gNnSc2	divadlo
Globe	globus	k1gInSc5	globus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
speciální	speciální	k2eAgNnPc4d1	speciální
povolení	povolení	k1gNnPc4	povolení
<g/>
.	.	kIx.	.
</s>
