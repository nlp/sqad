<s>
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
JerGym	JerGym	k1gInSc1
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Zřizovatel	zřizovatel	k1gMnSc1
</s>
<s>
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1932	#num#	k4
IČO	IČO	kA
</s>
<s>
46748075	#num#	k4
REDIZO	REDIZO	kA
</s>
<s>
600010589	#num#	k4
Ředitel	ředitel	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
Zástupce	zástupce	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Jiří	Jiří	k1gMnSc1
Vydra	Vydra	k1gMnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
425	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
460	#num#	k4
07	#num#	k4
Liberec	Liberec	k1gInSc1
7	#num#	k4
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Počet	počet	k1gInSc1
žáků	žák	k1gMnPc2
</s>
<s>
750	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
http://www.jergym.cz	http://www.jergym.cz	k1gInSc1
Obory	obora	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc2
</s>
<s>
7941K401	7941K401	k4
Gymnázium	gymnázium	k1gNnSc1
–	–	k?
čtyřleté	čtyřletý	k2eAgFnSc2d1
všeobecné	všeobecný	k2eAgFnSc2d1
<g/>
7941	#num#	k4
<g/>
K	k	k7c3
<g/>
801	#num#	k4
Gymnázium	gymnázium	k1gNnSc1
-	-	kIx~
osmileté	osmiletý	k2eAgFnSc2d1
všeobecné	všeobecný	k2eAgFnSc2d1
<g/>
7531	#num#	k4
<g/>
M	M	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
Střední	střední	k2eAgFnSc1d1
a	a	k8xC
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
<g/>
7842	#num#	k4
<g/>
M	M	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
Pedagogické	pedagogický	k2eAgNnSc4d1
lyceum	lyceum	k1gNnSc4
-	-	kIx~
čtyřleté	čtyřletý	k2eAgMnPc4d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
SOŠPg	SOŠPg	kA
Jeronýmova	Jeronýmův	k2eAgMnSc2d1
je	být	k5eAaImIp3nS
všeobecné	všeobecný	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
se	s	k7c7
čtyřletým	čtyřletý	k2eAgNnSc7d1
a	a	k8xC
osmiletým	osmiletý	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
a	a	k8xC
střední	střední	k2eAgFnSc7d1
odbornou	odborný	k2eAgFnSc7d1
školou	škola	k1gFnSc7
pedagogickou	pedagogický	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
gymnázia	gymnázium	k1gNnSc2
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1933	#num#	k4
v	v	k7c6
Jeronýmově	Jeronýmův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Horní	horní	k2eAgInSc4d1
Růžodol	růžodol	k1gInSc4
Josefem	Josef	k1gMnSc7
Votočkem	Votoček	k1gMnSc7
<g/>
,	,	kIx,
prvním	první	k4xOgMnSc7
českým	český	k2eAgMnSc7d1
starostou	starosta	k1gMnSc7
Horního	horní	k2eAgInSc2d1
Růžodolu	růžodol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Slavnost	slavnost	k1gFnSc1
položení	položení	k1gNnSc2
základního	základní	k2eAgInSc2d1
kamene	kámen	k1gInSc2
školy	škola	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
staviteli	stavitel	k1gMnPc7
Antonínem	Antonín	k1gMnSc7
Vašátkem	Vašátko	k1gMnSc7
z	z	k7c2
Velkých	velký	k2eAgInPc2d1
Hamrů	Hamry	k1gInPc2
a	a	k8xC
Josefem	Josef	k1gMnSc7
Frýdou	Frýda	k1gMnSc7
z	z	k7c2
Jilemnice	Jilemnice	k1gFnSc2
podle	podle	k7c2
projektu	projekt	k1gInSc2
městského	městský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Františka	František	k1gMnSc2
Cuce	Cuc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
den	den	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
od	od	k7c2
Votočků	Votoček	k1gMnPc2
mohutný	mohutný	k2eAgInSc4d1
průvod	průvod	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
prošel	projít	k5eAaPmAgInS
celým	celý	k2eAgNnSc7d1
městem	město	k1gNnSc7
až	až	k8xS
na	na	k7c4
staveniště	staveniště	k1gNnSc4
školy	škola	k1gFnSc2
kde	kde	k6eAd1
starosta	starosta	k1gMnSc1
Josef	Josef	k1gMnSc1
Votoček	Votočka	k1gFnPc2
pronesl	pronést	k5eAaPmAgMnS
slavnostní	slavnostní	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
školy	škola	k1gFnSc2
byla	být	k5eAaImAgFnS
slavnostně	slavnostně	k6eAd1
otevřena	otevřít	k5eAaPmNgFnS
o	o	k7c6
více	hodně	k6eAd2
než	než	k8xS
rok	rok	k1gInSc1
později	pozdě	k6eAd2
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1933	#num#	k4
<g/>
,	,	kIx,
opět	opět	k6eAd1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
průvod	průvod	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
tentokrát	tentokrát	k6eAd1
zastavil	zastavit	k5eAaPmAgMnS
u	u	k7c2
hrobu	hrob	k1gInSc2
Josefa	Josefa	k1gFnSc1
Votočka	Votočka	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
otevření	otevření	k1gNnSc2
školy	škola	k1gFnSc2
nedožil	dožít	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
škole	škola	k1gFnSc6
byly	být	k5eAaImAgFnP
instalovány	instalován	k2eAgFnPc4d1
bronzové	bronzový	k2eAgFnPc4d1
pamětní	pamětní	k2eAgFnPc4d1
desky	deska	k1gFnPc4
s	s	k7c7
nápisy	nápis	k1gInPc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Postaveno	postaven	k2eAgNnSc4d1
za	za	k7c2
prvního	první	k4xOgMnSc2
prezidenta	prezident	k1gMnSc2
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
a	a	k8xC
ministra	ministr	k1gMnSc2
školství	školství	k1gNnSc2
a	a	k8xC
národní	národní	k2eAgFnSc2d1
osvěty	osvěta	k1gFnSc2
Ivana	Ivan	k1gMnSc2
Dérera	Dérer	k1gMnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Památce	památka	k1gFnSc3
Josefa	Josefa	k1gFnSc1
Votočka	Votočka	k1gFnSc1
<g/>
,	,	kIx,
prvního	první	k4xOgMnSc2
českého	český	k2eAgMnSc2d1
starosty	starosta	k1gMnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
ostatních	ostatní	k2eAgMnPc2d1
průkopníků	průkopník	k1gMnPc2
českého	český	k2eAgNnSc2d1
školství	školství	k1gNnSc2
v	v	k7c6
Horním	horní	k2eAgInSc6d1
Růžodole	růžodol	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budově	budova	k1gFnSc6
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
Masarykova	Masarykův	k2eAgFnSc1d1
obecná	obecná	k1gFnSc1
a	a	k8xC
měšťanská	měšťanský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
školy	škola	k1gFnSc2
přestěhovalo	přestěhovat	k5eAaPmAgNnS
České	český	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
z	z	k7c2
Hálkovy	Hálkův	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
<g/>
,	,	kIx,
změněné	změněný	k2eAgFnPc4d1
reformou	reforma	k1gFnSc7
na	na	k7c4
jedenáctiletou	jedenáctiletý	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
pod	pod	k7c7
názvem	název	k1gInSc7
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnSc1d1
všeobecně	všeobecně	k6eAd1
vzdělávací	vzdělávací	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
zde	zde	k6eAd1
zůstalo	zůstat	k5eAaPmAgNnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
Střední	střední	k2eAgFnSc1d1
pedagogická	pedagogický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1975	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
se	se	k3xPyFc4
škola	škola	k1gFnSc1
vyskytovala	vyskytovat	k5eAaImAgFnS
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
vytvářela	vytvářet	k5eAaImAgFnS
struktura	struktura	k1gFnSc1
školy	škola	k1gFnSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
škola	škola	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
školní	školní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
v	v	k7c6
Jeronýmově	Jeronýmův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
bylo	být	k5eAaImAgNnS
zřízeno	zřízen	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
vznikly	vzniknout	k5eAaPmAgFnP
tři	tři	k4xCgFnPc4
třídy	třída	k1gFnPc4
prvního	první	k4xOgInSc2
ročníku	ročník	k1gInSc2
čtyřletého	čtyřletý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
opět	opět	k6eAd1
obnovena	obnoven	k2eAgFnSc1d1
<g/>
,	,	kIx,
rovněž	rovněž	k9
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
sedmileté	sedmiletý	k2eAgNnSc1d1
studium	studium	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
pak	pak	k6eAd1
i	i	k9
osmileté	osmiletý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
škola	škola	k1gFnSc1
zřídila	zřídit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
čtyřletý	čtyřletý	k2eAgInSc4d1
obor	obor	k1gInSc4
<g/>
,	,	kIx,
pedagogické	pedagogický	k2eAgNnSc1d1
lyceum	lyceum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
pedagogická	pedagogický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
školy	škola	k1gFnSc2
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
budovu	budova	k1gFnSc4
</s>
<s>
Budova	budova	k1gFnSc1
školy	škola	k1gFnSc2
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
byla	být	k5eAaImAgFnS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
bez	bez	k7c2
větších	veliký	k2eAgFnPc2d2
rekonstrukcí	rekonstrukce	k1gFnPc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nastalo	nastat	k5eAaPmAgNnS
období	období	k1gNnSc1
oprav	oprava	k1gFnPc2
a	a	k8xC
stavebně	stavebně	k6eAd1
technických	technický	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
trvalo	trvat	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
instalovány	instalován	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
rozvody	rozvod	k1gInPc1
vody	voda	k1gFnSc2
a	a	k8xC
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
vyměněna	vyměněn	k2eAgFnSc1d1
střešní	střešní	k2eAgFnSc1d1
krytina	krytina	k1gFnSc1
a	a	k8xC
nová	nový	k2eAgNnPc1d1
okna	okno	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
půdních	půdní	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
byly	být	k5eAaImAgFnP
vybudovány	vybudován	k2eAgFnPc1d1
nové	nový	k2eAgFnPc1d1
učebny	učebna	k1gFnPc1
<g/>
,	,	kIx,
kabinety	kabinet	k1gInPc1
a	a	k8xC
ateliéry	ateliér	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
etapa	etapa	k1gFnSc1
přístavby	přístavba	k1gFnSc2
školy	škola	k1gFnSc2
podle	podle	k7c2
projektu	projekt	k1gInSc2
Ing.	ing.	kA
Petra	Petr	k1gMnSc2
Veneše	Veneše	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavila	postavit	k5eAaPmAgFnS
se	se	k3xPyFc4
nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
s	s	k7c7
učebnami	učebna	k1gFnPc7
<g/>
,	,	kIx,
kuchyní	kuchyně	k1gFnSc7
<g/>
,	,	kIx,
jídelnou	jídelna	k1gFnSc7
<g/>
,	,	kIx,
velkou	velký	k2eAgFnSc7d1
tělocvičnou	tělocvična	k1gFnSc7
a	a	k8xC
vstupní	vstupní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
spojující	spojující	k2eAgFnPc4d1
obě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
etapa	etapa	k1gFnSc1
přístavby	přístavba	k1gFnSc2
školy	škola	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
oddělená	oddělený	k2eAgFnSc1d1
budova	budova	k1gFnSc1
s	s	k7c7
knihovnou	knihovna	k1gFnSc7
a	a	k8xC
studijním	studijní	k2eAgInSc7d1
sálem	sál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venku	venku	k6eAd1
za	za	k7c7
školou	škola	k1gFnSc7
se	se	k3xPyFc4
postavilo	postavit	k5eAaPmAgNnS
nové	nový	k2eAgNnSc1d1
sportoviště	sportoviště	k1gNnSc1
a	a	k8xC
parkovací	parkovací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
už	už	k6eAd1
jen	jen	k9
rekonstrukce	rekonstrukce	k1gFnSc1
malé	malý	k2eAgFnSc2d1
tělocvičny	tělocvična	k1gFnSc2
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
budově	budova	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Škola	škola	k1gFnSc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
tedy	tedy	k9
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
budov	budova	k1gFnPc2
a	a	k8xC
dvou	dva	k4xCgNnPc2
venkovních	venkovní	k2eAgNnPc2d1
hřišť	hřiště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výuce	výuka	k1gFnSc3
zde	zde	k6eAd1
slouží	sloužit	k5eAaImIp3nP
desítky	desítka	k1gFnPc1
klasických	klasický	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
odborných	odborný	k2eAgFnPc2d1
učeben	učebna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
vybaveny	vybaven	k2eAgFnPc1d1
DVD	DVD	kA
přehrávači	přehrávač	k1gInPc7
<g/>
,	,	kIx,
počítači	počítač	k1gInPc7
a	a	k8xC
dataprojektory	dataprojektor	k1gInPc1
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
učebna	učebna	k1gFnSc1
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
biologie	biologie	k1gFnSc2
<g/>
,	,	kIx,
zeměpisu	zeměpis	k1gInSc2
<g/>
,	,	kIx,
dějepisu	dějepis	k1gInSc2
<g/>
,	,	kIx,
angličtiny	angličtina	k1gFnSc2
a	a	k8xC
ZSV	ZSV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
tři	tři	k4xCgFnPc4
počítačové	počítačový	k2eAgFnPc4d1
učebny	učebna	k1gFnPc4
a	a	k8xC
tři	tři	k4xCgFnPc4
vybavené	vybavený	k2eAgFnPc4d1
laboratoře	laboratoř	k1gFnPc4
–	–	k?
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
biologie	biologie	k1gFnSc2
a	a	k8xC
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
škole	škola	k1gFnSc6
dva	dva	k4xCgInPc1
ateliéry	ateliér	k1gInPc1
–	–	k?
výtvarné	výtvarný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
hudební	hudební	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
samostatné	samostatný	k2eAgNnSc4d1
studium	studium	k1gNnSc4
žáků	žák	k1gMnPc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
školní	školní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
a	a	k8xC
pro	pro	k7c4
sportovní	sportovní	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
malá	malá	k1gFnSc1
a	a	k8xC
velká	velký	k2eAgFnSc1d1
tělocvična	tělocvična	k1gFnSc1
<g/>
,	,	kIx,
dvě	dva	k4xCgNnPc4
venkovní	venkovní	k2eAgNnPc4d1
hřiště	hřiště	k1gNnPc4
a	a	k8xC
nově	nově	k6eAd1
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1
posilovna	posilovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
ve	v	k7c6
škole	škola	k1gFnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
nová	nový	k2eAgFnSc1d1
sít	sít	k5eAaImF
Wi-Fi	Wi-Fe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ředitelé	ředitel	k1gMnPc1
školy	škola	k1gFnSc2
</s>
<s>
Helena	Helena	k1gFnSc1
Kotyková	Kotyková	k1gFnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kubín	Kubín	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdeňka	Zdeňka	k1gFnSc1
Kutínová	Kutínová	k1gFnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Názvy	název	k1gInPc1
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
škola	škola	k1gFnSc1
užívala	užívat	k5eAaImAgFnS
</s>
<s>
1933	#num#	k4
–	–	k?
Masarykova	Masarykův	k2eAgFnSc1d1
obecná	obecná	k1gFnSc1
a	a	k8xC
měšťanská	měšťanský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
1953	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
jedenáctiletá	jedenáctiletý	k2eAgFnSc1d1
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
1959	#num#	k4
–	–	k?
Dvanáctiletá	dvanáctiletý	k2eAgFnSc1d1
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
1961	#num#	k4
–	–	k?
Střední	střední	k2eAgFnSc1d1
všeobecně	všeobecně	k6eAd1
vzdělávací	vzdělávací	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
1970	#num#	k4
–	–	k?
Gymnasium	gymnasium	k1gNnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
1990	#num#	k4
–	–	k?
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
pedagogická	pedagogický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
2006	#num#	k4
–	–	k?
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
Studium	studium	k1gNnSc1
</s>
<s>
Ve	v	k7c6
škole	škola	k1gFnSc6
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
22	#num#	k4
tříd	třída	k1gFnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
8	#num#	k4
tříd	třída	k1gFnPc2
osmiletého	osmiletý	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
<g/>
,	,	kIx,
8	#num#	k4
čtyřletého	čtyřletý	k2eAgInSc2d1
<g/>
,	,	kIx,
4	#num#	k4
třídy	třída	k1gFnPc4
střední	střední	k2eAgFnSc2d1
odborné	odborný	k2eAgFnSc2d1
školy	škola	k1gFnSc2
pedagogické	pedagogický	k2eAgFnSc2d1
a	a	k8xC
2	#num#	k4
třídy	třída	k1gFnSc2
pedagogického	pedagogický	k2eAgNnSc2d1
lycea	lyceum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
–	–	k?
čtyřleté	čtyřletý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
</s>
<s>
Kód	kód	k1gInSc1
<g/>
:	:	kIx,
7941K401	7941K401	k4
</s>
<s>
Zaměření	zaměření	k1gNnSc1
čtyřletého	čtyřletý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
je	být	k5eAaImIp3nS
všeobecné	všeobecný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
žáci	žák	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
volitelnými	volitelný	k2eAgInPc7d1
předměty	předmět	k1gInPc7
částečně	částečně	k6eAd1
specializovat	specializovat	k5eAaBmF
na	na	k7c4
humanitně	humanitně	k6eAd1
nebo	nebo	k8xC
přírodovědně	přírodovědně	k6eAd1
zaměřenou	zaměřený	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
je	být	k5eAaImIp3nS
zakončeno	zakončit	k5eAaPmNgNnS
maturitní	maturitní	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
žáky	žák	k1gMnPc4
na	na	k7c4
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
–	–	k?
osmileté	osmiletý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
</s>
<s>
Kód	kód	k1gInSc1
<g/>
:	:	kIx,
7941K801	7941K801	k4
</s>
<s>
Zaměření	zaměření	k1gNnSc1
osmiletého	osmiletý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
je	být	k5eAaImIp3nS
všeobecné	všeobecný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
žáci	žák	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
vhodnými	vhodný	k2eAgInPc7d1
volitelnými	volitelný	k2eAgInPc7d1
předměty	předmět	k1gInPc7
specializovat	specializovat	k5eAaBmF
na	na	k7c4
humanitní	humanitní	k2eAgFnPc4d1
nebo	nebo	k8xC
přírodovědné	přírodovědný	k2eAgFnPc4d1
obory	obora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
je	být	k5eAaImIp3nS
zakončeno	zakončit	k5eAaPmNgNnS
maturitní	maturitní	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
žáky	žák	k1gMnPc4
na	na	k7c4
vysokou	vysoká	k1gFnSc4
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Střední	střední	k2eAgFnSc1d1
a	a	k8xC
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
</s>
<s>
Kód	kód	k1gInSc1
<g/>
:	:	kIx,
7531M005	7531M005	k4
</s>
<s>
Čtyřleté	čtyřletý	k2eAgNnSc1d1
studium	studium	k1gNnSc1
zakončené	zakončený	k2eAgNnSc1d1
maturitní	maturitní	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
<g/>
,	,	kIx,
studenti	student	k1gMnPc1
pedagogické	pedagogický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
jsou	být	k5eAaImIp3nP
připravování	připravování	k1gNnPc1
na	na	k7c4
učitelství	učitelství	k1gNnSc4
v	v	k7c6
</s>
<s>
mateřských	mateřský	k2eAgFnPc6d1
školách	škola	k1gFnPc6
nebo	nebo	k8xC
na	na	k7c4
následné	následný	k2eAgNnSc4d1
studium	studium	k1gNnSc4
na	na	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
obor	obor	k1gInSc4
umožňuje	umožňovat	k5eAaImIp3nS
studium	studium	k1gNnSc4
nejen	nejen	k6eAd1
dívkám	dívka	k1gFnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
chlapcům	chlapec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
pro	pro	k7c4
humanitně	humanitně	k6eAd1
zaměřené	zaměřený	k2eAgMnPc4d1
žáky	žák	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pedagogické	pedagogický	k2eAgNnSc1d1
lyceum	lyceum	k1gNnSc1
</s>
<s>
Kód	kód	k1gInSc1
<g/>
:	:	kIx,
7842M03	7842M03	k4
</s>
<s>
Studium	studium	k1gNnSc1
na	na	k7c6
Pedagogickém	pedagogický	k2eAgNnSc6d1
lyceu	lyceum	k1gNnSc6
je	být	k5eAaImIp3nS
čtyřleté	čtyřletý	k2eAgNnSc1d1
<g/>
,	,	kIx,
zakončené	zakončený	k2eAgNnSc1d1
maturitní	maturitní	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
lycea	lyceum	k1gNnSc2
jsou	být	k5eAaImIp3nP
připravováni	připravován	k2eAgMnPc1d1
k	k	k7c3
vysokoškolskému	vysokoškolský	k2eAgNnSc3d1
studiu	studio	k1gNnSc3
především	především	k6eAd1
na	na	k7c6
humanitně	humanitně	k6eAd1
zaměřených	zaměřený	k2eAgInPc6d1
VŠ	vš	k0
<g/>
,	,	kIx,
nebo	nebo	k8xC
pro	pro	k7c4
povolání	povolání	k1gNnSc4
asistent	asistent	k1gMnSc1
pedagoga	pedagog	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
široce	široko	k6eAd1
koncipovaný	koncipovaný	k2eAgInSc1d1
obor	obor	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
studium	studium	k1gNnSc4
nejen	nejen	k6eAd1
děvčatům	děvče	k1gNnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
chlapcům	chlapec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Projekty	projekt	k1gInPc1
a	a	k8xC
aktivity	aktivita	k1gFnPc1
</s>
<s>
Ve	v	k7c6
škole	škola	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
mnoho	mnoho	k4c1
projektů	projekt	k1gInPc2
a	a	k8xC
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
mnoha	mnoho	k4c7
aktivitami	aktivita	k1gFnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
projekt	projekt	k1gInSc1
Digitální	digitální	k2eAgFnSc1d1
třída	třída	k1gFnSc1
-	-	kIx~
první	první	k4xOgFnSc1
škola	škola	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
otestovat	otestovat	k5eAaPmF
ve	v	k7c6
výuce	výuka	k1gFnSc6
revoluční	revoluční	k2eAgInSc1d1
typ	typ	k1gInSc1
malého	malý	k2eAgMnSc2d1
přenosného	přenosný	k2eAgMnSc2d1
EeePC	EeePC	k1gMnSc2
</s>
<s>
pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Gaudea	Gaudeus	k1gMnSc2
–	–	k?
školní	školní	k2eAgInSc4d1
pěvecký	pěvecký	k2eAgInSc4d1
sbor	sbor	k1gInSc4
</s>
<s>
RATAB	RATAB	kA
–	–	k?
sdružení	sdružení	k1gNnSc2
pořádající	pořádající	k2eAgFnSc2d1
každoročně	každoročně	k6eAd1
tábor	tábor	k1gInSc1
pro	pro	k7c4
studenty	student	k1gMnPc4
libereckých	liberecký	k2eAgNnPc2d1
gymnázií	gymnázium	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
školní	školní	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Jégéčko	Jégéčko	k6eAd1
-	-	kIx~
poslední	poslední	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
:	:	kIx,
leden	leden	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
školního	školní	k2eAgInSc2d1
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
ho	on	k3xPp3gInSc2
vede	vést	k5eAaImIp3nS
třída	třída	k1gFnSc1
tercie	tercie	k1gFnSc1
</s>
<s>
školní	školní	k2eAgFnSc1d1
televize	televize	k1gFnSc1
jergymtv	jergymtv	k1gMnSc1
-	-	kIx~
šéfredaktor	šéfredaktor	k1gMnSc1
Matouš	Matouš	k1gMnSc1
Müller	Müller	k1gMnSc1
</s>
<s>
Školní	školní	k2eAgFnSc1d1
televize	televize	k1gFnSc1
jergymtv	jergymtva	k1gFnPc2
</s>
<s>
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
tehdejšího	tehdejší	k2eAgMnSc2d1
studenta	student	k1gMnSc2
tercie	tercie	k1gFnSc2
Matouše	Matouš	k1gMnSc2
Müllera	Müller	k1gMnSc2
a	a	k8xC
prof.	prof.	kA
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
Mgr.	Mgr.	kA
Žanety	Žaneta	k1gFnSc2
Horanové	Horanová	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
na	na	k7c6
libereckém	liberecký	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
školní	školní	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
měsíc	měsíc	k1gInSc4
vysílá	vysílat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
pořad	pořad	k1gInSc4
Zprávy	zpráva	k1gFnSc2
jergym	jergym	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ve	v	k7c6
zhruba	zhruba	k6eAd1
deseti	deset	k4xCc6
minutách	minuta	k1gFnPc6
představí	představit	k5eAaPmIp3nP
ty	ten	k3xDgFnPc4
nejzajímavější	zajímavý	k2eAgFnPc4d3
a	a	k8xC
nejdůležitější	důležitý	k2eAgFnPc4d3
události	událost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
se	se	k3xPyFc4
událo	udát	k5eAaPmAgNnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Gymnáziem	gymnázium	k1gNnSc7
&	&	k?
SOŠPg	SOŠPga	k1gFnPc2
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Školní	školní	k2eAgFnSc1d1
televize	televize	k1gFnSc1
mj.	mj.	kA
natáčí	natáčet	k5eAaImIp3nS
také	také	k9
společenské	společenský	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
např.	např.	kA
školní	školní	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
<g/>
,	,	kIx,
maturitní	maturitní	k2eAgInPc4d1
plesy	ples	k1gInPc4
<g/>
,	,	kIx,
besedy	beseda	k1gFnPc4
zajímavých	zajímavý	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
tváří	tvář	k1gFnSc7
školní	školní	k2eAgFnSc2d1
televize	televize	k1gFnSc2
studentka	studentek	k1gMnSc2
čtyřletého	čtyřletý	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
Tereza	Tereza	k1gFnSc1
Štěpařová	Štěpařová	k1gFnSc1
společně	společně	k6eAd1
se	s	k7c7
studentem	student	k1gMnSc7
gymnázia	gymnázium	k1gNnSc2
Vojtěchem	Vojtěch	k1gMnSc7
Bujárkem	Bujárek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facebookové	Facebookový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
spravuje	spravovat	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Kubánek	Kubánek	k1gMnSc1
(	(	kIx(
<g/>
ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Matouš	Matouš	k1gMnSc1
Müller	Müller	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Školní	školní	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Jégéčko	Jégéčko	k6eAd1
</s>
<s>
Jégéčko	Jégéčko	k6eAd1
je	být	k5eAaImIp3nS
studentský	studentský	k2eAgInSc1d1
měsíčník	měsíčník	k1gInSc1
při	při	k7c6
Gymnáziu	gymnázium	k1gNnSc6
&	&	k?
SOŠPg	SOŠPg	k1gInSc1
Liberec	Liberec	k1gInSc4
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
máte	mít	k5eAaImIp2nP
chuť	chuť	k1gFnSc4
se	se	k3xPyFc4
na	na	k7c6
tomto	tento	k3xDgInSc6
projektu	projekt	k1gInSc6
podílet	podílet	k5eAaImF
a	a	k8xC
nevíte	vědět	k5eNaImIp2nP
<g/>
,	,	kIx,
kde	kde	k6eAd1
začít	začít	k5eAaPmF
<g/>
,	,	kIx,
můžete	moct	k5eAaImIp2nP
nám	my	k3xPp1nPc3
nenápadně	nápadně	k6eNd1
podsouvat	podsouvat	k5eAaImF
články	článek	k1gInPc4
do	do	k7c2
rubrik	rubrika	k1gFnPc2
Čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
se	se	k3xPyFc4
baví	bavit	k5eAaImIp3nP
gympláci	gymplák	k1gMnPc1
či	či	k8xC
Cestování	cestování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvítáme	uvítat	k5eAaPmIp1nP
krátké	krátké	k1gNnSc4
recenze	recenze	k1gFnSc2
nedávných	dávný	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
též	též	k9
poezii	poezie	k1gFnSc4
a	a	k8xC
prózu	próza	k1gFnSc4
jinak	jinak	k6eAd1
skladovanou	skladovaný	k2eAgFnSc4d1
na	na	k7c6
dně	dno	k1gNnSc6
šuplíku	šuplíku	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
máte	mít	k5eAaImIp2nP
možnost	možnost	k1gFnSc4
pomocí	pomocí	k7c2
inzerátů	inzerát	k1gInPc2
prodat	prodat	k5eAaPmF
nebo	nebo	k8xC
nakoupit	nakoupit	k5eAaPmF
učebnice	učebnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
číslo	číslo	k1gNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
(	(	kIx(
<g/>
kvůli	kvůli	k7c3
rozpuštění	rozpuštění	k1gNnSc3
redakce	redakce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
Mgr.	Mgr.	kA
Žaneta	Žaneta	k1gFnSc1
Horanová	Horanová	k1gFnSc1
nechtěla	chtít	k5eNaImAgFnS
tento	tento	k3xDgInSc4
projekt	projekt	k1gInSc4
nechat	nechat	k5eAaPmF
zaniknout	zaniknout	k5eAaPmF
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
požádala	požádat	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
třídu	třída	k1gFnSc4
<g/>
,	,	kIx,
tercii	tercie	k1gFnSc4
<g/>
,	,	kIx,
jestli	jestli	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
tohoto	tento	k3xDgInSc2
projektu	projekt	k1gInSc2
neujala	ujmout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plánuje	plánovat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
novém	nový	k2eAgNnSc6d1
rozvržení	rozvržení	k1gNnSc6
(	(	kIx(
<g/>
A	a	k9
<g/>
4	#num#	k4
<g/>
→	→	k?
<g/>
A	a	k9
<g/>
5	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
nových	nový	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
zamýšlelo	zamýšlet	k5eAaImAgNnS
o	o	k7c6
novém	nový	k2eAgInSc6d1
názvu	název	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
redakce	redakce	k1gFnSc1
se	se	k3xPyFc4
shodla	shodnout	k5eAaPmAgFnS,k5eAaBmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
název	název	k1gInSc4
Jégéčko	Jégéčko	k6eAd1
již	již	k6eAd1
velmi	velmi	k6eAd1
zažitý	zažitý	k2eAgInSc1d1
a	a	k8xC
většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
na	na	k7c6
škole	škola	k1gFnSc6
by	by	kYmCp3nP
si	se	k3xPyFc3
na	na	k7c4
to	ten	k3xDgNnSc4
ani	ani	k8xC
nezvykla	zvyknout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KARPAŠ	KARPAŠ	kA
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
o	o	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Dialog	dialog	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
664	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86761	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KUTÍNOVÁ	KUTÍNOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeňka	k1gFnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Almanach	almanach	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Gymnázium	gymnázium	k1gNnSc1
s	s	k7c7
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
78	#num#	k4
s.	s.	k?
</s>
<s>
Gymnázium	gymnázium	k1gNnSc4
&	&	k?
SOŠPg	SOŠPg	k1gInSc1
Liberec	Liberec	k1gInSc1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
<g/>
,	,	kIx,
web	web	k1gInSc1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc4
&	&	k?
SOŠPg	SOŠPg	k1gInSc1
Liberec	Liberec	k1gInSc1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
školy	škola	k1gFnSc2
</s>
<s>
Školní	školní	k2eAgFnSc1d1
televize	televize	k1gFnSc1
jergymtv	jergymtv	k1gInSc1
<g/>
,	,	kIx,
web	web	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Gymnázium	gymnázium	k1gNnSc1
Jeronýmova	Jeronýmův	k2eAgInSc2d1
–	–	k?
čtyřleté	čtyřletý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
www.jergym.cz	www.jergym.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Gymnázium	gymnázium	k1gNnSc1
Jeronýmova	Jeronýmův	k2eAgInSc2d1
–	–	k?
osmileté	osmiletý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
www.jergym.cz	www.jergym.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Střední	střední	k2eAgFnSc1d1
a	a	k8xC
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
–	–	k?
Jeronýmova	Jeronýmův	k2eAgMnSc2d1
<g/>
.	.	kIx.
www.jergym.cz	www.jergym.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RATAB	RATAB	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Votoček	Votočka	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Liberec	Liberec	k1gInSc1
Doprava	doprava	k1gFnSc1
</s>
<s>
Doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Autobusové	autobusový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Historie	historie	k1gFnSc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Liberce	Liberec	k1gInSc2
•	•	k?
Dějiny	dějiny	k1gFnPc4
Liberce	Liberec	k1gInSc2
•	•	k?
Ještěd	Ještěd	k1gInSc1
(	(	kIx(
<g/>
obchodní	obchodní	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Reichenberger	Reichenberger	k1gInSc1
Tagesbote	Tagesbot	k1gInSc5
•	•	k?
Reichenberger	Reichenberger	k1gMnSc1
Zeitung	Zeitung	k1gMnSc1
•	•	k?
Valdštejnské	valdštejnský	k2eAgInPc4d1
domky	domek	k1gInPc4
Kultura	kultura	k1gFnSc1
</s>
<s>
Botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Centrum	centrum	k1gNnSc1
Babylon	Babylon	k1gInSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Divadlo	divadlo	k1gNnSc1
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
•	•	k?
Krajská	krajský	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Liberecký	liberecký	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Oblastní	oblastní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Severočeské	severočeský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Liberec	Liberec	k1gInSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Božského	božský	k2eAgNnSc2d1
srdce	srdce	k1gNnSc2
Páně	páně	k2eAgInSc4d1
•	•	k?
Kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Bonifáce	Bonifác	k1gMnSc2
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
Velikého	veliký	k2eAgNnSc2d1
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
Paduánského	paduánský	k2eAgNnSc2d1
•	•	k?
Kostel	kostel	k1gInSc1
svaté	svatá	k1gFnSc2
Máří	Máří	k?
Magdalény	Magdaléna	k1gFnSc2
a	a	k8xC
kapucínský	kapucínský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
•	•	k?
Kostel	kostel	k1gInSc1
Nalezení	nalezení	k1gNnSc2
svatého	svatý	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
•	•	k?
Kostel	kostel	k1gInSc1
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
•	•	k?
Kostel	kostel	k1gInSc1
Matky	matka	k1gFnSc2
Boží	božit	k5eAaImIp3nS
U	u	k7c2
Obrázku	obrázek	k1gInSc2
•	•	k?
Kostel	kostel	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
Pamětihodnosti	pamětihodnost	k1gFnSc2
</s>
<s>
Appeltův	Appeltův	k2eAgInSc1d1
dům	dům	k1gInSc1
•	•	k?
Liberecká	liberecký	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
•	•	k?
Městské	městský	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
•	•	k?
Palác	palác	k1gInSc1
Adria	Adria	k1gFnSc1
•	•	k?
Vila	vít	k5eAaImAgFnS
Franze	Franze	k1gFnSc1
Bognera	Bogner	k1gMnSc2
Sport	sport	k1gInSc1
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1
Tygři	tygr	k1gMnPc1
Liberec	Liberec	k1gInSc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gInSc4
Liberec	Liberec	k1gInSc1
Studium	studium	k1gNnSc1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
•	•	k?
Gymnázium	gymnázium	k1gNnSc1
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
•	•	k?
Obchodní	obchodní	k2eAgFnPc1d1
akademie	akademie	k1gFnPc1
a	a	k8xC
Jazyková	jazykový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
stavební	stavební	k2eAgFnSc1d1
Liberec	Liberec	k1gInSc4
•	•	k?
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
a	a	k8xC
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
představitelů	představitel	k1gMnPc2
Liberce	Liberec	k1gInSc2
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Dům	dům	k1gInSc1
obuvi	obuv	k1gFnSc2
Baťa	Baťa	k1gMnSc1
•	•	k?
Obchodní	obchodní	k2eAgInSc4d1
dům	dům	k1gInSc4
Brouk	brouk	k1gMnSc1
a	a	k8xC
Babka	babka	k1gFnSc1
•	•	k?
Krajská	krajský	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Vazební	vazební	k2eAgFnSc1d1
věznice	věznice	k1gFnSc1
Liberec	Liberec	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Liberec	Liberec	k1gInSc1
</s>
