<s>
Asteroid	asteroid	k1gInSc1	asteroid
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
často	často	k6eAd1	často
používané	používaný	k2eAgNnSc4d1	používané
označení	označení	k1gNnSc4	označení
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
nazývají	nazývat	k5eAaImIp3nP	nazývat
planetky	planetka	k1gFnPc4	planetka
<g/>
.	.	kIx.	.
</s>
