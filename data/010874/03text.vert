<p>
<s>
Asteroid	asteroid	k1gInSc1	asteroid
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
často	často	k6eAd1	často
používané	používaný	k2eAgNnSc4d1	používané
označení	označení	k1gNnSc4	označení
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
nazývají	nazývat	k5eAaImIp3nP	nazývat
planetky	planetka	k1gFnPc4	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesné	přesný	k2eNgNnSc1d1	nepřesné
pojmenování	pojmenování	k1gNnSc1	pojmenování
asteroid	asteroid	k1gInSc1	asteroid
má	mít	k5eAaImIp3nS	mít
historické	historický	k2eAgInPc4d1	historický
kořeny	kořen	k1gInPc4	kořen
z	z	k7c2	z
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgNnPc4	dva
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
už	už	k9	už
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
nové	nový	k2eAgNnSc1d1	nové
označení	označení	k1gNnSc1	označení
tohoto	tento	k3xDgInSc2	tento
dosud	dosud	k6eAd1	dosud
neznámého	známý	k2eNgInSc2d1	neznámý
typu	typ	k1gInSc2	typ
těles	těleso	k1gNnPc2	těleso
-	-	kIx~	-
planetka	planetka	k1gFnSc1	planetka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
takové	takový	k3xDgNnSc4	takový
těleso	těleso	k1gNnSc4	těleso
objevil	objevit	k5eAaPmAgMnS	objevit
Giuseppe	Giusepp	k1gInSc5	Giusepp
Piazzi	Piazh	k1gMnPc1	Piazh
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1801	[number]	k4	1801
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc1	název
Ceres	ceres	k1gInSc1	ceres
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
nová	nový	k2eAgFnSc1d1	nová
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
asteroid	asteroid	k1gInSc1	asteroid
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
hvězdě	hvězda	k1gFnSc3	hvězda
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Williama	William	k1gMnSc2	William
Herschela	Herschel	k1gMnSc2	Herschel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
jejich	jejich	k3xOp3gInSc4	jejich
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mohu	moct	k5eAaImIp1nS	moct
použít	použít	k5eAaPmF	použít
takového	takový	k3xDgInSc2	takový
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
proto	proto	k8xC	proto
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojmenování	pojmenování	k1gNnSc4	pojmenování
a	a	k8xC	a
nazývám	nazývat	k5eAaImIp1nS	nazývat
je	on	k3xPp3gInPc4	on
asteroidy	asteroid	k1gInPc4	asteroid
<g/>
;	;	kIx,	;
vyhrazuji	vyhrazovat	k5eAaImIp1nS	vyhrazovat
si	se	k3xPyFc3	se
však	však	k9	však
nicméně	nicméně	k8xC	nicméně
volnost	volnost	k1gFnSc1	volnost
změnit	změnit	k5eAaPmF	změnit
toto	tento	k3xDgNnSc4	tento
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
jiné	jiný	k2eAgFnSc3d1	jiná
<g/>
,	,	kIx,	,
výstižnější	výstižný	k2eAgFnSc3d2	výstižnější
povaze	povaha	k1gFnSc3	povaha
jejich	jejich	k3xOp3gNnSc4	jejich
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
jejich	jejich	k3xOp3gFnSc2	jejich
malé	malý	k2eAgFnSc2d1	malá
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
další	další	k2eAgFnPc1d1	další
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
asteroid	asteroida	k1gFnPc2	asteroida
tedy	tedy	k9	tedy
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
dalekohledu	dalekohled	k1gInSc6	dalekohled
vypadaly	vypadat	k5eAaImAgFnP	vypadat
<g/>
:	:	kIx,	:
byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
malé	malý	k2eAgInPc1d1	malý
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
planet	planeta	k1gFnPc2	planeta
šlo	jít	k5eAaImAgNnS	jít
pozorovat	pozorovat	k5eAaImF	pozorovat
kotoučky	kotouček	k1gInPc4	kotouček
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
některé	některý	k3yIgInPc1	některý
povrchové	povrchový	k2eAgInPc1d1	povrchový
útvary	útvar	k1gInPc1	útvar
jako	jako	k8xC	jako
pásy	pás	k1gInPc1	pás
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
či	či	k8xC	či
Saturnu	Saturn	k1gInSc6	Saturn
nebo	nebo	k8xC	nebo
tmavší	tmavý	k2eAgFnPc4d2	tmavší
oblasti	oblast	k1gFnPc4	oblast
či	či	k8xC	či
polární	polární	k2eAgFnPc4d1	polární
čepičky	čepička	k1gFnPc4	čepička
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
byly	být	k5eAaImAgInP	být
vzhledově	vzhledově	k6eAd1	vzhledově
hvězdám	hvězda	k1gFnPc3	hvězda
podobné	podobný	k2eAgNnSc1d1	podobné
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přesný	přesný	k2eAgInSc1d1	přesný
překlad	překlad	k1gInSc1	překlad
slova	slovo	k1gNnSc2	slovo
aster-oid	asterid	k1gInSc1	aster-oid
<g/>
)	)	kIx)	)
a	a	k8xC	a
lišily	lišit	k5eAaImAgFnP	lišit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
rychlým	rychlý	k2eAgInSc7d1	rychlý
pohybem	pohyb	k1gInSc7	pohyb
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
už	už	k6eAd1	už
asi	asi	k9	asi
30	[number]	k4	30
takových	takový	k3xDgNnPc2	takový
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
i	i	k8xC	i
první	první	k4xOgInSc4	první
měření	měření	k1gNnPc2	měření
či	či	k8xC	či
odhady	odhad	k1gInPc1	odhad
jejich	jejich	k3xOp3gFnPc2	jejich
velikostí	velikost	k1gFnPc2	velikost
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
)	)	kIx)	)
a	a	k8xC	a
astronomové	astronom	k1gMnPc1	astronom
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
popisné	popisný	k2eAgNnSc1d1	popisné
pojmenování	pojmenování	k1gNnSc1	pojmenování
asteroid	asteroid	k1gInSc1	asteroid
nevystihuje	vystihovat	k5eNaImIp3nS	vystihovat
podstatu	podstata	k1gFnSc4	podstata
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
astronomů	astronom	k1gMnPc2	astronom
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
těmto	tento	k3xDgNnPc3	tento
tělesům	těleso	k1gNnPc3	těleso
nejvíce	hodně	k6eAd3	hodně
věnovali	věnovat	k5eAaPmAgMnP	věnovat
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc1	tento
tělesa	těleso	k1gNnPc1	těleso
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kategorií	kategorie	k1gFnSc7	kategorie
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nazývat	nazývat	k5eAaImF	nazývat
minor	minor	k2eAgInSc4d1	minor
planets	planets	k1gInSc4	planets
<g/>
,	,	kIx,	,
kleine	kleinout	k5eAaPmIp3nS	kleinout
Planeten	Planeten	k2eAgMnSc1d1	Planeten
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
tedy	tedy	k9	tedy
malé	malý	k2eAgFnPc1d1	malá
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
vytvářet	vytvářet	k5eAaImF	vytvářet
zdrobněliny	zdrobnělina	k1gFnPc4	zdrobnělina
pomocí	pomocí	k7c2	pomocí
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
slovo	slovo	k1gNnSc1	slovo
planetky	planetka	k1gFnSc2	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
planetka	planetka	k1gFnSc1	planetka
tedy	tedy	k9	tedy
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
podstatu	podstata	k1gFnSc4	podstata
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
ne	ne	k9	ne
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
asteroid	asteroida	k1gFnPc2	asteroida
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
terminologii	terminologie	k1gFnSc6	terminologie
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
anglického	anglický	k2eAgInSc2d1	anglický
minor	minor	k2eAgMnPc1d1	minor
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
překladům	překlad	k1gInPc3	překlad
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
udržuje	udržovat	k5eAaImIp3nS	udržovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
2016	[number]	k4	2016
byly	být	k5eAaImAgFnP	být
přesně	přesně	k6eAd1	přesně
vypočítány	vypočítán	k2eAgFnPc1d1	vypočítána
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
celkem	celkem	k6eAd1	celkem
706	[number]	k4	706
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
označen	označit	k5eAaPmNgMnS	označit
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
455	[number]	k4	455
000	[number]	k4	000
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
odhady	odhad	k1gInPc1	odhad
uvádějí	uvádět	k5eAaImIp3nP	uvádět
celkový	celkový	k2eAgInSc4d1	celkový
počet	počet	k1gInSc4	počet
asteroidů	asteroid	k1gInPc2	asteroid
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
na	na	k7c4	na
1,1	[number]	k4	1,1
až	až	k9	až
1,9	[number]	k4	1,9
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
asteroidy	asteroid	k1gInPc7	asteroid
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Pallas	Pallas	k1gMnSc1	Pallas
a	a	k8xC	a
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Vesta	vesta	k1gFnSc1	vesta
<g/>
;	;	kIx,	;
průměr	průměr	k1gInSc4	průměr
obou	dva	k4xCgMnPc2	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
cca	cca	kA	cca
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Ceres	ceres	k1gInSc1	ceres
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
km	km	kA	km
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
přeřazen	přeřadit	k5eAaPmNgInS	přeřadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc1	klasifikace
asteroidů	asteroid	k1gInPc2	asteroid
==	==	k?	==
</s>
</p>
<p>
<s>
Asteroidy	asteroida	k1gFnPc1	asteroida
jsou	být	k5eAaImIp3nP	být
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
základě	základ	k1gInSc6	základ
charakteristik	charakteristika	k1gFnPc2	charakteristika
jejich	jejich	k3xOp3gFnPc2	jejich
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
detailů	detail	k1gInPc2	detail
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Orbitální	orbitální	k2eAgFnSc2d1	orbitální
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
Asteroidy	asteroida	k1gFnPc1	asteroida
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
rodin	rodina	k1gFnPc2	rodina
na	na	k7c6	na
základě	základ	k1gInSc6	základ
charakteristik	charakteristika	k1gFnPc2	charakteristika
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
objeveném	objevený	k2eAgMnSc6d1	objevený
členovi	člen	k1gMnSc6	člen
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
volná	volný	k2eAgNnPc1d1	volné
dynamická	dynamický	k2eAgNnPc1d1	dynamické
spojení	spojení	k1gNnPc1	spojení
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vazby	vazba	k1gFnPc1	vazba
v	v	k7c6	v
rodinách	rodina	k1gFnPc6	rodina
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
pevnější	pevný	k2eAgInPc1d2	pevnější
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
rozpadu	rozpad	k1gInSc2	rozpad
jednoho	jeden	k4xCgInSc2	jeden
velkého	velký	k2eAgInSc2d1	velký
asteroidu	asteroid	k1gInSc2	asteroid
na	na	k7c4	na
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spektrální	spektrální	k2eAgFnSc1d1	spektrální
klasifikace	klasifikace	k1gFnSc1	klasifikace
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
Clark	Clark	k1gInSc1	Clark
R.	R.	kA	R.
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Morrison	Morrison	k1gMnSc1	Morrison
a	a	k8xC	a
Ben	Ben	k1gInSc1	Ben
Zellner	Zellner	k1gMnSc1	Zellner
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
taxonomický	taxonomický	k2eAgInSc4d1	taxonomický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
schopnosti	schopnost	k1gFnSc6	schopnost
odrážet	odrážet	k5eAaImF	odrážet
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
tvarech	tvar	k1gInPc6	tvar
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
složení	složení	k1gNnSc4	složení
materiálu	materiál	k1gInSc2	materiál
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
asteroidu	asteroid	k1gInSc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
klasifikovali	klasifikovat	k5eAaImAgMnP	klasifikovat
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
typy	typ	k1gInPc1	typ
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
asteroidy	asteroid	k1gInPc4	asteroid
typu	typ	k1gInSc2	typ
C	C	kA	C
-	-	kIx~	-
uhličité	uhličitý	k2eAgInPc1d1	uhličitý
<g/>
,	,	kIx,	,
75	[number]	k4	75
<g/>
%	%	kIx~	%
známých	známý	k2eAgInPc2d1	známý
asteroidů	asteroid	k1gInPc2	asteroid
</s>
</p>
<p>
<s>
asteroidy	asteroid	k1gInPc4	asteroid
typu	typ	k1gInSc2	typ
S	s	k7c7	s
-	-	kIx~	-
křemičité	křemičitý	k2eAgInPc1d1	křemičitý
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
%	%	kIx~	%
známých	známý	k2eAgInPc2d1	známý
asteroidů	asteroid	k1gInPc2	asteroid
</s>
</p>
<p>
<s>
asteroidy	asteroid	k1gInPc4	asteroid
typu	typ	k1gInSc2	typ
M	M	kA	M
-	-	kIx~	-
kovové	kovový	k2eAgInPc4d1	kovový
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
asteroidůOd	asteroidůOda	k1gFnPc2	asteroidůOda
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
rozšířením	rozšíření	k1gNnPc3	rozšíření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgInPc2d1	nový
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Problémy	problém	k1gInPc1	problém
se	s	k7c7	s
spektrální	spektrální	k2eAgFnSc7d1	spektrální
klasifikací	klasifikace	k1gFnSc7	klasifikace
====	====	k?	====
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
spektrální	spektrální	k2eAgNnSc4d1	spektrální
označení	označení	k1gNnSc4	označení
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
určení	určení	k1gNnSc4	určení
složení	složení	k1gNnSc2	složení
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
C	C	kA	C
-	-	kIx~	-
uhličitan	uhličitan	k1gInSc1	uhličitan
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
carbonate	carbonat	k1gMnSc5	carbonat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
S	s	k7c7	s
-	-	kIx~	-
křemičitan	křemičitan	k1gInSc1	křemičitan
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
silicate	silicat	k1gMnSc5	silicat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
M	M	kA	M
-	-	kIx~	-
kovový	kovový	k2eAgInSc4d1	kovový
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
metallic	metallice	k1gFnPc2	metallice
<g/>
)	)	kIx)	)
<g/>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
omylu	omyl	k1gInSc3	omyl
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
typ	typ	k1gInSc1	typ
asteroidu	asteroid	k1gInSc2	asteroid
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
asteroidy	asteroid	k1gInPc4	asteroid
různých	různý	k2eAgFnPc2d1	různá
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
klasifikací	klasifikace	k1gFnPc2	klasifikace
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zaručené	zaručený	k2eAgNnSc1d1	zaručené
<g/>
,	,	kIx,	,
že	že	k8xS	že
asteroidy	asteroid	k1gInPc4	asteroid
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
taxonomické	taxonomický	k2eAgFnSc2d1	Taxonomická
jednotky	jednotka	k1gFnSc2	jednotka
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
stejné	stejný	k2eAgNnSc4d1	stejné
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
taxonomickém	taxonomický	k2eAgInSc6d1	taxonomický
systému	systém	k1gInSc6	systém
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
čehož	což	k3yQnSc2	což
spektrální	spektrální	k2eAgFnSc1d1	spektrální
klasifikace	klasifikace	k1gFnSc1	klasifikace
uvízla	uvíznout	k5eAaPmAgFnS	uvíznout
na	na	k7c6	na
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objevování	objevování	k1gNnPc2	objevování
asteroidů	asteroid	k1gInPc2	asteroid
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgFnPc1d1	historická
metody	metoda	k1gFnPc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
objevům	objev	k1gInPc3	objev
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
astronomie	astronomie	k1gFnSc2	astronomie
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
stoletích	století	k1gNnPc6	století
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
posunu	posun	k1gInSc3	posun
v	v	k7c6	v
objevování	objevování	k1gNnSc6	objevování
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
baron	baron	k1gMnSc1	baron
Franz	Franz	k1gMnSc1	Franz
Xaver	Xavera	k1gFnPc2	Xavera
von	von	k1gInSc1	von
Zach	Zach	k1gMnSc1	Zach
skupinu	skupina	k1gFnSc4	skupina
24	[number]	k4	24
astronomů	astronom	k1gMnPc2	astronom
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
najít	najít	k5eAaPmF	najít
chybějící	chybějící	k2eAgFnSc4d1	chybějící
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
předpokladu	předpoklad	k1gInSc2	předpoklad
Titius-Bodeova	Titius-Bodeův	k2eAgInSc2d1	Titius-Bodeův
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
nacházet	nacházet	k5eAaImF	nacházet
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2,8	[number]	k4	2,8
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgInS	být
úspěch	úspěch	k1gInSc1	úspěch
Williama	William	k1gMnSc2	William
Herschela	Herschel	k1gMnSc2	Herschel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
objevil	objevit	k5eAaPmAgMnS	objevit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
stejného	stejný	k2eAgInSc2d1	stejný
zákona	zákon	k1gInSc2	zákon
planetu	planeta	k1gFnSc4	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
připravit	připravit	k5eAaPmF	připravit
ručně	ručně	k6eAd1	ručně
kreslené	kreslený	k2eAgFnPc4d1	kreslená
mapy	mapa	k1gFnPc4	mapa
oblohy	obloha	k1gFnSc2	obloha
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
hvězdy	hvězda	k1gFnPc4	hvězda
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
až	až	k9	až
po	po	k7c4	po
dohodnutou	dohodnutý	k2eAgFnSc4d1	dohodnutá
limitní	limitní	k2eAgFnSc4d1	limitní
magnitudu	magnituda	k1gFnSc4	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgFnPc2d1	následující
nocí	noc	k1gFnPc2	noc
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
obloha	obloha	k1gFnSc1	obloha
zmapována	zmapován	k2eAgFnSc1d1	zmapována
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cílem	cíl	k1gInSc7	cíl
pátrání	pátrání	k1gNnSc2	pátrání
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nějaký	nějaký	k3yIgInSc4	nějaký
neznámý	známý	k2eNgInSc4d1	neznámý
pohybující	pohybující	k2eAgInSc4d1	pohybující
se	se	k3xPyFc4	se
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
nové	nový	k2eAgFnSc2d1	nová
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
činit	činit	k5eAaImF	činit
asi	asi	k9	asi
30	[number]	k4	30
úhlových	úhlový	k2eAgFnPc2d1	úhlová
vteřin	vteřina	k1gFnPc2	vteřina
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
pátrání	pátrání	k1gNnSc2	pátrání
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
planeta	planeta	k1gFnSc1	planeta
nýbrž	nýbrž	k8xC	nýbrž
asteroid	asteroid	k1gInSc4	asteroid
1	[number]	k4	1
Ceres	ceres	k1gInSc1	ceres
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
další	další	k2eAgFnPc1d1	další
asteroidy	asteroida	k1gFnPc1	asteroida
<g/>
:	:	kIx,	:
2	[number]	k4	2
Pallas	Pallas	k1gMnSc1	Pallas
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
Juno	Juno	k1gFnPc2	Juno
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
Vesta	vesta	k1gFnSc1	vesta
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
asteroidech	asteroid	k1gInPc6	asteroid
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
Karl	Karl	k1gMnSc1	Karl
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hencke	Hencke	k1gFnSc1	Hencke
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
15	[number]	k4	15
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
objevil	objevit	k5eAaPmAgInS	objevit
asteroid	asteroid	k1gInSc4	asteroid
5	[number]	k4	5
Astraea	Astraeum	k1gNnSc2	Astraeum
<g/>
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
našel	najít	k5eAaPmAgInS	najít
asteroid	asteroid	k1gInSc1	asteroid
6	[number]	k4	6
Hebe	Hebe	k1gFnPc2	Hebe
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pátrání	pátrání	k1gNnSc3	pátrání
se	se	k3xPyFc4	se
zanedlouho	zanedlouho	k6eAd1	zanedlouho
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
astronomové	astronom	k1gMnPc1	astronom
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
objeven	objevit	k5eAaPmNgInS	objevit
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
nový	nový	k2eAgInSc1d1	nový
asteroid	asteroid	k1gInSc1	asteroid
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
"	"	kIx"	"
<g/>
hledače	hledač	k1gInPc4	hledač
<g/>
"	"	kIx"	"
asteroidů	asteroid	k1gInPc2	asteroid
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Russell	Russell	k1gMnSc1	Russell
Hind	hind	k1gMnSc1	hind
<g/>
,	,	kIx,	,
Annibale	Annibal	k1gInSc6	Annibal
de	de	k?	de
Gasparis	Gasparis	k1gInSc1	Gasparis
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Theodor	Theodor	k1gMnSc1	Theodor
Robert	Robert	k1gMnSc1	Robert
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
Mayer	Mayer	k1gMnSc1	Mayer
Salomon	Salomon	k1gMnSc1	Salomon
Goldschmidt	Goldschmidt	k1gMnSc1	Goldschmidt
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Chacornac	Chacornac	k1gFnSc1	Chacornac
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Ferguson	Ferguson	k1gMnSc1	Ferguson
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Robert	Robert	k1gMnSc1	Robert
Pogson	Pogson	k1gMnSc1	Pogson
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leberecht	Leberecht	k1gMnSc1	Leberecht
Tempel	Tempel	k1gMnSc1	Tempel
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Craig	Craig	k1gMnSc1	Craig
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Heinrich	Heinrich	k1gMnSc1	Heinrich
Friedrich	Friedrich	k1gMnSc1	Friedrich
Peters	Peters	k1gInSc1	Peters
<g/>
,	,	kIx,	,
Alphonse	Alphonse	k1gFnSc1	Alphonse
Louis	Louis	k1gMnSc1	Louis
Nicolas	Nicolas	k1gMnSc1	Nicolas
Borrelly	Borrella	k1gFnSc2	Borrella
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Palisa	Palisa	k1gFnSc1	Palisa
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
Prosper	Prosper	k1gMnSc1	Prosper
Henry	Henry	k1gMnSc1	Henry
a	a	k8xC	a
Auguste	August	k1gMnSc5	August
Charlois	Charlois	k1gFnSc1	Charlois
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
použil	použít	k5eAaPmAgMnS	použít
Maximilian	Maximilian	k1gMnSc1	Maximilian
Franz	Franz	k1gMnSc1	Franz
Joseph	Joseph	k1gMnSc1	Joseph
Cornelius	Cornelius	k1gMnSc1	Cornelius
Wolf	Wolf	k1gMnSc1	Wolf
poprvé	poprvé	k6eAd1	poprvé
astrofotografii	astrofotografie	k1gFnSc4	astrofotografie
na	na	k7c4	na
zjišťování	zjišťování	k1gNnSc4	zjišťování
přítomnosti	přítomnost	k1gFnSc2	přítomnost
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
expozice	expozice	k1gFnSc2	expozice
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
proužky	proužek	k1gInPc4	proužek
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
fotografické	fotografický	k2eAgFnSc6d1	fotografická
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
předchozími	předchozí	k2eAgInPc7d1	předchozí
výrazně	výrazně	k6eAd1	výrazně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
nových	nový	k2eAgInPc6d1	nový
asteroidech	asteroid	k1gInPc6	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Wolf	Wolf	k1gMnSc1	Wolf
jich	on	k3xPp3gFnPc2	on
objevil	objevit	k5eAaPmAgMnS	objevit
celkem	celkem	k6eAd1	celkem
248	[number]	k4	248
počínaje	počínaje	k7c7	počínaje
323	[number]	k4	323
Brucia	Brucium	k1gNnSc2	Brucium
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
identifikován	identifikován	k2eAgInSc1d1	identifikován
stále	stále	k6eAd1	stále
jen	jen	k9	jen
zlomek	zlomek	k1gInSc1	zlomek
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
existujících	existující	k2eAgInPc2d1	existující
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
příliš	příliš	k6eAd1	příliš
nezabývali	zabývat	k5eNaImAgMnP	zabývat
a	a	k8xC	a
nazývali	nazývat	k5eAaImAgMnP	nazývat
je	on	k3xPp3gMnPc4	on
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nebeská	nebeský	k2eAgFnSc1d1	nebeská
havěť	havěť	k1gFnSc1	havěť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnPc1d1	moderní
metody	metoda	k1gFnPc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Asteroidy	asteroida	k1gFnPc1	asteroida
byly	být	k5eAaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
příchod	příchod	k1gInSc1	příchod
automatizovaných	automatizovaný	k2eAgInPc2d1	automatizovaný
systémů	systém	k1gInPc2	systém
<g/>
)	)	kIx)	)
hledány	hledán	k2eAgFnPc4d1	hledána
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
vyfotografovala	vyfotografovat	k5eAaPmAgFnS	vyfotografovat
část	část	k1gFnSc1	část
oblohy	obloha	k1gFnSc2	obloha
širokoúhlým	širokoúhlý	k2eAgInSc7d1	širokoúhlý
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
byly	být	k5eAaImAgFnP	být
pořízeny	pořízen	k2eAgInPc1d1	pořízen
dvě	dva	k4xCgFnPc1	dva
fotografie	fotografia	k1gFnPc1	fotografia
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
párové	párový	k2eAgFnPc1d1	párová
fotografie	fotografia	k1gFnPc1	fotografia
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
pomocí	pomocí	k7c2	pomocí
stereoskopu	stereoskop	k1gInSc2	stereoskop
<g/>
.	.	kIx.	.
</s>
<s>
Obrázek	obrázek	k1gInSc1	obrázek
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
tělesa	těleso	k1gNnSc2	těleso
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
stereoskopem	stereoskop	k1gInSc7	stereoskop
jevil	jevit	k5eAaImAgMnS	jevit
oproti	oproti	k7c3	oproti
hvězdám	hvězda	k1gFnPc3	hvězda
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
jako	jako	k8xC	jako
nepatrně	nepatrně	k6eAd1	nepatrně
"	"	kIx"	"
<g/>
plavající	plavající	k2eAgFnSc3d1	plavající
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
identifikovat	identifikovat	k5eAaBmF	identifikovat
pohybující	pohybující	k2eAgNnSc4d1	pohybující
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
přesně	přesně	k6eAd1	přesně
změřena	změřit	k5eAaPmNgFnS	změřit
použitím	použití	k1gNnSc7	použití
digitalizačního	digitalizační	k2eAgInSc2d1	digitalizační
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pozice	pozice	k1gFnSc1	pozice
se	se	k3xPyFc4	se
měřila	měřit	k5eAaImAgFnS	měřit
relativně	relativně	k6eAd1	relativně
vůči	vůči	k7c3	vůči
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
poloha	poloha	k1gFnSc1	poloha
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgInPc4	tento
tři	tři	k4xCgInPc4	tři
kroky	krok	k1gInPc4	krok
však	však	k9	však
ještě	ještě	k6eAd1	ještě
neznamenaly	znamenat	k5eNaImAgFnP	znamenat
objev	objev	k1gInSc4	objev
asteroidu	asteroid	k1gInSc2	asteroid
<g/>
:	:	kIx,	:
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
totiž	totiž	k9	totiž
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
našel	najít	k5eAaPmAgInS	najít
úkaz	úkaz	k1gInSc1	úkaz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
dočasné	dočasný	k2eAgNnSc4d1	dočasné
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
zaslání	zaslání	k1gNnSc1	zaslání
pozic	pozice	k1gFnPc2	pozice
a	a	k8xC	a
časů	čas	k1gInPc2	čas
měření	měření	k1gNnSc2	měření
Brianu	Brian	k1gMnSc3	Brian
Marsdenovi	Marsden	k1gMnSc3	Marsden
z	z	k7c2	z
Centra	centrum	k1gNnSc2	centrum
planetek	planetka	k1gFnPc2	planetka
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Minor	minor	k2eAgFnPc2d1	minor
Planet	planeta	k1gFnPc2	planeta
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Marsden	Marsdna	k1gFnPc2	Marsdna
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
počítačové	počítačový	k2eAgInPc4d1	počítačový
programy	program	k1gInPc4	program
schopné	schopný	k2eAgInPc4d1	schopný
vypočítat	vypočítat	k5eAaPmF	vypočítat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tento	tento	k3xDgInSc1	tento
úkaz	úkaz	k1gInSc1	úkaz
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
ucelené	ucelený	k2eAgFnSc6d1	ucelená
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
úkazu	úkaz	k1gInSc2	úkaz
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
právo	práv	k2eAgNnSc1d1	právo
jej	on	k3xPp3gMnSc4	on
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dostane	dostat	k5eAaPmIp3nS	dostat
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pojmenování	pojmenování	k1gNnSc2	pojmenování
objevených	objevený	k2eAgInPc2d1	objevený
asteroidů	asteroid	k1gInPc2	asteroid
===	===	k?	===
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
asteroidu	asteroid	k1gInSc2	asteroid
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
tento	tento	k3xDgInSc4	tento
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
i	i	k9	i
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
1	[number]	k4	1
Ceres	ceres	k1gInSc4	ceres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
prvních	první	k4xOgInPc2	první
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
postavách	postava	k1gFnPc6	postava
z	z	k7c2	z
řecko-římské	řecko-římský	k2eAgFnSc2d1	řecko-římská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
i	i	k8xC	i
jména	jméno	k1gNnPc1	jméno
slavných	slavný	k2eAgFnPc2d1	slavná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
manželek	manželka	k1gFnPc2	manželka
objevitelů	objevitel	k1gMnPc2	objevitel
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
televizních	televizní	k2eAgFnPc2d1	televizní
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Nepsaná	psaný	k2eNgFnSc1d1	psaný
tradice	tradice	k1gFnSc1	tradice
udělování	udělování	k1gNnPc2	udělování
pouze	pouze	k6eAd1	pouze
ženských	ženský	k2eAgNnPc2d1	ženské
jmen	jméno	k1gNnPc2	jméno
pomyslně	pomyslně	k6eAd1	pomyslně
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k6eAd1	až
asteroidem	asteroid	k1gInSc7	asteroid
334	[number]	k4	334
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Občasné	občasný	k2eAgNnSc1d1	občasné
pojmenovávání	pojmenovávání	k1gNnSc1	pojmenovávání
ženskými	ženská	k1gFnPc7	ženská
jmény	jméno	k1gNnPc7	jméno
však	však	k9	však
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asteroid	asteroid	k1gInSc1	asteroid
číslo	číslo	k1gNnSc1	číslo
9822	[number]	k4	9822
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
Observatoří	observatoř	k1gFnSc7	observatoř
Mt	Mt	k1gFnPc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Palomar	Palomar	k1gMnSc1	Palomar
<g/>
,	,	kIx,	,
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
názvem	název	k1gInSc7	název
(	(	kIx(	(
<g/>
9822	[number]	k4	9822
<g/>
)	)	kIx)	)
Hajduková	Hajdukový	k2eAgFnSc1d1	Hajduková
4114	[number]	k4	4114
T	T	kA	T
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
podle	podle	k7c2	podle
slovenské	slovenský	k2eAgFnSc2d1	slovenská
astronomky	astronomka	k1gFnSc2	astronomka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Márie	Márie	k1gFnSc1	Márie
Hajdukové	Hajdukové	k?	Hajdukové
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
"	"	kIx"	"
<g/>
české	český	k2eAgFnPc1d1	Česká
stopy	stopa	k1gFnPc1	stopa
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
(	(	kIx(	(
<g/>
3336	[number]	k4	3336
<g/>
)	)	kIx)	)
Grygar	Grygar	k1gMnSc1	Grygar
a	a	k8xC	a
(	(	kIx(	(
<g/>
3337	[number]	k4	3337
<g/>
)	)	kIx)	)
Miloš	Miloš	k1gMnSc1	Miloš
objevené	objevený	k2eAgFnSc2d1	objevená
J.	J.	kA	J.
Kohoutkem	kohoutek	k1gInSc7	kohoutek
r.	r.	kA	r.
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
6149	[number]	k4	6149
<g/>
)	)	kIx)	)
Pelčák	Pelčák	k1gInSc1	Pelčák
objevená	objevený	k2eAgFnSc1d1	objevená
A.	A.	kA	A.
<g/>
Mrkosem	Mrkos	k1gMnSc7	Mrkos
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
7532	[number]	k4	7532
<g/>
)	)	kIx)	)
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
M.	M.	kA	M.
<g/>
Tichý	tichý	k2eAgInSc4d1	tichý
r.	r.	kA	r.
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
7739	[number]	k4	7739
<g/>
)	)	kIx)	)
Čech	Čechy	k1gFnPc2	Čechy
-	-	kIx~	-
L.	L.	kA	L.
<g/>
Brožek	brožka	k1gFnPc2	brožka
r.	r.	kA	r.
<g/>
1982	[number]	k4	1982
....	....	k?	....
z	z	k7c2	z
novější	nový	k2eAgFnSc2d2	novější
doby	doba	k1gFnSc2	doba
např.	např.	kA	např.
(	(	kIx(	(
<g/>
10634	[number]	k4	10634
<g/>
)	)	kIx)	)
Pepibican	Pepibican	k1gInSc1	Pepibican
a	a	k8xC	a
(	(	kIx(	(
<g/>
10919	[number]	k4	10919
<g/>
)	)	kIx)	)
Pepikzicha	Pepikzich	k1gMnSc4	Pepikzich
od	od	k7c2	od
L.	L.	kA	L.
<g/>
Šarounové	Šarounový	k2eAgInPc1d1	Šarounový
(	(	kIx(	(
<g/>
další	další	k2eAgInPc1d1	další
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
stránce	stránka	k1gFnSc6	stránka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
skupin	skupina	k1gFnPc2	skupina
asteroidů	asteroid	k1gInPc2	asteroid
nese	nést	k5eAaImIp3nS	nést
jména	jméno	k1gNnPc4	jméno
s	s	k7c7	s
klasickým	klasický	k2eAgInSc7d1	klasický
námětem	námět	k1gInSc7	námět
jako	jako	k8xC	jako
např.	např.	kA	např.
Centauri	Centaur	k1gFnPc4	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
asteroidy	asteroida	k1gFnPc1	asteroida
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
obíhajícím	obíhající	k2eAgInSc6d1	obíhající
mezi	mezi	k7c7	mezi
Saturnem	Saturn	k1gInSc7	Saturn
a	a	k8xC	a
Neptunem	Neptun	k1gInSc7	Neptun
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
pojmenované	pojmenovaný	k2eAgFnPc1d1	pojmenovaná
podle	podle	k7c2	podle
legendárních	legendární	k2eAgMnPc2d1	legendární
kentaurů	kentaur	k1gMnPc2	kentaur
a	a	k8xC	a
hrdinů	hrdina	k1gMnPc2	hrdina
z	z	k7c2	z
Trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
nové	nový	k2eAgFnSc2d1	nová
skupiny	skupina	k1gFnSc2	skupina
asteroidů	asteroid	k1gInPc2	asteroid
Apoheles	Apoheles	k1gInSc1	Apoheles
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
technologie	technologie	k1gFnSc1	technologie
<g/>
:	:	kIx,	:
hledání	hledání	k1gNnSc1	hledání
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
asteroidů	asteroid	k1gInPc2	asteroid
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
zájem	zájem	k1gInSc1	zájem
identifikovat	identifikovat	k5eAaBmF	identifikovat
asteroidy	asteroid	k1gInPc4	asteroid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
protínají	protínat	k5eAaImIp3nP	protínat
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
tedy	tedy	k9	tedy
existuje	existovat	k5eAaImIp3nS	existovat
riziko	riziko	k1gNnSc4	riziko
srážky	srážka	k1gFnSc2	srážka
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tři	tři	k4xCgFnPc4	tři
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
skupiny	skupina	k1gFnPc4	skupina
asteroidů	asteroid	k1gInPc2	asteroid
blízko	blízko	k7c2	blízko
Země	zem	k1gFnSc2	zem
patří	patřit	k5eAaImIp3nS	patřit
Apollo	Apollo	k1gNnSc4	Apollo
<g/>
,	,	kIx,	,
asteroidy	asteroid	k1gInPc4	asteroid
Amors	Amorsa	k1gFnPc2	Amorsa
a	a	k8xC	a
asteroidy	asteroid	k1gInPc4	asteroid
Athens	Athensa	k1gFnPc2	Athensa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
asteroidů	asteroid	k1gInPc2	asteroid
blízko	blízko	k7c2	blízko
Země	zem	k1gFnSc2	zem
patří	patřit	k5eAaImIp3nS	patřit
í	í	k0	í
433	[number]	k4	433
Eros	Eros	k1gMnSc1	Eros
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
byla	být	k5eAaImAgNnP	být
objevena	objevit	k5eAaPmNgNnP	objevit
srovnatelná	srovnatelný	k2eAgNnPc1d1	srovnatelné
tělesa	těleso	k1gNnPc1	těleso
–	–	k?	–
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
byly	být	k5eAaImAgInP	být
objevené	objevený	k2eAgInPc1d1	objevený
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
1221	[number]	k4	1221
Amor	Amor	k1gMnSc1	Amor
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
2101	[number]	k4	2101
Adonis	Adonis	k1gFnPc2	Adonis
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
69230	[number]	k4	69230
Hermes	Hermes	k1gMnSc1	Hermes
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
pouhých	pouhý	k2eAgInPc2d1	pouhý
0,005	[number]	k4	0,005
AU	au	k0	au
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
události	událost	k1gFnPc1	událost
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
desetiletích	desetiletí	k1gNnPc6	desetiletí
ještě	ještě	k6eAd1	ještě
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
stupeň	stupeň	k1gInSc4	stupeň
znepokojení	znepokojení	k1gNnSc2	znepokojení
<g/>
:	:	kIx,	:
zvyšující	zvyšující	k2eAgFnSc2d1	zvyšující
se	se	k3xPyFc4	se
akceptace	akceptace	k1gFnSc2	akceptace
teorie	teorie	k1gFnSc2	teorie
Waltera	Walter	k1gMnSc2	Walter
Alvareze	Alvareze	k1gFnSc2	Alvareze
o	o	k7c6	o
vyhubení	vyhubení	k1gNnSc6	vyhubení
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
následkem	následkem	k7c2	následkem
dopadu	dopad	k1gInSc2	dopad
asteroidu	asteroid	k1gInSc2	asteroid
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
a	a	k8xC	a
pozorování	pozorování	k1gNnSc4	pozorování
srážky	srážka	k1gFnSc2	srážka
komety	kometa	k1gFnSc2	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
9	[number]	k4	9
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jiné	jiný	k2eAgFnPc1d1	jiná
události	událost	k1gFnPc1	událost
podnítily	podnítit	k5eAaPmAgFnP	podnítit
spuštění	spuštění	k1gNnSc4	spuštění
vysoce	vysoce	k6eAd1	vysoce
efektivních	efektivní	k2eAgInPc2d1	efektivní
automatických	automatický	k2eAgInPc2d1	automatický
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
sestávajících	sestávající	k2eAgInPc2d1	sestávající
z	z	k7c2	z
CCD	CCD	kA	CCD
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Charge-Coupled	Charge-Coupled	k1gInSc1	Charge-Coupled
Device	device	k1gInPc2	device
<g/>
)	)	kIx)	)
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
a	a	k8xC	a
počítačů	počítač	k1gInPc2	počítač
napojených	napojený	k2eAgFnPc2d1	napojená
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
dalekohledy	dalekohled	k1gInPc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
těmito	tento	k3xDgFnPc7	tento
systémy	systém	k1gInPc4	systém
objevena	objeven	k2eAgFnSc1d1	objevena
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lincoln	Lincoln	k1gMnSc1	Lincoln
Near-Earth	Near-Earth	k1gMnSc1	Near-Earth
Asteroid	asteroida	k1gFnPc2	asteroida
Research	Research	k1gMnSc1	Research
(	(	kIx(	(
<g/>
LINEAR	LINEAR	kA	LINEAR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Near-Earth	Near-Earth	k1gInSc1	Near-Earth
Asteroid	asteroid	k1gInSc1	asteroid
Tracking	Tracking	k1gInSc1	Tracking
(	(	kIx(	(
<g/>
NEAT	NEAT	kA	NEAT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spacewatch	Spacewatch	k1gMnSc1	Spacewatch
</s>
</p>
<p>
<s>
Lowell	Lowelnout	k5eAaPmAgMnS	Lowelnout
Observatory	Observator	k1gMnPc4	Observator
Near-Earth-Object	Near-Earth-Object	k1gMnSc1	Near-Earth-Object
Search	Search	k1gMnSc1	Search
(	(	kIx(	(
<g/>
LONEOS	LONEOS	kA	LONEOS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Catalina	Catalin	k2eAgFnSc1d1	Catalina
Sky	Sky	k1gFnSc1	Sky
Survey	Survea	k1gFnSc2	Survea
</s>
</p>
<p>
<s>
Campo	Campa	k1gFnSc5	Campa
Imperatore	Imperator	k1gMnSc5	Imperator
Near-Earth	Near-Earth	k1gInSc1	Near-Earth
Objects	Objects	k1gInSc4	Objects
Survey	Survea	k1gFnSc2	Survea
(	(	kIx(	(
<g/>
CINEOS	CINEOS	kA	CINEOS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Japanese	Japanese	k1gFnSc1	Japanese
Spaceguard	Spaceguarda	k1gFnPc2	Spaceguarda
Association	Association	k1gInSc4	Association
</s>
</p>
<p>
<s>
Asiago-DLR	Asiago-DLR	k?	Asiago-DLR
Asteroid	asteroid	k1gInSc1	asteroid
SurveySám	SurveySa	k1gFnPc3	SurveySa
systém	systém	k1gInSc1	systém
LINEAR	LINEAR	kA	LINEAR
objevil	objevit	k5eAaPmAgInS	objevit
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
více	hodně	k6eAd2	hodně
než	než	k8xS	než
84	[number]	k4	84
764	[number]	k4	764
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
136	[number]	k4	136
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Nicméně	nicméně	k8xC	nicméně
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
asteroidům	asteroid	k1gInPc3	asteroid
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
efektivní	efektivní	k2eAgFnPc1d1	efektivní
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průzkum	průzkum	k1gInSc1	průzkum
asteroidů	asteroid	k1gInPc2	asteroid
==	==	k?	==
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
byly	být	k5eAaImAgInP	být
asteroidy	asteroid	k1gInPc1	asteroid
i	i	k9	i
těmi	ten	k3xDgInPc7	ten
největšími	veliký	k2eAgInPc7d3	veliký
dalekohledy	dalekohled	k1gInPc7	dalekohled
vidět	vidět	k5eAaImF	vidět
jako	jako	k8xS	jako
pouhé	pouhý	k2eAgInPc4d1	pouhý
světelné	světelný	k2eAgInPc4d1	světelný
body	bod	k1gInPc4	bod
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
povrch	povrch	k6eAd1wR	povrch
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
záhadou	záhada	k1gFnSc7	záhada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
fotografie	fotografia	k1gFnPc1	fotografia
objektů	objekt	k1gInPc2	objekt
podobných	podobný	k2eAgInPc2d1	podobný
asteroidům	asteroid	k1gInPc3	asteroid
byla	být	k5eAaImAgFnS	být
zblízka	zblízka	k6eAd1	zblízka
pořízena	pořízen	k2eAgFnSc1d1	pořízena
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
vyfotografovala	vyfotografovat	k5eAaPmAgFnS	vyfotografovat
Phobos	Phobos	k1gInSc4	Phobos
a	a	k8xC	a
Deimos	Deimos	k1gInSc4	Deimos
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
malé	malý	k2eAgInPc4d1	malý
měsíce	měsíc	k1gInPc4	měsíc
planety	planeta	k1gFnSc2	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
gravitačně	gravitačně	k6eAd1	gravitačně
zachycenými	zachycený	k2eAgInPc7d1	zachycený
asteroidy	asteroid	k1gInPc7	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
fotografie	fotografia	k1gFnPc1	fotografia
odhalily	odhalit	k5eAaPmAgFnP	odhalit
jejich	jejich	k3xOp3gNnSc4	jejich
velmi	velmi	k6eAd1	velmi
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
skutečným	skutečný	k2eAgInSc7d1	skutečný
<g/>
,	,	kIx,	,
zblízka	zblízka	k6eAd1	zblízka
vyfotografovaným	vyfotografovaný	k2eAgInSc7d1	vyfotografovaný
asteroidem	asteroid	k1gInSc7	asteroid
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
951	[number]	k4	951
<g/>
)	)	kIx)	)
Gaspra	Gaspr	k1gMnSc2	Gaspr
následovaný	následovaný	k2eAgInSc1d1	následovaný
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
asteroidem	asteroid	k1gInSc7	asteroid
(	(	kIx(	(
<g/>
243	[number]	k4	243
<g/>
)	)	kIx)	)
Ida	Ida	k1gFnSc1	Ida
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
souputníkem	souputník	k1gMnSc7	souputník
Dactyl	Dactyl	k1gInSc1	Dactyl
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
byly	být	k5eAaImAgInP	být
vyfotografovány	vyfotografovat	k5eAaPmNgInP	vyfotografovat
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
zaměřenou	zaměřený	k2eAgFnSc7d1	zaměřená
na	na	k7c4	na
asteroidy	asteroid	k1gInPc4	asteroid
byla	být	k5eAaImAgFnS	být
NEAR	NEAR	kA	NEAR
Shoemaker	Shoemaker	k1gInSc4	Shoemaker
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyfotografovala	vyfotografovat	k5eAaPmAgFnS	vyfotografovat
asteroid	asteroid	k1gInSc4	asteroid
(	(	kIx(	(
<g/>
253	[number]	k4	253
<g/>
)	)	kIx)	)
Mathilde	Mathild	k1gMnSc5	Mathild
<g/>
,	,	kIx,	,
než	než	k8xS	než
začala	začít	k5eAaPmAgFnS	začít
obíhat	obíhat	k5eAaImF	obíhat
kolem	kolem	k7c2	kolem
asteroidu	asteroid	k1gInSc2	asteroid
(	(	kIx(	(
<g/>
433	[number]	k4	433
<g/>
)	)	kIx)	)
Eros	Eros	k1gMnSc1	Eros
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
roku	rok	k1gInSc6	rok
2001	[number]	k4	2001
i	i	k9	i
přistála	přistát	k5eAaPmAgFnS	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
asteroidy	asteroid	k1gInPc4	asteroid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
krátce	krátce	k6eAd1	krátce
sledovány	sledovat	k5eAaImNgInP	sledovat
sondami	sonda	k1gFnPc7	sonda
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
(	(	kIx(	(
<g/>
9969	[number]	k4	9969
<g/>
)	)	kIx)	)
Braille	Braille	k1gFnPc1	Braille
–	–	k?	–
sondou	sonda	k1gFnSc7	sonda
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gFnSc2	Space
1	[number]	k4	1
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
a	a	k8xC	a
(	(	kIx(	(
<g/>
5535	[number]	k4	5535
<g/>
)	)	kIx)	)
Annefrank	Annefrank	k1gInSc1	Annefrank
–	–	k?	–
sondou	sonda	k1gFnSc7	sonda
Stardust	Stardust	k1gMnSc1	Stardust
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střetnutí	střetnutí	k1gNnSc1	střetnutí
s	s	k7c7	s
asteroidy	asteroid	k1gInPc4	asteroid
měla	mít	k5eAaImAgFnS	mít
naplánované	naplánovaný	k2eAgInPc4d1	naplánovaný
i	i	k9	i
sonda	sonda	k1gFnSc1	sonda
Rosetta	Rosetto	k1gNnSc2	Rosetto
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vypuštěná	vypuštěný	k2eAgFnSc1d1	vypuštěná
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
sledovala	sledovat	k5eAaImAgFnS	sledovat
asteroid	asteroid	k1gInSc1	asteroid
(	(	kIx(	(
<g/>
2867	[number]	k4	2867
<g/>
)	)	kIx)	)
Šteins	Šteins	k1gInSc1	Šteins
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
asteroidu	asteroid	k1gInSc3	asteroid
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
Lutetia	Lutetium	k1gNnSc2	Lutetium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srážky	srážka	k1gFnPc1	srážka
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
geologické	geologický	k2eAgFnSc6d1	geologická
minulosti	minulost	k1gFnSc6	minulost
==	==	k?	==
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohokrát	mnohokrát	k6eAd1	mnohokrát
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
srážkám	srážka	k1gFnPc3	srážka
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
asteroidy	asteroid	k1gInPc7	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vymírání	vymírání	k1gNnSc4	vymírání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
zřejmě	zřejmě	k6eAd1	zřejmě
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
dopad	dopad	k1gInSc4	dopad
desetikilometrové	desetikilometrový	k2eAgFnSc2d1	desetikilometrová
planetky	planetka	k1gFnSc2	planetka
Chicxulub	Chicxuluba	k1gFnPc2	Chicxuluba
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
současného	současný	k2eAgInSc2d1	současný
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Planetka	planetka	k1gFnSc1	planetka
vážila	vážit	k5eAaImAgFnS	vážit
asi	asi	k9	asi
8	[number]	k4	8
bilionů	bilion	k4xCgInPc2	bilion
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
objem	objem	k1gInSc4	objem
kolem	kolem	k7c2	kolem
2600	[number]	k4	2600
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Srazila	srazit	k5eAaPmAgFnS	srazit
se	se	k3xPyFc4	se
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mělkého	mělký	k2eAgNnSc2d1	mělké
moře	moře	k1gNnSc2	moře
při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
kolem	kolem	k7c2	kolem
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Asteroid	asteroida	k1gFnPc2	asteroida
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
planetka	planetka	k1gFnSc1	planetka
</s>
</p>
<p>
<s>
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
astronomie	astronomie	k1gFnSc1	astronomie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
asteroid	asteroid	k1gInSc1	asteroid
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
asteroid	asteroida	k1gFnPc2	asteroida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Planetoidy	planetoida	k1gFnSc2	planetoida
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Kowalski	Kowalsk	k1gFnSc2	Kowalsk
<g/>
:	:	kIx,	:
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
planetek	planetka	k1gFnPc2	planetka
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
L.	L.	kA	L.
Hilton	Hilton	k1gInSc1	Hilton
<g/>
:	:	kIx,	:
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
asteroidů	asteroid	k1gInPc2	asteroid
staly	stát	k5eAaPmAgFnP	stát
planetky	planetka	k1gFnPc1	planetka
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
L.	L.	kA	L.
Hilton	Hilton	k1gInSc1	Hilton
<g/>
:	:	kIx,	:
When	When	k1gInSc1	When
Did	Did	k1gFnSc2	Did
the	the	k?	the
Asteroids	Asteroids	k1gInSc1	Asteroids
Become	Becom	k1gInSc5	Becom
Minor	minor	k2eAgInSc7d1	minor
Planets	Planets	k1gInSc4	Planets
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
anglický	anglický	k2eAgInSc1d1	anglický
originál	originál	k1gInSc1	originál
(	(	kIx(	(
<g/>
html	html	k1gInSc1	html
<g/>
)	)	kIx)	)
</s>
</p>
