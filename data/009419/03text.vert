<p>
<s>
Lyska	lyska	k1gFnSc1	lyska
černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
Fulica	Fulica	k1gMnSc1	Fulica
atra	atra	k1gMnSc1	atra
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Fulica	Fulica	k1gMnSc1	Fulica
prior	prior	k1gMnSc1	prior
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
plovavý	plovavý	k2eAgMnSc1d1	plovavý
pták	pták	k1gMnSc1	pták
široce	široko	k6eAd1	široko
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapka	mapka	k1gFnSc1	mapka
níže	níže	k1gFnSc2	níže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
37	[number]	k4	37
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
70	[number]	k4	70
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
0,6	[number]	k4	0,6
<g/>
–	–	k?	–
<g/>
1,2	[number]	k4	1,2
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Lyska	lyska	k1gFnSc1	lyska
černá	černý	k2eAgFnSc1d1	černá
má	mít	k5eAaImIp3nS	mít
robustní	robustní	k2eAgNnSc4d1	robustní
tělo	tělo	k1gNnSc4	tělo
celé	celá	k1gFnSc2	celá
porostlé	porostlý	k2eAgFnSc2d1	porostlá
lesklým	lesklý	k2eAgNnSc7d1	lesklé
černým	černý	k2eAgNnSc7d1	černé
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgFnPc4d1	odlišná
barvy	barva	k1gFnPc4	barva
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
bílý	bílý	k2eAgInSc1d1	bílý
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
zobák	zobák	k1gInSc1	zobák
a	a	k8xC	a
čelní	čelní	k2eAgInSc1d1	čelní
štítek	štítek	k1gInSc1	štítek
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
šedé	šedý	k2eAgFnPc1d1	šedá
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
nemá	mít	k5eNaImIp3nS	mít
plovací	plovací	k2eAgFnSc1d1	plovací
blány	blána	k1gFnPc1	blána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
kožní	kožní	k2eAgInSc4d1	kožní
lem	lem	k1gInSc4	lem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
je	být	k5eAaImIp3nS	být
slabě	slabě	k6eAd1	slabě
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
sexuální	sexuální	k2eAgInSc1d1	sexuální
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
něco	něco	k6eAd1	něco
větší	veliký	k2eAgInSc4d2	veliký
čelní	čelní	k2eAgInSc4d1	čelní
štítek	štítek	k1gInSc4	štítek
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
zbarvením	zbarvení	k1gNnSc7	zbarvení
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Nezralí	zralý	k2eNgMnPc1d1	nezralý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
barvitější	barvitý	k2eAgMnPc1d2	barvitější
<g/>
,	,	kIx,	,
postrádají	postrádat	k5eAaImIp3nP	postrádat
čelní	čelní	k2eAgInSc4d1	čelní
štítek	štítek	k1gInSc4	štítek
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
bíle	bíle	k6eAd1	bíle
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
přední	přední	k2eAgFnSc4d1	přední
část	část	k1gFnSc4	část
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Čelní	čelní	k2eAgInSc1d1	čelní
štítek	štítek	k1gInSc1	štítek
bývá	bývat	k5eAaImIp3nS	bývat
navíc	navíc	k6eAd1	navíc
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
než	než	k8xS	než
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
lyska	lyska	k1gFnSc1	lyska
navíc	navíc	k6eAd1	navíc
kývá	kývat	k5eAaImIp3nS	kývat
hlavou	hlava	k1gFnSc7	hlava
střídavě	střídavě	k6eAd1	střídavě
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
před	před	k7c7	před
potopením	potopení	k1gNnSc7	potopení
provádí	provádět	k5eAaImIp3nS	provádět
jakousi	jakýsi	k3yIgFnSc4	jakýsi
"	"	kIx"	"
<g/>
šipku	šipka	k1gFnSc4	šipka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Lyska	lyska	k1gFnSc1	lyska
černá	černá	k1gFnSc1	černá
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgMnSc7d1	typický
obyvatelem	obyvatel	k1gMnSc7	obyvatel
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
městech	město	k1gNnPc6	město
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
především	především	k9	především
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
na	na	k7c4	na
parková	parkový	k2eAgNnPc4d1	parkové
jezírka	jezírko	k1gNnPc4	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
tažná	tažný	k2eAgFnSc1d1	tažná
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
evropské	evropský	k2eAgFnSc2d1	Evropská
a	a	k8xC	a
asijské	asijský	k2eAgFnSc2d1	asijská
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
západ	západ	k1gInSc4	západ
a	a	k8xC	a
opouští	opouštět	k5eAaImIp3nS	opouštět
již	již	k6eAd1	již
zamrzlé	zamrzlý	k2eAgFnSc2d1	zamrzlá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
především	především	k9	především
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc7	druh
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
malí	malý	k2eAgMnPc1d1	malý
vodní	vodní	k2eAgMnPc1d1	vodní
živočichové	živočich	k1gMnPc1	živočich
nebo	nebo	k8xC	nebo
případě	případ	k1gInSc6	případ
části	část	k1gFnSc2	část
rostlin	rostlina	k1gFnPc2	rostlina
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vyplašena	vyplašen	k2eAgFnSc1d1	vyplašena
<g/>
,	,	kIx,	,
s	s	k7c7	s
cákáním	cákání	k1gNnSc7	cákání
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
rozbíhá	rozbíhat	k5eAaImIp3nS	rozbíhat
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
mohutně	mohutně	k6eAd1	mohutně
plácá	plácat	k5eAaImIp3nS	plácat
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
navodit	navodit	k5eAaPmF	navodit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
dobrého	dobrý	k2eAgMnSc4d1	dobrý
letce	letec	k1gMnSc4	letec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opak	opak	k1gInSc1	opak
je	být	k5eAaImIp3nS	být
pravdou	pravda	k1gFnSc7	pravda
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
při	při	k7c6	při
jejich	jejich	k3xOp3gFnPc6	jejich
mnohdy	mnohdy	k6eAd1	mnohdy
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
migracích	migrace	k1gFnPc6	migrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lyska	lyska	k1gFnSc1	lyska
černá	černý	k2eAgFnSc1d1	černá
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
poměrně	poměrně	k6eAd1	poměrně
hlučné	hlučný	k2eAgMnPc4d1	hlučný
ptáky	pták	k1gMnPc4	pták
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
škálou	škála	k1gFnSc7	škála
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
nebo	nebo	k8xC	nebo
vyplašení	vyplašení	k1gNnSc6	vyplašení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
ozývá	ozývat	k5eAaImIp3nS	ozývat
chraptivým	chraptivý	k2eAgInSc7d1	chraptivý
"	"	kIx"	"
<g/>
kev	kev	k?	kev
<g/>
"	"	kIx"	"
a	a	k8xC	a
samec	samec	k1gMnSc1	samec
ostrým	ostrý	k2eAgInSc7d1	ostrý
"	"	kIx"	"
<g/>
pix	pix	k?	pix
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
psssi	pssse	k1gFnSc4	pssse
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
lysky	lyska	k1gFnSc2	lyska
černé	černý	k2eAgFnSc2d1	černá
projevuje	projevovat	k5eAaImIp3nS	projevovat
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
agrese	agrese	k1gFnSc1	agrese
a	a	k8xC	a
masivní	masivní	k2eAgNnSc1d1	masivní
bránění	bránění	k1gNnSc1	bránění
svého	svůj	k3xOyFgNnSc2	svůj
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Ostrůvkovité	ostrůvkovitý	k2eAgNnSc1d1	ostrůvkovité
a	a	k8xC	a
kopcovité	kopcovitý	k2eAgNnSc1d1	kopcovité
hnízdo	hnízdo	k1gNnSc1	hnízdo
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nS	stavit
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
větviček	větvička	k1gFnPc2	větvička
a	a	k8xC	a
rákosu	rákos	k1gInSc2	rákos
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
umístěné	umístěný	k2eAgFnPc4d1	umístěná
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
buduje	budovat	k5eAaImIp3nS	budovat
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc1	jeden
hnízdo	hnízdo	k1gNnSc1	hnízdo
určené	určený	k2eAgNnSc1d1	určené
k	k	k7c3	k
přespání	přespání	k1gNnSc3	přespání
nebo	nebo	k8xC	nebo
odpočinku	odpočinek	k1gInSc3	odpočinek
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
května	květen	k1gInSc2	květen
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
6	[number]	k4	6
až	až	k9	až
15	[number]	k4	15
bíločerných	bíločerný	k2eAgNnPc2d1	bíločerné
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
po	po	k7c6	po
22	[number]	k4	22
dnech	den	k1gInPc6	den
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
krmena	krmen	k2eAgNnPc4d1	krmeno
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
dospělců	dospělec	k1gMnPc2	dospělec
objevuje	objevovat	k5eAaImIp3nS	objevovat
mezi	mezi	k7c7	mezi
brodivými	brodivý	k2eAgMnPc7d1	brodivý
ptáky	pták	k1gMnPc7	pták
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
agrese	agrese	k1gFnSc1	agrese
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
silného	silný	k2eAgNnSc2d1	silné
klování	klování	k1gNnSc2	klování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vychýlit	vychýlit	k5eAaPmF	vychýlit
až	až	k9	až
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc6	usmrcení
několika	několik	k4yIc2	několik
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
dlouhověký	dlouhověký	k2eAgInSc4d1	dlouhověký
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
početnost	početnost	k1gFnSc1	početnost
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
běžný	běžný	k2eAgInSc4d1	běžný
druh	druh	k1gInSc4	druh
vyskytující	vyskytující	k2eAgInSc4d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c4	na
76	[number]	k4	76
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
předmětem	předmět	k1gInSc7	předmět
Dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
africko-euroasijských	africkouroasijský	k2eAgMnPc2d1	africko-euroasijský
stěhovavých	stěhovavý	k2eAgMnPc2d1	stěhovavý
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
AEWA	AEWA	kA	AEWA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
lysky	lyska	k1gFnSc2	lyska
černé	černá	k1gFnSc2	černá
jsou	být	k5eAaImIp3nP	být
rozeznávány	rozeznáván	k2eAgInPc4d1	rozeznáván
4	[number]	k4	4
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fulica	Fulic	k2eAgFnSc1d1	Fulica
atra	atra	k1gFnSc1	atra
atra	atr	k2eAgFnSc1d1	atr
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Azory	azor	k1gInPc1	azor
<g/>
,	,	kIx,	,
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fulica	Fulic	k2eAgFnSc1d1	Fulica
atra	atr	k2eAgFnSc1d1	atr
lugubris	lugubris	k1gFnSc1	lugubris
–	–	k?	–
Jáva	Jáva	k1gFnSc1	Jáva
i	i	k8xC	i
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Fulica	Fulic	k2eAgFnSc1d1	Fulica
atra	atr	k2eAgFnSc1d1	atr
novaeguinea	novaeguinea	k1gFnSc1	novaeguinea
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Fulica	Fulic	k2eAgFnSc1d1	Fulica
atra	atr	k2eAgFnSc1d1	atr
australis	australis	k1gFnSc1	australis
–	–	k?	–
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Eurasian	Eurasian	k1gMnSc1	Eurasian
Coot	Coot	k1gMnSc1	Coot
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Łyska	Łyska	k1gFnSc1	Łyska
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lyska	lyska	k1gFnSc1	lyska
černá	černat	k5eAaImIp3nS	černat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
lyska	lyska	k1gFnSc1	lyska
černá	černat	k5eAaImIp3nS	černat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
