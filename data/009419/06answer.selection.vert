<s>
Odlišné	odlišný	k2eAgFnPc4d1	odlišná
barvy	barva	k1gFnPc4	barva
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
bílý	bílý	k2eAgInSc1d1	bílý
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
zobák	zobák	k1gInSc1	zobák
a	a	k8xC	a
čelní	čelní	k2eAgInSc1d1	čelní
štítek	štítek	k1gInSc1	štítek
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
šedé	šedý	k2eAgFnPc1d1	šedá
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
nemá	mít	k5eNaImIp3nS	mít
plovací	plovací	k2eAgFnSc1d1	plovací
blány	blána	k1gFnPc1	blána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
kožní	kožní	k2eAgInSc4d1	kožní
lem	lem	k1gInSc4	lem
<g/>
.	.	kIx.	.
</s>
