<s>
Džengiš	Džengiš	k1gInSc1
Čokusu	Čokus	k1gInSc2
</s>
<s>
Ж	Ж	k?
ч	ч	k?
Pohled	pohled	k1gInSc1
ze	z	k7c2
základního	základní	k2eAgInSc2d1
tábora	tábor	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
7439	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
4148	#num#	k4
m	m	kA
Seznamy	seznam	k1gInPc4
</s>
<s>
Sedmitisícovky	Sedmitisícovka	k1gFnPc1
#	#	kIx~
<g/>
64	#num#	k4
<g/>
Ultraprominentní	Ultraprominentní	k2eAgMnSc1d1
horySněžný	horySněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
#	#	kIx~
<g/>
2	#num#	k4
<g/>
Nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
asijských	asijský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
#	#	kIx~
<g/>
8	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc4
</s>
<s>
Asie	Asie	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
KyrgyzstánČína	KyrgyzstánČín	k1gMnSc2
Čína	Čína	k1gFnSc1
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Ťan-šan	Ťan-šan	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
80	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
DžengišČokusu	DžengišČokus	k1gMnSc3
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
1938	#num#	k4
<g/>
,	,	kIx,
L.	L.	kA
Gutman	Gutman	k1gMnSc1
nebo	nebo	k8xC
1956	#num#	k4
<g/>
,	,	kIx,
V.	V.	kA
Abalakov	Abalakov	k1gInSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Typ	typ	k1gInSc1
</s>
<s>
horský	horský	k2eAgInSc4d1
štít	štít	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dženiš	Dženiš	k1gInSc1
čokusu	čokus	k1gInSc2
nebo	nebo	k8xC
Tömür	Tömüra	k1gFnPc2
(	(	kIx(
<g/>
kyrgyzsky	kyrgyzsky	k6eAd1
Ж	Ж	k?
ч	ч	k?
<g/>
,	,	kIx,
Žeŋ	Žeŋ	k1gMnSc1
čokusu	čokus	k1gInSc2
<g/>
,	,	kIx,
ujgursky	ujgursky	k6eAd1
ت	ت	k?
<g/>
,	,	kIx,
transliterováno	transliterován	k2eAgNnSc1d1
Tömür	Tömür	k1gInSc4
<g/>
,	,	kIx,
čínsky	čínsky	k6eAd1
托	托	k?
<g/>
,	,	kIx,
pinyin	pinyin	k1gMnSc1
Tuō	Tuō	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ěr	ěr	k?
Fē	Fē	k1gInSc1
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
přepis	přepis	k1gInSc1
Tchuo-mu-er-feng	Tchuo-mu-er-fenga	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
pod	pod	k7c7
ruským	ruský	k2eAgInSc7d1
názvem	název	k1gInSc7
Pik	pik	k1gInSc1
Pobědy	poběda	k1gFnSc2
(	(	kIx(
<g/>
П	П	k?
П	П	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
„	„	k?
<g/>
Štít	štít	k1gInSc1
vítězství	vítězství	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
se	s	k7c7
7439	#num#	k4
m	m	kA
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
a	a	k8xC
celého	celý	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
Ťan-šan	Ťan-šan	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
2	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
1	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
3	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
2	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
4	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
3	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
5	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
4	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
6	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
5	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
7	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
6	#num#	k4
ul	ul	kA
<g/>
{	{	kIx(
<g/>
display	displaa	k1gMnSc2
<g/>
:	:	kIx,
<g/>
none	none	k6eAd1
<g/>
}	}	kIx)
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
hřebenu	hřeben	k1gInSc6
Kokšaal-Tau	Kokšaal-Taus	k1gInSc2
Centrálního	centrální	k2eAgInSc2d1
Ťan-Šanu	Ťan-Šan	k1gInSc2
na	na	k7c4
hranici	hranice	k1gFnSc4
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
a	a	k8xC
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
jezera	jezero	k1gNnSc2
Issyk-kul	Issyk-kula	k1gFnPc2
a	a	k8xC
16	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
druhé	druhý	k4xOgFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
hory	hora	k1gFnSc2
pohoří	pohoří	k1gNnPc2
Chan	Chan	k1gMnSc1
Tengri	Tengr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
geologického	geologický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
nejsevernější	severní	k2eAgInSc4d3
sedmitisícový	sedmitisícový	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
u	u	k7c2
severnějšího	severní	k2eAgMnSc2d2
Chan	Chan	k1gInSc4
Tengri	Tengr	k1gInPc7
překračuje	překračovat	k5eAaImIp3nS
výšku	výška	k1gFnSc4
sedmi	sedm	k4xCc2
tisíc	tisíc	k4xCgInPc2
metrů	metr	k1gInPc2
pouze	pouze	k6eAd1
ledovcová	ledovcový	k2eAgFnSc1d1
čapka	čapka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolí	okolí	k1gNnSc1
hory	hora	k1gFnPc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
zaledněno	zaledněn	k2eAgNnSc1d1
<g/>
,	,	kIx,
ze	z	k7c2
severních	severní	k2eAgNnPc2d1
úbočí	úbočí	k1gNnPc2
hory	hora	k1gFnSc2
stéká	stékat	k5eAaImIp3nS
ledovec	ledovec	k1gInSc1
Inylček	Inylček	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
délkou	délka	k1gFnSc7
62	#num#	k4
km	km	kA
čtvrtý	čtvrtý	k4xOgInSc4
nejdelší	dlouhý	k2eAgInSc4d3
na	na	k7c6
Zemi	zem	k1gFnSc6
(	(	kIx(
<g/>
ref-	ref-	k?
z	z	k7c2
anglické	anglický	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
centrálního	centrální	k2eAgInSc2d1
Ťan-Šanu	Ťan-Šan	k1gInSc2
historicky	historicky	k6eAd1
tvořila	tvořit	k5eAaImAgFnS
přirozenou	přirozený	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
sférou	sféra	k1gFnSc7
vlivu	vliv	k1gInSc2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
carská	carský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
v	v	k7c6
průběhu	průběh	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
několikrát	několikrát	k6eAd1
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
západní	západní	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
oblasti	oblast	k1gFnSc2
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
východ	východ	k1gInSc4
od	od	k7c2
vrcholu	vrchol	k1gInSc2
vždy	vždy	k6eAd1
zůstaly	zůstat	k5eAaPmAgFnP
součástí	součást	k1gFnSc7
Číny	Čína	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
topografickým	topografický	k2eAgNnSc7d1
měřením	měření	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
byl	být	k5eAaImAgInS
za	za	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Ťan-Šanu	Ťan-Šana	k1gFnSc4
považován	považován	k2eAgInSc1d1
Chan	Chan	k1gInSc1
Tengri	Tengr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
příležitosti	příležitost	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
založení	založení	k1gNnSc2
Komunistického	komunistický	k2eAgInSc2d1
svazu	svaz	k1gInSc2
mládeže	mládež	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
byla	být	k5eAaImAgFnS
do	do	k7c2
oblasti	oblast	k1gFnSc2
centrálního	centrální	k2eAgInSc2d1
Ťan-Šanu	Ťan-Šan	k1gInSc2
vyslána	vyslat	k5eAaPmNgFnS
horolezecká	horolezecký	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
s	s	k7c7
cílem	cíl	k1gInSc7
najít	najít	k5eAaPmF
a	a	k8xC
zdolat	zdolat	k5eAaPmF
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
celého	celý	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1938	#num#	k4
pak	pak	k6eAd1
členové	člen	k1gMnPc1
této	tento	k3xDgFnSc2
expedice	expedice	k1gFnSc2
L.	L.	kA
Gutman	Gutman	k1gMnSc1
<g/>
,	,	kIx,
E.	E.	kA
Ivanov	Ivanov	k1gInSc1
a	a	k8xC
A.	A.	kA
Sidorenko	Sidorenka	k1gFnSc5
z	z	k7c2
ledovce	ledovec	k1gInSc2
Zvězdočka	Zvězdočka	k1gFnSc1
vrchol	vrchol	k1gInSc4
zdolali	zdolat	k5eAaPmAgMnP
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
starého	starý	k2eAgInSc2d1
leteckého	letecký	k2eAgInSc2d1
altimetru	altimetr	k1gInSc2
určili	určit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnSc4
výšku	výška	k1gFnSc4
na	na	k7c4
6900	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
pojmenovali	pojmenovat	k5eAaPmAgMnP
Štít	štít	k1gInSc4
20	#num#	k4
let	léto	k1gNnPc2
Komsomola	Komsomola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
1943	#num#	k4
probíhal	probíhat	k5eAaImAgInS
v	v	k7c6
oblasti	oblast	k1gFnSc6
topografický	topografický	k2eAgInSc4d1
průzkum	průzkum	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
určil	určit	k5eAaPmAgInS
přesnou	přesný	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
vrcholu	vrchol	k1gInSc2
7439	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
dělalo	dělat	k5eAaImAgNnS
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Ťan-Šanu	Ťan-Šan	k1gInSc2
a	a	k8xC
druhý	druhý	k4xOgInSc4
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
počest	počest	k1gFnSc4
ruského	ruský	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
u	u	k7c2
Stalingradu	Stalingrad	k1gInSc2
byl	být	k5eAaImAgInS
vrchol	vrchol	k1gInSc1
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c4
Pik	pik	k1gInSc4
Pobedy	Pobeda	k1gMnSc2
(	(	kIx(
<g/>
Štít	štít	k1gInSc1
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
velkému	velký	k2eAgInSc3d1
výškovému	výškový	k2eAgInSc3d1
rozdílu	rozdíl	k1gInSc3
měření	měření	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zpochybnění	zpochybnění	k1gNnSc3
předešlého	předešlý	k2eAgInSc2d1
výstupu	výstup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
výstup	výstup	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
uznán	uznat	k5eAaPmNgInS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
bylo	být	k5eAaImAgNnS
vyvinuto	vyvinout	k5eAaPmNgNnS
značné	značný	k2eAgNnSc1d1
úsilí	úsilí	k1gNnSc1
o	o	k7c4
zlezení	zlezení	k1gNnPc4
tohoto	tento	k3xDgNnSc2
oficiálně	oficiálně	k6eAd1
dosud	dosud	k6eAd1
nezlezeného	zlezený	k2eNgInSc2d1
vrcholu	vrchol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
ke	k	k7c3
katastrofě	katastrofa	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
většina	většina	k1gFnSc1
účastníků	účastník	k1gMnPc2
kazašské	kazašský	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
E.	E.	kA
M.	M.	kA
Kolokolnikovem	Kolokolnikov	k1gInSc7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
zahynula	zahynout	k5eAaPmAgFnS
ve	v	k7c6
výšce	výška	k1gFnSc6
6900	#num#	k4
metrů	metr	k1gInPc2
ve	v	k7c6
sněhové	sněhový	k2eAgFnSc6d1
bouři	bouř	k1gFnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
nezpochybnitelný	zpochybnitelný	k2eNgInSc4d1
úspěšný	úspěšný	k2eAgInSc4d1
výstup	výstup	k1gInSc4
byl	být	k5eAaImAgInS
uskutečněn	uskutečnit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
týmem	tým	k1gInSc7
sovětských	sovětský	k2eAgMnPc2d1
horolezců	horolezec	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Vitalije	Vitalije	k1gFnSc2
M.	M.	kA
Abalakova	Abalakův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
byl	být	k5eAaImAgInS
vrchol	vrchol	k1gInSc1
poprvé	poprvé	k6eAd1
dosažen	dosáhnout	k5eAaPmNgInS
východním	východní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
skupinou	skupina	k1gFnSc7
I.	I.	kA
Erokina	Erokin	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
skupina	skupina	k1gFnSc1
D.	D.	kA
Medzmarišviliho	Medzmarišvili	k1gMnSc2
otevřela	otevřít	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
po	po	k7c6
západním	západní	k2eAgInSc6d1
hřebenu	hřeben	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
používána	používán	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
klasická	klasický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebezpečí	nebezpečí	k1gNnSc1
této	tento	k3xDgFnSc2
cesty	cesta	k1gFnSc2
spočívá	spočívat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
ve	v	k7c6
velmi	velmi	k6eAd1
dlouhém	dlouhý	k2eAgInSc6d1
postupu	postup	k1gInSc6
po	po	k7c6
hřebenu	hřeben	k1gInSc6
ve	v	k7c6
výškách	výška	k1gFnPc6
kolem	kolem	k7c2
7000	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
úspěšný	úspěšný	k2eAgInSc1d1
traverz	traverza	k1gFnPc2
celého	celý	k2eAgInSc2d1
vrcholového	vrcholový	k2eAgInSc2d1
hřebenu	hřeben	k1gInSc2
byl	být	k5eAaImAgInS
uskutečněn	uskutečnit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
výstup	výstup	k1gInSc4
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
–	–	k?
Čínské	čínský	k2eAgFnSc2d1
–	–	k?
strany	strana	k1gFnSc2
byl	být	k5eAaImAgInS
uskutečněn	uskutečnit	k5eAaPmNgInS
čínskou	čínský	k2eAgFnSc7d1
expedicí	expedice	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
výstupové	výstupový	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
jsou	být	k5eAaImIp3nP
náročnými	náročný	k2eAgInPc7d1
horolezeckými	horolezecký	k2eAgInPc7d1
výstupy	výstup	k1gInPc7
obtížnosti	obtížnost	k1gFnSc2
5B	5B	k4
nebo	nebo	k8xC
vyšší	vysoký	k2eAgFnSc2d2
ruské	ruský	k2eAgFnSc2d1
škály	škála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	hora	k1gFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nejobtížnější	obtížný	k2eAgFnSc4d3
sedmitisícovku	sedmitisícovka	k1gFnSc4
v	v	k7c6
bývalém	bývalý	k2eAgInSc6d1
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Džengiš	Džengiš	k1gMnSc1
Čokosu	Čokos	k1gInSc2
na	na	k7c4
Summitpost	Summitpost	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
↑	↑	k?
Velká	velký	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
↑	↑	k?
Novospasskoe-city	Novospasskoe-cit	k1gInPc4
<g/>
.	.	kIx.
<g/>
ru	ru	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chan	Chan	k1gInSc1
Tengri	Tengr	k1gFnSc2
</s>
<s>
Qullai	Qulla	k1gFnSc3
Ismoili	Ismoili	k1gFnSc3
Somoni	Somoň	k1gFnSc3
(	(	kIx(
<g/>
Pik	pik	k1gInSc1
Kommunizma	Kommunizma	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pik	Pik	k1gMnSc1
Lenina	Lenin	k1gMnSc2
</s>
<s>
Štít	štít	k1gInSc1
Korženěvské	Korženěvský	k2eAgFnSc2d1
</s>
<s>
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Džengiš	Džengiš	k1gMnSc1
Čokusu	Čokus	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Džengiš	Džengiš	k1gMnSc1
Čokusu	Čokus	k1gInSc2
na	na	k7c4
Wikimapia	Wikimapium	k1gNnPc4
</s>
<s>
Džengiš	Džengiš	k1gInSc1
Čokusu	Čokus	k1gInSc2
na	na	k7c6
Peakware	Peakwar	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Sedmitisícovky	Sedmitisícovka	k1gFnSc2
SSSR	SSSR	kA
</s>
<s>
Qullai	Qulla	k1gFnSc3
Ismoili	Ismoili	k1gFnSc3
Somoni	Somoň	k1gFnSc3
<g/>
/	/	kIx~
<g/>
Pik	pik	k1gInSc1
Kommunizma	Kommunizma	k1gNnSc1
(	(	kIx(
<g/>
7495	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Džengiš	Džengiš	k1gInSc1
Čokusu	Čokus	k1gInSc2
<g/>
/	/	kIx~
<g/>
Pik	pik	k1gInSc1
Pobedy	Pobeda	k1gMnSc2
(	(	kIx(
<g/>
7439	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Pik	pik	k1gInSc1
Lenina	Lenin	k1gMnSc2
(	(	kIx(
<g/>
7134	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Štít	štít	k1gInSc4
Korženěvské	Korženěvský	k2eAgNnSc1d1
(	(	kIx(
<g/>
7105	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Chan	Chan	k1gInSc1
Tengri	Tengr	k1gFnSc2
(	(	kIx(
<g/>
7010	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Asie	Asie	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Sýrie	Sýrie	k1gFnSc2
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc4
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Vánoční	vánoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4479171-9	4479171-9	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
234767171	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
