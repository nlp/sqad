<s>
Dženiš	Dženiš	k1gInSc1
čokusu	čokus	k1gInSc2
nebo	nebo	k8xC
Tömür	Tömüra	k1gFnPc2
(	(	kIx(
<g/>
kyrgyzsky	kyrgyzsky	k6eAd1
Ж	Ж	k?
ч	ч	k?
<g/>
,	,	kIx,
Žeŋ	Žeŋ	k1gMnSc1
čokusu	čokus	k1gInSc2
<g/>
,	,	kIx,
ujgursky	ujgursky	k6eAd1
ت	ت	k?
<g/>
,	,	kIx,
transliterováno	transliterován	k2eAgNnSc1d1
Tömür	Tömür	k1gInSc4
<g/>
,	,	kIx,
čínsky	čínsky	k6eAd1
托	托	k?
<g/>
,	,	kIx,
pinyin	pinyin	k1gMnSc1
Tuō	Tuō	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ěr	ěr	k?
Fē	Fē	k1gInSc1
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
přepis	přepis	k1gInSc1
Tchuo-mu-er-feng	Tchuo-mu-er-fenga	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
pod	pod	k7c7
ruským	ruský	k2eAgInSc7d1
názvem	název	k1gInSc7
Pik	pik	k1gInSc1
Pobědy	poběda	k1gFnSc2
(	(	kIx(
<g/>
П	П	k?
П	П	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
„	„	k?
<g/>
Štít	štít	k1gInSc1
vítězství	vítězství	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
se	s	k7c7
7439	#num#	k4
m	m	kA
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
a	a	k8xC
celého	celý	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
Ťan-šan	Ťan-šan	k1gFnPc2
<g/>
.	.	kIx.
</s>