<s>
OpenBSD	OpenBSD	k?
</s>
<s>
OpenBSD	OpenBSD	k?
</s>
<s>
Web	web	k1gInSc1
</s>
<s>
www.openbsd.org	www.openbsd.org	k1gInSc1
Vyvíjí	vyvíjet	k5eAaImIp3nS
</s>
<s>
The	The	k?
OpenBSD	OpenBSD	k1gMnSc1
Project	Project	k1gMnSc1
Rodina	rodina	k1gFnSc1
OS	OS	kA
</s>
<s>
BSD	BSD	kA
Druh	druh	k1gMnSc1
</s>
<s>
Open	Open	k1gNnSc1
source	sourec	k1gInSc2
Aktuální	aktuální	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
6.8	6.8	k4
/	/	kIx~
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Způsob	způsob	k1gInSc1
aktualizace	aktualizace	k1gFnSc2
</s>
<s>
binární	binární	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
instalačního	instalační	k2eAgInSc2d1
CD	CD	kA
<g/>
,	,	kIx,
FTP	FTP	kA
release	release	k6eAd1
Správce	správka	k1gFnSc6
balíčků	balíček	k1gInPc2
</s>
<s>
balíčky	balíček	k1gInPc1
<g/>
,	,	kIx,
porty	port	k1gInPc1
Podporované	podporovaný	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
</s>
<s>
Alpha	Alpha	k1gMnSc1
<g/>
,	,	kIx,
AMD	AMD	kA
<g/>
64	#num#	k4
<g/>
,	,	kIx,
armish	armish	k1gInSc1
<g/>
,	,	kIx,
hp	hp	k?
<g/>
300	#num#	k4
<g/>
,	,	kIx,
hppa	hppa	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
<g/>
386	#num#	k4
<g/>
,	,	kIx,
landisk	landisk	k1gInSc1
<g/>
,	,	kIx,
luna	luna	k1gFnSc1
<g/>
88	#num#	k4
<g/>
k	k	k7c3
<g/>
,	,	kIx,
mac	mac	k?
<g/>
68	#num#	k4
<g/>
k	k	k7c3
<g/>
,	,	kIx,
macppc	macppc	k1gInSc1
<g/>
,	,	kIx,
MIPS	MIPS	kA
<g/>
,	,	kIx,
mvme	mvm	k1gInSc2
<g/>
68	#num#	k4
<g/>
k	k	k7c3
<g/>
,	,	kIx,
mvme	mvm	k1gInSc2
<g/>
88	#num#	k4
<g/>
k	k	k7c3
<g/>
,	,	kIx,
sgi	sgi	k?
<g/>
,	,	kIx,
sparc	sparc	k1gFnSc1
<g/>
,	,	kIx,
sparc	sparc	k1gFnSc1
<g/>
64	#num#	k4
<g/>
,	,	kIx,
vax	vax	k?
<g/>
,	,	kIx,
zaurus	zaurus	k1gMnSc1
Typ	typ	k1gInSc4
jádra	jádro	k1gNnSc2
</s>
<s>
Monolitický	monolitický	k2eAgMnSc1d1
Licence	licence	k1gFnSc2
</s>
<s>
BSD	BSD	kA
licence	licence	k1gFnSc1
Stav	stav	k1gInSc1
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1
</s>
<s>
OpenBSD	OpenBSD	k?
je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
z	z	k7c2
rodiny	rodina	k1gFnSc2
BSD	BSD	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
fork	fork	k1gInSc1
projektu	projekt	k1gInSc2
NetBSD	NetBSD	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
autoři	autor	k1gMnPc1
kladou	klást	k5eAaImIp3nP
důraz	důraz	k1gInSc4
na	na	k7c4
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
na	na	k7c4
software	software	k1gInSc4
bez	bez	k7c2
děr	děra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
projektu	projekt	k1gInSc2
vytvořit	vytvořit	k5eAaPmF
svobodný	svobodný	k2eAgInSc4d1
a	a	k8xC
extrémně	extrémně	k6eAd1
bezpečný	bezpečný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
nejrůznější	různý	k2eAgFnPc4d3
platformy	platforma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
cíli	cíl	k1gInPc7
je	být	k5eAaImIp3nS
vytvoření	vytvoření	k1gNnSc1
vývojářské	vývojářský	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
či	či	k8xC
dodržování	dodržování	k1gNnSc2
ANSI	ANSI	kA
<g/>
,	,	kIx,
POSIX	POSIX	kA
a	a	k8xC
částečně	částečně	k6eAd1
X	X	kA
<g/>
/	/	kIx~
<g/>
Open	Openo	k1gNnPc2
standardů	standard	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofií	filozofie	k1gFnPc2
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
raději	rád	k6eAd2
méně	málo	k6eAd2
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c4
to	ten	k3xDgNnSc4
více	hodně	k6eAd2
bezpečných	bezpečný	k2eAgFnPc6d1
<g/>
,	,	kIx,
než	než	k8xS
rozsáhlý	rozsáhlý	k2eAgInSc1d1
multifunkční	multifunkční	k2eAgInSc1d1
systém	systém	k1gInSc1
plný	plný	k2eAgInSc1d1
chyb	chyba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
mottem	motto	k1gNnSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
Only	Only	k1gInPc1
two	two	k?
remote	remot	k1gInSc5
holes	holesa	k1gFnPc2
in	in	k?
the	the	k?
default	default	k1gInSc1
install	install	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
more	mor	k1gInSc5
than	thana	k1gFnPc2
10	#num#	k4
years	yearsa	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
též	též	k9
používá	používat	k5eAaImIp3nS
motto	motto	k1gNnSc1
"	"	kIx"
<g/>
Secure	Secur	k1gMnSc5
by	by	kYmCp3nS
default	default	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Bezpečný	bezpečný	k2eAgInSc1d1
ve	v	k7c6
výchozím	výchozí	k2eAgNnSc6d1
nastavení	nastavení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
OpenBSD	OpenBSD	k?
projekt	projekt	k1gInSc1
je	být	k5eAaImIp3nS
též	též	k9
autorem	autor	k1gMnSc7
ostatních	ostatní	k2eAgFnPc2d1
Open	Opena	k1gFnPc2
<g/>
*	*	kIx~
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
posléze	posléze	k6eAd1
masivně	masivně	k6eAd1
využívány	využíván	k2eAgInPc1d1
napříč	napříč	k7c7
platformami	platforma	k1gFnPc7
nejen	nejen	k6eAd1
z	z	k7c2
rodiny	rodina	k1gFnSc2
operačních	operační	k2eAgInPc2d1
systému	systém	k1gInSc2
*	*	kIx~
<g/>
BSD	BSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
nejznámějšími	známý	k2eAgInPc7d3
projekty	projekt	k1gInPc7
jsou	být	k5eAaImIp3nP
OpenSSH	OpenSSH	k1gMnSc1
<g/>
,	,	kIx,
OpenBGPD	OpenBGPD	k1gMnSc1
<g/>
,	,	kIx,
OpenNTPD	OpenNTPD	k1gMnSc1
či	či	k8xC
OpenCVS	OpenCVS	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgInS
projekt	projekt	k1gInSc1
vyvíjen	vyvíjet	k5eAaImNgInS
pod	pod	k7c7
částečným	částečný	k2eAgInSc7d1
sponzoringem	sponzoring	k1gInSc7
organizace	organizace	k1gFnSc2
DARPA	DARPA	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
podřízena	podřídit	k5eAaPmNgFnS
Ministerstvu	ministerstvo	k1gNnSc3
obrany	obrana	k1gFnSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
ale	ale	k8xC
projekt	projekt	k1gInSc1
závislý	závislý	k2eAgMnSc1d1
na	na	k7c6
darech	dar	k1gInPc6
dobrovolníků	dobrovolník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
financování	financování	k1gNnSc2
je	být	k5eAaImIp3nS
též	též	k9
prodávání	prodávání	k1gNnSc4
propagačních	propagační	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
a	a	k8xC
oficiálních	oficiální	k2eAgInPc2d1
CD	CD	kA
projektu	projekt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůdčí	vůdčí	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
Theo	Thea	k1gFnSc5
de	de	k?
Raadt	Raadta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
OpenBSD	OpenBSD	k?
podporuje	podporovat	k5eAaImIp3nS
emulaci	emulace	k1gFnSc3
většiny	většina	k1gFnSc2
binárních	binární	k2eAgInPc2d1
programů	program	k1gInPc2
ze	z	k7c2
SVR4	SVR4	k1gFnSc2
(	(	kIx(
<g/>
Solaris	Solaris	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
FreeBSD	FreeBSD	k1gFnSc2
<g/>
,	,	kIx,
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
BSD	BSD	kA
<g/>
/	/	kIx~
<g/>
OS	OS	kA
<g/>
,	,	kIx,
SunOS	SunOS	k1gFnSc2
a	a	k8xC
HP-UX	HP-UX	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
OpenBSD	OpenBSD	k1gFnSc2
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
novou	nový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
každých	každý	k3xTgInPc2
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
vždy	vždy	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
distribuovaných	distribuovaný	k2eAgNnPc2d1
vydání	vydání	k1gNnPc2
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
jen	jen	k6eAd1
a	a	k8xC
pouze	pouze	k6eAd1
software	software	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
kompatibilní	kompatibilní	k2eAgInPc1d1
s	s	k7c7
BSD	BSD	kA
licencí	licence	k1gFnSc7
–	–	k?
např.	např.	kA
Apache	Apache	k1gInSc4
1.0	1.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInSc1d1
software	software	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nevyhovují	vyhovovat	k5eNaImIp3nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
stáhnout	stáhnout	k5eAaPmF
z	z	k7c2
FTP	FTP	kA
repozitářů	repozitář	k1gMnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
nemohou	moct	k5eNaImIp3nP
se	se	k3xPyFc4
objevit	objevit	k5eAaPmF
na	na	k7c6
oficiálně	oficiálně	k6eAd1
distribuovaných	distribuovaný	k2eAgNnPc6d1
médiích	médium	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Maskotem	maskot	k1gInSc7
projektu	projekt	k1gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
BSD	BSD	kA
Démon	démon	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
verze	verze	k1gFnSc2
3.0	3.0	k4
je	být	k5eAaImIp3nS
jím	on	k3xPp3gInSc7
ryba	ryba	k1gFnSc1
ježík	ježík	k1gMnSc1
Puffy	puff	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
logu	log	k1gInSc6
každého	každý	k3xTgInSc2
projektu	projekt	k1gInSc2
začleněného	začleněný	k2eAgMnSc4d1
pod	pod	k7c7
OpenBSD	OpenBSD	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hezkou	hezký	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
vydání	vydání	k1gNnSc2
songu	song	k1gInSc2
k	k	k7c3
releasu	releas	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
komentuje	komentovat	k5eAaBmIp3nS
základní	základní	k2eAgInPc4d1
kroky	krok	k1gInPc4
či	či	k8xC
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
vývojáři	vývojář	k1gMnPc1
v	v	k7c6
tom	ten	k3xDgInSc6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
releasu	releas	k1gInSc6
řešili	řešit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Songy	song	k1gInPc4
píše	psát	k5eAaImIp3nS
Ty	ty	k3xPp2nSc5
Semaka	Semak	k1gMnSc2
ze	z	k7c2
skupiny	skupina	k1gFnSc2
"	"	kIx"
<g/>
Plaid	Plaid	k1gInSc1
Tongued	Tongued	k1gMnSc1
Devils	Devils	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
GNU	gnu	k1gMnSc1
-	-	kIx~
GNU	gnu	k1gMnSc1
GPL	GPL	kA
(	(	kIx(
<g/>
licence	licence	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Free	Free	k6eAd1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
FSF	FSF	kA
<g/>
)	)	kIx)
–	–	k?
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
nadace	nadace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zastřešuje	zastřešovat	k5eAaImIp3nS
Projekt	projekt	k1gInSc4
GNU	gnu	k1gNnSc2
</s>
<s>
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
-	-	kIx~
projekt	projekt	k1gInSc1
původně	původně	k6eAd1
Richarda	Richard	k1gMnSc2
Stallmana	Stallman	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
vyvinout	vyvinout	k5eAaPmF
kvalitní	kvalitní	k2eAgMnSc1d1
a	a	k8xC
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
-	-	kIx~
GNU	gnu	k1gNnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
GPL	GPL	kA
-	-	kIx~
licence	licence	k1gFnSc2
napsané	napsaný	k2eAgFnSc2d1
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
,	,	kIx,
k	k	k7c3
uskutečnění	uskutečnění	k1gNnSc3
cílů	cíl	k1gInPc2
Projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc3
</s>
<s>
GNU	gnu	k1gMnSc1
Hurd	hurda	k1gFnPc2
–	–	k?
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c6
mikrojádře	mikrojádra	k1gFnSc6
GNU	gnu	k1gNnSc2
Mach	macha	k1gFnPc2
<g/>
,	,	kIx,
vyvíjený	vyvíjený	k2eAgInSc1d1
Projektem	projekt	k1gInSc7
GNU	gnu	k1gNnPc4
</s>
<s>
GNU	gnu	k1gMnSc1
Mach	macha	k1gFnPc2
–	–	k?
jádro	jádro	k1gNnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
<g/>
;	;	kIx,
mikrojádro	mikrojádro	k6eAd1
</s>
<s>
GNU	gnu	k1gNnSc1
Hurd	hurda	k1gFnPc2
NG	NG	kA
–	–	k?
svobodný	svobodný	k2eAgInSc4d1
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
založený	založený	k2eAgInSc4d1
na	na	k7c6
mikrojádře	mikrojádra	k1gFnSc6
L4	L4	k1gFnSc1
(	(	kIx(
<g/>
jádro	jádro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyvíjený	vyvíjený	k2eAgInSc1d1
Projektem	projekt	k1gInSc7
GNU	gnu	k1gNnSc7
</s>
<s>
L4	L4	k4
(	(	kIx(
<g/>
jádro	jádro	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
jádro	jádro	k1gNnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
NG	NG	kA
<g/>
;	;	kIx,
mikrojádro	mikrojádro	k6eAd1
navržené	navržený	k2eAgNnSc1d1
a	a	k8xC
vytvořené	vytvořený	k2eAgNnSc1d1
vědcem	vědec	k1gMnSc7
Jochenem	Jochen	k1gMnSc7
Liedtkem	Liedtek	k1gMnSc7
</s>
<s>
Linux	linux	k1gInSc1
(	(	kIx(
<g/>
jádro	jádro	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
jádro	jádro	k1gNnSc1
svobodného	svobodný	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
vyvíjené	vyvíjený	k2eAgInPc1d1
Linux	linux	k1gInSc4
Foundation	Foundation	k1gInSc1
<g/>
;	;	kIx,
modulární	modulární	k2eAgNnSc1d1
monolitické	monolitický	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
</s>
<s>
Linux-libre	Linux-libr	k1gMnSc5
–	–	k?
jádro	jádro	k1gNnSc1
svobodného	svobodný	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
vyvíjené	vyvíjený	k2eAgFnSc2d1
dcerou	dcera	k1gFnSc7
FSF	FSF	kA
(	(	kIx(
<g/>
FSFLA	FSFLA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fork	fork	k1gInSc1
Linux	linux	k1gInSc1
(	(	kIx(
<g/>
jádro	jádro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
modulární	modulární	k2eAgNnSc1d1
monolitické	monolitický	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
</s>
<s>
BSD	BSD	kA
-	-	kIx~
BSD	BSD	kA
licence	licence	k1gFnSc2
</s>
<s>
Berkeley	Berkelea	k1gFnPc1
Software	software	k1gInSc1
Distribution	Distribution	k1gInSc4
–	–	k?
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
při	pře	k1gFnSc3
University	universita	k1gFnSc2
of	of	k?
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
Berkeley	Berkelea	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyvinula	vyvinout	k5eAaPmAgFnS
licenci	licence	k1gFnSc4
BSD	BSD	kA
a	a	k8xC
používala	používat	k5eAaImAgFnS
pro	pro	k7c4
práce	práce	k1gFnPc4
nad	nad	k7c7
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
BSD	BSD	kA
Unix	Unix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
BSD	BSD	kA
licence	licence	k1gFnSc1
-	-	kIx~
licence	licence	k1gFnSc1
organizace	organizace	k1gFnSc2
BSD	BSD	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
používala	používat	k5eAaImAgFnS
pro	pro	k7c4
BSD	BSD	kA
Unix	Unix	k1gInSc4
a	a	k8xC
odvozená	odvozený	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
</s>
<s>
FreeBSD	FreeBSD	k?
–	–	k?
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
BSD	BSD	kA
Unixu	Unix	k1gInSc2
<g/>
;	;	kIx,
modulární	modulární	k2eAgNnSc1d1
monolitické	monolitický	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
</s>
<s>
DragonFly	DragonFla	k1gFnPc1
BSD	BSD	kA
–	–	k?
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
fork	fork	k1gInSc1
FreeBSD	FreeBSD	k1gFnSc1
4.8	4.8	k4
s	s	k7c7
hybridním	hybridní	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
</s>
<s>
NetBSD	NetBSD	k?
–	–	k?
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
BSD	BSD	kA
Unixu	Unix	k1gInSc2
(	(	kIx(
<g/>
před	před	k7c7
FreeBSD	FreeBSD	k1gFnSc7
<g/>
)	)	kIx)
<g/>
;	;	kIx,
modulární	modulární	k2eAgNnSc1d1
monolitické	monolitický	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
</s>
<s>
OpenBSD	OpenBSD	k?
–	–	k?
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
fork	fork	k1gInSc1
NetBSD	NetBSD	k1gFnPc2
zaměřený	zaměřený	k2eAgMnSc1d1
na	na	k7c4
bezpečnost	bezpečnost	k1gFnSc4
<g/>
;	;	kIx,
monolitické	monolitický	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
</s>
<s>
MINIX	MINIX	kA
3	#num#	k4
-	-	kIx~
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
;	;	kIx,
mikrojádro	mikrojádro	k6eAd1
navržené	navržený	k2eAgFnPc4d1
a	a	k8xC
vytvořené	vytvořený	k2eAgFnPc4d1
vědcem	vědec	k1gMnSc7
Andrew	Andrew	k1gFnSc2
S.	S.	kA
Tanenbaumem	Tanenbaum	k1gInSc7
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
OpenBSD	OpenBSD	k1gFnSc1
6.8	6.8	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
OpenBSD	OpenBSD	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.OpenBSD.org	www.OpenBSD.org	k1gInSc1
–	–	k?
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
www.Undeadly.org	www.Undeadly.org	k1gMnSc1
–	–	k?
Blog	Blog	k1gMnSc1
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
OpenBSD	OpenBSD	k?
101	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
OpenBSD	OpenBSD	k?
MP3	MP3	k1gFnSc3
jednodisketový	jednodisketový	k2eAgInSc1d1
přehrávač	přehrávač	k1gInSc1
a	a	k8xC
i	i	k9
router	router	k1gInSc1
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Theo	Thea	k1gFnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
–	–	k?
Osobní	osobní	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
zakladatele	zakladatel	k1gMnSc2
a	a	k8xC
hlavního	hlavní	k2eAgMnSc4d1
vývojáře	vývojář	k1gMnSc4
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BSDAnywhere	BSDAnywhrat	k5eAaPmIp3nS
–	–	k?
Live	Live	k1gFnSc1
distribuce	distribuce	k1gFnSc1
OpenBSD	OpenBSD	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Doplňující	doplňující	k2eAgInPc4d1
odkazy	odkaz	k1gInPc4
</s>
<s>
OpenBSD	OpenBSD	k?
songy	song	k1gInPc1
–	–	k?
Texty	text	k1gInPc1
písní	píseň	k1gFnPc2
vydaných	vydaný	k2eAgFnPc2d1
k	k	k7c3
release	releasa	k1gFnSc3
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Operační	operační	k2eAgInPc1d1
systémy	systém	k1gInPc1
BSD	BSD	kA
</s>
<s>
FreeBSD	FreeBSD	k?
•	•	k?
DragonFly	DragonFla	k1gFnSc2
BSD	BSD	kA
•	•	k?
NetBSD	NetBSD	k1gMnSc1
•	•	k?
OpenBSD	OpenBSD	k1gMnSc1
—	—	k?
Minix	Minix	k1gInSc1
3	#num#	k4
Linux	Linux	kA
(	(	kIx(
<g/>
distribuce	distribuce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
Deb	Deb	k1gMnPc5
<g/>
)	)	kIx)
</s>
<s>
Debian	Debian	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Knoppix	Knoppix	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ubuntu	Ubunt	k1gInSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
RPM	RPM	kA
<g/>
)	)	kIx)
</s>
<s>
Red	Red	k?
Hat	hat	k0
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fedora	Fedora	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mandriva	Mandriva	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Portage	Portage	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Gentoo	Gentoo	k1gNnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
AUR	aura	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Arch	arch	k1gInSc1
Linux	Linux	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manjaro	Manjara	k1gFnSc5
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
další	další	k2eAgFnSc2d1
</s>
<s>
Slackware	Slackwar	k1gMnSc5
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slax	Slax	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SUSE	SUSE	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
další	další	k2eAgInSc4d1
<g/>
…	…	k?
</s>
<s>
GNU	gnu	k1gMnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
Hurd	hurda	k1gFnPc2
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
—	—	k?
seL	sít	k5eAaImAgInS
<g/>
4	#num#	k4
(	(	kIx(
<g/>
Mac	Mac	kA
<g/>
)	)	kIx)
OS	OS	kA
X	X	kA
•	•	k?
macOS	macOS	k?
</s>
<s>
10.6	10.6	k4
(	(	kIx(
<g/>
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.7	10.7	k4
(	(	kIx(
<g/>
Lion	Lion	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.8	10.8	k4
(	(	kIx(
<g/>
Mountain	Mountain	k1gMnSc1
Lion	Lion	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.9	10.9	k4
(	(	kIx(
<g/>
Mavericks	Mavericksa	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
OS	OS	kA
X	X	kA
10.10	10.10	k4
Yosemite	Yosemit	k1gInSc5
•	•	k?
OS	OS	kA
X	X	kA
10.11	10.11	k4
El	Ela	k1gFnPc2
Capitan	Capitan	k1gInSc4
•	•	k?
macOS	macOS	k?
10.12	10.12	k4
Sierra	Sierra	k1gFnSc1
•	•	k?
macOS	macOS	k?
10.13	10.13	k4
High	Higha	k1gFnPc2
Sierra	Sierra	k1gFnSc1
•	•	k?
macOS	macOS	k?
10.14	10.14	k4
Mojave	Mojav	k1gInSc5
•	•	k?
macOS	macOS	k?
10.15	10.15	k4
Catalina	Catalina	k1gFnSc1
•	•	k?
macOS	macOS	k?
11	#num#	k4
Big	Big	k1gFnPc7
Sur	Sur	k1gMnSc2
DOS	DOS	kA
</s>
<s>
MS-DOS	MS-DOS	k?
•	•	k?
DR-DOS	DR-DOS	k1gMnSc1
•	•	k?
FreeDOS	FreeDOS	k1gMnSc1
•	•	k?
PTS-DOS	PTS-DOS	k1gMnSc1
Windows	Windows	kA
</s>
<s>
Windows	Windows	kA
pro	pro	k7c4
MS-DOS	MS-DOS	k1gFnSc4
</s>
<s>
Windows	Windows	kA
1.0	1.0	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
2.0	2.0	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
3.0	3.0	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
3.1	3.1	k4
<g/>
x	x	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
95	#num#	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
98	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
ME	ME	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Windows	Windows	kA
NT	NT	kA
</s>
<s>
Windows	Windows	kA
NT	NT	kA
3.1	3.1	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
3.5	3.5	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
3.51	3.51	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
4.0	4.0	k4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
2000	#num#	k4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
XP	XP	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2003	#num#	k4
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Vista	vista	k2eAgFnSc1d1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2008	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
7	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2008	#num#	k4
R2	R2	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
8	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2012	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
8.1	8.1	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2012	#num#	k4
R2	R2	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
10	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Windows	Windows	kA
CE	CE	kA
</s>
<s>
Windows	Windows	kA
CE	CE	kA
→	→	k?
Windows	Windows	kA
Mobile	mobile	k1gNnSc1
→	→	k?
Windows	Windows	kA
Phone	Phon	k1gInSc5
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
10	#num#	k4
Mobile	mobile	k1gNnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Vývoj	vývoj	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
</s>
<s>
Cairo	Cairo	k6eAd1
•	•	k?
Nashville	Nashville	k1gInSc1
•	•	k?
Neptune	Neptun	k1gInSc5
•	•	k?
Odyssey	Odyssea	k1gFnSc2
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
a	a	k8xC
PDA	PDA	kA
</s>
<s>
Android	android	k1gInSc1
•	•	k?
Bada	Bada	k1gFnSc1
•	•	k?
BlackBerry	BlackBerra	k1gFnSc2
OS	OS	kA
•	•	k?
Firefox	Firefox	k1gInSc1
OS	OS	kA
•	•	k?
iOS	iOS	k?
•	•	k?
Maemo	Maema	k1gFnSc5
•	•	k?
Palm	Palmo	k1gNnPc2
OS	OS	kA
•	•	k?
Symbian	Symbian	k1gMnSc1
OS	OS	kA
•	•	k?
Tizen	Tizen	k1gInSc1
•	•	k?
Ubuntu	Ubunt	k1gInSc2
Touch	Touch	k1gMnSc1
•	•	k?
webOS	webOS	k?
•	•	k?
Windows	Windows	kA
Phone	Phon	k1gInSc5
další	další	k2eAgNnPc1d1
</s>
<s>
QNX	QNX	kA
•	•	k?
Solaris	Solaris	k1gInSc1
•	•	k?
BeOS	BeOS	k1gFnSc2
•	•	k?
OpenVMS	OpenVMS	k1gFnSc2
•	•	k?
Mac	Mac	kA
OS	OS	kA
•	•	k?
NeXTSTEP	NeXTSTEP	k1gMnSc1
•	•	k?
Syllable	Syllable	k1gMnSc1
•	•	k?
ReactOS	ReactOS	k1gMnSc1
•	•	k?
Haiku	Haika	k1gFnSc4
•	•	k?
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
•	•	k?
AmigaOS	AmigaOS	k1gMnSc7
historické	historický	k2eAgFnSc2d1
</s>
<s>
Mac	Mac	kA
OS	OS	kA
(	(	kIx(
<g/>
Classic	Classic	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Multics	Multics	k1gInSc1
•	•	k?
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
•	•	k?
Plan	plan	k1gInSc1
9	#num#	k4
from	from	k6eAd1
Bell	bell	k1gInSc1
Labs	Labs	k1gInSc1
•	•	k?
UNIX	UNIX	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4791366-6	4791366-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2003002707	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
180371448	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2003002707	#num#	k4
</s>
