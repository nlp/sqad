<p>
<s>
Zázvorníkotvaré	Zázvorníkotvarý	k2eAgNnSc1d1	Zázvorníkotvarý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zázvorotvaré	zázvorotvarý	k2eAgFnPc1d1	zázvorotvarý
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Zingiberales	Zingiberales	k1gInSc1	Zingiberales
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řád	řád	k1gInSc4	řád
jednoděložných	jednoděložný	k2eAgFnPc2d1	jednoděložná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojetí	pojetí	k1gNnSc2	pojetí
řádu	řád	k1gInSc2	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Pojetí	pojetí	k1gNnSc1	pojetí
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
taxonomických	taxonomický	k2eAgInPc6d1	taxonomický
systémech	systém	k1gInPc6	systém
měnilo	měnit	k5eAaImAgNnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
taxonomické	taxonomický	k2eAgInPc1d1	taxonomický
systémy	systém	k1gInPc1	systém
měly	mít	k5eAaImAgInP	mít
někdy	někdy	k6eAd1	někdy
řád	řád	k1gInSc4	řád
úžeji	úzko	k6eAd2	úzko
vymezen	vymezen	k2eAgInSc1d1	vymezen
a	a	k8xC	a
rozlišovaly	rozlišovat	k5eAaImAgInP	rozlišovat
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Tachtadžjanův	Tachtadžjanův	k2eAgInSc4d1	Tachtadžjanův
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
vyhraněnou	vyhraněný	k2eAgFnSc4d1	vyhraněná
skupinu	skupina	k1gFnSc4	skupina
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
změny	změna	k1gFnPc1	změna
nebyly	být	k5eNaImAgFnP	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Cronquistův	Cronquistův	k2eAgInSc4d1	Cronquistův
systém	systém	k1gInSc4	systém
má	mít	k5eAaImIp3nS	mít
řád	řád	k1gInSc1	řád
stejně	stejně	k6eAd1	stejně
vymezen	vymezit	k5eAaPmNgInS	vymezit
jako	jako	k9	jako
systém	systém	k1gInSc1	systém
APG	APG	kA	APG
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
řád	řád	k1gInSc1	řád
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
tropickým	tropický	k2eAgNnSc7d1	tropické
rozšířením	rozšíření	k1gNnSc7	rozšíření
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
lehce	lehko	k6eAd1	lehko
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
do	do	k7c2	do
subtropů	subtropy	k1gInPc2	subtropy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jako	jako	k8xC	jako
původní	původní	k2eAgMnSc1d1	původní
zcela	zcela	k6eAd1	zcela
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
většinou	většinou	k6eAd1	většinou
vytrvalé	vytrvalý	k2eAgMnPc4d1	vytrvalý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
robustní	robustní	k2eAgFnPc1d1	robustní
<g/>
,	,	kIx,	,
byliny	bylina	k1gFnPc1	bylina
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
netypické	typický	k2eNgFnPc4d1	netypická
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
oddenky	oddenek	k1gInPc7	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jednodomé	jednodomý	k2eAgInPc1d1	jednodomý
s	s	k7c7	s
oboupohlavnými	oboupohlavný	k2eAgInPc7d1	oboupohlavný
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
jednopohlavnými	jednopohlavný	k2eAgInPc7d1	jednopohlavný
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
střídavé	střídavý	k2eAgInPc1d1	střídavý
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
,	,	kIx,	,
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
nebo	nebo	k8xC	nebo
řidčeji	řídce	k6eAd2	řídce
přisedlé	přisedlý	k2eAgNnSc1d1	přisedlé
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
čárkovité	čárkovitý	k2eAgFnPc1d1	čárkovitá
<g/>
,	,	kIx,	,
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
,	,	kIx,	,
podlouhlé	podlouhlý	k2eAgFnPc1d1	podlouhlá
až	až	k6eAd1	až
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
<g/>
,	,	kIx,	,
celokrajné	celokrajný	k2eAgFnPc1d1	celokrajná
<g/>
,	,	kIx,	,
žilnatina	žilnatina	k1gFnSc1	žilnatina
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
zpeřená	zpeřený	k2eAgFnSc1d1	zpeřená
či	či	k8xC	či
zpeřeně	zpeřeně	k6eAd1	zpeřeně
souběžná	souběžný	k2eAgFnSc1d1	souběžná
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
čepele	čepel	k1gFnPc1	čepel
podél	podél	k7c2	podél
žilek	žilka	k1gFnPc2	žilka
trhají	trhat	k5eAaImIp3nP	trhat
a	a	k8xC	a
vypadají	vypadat	k5eAaImIp3nP	vypadat
zdánlivě	zdánlivě	k6eAd1	zdánlivě
jako	jako	k8xS	jako
zpeřeně	zpeřeně	k6eAd1	zpeřeně
složené	složený	k2eAgInPc1d1	složený
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
květenstvích	květenství	k1gNnPc6	květenství
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
složených	složený	k2eAgNnPc2d1	složené
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
nápadnými	nápadný	k2eAgInPc7d1	nápadný
a	a	k8xC	a
vybarvenými	vybarvený	k2eAgInPc7d1	vybarvený
listeny	listen	k1gInPc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řád	řád	k1gInSc4	řád
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
výrazně	výrazně	k6eAd1	výrazně
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
zygomorfické	zygomorfický	k2eAgInPc1d1	zygomorfický
až	až	k8xS	až
zcela	zcela	k6eAd1	zcela
asymetrické	asymetrický	k2eAgInPc4d1	asymetrický
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Okvětní	okvětní	k2eAgInPc1d1	okvětní
lístky	lístek	k1gInPc1	lístek
bývají	bývat	k5eAaImIp3nP	bývat
nestejné	stejný	k2eNgInPc1d1	nestejný
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dost	dost	k6eAd1	dost
atypické	atypický	k2eAgInPc1d1	atypický
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
rozlišeny	rozlišit	k5eAaPmNgFnP	rozlišit
na	na	k7c4	na
kalich	kalich	k1gInSc4	kalich
a	a	k8xC	a
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
jich	on	k3xPp3gFnPc2	on
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Fertilních	fertilní	k2eAgFnPc2d1	fertilní
tyčinek	tyčinka	k1gFnPc2	tyčinka
bývá	bývat	k5eAaImIp3nS	bývat
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
přeměny	přeměna	k1gFnPc1	přeměna
na	na	k7c4	na
staminodia	staminodium	k1gNnPc4	staminodium
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
petaloidní	petaloidní	k2eAgInPc1d1	petaloidní
(	(	kIx(	(
<g/>
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
korunu	koruna	k1gFnSc4	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
petaloidní	petaloidní	k2eAgFnSc1d1	petaloidní
i	i	k9	i
tyčinka	tyčinka	k1gFnSc1	tyčinka
nebo	nebo	k8xC	nebo
čnělka	čnělka	k1gFnSc1	čnělka
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
labellum	labellum	k1gInSc1	labellum
<g/>
,	,	kIx,	,
petaloidní	petaloidní	k2eAgNnPc1d1	petaloidní
staminodia	staminodium	k1gNnPc1	staminodium
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
nápadnější	nápadní	k2eAgFnSc1d2	nápadní
než	než	k8xS	než
vlastní	vlastní	k2eAgFnSc1d1	vlastní
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gNnSc1	Gyneceum
je	být	k5eAaImIp3nS	být
srostlé	srostlý	k2eAgNnSc1d1	srostlé
nejčastěji	často	k6eAd3	často
ze	z	k7c2	z
3	[number]	k4	3
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
spodní	spodní	k2eAgFnSc7d1	spodní
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
svrchní	svrchní	k2eAgFnSc1d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
tobolka	tobolka	k1gFnSc1	tobolka
nebo	nebo	k8xC	nebo
bobule	bobule	k1gFnSc1	bobule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
čeledi	čeleď	k1gFnSc2	čeleď
podle	podle	k7c2	podle
APG	APG	kA	APG
II	II	kA	II
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zázvorníkotvaré	zázvorníkotvarý	k2eAgFnPc4d1	zázvorníkotvarý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Angiosperm	Angiosperm	k1gInSc1	Angiosperm
Phylogeny	Phylogen	k1gInPc1	Phylogen
Website	Websit	k1gInSc5	Websit
</s>
</p>
