<s>
Teimuraz	Teimuraz	k1gInSc1	Teimuraz
Gabašvili	Gabašvili	k1gFnSc2	Gabašvili
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Т	Т	k?	Т
Г	Г	k?	Г
<g/>
,	,	kIx,	,
gruzínsky	gruzínsky	k6eAd1	gruzínsky
თ	თ	k?	თ
გ	გ	k?	გ
<g/>
,	,	kIx,	,
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1985	[number]	k4	1985
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
gruzínského	gruzínský	k2eAgInSc2d1	gruzínský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
ATP	atp	kA	atp
World	World	k1gInSc1	World
Tour	Tour	k1gInSc1	Tour
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
kariéře	kariéra	k1gFnSc6	kariéra
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
jeden	jeden	k4xCgInSc4	jeden
turnaj	turnaj	k1gInSc4	turnaj
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
společně	společně	k6eAd1	společně
s	s	k7c7	s
Litevcem	Litevec	k1gMnSc7	Litevec
Ričardasem	Ričardas	k1gMnSc7	Ričardas
Berankisem	Berankis	k1gInSc7	Berankis
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Houstonu	Houston	k1gInSc6	Houston
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
challengerech	challenger	k1gInPc6	challenger
ATP	atp	kA	atp
a	a	k8xC	a
okruhu	okruh	k1gInSc3	okruh
Futures	Futuresa	k1gFnPc2	Futuresa
získal	získat	k5eAaPmAgInS	získat
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
čtrnáct	čtrnáct	k4xCc4	čtrnáct
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
osm	osm	k4xCc4	osm
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříčko	k1gNnSc6	žebříčko
ATP	atp	kA	atp
byl	být	k5eAaImAgMnS	být
nejvýše	nejvýše	k6eAd1	nejvýše
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
na	na	k7c4	na
43	[number]	k4	43
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c4	na
101	[number]	k4	101
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
jej	on	k3xPp3gNnSc4	on
trénoval	trénovat	k5eAaImAgMnS	trénovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
argentinský	argentinský	k2eAgMnSc1d1	argentinský
tenista	tenista	k1gMnSc1	tenista
Guillermo	Guillerma	k1gFnSc5	Guillerma
Cañ	Cañ	k1gFnPc3	Cañ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
grandslamové	grandslamový	k2eAgFnSc6d1	grandslamová
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
dvouhře	dvouhra	k1gFnSc6	dvouhra
nejdále	daleko	k6eAd3	daleko
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
2010	[number]	k4	2010
a	a	k8xC	a
French	French	k1gMnSc1	French
Open	Open	k1gInSc4	Open
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Jürgena	Jürgen	k1gMnSc4	Jürgen
Melzera	Melzer	k1gMnSc4	Melzer
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
byl	být	k5eAaImAgInS	být
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInPc7	jeho
síly	síl	k1gInPc7	síl
Japonec	Japonec	k1gMnSc1	Japonec
Kei	Kei	k1gMnSc1	Kei
Nišikori	Nišikor	k1gMnSc3	Nišikor
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
daviscupovém	daviscupový	k2eAgInSc6d1	daviscupový
týmu	tým	k1gInSc6	tým
debutoval	debutovat	k5eAaBmAgInS	debutovat
jako	jako	k9	jako
23	[number]	k4	23
<g/>
letý	letý	k2eAgInSc4d1	letý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
prvním	první	k4xOgMnSc6	první
kolem	kolem	k7c2	kolem
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
,	,	kIx,	,
když	když	k8xS	když
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
už	už	k6eAd1	už
za	za	k7c2	za
rozhodnutého	rozhodnutý	k2eAgInSc2d1	rozhodnutý
stavu	stav	k1gInSc2	stav
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
singlového	singlový	k2eAgInSc2d1	singlový
zápasu	zápas	k1gInSc2	zápas
série	série	k1gFnSc2	série
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
porazil	porazit	k5eAaPmAgInS	porazit
Victora	Victor	k1gMnSc4	Victor
Crivoie	Crivoie	k1gFnSc2	Crivoie
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
v	v	k7c6	v
sérii	série	k1gFnSc6	série
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
poměrem	poměr	k1gInSc7	poměr
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
pěti	pět	k4xCc3	pět
mezistátním	mezistátní	k2eAgNnPc3d1	mezistátní
utkáním	utkání	k1gNnSc7	utkání
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
,	,	kIx,	,
otci	otec	k1gMnSc3	otec
Besikovi	Besik	k1gMnSc3	Besik
a	a	k8xC	a
matce	matka	k1gFnSc3	matka
Anně	Anna	k1gFnSc3	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Levana	Levan	k1gMnSc4	Levan
a	a	k8xC	a
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Jekatěrinu	Jekatěrin	k1gInSc2	Jekatěrin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tenisu	tenis	k1gInSc3	tenis
ho	on	k3xPp3gInSc2	on
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
přivedla	přivést	k5eAaPmAgFnS	přivést
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
povoláním	povolání	k1gNnSc7	povolání
doktorka	doktorka	k1gFnSc1	doktorka
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
trénovala	trénovat	k5eAaImAgFnS	trénovat
až	až	k8xS	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnPc2	jeho
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
se	se	k3xPyFc4	se
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
měl	mít	k5eAaImAgMnS	mít
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
tenistou	tenista	k1gMnSc7	tenista
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc7	jeho
idolem	idol	k1gInSc7	idol
americký	americký	k2eAgMnSc1d1	americký
basketbalista	basketbalista	k1gMnSc1	basketbalista
Michael	Michael	k1gMnSc1	Michael
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
povrch	povrch	k1gInSc4	povrch
(	(	kIx(	(
<g/>
baví	bavit	k5eAaImIp3nS	bavit
ho	on	k3xPp3gMnSc4	on
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
druzích	druh	k1gInPc6	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
úderem	úder	k1gInSc7	úder
je	být	k5eAaImIp3nS	být
return	return	k1gNnSc1	return
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
je	být	k5eAaImIp3nS	být
moldavská	moldavský	k2eAgFnSc1d1	Moldavská
tenistka	tenistka	k1gFnSc1	tenistka
Maria	Maria	k1gFnSc1	Maria
Melihovová	Melihovová	k1gFnSc1	Melihovová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Nicole	Nicole	k1gFnSc2	Nicole
(	(	kIx(	(
<g/>
narozená	narozený	k2eAgFnSc1d1	narozená
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
