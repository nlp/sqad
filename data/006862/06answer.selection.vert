<s>
Teimuraz	Teimuraz	k1gInSc1	Teimuraz
Gabašvili	Gabašvili	k1gFnSc2	Gabašvili
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Т	Т	k?	Т
Г	Г	k?	Г
<g/>
,	,	kIx,	,
gruzínsky	gruzínsky	k6eAd1	gruzínsky
თ	თ	k?	თ
გ	გ	k?	გ
<g/>
,	,	kIx,	,
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1985	[number]	k4	1985
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
gruzínského	gruzínský	k2eAgInSc2d1	gruzínský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
ATP	atp	kA	atp
World	World	k1gInSc1	World
Tour	Tour	k1gInSc1	Tour
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
