<s>
Příslovce	příslovce	k1gNnSc1
</s>
<s>
Příslovce	příslovce	k1gNnSc1
(	(	kIx(
<g/>
adverbium	adverbium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neohebný	ohebný	k2eNgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
bližší	blízký	k2eAgFnPc4d2
okolnosti	okolnost	k1gFnPc4
dějů	děj	k1gInPc2
a	a	k8xC
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
mezi	mezi	k7c7
slovními	slovní	k2eAgInPc7d1
druhy	druh	k1gInPc7
řadí	řadit	k5eAaImIp3nP
na	na	k7c4
šesté	šestý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
má	mít	k5eAaImIp3nS
ve	v	k7c6
větě	věta	k1gFnSc6
funkci	funkce	k1gFnSc6
příslovečného	příslovečný	k2eAgNnSc2d1
určení	určení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
přísudku	přísudek	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
Babička	babička	k1gFnSc1
seděla	sedět	k5eAaImAgFnS
vzpřímeně	vzpřímeně	k6eAd1
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
oblíbeném	oblíbený	k2eAgNnSc6d1
křesle	křeslo	k1gNnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PODMĚT	podmět	k1gInSc1
=	=	kIx~
PŘÍSUDEK	přísudek	k1gInSc1
+	+	kIx~
PŘ	př	kA
<g/>
.	.	kIx.
<g/>
URČ	URČ	kA
<g/>
.	.	kIx.
<g/>
ZPŮSOBU	způsob	k1gInSc2
+	+	kIx~
PŘ	př	kA
<g/>
.	.	kIx.
<g/>
URČ	URČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
MÍSTA	místo	k1gNnPc1
+	+	kIx~
PŘÍVLASTEK	přívlastek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
sponou	spona	k1gFnSc7
přísudku	přísudek	k1gInSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
mu	on	k3xPp3gNnSc3
smutno	smutno	k6eAd1
<g/>
)	)	kIx)
nebo	nebo	k8xC
přívlastkem	přívlastek	k1gInSc7
neshodným	shodný	k2eNgInSc7d1
(	(	kIx(
<g/>
kotoul	kotoul	k1gInSc1
letmo	letmo	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příslovce	příslovce	k1gNnSc1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
odvozováno	odvozován	k2eAgNnSc1d1
od	od	k7c2
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
(	(	kIx(
<g/>
malý	malý	k2eAgInSc1d1
–	–	k?
málo	málo	k6eAd1
<g/>
,	,	kIx,
rychlý	rychlý	k2eAgInSc1d1
–	–	k?
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
častý	častý	k2eAgInSc1d1
–	–	k?
často	často	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
příslovcí	příslovce	k1gNnPc2
jsou	být	k5eAaImIp3nP
ustrnulé	ustrnulý	k2eAgInPc1d1
tvary	tvar	k1gInPc1
nebo	nebo	k8xC
původní	původní	k2eAgFnPc1d1
spřežky	spřežka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
Příslovce	příslovce	k1gNnSc1
<g/>
,	,	kIx,
odvozená	odvozený	k2eAgFnSc1d1
od	od	k7c2
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
Příslovečné	příslovečný	k2eAgNnSc1d1
určení	určení	k1gNnSc1
času	čas	k1gInSc2
–	–	k?
vyjadřuje	vyjadřovat	k5eAaImIp3nS
čas	čas	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
probíhá	probíhat	k5eAaImIp3nS
děj	děj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídá	odpovídat	k5eAaImIp3nS
na	na	k7c4
otázky	otázka	k1gFnPc4
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
dokdy	dokdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
odkdy	odkdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
apod.	apod.	kA
</s>
<s>
ráno	ráno	k6eAd1
<g/>
,	,	kIx,
večer	večer	k6eAd1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
zítra	zítra	k6eAd1
<g/>
,	,	kIx,
teď	teď	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Příslovečné	příslovečný	k2eAgNnSc1d1
určení	určení	k1gNnSc1
místa	místo	k1gNnSc2
–	–	k?
vyjadřuje	vyjadřovat	k5eAaImIp3nS
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
probíhá	probíhat	k5eAaImIp3nS
děj	děj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídá	odpovídat	k5eAaImIp3nS
na	na	k7c4
otázky	otázka	k1gFnPc4
kde	kde	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
kam	kam	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
apod.	apod.	kA
<g/>
:	:	kIx,
</s>
<s>
nahoře	nahoře	k6eAd1
<g/>
,	,	kIx,
nahoru	nahoru	k6eAd1
<g/>
,	,	kIx,
shora	shora	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Příslovce	příslovce	k1gNnSc1
způsobu	způsob	k1gInSc2
–	–	k?
vyjadřuje	vyjadřovat	k5eAaImIp3nS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yIgMnPc3,k3yQgMnPc3
děj	děj	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídá	odpovídat	k5eAaImIp3nS
na	na	k7c4
otázku	otázka	k1gFnSc4
jak	jak	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
:	:	kIx,
</s>
<s>
špatně	špatně	k6eAd1
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Příslovečné	příslovečný	k2eAgNnSc1d1
určení	určení	k1gNnSc1
příčiny	příčina	k1gFnSc2
(	(	kIx(
<g/>
důvodu	důvod	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
vyjadřuje	vyjadřovat	k5eAaImIp3nS
příčinu	příčina	k1gFnSc4
(	(	kIx(
<g/>
důvod	důvod	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
děj	děj	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídá	odpovídat	k5eAaImIp3nS
na	na	k7c4
otázku	otázka	k1gFnSc4
proč	proč	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
:	:	kIx,
</s>
<s>
úmyslně	úmyslně	k6eAd1
<g/>
,	,	kIx,
náhodou	náhodou	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Příslovečné	příslovečný	k2eAgNnSc1d1
určení	určení	k1gNnSc1
míry	míra	k1gFnSc2
–	–	k?
vyjadřuje	vyjadřovat	k5eAaImIp3nS
míru	míra	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
děj	děj	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídá	odpovídat	k5eAaImIp3nS
na	na	k7c4
otázky	otázka	k1gFnPc4
jak	jak	k8xS,k8xC
mnoho	mnoho	k4c1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
málo	málo	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
apod.	apod.	kA
</s>
<s>
málo	málo	k6eAd1
<g/>
,	,	kIx,
hodně	hodně	k6eAd1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
<g/>
,	,	kIx,
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zájmenná	zájmenný	k2eAgNnPc1d1
příslovce	příslovce	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
pouze	pouze	k6eAd1
naznačují	naznačovat	k5eAaImIp3nP
okolnost	okolnost	k1gFnSc4
situace	situace	k1gFnSc2
(	(	kIx(
<g/>
mají	mít	k5eAaImIp3nP
k	k	k7c3
první	první	k4xOgFnSc3
skupině	skupina	k1gFnSc3
vztah	vztah	k1gInSc4
jako	jako	k8xC,k8xS
zájmena	zájmeno	k1gNnSc2
ke	k	k7c3
jménům	jméno	k1gNnPc3
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
ukazovací	ukazovací	k2eAgMnPc1d1
–	–	k?
zde	zde	k6eAd1
<g/>
,	,	kIx,
tam	tam	k6eAd1
<g/>
,	,	kIx,
tudy	tudy	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
tázací	tázací	k2eAgMnPc1d1
–	–	k?
kde	kde	k6eAd1
<g/>
,	,	kIx,
kam	kam	k6eAd1
<g/>
,	,	kIx,
kudy	kudy	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
vztažná	vztažný	k2eAgFnSc1d1
–	–	k?
jak	jak	k6eAd1
<g/>
,	,	kIx,
proč	proč	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
neurčitá	určitý	k2eNgFnSc1d1
–	–	k?
někde	někde	k6eAd1
<g/>
,	,	kIx,
někam	někam	k6eAd1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Stupňování	stupňování	k1gNnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Většinu	většina	k1gFnSc4
příslovcí	příslovce	k1gNnPc2
odvozených	odvozený	k2eAgNnPc2d1
od	od	k7c2
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
lze	lze	k6eAd1
stupňovat	stupňovat	k5eAaImF
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
přídavná	přídavný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájmenná	zájmenný	k2eAgNnPc1d1
příslovce	příslovce	k1gNnPc1
se	se	k3xPyFc4
nestupňují	stupňovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
(	(	kIx(
<g/>
pozitiv	pozitivum	k1gNnPc2
<g/>
)	)	kIx)
–	–	k?
je	být	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
tvar	tvar	k1gInSc4
příslovce	příslovce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Koncovky	koncovka	k1gFnPc1
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
tvoří	tvořit	k5eAaImIp3nP
z	z	k7c2
přípon	přípona	k1gFnPc2
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
(	(	kIx(
<g/>
-ský	-ský	k?
→	→	k?
-sky	-ska	k1gFnSc2
<g/>
,	,	kIx,
zký	zký	k?
→	→	k?
-zky	-zka	k1gFnSc2
<g/>
,	,	kIx,
-ký	-ký	k?
→	→	k?
-ce	-ce	k?
<g/>
,	,	kIx,
-cký	-cký	k?
→	→	k?
-cky	-cka	k1gFnSc2
<g/>
,	,	kIx,
-ý	-ý	k?
→	→	k?
-ě	-ě	k?
<g/>
,	,	kIx,
-ý	-ý	k?
→	→	k?
-e	-e	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
jemný	jemný	k2eAgInSc1d1
→	→	k?
jemně	jemně	k6eAd1
<g/>
,	,	kIx,
řídký	řídký	k2eAgInSc1d1
→	→	k?
řídce	řídce	k6eAd1
</s>
<s>
Druhý	druhý	k4xOgInSc1
stupeň	stupeň	k1gInSc1
(	(	kIx(
<g/>
komparativ	komparativ	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
přípona	přípona	k1gFnSc1
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nS
z	z	k7c2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přidávají	přidávat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
přípony	přípona	k1gFnSc2
-štěji	-ště	k6eAd2
<g/>
,	,	kIx,
-čeji	-čej	k1gMnPc7
<g/>
,	,	kIx,
-čtěji	-čtěj	k1gMnPc7
<g/>
,	,	kIx,
-ěji	-ěj	k1gMnPc7
a	a	k8xC
-eji	-ej	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
jemně	jemně	k6eAd1
→	→	k?
jemněji	jemně	k6eAd2
<g/>
,	,	kIx,
řídce	řídce	k6eAd1
→	→	k?
řidčeji	řídce	k6eAd2
</s>
<s>
Třetí	třetí	k4xOgInSc1
stupeň	stupeň	k1gInSc1
(	(	kIx(
<g/>
superlativ	superlativ	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
přípona	přípona	k1gFnSc1
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
před	před	k7c4
příslovce	příslovce	k1gNnSc4
ve	v	k7c6
druhém	druhý	k4xOgInSc6
stupni	stupeň	k1gInSc6
přidáme	přidat	k5eAaPmIp1nP
předponu	předpona	k1gFnSc4
nej-	nej-	k?
<g/>
.	.	kIx.
</s>
<s>
jemněji	jemně	k6eAd2
→	→	k?
nejjemněji	jemně	k6eAd3
<g/>
,	,	kIx,
řidčeji	řídce	k6eAd2
→	→	k?
nejřidčeji	řídce	k6eAd3
</s>
<s>
Existují	existovat	k5eAaImIp3nP
také	také	k9
nepravidelná	pravidelný	k2eNgNnPc1d1
příslovce	příslovce	k1gNnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
stupňování	stupňování	k1gNnSc1
se	se	k3xPyFc4
neřídí	řídit	k5eNaImIp3nS
žádným	žádný	k3yNgNnSc7
pravidlem	pravidlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
dobře	dobře	k6eAd1
→	→	k?
lépe	dobře	k6eAd2
→	→	k?
nejlépe	dobře	k6eAd3
<g/>
,	,	kIx,
špatně	špatně	k6eAd1
→	→	k?
hůře	zle	k6eAd2
→	→	k?
nejhůře	zle	k6eAd3
<g/>
,	,	kIx,
málo	málo	k6eAd1
→	→	k?
méně	málo	k6eAd2
→	→	k?
nejméně	málo	k6eAd3
<g/>
,	,	kIx,
hodně	hodně	k6eAd1
→	→	k?
více	hodně	k6eAd2
→	→	k?
nejvíce	hodně	k6eAd3,k6eAd1
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
→	→	k?
snáze	snadno	k6eAd2
→	→	k?
nejsnáze	snadno	k6eAd3
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
pro	pro	k7c4
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fortuna	Fortuna	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85298	#num#	k4
<g/>
-	-	kIx~
<g/>
32	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
příslovce	příslovce	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Příslovce	příslovce	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Adverbium	adverbium	k1gNnSc4
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
|	|	kIx~
Český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4191574-4	4191574-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6858	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85056262	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85056262	#num#	k4
</s>
