<p>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
(	(	kIx(	(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
GN	GN	kA	GN
<g/>
'	'	kIx"	'
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
kapelu	kapela	k1gFnSc4	kapela
celosvětově	celosvětově	k6eAd1	celosvětově
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
album	album	k1gNnSc1	album
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1987	[number]	k4	1987
-	-	kIx~	-
1993	[number]	k4	1993
mnoho	mnoho	k4c1	mnoho
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
několikrát	několikrát	k6eAd1	několikrát
platinová	platinový	k2eAgNnPc4d1	platinové
alba	album	k1gNnPc4	album
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc1	Your
Illusion	Illusion	k1gInSc4	Illusion
I	i	k9	i
a	a	k8xC	a
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
II	II	kA	II
a	a	k8xC	a
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sestava	sestava	k1gFnSc1	sestava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nahrála	nahrát	k5eAaBmAgFnS	nahrát
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
postupně	postupně	k6eAd1	postupně
kvůli	kvůli	k7c3	kvůli
drogám	droga	k1gFnPc3	droga
a	a	k8xC	a
sporům	spor	k1gInPc3	spor
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Axlem	Axl	k1gMnSc7	Axl
Rosem	Ros	k1gMnSc7	Ros
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
Rose	Rose	k1gMnSc1	Rose
zůstal	zůstat	k5eAaPmAgMnS	zůstat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
jediným	jediný	k2eAgInSc7d1	jediný
jejím	její	k3xOp3gInSc7	její
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c4	po
zlepšení	zlepšení	k1gNnSc4	zlepšení
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
členy	člen	k1gMnPc7	člen
Axlem	Axl	k1gMnSc7	Axl
<g/>
,	,	kIx,	,
Duffem	Duff	k1gMnSc7	Duff
McKaganem	McKagan	k1gMnSc7	McKagan
<g/>
,	,	kIx,	,
Slashem	Slash	k1gInSc7	Slash
<g/>
,	,	kIx,	,
doplněnými	doplněná	k1gFnPc7	doplněná
o	o	k7c4	o
bubeníka	bubeník	k1gMnSc4	bubeník
Franka	Frank	k1gMnSc4	Frank
Ferrera	Ferrer	k1gMnSc4	Ferrer
<g/>
,	,	kIx,	,
klávesisty	klávesista	k1gMnPc4	klávesista
Melissu	Melissa	k1gFnSc4	Melissa
Reese	Rees	k1gMnSc2	Rees
a	a	k8xC	a
Dizzyho	Dizzy	k1gMnSc2	Dizzy
Reeda	Reed	k1gMnSc2	Reed
a	a	k8xC	a
o	o	k7c4	o
doprovodného	doprovodný	k2eAgMnSc4d1	doprovodný
kytaristu	kytarista	k1gMnSc4	kytarista
Richarda	Richard	k1gMnSc4	Richard
Fortuse	Fortuse	k1gFnSc1	Fortuse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1985	[number]	k4	1985
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
založili	založit	k5eAaPmAgMnP	založit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
,	,	kIx,	,
kytaristé	kytarista	k1gMnPc1	kytarista
Tracii	Tracium	k1gNnPc7	Tracium
Guns	Gunsa	k1gFnPc2	Gunsa
a	a	k8xC	a
Izzy	Izza	k1gFnSc2	Izza
Stradlin	Stradlina	k1gFnPc2	Stradlina
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
Ole	Ola	k1gFnSc3	Ola
Beich	Beich	k1gInSc1	Beich
(	(	kIx(	(
<g/>
brzy	brzy	k6eAd1	brzy
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
<g/>
)	)	kIx)	)
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Rob	roba	k1gFnPc2	roba
Gardner	Gardner	k1gMnSc1	Gardner
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
jmen	jméno	k1gNnPc2	jméno
skupin	skupina	k1gFnPc2	skupina
Hollywood	Hollywood	k1gInSc1	Hollywood
Rose	Rose	k1gMnSc1	Rose
a	a	k8xC	a
L.A.	L.A.	k1gMnSc1	L.A.
Guns	Gunsa	k1gFnPc2	Gunsa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
zakládající	zakládající	k2eAgMnSc1d1	zakládající
členové	člen	k1gMnPc1	člen
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzestup	vzestup	k1gInSc4	vzestup
ke	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Tracii	Tracie	k1gFnSc3	Tracie
Guns	Gunsa	k1gFnPc2	Gunsa
a	a	k8xC	a
Rob	roba	k1gFnPc2	roba
Gardner	Gardnra	k1gFnPc2	Gardnra
nechtěli	chtít	k5eNaImAgMnP	chtít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
prvního	první	k4xOgNnSc2	první
vystoupení	vystoupení	k1gNnSc2	vystoupení
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
,	,	kIx,	,
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
oslovil	oslovit	k5eAaPmAgMnS	oslovit
kytaristu	kytarista	k1gMnSc4	kytarista
Slashe	Slash	k1gMnSc4	Slash
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc4	bubeník
Stevena	Steven	k2eAgMnSc4d1	Steven
Adlera	Adler	k1gMnSc4	Adler
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
přijali	přijmout	k5eAaPmAgMnP	přijmout
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
sestava	sestava	k1gFnSc1	sestava
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
přípravu	příprava	k1gFnSc4	příprava
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
hrála	hrát	k5eAaImAgFnS	hrát
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Troubadour	Troubadoura	k1gFnPc2	Troubadoura
v	v	k7c6	v
L.A.	L.A.	k1gFnSc6	L.A.
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
popsal	popsat	k5eAaPmAgMnS	popsat
první	první	k4xOgFnSc4	první
zkoušku	zkouška	k1gFnSc4	zkouška
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
jsme	být	k5eAaImIp1nP	být
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ryzí	ryzí	k2eAgFnSc1d1	ryzí
souhra	souhra	k1gFnSc1	souhra
<g/>
.	.	kIx.	.
</s>
<s>
Jakobysme	Jakobysit	k5eAaImRp1nP	Jakobysit
hráli	hrát	k5eAaImAgMnP	hrát
už	už	k9	už
celý	celý	k2eAgInSc4d1	celý
roky	rok	k1gInPc4	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
provázely	provázet	k5eAaImAgFnP	provázet
kapelu	kapela	k1gFnSc4	kapela
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
rozbitý	rozbitý	k2eAgInSc1d1	rozbitý
automobil	automobil	k1gInSc1	automobil
<g/>
,	,	kIx,	,
neochota	neochota	k1gFnSc1	neochota
majitelů	majitel	k1gMnPc2	majitel
vyplácet	vyplácet	k5eAaImF	vyplácet
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
vystupování	vystupování	k1gNnSc4	vystupování
apod.	apod.	kA	apod.
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
napsali	napsat	k5eAaBmAgMnP	napsat
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Jungle	Jungle	k1gNnPc2	Jungle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
losangelské	losangelský	k2eAgInPc4d1	losangelský
kluby	klub	k1gInPc4	klub
-	-	kIx~	-
The	The	k1gMnSc1	The
Troubadour	Troubadour	k1gMnSc1	Troubadour
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Roxy	Roxa	k1gFnSc2	Roxa
<g/>
,	,	kIx,	,
Whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
Go	Go	k1gFnSc2	Go
Go	Go	k1gFnSc2	Go
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
musela	muset	k5eAaImAgFnS	muset
projít	projít	k5eAaPmF	projít
obdobím	období	k1gNnSc7	období
Pay	Pay	k1gFnPc2	Pay
to	ten	k3xDgNnSc1	ten
Play	play	k0	play
-	-	kIx~	-
museli	muset	k5eAaImAgMnP	muset
buď	buď	k8xC	buď
prodat	prodat	k5eAaPmF	prodat
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
lístků	lístek	k1gInPc2	lístek
a	a	k8xC	a
nebo	nebo	k8xC	nebo
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
však	však	k9	však
už	už	k6eAd1	už
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
vždy	vždy	k6eAd1	vždy
prodala	prodat	k5eAaPmAgFnS	prodat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Manažerkou	manažerka	k1gFnSc7	manažerka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Vicky	Vicek	k1gMnPc4	Vicek
Hamiltonová	Hamiltonový	k2eAgFnSc1d1	Hamiltonová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
vznikají	vznikat	k5eAaImIp3nP	vznikat
nejslavnější	slavný	k2eAgFnPc1d3	nejslavnější
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
na	na	k7c6	na
budoucím	budoucí	k2eAgNnSc6d1	budoucí
albu	album	k1gNnSc6	album
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Troubadouru	Troubadour	k1gInSc6	Troubadour
poprvé	poprvé	k6eAd1	poprvé
zazněla	zaznít	k5eAaPmAgFnS	zaznít
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Jungle	Jungle	k1gNnPc2	Jungle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
v	v	k7c6	v
Troubadouru	Troubadour	k1gInSc6	Troubadour
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Rocket	Rocket	k1gInSc1	Rocket
Queen	Queen	k1gInSc1	Queen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
"	"	kIx"	"
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
poprvé	poprvé	k6eAd1	poprvé
kapela	kapela	k1gFnSc1	kapela
vyprodává	vyprodávat	k5eAaImIp3nS	vyprodávat
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
zazněl	zaznít	k5eAaPmAgInS	zaznít
"	"	kIx"	"
<g/>
Nightrain	Nightrain	k1gInSc1	Nightrain
<g/>
"	"	kIx"	"
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Michelle	Michell	k1gInSc6	Michell
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
působila	působit	k5eAaImAgFnS	působit
na	na	k7c6	na
losangelské	losangelský	k2eAgFnSc6d1	losangelská
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
obrovský	obrovský	k2eAgInSc4d1	obrovský
rozruch	rozruch	k1gInSc4	rozruch
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
jich	on	k3xPp3gInPc2	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
i	i	k9	i
proslulý	proslulý	k2eAgMnSc1d1	proslulý
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
talentů	talent	k1gInPc2	talent
Tom	Tom	k1gMnSc1	Tom
Zutaut	Zutaut	k1gMnSc1	Zutaut
z	z	k7c2	z
Geffen	Geffna	k1gFnPc2	Geffna
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
zajímá	zajímat	k5eAaImIp3nS	zajímat
Tom	Tom	k1gMnSc1	Tom
Zutaut	Zutaut	k1gMnSc1	Zutaut
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
společností	společnost	k1gFnSc7	společnost
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
získat	získat	k5eAaPmF	získat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jí	on	k3xPp3gFnSc3	on
ale	ale	k9	ale
získal	získat	k5eAaPmAgInS	získat
právě	právě	k9	právě
Zutaut	Zutaut	k1gInSc1	Zutaut
pro	pro	k7c4	pro
Geffen	Geffen	k2eAgInSc4d1	Geffen
Records	Records	k1gInSc4	Records
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
Zutaut	Zutaut	k1gMnSc1	Zutaut
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
vytvořit	vytvořit	k5eAaPmF	vytvořit
kolem	kolem	k7c2	kolem
skupiny	skupina	k1gFnSc2	skupina
větší	veliký	k2eAgInSc4d2	veliký
mýtus	mýtus	k1gInSc4	mýtus
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nepodporoval	podporovat	k5eNaImAgMnS	podporovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
kapelu	kapela	k1gFnSc4	kapela
omezit	omezit	k5eAaPmF	omezit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgInSc4	každý
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
kapela	kapela	k1gFnSc1	kapela
odehraje	odehrát	k5eAaPmIp3nS	odehrát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
událost	událost	k1gFnSc1	událost
<g/>
.	.	kIx.	.
</s>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nechtěli	chtít	k5eNaImAgMnP	chtít
koncertů	koncert	k1gInPc2	koncert
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
často	často	k6eAd1	často
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
"	"	kIx"	"
<g/>
Fargin	Fargin	k2eAgInSc1d1	Fargin
Bastydges	Bastydges	k1gInSc1	Bastydges
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
sama	sám	k3xTgFnSc1	sám
nahrála	nahrát	k5eAaPmAgFnS	nahrát
EP	EP	kA	EP
Live	Live	k1gFnSc1	Live
?!	?!	k?	?!
<g/>
*	*	kIx~	*
<g/>
@	@	kIx~	@
Like	Like	k1gInSc1	Like
a	a	k8xC	a
Suicide	Suicid	k1gMnSc5	Suicid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezičase	mezičas	k1gInSc6	mezičas
už	už	k6eAd1	už
ale	ale	k8xC	ale
pracovala	pracovat	k5eAaImAgFnS	pracovat
s	s	k7c7	s
Mikem	Mik	k1gMnSc7	Mik
Clinkem	Clinek	k1gMnSc7	Clinek
na	na	k7c6	na
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
hity	hit	k1gInPc4	hit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Jungle	Jungle	k1gNnPc2	Jungle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Sweet	Sweet	k1gMnSc1	Sweet
Child	Child	k1gMnSc1	Child
O	O	kA	O
<g/>
'	'	kIx"	'
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
třicet	třicet	k4xCc1	třicet
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chování	chování	k1gNnSc1	chování
členů	člen	k1gInPc2	člen
ale	ale	k8xC	ale
začalo	začít	k5eAaPmAgNnS	začít
přitahovat	přitahovat	k5eAaImF	přitahovat
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
<g/>
,	,	kIx,	,
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
a	a	k8xC	a
Steven	Steven	k2eAgMnSc1d1	Steven
Adler	Adler	k1gMnSc1	Adler
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
během	během	k7c2	během
koncertů	koncert	k1gInPc2	koncert
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
členů	člen	k1gInPc2	člen
štábů	štáb	k1gInPc2	štáb
prý	prý	k9	prý
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
koncertu	koncert	k1gInSc6	koncert
museli	muset	k5eAaImAgMnP	muset
Slashe	Slash	k1gFnPc4	Slash
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
doslova	doslova	k6eAd1	doslova
dotlačit	dotlačit	k5eAaPmF	dotlačit
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
skončení	skončení	k1gNnSc6	skončení
ztratil	ztratit	k5eAaPmAgInS	ztratit
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
festivalu	festival	k1gInSc2	festival
Monsters	Monsters	k1gInSc1	Monsters
of	of	k?	of
Rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
dva	dva	k4xCgMnPc1	dva
fanoušci	fanoušek	k1gMnPc1	fanoušek
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
začal	začít	k5eAaPmAgInS	začít
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
vystoupení	vystoupení	k1gNnSc2	vystoupení
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
tlačit	tlačit	k5eAaImF	tlačit
k	k	k7c3	k
pódiu	pódium	k1gNnSc3	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
obviňovala	obviňovat	k5eAaImAgNnP	obviňovat
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
fanoušků	fanoušek	k1gMnPc2	fanoušek
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
o	o	k7c6	o
incidentu	incident	k1gInSc6	incident
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
až	až	k6eAd1	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
koncertu	koncert	k1gInSc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
incidenty	incident	k1gInPc1	incident
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gInSc4	Roses
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k8xS	jako
nejnebezpečnější	bezpečný	k2eNgFnPc1d3	nejnebezpečnější
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
G	G	kA	G
N	N	kA	N
<g/>
'	'	kIx"	'
R	R	kA	R
Lies	Liesa	k1gFnPc2	Liesa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
také	také	k6eAd1	také
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
EP	EP	kA	EP
Live	Liv	k1gFnSc2	Liv
?!	?!	k?	?!
<g/>
*	*	kIx~	*
<g/>
@	@	kIx~	@
Like	Like	k1gInSc1	Like
a	a	k8xC	a
Suicide	Suicid	k1gMnSc5	Suicid
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
In	In	k1gFnSc1	In
A	a	k8xC	a
Million	Million	k1gInSc1	Million
<g/>
"	"	kIx"	"
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
velkou	velký	k2eAgFnSc4d1	velká
kontroverzi	kontroverze	k1gFnSc4	kontroverze
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
textu	text	k1gInSc6	text
objevují	objevovat	k5eAaImIp3nP	objevovat
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
niggers	niggers	k1gInSc1	niggers
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
negři	negr	k1gMnPc1	negr
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
faggots	faggots	k6eAd1	faggots
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
buzeranti	buzerant	k1gMnPc1	buzerant
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
obviněním	obvinění	k1gNnPc3	obvinění
skupiny	skupina	k1gFnSc2	skupina
z	z	k7c2	z
rasismu	rasismus	k1gInSc2	rasismus
a	a	k8xC	a
nesnášenlivosti	nesnášenlivost	k1gFnSc2	nesnášenlivost
vůči	vůči	k7c3	vůči
homosexuálům	homosexuál	k1gMnPc3	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
obvinění	obvinění	k1gNnSc2	obvinění
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
homosexuální	homosexuální	k2eAgMnPc4d1	homosexuální
zpěváky	zpěvák	k1gMnPc4	zpěvák
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Freddie	Freddie	k1gFnSc1	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
a	a	k8xC	a
Eltona	Elton	k1gMnSc2	Elton
Johna	John	k1gMnSc2	John
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byli	být	k5eAaImAgMnP	být
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
nominováni	nominován	k2eAgMnPc1d1	nominován
na	na	k7c4	na
několik	několik	k4yIc4	několik
American	Americana	k1gFnPc2	Americana
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
McKagan	McKagana	k1gFnPc2	McKagana
a	a	k8xC	a
Slash	Slasha	k1gFnPc2	Slasha
byli	být	k5eAaImAgMnP	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
mluvili	mluvit	k5eAaImAgMnP	mluvit
vulgárně	vulgárně	k6eAd1	vulgárně
<g/>
,	,	kIx,	,
když	když	k8xS	když
přebírali	přebírat	k5eAaImAgMnP	přebírat
ceny	cena	k1gFnPc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
heavy	heav	k1gMnPc4	heav
metalové	metalový	k2eAgNnSc1d1	metalové
album	album	k1gNnSc1	album
za	za	k7c4	za
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
heavy	heava	k1gFnPc4	heava
metalovou	metalový	k2eAgFnSc4d1	metalová
píseň	píseň	k1gFnSc4	píseň
za	za	k7c4	za
"	"	kIx"	"
<g/>
Paradise	Paradise	k1gFnSc2	Paradise
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
incidentu	incident	k1gInSc3	incident
byly	být	k5eAaImAgInP	být
následující	následující	k2eAgInPc1d1	následující
přenosy	přenos	k1gInPc1	přenos
vysílány	vysílán	k2eAgInPc1d1	vysílán
s	s	k7c7	s
pětisekundovým	pětisekundový	k2eAgNnSc7d1	pětisekundový
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Sweet	Sweet	k1gMnSc1	Sweet
Child	Child	k1gMnSc1	Child
O	O	kA	O
<g/>
'	'	kIx"	'
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
píseň	píseň	k1gFnSc1	píseň
z	z	k7c2	z
žánru	žánr	k1gInSc2	žánr
pop	pop	k1gInSc1	pop
<g/>
/	/	kIx~	/
<g/>
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
začalo	začít	k5eAaPmAgNnS	začít
požadovat	požadovat	k5eAaImF	požadovat
po	po	k7c6	po
některých	některý	k3yIgMnPc6	některý
členech	člen	k1gMnPc6	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změnili	změnit	k5eAaPmAgMnP	změnit
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
hrozil	hrozit	k5eAaImAgMnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupinu	skupina	k1gFnSc4	skupina
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nebudou	být	k5eNaImBp3nP	být
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
donutil	donutit	k5eAaPmAgMnS	donutit
členy	člen	k1gMnPc4	člen
léčení	léčení	k1gNnSc2	léčení
podstoupit	podstoupit	k5eAaPmF	podstoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Use	usus	k1gInSc5	usus
Your	Your	k1gMnSc1	Your
Illusion	Illusion	k1gInSc1	Illusion
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
svůj	svůj	k3xOyFgInSc4	svůj
zatím	zatím	k6eAd1	zatím
nejambicióznější	ambiciózní	k2eAgInSc4d3	nejambicióznější
projekt	projekt	k1gInSc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Bubeník	Bubeník	k1gMnSc1	Bubeník
Steven	Stevna	k1gFnPc2	Stevna
Adler	Adler	k1gMnSc1	Adler
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
svých	svůj	k3xOyFgFnPc2	svůj
potíží	potíž	k1gFnPc2	potíž
s	s	k7c7	s
kokainem	kokain	k1gInSc7	kokain
a	a	k8xC	a
heroinem	heroin	k1gInSc7	heroin
schopen	schopen	k2eAgMnSc1d1	schopen
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c4	v
nahrávání	nahrávání	k1gNnSc4	nahrávání
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
říkal	říkat	k5eAaImAgMnS	říkat
v	v	k7c6	v
interview	interview	k1gNnSc6	interview
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
Roses	Roses	k1gInSc1	Roses
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Civil	civil	k1gMnSc1	civil
War	War	k1gMnSc1	War
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ji	on	k3xPp3gFnSc4	on
nahrát	nahrát	k5eAaPmF	nahrát
až	až	k9	až
na	na	k7c4	na
šedesátý	šedesátý	k4xOgInSc4	šedesátý
pokus	pokus	k1gInSc4	pokus
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
se	se	k3xPyFc4	se
Stevenem	Steven	k1gMnSc7	Steven
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
Adler	Adler	k1gMnSc1	Adler
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1990	[number]	k4	1990
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Matt	Matt	k2eAgInSc4d1	Matt
Sorum	Sorum	k1gInSc4	Sorum
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Cult	Cult	k1gMnSc1	Cult
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
klávesista	klávesista	k1gMnSc1	klávesista
Dizzy	Dizza	k1gFnSc2	Dizza
Reed	Reed	k1gMnSc1	Reed
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
dost	dost	k6eAd1	dost
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
dvojalba	dvojalbum	k1gNnSc2	dvojalbum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vydat	vydat	k5eAaPmF	vydat
dvě	dva	k4xCgNnPc4	dva
samostatná	samostatný	k2eAgNnPc4d1	samostatné
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc1	Your
Illusion	Illusion	k1gInSc4	Illusion
I	i	k9	i
a	a	k8xC	a
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
nikomu	nikdo	k3yNnSc3	nikdo
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alb	alba	k1gFnPc2	alba
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
28	[number]	k4	28
měsíců	měsíc	k1gInPc2	měsíc
trvající	trvající	k2eAgFnSc1d1	trvající
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
Tour	Tour	k1gInSc1	Tour
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
incidentům	incident	k1gInPc3	incident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1991	[number]	k4	1991
během	běh	k1gInSc7	běh
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
skočil	skočit	k5eAaPmAgMnS	skočit
Axl	Axl	k1gFnSc4	Axl
Rose	Rosa	k1gFnSc3	Rosa
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
a	a	k8xC	a
napadl	napadnout	k5eAaPmAgMnS	napadnout
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
jejich	jejich	k3xOp3gNnSc4	jejich
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c4	na
videokameru	videokamera	k1gFnSc4	videokamera
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Rose	Rose	k1gMnSc1	Rose
ukončil	ukončit	k5eAaPmAgMnS	ukončit
koncert	koncert	k1gInSc4	koncert
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
zákulisí	zákulisí	k1gNnSc2	zákulisí
a	a	k8xC	a
dav	dav	k1gInSc1	dav
fanoušků	fanoušek	k1gMnPc2	fanoušek
začal	začít	k5eAaPmAgInS	začít
páchat	páchat	k5eAaImF	páchat
výtržnosti	výtržnost	k1gFnPc4	výtržnost
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vyprovokování	vyprovokování	k1gNnSc2	vyprovokování
výtržností	výtržnost	k1gFnPc2	výtržnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zatknout	zatknout	k5eAaPmF	zatknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kapela	kapela	k1gFnSc1	kapela
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rose	Rose	k1gMnSc1	Rose
incident	incident	k1gInSc4	incident
nevyprovokoval	vyprovokovat	k5eNaPmAgMnS	vyprovokovat
a	a	k8xC	a
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
Rosem	Rose	k1gMnSc6	Rose
kytarista	kytarista	k1gMnSc1	kytarista
Izzy	Izza	k1gFnSc2	Izza
Stradlin	Stradlin	k1gInSc1	Stradlin
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gNnSc4	on
Gilby	Gilba	k1gFnPc1	Gilba
Clarke	Clarke	k1gFnPc2	Clarke
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
na	na	k7c4	na
Freddie	Freddie	k1gFnPc4	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
Tribute	tribut	k1gInSc5	tribut
Concert	Concert	k1gInSc4	Concert
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
koncert	koncert	k1gInSc4	koncert
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
památky	památka	k1gFnSc2	památka
Freddie	Freddie	k1gFnSc2	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
a	a	k8xC	a
zahráli	zahrát	k5eAaPmAgMnP	zahrát
tam	tam	k6eAd1	tam
dvě	dva	k4xCgFnPc4	dva
své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
strahovském	strahovský	k2eAgInSc6d1	strahovský
stadionu	stadion	k1gInSc6	stadion
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
a	a	k8xC	a
Faith	Faitha	k1gFnPc2	Faitha
No	no	k9	no
More	mor	k1gInSc5	mor
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
i	i	k9	i
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
společně	společně	k6eAd1	společně
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Metallica	Metallica	k1gMnSc1	Metallica
(	(	kIx(	(
<g/>
Axl	Axl	k1gMnSc1	Axl
chtěl	chtít	k5eAaImAgMnS	chtít
původně	původně	k6eAd1	původně
pozvat	pozvat	k5eAaPmF	pozvat
skupinu	skupina	k1gFnSc4	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gMnSc1	její
frontman	frontman	k1gMnSc1	frontman
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
byl	být	k5eAaImAgMnS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
Metallicy	Metallica	k1gFnSc2	Metallica
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gInSc4	Hetfield
zraněn	zraněn	k2eAgInSc4d1	zraněn
pyrotechnikou	pyrotechnika	k1gFnSc7	pyrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallic	k2eAgFnSc1d1	Metallica
byla	být	k5eAaImAgFnS	být
tímto	tento	k3xDgNnSc7	tento
donucena	donucen	k2eAgFnSc1d1	donucena
ukončit	ukončit	k5eAaPmF	ukončit
své	svůj	k3xOyFgNnSc4	svůj
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
byli	být	k5eAaImAgMnP	být
požádáni	požádat	k5eAaPmNgMnP	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
zdržení	zdržení	k1gNnSc6	zdržení
se	se	k3xPyFc4	se
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
objevili	objevit	k5eAaPmAgMnP	objevit
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
čtyřech	čtyři	k4xCgFnPc6	čtyři
písních	píseň	k1gFnPc6	píseň
se	se	k3xPyFc4	se
Rose	Rose	k1gMnSc1	Rose
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vystoupení	vystoupení	k1gNnSc4	vystoupení
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
opět	opět	k6eAd1	opět
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
výtržnosti	výtržnost	k1gFnSc2	výtržnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
také	také	k9	také
natočila	natočit	k5eAaBmAgFnS	natočit
několik	několik	k4yIc4	několik
videoklipů	videoklip	k1gInPc2	videoklip
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
podpořit	podpořit	k5eAaPmF	podpořit
prodej	prodej	k1gInSc4	prodej
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
November	November	k1gMnSc1	November
Rain	Rain	k1gMnSc1	Rain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejdražší	drahý	k2eAgInPc4d3	nejdražší
kdy	kdy	k6eAd1	kdy
natočené	natočený	k2eAgInPc4d1	natočený
videoklipy	videoklip	k1gInPc4	videoklip
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
November	November	k1gMnSc1	November
Rain	Rain	k1gMnSc1	Rain
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejhranějším	hraný	k2eAgInSc7d3	nejhranější
videoklipem	videoklip	k1gInSc7	videoklip
na	na	k7c6	na
MTV	MTV	kA	MTV
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
show	show	k1gFnPc2	show
skupina	skupina	k1gFnSc1	skupina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
píseň	píseň	k1gFnSc4	píseň
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Eltonem	Elton	k1gMnSc7	Elton
Johnem	John	k1gMnSc7	John
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1993	[number]	k4	1993
si	se	k3xPyFc3	se
Gilby	Gilb	k1gInPc4	Gilb
Clarke	Clark	k1gFnSc2	Clark
zlomil	zlomit	k5eAaPmAgInS	zlomit
zápěstí	zápěstí	k1gNnSc4	zápěstí
při	při	k7c6	při
motocyklové	motocyklový	k2eAgFnSc6d1	motocyklová
nehodě	nehoda	k1gFnSc6	nehoda
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
jej	on	k3xPp3gInSc4	on
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
tady	tady	k6eAd1	tady
vrátil	vrátit	k5eAaPmAgInS	vrátit
Izzy	Izza	k1gFnSc2	Izza
Stradlin	Stradlin	k1gInSc1	Stradlin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odehrál	odehrát	k5eAaPmAgInS	odehrát
pět	pět	k4xCc4	pět
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
turné	turné	k1gNnSc1	turné
skončilo	skončit	k5eAaPmAgNnS	skončit
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1993	[number]	k4	1993
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
několik	několik	k4yIc4	několik
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
28	[number]	k4	28
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
bylo	být	k5eAaImAgNnS	být
odehráno	odehrát	k5eAaPmNgNnS	odehrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Aires	k1gInSc1	Aires
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
posledním	poslední	k2eAgInSc7d1	poslední
koncertem	koncert	k1gInSc7	koncert
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úpadek	úpadek	k1gInSc1	úpadek
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1993	[number]	k4	1993
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Spaghetti	spaghetti	k1gInPc2	spaghetti
Incident	incident	k1gInSc1	incident
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
především	především	k6eAd1	především
punkových	punkový	k2eAgFnPc2d1	punková
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
ovšem	ovšem	k9	ovšem
nezaznamenalo	zaznamenat	k5eNaPmAgNnS	zaznamenat
takový	takový	k3xDgInSc4	takový
úspěch	úspěch	k1gInSc4	úspěch
jako	jako	k8xS	jako
ta	ten	k3xDgFnSc1	ten
předchozí	předchozí	k2eAgFnSc1d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Gilby	Gilb	k1gInPc4	Gilb
Clarke	Clarke	k1gFnPc2	Clarke
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členů	člen	k1gMnPc2	člen
jeho	jeho	k3xOp3gFnSc2	jeho
schopnosti	schopnost	k1gFnSc2	schopnost
psát	psát	k5eAaImF	psát
písně	píseň	k1gFnPc4	píseň
nedosahovaly	dosahovat	k5eNaImAgFnP	dosahovat
takových	takový	k3xDgFnPc2	takový
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
užitečné	užitečný	k2eAgInPc1d1	užitečný
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
dalším	další	k2eAgInSc6d1	další
projektu	projekt	k1gInSc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c2	za
Clarka	Clark	k1gInSc2	Clark
přítel	přítel	k1gMnSc1	přítel
Axla	Axla	k1gMnSc1	Axla
Rose	Rose	k1gMnSc1	Rose
Paul	Paul	k1gMnSc1	Paul
Tobias	Tobias	k1gMnSc1	Tobias
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
skupiny	skupina	k1gFnSc2	skupina
nevycházel	vycházet	k5eNaImAgInS	vycházet
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
nahrála	nahrát	k5eAaPmAgFnS	nahrát
skupina	skupina	k1gFnSc1	skupina
cover	cover	k1gInSc1	cover
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnPc1	píseň
od	od	k7c2	od
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stones	k1gInSc4	Stones
"	"	kIx"	"
<g/>
Sympathy	Sympath	k1gInPc4	Sympath
for	forum	k1gNnPc2	forum
the	the	k?	the
Devil	Devil	k1gMnSc1	Devil
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Interview	interview	k1gNnSc2	interview
s	s	k7c7	s
upírem	upír	k1gMnSc7	upír
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Tobias	Tobias	k1gMnSc1	Tobias
nahrál	nahrát	k5eAaPmAgMnS	nahrát
několik	několik	k4yIc4	několik
vlastních	vlastní	k2eAgNnPc2d1	vlastní
sól	sólo	k1gNnPc2	sólo
jako	jako	k8xS	jako
přídavek	přídavka	k1gFnPc2	přídavka
k	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
nahrál	nahrát	k5eAaBmAgMnS	nahrát
Slash	Slash	k1gMnSc1	Slash
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
zuřil	zuřit	k5eAaImAgMnS	zuřit
<g/>
,	,	kIx,	,
když	když	k8xS	když
slyšel	slyšet	k5eAaImAgMnS	slyšet
konečnou	konečný	k2eAgFnSc4d1	konečná
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgNnP	být
použita	použit	k2eAgNnPc1d1	použito
Tobiasova	Tobiasův	k2eAgNnPc1d1	Tobiasův
sóla	sólo	k1gNnPc1	sólo
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Slashův	Slashův	k2eAgInSc1d1	Slashův
definitivní	definitivní	k2eAgInSc1d1	definitivní
odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
skupinu	skupina	k1gFnSc4	skupina
střídavě	střídavě	k6eAd1	střídavě
opouštěl	opouštět	k5eAaImAgMnS	opouštět
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vracel	vracet	k5eAaImAgInS	vracet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Slash	Slash	k1gInSc1	Slash
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
kapele	kapela	k1gFnSc3	kapela
Slash	Slash	k1gInSc1	Slash
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gFnSc7	Snakepit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Five	Five	k1gFnSc7	Five
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Clock	Clock	k1gInSc4	Clock
Somewhere	Somewher	k1gInSc5	Somewher
<g/>
.	.	kIx.	.
</s>
<s>
Slashe	Slashe	k6eAd1	Slashe
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
nahradil	nahradit	k5eAaPmAgInS	nahradit
Robin	robin	k2eAgInSc1d1	robin
Finck	Finck	k1gInSc1	Finck
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
vyhozen	vyhozen	k2eAgInSc1d1	vyhozen
Matt	Matt	k2eAgInSc1d1	Matt
Sorum	Sorum	k1gInSc1	Sorum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pohádal	pohádat	k5eAaPmAgMnS	pohádat
s	s	k7c7	s
Rosem	Ros	k1gMnSc7	Ros
ohledně	ohledně	k7c2	ohledně
negativních	negativní	k2eAgInPc2d1	negativní
komentářů	komentář	k1gInPc2	komentář
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
o	o	k7c6	o
Slashovi	Slash	k1gMnSc6	Slash
pronesl	pronést	k5eAaPmAgMnS	pronést
Paul	Paul	k1gMnSc1	Paul
Tobias	Tobias	k1gMnSc1	Tobias
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Axl	Axl	k?	Axl
Rose	Rose	k1gMnSc1	Rose
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
ze	z	k7c2	z
slavné	slavný	k2eAgFnSc2d1	slavná
sestavy	sestava	k1gFnSc2	sestava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nahrála	nahrát	k5eAaBmAgFnS	nahrát
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Noví	Nový	k1gMnPc1	Nový
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
pracuje	pracovat	k5eAaImIp3nS	pracovat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
Rose	Rose	k1gMnSc1	Rose
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
desce	deska	k1gFnSc6	deska
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
bubeník	bubeník	k1gMnSc1	bubeník
Josh	Josh	k1gMnSc1	Josh
Freese	Freese	k1gFnSc1	Freese
(	(	kIx(	(
<g/>
člen	člen	k1gMnSc1	člen
The	The	k1gFnSc2	The
Vandals	Vandals	k1gInSc1	Vandals
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
Tommy	Tomma	k1gFnSc2	Tomma
Stinson	Stinson	k1gMnSc1	Stinson
(	(	kIx(	(
<g/>
ex-člen	ex-člit	k5eAaPmNgMnS	ex-člit
The	The	k1gFnSc7	The
Replacements	Replacementsa	k1gFnPc2	Replacementsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klávesisté	klávesista	k1gMnPc1	klávesista
Dizzy	Dizza	k1gFnSc2	Dizza
Reed	Reed	k1gInSc1	Reed
a	a	k8xC	a
Chris	Chris	k1gFnSc1	Chris
Pitman	Pitman	k1gMnSc1	Pitman
a	a	k8xC	a
kytaristé	kytarista	k1gMnPc1	kytarista
Robin	Robina	k1gFnPc2	Robina
Finck	Finck	k1gInSc1	Finck
(	(	kIx(	(
<g/>
ex-člen	ex-člen	k2eAgInSc1d1	ex-člen
Nine	Nine	k1gInSc1	Nine
Inch	Incha	k1gFnPc2	Incha
Nails	Nailsa	k1gFnPc2	Nailsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Tobias	Tobias	k1gMnSc1	Tobias
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Oh	oh	k0	oh
My	my	k3xPp1nPc1	my
God	God	k1gMnPc1	God
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
předehru	předehra	k1gFnSc4	předehra
k	k	k7c3	k
připravovanému	připravovaný	k2eAgNnSc3d1	připravované
novému	nový	k2eAgNnSc3d1	nové
albu	album	k1gNnSc3	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Chinese	Chinese	k1gFnSc2	Chinese
Democracy	Democraca	k1gFnSc2	Democraca
<g/>
.	.	kIx.	.
</s>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
také	také	k6eAd1	také
vydali	vydat	k5eAaPmAgMnP	vydat
desku	deska	k1gFnSc4	deska
Live	Liv	k1gMnSc2	Liv
Era	Era	k1gMnSc2	Era
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
87	[number]	k4	87
<g/>
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
93	[number]	k4	93
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
živé	živá	k1gFnPc4	živá
nahrávky	nahrávka	k1gFnSc2	nahrávka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
do	do	k7c2	do
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
přišli	přijít	k5eAaPmAgMnP	přijít
avantgardní	avantgardní	k2eAgMnSc1d1	avantgardní
kytarista	kytarista	k1gMnSc1	kytarista
Buckethead	Buckethead	k1gInSc1	Buckethead
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Josh	Josh	k1gInSc1	Josh
Freese	Freesa	k1gFnSc3	Freesa
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Bryanem	Bryan	k1gMnSc7	Bryan
Mantiou	Mantia	k1gMnSc7	Mantia
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Primus	primus	k1gMnSc1	primus
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
sestava	sestava	k1gFnSc1	sestava
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgNnSc6	jeden
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc4	Vegas
a	a	k8xC	a
druhém	druhý	k4xOgNnSc6	druhý
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
časů	čas	k1gInPc2	čas
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
dvě	dva	k4xCgNnPc1	dva
vystoupení	vystoupení	k1gNnPc1	vystoupení
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
opět	opět	k6eAd1	opět
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
Paul	Paul	k1gMnSc1	Paul
Tobias	Tobias	k1gMnSc1	Tobias
kvůli	kvůli	k7c3	kvůli
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
s	s	k7c7	s
protahováním	protahování	k1gNnSc7	protahování
natáčení	natáčení	k1gNnSc4	natáčení
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Richard	Richard	k1gMnSc1	Richard
Fortus	Fortus	k1gMnSc1	Fortus
<g/>
,	,	kIx,	,
ex-člen	ex-člen	k2eAgMnSc1d1	ex-člen
Love	lov	k1gInSc5	lov
Spit	Spit	k1gInSc1	Spit
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
oznámila	oznámit	k5eAaPmAgFnS	oznámit
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
také	také	k9	také
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
MTV	MTV	kA	MTV
Music	Musice	k1gFnPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgNnSc1d1	americké
turné	turné	k1gNnSc1	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
podpořit	podpořit	k5eAaPmF	podpořit
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
album	album	k1gNnSc1	album
Chinese	Chinese	k1gFnSc2	Chinese
Democracy	Democraca	k1gFnSc2	Democraca
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
Rose	Ros	k1gMnSc4	Ros
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
a	a	k8xC	a
opět	opět	k6eAd1	opět
propukly	propuknout	k5eAaPmAgFnP	propuknout
výtržnosti	výtržnost	k1gFnPc1	výtržnost
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
se	s	k7c7	s
smíšenými	smíšený	k2eAgInPc7d1	smíšený
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc4	lístek
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
prodávaly	prodávat	k5eAaImAgFnP	prodávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
New	New	k1gMnSc6	New
Yorku	York	k1gInSc2	York
byly	být	k5eAaImAgFnP	být
vyprodány	vyprodat	k5eAaPmNgFnP	vyprodat
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Rose	Rose	k1gMnSc1	Rose
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
promotér	promotér	k1gMnSc1	promotér
zbytek	zbytek	k1gInSc4	zbytek
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
skupina	skupina	k1gFnSc1	skupina
Offspring	Offspring	k1gInSc1	Offspring
vydání	vydání	k1gNnSc2	vydání
nové	nový	k2eAgFnSc2d1	nová
desky	deska	k1gFnSc2	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Chinese	Chinese	k1gFnSc1	Chinese
Democracy	Democracy	k1gInPc1	Democracy
(	(	kIx(	(
<g/>
You	You	k1gFnSc1	You
Snooze	Snooz	k1gInSc5	Snooz
You	You	k1gMnSc5	You
Lose	los	k1gMnSc5	los
<g/>
)	)	kIx)	)
jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
ohlášeného	ohlášený	k2eAgMnSc4d1	ohlášený
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nevydaného	vydaný	k2eNgNnSc2d1	nevydané
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Axl	Axl	k?	Axl
Rose	Rose	k1gMnSc1	Rose
jim	on	k3xPp3gMnPc3	on
přes	přes	k7c4	přes
právníky	právník	k1gMnPc7	právník
poslal	poslat	k5eAaPmAgInS	poslat
výhrůžný	výhrůžný	k2eAgInSc1d1	výhrůžný
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změnili	změnit	k5eAaPmAgMnP	změnit
název	název	k1gInSc4	název
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Offspring	Offspring	k1gInSc4	Offspring
poté	poté	k6eAd1	poté
vydali	vydat	k5eAaPmAgMnP	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
žert	žert	k1gInSc4	žert
a	a	k8xC	a
že	že	k8xS	že
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
takto	takto	k6eAd1	takto
svou	svůj	k3xOyFgFnSc4	svůj
připravovanou	připravovaný	k2eAgFnSc4d1	připravovaná
desku	deska	k1gFnSc4	deska
nikdy	nikdy	k6eAd1	nikdy
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buckethead	Buckethead	k6eAd1	Buckethead
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2004	[number]	k4	2004
a	a	k8xC	a
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
nebyla	být	k5eNaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2004	[number]	k4	2004
také	také	k6eAd1	také
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
best-of	bestf	k1gMnSc1	best-of
album	album	k1gNnSc1	album
Greatest	Greatest	k1gMnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
<g/>
.	.	kIx.	.
</s>
<s>
Rose	Rose	k1gMnSc1	Rose
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nikdo	nikdo	k3yNnSc1	nikdo
nekonzultoval	konzultovat	k5eNaImAgInS	konzultovat
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgInS	podniknout
i	i	k9	i
neúspěšné	úspěšný	k2eNgInPc4d1	neúspěšný
právní	právní	k2eAgInPc4d1	právní
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gNnSc3	jeho
vydání	vydání	k1gNnSc3	vydání
zabránil	zabránit	k5eAaPmAgInS	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2004	[number]	k4	2004
vydalo	vydat	k5eAaPmAgNnS	vydat
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Rosův	Rosův	k2eAgInSc4d1	Rosův
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
<g/>
,	,	kIx,	,
Cleopatra	Cleopatra	k1gFnSc1	Cleopatra
Records	Records	k1gInSc1	Records
album	album	k1gNnSc4	album
The	The	k1gMnPc2	The
Roots	Rootsa	k1gFnPc2	Rootsa
of	of	k?	of
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
výběr	výběr	k1gInSc1	výběr
dem	dem	k?	dem
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
kdysi	kdysi	k6eAd1	kdysi
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
nahrál	nahrát	k5eAaPmAgMnS	nahrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2005	[number]	k4	2005
unikla	uniknout	k5eAaPmAgFnS	uniknout
na	na	k7c4	na
internet	internet	k1gInSc4	internet
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Management	management	k1gInSc1	management
skupiny	skupina	k1gFnSc2	skupina
požadoval	požadovat	k5eAaImAgInS	požadovat
její	její	k3xOp3gNnSc4	její
odstranění	odstranění	k1gNnSc4	odstranění
z	z	k7c2	z
webu	web	k1gInSc2	web
a	a	k8xC	a
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
nekvalitní	kvalitní	k2eNgFnPc4d1	nekvalitní
demo	demo	k2eAgFnPc4d1	demo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neodráží	odrážet	k5eNaImIp3nP	odrážet
potenciál	potenciál	k1gInSc4	potenciál
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšími	další	k2eAgMnPc7d1	další
úniku	únik	k1gInSc3	únik
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
internet	internet	k1gInSc4	internet
dostaly	dostat	k5eAaPmAgFnP	dostat
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Better	Better	k1gInSc1	Better
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Catcher	Catchra	k1gFnPc2	Catchra
In	In	k1gFnSc2	In
The	The	k1gMnSc1	The
Rye	Rye	k1gMnSc1	Rye
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
Was	Was	k1gMnSc5	Was
a	a	k8xC	a
Time	Time	k1gNnPc2	Time
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
zahájili	zahájit	k5eAaPmAgMnP	zahájit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
nové	nový	k2eAgNnSc4d1	nové
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Sazka	Sazka	k1gFnSc1	Sazka
Areně	Areeň	k1gFnPc1	Areeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
předkapely	předkapela	k1gFnSc2	předkapela
dělali	dělat	k5eAaImAgMnP	dělat
Avenged	Avenged	k1gMnSc1	Avenged
Sevenfold	Sevenfold	k1gMnSc1	Sevenfold
a	a	k8xC	a
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
rovněž	rovněž	k9	rovněž
přišel	přijít	k5eAaPmAgMnS	přijít
nový	nový	k2eAgMnSc1d1	nový
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
Ron	Ron	k1gMnSc1	Ron
Thal	Thal	k1gMnSc1	Thal
přezdívaný	přezdívaný	k2eAgInSc4d1	přezdívaný
Bumblefoot	Bumblefoot	k1gInSc4	Bumblefoot
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
mnoha	mnoho	k4c6	mnoho
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
toho	ten	k3xDgNnSc2	ten
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
host	host	k1gMnSc1	host
Izzy	Izza	k1gFnSc2	Izza
Stradlin	Stradlin	k2eAgMnSc1d1	Stradlin
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kytarista	kytarista	k1gMnSc1	kytarista
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
evropského	evropský	k2eAgNnSc2d1	Evropské
turné	turné	k1gNnSc2	turné
odjel	odjet	k5eAaPmAgMnS	odjet
domů	domů	k6eAd1	domů
bubeník	bubeník	k1gMnSc1	bubeník
Bryan	Bryan	k1gMnSc1	Bryan
Mantia	Mantia	k1gFnSc1	Mantia
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodilo	narodit	k5eAaPmAgNnS	narodit
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
turné	turné	k1gNnSc2	turné
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Frank	Frank	k1gMnSc1	Frank
Ferrer	Ferrer	k1gMnSc1	Ferrer
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Ferrer	Ferrer	k1gInSc1	Ferrer
oznámen	oznámit	k5eAaPmNgInS	oznámit
jako	jako	k8xS	jako
regulérní	regulérní	k2eAgMnSc1d1	regulérní
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
náhradník	náhradník	k1gMnSc1	náhradník
za	za	k7c4	za
Mantiu	Mantium	k1gNnSc3	Mantium
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Frank	Frank	k1gMnSc1	Frank
Ferrer	Ferrer	k1gMnSc1	Ferrer
stal	stát	k5eAaPmAgMnS	stát
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
bubeníkem	bubeník	k1gMnSc7	bubeník
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Mantia	Mantia	k1gFnSc1	Mantia
už	už	k6eAd1	už
jejím	její	k3xOp3gMnSc7	její
členem	člen	k1gMnSc7	člen
fakticky	fakticky	k6eAd1	fakticky
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
občas	občas	k6eAd1	občas
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předběžné	předběžný	k2eAgNnSc1d1	předběžné
datum	datum	k1gNnSc1	datum
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
Chinese	Chinese	k1gFnSc2	Chinese
Democracy	Democraca	k1gFnSc2	Democraca
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
nebyl	být	k5eNaImAgInS	být
dodržen	dodržen	k2eAgMnSc1d1	dodržen
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
webové	webový	k2eAgFnSc6d1	webová
stránce	stránka	k1gFnSc6	stránka
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahrávání	nahrávání	k1gNnSc1	nahrávání
alba	album	k1gNnSc2	album
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
alba	album	k1gNnSc2	album
trvalo	trvat	k5eAaImAgNnS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
a	a	k8xC	a
stálo	stát	k5eAaImAgNnS	stát
už	už	k6eAd1	už
přes	přes	k7c4	přes
třináct	třináct	k4xCc4	třináct
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
činí	činit	k5eAaImIp3nS	činit
nejdražší	drahý	k2eAgNnSc4d3	nejdražší
album	album	k1gNnSc4	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
unikly	uniknout	k5eAaPmAgFnP	uniknout
na	na	k7c4	na
internet	internet	k1gInSc4	internet
písně	píseň	k1gFnPc1	píseň
Chinese	Chinese	k1gFnSc2	Chinese
Democracy	Democraca	k1gFnSc2	Democraca
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
S	s	k7c7	s
a	a	k8xC	a
The	The	k1gFnSc3	The
Blues	blues	k1gNnSc1	blues
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
délce	délka	k1gFnSc6	délka
a	a	k8xC	a
ve	v	k7c6	v
výborné	výborný	k2eAgFnSc6d1	výborná
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
kapela	kapela	k1gFnSc1	kapela
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
domixováno	domixovat	k5eAaImNgNnS	domixovat
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc1	Vánoce
2007	[number]	k4	2007
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
ho	on	k3xPp3gInSc4	on
frontman	frontman	k1gMnSc1	frontman
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
již	již	k6eAd1	již
hotové	hotový	k2eAgNnSc4d1	hotové
předal	předat	k5eAaPmAgMnS	předat
nahrávací	nahrávací	k2eAgMnSc1d1	nahrávací
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
ceně	cena	k1gFnSc3	cena
bála	bát	k5eAaImAgFnS	bát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příliš	příliš	k6eAd1	příliš
neprodělala	prodělat	k5eNaPmAgFnS	prodělat
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
měla	mít	k5eAaImAgFnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
nasvědčoval	nasvědčovat	k5eAaImAgInS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
objevila	objevit	k5eAaPmAgFnS	objevit
oficiálně	oficiálně	k6eAd1	oficiálně
první	první	k4xOgFnSc4	první
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Shackler	Shackler	k1gInSc1	Shackler
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Revenge	Revenge	k1gFnSc7	Revenge
<g/>
"	"	kIx"	"
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
videohře	videohra	k1gFnSc6	videohra
Rock	rock	k1gInSc1	rock
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nakonec	nakonec	k6eAd1	nakonec
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kytarista	kytarista	k1gMnSc1	kytarista
Robin	robin	k2eAgInSc1d1	robin
Finck	Finck	k1gInSc1	Finck
připojil	připojit	k5eAaPmAgInS	připojit
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
původní	původní	k2eAgFnSc3d1	původní
kapele	kapela	k1gFnSc3	kapela
Nine	Nine	k1gFnSc1	Nine
Inch	Inch	k1gMnSc1	Inch
Nails	Nails	k1gInSc1	Nails
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
prvního	první	k4xOgMnSc2	první
kytaristy	kytarista	k1gMnSc2	kytarista
přišel	přijít	k5eAaPmAgMnS	přijít
excentrický	excentrický	k2eAgMnSc1d1	excentrický
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
Darren	Darrna	k1gFnPc2	Darrna
Jay	Jay	k1gMnSc1	Jay
Ashba	Ashba	k1gMnSc1	Ashba
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
DJ	DJ	kA	DJ
Ashba	Ashba	k1gFnSc1	Ashba
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
fanoušci	fanoušek	k1gMnPc1	fanoušek
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
snaží	snažit	k5eAaImIp3nP	snažit
podobat	podobat	k5eAaImF	podobat
Slashovi	Slashův	k2eAgMnPc1d1	Slashův
nebo	nebo	k8xC	nebo
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
také	také	k9	také
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
často	často	k6eAd1	často
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
s	s	k7c7	s
cigaretou	cigareta	k1gFnSc7	cigareta
v	v	k7c6	v
puse	pusa	k1gFnSc6	pusa
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Slash	Slash	k1gInSc1	Slash
černý	černý	k2eAgInSc4d1	černý
cylindr	cylindr	k1gInSc4	cylindr
a	a	k8xC	a
například	například	k6eAd1	například
při	při	k7c6	při
úvodním	úvodní	k2eAgNnSc6d1	úvodní
sólu	sólo	k1gNnSc6	sólo
ve	v	k7c4	v
Sweet	Sweet	k1gInSc4	Sweet
Child	Child	k1gInSc1	Child
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Mine	minout	k5eAaImIp3nS	minout
drží	držet	k5eAaImIp3nS	držet
kytaru	kytara	k1gFnSc4	kytara
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývala	bývat	k5eAaImAgFnS	bývat
kdysi	kdysi	k6eAd1	kdysi
Slashova	Slashův	k2eAgFnSc1d1	Slashův
specialita	specialita	k1gFnSc1	specialita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrat	návrat	k1gInSc1	návrat
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
nemohlo	moct	k5eNaImAgNnS	moct
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
s	s	k7c7	s
rozbitím	rozbití	k1gNnSc7	rozbití
původní	původní	k2eAgFnSc2d1	původní
sestavy	sestava	k1gFnSc2	sestava
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
médii	médium	k1gNnPc7	médium
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
obrovský	obrovský	k2eAgInSc4d1	obrovský
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
bývalé	bývalý	k2eAgMnPc4d1	bývalý
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
s	s	k7c7	s
Axlem	Axl	k1gInSc7	Axl
spojit	spojit	k5eAaPmF	spojit
a	a	k8xC	a
obnovit	obnovit	k5eAaPmF	obnovit
původní	původní	k2eAgFnSc4d1	původní
slavnou	slavný	k2eAgFnSc4d1	slavná
sestavu	sestava	k1gFnSc4	sestava
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahrála	nahrát	k5eAaPmAgFnS	nahrát
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
velmi	velmi	k6eAd1	velmi
komplikoval	komplikovat	k5eAaBmAgMnS	komplikovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
Slashem	Slash	k1gInSc7	Slash
a	a	k8xC	a
Axlem	Axl	k1gInSc7	Axl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
však	však	k9	však
Slash	Slash	k1gInSc1	Slash
změnil	změnit	k5eAaPmAgInS	změnit
názor	názor	k1gInSc4	názor
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
neříkám	říkat	k5eNaImIp1nS	říkat
nikdy	nikdy	k6eAd1	nikdy
<g/>
...	...	k?	...
tvrdím	tvrdit	k5eAaImIp1nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
dobrý	dobrý	k2eAgInSc1d1	dobrý
nápad	nápad	k1gInSc1	nápad
zahrát	zahrát	k5eAaPmF	zahrát
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
sestavě	sestava	k1gFnSc6	sestava
pár	pár	k4xCyI	pár
show	show	k1gFnPc2	show
jenom	jenom	k9	jenom
tak	tak	k6eAd1	tak
<g/>
...	...	k?	...
pro	pro	k7c4	pro
legraci	legrace	k1gFnSc4	legrace
<g/>
"	"	kIx"	"
Nicméně	nicméně	k8xC	nicméně
Rose	Rose	k1gMnSc1	Rose
se	se	k3xPyFc4	se
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
o	o	k7c6	o
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
se	s	k7c7	s
Slashem	Slash	k1gInSc7	Slash
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
<g/>
"	"	kIx"	"
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
Slashe	Slashe	k1gFnSc4	Slashe
"	"	kIx"	"
<g/>
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připustil	připustit	k5eAaPmAgMnS	připustit
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nic	nic	k3yNnSc1	nic
nenamítal	namítat	k5eNaImAgMnS	namítat
proti	proti	k7c3	proti
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Izzy	Izz	k2eAgFnPc1d1	Izz
Stradlinem	Stradlin	k1gInSc7	Stradlin
nebo	nebo	k8xC	nebo
Duffem	Duff	k1gMnSc7	Duff
McKaganem	McKagan	k1gMnSc7	McKagan
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
<g/>
,	,	kIx,	,
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
a	a	k8xC	a
Matt	Matt	k2eAgInSc1d1	Matt
Sorum	Sorum	k1gInSc1	Sorum
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c4	v
Velvet	Velvet	k1gInSc4	Velvet
Revolveru	revolver	k1gInSc2	revolver
<g/>
,	,	kIx,	,
hostují	hostovat	k5eAaImIp3nP	hostovat
na	na	k7c4	na
show	show	k1gNnSc4	show
Stevenovy	Stevenův	k2eAgFnSc2d1	Stevenova
skupiny	skupina	k1gFnSc2	skupina
Adler	Adler	k1gMnSc1	Adler
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Appetite	Appetit	k1gInSc5	Appetit
a	a	k8xC	a
udržovali	udržovat	k5eAaImAgMnP	udržovat
přátelský	přátelský	k2eAgInSc4d1	přátelský
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Izzy	Izza	k1gFnPc1	Izza
Stradlin	Stradlina	k1gFnPc2	Stradlina
se	se	k3xPyFc4	se
udobřil	udobřit	k5eAaPmAgInS	udobřit
jak	jak	k6eAd1	jak
s	s	k7c7	s
Axlem	Axl	k1gInSc7	Axl
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	s	k7c7	s
Slashem	Slash	k1gInSc7	Slash
(	(	kIx(	(
<g/>
kterému	který	k3yQgMnSc3	který
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
sólové	sólový	k2eAgFnSc6d1	sólová
desce	deska	k1gFnSc6	deska
<g/>
)	)	kIx)	)
<g/>
.14	.14	k4	.14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
společně	společně	k6eAd1	společně
s	s	k7c7	s
GN	GN	kA	GN
<g/>
'	'	kIx"	'
<g/>
R	R	kA	R
v	v	k7c4	v
londýnské	londýnský	k2eAgMnPc4d1	londýnský
O2	O2	k1gMnPc4	O2
aréně	aréna	k1gFnSc6	aréna
4	[number]	k4	4
písně	píseň	k1gFnPc1	píseň
-	-	kIx~	-
"	"	kIx"	"
<g/>
Patience	Patience	k1gFnSc1	Patience
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Knocking	Knocking	k1gInSc1	Knocking
on	on	k3xPp3gMnSc1	on
Heavens	Heavens	k1gInSc4	Heavens
Door	Door	k1gInSc1	Door
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Could	Could	k1gMnSc1	Could
Be	Be	k1gMnSc1	Be
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nice	Nice	k1gFnSc1	Nice
Boys	boy	k1gMnPc3	boy
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Play	play	k0	play
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Roll	Roll	k1gInSc4	Roll
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
opět	opět	k6eAd1	opět
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
velké	velký	k2eAgFnPc4d1	velká
diskuze	diskuze	k1gFnPc4	diskuze
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
sjednocení	sjednocení	k1gNnSc6	sjednocení
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Axl	Axl	k?	Axl
Rose	Rose	k1gMnSc1	Rose
představil	představit	k5eAaPmAgMnS	představit
Duffa	Duff	k1gMnSc4	Duff
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
hotelu	hotel	k1gInSc6	hotel
a	a	k8xC	a
slyším	slyšet	k5eAaImIp1nS	slyšet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
někdo	někdo	k3yInSc1	někdo
pouští	pouštět	k5eAaImIp3nS	pouštět
nahlas	nahlas	k6eAd1	nahlas
muziku	muzika	k1gFnSc4	muzika
<g/>
...	...	k?	...
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
Duff	Duff	k1gInSc1	Duff
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Dalším	další	k2eAgNnSc7d1	další
vodítkem	vodítko	k1gNnSc7	vodítko
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
"	"	kIx"	"
<g/>
Guns	Guns	k1gInSc1	Guns
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
postupné	postupný	k2eAgInPc4d1	postupný
odchody	odchod	k1gInPc4	odchod
nových	nový	k2eAgMnPc2d1	nový
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
lead	lead	k1gMnSc1	lead
kytarista	kytarista	k1gMnSc1	kytarista
DJ	DJ	kA	DJ
Ashba	Ashba	k1gMnSc1	Ashba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
věnovat	věnovat	k5eAaPmF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
skupině	skupina	k1gFnSc3	skupina
Sixx	Sixx	k1gInSc1	Sixx
<g/>
:	:	kIx,	:
A.	A.	kA	A.
<g/>
M.	M.	kA	M.
<g/>
,	,	kIx,	,
ženě	žena	k1gFnSc6	žena
a	a	k8xC	a
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
souborem	soubor	k1gInSc7	soubor
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
již	již	k6eAd1	již
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
ani	ani	k8xC	ani
Ron	Ron	k1gMnSc1	Ron
Bumblefoot	Bumblefoot	k1gMnSc1	Bumblefoot
Thal	Thal	k1gMnSc1	Thal
<g/>
,	,	kIx,	,
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
již	již	k6eAd1	již
definitivně	definitivně	k6eAd1	definitivně
nerozpadá	rozpadat	k5eNaImIp3nS	rozpadat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nechystá	chystat	k5eNaImIp3nS	chystat
dlouho	dlouho	k6eAd1	dlouho
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
reunion	reunion	k1gInSc4	reunion
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
původními	původní	k2eAgMnPc7d1	původní
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
uznal	uznat	k5eAaPmAgInS	uznat
Slash	Slash	k1gInSc1	Slash
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
Axlem	Axlem	k1gInSc4	Axlem
zlepšily	zlepšit	k5eAaPmAgFnP	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
jeho	jeho	k3xOp3gInSc1	jeho
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Musím	muset	k5eAaImIp1nS	muset
být	být	k5eAaImF	být
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
říkám	říkat	k5eAaImIp1nS	říkat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
všichni	všechen	k3xTgMnPc1	všechen
stáli	stát	k5eAaImAgMnP	stát
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
by	by	kYmCp3nP	by
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
jít	jít	k5eAaImF	jít
ze	z	k7c2	z
správných	správný	k2eAgInPc2d1	správný
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
fanoušci	fanoušek	k1gMnPc1	fanoušek
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc4	ten
jistě	jistě	k9	jistě
ocenili	ocenit	k5eAaPmAgMnP	ocenit
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zábava	zábava	k1gFnSc1	zábava
to	ten	k3xDgNnSc4	ten
zkusit	zkusit	k5eAaPmF	zkusit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
komplexní	komplexní	k2eAgNnSc4d1	komplexní
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
záležitost	záležitost	k1gFnSc1	záležitost
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prozradil	prozradit	k5eAaPmAgMnS	prozradit
Slash	Slash	k1gMnSc1	Slash
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
reálné	reálný	k2eAgFnPc1d1	reálná
se	se	k3xPyFc4	se
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
staly	stát	k5eAaPmAgInP	stát
<g/>
,	,	kIx,	,
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
i	i	k9	i
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vysíláno	vysílat	k5eAaImNgNnS	vysílat
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
kinech	kino	k1gNnPc6	kino
při	při	k7c6	při
premiérách	premiéra	k1gFnPc6	premiéra
filmu	film	k1gInSc2	film
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
-	-	kIx~	-
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
tematický	tematický	k2eAgInSc4d1	tematický
projev	projev	k1gInSc4	projev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
křičící	křičící	k2eAgInSc4d1	křičící
dav	dav	k1gInSc4	dav
podpořen	podpořit	k5eAaPmNgInS	podpořit
velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgInSc7d1	známý
hlasem	hlas	k1gInSc7	hlas
křičícím	křičící	k2eAgInSc7d1	křičící
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zní	znět	k5eAaImIp3nP	znět
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Welcome	Welcom	k1gInSc5	Welcom
To	ten	k3xDgNnSc1	ten
The	The	k1gFnSc3	The
Jungle	Jungle	k1gFnSc2	Jungle
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Do	do	k7c2	do
you	you	k?	you
know	know	k?	know
where	wher	k1gInSc5	wher
you	you	k?	you
are	ar	k1gInSc5	ar
<g/>
?	?	kIx.	?
</s>
<s>
You	You	k?	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
in	in	k?	in
the	the	k?	the
jungle	jungle	k1gFnSc2	jungle
baby	baba	k1gFnSc2	baba
<g/>
!	!	kIx.	!
</s>
<s>
You	You	k?	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
gonna	gonna	k6eAd1	gonna
<g/>
'	'	kIx"	'
die	die	k?	die
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuštění	vypuštění	k1gNnSc6	vypuštění
těchto	tento	k3xDgFnPc2	tento
promo	promo	k6eAd1	promo
videí	video	k1gNnPc2	video
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
i	i	k8xC	i
profilový	profilový	k2eAgInSc1d1	profilový
obrázek	obrázek	k1gInSc1	obrázek
oficiálního	oficiální	k2eAgInSc2d1	oficiální
facebookového	facebookový	k2eAgInSc2d1	facebookový
účtu	účet	k1gInSc2	účet
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
logo	logo	k1gNnSc1	logo
kapely	kapela	k1gFnSc2	kapela
z	z	k7c2	z
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
Slash	Slash	k1gMnSc1	Slash
i	i	k9	i
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
oficiálně	oficiálně	k6eAd1	oficiálně
přiznali	přiznat	k5eAaPmAgMnP	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
členy	člen	k1gMnPc4	člen
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
Izzy	Izza	k1gFnSc2	Izza
Stradlin	Stradlin	k2eAgMnSc1d1	Stradlin
a	a	k8xC	a
Steven	Steven	k2eAgMnSc1d1	Steven
Adler	Adler	k1gMnSc1	Adler
se	se	k3xPyFc4	se
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
prvního	první	k4xOgInSc2	první
dubna	duben	k1gInSc2	duben
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahraje	zahrát	k5eAaPmIp3nS	zahrát
večer	večer	k6eAd1	večer
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeleském	Angeleský	k2eAgInSc6d1	Angeleský
klubu	klub	k1gInSc6	klub
Troubadour	Troubadoura	k1gFnPc2	Troubadoura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kdysi	kdysi	k6eAd1	kdysi
odehrála	odehrát	k5eAaPmAgFnS	odehrát
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
show	show	k1gFnSc6	show
byly	být	k5eAaImAgInP	být
kromě	kromě	k7c2	kromě
skalních	skalní	k2eAgFnPc2d1	skalní
fandů	fandů	k?	fandů
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
šťastlivců	šťastlivec	k1gMnPc2	šťastlivec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
informaci	informace	k1gFnSc3	informace
brzo	brzo	k6eAd1	brzo
<g/>
,	,	kIx,	,
přítomny	přítomen	k2eAgInPc4d1	přítomen
také	také	k9	také
slavné	slavný	k2eAgFnSc2d1	slavná
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
muzikanti	muzikant	k1gMnPc1	muzikant
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
Troubadour	Troubadour	k1gMnSc1	Troubadour
(	(	kIx(	(
<g/>
v	v	k7c6	v
mezičase	mezičas	k1gInSc6	mezičas
si	se	k3xPyFc3	se
Axl	Axl	k1gMnSc1	Axl
zlomil	zlomit	k5eAaPmAgMnS	zlomit
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
všechny	všechen	k3xTgInPc4	všechen
koncerty	koncert	k1gInPc4	koncert
do	do	k7c2	do
června	červen	k1gInSc2	červen
odzpíval	odzpívat	k5eAaPmAgInS	odzpívat
v	v	k7c6	v
sedě	sedě	k6eAd1	sedě
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pak	pak	k6eAd1	pak
na	na	k7c6	na
známém	známý	k2eAgInSc6d1	známý
"	"	kIx"	"
<g/>
rockovém	rockový	k2eAgInSc6d1	rockový
trůně	trůn	k1gInSc6	trůn
<g/>
"	"	kIx"	"
kytaristy	kytarista	k1gMnSc2	kytarista
a	a	k8xC	a
frontmana	frontman	k1gMnSc2	frontman
Foo	Foo	k1gFnSc2	Foo
Fighters	Fightersa	k1gFnPc2	Fightersa
Davea	Davea	k1gMnSc1	Davea
Grohla	Grohla	k1gMnSc1	Grohla
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozjelo	rozjet	k5eAaPmAgNnS	rozjet
třiadvacet	třiadvacet	k4xCc4	třiadvacet
let	léto	k1gNnPc2	léto
očekávané	očekávaný	k2eAgNnSc4d1	očekávané
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
kapela	kapela	k1gFnSc1	kapela
nazvala	nazvat	k5eAaPmAgFnS	nazvat
symbolicky	symbolicky	k6eAd1	symbolicky
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
In	In	k1gMnPc2	In
This	Thisa	k1gFnPc2	Thisa
Lifetime	Lifetim	k1gInSc5	Lifetim
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ne	ne	k9	ne
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
životě	život	k1gInSc6	život
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahrála	zahrát	k5eAaPmAgFnS	zahrát
například	například	k6eAd1	například
již	již	k6eAd1	již
na	na	k7c6	na
předem	předem	k6eAd1	předem
ohlášeném	ohlášený	k2eAgInSc6d1	ohlášený
festivalu	festival	k1gInSc6	festival
Coachella	Coachello	k1gNnSc2	Coachello
<g/>
,	,	kIx,	,
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc4	Vegas
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
destinacích	destinace	k1gFnPc6	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
navíc	navíc	k6eAd1	navíc
začala	začít	k5eAaPmAgFnS	začít
znovu	znovu	k6eAd1	znovu
hrát	hrát	k5eAaImF	hrát
svá	svůj	k3xOyFgNnPc4	svůj
legendární	legendární	k2eAgNnPc4d1	legendární
intra	intro	k1gNnPc4	intro
jako	jako	k8xC	jako
Only	Only	k1gInPc4	Only
Women	Women	k2eAgInSc4d1	Women
Bleed	Bleed	k1gInSc4	Bleed
od	od	k7c2	od
Alice	Alice	k1gFnSc2	Alice
Coopera	Cooper	k1gMnSc2	Cooper
k	k	k7c3	k
Knocking	Knocking	k1gInSc1	Knocking
On	on	k3xPp3gMnSc1	on
Heavens	Heavens	k1gInSc4	Heavens
Door	Door	k1gMnSc1	Door
či	či	k8xC	či
Speak	Speak	k1gMnSc1	Speak
Softly	Softly	k1gMnSc1	Softly
Love	lov	k1gInSc5	lov
před	před	k7c7	před
Sweet	Sweet	k1gMnSc1	Sweet
Child	Child	k1gMnSc1	Child
O	O	kA	O
Mine	minout	k5eAaImIp3nS	minout
či	či	k8xC	či
Layla	Layla	k1gFnSc1	Layla
před	před	k7c4	před
November	November	k1gInSc4	November
Rain	Raina	k1gFnPc2	Raina
<g/>
.	.	kIx.	.
</s>
<s>
Axl	Axl	k?	Axl
Rose	Rose	k1gMnSc1	Rose
byl	být	k5eAaImAgMnS	být
dle	dle	k7c2	dle
fanoušků	fanoušek	k1gMnPc2	fanoušek
i	i	k8xC	i
kritiků	kritik	k1gMnPc2	kritik
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
hlasové	hlasový	k2eAgFnSc6d1	hlasová
formě	forma	k1gFnSc6	forma
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastoupí	zastoupit	k5eAaPmIp3nS	zastoupit
zdravotně	zdravotně	k6eAd1	zdravotně
indisponovaného	indisponovaný	k2eAgMnSc2d1	indisponovaný
zpěváka	zpěvák	k1gMnSc2	zpěvák
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
Briana	Brian	k1gMnSc2	Brian
Johnsona	Johnson	k1gMnSc2	Johnson
(	(	kIx(	(
<g/>
Johnson	Johnson	k1gMnSc1	Johnson
začal	začít	k5eAaPmAgMnS	začít
přicházet	přicházet	k5eAaImF	přicházet
o	o	k7c4	o
sluch	sluch	k1gInSc4	sluch
<g/>
)	)	kIx)	)
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
Or	Or	k1gFnSc2	Or
Bust	busta	k1gFnPc2	busta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
větší	veliký	k2eAgFnSc4d2	veliký
vlnu	vlna	k1gFnSc4	vlna
nevole	nevole	k1gFnSc2	nevole
od	od	k7c2	od
skalních	skalní	k2eAgMnPc2d1	skalní
fanoušků	fanoušek	k1gMnPc2	fanoušek
bandy	banda	k1gFnSc2	banda
kolem	kolem	k7c2	kolem
kytaristy	kytarista	k1gMnSc2	kytarista
Anguse	Anguse	k1gFnSc1	Anguse
Younga	Younga	k1gFnSc1	Younga
(	(	kIx(	(
<g/>
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
tour	tour	k1gMnSc1	tour
známéjako	známéjako	k6eAd1	známéjako
Axl	Axl	k1gFnSc2	Axl
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
pochybujících	pochybující	k2eAgFnPc2d1	pochybující
spokojena	spokojen	k2eAgFnSc1d1	spokojena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
Axl	Axl	k1gMnSc1	Axl
byl	být	k5eAaImAgMnS	být
skutečně	skutečně	k6eAd1	skutečně
ve	v	k7c6	v
skvělé	skvělý	k2eAgFnSc6d1	skvělá
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
částečné	částečný	k2eAgNnSc4d1	částečné
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
je	být	k5eAaImIp3nS	být
Rose	Rosa	k1gFnSc3	Rosa
stále	stále	k6eAd1	stále
frontmanem	frontman	k1gInSc7	frontman
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
a	a	k8xC	a
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
slíbených	slíbený	k2eAgInPc2d1	slíbený
závazků	závazek	k1gInPc2	závazek
k	k	k7c3	k
Angusi	Anguse	k1gFnSc3	Anguse
Youngovi	Young	k1gMnSc6	Young
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
připojí	připojit	k5eAaPmIp3nS	připojit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
domovské	domovský	k2eAgFnSc3d1	domovská
kapele	kapela	k1gFnSc3	kapela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Chris	Chris	k1gFnSc4	Chris
Cornell	Cornell	k1gMnSc1	Cornell
ze	z	k7c2	z
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
Faith	Faith	k1gInSc1	Faith
No	no	k9	no
More	mor	k1gInSc5	mor
předkapelou	předkapela	k1gFnSc7	předkapela
Guns	Guns	k1gInSc1	Guns
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
během	během	k7c2	během
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
Tour	Toura	k1gFnPc2	Toura
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
na	na	k7c6	na
Strahovském	strahovský	k2eAgInSc6d1	strahovský
stadionu	stadion	k1gInSc6	stadion
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Cornell	Cornell	k1gInSc1	Cornell
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
nahrál	nahrát	k5eAaPmAgMnS	nahrát
píseň	píseň	k1gFnSc4	píseň
Promise	Promise	k1gFnSc2	Promise
pro	pro	k7c4	pro
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Slash	Slasha	k1gFnPc2	Slasha
právě	právě	k9	právě
kytaristy	kytarista	k1gMnPc7	kytarista
Slashe	Slashe	k1gFnPc2	Slashe
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
celou	celý	k2eAgFnSc7d1	celá
smutnou	smutný	k2eAgFnSc7d1	smutná
událostí	událost	k1gFnSc7	událost
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
hrát	hrát	k5eAaImF	hrát
píseň	píseň	k1gFnSc1	píseň
Black	Black	k1gInSc4	Black
Hole	hole	k1gFnSc2	hole
Sun	Sun	kA	Sun
z	z	k7c2	z
alba	album	k1gNnSc2	album
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
Superunknown	Superunknowna	k1gFnPc2	Superunknowna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
Letišti	letiště	k1gNnSc6	letiště
Letňany	Letňan	k1gMnPc4	Letňan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zazněla	zaznět	k5eAaImAgFnS	zaznět
opět	opět	k6eAd1	opět
Black	Black	k1gInSc4	Black
Hole	hole	k1gFnSc2	hole
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
zemřela	zemřít	k5eAaPmAgFnS	zemřít
další	další	k2eAgFnSc1d1	další
hvězda	hvězda	k1gFnSc1	hvězda
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
inspirací	inspirace	k1gFnSc7	inspirace
kapela	kapela	k1gFnSc1	kapela
-	-	kIx~	-
Glen	Glen	k1gMnSc1	Glen
Campbell	Campbell	k1gMnSc1	Campbell
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
Cornella	Cornello	k1gNnSc2	Cornello
začala	začít	k5eAaPmAgFnS	začít
uctívat	uctívat	k5eAaImF	uctívat
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
jeho	jeho	k3xOp3gFnSc4	jeho
píseň	píseň	k1gFnSc4	píseň
Wichita	Wichita	k1gMnSc1	Wichita
Lineman	Lineman	k1gMnSc1	Lineman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
řijna	řijn	k1gInSc2	řijn
2017	[number]	k4	2017
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
<g/>
Axl	Axl	k1gFnSc1	Axl
ve	v	k7c6	v
střešním	střešní	k2eAgInSc6d1	střešní
baru	bar	k1gInSc6	bar
<g/>
,	,	kIx,	,
obklopen	obklopen	k2eAgInSc1d1	obklopen
ženami	žena	k1gFnPc7	žena
prý	prý	k9	prý
říkal	říkat	k5eAaImAgMnS	říkat
přátelům	přítel	k1gMnPc3	přítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
Slashem	Slash	k1gInSc7	Slash
vycházejí	vycházet	k5eAaImIp3nP	vycházet
skvěle	skvěle	k6eAd1	skvěle
a	a	k8xC	a
baví	bavit	k5eAaImIp3nS	bavit
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
hraním	hranit	k5eAaImIp1nS	hranit
svých	svůj	k3xOyFgMnPc2	svůj
starých	starý	k2eAgMnPc2d1	starý
hitů	hit	k1gInPc2	hit
ale	ale	k8xC	ale
i	i	k9	i
skládáním	skládání	k1gNnSc7	skládání
nějakých	nějaký	k3yIgFnPc2	nějaký
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
prý	prý	k9	prý
oba	dva	k4xCgMnPc1	dva
dostali	dostat	k5eAaPmAgMnP	dostat
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
tvrdohlavost	tvrdohlavost	k1gFnSc4	tvrdohlavost
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
spolu	spolu	k6eAd1	spolu
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
dalo	dát	k5eAaPmAgNnS	dát
zase	zase	k9	zase
do	do	k7c2	do
pohody	pohoda	k1gFnSc2	pohoda
a	a	k8xC	a
reunion	reunion	k1gInSc1	reunion
už	už	k6eAd1	už
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
snadný	snadný	k2eAgInSc1d1	snadný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc1	odkaz
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
změnily	změnit	k5eAaPmAgFnP	změnit
rockovou	rockový	k2eAgFnSc4d1	rocková
a	a	k8xC	a
heavy	heava	k1gFnSc2	heava
metalovou	metalový	k2eAgFnSc4d1	metalová
hudbu	hudba	k1gFnSc4	hudba
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
vysoce	vysoce	k6eAd1	vysoce
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Ozzy	Ozza	k1gFnPc1	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
a	a	k8xC	a
Joe	Joe	k1gMnPc1	Joe
Perry	Perra	k1gFnSc2	Perra
nazvali	nazvat	k5eAaPmAgMnP	nazvat
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
novými	nový	k2eAgMnPc7d1	nový
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
sestaveném	sestavený	k2eAgInSc6d1	sestavený
časopisem	časopis	k1gInSc7	časopis
Q	Q	kA	Q
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
50	[number]	k4	50
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
člověk	člověk	k1gMnSc1	člověk
musí	muset	k5eAaImIp3nS	muset
slyšet	slyšet	k5eAaImF	slyšet
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
VH1	VH1	k1gFnSc1	VH1
ohodnotila	ohodnotit	k5eAaPmAgFnS	ohodnotit
skupinu	skupina	k1gFnSc4	skupina
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
jako	jako	k8xS	jako
devátou	devátý	k4xOgFnSc4	devátý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hard	hard	k6eAd1	hard
rockovou	rockový	k2eAgFnSc4d1	rocková
skupinu	skupina	k1gFnSc4	skupina
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
vyhlášeném	vyhlášený	k2eAgInSc6d1	vyhlášený
časopisem	časopis	k1gInSc7	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
ušetřena	ušetřit	k5eAaPmNgFnS	ušetřit
kritiky	kritik	k1gMnPc7	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
vysoce	vysoce	k6eAd1	vysoce
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
alba	album	k1gNnPc4	album
Use	usus	k1gInSc5	usus
You	You	k1gFnPc3	You
Illusion	Illusion	k1gInSc1	Illusion
za	za	k7c4	za
velkou	velký	k2eAgFnSc4d1	velká
pompéznost	pompéznost	k1gFnSc4	pompéznost
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
klišé	klišé	k1gNnSc1	klišé
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
jejich	jejich	k3xOp3gNnSc2	jejich
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
Slashe	Slashe	k1gFnSc1	Slashe
a	a	k8xC	a
McKagana	McKagana	k1gFnSc1	McKagana
<g/>
,	,	kIx,	,
a	a	k8xC	a
slabost	slabost	k1gFnSc1	slabost
Axla	Axlus	k1gMnSc2	Axlus
Rose	Ros	k1gMnSc2	Ros
pro	pro	k7c4	pro
trička	tričko	k1gNnPc4	tričko
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Mansonem	Manson	k1gMnSc7	Manson
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
využívány	využívat	k5eAaPmNgInP	využívat
médii	médium	k1gNnPc7	médium
k	k	k7c3	k
prezentování	prezentování	k1gNnSc3	prezentování
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
špatného	špatný	k2eAgInSc2d1	špatný
vzoru	vzor	k1gInSc2	vzor
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
mladé	mladý	k2eAgMnPc4d1	mladý
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
kritika	kritika	k1gFnSc1	kritika
za	za	k7c4	za
délku	délka	k1gFnSc4	délka
nahrávání	nahrávání	k1gNnSc1	nahrávání
alba	album	k1gNnSc2	album
Chinese	Chinese	k1gFnSc2	Chinese
Democracy	Democracy	k1gInPc4	Democracy
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nP	blížit
deseti	deset	k4xCc2	deset
letům	léto	k1gNnPc3	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
je	být	k5eAaImIp3nS	být
také	také	k9	také
zdrojem	zdroj	k1gInSc7	zdroj
kontroverze	kontroverze	k1gFnSc2	kontroverze
a	a	k8xC	a
od	od	k7c2	od
odchodu	odchod	k1gInSc2	odchod
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
byl	být	k5eAaImAgInS	být
také	také	k9	také
častěji	často	k6eAd2	často
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
neustálé	neustálý	k2eAgNnSc1d1	neustálé
vyhýbání	vyhýbání	k1gNnSc1	vyhýbání
se	se	k3xPyFc4	se
novinářům	novinář	k1gMnPc3	novinář
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
tiskové	tiskový	k2eAgFnPc4d1	tisková
konference	konference	k1gFnPc4	konference
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
mnohým	mnohý	k2eAgFnPc3d1	mnohá
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
duševním	duševní	k2eAgNnSc6d1	duševní
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgMnPc1d1	hudební
kritici	kritik	k1gMnPc1	kritik
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
fanoušci	fanoušek	k1gMnPc1	fanoušek
jej	on	k3xPp3gInSc4	on
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
z	z	k7c2	z
rozbití	rozbití	k1gNnSc2	rozbití
původní	původní	k2eAgFnSc2d1	původní
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
jeho	jeho	k3xOp3gInSc4	jeho
perfekcionismus	perfekcionismus	k1gInSc4	perfekcionismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
a	a	k8xC	a
k	k	k7c3	k
časovým	časový	k2eAgFnPc3d1	časová
prodlevám	prodleva	k1gFnPc3	prodleva
mezi	mezi	k7c7	mezi
alby	album	k1gNnPc7	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Axl	Axl	k?	Axl
Rose	Rose	k1gMnSc1	Rose
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slash	Slash	k1gInSc1	Slash
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dizzy	Dizz	k1gInPc1	Dizz
Reed	Reeda	k1gFnPc2	Reeda
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagan	k1gMnSc1	McKagan
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Fortus	Fortus	k1gMnSc1	Fortus
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Ferrer	Ferrer	k1gMnSc1	Ferrer
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Melissa	Melissa	k1gFnSc1	Melissa
Reese	Reese	k1gFnSc2	Reese
–	–	k?	–
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
programování	programování	k1gNnSc1	programování
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Ole	Ola	k1gFnSc3	Ola
Beich	Beich	k1gMnSc1	Beich
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rob	roba	k1gFnPc2	roba
Gardner	Gardnra	k1gFnPc2	Gardnra
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tracii	Tracie	k1gFnSc4	Tracie
Guns	Gunsa	k1gFnPc2	Gunsa
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Adler	Adler	k1gMnSc1	Adler
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Izzy	Izzy	k1gInPc1	Izzy
Stradlin	Stradlina	k1gFnPc2	Stradlina
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gilby	Gilb	k1gInPc1	Gilb
Clarke	Clarke	k1gNnSc2	Clarke
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slash	Slash	k1gInSc1	Slash
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Matt	Matt	k2eAgInSc1d1	Matt
Sorum	Sorum	k1gInSc1	Sorum
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tommy	Tomm	k1gInPc1	Tomm
Stinson	Stinsona	k1gFnPc2	Stinsona
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josh	Josh	k1gInSc1	Josh
Freese	Freese	k1gFnSc1	Freese
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Tobias	Tobias	k1gMnSc1	Tobias
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Buckethead	Buckethead	k1gInSc1	Buckethead
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bryan	Bryan	k1gInSc1	Bryan
Mantia	Mantia	k1gFnSc1	Mantia
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Robin	robin	k2eAgInSc1d1	robin
Finck	Finck	k1gInSc1	Finck
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bumblefoot	Bumblefoot	k1gInSc1	Bumblefoot
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DJ	DJ	kA	DJ
Ashba	Ashba	k1gFnSc1	Ashba
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chris	Chris	k1gFnSc1	Chris
Pitman	Pitman	k1gMnSc1	Pitman
-	-	kIx~	-
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
programování	programování	k1gNnSc1	programování
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
pro	pro	k7c4	pro
turné	turné	k1gNnSc4	turné
===	===	k?	===
</s>
</p>
<p>
<s>
Tracey	Tracea	k1gMnSc2	Tracea
Amos	Amos	k1gMnSc1	Amos
-	-	kIx~	-
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Teddy	Tedd	k1gInPc1	Tedd
Andreadis	Andreadis	k1gFnSc2	Andreadis
-	-	kIx~	-
harmonika	harmonik	k1gMnSc2	harmonik
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roberta	Robert	k1gMnSc4	Robert
Freeman	Freeman	k1gMnSc1	Freeman
-	-	kIx~	-
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diane	Dianout	k5eAaImIp3nS	Dianout
Jones	Jones	k1gInSc4	Jones
-	-	kIx~	-
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cece	cecat	k5eAaImIp3nS	cecat
Worrall	Worrall	k1gInSc4	Worrall
-	-	kIx~	-
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Náhradníci	náhradník	k1gMnPc1	náhradník
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
===	===	k?	===
</s>
</p>
<p>
<s>
Fred	Fred	k1gMnSc1	Fred
Coury	Coury	k?	Coury
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
-	-	kIx~	-
osm	osm	k4xCc4	osm
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Harris	Harris	k1gInSc1	Harris
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Henley	Henlea	k1gFnSc2	Henlea
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Izzy	Izzy	k1gInPc1	Izzy
Stradlin	Stradlina	k1gFnPc2	Stradlina
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
pět	pět	k4xCc4	pět
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Ferrer	Ferrer	k1gMnSc1	Ferrer
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
jedenadvacet	jedenadvacet	k1gNnSc3	jedenadvacet
vystoupení	vystoupení	k1gNnSc3	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myles	Myles	k1gMnSc1	Myles
Kennedy	Kenneda	k1gMnSc2	Kenneda
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
-	-	kIx~	-
uvedení	uvedení	k1gNnSc1	uvedení
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
N	N	kA	N
<g/>
́	́	k?	́
<g/>
Roll	Roll	k1gMnSc1	Roll
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gilby	Gilb	k1gInPc1	Gilb
Clarke	Clarke	k1gNnSc2	Clarke
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
-	-	kIx~	-
uvedení	uvedení	k1gNnSc1	uvedení
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
N	N	kA	N
<g/>
́	́	k?	́
<g/>
Roll	Roll	k1gMnSc1	Roll
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hosté	host	k1gMnPc1	host
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
===	===	k?	===
</s>
</p>
<p>
<s>
Vince	Vince	k?	Vince
Neil	Neil	k1gMnSc1	Neil
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Matt	Matt	k2eAgInSc4d1	Matt
McKagan	McKagan	k1gInSc4	McKagan
-	-	kIx~	-
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
-	-	kIx~	-
čtyři	čtyři	k4xCgNnPc4	čtyři
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Hoon	Hoon	k1gMnSc1	Hoon
-	-	kIx~	-
bongo	bongo	k1gNnSc1	bongo
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
devět	devět	k4xCc4	devět
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
-	-	kIx~	-
patnáct	patnáct	k4xCc4	patnáct
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lenny	Lenny	k?	Lenny
Kravitz	Kravitz	k1gInSc1	Kravitz
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgInSc1d1	Steven
Tyler	Tyler	k1gInSc1	Tyler
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Joe	Joe	k?	Joe
Perry	Perra	k1gFnPc1	Perra
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
dvě	dva	k4xCgNnPc4	dva
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
-	-	kIx~	-
piano	piano	k6eAd1	piano
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ron	Ron	k1gMnSc1	Ron
Wood	Wood	k1gMnSc1	Wood
-	-	kIx~	-
piano	piano	k6eAd1	piano
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
dvě	dva	k4xCgNnPc4	dva
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Doyle	Doyle	k1gNnSc2	Doyle
-	-	kIx~	-
bongo	bongo	k1gNnSc1	bongo
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
čtyři	čtyři	k4xCgNnPc4	čtyři
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Monroe	Monroe	k1gFnSc1	Monroe
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blake	Blake	k1gInSc1	Blake
Stanton	Stanton	k1gInSc1	Stanton
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Izzy	Izzy	k1gInPc1	Izzy
Stradlin	Stradlina	k1gFnPc2	Stradlina
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
sedmnáct	sedmnáct	k4xCc4	sedmnáct
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kid	Kid	k?	Kid
Rock	rock	k1gInSc1	rock
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gilby	Gilb	k1gInPc1	Gilb
Clarke	Clarke	k1gNnSc2	Clarke
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
-	-	kIx~	-
uvedení	uvedení	k1gNnSc1	uvedení
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
N	N	kA	N
<g/>
́	́	k?	́
<g/>
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
Of	Of	k1gMnSc2	Of
Fame	Fam	k1gMnSc2	Fam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Časový	časový	k2eAgInSc1d1	časový
přehled	přehled	k1gInSc1	přehled
===	===	k?	===
</s>
</p>
<p>
<s>
Počty	počet	k1gInPc1	počet
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
prodala	prodat	k5eAaPmAgFnS	prodat
kapela	kapela	k1gFnSc1	kapela
asi	asi	k9	asi
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
svých	svůj	k3xOyFgFnPc2	svůj
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
IRS	IRS	kA	IRS
nebyla	být	k5eNaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
demoverzi	demoverze	k1gFnSc4	demoverze
uniklou	uniklý	k2eAgFnSc4d1	uniklá
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgNnP	být
rádia	rádio	k1gNnPc1	rádio
donucena	donucen	k2eAgFnSc1d1	donucena
ji	on	k3xPp3gFnSc4	on
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Videoklipy	videoklip	k1gInPc4	videoklip
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Velvet	Velvet	k1gInSc1	Velvet
Revolver	revolver	k1gInSc1	revolver
</s>
</p>
<p>
<s>
Slash	Slash	k1gInSc1	Slash
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gFnSc7	Snakepit
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gInSc1	Roses
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
