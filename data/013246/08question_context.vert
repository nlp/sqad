<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
(	(	kIx(	(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
GN	GN	kA	GN
<g/>
'	'	kIx"	'
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
kapelu	kapela	k1gFnSc4	kapela
celosvětově	celosvětově	k6eAd1	celosvětově
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
album	album	k1gNnSc1	album
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1987	[number]	k4	1987
-	-	kIx~	-
1993	[number]	k4	1993
mnoho	mnoho	k4c1	mnoho
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
několikrát	několikrát	k6eAd1	několikrát
platinová	platinový	k2eAgNnPc4d1	platinové
alba	album	k1gNnPc4	album
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc1	Your
Illusion	Illusion	k1gInSc4	Illusion
I	i	k9	i
a	a	k8xC	a
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
II	II	kA	II
a	a	k8xC	a
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sestava	sestava	k1gFnSc1	sestava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nahrála	nahrát	k5eAaBmAgFnS	nahrát
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
postupně	postupně	k6eAd1	postupně
kvůli	kvůli	k7c3	kvůli
drogám	droga	k1gFnPc3	droga
a	a	k8xC	a
sporům	spor	k1gInPc3	spor
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Axlem	Axl	k1gMnSc7	Axl
Rosem	Ros	k1gMnSc7	Ros
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
Rose	Rose	k1gMnSc1	Rose
zůstal	zůstat	k5eAaPmAgMnS	zůstat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
jediným	jediný	k2eAgInSc7d1	jediný
jejím	její	k3xOp3gInSc7	její
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c4	po
zlepšení	zlepšení	k1gNnSc4	zlepšení
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
členy	člen	k1gMnPc7	člen
Axlem	Axl	k1gMnSc7	Axl
<g/>
,	,	kIx,	,
Duffem	Duff	k1gMnSc7	Duff
McKaganem	McKagan	k1gMnSc7	McKagan
<g/>
,	,	kIx,	,
Slashem	Slash	k1gInSc7	Slash
<g/>
,	,	kIx,	,
doplněnými	doplněná	k1gFnPc7	doplněná
o	o	k7c4	o
bubeníka	bubeník	k1gMnSc4	bubeník
Franka	Frank	k1gMnSc4	Frank
Ferrera	Ferrer	k1gMnSc4	Ferrer
<g/>
,	,	kIx,	,
klávesisty	klávesista	k1gMnPc4	klávesista
Melissu	Melissa	k1gFnSc4	Melissa
Reese	Rees	k1gMnSc2	Rees
a	a	k8xC	a
Dizzyho	Dizzy	k1gMnSc2	Dizzy
Reeda	Reed	k1gMnSc2	Reed
a	a	k8xC	a
o	o	k7c4	o
doprovodného	doprovodný	k2eAgMnSc4d1	doprovodný
kytaristu	kytarista	k1gMnSc4	kytarista
Richarda	Richard	k1gMnSc4	Richard
Fortuse	Fortuse	k1gFnSc1	Fortuse
<g/>
.	.	kIx.	.
</s>

