<s>
Za	za	k7c2	za
hlavního	hlavní	k2eAgMnSc2d1	hlavní
objevitele	objevitel	k1gMnSc2	objevitel
bájného	bájný	k2eAgNnSc2d1	bájné
města	město	k1gNnSc2	město
Trója	Trója	k1gFnSc1	Trója
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
německý	německý	k2eAgMnSc1d1	německý
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
archeolog	archeolog	k1gMnSc1	archeolog
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Tróji	Trója	k1gFnSc6	Trója
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
