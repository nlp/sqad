<s>
Trója	Trója	k1gFnSc1	Trója
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Troy	Tro	k2eAgInPc1d1	Tro
<g/>
,	,	kIx,	,
chetitsky	chetitsky	k6eAd1	chetitsky
Truwisa	Truwisa	k1gFnSc1	Truwisa
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Τ	Τ	k?	Τ
<g/>
,	,	kIx,	,
Ί	Ί	k?	Ί
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Trō	Trō	k1gMnSc1	Trō
nebo	nebo	k8xC	nebo
Ī	Ī	k?	Ī
<g/>
,	,	kIx,	,
turecky	turecky	k6eAd1	turecky
Truva	Truva	k1gFnSc1	Truva
nebo	nebo	k8xC	nebo
Troia	Troia	k1gFnSc1	Troia
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
jednak	jednak	k8xC	jednak
skutečné	skutečný	k2eAgNnSc1d1	skutečné
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
bájné	bájný	k2eAgNnSc1d1	bájné
město	město	k1gNnSc1	město
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
umístění	umístění	k1gNnSc1	umístění
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
zvaném	zvaný	k2eAgInSc6d1	zvaný
Troas	Troas	k1gInSc1	Troas
<g/>
,	,	kIx,	,
pahorek	pahorek	k1gInSc1	pahorek
Hisarlik	Hisarlika	k1gFnPc2	Hisarlika
<g/>
.	.	kIx.	.
</s>
<s>
Trója	Trója	k1gFnSc1	Trója
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
Ilion	Ilion	k1gInSc1	Ilion
(	(	kIx(	(
<g/>
u	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
Ilios	Iliosa	k1gFnPc2	Iliosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgNnSc1d1	legendární
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgNnSc4d1	známé
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
Homérově	Homérův	k2eAgFnSc3d1	Homérova
Iliadě	Iliada	k1gFnSc3	Iliada
<g/>
,	,	kIx,	,
hrdinskému	hrdinský	k2eAgInSc3d1	hrdinský
eposu	epos	k1gInSc3	epos
o	o	k7c6	o
téměř	téměř	k6eAd1	téměř
desetileté	desetiletý	k2eAgFnSc6d1	desetiletá
Trójské	trójský	k2eAgFnSc6d1	Trójská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
událostech	událost	k1gFnPc6	událost
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
líčeno	líčit	k5eAaImNgNnS	líčit
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
dílech	díl	k1gInPc6	díl
vzniklých	vzniklý	k2eAgInPc6d1	vzniklý
až	až	k8xS	až
po	po	k7c6	po
Homérovi	Homér	k1gMnSc6	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
však	však	k9	však
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k9	jen
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
a	a	k8xC	a
zlomky	zlomek	k1gInPc4	zlomek
těchto	tento	k3xDgInPc2	tento
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
polohu	poloha	k1gFnSc4	poloha
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
uspokojivě	uspokojivě	k6eAd1	uspokojivě
dokázat	dokázat	k5eAaPmF	dokázat
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rokem	rok	k1gInSc7	rok
1870	[number]	k4	1870
začal	začít	k5eAaPmAgInS	začít
totiž	totiž	k9	totiž
na	na	k7c4	na
Hissarliku	Hissarlika	k1gFnSc4	Hissarlika
první	první	k4xOgInPc4	první
archeologické	archeologický	k2eAgInPc4d1	archeologický
výkopy	výkop	k1gInPc4	výkop
hlavní	hlavní	k2eAgMnSc1d1	hlavní
objevitel	objevitel	k1gMnSc1	objevitel
Tróje	Trója	k1gFnSc2	Trója
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
a	a	k8xC	a
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
jim	on	k3xPp3gMnPc3	on
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Iliady	Iliada	k1gFnSc2	Iliada
založili	založit	k5eAaPmAgMnP	založit
Achájové	Acháj	k1gMnPc1	Acháj
tábor	tábor	k1gMnSc1	tábor
blízko	blízko	k7c2	blízko
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Skamandros	Skamandrosa	k1gFnPc2	Skamandrosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
ukotvili	ukotvit	k5eAaPmAgMnP	ukotvit
své	svůj	k3xOyFgFnPc4	svůj
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Trója	Trója	k1gFnSc1	Trója
samo	sám	k3xTgNnSc1	sám
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
krajinou	krajina	k1gFnSc7	krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Skamandros	Skamandrosa	k1gFnPc2	Skamandrosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
měly	mít	k5eAaImAgFnP	mít
probíhat	probíhat	k5eAaImF	probíhat
údajné	údajný	k2eAgFnPc1d1	údajná
bitvy	bitva	k1gFnPc1	bitva
o	o	k7c4	o
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
starověkého	starověký	k2eAgNnSc2d1	starověké
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
asi	asi	k9	asi
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
3000	[number]	k4	3000
lety	léto	k1gNnPc7	léto
bylo	být	k5eAaImAgNnS	být
ústí	ústí	k1gNnSc1	ústí
řeky	řeka	k1gFnSc2	řeka
Skamander	Skamandra	k1gFnPc2	Skamandra
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
tutéž	týž	k3xTgFnSc4	týž
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
městu	město	k1gNnSc3	město
blíže	blízce	k6eAd2	blízce
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
řeky	řeka	k1gFnSc2	řeka
totiž	totiž	k9	totiž
tvořilo	tvořit	k5eAaImAgNnS	tvořit
veliký	veliký	k2eAgInSc4d1	veliký
záliv	záliv	k1gInSc4	záliv
–	–	k?	–
přírodní	přírodní	k2eAgInSc4d1	přírodní
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starověku	starověk	k1gInSc2	starověk
naplnil	naplnit	k5eAaPmAgInS	naplnit
naplaveninami	naplavenina	k1gFnPc7	naplavenina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
jako	jako	k9	jako
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
posunulo	posunout	k5eAaPmAgNnS	posunout
<g/>
"	"	kIx"	"
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgInPc1d1	nedávný
geologické	geologický	k2eAgInPc1d1	geologický
objevy	objev	k1gInPc1	objev
umožnily	umožnit	k5eAaPmAgInP	umožnit
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
podobu	podoba	k1gFnSc4	podoba
původního	původní	k2eAgNnSc2d1	původní
trójského	trójský	k2eAgNnSc2d1	trójský
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
drobné	drobný	k2eAgFnPc4d1	drobná
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
tato	tento	k3xDgFnSc1	tento
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
polohu	poloha	k1gFnSc4	poloha
popsanou	popsaný	k2eAgFnSc4d1	popsaná
v	v	k7c6	v
homérské	homérský	k2eAgFnSc6d1	homérská
Tróji	Trója	k1gFnSc6	Trója
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2001	[number]	k4	2001
geologové	geolog	k1gMnPc1	geolog
John	John	k1gMnSc1	John
C.	C.	kA	C.
Kraft	Kraft	k1gMnSc1	Kraft
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Delaware	Delawar	k1gMnSc5	Delawar
a	a	k8xC	a
John	John	k1gMnSc1	John
V.	V.	kA	V.
Luce	Luc	k1gFnPc1	Luc
z	z	k7c2	z
Trinity	Trinita	k1gFnSc2	Trinita
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
výsledky	výsledek	k1gInPc4	výsledek
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
začaly	začít	k5eAaPmAgInP	začít
rokem	rok	k1gInSc7	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Porovnali	porovnat	k5eAaPmAgMnP	porovnat
dnešní	dnešní	k2eAgFnSc4d1	dnešní
geologii	geologie	k1gFnSc4	geologie
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
pobřežních	pobřežní	k2eAgInPc2d1	pobřežní
útvarů	útvar	k1gInPc2	útvar
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgFnP	popsat
v	v	k7c6	v
Iliadě	Iliada	k1gFnSc6	Iliada
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
klasických	klasický	k2eAgInPc6d1	klasický
zdrojích	zdroj	k1gInPc6	zdroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Strabo	Straba	k1gFnSc5	Straba
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Geographia	Geographium	k1gNnPc1	Geographium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
shoda	shoda	k1gFnSc1	shoda
mezi	mezi	k7c7	mezi
polohou	poloha	k1gFnSc7	poloha
Schliemannovy	Schliemannův	k2eAgFnSc2d1	Schliemannův
Tróje	Trója	k1gFnSc2	Trója
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
zmiňovaným	zmiňovaný	k2eAgInSc7d1	zmiňovaný
řeckým	řecký	k2eAgInSc7d1	řecký
táborem	tábor	k1gInSc7	tábor
<g/>
,	,	kIx,	,
geologickými	geologický	k2eAgNnPc7d1	geologické
svědectvími	svědectví	k1gNnPc7	svědectví
<g/>
,	,	kIx,	,
popisy	popis	k1gInPc1	popis
topografie	topografie	k1gFnSc2	topografie
a	a	k8xC	a
úvahách	úvaha	k1gFnPc6	úvaha
o	o	k7c6	o
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Iliadě	Iliada	k1gFnSc6	Iliada
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
práce	práce	k1gFnSc1	práce
Johna	John	k1gMnSc2	John
Krafta	Kraft	k1gMnSc2	Kraft
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
společníků	společník	k1gMnPc2	společník
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Iliady	Iliada	k1gFnSc2	Iliada
nalézáme	nalézat	k5eAaImIp1nP	nalézat
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
Tróji	Trója	k1gFnSc6	Trója
i	i	k8xC	i
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
důležitém	důležitý	k2eAgNnSc6d1	důležité
literárním	literární	k2eAgNnSc6d1	literární
díle	dílo	k1gNnSc6	dílo
přisuzovaném	přisuzovaný	k2eAgMnSc6d1	přisuzovaný
Homérovi	Homér	k1gMnSc6	Homér
–	–	k?	–
v	v	k7c6	v
Odyseji	odysea	k1gFnSc6	odysea
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
antické	antický	k2eAgFnSc6d1	antická
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Homérovu	Homérův	k2eAgFnSc4d1	Homérova
legendu	legenda	k1gFnSc4	legenda
o	o	k7c4	o
Tróje	Trója	k1gFnPc4	Trója
podrobně	podrobně	k6eAd1	podrobně
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
římský	římský	k2eAgMnSc1d1	římský
básník	básník	k1gMnSc1	básník
Publius	Publius	k1gMnSc1	Publius
Vergilius	Vergilius	k1gMnSc1	Vergilius
Maro	Maro	k1gMnSc1	Maro
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Aeneis	Aeneis	k1gFnSc1	Aeneis
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
přijali	přijmout	k5eAaPmAgMnP	přijmout
Trójskou	trójský	k2eAgFnSc4d1	Trójská
válku	válka	k1gFnSc4	válka
jako	jako	k8xS	jako
historický	historický	k2eAgInSc4d1	historický
fakt	fakt	k1gInSc4	fakt
a	a	k8xC	a
identitu	identita	k1gFnSc4	identita
homérské	homérský	k2eAgFnSc2d1	homérská
Tróje	Trója	k1gFnSc2	Trója
přisoudili	přisoudit	k5eAaPmAgMnP	přisoudit
Malé	Malé	k2eAgFnSc4d1	Malé
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Veliký	veliký	k2eAgInSc1d1	veliký
například	například	k6eAd1	například
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
roku	rok	k1gInSc2	rok
334	[number]	k4	334
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
oběti	oběť	k1gFnPc4	oběť
u	u	k7c2	u
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgFnP	spojovat
s	s	k7c7	s
homérskými	homérský	k2eAgMnPc7d1	homérský
hrdiny	hrdina	k1gMnPc7	hrdina
Achillem	Achilles	k1gMnSc7	Achilles
a	a	k8xC	a
Patroklem	Patrokl	k1gInSc7	Patrokl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
po	po	k7c6	po
nálezu	nález	k1gInSc6	nález
luvijské	luvijský	k2eAgFnSc2d1	luvijský
pečeti	pečeť	k1gFnSc2	pečeť
z	z	k7c2	z
období	období	k1gNnSc2	období
Tróje	Trója	k1gFnSc2	Trója
VII	VII	kA	VII
(	(	kIx(	(
<g/>
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
horce	horce	k6eAd1	horce
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
Homérově	Homérův	k2eAgFnSc6d1	Homérova
Tróji	Trója	k1gFnSc6	Trója
vlastně	vlastně	k9	vlastně
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Starke	Stark	k1gInSc2	Stark
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Tübingenu	Tübingen	k1gInSc6	Tübingen
nedávno	nedávno	k6eAd1	nedávno
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
Priam	Priam	k1gInSc1	Priam
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
luvijským	luvijský	k2eAgNnSc7d1	luvijský
slovem	slovo	k1gNnSc7	slovo
Priimuua	Priimuu	k1gInSc2	Priimuu
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
mimořádně	mimořádně	k6eAd1	mimořádně
odvážný	odvážný	k2eAgInSc1d1	odvážný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trója	Trója	k1gFnSc1	Trója
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
luvijsky	luvijsky	k6eAd1	luvijsky
mluvící	mluvící	k2eAgFnSc2d1	mluvící
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
luvijština	luvijština	k1gFnSc1	luvijština
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
pouhým	pouhý	k2eAgNnSc7d1	pouhé
nářečím	nářečí	k1gNnSc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
menšina	menšina	k1gFnSc1	menšina
současných	současný	k2eAgMnPc2d1	současný
spisovatelů	spisovatel	k1gMnPc2	spisovatel
namítá	namítat	k5eAaImIp3nS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Homérova	Homérův	k2eAgFnSc1d1	Homérova
Trója	Trója	k1gFnSc1	Trója
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinde	jinde	k6eAd1	jinde
–	–	k?	–
adepty	adept	k1gMnPc7	adept
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
Tróje	Trója	k1gFnSc2	Trója
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
či	či	k8xC	či
Skandinávie	Skandinávie	k1gFnSc1	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
učenců	učenec	k1gMnPc2	učenec
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
však	však	k9	však
zcela	zcela	k6eAd1	zcela
odmítá	odmítat	k5eAaImIp3nS	odmítat
a	a	k8xC	a
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
hlavního	hlavní	k2eAgMnSc2d1	hlavní
objevitele	objevitel	k1gMnSc2	objevitel
bájného	bájný	k2eAgNnSc2d1	bájné
města	město	k1gNnSc2	město
Trója	Trója	k1gFnSc1	Trója
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
německý	německý	k2eAgMnSc1d1	německý
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
archeolog	archeolog	k1gMnSc1	archeolog
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Tróji	Trója	k1gFnSc6	Trója
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
francouzského	francouzský	k2eAgMnSc2d1	francouzský
cestovatele	cestovatel	k1gMnSc2	cestovatel
Lechevaliera	Lechevalier	k1gMnSc2	Lechevalier
byly	být	k5eAaImAgFnP	být
vyvěrající	vyvěrající	k2eAgInSc4d1	vyvěrající
prameny	pramen	k1gInPc4	pramen
(	(	kIx(	(
<g/>
Skamandros	Skamandrosa	k1gFnPc2	Skamandrosa
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
přítok	přítok	k1gInSc4	přítok
Siomis	Siomis	k1gFnSc2	Siomis
<g/>
)	)	kIx)	)
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
pahorku	pahorek	k1gInSc2	pahorek
Hissarlik	Hissarlika	k1gFnPc2	Hissarlika
shodné	shodný	k2eAgNnSc1d1	shodné
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
Homér	Homér	k1gMnSc1	Homér
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Iliadě	Iliada	k1gFnSc6	Iliada
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nikdo	nikdo	k3yNnSc1	nikdo
nevyvrátil	vyvrátit	k5eNaPmAgMnS	vyvrátit
<g/>
,	,	kIx,	,
přivedlo	přivést	k5eAaPmAgNnS	přivést
Schliemanna	Schliemanen	k2eAgFnSc1d1	Schliemanen
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
Bunarbaši	Bunarbaše	k1gFnSc3	Bunarbaše
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnPc1d1	dnešní
Pinarbasi	Pinarbas	k1gMnPc1	Pinarbas
<g/>
)	)	kIx)	)
–	–	k?	–
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
tradovalo	tradovat	k5eAaImAgNnS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
právě	právě	k9	právě
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
legendární	legendární	k2eAgFnSc2d1	legendární
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
Schliemann	Schliemann	k1gMnSc1	Schliemann
nedůvěřivý	důvěřivý	k2eNgMnSc1d1	nedůvěřivý
<g/>
,	,	kIx,	,
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
prostudovávat	prostudovávat	k5eAaImF	prostudovávat
místní	místní	k2eAgFnSc4d1	místní
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Schliemann	Schliemann	k1gMnSc1	Schliemann
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
pouhý	pouhý	k2eAgMnSc1d1	pouhý
archeologický	archeologický	k2eAgMnSc1d1	archeologický
amatér	amatér	k1gMnSc1	amatér
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
když	když	k8xS	když
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
prvních	první	k4xOgFnPc6	první
vykopávkách	vykopávka	k1gFnPc6	vykopávka
nenalezl	nalézt	k5eNaBmAgMnS	nalézt
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
starověké	starověký	k2eAgFnSc2d1	starověká
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
nebál	bát	k5eNaImAgMnS	bát
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
domněnky	domněnka	k1gFnPc1	domněnka
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
Tróji	Trója	k1gFnSc6	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
intuice	intuice	k1gFnSc1	intuice
<g/>
,	,	kIx,	,
badatelské	badatelský	k2eAgNnSc1d1	badatelské
nadání	nadání	k1gNnSc1	nadání
a	a	k8xC	a
perfektní	perfektní	k2eAgFnSc1d1	perfektní
znalost	znalost	k1gFnSc1	znalost
téměř	téměř	k6eAd1	téměř
každého	každý	k3xTgNnSc2	každý
slova	slovo	k1gNnSc2	slovo
Iliady	Iliada	k1gFnSc2	Iliada
Schliemanna	Schliemanno	k1gNnSc2	Schliemanno
nakonec	nakonec	k6eAd1	nakonec
dovedly	dovést	k5eAaPmAgFnP	dovést
právě	právě	k9	právě
až	až	k6eAd1	až
na	na	k7c4	na
zmiňovaný	zmiňovaný	k2eAgInSc4d1	zmiňovaný
pahorek	pahorek	k1gInSc4	pahorek
jménem	jméno	k1gNnSc7	jméno
Hissarlik	Hissarlika	k1gFnPc2	Hissarlika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Schliemann	Schliemann	k1gMnSc1	Schliemann
byl	být	k5eAaImAgMnS	být
zcela	zcela	k6eAd1	zcela
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Iliada	Iliada	k1gFnSc1	Iliada
není	být	k5eNaImIp3nS	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
mýtem	mýtus	k1gInSc7	mýtus
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
fakty	fakt	k1gInPc1	fakt
popisující	popisující	k2eAgFnSc2d1	popisující
historické	historický	k2eAgFnSc2d1	historická
události	událost	k1gFnSc2	událost
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
udály	udát	k5eAaPmAgFnP	udát
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
pak	pak	k6eAd1	pak
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
sestavit	sestavit	k5eAaPmF	sestavit
chronologii	chronologie	k1gFnSc4	chronologie
popisované	popisovaný	k2eAgFnSc2d1	popisovaná
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Tróju	Trója	k1gFnSc4	Trója
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
následně	následně	k6eAd1	následně
po	po	k7c6	po
logických	logický	k2eAgFnPc6d1	logická
úvahách	úvaha	k1gFnPc6	úvaha
určit	určit	k5eAaPmF	určit
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
dějišť	dějiště	k1gNnPc2	dějiště
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schliemann	Schliemann	k1gMnSc1	Schliemann
postrádal	postrádat	k5eAaImAgMnS	postrádat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
vykopávkám	vykopávka	k1gFnPc3	vykopávka
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
své	svůj	k3xOyFgFnSc2	svůj
archeologické	archeologický	k2eAgFnSc2d1	archeologická
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
snažení	snažení	k1gNnSc1	snažení
se	se	k3xPyFc4	se
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
velice	velice	k6eAd1	velice
záhy	záhy	k6eAd1	záhy
začal	začít	k5eAaPmAgInS	začít
nalézat	nalézat	k5eAaImF	nalézat
trosky	troska	k1gFnPc4	troska
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
zbytky	zbytek	k1gInPc4	zbytek
zdí	zeď	k1gFnPc2	zeď
budov	budova	k1gFnPc2	budova
postavených	postavený	k2eAgInPc2d1	postavený
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
budov	budova	k1gFnPc2	budova
jiných	jiný	k2eAgMnPc2d1	jiný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejného	stejný	k2eAgInSc2d1	stejný
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
musel	muset	k5eAaImAgMnS	muset
Schliemann	Schliemann	k1gNnSc4	Schliemann
velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
vykopávky	vykopávka	k1gFnPc4	vykopávka
ukončit	ukončit	k5eAaPmF	ukončit
a	a	k8xC	a
navrátil	navrátit	k5eAaPmAgMnS	navrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1871	[number]	k4	1871
již	již	k6eAd1	již
s	s	k7c7	s
legální	legální	k2eAgFnSc7d1	legální
licencí	licence	k1gFnSc7	licence
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
navázal	navázat	k5eAaPmAgInS	navázat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Schliemann	Schliemann	k1gInSc1	Schliemann
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Homérova	Homérův	k2eAgFnSc1d1	Homérova
Trója	Trója	k1gFnSc1	Trója
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
zasazována	zasazovat	k5eAaImNgFnS	zasazovat
do	do	k7c2	do
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
logicky	logicky	k6eAd1	logicky
proto	proto	k8xC	proto
nejníže	nízce	k6eAd3	nízce
položenou	položený	k2eAgFnSc7d1	položená
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
královského	královský	k2eAgNnSc2d1	královské
sídla	sídlo	k1gNnSc2	sídlo
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
neobydlená	obydlený	k2eNgFnSc1d1	neobydlená
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zcela	zcela	k6eAd1	zcela
chybná	chybný	k2eAgFnSc1d1	chybná
domněnka	domněnka	k1gFnSc1	domněnka
později	pozdě	k6eAd2	pozdě
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
jeho	jeho	k3xOp3gFnPc4	jeho
vykopávky	vykopávka	k1gFnPc4	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úmorném	úmorný	k2eAgNnSc6d1	úmorné
hledání	hledání	k1gNnSc6	hledání
nalezl	nalézt	k5eAaBmAgInS	nalézt
studnu	studna	k1gFnSc4	studna
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
kameny	kámen	k1gInPc1	kámen
však	však	k9	však
byly	být	k5eAaImAgInP	být
pospojovány	pospojovat	k5eAaPmNgInP	pospojovat
maltou	malta	k1gFnSc7	malta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
případě	případ	k1gInSc6	případ
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
doby	doba	k1gFnSc2	doba
–	–	k?	–
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
nálezem	nález	k1gInSc7	nález
římských	římský	k2eAgFnPc2d1	římská
mincí	mince	k1gFnPc2	mince
s	s	k7c7	s
hlavami	hlava	k1gFnPc7	hlava
Minervy	Minerva	k1gFnSc2	Minerva
<g/>
,	,	kIx,	,
Faustiny	Faustina	k1gFnSc2	Faustina
<g/>
,	,	kIx,	,
Aurelia	Aurelius	k1gMnSc2	Aurelius
a	a	k8xC	a
Commoda	Commod	k1gMnSc2	Commod
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnSc1	naděje
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
neopouštěly	opouštět	k5eNaImAgFnP	opouštět
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
odhalovat	odhalovat	k5eAaImF	odhalovat
zdi	zeď	k1gFnSc3	zeď
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
už	už	k6eAd1	už
nespojovala	spojovat	k5eNaImAgFnS	spojovat
malta	malta	k1gFnSc1	malta
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
mramorové	mramorový	k2eAgFnPc4d1	mramorová
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odkazovala	odkazovat	k5eAaImAgFnS	odkazovat
na	na	k7c4	na
krále	král	k1gMnSc4	král
Pergamonu	pergamon	k1gInSc2	pergamon
a	a	k8xC	a
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Další	další	k2eAgFnPc4d1	další
nalezené	nalezený	k2eAgFnPc4d1	nalezená
desky	deska	k1gFnPc4	deska
obsahující	obsahující	k2eAgInPc1d1	obsahující
řecké	řecký	k2eAgInPc1d1	řecký
texty	text	k1gInPc1	text
byly	být	k5eAaImAgInP	být
však	však	k9	však
patrně	patrně	k6eAd1	patrně
mladší	mladý	k2eAgMnSc1d2	mladší
a	a	k8xC	a
s	s	k7c7	s
Trójou	Trója	k1gFnSc7	Trója
neměly	mít	k5eNaImAgFnP	mít
pranic	pranic	k3yNnSc1	pranic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Jaký	jaký	k3yIgInSc1	jaký
šok	šok	k1gInSc1	šok
následoval	následovat	k5eAaImAgInS	následovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1871	[number]	k4	1871
objevil	objevit	k5eAaPmAgMnS	objevit
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
výkopu	výkop	k1gInSc2	výkop
primitivní	primitivní	k2eAgInPc1d1	primitivní
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
kladiva	kladivo	k1gNnPc1	kladivo
<g/>
,	,	kIx,	,
sekery	sekera	k1gFnPc1	sekera
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
kamenná	kamenný	k2eAgNnPc1d1	kamenné
ostří	ostří	k1gNnPc1	ostří
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
významnější	významný	k2eAgFnPc1d2	významnější
stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnPc1d1	kamenná
následovaly	následovat	k5eAaImAgFnP	následovat
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Schliemanna	Schliemann	k1gMnSc4	Schliemann
velmi	velmi	k6eAd1	velmi
zmátlo	zmást	k5eAaPmAgNnS	zmást
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dle	dle	k7c2	dle
jeho	on	k3xPp3gNnSc2	on
mínění	mínění	k1gNnSc2	mínění
byl	být	k5eAaImAgInS	být
trójský	trójský	k2eAgInSc1d1	trójský
národ	národ	k1gInSc1	národ
velice	velice	k6eAd1	velice
vyspělý	vyspělý	k2eAgInSc1d1	vyspělý
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
citem	cit	k1gInSc7	cit
pro	pro	k7c4	pro
estetičnost	estetičnost	k1gFnSc4	estetičnost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
sám	sám	k3xTgMnSc1	sám
Homér	Homér	k1gMnSc1	Homér
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
nástrojích	nástroj	k1gInPc6	nástroj
bronzových	bronzový	k2eAgInPc6d1	bronzový
a	a	k8xC	a
železných	železný	k2eAgInPc6d1	železný
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
trójské	trójský	k2eAgFnSc6d1	Trójská
civilizaci	civilizace	k1gFnSc6	civilizace
logicky	logicky	k6eAd1	logicky
musely	muset	k5eAaImAgFnP	muset
ležet	ležet	k5eAaImF	ležet
v	v	k7c4	v
mladší	mladý	k2eAgFnSc4d2	mladší
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
výše	vysoce	k6eAd2	vysoce
položené	položený	k2eAgFnSc3d1	položená
vrstvě	vrstva	k1gFnSc3	vrstva
země	zem	k1gFnSc2	zem
než	než	k8xS	než
ostatky	ostatek	k1gInPc7	ostatek
po	po	k7c6	po
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přesto	přesto	k8xC	přesto
žádné	žádný	k3yNgInPc4	žádný
nezvratné	zvratný	k2eNgInPc4d1	nezvratný
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
Tróje	Trója	k1gFnSc2	Trója
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
nalezl	nalézt	k5eAaBmAgInS	nalézt
právě	právě	k9	právě
nástroje	nástroj	k1gInPc4	nástroj
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
,	,	kIx,	,
neobjevil	objevit	k5eNaPmAgMnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
byly	být	k5eAaImAgInP	být
celkově	celkově	k6eAd1	celkově
dost	dost	k6eAd1	dost
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
čtyř	čtyři	k4xCgInPc2	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
dvacet	dvacet	k4xCc4	dvacet
metrů	metr	k1gInPc2	metr
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
samé	samý	k3xTgFnSc6	samý
hloubce	hloubka	k1gFnSc6	hloubka
nalézal	nalézat	k5eAaImAgMnS	nalézat
Schliemann	Schliemann	k1gMnSc1	Schliemann
římské	římský	k2eAgFnSc2d1	římská
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hledal	hledat	k5eAaImAgMnS	hledat
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
lidech	člověk	k1gMnPc6	člověk
žijících	žijící	k2eAgInPc2d1	žijící
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
,	,	kIx,	,
nalézal	nalézat	k5eAaImAgMnS	nalézat
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
lidech	člověk	k1gMnPc6	člověk
žijících	žijící	k2eAgInPc2d1	žijící
1500	[number]	k4	1500
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yQnSc6	co
Schliemann	Schliemanno	k1gNnPc2	Schliemanno
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
úvahách	úvaha	k1gFnPc6	úvaha
chyboval	chybovat	k5eAaImAgMnS	chybovat
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hora	hora	k1gFnSc1	hora
Hissarlik	Hissarlika	k1gFnPc2	Hissarlika
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
na	na	k7c4	na
sobě	se	k3xPyFc3	se
pravidelně	pravidelně	k6eAd1	pravidelně
navrstvenými	navrstvený	k2eAgFnPc7d1	navrstvená
homogenními	homogenní	k2eAgFnPc7d1	homogenní
kulturními	kulturní	k2eAgFnPc7d1	kulturní
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
naprosto	naprosto	k6eAd1	naprosto
chybný	chybný	k2eAgInSc1d1	chybný
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kultur	kultura	k1gFnPc2	kultura
se	se	k3xPyFc4	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navršily	navršit	k5eAaPmAgInP	navršit
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
plošném	plošný	k2eAgInSc6d1	plošný
rozsahu	rozsah	k1gInSc6	rozsah
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
dobýval	dobývat	k5eAaImAgMnS	dobývat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
kopce	kopec	k1gInSc2	kopec
zvenku	zvenku	k6eAd1	zvenku
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
narážel	narážet	k5eAaPmAgInS	narážet
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
vrstvy	vrstva	k1gFnPc4	vrstva
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yQnSc6	co
se	se	k3xPyFc4	se
Schliemann	Schliemann	k1gInSc4	Schliemann
nemýlil	mýlit	k5eNaImAgInS	mýlit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
střed	střed	k1gInSc1	střed
pahorku	pahorek	k1gInSc2	pahorek
nebyl	být	k5eNaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
pahorku	pahorek	k1gInSc2	pahorek
položený	položený	k2eAgInSc4d1	položený
mimo	mimo	k7c4	mimo
střed	střed	k1gInSc4	střed
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1871	[number]	k4	1871
byl	být	k5eAaImAgInS	být
Schliemann	Schliemann	k1gInSc1	Schliemann
rozrušen	rozrušen	k2eAgInSc1d1	rozrušen
objevem	objev	k1gInSc7	objev
měděných	měděný	k2eAgFnPc2d1	měděná
jehel	jehla	k1gFnPc2	jehla
<g/>
,	,	kIx,	,
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
mečů	meč	k1gInPc2	meč
a	a	k8xC	a
válečných	válečný	k2eAgFnPc2d1	válečná
seker	sekera	k1gFnPc2	sekera
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
pod	pod	k7c4	pod
nálezy	nález	k1gInPc4	nález
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
hlouběji	hluboko	k6eAd2	hluboko
pak	pak	k6eAd1	pak
nalezl	nalézt	k5eAaBmAgInS	nalézt
čepele	čepel	k1gFnPc4	čepel
nožů	nůž	k1gInPc2	nůž
z	z	k7c2	z
vulkanického	vulkanický	k2eAgNnSc2d1	vulkanické
skla	sklo	k1gNnSc2	sklo
s	s	k7c7	s
dvojitým	dvojitý	k2eAgNnSc7d1	dvojité
ostřím	ostří	k1gNnSc7	ostří
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
čím	čí	k3xOyQgNnSc7	čí
hlouběji	hluboko	k6eAd2	hluboko
kopal	kopat	k5eAaImAgMnS	kopat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
rostla	růst	k5eAaImAgFnS	růst
kvalita	kvalita	k1gFnSc1	kvalita
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
objevoval	objevovat	k5eAaImAgMnS	objevovat
první	první	k4xOgFnPc4	první
hliněné	hliněný	k2eAgFnPc4d1	hliněná
nádoby	nádoba	k1gFnPc4	nádoba
velice	velice	k6eAd1	velice
elegantního	elegantní	k2eAgInSc2d1	elegantní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
bez	bez	k7c2	bez
okras	okrasa	k1gFnPc2	okrasa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
metr	metr	k1gInSc4	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hliněné	hliněný	k2eAgFnSc2d1	hliněná
urny	urna	k1gFnSc2	urna
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1871	[number]	k4	1871
se	se	k3xPyFc4	se
Schliemann	Schliemann	k1gInSc1	Schliemann
prokopal	prokopat	k5eAaPmAgInS	prokopat
až	až	k9	až
k	k	k7c3	k
mohutným	mohutný	k2eAgInPc3d1	mohutný
otesaným	otesaný	k2eAgInPc3d1	otesaný
kvádrům	kvádr	k1gInPc3	kvádr
nemálo	málo	k6eNd1	málo
podobným	podobný	k2eAgInPc3d1	podobný
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgMnPc4	jaký
viděl	vidět	k5eAaImAgInS	vidět
v	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
–	–	k?	–
snad	snad	k9	snad
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
doufal	doufat	k5eAaImAgInS	doufat
<g/>
,	,	kIx,	,
základy	základ	k1gInPc1	základ
trojského	trojský	k2eAgInSc2d1	trojský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
90	[number]	k4	90
%	%	kIx~	%
nálezů	nález	k1gInPc2	nález
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
předhistorické	předhistorický	k2eAgFnSc2d1	předhistorická
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nepodával	podávat	k5eNaImAgInS	podávat
svědectví	svědectví	k1gNnPc4	svědectví
o	o	k7c4	o
homérské	homérský	k2eAgFnPc4d1	homérská
Tróje	Trója	k1gFnPc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
nebylo	být	k5eNaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
trojské	trojský	k2eAgFnSc2d1	Trojská
existují	existovat	k5eAaImIp3nP	existovat
nějaké	nějaký	k3yIgInPc4	nějaký
nápisy	nápis	k1gInPc4	nápis
<g/>
,	,	kIx,	,
doufal	doufat	k5eAaImAgMnS	doufat
Schliemann	Schliemann	k1gMnSc1	Schliemann
<g/>
,	,	kIx,	,
že	že	k8xS	že
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
najde	najít	k5eAaPmIp3nS	najít
právě	právě	k9	právě
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
vysílený	vysílený	k2eAgMnSc1d1	vysílený
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zatím	zatím	k6eAd1	zatím
víceméně	víceméně	k9	víceméně
spokojený	spokojený	k2eAgMnSc1d1	spokojený
Schliemann	Schliemann	k1gMnSc1	Schliemann
opět	opět	k6eAd1	opět
ukončil	ukončit	k5eAaPmAgMnS	ukončit
své	svůj	k3xOyFgFnPc4	svůj
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
po	po	k7c6	po
pečlivých	pečlivý	k2eAgFnPc6d1	pečlivá
přípravách	příprava	k1gFnPc6	příprava
zase	zase	k9	zase
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Nalézal	nalézat	k5eAaImAgMnS	nalézat
jen	jen	k9	jen
mnoho	mnoho	k4c4	mnoho
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
ležících	ležící	k2eAgInPc2d1	ležící
bloků	blok	k1gInPc2	blok
lasturového	lasturový	k2eAgInSc2d1	lasturový
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
budily	budit	k5eAaImAgFnP	budit
dojem	dojem	k1gInSc4	dojem
zbytků	zbytek	k1gInPc2	zbytek
zdí	zeď	k1gFnPc2	zeď
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
jehlice	jehlice	k1gFnPc4	jehlice
do	do	k7c2	do
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
rozbité	rozbitý	k2eAgFnSc2d1	rozbitá
pohřební	pohřební	k2eAgFnSc2d1	pohřební
urny	urna	k1gFnSc2	urna
a	a	k8xC	a
nádoby	nádoba	k1gFnSc2	nádoba
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
čepele	čepel	k1gFnPc4	čepel
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
oštěpy	oštěp	k1gInPc4	oštěp
a	a	k8xC	a
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
drobné	drobný	k2eAgInPc4d1	drobný
předměty	předmět	k1gInPc4	předmět
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnohých	mnohý	k2eAgInPc6d1	mnohý
blocích	blok	k1gInPc6	blok
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
soví	soví	k2eAgInPc4d1	soví
obličeje	obličej	k1gInPc4	obličej
s	s	k7c7	s
lidskými	lidský	k2eAgMnPc7d1	lidský
rysy	rys	k1gMnPc7	rys
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
představují	představovat	k5eAaImIp3nP	představovat
jednu	jeden	k4xCgFnSc4	jeden
jedinou	jediný	k2eAgFnSc4d1	jediná
bohyni	bohyně	k1gFnSc4	bohyně
–	–	k?	–
ilijskou	ilijský	k2eAgFnSc4d1	ilijský
Minervu	Minerva	k1gFnSc4	Minerva
(	(	kIx(	(
<g/>
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Homéra	Homér	k1gMnSc2	Homér
bohyně	bohyně	k1gFnSc1	bohyně
Athéna	Athéna	k1gFnSc1	Athéna
se	s	k7c7	s
soví	soví	k2eAgFnSc7d1	soví
tváří	tvář	k1gFnSc7	tvář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
odkrývání	odkrývání	k1gNnSc6	odkrývání
Tróje	Trója	k1gFnSc2	Trója
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
nádoby	nádoba	k1gFnPc1	nádoba
známé	známý	k2eAgFnPc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
pithos	pithos	k1gInSc4	pithos
–	–	k?	–
nádoby	nádoba	k1gFnSc2	nádoba
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
určené	určený	k2eAgFnSc2d1	určená
ke	k	k7c3	k
skladování	skladování	k1gNnSc3	skladování
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
cenné	cenný	k2eAgInPc1d1	cenný
nálezy	nález	k1gInPc1	nález
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
každým	každý	k3xTgNnSc7	každý
dnem	dno	k1gNnSc7	dno
a	a	k8xC	a
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
žádný	žádný	k3yNgInSc1	žádný
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
homérské	homérský	k2eAgFnSc2d1	homérská
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Schliemann	Schliemann	k1gInSc1	Schliemann
začal	začít	k5eAaPmAgInS	začít
pochybovat	pochybovat	k5eAaImF	pochybovat
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Trojané	Trojan	k1gMnPc1	Trojan
znali	znát	k5eAaImAgMnP	znát
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
nálezů	nález	k1gInPc2	nález
byla	být	k5eAaImAgFnS	být
zdobena	zdoben	k2eAgFnSc1d1	zdobena
jen	jen	k6eAd1	jen
symboly	symbol	k1gInPc7	symbol
–	–	k?	–
nejčastěji	často	k6eAd3	často
hákovým	hákový	k2eAgInSc7d1	hákový
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byl	být	k5eAaImAgMnS	být
objeven	objeven	k2eAgInSc4d1	objeven
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
skoro	skoro	k6eAd1	skoro
devadesát	devadesát	k4xCc4	devadesát
centimetrů	centimetr	k1gInPc2	centimetr
široký	široký	k2eAgInSc4d1	široký
blok	blok	k1gInSc4	blok
párského	párský	k2eAgInSc2d1	párský
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgMnS	být
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
bůh	bůh	k1gMnSc1	bůh
Helios	Helios	k1gMnSc1	Helios
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vesmírnými	vesmírný	k2eAgMnPc7d1	vesmírný
pádícími	pádící	k2eAgMnPc7d1	pádící
oři	oř	k1gMnPc7	oř
(	(	kIx(	(
<g/>
Heliova	Heliův	k2eAgFnSc1d1	Heliova
metopa	metopa	k1gFnSc1	metopa
dnes	dnes	k6eAd1	dnes
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
berlínském	berlínský	k2eAgNnSc6d1	berlínské
Pergamonském	Pergamonský	k2eAgNnSc6d1	Pergamonské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
způsobil	způsobit	k5eAaPmAgMnS	způsobit
tolik	tolik	k6eAd1	tolik
rozruchu	rozruch	k1gInSc2	rozruch
jako	jako	k8xS	jako
zmatku	zmatek	k1gInSc2	zmatek
–	–	k?	–
Homér	Homér	k1gMnSc1	Homér
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
opět	opět	k6eAd1	opět
nezmiňoval	zmiňovat	k5eNaImAgMnS	zmiňovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
eposu	epos	k1gInSc6	epos
o	o	k7c6	o
ničem	nic	k3yNnSc6	nic
souvisejícím	související	k2eAgNnSc6d1	související
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Heliem	helium	k1gNnSc7	helium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
úvahách	úvaha	k1gFnPc6	úvaha
však	však	k9	však
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
nález	nález	k1gInSc1	nález
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
ilijské	ilijský	k2eAgFnSc2d1	ilijský
Minervy	Minerva	k1gFnSc2	Minerva
–	–	k?	–
o	o	k7c6	o
nálezu	nález	k1gInSc6	nález
základů	základ	k1gInPc2	základ
tohoto	tento	k3xDgInSc2	tento
chrámu	chrám	k1gInSc2	chrám
byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgInS	přesvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Schliemann	Schliemann	k1gInSc1	Schliemann
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
však	však	k9	však
zcela	zcela	k6eAd1	zcela
vysílen	vysílen	k2eAgMnSc1d1	vysílen
a	a	k8xC	a
zklamán	zklamán	k2eAgMnSc1d1	zklamán
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
našel	najít	k5eAaPmAgMnS	najít
mnoho	mnoho	k4c4	mnoho
indicií	indicie	k1gFnPc2	indicie
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
zcela	zcela	k6eAd1	zcela
nevratně	vratně	k6eNd1	vratně
nedokazovalo	dokazovat	k5eNaImAgNnS	dokazovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zcela	zcela	k6eAd1	zcela
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
přenechat	přenechat	k5eAaPmF	přenechat
vykopávky	vykopávka	k1gFnPc4	vykopávka
profesionálním	profesionální	k2eAgFnPc3d1	profesionální
archeologickým	archeologický	k2eAgFnPc3d1	archeologická
organizacím	organizace	k1gFnPc3	organizace
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tu	tu	k6eAd1	tu
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
týdnech	týden	k1gInPc6	týden
objevil	objevit	k5eAaPmAgMnS	objevit
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozložení	rozložení	k1gNnSc2	rozložení
kamenů	kámen	k1gInPc2	kámen
kolem	kolem	k6eAd1	kolem
byl	být	k5eAaImAgInS	být
však	však	k9	však
Schliemann	Schliemann	k1gInSc4	Schliemann
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
zdi	zeď	k1gFnPc1	zeď
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
nálezem	nález	k1gInSc7	nález
byly	být	k5eAaImAgInP	být
základy	základ	k1gInPc1	základ
čtvercové	čtvercový	k2eAgFnSc2d1	čtvercová
věže	věž	k1gFnSc2	věž
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
12	[number]	k4	12
x	x	k?	x
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
skalním	skalní	k2eAgInSc6d1	skalní
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
si	se	k3xPyFc3	se
byl	být	k5eAaImAgInS	být
Schliemann	Schliemann	k1gInSc1	Schliemann
zcela	zcela	k6eAd1	zcela
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
tolik	tolik	k6eAd1	tolik
opěvovanou	opěvovaný	k2eAgFnSc4d1	opěvovaná
homérskou	homérský	k2eAgFnSc4d1	homérská
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
přerušení	přerušení	k1gNnSc6	přerušení
vykopávek	vykopávka	k1gFnPc2	vykopávka
se	se	k3xPyFc4	se
Schliemann	Schliemann	k1gInSc1	Schliemann
navrátil	navrátit	k5eAaPmAgInS	navrátit
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
pak	pak	k6eAd1	pak
s	s	k7c7	s
dělníky	dělník	k1gMnPc7	dělník
zcela	zcela	k6eAd1	zcela
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
zeď	zeď	k1gFnSc4	zeď
nalezenou	nalezený	k2eAgFnSc4d1	nalezená
v	v	k7c6	v
předešlém	předešlý	k2eAgInSc6d1	předešlý
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
objevil	objevit	k5eAaPmAgInS	objevit
nápis	nápis	k1gInSc1	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
hlásající	hlásající	k2eAgInSc1d1	hlásající
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc4	ten
hieron	hieron	k1gMnSc1	hieron
<g/>
"	"	kIx"	"
což	což	k3yQnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
svatyně	svatyně	k1gFnSc1	svatyně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Schliemanna	Schliemann	k1gInSc2	Schliemann
nebylo	být	k5eNaImAgNnS	být
pochyb	pochyba	k1gFnPc2	pochyba
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
Athénin	Athénin	k2eAgInSc4d1	Athénin
chrám	chrám	k1gInSc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Postupným	postupný	k2eAgNnSc7d1	postupné
odkrýváním	odkrývání	k1gNnSc7	odkrývání
cca	cca	kA	cca
87	[number]	k4	87
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
zdi	zeď	k1gFnSc2	zeď
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
podélná	podélný	k2eAgFnSc1d1	podélná
strana	strana	k1gFnSc1	strana
chrámu	chrám	k1gInSc2	chrám
<g/>
)	)	kIx)	)
Schliemann	Schliemann	k1gMnSc1	Schliemann
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
chrámu	chrám	k1gInSc6	chrám
jiném	jiný	k2eAgInSc6d1	jiný
<g/>
,	,	kIx,	,
starším	starý	k2eAgMnPc3d2	starší
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
zrovna	zrovna	k6eAd1	zrovna
tak	tak	k9	tak
–	–	k?	–
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
největší	veliký	k2eAgFnSc1d3	veliký
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
Tróji	Trója	k1gFnSc6	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
aby	aby	kYmCp3nS	aby
toho	ten	k3xDgMnSc4	ten
nebylo	být	k5eNaImAgNnS	být
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
dřívější	dřívější	k2eAgMnSc1d1	dřívější
Schliemannův	Schliemannův	k2eAgMnSc1d1	Schliemannův
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Calvert	Calvert	k1gMnSc1	Calvert
začal	začít	k5eAaPmAgMnS	začít
Schliemannovu	Schliemannův	k2eAgFnSc4d1	Schliemannův
práci	práce	k1gFnSc4	práce
zpochybňovat	zpochybňovat	k5eAaImF	zpochybňovat
a	a	k8xC	a
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
zatím	zatím	k6eAd1	zatím
vykopaných	vykopaný	k2eAgFnPc2d1	vykopaná
vrstev	vrstva	k1gFnPc2	vrstva
není	být	k5eNaImIp3nS	být
hledanou	hledaný	k2eAgFnSc7d1	hledaná
homérskou	homérský	k2eAgFnSc7d1	homérská
Trójou	Trója	k1gFnSc7	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
dubna	duben	k1gInSc2	duben
1873	[number]	k4	1873
Schliemann	Schliemanna	k1gFnPc2	Schliemanna
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
dělníci	dělník	k1gMnPc1	dělník
odkryli	odkrýt	k5eAaPmAgMnP	odkrýt
dům	dům	k1gInSc4	dům
charakterem	charakter	k1gInSc7	charakter
velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc4d1	podobný
domu	dům	k1gInSc3	dům
pompejskému	pompejský	k2eAgNnSc3d1	pompejský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
pak	pak	k6eAd1	pak
hlásil	hlásit	k5eAaImAgMnS	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
široká	široký	k2eAgFnSc1d1	široká
ulice	ulice	k1gFnSc1	ulice
Pergamonu	pergamon	k1gInSc2	pergamon
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
předtím	předtím	k6eAd1	předtím
objeveného	objevený	k2eAgInSc2d1	objevený
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
dlážděná	dlážděný	k2eAgFnSc1d1	dlážděná
až	až	k9	až
metr	metr	k1gInSc4	metr
a	a	k8xC	a
půl	půl	k6eAd1	půl
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
a	a	k8xC	a
až	až	k6eAd1	až
metr	metr	k1gInSc4	metr
širokými	široký	k2eAgFnPc7d1	široká
kamennými	kamenný	k2eAgFnPc7d1	kamenná
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nepochyboval	pochybovat	k5eNaImAgInS	pochybovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
ulice	ulice	k1gFnSc1	ulice
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
něčemu	něco	k3yInSc3	něco
důležitému	důležitý	k2eAgNnSc3d1	důležité
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
objektu	objekt	k1gInSc3	objekt
trójského	trójský	k2eAgNnSc2d1	trójský
hradního	hradní	k2eAgNnSc2d1	hradní
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
vykopávkách	vykopávka	k1gFnPc6	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
předčil	předčít	k5eAaPmAgInS	předčít
jeho	jeho	k3xOp3gNnSc4	jeho
očekávání	očekávání	k1gNnSc4	očekávání
–	–	k?	–
nalezl	naleznout	k5eAaPmAgMnS	naleznout
dvě	dva	k4xCgNnPc1	dva
šest	šest	k4xCc1	šest
metrů	metr	k1gMnPc2	metr
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
brány	brána	k1gFnPc1	brána
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc1	jejich
měděné	měděný	k2eAgInPc1d1	měděný
svorníky	svorník	k1gInPc1	svorník
–	–	k?	–
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
domněnek	domněnka	k1gFnPc2	domněnka
Sakajská	Sakajský	k2eAgFnSc1d1	Sakajský
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Budovu	budova	k1gFnSc4	budova
nalezenou	nalezený	k2eAgFnSc4d1	nalezená
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
pak	pak	k6eAd1	pak
bez	bez	k7c2	bez
rozpaků	rozpak	k1gInPc2	rozpak
identifikoval	identifikovat	k5eAaBmAgInS	identifikovat
jako	jako	k9	jako
Priamův	Priamův	k2eAgInSc1d1	Priamův
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
chyběly	chybět	k5eAaImAgInP	chybět
mu	on	k3xPp3gMnSc3	on
důkazy	důkaz	k1gInPc1	důkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
jeho	jeho	k3xOp3gNnSc4	jeho
tvrzení	tvrzení	k1gNnSc4	tvrzení
nezvratně	zvratně	k6eNd1	zvratně
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
podal	podat	k5eAaPmAgMnS	podat
Schliemann	Schliemann	k1gMnSc1	Schliemann
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
zcela	zcela	k6eAd1	zcela
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
tak	tak	k9	tak
hodlá	hodlat	k5eAaImIp3nS	hodlat
ukončit	ukončit	k5eAaPmF	ukončit
své	svůj	k3xOyFgFnPc4	svůj
archeologické	archeologický	k2eAgFnPc4d1	archeologická
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zcela	zcela	k6eAd1	zcela
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trója	Trója	k1gFnSc1	Trója
existovala	existovat	k5eAaImAgFnS	existovat
<g/>
,	,	kIx,	,
netušil	tušit	k5eNaImAgMnS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgInSc1d3	veliký
objev	objev	k1gInSc1	objev
učiní	učinit	k5eAaImIp3nS	učinit
až	až	k9	až
po	po	k7c6	po
napsání	napsání	k1gNnSc6	napsání
svého	svůj	k3xOyFgNnSc2	svůj
hlášení	hlášení	k1gNnSc2	hlášení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
není	být	k5eNaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jakého	jaký	k3yIgInSc2	jaký
kalendáře	kalendář	k1gInSc2	kalendář
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
prameny	pramen	k1gInPc1	pramen
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
–	–	k?	–
Schliemann	Schliemann	k1gInSc4	Schliemann
objevil	objevit	k5eAaPmAgMnS	objevit
za	za	k7c7	za
Priamovým	Priamův	k2eAgInSc7d1	Priamův
palácem	palác	k1gInSc7	palác
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
trojskou	trojský	k2eAgFnSc4d1	Trojská
hradbu	hradba	k1gFnSc4	hradba
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
od	od	k7c2	od
Sakajské	Sakajský	k2eAgFnSc2d1	Sakajský
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
bádání	bádání	k1gNnSc6	bádání
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Priamova	Priamův	k2eAgInSc2d1	Priamův
domu	dům	k1gInSc2	dům
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
měděný	měděný	k2eAgInSc4d1	měděný
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
ležela	ležet	k5eAaImAgFnS	ležet
asi	asi	k9	asi
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c4	na
kámen	kámen	k1gInSc4	kámen
zatvrdlého	zatvrdlý	k2eAgInSc2d1	zatvrdlý
červeného	červený	k2eAgInSc2d1	červený
popela	popel	k1gInSc2	popel
a	a	k8xC	a
kalcinovaných	kalcinovaný	k2eAgFnPc2d1	kalcinovaná
trosek	troska	k1gFnPc2	troska
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ukrytý	ukrytý	k2eAgInSc4d1	ukrytý
bájný	bájný	k2eAgInSc4d1	bájný
Priamův	Priamův	k2eAgInSc4d1	Priamův
poklad	poklad	k1gInSc4	poklad
–	–	k?	–
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
833	[number]	k4	833
předmětů	předmět	k1gInPc2	předmět
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
malé	malý	k2eAgInPc4d1	malý
lístečky	lísteček	k1gInPc4	lísteček
<g/>
,	,	kIx,	,
hvězdičky	hvězdička	k1gFnPc4	hvězdička
<g/>
,	,	kIx,	,
kroužky	kroužek	k1gInPc4	kroužek
a	a	k8xC	a
knoflíky	knoflík	k1gInPc4	knoflík
vyrobené	vyrobený	k2eAgInPc4d1	vyrobený
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
řetězů	řetěz	k1gInPc2	řetěz
a	a	k8xC	a
diadémů	diadém	k1gInPc2	diadém
<g/>
,	,	kIx,	,
měděný	měděný	k2eAgInSc4d1	měděný
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
kotlík	kotlík	k1gInSc4	kotlík
a	a	k8xC	a
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
vázy	váza	k1gFnPc4	váza
a	a	k8xC	a
čepele	čepel	k1gFnPc4	čepel
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc4d1	zlatá
lahve	lahev	k1gFnPc4	lahev
a	a	k8xC	a
poháry	pohár	k1gInPc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
váze	váha	k1gFnSc6	váha
také	také	k6eAd1	také
nalezl	naleznout	k5eAaPmAgMnS	naleznout
dva	dva	k4xCgInPc4	dva
zlaté	zlatý	k2eAgInPc4d1	zlatý
diadémy	diadém	k1gInPc4	diadém
<g/>
,	,	kIx,	,
čelenku	čelenka	k1gFnSc4	čelenka
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
náušnicové	náušnicový	k2eAgInPc4d1	náušnicový
přívěšky	přívěšek	k1gInPc4	přívěšek
<g/>
,	,	kIx,	,
korále	korále	k1gInPc4	korále
<g/>
,	,	kIx,	,
knoflíky	knoflík	k1gInPc4	knoflík
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
náramků	náramek	k1gInPc2	náramek
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
poháry	pohár	k1gInPc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc4	všechen
vykopával	vykopávat	k5eAaImAgMnS	vykopávat
sám	sám	k3xTgMnSc1	sám
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
si	se	k3xPyFc3	se
poklad	poklad	k1gInSc4	poklad
nechat	nechat	k5eAaPmF	nechat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
vyvézt	vyvézt	k5eAaPmF	vyvézt
jej	on	k3xPp3gMnSc4	on
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Schliemannovi	Schliemann	k1gMnSc3	Schliemann
na	na	k7c4	na
Hissarliku	Hissarlika	k1gFnSc4	Hissarlika
bádali	bádat	k5eAaImAgMnP	bádat
i	i	k9	i
Vilém	Vilém	k1gMnSc1	Vilém
Dröpfeld	Dröpfeld	k1gMnSc1	Dröpfeld
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Blegen	Blegen	k1gInSc1	Blegen
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jejich	jejich	k3xOp3gNnSc2	jejich
bádání	bádání	k1gNnSc2	bádání
podléhá	podléhat	k5eAaImIp3nS	podléhat
rozdělení	rozdělení	k1gNnSc1	rozdělení
fází	fáze	k1gFnPc2	fáze
existence	existence	k1gFnSc2	existence
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
fází	fáze	k1gFnPc2	fáze
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
ještě	ještě	k9	ještě
do	do	k7c2	do
šesti	šest	k4xCc2	šest
podfází	podfáze	k1gFnPc2	podfáze
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
40	[number]	k4	40
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
vykopávek	vykopávka	k1gFnPc2	vykopávka
ujal	ujmout	k5eAaPmAgInS	ujmout
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
tým	tým	k1gInSc1	tým
vedený	vedený	k2eAgInSc1d1	vedený
profesorem	profesor	k1gMnSc7	profesor
Manfredem	Manfred	k1gMnSc7	Manfred
Korfamnnem	Korfamnn	k1gMnSc7	Korfamnn
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
Briana	Brian	k1gMnSc2	Brian
Rosea	Roseus	k1gMnSc2	Roseus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
vykopávky	vykopávka	k1gFnPc4	vykopávka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
pozdně	pozdně	k6eAd1	pozdně
bronzové	bronzový	k2eAgNnSc1d1	bronzové
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Možný	možný	k2eAgInSc1d1	možný
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hrotů	hrot	k1gInPc2	hrot
šípů	šíp	k1gInPc2	šíp
skrytých	skrytý	k2eAgInPc2d1	skrytý
hluboko	hluboko	k6eAd1	hluboko
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
datovaných	datovaný	k2eAgFnPc2d1	datovaná
do	do	k7c2	do
brzkého	brzký	k2eAgNnSc2d1	brzké
období	období	k1gNnSc2	období
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
pomocí	pomocí	k7c2	pomocí
magnetického	magnetický	k2eAgNnSc2d1	magnetické
zobrazování	zobrazování	k1gNnSc2	zobrazování
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
pod	pod	k7c7	pod
pevností	pevnost	k1gFnSc7	pevnost
lokalizována	lokalizován	k2eAgFnSc1d1	lokalizována
a	a	k8xC	a
odkryta	odkryt	k2eAgFnSc1d1	odkryta
jáma	jáma	k1gFnSc1	jáma
situována	situován	k2eAgFnSc1d1	situována
mezi	mezi	k7c7	mezi
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
řeckým	řecký	k2eAgNnSc7d1	řecké
a	a	k8xC	a
římským	římský	k2eAgNnSc7d1	římské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
jámy	jáma	k1gFnSc2	jáma
jsou	být	k5eAaImIp3nP	být
datovány	datovat	k5eAaImNgFnP	datovat
do	do	k7c2	do
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
–	–	k?	–
údajné	údajný	k2eAgFnSc2d1	údajná
doby	doba	k1gFnSc2	doba
Homérovy	Homérův	k2eAgFnSc2d1	Homérova
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Korfmann	Korfmann	k1gMnSc1	Korfmann
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
díra	díra	k1gFnSc1	díra
kdysi	kdysi	k6eAd1	kdysi
mohla	moct	k5eAaImAgFnS	moct
označovat	označovat	k5eAaImF	označovat
venkovní	venkovní	k2eAgFnSc4d1	venkovní
obranu	obrana	k1gFnSc4	obrana
mnohem	mnohem	k6eAd1	mnohem
většího	veliký	k2eAgNnSc2d2	veliký
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
datováno	datovat	k5eAaImNgNnS	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nedávné	dávný	k2eNgInPc4d1	nedávný
archeologické	archeologický	k2eAgInPc4d1	archeologický
důkazy	důkaz	k1gInPc4	důkaz
nalezené	nalezený	k2eAgInPc4d1	nalezený
Korfmannovým	Korfmannův	k2eAgInSc7d1	Korfmannův
týmem	tým	k1gInSc7	tým
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
vskutku	vskutku	k9	vskutku
Homérova	Homérův	k2eAgFnSc1d1	Homérova
Trója	Trója	k1gFnSc1	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
vede	vést	k5eAaImIp3nS	vést
nově	nově	k6eAd1	nově
povolené	povolený	k2eAgInPc1d1	povolený
výkopy	výkop	k1gInPc1	výkop
Korfamnnův	Korfamnnův	k2eAgMnSc1d1	Korfamnnův
kolega	kolega	k1gMnSc1	kolega
Ernest	Ernest	k1gMnSc1	Ernest
Pernicka	Pernicka	k1gFnSc1	Pernicka
<g/>
.	.	kIx.	.
</s>
<s>
Schliemann	Schliemann	k1gMnSc1	Schliemann
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
výzkumech	výzkum	k1gInPc6	výzkum
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
toho	ten	k3xDgMnSc4	ten
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
vrstev	vrstva	k1gFnPc2	vrstva
legendárního	legendární	k2eAgNnSc2d1	legendární
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
archeologickém	archeologický	k2eAgInSc6d1	archeologický
průzkumu	průzkum	k1gInSc6	průzkum
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
vrstva	vrstva	k1gFnSc1	vrstva
spadá	spadat	k5eAaImIp3nS	spadat
již	již	k6eAd1	již
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
neolitu	neolit	k1gInSc2	neolit
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Homérská	homérský	k2eAgFnSc1d1	homérská
Trója	Trója	k1gFnSc1	Trója
byla	být	k5eAaImAgFnS	být
kladena	klást	k5eAaImNgFnS	klást
do	do	k7c2	do
sedmé	sedmý	k4xOgFnSc2	sedmý
vrstvy	vrstva	k1gFnSc2	vrstva
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schliemann	Schliemann	k1gInSc1	Schliemann
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
omylem	omylem	k6eAd1	omylem
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
Priamova	Priamův	k2eAgInSc2d1	Priamův
pokladu	poklad	k1gInSc2	poklad
<g/>
)	)	kIx)	)
kladl	klást	k5eAaImAgInS	klást
do	do	k7c2	do
vrstvy	vrstva	k1gFnSc2	vrstva
druhé	druhý	k4xOgFnPc4	druhý
<g/>
,	,	kIx,	,
z	z	k7c2	z
konce	konec	k1gInSc2	konec
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Trója	Trója	k1gFnSc1	Trója
I	I	kA	I
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovina	polovina	k1gFnSc1	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
<g/>
,	,	kIx,	,
období	období	k1gNnSc4	období
pozdního	pozdní	k2eAgInSc2d1	pozdní
neolitu	neolit	k1gInSc2	neolit
Trója	Trója	k1gFnSc1	Trója
II	II	kA	II
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
,	,	kIx,	,
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
nalézáme	nalézat	k5eAaImIp1nP	nalézat
keramiku	keramika	k1gFnSc4	keramika
kykladského	kykladský	k2eAgInSc2d1	kykladský
a	a	k8xC	a
helladského	helladský	k2eAgInSc2d1	helladský
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spadá	spadat	k5eAaImIp3nS	spadat
Schliemannův	Schliemannův	k2eAgInSc1d1	Schliemannův
poklad	poklad	k1gInSc1	poklad
přisuzovaný	přisuzovaný	k2eAgInSc1d1	přisuzovaný
homérskému	homérský	k2eAgInSc3d1	homérský
králi	král	k1gMnSc6	král
Priamovi	Priamos	k1gMnSc6	Priamos
Trója	Trója	k1gFnSc1	Trója
III	III	kA	III
–	–	k?	–
V	V	kA	V
<g/>
:	:	kIx,	:
Až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
<g />
.	.	kIx.	.
</s>
<s>
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
nenabylo	nabýt	k5eNaPmAgNnS	nabýt
tak	tak	k6eAd1	tak
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
jako	jako	k8xS	jako
ve	v	k7c6	v
fázích	fáze	k1gFnPc6	fáze
předtím	předtím	k6eAd1	předtím
Trója	Trója	k1gFnSc1	Trója
VI	VI	kA	VI
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
nalézáme	nalézat	k5eAaImIp1nP	nalézat
známky	známka	k1gFnPc4	známka
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
osídlení	osídlení	k1gNnSc1	osídlení
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zničeno	zničen	k2eAgNnSc1d1	zničeno
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
<g />
.	.	kIx.	.
</s>
<s>
počátkem	počátkem	k7c2	počátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Trója	Trója	k1gFnSc1	Trója
VIIa	VIIa	k1gFnSc1	VIIa
<g/>
:	:	kIx,	:
1300	[number]	k4	1300
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
1190	[number]	k4	1190
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
Homérova	Homérův	k2eAgFnSc1d1	Homérova
Trója	Trója	k1gFnSc1	Trója
<g/>
;	;	kIx,	;
Epos	epos	k1gInSc1	epos
Ilias	Ilias	k1gFnSc1	Ilias
je	být	k5eAaImIp3nS	být
však	však	k9	však
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
fikci	fikce	k1gFnSc4	fikce
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
přepis	přepis	k1gInSc1	přepis
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
badatelé	badatel	k1gMnPc1	badatel
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trója	Trója	k1gFnSc1	Trója
VIIa	VIIa	k1gFnSc1	VIIa
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
útokem	útok	k1gInSc7	útok
<g />
.	.	kIx.	.
</s>
<s>
tzv.	tzv.	kA	tzv.
Mořských	mořský	k2eAgInPc2d1	mořský
národů	národ	k1gInPc2	národ
Trója	Trója	k1gFnSc1	Trója
VIIb	VIIb	k1gInSc1	VIIb
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Trója	Trója	k1gFnSc1	Trója
VIIb	VIIb	k1gInSc1	VIIb
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Trója	Trója	k1gFnSc1	Trója
VIIb	VIIb	k1gInSc1	VIIb
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
do	do	k7c2	do
950	[number]	k4	950
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Trója	Trója	k1gFnSc1	Trója
VIII	VIII	kA	VIII
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
okolo	okolo	k7c2	okolo
700	[number]	k4	700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Trója	Trója	k1gFnSc1	Trója
IX	IX	kA	IX
<g/>
:	:	kIx,	:
Ilium	Ilium	k1gInSc1	Ilium
<g/>
,	,	kIx,	,
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
Helenistické	Helenistický	k2eAgNnSc1d1	Helenistický
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Římany	Říman	k1gMnPc7	Říman
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Augusta	August	k1gMnSc2	August
a	a	k8xC	a
představovalo	představovat	k5eAaImAgNnS	představovat
důležité	důležitý	k2eAgNnSc1d1	důležité
město	město	k1gNnSc1	město
obchodu	obchod	k1gInSc2	obchod
až	až	k9	až
do	do	k7c2	do
založení	založení	k1gNnSc2	založení
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
v	v	k7c6	v
byzantské	byzantský	k2eAgFnSc6d1	byzantská
době	doba	k1gFnSc6	doba
město	město	k1gNnSc1	město
upadalo	upadat	k5eAaPmAgNnS	upadat
<g/>
,	,	kIx,	,
až	až	k8xS	až
nakonec	nakonec	k6eAd1	nakonec
úplně	úplně	k6eAd1	úplně
vymizelo	vymizet	k5eAaPmAgNnS	vymizet
</s>
<s>
Archeologické	archeologický	k2eAgNnSc1d1	Archeologické
naleziště	naleziště	k1gNnSc1	naleziště
Trója	Trója	k1gFnSc1	Trója
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
celosvětově	celosvětově	k6eAd1	celosvětově
chráněných	chráněný	k2eAgFnPc2d1	chráněná
památek	památka	k1gFnPc2	památka
UNESCO	UNESCO	kA	UNESCO
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
nálezů	nález	k1gInPc2	nález
z	z	k7c2	z
Tróje	Trója	k1gFnSc2	Trója
vlastní	vlastní	k2eAgNnPc4d1	vlastní
státní	státní	k2eAgNnPc4d1	státní
muzea	muzeum	k1gNnPc4	muzeum
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
