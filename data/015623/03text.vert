<s>
Chamitataxus	Chamitataxus	k1gMnSc1
</s>
<s>
Chamitataxus	Chamitataxus	k1gMnSc1
Chamitataxus	Chamitataxus	k1gMnSc1
avitus	avitus	k1gInSc4
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
lasicovití	lasicovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Mustelidae	Mustelidae	k1gNnSc7
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc4
</s>
<s>
Taxidiinae	Taxidiinae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
ChamitataxusOwen	ChamitataxusOwen	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Chamitataxus	Chamitataxus	k1gInSc1
avitusOwen	avitusOwna	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chamitataxus	Chamitataxus	k1gInSc1
je	být	k5eAaImIp3nS
vyhynulý	vyhynulý	k2eAgMnSc1d1
jezevec	jezevec	k1gMnSc1
z	z	k7c2
pozdního	pozdní	k2eAgInSc2d1
miocénu	miocén	k1gInSc2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
tedy	tedy	k9
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
zhruba	zhruba	k6eAd1
6	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
znám	znám	k2eAgInSc1d1
jediný	jediný	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
Chamitataxus	Chamitataxus	k1gMnSc1
avitus	avitus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
žijícím	žijící	k2eAgMnSc7d1
jezevcem	jezevec	k1gMnSc7
americkým	americký	k2eAgFnPc3d1
(	(	kIx(
<g/>
Taxidea	Taxidea	k1gFnSc1
taxus	taxus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
vyhynulým	vyhynulý	k2eAgMnSc7d1
jezevcem	jezevec	k1gMnSc7
rodu	rod	k1gInSc2
Pliotaxidea	Pliotaxidea	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc1d1
podčeleď	podčeleď	k1gFnSc1
Taxidiinae	Taxidiina	k1gFnSc2
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
tedy	tedy	k9
blízce	blízce	k6eAd1
příbuzní	příbuzný	k1gMnPc1
ostatním	ostatní	k2eAgMnPc3d1
jezevcům	jezevec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chamitataxus	Chamitataxus	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
těchto	tento	k3xDgFnPc2
tří	tři	k4xCgFnPc2
nejprimitivnější	primitivní	k2eAgFnSc7d3
<g/>
.	.	kIx.
</s>
<s>
Chamitataxus	Chamitataxus	k1gMnSc1
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
z	z	k7c2
jediné	jediný	k2eAgFnSc2d1
neúplné	úplný	k2eNgFnSc2d1
lebky	lebka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Chamita	Chamitum	k1gNnSc2
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
lomu	lom	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
také	také	k6eAd1
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
několik	několik	k4yIc1
fosílií	fosílie	k1gFnPc2
vyhynulého	vyhynulý	k2eAgMnSc2d1
vidloroha	vidloroh	k1gMnSc2
rodu	rod	k1gInSc2
Osbornoceros	Osbornocerosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Chamitataxus	Chamitataxus	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
