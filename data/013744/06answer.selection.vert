<s>
Spouštěčem	spouštěč	k1gInSc7
migrace	migrace	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
místní	místní	k2eAgNnSc1d1
podnebí	podnebí	k1gNnSc1
<g/>
,	,	kIx,
dostupnost	dostupnost	k1gFnSc1
potravy	potrava	k1gFnSc2
<g/>
,	,	kIx,
roční	roční	k2eAgNnSc1d1
období	období	k1gNnSc1
či	či	k8xC
potřeba	potřeba	k1gFnSc1
páření	páření	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Migrace	migrace	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pravidelné	pravidelný	k2eAgFnPc1d1
(	(	kIx(
<g/>
cyklicky	cyklicky	k6eAd1
se	se	k3xPyFc4
opakující	opakující	k2eAgMnPc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
nepravidelné	pravidelný	k2eNgNnSc1d1
(	(	kIx(
<g/>
náhodné	náhodný	k2eAgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
různé	různý	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
<g/>
.	.	kIx.
</s>