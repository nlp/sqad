<s>
Migrace	migrace	k1gFnSc1
zvířat	zvíře	k1gNnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pakoně	pakůň	k1gMnPc1
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
parku	park	k1gInSc2
Serengeti	Serenget	k1gMnPc1
při	při	k7c6
své	svůj	k3xOyFgFnSc6
pravidelné	pravidelný	k2eAgFnSc6d1
migraci	migrace	k1gFnSc6
</s>
<s>
Migrace	migrace	k1gFnSc1
zvířat	zvíře	k1gNnPc2
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
dálkový	dálkový	k2eAgInSc1d1
přesun	přesun	k1gInSc1
zvířat	zvíře	k1gNnPc2
z	z	k7c2
jedné	jeden	k4xCgFnSc2
oblasti	oblast	k1gFnSc2
do	do	k7c2
druhé	druhý	k4xOgFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děje	děj	k1gInSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
v	v	k7c6
sezónních	sezónní	k2eAgInPc6d1
cyklech	cyklus	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházíme	nacházet	k5eAaImIp1nP
ji	on	k3xPp3gFnSc4
u	u	k7c2
všech	všecek	k3xTgFnPc2
velkých	velký	k2eAgFnPc2d1
zvířecích	zvířecí	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
u	u	k7c2
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
obojživelníků	obojživelník	k1gMnPc2
<g/>
,	,	kIx,
hmyzu	hmyz	k1gInSc2
i	i	k8xC
korýšů	korýš	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spouštěčem	spouštěč	k1gInSc7
migrace	migrace	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
místní	místní	k2eAgNnSc1d1
podnebí	podnebí	k1gNnSc1
<g/>
,	,	kIx,
dostupnost	dostupnost	k1gFnSc1
potravy	potrava	k1gFnSc2
<g/>
,	,	kIx,
roční	roční	k2eAgNnSc1d1
období	období	k1gNnSc1
či	či	k8xC
potřeba	potřeba	k1gFnSc1
páření	páření	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Migrace	migrace	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pravidelné	pravidelný	k2eAgFnPc1d1
(	(	kIx(
<g/>
cyklicky	cyklicky	k6eAd1
se	se	k3xPyFc4
opakující	opakující	k2eAgMnPc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
nepravidelné	pravidelný	k2eNgNnSc1d1
(	(	kIx(
<g/>
náhodné	náhodný	k2eAgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
různé	různý	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pravidelné	pravidelný	k2eAgFnPc4d1
migrace	migrace	k1gFnPc4
</s>
<s>
Pravidelné	pravidelný	k2eAgFnPc1d1
migrace	migrace	k1gFnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
dané	daný	k2eAgFnSc2d1
některými	některý	k3yIgFnPc7
fázemi	fáze	k1gFnPc7
životního	životní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
opakovat	opakovat	k5eAaImF
v	v	k7c6
periodě	perioda	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
denní	denní	k2eAgFnSc4d1
(	(	kIx(
<g/>
cirkadiánní	cirkadiánní	k2eAgFnSc4d1
<g/>
)	)	kIx)
–	–	k?
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
fytoplankton	fytoplankton	k1gInSc1
migrující	migrující	k2eAgInSc1d1
za	za	k7c2
denního	denní	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
k	k	k7c3
hladině	hladina	k1gFnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
mohl	moct	k5eAaImAgMnS
provádět	provádět	k5eAaImF
fotosyntézu	fotosyntéza	k1gFnSc4
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
v	v	k7c6
noci	noc	k1gFnSc6
se	se	k3xPyFc4
stahuje	stahovat	k5eAaImIp3nS
do	do	k7c2
větších	veliký	k2eAgFnPc2d2
hloubek	hloubka	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
unikl	uniknout	k5eAaPmAgMnS
případným	případný	k2eAgMnSc7d1
predátorům	predátor	k1gMnPc3
</s>
<s>
měsíční	měsíční	k2eAgInPc4d1
(	(	kIx(
<g/>
lunární	lunární	k2eAgInPc4d1
<g/>
)	)	kIx)
–	–	k?
poměrně	poměrně	k6eAd1
řídký	řídký	k2eAgInSc4d1
případ	případ	k1gInSc4
<g/>
,	,	kIx,
souvisí	souviset	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
s	s	k7c7
rozmnožováním	rozmnožování	k1gNnSc7
spojeným	spojený	k2eAgNnSc7d1
s	s	k7c7
konkrétní	konkrétní	k2eAgFnSc7d1
lunární	lunární	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
</s>
<s>
roční	roční	k2eAgInSc1d1
–	–	k?
nejčastější	častý	k2eAgInSc4d3
případ	případ	k1gInSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
jak	jak	k8xC,k8xS
migrace	migrace	k1gFnSc1
ptačí	ptačit	k5eAaImIp3nS
(	(	kIx(
<g/>
tah	tah	k1gInSc1
do	do	k7c2
„	„	k?
<g/>
teplých	teplý	k2eAgFnPc2d1
krajin	krajina	k1gFnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
migrace	migrace	k1gFnSc1
velkých	velký	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
na	na	k7c6
savanách	savana	k1gFnPc6
řízené	řízený	k2eAgFnSc2d1
střídáním	střídání	k1gNnSc7
období	období	k1gNnSc4
sucha	sucho	k1gNnSc2
a	a	k8xC
dešťů	dešť	k1gInPc2
(	(	kIx(
<g/>
popřípadě	popřípadě	k6eAd1
tah	tah	k1gInSc1
sobů	sob	k1gMnPc2
v	v	k7c6
severské	severský	k2eAgFnSc6d1
tundře	tundra	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známá	k1gFnPc1
jsou	být	k5eAaImIp3nP
i	i	k9
roční	roční	k2eAgInPc1d1
tahy	tah	k1gInPc1
lososovitých	lososovitý	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
koryty	koryto	k1gNnPc7
řek	řeka	k1gFnPc2
či	či	k8xC
tah	tah	k1gInSc1
lumíků	lumík	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
zajímavé	zajímavý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
tahy	tah	k1gInPc1
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
hmyzu	hmyz	k1gInSc2
<g/>
,	,	kIx,
kupř	kupř	kA
<g/>
.	.	kIx.
severoamerického	severoamerický	k2eAgInSc2d1
druhu	druh	k1gInSc2
motýla	motýl	k1gMnSc2
monarchy	monarcha	k1gMnSc2
stěhovavého	stěhovavý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Nepravidelné	pravidelný	k2eNgFnPc1d1
migrace	migrace	k1gFnPc1
</s>
<s>
Nepravidelné	pravidelný	k2eNgFnPc1d1
migrace	migrace	k1gFnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
za	za	k7c4
příčinu	příčina	k1gFnSc4
lokální	lokální	k2eAgNnSc1d1
přemnožení	přemnožení	k1gNnSc1
<g/>
,	,	kIx,
zhoršení	zhoršení	k1gNnSc1
životních	životní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
znečištění	znečištění	k1gNnSc1
<g/>
,	,	kIx,
úbytek	úbytek	k1gInSc1
potravy	potrava	k1gFnSc2
či	či	k8xC
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
nárůst	nárůst	k1gInSc4
počtu	počet	k1gInSc2
predátorů	predátor	k1gMnPc2
nebo	nebo	k8xC
kompetitorů	kompetitor	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
nepravidelných	pravidelný	k2eNgFnPc2d1
migrací	migrace	k1gFnPc2
lze	lze	k6eAd1
zařadit	zařadit	k5eAaPmF
i	i	k9
šíření	šíření	k1gNnSc4
organismů	organismus	k1gInPc2
-	-	kIx~
např.	např.	kA
invazní	invazní	k2eAgInPc1d1
nebo	nebo	k8xC
expanzivní	expanzivní	k2eAgInPc1d1
druhy	druh	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
migrace	migrace	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
velice	velice	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
několika	několik	k4yIc2
metrů	metr	k1gInPc2
až	až	k6eAd1
po	po	k7c4
tisíce	tisíc	k4xCgInPc4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
nejdelší	dlouhý	k2eAgFnSc4d3
migraci	migrace	k1gFnSc4
vykonává	vykonávat	k5eAaImIp3nS
rybák	rybák	k1gMnSc1
dlouhoocasý	dlouhoocasý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Sterna	sternum	k1gNnSc2
paradisaea	paradisae	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dvakrát	dvakrát	k6eAd1
za	za	k7c4
rok	rok	k1gInSc4
překoná	překonat	k5eAaPmIp3nS
téměř	téměř	k6eAd1
polovinu	polovina	k1gFnSc4
planety	planeta	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
léto	léto	k1gNnSc1
tráví	trávit	k5eAaImIp3nS
v	v	k7c6
Arktidě	Arktida	k1gFnSc6
a	a	k8xC
zimu	zima	k1gFnSc4
v	v	k7c6
Antarktidě	Antarktida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
migrační	migrační	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
tak	tak	k6eAd1
přesahuje	přesahovat	k5eAaImIp3nS
17	#num#	k4
000	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavý	zajímavý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
případ	případ	k1gInSc1
migrace	migrace	k1gFnSc2
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
hmyzu	hmyz	k1gInSc2
přes	přes	k7c4
Atlantský	atlantský	k2eAgInSc4d1
oceán	oceán	k1gInSc4
(	(	kIx(
<g/>
4500	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
saranče	saranče	k1gFnSc1
Schistocerca	Schistocerc	k1gInSc2
gregaria	gregarium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Migrace	migrace	k1gFnPc1
jsou	být	k5eAaImIp3nP
motorem	motor	k1gInSc7
genového	genový	k2eAgInSc2d1
toku	tok	k1gInSc2
–	–	k?
procesu	proces	k1gInSc2
obměny	obměna	k1gFnSc2
genofondu	genofond	k1gInSc6
geny	gen	k1gInPc1
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
populací	populace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dingle	Dingle	k1gInSc1
<g/>
,	,	kIx,
Hugh	Hugh	k1gMnSc1
<g/>
;	;	kIx,
DRAKE	DRAKE	kA
<g/>
,	,	kIx,
V.	V.	kA
ALISTAIR	ALISTAIR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k2eAgInSc1d1
is	is	k?
migration	migration	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioScience	BioScience	k1gFnSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
113	#num#	k4
<g/>
–	–	k?
<g/>
121	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.164	10.164	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
B	B	kA
<g/>
570206	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
Animals	Animalsa	k1gFnPc2
Migrate	Migrat	k1gMnSc5
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Migrace	migrace	k1gFnSc1
rostlin	rostlina	k1gFnPc2
</s>
<s>
Migrace	migrace	k1gFnSc1
ryb	ryba	k1gFnPc2
</s>
<s>
Foréze	Foréza	k1gFnSc3
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4133439-5	4133439-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
987	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85005211	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85005211	#num#	k4
</s>
