<s>
Geolog	geolog	k1gMnSc1	geolog
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
geologií	geologie	k1gFnSc7	geologie
<g/>
.	.	kIx.	.
</s>
<s>
Geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
věcmi	věc	k1gFnPc7	věc
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
dějí	dít	k5eAaBmIp3nP	dít
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
hlavní	hlavní	k2eAgFnPc4d1	hlavní
zájmové	zájmový	k2eAgFnPc4d1	zájmová
skupiny	skupina	k1gFnPc4	skupina
patří	patřit	k5eAaImIp3nS	patřit
horninové	horninový	k2eAgNnSc1d1	horninové
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
zemského	zemský	k2eAgNnSc2d1	zemské
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jí	on	k3xPp3gFnSc3	on
formují	formovat	k5eAaImIp3nP	formovat
a	a	k8xC	a
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
sledované	sledovaný	k2eAgFnSc2d1	sledovaná
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sběru	sběr	k1gInSc6	sběr
vzorků	vzorek	k1gInPc2	vzorek
a	a	k8xC	a
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
geologických	geologický	k2eAgFnPc2d1	geologická
map	mapa	k1gFnPc2	mapa
zájmového	zájmový	k2eAgNnSc2d1	zájmové
území	území	k1gNnSc2	území
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
interpretace	interpretace	k1gFnSc1	interpretace
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
pracovní	pracovní	k2eAgFnPc4d1	pracovní
pomůcky	pomůcka	k1gFnPc4	pomůcka
patří	patřit	k5eAaImIp3nS	patřit
geologické	geologický	k2eAgNnSc1d1	geologické
kladívko	kladívko	k1gNnSc1	kladívko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
sběr	sběr	k1gInSc4	sběr
vzorků	vzorek	k1gInPc2	vzorek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
jako	jako	k9	jako
základní	základní	k2eAgNnSc4d1	základní
geologické	geologický	k2eAgNnSc4d1	geologické
měřítko	měřítko	k1gNnSc4	měřítko
používané	používaný	k2eAgNnSc4d1	používané
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
při	při	k7c6	při
pořizování	pořizování	k1gNnSc6	pořizování
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
je	být	k5eAaImIp3nS	být
geologický	geologický	k2eAgInSc1d1	geologický
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
měřit	měřit	k5eAaImF	měřit
struktury	struktura	k1gFnPc4	struktura
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
určovat	určovat	k5eAaImF	určovat
jejich	jejich	k3xOp3gInSc4	jejich
základní	základní	k2eAgInSc4d1	základní
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
tužky	tužka	k1gFnSc2	tužka
a	a	k8xC	a
papíru	papír	k1gInSc2	papír
pak	pak	k6eAd1	pak
vypracovávat	vypracovávat	k5eAaImF	vypracovávat
základní	základní	k2eAgFnSc4d1	základní
mapu	mapa	k1gFnSc4	mapa
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odbornou	odborný	k2eAgFnSc4d1	odborná
profesi	profese	k1gFnSc4	profese
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
mluvě	mluva	k1gFnSc6	mluva
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
příbuzná	příbuzný	k2eAgNnPc4d1	příbuzné
povolání	povolání	k1gNnPc4	povolání
patří	patřit	k5eAaImIp3nP	patřit
paleontolog	paleontolog	k1gMnSc1	paleontolog
<g/>
,	,	kIx,	,
vulkanolog	vulkanolog	k1gMnSc1	vulkanolog
<g/>
,	,	kIx,	,
seismolog	seismolog	k1gMnSc1	seismolog
<g/>
,	,	kIx,	,
mineralog	mineralog	k1gMnSc1	mineralog
<g/>
,	,	kIx,	,
hydrogeolog	hydrogeolog	k1gMnSc1	hydrogeolog
<g/>
,	,	kIx,	,
či	či	k8xC	či
petrolog	petrolog	k1gMnSc1	petrolog
<g/>
.	.	kIx.	.
</s>
<s>
Harrison	Harrison	k1gMnSc1	Harrison
H.	H.	kA	H.
Schmitt	Schmitt	k1gMnSc1	Schmitt
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
Apolla	Apollo	k1gNnSc2	Apollo
17	[number]	k4	17
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
geolog	geolog	k1gMnSc1	geolog
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
doposud	doposud	k6eAd1	doposud
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
