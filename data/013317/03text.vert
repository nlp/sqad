<p>
<s>
Lukoil	Lukoil	k1gInSc1	Lukoil
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Л	Л	k?	Л
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
ruská	ruský	k2eAgFnSc1d1	ruská
firma	firma	k1gFnSc1	firma
podnikající	podnikající	k2eAgFnSc1d1	podnikající
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
těžby	těžba	k1gFnSc2	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc2	zpracování
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
její	její	k3xOp3gFnSc2	její
centrály	centrála	k1gFnSc2	centrála
je	být	k5eAaImIp3nS	být
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
provozuje	provozovat	k5eAaImIp3nS	provozovat
ropné	ropný	k2eAgFnPc4d1	ropná
rafinérie	rafinérie	k1gFnPc4	rafinérie
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
spoluvlastní	spoluvlastnit	k5eAaImIp3nS	spoluvlastnit
pak	pak	k6eAd1	pak
rafinérii	rafinérie	k1gFnSc4	rafinérie
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hodnocení	hodnocení	k1gNnSc2	hodnocení
odbytu	odbyt	k1gInSc2	odbyt
produkce	produkce	k1gFnSc2	produkce
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
Lukoil	Lukoil	k1gMnSc1	Lukoil
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
mezi	mezi	k7c7	mezi
největšími	veliký	k2eAgMnPc7d3	veliký
ruskými	ruský	k2eAgMnPc7d1	ruský
společnostmi	společnost	k1gFnPc7	společnost
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
Gazprom	Gazprom	k1gInSc1	Gazprom
<g/>
,	,	kIx,	,
na	na	k7c6	na
třetím	třetí	k4xOgMnSc6	třetí
Rosněfť	Rosněfť	k1gFnSc1	Rosněfť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Lukoil	Lukoil	k1gInSc1	Lukoil
prodává	prodávat	k5eAaImIp3nS	prodávat
automobilové	automobilový	k2eAgInPc4d1	automobilový
benzíny	benzín	k1gInPc4	benzín
a	a	k8xC	a
motorovou	motorový	k2eAgFnSc4d1	motorová
naftu	nafta	k1gFnSc4	nafta
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
22	[number]	k4	22
zemích	zem	k1gFnPc6	zem
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ale	ale	k8xC	ale
už	už	k6eAd1	už
ne	ne	k9	ne
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
skupina	skupina	k1gFnSc1	skupina
MOL	mol	k1gInSc1	mol
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
dokončila	dokončit	k5eAaPmAgFnS	dokončit
převzetí	převzetí	k1gNnSc4	převzetí
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
Lukoil	Lukoil	k1gMnSc1	Lukoil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Lukoil	Lukoil	k1gMnSc1	Lukoil
sponzoroval	sponzorovat	k5eAaImAgMnS	sponzorovat
překlad	překlad	k1gInSc4	překlad
a	a	k8xC	a
ruské	ruský	k2eAgNnSc1d1	ruské
vydání	vydání	k1gNnSc1	vydání
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
knihy	kniha	k1gFnSc2	kniha
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
Modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zelená	zelený	k2eAgFnSc1d1	zelená
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bagatelizuje	bagatelizovat	k5eAaImIp3nS	bagatelizovat
dopady	dopad	k1gInPc4	dopad
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
snahy	snaha	k1gFnPc4	snaha
ekologů	ekolog	k1gMnPc2	ekolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
Lukoil	Lukoila	k1gFnPc2	Lukoila
Aviation	Aviation	k1gInSc4	Aviation
Czech	Czech	k1gInSc4	Czech
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dodávání	dodávání	k1gNnSc4	dodávání
leteckého	letecký	k2eAgNnSc2d1	letecké
paliva	palivo	k1gNnSc2	palivo
pro	pro	k7c4	pro
státem	stát	k1gInSc7	stát
vlastněné	vlastněný	k2eAgNnSc1d1	vlastněné
letiště	letiště	k1gNnSc1	letiště
Praha	Praha	k1gFnSc1	Praha
Ruzyně	Ruzyně	k1gFnSc1	Ruzyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lukoil	Lukoila	k1gFnPc2	Lukoila
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lukoil	Lukoil	k1gInSc4	Lukoil
oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Lukoil	Lukoil	k1gInSc4	Lukoil
oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Sabina	Sabina	k1gFnSc1	Sabina
Slonková	Slonková	k1gFnSc1	Slonková
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Bártová	Bártová	k1gFnSc1	Bártová
<g/>
:	:	kIx,	:
Vláda	vláda	k1gFnSc1	vláda
Topolánka	Topolánek	k1gMnSc2	Topolánek
a	a	k8xC	a
Šloufa	Šlouf	k1gMnSc2	Šlouf
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Lukoil	Lukoil	k1gInSc1	Lukoil
tiše	tiš	k1gFnSc2	tiš
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
byznys	byznys	k1gInSc1	byznys
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
aktualne	aktualnout	k5eAaImIp3nS	aktualnout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
