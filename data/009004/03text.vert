<p>
<s>
Otorhinolaryngologie	otorhinolaryngologie	k1gFnSc1	otorhinolaryngologie
(	(	kIx(	(
<g/>
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
otorinolaryngologie	otorinolaryngologie	k1gFnPc1	otorinolaryngologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ORL	ORL	kA	ORL
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
lékařský	lékařský	k2eAgInSc1d1	lékařský
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
diagnózu	diagnóza	k1gFnSc4	diagnóza
a	a	k8xC	a
léčbu	léčba	k1gFnSc4	léčba
chorob	choroba	k1gFnPc2	choroba
ušních	ušní	k2eAgMnPc2d1	ušní
<g/>
,	,	kIx,	,
nosních	nosní	k2eAgMnPc2d1	nosní
a	a	k8xC	a
krčních	krční	k2eAgMnPc2d1	krční
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
zabývá	zabývat	k5eAaImIp3nS	zabývat
onemocněními	onemocnění	k1gNnPc7	onemocnění
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
lékařské	lékařský	k2eAgFnSc2d1	lékařská
disciplíny	disciplína	k1gFnSc2	disciplína
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
otorhinolaryngologie	otorhinolaryngologie	k1gFnSc1	otorhinolaryngologie
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc1	chirurgie
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
disciplínou	disciplína	k1gFnSc7	disciplína
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
otorhinolaryngologové	otorhinolaryngolog	k1gMnPc1	otorhinolaryngolog
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nelékaři	nelékař	k1gMnPc7	nelékař
běžným	běžný	k2eAgInSc7d1	běžný
názvem	název	k1gInSc7	název
této	tento	k3xDgFnSc2	tento
lékařské	lékařský	k2eAgFnSc2d1	lékařská
disciplíny	disciplína	k1gFnSc2	disciplína
je	být	k5eAaImIp3nS	být
ušní	ušeň	k1gFnSc7	ušeň
nosní	nosní	k2eAgFnSc1d1	nosní
<g/>
,	,	kIx,	,
krční	krční	k2eAgFnSc1d1	krční
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
zkratka	zkratka	k1gFnSc1	zkratka
ORL	ORL	kA	ORL
(	(	kIx(	(
<g/>
OtoRhinoLaryngologie	otorhinolaryngologie	k1gFnSc1	otorhinolaryngologie
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
ω	ω	k?	ω
(	(	kIx(	(
<g/>
otos	otos	k1gInSc1	otos
=	=	kIx~	=
druhý	druhý	k4xOgInSc1	druhý
pád	pád	k1gInSc1	pád
slova	slovo	k1gNnSc2	slovo
ucho	ucho	k1gNnSc1	ucho
<g/>
,	,	kIx,	,
rhinos	rhinos	k1gInSc1	rhinos
=	=	kIx~	=
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
larynx	larynx	k1gInSc1	larynx
=	=	kIx~	=
hrtan	hrtan	k1gInSc1	hrtan
(	(	kIx(	(
<g/>
krk	krk	k1gInSc1	krk
<g/>
/	/	kIx~	/
<g/>
hrdlo	hrdlo	k1gNnSc1	hrdlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
logos	logos	k1gInSc1	logos
=	=	kIx~	=
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
této	tento	k3xDgFnSc2	tento
zkratky	zkratka	k1gFnSc2	zkratka
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
uchu	ucho	k1gNnSc6	ucho
<g/>
,	,	kIx,	,
nosu	nos	k1gInSc6	nos
a	a	k8xC	a
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Atestace	atestace	k1gFnPc1	atestace
v	v	k7c6	v
ORL	ORL	kA	ORL
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
oboru	obor	k1gInSc2	obor
ORL	ORL	kA	ORL
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přihlásit	přihlásit	k5eAaPmF	přihlásit
lékař	lékař	k1gMnSc1	lékař
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
<g/>
.	.	kIx.	.
</s>
<s>
Předatestační	Předatestační	k2eAgFnSc1d1	Předatestační
praxe	praxe	k1gFnSc1	praxe
trvá	trvat	k5eAaImIp3nS	trvat
minimálně	minimálně	k6eAd1	minimálně
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lékař	lékař	k1gMnSc1	lékař
ucházející	ucházející	k2eAgMnSc1d1	ucházející
se	se	k3xPyFc4	se
o	o	k7c4	o
atestaci	atestace	k1gFnSc4	atestace
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
musí	muset	k5eAaImIp3nS	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
minimálně	minimálně	k6eAd1	minimálně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
60	[number]	k4	60
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
ORL	ORL	kA	ORL
s	s	k7c7	s
lůžkovou	lůžkový	k2eAgFnSc7d1	lůžková
i	i	k8xC	i
ambulantní	ambulantní	k2eAgFnSc7d1	ambulantní
částí	část	k1gFnSc7	část
</s>
</p>
<p>
<s>
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
chirurgickém	chirurgický	k2eAgNnSc6d1	chirurgické
</s>
</p>
<p>
<s>
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
interním	interní	k2eAgInSc7d1	interní
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
ARO	ara	k1gMnSc5	ara
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
maxilofaciální	maxilofaciální	k2eAgFnSc2d1	maxilofaciální
chirurgie	chirurgie	k1gFnSc2	chirurgie
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
traumatologie	traumatologie	k1gFnSc2	traumatologie
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
onkologie	onkologie	k1gFnSc2	onkologie
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
pediatrie	pediatrie	k1gFnSc2	pediatrie
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
plastické	plastický	k2eAgFnSc2d1	plastická
chirurgiedoporučena	chirurgiedoporučit	k5eAaPmNgFnS	chirurgiedoporučit
je	být	k5eAaImIp3nS	být
i	i	k9	i
praxe	praxe	k1gFnSc1	praxe
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
neurologie	neurologie	k1gFnSc2	neurologie
</s>
</p>
<p>
<s>
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
infekčního	infekční	k2eAgNnSc2d1	infekční
lékařství	lékařství	k1gNnSc2	lékařství
</s>
</p>
<p>
<s>
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
na	na	k7c4	na
oddělení	oddělení	k1gNnSc4	oddělení
očního	oční	k2eAgInSc2d1	oční
lékařstvíPo	lékařstvíPo	k6eAd1	lékařstvíPo
absolvování	absolvování	k1gNnSc4	absolvování
předepsané	předepsaný	k2eAgFnSc2d1	předepsaná
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
složení	složení	k1gNnSc2	složení
atestační	atestační	k2eAgFnSc2d1	atestační
zkoušky	zkouška	k1gFnSc2	zkouška
je	být	k5eAaImIp3nS	být
lékař	lékař	k1gMnSc1	lékař
plně	plně	k6eAd1	plně
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
práce	práce	k1gFnSc2	práce
ORL	ORL	kA	ORL
specialisty	specialista	k1gMnSc2	specialista
v	v	k7c6	v
nemocniční	nemocniční	k2eAgFnSc6d1	nemocniční
i	i	k8xC	i
ambulantní	ambulantní	k2eAgFnSc6d1	ambulantní
sféře	sféra	k1gFnSc6	sféra
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
i	i	k8xC	i
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dále	daleko	k6eAd2	daleko
specializovat	specializovat	k5eAaBmF	specializovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
audiologie	audiologie	k1gFnSc2	audiologie
a	a	k8xC	a
foniatrie	foniatrie	k1gFnSc2	foniatrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástavbové	nástavbový	k2eAgInPc4d1	nástavbový
obory	obor	k1gInPc4	obor
ORL	ORL	kA	ORL
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
foniatrie	foniatrie	k1gFnSc1	foniatrie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
poruchami	porucha	k1gFnPc7	porucha
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
sluchu	sluch	k1gInSc2	sluch
a	a	k8xC	a
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
nejen	nejen	k6eAd1	nejen
běžných	běžný	k2eAgFnPc2d1	běžná
léčebných	léčebný	k2eAgFnPc2d1	léčebná
metod	metoda	k1gFnPc2	metoda
–	–	k?	–
medikamentosních	medikamentosní	k2eAgNnPc2d1	medikamentosní
<g/>
,	,	kIx,	,
chirurgických	chirurgický	k2eAgNnPc2d1	chirurgické
–	–	k?	–
ale	ale	k8xC	ale
také	také	k9	také
speciální	speciální	k2eAgFnPc4d1	speciální
metody	metoda	k1gFnPc4	metoda
reedukační	reedukační	k2eAgNnSc4d1	reedukační
či	či	k8xC	či
edukační	edukační	k2eAgNnSc4d1	edukační
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
nácvik	nácvik	k1gInSc4	nácvik
správné	správný	k2eAgFnSc2d1	správná
tvorby	tvorba	k1gFnSc2	tvorba
hlasové	hlasový	k2eAgFnSc2d1	hlasová
–	–	k?	–
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
především	především	k9	především
u	u	k7c2	u
hlasových	hlasový	k2eAgMnPc2d1	hlasový
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
<g/>
neurootologie	neurootologie	k1gFnSc1	neurootologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
ušními	ušní	k2eAgInPc7d1	ušní
šelesty	šelest	k1gInPc7	šelest
a	a	k8xC	a
závraťovými	závraťový	k2eAgInPc7d1	závraťový
stavy	stav	k1gInPc7	stav
ORL	ORL	kA	ORL
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
speciálních	speciální	k2eAgFnPc2d1	speciální
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
kraniokorpografie	kraniokorpografie	k1gFnSc1	kraniokorpografie
<g/>
,	,	kIx,	,
elektronystagmografie	elektronystagmografie	k1gFnSc1	elektronystagmografie
<g/>
,	,	kIx,	,
videonystagmografie	videonystagmografie	k1gFnSc1	videonystagmografie
a	a	k8xC	a
posturografie	posturografie	k1gFnSc1	posturografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
</s>
</p>
<p>
<s>
Atestace	atestace	k1gFnSc2	atestace
(	(	kIx(	(
<g/>
lékaře	lékař	k1gMnSc2	lékař
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
lékařských	lékařský	k2eAgInPc2d1	lékařský
oborů	obor	k1gInPc2	obor
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
otorhinolaryngologie	otorhinolaryngologie	k1gFnSc2	otorhinolaryngologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
otorinolaryngologie	otorinolaryngologie	k1gFnSc2	otorinolaryngologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
otorhinolaryngologii	otorhinolaryngologie	k1gFnSc4	otorhinolaryngologie
a	a	k8xC	a
chirurgii	chirurgie	k1gFnSc4	chirurgie
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
ČLS	ČLS	kA	ČLS
JEP	JEP	kA	JEP
</s>
</p>
