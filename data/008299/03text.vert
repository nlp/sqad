<p>
<s>
Kostel	kostel	k1gInSc4	kostel
Zjevení	zjevení	k1gNnSc2	zjevení
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
archanděla	archanděl	k1gMnSc2	archanděl
(	(	kIx(	(
<g/>
též	též	k9	též
nazýván	nazývat	k5eAaImNgInS	nazývat
svatého	svatý	k2eAgMnSc4d1	svatý
Michaela	Michael	k1gMnSc4	Michael
archanděla	archanděl	k1gMnSc4	archanděl
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Andělská	andělský	k2eAgFnSc1d1	Andělská
Hora	hora	k1gFnSc1	hora
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
upraven	upravit	k5eAaPmNgInS	upravit
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
památkově	památkově	k6eAd1	památkově
chráněn	chráněn	k2eAgMnSc1d1	chráněn
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
současně	současně	k6eAd1	současně
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
domy	dům	k1gInPc7	dům
obce	obec	k1gFnSc2	obec
vystavěn	vystavět	k5eAaPmNgInS	vystavět
v	v	k7c6	v
letech	let	k1gInPc6	let
1487	[number]	k4	1487
<g/>
–	–	k?	–
<g/>
1490	[number]	k4	1490
kamenickým	kamenický	k2eAgMnSc7d1	kamenický
mistrem	mistr	k1gMnSc7	mistr
Erhartem	Erhart	k1gMnSc7	Erhart
Bauerem	Bauer	k1gMnSc7	Bauer
z	z	k7c2	z
Chebu	Cheb	k1gInSc2	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Potkaly	potkat	k5eAaPmAgFnP	potkat
jej	on	k3xPp3gMnSc4	on
dva	dva	k4xCgInPc4	dva
ničivé	ničivý	k2eAgInPc4d1	ničivý
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1641	[number]	k4	1641
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
pak	pak	k9	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1718	[number]	k4	1718
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgFnSc4	první
opravu	oprava	k1gFnSc4	oprava
se	se	k3xPyFc4	se
složili	složit	k5eAaPmAgMnP	složit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postihl	postihnout	k5eAaPmAgInS	postihnout
i	i	k9	i
obec	obec	k1gFnSc4	obec
samotnou	samotný	k2eAgFnSc4d1	samotná
a	a	k8xC	a
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
přestavbou	přestavba	k1gFnSc7	přestavba
prošel	projít	k5eAaPmAgInS	projít
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
upraven	upravit	k5eAaPmNgInS	upravit
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
architekta	architekt	k1gMnSc2	architekt
Antona	Anton	k1gMnSc2	Anton
Viktora	Viktor	k1gMnSc2	Viktor
Barvitia	Barvitius	k1gMnSc2	Barvitius
<g/>
.	.	kIx.	.
</s>
<s>
Zasazen	zasazen	k2eAgInSc1d1	zasazen
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
oltář	oltář	k1gInSc1	oltář
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
ten	ten	k3xDgInSc1	ten
původní	původní	k2eAgMnSc1d1	původní
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
účel	účel	k1gInSc4	účel
kostela	kostel	k1gInSc2	kostel
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
přidány	přidat	k5eAaPmNgFnP	přidat
byly	být	k5eAaImAgFnP	být
varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
stěny	stěna	k1gFnPc1	stěna
a	a	k8xC	a
strop	strop	k1gInSc1	strop
byly	být	k5eAaImAgInP	být
nově	nově	k6eAd1	nově
vymalovány	vymalován	k2eAgInPc1d1	vymalován
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
se	se	k3xPyFc4	se
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
dočkala	dočkat	k5eAaPmAgFnS	dočkat
kostelní	kostelní	k2eAgFnSc1d1	kostelní
fasáda	fasáda	k1gFnSc1	fasáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
filiální	filiální	k2eAgInSc1d1	filiální
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
Stanovice	Stanovice	k1gFnSc2	Stanovice
<g/>
,	,	kIx,	,
Vikariát	vikariát	k1gInSc4	vikariát
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Diecéze	diecéze	k1gFnSc1	diecéze
plzeňská	plzeňská	k1gFnSc1	plzeňská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
stojí	stát	k5eAaImIp3nS	stát
při	při	k7c6	při
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
nejvýše	vysoce	k6eAd3	vysoce
položeném	položený	k2eAgNnSc6d1	položené
místě	místo	k1gNnSc6	místo
zástavby	zástavba	k1gFnSc2	zástavba
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obdélný	obdélný	k2eAgInSc4d1	obdélný
půdorys	půdorys	k1gInSc4	půdorys
16	[number]	k4	16
<g/>
x	x	k?	x
<g/>
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
krytý	krytý	k2eAgInSc1d1	krytý
plechovou	plechový	k2eAgFnSc7d1	plechová
sedlovou	sedlový	k2eAgFnSc7d1	sedlová
střechou	střecha	k1gFnSc7	střecha
s	s	k7c7	s
vikýři	vikýř	k1gInPc7	vikýř
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
obdélným	obdélný	k2eAgInSc7d1	obdélný
presbytářem	presbytář	k1gInSc7	presbytář
s	s	k7c7	s
plechovou	plechový	k2eAgFnSc7d1	plechová
valbovou	valbový	k2eAgFnSc7d1	valbová
střechou	střecha	k1gFnSc7	střecha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
sanktusová	sanktusový	k2eAgFnSc1d1	sanktusová
zvonička	zvonička	k1gFnSc1	zvonička
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
patrová	patrový	k2eAgFnSc1d1	patrová
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
místnost	místnost	k1gFnSc1	místnost
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uchovávání	uchovávání	k1gNnSc3	uchovávání
rouch	roucho	k1gNnPc2	roucho
<g/>
,	,	kIx,	,
v	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
patře	patro	k1gNnSc6	patro
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
další	další	k2eAgInPc1d1	další
liturgické	liturgický	k2eAgInPc1d1	liturgický
předměty	předmět	k1gInPc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
hranolovou	hranolový	k2eAgFnSc7d1	hranolová
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
s	s	k7c7	s
barokním	barokní	k2eAgNnSc7d1	barokní
zdivem	zdivo	k1gNnSc7	zdivo
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
<g/>
;	;	kIx,	;
postranní	postranní	k2eAgInSc1d1	postranní
vchod	vchod	k1gInSc1	vchod
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věži	věž	k1gFnSc6	věž
jsou	být	k5eAaImIp3nP	být
čtvercové	čtvercový	k2eAgFnPc1d1	čtvercová
hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Interiér	interiér	k1gInSc1	interiér
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
tyrolským	tyrolský	k2eAgMnSc7d1	tyrolský
sochařem	sochař	k1gMnSc7	sochař
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Stuflesserem	Stuflesser	k1gInSc7	Stuflesser
ze	z	k7c2	z
St.	st.	kA	st.
Ulrichu	Ulrich	k1gMnSc6	Ulrich
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
oltářem	oltář	k1gInSc7	oltář
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Archanděla	archanděl	k1gMnSc2	archanděl
Michaela	Michael	k1gMnSc2	Michael
porážejícího	porážející	k2eAgMnSc2d1	porážející
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
jsou	být	k5eAaImIp3nP	být
obrazy	obraz	k1gInPc1	obraz
křížové	křížový	k2eAgFnSc2d1	křížová
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc4	varhany
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
varhanář	varhanář	k1gMnSc1	varhanář
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Langenauer	Langenauer	k1gMnSc1	Langenauer
<g/>
.	.	kIx.	.
</s>
<s>
Strop	strop	k1gInSc1	strop
je	být	k5eAaImIp3nS	být
rákosový	rákosový	k2eAgInSc1d1	rákosový
se	s	k7c7	s
štukovanými	štukovaný	k2eAgNnPc7d1	štukovaný
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
novorenesanční	novorenesanční	k2eAgNnSc1d1	novorenesanční
zařízení	zařízení	k1gNnSc1	zařízení
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
architekta	architekt	k1gMnSc2	architekt
Antona	Anton	k1gMnSc2	Anton
Viktora	Viktor	k1gMnSc2	Viktor
Barvitia	Barvitius	k1gMnSc2	Barvitius
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
umělci	umělec	k1gMnPc1	umělec
<g/>
:	:	kIx,	:
pražský	pražský	k2eAgMnSc1d1	pražský
malíř	malíř	k1gMnSc1	malíř
Zikmund	Zikmund	k1gMnSc1	Zikmund
Rudl	rudnout	k5eAaImAgMnS	rudnout
<g/>
,	,	kIx,	,
umělecký	umělecký	k2eAgMnSc1d1	umělecký
truhlář	truhlář	k1gMnSc1	truhlář
Lorenz	Lorenz	k1gMnSc1	Lorenz
a	a	k8xC	a
pražský	pražský	k2eAgMnSc1d1	pražský
sochař	sochař	k1gMnSc1	sochař
Bernard	Bernard	k1gMnSc1	Bernard
Ottau	Ottaus	k1gInSc2	Ottaus
Seeling	Seeling	k1gInSc1	Seeling
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnSc1	okolí
kostela	kostel	k1gInSc2	kostel
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c4	před
kostel	kostel	k1gInSc4	kostel
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1	rekonstruovaná
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
pocházející	pocházející	k2eAgFnSc7d1	pocházející
ze	z	k7c2	z
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
obce	obec	k1gFnSc2	obec
Svatobor	Svatobor	k1gInSc1	Svatobor
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
sochař	sochař	k1gMnSc1	sochař
Jan	Jan	k1gMnSc1	Jan
Pekař	Pekař	k1gMnSc1	Pekař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
Arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
pražská	pražský	k2eAgFnSc1d1	Pražská
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
378	[number]	k4	378
s.	s.	k?	s.
S.	S.	kA	S.
106	[number]	k4	106
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Škach	Škach	k1gMnSc1	Škach
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
Soukup	Soukup	k1gMnSc1	Soukup
Emil	Emil	k1gMnSc1	Emil
<g/>
;	;	kIx,	;
Vintrová	Vintrová	k1gFnSc1	Vintrová
Martina	Martina	k1gFnSc1	Martina
<g/>
,	,	kIx,	,
Dolanský	Dolanský	k1gMnSc1	Dolanský
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Biskupství	biskupství	k1gNnSc1	biskupství
plzeňské	plzeňská	k1gFnSc2	plzeňská
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
292	[number]	k4	292
s.	s.	k?	s.
S.	S.	kA	S.
57	[number]	k4	57
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIKAR	FIKAR	kA	FIKAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarská	karlovarský	k2eAgNnPc4d1	Karlovarské
předměstí	předměstí	k1gNnPc4	předměstí
2	[number]	k4	2
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
:	:	kIx,	:
autor	autor	k1gMnSc1	autor
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
198	[number]	k4	198
s.	s.	k?	s.
S.	S.	kA	S.
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Památky	památka	k1gFnPc1	památka
a	a	k8xC	a
příroda	příroda	k1gFnSc1	příroda
Karlovarska	Karlovarsko	k1gNnSc2	Karlovarsko
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
-	-	kIx~	-
Památkový	památkový	k2eAgInSc1d1	památkový
katalog	katalog	k1gInSc1	katalog
</s>
</p>
<p>
<s>
Diecéze	diecéze	k1gFnSc1	diecéze
plzeňská	plzeňská	k1gFnSc1	plzeňská
-	-	kIx~	-
katalog	katalog	k1gInSc1	katalog
</s>
</p>
