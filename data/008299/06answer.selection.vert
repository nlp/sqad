<s>
Kostel	kostel	k1gInSc4	kostel
Zjevení	zjevení	k1gNnSc2	zjevení
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
archanděla	archanděl	k1gMnSc2	archanděl
(	(	kIx(	(
<g/>
též	též	k9	též
nazýván	nazývat	k5eAaImNgInS	nazývat
svatého	svatý	k2eAgMnSc4d1	svatý
Michaela	Michael	k1gMnSc4	Michael
archanděla	archanděl	k1gMnSc4	archanděl
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Andělská	andělský	k2eAgFnSc1d1	Andělská
Hora	hora	k1gFnSc1	hora
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
