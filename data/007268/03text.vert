<s>
Dopamin	dopamin	k1gInSc1	dopamin
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
látka	látka	k1gFnSc1	látka
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
katecholaminů	katecholamin	k1gInPc2	katecholamin
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přirozeně	přirozeně	k6eAd1	přirozeně
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
většiny	většina	k1gFnSc2	většina
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Dopamin	dopamin	k1gInSc1	dopamin
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
neuropřenašeč	neuropřenašeč	k1gInSc1	neuropřenašeč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
jistých	jistý	k2eAgFnPc6d1	jistá
částech	část	k1gFnPc6	část
mozku	mozek	k1gInSc2	mozek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenos	přenos	k1gInSc1	přenos
impulsů	impuls	k1gInPc2	impuls
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
dopaminových	dopaminův	k2eAgFnPc2d1	dopaminův
drah	draha	k1gFnPc2	draha
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1	Parkinsonova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnSc2d1	jiná
poruchy	porucha	k1gFnSc2	porucha
dopaminového	dopaminový	k2eAgInSc2d1	dopaminový
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
se	se	k3xPyFc4	se
vznikem	vznik	k1gInSc7	vznik
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
či	či	k8xC	či
bipolární	bipolární	k2eAgFnSc2d1	bipolární
afektivní	afektivní	k2eAgFnSc2d1	afektivní
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Dopamin	dopamin	k1gInSc1	dopamin
funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
jako	jako	k9	jako
neurohormon	neurohormon	k1gNnSc1	neurohormon
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
v	v	k7c6	v
hypotalamu	hypotalamus	k1gInSc6	hypotalamus
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
též	též	k9	též
jako	jako	k9	jako
prolaktin	prolaktin	k1gInSc1	prolaktin
inhibující	inhibující	k2eAgInSc4d1	inhibující
hormon	hormon	k1gInSc4	hormon
(	(	kIx(	(
<g/>
PIH	piha	k1gFnPc2	piha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc1	množství
ho	on	k3xPp3gMnSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
také	také	k9	také
dřeň	dřeň	k1gFnSc1	dřeň
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
dopaminu	dopamin	k1gInSc2	dopamin
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
odhalil	odhalit	k5eAaPmAgMnS	odhalit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
švédský	švédský	k2eAgMnSc1d1	švédský
neurobiolog	neurobiolog	k1gMnSc1	neurobiolog
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
Arvid	Arvida	k1gFnPc2	Arvida
Carlsson	Carlsson	k1gMnSc1	Carlsson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Dopamin	dopamin	k1gInSc1	dopamin
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
poměrně	poměrně	k6eAd1	poměrně
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
spektrum	spektrum	k1gNnSc1	spektrum
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
centrální	centrální	k2eAgFnSc6d1	centrální
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
pět	pět	k4xCc4	pět
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
tzv.	tzv.	kA	tzv.
dopaminových	dopaminův	k2eAgInPc2d1	dopaminův
receptorů	receptor	k1gInPc2	receptor
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
dopamin	dopamin	k1gInSc1	dopamin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
nervových	nervový	k2eAgFnPc6d1	nervová
drahách	draha	k1gFnPc6	draha
uvolňován	uvolňovat	k5eAaImNgInS	uvolňovat
nervovými	nervový	k2eAgFnPc7d1	nervová
buňkami	buňka	k1gFnPc7	buňka
na	na	k7c6	na
synapsích	synapse	k1gFnPc6	synapse
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
příslušné	příslušný	k2eAgInPc4d1	příslušný
receptory	receptor	k1gInPc4	receptor
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenos	přenos	k1gInSc1	přenos
nervových	nervový	k2eAgInPc2d1	nervový
impulsů	impuls	k1gInPc2	impuls
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
nervové	nervový	k2eAgFnSc2d1	nervová
buňky	buňka	k1gFnSc2	buňka
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
šest	šest	k4xCc4	šest
nervových	nervový	k2eAgFnPc2d1	nervová
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
dopamin	dopamin	k1gInSc1	dopamin
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
dopaminergní	dopaminergní	k2eAgFnPc4d1	dopaminergní
dráhy	dráha	k1gFnPc4	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Produkující	produkující	k2eAgFnSc1d1	produkující
buňka	buňka	k1gFnSc1	buňka
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
dopamin	dopamin	k1gInSc4	dopamin
klasickým	klasický	k2eAgInSc7d1	klasický
způsobem	způsob	k1gInSc7	způsob
závislým	závislý	k2eAgInSc7d1	závislý
na	na	k7c6	na
vápenatých	vápenatý	k2eAgInPc6d1	vápenatý
iontech	ion	k1gInPc6	ion
<g/>
;	;	kIx,	;
dopamin	dopamin	k1gInSc1	dopamin
se	se	k3xPyFc4	se
rozlije	rozlít	k5eAaPmIp3nS	rozlít
do	do	k7c2	do
synaptické	synaptický	k2eAgFnSc2d1	synaptická
štěrbiny	štěrbina	k1gFnSc2	štěrbina
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
na	na	k7c4	na
dopaminové	dopaminový	k2eAgInPc4d1	dopaminový
receptory	receptor	k1gInPc4	receptor
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
vstřebáván	vstřebávat	k5eAaImNgInS	vstřebávat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
produkující	produkující	k2eAgFnSc2d1	produkující
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Uvolnění	uvolnění	k1gNnSc1	uvolnění
dopaminu	dopamin	k1gInSc2	dopamin
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
způsobit	způsobit	k5eAaPmF	způsobit
také	také	k9	také
dávka	dávka	k1gFnSc1	dávka
amfetaminů	amfetamin	k1gInPc2	amfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámější	známý	k2eAgFnSc1d3	nejznámější
funkce	funkce	k1gFnSc1	funkce
plní	plnit	k5eAaImIp3nS	plnit
dopamin	dopamin	k1gInSc4	dopamin
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
mezolimbické	mezolimbický	k2eAgFnSc6d1	mezolimbický
dopaminové	dopaminový	k2eAgFnSc6d1	dopaminová
dráze	dráha	k1gFnSc6	dráha
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
ze	z	k7c2	z
středního	střední	k2eAgInSc2d1	střední
mozku	mozek	k1gInSc2	mozek
přes	přes	k7c4	přes
nucleus	nucleus	k1gInSc4	nucleus
accumbens	accumbens	k6eAd1	accumbens
až	až	k9	až
do	do	k7c2	do
čelní	čelní	k2eAgFnSc2d1	čelní
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dráha	dráha	k1gFnSc1	dráha
hraje	hrát	k5eAaImIp3nS	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vzniku	vznik	k1gInSc6	vznik
motivace	motivace	k1gFnSc2	motivace
<g/>
,	,	kIx,	,
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
systému	systém	k1gInSc6	systém
potěšení	potěšení	k1gNnSc2	potěšení
a	a	k8xC	a
"	"	kIx"	"
<g/>
odměn	odměna	k1gFnPc2	odměna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vznik	vznik	k1gInSc4	vznik
příjemných	příjemný	k2eAgInPc2d1	příjemný
pocitů	pocit	k1gInPc2	pocit
buď	buď	k8xC	buď
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
události	událost	k1gFnPc4	událost
či	či	k8xC	či
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vlivem	vliv	k1gInSc7	vliv
požití	požití	k1gNnSc2	požití
jistých	jistý	k2eAgFnPc2d1	jistá
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
stimulačních	stimulační	k2eAgInPc6d1	stimulační
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kokainu	kokain	k1gInSc2	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Drogová	drogový	k2eAgFnSc1d1	drogová
závislost	závislost	k1gFnSc1	závislost
je	být	k5eAaImIp3nS	být
dávána	dávat	k5eAaImNgFnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
právě	právě	k6eAd1	právě
s	s	k7c7	s
touhou	touha	k1gFnSc7	touha
jedince	jedinec	k1gMnSc2	jedinec
po	po	k7c6	po
příjemných	příjemný	k2eAgInPc6d1	příjemný
pocitech	pocit	k1gInPc6	pocit
navozených	navozený	k2eAgMnPc2d1	navozený
dopaminem	dopamin	k1gInSc7	dopamin
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopaminergní	dopaminergní	k2eAgFnSc7d1	dopaminergní
drahou	draha	k1gFnSc7	draha
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c4	mezi
substantia	substantius	k1gMnSc4	substantius
nigra	nigr	k1gMnSc4	nigr
a	a	k8xC	a
oblastí	oblast	k1gFnSc7	oblast
zvanou	zvaný	k2eAgFnSc7d1	zvaná
striatum	striatum	k1gNnSc4	striatum
<g/>
.	.	kIx.	.
</s>
<s>
Substantia	Substantia	k1gFnSc1	Substantia
nigra	nigr	k1gMnSc2	nigr
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
bazálních	bazální	k2eAgFnPc2d1	bazální
ganglií	ganglie	k1gFnPc2	ganglie
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
plánování	plánování	k1gNnSc6	plánování
a	a	k8xC	a
programování	programování	k1gNnSc6	programování
pohybu	pohyb	k1gInSc2	pohyb
–	–	k?	–
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
myšlenka	myšlenka	k1gFnSc1	myšlenka
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
vůlí	vůle	k1gFnSc7	vůle
řízenou	řízený	k2eAgFnSc4d1	řízená
akci	akce	k1gFnSc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
propojena	propojen	k2eAgFnSc1d1	propojena
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
pohybovými	pohybový	k2eAgInPc7d1	pohybový
centry	centr	k1gInPc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Degenerace	degenerace	k1gFnSc1	degenerace
dopaminových	dopaminův	k2eAgNnPc2d1	dopaminův
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
substantia	substantius	k1gMnSc2	substantius
nigra	nigr	k1gMnSc2	nigr
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
Parkinsonovu	Parkinsonův	k2eAgFnSc4d1	Parkinsonova
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
právě	právě	k6eAd1	právě
poruchami	porucha	k1gFnPc7	porucha
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dopamin	dopamin	k1gInSc1	dopamin
uměle	uměle	k6eAd1	uměle
vpraven	vpravit	k5eAaPmNgInS	vpravit
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
vegetativní	vegetativní	k2eAgFnSc4d1	vegetativní
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
–	–	k?	–
například	například	k6eAd1	například
povede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zrychlení	zrychlení	k1gNnSc3	zrychlení
tepu	tep	k1gInSc3	tep
nebo	nebo	k8xC	nebo
zvýšení	zvýšení	k1gNnSc3	zvýšení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
dopamin	dopamin	k1gInSc1	dopamin
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
vstřebat	vstřebat	k5eAaPmF	vstřebat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
dopamin	dopamin	k1gInSc1	dopamin
podaný	podaný	k2eAgInSc1d1	podaný
jako	jako	k8xC	jako
lék	lék	k1gInSc1	lék
neovlivní	ovlivnit	k5eNaPmIp3nS	ovlivnit
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
hladiny	hladina	k1gFnSc2	hladina
dopaminu	dopamin	k1gInSc2	dopamin
např.	např.	kA	např.
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
pacientů	pacient	k1gMnPc2	pacient
trpících	trpící	k2eAgFnPc2d1	trpící
Parkinsonovou	Parkinsonový	k2eAgFnSc7d1	Parkinsonová
chorobou	choroba	k1gFnSc7	choroba
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přispět	přispět	k5eAaPmF	přispět
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
podáním	podání	k1gNnSc7	podání
prekurzoru	prekurzor	k1gInSc2	prekurzor
dopaminu	dopamin	k1gInSc2	dopamin
<g/>
.	.	kIx.	.
</s>
