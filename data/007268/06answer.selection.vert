<s>
Dopamin	dopamin	k1gInSc1	dopamin
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
neuropřenašeč	neuropřenašeč	k1gInSc1	neuropřenašeč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
jistých	jistý	k2eAgFnPc6d1	jistá
částech	část	k1gFnPc6	část
mozku	mozek	k1gInSc2	mozek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenos	přenos	k1gInSc1	přenos
impulsů	impuls	k1gInPc2	impuls
<g/>
.	.	kIx.	.
</s>
