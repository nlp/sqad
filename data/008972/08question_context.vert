<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Neumannová	Neumannová	k1gFnSc1	Neumannová
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1973	[number]	k4	1973
Písek	Písek	k1gInSc1	Písek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
česká	český	k2eAgFnSc1d1	Česká
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
cyklistka	cyklistka	k1gFnSc1	cyklistka
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
a	a	k8xC	a
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidentky	prezidentka	k1gFnSc2	prezidentka
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
severském	severský	k2eAgNnSc6d1	severské
lyžování	lyžování	k1gNnSc6	lyžování
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nechvalně	chvalně	k6eNd1	chvalně
známé	známý	k2eAgNnSc1d1	známé
vysokým	vysoký	k2eAgNnSc7d1	vysoké
zadlužením	zadlužení	k1gNnSc7	zadlužení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
byla	být	k5eAaImAgFnS	být
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
kritizována	kritizován	k2eAgFnSc1d1	kritizována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
obrany	obrana	k1gFnSc2	obrana
jako	jako	k8xC	jako
referentka	referentka	k1gFnSc1	referentka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
koncepci	koncepce	k1gFnSc3	koncepce
armádního	armádní	k2eAgInSc2d1	armádní
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
prosazovat	prosazovat	k5eAaImF	prosazovat
již	již	k6eAd1	již
od	od	k7c2	od
juniorského	juniorský	k2eAgInSc2d1	juniorský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
její	její	k3xOp3gMnSc1	její
talent	talent	k1gInSc4	talent
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
trenér	trenér	k1gMnSc1	trenér
československého	československý	k2eAgNnSc2d1	Československé
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
družstva	družstvo	k1gNnSc2	družstvo
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
katedry	katedra	k1gFnSc2	katedra
Jan	Jan	k1gMnSc1	Jan
Weisshäutel	Weisshäutel	k1gMnSc1	Weisshäutel
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ji	on	k3xPp3gFnSc4	on
i	i	k8xC	i
jejího	její	k3xOp3gMnSc2	její
trenéra	trenér	k1gMnSc2	trenér
Stanislava	Stanislav	k1gMnSc2	Stanislav
Frühaufa	Frühauf	k1gMnSc2	Frühauf
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
sportu	sport	k1gInSc2	sport
profesionálně	profesionálně	k6eAd1	profesionálně
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k6eAd1	také
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc4	první
stříbrnou	stříbrná	k1gFnSc4	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
Reit	Reitum	k1gNnPc2	Reitum
im	im	k?	im
Winklu	Winkl	k1gInSc6	Winkl
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
Vuokatti	Vuokatť	k1gFnSc6	Vuokatť
a	a	k8xC	a
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přidala	přidat	k5eAaPmAgNnP	přidat
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatá	k1gFnPc4	zlatá
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrná	k1gFnSc4	stříbrná
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
kariéry	kariéra	k1gFnSc2	kariéra
získala	získat	k5eAaPmAgFnS	získat
mnoho	mnoho	k4c4	mnoho
triumfů	triumf	k1gInPc2	triumf
<g/>
,	,	kIx,	,
medailí	medaile	k1gFnPc2	medaile
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mistryní	mistryně	k1gFnSc7	mistryně
světa	svět	k1gInSc2	svět
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sbírce	sbírka	k1gFnSc6	sbírka
i	i	k9	i
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Startovala	startovat	k5eAaBmAgFnS	startovat
již	již	k6eAd1	již
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
1992	[number]	k4	1992
a	a	k8xC	a
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
bronz	bronz	k1gInSc1	bronz
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
volným	volný	k2eAgInSc7d1	volný
stylem	styl	k1gInSc7	styl
a	a	k8xC	a
přidala	přidat	k5eAaPmAgFnS	přidat
i	i	k9	i
stříbro	stříbro	k1gNnSc4	stříbro
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnSc1	Lake
City	City	k1gFnSc1	City
získala	získat	k5eAaPmAgFnS	získat
dvakrát	dvakrát	k6eAd1	dvakrát
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
olympiádě	olympiáda	k1gFnSc6	olympiáda
načas	načas	k6eAd1	načas
kvůli	kvůli	k7c3	kvůli
těhotenství	těhotenství	k1gNnSc3	těhotenství
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
Lucie	Lucie	k1gFnSc2	Lucie
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
narodila	narodit	k5eAaPmAgFnS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
přerušila	přerušit	k5eAaPmAgFnS	přerušit
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
obdivuhodně	obdivuhodně	k6eAd1	obdivuhodně
<g/>
.	.	kIx.	.
</s>
<s>
Půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Neumannová	Neumannová	k1gFnSc1	Neumannová
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
33	[number]	k4	33
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
2006	[number]	k4	2006
stala	stát	k5eAaPmAgFnS	stát
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
vítězkou	vítězka	k1gFnSc7	vítězka
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
volně	volně	k6eAd1	volně
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
stříbro	stříbro	k1gNnSc4	stříbro
ve	v	k7c6	v
skiatlonu	skiatlon	k1gInSc6	skiatlon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zpětném	zpětný	k2eAgNnSc6d1	zpětné
testování	testování	k1gNnSc6	testování
vzorků	vzorek	k1gInPc2	vzorek
měla	mít	k5eAaImAgFnS	mít
vítězka	vítězka	k1gFnSc1	vítězka
turínského	turínský	k2eAgInSc2d1	turínský
skiatlonu	skiatlon	k1gInSc2	skiatlon
Kristina	Kristin	k2eAgNnSc2d1	Kristino
Šmigun-Vähi	Šmigun-Vähi	k1gNnSc2	Šmigun-Vähi
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
dopingové	dopingový	k2eAgInPc4d1	dopingový
nálezy	nález	k1gInPc4	nález
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nS	by
po	po	k7c6	po
případném	případný	k2eAgNnSc6d1	případné
přerozdělování	přerozdělování	k1gNnSc6	přerozdělování
cenných	cenný	k2eAgInPc2d1	cenný
kovů	kov	k1gInPc2	kov
měla	mít	k5eAaImAgFnS	mít
Neumannová	Neumannová	k1gFnSc1	Neumannová
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc4	místo
stříbrné	stříbrná	k1gFnSc3	stříbrná
druhou	druhý	k4xOgFnSc4	druhý
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
během	během	k7c2	během
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
na	na	k7c6	na
letní	letní	k2eAgFnSc6d1	letní
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
soutěže	soutěž	k1gFnPc4	soutěž
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
na	na	k7c4	na
31,9	[number]	k4	31,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Skončila	skončit	k5eAaPmAgFnS	skončit
osmnáctá	osmnáctý	k4xOgFnSc1	osmnáctý
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
první	první	k4xOgFnSc7	první
českou	český	k2eAgFnSc7d1	Česká
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
letních	letní	k2eAgFnPc2d1	letní
i	i	k8xC	i
zimních	zimní	k2eAgFnPc2d1	zimní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
<g/>
Zásluhou	zásluhou	k7c2	zásluhou
vynikajících	vynikající	k2eAgInPc2d1	vynikající
sportovních	sportovní	k2eAgInPc2d1	sportovní
výsledků	výsledek	k1gInPc2	výsledek
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
umisťovala	umisťovat	k5eAaImAgFnS	umisťovat
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
o	o	k7c6	o
pořadí	pořadí	k1gNnSc6	pořadí
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
sportovní	sportovní	k2eAgMnSc1d1	sportovní
novináři	novinář	k1gMnPc1	novinář
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
Klubu	klub	k1gInSc2	klub
sportovních	sportovní	k2eAgMnPc2d1	sportovní
novinářů	novinář	k1gMnPc2	novinář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
sportovců	sportovec	k1gMnPc2	sportovec
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obsadila	obsadit	k5eAaPmAgFnS	obsadit
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
elitou	elita	k1gFnSc7	elita
chyběla	chybět	k5eAaImAgFnS	chybět
jen	jen	k9	jen
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
na	na	k7c4	na
MS	MS	kA	MS
2005	[number]	k4	2005
obsadila	obsadit	k5eAaPmAgFnS	obsadit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
druhá	druhý	k4xOgNnPc4	druhý
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
medaile	medaile	k1gFnPc4	medaile
ze	z	k7c2	z
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
2002	[number]	k4	2002
jí	on	k3xPp3gFnSc3	on
vynesly	vynést	k5eAaPmAgInP	vynést
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
anketě	anketa	k1gFnSc6	anketa
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
–	–	k?	–
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
sedmkrát	sedmkrát	k6eAd1	sedmkrát
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Král	Král	k1gMnSc1	Král
bílé	bílý	k2eAgFnSc2d1	bílá
stopy	stopa	k1gFnSc2	stopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
sportovních	sportovní	k2eAgInPc6d1	sportovní
úspěších	úspěch	k1gInPc6	úspěch
měl	mít	k5eAaImAgMnS	mít
její	její	k3xOp3gNnSc4	její
manažer	manažer	k1gMnSc1	manažer
Josef	Josef	k1gMnSc1	Josef
Jindra	Jindra	k1gMnSc1	Jindra
(	(	kIx(	(
<g/>
o	o	k7c4	o
15	[number]	k4	15
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc1d2	starší
než	než	k8xS	než
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zajistil	zajistit	k5eAaPmAgInS	zajistit
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pronájem	pronájem	k1gInSc4	pronájem
speciálního	speciální	k2eAgInSc2d1	speciální
stanu	stan	k1gInSc2	stan
simulujícího	simulující	k2eAgInSc2d1	simulující
vysokohorské	vysokohorský	k2eAgNnSc4d1	vysokohorské
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
angažování	angažování	k1gNnSc4	angažování
švédského	švédský	k2eAgMnSc2d1	švédský
servismana	servisman	k1gMnSc2	servisman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
potkali	potkat	k5eAaPmAgMnP	potkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Neumannová	Neumannová	k1gFnSc1	Neumannová
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
hledala	hledat	k5eAaImAgFnS	hledat
nového	nový	k2eAgMnSc4d1	nový
manažera	manažer	k1gMnSc4	manažer
a	a	k8xC	a
volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaPmAgFnS	padnout
právě	právě	k9	právě
na	na	k7c4	na
Jindru	Jindra	k1gFnSc4	Jindra
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
neuvedla	uvést	k5eNaPmAgFnS	uvést
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
otcem	otec	k1gMnSc7	otec
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
Lucie	Lucie	k1gFnSc2	Lucie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
veřejným	veřejný	k2eAgNnSc7d1	veřejné
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	on	k3xPp3gNnSc7	on
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
Jindra	Jindra	k1gFnSc1	Jindra
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
její	její	k3xOp3gFnSc2	její
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
sportovní	sportovní	k2eAgMnSc1d1	sportovní
novináři	novinář	k1gMnPc1	novinář
zachovávali	zachovávat	k5eAaImAgMnP	zachovávat
nepsanou	nepsaný	k2eAgFnSc4d1	nepsaná
gentlemanskou	gentlemanský	k2eAgFnSc4d1	gentlemanská
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
o	o	k7c6	o
Jindrově	Jindrův	k2eAgNnSc6d1	Jindrovo
otcovství	otcovství	k1gNnSc6	otcovství
se	se	k3xPyFc4	se
nezmiňovali	zmiňovat	k5eNaImAgMnP	zmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
Jindrou	Jindra	k1gMnSc7	Jindra
spojuje	spojovat	k5eAaImIp3nS	spojovat
i	i	k9	i
podnikání	podnikání	k1gNnSc6	podnikání
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
spolumajiteli	spolumajitel	k1gMnPc7	spolumajitel
luxusního	luxusní	k2eAgInSc2d1	luxusní
penzionu	penzion	k1gInSc2	penzion
na	na	k7c6	na
Churáňově	Churáňův	k2eAgFnSc6d1	Churáňova
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
údajně	údajně	k6eAd1	údajně
stála	stát	k5eAaImAgFnS	stát
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
podílníky	podílník	k1gMnPc4	podílník
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jindra	Jindra	k1gMnSc1	Jindra
měl	mít	k5eAaImAgMnS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
překvapivé	překvapivý	k2eAgNnSc4d1	překvapivé
jmenování	jmenování	k1gNnSc4	jmenování
Neumannové	Neumannová	k1gFnSc2	Neumannová
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidentky	prezidentka	k1gFnSc2	prezidentka
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
severském	severský	k2eAgNnSc6d1	severské
lyžování	lyžování	k1gNnSc6	lyžování
2009	[number]	k4	2009
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
její	její	k3xOp3gFnSc2	její
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
jí	on	k3xPp3gFnSc7	on
vytýkají	vytýkat	k5eAaImIp3nP	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
větší	veliký	k2eAgFnPc4d2	veliký
manažerské	manažerský	k2eAgFnPc4d1	manažerská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
že	že	k8xS	že
výbor	výbor	k1gInSc1	výbor
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
řídí	řídit	k5eAaImIp3nS	řídit
Jindra	Jindra	k1gFnSc1	Jindra
<g/>
.	.	kIx.	.
</s>

