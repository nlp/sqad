<s>
Proto	proto	k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
v	v	k7c6
nemocnicích	nemocnice	k1gFnPc6
převážně	převážně	k6eAd1
zelené	zelený	k2eAgFnPc1d1
<g/>
,	,	kIx,
modré	modrý	k2eAgFnPc1d1
nebo	nebo	k8xC
žluté	žlutý	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
<g/>
,	,	kIx,
protože	protože	k8xS
ty	ten	k3xDgMnPc4
navozují	navozovat	k5eAaImIp3nP
u	u	k7c2
člověka	člověk	k1gMnSc2
pocity	pocit	k1gInPc4
klidu	klid	k1gInSc2
a	a	k8xC
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>