<s>
Národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
list	list	k1gInSc1	list
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
horního	horní	k2eAgInSc2d1	horní
červeného	červený	k2eAgInSc2d1	červený
pruhu	pruh	k1gInSc2	pruh
zabírajícího	zabírající	k2eAgInSc2d1	zabírající
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
spodního	spodní	k2eAgInSc2d1	spodní
zeleného	zelený	k2eAgInSc2d1	zelený
pruhu	pruh	k1gInSc2	pruh
zabírajícího	zabírající	k2eAgInSc2d1	zabírající
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
výšky	výška	k1gFnSc2	výška
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
