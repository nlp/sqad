<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1995	[number]	k4	1995
na	na	k7c6	na
základě	základ	k1gInSc6	základ
referenda	referendum	k1gNnSc2	referendum
běloruských	běloruský	k2eAgMnPc2d1	běloruský
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
mírně	mírně	k6eAd1	mírně
upravena	upravit	k5eAaPmNgFnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
nahradila	nahradit	k5eAaPmAgFnS	nahradit
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
užívanou	užívaný	k2eAgFnSc7d1	užívaná
Běloruskou	běloruský	k2eAgFnSc7d1	Běloruská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
stalo	stát	k5eAaPmAgNnS	stát
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Referendum	referendum	k1gNnSc1	referendum
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
kvůli	kvůli	k7c3	kvůli
použití	použití	k1gNnSc3	použití
staré	starý	k2eAgFnSc2d1	stará
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
veteránům	veterán	k1gMnPc3	veterán
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
modifikací	modifikace	k1gFnSc7	modifikace
vlajky	vlajka	k1gFnSc2	vlajka
BSSR	BSSR	kA	BSSR
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc4	některý
zájmové	zájmový	k2eAgFnPc4d1	zájmová
skupiny	skupina	k1gFnPc4	skupina
nadále	nadále	k6eAd1	nadále
používají	používat	k5eAaImIp3nP	používat
původní	původní	k2eAgFnSc4d1	původní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
její	její	k3xOp3gNnSc1	její
používání	používání	k1gNnSc1	používání
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
prezidentem	prezident	k1gMnSc7	prezident
Lukašenkem	Lukašenek	k1gMnSc7	Lukašenek
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
k	k	k7c3	k
protestu	protest	k1gInSc3	protest
proti	proti	k7c3	proti
současné	současný	k2eAgFnSc3d1	současná
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
nesplňovalo	splňovat	k5eNaImAgNnS	splňovat
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
podobě	podoba	k1gFnSc6	podoba
vlajky	vlajka	k1gFnPc1	vlajka
základní	základní	k2eAgInPc4d1	základní
demokratické	demokratický	k2eAgInPc4d1	demokratický
principy	princip	k1gInPc4	princip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
list	list	k1gInSc1	list
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
horního	horní	k2eAgInSc2d1	horní
červeného	červený	k2eAgInSc2d1	červený
pruhu	pruh	k1gInSc2	pruh
zabírajícího	zabírající	k2eAgInSc2d1	zabírající
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
spodního	spodní	k2eAgInSc2d1	spodní
zeleného	zelený	k2eAgInSc2d1	zelený
pruhu	pruh	k1gInSc2	pruh
zabírajícího	zabírající	k2eAgInSc2d1	zabírající
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
výšky	výška	k1gFnSc2	výška
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žerďové	žerďové	k2eAgFnSc6d1	žerďové
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
bílý	bílý	k2eAgInSc1d1	bílý
běloruský	běloruský	k2eAgInSc1d1	běloruský
dekorační	dekorační	k2eAgInSc1d1	dekorační
pruh	pruh	k1gInSc1	pruh
zabírající	zabírající	k2eAgFnSc4d1	zabírající
jednu	jeden	k4xCgFnSc4	jeden
devítinu	devítina	k1gFnSc4	devítina
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
šířky	šířka	k1gFnSc2	šířka
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc1d1	historická
běloruské	běloruský	k2eAgFnPc1d1	Běloruská
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
běloruských	běloruský	k2eAgFnPc2d1	Běloruská
oblastí	oblast	k1gFnPc2	oblast
==	==	k?	==
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
6	[number]	k4	6
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Minsk	Minsk	k1gInSc1	Minsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
</s>
</p>
<p>
<s>
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
národních	národní	k2eAgInPc6d1	národní
symbolech	symbol	k1gInPc6	symbol
na	na	k7c6	na
webu	web	k1gInSc6	web
prezidenta	prezident	k1gMnSc2	prezident
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
<g/>
)	)	kIx)	)
</s>
</p>
