<s>
Torosaurus	Torosaurus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Děrovaný	děrovaný	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
často	často	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
nesprávně	správně	k6eNd1
překládáno	překládán	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
býčí	býčí	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
velkého	velký	k2eAgMnSc2d1
ceratopsidního	ceratopsidní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
žijícího	žijící	k2eAgMnSc2d1
na	na	k7c6
konci	konec	k1gInSc6
křídového	křídový	k2eAgNnSc2d1
období	období	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
západu	západ	k1gInSc2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>