<s>
Torosaurus	Torosaurus	k1gMnSc1
</s>
<s>
TorosaurusStratigrafický	TorosaurusStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Svrchní	svrchní	k2eAgFnSc1d1
křída	křída	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
68	#num#	k4
až	až	k8xS
66	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
Rekonstrukce	rekonstrukce	k1gFnSc1
torosaura	torosaura	k1gFnSc1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Sauropsida	Sauropsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
Dinosauria	Dinosaurium	k1gNnPc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
ptakopánví	ptakopánví	k1gNnSc1
(	(	kIx(
<g/>
Ornithischia	Ornithischia	k1gFnSc1
<g/>
)	)	kIx)
Nadčeleď	nadčeleď	k1gFnSc1
</s>
<s>
Ceratopsia	Ceratopsia	k1gFnSc1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Ceratopsidae	Ceratopsidae	k1gFnSc1
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
Ceratopsinae	Ceratopsinae	k6eAd1
Tribus	Tribus	k1gInSc1
</s>
<s>
Triceratopsini	Triceratopsin	k2eAgMnPc1d1
Rod	rod	k1gInSc4
</s>
<s>
TorosaurusMarsh	TorosaurusMarsh	k1gMnSc1
<g/>
,	,	kIx,
1891	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Torosaurus	Torosaurus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Děrovaný	děrovaný	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
často	často	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
nesprávně	správně	k6eNd1
překládáno	překládán	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
býčí	býčí	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
velkého	velký	k2eAgMnSc2d1
ceratopsidního	ceratopsidní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
žijícího	žijící	k2eAgMnSc2d1
na	na	k7c6
konci	konec	k1gInSc6
křídového	křídový	k2eAgNnSc2d1
období	období	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
západu	západ	k1gInSc2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
získal	získat	k5eAaPmAgMnS
tento	tento	k3xDgMnSc1
rohatý	rohatý	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
podle	podle	k7c2
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgMnS
výrazná	výrazný	k2eAgFnSc1d1
„	„	k?
<g/>
okna	okno	k1gNnSc2
<g/>
“	“	k?
v	v	k7c6
límcové	límcový	k2eAgFnSc6d1
části	část	k1gFnSc6
lebky	lebka	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
třeba	třeba	k6eAd1
od	od	k7c2
známého	známý	k2eAgInSc2d1
rodu	rod	k1gInSc2
Triceratops	Triceratopsa	k1gFnPc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
však	však	k9
podle	podle	k7c2
nové	nový	k2eAgFnSc2d1
studie	studie	k1gFnSc2
možná	možná	k9
fakticky	fakticky	k6eAd1
náleží	náležet	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
a	a	k8xC
velikost	velikost	k1gFnSc1
</s>
<s>
Torosaurus	Torosaurus	k1gInSc1
byl	být	k5eAaImAgInS
popsán	popsat	k5eAaPmNgInS
O.	O.	kA
C.	C.	kA
Marshem	Marsh	k1gInSc7
roku	rok	k1gInSc2
1891	#num#	k4
podle	podle	k7c2
fosiliní	fosilinit	k5eAaPmIp3nP
objevených	objevený	k2eAgFnPc2d1
ve	v	k7c6
Wyomingu	Wyoming	k1gInSc6
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
(	(	kIx(
<g/>
typový	typový	k2eAgInSc1d1
<g/>
)	)	kIx)
druh	druh	k1gInSc1
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
Torosaurus	Torosaurus	k1gInSc1
latus	latus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
mohutný	mohutný	k2eAgMnSc1d1
čtyřnohý	čtyřnohý	k2eAgMnSc1d1
býložravec	býložravec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
dosahoval	dosahovat	k5eAaImAgMnS
délky	délka	k1gFnPc4
asi	asi	k9
8	#num#	k4
až	až	k9
9	#num#	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
zhruba	zhruba	k6eAd1
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
4	#num#	k4
až	až	k9
6	#num#	k4
(	(	kIx(
<g/>
možná	možná	k9
ale	ale	k9
až	až	k9
9,7	9,7	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
největší	veliký	k2eAgMnPc4d3
známé	známý	k2eAgMnPc4d1
rohaté	rohatý	k1gMnPc4
dinosaury	dinosaurus	k1gMnPc4
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
z	z	k7c2
nejlépe	dobře	k6eAd3
zachovaných	zachovaný	k2eAgInPc2d1
exemplářů	exemplář	k1gInPc2
torosaura	torosaur	k1gMnSc2
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
2017	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
Colorada	Colorado	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podobně	podobně	k6eAd1
velkým	velký	k2eAgInSc7d1
druhem	druh	k1gInSc7
byl	být	k5eAaImAgInS
také	také	k9
jižněji	jižně	k6eAd2
žijící	žijící	k2eAgMnSc1d1
Torosaurus	Torosaurus	k1gMnSc1
utahensis	utahensis	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
fosilie	fosilie	k1gFnPc1
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgFnPc1d1
zejména	zejména	k9
ze	z	k7c2
sedimentů	sediment	k1gInPc2
souvrství	souvrství	k1gNnSc2
Ojo	Ojo	k1gFnSc2
Alamo	Alama	k1gFnSc5
(	(	kIx(
<g/>
a	a	k8xC
zřejmě	zřejmě	k6eAd1
i	i	k9
souvrství	souvrství	k1gNnSc1
McRae	McRa	k1gFnSc2
<g/>
,	,	kIx,
souvrství	souvrství	k1gNnSc1
Javelina	Javelin	k2eAgNnSc2d1
a	a	k8xC
souvrství	souvrství	k1gNnSc2
North	North	k1gMnSc1
Horn	Horn	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
Nového	Nového	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
Texasu	Texas	k1gInSc2
a	a	k8xC
Utahu	Utah	k1gInSc2
(	(	kIx(
<g/>
resp.	resp.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obřím	obří	k2eAgMnSc7d1
jedincem	jedinec	k1gMnSc7
torosaura	torosaur	k1gMnSc2
(	(	kIx(
<g/>
spíše	spíše	k9
ale	ale	k9
triceratopse	triceratopse	k6eAd1
<g/>
)	)	kIx)
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
také	také	k9
původce	původce	k1gMnSc4
fosilní	fosilní	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
<g/>
,	,	kIx,
popsané	popsaný	k2eAgFnPc4d1
formálně	formálně	k6eAd1
jako	jako	k8xS,k8xC
ichnodruh	ichnodruh	k1gInSc1
Ceratopsipes	Ceratopsipesa	k1gFnPc2
goldenensis	goldenensis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
80	#num#	k4
cm	cm	kA
široká	široký	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
byla	být	k5eAaImAgFnS
popsána	popsán	k2eAgFnSc1d1
roku	rok	k1gInSc2
1995	#num#	k4
ze	z	k7c2
sedimentů	sediment	k1gInPc2
souvrství	souvrství	k1gNnSc2
Laramie	Laramie	k1gFnSc2
v	v	k7c6
Coloradu	Colorado	k1gNnSc6
a	a	k8xC
mohla	moct	k5eAaImAgFnS
patřit	patřit	k5eAaImF
velkému	velký	k2eAgMnSc3d1
jedinci	jedinec	k1gMnSc3
o	o	k7c6
délce	délka	k1gFnSc6
až	až	k9
12	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
přes	přes	k7c4
12	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velikost	velikost	k1gFnSc1
lebky	lebka	k1gFnSc2
</s>
<s>
Torosaurus	Torosaurus	k1gInSc1
disponoval	disponovat	k5eAaBmAgInS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejdelších	dlouhý	k2eAgFnPc2d3
letbek	letbka	k1gFnPc2
mezi	mezi	k7c7
všemi	všecek	k3xTgMnPc7
rohatými	rohatý	k2eAgMnPc7d1
dinosaury	dinosaurus	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
suchozemskými	suchozemský	k2eAgMnPc7d1
obratlovci	obratlovec	k1gMnPc7
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
instituce	instituce	k1gFnSc2
Museum	museum	k1gNnSc1
of	of	k?
the	the	k?
Rockies	Rockies	k1gMnSc1
v	v	k7c6
Bozemanu	Bozeman	k1gInSc6
(	(	kIx(
<g/>
Montana	Montana	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
uloženy	uložit	k5eAaPmNgFnP
dvě	dva	k4xCgFnPc1
rekonstruované	rekonstruovaný	k2eAgFnPc1d1
lebky	lebka	k1gFnPc1
druhu	druh	k1gInSc2
Torosaurus	Torosaurus	k1gMnSc1
latus	latus	k1gMnSc1
ze	z	k7c2
souvrství	souvrství	k1gNnSc2
Hell	Hell	k1gMnSc1
Creek	Creek	k1gMnSc1
<g/>
,	,	kIx,
ktratší	ktratší	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
252	#num#	k4
cm	cm	kA
a	a	k8xC
delší	dlouhý	k2eAgInPc1d2
277	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
nejdelší	dlouhý	k2eAgFnSc4d3
známou	známý	k2eAgFnSc4d1
dinosauří	dinosauří	k2eAgFnSc4d1
lebku	lebka	k1gFnSc4
vůbec	vůbec	k9
<g/>
,	,	kIx,
pokud	pokud	k8xS
bereme	brát	k5eAaImIp1nP
v	v	k7c4
potaz	potaz	k1gInSc4
pouze	pouze	k6eAd1
relativně	relativně	k6eAd1
dobře	dobře	k6eAd1
dochované	dochovaný	k2eAgFnPc4d1
fosilie	fosilie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
ceratopsidem	ceratopsid	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
mohl	moct	k5eAaImAgInS
mít	mít	k5eAaImF
ještě	ještě	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
lebku	lebka	k1gFnSc4
(	(	kIx(
<g/>
o	o	k7c6
délce	délka	k1gFnSc6
zhruba	zhruba	k6eAd1
rovných	rovný	k2eAgInPc2d1
3	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
mírně	mírně	k6eAd1
geologicky	geologicky	k6eAd1
starší	starý	k2eAgInSc4d2
druh	druh	k1gInSc4
Eotriceratops	Eotriceratopsa	k1gFnPc2
xerinsularis	xerinsularis	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
fosilie	fosilie	k1gFnPc1
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
v	v	k7c6
kanadské	kanadský	k2eAgFnSc6d1
Albertě	Alberta	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pouze	pouze	k6eAd1
dospělý	dospělý	k2eAgInSc1d1
triceratops	triceratops	k1gInSc1
<g/>
?	?	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
vystoupili	vystoupit	k5eAaPmAgMnP
američtí	americký	k2eAgMnPc1d1
paleontologové	paleontolog	k1gMnPc1
John	John	k1gMnSc1
Scannella	Scannella	k1gMnSc1
a	a	k8xC
Jack	Jack	k1gMnSc1
Horner	Horner	k1gMnSc1
s	s	k7c7
domněnkou	domněnka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
torosaurus	torosaurus	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
možná	možná	k9
jen	jen	k6eAd1
odrostlým	odrostlý	k2eAgMnSc7d1
dospělcem	dospělec	k1gMnSc7
samce	samec	k1gInSc2
triceratopse	triceratopse	k1gFnSc1
s	s	k7c7
plně	plně	k6eAd1
vyvinutým	vyvinutý	k2eAgInSc7d1
krčním	krční	k2eAgInSc7d1
límcem	límec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
v	v	k7c6
průběhu	průběh	k1gInSc6
vývoje	vývoj	k1gInSc2
postupně	postupně	k6eAd1
měnit	měnit	k5eAaImF
a	a	k8xC
plasticky	plasticky	k6eAd1
"	"	kIx"
<g/>
protahovat	protahovat	k5eAaImF
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
domněnku	domněnka	k1gFnSc4
pak	pak	k6eAd1
dále	daleko	k6eAd2
potvrzovala	potvrzovat	k5eAaImAgFnS
odborná	odborný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
byla	být	k5eAaImAgFnS
lebeční	lebeční	k2eAgFnSc1d1
kost	kost	k1gFnSc1
těchto	tento	k3xDgMnPc2
dinosaurů	dinosaurus	k1gMnPc2
velmi	velmi	k6eAd1
plastická	plastický	k2eAgFnSc1d1
a	a	k8xC
morfologicky	morfologicky	k6eAd1
se	se	k3xPyFc4
měnila	měnit	k5eAaImAgFnS
až	až	k9
do	do	k7c2
pozdního	pozdní	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
objevila	objevit	k5eAaPmAgFnS
charakteristická	charakteristický	k2eAgNnPc4d1
„	„	k?
<g/>
okna	okno	k1gNnPc4
<g/>
"	"	kIx"
(	(	kIx(
<g/>
výrazné	výrazný	k2eAgInPc4d1
otvory	otvor	k1gInPc4
<g/>
)	)	kIx)
torosaura	torosaura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc4
závěr	závěr	k1gInSc4
potvrzují	potvrzovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
přestože	přestože	k8xS
vědecký	vědecký	k2eAgInSc1d1
konsenzus	konsenzus	k1gInSc1
zatím	zatím	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
spíše	spíše	k9
převažuje	převažovat	k5eAaImIp3nS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Torosaurus	Torosaurus	k1gInSc1
je	být	k5eAaImIp3nS
skutečným	skutečný	k2eAgInSc7d1
samostatným	samostatný	k2eAgInSc7d1
rodem	rod	k1gInSc7
ceratopsida	ceratopsid	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
literární	literární	k2eAgFnSc6d1
fikci	fikce	k1gFnSc6
</s>
<s>
Torosaurus	Torosaurus	k1gMnSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
některých	některý	k3yIgFnPc6
sci-fi	sci-fi	k1gFnPc6
příbězích	příběh	k1gInPc6
o	o	k7c4
cestování	cestování	k1gNnSc4
časem	časem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
české	český	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
knize	kniha	k1gFnSc6
Poslední	poslední	k2eAgInPc4d1
dny	den	k1gInPc4
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
stádo	stádo	k1gNnSc4
těchto	tento	k3xDgMnPc2
mohutných	mohutný	k2eAgMnPc2d1
ceratopsidů	ceratopsid	k1gMnPc2
ohrozí	ohrozit	k5eAaPmIp3nS
životy	život	k1gInPc4
hlavních	hlavní	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
podrážděnými	podrážděný	k2eAgInPc7d1
torosaury	torosaur	k1gInPc7
téměř	téměř	k6eAd1
ušlapáni	ušlapán	k2eAgMnPc1d1
(	(	kIx(
<g/>
se	se	k3xPyFc4
štěstím	štěstí	k1gNnSc7
ale	ale	k8xC
přežijí	přežít	k5eAaPmIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legenda	legenda	k1gFnSc1
jménem	jméno	k1gNnSc7
Tyrannosaurus	Tyrannosaurus	k1gInSc1
rex	rex	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Mervart	Mervart	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7465	#num#	k4
<g/>
-	-	kIx~
<g/>
369	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
278	#num#	k4
<g/>
-	-	kIx~
<g/>
279	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Holtz	Holtz	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
R.	R.	kA
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
<g/>
;	;	kIx,
Rey	Rea	k1gFnSc2
<g/>
,	,	kIx,
Luis	Luisa	k1gFnPc2
V.	V.	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Most	most	k1gInSc4
Complete	Comple	k1gNnSc2
<g/>
,	,	kIx,
Up-to-Date	Up-to-Dat	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnPc1
for	forum	k1gNnPc2
Dinosaur	Dinosaura	k1gFnPc2
Lovers	Loversa	k1gFnPc2
of	of	k?
All	All	k1gFnSc2
Ages	Agesa	k1gFnPc2
(	(	kIx(
<g/>
Aktualizovaný	aktualizovaný	k2eAgInSc1d1
internetový	internetový	k2eAgInSc1d1
dodatek	dodatek	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
52	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
375	#num#	k4
<g/>
-	-	kIx~
<g/>
82419	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Roger	Rogero	k1gNnPc2
B.	B.	kA
J.	J.	kA
Benson	Benson	k1gMnSc1
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
T.	T.	kA
Carrano	Carrana	k1gFnSc5
&	&	k?
Nicolás	Nicolás	k1gInSc1
Campione	Campion	k1gInSc5
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cope	cop	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
rule	rula	k1gFnSc6
and	and	k?
the	the	k?
adaptive	adaptiv	k1gInSc5
landscape	landscapat	k5eAaPmIp3nS
of	of	k?
dinosaur	dinosaur	k1gMnSc1
body	bod	k1gInPc4
size	size	k6eAd1
evolution	evolution	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeontology	Palaeontolog	k1gMnPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
pala	pal	k1gInSc2
<g/>
.12329	.12329	k4
http://onlinelibrary.wiley.com/doi/10.1111/pala.12329/full	http://onlinelibrary.wiley.com/doi/10.1111/pala.12329/full	k1gMnSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.dmns.org	www.dmns.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://dinosaurusblog.com/2020/04/16/posledni-z-velociraptoru/	https://dinosaurusblog.com/2020/04/16/posledni-z-velociraptoru/	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopa	stopa	k1gFnSc1
největšího	veliký	k2eAgMnSc2d3
rohatého	rohatý	k1gMnSc2
dinosaura	dinosaurus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
s	s	k7c7
nejdelší	dlouhý	k2eAgFnSc7d3
lebkou	lebka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Scannella	Scannella	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
and	and	k?
Horner	Horner	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
R.	R.	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Torosaurus	Torosaurus	k1gMnSc1
Marsh	Marsh	k1gMnSc1
<g/>
,	,	kIx,
1891	#num#	k4
<g/>
,	,	kIx,
is	is	k?
Triceratops	Triceratops	k1gInSc1
Marsh	Marsha	k1gFnPc2
<g/>
,	,	kIx,
1889	#num#	k4
(	(	kIx(
<g/>
Ceratopsidae	Ceratopsida	k1gInSc2
<g/>
:	:	kIx,
Chasmosaurinae	Chasmosaurinae	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
:	:	kIx,
synonymy	synonymum	k1gNnPc7
through	through	k1gInSc1
ontogeny	ontogen	k1gInPc1
.	.	kIx.
<g/>
"	"	kIx"
Journal	Journal	k1gFnSc1
of	of	k?
Vertebrate	Vertebrat	k1gInSc5
Paleontology	paleontolog	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1157	#num#	k4
-	-	kIx~
1168	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2724634.2010	2724634.2010	k4
<g/>
.483632	.483632	k4
<g/>
↑	↑	k?
Study	stud	k1gInPc4
finds	finds	k6eAd1
Triceratops	Triceratopsa	k1gFnPc2
<g/>
,	,	kIx,
Torosaurus	Torosaurus	k1gMnSc1
were	wer	k1gFnSc2
different	different	k1gMnSc1
stages	stages	k1gMnSc1
of	of	k?
one	one	k?
dinosaur	dinosaur	k1gMnSc1
<g/>
↑	↑	k?
http://www.sciencedaily.com/releases/2010/07/100714131244.htm	http://www.sciencedaily.com/releases/2010/07/100714131244.htm	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolosální	kolosální	k2eAgInSc1d1
ceratopsid	ceratopsid	k1gInSc1
Torosaurus	Torosaurus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
dny	den	k1gInPc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
nakl	naknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radioservis	Radioservis	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-87530-74-0	978-80-87530-74-0	k4
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
172	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Sullivan	Sullivan	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
M.	M.	kA
<g/>
,	,	kIx,
A.	A.	kA
C.	C.	kA
Boere	Boer	k1gMnSc5
<g/>
,	,	kIx,
and	and	k?
S.	S.	kA
G.	G.	kA
Lucas	Lucas	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redescription	Redescription	k1gInSc1
of	of	k?
the	the	k?
ceratopsid	ceratopsid	k1gInSc1
dinosaur	dinosaur	k1gMnSc1
Torosaurus	Torosaurus	k1gMnSc1
utahensis	utahensis	k1gFnSc4
(	(	kIx(
<g/>
Gilmore	Gilmor	k1gMnSc5
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
)	)	kIx)
and	and	k?
a	a	k8xC
revision	revision	k1gInSc1
of	of	k?
the	the	k?
genus	genus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Paleontology	paleontolog	k1gMnPc4
79	#num#	k4
<g/>
:	:	kIx,
<g/>
564	#num#	k4
<g/>
-	-	kIx~
<g/>
582	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dodson	Dodson	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Horned	Horned	k1gMnSc1
Dinosaurs	Dinosaurs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
Pinceton	Pinceton	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Jersey	Jersea	k1gFnSc2
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
xiv-	xiv-	k?
<g/>
346	#num#	k4
</s>
<s>
Farke	Farke	k1gFnSc1
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cranial	Cranial	k1gInSc1
osteology	osteolog	k1gMnPc4
and	and	k?
phylogenetic	phylogenetice	k1gFnPc2
relationships	relationships	k6eAd1
of	of	k?
the	the	k?
chasmosaurine	chasmosaurin	k1gInSc5
ceratopsid	ceratopsida	k1gFnPc2
Torosaurus	Torosaurus	k1gMnSc1
latus	latus	k1gMnSc1
<g/>
;	;	kIx,
pp	pp	k?
<g/>
.	.	kIx.
235-257	235-257	k4
in	in	k?
K.	K.	kA
Carpenter	Carpenter	k1gInSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Horns	Horns	k1gInSc1
and	and	k?
Beaks	Beaks	k1gInSc1
<g/>
:	:	kIx,
Ceratopsian	Ceratopsian	k1gInSc1
and	and	k?
Ornithopod	Ornithopod	k1gInSc1
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indiana	Indiana	k1gFnSc1
University	universita	k1gFnSc2
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Bloomington	Bloomington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
ReBecca	ReBecca	k1gMnSc1
K.	K.	kA
and	and	k?
Thomas	Thomas	k1gMnSc1
M.	M.	kA
Lehman	Lehman	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attributes	Attributes	k1gMnSc1
of	of	k?
the	the	k?
ceratopsian	ceratopsian	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
Torosaurus	Torosaurus	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
new	new	k?
material	material	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Javelina	Javelina	k1gFnSc1
Formation	Formation	k1gInSc1
(	(	kIx(
<g/>
Maastrichtian	Maastrichtian	k1gInSc1
<g/>
)	)	kIx)
of	of	k?
Texas	Texas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Paleontology	paleontolog	k1gMnPc4
82	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1127	#num#	k4
<g/>
-	-	kIx~
<g/>
1138	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
vědecké	vědecký	k2eAgFnSc6d1
platnosti	platnost	k1gFnSc6
torosaura	torosaura	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
DinosaurusBlog	DinosaurusBloga	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolosální	kolosální	k2eAgInSc1d1
ceratopsid	ceratopsid	k1gInSc1
Torosaurus	Torosaurus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
