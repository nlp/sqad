<s>
Svitava	Svitava	k1gFnSc1	Svitava
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
postupně	postupně	k6eAd1	postupně
okresy	okres	k1gInPc4	okres
Svitavy	Svitava	k1gFnSc2	Svitava
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
a	a	k8xC	a
Brno-venkov	Brnoenkov	k1gInSc4	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
97	[number]	k4	97
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
1150	[number]	k4	1150
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Svitav	Svitava	k1gFnPc2	Svitava
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Javorník	Javorník	k1gInSc1	Javorník
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Blanskem	Blansko	k1gNnSc7	Blansko
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
proráží	prorážet	k5eAaImIp3nS	prorážet
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
úzkým	úzký	k2eAgNnSc7d1	úzké
údolím	údolí	k1gNnSc7	údolí
okraj	okraj	k1gInSc4	okraj
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
<g/>
,	,	kIx,	,
ústí	ústit	k5eAaImIp3nS	ústit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nP	ležet
Svitavy	Svitava	k1gFnPc1	Svitava
<g/>
,	,	kIx,	,
Letovice	Letovice	k1gFnPc1	Letovice
<g/>
,	,	kIx,	,
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Adamov	Adamov	k1gInSc1	Adamov
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Svitavy	Svitava	k1gFnSc2	Svitava
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
mimořádné	mimořádný	k2eAgFnPc1d1	mimořádná
povodně	povodeň	k1gFnPc1	povodeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
opakovaly	opakovat	k5eAaImAgFnP	opakovat
i	i	k9	i
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Svitava	Svitava	k1gFnSc1	Svitava
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
protékala	protékat	k5eAaImAgFnS	protékat
Boskovickou	boskovický	k2eAgFnSc7d1	boskovická
brázdou	brázda	k1gFnSc7	brázda
a	a	k8xC	a
Malou	malý	k2eAgFnSc4d1	malá
Hanou	Haná	k1gFnSc4	Haná
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
horotvorné	horotvorný	k2eAgInPc4d1	horotvorný
procesy	proces	k1gInPc4	proces
změnily	změnit	k5eAaPmAgFnP	změnit
průběh	průběh	k1gInSc4	průběh
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
současným	současný	k2eAgInSc7d1	současný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Křetínka	Křetínka	k1gFnSc1	Křetínka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
66,4	[number]	k4	66,4
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
49,5	[number]	k4	49,5
Punkva	Punkva	k1gFnSc1	Punkva
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
32,9	[number]	k4	32,9
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
úseku	úsek	k1gInSc2	úsek
mezi	mezi	k7c7	mezi
Blanskem	Blansko	k1gNnSc7	Blansko
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
také	také	k9	také
úseku	úsek	k1gInSc2	úsek
nad	nad	k7c4	nad
Březovou	březový	k2eAgFnSc4d1	Březová
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
významné	významný	k2eAgNnSc1d1	významné
prameniště	prameniště	k1gNnSc1	prameniště
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
dodávána	dodávat	k5eAaImNgFnS	dodávat
pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc4d1	celé
údolí	údolí	k1gNnSc4	údolí
Svitavy	Svitava	k1gFnSc2	Svitava
velmi	velmi	k6eAd1	velmi
hustě	hustě	k6eAd1	hustě
osídleno	osídlen	k2eAgNnSc1d1	osídleno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
toku	tok	k1gInSc6	tok
leží	ležet	k5eAaImIp3nP	ležet
města	město	k1gNnPc1	město
a	a	k8xC	a
městyse	městys	k1gInPc1	městys
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Březová	březový	k2eAgFnSc1d1	Březová
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Letovice	Letovice	k1gFnSc1	Letovice
<g/>
,	,	kIx,	,
Svitávka	Svitávka	k1gFnSc1	Svitávka
<g/>
,	,	kIx,	,
Skalice	Skalice	k1gFnSc1	Skalice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Adamov	Adamov	k1gInSc1	Adamov
<g/>
,	,	kIx,	,
Bílovice	Bílovice	k1gInPc1	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řeku	řeka	k1gFnSc4	řeka
vodácky	vodácky	k6eAd1	vodácky
využitelnou	využitelný	k2eAgFnSc4d1	využitelná
a	a	k8xC	a
sice	sice	k8xC	sice
za	za	k7c2	za
vyššího	vysoký	k2eAgInSc2d2	vyšší
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Nejhezčí	hezký	k2eAgInSc1d3	nejhezčí
úsek	úsek	k1gInSc1	úsek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hlubokém	hluboký	k2eAgNnSc6d1	hluboké
údolí	údolí	k1gNnSc6	údolí
mezi	mezi	k7c7	mezi
Blanskem	Blansko	k1gNnSc7	Blansko
a	a	k8xC	a
Adamovem	Adamov	k1gInSc7	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
špatně	špatně	k6eAd1	špatně
přenositelných	přenositelný	k2eAgInPc2d1	přenositelný
jezů	jez	k1gInPc2	jez
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
místy	místy	k6eAd1	místy
zarostlá	zarostlý	k2eAgFnSc1d1	zarostlá
a	a	k8xC	a
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
v	v	k7c6	v
úzkém	úzký	k2eAgNnSc6d1	úzké
korytě	koryto	k1gNnSc6	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
papírny	papírna	k1gFnPc1	papírna
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Svitavě	Svitava	k1gFnSc6	Svitava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
<g/>
,	,	kIx,	,
Březové	březový	k2eAgFnSc3d1	Březová
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Brněnci	Brněnec	k1gInPc7	Brněnec
a	a	k8xC	a
Rozhrání	rozhráň	k1gFnSc7	rozhráň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
vývoj	vývoj	k1gInSc1	vývoj
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
článek	článek	k1gInSc1	článek
Miroslava	Miroslav	k1gMnSc2	Miroslav
Kučery	Kučera	k1gMnSc2	Kučera
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
papíren	papírna	k1gFnPc2	papírna
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
okresu	okres	k1gInSc2	okres
Svitavy	Svitava	k1gFnSc2	Svitava
(	(	kIx(	(
<g/>
sborník	sborník	k1gInSc1	sborník
Pomezí	pomezí	k1gNnSc1	pomezí
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studený	studený	k2eAgInSc1d1	studený
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svitava	Svitava	k1gFnSc1	Svitava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Svitava	Svitava	k1gFnSc1	Svitava
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Bílovice	Bílovice	k1gInPc4	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
-	-	kIx~	-
aktuální	aktuální	k2eAgInSc1d1	aktuální
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
Vodácký	vodácký	k2eAgMnSc1d1	vodácký
průvodce	průvodce	k1gMnSc1	průvodce
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
ČHMÚ	ČHMÚ	kA	ČHMÚ
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Hlásné	hlásný	k2eAgInPc1d1	hlásný
profily	profil	k1gInPc1	profil
povodňové	povodňový	k2eAgFnSc2d1	povodňová
služby	služba	k1gFnSc2	služba
VÚV	VÚV	kA	VÚV
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
-	-	kIx~	-
Oddělení	oddělení	k1gNnSc1	oddělení
GIS	gis	k1gNnSc2	gis
-	-	kIx~	-
Charakteristiky	charakteristika	k1gFnPc4	charakteristika
toků	tok	k1gInPc2	tok
a	a	k8xC	a
povodí	povodit	k5eAaPmIp3nS	povodit
ČR	ČR	kA	ČR
Řeka	řeka	k1gFnSc1	řeka
Svitava	Svitava	k1gFnSc1	Svitava
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
2.5	[number]	k4	2.5
<g/>
.2009	.2009	k4	.2009
...	...	k?	...
<g/>
aneb	aneb	k?	aneb
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
I.	I.	kA	I.
...	...	k?	...
Putování	putování	k1gNnSc1	putování
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
</s>
