<s>
DME	dmout	k5eAaImIp3nS
</s>
<s>
DME	dmout	kA
(	(	kIx(
<g/>
Distance	distance	k1gFnSc1
Measuring	Measuring	k1gInSc1
Equipment	Equipment	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc4
používané	používaný	k2eAgNnSc4d1
v	v	k7c6
letectví	letectví	k1gNnSc6
pro	pro	k7c4
určení	určení	k1gNnSc4
šikmé	šikmý	k2eAgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
mezi	mezi	k7c7
letadlem	letadlo	k1gNnSc7
a	a	k8xC
pozemním	pozemní	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Využíváno	využíván	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
jak	jak	k6eAd1
pro	pro	k7c4
navigaci	navigace	k1gFnSc4
po	po	k7c6
trati	trať	k1gFnSc6
tak	tak	k9
i	i	k9
pro	pro	k7c4
určení	určení	k1gNnSc4
vzdálenosti	vzdálenost	k1gFnSc2
letadla	letadlo	k1gNnSc2
od	od	k7c2
letiště	letiště	k1gNnSc2
při	při	k7c6
přiblížení	přiblížení	k1gNnSc6
na	na	k7c4
přistání	přistání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
pozemní	pozemní	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
DME	dmout	k5eAaImIp3nS
umisťováno	umisťovat	k5eAaImNgNnS
na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
automaticky	automaticky	k6eAd1
laděno	laděn	k2eAgNnSc1d1
se	se	k3xPyFc4
zařízeními	zařízení	k1gNnPc7
VOR	vor	k1gInSc1
<g/>
,	,	kIx,
ILS	ILS	kA
nebo	nebo	k8xC
TACAN	TACAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
umístěno	umístit	k5eAaPmNgNnS
i	i	k9
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Zařízení	zařízení	k1gNnSc1
na	na	k7c6
palubě	paluba	k1gFnSc6
letadla	letadlo	k1gNnSc2
vypočítá	vypočítat	k5eAaPmIp3nS
šikmou	šikmý	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
mezi	mezi	k7c7
letadlem	letadlo	k1gNnSc7
a	a	k8xC
pozemním	pozemní	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
na	na	k7c6
základě	základ	k1gInSc6
měření	měření	k1gNnSc2
časové	časový	k2eAgFnSc2d1
prodlevy	prodleva	k1gFnSc2
mezi	mezi	k7c7
odesláním	odeslání	k1gNnSc7
radiového	radiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
z	z	k7c2
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
zpracováním	zpracování	k1gNnSc7
a	a	k8xC
přeposláním	přeposlánět	k5eAaImIp1nS
pozemní	pozemní	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
a	a	k8xC
zpětným	zpětný	k2eAgNnSc7d1
doručením	doručení	k1gNnSc7
přístroji	přístroj	k1gInSc3
na	na	k7c6
palubě	paluba	k1gFnSc6
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Technický	technický	k2eAgInSc1d1
popis	popis	k1gInSc1
</s>
<s>
DME	dmout	k5eAaImIp3nS
system	syst	k1gMnSc7
block	block	k6eAd1
schema	schema	k1gNnSc4
</s>
<s>
DME	dmout	k5eAaImIp3nS
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
pásmu	pásmo	k1gNnSc6
960	#num#	k4
až	až	k9
1215	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dotazovače	dotazovač	k1gMnPc4
mohou	moct	k5eAaImIp3nP
vysílat	vysílat	k5eAaImF
celkem	celkem	k6eAd1
ve	v	k7c6
126	#num#	k4
kanálech	kanál	k1gInPc6
v	v	k7c6
rozsahu	rozsah	k1gInSc6
1025	#num#	k4
až	až	k9
1150	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemní	pozemní	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
DME	dmout	k5eAaImIp3nS
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
najednou	najednou	k6eAd1
obsluhovat	obsluhovat	k5eAaImF
až	až	k9
100	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případnému	případný	k2eAgNnSc3d1
přetížení	přetížení	k1gNnSc3
je	být	k5eAaImIp3nS
zabráněno	zabráněn	k2eAgNnSc1d1
snížením	snížení	k1gNnSc7
citlivosti	citlivost	k1gFnSc2
přijímače	přijímač	k1gInSc2
při	při	k7c6
dosažení	dosažení	k1gNnSc6
90	#num#	k4
%	%	kIx~
obslužné	obslužný	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dotazovače	dotazovač	k1gMnSc4
odhadují	odhadovat	k5eAaImIp3nP
vlastní	vlastní	k2eAgInSc4d1
dotaz	dotaz	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
stabilního	stabilní	k2eAgInSc2d1
času	čas	k1gInSc2
této	tento	k3xDgFnSc2
odezvy	odezva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odezvy	odezva	k1gFnSc2
od	od	k7c2
ostatních	ostatní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
jsou	být	k5eAaImIp3nP
maskovány	maskovat	k5eAaBmNgFnP
časovým	časový	k2eAgNnSc7d1
oknem	okno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímač	přijímač	k1gInSc1
dotazovače	dotazovač	k1gMnSc2
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgInSc4d1
pracovat	pracovat	k5eAaImF
ve	v	k7c6
dvou	dva	k4xCgInPc6
režimech	režim	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Režim	režim	k1gInSc1
vyhledávání	vyhledávání	k1gNnSc2
-	-	kIx~
První	první	k4xOgFnSc1
přijatá	přijatý	k2eAgFnSc1d1
odezva	odezva	k1gFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
vlastní	vlastní	k2eAgInSc4d1
dotaz	dotaz	k1gInSc4
<g/>
,	,	kIx,
následně	následně	k6eAd1
je	být	k5eAaImIp3nS
vyslán	vyslán	k2eAgInSc4d1
nový	nový	k2eAgInSc4d1
dotaz	dotaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
době	doba	k1gFnSc6
první	první	k4xOgFnSc2
odezvy	odezva	k1gFnSc2
nepřijde	přijít	k5eNaPmIp3nS
odpověď	odpověď	k1gFnSc4
na	na	k7c4
nový	nový	k2eAgInSc4d1
dotaz	dotaz	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
je	být	k5eAaImIp3nS
prodloužena	prodloužit	k5eAaPmNgFnS
časová	časový	k2eAgFnSc1d1
maska	maska	k1gFnSc1
a	a	k8xC
vyslán	vyslán	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
dotaz	dotaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
je	být	k5eAaImIp3nS
pokračováno	pokračován	k2eAgNnSc1d1
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
odpověď	odpověď	k1gFnSc1
na	na	k7c4
dotaz	dotaz	k1gInSc4
přichází	přicházet	k5eAaImIp3nS
ve	v	k7c4
stabilní	stabilní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
přijímač	přijímač	k1gMnSc1
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
režimu	režim	k1gInSc2
sledování	sledování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Režim	režim	k1gInSc1
sledování	sledování	k1gNnSc2
-	-	kIx~
V	v	k7c6
tomto	tento	k3xDgInSc6
režimu	režim	k1gInSc6
přijímač	přijímač	k1gMnSc1
udržuje	udržovat	k5eAaImIp3nS
časovou	časový	k2eAgFnSc4d1
masku	maska	k1gFnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
vlastního	vlastní	k2eAgInSc2d1
dotazu	dotaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
každou	každý	k3xTgFnSc7
novou	nový	k2eAgFnSc7d1
odpovědí	odpověď	k1gFnSc7
na	na	k7c4
vlastní	vlastní	k2eAgInSc4d1
dotaz	dotaz	k1gInSc4
je	být	k5eAaImIp3nS
časová	časový	k2eAgFnSc1d1
maska	maska	k1gFnSc1
posunuta	posunout	k5eAaPmNgFnS
symetricky	symetricky	k6eAd1
okolo	okolo	k7c2
přijaté	přijatý	k2eAgFnSc2d1
odpovědi	odpověď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
měřit	měřit	k5eAaImF
i	i	k9
velmi	velmi	k6eAd1
krátké	krátký	k2eAgFnPc4d1
(	(	kIx(
<g/>
nulové	nulový	k2eAgFnPc4d1
<g/>
)	)	kIx)
vzdálenosti	vzdálenost	k1gFnPc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
pozemní	pozemní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
definovanou	definovaný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
zpoždění	zpoždění	k1gNnPc2
50	#num#	k4
μ	μ	k6eAd1
mezi	mezi	k7c7
přijetím	přijetí	k1gNnSc7
dotazu	dotaz	k1gInSc2
a	a	k8xC
odesláním	odeslání	k1gNnSc7
odpovědi	odpověď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kmitočet	kmitočet	k1gInSc1
vysílání	vysílání	k1gNnSc2
dvojic	dvojice	k1gFnPc2
pulsů	puls	k1gInPc2
dotazu	dotaz	k1gInSc2
je	být	k5eAaImIp3nS
150	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
pro	pro	k7c4
režim	režim	k1gInSc4
vyhledávání	vyhledávání	k1gNnSc2
a	a	k8xC
30	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
pro	pro	k7c4
režim	režim	k1gInSc4
sledování	sledování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
DME	dmout	k5eAaImIp3nS
odpovídač	odpovídač	k1gInSc1
vysílá	vysílat	k5eAaImIp3nS
svojí	svůj	k3xOyFgFnSc3
identifikaci	identifikace	k1gFnSc3
v	v	k7c6
morse	mors	k1gInSc6
code	codat	k5eAaPmIp3nS
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
že	že	k8xS
vysílá	vysílat	k5eAaImIp3nS
dvojice	dvojice	k1gFnSc1
s	s	k7c7
kmitočtem	kmitočet	k1gInSc7
1350	#num#	k4
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Tato	tento	k3xDgFnSc1
frekvence	frekvence	k1gFnPc4
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
slyšitelná	slyšitelný	k2eAgFnSc1d1
ve	v	k7c6
sluchátkách	sluchátko	k1gNnPc6
a	a	k8xC
odpovídač	odpovídač	k1gInSc1
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
identifikován	identifikován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Formát	formát	k1gInSc1
dotazu	dotaz	k1gInSc2
</s>
<s>
Dotazovač	dotazovač	k1gMnSc1
vysílá	vysílat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
impulsy	impuls	k1gInPc4
s	s	k7c7
roztečí	rozteč	k1gFnSc7
buď	buď	k8xC
12	#num#	k4
μ	μ	k1gFnPc2
pro	pro	k7c4
kanály	kanál	k1gInPc4
X	X	kA
a	a	k8xC
nebo	nebo	k8xC
36	#num#	k4
μ	μ	k1gFnPc2
pro	pro	k7c4
kanály	kanál	k1gInPc4
Y.	Y.	kA
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
rozšířen	rozšířit	k5eAaPmNgInS
efektivní	efektivní	k2eAgInSc1d1
počet	počet	k1gInSc1
kanálů	kanál	k1gInPc2
na	na	k7c4
252	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1	#num#	k4
<g/>
X	X	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
Y	Y	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
X	X	kA
<g/>
,	,	kIx,
2Y	2Y	k4
atd.	atd.	kA
až	až	k9
126	#num#	k4
<g/>
Y	Y	kA
<g/>
)	)	kIx)
</s>
<s>
Formát	formát	k1gInSc1
odpovědi	odpověď	k1gFnSc2
</s>
<s>
Odpovídač	odpovídač	k1gInSc1
přijatou	přijatý	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
přeloží	přeložit	k5eAaPmIp3nS
podle	podle	k7c2
následujícího	následující	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
;	;	kIx,
Odešle	odeslat	k5eAaPmIp3nS
odpověď	odpověď	k1gFnSc4
na	na	k7c4
dotaz	dotaz	k1gInSc4
X	X	kA
s	s	k7c7
roztečí	rozteč	k1gFnSc7
12	#num#	k4
μ	μ	k1gFnPc2
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
Fo	Fo	k1gFnSc2
=	=	kIx~
Fd	Fd	k1gFnSc1
–	–	k?
63	#num#	k4
pro	pro	k7c4
kanály	kanál	k1gInPc4
s	s	k7c7
číslem	číslo	k1gNnSc7
1	#num#	k4
-63	-63	k4
a	a	k8xC
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
Fo	Fo	k1gFnSc2
<g/>
=	=	kIx~
<g/>
fd	fd	k?
–	–	k?
63	#num#	k4
pro	pro	k7c4
kanály	kanál	k1gInPc4
64	#num#	k4
<g/>
-	-	kIx~
<g/>
126	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovědi	odpověď	k1gFnPc4
na	na	k7c4
dotazy	dotaz	k1gInPc4
v	v	k7c6
kanálech	kanál	k1gInPc6
Y	Y	kA
jsou	být	k5eAaImIp3nP
vysílány	vysílat	k5eAaImNgInP
s	s	k7c7
roztečí	rozteč	k1gFnSc7
30	#num#	k4
μ	μ	k1gFnPc2
na	na	k7c6
frekvencích	frekvence	k1gFnPc6
Fo	Fo	k1gMnPc2
=	=	kIx~
Fd	Fd	k1gMnSc1
+	+	kIx~
63	#num#	k4
pro	pro	k7c4
kanály	kanál	k1gInPc4
1-63	1-63	k4
a	a	k8xC
na	na	k7c6
frekvencích	frekvence	k1gFnPc6
Fo	Fo	k1gFnSc2
=	=	kIx~
Fd	Fd	k1gFnSc1
–	–	k?
63	#num#	k4
pro	pro	k7c4
kanály	kanál	k1gInPc4
64	#num#	k4
<g/>
-	-	kIx~
<g/>
126	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
dotazovače	dotazovač	k1gMnSc2
může	moct	k5eAaImIp3nS
proto	proto	k8xC
být	být	k5eAaImF
dotaz	dotaz	k1gInSc1
vysílán	vysílán	k2eAgInSc1d1
na	na	k7c6
dvou	dva	k4xCgInPc6
kanálech	kanál	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
odpovídač	odpovídač	k1gInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
každém	každý	k3xTgInSc6
kmitočtu	kmitočet	k1gInSc6
jen	jen	k9
jeden	jeden	k4xCgInSc4
kanál	kanál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přesnost	přesnost	k1gFnSc1
a	a	k8xC
dosah	dosah	k1gInSc1
</s>
<s>
Teoretická	teoretický	k2eAgFnSc1d1
přesnost	přesnost	k1gFnSc1
měření	měření	k1gNnSc2
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgMnSc1d2
než	než	k8xS
400	#num#	k4
m	m	kA
nebo	nebo	k8xC
0,25	0,25	k4
%	%	kIx~
(	(	kIx(
<g/>
větší	veliký	k2eAgFnSc1d2
z	z	k7c2
těchto	tento	k3xDgFnPc2
hodnot	hodnota	k1gFnPc2
<g/>
)	)	kIx)
běžně	běžně	k6eAd1
ale	ale	k8xC
0,93	0,93	k4
<g/>
km	km	kA
<g/>
,	,	kIx,
dosah	dosah	k1gInSc1
je	být	k5eAaImIp3nS
370	#num#	k4
km	km	kA
ve	v	k7c6
výškách	výška	k1gFnPc6
22900	#num#	k4
<g/>
m.	m.	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Letadlové	letadlový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Vratoslav	Vratoslav	k1gMnSc1
Věk	věk	k1gInSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Jana	Jana	k1gFnSc1
Celerinová	Celerinová	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2501	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
71	#num#	k4
</s>
<s>
Rádiové	rádiový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
FEL	FEL	kA
ČVUT	ČVUT	kA
</s>
<s>
airnav	airnat	k5eAaPmDgInS
<g/>
.	.	kIx.
<g/>
eu	eu	k?
</s>
