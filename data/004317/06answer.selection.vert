<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Šumava	Šumava	k1gFnSc1	Šumava
(	(	kIx(	(
<g/>
69	[number]	k4	69
030	[number]	k4	030
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
