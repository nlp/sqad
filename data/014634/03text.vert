<s>
Musée	Musée	k1gFnSc1
Moissan	Moissana	k1gFnPc2
</s>
<s>
Musée	Muséat	k5eAaPmIp3nS
Moissan	Moissan	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Údaje	údaj	k1gInSc2
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Paříž	Paříž	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Faculté	Facultý	k2eAgFnPc4d1
de	de	k?
Pharmacie	Pharmacie	k1gFnPc4
4	#num#	k4
<g/>
,	,	kIx,
avenue	avenue	k1gFnSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Observatoire	Observatoir	k1gMnSc5
<g/>
,	,	kIx,
75006	#num#	k4
Paris	Paris	k1gMnSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Louis	Louis	k1gMnSc1
Moissan	Moissan	k1gMnSc1
Založeno	založen	k2eAgNnSc4d1
</s>
<s>
1925	#num#	k4
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Musée	Musée	k1gInSc1
Moissan	Moissan	k1gInSc1
(	(	kIx(
<g/>
Moissanovo	Moissanův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
obvodu	obvod	k1gInSc2
na	na	k7c6
Avenue	avenue	k1gFnSc6
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Observatoire	Observatoir	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
dnešní	dnešní	k2eAgFnSc2d1
Univerzity	univerzita	k1gFnSc2
Paříž	Paříž	k1gFnSc1
V	V	kA
a	a	k8xC
je	být	k5eAaImIp3nS
zasvěceno	zasvěcen	k2eAgNnSc1d1
životu	život	k1gInSc3
a	a	k8xC
dílu	dílo	k1gNnSc3
francouzského	francouzský	k2eAgMnSc2d1
chemika	chemik	k1gMnSc2
Henri	Henr	k1gFnSc2
Moissana	Moissan	k1gMnSc2
(	(	kIx(
<g/>
1852	#num#	k4
<g/>
-	-	kIx~
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
držitele	držitel	k1gMnSc4
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
chemii	chemie	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Musée	Musé	k1gFnSc2
Moissan	Moissana	k1gFnPc2
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Dějiny	dějiny	k1gFnPc1
muzea	muzeum	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
