<s desamb="1">
Muzeum	muzeum	k1gNnSc1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
dnešní	dnešní	k2eAgFnSc2d1
Univerzity	univerzita	k1gFnSc2
Paříž	Paříž	k1gFnSc1
V	V	kA
a	a	k8xC
je	být	k5eAaImIp3nS
zasvěceno	zasvěcen	k2eAgNnSc1d1
životu	život	k1gInSc3
a	a	k8xC
dílu	dílo	k1gNnSc3
francouzského	francouzský	k2eAgMnSc2d1
chemika	chemik	k1gMnSc2
Henri	Henr	k1gFnSc2
Moissana	Moissan	k1gMnSc2
(	(	kIx(
<g/>
1852	#num#	k4
<g/>
-	-	kIx~
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
držitele	držitel	k1gMnSc4
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
chemii	chemie	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
.	.	kIx.
</s>