<s>
První	první	k4xOgFnPc1	první
Remosky	remoska	k1gFnPc1	remoska
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
sériově	sériově	k6eAd1	sériově
vyrábět	vyrábět	k5eAaImF	vyrábět
ve	v	k7c6	v
Zdicích	Zdice	k1gFnPc6	Zdice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zdejší	zdejší	k2eAgInSc1d1	zdejší
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgInS	specializovat
na	na	k7c4	na
pečicí	pečicí	k2eAgFnPc4d1	pečicí
trouby	trouba	k1gFnPc4	trouba
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
výrobky	výrobek	k1gInPc1	výrobek
nesly	nést	k5eAaImAgInP	nést
značku	značka	k1gFnSc4	značka
ESČ	ESČ	kA	ESČ
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
