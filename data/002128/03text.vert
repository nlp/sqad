<s>
Kodaň	Kodaň	k1gFnSc1	Kodaň
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
Kø	Kø	k1gFnSc1	Kø
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
kh	kh	k0	kh
<g/>
̥	̥	k?	̥
<g/>
ə	ə	k?	ə
<g/>
̯	̯	k?	̯
<g/>
ˀ	ˀ	k?	ˀ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Sjæ	Sjæ	k1gFnSc2	Sjæ
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Amager	Amager	k1gInSc1	Amager
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
samém	samý	k3xTgMnSc6	samý
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
městské	městský	k2eAgFnSc6d1	městská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
přes	přes	k7c4	přes
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Kodaň	Kodaň	k1gFnSc1	Kodaň
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
dánské	dánský	k2eAgFnSc2d1	dánská
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Kodani	Kodaň	k1gFnSc6	Kodaň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
přístavní	přístavní	k2eAgNnSc1d1	přístavní
sídlo	sídlo	k1gNnSc1	sídlo
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
století	století	k1gNnSc6	století
patnáctém	patnáctý	k4xOgNnSc6	patnáctý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Kristiána	Kristián	k1gMnSc2	Kristián
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Kodaň	Kodaň	k1gFnSc1	Kodaň
významným	významný	k2eAgMnSc7d1	významný
centrem	centr	k1gMnSc7	centr
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
Mostem	most	k1gInSc7	most
přes	přes	k7c4	přes
Ø	Ø	k?	Ø
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
švédským	švédský	k2eAgInSc7d1	švédský
Malmö	Malmö	k1gFnSc4	Malmö
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kodaně	Kodaň	k1gFnSc2	Kodaň
a	a	k8xC	a
Malmö	Malmö	k1gFnSc2	Malmö
se	se	k3xPyFc4	se
tak	tak	k9	tak
pomalu	pomalu	k6eAd1	pomalu
stává	stávat	k5eAaImIp3nS	stávat
jedna	jeden	k4xCgFnSc1	jeden
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
50	[number]	k4	50
km	km	kA	km
žije	žít	k5eAaImIp3nS	žít
cca	cca	kA	cca
2,7	[number]	k4	2,7
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kodaň	Kodaň	k1gFnSc1	Kodaň
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
regionálním	regionální	k2eAgNnSc7d1	regionální
centrem	centrum	k1gNnSc7	centrum
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
průzkumy	průzkum	k1gInPc1	průzkum
a	a	k8xC	a
žebříčky	žebříček	k1gInPc1	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
s	s	k7c7	s
nejkvalitnější	kvalitní	k2eAgFnSc7d3	nejkvalitnější
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
nejšetrnějším	šetrný	k2eAgInSc7d3	nejšetrnější
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
největší	veliký	k2eAgNnSc4d3	veliký
skandinávské	skandinávský	k2eAgNnSc4d1	skandinávské
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgFnPc2d3	nejnavštěvovanější
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
je	být	k5eAaImIp3nS	být
bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
malé	malý	k2eAgFnSc2d1	malá
mořské	mořský	k2eAgFnSc2d1	mořská
víly	víla	k1gFnSc2	víla
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
blízkosti	blízkost	k1gFnSc6	blízkost
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
přístavní	přístavní	k2eAgFnSc1d1	přístavní
pevnost	pevnost	k1gFnSc1	pevnost
Kastellet	Kastellet	k1gInSc4	Kastellet
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Kodaně	Kodaň	k1gFnSc2	Kodaň
leží	ležet	k5eAaImIp3nS	ležet
známý	známý	k2eAgInSc1d1	známý
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Tivoli	Tivole	k1gFnSc4	Tivole
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
Kodaň	Kodaň	k1gFnSc4	Kodaň
Evropským	evropský	k2eAgNnSc7d1	Evropské
městem	město	k1gNnSc7	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
dánský	dánský	k2eAgInSc1d1	dánský
název	název	k1gInSc1	název
Kø	Kø	k1gFnSc2	Kø
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
středověkého	středověký	k2eAgNnSc2d1	středověké
pojmenování	pojmenování	k1gNnSc2	pojmenování
v	v	k7c6	v
dánštině	dánština	k1gFnSc6	dánština
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
Kø	Kø	k1gFnSc7	Kø
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Přístav	přístav	k1gInSc1	přístav
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1043	[number]	k4	1043
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
poprvé	poprvé	k6eAd1	poprvé
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
Havn	Havn	k1gInSc1	Havn
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
také	také	k9	také
s	s	k7c7	s
latinským	latinský	k2eAgNnSc7d1	latinské
označením	označení	k1gNnSc7	označení
Hafnia	hafnium	k1gNnSc2	hafnium
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
znamenají	znamenat	k5eAaImIp3nP	znamenat
totéž	týž	k3xTgNnSc4	týž
-	-	kIx~	-
Přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
právě	právě	k6eAd1	právě
od	od	k7c2	od
latinského	latinský	k2eAgNnSc2d1	latinské
pojmenování	pojmenování	k1gNnSc2	pojmenování
města	město	k1gNnSc2	město
odvozen	odvodit	k5eAaPmNgInS	odvodit
název	název	k1gInSc1	název
pro	pro	k7c4	pro
hafnium	hafnium	k1gNnSc4	hafnium
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
objevili	objevit	k5eAaPmAgMnP	objevit
Dirk	Dirk	k1gMnSc1	Dirk
Coster	Coster	k1gMnSc1	Coster
a	a	k8xC	a
Georg	Georg	k1gMnSc1	Georg
von	von	k1gInSc4	von
Hevesy	Hevesa	k1gFnSc2	Hevesa
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
Kodaně	Kodaň	k1gFnSc2	Kodaň
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
anglicky	anglicky	k6eAd1	anglicky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Copenhagen	Copenhagen	k1gInSc1	Copenhagen
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
a	a	k8xC	a
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Kopenhagen	Kopenhagen	k1gInSc4	Kopenhagen
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Kopenhaga	Kopenhaga	k1gFnSc1	Kopenhaga
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Köpenhamn	Köpenhamn	k1gInSc1	Köpenhamn
<g/>
,	,	kIx,	,
islandsky	islandsky	k6eAd1	islandsky
Kaupmannahöfn	Kaupmannahöfn	k1gInSc1	Kaupmannahöfn
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
portugalsky	portugalsky	k6eAd1	portugalsky
Copenhague	Copenhague	k1gNnSc1	Copenhague
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
lidskou	lidský	k2eAgFnSc4d1	lidská
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
datující	datující	k2eAgNnSc1d1	datující
se	se	k3xPyFc4	se
do	do	k7c2	do
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
trvalé	trvalý	k2eAgNnSc1d1	trvalé
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
průzkumů	průzkum	k1gInPc2	průzkum
tu	tu	k6eAd1	tu
první	první	k4xOgNnSc1	první
trvalé	trvalý	k2eAgNnSc1d1	trvalé
osídlení	osídlení	k1gNnSc1	osídlení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
osady	osada	k1gFnPc4	osada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Hø	Hø	k1gMnSc2	Hø
Plads	Pladsa	k1gFnPc2	Pladsa
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nacházel	nacházet	k5eAaImAgMnS	nacházet
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
význam	význam	k1gInSc1	význam
Kodaně	Kodaň	k1gFnSc2	Kodaň
rostl	růst	k5eAaImAgInS	růst
a	a	k8xC	a
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
posíleno	posílit	k5eAaPmNgNnS	posílit
zemními	zemní	k2eAgFnPc7d1	zemní
pracemi	práce	k1gFnPc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
katedrály	katedrála	k1gFnPc4	katedrála
v	v	k7c6	v
Roskilde	Roskild	k1gInSc5	Roskild
a	a	k8xC	a
Lundu	Lunda	k1gMnSc4	Lunda
a	a	k8xC	a
položila	položit	k5eAaPmAgFnS	položit
tak	tak	k9	tak
základ	základ	k1gInSc4	základ
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
těchto	tento	k3xDgNnPc2	tento
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
ideální	ideální	k2eAgFnSc4d1	ideální
polohu	poloha	k1gFnSc4	poloha
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Kodani	Kodaň	k1gFnSc6	Kodaň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
píše	psát	k5eAaImIp3nS	psát
Saxo	Saxo	k6eAd1	Saxo
Grammaticus	Grammaticus	k1gInSc4	Grammaticus
jako	jako	k8xC	jako
o	o	k7c4	o
Portus	Portus	k1gInSc4	Portus
Mercatorum	Mercatorum	k1gNnSc4	Mercatorum
<g/>
,	,	kIx,	,
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
dánštině	dánština	k1gFnSc6	dánština
tedy	tedy	k8xC	tedy
Kø	Kø	k1gFnSc6	Kø
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Urban	Urban	k1gMnSc1	Urban
III	III	kA	III
<g/>
.	.	kIx.	.
nazval	nazvat	k5eAaPmAgInS	nazvat
město	město	k1gNnSc4	město
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1186	[number]	k4	1186
jako	jako	k8xC	jako
Hafn	Hafn	k1gInSc1	Hafn
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
šlo	jít	k5eAaImAgNnS	jít
ale	ale	k9	ale
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
a	a	k8xC	a
suburbanizace	suburbanizace	k1gFnSc2	suburbanizace
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
jasný	jasný	k2eAgInSc4d1	jasný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
píše	psát	k5eAaImIp3nS	psát
Dan	Dan	k1gMnSc1	Dan
Christensen	Christensen	k2eAgMnSc1d1	Christensen
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
Copenhagen	Copenhagen	k1gInSc1	Copenhagen
<g/>
:	:	kIx,	:
views	views	k1gInSc1	views
a	a	k8xC	a
visions	visions	k1gInSc1	visions
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
sama	sám	k3xTgFnSc1	sám
Kodaňská	kodaňský	k2eAgFnSc1d1	Kodaňská
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vydala	vydat	k5eAaPmAgFnS	vydat
radnice	radnice	k1gFnSc1	radnice
plán	plán	k1gInSc4	plán
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
publikaci	publikace	k1gFnSc6	publikace
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
plánování	plánování	k1gNnSc1	plánování
začalo	začít	k5eAaPmAgNnS	začít
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1948	[number]	k4	1948
publikován	publikovat	k5eAaBmNgInS	publikovat
návrh	návrh	k1gInSc1	návrh
"	"	kIx"	"
<g/>
Regionálního	regionální	k2eAgInSc2d1	regionální
plánu	plán	k1gInSc2	plán
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
Kodaň	Kodaň	k1gFnSc4	Kodaň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
podél	podél	k7c2	podél
S-trains	Srainsa	k1gFnPc2	S-trainsa
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
zůstat	zůstat	k5eAaPmF	zůstat
centrem	centrum	k1gNnSc7	centrum
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
stanic	stanice	k1gFnPc2	stanice
vlaku	vlak	k1gInSc2	vlak
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
soustředit	soustředit	k5eAaPmF	soustředit
servisní	servisní	k2eAgFnPc4d1	servisní
střediska	středisko	k1gNnPc4	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
každé	každý	k3xTgFnSc2	každý
stanice	stanice	k1gFnSc2	stanice
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
postaveno	postaven	k2eAgNnSc4d1	postaveno
bydlení	bydlení	k1gNnSc4	bydlení
pro	pro	k7c4	pro
10	[number]	k4	10
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
plánu	plán	k1gInSc3	plán
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
Stan	stan	k1gInSc4	stan
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plán	plán	k1gInSc1	plán
měl	mít	k5eAaImAgInS	mít
pokrýt	pokrýt	k5eAaPmF	pokrýt
3	[number]	k4	3
000	[number]	k4	000
km2	km2	k4	km2
a	a	k8xC	a
naplánovat	naplánovat	k5eAaBmF	naplánovat
bydlení	bydlení	k1gNnSc4	bydlení
pro	pro	k7c4	pro
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
neudržitelný	udržitelný	k2eNgInSc1d1	neudržitelný
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc1	rozvoj
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
nový	nový	k2eAgInSc1d1	nový
plán	plán	k1gInSc1	plán
Figure	Figur	k1gMnSc5	Figur
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
plánu	plán	k1gInSc6	plán
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
rozvoj	rozvoj	k1gInSc1	rozvoj
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
oblasti	oblast	k1gFnSc2	oblast
okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
rychlost	rychlost	k1gFnSc1	rychlost
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
neudržitelná	udržitelný	k2eNgFnSc1d1	neudržitelná
i	i	k9	i
podle	podle	k7c2	podle
druhého	druhý	k4xOgInSc2	druhý
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
nad	nad	k7c7	nad
rozvojem	rozvoj	k1gInSc7	rozvoj
Kodaně	Kodaň	k1gFnSc2	Kodaň
diskuze	diskuze	k1gFnSc2	diskuze
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
rozvojem	rozvoj	k1gInSc7	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Regional	Regional	k1gMnPc2	Regional
Planning	Planning	k1gInSc4	Planning
Council	Councila	k1gFnPc2	Councila
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
debaty	debata	k1gFnSc2	debata
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
instituce	instituce	k1gFnSc2	instituce
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
"	"	kIx"	"
<g/>
Regionální	regionální	k2eAgInSc1d1	regionální
plán	plán	k1gInSc1	plán
1973	[number]	k4	1973
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Figure	Figur	k1gMnSc5	Figur
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
měl	mít	k5eAaImAgInS	mít
řešit	řešit	k5eAaImF	řešit
problém	problém	k1gInSc4	problém
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
doprava	doprava	k1gFnSc1	doprava
vedena	vést	k5eAaImNgFnS	vést
hlavně	hlavně	k9	hlavně
okolo	okolo	k7c2	okolo
S-train	Sraina	k1gFnPc2	S-traina
linek	linka	k1gFnPc2	linka
směrem	směr	k1gInSc7	směr
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
do	do	k7c2	do
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k6eAd1	potřeba
vybudovat	vybudovat	k5eAaPmF	vybudovat
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odvede	odvést	k5eAaPmIp3nS	odvést
dopravu	doprava	k1gFnSc4	doprava
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
<g/>
,	,	kIx,	,
až	až	k9	až
téměř	téměř	k6eAd1	téměř
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
ropný	ropný	k2eAgInSc1d1	ropný
šok	šok	k1gInSc1	šok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
vypracován	vypracován	k2eAgInSc1d1	vypracován
plán	plán	k1gInSc1	plán
"	"	kIx"	"
<g/>
Regionální	regionální	k2eAgInSc1d1	regionální
plán	plán	k1gInSc1	plán
1989	[number]	k4	1989
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Figure	Figur	k1gMnSc5	Figur
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hlavních	hlavní	k2eAgFnPc2d1	hlavní
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tepen	tepna	k1gFnPc2	tepna
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
S-trains	Srains	k1gInSc1	S-trains
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
je	být	k5eAaImIp3nS	být
suburbanizace	suburbanizace	k1gFnSc1	suburbanizace
řízená	řízený	k2eAgFnSc1d1	řízená
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
předměstí	předměstí	k1gNnPc1	předměstí
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nP	soustředit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
těchto	tento	k3xDgFnPc2	tento
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
"	"	kIx"	"
<g/>
Finger	Finger	k1gInSc1	Finger
Plan	plan	k1gInSc1	plan
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
byly	být	k5eAaImAgInP	být
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
suburbanizace	suburbanizace	k1gFnSc1	suburbanizace
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
soustředí	soustředit	k5eAaPmIp3nS	soustředit
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
S-trains	Srainsa	k1gFnPc2	S-trainsa
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
mapa	mapa	k1gFnSc1	mapa
města	město	k1gNnSc2	město
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
ruka	ruka	k1gFnSc1	ruka
s	s	k7c7	s
prsty	prst	k1gInPc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Hansen	Hansen	k1gInSc1	Hansen
a	a	k8xC	a
Petersen	Petersen	k1gInSc1	Petersen
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
také	také	k6eAd1	také
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
studie	studie	k1gFnSc2	studie
docházelo	docházet	k5eAaImAgNnS	docházet
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
k	k	k7c3	k
odlivu	odliv	k1gInSc3	odliv
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
do	do	k7c2	do
okraje	okraj	k1gInSc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Suburbanizace	Suburbanizace	k1gFnSc1	Suburbanizace
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
zpomalila	zpomalit	k5eAaPmAgFnS	zpomalit
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nesnižoval	snižovat	k5eNaImAgMnS	snižovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
stagnoval	stagnovat	k5eAaImAgMnS	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
Stádium	stádium	k1gNnSc1	stádium
suburbanizace	suburbanizace	k1gFnSc2	suburbanizace
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1945	[number]	k4	1945
a	a	k8xC	a
1978	[number]	k4	1978
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
pokračováním	pokračování	k1gNnSc7	pokračování
stádia	stádium	k1gNnSc2	stádium
desurbanizace	desurbanizace	k1gFnSc2	desurbanizace
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
stavu	stav	k1gInSc3	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
než	než	k8xS	než
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
už	už	k6eAd1	už
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
2,5	[number]	k4	2,5
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
než	než	k8xS	než
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
také	také	k9	také
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgInPc4d1	mírný
<g/>
,	,	kIx,	,
přechodné	přechodný	k2eAgInPc4d1	přechodný
mezi	mezi	k7c7	mezi
oceánským	oceánský	k2eAgInSc7d1	oceánský
a	a	k8xC	a
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
proměnlivé	proměnlivý	k2eAgFnSc2d1	proměnlivá
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
padá	padat	k5eAaImIp3nS	padat
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
března	březen	k1gInSc2	březen
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
sněžení	sněžení	k1gNnSc1	sněžení
stejně	stejně	k6eAd1	stejně
časté	častý	k2eAgNnSc1d1	časté
jako	jako	k9	jako
déšť	déšť	k1gInSc4	déšť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
fouká	foukat	k5eAaImIp3nS	foukat
vítr	vítr	k1gInSc1	vítr
od	od	k7c2	od
východu	východ	k1gInSc2	východ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
celodenní	celodenní	k2eAgInSc4d1	celodenní
mráz	mráz	k1gInSc4	mráz
a	a	k8xC	a
po	po	k7c6	po
ránu	ráno	k1gNnSc6	ráno
i	i	k9	i
méně	málo	k6eAd2	málo
než	než	k8xS	než
-10	-10	k4	-10
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Jaro	jaro	k1gNnSc1	jaro
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
evropě	evropa	k1gFnSc6	evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Bouřky	bouřka	k1gFnPc1	bouřka
mohou	moct	k5eAaImIp3nP	moct
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgFnPc1d1	vzácná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
od	od	k7c2	od
půli	půle	k1gFnSc4	půle
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
naopak	naopak	k6eAd1	naopak
mohou	moct	k5eAaImIp3nP	moct
východní	východní	k2eAgInPc4d1	východní
větry	vítr	k1gInPc4	vítr
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tlakovou	tlakový	k2eAgFnSc7d1	tlaková
výší	výše	k1gFnSc7	výše
přinést	přinést	k5eAaPmF	přinést
vedra	vedro	k1gNnPc4	vedro
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
33.8	[number]	k4	33.8
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota-	teplota-	k?	teplota-
<g/>
24.2	[number]	k4	24.2
°	°	k?	°
<g/>
C	C	kA	C
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
největší	veliký	k2eAgInSc1d3	veliký
dánský	dánský	k2eAgInSc1d1	dánský
přístav	přístav	k1gInSc1	přístav
má	mít	k5eAaImIp3nS	mít
Kodaň	Kodaň	k1gFnSc4	Kodaň
lodní	lodní	k2eAgNnSc1d1	lodní
spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Polskem	Polsko	k1gNnSc7	Polsko
i	i	k8xC	i
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Amager	Amager	k1gInSc1	Amager
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
evropských	evropský	k2eAgNnPc2d1	Evropské
letišť	letiště	k1gNnPc2	letiště
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
skandinávské	skandinávský	k2eAgNnSc1d1	skandinávské
letiště	letiště	k1gNnSc1	letiště
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Kastrup	Kastrup	k1gInSc1	Kastrup
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Sjæ	Sjæ	k1gMnSc1	Sjæ
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgMnSc1d1	spojen
s	s	k7c7	s
okolní	okolní	k2eAgFnSc7d1	okolní
pevninou	pevnina	k1gFnSc7	pevnina
řadou	řada	k1gFnSc7	řada
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
železniční	železniční	k2eAgFnSc4d1	železniční
a	a	k8xC	a
silniční	silniční	k2eAgFnSc4d1	silniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
velice	velice	k6eAd1	velice
moderního	moderní	k2eAgNnSc2d1	moderní
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
automatického	automatický	k2eAgNnSc2d1	automatické
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
vlakové	vlakový	k2eAgFnPc1d1	vlaková
soupravy	souprava	k1gFnPc1	souprava
jezdí	jezdit	k5eAaImIp3nP	jezdit
bez	bez	k7c2	bez
řidičů	řidič	k1gMnPc2	řidič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
o	o	k7c4	o
okružní	okružní	k2eAgFnPc4d1	okružní
linky	linka	k1gFnPc4	linka
M3	M3	k1gFnSc2	M3
a	a	k8xC	a
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
je	být	k5eAaImIp3nS	být
Kø	Kø	k1gFnSc1	Kø
H.	H.	kA	H.
</s>
