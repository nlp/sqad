<p>
<s>
Šinzó	Šinzó	k?	Šinzó
Abe	Abe	k1gFnSc1	Abe
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
安	安	k?	安
晋	晋	k?	晋
<g/>
,	,	kIx,	,
Abe	Abe	k1gMnSc1	Abe
Šinzó	Šinzó	k1gMnSc1	Šinzó
<g/>
;	;	kIx,	;
narozen	narozen	k2eAgMnSc1d1	narozen
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgMnSc1d1	japonský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
znovuzvolený	znovuzvolený	k2eAgMnSc1d1	znovuzvolený
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Abe	Abe	k?	Abe
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
dědečkem	dědeček	k1gMnSc7	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
Nobusuke	Nobusuke	k1gInSc1	Nobusuke
Kiši	Kiš	k1gFnSc2	Kiš
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
války	válka	k1gFnSc2	válka
správcem	správce	k1gMnSc7	správce
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
v	v	k7c6	v
Tódžově	Tódžův	k2eAgInSc6d1	Tódžův
kabinetu	kabinet	k1gInSc6	kabinet
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
vyšetřován	vyšetřován	k2eAgInSc1d1	vyšetřován
jako	jako	k8xS	jako
válečný	válečný	k2eAgMnSc1d1	válečný
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
letech	let	k1gInPc6	let
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
premiérem	premiér	k1gMnSc7	premiér
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Kišiho	Kišize	k6eAd1	Kišize
bratr	bratr	k1gMnSc1	bratr
Eisaku	Eisak	k1gInSc2	Eisak
Sató	Sató	k1gMnSc1	Sató
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abe	Abe	k?	Abe
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
studiu	studio	k1gNnSc6	studio
politologie	politologie	k1gFnSc1	politologie
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
sektoru	sektor	k1gInSc6	sektor
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
pro	pro	k7c4	pro
vládní	vládní	k2eAgFnSc4d1	vládní
administrativu	administrativa	k1gFnSc4	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Jamaguči	Jamaguč	k1gFnSc6	Jamaguč
<g/>
.	.	kIx.	.
</s>
<s>
Abe	Abe	k?	Abe
sloužil	sloužit	k5eAaImAgInS	sloužit
pod	pod	k7c7	pod
premiéry	premiér	k1gMnPc7	premiér
Joširó	Joširó	k1gMnSc1	Joširó
Morim	Morim	k1gMnSc1	Morim
a	a	k8xC	a
Džuničiró	Džuničiró	k1gMnSc1	Džuničiró
Koizumim	Koizumim	k1gMnSc1	Koizumim
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Koizumiho	Koizumi	k1gMnSc2	Koizumi
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vládním	vládní	k2eAgInSc7d1	vládní
tajemníkem	tajemník	k1gInSc7	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Abe	Abe	k?	Abe
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c4	v
povědomí	povědomí	k1gNnSc4	povědomí
Japonců	Japonec	k1gMnPc2	Japonec
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
silný	silný	k2eAgInSc4d1	silný
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zastával	zastávat	k5eAaImAgInS	zastávat
vůči	vůči	k7c3	vůči
Severní	severní	k2eAgFnSc3d1	severní
Koreji	Korea	k1gFnSc3	Korea
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gNnSc4	on
nakonec	nakonec	k6eAd1	nakonec
vyneslo	vynést	k5eAaPmAgNnS	vynést
až	až	k9	až
do	do	k7c2	do
předsednictví	předsednictví	k1gNnSc2	předsednictví
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
Liberálně	liberálně	k6eAd1	liberálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
LDP	LDP	kA	LDP
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Abe	Abe	k1gMnSc1	Abe
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
politiku	politika	k1gFnSc4	politika
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
napjaté	napjatý	k2eAgInPc4d1	napjatý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
90	[number]	k4	90
<g/>
.	.	kIx.	.
předsedou	předseda	k1gMnSc7	předseda
japonské	japonský	k2eAgFnSc2d1	japonská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c6	na
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
zasedání	zasedání	k1gNnSc6	zasedání
parlamentu	parlament	k1gInSc2	parlament
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
japonským	japonský	k2eAgMnSc7d1	japonský
premiérem	premiér	k1gMnSc7	premiér
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
však	však	k9	však
náhle	náhle	k6eAd1	náhle
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
po	po	k7c6	po
měsících	měsíc	k1gInPc6	měsíc
rostoucího	rostoucí	k2eAgInSc2d1	rostoucí
politického	politický	k2eAgInSc2d1	politický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
Jasuo	Jasuo	k1gMnSc1	Jasuo
Fukuda	Fukuda	k1gMnSc1	Fukuda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
91	[number]	k4	91
<g/>
.	.	kIx.	.
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
japonské	japonský	k2eAgFnSc2d1	japonská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
Abe	Abe	k1gMnSc1	Abe
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jím	jíst	k5eAaImIp1nS	jíst
vedená	vedený	k2eAgFnSc1d1	vedená
Liberálně	liberálně	k6eAd1	liberálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
předčasné	předčasný	k2eAgFnPc4d1	předčasná
prosincové	prosincový	k2eAgFnPc4d1	prosincová
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Liberální	liberální	k2eAgMnPc1d1	liberální
demokraté	demokrat	k1gMnPc1	demokrat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
vlády	vláda	k1gFnSc2	vláda
vrátili	vrátit	k5eAaPmAgMnP	vrátit
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jím	jíst	k5eAaImIp1nS	jíst
provedená	provedený	k2eAgFnSc1d1	provedená
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
opatření	opatření	k1gNnSc1	opatření
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
Abenomika	Abenomika	k1gFnSc1	Abenomika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
oznámil	oznámit	k5eAaPmAgMnS	oznámit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
záměr	záměr	k1gInSc1	záměr
rozpustit	rozpustit	k5eAaPmF	rozpustit
vládu	vláda	k1gFnSc4	vláda
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vedením	vedení	k1gNnSc7	vedení
a	a	k8xC	a
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
předčasné	předčasný	k2eAgFnPc4d1	předčasná
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
dojíti	dojít	k5eAaPmF	dojít
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
bude	být	k5eAaImBp3nS	být
čelit	čelit	k5eAaImF	čelit
guvernérce	guvernérka	k1gFnSc3	guvernérka
Tokia	Tokio	k1gNnSc2	Tokio
Jurice	Jurice	k1gFnSc2	Jurice
Koikeové	Koikeová	k1gFnSc2	Koikeová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
premiérů	premiér	k1gMnPc2	premiér
Japonska	Japonsko	k1gNnSc2	Japonsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šinzó	Šinzó	k1gFnSc2	Šinzó
Abe	Abe	k1gFnSc2	Abe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Šinzó	Šinzó	k1gFnSc1	Šinzó
Abe	Abe	k1gFnSc1	Abe
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
