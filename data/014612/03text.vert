<s>
Lípa	lípa	k1gFnSc1
na	na	k7c4
Krčmově	krčmově	k6eAd1
</s>
<s>
Památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
(	(	kIx(
<g/>
chráněný	chráněný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
Lípa	lípa	k1gFnSc1
na	na	k7c4
Krčmově	krčmově	k6eAd1
Lípa	lípa	k1gFnSc1
na	na	k7c4
KrčmověDruh	KrčmověDruh	k1gInSc4
</s>
<s>
lípa	lípa	k1gFnSc1
malolistáTilia	malolistáTilium	k1gNnSc2
platyphyllos	platyphyllosa	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Evidenční	evidenční	k2eAgFnSc1d1
č.	č.	k?
</s>
<s>
101480	#num#	k4
(	(	kIx(
<g/>
8280	#num#	k4
<g/>
)	)	kIx)
Ochrana	ochrana	k1gFnSc1
</s>
<s>
od	od	k7c2
května	květen	k1gInSc2
2001	#num#	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Náchod	Náchod	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Adršpach	Adršpach	k1gInSc1
Katastr	katastr	k1gInSc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
Adršpach	Adršpach	k1gInSc1
Poloha	poloha	k1gFnSc1
</s>
<s>
na	na	k7c6
louce	louka	k1gFnSc6
u	u	k7c2
křižovatky	křižovatka	k1gFnSc2
na	na	k7c4
Krčmově	krčmově	k6eAd1
nad	nad	k7c7
Horním	horní	k2eAgInSc7d1
Adršpachem	Adršpach	k1gInSc7
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
591	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
12,12	12,12	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
7,94	7,94	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Lípa	lípa	k1gFnSc1
na	na	k7c4
Krčmově	krčmově	k6eAd1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
na	na	k7c6
Krčmově	Krčmov	k1gInSc6
je	být	k5eAaImIp3nS
památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
–	–	kIx~
lípa	lípa	k1gFnSc1
malolistá	malolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
<g/>
)	)	kIx)
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Horní	horní	k2eAgInSc1d1
Adršpach	Adršpach	k1gInSc1
(	(	kIx(
<g/>
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Adršpach	Adršpach	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strom	strom	k1gInSc1
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
v	v	k7c6
louce	louka	k1gFnSc6
u	u	k7c2
křižovatky	křižovatka	k1gFnSc2
na	na	k7c4
Krčmově	krčmově	k6eAd1
nad	nad	k7c7
obcí	obec	k1gFnSc7
Adršpach	Adršpach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášena	vyhlášen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
vzrůst	vzrůst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
číslo	číslo	k1gNnSc1
seznamu	seznam	k1gInSc2
<g/>
:	:	kIx,
605042.1	605042.1	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
</s>
<s>
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
390	#num#	k4
cm	cm	kA
</s>
<s>
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
21	#num#	k4
m	m	kA
</s>
<s>
věk	věk	k1gInSc1
<g/>
:	:	kIx,
90	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Registr	registr	k1gInSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
AOPK	AOPK	kA
ČR	ČR	kA
drusop	drusop	k1gInSc4
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Strom	strom	k1gInSc1
v	v	k7c6
evidenci	evidence	k1gFnSc6
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
Agentury	agentura	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Stromy	strom	k1gInPc1
</s>
