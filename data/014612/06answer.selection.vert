<s>
Lípa	lípa	k1gFnSc1
na	na	k7c6
Krčmově	Krčmov	k1gInSc6
je	být	k5eAaImIp3nS
památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
–	–	kIx~
lípa	lípa	k1gFnSc1
malolistá	malolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
<g/>
)	)	kIx)
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Horní	horní	k2eAgInSc1d1
Adršpach	Adršpach	k1gInSc1
(	(	kIx(
<g/>
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Adršpach	Adršpach	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>