<p>
<s>
Fez	fez	k1gInSc1	fez
je	být	k5eAaImIp3nS	být
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
nošená	nošený	k2eAgFnSc1d1	nošená
především	především	k9	především
v	v	k7c6	v
islámských	islámský	k2eAgFnPc6d1	islámská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgMnSc1d1	mužský
má	mít	k5eAaImIp3nS	mít
černý	černý	k2eAgInSc1d1	černý
nebo	nebo	k8xC	nebo
modrý	modrý	k2eAgInSc1d1	modrý
střapec	střapec	k1gInSc1	střapec
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
je	být	k5eAaImIp3nS	být
zdobený	zdobený	k2eAgInSc1d1	zdobený
perlami	perla	k1gFnPc7	perla
nebo	nebo	k8xC	nebo
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
fez	fez	k1gInSc1	fez
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
města	město	k1gNnSc2	město
Fè	Fè	k1gFnSc2	Fè
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Fes	fes	k1gNnSc2	fes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fez	fez	k1gInSc1	fez
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
rakousko-uherské	rakouskoherský	k2eAgFnSc6d1	rakousko-uherská
armádě	armáda	k1gFnSc6	armáda
u	u	k7c2	u
bosensko-hercegovinských	bosenskoercegovinský	k2eAgMnPc2d1	bosensko-hercegovinský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
poddůstojníků	poddůstojník	k1gMnPc2	poddůstojník
(	(	kIx(	(
<g/>
důstojníci	důstojník	k1gMnPc1	důstojník
to	ten	k3xDgNnSc4	ten
měli	mít	k5eAaImAgMnP	mít
nepovinné	povinný	k2eNgInPc4d1	nepovinný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
Tonaku	tonak	k1gInSc6	tonak
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
(	(	kIx(	(
<g/>
bývalé	bývalý	k2eAgNnSc1d1	bývalé
Fezko	Fezko	k1gNnSc1	Fezko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
</s>
</p>
<p>
<s>
Kipa	Kipa	k6eAd1	Kipa
</s>
</p>
<p>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
</s>
</p>
<p>
<s>
Čepice	čepice	k1gFnSc1	čepice
</s>
</p>
<p>
<s>
Tonak	tonak	k1gInSc1	tonak
</s>
</p>
<p>
<s>
Fezko	Fezko	k6eAd1	Fezko
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
fez	fez	k1gInSc1	fez
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Fez	fez	k1gInSc1	fez
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Fezzes	Fezzes	k1gInSc1	Fezzes
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
TONAK	tonak	k1gInSc1	tonak
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Fezko	Fezko	k1gNnSc1	Fezko
Thierry	Thierra	k1gFnSc2	Thierra
a.s.	a.s.	k?	a.s.
</s>
</p>
