<p>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1289	[number]	k4	1289
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sedmý	sedmý	k4xOgMnSc1	sedmý
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
polský	polský	k2eAgMnSc1d1	polský
(	(	kIx(	(
<g/>
1305	[number]	k4	1305
<g/>
–	–	k?	–
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
(	(	kIx(	(
<g/>
1301	[number]	k4	1301
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
druhorozeným	druhorozený	k2eAgMnSc7d1	druhorozený
synem	syn	k1gMnSc7	syn
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc4d1	český
a	a	k8xC	a
polského	polský	k2eAgMnSc4d1	polský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
Guty	Guta	k1gFnPc1	Guta
Habsburské	habsburský	k2eAgFnPc1d1	habsburská
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnPc1	dcera
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Rudolfa	Rudolf	k1gMnSc2	Rudolf
I.	I.	kA	I.
Habsburského	habsburský	k2eAgInSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Anežka	Anežka	k1gFnSc1	Anežka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Přeživšími	přeživší	k2eAgMnPc7d1	přeživší
sourozenci	sourozenec	k1gMnPc7	sourozenec
byli	být	k5eAaImAgMnP	být
Anna	Anna	k1gFnSc1	Anna
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
a	a	k8xC	a
polorodá	polorodý	k2eAgFnSc1d1	polorodá
sestra	sestra	k1gFnSc1	sestra
Anežka	Anežka	k1gFnSc1	Anežka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uherská	uherský	k2eAgFnSc1d1	uherská
koruna	koruna	k1gFnSc1	koruna
==	==	k?	==
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1298	[number]	k4	1298
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
zasnouben	zasnoubit	k5eAaPmNgMnS	zasnoubit
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
dcerou	dcera	k1gFnSc7	dcera
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Ondřej	Ondřej	k1gMnSc1	Ondřej
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nedočkal	dočkat	k5eNaPmAgInS	dočkat
dalšího	další	k2eAgNnSc2d1	další
potomstva	potomstvo	k1gNnSc2	potomstvo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1301	[number]	k4	1301
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Arpádovců	Arpádovec	k1gMnPc2	Arpádovec
jím	jíst	k5eAaImIp1nS	jíst
vymřel	vymřít	k5eAaPmAgMnS	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
najít	najít	k5eAaPmF	najít
nového	nový	k2eAgMnSc4d1	nový
panovníka	panovník	k1gMnSc4	panovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
těle	tělo	k1gNnSc6	tělo
by	by	kYmCp3nS	by
kolovala	kolovat	k5eAaImAgFnS	kolovat
arpádovská	arpádovský	k2eAgFnSc1d1	arpádovský
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
</s>
<s>
Uherští	uherský	k2eAgMnPc1d1	uherský
magnáti	magnát	k1gMnPc1	magnát
nechtěli	chtít	k5eNaImAgMnP	chtít
přijmout	přijmout	k5eAaPmF	přijmout
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
a	a	k8xC	a
hledali	hledat	k5eAaImAgMnP	hledat
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadal	připadat	k5eAaImAgInS	připadat
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dolnobavorský	Dolnobavorský	k2eAgMnSc1d1	Dolnobavorský
<g/>
,	,	kIx,	,
Arpádovec	Arpádovec	k1gMnSc1	Arpádovec
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
Haličské	haličský	k2eAgFnSc2d1	Haličská
a	a	k8xC	a
snoubenec	snoubenec	k1gMnSc1	snoubenec
desetileté	desetiletý	k2eAgFnSc2d1	desetiletá
osiřelé	osiřelý	k2eAgFnSc2d1	osiřelá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
bohatství	bohatství	k1gNnSc1	bohatství
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
královské	královský	k2eAgFnSc2d1	královská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
uherského	uherský	k2eAgInSc2d1	uherský
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
uherští	uherský	k2eAgMnPc1d1	uherský
páni	pan	k1gMnPc1	pan
byli	být	k5eAaImAgMnP	být
uplaceni	uplatit	k5eAaPmNgMnP	uplatit
kutnohorským	kutnohorský	k2eAgNnSc7d1	kutnohorské
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1301	[number]	k4	1301
byl	být	k5eAaImAgMnS	být
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
jednáních	jednání	k1gNnPc6	jednání
ve	v	k7c6	v
Stoličném	stoličný	k1gMnSc6	stoličný
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
korunován	korunovat	k5eAaBmNgMnS	korunovat
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
a	a	k8xC	a
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
uherských	uherský	k2eAgMnPc2d1	uherský
králů	král	k1gMnPc2	král
získal	získat	k5eAaPmAgInS	získat
jméno	jméno	k1gNnSc4	jméno
Ladislav	Ladislava	k1gFnPc2	Ladislava
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Faktický	faktický	k2eAgInSc1d1	faktický
vliv	vliv	k1gInSc1	vliv
však	však	k9	však
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
mladičký	mladičký	k2eAgMnSc1d1	mladičký
král	král	k1gMnSc1	král
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
září	září	k1gNnSc6	září
1301	[number]	k4	1301
se	se	k3xPyFc4	se
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
objevil	objevit	k5eAaPmAgMnS	objevit
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Boccasini	Boccasin	k2eAgMnPc1d1	Boccasin
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XI	XI	kA	XI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zastupující	zastupující	k2eAgInPc1d1	zastupující
zájmy	zájem	k1gInPc1	zájem
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
a	a	k8xC	a
tlumočící	tlumočící	k2eAgInSc4d1	tlumočící
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
uherském	uherský	k2eAgMnSc6d1	uherský
králi	král	k1gMnSc6	král
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Václava	Václav	k1gMnSc4	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
osočil	osočit	k5eAaPmAgMnS	osočit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neprávem	neprávo	k1gNnSc7	neprávo
zve	zvát	k5eAaImIp3nS	zvát
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jej	on	k3xPp3gMnSc4	on
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
předložil	předložit	k5eAaPmAgMnS	předložit
doklady	doklad	k1gInPc4	doklad
synových	synův	k2eAgInPc2d1	synův
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
svatoštěpánskou	svatoštěpánský	k2eAgFnSc4d1	Svatoštěpánská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
i	i	k9	i
Anjouovce	Anjouovec	k1gMnSc4	Anjouovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opozice	opozice	k1gFnSc1	opozice
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
i	i	k8xC	i
Uhrách	Uhry	k1gFnPc6	Uhry
proti	proti	k7c3	proti
přemyslovskému	přemyslovský	k2eAgNnSc3d1	přemyslovské
soustátí	soustátí	k1gNnSc3	soustátí
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
král	král	k1gMnSc1	král
Albrecht	Albrecht	k1gMnSc1	Albrecht
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
papeže	papež	k1gMnSc2	papež
Bonifáce	Bonifác	k1gMnSc2	Bonifác
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
eventuálně	eventuálně	k6eAd1	eventuálně
mohl	moct	k5eAaImAgMnS	moct
udělat	udělat	k5eAaPmF	udělat
císaře	císař	k1gMnSc4	císař
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
byli	být	k5eAaImAgMnP	být
dosud	dosud	k6eAd1	dosud
spojenci	spojenec	k1gMnPc1	spojenec
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
koruně	koruna	k1gFnSc3	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
podporu	podpora	k1gFnSc4	podpora
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
Sličného	sličný	k2eAgMnSc2d1	sličný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
neměl	mít	k5eNaImAgMnS	mít
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1303	[number]	k4	1303
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
Bonifác	Bonifác	k1gMnSc1	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
svatoštěpánskou	svatoštěpánský	k2eAgFnSc4d1	Svatoštěpánská
korunu	koruna	k1gFnSc4	koruna
Karlu	Karel	k1gMnSc3	Karel
Robertovi	Robert	k1gMnSc3	Robert
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
poddané	poddaná	k1gFnPc1	poddaná
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
zprostil	zprostit	k5eAaPmAgMnS	zprostit
přísah	přísaha	k1gFnPc2	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
samozřejmě	samozřejmě	k6eAd1	samozřejmě
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
začal	začít	k5eAaPmAgInS	začít
rychle	rychle	k6eAd1	rychle
ztrácet	ztrácet	k5eAaImF	ztrácet
podporu	podpora	k1gFnSc4	podpora
šlechty	šlechta	k1gFnSc2	šlechta
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Karla	Karel	k1gMnSc2	Karel
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc4d1	Anjý
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zůstal	zůstat	k5eAaPmAgMnS	zůstat
obležený	obležený	k2eAgMnSc1d1	obležený
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
ho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
musel	muset	k5eAaImAgMnS	muset
vojensky	vojensky	k6eAd1	vojensky
hájit	hájit	k5eAaImF	hájit
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
za	za	k7c2	za
otcovy	otcův	k2eAgFnSc2d1	otcova
vydatné	vydatný	k2eAgFnSc2d1	vydatná
armádní	armádní	k2eAgFnSc2d1	armádní
pomoci	pomoc	k1gFnSc2	pomoc
odjel	odjet	k5eAaPmAgMnS	odjet
i	i	k9	i
s	s	k7c7	s
uherskou	uherský	k2eAgFnSc7d1	uherská
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
rukojmími	rukojmí	k1gMnPc7	rukojmí
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
vládli	vládnout	k5eAaImAgMnP	vládnout
území	území	k1gNnSc4	území
většímu	veliký	k2eAgInSc3d2	veliký
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
<g/>
,	,	kIx,	,
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
moc	moc	k1gFnSc1	moc
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otka	k1gFnSc6	otka
roku	rok	k1gInSc2	rok
1305	[number]	k4	1305
zdědil	zdědit	k5eAaPmAgMnS	zdědit
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
polskou	polský	k2eAgFnSc4d1	polská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
korunami	koruna	k1gFnPc7	koruna
ovšem	ovšem	k9	ovšem
stál	stát	k5eAaImAgInS	stát
Albrecht	Albrecht	k1gMnSc1	Albrecht
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc1d1	habsburský
v	v	k7c4	v
říši	říše	k1gFnSc4	říše
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
král	král	k1gMnSc1	král
zrušil	zrušit	k5eAaPmAgMnS	zrušit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1305	[number]	k4	1305
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
s	s	k7c7	s
Violou	Viola	k1gFnSc7	Viola
Těšínskou	Těšínská	k1gFnSc7	Těšínská
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nerovný	rovný	k2eNgInSc4d1	nerovný
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
důvody	důvod	k1gInPc1	důvod
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
pozdější	pozdní	k2eAgMnPc1d2	pozdější
kronikáři	kronikář	k1gMnPc1	kronikář
popisují	popisovat	k5eAaImIp3nP	popisovat
Violu	Viola	k1gFnSc4	Viola
jako	jako	k8xC	jako
půvabnou	půvabný	k2eAgFnSc4d1	půvabná
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
zřejmě	zřejmě	k6eAd1	zřejmě
nebyla	být	k5eNaImAgFnS	být
právě	právě	k9	právě
její	její	k3xOp3gFnSc1	její
krása	krása	k1gFnSc1	krása
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
sehrála	sehrát	k5eAaPmAgFnS	sehrát
roli	role	k1gFnSc4	role
strategická	strategický	k2eAgFnSc1d1	strategická
poloha	poloha	k1gFnSc1	poloha
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
pod	pod	k7c7	pod
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
kuratelou	kuratela	k1gFnSc7	kuratela
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Václav	Václav	k1gMnSc1	Václav
ještě	ještě	k9	ještě
použil	použít	k5eAaPmAgMnS	použít
titul	titul	k1gInSc4	titul
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
v	v	k7c6	v
Bruntále	Bruntál	k1gInSc6	Bruntál
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
titulu	titul	k1gInSc3	titul
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
uherskými	uherský	k2eAgInPc7d1	uherský
korunovačními	korunovační	k2eAgInPc7d1	korunovační
klenoty	klenot	k1gInPc7	klenot
jej	on	k3xPp3gMnSc4	on
předal	předat	k5eAaPmAgMnS	předat
bratranci	bratranec	k1gMnSc3	bratranec
Otovi	Ota	k1gMnSc3	Ota
Dolnobavorskému	Dolnobavorský	k2eAgMnSc3d1	Dolnobavorský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Albrechtem	Albrecht	k1gMnSc7	Albrecht
Habsburským	habsburský	k2eAgMnSc7d1	habsburský
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
postoupil	postoupit	k5eAaPmAgInS	postoupit
některá	některý	k3yIgNnPc1	některý
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
Chebsko	Chebsko	k1gNnSc1	Chebsko
<g/>
,	,	kIx,	,
Plísensko	Plísensko	k1gNnSc1	Plísensko
a	a	k8xC	a
Míšeňsko	Míšeňsko	k1gNnSc1	Míšeňsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
ruce	ruka	k1gFnPc4	ruka
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
polské	polský	k2eAgFnSc2d1	polská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1306	[number]	k4	1306
<g/>
,	,	kIx,	,
snad	snad	k9	snad
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
února	únor	k1gInSc2	únor
či	či	k8xC	či
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Lokýtek	lokýtek	k1gInSc1	lokýtek
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
držený	držený	k2eAgInSc1d1	držený
českou	český	k2eAgFnSc7d1	Česká
posádkou	posádka	k1gFnSc7	posádka
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1306	[number]	k4	1306
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
v	v	k7c6	v
Toruni	Toruň	k1gFnSc6	Toruň
s	s	k7c7	s
"	"	kIx"	"
<g/>
plnomocníky	plnomocník	k1gMnPc7	plnomocník
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
"	"	kIx"	"
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
trvat	trvat	k5eAaImF	trvat
"	"	kIx"	"
<g/>
až	až	k9	až
do	do	k7c2	do
sv.	sv.	kA	sv.
Michala	Michala	k1gFnSc1	Michala
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
král	král	k1gMnSc1	král
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
1306	[number]	k4	1306
sbírat	sbírat	k5eAaImF	sbírat
zemskou	zemský	k2eAgFnSc4d1	zemská
hotovost	hotovost	k1gFnSc4	hotovost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
5	[number]	k4	5
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
(	(	kIx(	(
<g/>
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
ještě	ještě	k9	ještě
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vydal	vydat	k5eAaPmAgMnS	vydat
listinu	listina	k1gFnSc4	listina
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pražského	pražský	k2eAgMnSc2d1	pražský
měšťana	měšťan	k1gMnSc2	měšťan
a	a	k8xC	a
kutnohorského	kutnohorský	k2eAgMnSc2d1	kutnohorský
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
)	)	kIx)	)
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
dne	den	k1gInSc2	den
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
;	;	kIx,	;
jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
po	po	k7c6	po
poledni	poledne	k1gNnSc6	poledne
odpočíval	odpočívat	k5eAaImAgMnS	odpočívat
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
v	v	k7c6	v
domě	dům	k1gInSc6	dům
bývalého	bývalý	k2eAgNnSc2d1	bývalé
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
děkanství	děkanství	k1gNnSc2	děkanství
(	(	kIx(	(
<g/>
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
dnešního	dnešní	k2eAgInSc2d1	dnešní
dómu	dóm	k1gInSc2	dóm
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zabit	zabit	k2eAgMnSc1d1	zabit
-	-	kIx~	-
neznámo	neznámo	k1gNnSc1	neznámo
kým	kdo	k3yInSc7	kdo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vina	vina	k1gFnSc1	vina
dlouho	dlouho	k6eAd1	dlouho
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
jistém	jistý	k2eAgMnSc6d1	jistý
Konrádovi	Konrád	k1gMnSc6	Konrád
z	z	k7c2	z
Botenštejna	Botenštejno	k1gNnSc2	Botenštejno
-	-	kIx~	-
třemi	tři	k4xCgFnPc7	tři
ranami	rána	k1gFnPc7	rána
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
necelých	celý	k2eNgNnPc6d1	necelé
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
dožil	dožít	k5eAaPmAgMnS	dožít
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
věku	věk	k1gInSc2	věk
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgMnPc7	svůj
současníky	současník	k1gMnPc7	současník
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
vysoko	vysoko	k6eAd1	vysoko
ceněn	cenit	k5eAaImNgMnS	cenit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
kronika	kronika	k1gFnSc1	kronika
o	o	k7c6	o
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
královský	královský	k2eAgInSc4d1	královský
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
rozmařilý	rozmařilý	k2eAgInSc1d1	rozmařilý
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
autor	autor	k1gMnSc1	autor
kroniky	kronika	k1gFnSc2	kronika
k	k	k7c3	k
Václavovi	Václav	k1gMnSc3	Václav
příliš	příliš	k6eAd1	příliš
kritický	kritický	k2eAgInSc1d1	kritický
-	-	kIx~	-
bujaré	bujarý	k2eAgFnSc2d1	bujará
pitky	pitka	k1gFnSc2	pitka
pořádal	pořádat	k5eAaImAgMnS	pořádat
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
však	však	k9	však
kronikář	kronikář	k1gMnSc1	kronikář
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
krátká	krátký	k2eAgFnSc1d1	krátká
vláda	vláda	k1gFnSc1	vláda
vcelku	vcelku	k6eAd1	vcelku
rozumná	rozumný	k2eAgFnSc1d1	rozumná
a	a	k8xC	a
prozíravá	prozíravý	k2eAgFnSc1d1	prozíravá
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
přinést	přinést	k5eAaPmF	přinést
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
výsledky	výsledek	k1gInPc7	výsledek
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
už	už	k6eAd1	už
nestihl	stihnout	k5eNaPmAgMnS	stihnout
dopracovat	dopracovat	k5eAaPmF	dopracovat
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
pověst	pověst	k1gFnSc1	pověst
opilce	opilec	k1gMnSc2	opilec
a	a	k8xC	a
povaleče	povaleč	k1gMnSc2	povaleč
však	však	k9	však
už	už	k6eAd1	už
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
III	III	kA	III
<g/>
.	.	kIx.	.
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
==	==	k?	==
</s>
</p>
<p>
<s>
Vražda	vražda	k1gFnSc1	vražda
šestnáctiletého	šestnáctiletý	k2eAgMnSc2d1	šestnáctiletý
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
vymřela	vymřít	k5eAaPmAgFnS	vymřít
legitimní	legitimní	k2eAgFnSc1d1	legitimní
větev	větev	k1gFnSc1	větev
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
neobjasněná	objasněný	k2eNgFnSc1d1	neobjasněná
<g/>
,	,	kIx,	,
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Botenštejna	Botenštejn	k1gInSc2	Botenštejn
(	(	kIx(	(
<g/>
z	z	k7c2	z
Mulhova	Mulhův	k2eAgInSc2d1	Mulhův
<g/>
)	)	kIx)	)
vrahem	vrah	k1gMnSc7	vrah
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgMnSc1d1	pravý
vrah	vrah	k1gMnSc1	vrah
možná	možná	k9	možná
podplatil	podplatit	k5eAaPmAgMnS	podplatit
stráže	stráž	k1gFnPc4	stráž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Konráda	Konrád	k1gMnSc2	Konrád
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
snad	snad	k9	snad
chtěl	chtít	k5eAaImAgMnS	chtít
ohlásit	ohlásit	k5eAaPmF	ohlásit
vraždu	vražda	k1gFnSc4	vražda
<g/>
)	)	kIx)	)
ubili	ubít	k5eAaPmAgMnP	ubít
<g/>
.	.	kIx.	.
</s>
<s>
Strážím	stráž	k1gFnPc3	stráž
snad	snad	k9	snad
stačilo	stačit	k5eAaBmAgNnS	stačit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
viděli	vidět	k5eAaImAgMnP	vidět
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
iniciátory	iniciátor	k1gMnPc4	iniciátor
vraždy	vražda	k1gFnSc2	vražda
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
označováni	označován	k2eAgMnPc1d1	označován
Habsburkové	Habsburk	k1gMnPc1	Habsburk
nebo	nebo	k8xC	nebo
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
z	z	k7c2	z
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
smrti	smrt	k1gFnSc2	smrt
těžili	těžit	k5eAaImAgMnP	těžit
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
naopak	naopak	k6eAd1	naopak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
historiky	historik	k1gMnPc7	historik
spíše	spíše	k9	spíše
převládá	převládat	k5eAaImIp3nS	převládat
názor	názor	k1gInSc4	názor
o	o	k7c6	o
jakési	jakýsi	k3yIgFnSc6	jakýsi
kolektivní	kolektivní	k2eAgFnSc6d1	kolektivní
vině	vina	k1gFnSc6	vina
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
promyšlenou	promyšlený	k2eAgFnSc4d1	promyšlená
akci	akce	k1gFnSc4	akce
určité	určitý	k2eAgFnSc2d1	určitá
zájmové	zájmový	k2eAgFnSc2d1	zájmová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
1306	[number]	k4	1306
<g/>
-	-	kIx~	-
<g/>
1326	[number]	k4	1326
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jej	on	k3xPp3gInSc4	on
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
nechala	nechat	k5eAaPmAgFnS	nechat
sestra	sestra	k1gFnSc1	sestra
Eliška	Eliška	k1gFnSc1	Eliška
převézt	převézt	k5eAaPmF	převézt
do	do	k7c2	do
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
uložit	uložit	k5eAaPmF	uložit
vedle	vedle	k7c2	vedle
ostatků	ostatek	k1gInPc2	ostatek
otce	otec	k1gMnSc2	otec
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovské	přemyslovský	k2eAgInPc1d1	přemyslovský
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
se	se	k3xPyFc4	se
však	však	k9	však
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
,	,	kIx,	,
kostra	kostra	k1gFnSc1	kostra
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
dobových	dobový	k2eAgNnPc2d1	dobové
zobrazení	zobrazení	k1gNnPc2	zobrazení
mladého	mladý	k2eAgMnSc2d1	mladý
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
kroniky	kronika	k1gFnSc2	kronika
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
obrázkové	obrázkový	k2eAgFnSc2d1	obrázková
kroniky	kronika	k1gFnSc2	kronika
je	být	k5eAaImIp3nS	být
panovník	panovník	k1gMnSc1	panovník
zpodobněn	zpodobnit	k5eAaPmNgMnS	zpodobnit
jako	jako	k9	jako
klečící	klečící	k2eAgMnSc1d1	klečící
pokorný	pokorný	k2eAgMnSc1d1	pokorný
prosebník	prosebník	k1gMnSc1	prosebník
i	i	k8xC	i
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
rytinách	rytina	k1gFnPc6	rytina
relikviáře	relikviář	k1gInSc2	relikviář
sv.	sv.	kA	sv.
Blažeje	Blažej	k1gMnSc2	Blažej
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
uchováván	uchovávat	k5eAaImNgMnS	uchovávat
v	v	k7c6	v
Escorialu	Escorial	k1gInSc6	Escorial
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgMnPc7	tři
na	na	k7c4	na
Václavovu	Václavův	k2eAgFnSc4d1	Václavova
zakázku	zakázka	k1gFnSc4	zakázka
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
relikviáři	relikviář	k1gInPc7	relikviář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
kroniky	kronika	k1gFnPc1	kronika
koruny	koruna	k1gFnSc2	koruna
uherské	uherský	k2eAgNnSc1d1	Uherské
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Richard	Richard	k1gMnSc1	Richard
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Dagmar	Dagmar	k1gFnSc1	Dagmar
Bartoňková	Bartoňková	k1gFnSc1	Bartoňková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Nechutová	Nechutový	k2eAgFnSc1d1	Nechutová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
389	[number]	k4	389
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
František	františek	k1gInSc1	františek
Heřmanský	Heřmanský	k2eAgInSc1d1	Heřmanský
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
597	[number]	k4	597
s.	s.	k?	s.
</s>
</p>
<p>
<s>
HÁDEK	hádek	k1gMnSc1	hádek
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7304	[number]	k4	7304
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
67	[number]	k4	67
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IVANOV	IVANOV	kA	IVANOV
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
pitaval	pitaval	k1gInSc1	pitaval
aneb	aneb	k?	aneb
Kralovraždy	kralovražda	k1gFnSc2	kralovražda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
300	[number]	k4	300
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7388	[number]	k4	7388
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MARÁZ	MARÁZ	kA	MARÁZ
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
131	[number]	k4	131
<g/>
-	-	kIx~	-
<g/>
143	[number]	k4	143
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MARÁZ	MARÁZ	kA	MARÁZ
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1289	[number]	k4	1289
<g/>
-	-	kIx~	-
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
:	:	kIx,	:
poslední	poslední	k2eAgMnSc1d1	poslední
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MARÁZ	MARÁZ	kA	MARÁZ
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hodnostářům	hodnostář	k1gMnPc3	hodnostář
a	a	k8xC	a
úředníkům	úředník	k1gMnPc3	úředník
uherského	uherský	k2eAgInSc2d1	uherský
(	(	kIx(	(
<g/>
1301	[number]	k4	1301
<g/>
-	-	kIx~	-
<g/>
1304	[number]	k4	1304
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
polského	polský	k2eAgInSc2d1	polský
(	(	kIx(	(
<g/>
1305	[number]	k4	1305
<g/>
-	-	kIx~	-
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Mediaevalia	Mediaevalia	k1gFnSc1	Mediaevalia
historica	historica	k1gFnSc1	historica
Bohemica	Bohemica	k1gFnSc1	Bohemica
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
103	[number]	k4	103
<g/>
-	-	kIx~	-
<g/>
113	[number]	k4	113
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
979	[number]	k4	979
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MARÁZ	MARÁZ	kA	MARÁZ
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
osobnosti	osobnost	k1gFnSc6	osobnost
a	a	k8xC	a
vraždě	vražda	k1gFnSc6	vražda
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
FIFKOVÁ	FIFKOVÁ	kA	FIFKOVÁ
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Sága	sága	k1gFnSc1	sága
moravských	moravský	k2eAgMnPc2d1	moravský
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
:	:	kIx,	:
život	život	k1gInSc4	život
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
od	od	k7c2	od
XI	XI	kA	XI
<g/>
.	.	kIx.	.
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
a	a	k8xC	a
katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
Vlastivědným	vlastivědný	k2eAgNnSc7d1	Vlastivědné
muzeem	muzeum	k1gNnSc7	muzeum
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Muzeem	muzeum	k1gNnSc7	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
k	k	k7c3	k
700	[number]	k4	700
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
tragické	tragický	k2eAgFnSc2d1	tragická
smrti	smrt	k1gFnSc2	smrt
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
;	;	kIx,	;
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Vlastivědné	vlastivědný	k2eAgNnSc1d1	Vlastivědné
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
;	;	kIx,	;
Muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85037	[number]	k4	85037
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POJSL	POJSL	kA	POJSL
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
;	;	kIx,	;
ŘEHOLKA	ŘEHOLKA	kA	ŘEHOLKA
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
;	;	kIx,	;
SULITKOVÁ	SULITKOVÁ	kA	SULITKOVÁ
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Panovnická	panovnický	k2eAgFnSc1d1	panovnická
kancelář	kancelář	k1gFnSc1	kancelář
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
archivních	archivní	k2eAgFnPc2d1	archivní
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
261	[number]	k4	261
<g/>
-	-	kIx~	-
<g/>
365	[number]	k4	365
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5246	[number]	k4	5246
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOVADINA	SOVADINA	kA	SOVADINA
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
SMUTNÁ	Smutná	k1gFnSc1	Smutná
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
profesora	profesor	k1gMnSc2	profesor
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Šebánka	Šebánek	k1gMnSc2	Šebánek
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
..	..	k?	..
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
archiv	archiv	k1gInSc1	archiv
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
7225	[number]	k4	7225
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
155	[number]	k4	155
<g/>
-	-	kIx~	-
<g/>
167	[number]	k4	167
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠTĚPÁN	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
vraždy	vražda	k1gFnSc2	vražda
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc4	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
roku	rok	k1gInSc2	rok
1306	[number]	k4	1306
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Slezského	slezský	k2eAgNnSc2d1	Slezské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
B.	B.	kA	B.
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
,	,	kIx,	,
s.	s.	k?	s.
115	[number]	k4	115
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1211	[number]	k4	1211
<g/>
-	-	kIx~	-
<g/>
3131	[number]	k4	3131
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Soumrak	soumrak	k1gInSc1	soumrak
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
803	[number]	k4	803
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Kus	kus	k1gInSc1	kus
středověké	středověký	k2eAgFnSc2d1	středověká
historie	historie	k1gFnSc2	historie
našeho	náš	k3xOp1gInSc2	náš
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
538	[number]	k4	538
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
376	[number]	k4	376
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
III	III	kA	III
<g/>
.	.	kIx.	.
1250	[number]	k4	1250
<g/>
-	-	kIx~	-
<g/>
1310	[number]	k4	1310
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
760	[number]	k4	760
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
281	[number]	k4	281
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Václav	Václava	k1gFnPc2	Václava
III	III	kA	III
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Václav	Václav	k1gMnSc1	Václav
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Genealogie	genealogie	k1gFnSc1	genealogie
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
KDO	kdo	k3yRnSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
noc	noc	k1gFnSc1	noc
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
</s>
</p>
<p>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Botenštejna	Botenštejn	k1gInSc2	Botenštejn
</s>
</p>
<p>
<s>
Durynk	Durynk	k1gMnSc1	Durynk
zabil	zabít	k5eAaPmAgMnS	zabít
krále	král	k1gMnSc4	král
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
chemická	chemický	k2eAgFnSc1d1	chemická
válka	válka	k1gFnSc1	válka
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
pověst	pověst	k1gFnSc1	pověst
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zavraždění	zavraždění	k1gNnSc1	zavraždění
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
