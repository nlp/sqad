<p>
<s>
Kolibřík	kolibřík	k1gMnSc1	kolibřík
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Patagona	Patagona	k1gFnSc1	Patagona
gigas	gigas	k1gMnSc1	gigas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
délkou	délka	k1gFnSc7	délka
21,5	[number]	k4	21,5
cm	cm	kA	cm
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
mezi	mezi	k7c7	mezi
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
g	g	kA	g
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
kolibříkovitých	kolibříkovitý	k2eAgInPc2d1	kolibříkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
též	též	k9	též
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
představitele	představitel	k1gMnSc4	představitel
rodu	rod	k1gInSc2	rod
Patagona	Patagona	k1gFnSc1	Patagona
<g/>
.	.	kIx.	.
</s>
<s>
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
,	,	kIx,	,
spodina	spodina	k1gFnSc1	spodina
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
samců	samec	k1gInPc2	samec
oranžová	oranžový	k2eAgFnSc1d1	oranžová
a	a	k8xC	a
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
naoranžovělá	naoranžovělý	k2eAgFnSc1d1	naoranžovělá
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
suchých	suchý	k2eAgInPc6d1	suchý
otevřených	otevřený	k2eAgInPc6d1	otevřený
andských	andský	k2eAgInPc6d1	andský
lesích	les	k1gInPc6	les
a	a	k8xC	a
křovinatých	křovinatý	k2eAgInPc6d1	křovinatý
porostech	porost	k1gInPc6	porost
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c4	mezi
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
4300	[number]	k4	4300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
až	až	k9	až
po	po	k7c4	po
střed	střed	k1gInSc4	střed
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolibřík	kolibřík	k1gMnSc1	kolibřík
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kolibřík	kolibřík	k1gMnSc1	kolibřík
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc4	taxon
Patagona	Patagon	k1gMnSc2	Patagon
gigas	gigasa	k1gFnPc2	gigasa
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
