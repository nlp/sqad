<s>
Kolibřík	kolibřík	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Patagona	Patagona	k1gFnSc1
gigas	gigas	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
21,5	[number]	k4
cm	cm	kA
a	a	k8xC
hmotností	hmotnost	k1gFnSc7
mezi	mezi	k7c7
18	[number]	k4
<g/>
–	–	k?
<g/>
20	[number]	k4
g	g	kA
největším	veliký	k2eAgMnSc7d3
zástupcem	zástupce	k1gMnSc7
čeledi	čeleď	k1gFnSc2
kolibříkovitých	kolibříkovitý	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>