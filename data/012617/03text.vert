<p>
<s>
Žralok	žralok	k1gMnSc1	žralok
obrovský	obrovský	k2eAgMnSc1d1	obrovský
(	(	kIx(	(
<g/>
Rhincodon	Rhincodon	k1gInSc1	Rhincodon
typus	typus	k?	typus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
žralok	žralok	k1gMnSc1	žralok
velrybí	velrybí	k2eAgMnSc1d1	velrybí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
zástupce	zástupce	k1gMnSc1	zástupce
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnPc4	velikost
kolem	kolem	k7c2	kolem
osmi	osm	k4xCc2	osm
až	až	k9	až
devíti	devět	k4xCc2	devět
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
mimořádných	mimořádný	k2eAgInPc6d1	mimořádný
případech	případ	k1gInPc6	případ
okolo	okolo	k7c2	okolo
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
exemplářích	exemplář	k1gInPc6	exemplář
velkých	velký	k2eAgInPc6d1	velký
až	až	k8xS	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
skutečně	skutečně	k6eAd1	skutečně
věrohodně	věrohodně	k6eAd1	věrohodně
potvrzené	potvrzený	k2eAgNnSc1d1	potvrzené
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vyloučit	vyloučit	k5eAaPmF	vyloučit
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
věrohodným	věrohodný	k2eAgInSc7d1	věrohodný
údajem	údaj	k1gInSc7	údaj
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
míra	míra	k1gFnSc1	míra
18,8	[number]	k4	18,8
metru	metr	k1gInSc2	metr
pro	pro	k7c4	pro
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
chyceného	chycený	k2eAgInSc2d1	chycený
v	v	k7c6	v
Arabském	arabský	k2eAgNnSc6d1	arabské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
není	být	k5eNaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
planktonem	plankton	k1gInSc7	plankton
a	a	k8xC	a
malými	malý	k2eAgFnPc7d1	malá
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
oceánů	oceán	k1gInPc2	oceán
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
dožít	dožít	k5eAaPmF	dožít
60	[number]	k4	60
až	až	k9	až
150	[number]	k4	150
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alternativní	alternativní	k2eAgInPc1d1	alternativní
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Micristodus	Micristodus	k1gMnSc1	Micristodus
punctatus	punctatus	k1gMnSc1	punctatus
Gill	Gill	k1gMnSc1	Gill
<g/>
,	,	kIx,	,
1865	[number]	k4	1865
</s>
</p>
<p>
<s>
Rhicodon	Rhicodon	k1gMnSc1	Rhicodon
typus	typus	k?	typus
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
</s>
</p>
<p>
<s>
Rhineodon	Rhineodon	k1gMnSc1	Rhineodon
typus	typus	k?	typus
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
</s>
</p>
<p>
<s>
Rhiniodon	Rhiniodon	k1gInSc1	Rhiniodon
typus	typus	k?	typus
</s>
</p>
<p>
<s>
Rhinodon	Rhinodon	k1gMnSc1	Rhinodon
pentalineatus	pentalineatus	k1gMnSc1	pentalineatus
Kishinouye	Kishinouye	k1gFnSc1	Kishinouye
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
Rhinodon	Rhinodon	k1gMnSc1	Rhinodon
typicus	typicus	k1gMnSc1	typicus
Müller	Müller	k1gMnSc1	Müller
&	&	k?	&
Henle	Henle	k1gInSc1	Henle
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
</s>
</p>
<p>
<s>
Rhinodon	Rhinodon	k1gMnSc1	Rhinodon
typicus	typicus	k1gMnSc1	typicus
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
</s>
</p>
<p>
<s>
Rhinodon	Rhinodon	k1gInSc1	Rhinodon
typus	typus	k?	typus
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
největší	veliký	k2eAgFnSc1d3	veliký
paryba	paryba	k1gFnSc1	paryba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
má	mít	k5eAaImIp3nS	mít
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
zbarvení	zbarvení	k1gNnSc1	zbarvení
-	-	kIx~	-
světlé	světlý	k2eAgFnPc1d1	světlá
skvrny	skvrna	k1gFnPc1	skvrna
a	a	k8xC	a
pruhy	pruh	k1gInPc1	pruh
na	na	k7c6	na
tmavém	tmavý	k2eAgNnSc6d1	tmavé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kůže	kůže	k1gFnSc1	kůže
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tlustá	tlustý	k2eAgFnSc1d1	tlustá
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Relativně	relativně	k6eAd1	relativně
velkou	velký	k2eAgFnSc4d1	velká
tlamu	tlama	k1gFnSc4	tlama
má	mít	k5eAaImIp3nS	mít
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
napříč	napříč	k6eAd1	napříč
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
2	[number]	k4	2
metry	metr	k1gInPc4	metr
a	a	k8xC	a
vejde	vejít	k5eAaPmIp3nS	vejít
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
až	až	k9	až
5	[number]	k4	5
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
vzadu	vzadu	k6eAd1	vzadu
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
výrůstky	výrůstek	k1gInPc1	výrůstek
vytvářející	vytvářející	k2eAgInPc1d1	vytvářející
jakési	jakýsi	k3yIgNnSc1	jakýsi
"	"	kIx"	"
<g/>
síto	síto	k1gNnSc1	síto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
mořskou	mořský	k2eAgFnSc4d1	mořská
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žralok	žralok	k1gMnSc1	žralok
obrovský	obrovský	k2eAgMnSc1d1	obrovský
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
soudobým	soudobý	k2eAgMnSc7d1	soudobý
žralokem	žralok	k1gMnSc7	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
až	až	k9	až
20	[number]	k4	20
m	m	kA	m
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc4d3	veliký
změřený	změřený	k2eAgInSc4d1	změřený
exemplář	exemplář	k1gInSc4	exemplář
měřil	měřit	k5eAaImAgMnS	měřit
13,7	[number]	k4	13,7
m.	m.	k?	m.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
žraloků	žralok	k1gMnPc2	žralok
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
dobrý	dobrý	k2eAgMnSc1d1	dobrý
plavec	plavec	k1gMnSc1	plavec
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
okolo	okolo	k7c2	okolo
5	[number]	k4	5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
vejcoživorodý	vejcoživorodý	k2eAgMnSc1d1	vejcoživorodý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
samice	samice	k1gFnSc2	samice
rodí	rodit	k5eAaImIp3nS	rodit
i	i	k9	i
300	[number]	k4	300
mláďat	mládě	k1gNnPc2	mládě
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
okolo	okolo	k7c2	okolo
0,5	[number]	k4	0,5
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
volných	volný	k2eAgNnPc2d1	volné
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
tropického	tropický	k2eAgNnSc2d1	tropické
a	a	k8xC	a
subtropického	subtropický	k2eAgNnSc2d1	subtropické
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západ	západ	k1gInSc1	západ
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
:	:	kIx,	:
USA	USA	kA	USA
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Východ	východ	k1gInSc1	východ
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
:	:	kIx,	:
Senegal	Senegal	k1gInSc1	Senegal
-	-	kIx~	-
Angola	Angola	k1gFnSc1	Angola
</s>
</p>
<p>
<s>
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
východ	východ	k1gInSc1	východ
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
:	:	kIx,	:
Japonsko	Japonsko	k1gNnSc1	Japonsko
-	-	kIx~	-
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
</s>
</p>
<p>
<s>
Západ	západ	k1gInSc1	západ
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
:	:	kIx,	:
Mexiko	Mexiko	k1gNnSc1	Mexiko
-	-	kIx~	-
Chile	Chile	k1gNnSc1	Chile
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
žralok	žralok	k1gMnSc1	žralok
obrovský	obrovský	k2eAgMnSc1d1	obrovský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Rhincodon	Rhincodon	k1gInSc1	Rhincodon
typus	typus	k?	typus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
