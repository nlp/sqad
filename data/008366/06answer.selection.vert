<s>
Ostrov	ostrov	k1gInSc1	ostrov
Šikoku	Šikok	k1gInSc2	Šikok
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
四	四	k?	四
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čtyři	čtyři	k4xCgFnPc1	čtyři
provincie	provincie	k1gFnPc1	provincie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
obydlený	obydlený	k2eAgMnSc1d1	obydlený
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
japonských	japonský	k2eAgInPc2d1	japonský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
