<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Šikoku	Šikok	k1gInSc2	Šikok
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
四	四	k?	四
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čtyři	čtyři	k4xCgFnPc1	čtyři
provincie	provincie	k1gFnPc1	provincie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
obydlený	obydlený	k2eAgMnSc1d1	obydlený
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
japonských	japonský	k2eAgInPc2d1	japonský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
starobylé	starobylý	k2eAgInPc4d1	starobylý
názvy	název	k1gInPc4	název
ostrova	ostrov	k1gInSc2	ostrov
patří	patřit	k5eAaImIp3nP	patřit
Ijo-no-futana-šima	Ijooutana-šima	k1gNnSc4	Ijo-no-futana-šima
(	(	kIx(	(
<g/>
伊	伊	k?	伊
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ijo-šima	Ijo-šima	k1gFnSc1	Ijo-šima
(	(	kIx(	(
<g/>
伊	伊	k?	伊
<g/>
)	)	kIx)	)
a	a	k8xC	a
Futana-šima	Futana-šima	k1gFnSc1	Futana-šima
(	(	kIx(	(
<g/>
二	二	k?	二
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
staré	starý	k2eAgFnPc4d1	stará
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
ostrov	ostrov	k1gInSc1	ostrov
dříve	dříve	k6eAd2	dříve
dělil	dělit	k5eAaImAgInS	dělit
<g/>
:	:	kIx,	:
Awa	Awa	k1gFnSc1	Awa
<g/>
,	,	kIx,	,
Ijo	Ijo	k1gFnSc1	Ijo
<g/>
,	,	kIx,	,
Sanuki	Sanuki	k1gNnSc1	Sanuki
a	a	k8xC	a
Tosa	Tosa	k1gFnSc1	Tosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Šikoku	Šikok	k1gInSc2	Šikok
–	–	k?	–
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Šikoku	Šikok	k1gInSc2	Šikok
(	(	kIx(	(
<g/>
plochou	plochý	k2eAgFnSc4d1	plochá
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
např.	např.	kA	např.
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
)	)	kIx)	)
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
18	[number]	k4	18
800	[number]	k4	800
km2	km2	k4	km2
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
prefektury	prefektura	k1gFnPc4	prefektura
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
oněm	onen	k3xDgNnPc3	onen
čtyřem	čtyři	k4xCgInPc3	čtyři
provinciím	provincie	k1gFnPc3	provincie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ehime	Ehimat	k5eAaPmIp3nS	Ehimat
–	–	k?	–
Ijo	Ijo	k1gFnSc1	Ijo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Kagawa	Kagawa	k1gFnSc1	Kagawa
–	–	k?	–
Sanuki	Sanuk	k1gFnSc2	Sanuk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Kóči	Kóči	k6eAd1	Kóči
–	–	k?	–
Tosa	Tosa	k1gMnSc1	Tosa
a	a	k8xC	a
</s>
</p>
<p>
<s>
Tokušima	Tokušima	k1gFnSc1	Tokušima
–	–	k?	–
Awa	Awa	k1gFnSc2	Awa
<g/>
.	.	kIx.	.
<g/>
Šikoku	Šikok	k1gInSc2	Šikok
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Honšú	Honšú	k1gFnSc7	Honšú
spojeno	spojit	k5eAaPmNgNnS	spojit
systémem	systém	k1gInSc7	systém
trajektů	trajekt	k1gInPc2	trajekt
<g/>
,	,	kIx,	,
leteckých	letecký	k2eAgFnPc2d1	letecká
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
i	i	k8xC	i
sítí	sítí	k1gNnSc1	sítí
mostů	most	k1gInPc2	most
Seto	sít	k5eAaImNgNnS	sít
Óhaši	Óhaš	k1gInPc7	Óhaš
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
dokončení	dokončení	k1gNnSc2	dokončení
mostů	most	k1gInPc2	most
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
izolován	izolován	k2eAgInSc1d1	izolován
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
od	od	k7c2	od
možnosti	možnost	k1gFnSc2	možnost
snadnější	snadný	k2eAgFnSc2d2	snazší
dopravy	doprava	k1gFnSc2	doprava
mezi	mezi	k7c7	mezi
Honšú	Honšú	k1gFnSc7	Honšú
a	a	k8xC	a
Šikoku	Šikok	k1gInSc2	Šikok
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
zrychlení	zrychlení	k1gNnSc1	zrychlení
rozvoje	rozvoj	k1gInSc2	rozvoj
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlehlé	rozlehlý	k2eAgFnPc1d1	rozlehlá
hory	hora	k1gFnPc1	hora
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
Šikoku	Šikok	k1gInSc3	Šikok
na	na	k7c4	na
významnější	významný	k2eAgFnSc4d2	významnější
úzkou	úzký	k2eAgFnSc4d1	úzká
severní	severní	k2eAgFnSc4d1	severní
oblast	oblast	k1gFnSc4	oblast
omývanou	omývaný	k2eAgFnSc4d1	omývaná
Vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
mořem	moře	k1gNnSc7	moře
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
provinciemi	provincie	k1gFnPc7	provincie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
u	u	k7c2	u
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
prefekturu	prefektura	k1gFnSc4	prefektura
Kóči	Kóč	k1gFnSc2	Kóč
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ze	z	k7c2	z
4,5	[number]	k4	4,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
tam	tam	k6eAd1	tam
také	také	k9	také
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nP	ležet
všechna	všechen	k3xTgNnPc4	všechen
větší	veliký	k2eAgNnPc4d2	veliký
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
Išizuči	Išizuč	k1gInSc6	Išizuč
(	(	kIx(	(
<g/>
石	石	k?	石
<g/>
)	)	kIx)	)
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Ehime	Ehim	k1gInSc5	Ehim
je	on	k3xPp3gInPc4	on
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1	[number]	k4	1
982	[number]	k4	982
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zpracování	zpracování	k1gNnSc4	zpracování
rudy	ruda	k1gFnSc2	ruda
z	z	k7c2	z
důležitého	důležitý	k2eAgInSc2d1	důležitý
dolu	dol	k1gInSc2	dol
na	na	k7c4	na
měď	měď	k1gFnSc4	měď
v	v	k7c6	v
Bešši	Bešše	k1gFnSc6	Bešše
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
obdělávána	obdělávat	k5eAaImNgFnS	obdělávat
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náplavových	náplavový	k2eAgFnPc6d1	náplavová
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
ječmen	ječmen	k1gInSc1	ječmen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gMnPc2	druh
ovoce	ovoce	k1gNnSc2	ovoce
včetně	včetně	k7c2	včetně
citrusů	citrus	k1gInPc2	citrus
<g/>
,	,	kIx,	,
broskví	broskev	k1gFnPc2	broskev
<g/>
,	,	kIx,	,
kaki	kaki	k1gNnPc2	kaki
a	a	k8xC	a
grapefruitů	grapefruit	k1gInPc2	grapefruit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
produkci	produkce	k1gFnSc3	produkce
pšenice	pšenice	k1gFnSc2	pšenice
se	se	k3xPyFc4	se
během	během	k7c2	během
období	období	k1gNnSc2	období
Edo	Eda	k1gMnSc5	Eda
staly	stát	k5eAaPmAgFnP	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Kagawa	Kagaw	k2eAgFnSc1d1	Kagaw
nudle	nudle	k1gFnSc1	nudle
sanuki	sanuk	k1gFnSc2	sanuk
udon	udona	k1gFnPc2	udona
(	(	kIx(	(
<g/>
讃	讃	k?	讃
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Šikoku	Šikok	k1gInSc2	Šikok
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
řídce	řídce	k6eAd1	řídce
osídlená	osídlený	k2eAgFnSc1d1	osídlená
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
větší	veliký	k2eAgFnSc1d2	veliký
nížina	nížina	k1gFnSc1	nížina
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
kolem	kolem	k7c2	kolem
Kóči	Kóči	k1gNnSc2	Kóči
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
prefektury	prefektura	k1gFnSc2	prefektura
Kóči	Kóč	k1gFnSc2	Kóč
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgFnPc1d1	mírná
zimy	zima	k1gFnPc1	zima
podporují	podporovat	k5eAaImIp3nP	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
zelinářství	zelinářství	k1gNnSc2	zelinářství
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
pěstování	pěstování	k1gNnSc4	pěstování
mimosezónní	mimosezónní	k2eAgFnSc2d1	mimosezónní
zeleniny	zelenina	k1gFnSc2	zelenina
pod	pod	k7c7	pod
plastovými	plastový	k2eAgFnPc7d1	plastová
fóliemi	fólie	k1gFnPc7	fólie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc1d1	běžná
dvě	dva	k4xCgFnPc1	dva
sklizně	sklizeň	k1gFnPc1	sklizeň
rýže	rýže	k1gFnSc2	rýže
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
a	a	k8xC	a
papírenský	papírenský	k2eAgInSc1d1	papírenský
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
dostatku	dostatek	k1gInSc3	dostatek
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
velmi	velmi	k6eAd1	velmi
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šikoku	Šikok	k1gInSc3	Šikok
je	být	k5eAaImIp3nS	být
také	také	k9	také
slavné	slavný	k2eAgNnSc4d1	slavné
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Putování	putování	k1gNnSc1	putování
po	po	k7c6	po
88	[number]	k4	88
chrámech	chrám	k1gInPc6	chrám
mnicha	mnich	k1gMnSc2	mnich
Kúkaie	Kúkaie	k1gFnSc1	Kúkaie
má	mít	k5eAaImIp3nS	mít
tradici	tradice	k1gFnSc4	tradice
už	už	k6eAd1	už
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
při	při	k7c6	při
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
moři	moře	k1gNnSc6	moře
tvoří	tvořit	k5eAaImIp3nS	tvořit
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
–	–	k?	–
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
cíl	cíl	k1gInSc1	cíl
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
samo	sám	k3xTgNnSc1	sám
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
přetížené	přetížený	k2eAgNnSc1d1	přetížené
průmyslem	průmysl	k1gInSc7	průmysl
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
břehů	břeh	k1gInPc2	břeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Shikoku	Shikok	k1gInSc2	Shikok
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
==	==	k?	==
</s>
</p>
<p>
<s>
Bungo	Bungo	k1gNnSc1	Bungo
(	(	kIx(	(
<g/>
kanál	kanál	k1gInSc1	kanál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šikoku	Šikok	k1gInSc2	Šikok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Wikivoyage	Wikivoyage	k1gFnSc1	Wikivoyage
<g/>
:	:	kIx,	:
Shikoku	Shikok	k1gInSc2	Shikok
</s>
</p>
