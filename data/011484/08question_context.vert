<s>
Franklin	Franklin	k1gInSc1	Franklin
Delano	Delana	k1gFnSc5	Delana
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1882	[number]	k4	1882
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
32	[number]	k4	32
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
nejdéle	dlouho	k6eAd3	dlouho
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgMnS	zvolit
byl	být	k5eAaImAgMnS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
