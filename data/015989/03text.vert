<s>
Hidalgo	Hidalgo	k1gMnSc1
(	(	kIx(
<g/>
planetka	planetka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
HidalgoIdentifikátory	HidalgoIdentifikátor	k1gInPc1
Označení	označení	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
944	#num#	k4
<g/>
)	)	kIx)
Hidalgo	Hidalgo	k1gMnSc1
Předběžné	předběžný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
1920	#num#	k4
HZ	Hz	kA
Katalogové	katalogový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
944	#num#	k4
Objevena	objeven	k2eAgFnSc1d1
Datum	datum	k1gNnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1920	#num#	k4
Místo	místo	k7c2
</s>
<s>
Bergedorf	Bergedorf	k1gInSc1
Observatory	Observator	k1gInPc1
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Baade	Baad	k1gInSc5
Jméno	jméno	k1gNnSc1
po	po	k7c6
</s>
<s>
Miguel	Miguel	k1gMnSc1
Hidalgo	Hidalgo	k1gMnSc1
y	y	k?
Costilla	Costilla	k1gFnSc1
Elementy	element	k1gInPc4
dráhy	dráha	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Epocha	epocha	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
20042453200,5	20042453200,5	k4
JD	JD	kA
Velká	velký	k2eAgFnSc1d1
poloosa	poloosa	k1gFnSc1
</s>
<s>
5,745	5,745	k4
AU	au	k0
859	#num#	k4
425	#num#	k4
000	#num#	k4
km	km	kA
Excentricita	excentricita	k1gFnSc1
</s>
<s>
0,660	0,660	k4
Perihel	perihel	k1gInSc1
</s>
<s>
1,951	1,951	k4
AU	au	k0
291,846,000	291,846,000	k4
km	km	kA
Afel	afel	k1gInSc1
</s>
<s>
9,539	9,539	k4
AU	au	k0
1427	#num#	k4
003	#num#	k4
000	#num#	k4
km	km	kA
Perioda	perioda	k1gFnSc1
(	(	kIx(
<g/>
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
13,77	13,77	k4
roku	rok	k1gInSc2
5029,467	5029,467	k4
dne	den	k1gInSc2
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
k	k	k7c3
ekliptice	ekliptika	k1gFnSc3
</s>
<s>
42,567	42,567	k4
<g/>
°	°	k?
Délka	délka	k1gFnSc1
vzestupného	vzestupný	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
</s>
<s>
31,549	31,549	k4
<g/>
°	°	k?
Argument	argument	k1gInSc1
šířky	šířka	k1gFnSc2
perihelu	perihel	k1gInSc2
</s>
<s>
56,569	56,569	k4
<g/>
°	°	k?
Střední	střední	k2eAgFnSc2d1
anomálie	anomálie	k1gFnSc2
</s>
<s>
346,285	346,285	k4
<g/>
°	°	k?
Fyzikální	fyzikální	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
10,77	10,77	k4
Odhadovaný	odhadovaný	k2eAgInSc1d1
průměr	průměr	k1gInSc1
</s>
<s>
20	#num#	k4
km	km	kA
Albedo	Albedo	k1gNnSc1
</s>
<s>
0,1	0,1	k4
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
8,4	8,4	k4
<g/>
×	×	k?
<g/>
1015	#num#	k4
kg	kg	kA
Střední	střední	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
</s>
<s>
2	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
</s>
<s>
(	(	kIx(
<g/>
944	#num#	k4
<g/>
)	)	kIx)
Hidalgo	Hidalgo	k1gMnSc1
je	být	k5eAaImIp3nS
netypická	typický	k2eNgFnSc1d1
planetka	planetka	k1gFnSc1
s	s	k7c7
nejdelší	dlouhý	k2eAgFnSc7d3
dobou	doba	k1gFnSc7
oběhu	oběh	k1gInSc2
(	(	kIx(
<g/>
13,77	13,77	k4
pozemských	pozemský	k2eAgInPc2d1
roků	rok	k1gInPc2
<g/>
)	)	kIx)
z	z	k7c2
planetek	planetka	k1gFnPc2
situovaných	situovaný	k2eAgFnPc2d1
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
pásu	pás	k1gInSc2
asteroidů	asteroid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Dráha	dráha	k1gFnSc1
944	#num#	k4
Hidalgo	Hidalgo	k1gMnSc1
</s>
<s>
S	s	k7c7
poměrně	poměrně	k6eAd1
vysokou	vysoký	k2eAgFnSc7d1
excentricitou	excentricita	k1gFnSc7
(	(	kIx(
<g/>
0,66	0,66	k4
<g/>
)	)	kIx)
zasahuje	zasahovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
perihélium	perihélium	k1gNnSc1
(	(	kIx(
<g/>
1,95	1,95	k4
AU	au	k0
<g/>
)	)	kIx)
do	do	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
hrany	hrana	k1gFnSc2
pásu	pás	k1gInSc2
asteroidů	asteroid	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gNnSc1
afélium	afélium	k1gNnSc1
(	(	kIx(
<g/>
9,54	9,54	k4
AU	au	k0
<g/>
)	)	kIx)
sahá	sahat	k5eAaImIp3nS
přímo	přímo	k6eAd1
k	k	k7c3
orbitu	orbita	k1gFnSc4
Saturnu	Saturn	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
charakteristika	charakteristika	k1gFnSc1
<g/>
,	,	kIx,
normálně	normálně	k6eAd1
přisuzovaná	přisuzovaný	k2eAgFnSc1d1
jen	jen	k6eAd1
Saturnovým	Saturnův	k2eAgFnPc3d1
kometám	kometa	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
astronomové	astronom	k1gMnPc1
proto	proto	k8xC
předpokládají	předpokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc4
kdysi	kdysi	k6eAd1
byla	být	k5eAaImAgFnS
kometa	kometa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečeno	říct	k5eAaPmNgNnS
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
Hidalgo	Hidalgo	k1gMnSc1
je	být	k5eAaImIp3nS
spíše	spíše	k9
Saturnův	Saturnův	k2eAgInSc1d1
úlomek	úlomek	k1gInSc1
než	než	k8xS
Saturnův	Saturnův	k2eAgInSc1d1
„	„	k?
<g/>
crosser	crosser	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
těleso	těleso	k1gNnSc1
zkřižující	zkřižující	k2eAgNnSc1d1
jeho	jeho	k3xOp3gFnSc4
dráhu	dráha	k1gFnSc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
Centaur	Centaur	k1gMnSc1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jeho	jeho	k3xOp3gNnSc1
afélium	afélium	k1gNnSc1
nekopíruje	kopírovat	k5eNaImIp3nS
Saturnovo	Saturnův	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hidalga	Hidalgo	k1gMnSc2
svírá	svírat	k5eAaImIp3nS
s	s	k7c7
rovinou	rovina	k1gFnSc7
ekliptiky	ekliptika	k1gFnSc2
úhel	úhel	k1gInSc1
zhruba	zhruba	k6eAd1
42.56	42.56	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
část	část	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
trajektorie	trajektorie	k1gFnSc2
„	„	k?
<g/>
nad	nad	k7c7
<g/>
“	“	k?
rovinou	rovina	k1gFnSc7
ekliptikou	ekliptika	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
protíná	protínat	k5eAaImIp3nS
v	v	k7c4
mezikruží	mezikruží	k1gNnSc4
drah	draha	k1gFnPc2
Marsu	Mars	k1gInSc2
a	a	k8xC
Jupitera	Jupiter	k1gMnSc2
<g/>
,	,	kIx,
vyčnívá	vyčnívat	k5eAaImIp3nS
cca	cca	kA
18	#num#	k4
<g/>
%	%	kIx~
její	její	k3xOp3gFnSc2
dráhy	dráha	k1gFnSc2
a	a	k8xC
(	(	kIx(
<g/>
z	z	k7c2
pohledu	pohled	k1gInSc2
shora	shora	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
severním	severní	k2eAgInSc6d1
obratníku	obratník	k1gInSc6
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
dráhy	dráha	k1gFnSc2
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
trajektorie	trajektorie	k1gFnSc2
je	být	k5eAaImIp3nS
nepatrně	patrně	k6eNd1,k6eAd1
delší	dlouhý	k2eAgFnSc1d2
než	než	k8xS
trajektorie	trajektorie	k1gFnSc1
Jupitera	Jupiter	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jenom	jenom	k9
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
elipsoidní	elipsoidní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hidalgova	Hidalgův	k2eAgFnSc1d1
rapidní	rapidní	k2eAgFnSc1d1
orbitální	orbitální	k2eAgFnSc1d1
inklinace	inklinace	k1gFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
výsledek	výsledek	k1gInSc4
blízkého	blízký	k2eAgNnSc2d1
setkání	setkání	k1gNnSc2
s	s	k7c7
Jupiterem	Jupiter	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
průměr	průměr	k1gInSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
20	#num#	k4
km	km	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
13	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
10	#num#	k4
měsíců	měsíc	k1gInPc2
a	a	k8xC
cca	cca	kA
12	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Objevení	objevení	k1gNnSc1
</s>
<s>
944	#num#	k4
Hidalgo	Hidalgo	k1gMnSc1
byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
Walterem	Walter	k1gInSc7
Baade	Baad	k1gInSc5
dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
1920	#num#	k4
v	v	k7c6
observatoři	observatoř	k1gFnSc6
Bergedorf	Bergedorf	k1gMnSc1
poblíž	poblíž	k7c2
německého	německý	k2eAgInSc2d1
Hamburku	Hamburk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němečtí	německý	k2eAgMnPc1d1
astronomové	astronom	k1gMnPc1
pozorovali	pozorovat	k5eAaImAgMnP
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1923	#num#	k4
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
úplné	úplný	k2eAgFnSc2d1
sluneční	sluneční	k2eAgFnSc2d1
zatmění	zatmění	k1gNnSc4
za	za	k7c4
účasti	účast	k1gFnPc4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
prezidenta	prezident	k1gMnSc2
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
počest	počest	k1gFnSc6
pojmenovali	pojmenovat	k5eAaPmAgMnP
objevenou	objevený	k2eAgFnSc4d1
planetku	planetka	k1gFnSc4
po	po	k7c6
Mexickém	mexický	k2eAgInSc6d1
národním	národní	k2eAgInSc6d1
hrdinovi	hrdinův	k2eAgMnPc1d1
Miguelovi	Miguelův	k2eAgMnPc1d1
Hidalgo	Hidalgo	k1gMnSc1
y	y	k?
Cistilla	Cistilla	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hidalgo	Hidalgo	k1gMnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
pěti	pět	k4xCc2
menších	malý	k2eAgFnPc2d2
planet	planeta	k1gFnPc2
zahrnuta	zahrnut	k2eAgFnSc1d1
ze	z	k7c2
studie	studie	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
–	–	k?
Transition	Transition	k1gInSc1
Comets	Comets	k1gInSc1
--	--	k?
UV	UV	kA
Search	Search	k1gMnSc1
for	forum	k1gNnPc2
OH	OH	kA
Emissions	Emissionsa	k1gFnPc2
in	in	k?
Asteroids	Asteroidsa	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
výzkum	výzkum	k1gInSc4
zapojující	zapojující	k2eAgMnPc4d1
amatérské	amatérský	k2eAgMnPc4d1
astronomy	astronom	k1gMnPc4
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
bylo	být	k5eAaImAgNnS
povoleno	povolit	k5eAaPmNgNnS
použití	použití	k1gNnSc1
Hubbleova	Hubbleův	k2eAgInSc2d1
Vesmírného	vesmírný	k2eAgInSc2d1
Teleskopu	teleskop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
polovina	polovina	k1gFnSc1
roku	rok	k1gInSc2
2018	#num#	k4
bude	být	k5eAaImBp3nS
nejbližší	blízký	k2eAgFnSc1d3
doba	doba	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Hidalgo	Hidalgo	k1gMnSc1
nejvíce	hodně	k6eAd3,k6eAd1
přiblíží	přiblížit	k5eAaPmIp3nS
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
kolem	kolem	k7c2
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
t.	t.	k?
<g/>
r.	r.	kA
protne	protnout	k5eAaPmIp3nS
rovinu	rovina	k1gFnSc4
ekliptiky	ekliptika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
944	#num#	k4
Hidalgo	Hidalgo	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc4
planetek	planetka	k1gFnPc2
751-1000	751-1000	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
944	#num#	k4
<g/>
)	)	kIx)
Hidalgo	Hidalgo	k1gMnSc1
na	na	k7c6
webu	web	k1gInSc6
České	český	k2eAgFnSc2d1
astronomické	astronomický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Java	Java	k6eAd1
applet	applet	k5eAaImF,k5eAaPmF
zobrazující	zobrazující	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
planetky	planetka	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
