<s>
Pipin	pipina	k1gFnPc2	pipina
lll	lll	k?	lll
<g/>
.	.	kIx.	.
Krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
též	též	k9	též
Pipin	pipina	k1gFnPc2	pipina
Mladší	mladý	k2eAgFnSc1d2	mladší
(	(	kIx(	(
<g/>
714	[number]	k4	714
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
768	[number]	k4	768
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
751	[number]	k4	751
<g/>
–	–	k?	–
<g/>
768	[number]	k4	768
první	první	k4xOgMnSc1	první
francký	francký	k2eAgMnSc1d1	francký
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Karlovců	Karlovac	k1gInPc2	Karlovac
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
merovejského	merovejský	k2eAgMnSc4d1	merovejský
majordoma	majordom	k1gMnSc4	majordom
Karla	Karel	k1gMnSc4	Karel
Martela	Martel	k1gMnSc4	Martel
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
roku	rok	k1gInSc2	rok
741	[number]	k4	741
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
Pipinův	Pipinův	k2eAgMnSc1d1	Pipinův
otec	otec	k1gMnSc1	otec
Karel	Karel	k1gMnSc1	Karel
Martel	Martel	k1gMnSc1	Martel
<g/>
,	,	kIx,	,
faktický	faktický	k2eAgMnSc1d1	faktický
vládce	vládce	k1gMnSc1	vládce
franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
merovejské	merovejský	k2eAgNnSc4d1	merovejský
království	království	k1gNnSc4	království
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
sféry	sféra	k1gFnPc4	sféra
vlivu	vliv	k1gInSc2	vliv
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
Karloman	Karloman	k1gMnSc1	Karloman
získal	získat	k5eAaPmAgMnS	získat
Austrasii	Austrasie	k1gFnSc4	Austrasie
<g/>
,	,	kIx,	,
Alemanii	Alemanie	k1gFnSc4	Alemanie
a	a	k8xC	a
Durynsko	Durynsko	k1gNnSc4	Durynsko
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnPc1d2	mladší
Pipin	pipina	k1gFnPc2	pipina
Neustrii	Neustrie	k1gFnSc4	Neustrie
<g/>
,	,	kIx,	,
Burgundsko	Burgundsko	k1gNnSc4	Burgundsko
a	a	k8xC	a
Provence	Provence	k1gFnPc4	Provence
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
ovšem	ovšem	k9	ovšem
bratři	bratr	k1gMnPc1	bratr
museli	muset	k5eAaImAgMnP	muset
uhájit	uhájit	k5eAaPmF	uhájit
své	svůj	k3xOyFgNnSc4	svůj
dědictví	dědictví	k1gNnSc4	dědictví
před	před	k7c7	před
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
bratrem	bratr	k1gMnSc7	bratr
Grifem	grif	k1gInSc7	grif
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nechali	nechat	k5eAaPmAgMnP	nechat
zavřít	zavřít	k5eAaPmF	zavřít
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gNnSc1	jejich
mocenský	mocenský	k2eAgInSc1d1	mocenský
vzestup	vzestup	k1gInSc1	vzestup
příliš	příliš	k6eAd1	příliš
nedráždil	dráždit	k5eNaImAgInS	dráždit
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
dosadili	dosadit	k5eAaPmAgMnP	dosadit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
loutkového	loutkový	k2eAgMnSc2d1	loutkový
panovníka	panovník	k1gMnSc2	panovník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Merovejců	Merovejec	k1gMnPc2	Merovejec
Childericha	Childerich	k1gMnSc2	Childerich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
jejich	jejich	k3xOp3gNnPc4	jejich
správcovství	správcovství	k1gNnPc4	správcovství
dodat	dodat	k5eAaPmF	dodat
punc	punc	k1gInSc4	punc
oficiality	oficialita	k1gFnSc2	oficialita
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
obou	dva	k4xCgMnPc2	dva
bratří	bratr	k1gMnPc2	bratr
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
Karlomanovým	Karlomanův	k2eAgInSc7d1	Karlomanův
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
předání	předání	k1gNnSc1	předání
jeho	jeho	k3xOp3gFnPc2	jeho
zemí	zem	k1gFnPc2	zem
Pipinovi	Pipinovi	k1gRnPc5	Pipinovi
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
byla	být	k5eAaImAgFnS	být
bratrská	bratrský	k2eAgFnSc1d1	bratrská
soudržnost	soudržnost	k1gFnSc4	soudržnost
pouhým	pouhý	k2eAgNnSc7d1	pouhé
zdáním	zdání	k1gNnSc7	zdání
a	a	k8xC	a
zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
odchod	odchod	k1gInSc4	odchod
dobrovolný	dobrovolný	k2eAgMnSc1d1	dobrovolný
<g/>
,	,	kIx,	,
chybějí	chybět	k5eAaImIp3nP	chybět
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Pipinovi	Pipin	k1gMnSc3	Pipin
přestal	přestat	k5eAaPmAgMnS	přestat
stačit	stačit	k5eAaBmF	stačit
titul	titul	k1gInSc4	titul
pouhého	pouhý	k2eAgMnSc2d1	pouhý
majordoma	majordom	k1gMnSc2	majordom
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
už	už	k6eAd1	už
dlouho	dlouho	k6eAd1	dlouho
vládla	vládnout	k5eAaImAgFnS	vládnout
královskou	královský	k2eAgFnSc7d1	královská
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
jí	on	k3xPp3gFnSc7	on
však	však	k9	však
nenáležel	náležet	k5eNaImAgInS	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
podložil	podložit	k5eAaPmAgInS	podložit
Pipin	pipina	k1gFnPc2	pipina
odpovědí	odpověď	k1gFnPc2	odpověď
papeže	papež	k1gMnSc2	papež
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
králem	král	k1gMnSc7	král
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
moc	moc	k6eAd1	moc
nedrží	držet	k5eNaImIp3nS	držet
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
stálo	stát	k5eAaImAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
správné	správný	k2eAgNnSc1d1	správné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
králem	král	k1gMnSc7	král
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
751	[number]	k4	751
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
Pipin	pipina	k1gFnPc2	pipina
nechal	nechat	k5eAaPmAgInS	nechat
v	v	k7c4	v
Soissons	Soissons	k1gInSc4	Soissons
provolat	provolat	k5eAaPmF	provolat
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
Rex	Rex	k1gFnSc1	Rex
francorum	francorum	k1gInSc1	francorum
<g/>
)	)	kIx)	)
a	a	k8xC	a
posledního	poslední	k2eAgMnSc2d1	poslední
merovejského	merovejský	k2eAgMnSc2d1	merovejský
panovníka	panovník	k1gMnSc2	panovník
poslal	poslat	k5eAaPmAgInS	poslat
i	i	k9	i
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
754	[number]	k4	754
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
papež	papež	k1gMnSc1	papež
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ohrožovaný	ohrožovaný	k2eAgInSc1d1	ohrožovaný
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
Langobardy	Langobarda	k1gFnSc2	Langobarda
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
754	[number]	k4	754
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
korunoval	korunovat	k5eAaBmAgMnS	korunovat
Pipina	pipina	k1gFnSc1	pipina
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
syny	syn	k1gMnPc4	syn
Karlomana	Karloman	k1gMnSc4	Karloman
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
na	na	k7c4	na
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
755	[number]	k4	755
<g/>
–	–	k?	–
<g/>
756	[number]	k4	756
potom	potom	k6eAd1	potom
Pipin	pipina	k1gFnPc2	pipina
opravdu	opravdu	k6eAd1	opravdu
Langobardy	Langobarda	k1gFnSc2	Langobarda
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
porazil	porazit	k5eAaPmAgMnS	porazit
a	a	k8xC	a
znovudosadil	znovudosadit	k5eAaPmAgMnS	znovudosadit
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Daroval	darovat	k5eAaPmAgInS	darovat
mu	on	k3xPp3gNnSc3	on
také	také	k9	také
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
Pipinova	Pipinův	k2eAgFnSc1d1	Pipinova
donace	donace	k1gFnSc1	donace
<g/>
)	)	kIx)	)
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
říši	říše	k1gFnSc4	říše
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
Karla	Karel	k1gMnSc4	Karel
a	a	k8xC	a
Karlomana	Karloman	k1gMnSc4	Karloman
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
768	[number]	k4	768
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
<g/>
.	.	kIx.	.
</s>
<s>
DRŠKA	drška	k1gFnSc1	drška
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Pipinovo	Pipinův	k2eAgNnSc1d1	Pipinův
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
11	[number]	k4	11
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
106	[number]	k4	106
<g/>
-	-	kIx~	-
<g/>
108	[number]	k4	108
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
<s>
JAMES	JAMES	kA	JAMES
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
:	:	kIx,	:
Frankové	Frankové	k2eAgMnSc1d1	Frankové
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7106-200-6	[number]	k4	80-7106-200-6
Karlovci	Karlovac	k1gInPc7	Karlovac
Franská	franský	k2eAgFnSc1d1	Franská
říše	říš	k1gFnSc2	říš
Pipinova	Pipinův	k2eAgFnSc1d1	Pipinova
donace	donace	k1gFnSc1	donace
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pipin	pipina	k1gFnPc2	pipina
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Krátký	krátký	k2eAgMnSc1d1	krátký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
