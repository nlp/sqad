<p>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
poháru	pohár	k1gInSc2	pohár
Rudého	rudý	k2eAgNnSc2d1	Rudé
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1977	[number]	k4	1977
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
dvě	dva	k4xCgNnPc1	dva
reprezentační	reprezentační	k2eAgNnPc1d1	reprezentační
mužstva	mužstvo	k1gNnPc1	mužstvo
a	a	k8xC	a
zástupce	zástupce	k1gMnSc1	zástupce
WHA	WHA	kA	WHA
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
Stingers	Stingers	k1gInSc1	Stingers
<g/>
.	.	kIx.	.
</s>
<s>
Mužstva	mužstvo	k1gNnSc2	mužstvo
se	se	k3xPyFc4	se
utkala	utkat	k5eAaPmAgFnS	utkat
dvoukolovým	dvoukolový	k2eAgInSc7d1	dvoukolový
systémem	systém	k1gInSc7	systém
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
tabulka	tabulka	k1gFnSc1	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
–	–	k?	–
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
Stingers	Stingers	k1gInSc1	Stingers
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
Šadrin	Šadrin	k1gInSc1	Šadrin
2	[number]	k4	2
<g/>
,	,	kIx,	,
Michajlov	Michajlov	k1gInSc1	Michajlov
2	[number]	k4	2
<g/>
,	,	kIx,	,
Volčkov	Volčkov	k1gInSc1	Volčkov
2	[number]	k4	2
<g/>
,	,	kIx,	,
Kapustin	Kapustin	k1gInSc1	Kapustin
2	[number]	k4	2
<g/>
,	,	kIx,	,
Žluktov	Žluktov	k1gInSc1	Žluktov
<g/>
,	,	kIx,	,
Charlamov	Charlamov	k1gInSc1	Charlamov
<g/>
,	,	kIx,	,
Jakušev	Jakušev	k1gFnSc1	Jakušev
–	–	k?	–
Abgrall	Abgrall	k1gInSc1	Abgrall
2	[number]	k4	2
<g/>
,	,	kIx,	,
Locas	Locas	k1gInSc1	Locas
<g/>
,	,	kIx,	,
Beaudoin	Beaudoin	k1gInSc1	Beaudoin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
:	:	kIx,	:
Šubrt	Šubrt	k1gMnSc1	Šubrt
–	–	k?	–
Exner	Exner	k1gMnSc1	Exner
<g/>
,	,	kIx,	,
Sládeček	Sládeček	k1gMnSc1	Sládeček
(	(	kIx(	(
<g/>
TCH	tch	k0	tch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
<g/>
:	:	kIx,	:
Treťjak	Treťjak	k1gMnSc1	Treťjak
–	–	k?	–
Cygankov	Cygankov	k1gInSc1	Cygankov
<g/>
,	,	kIx,	,
Fetisov	Fetisov	k1gInSc1	Fetisov
<g/>
,	,	kIx,	,
Babinov	Babinov	k1gInSc1	Babinov
<g/>
,	,	kIx,	,
Lutčenko	Lutčenka	k1gFnSc5	Lutčenka
<g/>
,	,	kIx,	,
Pervuchin	Pervuchin	k1gMnSc1	Pervuchin
<g/>
,	,	kIx,	,
Gusev	Gusev	k1gFnSc1	Gusev
–	–	k?	–
Michajlov	Michajlov	k1gInSc1	Michajlov
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
Charlamov	Charlamov	k1gInSc1	Charlamov
–	–	k?	–
Malcev	Malcev	k1gFnSc1	Malcev
<g/>
,	,	kIx,	,
Žluktov	Žluktov	k1gInSc1	Žluktov
<g/>
,	,	kIx,	,
Kapustin	Kapustin	k2eAgInSc1d1	Kapustin
–	–	k?	–
Šalimov	Šalimov	k1gInSc1	Šalimov
<g/>
,	,	kIx,	,
Šadrin	Šadrin	k1gInSc1	Šadrin
<g/>
,	,	kIx,	,
Jakušev	Jakušev	k1gFnSc1	Jakušev
–	–	k?	–
V.	V.	kA	V.
Golikov	Golikov	k1gInSc1	Golikov
<g/>
,	,	kIx,	,
Volčkov	Volčkov	k1gInSc1	Volčkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cincinnati	Cincinnat	k5eAaImF	Cincinnat
Stingers	Stingers	k1gInSc4	Stingers
<g/>
:	:	kIx,	:
Wakely	Wakela	k1gFnSc2	Wakela
–	–	k?	–
Plumb	Plumb	k1gMnSc1	Plumb
<g/>
,	,	kIx,	,
Beaudoin	Beaudoin	k1gMnSc1	Beaudoin
<g/>
,	,	kIx,	,
Norwich	Norwich	k1gMnSc1	Norwich
<g/>
,	,	kIx,	,
Marotte	Marott	k1gMnSc5	Marott
<g/>
,	,	kIx,	,
Merlose	Merlosa	k1gFnSc3	Merlosa
<g/>
,	,	kIx,	,
Legge	Legge	k1gNnSc3	Legge
<g/>
,	,	kIx,	,
Lahache	Lahache	k1gFnSc3	Lahache
–	–	k?	–
Abgrall	Abgrall	k1gInSc4	Abgrall
<g/>
,	,	kIx,	,
Leduc	Leduc	k1gFnSc4	Leduc
<g/>
,	,	kIx,	,
Larose	Larosa	k1gFnSc3	Larosa
–	–	k?	–
Marsh	Marsh	k1gMnSc1	Marsh
<g/>
,	,	kIx,	,
Ftorek	Ftorek	k1gMnSc1	Ftorek
<g/>
,	,	kIx,	,
Hall	Hall	k1gMnSc1	Hall
–	–	k?	–
Stoughton	Stoughton	k1gInSc1	Stoughton
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Sobchuk	Sobchuk	k1gInSc1	Sobchuk
<g/>
,	,	kIx,	,
Dudley	Dudley	k1gInPc1	Dudley
–	–	k?	–
Hislop	Hislop	k1gInSc1	Hislop
<g/>
,	,	kIx,	,
Locas	Locas	k1gInSc1	Locas
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Sobchuk	Sobchuk	k1gInSc1	Sobchuk
–	–	k?	–
Gilligan	Gilligan	k1gInSc1	Gilligan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
Stingers	Stingers	k1gInSc1	Stingers
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
Ebermann	Ebermann	k1gInSc1	Ebermann
2	[number]	k4	2
<g/>
,	,	kIx,	,
Nový	nový	k2eAgMnSc1d1	nový
2	[number]	k4	2
<g/>
,	,	kIx,	,
Hlinka	hlinka	k1gFnSc1	hlinka
2	[number]	k4	2
<g/>
,	,	kIx,	,
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
Martinec	Martinec	k1gMnSc1	Martinec
–	–	k?	–
Locas	Locas	k1gMnSc1	Locas
<g/>
,	,	kIx,	,
Leduc	Leduc	k1gFnSc1	Leduc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
:	:	kIx,	:
Järvi	Järev	k1gFnSc6	Järev
(	(	kIx(	(
<g/>
FIN	Fina	k1gFnPc2	Fina
<g/>
)	)	kIx)	)
–	–	k?	–
Koval	kovat	k5eAaImAgMnS	kovat
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
TCH	tch	k0	tch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
M.	M.	kA	M.
Šťastný	šťastný	k2eAgMnSc1d1	šťastný
a	a	k8xC	a
Locas	Locas	k1gInSc1	Locas
na	na	k7c4	na
5	[number]	k4	5
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČSSR	ČSSR	kA	ČSSR
<g/>
:	:	kIx,	:
Holeček	Holeček	k1gMnSc1	Holeček
(	(	kIx(	(
<g/>
Králík	Králík	k1gMnSc1	Králík
<g/>
)	)	kIx)	)
–	–	k?	–
Kaberle	Kaberle	k1gInSc1	Kaberle
<g/>
,	,	kIx,	,
Chalupa	chalupa	k1gFnSc1	chalupa
<g/>
,	,	kIx,	,
Bubla	Bubla	k1gFnSc1	Bubla
<g/>
,	,	kIx,	,
Kajkl	Kajkl	k1gInSc1	Kajkl
<g/>
,	,	kIx,	,
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Roháčik	Roháčik	k1gMnSc1	Roháčik
<g/>
,	,	kIx,	,
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Joun	Joun	k1gMnSc1	Joun
–	–	k?	–
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Ebermann	Ebermann	k1gMnSc1	Ebermann
–	–	k?	–
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Hlinka	Hlinka	k1gMnSc1	Hlinka
<g/>
,	,	kIx,	,
Augusta	Augusta	k1gMnSc1	Augusta
–	–	k?	–
M.	M.	kA	M.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Pouzar	Pouzar	k1gMnSc1	Pouzar
–	–	k?	–
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
Sýkora	Sýkora	k1gMnSc1	Sýkora
<g/>
,	,	kIx,	,
Richter	Richter	k1gMnSc1	Richter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cincinnati	Cincinnat	k5eAaImF	Cincinnat
Stingers	Stingers	k1gInSc4	Stingers
<g/>
:	:	kIx,	:
Lapointe	Lapoint	k1gMnSc5	Lapoint
–	–	k?	–
Norwich	Norwicha	k1gFnPc2	Norwicha
<g/>
,	,	kIx,	,
Marotte	Marott	k1gMnSc5	Marott
<g/>
,	,	kIx,	,
Plumb	Plumba	k1gFnPc2	Plumba
<g/>
,	,	kIx,	,
Beaudoin	Beaudoina	k1gFnPc2	Beaudoina
<g/>
,	,	kIx,	,
Merlose	Merlosa	k1gFnSc6	Merlosa
<g/>
,	,	kIx,	,
Legge	Legge	k1gFnSc1	Legge
<g/>
,	,	kIx,	,
Lahache	Lahache	k1gFnSc1	Lahache
–	–	k?	–
Abgrall	Abgrall	k1gInSc4	Abgrall
<g/>
,	,	kIx,	,
Leduc	Leduc	k1gFnSc4	Leduc
<g/>
,	,	kIx,	,
Larose	Larosa	k1gFnSc3	Larosa
–	–	k?	–
Hislop	Hislop	k1gInSc1	Hislop
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Sobchuk	Sobchuk	k1gInSc1	Sobchuk
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Sobchuk	Sobchuk	k1gMnSc1	Sobchuk
–	–	k?	–
Marsh	Marsh	k1gMnSc1	Marsh
<g/>
,	,	kIx,	,
Locas	Locas	k1gMnSc1	Locas
<g/>
,	,	kIx,	,
Hall	Hall	k1gMnSc1	Hall
–	–	k?	–
Gilligan	Gilligan	k1gMnSc1	Gilligan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
SSSR	SSSR	kA	SSSR
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
Bubla	Bubla	k1gFnSc1	Bubla
–	–	k?	–
Michajlov	Michajlov	k1gInSc1	Michajlov
2	[number]	k4	2
<g/>
,	,	kIx,	,
Balderis	Balderis	k1gFnSc1	Balderis
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
:	:	kIx,	:
Karlsson	Karlsson	k1gMnSc1	Karlsson
(	(	kIx(	(
<g/>
SWE	SWE	kA	SWE
<g/>
)	)	kIx)	)
–	–	k?	–
Schell	Schell	k1gMnSc1	Schell
(	(	kIx(	(
<g/>
HUN	Hun	k1gMnSc1	Hun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Westreicher	Westreichra	k1gFnPc2	Westreichra
(	(	kIx(	(
<g/>
AUT	aut	k1gInSc1	aut
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ČSSR	ČSSR	kA	ČSSR
<g/>
:	:	kIx,	:
Holeček	Holeček	k1gMnSc1	Holeček
–	–	k?	–
Kaberle	Kaberle	k1gInSc1	Kaberle
<g/>
,	,	kIx,	,
Chalupa	chalupa	k1gFnSc1	chalupa
<g/>
,	,	kIx,	,
Bubla	Bubla	k1gFnSc1	Bubla
<g/>
,	,	kIx,	,
Kajkl	Kajkl	k1gInSc1	Kajkl
<g/>
,	,	kIx,	,
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Roháčik	Roháčik	k1gMnSc1	Roháčik
<g/>
,	,	kIx,	,
Dvořák	Dvořák	k1gMnSc1	Dvořák
–	–	k?	–
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Ebermann	Ebermann	k1gMnSc1	Ebermann
–	–	k?	–
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Hlinka	Hlinka	k1gMnSc1	Hlinka
<g/>
,	,	kIx,	,
Augusta	Augusta	k1gMnSc1	Augusta
–	–	k?	–
M.	M.	kA	M.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Pouzar	Pouzar	k1gMnSc1	Pouzar
–	–	k?	–
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Černík	Černík	k1gMnSc1	Černík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
<g/>
:	:	kIx,	:
Treťjak	Treťjak	k1gMnSc1	Treťjak
–	–	k?	–
Cygankov	Cygankov	k1gInSc1	Cygankov
<g/>
,	,	kIx,	,
Fetisov	Fetisov	k1gInSc1	Fetisov
<g/>
,	,	kIx,	,
<g/>
Vasiljev	Vasiljev	k1gFnSc1	Vasiljev
<g/>
,	,	kIx,	,
Gusev	Gusev	k1gFnSc1	Gusev
<g/>
,	,	kIx,	,
Babinov	Babinov	k1gInSc1	Babinov
<g/>
,	,	kIx,	,
Lutčenko	Lutčenka	k1gFnSc5	Lutčenka
<g/>
,	,	kIx,	,
Pervuchin	Pervuchin	k1gMnSc1	Pervuchin
<g/>
,	,	kIx,	,
Biljaletdinov	Biljaletdinov	k1gInSc1	Biljaletdinov
–	–	k?	–
Michajlov	Michajlov	k1gInSc1	Michajlov
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
Charlamov	Charlamov	k1gInSc1	Charlamov
–	–	k?	–
Šalimov	Šalimov	k1gInSc1	Šalimov
<g/>
,	,	kIx,	,
Šadrin	Šadrin	k1gInSc1	Šadrin
<g/>
,	,	kIx,	,
Jakušev	Jakušev	k1gFnSc1	Jakušev
–	–	k?	–
Balderis	Balderis	k1gInSc1	Balderis
<g/>
,	,	kIx,	,
Žluktov	Žluktov	k1gInSc1	Žluktov
<g/>
,	,	kIx,	,
Kapustin	Kapustin	k1gInSc1	Kapustin
–	–	k?	–
Malcev	Malcev	k1gFnSc1	Malcev
<g/>
,	,	kIx,	,
Golikov	Golikov	k1gInSc1	Golikov
<g/>
,	,	kIx,	,
Volčkov	Volčkov	k1gInSc1	Volčkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
–	–	k?	–
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
Stingers	Stingers	k1gInSc1	Stingers
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
Michajlov	Michajlov	k1gInSc1	Michajlov
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
Charlamov	Charlamov	k1gInSc1	Charlamov
<g/>
,	,	kIx,	,
Balderis	Balderis	k1gFnSc5	Balderis
<g/>
,	,	kIx,	,
Lutčenko	Lutčenka	k1gFnSc5	Lutčenka
–	–	k?	–
G.	G.	kA	G.
Sobchuk	Sobchuk	k1gInSc1	Sobchuk
<g/>
,	,	kIx,	,
Leduc	Leduc	k1gInSc1	Leduc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
:	:	kIx,	:
Šubrt	Šubrt	k1gMnSc1	Šubrt
–	–	k?	–
Koval	kovat	k5eAaImAgMnS	kovat
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
TCH	tch	k0	tch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
<g/>
:	:	kIx,	:
Babarik	Babarik	k1gMnSc1	Babarik
–	–	k?	–
Cygankov	Cygankov	k1gInSc1	Cygankov
<g/>
,	,	kIx,	,
Fetisov	Fetisov	k1gInSc1	Fetisov
<g/>
,	,	kIx,	,
<g/>
Vasiljev	Vasiljev	k1gFnSc1	Vasiljev
<g/>
,	,	kIx,	,
Gusev	Gusev	k1gFnSc1	Gusev
<g/>
,	,	kIx,	,
Babinov	Babinov	k1gInSc1	Babinov
<g/>
,	,	kIx,	,
Lutčenko	Lutčenka	k1gFnSc5	Lutčenka
<g/>
,	,	kIx,	,
Pervuchin	Pervuchin	k1gMnSc1	Pervuchin
<g/>
,	,	kIx,	,
Biljaletdinov	Biljaletdinov	k1gInSc1	Biljaletdinov
–	–	k?	–
Michajlov	Michajlov	k1gInSc1	Michajlov
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
Charlamov	Charlamov	k1gInSc1	Charlamov
–	–	k?	–
Šalimov	Šalimov	k1gInSc1	Šalimov
<g/>
,	,	kIx,	,
Šadrin	Šadrin	k1gInSc1	Šadrin
<g/>
,	,	kIx,	,
Jakušev	Jakušev	k1gFnSc1	Jakušev
–	–	k?	–
Balderis	Balderis	k1gInSc1	Balderis
<g/>
,	,	kIx,	,
Žluktov	Žluktov	k1gInSc1	Žluktov
<g/>
,	,	kIx,	,
Kapustin	Kapustin	k1gInSc1	Kapustin
–	–	k?	–
Malcev	Malcev	k1gFnSc1	Malcev
<g/>
,	,	kIx,	,
Golikov	Golikov	k1gInSc1	Golikov
<g/>
,	,	kIx,	,
Volčkov	Volčkov	k1gInSc1	Volčkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
Stingers	Stingers	k1gInSc1	Stingers
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
Nový	Nový	k1gMnSc1	Nový
2	[number]	k4	2
<g/>
,	,	kIx,	,
Kaberle	Kaberle	k1gFnSc1	Kaberle
2	[number]	k4	2
<g/>
,	,	kIx,	,
Pouzar	Pouzar	k1gInSc1	Pouzar
2	[number]	k4	2
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Kajkl	Kajkl	k1gInSc1	Kajkl
<g/>
,	,	kIx,	,
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Martinec	Martinec	k1gMnSc1	Martinec
–	–	k?	–
Leduc	Leduc	k1gInSc1	Leduc
2	[number]	k4	2
<g/>
,	,	kIx,	,
Abgrall	Abgrall	k1gInSc1	Abgrall
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
:	:	kIx,	:
Järvi	Järev	k1gFnSc6	Järev
(	(	kIx(	(
<g/>
FIN	Fina	k1gFnPc2	Fina
<g/>
)	)	kIx)	)
–	–	k?	–
Exner	Exner	k1gMnSc1	Exner
<g/>
,	,	kIx,	,
Sládeček	Sládeček	k1gMnSc1	Sládeček
(	(	kIx(	(
<g/>
TCH	tch	k0	tch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
Beaudoin	Beaudoin	k2eAgInSc4d1	Beaudoin
na	na	k7c4	na
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Šťastný	Šťastný	k1gMnSc1	Šťastný
na	na	k7c6	na
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Šťastný	Šťastný	k1gMnSc1	Šťastný
a	a	k8xC	a
Marotte	Marott	k1gInSc5	Marott
na	na	k7c4	na
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČSSR	ČSSR	kA	ČSSR
<g/>
:	:	kIx,	:
Sakač	Sakač	k1gMnSc1	Sakač
–	–	k?	–
Kaberle	Kaberle	k1gInSc1	Kaberle
<g/>
,	,	kIx,	,
Chalupa	chalupa	k1gFnSc1	chalupa
<g/>
,	,	kIx,	,
Bubla	Bubla	k1gFnSc1	Bubla
<g/>
,	,	kIx,	,
Kajkl	Kajkl	k1gInSc1	Kajkl
<g/>
,	,	kIx,	,
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Joun	Joun	k1gMnSc1	Joun
<g/>
,	,	kIx,	,
Roháčik	Roháčik	k1gMnSc1	Roháčik
<g/>
,	,	kIx,	,
Dvořák	Dvořák	k1gMnSc1	Dvořák
–	–	k?	–
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Ebermann	Ebermann	k1gMnSc1	Ebermann
–	–	k?	–
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Hlinka	Hlinka	k1gMnSc1	Hlinka
<g/>
,	,	kIx,	,
Augusta	Augusta	k1gMnSc1	Augusta
–	–	k?	–
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
Sýkora	Sýkora	k1gMnSc1	Sýkora
<g/>
,	,	kIx,	,
Richter	Richter	k1gMnSc1	Richter
–	–	k?	–
M.	M.	kA	M.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Pouzar	Pouzar	k1gMnSc1	Pouzar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cincinnati	Cincinnat	k5eAaPmF	Cincinnat
Stingers	Stingers	k1gInSc4	Stingers
<g/>
:	:	kIx,	:
Lapointe	Lapoint	k1gMnSc5	Lapoint
(	(	kIx(	(
<g/>
Liut	Liutum	k1gNnPc2	Liutum
<g/>
)	)	kIx)	)
–	–	k?	–
Merlose	Merlosa	k1gFnSc6	Merlosa
<g/>
,	,	kIx,	,
Legge	Legge	k1gNnSc6	Legge
<g/>
,	,	kIx,	,
Plumb	Plumb	k1gMnSc1	Plumb
<g/>
,	,	kIx,	,
Beaudoin	Beaudoin	k1gMnSc1	Beaudoin
<g/>
,	,	kIx,	,
Lahache	Lahachus	k1gMnSc5	Lahachus
<g/>
,	,	kIx,	,
Marotte	Marott	k1gMnSc5	Marott
–	–	k?	–
G.	G.	kA	G.
Sobchuk	Sobchuk	k1gMnSc1	Sobchuk
<g/>
,	,	kIx,	,
Locas	Locas	k1gMnSc1	Locas
<g/>
,	,	kIx,	,
Gilligan	Gilligan	k1gMnSc1	Gilligan
–	–	k?	–
Stoughton	Stoughton	k1gInSc1	Stoughton
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Sobchuk	Sobchuk	k1gInSc1	Sobchuk
<g/>
,	,	kIx,	,
Marsh	Marsh	k1gInSc1	Marsh
–	–	k?	–
Abgrall	Abgrall	k1gInSc4	Abgrall
<g/>
,	,	kIx,	,
Leduc	Leduc	k1gFnSc4	Leduc
<g/>
,	,	kIx,	,
Larose	Larosa	k1gFnSc3	Larosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
SSSR	SSSR	kA	SSSR
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
Pouzar	Pouzar	k1gMnSc1	Pouzar
<g/>
,	,	kIx,	,
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
Eberman	Eberman	k1gMnSc1	Eberman
<g/>
,	,	kIx,	,
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Hlinka	Hlinka	k1gMnSc1	Hlinka
–	–	k?	–
Petrov	Petrov	k1gInSc1	Petrov
2	[number]	k4	2
<g/>
,	,	kIx,	,
Golikov	Golikov	k1gInSc1	Golikov
<g/>
,	,	kIx,	,
Jakušev	Jakušev	k1gFnSc1	Jakušev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
:	:	kIx,	:
Karlsson	Karlsson	k1gMnSc1	Karlsson
(	(	kIx(	(
<g/>
SWE	SWE	kA	SWE
<g/>
)	)	kIx)	)
–	–	k?	–
Schell	Schell	k1gMnSc1	Schell
(	(	kIx(	(
<g/>
HUN	Hun	k1gMnSc1	Hun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Westreicher	Westreichra	k1gFnPc2	Westreichra
(	(	kIx(	(
<g/>
AUT	aut	k1gInSc1	aut
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ČSSR	ČSSR	kA	ČSSR
<g/>
:	:	kIx,	:
Králík	Králík	k1gMnSc1	Králík
–	–	k?	–
Kaberle	Kaberle	k1gInSc1	Kaberle
<g/>
,	,	kIx,	,
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Bubla	Bubla	k1gMnSc1	Bubla
<g/>
,	,	kIx,	,
Kajkl	Kajkl	k1gInSc1	Kajkl
<g/>
,	,	kIx,	,
Roháčik	Roháčik	k1gMnSc1	Roháčik
<g/>
,	,	kIx,	,
Dvořák	Dvořák	k1gMnSc1	Dvořák
–	–	k?	–
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Ebermann	Ebermann	k1gMnSc1	Ebermann
–	–	k?	–
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Hlinka	Hlinka	k1gMnSc1	Hlinka
<g/>
,	,	kIx,	,
Augusta	Augusta	k1gMnSc1	Augusta
–	–	k?	–
M.	M.	kA	M.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Pouzar	Pouzar	k1gMnSc1	Pouzar
–	–	k?	–
Černík	Černík	k1gMnSc1	Černík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
<g/>
:	:	kIx,	:
Treťjak	Treťjak	k1gMnSc1	Treťjak
–	–	k?	–
Cygankov	Cygankov	k1gInSc1	Cygankov
<g/>
,	,	kIx,	,
Fetisov	Fetisov	k1gInSc1	Fetisov
<g/>
,	,	kIx,	,
Biljaletdinov	Biljaletdinov	k1gInSc1	Biljaletdinov
<g/>
,	,	kIx,	,
Gusev	Gusev	k1gFnSc1	Gusev
<g/>
,	,	kIx,	,
Babinov	Babinov	k1gInSc1	Babinov
<g/>
,	,	kIx,	,
Lutčenko	Lutčenka	k1gFnSc5	Lutčenka
<g/>
,	,	kIx,	,
Vasiljev	Vasiljev	k1gFnSc5	Vasiljev
<g/>
,	,	kIx,	,
Pervuchin	Pervuchin	k1gMnSc1	Pervuchin
–	–	k?	–
Michajlov	Michajlov	k1gInSc1	Michajlov
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
Charlamov	Charlamov	k1gInSc1	Charlamov
–	–	k?	–
Šalimov	Šalimov	k1gInSc1	Šalimov
<g/>
,	,	kIx,	,
Šadrin	Šadrin	k1gInSc1	Šadrin
<g/>
,	,	kIx,	,
Jakušev	Jakušev	k1gFnSc1	Jakušev
–	–	k?	–
Balderis	Balderis	k1gInSc1	Balderis
<g/>
,	,	kIx,	,
Žluktov	Žluktov	k1gInSc1	Žluktov
<g/>
,	,	kIx,	,
Kapustin	Kapustin	k1gInSc1	Kapustin
–	–	k?	–
Malcev	Malcev	k1gFnSc1	Malcev
<g/>
,	,	kIx,	,
Golikov	Golikov	k1gInSc1	Golikov
<g/>
,	,	kIx,	,
Volčkov	Volčkov	k1gInSc1	Volčkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Gut	Gut	k1gMnSc1	Gut
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Pacina	Pacina	k1gMnSc1	Pacina
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Archív	archív	k1gInSc1	archív
časopisů	časopis	k1gInPc2	časopis
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
hockeyarchives	hockeyarchives	k1gMnSc1	hockeyarchives
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
<g/>
snt	snt	k?	snt
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
/	/	kIx~	/
</s>
</p>
