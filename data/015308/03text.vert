<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jeseníky	Jeseník	k1gInPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
JeseníkyIUCN	JeseníkyIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Hřeben	hřeben	k1gInSc1
Jeseníků	Jeseník	k1gInPc2
–	–	k?
pohled	pohled	k1gInSc1
z	z	k7c2
Dolní	dolní	k2eAgFnSc2d1
MoraviceZákladní	MoraviceZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1969	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
740	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
AOPK	AOPK	kA
ČR	ČR	kA
Regionální	regionální	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
a	a	k8xC
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Jeseník	Jeseník	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jeseníky	Jeseník	k1gInPc4
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
83	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.jeseniky.ochranaprirody.cz	www.jeseniky.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
je	být	k5eAaImIp3nS
chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Hrubý	hrubý	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
<g/>
,	,	kIx,
vyhlášená	vyhlášený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
výměrou	výměra	k1gFnSc7
744	#num#	k4
km	km	kA
<g/>
2	#num#	k4
(	(	kIx(
<g/>
dle	dle	k7c2
GIS	gis	k1gNnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
k	k	k7c3
největším	veliký	k2eAgFnPc3d3
CHKO	CHKO	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
a	a	k8xC
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
částech	část	k1gFnPc6
okresů	okres	k1gInPc2
Jeseník	Jeseník	k1gInSc1
<g/>
,	,	kIx,
Šumperk	Šumperk	k1gInSc1
a	a	k8xC
Bruntál	Bruntál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
CHKO	CHKO	kA
zasahují	zasahovat	k5eAaImIp3nP
územní	územní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
–	–	k?
Jeseník	Jeseník	k1gInSc4
<g/>
,	,	kIx,
Šumperk	Šumperk	k1gInSc1
<g/>
,	,	kIx,
Bruntál	Bruntál	k1gInSc1
<g/>
,	,	kIx,
Krnov	Krnov	k1gInSc1
a	a	k8xC
Rýmařov	Rýmařov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
CHKO	CHKO	kA
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
celkem	celkem	k6eAd1
61	#num#	k4
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
41	#num#	k4
plně	plně	k6eAd1
a	a	k8xC
20	#num#	k4
částí	část	k1gFnPc2
katastru	katastr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc4
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
je	být	k5eAaImIp3nS
v	v	k7c6
Jeseníku	Jeseník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jeseníky	Jeseník	k1gInPc4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
Výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
č.	č.	k?
j.	j.	k?
9886	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
-II	-II	k?
<g/>
/	/	kIx~
<g/>
2	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
podle	podle	k7c2
§	§	k?
<g/>
8	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
40	#num#	k4
<g/>
/	/	kIx~
<g/>
1956	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
výnosu	výnos	k1gInSc2
je	být	k5eAaImIp3nS
příloha	příloha	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
vymezuje	vymezovat	k5eAaImIp3nS
území	území	k1gNnSc3
CHKO	CHKO	kA
(	(	kIx(
<g/>
popis	popis	k1gInSc1
hranic	hranice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plocha	plocha	k1gFnSc1
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
podle	podle	k7c2
zřizovacího	zřizovací	k2eAgInSc2d1
výnosu	výnos	k1gInSc2
je	být	k5eAaImIp3nS
740	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Předměty	předmět	k1gInPc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Předměty	předmět	k1gInPc1
ochrany	ochrana	k1gFnSc2
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
vyhlášce	vyhláška	k1gFnSc6
přesně	přesně	k6eAd1
vyjmenovány	vyjmenován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodů	důvod	k1gInPc2
k	k	k7c3
existenci	existence	k1gFnSc3
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
je	být	k5eAaImIp3nS
však	však	k9
několik	několik	k4yIc4
<g/>
:	:	kIx,
<g/>
Petrovy	Petrův	k2eAgInPc1d1
kameny	kámen	k1gInPc1
</s>
<s>
vysokohorské	vysokohorský	k2eAgNnSc1d1
bezlesí	bezlesí	k1gNnSc1
–	–	k?
Pouze	pouze	k6eAd1
3	#num#	k4
pohoří	pohoří	k1gNnPc2
v	v	k7c6
ČR	ČR	kA
dosahují	dosahovat	k5eAaImIp3nP
výšek	výška	k1gFnPc2
nad	nad	k7c4
horní	horní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
lesa	les	k1gInSc2
(	(	kIx(
<g/>
Krkonoše	Krkonoše	k1gFnPc1
<g/>
,	,	kIx,
Hrubý	hrubý	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
<g/>
,	,	kIx,
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
vysoká	vysoký	k2eAgFnSc1d1
lesnatost	lesnatost	k1gFnSc1
a	a	k8xC
smrkové	smrkový	k2eAgInPc1d1
pralesy	prales	k1gInPc1
–	–	k?
Lesnatost	lesnatost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
80	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
Jeseníky	Jeseník	k1gInPc1
jsou	být	k5eAaImIp3nP
nejlesnatější	lesnatý	k2eAgFnSc4d3
CHKO	CHKO	kA
v	v	k7c6
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kromě	kromě	k7c2
smrkových	smrkový	k2eAgInPc2d1
pralesů	prales	k1gInPc2
stojí	stát	k5eAaImIp3nS
za	za	k7c4
pozornost	pozornost	k1gFnSc4
i	i	k9
zachovalé	zachovalý	k2eAgInPc1d1
fragmenty	fragment	k1gInPc1
bučin	bučina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
parková	parkový	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
lesa	les	k1gInSc2
–	–	k?
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Krkonoš	Krkonoše	k1gFnPc2
nebo	nebo	k8xC
Tater	Tatra	k1gFnPc2
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
nerostla	růst	k5eNaImAgFnS
kleč	kleč	k1gFnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
unikátní	unikátní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
lesa	les	k1gInSc2
připomínající	připomínající	k2eAgFnSc4d1
parkovou	parkový	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
rašeliniště	rašeliniště	k1gNnPc4
a	a	k8xC
prameny	pramen	k1gInPc4
–	–	k?
Voda	voda	k1gFnSc1
hraje	hrát	k5eAaImIp3nS
velmi	velmi	k6eAd1
významnou	významný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
v	v	k7c6
krajině	krajina	k1gFnSc6
<g/>
,	,	kIx,
mnohá	mnohý	k2eAgNnPc1d1
rašeliniště	rašeliniště	k1gNnPc1
jsou	být	k5eAaImIp3nP
chráněná	chráněný	k2eAgNnPc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
,	,	kIx,
prameny	pramen	k1gInPc1
zapříčinily	zapříčinit	k5eAaPmAgInP
vznik	vznik	k1gInSc4
několika	několik	k4yIc2
lázeňských	lázeňský	k2eAgNnPc2d1
středisek	středisko	k1gNnPc2
(	(	kIx(
<g/>
Karlova	Karlův	k2eAgFnSc1d1
Studánka	studánka	k1gFnSc1
<g/>
,	,	kIx,
Jeseník	Jeseník	k1gInSc1
<g/>
,	,	kIx,
Lipová-lázně	Lipová-lázeň	k1gFnPc1
<g/>
,	,	kIx,
Velké	velký	k2eAgFnPc1d1
Losiny	Losiny	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
NPR	NPR	kA
Rašeliniště	rašeliniště	k1gNnSc1
Skřítek	skřítek	k1gMnSc1
</s>
<s>
vzácné	vzácný	k2eAgInPc1d1
druhy	druh	k1gInPc1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
–	–	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
o	o	k7c4
jesenické	jesenický	k2eAgInPc4d1
endemity	endemit	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nejen	nejen	k6eAd1
o	o	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s>
lidová	lidový	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
–	–	k?
Posláním	poslání	k1gNnSc7
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
také	také	k9
ochrana	ochrana	k1gFnSc1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
štoly	štola	k1gFnPc1
a	a	k8xC
podzemí	podzemí	k1gNnSc1
–	–	k?
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
poddolovaných	poddolovaný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
nejen	nejen	k6eAd1
technickou	technický	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
poskytují	poskytovat	k5eAaImIp3nP
prostor	prostor	k1gInSc4
jako	jako	k8xC,k8xS
zimoviště	zimoviště	k1gNnSc2
četným	četný	k2eAgInPc3d1
letounům	letoun	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
krajinný	krajinný	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
,	,	kIx,
louky	louka	k1gFnPc4
a	a	k8xC
meze	mez	k1gFnSc2
–	–	k?
Např.	např.	kA
květnaté	květnatý	k2eAgFnSc2d1
druhově	druhově	k6eAd1
bohaté	bohatý	k2eAgFnSc2d1
louky	louka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
geomorfologie	geomorfologie	k1gFnSc1
–	–	k?
V	v	k7c6
CHKO	CHKO	kA
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
najít	najít	k5eAaPmF
různé	různý	k2eAgInPc1d1
geomorfologické	geomorfologický	k2eAgInPc1d1
tvary	tvar	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
ledovcový	ledovcový	k2eAgInSc1d1
kar	kar	k1gInSc1
Velké	velký	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
,	,	kIx,
mrazový	mrazový	k2eAgInSc1d1
srub	srub	k1gInSc1
Petrových	Petrových	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
četná	četný	k2eAgNnPc4d1
kamenná	kamenný	k2eAgNnPc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
zachovalá	zachovalý	k2eAgFnSc1d1
geomorfologie	geomorfologie	k1gFnSc1
říčních	říční	k2eAgInPc2d1
toků	tok	k1gInPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
mineralogické	mineralogický	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
–	–	k?
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
naleziště	naleziště	k1gNnPc4
např.	např.	kA
epidotu	epidot	k1gInSc2
<g/>
,	,	kIx,
křišťálu	křišťál	k1gInSc2
<g/>
,	,	kIx,
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
almandinu	almandin	k1gInSc2
<g/>
,	,	kIx,
krupníku	krupník	k1gInSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc1
byla	být	k5eAaImAgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
ochrany	ochrana	k1gFnSc2
evropsky	evropsky	k6eAd1
významných	významný	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
a	a	k8xC
druhů	druh	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
soustavy	soustava	k1gFnSc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
část	část	k1gFnSc1
území	území	k1gNnSc2
vyhlášena	vyhlásit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
Ptačí	ptačit	k5eAaImIp3nP
oblast	oblast	k1gFnSc4
Jeseníky	Jeseník	k1gInPc1
a	a	k8xC
do	do	k7c2
seznamu	seznam	k1gInSc2
evropsky	evropsky	k6eAd1
významných	významný	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
zařazeno	zařadit	k5eAaPmNgNnS
celkem	celkem	k6eAd1
14	#num#	k4
lokalit	lokalita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
část	část	k1gFnSc1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
v	v	k7c6
Pradědské	Pradědský	k2eAgFnSc6d1
hornatině	hornatina	k1gFnSc6
je	být	k5eAaImIp3nS
zařazena	zařazen	k2eAgFnSc1d1
do	do	k7c2
celoevropské	celoevropský	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
EECONET	EECONET	kA
(	(	kIx(
<g/>
European	European	k1gMnSc1
Ecological	Ecological	k1gFnSc2
Network	network	k1gInSc1
–	–	k?
zóny	zóna	k1gFnSc2
zvýšené	zvýšený	k2eAgFnSc2d1
péče	péče	k1gFnSc2
o	o	k7c4
krajinu	krajina	k1gFnSc4
navržené	navržený	k2eAgFnSc2d1
pro	pro	k7c4
Evropskou	evropský	k2eAgFnSc4d1
ekologickou	ekologický	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celá	celý	k2eAgFnSc1d1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
tvoří	tvořit	k5eAaImIp3nS
IBA	iba	k6eAd1
(	(	kIx(
<g/>
Important	Important	k1gMnSc1
Bird	Bird	k1gMnSc1
Area	area	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
EVL	EVL	kA
Praděd	praděd	k1gMnSc1
<g/>
,	,	kIx,
NPR	NPR	kA
Rejvíz	Rejvíz	k1gInSc1
a	a	k8xC
PR	pr	k0
Šumárník	Šumárník	k1gInSc1
jsou	být	k5eAaImIp3nP
zařazeny	zařadit	k5eAaPmNgInP
mezi	mezi	k7c4
IPA	IPA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
(	(	kIx(
<g/>
Important	Important	k1gMnSc1
Plant	planta	k1gFnPc2
Areas	Areas	k1gMnSc1
<g/>
,	,	kIx,
vymezená	vymezený	k2eAgFnSc1d1
pro	pro	k7c4
Planta	planta	k1gFnSc1
Europea	Europea	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
mezinárodně	mezinárodně	k6eAd1
významných	významný	k2eAgFnPc2d1
částí	část	k1gFnPc2
přírody	příroda	k1gFnSc2
dle	dle	k7c2
kategorizace	kategorizace	k1gFnSc2
EU	EU	kA
(	(	kIx(
<g/>
Corine	Corin	k1gInSc5
Biotopes	Biotopesa	k1gFnPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc7
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
8	#num#	k4
takto	takto	k6eAd1
klasifikovaných	klasifikovaný	k2eAgNnPc2d1
komplexních	komplexní	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
deset	deset	k4xCc4
dílčích	dílčí	k2eAgFnPc2d1
území	území	k1gNnSc4
pak	pak	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
lokality	lokalita	k1gFnPc4
vymezené	vymezený	k2eAgFnPc4d1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
jednoho	jeden	k4xCgInSc2
typu	typ	k1gInSc2
přírodního	přírodní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
či	či	k8xC
populace	populace	k1gFnSc2
jedné	jeden	k4xCgFnSc2
skupiny	skupina	k1gFnSc2
organismů	organismus	k1gInPc2
s	s	k7c7
obdobnými	obdobný	k2eAgInPc7d1
ekologickými	ekologický	k2eAgInPc7d1
nároky	nárok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Rejvíz	Rejvíz	k1gMnSc1
byla	být	k5eAaImAgFnS
zařazena	zařadit	k5eAaPmNgFnS
do	do	k7c2
soustavy	soustava	k1gFnSc2
EMERALD	EMERALD	kA
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInSc1d1
evropsky	evropsky	k6eAd1
významná	významný	k2eAgNnPc4d1
území	území	k1gNnPc4
vymezená	vymezený	k2eAgNnPc4d1
v	v	k7c6
rámci	rámec	k1gInSc6
Bernské	bernský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Horské	Horské	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
primární	primární	k2eAgNnSc1d1
vysokohorské	vysokohorský	k2eAgNnSc1d1
a	a	k8xC
rašelinné	rašelinný	k2eAgNnSc1d1
bezlesí	bezlesí	k1gNnSc1
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
zařazeny	zařazen	k2eAgMnPc4d1
mezi	mezi	k7c4
významná	významný	k2eAgNnPc4d1
motýlí	motýlí	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
Lipnice	Lipnice	k1gFnSc1
jesenickáZvonek	jesenickáZvonka	k1gFnPc2
jesenický	jesenický	k2eAgInSc1d1
</s>
<s>
Jesenická	jesenický	k2eAgFnSc1d1
flóra	flóra	k1gFnSc1
čítá	čítat	k5eAaImIp3nS
asi	asi	k9
1200	#num#	k4
druhů	druh	k1gInPc2
a	a	k8xC
poddruhů	poddruh	k1gInPc2
vyšších	vysoký	k2eAgFnPc2d2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
třetina	třetina	k1gFnSc1
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
rostou	růst	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flóra	Flóra	k1gFnSc1
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
je	být	k5eAaImIp3nS
charakterizována	charakterizovat	k5eAaBmNgFnS
především	především	k9
jesenickými	jesenický	k2eAgInPc7d1
vysokohorskými	vysokohorský	k2eAgInPc7d1
endemity	endemit	k1gInPc7
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
jsou	být	k5eAaImIp3nP
lipnice	lipnice	k1gFnSc1
jesenická	jesenický	k2eAgFnSc1d1
(	(	kIx(
<g/>
Poa	Poa	k1gFnSc1
riphaea	riphaea	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvonek	zvonek	k1gInSc4
jesenický	jesenický	k2eAgInSc4d1
(	(	kIx(
<g/>
Campanula	Campanula	k1gFnSc1
gelida	gelida	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hvozdík	hvozdík	k1gInSc1
kartouzek	kartouzek	k1gInSc1
sudetský	sudetský	k2eAgInSc1d1
(	(	kIx(
<g/>
Dianthus	Dianthus	k1gInSc1
carthusianorum	carthusianorum	k1gNnSc1
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
sudeticus	sudeticus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jitrocel	jitrocel	k1gInSc1
černavý	černavý	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
sudetský	sudetský	k2eAgInSc4d1
(	(	kIx(
<g/>
Plantago	Plantago	k6eAd1
atrata	atrata	k1gFnSc1
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
sudetica	sudetica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pupava	pupava	k1gFnSc1
Biebersteinova	Biebersteinův	k2eAgFnSc1d1
sudetská	sudetský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carlina	Carlina	k1gFnSc1
biebersteinii	biebersteinie	k1gFnSc4
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
sudetica	sudetica	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
glaciálními	glaciální	k2eAgInPc7d1
relikty	relikt	k1gInPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
vrba	vrba	k1gFnSc1
bylinná	bylinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Salix	Salix	k1gInSc1
herbacea	herbace	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrba	vrba	k1gFnSc1
laponská	laponský	k2eAgFnSc1d1
(	(	kIx(
<g/>
S.	S.	kA
lapponum	lapponum	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řeřišnice	řeřišnice	k1gFnSc1
rýtolistá	rýtolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cardamine	Cardamin	k1gInSc5
resedifolia	resedifolium	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lepnice	lepnice	k1gFnSc1
alpská	alpský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bartsia	Bartsia	k1gFnSc1
alpina	alpinum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
psineček	psineček	k1gInSc1
alpský	alpský	k2eAgInSc1d1
(	(	kIx(
<g/>
Agrostis	Agrostis	k1gInSc1
alpina	alpinum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lipnice	lipnice	k1gFnSc1
alpská	alpský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Poa	Poa	k1gFnSc1
alpina	alpinum	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alpínské	alpínský	k2eAgInPc1d1
trávníky	trávník	k1gInPc1
charakterizují	charakterizovat	k5eAaBmIp3nP
jestřábník	jestřábník	k1gInSc1
alpský	alpský	k2eAgInSc1d1
(	(	kIx(
<g/>
Hieracium	Hieracium	k1gNnSc1
alpinum	alpinum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prasetník	prasetník	k1gInSc1
jednoúborný	jednoúborný	k2eAgInSc1d1
(	(	kIx(
<g/>
Hypochaeris	Hypochaeris	k1gInSc1
uniflora	uniflor	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
Bigelowova	Bigelowov	k1gInSc2
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
bigelowii	bigelowie	k1gFnSc4
<g/>
)	)	kIx)
či	či	k8xC
sasanka	sasanka	k1gFnSc1
narcisokvětá	narcisokvětý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Anemone	Anemon	k1gInSc5
narcissiflora	narcissiflor	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
Jeseníky	Jeseník	k1gInPc4
i	i	k8xC
druhy	druh	k1gInPc4
se	s	k7c7
subalpínským	subalpínský	k2eAgNnSc7d1
a	a	k8xC
supramontánním	supramontánní	k2eAgNnSc7d1
těžištěm	těžiště	k1gNnSc7
výskytu	výskyt	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
havez	havez	k1gFnSc1
česnáčková	česnáčková	k1gFnSc1
(	(	kIx(
<g/>
Adenostyles	Adenostyles	k1gInSc1
alliarae	alliara	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oměj	oměj	k1gInSc1
šalamounek	šalamounek	k1gInSc1
(	(	kIx(
<g/>
Aconitum	Aconitum	k1gNnSc1
plicatum	plicatum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stračka	stračka	k1gFnSc1
vyvýšená	vyvýšený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Delphinium	Delphinium	k1gNnSc1
elatum	elatum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
atrata	atrat	k1gMnSc2
<g/>
)	)	kIx)
aj.	aj.	kA
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
botanických	botanický	k2eAgInPc2d1
průzkumů	průzkum	k1gInPc2
Jeseníků	Jeseník	k1gInPc2
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
současnosti	současnost	k1gFnSc2
byl	být	k5eAaImAgInS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
zaznamenán	zaznamenán	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
celkem	celkem	k6eAd1
135	#num#	k4
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgInPc2d1
druhů	druh	k1gInPc2
cévnatých	cévnatý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
uvedených	uvedený	k2eAgInPc2d1
ve	v	k7c6
vyhlášce	vyhláška	k1gFnSc6
MŽP	MŽP	kA
č.	č.	k?
395	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
113	#num#	k4
zvláště	zvláště	k9
chráněných	chráněný	k2eAgMnPc2d1
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
zbývajících	zbývající	k2eAgInPc2d1
22	#num#	k4
zvláště	zvláště	k9
chráněných	chráněný	k2eAgMnPc2d1
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
považovaných	považovaný	k2eAgFnPc2d1
pro	pro	k7c4
Jeseníky	Jeseník	k1gInPc4
za	za	k7c4
nezvěstné	zvěstný	k2eNgMnPc4d1
nebo	nebo	k8xC
vyhynulé	vyhynulý	k2eAgMnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Z	z	k7c2
uvedeného	uvedený	k2eAgInSc2d1
počtu	počet	k1gInSc2
113	#num#	k4
recentně	recentně	k6eAd1
se	se	k3xPyFc4
vyskytujících	vyskytující	k2eAgInPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgInPc2d1
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
kategorii	kategorie	k1gFnSc6
kriticky	kriticky	k6eAd1
ohrožených	ohrožený	k2eAgMnPc2d1
zařazeno	zařadit	k5eAaPmNgNnS
40	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
kategorii	kategorie	k1gFnSc6
silně	silně	k6eAd1
ohrožených	ohrožený	k2eAgMnPc2d1
je	být	k5eAaImIp3nS
zařazeno	zařadit	k5eAaPmNgNnS
38	#num#	k4
druhů	druh	k1gInPc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
ohrožených	ohrožený	k2eAgInPc2d1
je	být	k5eAaImIp3nS
35	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
v	v	k7c6
ČR	ČR	kA
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
vyskytují	vyskytovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
Agrostis	Agrostis	k1gInSc4
alpina	alpinum	k1gNnSc2
<g/>
,	,	kIx,
Crepis	Crepis	k1gFnSc1
sibirica	sibirica	k1gFnSc1
<g/>
,	,	kIx,
Cystopteris	Cystopteris	k1gFnSc1
sudetica	sudetica	k1gFnSc1
<g/>
,	,	kIx,
Gentiana	Gentiana	k1gFnSc1
punctata	punctata	k1gFnSc1
<g/>
,	,	kIx,
Helianthemum	Helianthemum	k1gInSc1
grandiflorum	grandiflorum	k1gInSc1
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
grandiflorum	grandiflorum	k1gNnSc1
<g/>
,	,	kIx,
Hieracium	Hieracium	k1gNnSc1
moravicum	moravicum	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Hieracium	Hieracium	k1gNnSc1
villosum	villosum	k1gInSc4
<g/>
,	,	kIx,
Poa	Poa	k1gFnSc4
alpina	alpinum	k1gNnSc2
<g/>
,	,	kIx,
Salix	Salix	k1gInSc1
hastata	hastata	k1gFnSc1
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
vegeta	vegeta	k1gFnSc1
<g/>
,	,	kIx,
Thymus	Thymus	k1gMnSc1
pulcherrimus	pulcherrimus	k1gMnSc1
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
sudeticus	sudeticus	k1gMnSc1
...	...	k?
<g/>
)	)	kIx)
a	a	k8xC
rovněž	rovněž	k9
řada	řada	k1gFnSc1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vedle	vedle	k7c2
Jeseníků	Jeseník	k1gInPc2
v	v	k7c6
ČR	ČR	kA
vyskytují	vyskytovat	k5eAaImIp3nP
jen	jen	k9
na	na	k7c6
několika	několik	k4yIc6
málo	málo	k6eAd1
dalších	další	k2eAgFnPc6d1
lokalitách	lokalita	k1gFnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
např.	např.	kA
Arabis	Arabis	k1gFnSc1
sudetica	sudetica	k1gFnSc1
<g/>
,	,	kIx,
Bartsia	Bartsia	k1gFnSc1
alpina	alpinum	k1gNnSc2
<g/>
,	,	kIx,
Bupleurum	Bupleurum	k1gInSc1
longifolium	longifolium	k1gNnSc1
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
vapincense	vapincens	k1gMnSc2
<g/>
,	,	kIx,
Campanula	Campanula	k1gMnSc2
rotundifolia	rotundifolius	k1gMnSc2
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
sudetica	sudetica	k1gMnSc1
<g/>
,	,	kIx,
Cardamine	Cardamin	k1gInSc5
resedifolia	resedifolium	k1gNnPc1
<g/>
,	,	kIx,
Carex	Carex	k1gInSc1
aterrima	aterrima	k1gFnSc1
<g/>
,	,	kIx,
Carex	Carex	k1gInSc1
atrata	atrata	k1gFnSc1
<g/>
,	,	kIx,
Carex	Carex	k1gInSc1
rupestris	rupestris	k1gInSc1
<g/>
,	,	kIx,
Carex	Carex	k1gInSc1
vaginata	vaginata	k1gFnSc1
<g/>
,	,	kIx,
Dianthus	Dianthus	k1gMnSc1
superbus	superbus	k1gMnSc1
subsp	subsp	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
alpestris	alpestris	k1gFnSc1
<g/>
,	,	kIx,
Hieracium	Hieracium	k1gNnSc1
alpinum	alpinum	k1gNnSc1
<g/>
,	,	kIx,
Hedysarum	Hedysarum	k1gNnSc1
hedysaroides	hedysaroidesa	k1gFnPc2
<g/>
,	,	kIx,
Rhodiola	Rhodiola	k1gFnSc1
rosea	rosea	k1gFnSc1
<g/>
,	,	kIx,
Salix	Salix	k1gInSc1
herbacea	herbacea	k1gFnSc1
<g/>
,	,	kIx,
Salix	Salix	k1gInSc1
lapponum	lapponum	k1gInSc1
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
lapponum	lapponum	k1gInSc1
<g/>
,	,	kIx,
Scabiosa	Scabiosa	k1gFnSc1
lucida	lucida	k1gFnSc1
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
lucida	lucida	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
na	na	k7c6
Králickém	králický	k2eAgInSc6d1
Sněžníku	Sněžník	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
Campanula	Campanula	k1gFnSc1
barbata	barbata	k1gFnSc1
<g/>
,	,	kIx,
Cerastium	Cerastium	k1gNnSc1
fontanum	fontanum	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Helictochloa	Helictochlo	k2eAgFnSc1d1
planiculmis	planiculmis	k1gFnSc1
<g/>
,	,	kIx,
Hieracium	Hieracium	k1gNnSc1
chrysostyloides	chrysostyloidesa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
jen	jen	k9
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
a	a	k8xC
v	v	k7c6
obou	dva	k4xCgNnPc6
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgNnPc6d1
pohořích	pohoří	k1gNnPc6
(	(	kIx(
<g/>
např.	např.	kA
Anemonastrum	Anemonastrum	k1gNnSc1
narcissiflorum	narcissiflorum	k1gInSc1
<g/>
,	,	kIx,
Cardamine	Cardamin	k1gInSc5
amara	amaro	k1gNnSc2
subsp	subsp	k1gInSc4
<g/>
.	.	kIx.
opicii	opicie	k1gFnSc4
<g/>
,	,	kIx,
Carex	Carex	k1gInSc4
bigelowii	bigelowie	k1gFnSc3
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
dacica	dacica	k1gFnSc1
<g/>
,	,	kIx,
Hieracium	Hieracium	k1gNnSc1
inuloides	inuloides	k1gMnSc1
<g/>
,	,	kIx,
hinanthus	hinanthus	k1gMnSc1
riphaeus	riphaeus	k1gMnSc1
<g/>
,	,	kIx,
Selaginella	Selaginella	k1gMnSc1
selaginoides	selaginoides	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Jedinečnost	jedinečnost	k1gFnSc1
fauny	fauna	k1gFnSc2
Jeseníků	Jeseník	k1gInPc2
dokazuje	dokazovat	k5eAaImIp3nS
vedle	vedle	k7c2
přítomnosti	přítomnost	k1gFnSc2
glaciálních	glaciální	k2eAgInPc2d1
reliktů	relikt	k1gInPc2
také	také	k9
několik	několik	k4yIc4
endemických	endemický	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
vyskytujících	vyskytující	k2eAgMnPc2d1
se	se	k3xPyFc4
pouze	pouze	k6eAd1
zde	zde	k6eAd1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
dvacet	dvacet	k4xCc1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
vyskytujících	vyskytující	k2eAgInPc2d1
na	na	k7c4
území	území	k1gNnSc4
CHKO	CHKO	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zařazeno	zařadit	k5eAaPmNgNnS
do	do	k7c2
kategorie	kategorie	k1gFnSc2
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bezobratlí	bezobratlí	k1gMnPc1
</s>
<s>
Saranče	saranče	k1gFnSc1
horskáStřevlík	horskáStřevlík	k1gMnSc1
hrbolatý	hrbolatý	k2eAgMnSc1d1
</s>
<s>
Ze	z	k7c2
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
motýlů	motýl	k1gMnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
především	především	k9
jasoň	jasoň	k1gMnSc1
dymnivkový	dymnivkový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Parnassius	Parnassius	k1gMnSc1
mnemosyne	mnemosynout	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
3	#num#	k4
lokality	lokalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeseničtí	jesenický	k2eAgMnPc5d1
endemiti	endemit	k5eAaPmF
okáč	okáč	k1gMnSc1
menší	malý	k2eAgMnSc1d2
(	(	kIx(
<g/>
Erebia	Erebius	k1gMnSc4
sudetica	sudeticus	k1gMnSc4
sudetica	sudeticus	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
okáč	okáč	k1gMnSc1
horský	horský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Erebia	Erebia	k1gFnSc1
epiphron	epiphron	k1gInSc1
silesiana	silesiana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
okáč	okáč	k1gMnSc1
horský	horský	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
druhotně	druhotně	k6eAd1
vysazen	vysadit	k5eAaPmNgInS
i	i	k9
do	do	k7c2
Krkonoš	Krkonoše	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
obaleč	obaleč	k1gMnSc1
Sparganothis	Sparganothis	k1gFnSc2
rubicundana	rubicundana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vrcholových	vrcholový	k2eAgFnPc2d1
částí	část	k1gFnPc2
Jeseníků	Jeseník	k1gInPc2
a	a	k8xC
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
ho	on	k3xPp3gInSc4
můžeme	moct	k5eAaImIp1nP
najít	najít	k5eAaPmF
nejblíže	blízce	k6eAd3
až	až	k9
v	v	k7c6
severní	severní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
Skandinávie	Skandinávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgNnSc7d3
ohrožením	ohrožení	k1gNnSc7
pro	pro	k7c4
druhy	druh	k1gInPc4
alpínského	alpínský	k2eAgNnSc2d1
bezlesí	bezlesí	k1gNnSc2
představují	představovat	k5eAaImIp3nP
porosty	porost	k1gInPc1
borovice	borovice	k1gFnSc2
kleče	kleč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zarůstáním	zarůstání	k1gNnSc7
vhodných	vhodný	k2eAgNnPc2d1
stanovišť	stanoviště	k1gNnPc2
klečí	klečet	k5eAaImIp3nS
zcela	zcela	k6eAd1
vymizel	vymizet	k5eAaPmAgMnS
okáč	okáč	k1gMnSc1
menší	malý	k2eAgMnSc1d2
ze	z	k7c2
Sněžné	sněžný	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
i	i	k9
z	z	k7c2
okolí	okolí	k1gNnSc2
Jelení	jelení	k2eAgFnSc2d1
studánky	studánka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Rejvíz	Rejvíz	k1gMnSc1
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
šídla	šídlo	k1gNnSc2
rašelinného	rašelinný	k2eAgNnSc2d1
(	(	kIx(
<g/>
Aeshna	Aeshno	k1gNnSc2
subarctica	subarctic	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
tyrfobiontní	tyrfobiontní	k2eAgInSc1d1
glaciální	glaciální	k2eAgInSc1d1
relikt	relikt	k1gInSc1
zde	zde	k6eAd1
má	mít	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc4d1
lokalitu	lokalita	k1gFnSc4
nejen	nejen	k6eAd1
v	v	k7c6
CHKO	CHKO	kA
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
vzácné	vzácný	k2eAgInPc1d1
druhy	druh	k1gInPc1
vážek	vážka	k1gFnPc2
jsou	být	k5eAaImIp3nP
lesknice	lesknice	k1gFnSc1
horská	horský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Somatochlora	Somatochlora	k1gFnSc1
alpestris	alpestris	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
vážka	vážka	k1gFnSc1
čárkovaná	čárkovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leucorrhinia	Leucorrhinium	k1gNnSc2
dubia	dubium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1
brouk	brouk	k1gMnSc1
Jeseníků	Jeseník	k1gInPc2
zákonem	zákon	k1gInSc7
chráněný	chráněný	k2eAgMnSc1d1
jako	jako	k9
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
je	být	k5eAaImIp3nS
roháček	roháček	k1gInSc1
jedlový	jedlový	k2eAgInSc1d1
(	(	kIx(
<g/>
Ceruchus	Ceruchus	k1gInSc1
chrysomelinus	chrysomelinus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
hojný	hojný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
střevlík	střevlík	k1gMnSc1
hrbolatý	hrbolatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Carabus	Carabus	k1gMnSc1
variolosus	variolosus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákonem	zákon	k1gInSc7
chráněný	chráněný	k2eAgInSc4d1
druh	druh	k1gInSc4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
silně	silně	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
obývá	obývat	k5eAaImIp3nS
především	především	k9
okolí	okolí	k1gNnSc1
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
i	i	k9
legislativou	legislativa	k1gFnSc7
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významný	významný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
tesařík	tesařík	k1gMnSc1
čtyřpásý	čtyřpásý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cornumutila	Cornumutila	k1gMnSc1
quadrivittata	quadrivittat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
obývá	obývat	k5eAaImIp3nS
smrky	smrk	k1gInPc4
při	při	k7c6
horní	horní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
lesa	les	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
další	další	k2eAgFnSc7d1
nejbližší	blízký	k2eAgFnSc7d3
lokalitou	lokalita	k1gFnSc7
jsou	být	k5eAaImIp3nP
až	až	k9
Tatry	Tatra	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrcholové	vrcholový	k2eAgNnSc4d1
bezlesí	bezlesí	k1gNnSc4
je	být	k5eAaImIp3nS
zase	zase	k9
vázán	vázat	k5eAaImNgMnS
hnojník	hnojník	k1gMnSc1
Aphodius	Aphodius	k1gMnSc1
bilimeckii	bilimeckie	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
pravděpodobně	pravděpodobně	k6eAd1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
svůj	svůj	k3xOyFgInSc4
endemický	endemický	k2eAgInSc4d1
poddruh	poddruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Unikátní	unikátní	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
bezobratlých	bezobratlý	k2eAgMnPc2d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Velká	velký	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
v	v	k7c6
NPR	NPR	kA
Praděd	praděd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
pro	pro	k7c4
vědu	věda	k1gFnSc4
několik	několik	k4yIc4
nových	nový	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
jeden	jeden	k4xCgInSc4
nový	nový	k2eAgInSc4d1
rod	rod	k1gInSc4
chvostoskoků	chvostoskok	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dostal	dostat	k5eAaPmAgMnS
odborný	odborný	k2eAgInSc4d1
název	název	k1gInSc4
Jesenikia	Jesenikium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spousta	spousta	k1gFnSc1
druhů	druh	k1gInPc2
bezobratlých	bezobratlý	k2eAgMnPc2d1
má	mít	k5eAaImIp3nS
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
svoji	svůj	k3xOyFgFnSc4
jedinou	jediný	k2eAgFnSc4d1
lokalitu	lokalita	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
jmenujme	jmenovat	k5eAaBmRp1nP,k5eAaImRp1nP
alespoň	alespoň	k9
střevlíčka	střevlíček	k1gMnSc4
Paradromius	Paradromius	k1gMnSc1
strigiceps	strigicepsa	k1gFnPc2
nebo	nebo	k8xC
nosatce	nosatec	k1gMnSc2
Ranunculiphilus	Ranunculiphilus	k1gInSc1
pseudinclemens	pseudinclemens	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
z	z	k7c2
hlediska	hledisko	k1gNnSc2
ochrany	ochrana	k1gFnSc2
bezobratlých	bezobratlý	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
je	být	k5eAaImIp3nS
PR	pr	k0
Pod	pod	k7c7
Jelení	jelení	k2eAgFnSc7d1
studánkou	studánka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
největších	veliký	k2eAgInPc2d3
komplexů	komplex	k1gInPc2
mravenišť	mraveniště	k1gNnPc2
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
možná	možná	k9
i	i	k9
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
posledního	poslední	k2eAgNnSc2d1
sčítání	sčítání	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
rezervaci	rezervace	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
1265	#num#	k4
hnízd	hnízdo	k1gNnPc2
mravenců	mravenec	k1gMnPc2
podhorních	podhorní	k2eAgMnPc2d1
(	(	kIx(
<g/>
Formica	Formica	k1gMnSc1
lugubris	lugubris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
hustotě	hustota	k1gFnSc6
až	až	k9
21	#num#	k4
hnízd	hnízdo	k1gNnPc2
na	na	k7c4
hektar	hektar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
přechodně	přechodně	k6eAd1
chráněná	chráněný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
Prameny	pramen	k1gInPc1
Javorné	Javorný	k2eAgInPc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
ochrany	ochrana	k1gFnSc2
bohatého	bohatý	k2eAgNnSc2d1
společenstva	společenstvo	k1gNnSc2
měkkýšů	měkkýš	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
3	#num#	k4
ha	ha	kA
bylo	být	k5eAaImAgNnS
napočítáno	napočítán	k2eAgNnSc1d1
50	#num#	k4
druhů	druh	k1gInPc2
měkkýšů	měkkýš	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
moravský	moravský	k2eAgInSc4d1
endemit	endemit	k1gInSc4
vřetenatka	vřetenatka	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vestia	Vestia	k1gFnSc1
ranojevici	ranojevice	k1gFnSc4
moravica	moravic	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzácným	vzácný	k2eAgMnSc7d1
obyvatelem	obyvatel	k1gMnSc7
extenzivních	extenzivní	k2eAgFnPc2d1
pastvin	pastvina	k1gFnPc2
je	být	k5eAaImIp3nS
saranče	saranče	k1gFnSc1
vrzavá	vrzavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Psophus	Psophus	k1gMnSc1
stridulus	stridulus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
dvě	dva	k4xCgFnPc4
lokality	lokalita	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vrcholových	vrcholový	k2eAgFnPc6d1
partiích	partie	k1gFnPc6
NPR	NPR	kA
Praděd	praděd	k1gMnSc1
žije	žít	k5eAaImIp3nS
saranče	saranče	k1gFnPc4
horská	horský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojí	stát	k5eAaImIp3nS
i	i	k9
ploštice	ploštice	k1gFnSc1
Pithanus	Pithanus	k1gInSc1
hrabei	hrabee	k1gFnSc4
popsaná	popsaný	k2eAgFnSc1d1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
roku	rok	k1gInSc2
1947	#num#	k4
a	a	k8xC
znovunalezená	znovunalezený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
po	po	k7c6
63	#num#	k4
letech	léto	k1gNnPc6
„	„	k?
<g/>
nezvěstnosti	nezvěstnost	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
okrajových	okrajový	k2eAgFnPc6d1
částech	část	k1gFnPc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
zaznamenaný	zaznamenaný	k2eAgInSc1d1
i	i	k8xC
ojedinělý	ojedinělý	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
raka	rak	k1gMnSc2
říčního	říční	k2eAgMnSc2d1
(	(	kIx(
<g/>
Astacus	Astacus	k1gInSc1
astacus	astacus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obratlovci	obratlovec	k1gMnPc1
</s>
<s>
V	v	k7c6
řekách	řeka	k1gFnPc6
můžeme	moct	k5eAaImIp1nP
najít	najít	k5eAaPmF
mihuli	mihule	k1gFnSc4
potoční	potoční	k2eAgFnSc4d1
<g/>
,	,	kIx,
z	z	k7c2
ryb	ryba	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
např.	např.	kA
pstruh	pstruh	k1gMnSc1
potoční	potoční	k2eAgMnSc1d1
<g/>
,	,	kIx,
vranka	vranka	k1gFnSc1
pruhoploutvá	pruhoploutvá	k1gFnSc1
nebo	nebo	k8xC
lipan	lipan	k1gMnSc1
podhorní	podhorní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zástupci	zástupce	k1gMnPc1
obojživelníků	obojživelník	k1gMnPc2
jsou	být	k5eAaImIp3nP
především	především	k9
čolek	čolek	k1gMnSc1
karpatský	karpatský	k2eAgMnSc1d1
<g/>
,	,	kIx,
čolek	čolek	k1gMnSc1
horský	horský	k2eAgMnSc1d1
<g/>
,	,	kIx,
čolek	čolek	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
ropucha	ropucha	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
skokan	skokan	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
a	a	k8xC
mlok	mlok	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Doposud	doposud	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
téměř	téměř	k6eAd1
200	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
z	z	k7c2
tohoto	tento	k3xDgInSc2
počtu	počet	k1gInSc2
na	na	k7c4
území	území	k1gNnSc4
CHKO	CHKO	kA
také	také	k6eAd1
pravidelně	pravidelně	k6eAd1
hnízdí	hnízdit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
cennosti	cennost	k1gFnSc3
pro	pro	k7c4
ptačí	ptačí	k2eAgFnSc4d1
faunu	fauna	k1gFnSc4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc4
Jeseníků	Jeseník	k1gInPc2
vyhlášeno	vyhlášen	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
významné	významný	k2eAgNnSc1d1
ptačí	ptačí	k2eAgNnSc1d1
území	území	k1gNnSc1
a	a	k8xC
následně	následně	k6eAd1
pak	pak	k6eAd1
jako	jako	k8xS,k8xC
Ptačí	ptačí	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jeseníky	Jeseník	k1gInPc4
soustavy	soustava	k1gFnSc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
předmět	předmět	k1gInSc1
ochrany	ochrana	k1gFnSc2
ptačí	ptačí	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
jeřábek	jeřábek	k1gInSc1
lesní	lesní	k2eAgMnSc1d1
a	a	k8xC
chřástal	chřástal	k1gMnSc1
polní	polní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nejvzácnějších	vzácný	k2eAgMnPc2d3
druhů	druh	k1gMnPc2
ptáků	pták	k1gMnPc2
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
je	být	k5eAaImIp3nS
sokol	sokol	k1gMnSc1
stěhovavý	stěhovavý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Tetřev	tetřev	k1gMnSc1
hlušec	hlušec	k1gMnSc1
a	a	k8xC
tetřívek	tetřívek	k1gMnSc1
obecný	obecný	k2eAgInSc4d1
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
prakticky	prakticky	k6eAd1
vyhubeni	vyhuben	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
méně	málo	k6eAd2
náročný	náročný	k2eAgInSc1d1
na	na	k7c4
biotop	biotop	k1gInSc4
je	být	k5eAaImIp3nS
jeřábek	jeřábek	k1gMnSc1
lesní	lesní	k2eAgFnSc2d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
nemalá	malý	k2eNgFnSc1d1
populace	populace	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
plně	plně	k6eAd1
životaschopná	životaschopný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
lesní	lesní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
jsou	být	k5eAaImIp3nP
vázány	vázán	k2eAgInPc1d1
druhy	druh	k1gInPc1
jako	jako	k8xS,k8xC
krkavec	krkavec	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
a	a	k8xC
dutinové	dutinový	k2eAgInPc4d1
druhy	druh	k1gInPc4
datel	datel	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
žluna	žluna	k1gFnSc1
šedá	šedá	k1gFnSc1
<g/>
,	,	kIx,
holub	holub	k1gMnSc1
doupňák	doupňák	k1gMnSc1
či	či	k8xC
noční	noční	k2eAgInPc1d1
druhy	druh	k1gInPc1
jako	jako	k8xC,k8xS
sýc	sýc	k1gMnSc1
rousný	rousný	k2eAgMnSc1d1
a	a	k8xC
vzácnější	vzácný	k2eAgMnSc1d2
kulíšek	kulíšek	k1gMnSc1
nejmenší	malý	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
v	v	k7c6
arktoalpínském	arktoalpínský	k2eAgNnSc6d1
bezlesí	bezlesí	k1gNnSc6
se	se	k3xPyFc4
setkáme	setkat	k5eAaPmIp1nP
se	s	k7c7
zástupci	zástupce	k1gMnPc7
ptačí	ptačí	k2eAgFnSc2d1
říše	říš	k1gFnSc2
např.	např.	kA
se	s	k7c7
silně	silně	k6eAd1
ohroženou	ohrožený	k2eAgFnSc7d1
linduškou	linduška	k1gFnSc7
horskou	horský	k2eAgFnSc7d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
linduška	linduška	k1gFnSc1
luční	luční	k2eAgFnSc1d1
<g/>
,	,	kIx,
bělořit	bělořit	k1gMnSc1
šedý	šedý	k2eAgMnSc1d1
<g/>
,	,	kIx,
bramborníček	bramborníček	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
<g/>
,	,	kIx,
čečetka	čečetka	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
kos	kos	k1gMnSc1
horský	horský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podhorské	Podhorské	k2eAgFnPc1d1
louky	louka	k1gFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
CHKO	CHKO	kA
útočištěm	útočiště	k1gNnSc7
chřástala	chřástal	k1gMnSc2
polního	polní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejsilnějších	silný	k2eAgFnPc2d3
populací	populace	k1gFnPc2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
se	se	k3xPyFc4
aktuálně	aktuálně	k6eAd1
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Rejvízu	Rejvíz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírně	mírně	k6eAd1
vzestupnou	vzestupný	k2eAgFnSc4d1
tendenci	tendence	k1gFnSc4
mají	mít	k5eAaImIp3nP
pozorování	pozorování	k1gNnSc4
kriticky	kriticky	k6eAd1
ohroženého	ohrožený	k2eAgMnSc2d1
strnada	strnad	k1gMnSc2
lučního	luční	k2eAgMnSc2d1
či	či	k8xC
ťuhýka	ťuhýk	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
největším	veliký	k2eAgFnPc3d3
ornitologickým	ornitologický	k2eAgFnPc3d1
zajímavostem	zajímavost	k1gFnPc3
posledních	poslední	k2eAgNnPc2d1
let	léto	k1gNnPc2
patří	patřit	k5eAaImIp3nS
pravidelné	pravidelný	k2eAgFnPc4d1
hnízdění	hnízdění	k1gNnPc4
jeřába	jeřáb	k1gMnSc2
popelavého	popelavý	k2eAgMnSc2d1
a	a	k8xC
historicky	historicky	k6eAd1
první	první	k4xOgNnSc4
prokázané	prokázaný	k2eAgNnSc4d1
hnízdění	hnízdění	k1gNnSc4
datlíka	datlík	k1gMnSc2
tříprstého	tříprstý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
proběhlo	proběhnout	k5eAaPmAgNnS
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc7
mapování	mapování	k1gNnSc2
hnízdního	hnízdní	k2eAgNnSc2d1
rozšíření	rozšíření	k1gNnSc2
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
108	#num#	k4
druhů	druh	k1gMnPc2
ptáků	pták	k1gMnPc2
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
prokázané	prokázaný	k2eAgFnSc2d1
a	a	k8xC
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
hnízdění	hnízdění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
počtu	počet	k1gInSc2
bylo	být	k5eAaImAgNnS
34	#num#	k4
druhů	druh	k1gInPc2
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgMnPc2d1
a	a	k8xC
15	#num#	k4
druhů	druh	k1gInPc2
chráněno	chráněn	k2eAgNnSc1d1
legislativou	legislativa	k1gFnSc7
EU	EU	kA
(	(	kIx(
<g/>
příloha	příloha	k1gFnSc1
I	i	k8xC
směrnice	směrnice	k1gFnPc1
o	o	k7c6
ptácích	pták	k1gMnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
savců	savec	k1gMnPc2
zaslouží	zasloužit	k5eAaPmIp3nS
pozornost	pozornost	k1gFnSc4
letouni	letoun	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
pravidelně	pravidelně	k6eAd1
sledováni	sledován	k2eAgMnPc1d1
na	na	k7c6
zimovištích	zimoviště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podzemí	podzemí	k1gNnSc6
lze	lze	k6eAd1
zastihnout	zastihnout	k5eAaPmF
pravidelně	pravidelně	k6eAd1
kolem	kolem	k7c2
11	#num#	k4
druhů	druh	k1gInPc2
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
druhů	druh	k1gInPc2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
např.	např.	kA
netopýr	netopýr	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
netopýr	netopýr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
netopýr	netopýr	k1gMnSc1
brvitý	brvitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
vrápenec	vrápenec	k1gMnSc1
malý	malý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
letních	letní	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
jediná	jediný	k2eAgFnSc1d1
(	(	kIx(
<g/>
EVL	EVL	kA
Kolštejn	Kolštejna	k1gFnPc2
Branná	branný	k2eAgFnSc1d1
<g/>
)	)	kIx)
s	s	k7c7
výskytem	výskyt	k1gInSc7
vrápence	vrápenec	k1gMnSc2
malého	malý	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
drobných	drobná	k1gFnPc2
horských	horský	k2eAgMnPc2d1
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
vzácných	vzácný	k2eAgMnPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
rozhodně	rozhodně	k6eAd1
za	za	k7c4
zmínku	zmínka	k1gFnSc4
pravidelný	pravidelný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
myšivky	myšivka	k1gFnSc2
horské	horský	k2eAgFnSc2d1
<g/>
,	,	kIx,
rejska	rejsek	k1gMnSc2
horského	horský	k2eAgMnSc2d1
a	a	k8xC
plcha	plch	k1gMnSc2
lesního	lesní	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opomenout	opomenout	k5eAaPmF
nelze	lze	k6eNd1
ani	ani	k8xC
velké	velký	k2eAgFnPc1d1
šelmy	šelma	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
populace	populace	k1gFnSc1
rysa	rys	k1gMnSc2
ostrovida	ostrovid	k1gMnSc2
v	v	k7c6
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc1
obnovila	obnovit	k5eAaPmAgFnS
přirozeně	přirozeně	k6eAd1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezákonný	zákonný	k2eNgInSc1d1
lov	lov	k1gInSc1
(	(	kIx(
<g/>
pytláctví	pytláctví	k1gNnSc1
<g/>
)	)	kIx)
jeho	jeho	k3xOp3gFnSc1
populace	populace	k1gFnSc1
decimuje	decimovat	k5eAaBmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
udržuje	udržovat	k5eAaImIp3nS
již	již	k6eAd1
řadu	řada	k1gFnSc4
let	léto	k1gNnPc2
jen	jen	k9
při	při	k7c6
hranici	hranice	k1gFnSc6
přežití	přežití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lépe	dobře	k6eAd2
je	být	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc1d1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnPc4
pobytové	pobytový	k2eAgFnPc4d1
známky	známka	k1gFnPc4
lze	lze	k6eAd1
zjistit	zjistit	k5eAaPmF
na	na	k7c6
všech	všecek	k3xTgInPc6
významných	významný	k2eAgInPc6d1
tocích	tok	k1gInPc6
<g/>
,	,	kIx,
odvádějících	odvádějící	k2eAgMnPc2d1
vody	voda	k1gFnSc2
z	z	k7c2
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
velkých	velký	k2eAgInPc2d1
druhů	druh	k1gInPc2
savců	savec	k1gMnPc2
je	být	k5eAaImIp3nS
nejpočetněji	početně	k6eAd3
zastoupena	zastoupen	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
jelení	jelení	k2eAgFnSc1d1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vyhovují	vyhovovat	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
jesenické	jesenický	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc1
vyšší	vysoký	k2eAgInPc1d2
stavy	stav	k1gInPc1
již	již	k6eAd1
dlouhodobě	dlouhodobě	k6eAd1
negativně	negativně	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
druhovou	druhový	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
i	i	k8xC
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
lesních	lesní	k2eAgInPc2d1
porostů	porost	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
velkému	velký	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
populace	populace	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
u	u	k7c2
prasete	prase	k1gNnSc2
divokého	divoký	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeseníky	Jeseník	k1gInPc7
jsou	být	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
výskytu	výskyt	k1gInSc2
kamzíka	kamzík	k1gMnSc2
horského	horský	k2eAgMnSc2d1
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Kamzík	kamzík	k1gMnSc1
horský	horský	k2eAgMnSc1d1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
</s>
<s>
V	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
byli	být	k5eAaImAgMnP
také	také	k9
uměle	uměle	k6eAd1
vysazeni	vysazen	k2eAgMnPc1d1
kamzíci	kamzík	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
dobře	dobře	k6eAd1
aklimatizovali	aklimatizovat	k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byli	být	k5eAaImAgMnP
do	do	k7c2
Jeseníků	Jeseník	k1gInPc2
dovezeni	dovézt	k5eAaPmNgMnP
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
z	z	k7c2
Alp	Alpy	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Pradědu	praděd	k1gMnSc6
napočítáno	napočítán	k2eAgNnSc1d1
65	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
jejich	jejich	k3xOp3gFnSc1
jesenická	jesenický	k2eAgFnSc1d1
populace	populace	k1gFnSc1
dosahovala	dosahovat	k5eAaImAgFnS
už	už	k6eAd1
600	#num#	k4
až	až	k9
900	#num#	k4
jedinců	jedinec	k1gMnPc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
podle	podle	k7c2
vedení	vedení	k1gNnSc2
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
150	#num#	k4
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
Sotirise	Sotirise	k1gFnSc2
Joanidise	Joanidise	k1gFnSc2
<g/>
,	,	kIx,
autora	autor	k1gMnSc2
knih	kniha	k1gFnPc2
o	o	k7c6
myslivosti	myslivost	k1gFnSc6
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
40	#num#	k4
až	až	k9
80	#num#	k4
kamzíků	kamzík	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hlavním	hlavní	k2eAgInSc7d1
problémem	problém	k1gInSc7
<g/>
,	,	kIx,
spojeným	spojený	k2eAgInSc7d1
s	s	k7c7
vysazením	vysazení	k1gNnSc7
kamzíků	kamzík	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ohrožení	ohrožení	k1gNnSc1
několika	několik	k4yIc2
druhů	druh	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
vzácných	vzácný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
ve	v	k7c6
dvou	dva	k4xCgInPc6
případech	případ	k1gInPc6
dokonce	dokonce	k9
místních	místní	k2eAgMnPc2d1
endemitů	endemit	k1gInPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
kamzíci	kamzík	k1gMnPc1
se	se	k3xPyFc4
pasou	pást	k5eAaImIp3nP
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
na	na	k7c6
skalnatých	skalnatý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
Velké	velký	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
jiní	jiný	k2eAgMnPc1d1
velcí	velký	k2eAgMnPc1d1
býložraví	býložravý	k2eAgMnPc1d1
savci	savec	k1gMnPc1
nedostanou	dostat	k5eNaPmIp3nP
a	a	k8xC
přímo	přímo	k6eAd1
si	se	k3xPyFc3
jako	jako	k8xC,k8xS
potravu	potrava	k1gFnSc4
vybírají	vybírat	k5eAaImIp3nP
některé	některý	k3yIgInPc1
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
(	(	kIx(
<g/>
jitrocel	jitrocel	k1gInSc1
tmavý	tmavý	k2eAgInSc1d1
sudetský	sudetský	k2eAgInSc1d1
a	a	k8xC
hvozdík	hvozdík	k1gInSc1
kartouzek	kartouzek	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
sudetský	sudetský	k2eAgInSc1d1
–	–	k?
endemické	endemický	k2eAgInPc4d1
poddruhy	poddruh	k1gInPc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
škarda	škarda	k1gFnSc1
sibiřská	sibiřský	k2eAgFnSc1d1
<g/>
,	,	kIx,
kopyšník	kopyšník	k1gInSc1
tmavý	tmavý	k2eAgInSc1d1
<g/>
,	,	kIx,
jestřábník	jestřábník	k1gInSc1
alpský	alpský	k2eAgInSc1d1
<g/>
,	,	kIx,
jestřábník	jestřábník	k1gInSc1
huňatý	huňatý	k2eAgInSc1d1
<g/>
,	,	kIx,
jestřábník	jestřábník	k1gInSc1
slezský	slezský	k2eAgInSc1d1
<g/>
,	,	kIx,
vrbovka	vrbovka	k1gFnSc1
drchničkolistá	drchničkolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
hvězdnice	hvězdnice	k1gFnSc1
alpská	alpský	k2eAgFnSc1d1
<g/>
,	,	kIx,
kapradina	kapradina	k1gFnSc1
hrálovitá	hrálovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
cídivka	cídivka	k1gFnSc1
zimní	zimní	k2eAgFnSc2d1
<g/>
,	,	kIx,
lipnice	lipnice	k1gFnSc2
alpská	alpský	k2eAgFnSc1d1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proto	proto	k8xC
si	se	k3xPyFc3
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
v	v	k7c6
plánu	plán	k1gInSc6
péče	péče	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
stanovila	stanovit	k5eAaPmAgFnS
za	za	k7c4
dlouhodobý	dlouhodobý	k2eAgInSc4d1
cíl	cíl	k1gInSc4
úplné	úplný	k2eAgNnSc1d1
vyloučení	vyloučení	k1gNnSc1
kamzičí	kamzičí	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
z	z	k7c2
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
zorganizována	zorganizován	k2eAgFnSc1d1
petice	petice	k1gFnSc1
„	„	k?
<g/>
Za	za	k7c4
záchranu	záchrana	k1gFnSc4
a	a	k8xC
zachování	zachování	k1gNnSc4
populace	populace	k1gFnSc2
Kamzíka	kamzík	k1gMnSc2
horského	horský	k2eAgMnSc2d1
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
pro	pro	k7c4
příští	příští	k2eAgFnPc4d1
generace	generace	k1gFnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
předána	předat	k5eAaPmNgFnS
s	s	k7c7
25	#num#	k4
132	#num#	k4
podpisy	podpis	k1gInPc7
v	v	k7c6
Parlamentu	parlament	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
ji	on	k3xPp3gFnSc4
převzali	převzít	k5eAaPmAgMnP
místopředseda	místopředseda	k1gMnSc1
PS	PS	kA
PČR	PČR	kA
Lubomír	Lubomír	k1gMnSc1
Zaorálek	Zaorálek	k1gMnSc1
<g/>
,	,	kIx,
senátoři	senátor	k1gMnPc1
Eva	Eva	k1gFnSc1
Richtrová	Richtrová	k1gFnSc1
a	a	k8xC
Jaromír	Jaromír	k1gMnSc1
Jermář	Jermář	k1gMnSc1
a	a	k8xC
náměstek	náměstek	k1gMnSc1
ministra	ministr	k1gMnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Tomáš	Tomáš	k1gMnSc1
Tesař	Tesař	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
následném	následný	k2eAgInSc6d1
plánu	plán	k1gInSc6
péče	péče	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2023	#num#	k4
<g/>
)	)	kIx)
nebyl	být	k5eNaImAgMnS
kamzík	kamzík	k1gMnSc1
přijímán	přijímat	k5eAaImNgMnS
škodlivě	škodlivě	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jeho	jeho	k3xOp3gInPc1
stavy	stav	k1gInPc1
nepřesáhnou	přesáhnout	k5eNaPmIp3nP
180	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sofistikovaných	sofistikovaný	k2eAgInPc2d1
odhadů	odhad	k1gInPc2
jich	on	k3xPp3gMnPc2
však	však	k9
může	moct	k5eAaImIp3nS
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
být	být	k5eAaImF
až	až	k6eAd1
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
CHKO	CHKO	kA
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
je	být	k5eAaImIp3nS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
(	(	kIx(
<g/>
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
)	)	kIx)
celkem	celkem	k6eAd1
31	#num#	k4
maloplošných	maloplošný	k2eAgFnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgFnPc2d1
území	území	k1gNnSc6
</s>
<s>
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
cca	cca	kA
4	#num#	k4
915	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
tj.	tj.	kA
6,6	6,6	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s>
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Praděd	praděd	k1gMnSc1
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
Skřítek	skřítek	k1gMnSc1
</s>
<s>
Rejvíz	Rejvíz	k1gMnSc1
</s>
<s>
Šerák-Keprník	Šerák-Keprník	k1gMnSc1
</s>
<s>
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Javorový	javorový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgMnSc1d1
rezervacePR	rezervacePR	k?
BřidličnáPR	BřidličnáPR	k1gMnSc1
Borek	Borek	k1gMnSc1
u	u	k7c2
Domašova	Domašův	k2eAgInSc2d1
</s>
<s>
Borek	borek	k1gInSc1
u	u	k7c2
Domašova	Domašův	k2eAgInSc2d1
</s>
<s>
Břidličná	břidličný	k2eAgFnSc1d1
</s>
<s>
Bučina	bučina	k1gFnSc1
pod	pod	k7c7
Františkovou	Františkův	k2eAgFnSc7d1
myslivnou	myslivna	k1gFnSc7
</s>
<s>
Filipovické	Filipovický	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Františkov	Františkov	k1gInSc1
</s>
<s>
Franz-Franz	Franz-Franz	k1gMnSc1
</s>
<s>
Jelení	jelení	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
</s>
<s>
Niva	niva	k1gFnSc1
Branné	branný	k2eAgFnSc2d1
</s>
<s>
Pod	pod	k7c7
Jelení	jelení	k2eAgFnSc7d1
studánkou	studánka	k1gFnSc7
</s>
<s>
Pod	pod	k7c7
Slunečnou	slunečný	k2eAgFnSc7d1
strání	stráň	k1gFnSc7
</s>
<s>
Pstruží	pstruží	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Rabštejn	Rabštejn	k1gMnSc1
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
</s>
<s>
Skalní	skalní	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Sněžná	sněžný	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Suchý	suchý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Šumárník	Šumárník	k1gMnSc1
</s>
<s>
U	u	k7c2
Slatinného	slatinný	k2eAgInSc2d1
potoka	potok	k1gInSc2
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1
vodopád	vodopád	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Chebzí	chebzí	k1gNnSc1
</s>
<s>
Louka	louka	k1gFnSc1
Na	na	k7c6
Miroslavi	Miroslaev	k1gFnSc6
</s>
<s>
Morgenland	Morgenland	k1gInSc1
</s>
<s>
Pasák	pasák	k1gMnSc1
</s>
<s>
Smrčina	smrčina	k1gFnSc1
</s>
<s>
Pfarrerb	Pfarrerb	k1gMnSc1
</s>
<s>
Zadní	zadní	k2eAgNnSc1d1
Hutisko	Hutisko	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
</s>
<s>
Prvních	první	k4xOgFnPc2
10	#num#	k4
nejvyšších	vysoký	k2eAgInPc2d3
hlavních	hlavní	k2eAgInPc2d1
vrcholů	vrchol	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Praděd	praděd	k1gMnSc1
(	(	kIx(
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
hole	hole	k1gFnSc1
(	(	kIx(
<g/>
1465	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Keprník	Keprník	k1gInSc1
(	(	kIx(
<g/>
1423	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velký	velký	k2eAgInSc1d1
Máj	máj	k1gInSc1
(	(	kIx(
<g/>
1386	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vozka	vozka	k1gMnSc1
(	(	kIx(
<g/>
1377	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Malý	Malý	k1gMnSc1
Děd	děd	k1gMnSc1
(	(	kIx(
<g/>
1368	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jelení	jelení	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
(	(	kIx(
<g/>
1367	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Břidličná	břidličný	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1358	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
(	(	kIx(
<g/>
1353	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šerák	Šerák	k1gMnSc1
(	(	kIx(
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
schválilo	schválit	k5eAaPmAgNnS
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
koncepčním	koncepční	k2eAgInSc6d1
materiálu	materiál	k1gInSc6
vyhlášení	vyhlášení	k1gNnSc2
NP	NP	kA
Jeseníky	Jeseník	k1gInPc1
(	(	kIx(
<g/>
jde	jít	k5eAaImIp3nS
o	o	k7c4
vrcholové	vrcholový	k2eAgFnPc4d1
partie	partie	k1gFnPc4
pohoří	pohořet	k5eAaPmIp3nS
rozdělené	rozdělený	k2eAgNnSc1d1
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
oblastí	oblast	k1gFnPc2
Červenohorského	červenohorský	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
rozšíření	rozšíření	k1gNnSc2
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc1
o	o	k7c6
území	území	k1gNnSc6
Rychlebských	Rychlebský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
(	(	kIx(
<g/>
SZ	SZ	kA
Jeseníku	Jeseník	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vyhlášením	vyhlášení	k1gNnSc7
se	se	k3xPyFc4
však	však	k9
nepočítá	počítat	k5eNaImIp3nS
v	v	k7c6
nejbližších	blízký	k2eAgNnPc6d3
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
předběžné	předběžný	k2eAgInPc4d1
návrhy	návrh	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
ještě	ještě	k6eAd1
značně	značně	k6eAd1
upravovat	upravovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
</s>
<s>
Udržitelný	udržitelný	k2eAgInSc1d1
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
je	být	k5eAaImIp3nS
důležitým	důležitý	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
rozvoje	rozvoj	k1gInSc2
regionu	region	k1gInSc2
Jeseníků	Jeseník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partnerem	partner	k1gMnSc7
je	být	k5eAaImIp3nS
pro	pro	k7c4
Správu	správa	k1gFnSc4
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
regionální	regionální	k2eAgFnSc2d1
destinační	destinační	k2eAgFnSc2d1
management	management	k1gInSc1
Jeseníky	Jeseník	k1gInPc1
–	–	k?
Sdružení	sdružení	k1gNnSc1
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
,	,	kIx,
Euroregion	euroregion	k1gInSc1
Praděd	praděd	k1gMnSc1
<g/>
,	,	kIx,
Euroregion	euroregion	k1gInSc1
Glacensis	Glacensis	k1gFnSc2
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Pardubický	pardubický	k2eAgInSc1d1
a	a	k8xC
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Lesy	les	k1gInPc1
ČR	ČR	kA
a	a	k8xC
další	další	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
environmentální	environmentální	k2eAgFnSc2d1
osvěty	osvěta	k1gFnSc2
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgFnSc2d1
a	a	k8xC
také	také	k9
sportovní	sportovní	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
z	z	k7c2
Jeseníků	Jeseník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc1
s	s	k7c7
uvedenými	uvedený	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
spolupracuje	spolupracovat	k5eAaImIp3nS
a	a	k8xC
vzájemně	vzájemně	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnSc1
činnost	činnost	k1gFnSc1
doplňuje	doplňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Geopark	Geopark	k1gInSc1
</s>
<s>
Místní	místní	k2eAgFnSc1d1
akční	akční	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Opavsko	Opavsko	k1gNnSc4
plánuje	plánovat	k5eAaImIp3nS
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
geopark	geopark	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc3
plánovaného	plánovaný	k2eAgInSc2d1
geoparku	geopark	k1gInSc2
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
od	od	k7c2
jihu	jih	k1gInSc2
od	od	k7c2
Budišova	Budišův	k2eAgNnSc2d1
nad	nad	k7c7
Budišovkou	Budišovka	k1gFnSc7
přes	přes	k7c4
masiv	masiv	k1gInSc4
Nízkého	nízký	k2eAgInSc2d1
a	a	k8xC
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
až	až	k9
po	po	k7c4
Rychlebské	Rychlebský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
na	na	k7c6
severu	sever	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
vybíhá	vybíhat	k5eAaImIp3nS
Staroměstsko	Staroměstsko	k1gNnSc1
a	a	k8xC
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
Krnovsko	Krnovsko	k1gNnSc4
a	a	k8xC
Osoblažsko	Osoblažsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
účely	účel	k1gInPc4
fungování	fungování	k1gNnSc2
geoparku	geopark	k1gInSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
Správou	správa	k1gFnSc7
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
Sdružení	sdružení	k1gNnSc1
pro	pro	k7c4
geopark	geopark	k1gInSc4
Jeseníky	Jeseník	k1gInPc4
o.s.	o.s.	k?
Zakládajícími	zakládající	k2eAgMnPc7d1
členy	člen	k1gMnPc7
jsou	být	k5eAaImIp3nP
MAS	masa	k1gFnPc2
–	–	k?
Orlicko	Orlicko	k1gNnSc1
<g/>
,	,	kIx,
MAS	maso	k1gNnPc2
Hrubý	hrubý	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
<g/>
,	,	kIx,
MAS	masa	k1gFnPc2
Nízký	nízký	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
<g/>
,	,	kIx,
MAS	masa	k1gFnPc2
Šumperský	šumperský	k2eAgInSc1d1
venkov	venkov	k1gInSc1
<g/>
,	,	kIx,
MAS	masa	k1gFnPc2
Opavsko	Opavsko	k1gNnSc1
<g/>
,	,	kIx,
ACTAEA	ACTAEA	kA
–	–	k?
společnost	společnost	k1gFnSc1
pro	pro	k7c4
přírodu	příroda	k1gFnSc4
a	a	k8xC
krajinu	krajina	k1gFnSc4
a	a	k8xC
Jeseníky	Jeseník	k1gInPc4
–	–	k?
Sdružení	sdružení	k1gNnSc1
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Plán	plán	k1gInSc1
péče	péče	k1gFnSc2
o	o	k7c6
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc7
na	na	k7c6
období	období	k1gNnSc6
2014	#num#	k4
<g/>
–	–	k?
<g/>
2023	#num#	k4
<g/>
,	,	kIx,
rozborová	rozborový	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
↑	↑	k?
PECHÁČEK	Pecháček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamzíci	kamzík	k1gMnPc1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
1913	#num#	k4
-	-	kIx~
2013	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Šumperku	Šumperk	k1gInSc6
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MOTÝL	motýl	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečné	Konečné	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
kamzičí	kamzičí	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Březen	březen	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
BUREŠ	Bureš	k1gMnSc1
<g/>
,	,	kIx,
Leo	Leo	k1gMnSc1
<g/>
;	;	kIx,
BUREŠOVÁ	Burešová	k1gFnSc1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
;	;	kIx,
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Vojta	Vojta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácné	vzácný	k2eAgFnPc4d1
a	a	k8xC
ohrožené	ohrožený	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
Jeseníků	Jeseník	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bruntál	Bruntál	k1gInSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochránců	ochránce	k1gMnPc2
přírody	příroda	k1gFnSc2
OV	OV	kA
Bruntál	Bruntál	k1gInSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
14	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stránky	stránka	k1gFnSc2
petice	petice	k1gFnSc2
Za	za	k7c4
záchranu	záchrana	k1gFnSc4
Kamzíka	kamzík	k1gMnSc2
horského	horský	k2eAgNnSc2d1
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
<g/>
.	.	kIx.
kamzici	kamzice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
Jeseníky	Jeseník	k1gInPc7
je	být	k5eAaImIp3nS
před	před	k7c7
zrozením	zrození	k1gNnSc7
<g/>
↑	↑	k?
V	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
vzniká	vznikat	k5eAaImIp3nS
geopark	geopark	k1gInSc1
<g/>
↑	↑	k?
Geopark	Geopark	k1gInSc1
Jeseníky	Jeseník	k1gInPc1
<g/>
.	.	kIx.
www.geopark-jeseniky.cz	www.geopark-jeseniky.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jeseníky	Jeseník	k1gInPc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Navštivte	navštívit	k5eAaPmRp2nP
Jeseníky	Jeseník	k1gInPc4
</s>
<s>
Jeseníky	Jeseník	k1gInPc1
Info	Info	k1gNnSc1
–	–	k?
turistický	turistický	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
V-Jeseníkách	V-Jeseník	k1gInPc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
turistický	turistický	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
Informace	informace	k1gFnPc1
pro	pro	k7c4
turisty	turist	k1gMnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Jeseníků	Jeseník	k1gInPc2
</s>
<s>
Fotografie	fotografia	k1gFnPc1
z	z	k7c2
Jeseníků	Jeseník	k1gInPc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
plánovaném	plánovaný	k2eAgInSc6d1
NP	NP	kA
Jeseníky	Jeseník	k1gInPc5
</s>
<s>
Stát	stát	k1gInSc1
chystá	chystat	k5eAaImIp3nS
nové	nový	k2eAgInPc4d1
národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
<g/>
:	:	kIx,
Křivoklátsko	Křivoklátsko	k1gNnSc1
a	a	k8xC
Jeseníky	Jeseník	k1gInPc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Jeseníky	Jeseník	k1gInPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Obcí	obec	k1gFnPc2
se	se	k3xPyFc4
prý	prý	k9
nedotkne	dotknout	k5eNaPmIp3nS
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Jeseníky	Jeseník	k1gInPc1
<g/>
:	:	kIx,
starostové	starosta	k1gMnPc1
nic	nic	k3yNnSc4
nevědí	vědět	k5eNaImIp3nP
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Bruntál	Bruntál	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Jeseníky	Jeseník	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
Pavlovický	pavlovický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Sovinecko	Sovinecko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Praděd	praděd	k1gMnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Skřítek	skřítek	k1gMnSc1
•	•	k?
Rešovské	Rešovský	k2eAgInPc4d1
vodopády	vodopád	k1gInPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Javorový	javorový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Ptačí	ptačit	k5eAaImIp3nS
hora	hora	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Roudný	Roudný	k2eAgInSc1d1
Přírodní	přírodní	k2eAgInSc1d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Břidličná	břidličný	k2eAgFnSc1d1
•	•	k?
Džungle	džungle	k1gFnSc1
•	•	k?
Franz-Franz	Franz-Franz	k1gInSc1
•	•	k?
Jelení	jelení	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Karlovice	Karlovice	k1gFnPc1
-	-	kIx~
sever	sever	k1gInSc1
•	•	k?
Krasovský	Krasovský	k2eAgInSc1d1
kotel	kotel	k1gInSc1
•	•	k?
Kunov	Kunov	k1gInSc1
•	•	k?
Mokřiny	mokřina	k1gFnSc2
u	u	k7c2
Krahulčí	krahulčí	k2eAgFnSc2d1
•	•	k?
Niva	niva	k1gFnSc1
Moravice	Moravice	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Jelení	jelení	k2eAgFnSc7d1
studánkou	studánka	k1gFnSc7
•	•	k?
Pstruží	pstruží	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Pustá	pustý	k2eAgFnSc1d1
Rudná	rudný	k2eAgFnSc1d1
•	•	k?
Radim	Radim	k1gMnSc1
•	•	k?
Růžová	růžový	k2eAgFnSc1d1
•	•	k?
Skalní	skalní	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Skalské	Skalská	k1gFnSc2
rašeliniště	rašeliniště	k1gNnSc2
•	•	k?
Suchý	suchý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
U	u	k7c2
Slatinného	slatinný	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Pavlovický	pavlovický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Lávový	lávový	k2eAgInSc1d1
proud	proud	k1gInSc1
u	u	k7c2
Meziny	Mezina	k1gFnSc2
•	•	k?
Liptaňský	Liptaňský	k2eAgInSc1d1
bludný	bludný	k2eAgInSc1d1
balvan	balvan	k1gInSc1
•	•	k?
Morgenland	Morgenland	k1gInSc1
•	•	k?
Oblík	oblík	k1gInSc1
u	u	k7c2
Dívčího	dívčí	k2eAgInSc2d1
hradu	hrad	k1gInSc2
•	•	k?
Osoblažský	osoblažský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
•	•	k?
Razovské	Razovský	k2eAgInPc4d1
tufity	tufit	k1gInPc4
•	•	k?
Staré	Stará	k1gFnSc2
hliniště	hliniště	k1gNnSc2
•	•	k?
Štola	štola	k1gFnSc1
pod	pod	k7c7
Jelení	jelení	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Uhlířský	uhlířský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
</s>
