<p>
<s>
Bambus	bambus	k1gInSc1	bambus
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
množství	množství	k1gNnSc4	množství
rodů	rod	k1gInPc2	rod
trav	tráva	k1gFnPc2	tráva
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
vyznačujících	vyznačující	k2eAgFnPc2d1	vyznačující
se	s	k7c7	s
dřevnatými	dřevnatý	k2eAgNnPc7d1	dřevnaté
stébly	stéblo	k1gNnPc7	stéblo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
bambusové	bambusový	k2eAgFnSc2d1	bambusová
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
druhy	druh	k1gInPc1	druh
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
až	až	k9	až
přes	přes	k7c4	přes
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Bambus	bambus	k1gInSc1	bambus
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
mnohostranný	mnohostranný	k2eAgInSc4d1	mnohostranný
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
podobné	podobný	k2eAgInPc4d1	podobný
účely	účel	k1gInPc4	účel
jako	jako	k8xC	jako
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
jako	jako	k9	jako
potravina	potravina	k1gFnSc1	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
převážně	převážně	k6eAd1	převážně
bílkoviny	bílkovina	k1gFnPc4	bílkovina
a	a	k8xC	a
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bambus	bambus	k1gInSc1	bambus
je	být	k5eAaImIp3nS	být
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
obdobu	obdoba	k1gFnSc4	obdoba
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Bambus	bambus	k1gInSc1	bambus
není	být	k5eNaImIp3nS	být
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tráva	tráva	k1gFnSc1	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podzemní	podzemní	k2eAgInPc4d1	podzemní
stonky	stonek	k1gInPc4	stonek
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
traviny	travina	k1gFnPc1	travina
<g/>
.	.	kIx.	.
</s>
<s>
Odolná	odolný	k2eAgNnPc1d1	odolné
a	a	k8xC	a
pevná	pevný	k2eAgNnPc1d1	pevné
vlákna	vlákno	k1gNnPc1	vlákno
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
měkčí	měkký	k2eAgInSc1d2	měkčí
a	a	k8xC	a
pružnější	pružný	k2eAgInSc1d2	pružnější
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
když	když	k8xS	když
tak	tak	k6eAd1	tak
zvenčí	zvenčí	k6eAd1	zvenčí
působícího	působící	k2eAgInSc2d1	působící
tlaku	tlak	k1gInSc2	tlak
umožní	umožnit	k5eAaPmIp3nS	umožnit
tuhým	tuhý	k2eAgNnPc3d1	tuhé
vláknům	vlákno	k1gNnPc3	vlákno
rozestoupit	rozestoupit	k5eAaPmF	rozestoupit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgInSc1d1	dutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
bambusů	bambus	k1gInPc2	bambus
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c4	mnoho
využití	využití	k1gNnPc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
Samostatný	samostatný	k2eAgInSc1d1	samostatný
velký	velký	k2eAgInSc1d1	velký
exemplář	exemplář	k1gInSc1	exemplář
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
výraznou	výrazný	k2eAgFnSc7d1	výrazná
solitérou	solitéra	k1gFnSc7	solitéra
v	v	k7c6	v
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
části	část	k1gFnSc6	část
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
využít	využít	k5eAaPmF	využít
i	i	k9	i
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zaclonění	zaclonění	k1gNnSc4	zaclonění
nevzhledných	vzhledný	k2eNgNnPc2d1	nevzhledné
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
velké	velký	k2eAgNnSc4d1	velké
uplatnění	uplatnění	k1gNnSc4	uplatnění
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
terase	terasa	k1gFnSc6	terasa
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
tyčovina	tyčovina	k1gFnSc1	tyčovina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využít	využít	k5eAaPmF	využít
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
aranžérských	aranžérský	k2eAgFnPc6d1	aranžérská
příležitostech	příležitost	k1gFnPc6	příležitost
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
i	i	k8xC	i
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
mladé	mladý	k2eAgInPc4d1	mladý
výhonky	výhonek	k1gInPc4	výhonek
zase	zase	k9	zase
pro	pro	k7c4	pro
kuchyňské	kuchyňský	k2eAgInPc4d1	kuchyňský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc4d1	základní
oblasti	oblast	k1gFnPc4	oblast
využití	využití	k1gNnSc2	využití
bambusů	bambus	k1gInPc2	bambus
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stavební	stavební	k2eAgInSc1d1	stavební
a	a	k8xC	a
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
materiál	materiál	k1gInSc1	materiál
</s>
</p>
<p>
<s>
surovina	surovina	k1gFnSc1	surovina
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
věcí	věc	k1gFnSc7	věc
denní	denní	k2eAgFnSc7d1	denní
potřeby	potřeba	k1gFnPc4	potřeba
</s>
</p>
<p>
<s>
potravina	potravina	k1gFnSc1	potravina
(	(	kIx(	(
<g/>
bambusové	bambusový	k2eAgInPc1d1	bambusový
výhonky	výhonek	k1gInPc1	výhonek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
buničiny	buničina	k1gFnSc2	buničina
</s>
</p>
<p>
<s>
stabilizace	stabilizace	k1gFnSc1	stabilizace
půdy	půda	k1gFnSc2	půda
před	před	k7c7	před
erozí	eroze	k1gFnSc7	eroze
</s>
</p>
<p>
<s>
léčiva	léčivo	k1gNnPc1	léčivo
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
bambusu	bambus	k1gInSc2	bambus
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
udělat	udělat	k5eAaPmF	udělat
skoro	skoro	k6eAd1	skoro
vše	všechen	k3xTgNnSc4	všechen
<g/>
;	;	kIx,	;
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
například	například	k6eAd1	například
zhotoveny	zhotoven	k2eAgInPc4d1	zhotoven
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
jízdní	jízdní	k2eAgNnPc4d1	jízdní
kola	kolo	k1gNnPc4	kolo
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bambus	bambus	k1gInSc1	bambus
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
rychlosti	rychlost	k1gFnSc3	rychlost
růstu	růst	k1gInSc2	růst
zapsán	zapsat	k5eAaPmNgMnS	zapsat
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
jako	jako	k8xS	jako
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
rostlina	rostlina	k1gFnSc1	rostlina
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyrůst	vyrůst	k5eAaPmF	vyrůst
až	až	k9	až
o	o	k7c4	o
91	[number]	k4	91
centimetrů	centimetr	k1gInPc2	centimetr
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
0,00003	[number]	k4	0,00003
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
z	z	k7c2	z
bambusu	bambus	k1gInSc2	bambus
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc4d1	mnohý
jiné	jiný	k2eAgInPc4d1	jiný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
bambusů	bambus	k1gInPc2	bambus
==	==	k?	==
</s>
</p>
<p>
<s>
BAMBUSEAH	BAMBUSEAH	kA	BAMBUSEAH
(	(	kIx(	(
<g/>
1447	[number]	k4	1447
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ARUNDINARIINAE	ARUNDINARIINAE	kA	ARUNDINARIINAE
(	(	kIx(	(
<g/>
249	[number]	k4	249
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Acidosasa	Acidosasa	k1gFnSc1	Acidosasa
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arundinaria	Arundinarium	k1gNnPc1	Arundinarium
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bashania	Bashanium	k1gNnPc1	Bashanium
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferrocalamus	Ferrocalamus	k1gInSc1	Ferrocalamus
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gaoligongshania	Gaoligongshanium	k1gNnPc1	Gaoligongshanium
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gelidocalamus	Gelidocalamus	k1gInSc1	Gelidocalamus
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indocalamus	Indocalamus	k1gInSc1	Indocalamus
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Menstruocalamus	Menstruocalamus	k1gInSc1	Menstruocalamus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Metasasa	Metasasa	k1gFnSc1	Metasasa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oligostachyum	Oligostachyum	k1gNnSc1	Oligostachyum
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pleioblastus	Pleioblastus	k1gInSc1	Pleioblastus
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polyanthus	Polyanthus	k1gInSc1	Polyanthus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pseudosasa	Pseudosasa	k1gFnSc1	Pseudosasa
(	(	kIx(	(
<g/>
36	[number]	k4	36
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sasa	Sas	k1gMnSc4	Sas
(	(	kIx(	(
<g/>
58	[number]	k4	58
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sasaella	Sasaella	k1gFnSc1	Sasaella
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vietnamocalamus	Vietnamocalamus	k1gInSc1	Vietnamocalamus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
THAMNOCALAMINAE	THAMNOCALAMINAE	kA	THAMNOCALAMINAE
(	(	kIx(	(
<g/>
226	[number]	k4	226
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ampelocalamus	Ampelocalamus	k1gInSc1	Ampelocalamus
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Borinda	Borinda	k1gFnSc1	Borinda
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chimonocalamus	Chimonocalamus	k1gInSc1	Chimonocalamus
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Drepanostachyum	Drepanostachyum	k1gNnSc1	Drepanostachyum
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fargesia	Fargesia	k1gFnSc1	Fargesia
(	(	kIx(	(
<g/>
83	[number]	k4	83
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Himalayacalamus	Himalayacalamus	k1gInSc1	Himalayacalamus
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thamnocalamus	Thamnocalamus	k1gInSc1	Thamnocalamus
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Yushania	Yushanium	k1gNnPc1	Yushanium
(	(	kIx(	(
<g/>
84	[number]	k4	84
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RACEMOBAMBOSINAE	RACEMOBAMBOSINAE	kA	RACEMOBAMBOSINAE
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neomicrocalamus	Neomicrocalamus	k1gInSc1	Neomicrocalamus
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Racemobambos	Racemobambos	k1gInSc1	Racemobambos
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vietnamosasa	Vietnamosasa	k1gFnSc1	Vietnamosasa
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHIBATAEINAE	SHIBATAEINAE	kA	SHIBATAEINAE
(	(	kIx(	(
<g/>
182	[number]	k4	182
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brachystachyum	Brachystachyum	k1gNnSc1	Brachystachyum
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chimonobambusa	Chimonobambus	k1gMnSc4	Chimonobambus
(	(	kIx(	(
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hibanobambusa	Hibanobambus	k1gMnSc4	Hibanobambus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indosasa	Indosasa	k1gFnSc1	Indosasa
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Phyllostachys	Phyllostachys	k1gInSc1	Phyllostachys
(	(	kIx(	(
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Semiarundinana	Semiarundinana	k1gFnSc1	Semiarundinana
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shibataea	Shibataea	k1gFnSc1	Shibataea
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sinobambusa	Sinobambus	k1gMnSc4	Sinobambus
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BAMBUSINAE	BAMBUSINAE	kA	BAMBUSINAE
(	(	kIx(	(
<g/>
297	[number]	k4	297
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bambusa	Bambus	k1gMnSc4	Bambus
(	(	kIx(	(
<g/>
139	[number]	k4	139
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bonia	Bonia	k1gFnSc1	Bonia
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dendrocalamus	Dendrocalamus	k1gInSc1	Dendrocalamus
(	(	kIx(	(
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dinochloa	Dinochloa	k1gFnSc1	Dinochloa
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gigantochloa	Gigantochloa	k1gFnSc1	Gigantochloa
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Holttumochloa	Holttumochloa	k1gFnSc1	Holttumochloa
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kinabaluchloa	Kinabaluchloa	k1gFnSc1	Kinabaluchloa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klemachloa	Klemachloa	k1gFnSc1	Klemachloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maclurochloa	Maclurochloa	k1gFnSc1	Maclurochloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Melocalamus	Melocalamus	k1gInSc1	Melocalamus
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oreobambos	Oreobambos	k1gInSc1	Oreobambos
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oxytenanthera	Oxytenanthera	k1gFnSc1	Oxytenanthera
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pseudobambusa	Pseudobambus	k1gMnSc4	Pseudobambus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pseudoxytenanthera	Pseudoxytenanthera	k1gFnSc1	Pseudoxytenanthera
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soejatmia	Soejatmia	k1gFnSc1	Soejatmia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sphaerobambos	Sphaerobambos	k1gInSc1	Sphaerobambos
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thyrsostachys	Thyrsostachys	k1gInSc1	Thyrsostachys
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MELOCANNINAE	MELOCANNINAE	kA	MELOCANNINAE
(	(	kIx(	(
<g/>
87	[number]	k4	87
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cephalostachyum	Cephalostachyum	k1gNnSc1	Cephalostachyum
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Davidsea	Davidsea	k1gFnSc1	Davidsea
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dendrochloa	Dendrochloa	k1gFnSc1	Dendrochloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Melocanna	Melocanna	k1gFnSc1	Melocanna
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neohouzeaua	Neohouzeaua	k1gFnSc1	Neohouzeaua
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ochlandra	Ochlandra	k1gFnSc1	Ochlandra
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pseudostachyum	Pseudostachyum	k1gNnSc1	Pseudostachyum
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Schizostachyum	Schizostachyum	k1gNnSc1	Schizostachyum
(	(	kIx(	(
<g/>
45	[number]	k4	45
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Teinostachyum	Teinostachyum	k1gNnSc1	Teinostachyum
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HICKELIINAE	HICKELIINAE	kA	HICKELIINAE
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Decaryochloa	Decaryochloa	k1gFnSc1	Decaryochloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Greslania	Greslanium	k1gNnPc1	Greslanium
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hickelia	Hickelia	k1gFnSc1	Hickelia
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hitchcockella	Hitchcockella	k1gFnSc1	Hitchcockella
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nastus	Nastus	k1gInSc1	Nastus
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Perrierbambus	Perrierbambus	k1gInSc1	Perrierbambus
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Temburongia	Temburongia	k1gFnSc1	Temburongia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GUADUINAE	GUADUINAE	kA	GUADUINAE
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Criciuma	Criciuma	k1gFnSc1	Criciuma
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eremocaulon	Eremocaulon	k1gInSc1	Eremocaulon
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Guadua	Guadua	k1gFnSc1	Guadua
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Olmeca	Olmeca	k1gFnSc1	Olmeca
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otatea	Otatea	k1gFnSc1	Otatea
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHUSQUEINAE	CHUSQUEINAE	kA	CHUSQUEINAE
(	(	kIx(	(
<g/>
156	[number]	k4	156
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chusquea	Chusquea	k1gFnSc1	Chusquea
(	(	kIx(	(
<g/>
135	[number]	k4	135
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neurolepis	Neurolepis	k1gFnSc1	Neurolepis
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ARTHROSTYLIDIINAE	ARTHROSTYLIDIINAE	kA	ARTHROSTYLIDIINAE
(	(	kIx(	(
<g/>
146	[number]	k4	146
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Actinocladum	Actinocladum	k1gNnSc1	Actinocladum
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alvimia	Alvimia	k1gFnSc1	Alvimia
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Apoclada	Apoclada	k1gFnSc1	Apoclada
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arthrostylidium	Arthrostylidium	k1gNnSc1	Arthrostylidium
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Athroostachys	Athroostachys	k1gInSc1	Athroostachys
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Atractantha	Atractantha	k1gFnSc1	Atractantha
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aulonemia	Aulonemia	k1gFnSc1	Aulonemia
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Colanthelia	Colanthelia	k1gFnSc1	Colanthelia
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elytrostachys	Elytrostachys	k1gInSc1	Elytrostachys
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glaziophyton	Glaziophyton	k1gInSc1	Glaziophyton
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Merostachys	Merostachys	k1gInSc1	Merostachys
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myriocladus	Myriocladus	k1gInSc1	Myriocladus
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rhipidocladum	Rhipidocladum	k1gNnSc1	Rhipidocladum
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OLYREAE	OLYREAE	kA	OLYREAE
(	(	kIx(	(
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Agnesia	Agnesia	k1gFnSc1	Agnesia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arberella	Arberella	k1gFnSc1	Arberella
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cryptochloa	Cryptochloa	k1gFnSc1	Cryptochloa
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diandrolyra	Diandrolyra	k1gFnSc1	Diandrolyra
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ekmanochloa	Ekmanochloa	k1gFnSc1	Ekmanochloa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Froesiochloa	Froesiochloa	k1gFnSc1	Froesiochloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lithachne	Lithachnout	k5eAaPmIp3nS	Lithachnout
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maclurolyra	Maclurolyra	k1gFnSc1	Maclurolyra
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mniochloa	Mniochloa	k1gFnSc1	Mniochloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Olyra	Olyra	k1gFnSc1	Olyra
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Parodiolyra	Parodiolyra	k1gFnSc1	Parodiolyra
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Piresia	Piresia	k1gFnSc1	Piresia
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Piresiella	Piresiella	k1gFnSc1	Piresiella
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Raddia	Raddium	k1gNnPc1	Raddium
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Raddiella	Raddiella	k1gFnSc1	Raddiella
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rehia	Rehia	k1gFnSc1	Rehia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Reitzia	Reitzia	k1gFnSc1	Reitzia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sucrea	Sucrea	k1gFnSc1	Sucrea
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PARIANEAE	PARIANEAE	kA	PARIANEAE
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eremitis	Eremitis	k1gFnSc1	Eremitis
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pariana	Pariana	k1gFnSc1	Pariana
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BUERGERSIOCHLOEAE	BUERGERSIOCHLOEAE	kA	BUERGERSIOCHLOEAE
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Buergersiochloa	Buergersiochloa	k1gFnSc1	Buergersiochloa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PUELIEAE	PUELIEAE	kA	PUELIEAE
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Puelia	Puelia	k1gFnSc1	Puelia
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GUADUELLEAE	GUADUELLEAE	kA	GUADUELLEAE
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Guaduella	Guaduella	k1gFnSc1	Guaduella
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
kuchyně	kuchyně	k1gFnSc1	kuchyně
</s>
</p>
<p>
<s>
Asijská	asijský	k2eAgFnSc1d1	asijská
kuchyně	kuchyně	k1gFnSc1	kuchyně
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bambus	bambus	k1gInSc1	bambus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bambus	bambus	k1gInSc1	bambus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
bambusová	bambusový	k2eAgFnSc1d1	bambusová
asociace	asociace	k1gFnSc1	asociace
</s>
</p>
