<s>
Menhir	menhir	k1gInSc1	menhir
(	(	kIx(	(
<g/>
z	z	k7c2	z
bretonštiny	bretonština	k1gFnSc2	bretonština
men	men	k?	men
-	-	kIx~	-
kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
hir	hir	k?	hir
-	-	kIx~	-
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kamenný	kamenný	k2eAgInSc1d1	kamenný
blok	blok	k1gInSc1	blok
svisle	svisle	k6eAd1	svisle
zasazený	zasazený	k2eAgMnSc1d1	zasazený
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
beze	beze	k7c2	beze
stop	stop	k2eAgNnSc2d1	stop
opracování	opracování	k1gNnSc2	opracování
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
je	být	k5eAaImIp3nS	být
naznačená	naznačený	k2eAgFnSc1d1	naznačená
lidská	lidský	k2eAgFnSc1d1	lidská
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oděv	oděv	k1gInSc1	oděv
a	a	k8xC	a
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Menhirové	menhirový	k2eAgFnPc1d1	menhirová
sochy	socha	k1gFnPc1	socha
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
ze	z	k7c2	z
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Korsiky	Korsika	k1gFnSc2	Korsika
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
falický	falický	k2eAgInSc4d1	falický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
místy	místy	k6eAd1	místy
s	s	k7c7	s
ženskými	ženský	k2eAgInPc7d1	ženský
znaky	znak	k1gInPc7	znak
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
tvoří	tvořit	k5eAaImIp3nP	tvořit
řady	řad	k1gInPc1	řad
alignement	alignement	k1gInSc1	alignement
nebo	nebo	k8xC	nebo
kruhy	kruh	k1gInPc1	kruh
(	(	kIx(	(
<g/>
kromlech	kromlech	k1gInSc1	kromlech
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
od	od	k7c2	od
pozdního	pozdní	k2eAgInSc2d1	pozdní
neolitu	neolit	k1gInSc2	neolit
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
menhir	menhir	k1gInSc1	menhir
je	být	k5eAaImIp3nS	být
novodobá	novodobý	k2eAgFnSc1d1	novodobá
umělá	umělý	k2eAgFnSc1d1	umělá
složenina	složenina	k1gFnSc1	složenina
<g/>
,	,	kIx,	,
používající	používající	k2eAgNnPc4d1	používající
bretonská	bretonský	k2eAgNnPc4d1	bretonský
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
bretonštině	bretonština	k1gFnSc6	bretonština
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
menhir	menhir	k1gInSc4	menhir
používá	používat	k5eAaImIp3nS	používat
slovo	slovo	k1gNnSc4	slovo
peulvan	peulvan	k1gMnSc1	peulvan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
české	český	k2eAgNnSc4d1	české
slovo	slovo	k1gNnSc4	slovo
balvan	balvan	k1gInSc1	balvan
a	a	k8xC	a
bretonské	bretonský	k2eAgNnSc1d1	bretonský
peulvan	peulvan	k1gMnSc1	peulvan
mají	mít	k5eAaImIp3nP	mít
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
českým	český	k2eAgInSc7d1	český
kamenem	kámen	k1gInSc7	kámen
<g/>
,	,	kIx,	,
považovaným	považovaný	k2eAgMnSc7d1	považovaný
za	za	k7c4	za
menhir	menhir	k1gInSc4	menhir
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Kamenný	kamenný	k2eAgMnSc1d1	kamenný
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgMnSc1d1	stojící
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Telce	telce	k1gNnSc1	telce
a	a	k8xC	a
Klobuky	Klobuk	k1gInPc1	Klobuk
na	na	k7c6	na
Slánsku	Slánsko	k1gNnSc6	Slánsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
330	[number]	k4	330
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Býval	bývat	k5eAaImAgMnS	bývat
prý	prý	k9	prý
kdysi	kdysi	k6eAd1	kdysi
obklopen	obklopit	k5eAaPmNgInS	obklopit
kruhem	kruh	k1gInSc7	kruh
devíti	devět	k4xCc2	devět
kamenů	kámen	k1gInPc2	kámen
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
snad	snad	k9	snad
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
předpokládané	předpokládaný	k2eAgInPc4d1	předpokládaný
menhiry	menhir	k1gInPc4	menhir
v	v	k7c6	v
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
kámen	kámen	k1gInSc1	kámen
z	z	k7c2	z
Jemníků	Jemník	k1gMnPc2	Jemník
na	na	k7c6	na
Slánsku	Slánsko	k1gNnSc6	Slánsko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
znovu	znovu	k6eAd1	znovu
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
1	[number]	k4	1
m	m	kA	m
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
slepencem	slepenec	k1gInSc7	slepenec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
povalené	povalený	k2eAgInPc1d1	povalený
menhiry	menhir	k1gInPc1	menhir
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Slánské	Slánské	k2eAgFnSc2d1	Slánské
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
a	a	k8xC	a
150	[number]	k4	150
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
menhirem	menhir	k1gInSc7	menhir
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Slánsku	Slánsko	k1gNnSc6	Slánsko
<g/>
,	,	kIx,	,
je	on	k3xPp3gNnSc4	on
ten	ten	k3xDgMnSc1	ten
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1	[number]	k4	1
metr	metr	k1gInSc1	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
latinský	latinský	k2eAgInSc4d1	latinský
kříž	kříž	k1gInSc4	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
menhiry	menhir	k1gInPc1	menhir
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
Žatecku	Žatecko	k1gNnSc6	Žatecko
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
severně	severně	k6eAd1	severně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Drahomyšl	Drahomyšl	k1gInSc1	Drahomyšl
(	(	kIx(	(
<g/>
Zakletý	zakletý	k2eAgInSc1d1	zakletý
mnich	mnich	k1gInSc1	mnich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
žateckého	žatecký	k2eAgNnSc2d1	žatecké
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Březno	Březno	k1gNnSc1	Březno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
údajný	údajný	k2eAgInSc1d1	údajný
menhir	menhir	k1gInSc1	menhir
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Kluček	Klučka	k1gFnPc2	Klučka
a	a	k8xC	a
jiný	jiný	k2eAgInSc1d1	jiný
žulový	žulový	k2eAgInSc1d1	žulový
menhir	menhir	k1gInSc1	menhir
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Líčkov	Líčkov	k1gInSc1	Líčkov
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vytesán	vytesat	k5eAaPmNgInS	vytesat
letopočet	letopočet	k1gInSc1	letopočet
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lounsku	Lounsko	k1gNnSc6	Lounsko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tyto	tento	k3xDgFnPc1	tento
památky	památka	k1gFnPc1	památka
<g/>
:	:	kIx,	:
menhir	menhir	k1gInSc1	menhir
v	v	k7c6	v
lounském	lounský	k2eAgNnSc6d1	Lounské
muzeu	muzeum	k1gNnSc6	muzeum
(	(	kIx(	(
<g/>
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Selibice	Selibice	k1gFnSc2	Selibice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baba	baba	k1gFnSc1	baba
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Slavětín	Slavětín	k1gInSc1	Slavětín
a	a	k8xC	a
menhir	menhir	k1gInSc1	menhir
v	v	k7c6	v
Orasicích	Orasice	k1gFnPc6	Orasice
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
i	i	k9	i
dolnochaberský	dolnochaberský	k2eAgMnSc1d1	dolnochaberský
Zkamenělý	zkamenělý	k2eAgMnSc1d1	zkamenělý
slouha	slouha	k1gMnSc1	slouha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
160	[number]	k4	160
cm	cm	kA	cm
vysoký	vysoký	k2eAgInSc1d1	vysoký
menhir	menhir	k1gInSc1	menhir
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
u	u	k7c2	u
plotu	plot	k1gInSc2	plot
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Ládevské	Ládevský	k2eAgFnSc6d1	Ládevský
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Dolních	dolní	k2eAgInPc6d1	dolní
Chabrech	Chabr	k1gInPc6	Chabr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
menhiry	menhir	k1gInPc4	menhir
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
nedochovaný	dochovaný	k2eNgInSc1d1	nedochovaný
menhir	menhir	k1gInSc1	menhir
stával	stávat	k5eAaImAgInS	stávat
v	v	k7c6	v
Ratiboři	Ratiboř	k1gFnSc6	Ratiboř
u	u	k7c2	u
Chyšek	chyška	k1gFnPc2	chyška
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
rodinného	rodinný	k2eAgInSc2d1	rodinný
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
odstřelen	odstřelit	k5eAaPmNgMnS	odstřelit
na	na	k7c4	na
popud	popud	k1gInSc4	popud
místního	místní	k2eAgNnSc2d1	místní
JZD	JZD	kA	JZD
<g/>
.	.	kIx.	.
</s>
