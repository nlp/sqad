<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Maláčová	Maláčová	k1gFnSc1	Maláčová
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1981	[number]	k4	1981
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
úřednice	úřednice	k1gFnSc1	úřednice
a	a	k8xC	a
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
ministryně	ministryně	k1gFnSc1	ministryně
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
ČR	ČR	kA	ČR
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
Babišově	Babišův	k2eAgFnSc6d1	Babišova
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
od	od	k7c2	od
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
místopředsedkyně	místopředsedkyně	k1gFnSc2	místopředsedkyně
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k9	až
2007	[number]	k4	2007
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
magisterský	magisterský	k2eAgInSc4d1	magisterský
studijní	studijní	k2eAgInSc4d1	studijní
obor	obor	k1gInSc4	obor
politologie	politologie	k1gFnSc2	politologie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Johanna	Johann	k1gMnSc4	Johann
Wolfganga	Wolfgang	k1gMnSc4	Wolfgang
Goetheho	Goethe	k1gMnSc4	Goethe
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
Dipl	Dipla	k1gFnPc2	Dipla
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
Pol.	Pol.	k1gFnSc1	Pol.
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
2016	[number]	k4	2016
magisterský	magisterský	k2eAgInSc4d1	magisterský
studijní	studijní	k2eAgInSc4d1	studijní
obor	obor	k1gInSc4	obor
politická	politický	k2eAgFnSc1d1	politická
ekonomie	ekonomie	k1gFnSc1	ekonomie
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c6	na
London	London	k1gMnSc1	London
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Economics	Economicsa	k1gFnPc2	Economicsa
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
MSc	MSc	k1gFnSc2	MSc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
tak	tak	k9	tak
plynně	plynně	k6eAd1	plynně
němčinu	němčina	k1gFnSc4	němčina
i	i	k8xC	i
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
až	až	k9	až
2011	[number]	k4	2011
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
ČR	ČR	kA	ČR
jako	jako	k8xS	jako
analytička	analytička	k1gFnSc1	analytička
fondů	fond	k1gInPc2	fond
EU	EU	kA	EU
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2012	[number]	k4	2012
a	a	k8xC	a
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
zástupkyní	zástupkyně	k1gFnSc7	zástupkyně
Kanceláře	kancelář	k1gFnSc2	kancelář
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
při	při	k7c6	při
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
vedla	vést	k5eAaImAgFnS	vést
oddělení	oddělení	k1gNnSc3	oddělení
institucionální	institucionální	k2eAgFnSc2d1	institucionální
komunikace	komunikace	k1gFnSc2	komunikace
EU	EU	kA	EU
Sekce	sekce	k1gFnSc1	sekce
pro	pro	k7c4	pro
záležitosti	záležitost	k1gFnPc4	záležitost
EU	EU	kA	EU
na	na	k7c4	na
Úřadu	úřada	k1gMnSc4	úřada
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
ředitelkou	ředitelka	k1gFnSc7	ředitelka
odboru	odbor	k1gInSc2	odbor
rodinné	rodinný	k2eAgFnSc2d1	rodinná
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
stárnutí	stárnutí	k1gNnSc2	stárnutí
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
<g/>
Jana	Jan	k1gMnSc2	Jan
Maláčová	Maláčová	k1gFnSc1	Maláčová
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
je	být	k5eAaImIp3nS	být
Aleš	Aleš	k1gMnSc1	Aleš
Chmelař	Chmelař	k1gMnSc1	Chmelař
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
pro	pro	k7c4	pro
evropské	evropský	k2eAgFnPc4d1	Evropská
záležitosti	záležitost	k1gFnPc4	záležitost
při	při	k7c6	při
Úřadu	úřad	k1gInSc6	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
Gustava	Gustav	k1gMnSc4	Gustav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
členkou	členka	k1gFnSc7	členka
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
spolku	spolek	k1gInSc2	spolek
Oranžový	oranžový	k2eAgInSc1d1	oranžový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
spravedlivé	spravedlivý	k2eAgNnSc4d1	spravedlivé
zastoupení	zastoupení	k1gNnSc4	zastoupení
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Česko-německého	českoěmecký	k2eAgInSc2d1	česko-německý
fondu	fond	k1gInSc2	fond
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ministr	ministr	k1gMnSc1	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
ČR	ČR	kA	ČR
Petr	Petr	k1gMnSc1	Petr
Krčál	Krčál	k1gMnSc1	Krčál
kvůli	kvůli	k7c3	kvůli
plagiátorské	plagiátorský	k2eAgFnSc3d1	plagiátorská
aféře	aféra	k1gFnSc6	aféra
svou	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
,	,	kIx,	,
informoval	informovat	k5eAaBmAgMnS	informovat
předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
<g/>
,	,	kIx,	,
že	že	k8xS	že
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Maláčovou	Maláčův	k2eAgFnSc7d1	Maláčova
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc3	jeho
nástupkyni	nástupkyně	k1gFnSc3	nástupkyně
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
</s>
<s>
Maláčová	Maláčová	k1gFnSc1	Maláčová
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc7	její
prioritou	priorita	k1gFnSc7	priorita
bude	být	k5eAaImBp3nS	být
rodinná	rodinný	k2eAgFnSc1d1	rodinná
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
téma	téma	k1gNnSc1	téma
stárnutí	stárnutí	k1gNnSc2	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k8xC	i
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
Vládě	Vláďa	k1gFnSc3	Vláďa
hodlá	hodlat	k5eAaImIp3nS	hodlat
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
předložit	předložit	k5eAaPmF	předložit
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
rodičovského	rodičovský	k2eAgInSc2d1	rodičovský
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
absolutní	absolutní	k2eAgFnSc6d1	absolutní
hodnotě	hodnota	k1gFnSc6	hodnota
zvýšit	zvýšit	k5eAaPmF	zvýšit
o	o	k7c4	o
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
260	[number]	k4	260
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
41	[number]	k4	41
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
zvolena	zvolit	k5eAaPmNgFnS	zvolit
řadovou	řadový	k2eAgFnSc7d1	řadová
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
390	[number]	k4	390
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
472	[number]	k4	472
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Oponenty	oponent	k1gMnPc7	oponent
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
levicový	levicový	k2eAgInSc4d1	levicový
program	program	k1gInSc4	program
přezdívána	přezdíván	k2eAgFnSc1d1	přezdívána
"	"	kIx"	"
<g/>
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
