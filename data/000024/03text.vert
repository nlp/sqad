<s>
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Le	Le	k1gFnSc1	Le
petit	petit	k1gInSc4	petit
prince	princ	k1gMnSc2	princ
<g/>
)	)	kIx)	)
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgNnSc4d3	nejznámější
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
pilota	pilot	k1gMnSc2	pilot
Antoina	Antoin	k1gMnSc2	Antoin
de	de	k?	de
Saint-Exupéryho	Saint-Exupéry	k1gMnSc2	Saint-Exupéry
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
současně	současně	k6eAd1	současně
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
příběhů	příběh	k1gInPc2	příběh
moderní	moderní	k2eAgFnSc2d1	moderní
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
metafor	metafora	k1gFnPc2	metafora
a	a	k8xC	a
přirovnání	přirovnání	k1gNnPc2	přirovnání
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zde	zde	k6eAd1	zde
najít	najít	k5eAaPmF	najít
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
autor	autor	k1gMnSc1	autor
konkrétněji	konkrétně	k6eAd2	konkrétně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
pilota	pilot	k1gMnSc2	pilot
(	(	kIx(	(
<g/>
samotného	samotný	k2eAgMnSc2d1	samotný
Exupéryho	Exupéry	k1gMnSc2	Exupéry
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ztroskotá	ztroskotat	k5eAaPmIp3nS	ztroskotat
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Malým	Malý	k1gMnSc7	Malý
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
sem	sem	k6eAd1	sem
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
daleké	daleký	k2eAgFnSc2d1	daleká
planetky	planetka	k1gFnSc2	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
autorovy	autorův	k2eAgFnPc4d1	autorova
kresby	kresba	k1gFnPc4	kresba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
reprodukovány	reprodukovat	k5eAaBmNgFnP	reprodukovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
180	[number]	k4	180
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
50	[number]	k4	50
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
knih	kniha	k1gFnPc2	kniha
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmových	filmový	k2eAgFnPc6d1	filmová
adaptacích	adaptace	k1gFnPc6	adaptace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
filmového	filmový	k2eAgInSc2d1	filmový
muzikálu	muzikál	k1gInSc2	muzikál
Lernera	Lerner	k1gMnSc4	Lerner
Loeweho	Loewe	k1gMnSc4	Loewe
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc6	dva
oper	opera	k1gFnPc2	opera
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
knihu	kniha	k1gFnSc4	kniha
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
francouzského	francouzský	k2eAgInSc2d1	francouzský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
v	v	k7c6	v
osmi	osm	k4xCc6	osm
kapitolách	kapitola	k1gFnPc6	kapitola
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
čtenáře	čtenář	k1gMnPc4	čtenář
s	s	k7c7	s
příběhem	příběh	k1gInSc7	příběh
Malého	Malý	k1gMnSc2	Malý
prince	princ	k1gMnSc2	princ
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
(	(	kIx(	(
<g/>
samotný	samotný	k2eAgMnSc1d1	samotný
Antoine	Antoin	k1gInSc5	Antoin
de	de	k?	de
Saint-Exupéry	Saint-Exupér	k1gInPc4	Saint-Exupér
<g/>
)	)	kIx)	)
uvízne	uvíznout	k5eAaPmIp3nS	uvíznout
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
opravit	opravit	k5eAaPmF	opravit
svůj	svůj	k3xOyFgInSc4	svůj
letoun	letoun	k1gInSc4	letoun
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
objeví	objevit	k5eAaPmIp3nS	objevit
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
<g/>
.	.	kIx.	.
</s>
<s>
Prosí	prosit	k5eAaImIp3nP	prosit
vypravěče	vypravěč	k1gMnSc4	vypravěč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
beránka	beránek	k1gMnSc4	beránek
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
nakreslit	nakreslit	k5eAaPmF	nakreslit
<g/>
,	,	kIx,	,
nakreslí	nakreslit	k5eAaPmIp3nP	nakreslit
hroznýše	hroznýš	k1gMnSc4	hroznýš
se	s	k7c7	s
slonem	slon	k1gMnSc7	slon
uvnitř	uvnitř	k7c2	uvnitř
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
kresbu	kresba	k1gFnSc4	kresba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
považovali	považovat	k5eAaImAgMnP	považovat
dospělí	dospělí	k1gMnPc1	dospělí
za	za	k7c4	za
klobouk	klobouk	k1gInSc4	klobouk
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Né	Né	k?	Né
Né	Né	k?	Né
<g/>
"	"	kIx"	"
říká	říkat	k5eAaImIp3nS	říkat
princ	princ	k1gMnSc1	princ
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
nechci	chtít	k5eNaImIp1nS	chtít
hroznýše	hroznýš	k1gMnSc4	hroznýš
se	s	k7c7	s
slonem	slon	k1gMnSc7	slon
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
!	!	kIx.	!
</s>
<s>
Já	já	k3xPp1nSc1	já
chci	chtít	k5eAaImIp1nS	chtít
beránka	beránek	k1gMnSc4	beránek
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Vypravěč	vypravěč	k1gMnSc1	vypravěč
nakreslí	nakreslit	k5eAaPmIp3nS	nakreslit
beránka	beránek	k1gMnSc4	beránek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kresby	kresba	k1gFnPc1	kresba
se	se	k3xPyFc4	se
Malému	Malý	k1gMnSc3	Malý
princi	princ	k1gMnSc3	princ
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vypravěč	vypravěč	k1gMnSc1	vypravěč
nakreslí	nakreslit	k5eAaPmIp3nS	nakreslit
bednu	bedna	k1gFnSc4	bedna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
beránek	beránek	k1gInSc1	beránek
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vidí	vidět	k5eAaImIp3nS	vidět
beránka	beránek	k1gMnSc4	beránek
v	v	k7c6	v
bedně	bedna	k1gFnSc6	bedna
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vidí	vidět	k5eAaImIp3nP	vidět
slona	slon	k1gMnSc4	slon
v	v	k7c6	v
hroznýši	hroznýš	k1gMnSc6	hroznýš
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
perfektní	perfektní	k2eAgNnSc1d1	perfektní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vypravěč	vypravěč	k1gMnSc1	vypravěč
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
asteroidu	asteroid	k1gInSc6	asteroid
<g/>
,	,	kIx,	,
domovu	domov	k1gInSc6	domov
malého	malý	k2eAgMnSc2d1	malý
prince	princ	k1gMnSc2	princ
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgMnSc1d1	malý
princ	princ	k1gMnSc1	princ
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
planetce	planetka	k1gFnSc6	planetka
B612	B612	k1gFnSc2	B612
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tráví	trávit	k5eAaImIp3nP	trávit
svůj	svůj	k3xOyFgInSc4	svůj
den	den	k1gInSc4	den
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
vytrhává	vytrhávat	k5eAaImIp3nS	vytrhávat
baobaby	baobab	k1gInPc4	baobab
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tam	tam	k6eAd1	tam
neustále	neustále	k6eAd1	neustále
zakořeňují	zakořeňovat	k5eAaImIp3nP	zakořeňovat
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
by	by	kYmCp3nP	by
proměnily	proměnit	k5eAaPmAgInP	proměnit
planetku	planetka	k1gFnSc4	planetka
v	v	k7c4	v
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
je	on	k3xPp3gInPc4	on
někdo	někdo	k3yInSc1	někdo
nevytrhal	vytrhat	k5eNaPmAgMnS	vytrhat
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
si	se	k3xPyFc3	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
růžičku	růžička	k1gFnSc4	růžička
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
cit	cit	k1gInSc1	cit
opětuje	opětovat	k5eAaImIp3nS	opětovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
největší	veliký	k2eAgFnSc1d3	veliký
ho	on	k3xPp3gMnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
čisté	čistý	k2eAgFnSc2d1	čistá
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
planetce	planetka	k1gFnSc6	planetka
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
tři	tři	k4xCgFnPc1	tři
sopky	sopka	k1gFnPc1	sopka
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
činné	činný	k2eAgFnPc1d1	činná
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
vyhaslá	vyhaslý	k2eAgFnSc1d1	vyhaslá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgMnSc1d1	malý
princ	princ	k1gMnSc1	princ
opouští	opouštět	k5eAaImIp3nS	opouštět
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
viděl	vidět	k5eAaImAgInS	vidět
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
navštíví	navštívit	k5eAaPmIp3nS	navštívit
šest	šest	k4xCc1	šest
dalších	další	k2eAgFnPc2d1	další
planetek	planetka	k1gFnPc2	planetka
(	(	kIx(	(
<g/>
číslovaných	číslovaný	k2eAgInPc2d1	číslovaný
od	od	k7c2	od
325	[number]	k4	325
do	do	k7c2	do
330	[number]	k4	330
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
obývaná	obývaný	k2eAgFnSc1d1	obývaná
jiným	jiný	k2eAgMnSc7d1	jiný
dospělým	dospělí	k1gMnPc3	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
celému	celý	k2eAgInSc3d1	celý
vesmíru	vesmír	k1gInSc3	vesmír
<g/>
,	,	kIx,	,
káže	kázat	k5eAaImIp3nS	kázat
hvězdám	hvězda	k1gFnPc3	hvězda
činit	činit	k5eAaImF	činit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dělají	dělat	k5eAaImIp3nP	dělat
i	i	k9	i
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
poddaný	poddaný	k1gMnSc1	poddaný
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
ho	on	k3xPp3gMnSc4	on
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
rozkazy	rozkaz	k1gInPc4	rozkaz
rozumné	rozumný	k2eAgNnSc1d1	rozumné
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
ho	on	k3xPp3gMnSc4	on
král	král	k1gMnSc1	král
svým	svůj	k3xOyFgMnSc7	svůj
vyslancem	vyslanec	k1gMnSc7	vyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Domýšlivec	domýšlivec	k1gMnSc1	domýšlivec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
obdivován	obdivován	k2eAgInSc4d1	obdivován
všemi	všecek	k3xTgFnPc7	všecek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
planetce	planetka	k1gFnSc6	planetka
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Neslyší	slyšet	k5eNaImIp3nS	slyšet
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
není	být	k5eNaImIp3nS	být
projevem	projev	k1gInSc7	projev
obdivu	obdiv	k1gInSc2	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Pijan	pijan	k1gMnSc1	pijan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stydí	stydět	k5eAaImIp3nS	stydět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pije	pít	k5eAaImIp3nS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Byznysmen	byznysmen	k1gMnSc1	byznysmen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
zaneprázdněný	zaneprázdněný	k2eAgInSc4d1	zaneprázdněný
počítáním	počítání	k1gNnSc7	počítání
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc4	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Přeje	přát	k5eAaImIp3nS	přát
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
koupit	koupit	k5eAaPmF	koupit
více	hodně	k6eAd2	hodně
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Lampář	lampář	k1gMnSc1	lampář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
planetce	planetka	k1gFnSc6	planetka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
den	den	k1gInSc1	den
a	a	k8xC	a
noc	noc	k1gFnSc1	noc
střídají	střídat	k5eAaImIp3nP	střídat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nějakou	nějaký	k3yIgFnSc7	nějaký
dobou	doba	k1gFnSc7	doba
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
rozsvěcet	rozsvěcet	k5eAaImF	rozsvěcet
lampu	lampa	k1gFnSc4	lampa
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
zhasínat	zhasínat	k5eAaImF	zhasínat
ji	on	k3xPp3gFnSc4	on
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
otáčelo	otáčet	k5eAaImAgNnS	otáčet
rozumnou	rozumný	k2eAgFnSc7d1	rozumná
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
lampář	lampář	k1gMnSc1	lampář
měl	mít	k5eAaImAgMnS	mít
čas	čas	k1gInSc4	čas
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ale	ale	k9	ale
rychlost	rychlost	k1gFnSc1	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Lampář	lampář	k1gMnSc1	lampář
chtěl	chtít	k5eAaImAgMnS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
věrný	věrný	k2eAgMnSc1d1	věrný
příkazu	příkaz	k1gInSc2	příkaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
rozžíná	rozžínat	k5eAaImIp3nS	rozžínat
a	a	k8xC	a
zhasíná	zhasínat	k5eAaImIp3nS	zhasínat
lampu	lampa	k1gFnSc4	lampa
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisec	zeměpisec	k1gMnSc1	zeměpisec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tráví	trávit	k5eAaImIp3nS	trávit
všechen	všechen	k3xTgInSc4	všechen
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
tvorbou	tvorba	k1gFnSc7	tvorba
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
neopouští	opouštět	k5eNaImIp3nP	opouštět
svůj	svůj	k3xOyFgInSc4	svůj
stůl	stůl	k1gInSc4	stůl
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
objevům	objev	k1gInPc3	objev
<g/>
.	.	kIx.	.
</s>
<s>
Hlubinný	hlubinný	k2eAgMnSc1d1	hlubinný
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
Eugen	Eugna	k1gFnPc2	Eugna
Drewermann	Drewermann	k1gMnSc1	Drewermann
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
tuto	tento	k3xDgFnSc4	tento
pohádku	pohádka	k1gFnSc4	pohádka
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
očím	oko	k1gNnPc3	oko
neviditelné	viditelný	k2eNgFnPc1d1	neviditelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
postavy	postava	k1gFnPc4	postava
pohádky	pohádka	k1gFnSc2	pohádka
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Malý	malý	k2eAgMnSc1d1	malý
Princ	princ	k1gMnSc1	princ
-	-	kIx~	-
disociované	disociovaný	k2eAgNnSc1d1	disociované
alterego	alterego	k1gNnSc1	alterego
samotného	samotný	k2eAgMnSc2d1	samotný
vypravěče	vypravěč	k1gMnSc2	vypravěč
(	(	kIx(	(
<g/>
Antoine	Antoin	k1gInSc5	Antoin
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnPc4	Saint-Exupéra
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
AdSE	AdSE	k1gFnSc1	AdSE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Disociaci	disociace	k1gFnSc3	disociace
štěpení	štěpení	k1gNnSc1	štěpení
spouští	spouštět	k5eAaImIp3nS	spouštět
stres	stres	k1gInSc4	stres
ze	z	k7c2	z
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgFnSc2d1	ohrožující
nehody	nehoda	k1gFnSc2	nehoda
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
mezi	mezi	k7c7	mezi
pilotem	pilot	k1gMnSc7	pilot
a	a	k8xC	a
Malým	Malý	k1gMnSc7	Malý
Princem	princ	k1gMnSc7	princ
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
rozhovor	rozhovor	k1gInSc4	rozhovor
dospělého	dospělý	k1gMnSc2	dospělý
AdSE	AdSE	k1gMnPc2	AdSE
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
dítětem	dítě	k1gNnSc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růže	k1gFnSc1	růže
-	-	kIx~	-
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
<g/>
,	,	kIx,	,
vyčítavá	vyčítavý	k2eAgFnSc1d1	vyčítavá
matka	matka	k1gFnSc1	matka
až	až	k9	až
možná	možná	k9	možná
s	s	k7c7	s
hysterickými	hysterický	k2eAgInPc7d1	hysterický
rysy	rys	k1gInPc7	rys
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
synovi	syn	k1gMnSc3	syn
(	(	kIx(	(
<g/>
AdSE	AdSE	k1gFnSc1	AdSE
<g/>
)	)	kIx)	)
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
citové	citový	k2eAgNnSc1d1	citové
odpoutání	odpoutání	k1gNnSc1	odpoutání
<g/>
.	.	kIx.	.
</s>
<s>
Liška	liška	k1gFnSc1	liška
-	-	kIx~	-
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
podle	podle	k7c2	podle
Drewermanna	Drewermann	k1gInSc2	Drewermann
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
vypořádává	vypořádávat	k5eAaImIp3nS	vypořádávat
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
mezi	mezi	k7c7	mezi
vlastním	vlastní	k2eAgNnSc7d1	vlastní
dětstvím	dětství	k1gNnSc7	dětství
a	a	k8xC	a
dospělostí	dospělost	k1gFnSc7	dospělost
<g/>
.	.	kIx.	.
</s>
