<s>
Cholera	cholera	k1gFnSc1	cholera
je	být	k5eAaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
průjmové	průjmový	k2eAgNnSc1d1	průjmové
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
gramnegativní	gramnegativní	k2eAgFnSc1d1	gramnegativní
bakterie	bakterie	k1gFnSc1	bakterie
Vibrio	vibrio	k1gNnSc4	vibrio
cholerae	cholera	k1gInSc2	cholera
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kmeny	kmen	k1gInPc1	kmen
Clasica	Clasicum	k1gNnSc2	Clasicum
I	i	k8xC	i
a	a	k9	a
El	Ela	k1gFnPc2	Ela
Tor	Tor	k1gMnSc1	Tor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejčastějšího	častý	k2eAgInSc2d3	nejčastější
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
asijská	asijský	k2eAgFnSc1d1	asijská
cholera	cholera	k1gFnSc1	cholera
(	(	kIx(	(
<g/>
cholera	cholera	k1gFnSc1	cholera
asiatica	asiatica	k1gMnSc1	asiatica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
alimentární	alimentární	k2eAgFnSc7d1	alimentární
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
znečištěnou	znečištěný	k2eAgFnSc4d1	znečištěná
fekáliemi	fekálie	k1gFnPc7	fekálie
(	(	kIx(	(
<g/>
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
Vibrio	vibrio	k1gNnSc1	vibrio
cholerae	cholera	k1gInSc2	cholera
schopno	schopen	k2eAgNnSc1d1	schopno
přežívat	přežívat	k5eAaImF	přežívat
až	až	k9	až
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
potravinami	potravina	k1gFnPc7	potravina
(	(	kIx(	(
<g/>
dny	den	k1gInPc1	den
až	až	k9	až
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
přežití	přežití	k1gNnSc2	přežití
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
klesající	klesající	k2eAgFnSc7d1	klesající
teplotou	teplota	k1gFnSc7	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účinnou	účinný	k2eAgFnSc4d1	účinná
prevenci	prevence	k1gFnSc4	prevence
představuje	představovat	k5eAaImIp3nS	představovat
mytí	mytí	k1gNnSc1	mytí
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
převařování	převařování	k1gNnSc4	převařování
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
její	její	k3xOp3gFnSc2	její
dezinfekce	dezinfekce	k1gFnSc2	dezinfekce
<g/>
,	,	kIx,	,
dodržování	dodržování	k1gNnSc1	dodržování
základních	základní	k2eAgNnPc2d1	základní
hygienických	hygienický	k2eAgNnPc2d1	hygienické
pravidel	pravidlo	k1gNnPc2	pravidlo
při	při	k7c6	při
zacházení	zacházení	k1gNnSc6	zacházení
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
a	a	k8xC	a
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
jen	jen	k9	jen
na	na	k7c4	na
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
nákazy	nákaza	k1gFnSc2	nákaza
je	být	k5eAaImIp3nS	být
nemocný	mocný	k2eNgMnSc1d1	nemocný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
člověk-přenašeč	člověkřenašeč	k1gMnSc1	člověk-přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
dostatečně	dostatečně	k6eAd1	dostatečně
velkou	velký	k2eAgFnSc4d1	velká
dávku	dávka	k1gFnSc4	dávka
vibrií	vibrio	k1gNnPc2	vibrio
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nesnášejí	snášet	k5eNaImIp3nP	snášet
kyselé	kyselý	k2eAgNnSc4d1	kyselé
prostředí	prostředí	k1gNnSc4	prostředí
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jich	on	k3xPp3gMnPc2	on
cestu	cesta	k1gFnSc4	cesta
přes	přes	k7c4	přes
něj	on	k3xPp3gMnSc4	on
nepřežije	přežít	k5eNaPmIp3nS	přežít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
projde	projít	k5eAaPmIp3nS	projít
až	až	k9	až
do	do	k7c2	do
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
množit	množit	k5eAaImF	množit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
produkují	produkovat	k5eAaImIp3nP	produkovat
toxin	toxin	k1gInSc4	toxin
zvaný	zvaný	k2eAgInSc4d1	zvaný
choleratoxin	choleratoxin	k1gInSc4	choleratoxin
<g/>
.	.	kIx.	.
</s>
<s>
Choleratoxin	Choleratoxin	k1gInSc1	Choleratoxin
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
epitelové	epitelový	k2eAgFnPc4d1	epitelová
buňky	buňka	k1gFnPc4	buňka
Lieberkühnových	Lieberkühnův	k2eAgFnPc2d1	Lieberkühnův
krypt	krypta	k1gFnPc2	krypta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
secernují	secernovat	k5eAaImIp3nP	secernovat
do	do	k7c2	do
lumen	lumen	k1gInSc4	lumen
střeva	střevo	k1gNnSc2	střevo
Cl-	Cl-	k1gFnSc2	Cl-
<g/>
.	.	kIx.	.
</s>
<s>
Chloridový	chloridový	k2eAgInSc1d1	chloridový
aniont	aniont	k1gInSc1	aniont
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
sodným	sodný	k2eAgInSc7d1	sodný
kationtem	kation	k1gInSc7	kation
<g/>
.	.	kIx.	.
</s>
<s>
Choleratoxin	Choleratoxin	k1gInSc1	Choleratoxin
v	v	k7c6	v
zasažených	zasažený	k2eAgFnPc6d1	zasažená
buňkách	buňka	k1gFnPc6	buňka
blokuje	blokovat	k5eAaImIp3nS	blokovat
GTPasu	GTPasa	k1gFnSc4	GTPasa
Gs-proteinů	Gsrotein	k1gMnPc2	Gs-protein
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
permanentně	permanentně	k6eAd1	permanentně
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
adenylátcyklasu	adenylátcyklasa	k1gFnSc4	adenylátcyklasa
<g/>
.	.	kIx.	.
</s>
<s>
Podjednotka	podjednotka	k1gFnSc1	podjednotka
cholera	cholera	k1gFnSc1	cholera
toxinu	toxin	k1gInSc2	toxin
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
ADP	ADP	kA	ADP
ribosylační	ribosylační	k2eAgInSc1d1	ribosylační
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
aktivitu	aktivita	k1gFnSc4	aktivita
Gα	Gα	k1gFnSc2	Gα
podjednotky	podjednotka	k1gFnSc2	podjednotka
heterotrimerního	heterotrimerní	k2eAgInSc2d1	heterotrimerní
G	G	kA	G
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
cAMP	camp	k1gInSc1	camp
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Kanály	kanál	k1gInPc1	kanál
secernující	secernující	k2eAgFnSc2d1	secernující
Cl-	Cl-	k1gFnSc2	Cl-
tedy	tedy	k9	tedy
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
lumen	lumen	k1gInSc1	lumen
střeva	střevo	k1gNnSc2	střevo
jimi	on	k3xPp3gInPc7	on
prochází	procházet	k5eAaImIp3nS	procházet
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
chloridových	chloridový	k2eAgInPc2d1	chloridový
aniontů	anion	k1gInPc2	anion
doprovázených	doprovázený	k2eAgInPc2d1	doprovázený
sodíkem	sodík	k1gInSc7	sodík
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
vody	voda	k1gFnSc2	voda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
26	[number]	k4	26
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
HCO3-	HCO3-	k1gFnPc2	HCO3-
a	a	k8xC	a
K	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
až	až	k8xS	až
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
u	u	k7c2	u
těžké	těžký	k2eAgFnSc2d1	těžká
varianty	varianta	k1gFnSc2	varianta
choroby	choroba	k1gFnSc2	choroba
<g/>
)	)	kIx)	)
křečovité	křečovitý	k2eAgFnPc1d1	křečovitá
bolesti	bolest	k1gFnPc1	bolest
břicha	břicho	k1gNnSc2	břicho
a	a	k8xC	a
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc4	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
krevního	krevní	k2eAgInSc2d1	krevní
objemu	objem	k1gInSc2	objem
a	a	k8xC	a
acidóze	acidóza	k1gFnSc6	acidóza
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
léčby	léčba	k1gFnSc2	léčba
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
draslíkových	draslíkův	k2eAgInPc2d1	draslíkův
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
zhroucení	zhroucení	k1gNnSc3	zhroucení
homeostázy	homeostáza	k1gFnSc2	homeostáza
a	a	k8xC	a
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
přítomností	přítomnost	k1gFnSc7	přítomnost
vibrií	vibrio	k1gNnPc2	vibrio
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
krevními	krevní	k2eAgInPc7d1	krevní
testy	test	k1gInPc7	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
nahrazování	nahrazování	k1gNnSc6	nahrazování
ztracené	ztracený	k2eAgFnSc2d1	ztracená
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
minerálií	minerálie	k1gFnPc2	minerálie
(	(	kIx(	(
<g/>
u	u	k7c2	u
lehčích	lehký	k2eAgInPc2d2	lehčí
případů	případ	k1gInPc2	případ
dostatečným	dostatečný	k2eAgNnSc7d1	dostatečné
pitím	pití	k1gNnSc7	pití
-	-	kIx~	-
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
je	být	k5eAaImIp3nS	být
solnoglukózní	solnoglukózní	k2eAgInSc1d1	solnoglukózní
roztok	roztok	k1gInSc1	roztok
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
u	u	k7c2	u
těžkých	těžký	k2eAgInPc2d1	těžký
případů	případ	k1gInPc2	případ
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
ztrátami	ztráta	k1gFnPc7	ztráta
tekutin	tekutina	k1gFnPc2	tekutina
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
intravenózní	intravenózní	k2eAgNnSc1d1	intravenózní
doplnění	doplnění	k1gNnSc1	doplnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
těžších	těžký	k2eAgInPc2d2	těžší
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
nasazují	nasazovat	k5eAaImIp3nP	nasazovat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
neléčená	léčený	k2eNgFnSc1d1	neléčená
cholera	cholera	k1gFnSc1	cholera
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
asi	asi	k9	asi
v	v	k7c6	v
50	[number]	k4	50
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
při	při	k7c6	při
včasné	včasný	k2eAgFnSc6d1	včasná
diagnóze	diagnóza	k1gFnSc6	diagnóza
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc6	dostupnost
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
umírá	umírat	k5eAaImIp3nS	umírat
něco	něco	k3yInSc4	něco
kolem	kolem	k7c2	kolem
0,75	[number]	k4	0,75
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
při	při	k7c6	při
epidemiích	epidemie	k1gFnPc6	epidemie
v	v	k7c6	v
chudých	chudý	k2eAgFnPc6d1	chudá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
3-15	[number]	k4	3-15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
prokazatelné	prokazatelný	k2eAgFnPc1d1	prokazatelná
epidemie	epidemie	k1gFnPc1	epidemie
cholery	cholera	k1gFnSc2	cholera
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tyto	tento	k3xDgFnPc1	tento
pandemie	pandemie	k1gFnPc1	pandemie
cholery	cholera	k1gFnSc2	cholera
<g/>
:	:	kIx,	:
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1826	[number]	k4	1826
<g/>
:	:	kIx,	:
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1851	[number]	k4	1851
<g/>
:	:	kIx,	:
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
1852	[number]	k4	1852
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
:	:	kIx,	:
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
:	:	kIx,	:
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
:	:	kIx,	:
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
od	od	k7c2	od
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
Tor	Tor	k1gMnSc1	Tor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
mírná	mírný	k2eAgFnSc1d1	mírná
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
asymptomatická	asymptomatický	k2eAgFnSc1d1	asymptomatická
<g/>
.	.	kIx.	.
od	od	k7c2	od
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
139	[number]	k4	139
Bengal	Bengal	k1gInSc4	Bengal
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
spíše	spíše	k9	spíše
jen	jen	k9	jen
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
od	od	k7c2	od
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
.	.	kIx.	.
od	od	k7c2	od
2015	[number]	k4	2015
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
</s>
<s>
Bakterii	bakterie	k1gFnSc4	bakterie
Vibrio	vibrio	k1gNnSc4	vibrio
cholerae	cholerae	k1gInSc1	cholerae
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
izoloval	izolovat	k5eAaBmAgMnS	izolovat
italský	italský	k2eAgMnSc1d1	italský
anatom	anatom	k1gMnSc1	anatom
Filippo	Filippa	k1gFnSc5	Filippa
Pacini	Pacin	k2eAgMnPc1d1	Pacin
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
Silbernagl	Silbernagl	k1gInSc1	Silbernagl
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
fyziologie	fyziologie	k1gFnSc2	fyziologie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
Cholera	cholera	k1gFnSc1	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cholera	cholera	k1gFnSc1	cholera
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cholera	cholera	k1gFnSc1	cholera
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
