<s>
Cumulonimbus	Cumulonimbus	k1gMnSc1	Cumulonimbus
(	(	kIx(	(
<g/>
Cb	Cb	k1gMnSc1	Cb
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bouřkový	bouřkový	k2eAgInSc4d1	bouřkový
oblak	oblak	k1gInSc4	oblak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roste	růst	k5eAaImIp3nS	růst
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
výšek	výška	k1gFnPc2	výška
kolem	kolem	k7c2	kolem
5000	[number]	k4	5000
až	až	k9	až
15	[number]	k4	15
000	[number]	k4	000
m.	m.	k?	m.
</s>
<s>
Díky	díky	k7c3	díky
tropopauze	tropopauza	k1gFnSc3	tropopauza
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gFnSc4	jeho
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
typický	typický	k2eAgInSc4d1	typický
tvar	tvar	k1gInSc4	tvar
kovadliny	kovadlina	k1gFnSc2	kovadlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
příčinou	příčina	k1gFnSc7	příčina
všech	všecek	k3xTgFnPc2	všecek
bouřek	bouřka	k1gFnPc2	bouřka
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
jevů	jev	k1gInPc2	jev
jako	jako	k8xS	jako
například	například	k6eAd1	například
tornádo	tornádo	k1gNnSc1	tornádo
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gInSc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
intenzity	intenzita	k1gFnSc2	intenzita
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
související	související	k2eAgInPc4d1	související
pruhy	pruh	k1gInPc4	pruh
virga	virg	k1gMnSc2	virg
<g/>
,	,	kIx,	,
krupobití	krupobití	k1gNnSc1	krupobití
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
sněhové	sněhový	k2eAgFnSc2d1	sněhová
bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Jeho	jeho	k3xOp3gInPc4	jeho
tvary	tvar	k1gInPc4	tvar
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
stupnice	stupnice	k1gFnSc1	stupnice
intenzity	intenzita	k1gFnSc2	intenzita
srážek	srážka	k1gFnPc2	srážka
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vypadávající	vypadávající	k2eAgFnPc1d1	vypadávající
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
a	a	k8xC	a
proto	proto	k8xC	proto
tento	tento	k3xDgInSc4	tento
oblak	oblak	k1gInSc4	oblak
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
různá	různý	k2eAgNnPc1d1	různé
latinsky	latinsky	k6eAd1	latinsky
popsaná	popsaný	k2eAgNnPc1d1	popsané
stádia	stádium	k1gNnPc1	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
Cb	Cb	k1gFnSc1	Cb
mamma	mamma	k1gFnSc1	mamma
vznikající	vznikající	k2eAgFnSc1d1	vznikající
v	v	k7c6	v
prudkých	prudký	k2eAgFnPc6d1	prudká
letních	letní	k2eAgFnPc6d1	letní
bouřích	bouř	k1gFnPc6	bouř
jako	jako	k8xS	jako
jev	jev	k1gInSc4	jev
ustupující	ustupující	k2eAgFnSc2d1	ustupující
síly	síla	k1gFnSc2	síla
bouře	bouř	k1gFnSc2	bouř
a	a	k8xC	a
rozpadu	rozpad	k1gInSc2	rozpad
oblačnosti	oblačnost	k1gFnSc2	oblačnost
do	do	k7c2	do
cirrů	cirr	k1gInPc2	cirr
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
nastupující	nastupující	k2eAgFnSc2d1	nastupující
menší	malý	k2eAgFnSc2d2	menší
bouřky	bouřka	k1gFnSc2	bouřka
lokálních	lokální	k2eAgInPc2d1	lokální
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Cb	Cb	k?	Cb
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
z	z	k7c2	z
oblačnosti	oblačnost	k1gFnSc2	oblačnost
typu	typ	k1gInSc2	typ
cumulus	cumulus	k1gInSc1	cumulus
congestus	congestus	k1gInSc1	congestus
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
nejvíc	nejvíc	k6eAd1	nejvíc
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
jako	jako	k8xC	jako
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
jev	jev	k1gInSc1	jev
vlhkého	vlhký	k2eAgNnSc2d1	vlhké
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
větších	veliký	k2eAgNnPc2d2	veliký
veder	vedro	k1gNnPc2	vedro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
nástup	nástup	k1gInSc1	nástup
studené	studený	k2eAgFnSc2d1	studená
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Cb	Cb	k1gFnSc1	Cb
znamená	znamenat	k5eAaImIp3nS	znamenat
široké	široký	k2eAgNnSc4d1	široké
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
oblaku	oblak	k1gInSc2	oblak
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
velkého	velký	k2eAgInSc2d1	velký
výskytu	výskyt	k1gInSc2	výskyt
bouřkové	bouřkový	k2eAgFnSc2d1	bouřková
činnosti	činnost	k1gFnSc2	činnost
zejména	zejména	k9	zejména
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cumulonimbus	Cumulonimbus	k1gInSc1	Cumulonimbus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cumulonimbus	cumulonimbus	k1gInSc1	cumulonimbus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
