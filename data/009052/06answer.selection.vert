<s>
Guelfové	guelf	k1gMnPc1	guelf
a	a	k8xC	a
ghibellini	ghibellin	k1gMnPc1	ghibellin
byla	být	k5eAaImAgNnP	být
mocenská	mocenský	k2eAgNnPc1d1	mocenské
seskupení	seskupení	k1gNnPc1	seskupení
italských	italský	k2eAgFnPc2d1	italská
(	(	kIx(	(
<g/>
i	i	k8xC	i
německých	německý	k2eAgInPc2d1	německý
<g/>
)	)	kIx)	)
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
středověku	středověk	k1gInSc2	středověk
bojovala	bojovat	k5eAaImAgFnS	bojovat
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
území	území	k1gNnSc2	území
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
