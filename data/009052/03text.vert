<p>
<s>
Guelfové	guelf	k1gMnPc1	guelf
a	a	k8xC	a
ghibellini	ghibellin	k1gMnPc1	ghibellin
byla	být	k5eAaImAgNnP	být
mocenská	mocenský	k2eAgNnPc1d1	mocenské
seskupení	seskupení	k1gNnPc1	seskupení
italských	italský	k2eAgFnPc2d1	italská
(	(	kIx(	(
<g/>
i	i	k8xC	i
německých	německý	k2eAgInPc2d1	německý
<g/>
)	)	kIx)	)
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
středověku	středověk	k1gInSc2	středověk
bojovala	bojovat	k5eAaImAgFnS	bojovat
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
území	území	k1gNnSc2	území
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
ghibellini	ghibellin	k1gMnPc1	ghibellin
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
jména	jméno	k1gNnSc2	jméno
Waiblingen	Waiblingen	k1gInSc1	Waiblingen
<g/>
,	,	kIx,	,
rodového	rodový	k2eAgInSc2d1	rodový
hradu	hrad	k1gInSc2	hrad
Štaufů	Štauf	k1gMnPc2	Štauf
<g/>
;	;	kIx,	;
guelfové	guelf	k1gMnPc1	guelf
je	on	k3xPp3gInPc4	on
zase	zase	k9	zase
italský	italský	k2eAgInSc1d1	italský
přepis	přepis	k1gInSc1	přepis
jména	jméno	k1gNnSc2	jméno
rodu	rod	k1gInSc2	rod
Welfů	Welf	k1gInPc2	Welf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nástupu	nástup	k1gInSc2	nástup
štaufské	štaufský	k2eAgFnSc2d1	štaufská
dynastie	dynastie	k1gFnSc2	dynastie
na	na	k7c4	na
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
(	(	kIx(	(
<g/>
1138	[number]	k4	1138
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
římsko-němečtí	římskoěmecký	k2eAgMnPc1d1	římsko-německý
císaři	císař	k1gMnPc1	císař
snažili	snažit	k5eAaImAgMnP	snažit
politicky	politicky	k6eAd1	politicky
i	i	k8xC	i
vojensky	vojensky	k6eAd1	vojensky
ovládnout	ovládnout	k5eAaPmF	ovládnout
území	území	k1gNnSc4	území
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
formálně	formálně	k6eAd1	formálně
náleželo	náležet	k5eAaImAgNnS	náležet
ke	k	k7c3	k
Svaté	svatý	k2eAgFnSc3d1	svatá
říši	říš	k1gFnSc3	říš
římské	římský	k2eAgFnSc3d1	římská
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
papežů	papež	k1gMnPc2	papež
a	a	k8xC	a
na	na	k7c6	na
říši	říš	k1gFnSc6	říš
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ghibellini	ghibellin	k1gMnPc5	ghibellin
==	==	k?	==
</s>
</p>
<p>
<s>
Štaufové	Štauf	k1gMnPc1	Štauf
byli	být	k5eAaImAgMnP	být
mocným	mocný	k2eAgInSc7d1	mocný
rodem	rod	k1gInSc7	rod
vládnoucím	vládnoucí	k2eAgInSc7d1	vládnoucí
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
císaři	císař	k1gMnSc3	císař
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
králové	král	k1gMnPc1	král
Itálie	Itálie	k1gFnSc2	Itálie
nemohli	moct	k5eNaImAgMnP	moct
nečinně	činně	k6eNd1	činně
přihlížet	přihlížet	k5eAaImF	přihlížet
osamostatňujícím	osamostatňující	k2eAgFnPc3d1	osamostatňující
tendencím	tendence	k1gFnPc3	tendence
severoitalských	severoitalský	k2eAgFnPc2d1	severoitalská
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
papeži	papež	k1gMnSc3	papež
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přidaly	přidat	k5eAaPmAgInP	přidat
italské	italský	k2eAgInPc1d1	italský
rody	rod	k1gInPc1	rod
Gonzaga	Gonzag	k1gMnSc2	Gonzag
a	a	k8xC	a
Scala	scát	k5eAaImAgFnS	scát
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jihoněmecká	jihoněmecký	k2eAgNnPc1d1	jihoněmecké
knížata	kníže	k1gNnPc1	kníže
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
podporovala	podporovat	k5eAaImAgFnS	podporovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
vůči	vůči	k7c3	vůči
Welfům	Welf	k1gMnPc3	Welf
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
alianci	aliance	k1gFnSc3	aliance
patřila	patřit	k5eAaImAgFnS	patřit
italská	italský	k2eAgNnPc1d1	italské
města	město	k1gNnPc4	město
Arezzo	Arezza	k1gFnSc5	Arezza
<g/>
,	,	kIx,	,
Forli	Forl	k1gFnSc5	Forl
<g/>
,	,	kIx,	,
Modena	Modena	k1gFnSc1	Modena
<g/>
,	,	kIx,	,
Osimo	Osima	k1gFnSc5	Osima
<g/>
,	,	kIx,	,
Pavia	Pavia	k1gFnSc1	Pavia
<g/>
,	,	kIx,	,
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
,	,	kIx,	,
Pistoia	Pistoia	k1gFnSc1	Pistoia
<g/>
,	,	kIx,	,
Siena	Siena	k1gFnSc1	Siena
<g/>
,	,	kIx,	,
Spoleto	Spolet	k2eAgNnSc1d1	Spoleto
a	a	k8xC	a
Todi	Todi	k1gNnPc7	Todi
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
podpora	podpora	k1gFnSc1	podpora
byla	být	k5eAaImAgFnS	být
střídavá	střídavý	k2eAgFnSc1d1	střídavá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Guelfové	guelf	k1gMnPc5	guelf
==	==	k?	==
</s>
</p>
<p>
<s>
Welfové	Welf	k1gMnPc1	Welf
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
soupeřící	soupeřící	k2eAgInSc1d1	soupeřící
se	s	k7c7	s
Štaufy	Štauf	k1gInPc7	Štauf
o	o	k7c4	o
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
naopak	naopak	k6eAd1	naopak
stranili	stranit	k5eAaImAgMnP	stranit
papežům	papež	k1gMnPc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Podpořili	podpořit	k5eAaPmAgMnP	podpořit
je	on	k3xPp3gNnSc4	on
italský	italský	k2eAgInSc1d1	italský
rod	rod	k1gInSc1	rod
Este	Est	k1gMnSc5	Est
a	a	k8xC	a
italská	italský	k2eAgNnPc1d1	italské
města	město	k1gNnPc1	město
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
,	,	kIx,	,
Brescia	Brescia	k1gFnSc1	Brescia
<g/>
,	,	kIx,	,
Crema	Crema	k1gFnSc1	Crema
<g/>
,	,	kIx,	,
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Lodi	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
Mantua	Mantua	k1gFnSc1	Mantua
<g/>
,	,	kIx,	,
Orvieto	Orvieto	k1gNnSc1	Orvieto
a	a	k8xC	a
Perugia	Perugia	k1gFnSc1	Perugia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Franco	Franco	k6eAd1	Franco
Cardini	Cardin	k2eAgMnPc1d1	Cardin
<g/>
:	:	kIx,	:
Ghibellinen	Ghibellinen	k1gInSc1	Ghibellinen
<g/>
,	,	kIx,	,
Lexikon	lexikon	k1gInSc1	lexikon
des	des	k1gNnSc1	des
Mittelalters	Mittelalters	k1gInSc1	Mittelalters
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
1436	[number]	k4	1436
<g/>
–	–	k?	–
<g/>
1438	[number]	k4	1438
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franco	Franco	k6eAd1	Franco
Cardini	Cardin	k2eAgMnPc1d1	Cardin
<g/>
:	:	kIx,	:
Guelfen	Guelfen	k1gInSc1	Guelfen
<g/>
,	,	kIx,	,
Lexikon	lexikon	k1gInSc1	lexikon
des	des	k1gNnSc1	des
Mittelalters	Mittelalters	k1gInSc1	Mittelalters
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
1763	[number]	k4	1763
<g/>
–	–	k?	–
<g/>
1765	[number]	k4	1765
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guelfové	guelf	k1gMnPc1	guelf
a	a	k8xC	a
ghibellini	ghibellin	k1gMnPc1	ghibellin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Guelphs	Guelphs	k1gInSc1	Guelphs
and	and	k?	and
Ghibellines	Ghibellines	k1gInSc1	Ghibellines
</s>
</p>
