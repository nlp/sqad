<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc2	narození
neznámé	známý	k2eNgNnSc1d1	neznámé
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1109	[number]	k4	1109
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1117	[number]	k4	1117
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1120	[number]	k4	1120
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc4	jméno
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
strýci	strýc	k1gMnSc6	strýc
<g/>
,	,	kIx,	,
polském	polský	k2eAgMnSc6d1	polský
knížeti	kníže	k1gMnSc6	kníže
Vladislavu	Vladislav	k1gMnSc6	Vladislav
Hermanovi	Herman	k1gMnSc6	Herman
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
byl	být	k5eAaImAgMnS	být
druhorozeným	druhorozený	k2eAgMnSc7d1	druhorozený
synem	syn	k1gMnSc7	syn
krále	král	k1gMnSc2	král
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Svatavy	Svatava	k1gFnPc1	Svatava
Polské	polský	k2eAgFnPc1d1	polská
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgMnSc7	třetí
z	z	k7c2	z
Vratislavových	Vratislavův	k2eAgMnPc2d1	Vratislavův
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
bratrem	bratr	k1gMnSc7	bratr
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Soběslava	Soběslava	k1gFnSc1	Soběslava
I.	I.	kA	I.
a	a	k8xC	a
mladším	mladý	k2eAgMnSc6d2	mladší
(	(	kIx(	(
<g/>
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
<g/>
)	)	kIx)	)
bratrem	bratr	k1gMnSc7	bratr
Břetislava	Břetislav	k1gMnSc2	Břetislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Vladislav	Vladislav	k1gMnSc1	Vladislav
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
knížetem	kníže	k1gMnSc7	kníže
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
druhým	druhý	k4xOgMnSc7	druhý
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
odvozovala	odvozovat	k5eAaImAgFnS	odvozovat
linie	linie	k1gFnSc1	linie
dědičných	dědičný	k2eAgMnPc2d1	dědičný
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
synem	syn	k1gMnSc7	syn
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
vnukem	vnuk	k1gMnSc7	vnuk
Vladislava	Vladislav	k1gMnSc2	Vladislav
I.	I.	kA	I.
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
se	se	k3xPyFc4	se
o	o	k7c6	o
Vladislavově	Vladislavův	k2eAgNnSc6d1	Vladislavovo
mládí	mládí	k1gNnSc6	mládí
příliš	příliš	k6eAd1	příliš
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
ale	ale	k8xC	ale
žil	žít	k5eAaImAgMnS	žít
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
knížetem	kníže	k1gMnSc7	kníže
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
pokusil	pokusit	k5eAaPmAgMnS	pokusit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1105	[number]	k4	1105
sesadit	sesadit	k5eAaPmF	sesadit
Vladislavova	Vladislavův	k2eAgMnSc4d1	Vladislavův
bratra	bratr	k1gMnSc4	bratr
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
ještě	ještě	k9	ještě
na	na	k7c6	na
Bořivojově	Bořivojův	k2eAgFnSc6d1	Bořivojova
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
popudil	popudit	k5eAaPmAgMnS	popudit
většinu	většina	k1gFnSc4	většina
velmožů	velmož	k1gMnPc2	velmož
<g/>
,	,	kIx,	,
obklopil	obklopit	k5eAaPmAgMnS	obklopit
se	se	k3xPyFc4	se
novými	nový	k2eAgFnPc7d1	nová
rádci	rádce	k1gMnSc3	rádce
a	a	k8xC	a
i	i	k9	i
Vladislav	Vladislav	k1gMnSc1	Vladislav
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
převzetí	převzetí	k1gNnSc4	převzetí
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1107	[number]	k4	1107
Svatoplukovi	Svatopluk	k1gMnSc3	Svatopluk
vyšel	vyjít	k5eAaPmAgMnS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
knížetem	kníže	k1gNnSc7wR	kníže
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
se	s	k7c7	s
šlechtou	šlechta	k1gFnSc7	šlechta
byl	být	k5eAaImAgInS	být
slib	slib	k1gInSc1	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
Svatoplukovým	Svatoplukův	k2eAgInSc7d1	Svatoplukův
nástupce	nástupce	k1gMnSc4	nástupce
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1109	[number]	k4	1109
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
moravským	moravský	k2eAgNnSc7d1	Moravské
vojskem	vojsko	k1gNnSc7	vojsko
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
knížete	kníže	k1gMnSc4	kníže
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Ota	Ota	k1gMnSc1	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
Svatoplukův	Svatoplukův	k2eAgMnSc1d1	Svatoplukův
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
šlechta	šlechta	k1gFnSc1	šlechta
ale	ale	k9	ale
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c6	na
předchozí	předchozí	k2eAgFnSc6d1	předchozí
dohodě	dohoda	k1gFnSc6	dohoda
a	a	k8xC	a
Ota	Ota	k1gMnSc1	Ota
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Vladislavovy	Vladislavův	k2eAgFnSc2d1	Vladislavova
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odjel	odjet	k5eAaPmAgMnS	odjet
přijmout	přijmout	k5eAaPmF	přijmout
Čechy	Čech	k1gMnPc4	Čech
v	v	k7c6	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
ovšem	ovšem	k9	ovšem
rychle	rychle	k6eAd1	rychle
získal	získat	k5eAaPmAgInS	získat
podporu	podpora	k1gFnSc4	podpora
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
-	-	kIx~	-
za	za	k7c4	za
500	[number]	k4	500
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1110	[number]	k4	1110
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
znepřátelených	znepřátelený	k2eAgFnPc2d1	znepřátelená
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Rokycanech	Rokycany	k1gInPc6	Rokycany
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Vladislavovu	Vladislavův	k2eAgFnSc4d1	Vladislavova
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
tvrdě	tvrdě	k6eAd1	tvrdě
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
odpůrci	odpůrce	k1gMnPc7	odpůrce
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Soběslav	Soběslava	k1gFnPc2	Soběslava
I.	I.	kA	I.
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
získal	získat	k5eAaPmAgMnS	získat
krále	král	k1gMnSc4	král
Boleslava	Boleslav	k1gMnSc4	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Křivoústého	křivoústý	k2eAgNnSc2d1	křivoústý
<g/>
;	;	kIx,	;
postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
polským	polský	k2eAgInPc3d1	polský
vpádům	vpád	k1gInPc3	vpád
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Soběslav	Soběslav	k1gMnSc1	Soběslav
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
díky	díky	k7c3	díky
Křivoústému	křivoústý	k2eAgInSc3d1	křivoústý
mohl	moct	k5eAaImAgInS	moct
vrátit	vrátit	k5eAaPmF	vrátit
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
různé	různý	k2eAgInPc4d1	různý
úděly	úděl	k1gInPc4	úděl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1114	[number]	k4	1114
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
významného	významný	k2eAgInSc2d1	významný
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Jindřicha	Jindřich	k1gMnSc4	Jindřich
V.	V.	kA	V.
s	s	k7c7	s
Matyldou	Matylda	k1gFnSc7	Matylda
Anglickou	anglický	k2eAgFnSc7d1	anglická
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
zastával	zastávat	k5eAaImAgMnS	zastávat
jako	jako	k9	jako
kníže	kníže	k1gMnSc1	kníže
úřad	úřad	k1gInSc1	úřad
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
číšníka	číšník	k1gMnSc2	číšník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
byla	být	k5eAaImAgFnS	být
předstupněm	předstupeň	k1gInSc7	předstupeň
pro	pro	k7c4	pro
právo	právo	k1gNnSc4	právo
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
náležet	náležet	k5eAaImF	náležet
mezi	mezi	k7c7	mezi
kurfiřty	kurfiřt	k1gMnPc7	kurfiřt
a	a	k8xC	a
volit	volit	k5eAaImF	volit
německého	německý	k2eAgMnSc4d1	německý
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1115	[number]	k4	1115
se	se	k3xPyFc4	se
znepřátelení	znepřátelený	k2eAgMnPc1d1	znepřátelený
představitelé	představitel	k1gMnPc1	představitel
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
rodu	rod	k1gInSc2	rod
postupně	postupně	k6eAd1	postupně
sbližovali	sbližovat	k5eAaImAgMnP	sbližovat
<g/>
.	.	kIx.	.
</s>
<s>
Soběslav	Soběslav	k1gMnSc1	Soběslav
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
úděly	úděl	k1gInPc1	úděl
Žatecko	Žatecko	k1gNnSc1	Žatecko
<g/>
,	,	kIx,	,
Brněnsko	Brněnsko	k1gNnSc1	Brněnsko
a	a	k8xC	a
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
<g/>
;	;	kIx,	;
Olomoucko	Olomoucko	k1gNnSc1	Olomoucko
bylo	být	k5eAaImAgNnS	být
Otovi	Otův	k2eAgMnPc1d1	Otův
II	II	kA	II
<g/>
.	.	kIx.	.
vráceno	vrátit	k5eAaPmNgNnS	vrátit
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1115	[number]	k4	1115
založil	založit	k5eAaPmAgMnS	založit
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
kladrubský	kladrubský	k2eAgInSc4d1	kladrubský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1117	[number]	k4	1117
se	se	k3xPyFc4	se
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
mu	on	k3xPp3gMnSc3	on
z	z	k7c2	z
nepříliš	příliš	k6eNd1	příliš
jasných	jasný	k2eAgFnPc2d1	jasná
příčin	příčina	k1gFnPc2	příčina
dobrovolně	dobrovolně	k6eAd1	dobrovolně
předal	předat	k5eAaPmAgMnS	předat
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
si	se	k3xPyFc3	se
vyčlenil	vyčlenit	k5eAaPmAgMnS	vyčlenit
panství	panství	k1gNnSc4	panství
na	na	k7c6	na
Žatecku	Žatecko	k1gNnSc6	Žatecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1120	[number]	k4	1120
se	s	k7c7	s
Vladislav	Vladislava	k1gFnPc2	Vladislava
I.	I.	kA	I.
znovu	znovu	k6eAd1	znovu
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
prchl	prchnout	k5eAaPmAgInS	prchnout
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
roky	rok	k1gInPc1	rok
byly	být	k5eAaImAgInP	být
opět	opět	k6eAd1	opět
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rozporů	rozpor	k1gInPc2	rozpor
-	-	kIx~	-
Soběslav	Soběslav	k1gMnSc1	Soběslav
(	(	kIx(	(
<g/>
I.	I.	kA	I.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zbaven	zbaven	k2eAgMnSc1d1	zbaven
moravských	moravský	k2eAgMnPc2d1	moravský
údělů	úděl	k1gInPc2	úděl
a	a	k8xC	a
hledal	hledat	k5eAaImAgMnS	hledat
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Brněnsko	Brněnsko	k1gNnSc4	Brněnsko
získal	získat	k5eAaPmAgMnS	získat
Ota	Ota	k1gMnSc1	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Litolda	Litold	k1gMnSc2	Litold
Znojemského	znojemský	k2eAgMnSc2d1	znojemský
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
určil	určit	k5eAaPmAgMnS	určit
Vladislav	Vladislav	k1gMnSc1	Vladislav
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
Otu	Ota	k1gMnSc4	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomouckého	olomoucký	k2eAgMnSc4d1	olomoucký
<g/>
,	,	kIx,	,
teprve	teprve	k9	teprve
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
královny	královna	k1gFnSc2	královna
vdovy	vdova	k1gFnSc2	vdova
Svatavy	Svatava	k1gFnSc2	Svatava
a	a	k8xC	a
biskupa	biskup	k1gMnSc2	biskup
Oty	Ota	k1gMnSc2	Ota
Bamberského	bamberský	k2eAgMnSc2d1	bamberský
změnil	změnit	k5eAaPmAgInS	změnit
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Příštím	příští	k2eAgMnSc7d1	příští
knížetem	kníže	k1gMnSc7	kníže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Soběslav	Soběslav	k1gMnSc1	Soběslav
I.	I.	kA	I.
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
benediktinském	benediktinský	k2eAgInSc6d1	benediktinský
kladrubském	kladrubský	k2eAgInSc6d1	kladrubský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
uprostřed	uprostřed	k7c2	uprostřed
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
konventního	konventní	k2eAgInSc2d1	konventní
chrámu	chrám	k1gInSc2	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
hrob	hrob	k1gInSc1	hrob
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1653	[number]	k4	1653
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
náhrobek	náhrobek	k1gInSc1	náhrobek
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
částí	část	k1gFnSc7	část
chóru	chór	k1gInSc2	chór
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Richenzou	Richenza	k1gFnSc7	Richenza
z	z	k7c2	z
Bergu	Berg	k1gInSc2	Berg
(	(	kIx(	(
<g/>
†	†	k?	†
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Vladislav	Vladislav	k1gMnSc1	Vladislav
(	(	kIx(	(
<g/>
1110	[number]	k4	1110
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
1174	[number]	k4	1174
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
a	a	k8xC	a
král	král	k1gMnSc1	král
∞	∞	k?	∞
1140	[number]	k4	1140
Gertruda	Gertrud	k1gMnSc2	Gertrud
Babenberská	babenberský	k2eAgFnSc1d1	Babenberská
∞	∞	k?	∞
1153	[number]	k4	1153
Judita	Judita	k1gFnSc1	Judita
Durynská	durynský	k2eAgFnSc1d1	Durynská
Děpolt	Děpolt	k1gInSc4	Děpolt
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1120	[number]	k4	1120
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
-	-	kIx~	-
1167	[number]	k4	1167
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
rodu	rod	k1gInSc2	rod
Děpolticů	Děpoltice	k1gMnPc2	Děpoltice
∞	∞	k?	∞
Gertruda	Gertruda	k1gFnSc1	Gertruda
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
†	†	k?	†
po	po	k7c6	po
1169	[number]	k4	1169
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Markéta	Markéta	k1gFnSc1	Markéta
Svatava	Svatava	k1gFnSc1	Svatava
(	(	kIx(	(
<g/>
†	†	k?	†
po	po	k7c6	po
1146	[number]	k4	1146
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Fridrich	Fridrich	k1gMnSc1	Fridrich
z	z	k7c2	z
Bogenu	Bogen	k1gInSc2	Bogen
<g/>
,	,	kIx,	,
purkrabí	purkrabí	k1gMnSc1	purkrabí
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
</s>
