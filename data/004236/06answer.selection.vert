<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
381	[number]	k4	381
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
