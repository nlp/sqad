<s>
Dněpr	Dněpr	k1gInSc1	Dněpr
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
Dnipro	Dnipro	k1gNnSc1	Dnipro
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
Dňapro	Dňapro	k1gNnSc1	Dňapro
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
Dňěpr	Dňěpr	k1gInSc1	Dňěpr
<g/>
,	,	kIx,	,
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
Borysthenés	Borysthenés	k1gInSc1	Borysthenés
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
postupně	postupně	k6eAd1	postupně
územím	území	k1gNnSc7	území
západního	západní	k2eAgNnSc2d1	západní
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Smolenská	Smolenský	k2eAgFnSc1d1	Smolenská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
východní	východní	k2eAgNnSc4d1	východní
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
(	(	kIx(	(
<g/>
Vitebská	Vitebský	k2eAgFnSc1d1	Vitebská
<g/>
,	,	kIx,	,
Mohylevská	Mohylevský	k2eAgFnSc1d1	Mohylevský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
a	a	k8xC	a
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
(	(	kIx(	(
<g/>
Černihivská	Černihivský	k2eAgFnSc1d1	Černihivský
<g/>
,	,	kIx,	,
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
<g/>
,	,	kIx,	,
Čerkaská	Čerkaský	k2eAgFnSc1d1	Čerkaská
<g/>
,	,	kIx,	,
Kirovohradská	Kirovohradský	k2eAgFnSc1d1	Kirovohradský
<g/>
,	,	kIx,	,
Poltavská	poltavský	k2eAgFnSc1d1	Poltavská
<g/>
,	,	kIx,	,
Dněpropetrovská	Dněpropetrovský	k2eAgFnSc1d1	Dněpropetrovská
<g/>
,	,	kIx,	,
Záporožská	záporožský	k2eAgFnSc1d1	Záporožská
<g/>
,	,	kIx,	,
Chersonská	chersonský	k2eAgFnSc1d1	Chersonská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
