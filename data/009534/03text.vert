<p>
<s>
Dněpr	Dněpr	k1gInSc1	Dněpr
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
Dnipro	Dnipro	k1gNnSc1	Dnipro
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
Dňapro	Dňapro	k1gNnSc1	Dňapro
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
Dňěpr	Dňěpr	k1gInSc1	Dňěpr
<g/>
,	,	kIx,	,
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
Borysthenés	Borysthenés	k1gInSc1	Borysthenés
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
postupně	postupně	k6eAd1	postupně
územím	území	k1gNnSc7	území
západního	západní	k2eAgNnSc2d1	západní
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Smolenská	Smolenský	k2eAgFnSc1d1	Smolenská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
východní	východní	k2eAgNnSc4d1	východní
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
(	(	kIx(	(
<g/>
Vitebská	Vitebský	k2eAgFnSc1d1	Vitebská
<g/>
,	,	kIx,	,
Mohylevská	Mohylevský	k2eAgFnSc1d1	Mohylevský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
a	a	k8xC	a
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
(	(	kIx(	(
<g/>
Černihivská	Černihivský	k2eAgFnSc1d1	Černihivský
<g/>
,	,	kIx,	,
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
<g/>
,	,	kIx,	,
Čerkaská	Čerkaský	k2eAgFnSc1d1	Čerkaská
<g/>
,	,	kIx,	,
Kirovohradská	Kirovohradský	k2eAgFnSc1d1	Kirovohradský
<g/>
,	,	kIx,	,
Poltavská	poltavský	k2eAgFnSc1d1	Poltavská
<g/>
,	,	kIx,	,
Dněpropetrovská	Dněpropetrovský	k2eAgFnSc1d1	Dněpropetrovská
<g/>
,	,	kIx,	,
Záporožská	záporožský	k2eAgFnSc1d1	Záporožská
<g/>
,	,	kIx,	,
Chersonská	chersonský	k2eAgFnSc1d1	Chersonská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
2201	[number]	k4	2201
km	km	kA	km
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
Volze	Volha	k1gFnSc6	Volha
<g/>
,	,	kIx,	,
Dunaji	Dunaj	k1gInSc6	Dunaj
a	a	k8xC	a
Uralu	Ural	k1gInSc6	Ural
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
délky	délka	k1gFnSc2	délka
řeky	řeka	k1gFnSc2	řeka
připadá	připadat	k5eAaImIp3nS	připadat
485	[number]	k4	485
km	km	kA	km
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
16	[number]	k4	16
km	km	kA	km
na	na	k7c4	na
bělorusko-ruskou	běloruskouský	k2eAgFnSc4d1	bělorusko-ruský
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
595	[number]	k4	595
km	km	kA	km
na	na	k7c4	na
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
,	,	kIx,	,
115	[number]	k4	115
km	km	kA	km
na	na	k7c4	na
bělorusko-ukrajinskou	běloruskokrajinský	k2eAgFnSc4d1	bělorusko-ukrajinský
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
516	[number]	k4	516
300	[number]	k4	300
km2	km2	k4	km2
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
Volze	Volha	k1gFnSc6	Volha
a	a	k8xC	a
Dunaji	Dunaj	k1gInSc6	Dunaj
řadí	řadit	k5eAaImIp3nP	řadit
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
Valdajské	Valdajský	k2eAgFnSc6d1	Valdajská
vysočině	vysočina	k1gFnSc6	vysočina
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
poblíž	poblíž	k7c2	poblíž
Chersonu	Cherson	k1gInSc2	Cherson
do	do	k7c2	do
Dněperského	dněperský	k2eAgInSc2d1	dněperský
limanu	liman	k1gInSc2	liman
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
do	do	k7c2	do
Kyjeva	Kyjev	k1gInSc2	Kyjev
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
1320	[number]	k4	1320
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
je	být	k5eAaImIp3nS	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
až	až	k6eAd1	až
nadbytečné	nadbytečný	k2eAgInPc1d1	nadbytečný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
zónou	zóna	k1gFnSc7	zóna
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
část	část	k1gFnSc1	část
toku	tok	k1gInSc2	tok
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Dorogobuž	Dorogobuž	k1gFnSc1	Dorogobuž
vede	vést	k5eAaImIp3nS	vést
mezi	mezi	k7c7	mezi
nízkými	nízký	k2eAgInPc7d1	nízký
a	a	k8xC	a
bažinatými	bažinatý	k2eAgInPc7d1	bažinatý
břehy	břeh	k1gInPc7	břeh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
převážně	převážně	k6eAd1	převážně
borovými	borový	k2eAgInPc7d1	borový
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
březovými	březový	k2eAgInPc7d1	březový
a	a	k8xC	a
smrkovými	smrkový	k2eAgInPc7d1	smrkový
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
až	až	k9	až
k	k	k7c3	k
městu	město	k1gNnSc3	město
Šklov	Šklovo	k1gNnPc2	Šklovo
protéká	protékat	k5eAaImIp3nS	protékat
kopcovitou	kopcovitý	k2eAgFnSc7d1	kopcovitá
krajinou	krajina	k1gFnSc7	krajina
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnPc1d1	říční
údolí	údolí	k1gNnPc1	údolí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těch	ten	k3xDgNnPc6	ten
místech	místo	k1gNnPc6	místo
úzké	úzký	k2eAgNnSc1d1	úzké
jen	jen	k6eAd1	jen
0,5	[number]	k4	0,5
až	až	k9	až
1	[number]	k4	1
km	km	kA	km
a	a	k8xC	a
říční	říční	k2eAgFnSc1d1	říční
niva	niva	k1gFnSc1	niva
místy	místy	k6eAd1	místy
zcela	zcela	k6eAd1	zcela
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Orša	Orš	k1gInSc2	Orš
řeka	řeka	k1gFnSc1	řeka
překonává	překonávat	k5eAaImIp3nS	překonávat
Kobeljacké	Kobeljacký	k2eAgInPc4d1	Kobeljacký
peřeje	peřej	k1gInPc4	peřej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
mezi	mezi	k7c7	mezi
Mohylevem	Mohylev	k1gInSc7	Mohylev
a	a	k8xC	a
Kyjevem	Kyjev	k1gInSc7	Kyjev
se	se	k3xPyFc4	se
říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
,	,	kIx,	,
niva	niva	k1gFnSc1	niva
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
šířky	šířka	k1gFnSc2	šířka
až	až	k9	až
14	[number]	k4	14
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
zaplavovanými	zaplavovaný	k2eAgInPc7d1	zaplavovaný
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
porosty	porost	k1gInPc7	porost
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
borovými	borový	k2eAgInPc7d1	borový
a	a	k8xC	a
listnatými	listnatý	k2eAgInPc7d1	listnatý
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
do	do	k7c2	do
Záporoží	Záporoží	k1gNnSc2	Záporoží
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
555	[number]	k4	555
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
a	a	k8xC	a
přísun	přísun	k1gInSc1	přísun
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
nestálý	stálý	k2eNgMnSc1d1	nestálý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
zónou	zóna	k1gFnSc7	zóna
lesostepí	lesostep	k1gFnPc2	lesostep
a	a	k8xC	a
stepí	step	k1gFnPc2	step
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
tohoto	tento	k3xDgInSc2	tento
úseku	úsek	k1gInSc2	úsek
je	být	k5eAaImIp3nS	být
dolina	dolina	k1gFnSc1	dolina
řeky	řeka	k1gFnSc2	řeka
široká	široký	k2eAgFnSc1d1	široká
6	[number]	k4	6
až	až	k9	až
18	[number]	k4	18
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
břehové	břehový	k2eAgFnPc1d1	Břehová
terasy	terasa	k1gFnPc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
břeh	břeh	k1gInSc1	břeh
je	být	k5eAaImIp3nS	být
vyvýšen	vyvýšen	k2eAgInSc1d1	vyvýšen
a	a	k8xC	a
prudce	prudko	k6eAd1	prudko
spadá	spadat	k5eAaImIp3nS	spadat
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
představuje	představovat	k5eAaImIp3nS	představovat
kaskádu	kaskáda	k1gFnSc4	kaskáda
přehrad	přehrada	k1gFnPc2	přehrada
(	(	kIx(	(
<g/>
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
<g/>
,	,	kIx,	,
Kaněvská	Kaněvský	k2eAgFnSc1d1	Kaněvský
<g/>
,	,	kIx,	,
Kremenčucká	Kremenčucký	k2eAgFnSc1d1	Kremenčucký
<g/>
,	,	kIx,	,
Kamjanská	Kamjanský	k2eAgFnSc1d1	Kamjanský
<g/>
,	,	kIx,	,
Dněperská	dněperský	k2eAgFnSc1d1	Dněperská
<g/>
)	)	kIx)	)
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
Kamjanským	Kamjanský	k2eAgMnSc7d1	Kamjanský
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
krátký	krátký	k2eAgInSc1d1	krátký
úsek	úsek	k1gInSc1	úsek
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
koryta	koryto	k1gNnSc2	koryto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Záporoží	Záporoží	k1gNnSc2	Záporoží
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
325	[number]	k4	325
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
zónou	zóna	k1gFnSc7	zóna
stepí	step	k1gFnPc2	step
v	v	k7c6	v
Přičernomořské	Přičernomořský	k2eAgFnSc6d1	Přičernomořský
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
vzdutím	vzdutí	k1gNnSc7	vzdutí
Kachovské	Kachovský	k2eAgFnSc2d1	Kachovský
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přítoky	přítok	k1gInPc4	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přítoky	přítok	k1gInPc4	přítok
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
rozdělené	rozdělená	k1gFnSc2	rozdělená
podle	podle	k7c2	podle
úseků	úsek	k1gInPc2	úsek
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
označují	označovat	k5eAaImIp3nP	označovat
pravý	pravý	k2eAgInSc4d1	pravý
a	a	k8xC	a
levý	levý	k2eAgInSc4d1	levý
přítok	přítok	k1gInSc4	přítok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
smíšený	smíšený	k2eAgInSc1d1	smíšený
a	a	k8xC	a
formuje	formovat	k5eAaImIp3nS	formovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
nad	nad	k7c7	nad
Kyjevem	Kyjev	k1gInSc7	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
27	[number]	k4	27
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
23	[number]	k4	23
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
roste	růst	k5eAaImIp3nS	růst
role	role	k1gFnSc1	role
srážek	srážka	k1gFnPc2	srážka
sněhových	sněhový	k2eAgFnPc2d1	sněhová
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
dešťových	dešťový	k2eAgMnPc2d1	dešťový
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
Kyjeva	Kyjev	k1gInSc2	Kyjev
činí	činit	k5eAaImIp3nS	činit
1380	[number]	k4	1380
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
25	[number]	k4	25
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgInSc4d1	minimální
200	[number]	k4	200
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgNnSc1d1	běžné
jarní	jarní	k2eAgNnSc1d1	jarní
maximum	maximum	k1gNnSc1	maximum
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7000	[number]	k4	7000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
.	.	kIx.	.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
celkový	celkový	k2eAgInSc1d1	celkový
roční	roční	k2eAgInSc1d1	roční
odtok	odtok	k1gInSc1	odtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
představuje	představovat	k5eAaImIp3nS	představovat
53	[number]	k4	53
km3	km3	k4	km3
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
73	[number]	k4	73
km3	km3	k4	km3
a	a	k8xC	a
minimální	minimální	k2eAgInSc4d1	minimální
24	[number]	k4	24
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
činí	činit	k5eAaImIp3nS	činit
pak	pak	k6eAd1	pak
1670	[number]	k4	1670
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Během	během	k7c2	během
jarního	jarní	k2eAgNnSc2d1	jarní
období	období	k1gNnSc2	období
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
vodního	vodní	k2eAgInSc2d1	vodní
stavu	stav	k1gInSc2	stav
proteče	protéct	k5eAaPmIp3nS	protéct
řekou	řeka	k1gFnSc7	řeka
60	[number]	k4	60
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
letech	let	k1gInPc6	let
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
celkového	celkový	k2eAgInSc2d1	celkový
ročního	roční	k2eAgInSc2d1	roční
průtoku	průtok	k1gInSc2	průtok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
povodním	povodeň	k1gFnPc3	povodeň
při	při	k7c6	při
deštích	dešť	k1gInPc6	dešť
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
při	při	k7c6	při
oteplení	oteplení	k1gNnSc6	oteplení
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
přítoky	přítok	k1gInPc1	přítok
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnPc4d1	základní
vodní	vodní	k2eAgFnPc4d1	vodní
cesty	cesta	k1gFnPc4	cesta
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
města	město	k1gNnSc2	město
Dorogobuž	Dorogobuž	k1gFnSc2	Dorogobuž
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1990	[number]	k4	1990
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
přehrad	přehrada	k1gFnPc2	přehrada
zmizely	zmizet	k5eAaPmAgInP	zmizet
někdejší	někdejší	k2eAgInPc1d1	někdejší
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
peřeje	peřej	k1gInPc1	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přístavy	přístav	k1gInPc1	přístav
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
jsou	být	k5eAaImIp3nP	být
Mohylev	Mohylev	k1gFnSc4	Mohylev
<g/>
,	,	kIx,	,
Rahačou	Rahačá	k1gFnSc4	Rahačá
<g/>
,	,	kIx,	,
Žlobin	Žlobin	k2eAgInSc4d1	Žlobin
<g/>
,	,	kIx,	,
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
Kaniv	Kaniva	k1gFnPc2	Kaniva
<g/>
,	,	kIx,	,
Čerkasy	Čerkasa	k1gFnPc1	Čerkasa
<g/>
,	,	kIx,	,
Kremenčuk	Kremenčuk	k1gInSc1	Kremenčuk
<g/>
,	,	kIx,	,
Dnipro	Dnipro	k1gNnSc1	Dnipro
<g/>
,	,	kIx,	,
Záporoží	Záporoží	k1gNnSc1	Záporoží
<g/>
,	,	kIx,	,
Nikopol	Nikopol	k1gInSc1	Nikopol
<g/>
,	,	kIx,	,
Cherson	Cherson	k1gInSc1	Cherson
<g/>
.	.	kIx.	.
</s>
<s>
Umělými	umělý	k2eAgFnPc7d1	umělá
vodními	vodní	k2eAgFnPc7d1	vodní
cestami	cesta	k1gFnPc7	cesta
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
řekami	řeka	k1gFnPc7	řeka
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
se	s	k7c7	s
Západní	západní	k2eAgFnSc7d1	západní
Dvinou	Dvina	k1gFnSc7	Dvina
(	(	kIx(	(
<g/>
Berezinská	berezinský	k2eAgFnSc1d1	berezinský
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
Němanem	Něman	k1gInSc7	Něman
(	(	kIx(	(
<g/>
Dněpersko-němanská	Dněperskoěmanský	k2eAgFnSc1d1	Dněpersko-němanský
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
Bugem	Bug	k1gInSc7	Bug
(	(	kIx(	(
<g/>
Dněpersko-bugská	Dněperskougský	k2eAgFnSc1d1	Dněpersko-bugský
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetika	k1gFnSc1	energetika
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
přehradní	přehradní	k2eAgFnPc1d1	přehradní
hráze	hráz	k1gFnPc1	hráz
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimiž	jenž	k3xRgMnPc7	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
přehradní	přehradní	k2eAgFnPc1d1	přehradní
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Dněperská	dněperský	k2eAgFnSc1d1	Dněperská
kaskáda	kaskáda	k1gFnSc1	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnPc1	ústí
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
Kaněvská	Kaněvský	k2eAgFnSc1d1	Kaněvský
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
Kaněvská	Kaněvský	k2eAgFnSc1d1	Kaněvský
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
Kremenčucká	Kremenčucký	k2eAgFnSc1d1	Kremenčucký
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
Kremenčucká	Kremenčucký	k2eAgFnSc1d1	Kremenčucký
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
Kamjanská	Kamjanský	k2eAgFnSc1d1	Kamjanský
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
Středodněperská	Středodněperský	k2eAgFnSc1d1	Středodněperský
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
Dněperská	dněperský	k2eAgFnSc1d1	Dněperská
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
DněproGES	DněproGES	k1gFnSc1	DněproGES
</s>
</p>
<p>
<s>
Kachovská	Kachovský	k2eAgFnSc1d1	Kachovský
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
Kachovská	Kachovský	k2eAgFnSc1d1	Kachovský
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
===	===	k?	===
Zásobování	zásobování	k1gNnSc2	zásobování
vodou	voda	k1gFnSc7	voda
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Kachovské	Kachovský	k2eAgFnSc2d1	Kachovský
přehrady	přehrada	k1gFnSc2	přehrada
odtékají	odtékat	k5eAaImIp3nP	odtékat
zavlažovací	zavlažovací	k2eAgInPc1d1	zavlažovací
kanály	kanál	k1gInPc1	kanál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kanál	kanál	k1gInSc1	kanál
Dněpr-Krivoj	Dněpr-Krivoj	k1gInSc1	Dněpr-Krivoj
Roh	roh	k1gInSc1	roh
</s>
</p>
<p>
<s>
Severokrymský	Severokrymský	k2eAgInSc1d1	Severokrymský
kanál	kanál	k1gInSc1	kanál
</s>
</p>
<p>
<s>
===	===	k?	===
Osídlení	osídlení	k1gNnSc2	osídlení
===	===	k?	===
</s>
</p>
<p>
<s>
Města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Sídla	sídlo	k1gNnPc1	sídlo
oblastí	oblast	k1gFnSc7	oblast
jsou	být	k5eAaImIp3nP	být
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
tučně	tučně	k6eAd1	tučně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
o	o	k7c6	o
řece	řeka	k1gFnSc6	řeka
(	(	kIx(	(
<g/>
Borysthenés	Borysthenés	k1gInSc1	Borysthenés
(	(	kIx(	(
<g/>
Β	Β	k?	Β
<g/>
))	))	k?	))
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Dějinách	dějiny	k1gFnPc6	dějiny
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
Skythů	Skyth	k1gMnPc2	Skyth
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
o	o	k7c6	o
třetí	třetí	k4xOgFnSc6	třetí
největší	veliký	k2eAgFnSc6d3	veliký
známé	známý	k2eAgFnSc6d1	známá
řece	řeka	k1gFnSc6	řeka
po	po	k7c6	po
Nilu	Nil	k1gInSc6	Nil
a	a	k8xC	a
Istru	Ister	k1gInSc6	Ister
(	(	kIx(	(
<g/>
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
důležitou	důležitý	k2eAgFnSc4d1	důležitá
dopravní	dopravní	k2eAgFnSc4d1	dopravní
a	a	k8xC	a
tržní	tržní	k2eAgFnSc7d1	tržní
cestou	cesta	k1gFnSc7	cesta
mezi	mezi	k7c7	mezi
Baltským	baltský	k2eAgNnSc7d1	Baltské
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
využívaly	využívat	k5eAaPmAgFnP	využívat
i	i	k8xC	i
antické	antický	k2eAgFnPc1d1	antická
kolonie	kolonie	k1gFnPc1	kolonie
na	na	k7c6	na
černomořském	černomořský	k2eAgNnSc6d1	černomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
tržní	tržní	k2eAgInSc4d1	tržní
význam	význam	k1gInSc4	význam
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nájezdů	nájezd	k1gInPc2	nájezd
Hunů	Hun	k1gMnPc2	Hun
a	a	k8xC	a
Bulharů	Bulhar	k1gMnPc2	Bulhar
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
opět	opět	k6eAd1	opět
rostl	růst	k5eAaImAgInS	růst
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
formováním	formování	k1gNnSc7	formování
prvních	první	k4xOgInPc2	první
slovanských	slovanský	k2eAgInPc2d1	slovanský
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
obchodní	obchodní	k2eAgFnSc1d1	obchodní
cesta	cesta	k1gFnSc1	cesta
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
letopisech	letopis	k1gInPc6	letopis
"	"	kIx"	"
<g/>
iz	iz	k?	iz
Varjag	Varjag	k1gMnSc1	Varjag
v	v	k7c6	v
Greki	Grek	k1gInSc6	Grek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
s	s	k7c7	s
Černomořím	Černomoří	k1gNnSc7	Černomoří
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
města	město	k1gNnSc2	město
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
Smolensk	Smolensk	k1gInSc1	Smolensk
<g/>
,	,	kIx,	,
Černigov	Černigov	k1gInSc1	Černigov
<g/>
,	,	kIx,	,
Perejaslav	Perejaslav	k1gMnSc1	Perejaslav
<g/>
,	,	kIx,	,
Ljubeč	Ljubeč	k1gMnSc1	Ljubeč
<g/>
,	,	kIx,	,
Vyšgorod	Vyšgorod	k1gInSc1	Vyšgorod
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
Finský	finský	k2eAgInSc4d1	finský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Rižský	rižský	k2eAgInSc1d1	rižský
záliv	záliv	k1gInSc1	záliv
do	do	k7c2	do
Smolenska	Smolensko	k1gNnSc2	Smolensko
odtud	odtud	k6eAd1	odtud
po	po	k7c6	po
Dněpru	Dněpr	k1gInSc6	Dněpr
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
provozovali	provozovat	k5eAaImAgMnP	provozovat
švédští	švédský	k2eAgMnPc1d1	švédský
vikingové	viking	k1gMnPc1	viking
<g/>
,	,	kIx,	,
varjagové	varjag	k1gMnPc1	varjag
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vybírali	vybírat	k5eAaImAgMnP	vybírat
od	od	k7c2	od
místního	místní	k2eAgNnSc2d1	místní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tribut	tribut	k1gInSc1	tribut
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
se	s	k7c7	s
zajatci	zajatec	k1gMnPc7	zajatec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
prodávali	prodávat	k5eAaImAgMnP	prodávat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Cenné	cenný	k2eAgFnPc1d1	cenná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
podněperských	podněperský	k2eAgFnPc2d1	podněperský
oblastí	oblast	k1gFnPc2	oblast
podává	podávat	k5eAaImIp3nS	podávat
nejstarší	starý	k2eAgMnSc1d3	nejstarší
známý	známý	k2eAgMnSc1d1	známý
ruský	ruský	k2eAgMnSc1d1	ruský
letopisec	letopisec	k1gMnSc1	letopisec
Nestor	Nestor	k1gMnSc1	Nestor
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Povesti	Povest	k1gFnSc6	Povest
vremennych	vremennych	k1gMnSc1	vremennych
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mongolsko-tatarských	mongolskoatarský	k2eAgInPc2d1	mongolsko-tatarský
nájezdů	nájezd	k1gInPc2	nájezd
nastal	nastat	k5eAaPmAgInS	nastat
odliv	odliv	k1gInSc1	odliv
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ze	z	k7c2	z
středního	střední	k2eAgNnSc2d1	střední
podněpří	podněpří	k1gNnSc2	podněpří
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
řeka	řeka	k1gFnSc1	řeka
udržuje	udržovat	k5eAaImIp3nS	udržovat
svůj	svůj	k3xOyFgInSc4	svůj
obchodní	obchodní	k2eAgInSc4d1	obchodní
význam	význam	k1gInSc4	význam
především	především	k9	především
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
usazovali	usazovat	k5eAaImAgMnP	usazovat
Kozáci	Kozák	k1gMnPc1	Kozák
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
kozáckým	kozácký	k2eAgNnSc7d1	kozácké
střediskem	středisko	k1gNnSc7	středisko
byla	být	k5eAaImAgFnS	být
Záporožská	záporožský	k2eAgFnSc1d1	Záporožská
Síč	Síč	k1gFnSc1	Síč
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Chortycji	Chortycje	k1gFnSc4	Chortycje
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
zlikvidovala	zlikvidovat	k5eAaPmAgFnS	zlikvidovat
Kateřina	Kateřina	k1gFnSc1	Kateřina
Veliká	veliký	k2eAgFnSc1d1	veliká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
poslední	poslední	k2eAgMnSc1d1	poslední
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Stanislav	Stanislav	k1gMnSc1	Stanislav
August	August	k1gMnSc1	August
Poniatowski	Poniatowske	k1gFnSc4	Poniatowske
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
jižního	jižní	k2eAgNnSc2d1	jižní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
Dněpersko-bugský	Dněperskougský	k2eAgInSc4d1	Dněpersko-bugský
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
propojit	propojit	k5eAaPmF	propojit
Dněpr	Dněpr	k1gInSc4	Dněpr
s	s	k7c7	s
Vislou	Visla	k1gFnSc7	Visla
<g/>
.	.	kIx.	.
</s>
<s>
Dokončen	dokončit	k5eAaPmNgInS	dokončit
byl	být	k5eAaImAgInS	být
však	však	k9	však
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
aktivizace	aktivizace	k1gFnSc1	aktivizace
středního	střední	k2eAgInSc2d1	střední
toku	tok	k1gInSc2	tok
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
oslabením	oslabení	k1gNnSc7	oslabení
Krymského	krymský	k2eAgInSc2d1	krymský
chanátu	chanát	k1gInSc2	chanát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
jižní	jižní	k2eAgFnSc2d1	jižní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Krymu	Krym	k1gInSc2	Krym
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
ještě	ještě	k6eAd1	ještě
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
role	role	k1gFnSc1	role
řeky	řeka	k1gFnSc2	řeka
jako	jako	k8xS	jako
spojnice	spojnice	k1gFnSc2	spojnice
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Černého	černé	k1gNnSc2	černé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
růstu	růst	k1gInSc3	růst
osídlení	osídlení	k1gNnSc2	osídlení
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Jekatěrinoslav	Jekatěrinoslava	k1gFnPc2	Jekatěrinoslava
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
význam	význam	k1gInSc1	význam
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
rozvoji	rozvoj	k1gInSc3	rozvoj
dopravy	doprava	k1gFnSc2	doprava
stále	stále	k6eAd1	stále
bránily	bránit	k5eAaImAgFnP	bránit
peřeje	peřej	k1gFnPc1	peřej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
k	k	k7c3	k
prudkým	prudký	k2eAgInPc3d1	prudký
bojům	boj	k1gInPc3	boj
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
během	během	k7c2	během
Rusko-polské	ruskoolský	k2eAgFnSc2d1	rusko-polská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
Wrangelových	Wrangelův	k2eAgMnPc2d1	Wrangelův
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
během	během	k7c2	během
Ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
důležitou	důležitý	k2eAgFnSc4d1	důležitá
strategickou	strategický	k2eAgFnSc4d1	strategická
hranicí	hranice	k1gFnSc7	hranice
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
byly	být	k5eAaImAgInP	být
svedeny	sveden	k2eAgInPc1d1	sveden
velké	velký	k2eAgInPc1d1	velký
boje	boj	k1gInPc1	boj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
a	a	k8xC	a
především	především	k6eAd1	především
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Dněpr	Dněpr	k1gInSc4	Dněpr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
střední	střední	k2eAgMnSc1d1	střední
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
spoután	spoutat	k5eAaPmNgMnS	spoutat
kaskádou	kaskáda	k1gFnSc7	kaskáda
přehrad	přehrada	k1gFnPc2	přehrada
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
tak	tak	k6eAd1	tak
pomohla	pomoct	k5eAaPmAgFnS	pomoct
rozvoji	rozvoj	k1gInSc3	rozvoj
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
přirozený	přirozený	k2eAgInSc4d1	přirozený
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
o	o	k7c4	o
lužní	lužní	k2eAgInPc4d1	lužní
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Davydovv	Davydovv	k1gInSc1	Davydovv
L.	L.	kA	L.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Hydrografie	hydrografie	k1gFnSc1	hydrografie
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Д	Д	k?	Д
Л	Л	k?	Л
К	К	k?	К
<g/>
,	,	kIx,	,
Г	Г	k?	Г
С	С	k?	С
<g/>
,	,	kIx,	,
т	т	k?	т
2	[number]	k4	2
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Mirošničenko	Mirošničenka	k1gFnSc5	Mirošničenka
B.	B.	kA	B.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Po	po	k7c6	po
Dněpru	Dněpr	k1gInSc6	Dněpr
(	(	kIx(	(
<g/>
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
М	М	k?	М
Б	Б	k?	Б
А	А	k?	А
<g/>
,	,	kIx,	,
П	П	k?	П
Д	Д	k?	Д
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Laponogov	Laponogov	k1gInSc1	Laponogov
A.	A.	kA	A.
N.	N.	kA	N.
<g/>
,	,	kIx,	,
Po	po	k7c6	po
Dněpru	Dněpr	k1gInSc6	Dněpr
(	(	kIx(	(
<g/>
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Л	Л	k?	Л
А	А	k?	А
Н	Н	k?	Н
<g/>
,	,	kIx,	,
П	П	k?	П
Д	Д	k?	Д
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Д	Д	k?	Д
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Dněpr	Dněpr	k1gInSc1	Dněpr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dněpr	Dněpr	k1gInSc1	Dněpr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Borysthenes	Borysthenesa	k1gFnPc2	Borysthenesa
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
ekologické	ekologický	k2eAgFnSc2d1	ekologická
stránky	stránka	k1gFnSc2	stránka
-	-	kIx~	-
Dněpr	Dněpr	k1gInSc1	Dněpr
</s>
</p>
