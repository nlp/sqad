<s desamb="1">
Za	za	k7c2
své	svůj	k3xOyFgFnSc2
zásluhy	zásluha	k1gFnSc2
a	a	k8xC
na	na	k7c4
přímluvu	přímluva	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
bratrance	bratranec	k1gMnSc2
Františka	František	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Kolowrata	Kolowrat	k1gMnSc2
je	být	k5eAaImIp3nS
roku	rok	k1gInSc2
1830	#num#	k4
dosazen	dosadit	k5eAaPmNgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
na	na	k7c4
post	post	k1gInSc4
rakouského	rakouský	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
čtyřleté	čtyřletý	k2eAgNnSc1d1
vládní	vládní	k2eAgNnSc1d1
období	období	k1gNnSc1
bylo	být	k5eAaImAgNnS
neúspěšné	úspěšný	k2eNgNnSc1d1
a	a	k8xC
skončilo	skončit	k5eAaPmAgNnS
státním	státní	k2eAgInSc7d1
dluhem	dluh	k1gInSc7
<g/>
.	.	kIx.
</s>