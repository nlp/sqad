<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
Narození	narození	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1774	#num#	k4
nebo	nebo	k8xC
1774	#num#	k4
<g/>
Třebívlice	Třebívlice	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1857	#num#	k4
nebo	nebo	k8xC
1857	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
82	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Třebívlice	Třebívlice	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
ekonom	ekonom	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Amalie	Amalie	k1gFnSc1
von	von	k1gInSc1
Klebelsberg	Klebelsberg	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
na	na	k7c6
Thumburgu	Thumburg	k1gInSc6
(	(	kIx(
<g/>
1774	#num#	k4
<g/>
–	–	k?
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
<g/>
,	,	kIx,
sběratel	sběratel	k1gMnSc1
umění	umění	k1gNnSc2
a	a	k8xC
spoluzakladatel	spoluzakladatel	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Pražské	pražský	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
české	český	k2eAgFnSc2d1
větve	větev	k1gFnSc2
původně	původně	k6eAd1
tyrolského	tyrolský	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
povýšeného	povýšený	k2eAgMnSc2d1
do	do	k7c2
šlechtického	šlechtický	k2eAgInSc2d1
stavu	stav	k1gInSc2
svobodných	svobodný	k2eAgMnPc2d1
pánů	pan	k1gMnPc2
roku	rok	k1gInSc2
1530	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
pobělohorské	pobělohorský	k2eAgFnSc6d1
době	doba	k1gFnSc6
založil	založit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
českou	český	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1666	#num#	k4
získali	získat	k5eAaPmAgMnP
Klebelsbergové	Klebelsberg	k1gMnPc1
český	český	k2eAgInSc4d1
inkolát	inkolát	k1gInSc4
a	a	k8xC
roku	rok	k1gInSc2
1733	#num#	k4
byl	být	k5eAaImAgMnS
Maxmilán	Maxmilán	k2eAgMnSc1d1
Lambert	Lambert	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
povýšen	povýšen	k2eAgInSc4d1
do	do	k7c2
hraběcího	hraběcí	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
byl	být	k5eAaImAgMnS
vnukem	vnuk	k1gMnSc7
Josefa	Josef	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
(	(	kIx(
<g/>
1696	#num#	k4
<g/>
–	–	k?
<g/>
1737	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
syn	syn	k1gMnSc1
Vojtěcha	Vojtěch	k1gMnSc4
Václava	Václav	k1gMnSc4
<g/>
,	,	kIx,
hraběte	hrabě	k1gMnSc4
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
(	(	kIx(
<g/>
†	†	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
apelačního	apelační	k2eAgMnSc2d1
rady	rada	k1gMnSc2
a	a	k8xC
zemského	zemský	k2eAgMnSc2d1
soudce	soudce	k1gMnSc2
Království	království	k1gNnSc2
českého	český	k2eAgNnSc2d1
z	z	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
druhého	druhý	k4xOgInSc2
sňatku	sňatek	k1gInSc2
s	s	k7c7
Antonií	Antonie	k1gFnSc7
Kolowrat-Krakowskou	Kolowrat-Krakowska	k1gFnSc7
(	(	kIx(
<g/>
1737	#num#	k4
<g/>
–	–	k?
<g/>
1799	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
mladšího	mladý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
Ferdinanda	Ferdinand	k1gMnSc2
Františka	František	k1gMnSc2
(	(	kIx(
<g/>
1775	#num#	k4
<g/>
–	–	k?
<g/>
1793	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
jako	jako	k8xS,k8xC
praporečník	praporečník	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
u	u	k7c2
Maubeuge	Maubeug	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
otcova	otcův	k2eAgInSc2d1
prvního	první	k4xOgInSc2
sňatku	sňatek	k1gInSc2
s	s	k7c7
Gabrielou	Gabriela	k1gFnSc7
Lažanskou	Lažanský	k2eAgFnSc7d1
měl	mít	k5eAaImAgMnS
František	František	k1gMnSc1
nevlastního	vlastní	k2eNgMnSc2d1
bratra	bratr	k1gMnSc2
Jana	Jan	k1gMnSc2
Nepomuka	Nepomuk	k1gMnSc2
(	(	kIx(
<g/>
1768	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
sestru	sestra	k1gFnSc4
Annu	Anna	k1gFnSc4
Marii	Maria	k1gFnSc4
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1772	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
provdanou	provdaný	k2eAgFnSc4d1
Hildprandtovou	Hildprandtová	k1gFnSc4
z	z	k7c2
Ottenhausenu	Ottenhausen	k2eAgFnSc4d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzděláním	vzdělání	k1gNnSc7
a	a	k8xC
diplomatickými	diplomatický	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
na	na	k7c6
sebe	sebe	k3xPyFc4
František	František	k1gMnSc1
upozornil	upozornit	k5eAaPmAgMnS
již	již	k6eAd1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
rakouské	rakouský	k2eAgFnSc2d1
diplomacie	diplomacie	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgMnS
postavení	postavení	k1gNnSc4
tajného	tajný	k2eAgMnSc2d1
císařského	císařský	k2eAgMnSc2d1
komorníka	komorník	k1gMnSc2
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgMnS
postupně	postupně	k6eAd1
třem	tři	k4xCgInPc3
císařům	císař	k1gMnPc3
(	(	kIx(
<g/>
Leopoldu	Leopold	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Františkovi	František	k1gMnSc3
I.	I.	kA
a	a	k8xC
Ferdinandu	Ferdinand	k1gMnSc3
V.	V.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pomalu	pomalu	k6eAd1
stoupal	stoupat	k5eAaImAgInS
po	po	k7c6
dvorských	dvorský	k2eAgFnPc6d1
a	a	k8xC
správních	správní	k2eAgFnPc6d1
funkcích	funkce	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1825	#num#	k4
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
zastupujícím	zastupující	k2eAgMnPc3d1
místodržícím	místodržící	k1gMnPc3
moravsko-slezským	moravsko-slezský	k2eAgMnPc3d1
(	(	kIx(
<g/>
guberniálním	guberniální	k2eAgMnSc7d1
viceprezidentem	viceprezident	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1827	#num#	k4
skutečným	skutečný	k2eAgMnSc7d1
tajným	tajný	k2eAgMnSc7d1
radou	rada	k1gMnSc7
a	a	k8xC
zastupujícím	zastupující	k2eAgMnSc7d1
místodržícím	místodržící	k1gMnSc7
českým	český	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
je	být	k5eAaImIp3nS
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
místodržícím	místodržící	k1gMnSc7
Dolních	dolní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
se	se	k3xPyFc4
vyznamenal	vyznamenat	k5eAaPmAgInS
při	pře	k1gFnSc3
pomoci	pomoct	k5eAaPmF
obětem	oběť	k1gFnPc3
a	a	k8xC
odstraňování	odstraňování	k1gNnSc3
následků	následek	k1gInPc2
škod	škoda	k1gFnPc2
velké	velký	k2eAgFnSc2d1
dunajské	dunajský	k2eAgFnSc2d1
povodně	povodeň	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
své	svůj	k3xOyFgFnSc2
zásluhy	zásluha	k1gFnSc2
a	a	k8xC
na	na	k7c4
přímluvu	přímluva	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
bratrance	bratranec	k1gMnSc2
Františka	František	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Kolowrata	Kolowrat	k1gMnSc2
je	být	k5eAaImIp3nS
roku	rok	k1gInSc2
1830	#num#	k4
dosazen	dosadit	k5eAaPmNgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
na	na	k7c4
post	post	k1gInSc4
rakouského	rakouský	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
čtyřleté	čtyřletý	k2eAgNnSc1d1
vládní	vládní	k2eAgNnSc1d1
období	období	k1gNnSc1
bylo	být	k5eAaImAgNnS
neúspěšné	úspěšný	k2eNgNnSc1d1
a	a	k8xC
skončilo	skončit	k5eAaPmAgNnS
státním	státní	k2eAgInSc7d1
dluhem	dluh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
vystupoval	vystupovat	k5eAaImAgInS
proti	proti	k7c3
velkým	velký	k2eAgInPc3d1
monopolům	monopol	k1gInPc3
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgInSc2
prosazoval	prosazovat	k5eAaImAgMnS
raději	rád	k6eAd2
volnou	volný	k2eAgFnSc4d1
hospodářskou	hospodářský	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
funkci	funkce	k1gFnSc6
také	také	k9
podpořil	podpořit	k5eAaPmAgMnS
vznik	vznik	k1gInSc4
Rakouského	rakouský	k2eAgInSc2d1
Lloydu	Lloyd	k1gInSc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc3
rakouské	rakouský	k2eAgFnSc3d1
paroplavební	paroplavební	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Dědictvím	dědictví	k1gNnSc7
po	po	k7c6
otci	otec	k1gMnSc6
vlastnil	vlastnit	k5eAaImAgInS
zámek	zámek	k1gInSc1
Třebívlice	Třebívlice	k1gFnSc2
na	na	k7c6
Litoměřicku	Litoměřicko	k1gNnSc6
a	a	k8xC
pražský	pražský	k2eAgInSc1d1
palác	palác	k1gInSc1
čp.	čp.	k?
144	#num#	k4
<g/>
/	/	kIx~
<g/>
II	II	kA
na	na	k7c6
nároží	nároží	k1gNnSc6
ulic	ulice	k1gFnPc2
Voršilské	voršilský	k2eAgFnSc2d1
a	a	k8xC
Ostrovní	ostrovní	k2eAgFnSc2d1
(	(	kIx(
<g/>
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
zbořen	zbořen	k2eAgMnSc1d1
před	před	k7c7
stavbou	stavba	k1gFnSc7
univerzitní	univerzitní	k2eAgFnSc2d1
koleje	kolej	k1gFnSc2
Arnošta	Arnošt	k1gMnSc2
z	z	k7c2
Pardubic	Pardubice	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
palác	palác	k1gInSc1
v	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
Lázních	lázeň	k1gFnPc6
si	se	k3xPyFc3
dal	dát	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
roku	rok	k1gInSc2
1821	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Městečko	městečko	k1gNnSc1
Třebívlice	Třebívlice	k1gFnSc2
nadále	nadále	k6eAd1
zveleboval	zvelebovat	k5eAaImAgMnS
novou	nový	k2eAgFnSc7d1
výstavbou	výstavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finančně	finančně	k6eAd1
pomohl	pomoct	k5eAaPmAgMnS
se	s	k7c7
stavbou	stavba	k1gFnSc7
nové	nový	k2eAgFnSc2d1
obecní	obecní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
i	i	k8xC
radnice	radnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1818	#num#	k4
dokonce	dokonce	k9
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
přesvědčil	přesvědčit	k5eAaPmAgMnS
české	český	k2eAgNnSc4d1
gubernium	gubernium	k1gNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
jmenovalo	jmenovat	k5eAaBmAgNnS,k5eAaImAgNnS
Třebívlice	Třebívlice	k1gFnSc1
lázněmi	lázeň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
založil	založit	k5eAaPmAgMnS
v	v	k7c6
Třebívlicích	Třebívlice	k1gFnPc6
chudinský	chudinský	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
následně	následně	k6eAd1
i	i	k9
dotoval	dotovat	k5eAaBmAgInS
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Po	po	k7c6
dlouholetém	dlouholetý	k2eAgInSc6d1
vztahu	vztah	k1gInSc6
se	se	k3xPyFc4
roku	rok	k1gInSc2
1843	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
altmarskou	altmarský	k2eAgFnSc7d1
šlechtičnou	šlechtična	k1gFnSc7
Amálií	Amálie	k1gFnPc2
von	von	k1gInSc4
Broesigke	Broesigke	k1gFnSc4
<g/>
,	,	kIx,
ovdovělou	ovdovělý	k2eAgFnSc4d1
paní	paní	k1gFnSc4
von	von	k1gInSc4
Levetzow	Levetzow	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sňatku	sňatek	k1gInSc2
dlouho	dlouho	k6eAd1
bránil	bránit	k5eAaImAgInS
stav	stav	k1gInSc1
rozvedené	rozvedený	k2eAgFnSc2d1
nevěsty	nevěsta	k1gFnSc2
a	a	k8xC
odlišná	odlišný	k2eAgFnSc1d1
konfese	konfese	k1gFnSc1
<g/>
,	,	kIx,
Klebelsberg	Klebelsberg	k1gMnSc1
byl	být	k5eAaImAgMnS
katolík	katolík	k1gMnSc1
<g/>
,	,	kIx,
Amálie	Amálie	k1gFnSc1
protestantka	protestantka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
manželství	manželství	k1gNnSc1
bylo	být	k5eAaImAgNnS
bezdětné	bezdětný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amálie	Amálie	k1gFnPc4
si	se	k3xPyFc3
však	však	k9
ze	z	k7c2
dvou	dva	k4xCgNnPc2
předchozích	předchozí	k2eAgNnPc2d1
manželství	manželství	k1gNnPc2
přivedla	přivést	k5eAaPmAgFnS
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejstarší	starý	k2eAgFnSc1d3
Ulrika	Ulrika	k1gFnSc1
von	von	k1gInSc1
Levetzow	Levetzow	k1gFnSc1
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
–	–	k?
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
zdědila	zdědit	k5eAaPmAgFnS
Třebívlice	Třebívlice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Klebelsberg	Klebelsberg	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
doma	doma	k6eAd1
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Třebívlicích	Třebívlice	k1gFnPc6
a	a	k8xC
byl	být	k5eAaImAgMnS
stejně	stejně	k6eAd1
jako	jako	k9
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
rodiče	rodič	k1gMnSc4
pohřben	pohřben	k2eAgMnSc1d1
do	do	k7c2
rodinné	rodinný	k2eAgFnSc2d1
hrobky	hrobka	k1gFnSc2
pod	pod	k7c7
výklenkovou	výklenkový	k2eAgFnSc7d1
kaplí	kaple	k1gFnSc7
na	na	k7c6
katolickém	katolický	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc2
v	v	k7c6
Třebívlicích	Třebívlice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Zájmy	zájem	k1gInPc1
</s>
<s>
Klebelsberg	Klebelsberg	k1gMnSc1
byl	být	k5eAaImAgMnS
všestranně	všestranně	k6eAd1
vzdělaný	vzdělaný	k2eAgMnSc1d1
aristokrat	aristokrat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
velkostatkář	velkostatkář	k1gMnSc1
byl	být	k5eAaImAgMnS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
panství	panství	k1gNnSc4
dobrým	dobrý	k2eAgMnSc7d1
hospodářem	hospodář	k1gMnSc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
člen	člen	k1gInSc1
zemských	zemský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
se	se	k3xPyFc4
zasazoval	zasazovat	k5eAaImAgInS
o	o	k7c6
rozšíření	rozšíření	k1gNnSc6
chovu	chov	k1gInSc3
ovcí	ovce	k1gFnPc2
či	či	k8xC
pěstování	pěstování	k1gNnSc4
bource	bourec	k1gMnSc2
morušového	morušový	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pražském	pražský	k2eAgInSc6d1
paláci	palác	k1gInSc6
a	a	k8xC
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Třebívlicích	Třebívlice	k1gFnPc6
vybudoval	vybudovat	k5eAaPmAgMnS
knihovnu	knihovna	k1gFnSc4
se	s	k7c7
vzácnými	vzácný	k2eAgInPc7d1
rukopisy	rukopis	k1gInPc7
<g/>
,	,	kIx,
prvotiyk	prvotiyk	k6eAd1
a	a	k8xC
literaturou	literatura	k1gFnSc7
mnoha	mnoho	k4c2
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
filozofie	filozofie	k1gFnSc2
přes	přes	k7c4
přírodovědu	přírodověda	k1gFnSc4
(	(	kIx(
<g/>
geologie	geologie	k1gFnSc1
<g/>
,	,	kIx,
mineralogie	mineralogie	k1gFnSc1
<g/>
,	,	kIx,
geografie	geografie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
společenské	společenský	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
(	(	kIx(
<g/>
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
archeologie	archeologie	k1gFnSc1
<g/>
,	,	kIx,
heraldika	heraldika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
beletrií	beletrie	k1gFnPc2
<g/>
,	,	kIx,
cestopisy	cestopis	k1gInPc1
i	i	k8xC
knihy	kniha	k1gFnPc1
o	o	k7c6
výtvarném	výtvarný	k2eAgNnSc6d1
umění	umění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
knihy	kniha	k1gFnSc2
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
znalosti	znalost	k1gFnSc6
nejméně	málo	k6eAd3
šesti	šest	k4xCc2
jazyků	jazyk	k1gInPc2
-	-	kIx~
němčiny	němčina	k1gFnSc2
<g/>
,	,	kIx,
francouzštiny	francouzština	k1gFnSc2
<g/>
,	,	kIx,
španělštiny	španělština	k1gFnSc2
<g/>
,	,	kIx,
angličtiny	angličtina	k1gFnSc2
a	a	k8xC
latiny	latina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
milovníkem	milovník	k1gMnSc7
krásných	krásný	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
života	život	k1gInSc2
<g/>
,	,	kIx,
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
společníkem	společník	k1gMnSc7
a	a	k8xC
obdivovatelem	obdivovatel	k1gMnSc7
J.	J.	kA
J.	J.	kA
Winckelmanna	Winckelmanna	k1gFnSc1
a	a	k8xC
J.	J.	kA
W.	W.	kA
Goetha	Goetha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Klebelsberg	Klebelsberg	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
majitelem	majitel	k1gMnSc7
bohatých	bohatý	k2eAgFnPc2d1
granátových	granátový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Třebívlic	Třebívlice	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1819	#num#	k4
založil	založit	k5eAaPmAgMnS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
panství	panství	k1gNnSc6
brusírnu	brusírna	k1gFnSc4
českých	český	k2eAgMnPc2d1
granátů	granát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
společenským	společenský	k2eAgFnPc3d1
normám	norma	k1gFnPc3
jeho	jeho	k3xOp3gNnSc4
doby	doba	k1gFnPc1
patřily	patřit	k5eAaImAgFnP
časté	častý	k2eAgInPc4d1
pobyty	pobyt	k1gInPc4
v	v	k7c6
lázních	lázeň	k1gFnPc6
<g/>
,	,	kIx,
proto	proto	k8xC
si	se	k3xPyFc3
dal	dát	k5eAaPmAgMnS
v	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
lázních	lázeň	k1gFnPc6
vystavět	vystavět	k5eAaPmF
hotel	hotel	k1gInSc4
Weimar	Weimar	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zčásti	zčásti	k6eAd1
užíval	užívat	k5eAaImAgMnS
pro	pro	k7c4
sebe	sebe	k3xPyFc4
a	a	k8xC
své	svůj	k3xOyFgMnPc4
hosty	host	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
především	především	k6eAd1
pronajímal	pronajímat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštěvoval	navštěvovat	k5eAaImAgMnS
také	také	k9
rád	rád	k6eAd1
Teplice	Teplice	k1gFnPc1
nebo	nebo	k8xC
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Organizátor	organizátor	k1gMnSc1
</s>
<s>
Roku	rok	k1gInSc2
1810	#num#	k4
patřil	patřit	k5eAaImAgInS
v	v	k7c6
Praze	Praha	k1gFnSc6
k	k	k7c3
8	#num#	k4
šlechtickým	šlechtický	k2eAgMnPc3d1
signatářům	signatář	k1gMnPc3
zakládací	zakládací	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
Společnosti	společnost	k1gFnSc2
pro	pro	k7c4
zvelebení	zvelebení	k1gNnSc4
hudby	hudba	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
Pražská	pražský	k2eAgFnSc1d1
konzervatoř	konzervatoř	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
výuku	výuka	k1gFnSc4
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1818	#num#	k4
se	se	k3xPyFc4
přihlásil	přihlásit	k5eAaPmAgMnS
do	do	k7c2
užšího	úzký	k2eAgInSc2d2
okruhu	okruh	k1gInSc2
vlastenecky	vlastenecky	k6eAd1
smýšlejících	smýšlející	k2eAgMnPc2d1
aristokratů	aristokrat	k1gMnPc2
-	-	kIx~
zakladatelů	zakladatel	k1gMnPc2
Vlasteneckého	vlastenecký	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Janem	Jan	k1gMnSc7
Norbertem	Norbert	k1gMnSc7
z	z	k7c2
Neuberka	Neuberka	k1gFnSc1
sestavil	sestavit	k5eAaPmAgInS
a	a	k8xC
vlastní	vlastní	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
sepsal	sepsat	k5eAaPmAgMnS
první	první	k4xOgFnPc4
stanovy	stanova	k1gFnPc4
muzea	muzeum	k1gNnSc2
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
finanční	finanční	k2eAgInSc4d1
know-how	know-how	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
podporoval	podporovat	k5eAaImAgInS
až	až	k9
do	do	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrancem	bratranec	k1gMnSc7
Kašparem	Kašpar	k1gMnSc7
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
je	být	k5eAaImIp3nS
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
jeho	jeho	k3xOp3gMnPc4
zakladatele	zakladatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Pozůstalost	pozůstalost	k1gFnSc1
</s>
<s>
Svůj	svůj	k3xOyFgInSc1
majetek	majetek	k1gInSc1
odkázal	odkázat	k5eAaPmAgInS
manželce	manželka	k1gFnSc3
Amálii	Amálie	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
jej	on	k3xPp3gNnSc4
zdědila	zdědit	k5eAaPmAgFnS
svobodná	svobodný	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Ulrika	Ulrika	k1gFnSc1
a	a	k8xC
dva	dva	k4xCgInPc4
její	její	k3xOp3gNnSc4
synovci	synovec	k1gMnPc1
Franz	Franz	k1gMnSc1
a	a	k8xC
Adalbert	Adalbert	k1gMnSc1
Rauchovi	Rauch	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
o	o	k7c4
10	#num#	k4
tisících	tisící	k4xOgInPc2
dvazků	dvazek	k1gInPc2
se	s	k7c7
sbírkou	sbírka	k1gFnSc7
grafiky	grafika	k1gFnSc2
byla	být	k5eAaImAgFnS
deponována	deponovat	k5eAaBmNgFnS
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
malého	malý	k2eAgInSc2d1
souboru	soubor	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
rukopisné	rukopisný	k2eAgFnPc1d1
knížky	knížka	k1gFnPc1
památníku	památník	k1gInSc2
syna	syn	k1gMnSc2
Tychona	Tychon	k1gMnSc2
de	de	k?
Brahe	Brah	k1gFnSc2
a	a	k8xC
11	#num#	k4
tisků	tisk	k1gInPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
posléze	posléze	k6eAd1
rozptýlena	rozptýlen	k2eAgFnSc1d1
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc1
grafické	grafický	k2eAgInPc1d1
listy	list	k1gInPc1
rozprodány	rozprodán	k2eAgInPc1d1
na	na	k7c6
aukci	aukce	k1gFnSc6
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národnímu	národní	k2eAgNnSc3d1
muzeu	muzeum	k1gNnSc3
v	v	k7c6
Praze	Praha	k1gFnSc6
Klebelsberg	Klebelsberg	k1gInSc1
odkázal	odkázat	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
bustu	busta	k1gFnSc4
<g/>
,	,	kIx,
sbírku	sbírka	k1gFnSc4
sádrových	sádrový	k2eAgInPc2d1
odlitků	odlitek	k1gInPc2
a	a	k8xC
čestných	čestný	k2eAgInPc2d1
klíčů	klíč	k1gInPc2
tajného	tajný	k2eAgMnSc2d1
komořího	komoří	k1gMnSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
pozůstalosti	pozůstalost	k1gFnSc2
Ulriky	Ulrika	k1gFnSc2
von	von	k1gInSc1
Lewetzow	Lewetzow	k1gFnSc2
se	se	k3xPyFc4
Klebelsbergův	Klebelsbergův	k2eAgMnSc1d1
mobilář	mobilář	k1gMnSc1
ze	z	k7c2
zámku	zámek	k1gInSc2
v	v	k7c6
Třebívlicích	Třebívlice	k1gFnPc6
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
muzea	muzeum	k1gNnSc2
města	město	k1gNnSc2
Mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc4d1
část	část	k1gFnSc4
odeslali	odeslat	k5eAaPmAgMnP
sudetoněmečtí	sudetoněmecký	k2eAgMnPc1d1
radní	radní	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
do	do	k7c2
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
Goethova	Goethův	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
ve	v	k7c6
Výmaru	Výmar	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
vrátila	vrátit	k5eAaPmAgFnS
zpět	zpět	k6eAd1
do	do	k7c2
Mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
jsou	být	k5eAaImIp3nP
tak	tak	k9
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
Oblastního	oblastní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
a	a	k8xC
galerie	galerie	k1gFnSc2
v	v	k7c6
Mostě	most	k1gInSc6
a	a	k8xC
v	v	k7c6
památníku	památník	k1gInSc6
města	město	k1gNnSc2
Třebívlice	Třebívlice	k1gFnSc2
v	v	k7c6
zahradním	zahradní	k2eAgInSc6d1
domku	domek	k1gInSc6
tamějšího	tamější	k2eAgInSc2d1
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Klebelsbergova	Klebelsbergův	k2eAgFnSc1d1
písemná	písemný	k2eAgFnSc1d1
pozůstalost	pozůstalost	k1gFnSc1
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
dvou	dva	k4xCgInPc2
fondů	fond	k1gInPc2
<g/>
:	:	kIx,
Osobní	osobní	k2eAgFnPc1d1
korespondence	korespondence	k1gFnPc1
a	a	k8xC
fragmenty	fragment	k1gInPc1
genealogického	genealogický	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
jsou	být	k5eAaImIp3nP
uloženy	uložit	k5eAaPmNgFnP
v	v	k7c6
Archivu	archiv	k1gInSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
fragmenty	fragment	k1gInPc1
knihovny	knihovna	k1gFnSc2
v	v	k7c6
Knihovně	knihovna	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
evidence	evidence	k1gFnSc2
v	v	k7c6
Památníku	památník	k1gInSc6
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Rudolph	Rudolph	k1gMnSc1
Johann	Johann	k1gMnSc1
Meraviglia-Crivelli	Meraviglia-Crivell	k1gMnSc3
<g/>
,	,	kIx,
Der	drát	k5eAaImRp2nS
böhmische	böhmische	k1gNnSc4
Adel	Adel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nürnberg	Nürnberg	k1gInSc1
1886	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
130	#num#	k4
<g/>
,	,	kIx,
tab	tab	kA
<g/>
.	.	kIx.
64	#num#	k4
<g/>
↑	↑	k?
Rudolph	Rudolph	k1gMnSc1
Johann	Johann	k1gMnSc1
Meraviglia-Crivelli	Meraviglia-Crivell	k1gMnSc3
<g/>
,	,	kIx,
Der	drát	k5eAaImRp2nS
böhmische	böhmische	k1gNnSc4
Adel	Adel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nürnberg	Nürnberg	k1gInSc1
1886	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
130	#num#	k4
<g/>
,	,	kIx,
tab	tab	kA
<g/>
.	.	kIx.
64	#num#	k4
<g/>
↑	↑	k?
Knihovna	knihovna	k1gFnSc1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
↑	↑	k?
Dana	Dana	k1gFnSc1
Stehlíková	Stehlíková	k1gFnSc1
<g/>
,	,	kIx,
More	mor	k1gInSc5
valuable	valuable	k6eAd1
than	than	k1gNnSc4
originals	originalsa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Plaster	Plaster	k1gInSc1
cast	cast	k2eAgInSc4d1
collection	collection	k1gInSc4
in	in	k?
the	the	k?
National	National	k1gMnSc1
Museum	museum	k1gNnSc1
in	in	k?
Prague	Pragu	k1gInSc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Plaster	Plaster	k1gInSc1
casts	casts	k1gInSc1
:	:	kIx,
Making	Making	k1gInSc1
<g/>
,	,	kIx,
Collecting	Collecting	k1gInSc1
and	and	k?
Displaying	Displaying	k1gInSc1
from	from	k1gInSc1
Classical	Classical	k1gMnSc2
Antiquity	Antiquita	k1gMnSc2
to	ten	k3xDgNnSc1
the	the	k?
Present	Present	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
de	de	k?
De	De	k?
Gruyter	Gruytra	k1gFnPc2
Berlin-New	Berlin-New	k1gFnSc1
York	York	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
517-538	517-538	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HANUŠ	Hanuš	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgNnSc1d1
museum	museum	k1gNnSc1
a	a	k8xC
naše	náš	k3xOp1gNnSc4
obrození	obrození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I-II	I-II	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1921	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
::	::	k?
Šlechtické	šlechtický	k2eAgInPc1d1
rody	rod	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
od	od	k7c2
Bílé	bílý	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
,	,	kIx,
díl	díl	k1gInSc4
I	I	kA
<g/>
,	,	kIx,
A-	A-	k1gFnSc1
<g/>
M.	M.	kA
Praha	Praha	k1gFnSc1
Argo	Argo	k1gNnSc1
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
460	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SKLENÁŘ	Sklenář	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
Obraz	obraz	k1gInSc1
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
příběh	příběh	k1gInSc1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
-	-	kIx~
Litomyšl	Litomyšl	k1gFnSc1
:	:	kIx,
Paseka	paseka	k1gFnSc1
2005	#num#	k4
</s>
<s>
ŠLAJSNA	šlajsna	k1gFnSc1
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Hrabě	Hrabě	k1gMnSc1
Jeroným	Jeroným	k1gMnSc1
František	František	k1gMnSc1
Tadeáš	Tadeáš	k1gMnSc1
Michael	Michael	k1gMnSc1
Maria	Maria	k1gFnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
na	na	k7c6
Thumburgu	Thumburg	k1gInSc6
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Pod	pod	k7c7
Hněvínem	Hněvín	k1gInSc7
5	#num#	k4
<g/>
,	,	kIx,
Oblastní	oblastní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
a	a	k8xC
galerie	galerie	k1gFnSc1
v	v	k7c6
Mostě	most	k1gInSc6
<g/>
,	,	kIx,
Most	most	k1gInSc1
2020	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
z	z	k7c2
Klebelsbergu	Klebelsberg	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
122966015	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
1225	#num#	k4
1182	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
52584746	#num#	k4
</s>
