<s>
Zánět	zánět	k1gInSc1	zánět
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
inflammatio	inflammatio	k6eAd1	inflammatio
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
φ	φ	k?	φ
–	–	k?	–
flogósis	flogósis	k1gInSc1	flogósis
<g/>
;	;	kIx,	;
zastar	zastar	k1gInSc1	zastar
<g/>
.	.	kIx.	.
lid	lid	k1gInSc1	lid
<g/>
.	.	kIx.	.
prant	prant	k1gInSc1	prant
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složitá	složitý	k2eAgFnSc1d1	složitá
reakce	reakce	k1gFnSc1	reakce
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
na	na	k7c6	na
poškození	poškození	k1gNnSc6	poškození
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
komplex	komplex	k1gInSc1	komplex
biochemických	biochemický	k2eAgFnPc2d1	biochemická
a	a	k8xC	a
imunologických	imunologický	k2eAgFnPc2d1	imunologická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
definic	definice	k1gFnPc2	definice
zánětu	zánět	k1gInSc2	zánět
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vypovídací	vypovídací	k2eAgFnSc4d1	vypovídací
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uvedeno	uvést	k5eAaPmNgNnS	uvést
více	hodně	k6eAd2	hodně
definic	definice	k1gFnPc2	definice
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
stereotypní	stereotypní	k2eAgInSc1d1	stereotypní
<g/>
,	,	kIx,	,
složitá	složitý	k2eAgFnSc1d1	složitá
vývojem	vývoj	k1gInSc7	vývoj
získaná	získaný	k2eAgFnSc1d1	získaná
schopnost	schopnost	k1gFnSc1	schopnost
reakce	reakce	k1gFnSc2	reakce
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
poškození	poškození	k1gNnPc4	poškození
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgInPc4d1	sestávající
se	se	k3xPyFc4	se
z	z	k7c2	z
jevů	jev	k1gInPc2	jev
charakteru	charakter	k1gInSc2	charakter
alterativního	alterativní	k2eAgInSc2d1	alterativní
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnPc1	poškození
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
exsudativního	exsudativní	k2eAgInSc2d1	exsudativní
<g/>
,	,	kIx,	,
proliferativního	proliferativní	k2eAgInSc2d1	proliferativní
a	a	k8xC	a
imunitního	imunitní	k2eAgInSc2d1	imunitní
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obvykle	obvykle	k6eAd1	obvykle
probíhají	probíhat	k5eAaImIp3nP	probíhat
sukcesivně	sukcesivně	k6eAd1	sukcesivně
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
zánět	zánět	k1gInSc4	zánět
reparativní	reparativní	k2eAgInSc4d1	reparativní
anebo	anebo	k8xC	anebo
simultánně	simultánně	k6eAd1	simultánně
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
zánět	zánět	k1gInSc4	zánět
defenzivní	defenzivní	k2eAgMnSc1d1	defenzivní
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
nástrojem	nástroj	k1gInSc7	nástroj
vrozené	vrozený	k2eAgFnSc2d1	vrozená
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výrazem	výraz	k1gInSc7	výraz
ochranné	ochranný	k2eAgFnSc2d1	ochranná
přizpůsobivosti	přizpůsobivost	k1gFnSc2	přizpůsobivost
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
regulačních	regulační	k2eAgInPc2d1	regulační
mechanismů	mechanismus	k1gInPc2	mechanismus
homeostázy	homeostáza	k1gFnSc2	homeostáza
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
obranou	obrana	k1gFnSc7	obrana
a	a	k8xC	a
také	také	k9	také
sebepoškozující	sebepoškozující	k2eAgFnSc7d1	sebepoškozující
reakcí	reakce	k1gFnSc7	reakce
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
nejde	jít	k5eNaImIp3nS	jít
odlišit	odlišit	k5eAaPmF	odlišit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zánětlivá	zánětlivý	k2eAgFnSc1d1	zánětlivá
reakce	reakce	k1gFnSc1	reakce
mobilizovaná	mobilizovaný	k2eAgFnSc1d1	mobilizovaná
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
organismus	organismus	k1gInSc1	organismus
zbytečně	zbytečně	k6eAd1	zbytečně
poškozován	poškozovat	k5eAaImNgInS	poškozovat
<g/>
.	.	kIx.	.
</s>
<s>
Zánětlivá	zánětlivý	k2eAgFnSc1d1	zánětlivá
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
vaskularizované	vaskularizovaný	k2eAgFnSc6d1	vaskularizovaná
pojivové	pojivový	k2eAgFnSc6d1	pojivová
tkáni	tkáň	k1gFnSc6	tkáň
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
cirkulující	cirkulující	k2eAgFnPc1d1	cirkulující
krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgFnPc1d1	krevní
cévy	céva	k1gFnPc1	céva
<g/>
,	,	kIx,	,
a	a	k8xC	a
buněčné	buněčný	k2eAgFnPc1d1	buněčná
a	a	k8xC	a
mezibuněčné	mezibuněčný	k2eAgFnPc1d1	mezibuněčná
složky	složka	k1gFnPc1	složka
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
Buňky	buňka	k1gFnPc4	buňka
poškozené	poškozený	k2eAgFnSc2d1	poškozená
tkáně	tkáň	k1gFnSc2	tkáň
uvolní	uvolnit	k5eAaPmIp3nP	uvolnit
chemické	chemický	k2eAgFnPc1d1	chemická
signalizační	signalizační	k2eAgFnPc1d1	signalizační
molekuly	molekula	k1gFnPc1	molekula
(	(	kIx(	(
<g/>
histamin	histamin	k1gInSc1	histamin
a	a	k8xC	a
prostaglandin	prostaglandin	k2eAgInSc1d1	prostaglandin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
Rozšíření	rozšíření	k1gNnSc1	rozšíření
blízkých	blízký	k2eAgFnPc2d1	blízká
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
více	hodně	k6eAd2	hodně
propustné	propustný	k2eAgInPc1d1	propustný
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
srážlivé	srážlivý	k2eAgInPc1d1	srážlivý
elementy	element	k1gInPc1	element
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
proces	proces	k1gInSc1	proces
srážení	srážení	k1gNnSc2	srážení
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
Chemotaktické	chemotaktický	k2eAgInPc1d1	chemotaktický
faktory	faktor	k1gInPc1	faktor
(	(	kIx(	(
<g/>
chemokiny	chemokina	k1gFnPc1	chemokina
<g/>
)	)	kIx)	)
uvolněné	uvolněný	k2eAgFnPc1d1	uvolněná
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
buněk	buňka	k1gFnPc2	buňka
lákají	lákat	k5eAaImIp3nP	lákat
fagocytární	fagocytární	k2eAgFnPc1d1	fagocytární
buňky	buňka	k1gFnPc1	buňka
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
fáze	fáze	k1gFnSc2	fáze
<g/>
:	:	kIx,	:
Fagocytární	fagocytární	k2eAgFnPc1d1	fagocytární
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
ke	k	k7c3	k
zraněnému	zraněný	k2eAgNnSc3d1	zraněné
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
patogeny	patogen	k1gInPc4	patogen
a	a	k8xC	a
zbytky	zbytek	k1gInPc4	zbytek
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
uzdravení	uzdravení	k1gNnSc1	uzdravení
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
mírně	mírně	k6eAd1	mírně
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Neživé	živý	k2eNgFnPc1d1	neživá
příčiny	příčina	k1gFnPc1	příčina
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
chemické	chemický	k2eAgInPc1d1	chemický
nebo	nebo	k8xC	nebo
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
faktory	faktor	k1gInPc1	faktor
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
aseptický	aseptický	k2eAgInSc1d1	aseptický
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c6	o
poškození	poškození	k1gNnSc6	poškození
tkáně	tkáň	k1gFnSc2	tkáň
při	při	k7c6	při
chirurgickém	chirurgický	k2eAgInSc6d1	chirurgický
zákroku	zákrok	k1gInSc6	zákrok
<g/>
,	,	kIx,	,
traumatickém	traumatický	k2eAgNnSc6d1	traumatické
poškození	poškození	k1gNnSc6	poškození
(	(	kIx(	(
<g/>
zlomeniny	zlomenina	k1gFnPc4	zlomenina
<g/>
,	,	kIx,	,
přetržení	přetržení	k1gNnSc4	přetržení
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
či	či	k8xC	či
popálení	popálení	k1gNnSc1	popálení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
netvoří	tvořit	k5eNaImIp3nP	tvořit
se	se	k3xPyFc4	se
protilátky	protilátka	k1gFnPc1	protilátka
proti	proti	k7c3	proti
příčině	příčina	k1gFnSc3	příčina
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
proti	proti	k7c3	proti
poškozeným	poškozený	k2eAgFnPc3d1	poškozená
buňkám	buňka	k1gFnPc3	buňka
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
zánětu	zánět	k1gInSc6	zánět
reparativním	reparativní	k2eAgInSc6d1	reparativní
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgFnPc1d1	živá
příčiny	příčina	k1gFnPc1	příčina
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
antigenní	antigenní	k2eAgFnSc2d1	antigenní
povahy	povaha	k1gFnSc2	povaha
(	(	kIx(	(
<g/>
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
parazité	parazit	k1gMnPc1	parazit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
složitou	složitý	k2eAgFnSc4d1	složitá
reakci	reakce	k1gFnSc4	reakce
a	a	k8xC	a
zánět	zánět	k1gInSc4	zánět
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
jako	jako	k8xC	jako
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
zánět	zánět	k1gInSc1	zánět
obranný	obranný	k2eAgInSc1d1	obranný
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zánět	zánět	k1gInSc1	zánět
projevuje	projevovat	k5eAaImIp3nS	projevovat
<g/>
,	,	kIx,	,
věděli	vědět	k5eAaImAgMnP	vědět
už	už	k6eAd1	už
lékaři	lékař	k1gMnPc1	lékař
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
popsal	popsat	k5eAaPmAgMnS	popsat
4	[number]	k4	4
základní	základní	k2eAgInPc1d1	základní
příznaky	příznak	k1gInPc1	příznak
(	(	kIx(	(
<g/>
rubor	rubor	k1gInSc1	rubor
<g/>
,	,	kIx,	,
tumor	tumor	k1gInSc1	tumor
<g/>
,	,	kIx,	,
calor	calor	k1gMnSc1	calor
<g/>
,	,	kIx,	,
dolor	dolor	k1gMnSc1	dolor
<g/>
)	)	kIx)	)
římský	římský	k2eAgMnSc1d1	římský
lékař	lékař	k1gMnSc1	lékař
Aulus	Aulus	k1gMnSc1	Aulus
Cornelius	Cornelius	k1gMnSc1	Cornelius
Celsus	Celsus	k1gMnSc1	Celsus
<g/>
.	.	kIx.	.
</s>
<s>
Virchow	Virchow	k?	Virchow
později	pozdě	k6eAd2	pozdě
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
čtyřem	čtyři	k4xCgInPc3	čtyři
ještě	ještě	k9	ještě
functio	functio	k1gMnSc1	functio
laesa	laes	k1gMnSc2	laes
(	(	kIx(	(
<g/>
porušení	porušení	k1gNnSc1	porušení
funkce	funkce	k1gFnSc2	funkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rubor	rubor	k1gInSc1	rubor
=	=	kIx~	=
zčervenání	zčervenání	k1gNnSc1	zčervenání
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
hyperémie	hyperémie	k1gFnSc2	hyperémie
zánětlivého	zánětlivý	k2eAgNnSc2d1	zánětlivé
ložiska	ložisko	k1gNnSc2	ložisko
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jak	jak	k6eAd1	jak
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
cévní	cévní	k2eAgFnSc6d1	cévní
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
množství	množství	k1gNnSc1	množství
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
naplněných	naplněný	k2eAgFnPc2d1	naplněná
krví	krev	k1gFnPc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Tumor	tumor	k1gInSc1	tumor
=	=	kIx~	=
otok	otok	k1gInSc1	otok
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
zvětšení	zvětšení	k1gNnSc1	zvětšení
objemu	objem	k1gInSc2	objem
tkání	tkáň	k1gFnPc2	tkáň
<g/>
;	;	kIx,	;
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
objemem	objem	k1gInSc7	objem
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
ložisku	ložisko	k1gNnSc6	ložisko
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
výstupem	výstup	k1gInSc7	výstup
tekutiny	tekutina	k1gFnSc2	tekutina
a	a	k8xC	a
krevních	krevní	k2eAgFnPc2d1	krevní
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
zvaný	zvaný	k2eAgInSc1d1	zvaný
exsudace	exsudace	k1gFnSc2	exsudace
a	a	k8xC	a
infiltrace	infiltrace	k1gFnSc2	infiltrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Calor	Calor	k1gInSc1	Calor
=	=	kIx~	=
zteplání	zteplání	k1gNnSc1	zteplání
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
průtokem	průtok	k1gInSc7	průtok
krve	krev	k1gFnSc2	krev
ložiskem	ložisko	k1gNnSc7	ložisko
(	(	kIx(	(
<g/>
hyperémie	hyperémie	k1gFnSc2	hyperémie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
intenzitou	intenzita	k1gFnSc7	intenzita
katabolických	katabolický	k2eAgInPc2d1	katabolický
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
pyrogenních	pyrogenní	k2eAgFnPc2d1	pyrogenní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dolor	Dolor	k1gInSc1	Dolor
=	=	kIx~	=
bolest	bolest	k1gFnSc1	bolest
<g/>
;	;	kIx,	;
způsobena	způsoben	k2eAgFnSc1d1	způsobena
biochemickými	biochemický	k2eAgFnPc7d1	biochemická
<g/>
,	,	kIx,	,
fyzikálně-chemickými	fyzikálněhemický	k2eAgFnPc7d1	fyzikálně-chemická
a	a	k8xC	a
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
zánětlivém	zánětlivý	k2eAgNnSc6d1	zánětlivé
ložisku	ložisko	k1gNnSc6	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
hromadění	hromadění	k1gNnSc4	hromadění
kyselých	kyselý	k2eAgFnPc2d1	kyselá
metabolických	metabolický	k2eAgFnPc2d1	metabolická
zplodin	zplodina	k1gFnPc2	zplodina
(	(	kIx(	(
<g/>
acidóza	acidóza	k1gFnSc1	acidóza
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
onkotický	onkotický	k2eAgInSc1d1	onkotický
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
,	,	kIx,	,
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
koncentraci	koncentrace	k1gFnSc4	koncentrace
draselných	draselný	k2eAgInPc2d1	draselný
a	a	k8xC	a
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
kationů	kation	k1gInPc2	kation
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
mechanický	mechanický	k2eAgInSc1d1	mechanický
tlak	tlak	k1gInSc1	tlak
tkáně	tkáň	k1gFnSc2	tkáň
působící	působící	k2eAgMnPc1d1	působící
na	na	k7c4	na
nervová	nervový	k2eAgNnPc4d1	nervové
zakončení	zakončení	k1gNnPc4	zakončení
v	v	k7c6	v
ložisku	ložisko	k1gNnSc6	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Functio	Functio	k6eAd1	Functio
laesa	laesa	k1gFnSc1	laesa
=	=	kIx~	=
porucha	porucha	k1gFnSc1	porucha
funkce	funkce	k1gFnSc2	funkce
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
způsobena	způsoben	k2eAgFnSc1d1	způsobena
poškozením	poškození	k1gNnSc7	poškození
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
poruchami	porucha	k1gFnPc7	porucha
krevního	krevní	k2eAgInSc2d1	krevní
a	a	k8xC	a
lymfatického	lymfatický	k2eAgInSc2d1	lymfatický
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
reflexním	reflexní	k2eAgInSc7d1	reflexní
útlumem	útlum	k1gInSc7	útlum
aktivity	aktivita	k1gFnSc2	aktivita
postiženého	postižený	k2eAgInSc2d1	postižený
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Alterace	alterace	k1gFnSc1	alterace
–	–	k?	–
poškození	poškození	k1gNnSc2	poškození
buněk	buňka	k1gFnPc2	buňka
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
degenerace	degenerace	k1gFnSc2	degenerace
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Exsudace	Exsudace	k1gFnSc1	Exsudace
a	a	k8xC	a
infiltrace	infiltrace	k1gFnSc1	infiltrace
<g/>
:	:	kIx,	:
hemodynamické	hemodynamický	k2eAgFnSc2d1	hemodynamická
změny	změna	k1gFnSc2	změna
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
(	(	kIx(	(
<g/>
vazodilatace	vazodilatace	k1gFnSc1	vazodilatace
<g/>
)	)	kIx)	)
arteriol	arteriol	k1gInSc1	arteriol
<g/>
,	,	kIx,	,
kapilár	kapilára	k1gFnPc2	kapilára
a	a	k8xC	a
venul	venula	k1gFnPc2	venula
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
krev	krev	k1gFnSc1	krev
zpomalí	zpomalet	k5eAaPmIp3nS	zpomalet
až	až	k9	až
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
stagnaci	stagnace	k1gFnSc3	stagnace
krevního	krevní	k2eAgInSc2d1	krevní
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
zánětlivém	zánětlivý	k2eAgNnSc6d1	zánětlivé
ložisku	ložisko	k1gNnSc6	ložisko
(	(	kIx(	(
<g/>
hyperémie	hyperémie	k1gFnSc1	hyperémie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
propustnost	propustnost	k1gFnSc1	propustnost
(	(	kIx(	(
<g/>
permeabilita	permeabilita	k1gFnSc1	permeabilita
<g/>
)	)	kIx)	)
cév	céva	k1gFnPc2	céva
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
tekutiny	tekutina	k1gFnSc2	tekutina
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
z	z	k7c2	z
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
zánětlivý	zánětlivý	k2eAgInSc1d1	zánětlivý
otok	otok	k1gInSc1	otok
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
krevních	krevní	k2eAgFnPc2d1	krevní
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
neutrofily	neutrofil	k1gMnPc4	neutrofil
<g/>
,	,	kIx,	,
monocyty	monocyt	k1gInPc1	monocyt
<g/>
,	,	kIx,	,
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
<g/>
)	)	kIx)	)
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
zánětlivého	zánětlivý	k2eAgNnSc2d1	zánětlivé
ložiska	ložisko	k1gNnSc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Proliferace	proliferace	k1gFnSc1	proliferace
–	–	k?	–
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
reparace	reparace	k1gFnSc2	reparace
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
neokapilarizaci	neokapilarizace	k1gFnSc4	neokapilarizace
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tvorba	tvorba	k1gFnSc1	tvorba
nových	nový	k2eAgFnPc2d1	nová
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
nové	nový	k2eAgFnSc2d1	nová
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
chronický	chronický	k2eAgInSc4d1	chronický
zánět	zánět	k1gInSc4	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Imunitní	imunitní	k2eAgInPc1d1	imunitní
jevy	jev	k1gInPc1	jev
–	–	k?	–
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
humorální	humorální	k2eAgFnPc4d1	humorální
složky	složka	k1gFnPc4	složka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
jakož	jakož	k8xC	jakož
i	i	k9	i
specifickou	specifický	k2eAgFnSc4d1	specifická
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
zastoupenou	zastoupený	k2eAgFnSc7d1	zastoupená
B-lymfocyty	Bymfocyt	k1gInPc7	B-lymfocyt
a	a	k8xC	a
T-lymfocyty	Tymfocyt	k1gInPc7	T-lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časového	časový	k2eAgInSc2d1	časový
projevu	projev	k1gInSc2	projev
dělíme	dělit	k5eAaImIp1nP	dělit
záněty	zánět	k1gInPc4	zánět
na	na	k7c6	na
akutní	akutní	k2eAgFnSc6d1	akutní
a	a	k8xC	a
chronické	chronický	k2eAgFnSc6d1	chronická
<g/>
.	.	kIx.	.
</s>
<s>
Chronický	chronický	k2eAgInSc1d1	chronický
zánět	zánět	k1gInSc1	zánět
může	moct	k5eAaImIp3nS	moct
vznikat	vznikat	k5eAaImF	vznikat
jak	jak	k6eAd1	jak
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
nevyhojeného	vyhojený	k2eNgInSc2d1	vyhojený
akutního	akutní	k2eAgInSc2d1	akutní
zánětu	zánět	k1gInSc2	zánět
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pozvolným	pozvolný	k2eAgInSc7d1	pozvolný
plíživým	plíživý	k2eAgInSc7d1	plíživý
rozvojem	rozvoj	k1gInSc7	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
zánětu	zánět	k1gInSc2	zánět
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
záněty	zánět	k1gInPc4	zánět
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
formách	forma	k1gFnPc6	forma
zánětu	zánět	k1gInSc2	zánět
<g/>
:	:	kIx,	:
specifické	specifický	k2eAgInPc1d1	specifický
záněty	zánět	k1gInPc1	zánět
-	-	kIx~	-
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pro	pro	k7c4	pro
vyvolávající	vyvolávající	k2eAgNnSc4d1	vyvolávající
agens	agens	k1gNnSc4	agens
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
například	například	k6eAd1	například
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
aktinomykóza	aktinomykóza	k1gFnSc1	aktinomykóza
nebo	nebo	k8xC	nebo
lepra	lepra	k1gFnSc1	lepra
<g/>
.	.	kIx.	.
nespecifické	specifický	k2eNgInPc1d1	nespecifický
záněty	zánět	k1gInPc1	zánět
-	-	kIx~	-
zánět	zánět	k1gInSc1	zánět
probíhá	probíhat	k5eAaImIp3nS	probíhat
poměrně	poměrně	k6eAd1	poměrně
uniformním	uniformní	k2eAgInSc7d1	uniformní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
z	z	k7c2	z
morfologie	morfologie	k1gFnSc2	morfologie
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
agens	agens	k1gInSc4	agens
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
trvání	trvání	k1gNnSc2	trvání
<g/>
:	:	kIx,	:
Akutní	akutní	k2eAgFnPc1d1	akutní
-	-	kIx~	-
nejdéle	dlouho	k6eAd3	dlouho
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
odezní	odeznět	k5eAaPmIp3nS	odeznět
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
<g/>
,	,	kIx,	,
výborná	výborná	k1gFnSc1	výborná
regenerace	regenerace	k1gFnSc2	regenerace
Chronický	chronický	k2eAgMnSc1d1	chronický
-	-	kIx~	-
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
jejímu	její	k3xOp3gInSc3	její
nahrazení	nahrazený	k2eAgMnPc1d1	nahrazený
vazivem	vazivo	k1gNnSc7	vazivo
Nespecifické	specifický	k2eNgInPc1d1	nespecifický
záněty	zánět	k1gInPc1	zánět
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
převažující	převažující	k2eAgFnSc2d1	převažující
formy	forma	k1gFnSc2	forma
na	na	k7c4	na
zánět	zánět	k1gInSc4	zánět
<g/>
:	:	kIx,	:
alterativní	alterativní	k2eAgInSc4d1	alterativní
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
parenchymatózní	parenchymatózní	k2eAgMnSc1d1	parenchymatózní
<g/>
)	)	kIx)	)
-	-	kIx~	-
převažuje	převažovat	k5eAaImIp3nS	převažovat
poškození	poškození	k1gNnSc1	poškození
tkáně	tkáň	k1gFnSc2	tkáň
exsudativní	exsudativní	k2eAgFnSc2d1	exsudativní
-	-	kIx~	-
převažuje	převažovat	k5eAaImIp3nS	převažovat
produkce	produkce	k1gFnSc1	produkce
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
exsudátu	exsudát	k1gInSc2	exsudát
<g/>
)	)	kIx)	)
sérozní	sérozní	k2eAgInSc1d1	sérozní
-	-	kIx~	-
exsudát	exsudát	k1gInSc1	exsudát
je	být	k5eAaImIp3nS	být
vodnatý	vodnatý	k2eAgInSc1d1	vodnatý
nehnisavý	hnisavý	k2eNgInSc1d1	nehnisavý
(	(	kIx(	(
<g/>
nonpurulentní	nonpurulentní	k2eAgInSc1d1	nonpurulentní
<g/>
,	,	kIx,	,
lymfoplazmocytární	lymfoplazmocytární	k2eAgInSc1d1	lymfoplazmocytární
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
exsudátu	exsudát	k1gInSc6	exsudát
převažují	převažovat	k5eAaImIp3nP	převažovat
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
a	a	k8xC	a
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
buňky	buňka	k1gFnSc2	buňka
hnisavý	hnisavý	k2eAgMnSc1d1	hnisavý
(	(	kIx(	(
<g/>
purulentní	purulentní	k2eAgMnSc1d1	purulentní
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
exsudátu	exsudát	k1gInSc6	exsudát
převažují	převažovat	k5eAaImIp3nP	převažovat
<g />
.	.	kIx.	.
</s>
<s>
granulocyty	granulocyt	k1gInPc1	granulocyt
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
především	především	k9	především
neutrofily	neutrofil	k1gMnPc4	neutrofil
<g/>
)	)	kIx)	)
fibrinózní	fibrinózní	k2eAgMnPc4d1	fibrinózní
-	-	kIx~	-
exsudát	exsudát	k1gInSc1	exsudát
je	být	k5eAaImIp3nS	být
zpevněn	zpevněn	k2eAgMnSc1d1	zpevněn
vlákny	vlákna	k1gFnPc4	vlákna
fibrinu	fibrin	k1gInSc2	fibrin
krupózní	krupózní	k2eAgFnSc2d1	krupózní
difteroidní	difteroidní	k2eAgFnSc2d1	difteroidní
eschariotický	eschariotický	k2eAgInSc4d1	eschariotický
(	(	kIx(	(
<g/>
příškavarový	příškavarový	k2eAgInSc4d1	příškavarový
<g/>
)	)	kIx)	)
gangrenózní	gangrenózní	k2eAgInSc4d1	gangrenózní
-	-	kIx~	-
fibrinózní	fibrinózní	k2eAgInSc4d1	fibrinózní
exsudát	exsudát	k1gInSc4	exsudát
je	být	k5eAaImIp3nS	být
sekundárně	sekundárně	k6eAd1	sekundárně
modifikován	modifikovat	k5eAaBmNgInS	modifikovat
ischémií	ischémie	k1gFnSc7	ischémie
nebo	nebo	k8xC	nebo
nekrózou	nekróza	k1gFnSc7	nekróza
proliferativní	proliferativní	k2eAgFnSc7d1	proliferativní
(	(	kIx(	(
<g/>
produktivní	produktivní	k2eAgFnSc7d1	produktivní
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlavním	hlavní	k2eAgInSc7d1	hlavní
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
vaziva	vazivo	k1gNnSc2	vazivo
Základní	základní	k2eAgFnSc7d1	základní
charakteristikou	charakteristika	k1gFnSc7	charakteristika
alterativního	alterativní	k2eAgInSc2d1	alterativní
zánětu	zánět	k1gInSc2	zánět
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrazně	výrazně	k6eAd1	výrazně
převažuje	převažovat	k5eAaImIp3nS	převažovat
poškození	poškození	k1gNnSc1	poškození
(	(	kIx(	(
<g/>
alterace	alterace	k1gFnPc1	alterace
<g/>
)	)	kIx)	)
buněčných	buněčný	k2eAgFnPc2d1	buněčná
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Postihuje	postihovat	k5eAaImIp3nS	postihovat
především	především	k9	především
parenchym	parenchym	k1gInSc1	parenchym
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
vazivové	vazivový	k2eAgNnSc1d1	vazivové
stroma	stroma	k1gNnSc1	stroma
je	být	k5eAaImIp3nS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
řidčeji	řídce	k6eAd2	řídce
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgInPc1d1	čistý
alterativní	alterativní	k2eAgInPc1d1	alterativní
záněty	zánět	k1gInPc1	zánět
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
mikrobiálně	mikrobiálně	k6eAd1	mikrobiálně
podmíněná	podmíněný	k2eAgFnSc1d1	podmíněná
nekróza	nekróza	k1gFnSc1	nekróza
u	u	k7c2	u
agranulocytózy	agranulocytóza	k1gFnSc2	agranulocytóza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
serózního	serózní	k2eAgInSc2d1	serózní
zánětu	zánět	k1gInSc2	zánět
převažuje	převažovat	k5eAaImIp3nS	převažovat
produkce	produkce	k1gFnSc1	produkce
vodnatého	vodnatý	k2eAgInSc2d1	vodnatý
exsudátu	exsudát	k1gInSc2	exsudát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
probíhá	probíhat	k5eAaImIp3nS	probíhat
serózní	serózní	k2eAgInSc4d1	serózní
zánět	zánět	k1gInSc4	zánět
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
míšení	míšení	k1gNnSc3	míšení
exsudátu	exsudát	k1gInSc2	exsudát
s	s	k7c7	s
hlenem	hlen	k1gInSc7	hlen
a	a	k8xC	a
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
katarálním	katarální	k2eAgInSc6d1	katarální
zánětu	zánět	k1gInSc6	zánět
(	(	kIx(	(
<g/>
kataru	katar	k1gInSc2	katar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytujícím	vyskytující	k2eAgInSc7d1	vyskytující
serózním	serózní	k2eAgInSc7d1	serózní
zánětem	zánět	k1gInSc7	zánět
je	být	k5eAaImIp3nS	být
katarální	katarální	k2eAgFnSc1d1	katarální
rhinitida	rhinitida	k1gFnSc1	rhinitida
(	(	kIx(	(
<g/>
rýma	rýma	k1gFnSc1	rýma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
zánětu	zánět	k1gInSc2	zánět
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
obvykle	obvykle	k6eAd1	obvykle
nepříliš	příliš	k6eNd1	příliš
hojný	hojný	k2eAgInSc1d1	hojný
vodnatý	vodnatý	k2eAgInSc1d1	vodnatý
exsudát	exsudát	k1gInSc1	exsudát
a	a	k8xC	a
infiltrace	infiltrace	k1gFnSc1	infiltrace
tkáně	tkáň	k1gFnSc2	tkáň
především	především	k6eAd1	především
lymfocyty	lymfocyt	k1gInPc7	lymfocyt
a	a	k8xC	a
plazmatickými	plazmatický	k2eAgFnPc7d1	plazmatická
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
chronická	chronický	k2eAgFnSc1d1	chronická
gastritida	gastritida	k1gFnSc1	gastritida
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
seróznímu	serózní	k2eAgInSc3d1	serózní
<g/>
,	,	kIx,	,
exsudát	exsudát	k1gInSc1	exsudát
je	být	k5eAaImIp3nS	být
však	však	k9	však
hojně	hojně	k6eAd1	hojně
prostoupen	prostoupit	k5eAaPmNgInS	prostoupit
rozpadajícími	rozpadající	k2eAgInPc7d1	rozpadající
se	se	k3xPyFc4	se
granulocyty	granulocyt	k1gInPc1	granulocyt
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c4	v
hnis	hnis	k1gInSc4	hnis
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
je	být	k5eAaImIp3nS	být
hnisavý	hnisavý	k2eAgInSc4d1	hnisavý
zánět	zánět	k1gInSc4	zánět
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c4	na
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
formami	forma	k1gFnPc7	forma
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
<g/>
:	:	kIx,	:
absces	absces	k1gInSc1	absces
-	-	kIx~	-
lokalizovaný	lokalizovaný	k2eAgInSc1d1	lokalizovaný
ohraničený	ohraničený	k2eAgInSc1d1	ohraničený
rozpad	rozpad	k1gInSc1	rozpad
tkáně	tkáň	k1gFnSc2	tkáň
flegmóna	flegmóna	k1gFnSc1	flegmóna
-	-	kIx~	-
hnisavý	hnisavý	k2eAgInSc1d1	hnisavý
zánět	zánět	k1gInSc1	zánět
šířící	šířící	k2eAgInSc1d1	šířící
se	se	k3xPyFc4	se
difúzně	difúzně	k6eAd1	difúzně
tkání	tkáň	k1gFnSc7	tkáň
Krupózní	krupózní	k2eAgInSc1d1	krupózní
zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
formou	forma	k1gFnSc7	forma
fibrinózního	fibrinózní	k2eAgInSc2d1	fibrinózní
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Fibrin	fibrin	k1gInSc1	fibrin
v	v	k7c6	v
exsudátu	exsudát	k1gInSc6	exsudát
nelne	lnout	k5eNaImIp3nS	lnout
ke	k	k7c3	k
sliznici	sliznice	k1gFnSc3	sliznice
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc4	poškození
epitelu	epitel	k1gInSc2	epitel
pod	pod	k7c7	pod
fibrinem	fibrin	k1gInSc7	fibrin
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgNnSc1d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
krupózní	krupózní	k2eAgFnSc1d1	krupózní
pneumonie	pneumonie	k1gFnSc1	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
podle	podle	k7c2	podle
záškrtu	záškrt	k1gInSc2	záškrt
(	(	kIx(	(
<g/>
difterie	difterie	k1gFnSc1	difterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
předchozího	předchozí	k2eAgNnSc2d1	předchozí
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fibrinová	fibrinový	k2eAgFnSc1d1	fibrinová
pablána	pablána	k1gFnSc1	pablána
lne	lnout	k5eAaImIp3nS	lnout
k	k	k7c3	k
epitelu	epitel	k1gInSc3	epitel
poměrně	poměrně	k6eAd1	poměrně
pevně	pevně	k6eAd1	pevně
<g/>
.	.	kIx.	.
</s>
<s>
Eschariotický	Eschariotický	k2eAgInSc1d1	Eschariotický
zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
vystupňovanou	vystupňovaný	k2eAgFnSc7d1	vystupňovaná
podobou	podoba	k1gFnSc7	podoba
zánětu	zánět	k1gInSc2	zánět
difterického	difterický	k2eAgInSc2d1	difterický
<g/>
.	.	kIx.	.
</s>
<s>
Sliznice	sliznice	k1gFnSc1	sliznice
pod	pod	k7c7	pod
fibrinovou	fibrinový	k2eAgFnSc7d1	fibrinová
pablánou	pablána	k1gFnSc7	pablána
podléhá	podléhat	k5eAaImIp3nS	podléhat
nekróze	nekróza	k1gFnSc3	nekróza
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
cárech	cár	k1gInPc6	cár
odtrhávat	odtrhávat	k5eAaImF	odtrhávat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
gangrenózním	gangrenózní	k2eAgInSc6d1	gangrenózní
zánětu	zánět	k1gInSc6	zánět
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
zánětem	zánět	k1gInSc7	zánět
poškozené	poškozený	k2eAgFnSc2d1	poškozená
tkáně	tkáň	k1gFnSc2	tkáň
sekundárně	sekundárně	k6eAd1	sekundárně
ischemické	ischemický	k2eAgFnPc1d1	ischemická
a	a	k8xC	a
sekundárně	sekundárně	k6eAd1	sekundárně
je	on	k3xPp3gInPc4	on
infikují	infikovat	k5eAaBmIp3nP	infikovat
hnilobné	hnilobný	k2eAgNnSc1d1	hnilobné
(	(	kIx(	(
<g/>
putridní	putridní	k2eAgFnSc1d1	putridní
<g/>
)	)	kIx)	)
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
terminologické	terminologický	k2eAgNnSc4d1	terminologické
odlišení	odlišení	k1gNnSc4	odlišení
zánětlivých	zánětlivý	k2eAgFnPc2d1	zánětlivá
změn	změna	k1gFnPc2	změna
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
patologických	patologický	k2eAgInPc2d1	patologický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
řeckému	řecký	k2eAgInSc3d1	řecký
názvu	název	k1gInSc3	název
postižené	postižený	k2eAgFnSc2d1	postižená
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
orgánu	orgán	k1gInSc2	orgán
<g/>
)	)	kIx)	)
přidává	přidávat	k5eAaImIp3nS	přidávat
přípona	přípona	k1gFnSc1	přípona
-itis	tis	k1gFnSc1	-itis
<g/>
,	,	kIx,	,
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
formě	forma	k1gFnSc6	forma
-itida	tid	k1gMnSc2	-itid
<g/>
.	.	kIx.	.
</s>
<s>
Významem	význam	k1gInSc7	význam
koncovky	koncovka	k1gFnSc2	koncovka
-itis	tis	k1gFnSc1	-itis
je	být	k5eAaImIp3nS	být
vytvoření	vytvoření	k1gNnSc4	vytvoření
přivlastňovacího	přivlastňovací	k2eAgNnSc2d1	přivlastňovací
adjektiva	adjektivum	k1gNnSc2	adjektivum
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
sousloví	sousloví	k1gNnSc6	sousloví
např.	např.	kA	např.
nephritis	nephritis	k1gFnSc1	nephritis
nosos	nosos	k1gMnSc1	nosos
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
nephron	nephron	k1gInSc1	nephron
ledvina	ledvina	k1gFnSc1	ledvina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
onemocnění	onemocnění	k1gNnSc1	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
nemoc	nemoc	k1gFnSc1	nemoc
ledvinina	ledvinin	k2eAgFnSc1d1	ledvinin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
z	z	k7c2	z
adjektiva	adjektivum	k1gNnSc2	adjektivum
stalo	stát	k5eAaPmAgNnS	stát
substantivum	substantivum	k1gNnSc1	substantivum
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Nověji	nově	k6eAd2	nově
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
označení	označení	k1gNnSc4	označení
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
latinským	latinský	k2eAgInSc7d1	latinský
kmenem	kmen	k1gInSc7	kmen
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
červovitého	červovitý	k2eAgInSc2d1	červovitý
přívěsku	přívěsek	k1gInSc2	přívěsek
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
apendicitis	apendicitis	k1gFnSc1	apendicitis
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
původně	původně	k6eAd1	původně
správné	správný	k2eAgNnSc1d1	správné
epityphlitis	epityphlitis	k1gFnSc1	epityphlitis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
-	-	kIx~	-
jednak	jednak	k8xC	jednak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nezánětlivé	zánětlivý	k2eNgFnPc1d1	nezánětlivá
poruchy	porucha	k1gFnPc1	porucha
pojmenované	pojmenovaný	k2eAgFnPc1d1	pojmenovaná
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-itis	tis	k1gFnSc2	-itis
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
názvu	název	k1gInSc6	název
koncovku	koncovka	k1gFnSc4	koncovka
-itis	tis	k1gFnSc4	-itis
<g/>
.	.	kIx.	.
</s>
