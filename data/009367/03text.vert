<p>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
déšť	déšť	k1gInSc4	déšť
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Karla	Karel	k1gMnSc2	Karel
Kachyni	Kachyně	k1gFnSc4	Kachyně
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
z	z	k7c2	z
konce	konec	k1gInSc2	konec
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
ze	z	k7c2	z
života	život	k1gInSc2	život
dvanáctileté	dvanáctiletý	k2eAgFnSc2d1	dvanáctiletá
Alenky	Alenka	k1gFnSc2	Alenka
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
panelovém	panelový	k2eAgNnSc6d1	panelové
sídlišti	sídliště	k1gNnSc6	sídliště
Bohnice	Bohnice	k1gInPc1	Bohnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Dita	Dita	k1gFnSc1	Dita
Kaplanová-	Kaplanová-	k1gFnSc1	Kaplanová-
Alena	Alena	k1gFnSc1	Alena
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Sedláček-	Sedláček-	k1gMnSc1	Sedláček-
Pavel	Pavel	k1gMnSc1	Pavel
</s>
</p>
<p>
<s>
Laďka	Laďka	k6eAd1	Laďka
Kozderková	Kozderkový	k2eAgFnSc1d1	Kozderková
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Řehoř-	Řehoř-	k1gMnSc1	Řehoř-
Taraba	Taraba	k1gMnSc1	Taraba
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Somr-	Somr-	k1gFnSc2	Somr-
Pavlův	Pavlův	k2eAgMnSc1d1	Pavlův
otec	otec	k1gMnSc1	otec
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Hofbauer-	Hofbauer-	k1gMnSc1	Hofbauer-
zrzek	zrzek	k1gMnSc1	zrzek
</s>
</p>
<p>
<s>
Monika	Monika	k1gFnSc1	Monika
Hálová	Hálová	k1gFnSc1	Hálová
-	-	kIx~	-
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
dívka	dívka	k1gFnSc1	dívka
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Trojan-	Trojan-	k1gMnSc1	Trojan-
Duda	Duda	k1gMnSc1	Duda
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Bartoška-	Bartoška-	k1gMnSc1	Bartoška-
Budil	Budil	k1gMnSc1	Budil
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Motlová-	Motlová-	k1gFnSc1	Motlová-
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
samoobsluze	samoobsluha	k1gFnSc6	samoobsluha
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
Slováček-	Slováček-	k1gMnSc1	Slováček-
voják	voják	k1gMnSc1	voják
</s>
</p>
<p>
<s>
Barbora	Barbora	k1gFnSc1	Barbora
Štěpánová-	Štěpánová-	k1gFnSc1	Štěpánová-
dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
výtahuv	výtahuva	k1gFnPc2	výtahuva
dalších	další	k2eAgFnPc6d1	další
rolích	role	k1gFnPc6	role
</s>
</p>
<p>
<s>
Milada	Milada	k1gFnSc1	Milada
Ježková	Ježková	k1gFnSc1	Ježková
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Velímská	Velímský	k2eAgFnSc1d1	Velímský
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
Richterová	Richterová	k1gFnSc1	Richterová
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Štrosová	Štrosová	k1gFnSc1	Štrosová
<g/>
,	,	kIx,	,
Zdenka	Zdenka	k1gFnSc1	Zdenka
Smrčková	Smrčková	k1gFnSc1	Smrčková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Pehrová-Krausová	Pehrová-Krausový	k2eAgFnSc1d1	Pehrová-Krausový
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Fišerová-Svátková	Fišerová-Svátkový	k2eAgFnSc1d1	Fišerová-Svátkový
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Kazdová	Kazdová	k1gFnSc1	Kazdová
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Koubek	Koubek	k1gMnSc1	Koubek
<g/>
,	,	kIx,	,
Bert	Berta	k1gFnPc2	Berta
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šmajzl	šmajznout	k5eAaPmAgMnS	šmajznout
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vondráček	Vondráček	k1gMnSc1	Vondráček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
==	==	k?	==
</s>
</p>
<p>
<s>
Dvanáctiletá	dvanáctiletý	k2eAgFnSc1d1	dvanáctiletá
Alenka	Alenka	k1gFnSc1	Alenka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
doma	doma	k6eAd1	doma
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
téměř	téměř	k6eAd1	téměř
liduprázdném	liduprázdný	k2eAgNnSc6d1	liduprázdné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
dospělých	dospělí	k1gMnPc2	dospělí
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
na	na	k7c6	na
prázdninách	prázdniny	k1gFnPc6	prázdniny
a	a	k8xC	a
dovolených	dovolená	k1gFnPc6	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
kadeřnice	kadeřnice	k1gFnSc2	kadeřnice
chodí	chodit	k5eAaImIp3nS	chodit
přes	přes	k7c4	přes
den	den	k1gInSc4	den
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Alenka	Alenka	k1gFnSc1	Alenka
musí	muset	k5eAaImIp3nS	muset
zabavit	zabavit	k5eAaPmF	zabavit
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
ráda	rád	k2eAgMnSc4d1	rád
maminčina	maminčin	k2eAgMnSc4d1	maminčin
nápadníka	nápadník	k1gMnSc4	nápadník
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
velký	velký	k2eAgInSc4d1	velký
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
kamínky	kamínek	k1gInPc4	kamínek
<g/>
,	,	kIx,	,
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
slunce	slunce	k1gNnSc1	slunce
zakouřeným	zakouřený	k2eAgNnSc7d1	zakouřené
sklíčkem	sklíčko	k1gNnSc7	sklíčko
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
mužem	muž	k1gMnSc7	muž
(	(	kIx(	(
<g/>
Budil	Budil	k1gMnSc1	Budil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
tajně	tajně	k6eAd1	tajně
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
však	však	k9	však
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
vždy	vždy	k6eAd1	vždy
naprostým	naprostý	k2eAgInSc7d1	naprostý
nezájmem	nezájem	k1gInSc7	nezájem
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
toho	ten	k3xDgMnSc2	ten
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Alenka	Alenka	k1gFnSc1	Alenka
si	se	k3xPyFc3	se
přesto	přesto	k6eAd1	přesto
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
fabuluje	fabulovat	k5eAaImIp3nS	fabulovat
romantický	romantický	k2eAgInSc1d1	romantický
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
přátelí	přátelit	k5eAaImIp3nS	přátelit
se	s	k7c7	s
slepým	slepý	k2eAgMnSc7d1	slepý
panem	pan	k1gMnSc7	pan
Tarabou	Taraba	k1gMnSc7	Taraba
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Taraba	Taraba	k1gFnSc1	Taraba
chodí	chodit	k5eAaImIp3nS	chodit
na	na	k7c4	na
vyšetření	vyšetření	k1gNnSc4	vyšetření
kvůli	kvůli	k7c3	kvůli
operaci	operace	k1gFnSc3	operace
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
hlídá	hlídat	k5eAaImIp3nS	hlídat
jeho	jeho	k3xOp3gFnSc4	jeho
fenu	fena	k1gFnSc4	fena
vlčáka	vlčák	k1gMnSc2	vlčák
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
potkává	potkávat	k5eAaImIp3nS	potkávat
mladého	mladý	k2eAgMnSc4d1	mladý
souseda	soused	k1gMnSc4	soused
-	-	kIx~	-
vojáka	voják	k1gMnSc4	voják
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
opuštěn	opuštěn	k2eAgMnSc1d1	opuštěn
svojí	svůj	k3xOyFgFnSc7	svůj
dívkou	dívka	k1gFnSc7	dívka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
naváže	navázat	k5eAaPmIp3nS	navázat
přátelský	přátelský	k2eAgInSc1d1	přátelský
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
platonicky	platonicky	k6eAd1	platonicky
romantický	romantický	k2eAgInSc1d1	romantický
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
Alenka	Alenka	k1gFnSc1	Alenka
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
dívka	dívka	k1gFnSc1	dívka
se	se	k3xPyFc4	se
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
objevuje	objevovat	k5eAaImIp3nS	objevovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
vrací	vracet	k5eAaImIp3nS	vracet
a	a	k8xC	a
Alenka	Alenka	k1gFnSc1	Alenka
zhrzeně	zhrzeně	k6eAd1	zhrzeně
utíká	utíkat	k5eAaImIp3nS	utíkat
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
filmu	film	k1gInSc2	film
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinka	hrdinka	k1gFnSc1	hrdinka
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
déšť	déšť	k1gInSc4	déšť
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
kamarádky	kamarádka	k1gFnPc1	kamarádka
pak	pak	k6eAd1	pak
přijedou	přijet	k5eAaPmIp3nP	přijet
z	z	k7c2	z
prázdnin	prázdniny	k1gFnPc2	prázdniny
domů	dům	k1gInPc2	dům
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
setká	setkat	k5eAaPmIp3nS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
se	se	k3xPyFc4	se
také	také	k9	také
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
zrzek	zrzek	k1gMnSc1	zrzek
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
zpočátku	zpočátku	k6eAd1	zpočátku
odmítá	odmítat	k5eAaImIp3nS	odmítat
kamarádit	kamarádit	k5eAaImF	kamarádit
a	a	k8xC	a
zlobí	zlobit	k5eAaImIp3nS	zlobit
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
usmíří	usmířit	k5eAaPmIp3nS	usmířit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
Alenka	Alenka	k1gFnSc1	Alenka
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pana	pan	k1gMnSc4	pan
Tarabu	Taraba	k1gMnSc4	Taraba
srazilo	srazit	k5eAaPmAgNnS	srazit
auto	auto	k1gNnSc1	auto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
konečně	konečně	k6eAd1	konečně
přichází	přicházet	k5eAaImIp3nS	přicházet
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
sídlišti	sídliště	k1gNnSc6	sídliště
Bohnice	Bohnice	k1gInPc1	Bohnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
společný	společný	k2eAgInSc4d1	společný
rozpočet	rozpočet	k1gInSc4	rozpočet
s	s	k7c7	s
filmem	film	k1gInSc7	film
Setkání	setkání	k1gNnSc1	setkání
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
déšť	déšť	k1gInSc4	déšť
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
déšť	déšť	k1gInSc4	déšť
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
