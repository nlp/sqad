<s>
Roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
opakovanou	opakovaný	k2eAgFnSc7d1	opakovaná
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
změnou	změna	k1gFnSc7	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
a	a	k8xC	a
subtropickém	subtropický	k2eAgInSc6d1	subtropický
pásu	pás	k1gInSc6	pás
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
(	(	kIx(	(
<g/>
monzunovém	monzunový	k2eAgNnSc6d1	monzunové
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
a	a	k8xC	a
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
srážek	srážka	k1gFnPc2	srážka
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
výraznější	výrazný	k2eAgFnPc1d2	výraznější
než	než	k8xS	než
změna	změna	k1gFnSc1	změna
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
tři	tři	k4xCgNnPc1	tři
<g/>
:	:	kIx,	:
chladné	chladný	k2eAgNnSc1d1	chladné
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
horké	horký	k2eAgNnSc1d1	horké
období	období	k1gNnSc1	období
a	a	k8xC	a
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
a	a	k8xC	a
polárním	polární	k2eAgInSc6d1	polární
pásu	pás	k1gInSc6	pás
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
<g/>
:	:	kIx,	:
jaro	jaro	k1gNnSc4	jaro
<g/>
,	,	kIx,	,
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
otočena	otočen	k2eAgFnSc1d1	otočena
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
změnám	změna	k1gFnPc3	změna
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
opačně	opačně	k6eAd1	opačně
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
větší	veliký	k2eAgInPc4d2	veliký
rozdíly	rozdíl	k1gInPc4	rozdíl
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
než	než	k8xS	než
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
teplejší	teplý	k2eAgInSc4d2	teplejší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stromech	strom	k1gInPc6	strom
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
pupeny	pupen	k1gInPc1	pupen
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
začínají	začínat	k5eAaImIp3nP	začínat
klíčit	klíčit	k5eAaImF	klíčit
a	a	k8xC	a
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
přilétají	přilétat	k5eAaImIp3nP	přilétat
z	z	k7c2	z
teplých	teplý	k2eAgFnPc2d1	teplá
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
stavějí	stavět	k5eAaImIp3nP	stavět
si	se	k3xPyFc3	se
hnízda	hnízdo	k1gNnPc1	hnízdo
a	a	k8xC	a
snášejí	snášet	k5eAaImIp3nP	snášet
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
si	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
partnery	partner	k1gMnPc4	partner
a	a	k8xC	a
přivádějí	přivádět	k5eAaImIp3nP	přivádět
na	na	k7c4	na
svět	svět	k1gInSc4	svět
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Astronomické	astronomický	k2eAgNnSc1d1	astronomické
jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
okamžikem	okamžik	k1gInSc7	okamžik
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
(	(	kIx(	(
<g/>
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
19	[number]	k4	19
<g/>
.	.	kIx.	.
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
<g/>
,	,	kIx,	,
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zpravidla	zpravidla	k6eAd1	zpravidla
20	[number]	k4	20
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
astronomický	astronomický	k2eAgInSc4d1	astronomický
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
zapadá	zapadat	k5eAaImIp3nS	zapadat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
den	den	k1gInSc4	den
i	i	k8xC	i
noc	noc	k1gFnSc4	noc
trvá	trvat	k5eAaImIp3nS	trvat
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
nastává	nastávat	k5eAaImIp3nS	nastávat
polární	polární	k2eAgInSc1d1	polární
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
do	do	k7c2	do
podzimní	podzimní	k2eAgFnSc2d1	podzimní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologické	meteorologický	k2eAgNnSc1d1	meteorologické
jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jarními	jarní	k2eAgInPc7d1	jarní
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
březen	březen	k1gInSc4	březen
<g/>
,	,	kIx,	,
duben	duben	k1gInSc4	duben
a	a	k8xC	a
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejteplejší	teplý	k2eAgNnSc1d3	nejteplejší
období	období	k1gNnSc1	období
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
a	a	k8xC	a
pozdě	pozdě	k6eAd1	pozdě
se	se	k3xPyFc4	se
stmívá	stmívat	k5eAaImIp3nS	stmívat
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
kvetou	kvést	k5eAaImIp3nP	kvést
<g/>
,	,	kIx,	,
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
obilí	obilí	k1gNnSc4	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Astronomické	astronomický	k2eAgNnSc1d1	astronomické
léto	léto	k1gNnSc1	léto
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
okamžikem	okamžik	k1gInSc7	okamžik
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
den	den	k1gInSc4	den
a	a	k8xC	a
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
astronomická	astronomický	k2eAgFnSc1d1	astronomická
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologické	meteorologický	k2eAgNnSc1d1	meteorologické
léto	léto	k1gNnSc1	léto
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Letními	letní	k2eAgInPc7d1	letní
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
červen	červen	k1gInSc4	červen
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
a	a	k8xC	a
chladnější	chladný	k2eAgInPc1d2	chladnější
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
odkvétají	odkvétat	k5eAaImIp3nP	odkvétat
a	a	k8xC	a
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
opadává	opadávat	k5eAaImIp3nS	opadávat
listí	listí	k1gNnSc1	listí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stromech	strom	k1gInPc6	strom
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Obilí	obilí	k1gNnSc1	obilí
je	být	k5eAaImIp3nS	být
sklizeno	sklidit	k5eAaPmNgNnS	sklidit
a	a	k8xC	a
pole	pole	k1gNnPc1	pole
jsou	být	k5eAaImIp3nP	být
zoraná	zoraný	k2eAgNnPc1d1	zorané
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
zvířata	zvíře	k1gNnPc1	zvíře
si	se	k3xPyFc3	se
připravují	připravovat	k5eAaImIp3nP	připravovat
zásoby	zásoba	k1gFnPc1	zásoba
potravin	potravina	k1gFnPc2	potravina
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Stěhovavá	stěhovavý	k2eAgNnPc1d1	stěhovavé
zvířata	zvíře	k1gNnPc1	zvíře
odlétají	odlétat	k5eAaImIp3nP	odlétat
do	do	k7c2	do
teplých	teplý	k2eAgFnPc2d1	teplá
krajin	krajina	k1gFnPc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Astronomický	astronomický	k2eAgInSc4d1	astronomický
podzim	podzim	k1gInSc4	podzim
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
okamžikem	okamžik	k1gInSc7	okamžik
podzimní	podzimní	k2eAgFnSc3d1	podzimní
rovnodennosti	rovnodennost	k1gFnSc3	rovnodennost
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
astronomické	astronomický	k2eAgNnSc4d1	astronomické
jaro	jaro	k1gNnSc4	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologický	meteorologický	k2eAgInSc4d1	meteorologický
podzim	podzim	k1gInSc4	podzim
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Podzimními	podzimní	k2eAgInPc7d1	podzimní
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
září	září	k1gNnSc4	září
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc4	říjen
a	a	k8xC	a
listopad	listopad	k1gInSc4	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgNnSc1d3	nejchladnější
období	období	k1gNnSc1	období
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgNnSc4d1	krátké
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stmívá	stmívat	k5eAaImIp3nS	stmívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
přestávají	přestávat	k5eAaImIp3nP	přestávat
růst	růst	k1gInSc4	růst
a	a	k8xC	a
stromy	strom	k1gInPc4	strom
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
zvířatům	zvíře	k1gNnPc3	zvíře
naroste	narůst	k5eAaPmIp3nS	narůst
huňatější	huňatý	k2eAgFnSc4d2	huňatější
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gFnPc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
chladem	chlad	k1gInSc7	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1	jiné
zvířata	zvíře	k1gNnPc1	zvíře
celou	celý	k2eAgFnSc4d1	celá
zimu	zima	k1gFnSc4	zima
prospí	prospat	k5eAaPmIp3nS	prospat
<g/>
.	.	kIx.	.
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1	astronomická
zima	zima	k1gFnSc1	zima
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
okamžikem	okamžik	k1gInSc7	okamžik
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
noc	noc	k1gFnSc1	noc
a	a	k8xC	a
nejkratší	krátký	k2eAgInSc1d3	nejkratší
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
astronomické	astronomický	k2eAgNnSc4d1	astronomické
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
zima	zima	k1gFnSc1	zima
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
(	(	kIx(	(
<g/>
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zimními	zimní	k2eAgInPc7d1	zimní
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
prosinec	prosinec	k1gInSc4	prosinec
<g/>
,	,	kIx,	,
leden	leden	k1gInSc4	leden
a	a	k8xC	a
únor	únor	k1gInSc4	únor
<g/>
.	.	kIx.	.
</s>
<s>
Astronomické	astronomický	k2eAgNnSc1d1	astronomické
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc1	podzim
ani	ani	k8xC	ani
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
definované	definovaný	k2eAgInPc4d1	definovaný
astronomickými	astronomický	k2eAgInPc7d1	astronomický
okamžiky	okamžik	k1gInPc7	okamžik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jarní	jarní	k2eAgFnSc4d1	jarní
a	a	k8xC	a
podzimní	podzimní	k2eAgFnSc4d1	podzimní
rovnodennost	rovnodennost	k1gFnSc4	rovnodennost
a	a	k8xC	a
letní	letní	k2eAgInSc4d1	letní
a	a	k8xC	a
zimní	zimní	k2eAgInSc4d1	zimní
slunovrat	slunovrat	k1gInSc4	slunovrat
<g/>
,	,	kIx,	,
nezačínají	začínat	k5eNaImIp3nP	začínat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
letech	léto	k1gNnPc6	léto
vždy	vždy	k6eAd1	vždy
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sluneční	sluneční	k2eAgInSc1d1	sluneční
rok	rok	k1gInSc1	rok
není	být	k5eNaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
přesnému	přesný	k2eAgInSc3d1	přesný
počtu	počet	k1gInSc3	počet
365	[number]	k4	365
dní	den	k1gInPc2	den
a	a	k8xC	a
kalendář	kalendář	k1gInSc4	kalendář
si	se	k3xPyFc3	se
vypomáhá	vypomáhat	k5eAaImIp3nS	vypomáhat
přestupnými	přestupný	k2eAgInPc7d1	přestupný
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Fenologická	fenologický	k2eAgNnPc1d1	fenologické
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
Ekologická	ekologický	k2eAgNnPc1d1	ekologické
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
Klimatická	klimatický	k2eAgNnPc1d1	klimatické
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
Agrotechnická	agrotechnický	k2eAgNnPc1d1	agrotechnické
období	období	k1gNnPc1	období
Hydrologická	hydrologický	k2eAgNnPc1d1	hydrologické
období	období	k1gNnPc1	období
Lidské	lidský	k2eAgInPc4d1	lidský
biorytmy	biorytmus	k1gInPc4	biorytmus
Fenologie	fenologie	k1gFnSc2	fenologie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
roční	roční	k2eAgFnSc2d1	roční
období	období	k1gNnSc4	období
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
z	z	k7c2	z
jiného	jiné	k1gNnSc2	jiné
pohledu	pohled	k1gInSc2	pohled
</s>
