<s>
Zaklínač	zaklínač	k1gMnSc1	zaklínač
je	být	k5eAaImIp3nS	být
fantasy	fantas	k1gInPc4	fantas
cyklus	cyklus	k1gInSc1	cyklus
polského	polský	k2eAgMnSc2d1	polský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Andrzeje	Andrzeje	k1gMnSc2	Andrzeje
Sapkowského	Sapkowský	k1gMnSc2	Sapkowský
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jako	jako	k9	jako
série	série	k1gFnPc4	série
krátkých	krátký	k2eAgInPc2d1	krátký
příběhů	příběh	k1gInPc2	příběh
publikovaných	publikovaný	k2eAgInPc2d1	publikovaný
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
sci-fi	scii	k1gNnSc6	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
časopise	časopis	k1gInSc6	časopis
Fantastyka	Fantastyek	k1gInSc2	Fantastyek
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
povídka	povídka	k1gFnSc1	povídka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Wiedźmin	Wiedźmin	k1gInSc1	Wiedźmin
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
)	)	kIx)	)
napsána	napsán	k2eAgFnSc1d1	napsána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
jako	jako	k8xC	jako
příspěvek	příspěvek	k1gInSc1	příspěvek
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
pokračování	pokračování	k1gNnSc2	pokračování
Krev	krev	k1gFnSc1	krev
elfů	elf	k1gMnPc2	elf
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bouřková	bouřkový	k2eAgFnSc1d1	bouřková
sezóna	sezóna	k1gFnSc1	sezóna
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
předchozí	předchozí	k2eAgFnSc4d1	předchozí
sérii	série	k1gFnSc4	série
jako	jako	k8xC	jako
prequel	prequel	k1gInSc4	prequel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
vydána	vydán	k2eAgFnSc1d1	vydána
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgInPc2d3	nejprodávanější
titulů	titul	k1gInPc2	titul
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
listopad	listopad	k1gInSc1	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
je	být	k5eAaImIp3nS	být
oceňován	oceňovat	k5eAaImNgInS	oceňovat
pro	pro	k7c4	pro
lehce	lehko	k6eAd1	lehko
ironický	ironický	k2eAgInSc4d1	ironický
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
jemných	jemný	k2eAgInPc2d1	jemný
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vzorců	vzorec	k1gInPc2	vzorec
hrdinské	hrdinský	k2eAgInPc4d1	hrdinský
fantasy	fantas	k1gInPc4	fantas
zde	zde	k6eAd1	zde
nezaznamenáme	zaznamenat	k5eNaPmIp1nP	zaznamenat
rozdělení	rozdělení	k1gNnSc4	rozdělení
postav	postava	k1gFnPc2	postava
na	na	k7c4	na
černé	černý	k2eAgFnPc4d1	černá
a	a	k8xC	a
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Geraltův	Geraltův	k2eAgInSc1d1	Geraltův
svět	svět	k1gInSc1	svět
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
označit	označit	k5eAaPmF	označit
ani	ani	k8xC	ani
za	za	k7c7	za
typickou	typický	k2eAgFnSc7d1	typická
dark	dark	k1gInSc1	dark
fantasy	fantas	k1gInPc1	fantas
-	-	kIx~	-
Sapkowski	Sapkowski	k1gNnSc1	Sapkowski
spíše	spíše	k9	spíše
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
odstíny	odstín	k1gInPc4	odstín
"	"	kIx"	"
<g/>
šedé	šedá	k1gFnPc4	šedá
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgInS	dopustit
incestu	incest	k1gInSc2	incest
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vykreslen	vykreslen	k2eAgMnSc1d1	vykreslen
jako	jako	k8xC	jako
starostlivý	starostlivý	k2eAgMnSc1d1	starostlivý
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
próz	próza	k1gFnPc2	próza
je	být	k5eAaImIp3nS	být
Geralt	Geralt	k1gInSc1	Geralt
z	z	k7c2	z
Rivie	Rivie	k1gFnSc2	Rivie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
povoláním	povolání	k1gNnSc7	povolání
zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nájemný	nájemný	k2eAgMnSc1d1	nájemný
lovec	lovec	k1gMnSc1	lovec
nestvůr	nestvůra	k1gFnPc2	nestvůra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ději	děj	k1gInSc6	děj
se	se	k3xPyFc4	se
mihnou	mihnout	k5eAaPmIp3nP	mihnout
i	i	k9	i
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
postavy	postava	k1gFnPc1	postava
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
zaklínačů	zaklínač	k1gMnPc2	zaklínač
<g/>
.	.	kIx.	.
</s>
<s>
Sapkowski	Sapkowski	k6eAd1	Sapkowski
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
zaklínače	zaklínač	k1gMnPc4	zaklínač
jako	jako	k8xS	jako
mutanty	mutant	k1gMnPc4	mutant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
proměněni	proměnit	k5eAaPmNgMnP	proměnit
pomocí	pomocí	k7c2	pomocí
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
mutagenů	mutagen	k1gInPc2	mutagen
a	a	k8xC	a
jedů	jed	k1gInPc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Získali	získat	k5eAaPmAgMnP	získat
tak	tak	k6eAd1	tak
magické	magický	k2eAgFnPc4d1	magická
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
výcvikem	výcvik	k1gInSc7	výcvik
si	se	k3xPyFc3	se
osvojili	osvojit	k5eAaPmAgMnP	osvojit
též	též	k9	též
pokročilé	pokročilý	k2eAgFnPc4d1	pokročilá
bojové	bojový	k2eAgFnPc4d1	bojová
techniky	technika	k1gFnPc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
nadlidské	nadlidský	k2eAgFnPc4d1	nadlidská
schopnosti	schopnost	k1gFnPc4	schopnost
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k6eAd1	především
k	k	k7c3	k
boji	boj	k1gInSc3	boj
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
obludami	obluda	k1gFnPc7	obluda
a	a	k8xC	a
magickými	magický	k2eAgMnPc7d1	magický
tvory	tvor	k1gMnPc7	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sužují	sužovat	k5eAaImIp3nP	sužovat
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
série	série	k1gFnSc2	série
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
je	být	k5eAaImIp3nS	být
princezna	princezna	k1gFnSc1	princezna
Cirilla	Cirilla	k1gFnSc1	Cirilla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nepodrobí	podrobit	k5eNaPmIp3nS	podrobit
mutacím	mutace	k1gFnPc3	mutace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstoupí	podstoupit	k5eAaPmIp3nS	podstoupit
zaklínačský	zaklínačský	k2eAgInSc4d1	zaklínačský
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Zaklínači	zaklínač	k1gMnPc1	zaklínač
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Sapkowského	Sapkowského	k2eAgMnPc1d1	Sapkowského
představují	představovat	k5eAaImIp3nP	představovat
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
druidů	druid	k1gMnPc2	druid
a	a	k8xC	a
mágů	mág	k1gMnPc2	mág
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
různé	různý	k2eAgInPc4d1	různý
postupy	postup	k1gInPc4	postup
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pomocí	pomocí	k7c2	pomocí
mutagenů	mutagen	k1gInPc2	mutagen
<g/>
,	,	kIx,	,
jedů	jed	k1gInPc2	jed
a	a	k8xC	a
magických	magický	k2eAgInPc2d1	magický
přípravků	přípravek	k1gInPc2	přípravek
upravit	upravit	k5eAaPmF	upravit
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pracoval	pracovat	k5eAaImAgMnS	pracovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
efektivněji	efektivně	k6eAd2	efektivně
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
zostřené	zostřený	k2eAgInPc4d1	zostřený
smysly	smysl	k1gInPc4	smysl
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
používat	používat	k5eAaImF	používat
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
magii	magie	k1gFnSc4	magie
<g/>
.	.	kIx.	.
</s>
<s>
Mutace	mutace	k1gFnSc1	mutace
v	v	k7c4	v
zaklínače	zaklínač	k1gMnSc4	zaklínač
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Zkouška	zkouška	k1gFnSc1	zkouška
trav	tráva	k1gFnPc2	tráva
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
velice	velice	k6eAd1	velice
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
a	a	k8xC	a
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
adeptů	adept	k1gMnPc2	adept
nepřežila	přežít	k5eNaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
proměna	proměna	k1gFnSc1	proměna
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
-	-	kIx~	-
zejména	zejména	k9	zejména
neplodnost	neplodnost	k1gFnSc4	neplodnost
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
citovou	citový	k2eAgFnSc4d1	citová
plochost	plochost	k1gFnSc4	plochost
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
Cesta	cesta	k1gFnSc1	cesta
meče	meč	k1gInSc2	meč
-	-	kIx~	-
bojový	bojový	k2eAgInSc1d1	bojový
výcvik	výcvik	k1gInSc1	výcvik
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
boj	boj	k1gInSc4	boj
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
a	a	k8xC	a
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
připravenost	připravenost	k1gFnSc4	připravenost
<g/>
.	.	kIx.	.
</s>
<s>
Cech	cech	k1gInSc4	cech
zaklínačů	zaklínač	k1gMnPc2	zaklínač
získával	získávat	k5eAaImAgMnS	získávat
adepty	adept	k1gMnPc4	adept
a	a	k8xC	a
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
přerod	přerod	k1gInSc4	přerod
a	a	k8xC	a
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Nábor	nábor	k1gInSc1	nábor
zaklínačů	zaklínač	k1gMnPc2	zaklínač
probíhal	probíhat	k5eAaImAgInS	probíhat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
byl	být	k5eAaImAgInS	být
nahodilý	nahodilý	k2eAgInSc1d1	nahodilý
výběr	výběr	k1gInSc1	výběr
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
osudu	osud	k1gInSc2	osud
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dáš	dát	k5eAaPmIp2nS	dát
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nalezneš	nalézt	k5eAaBmIp2nS	nalézt
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
bys	by	kYmCp2nS	by
to	ten	k3xDgNnSc4	ten
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
tak	tak	k6eAd1	tak
zněla	znět	k5eAaImAgFnS	znět
často	často	k6eAd1	často
odpověď	odpověď	k1gFnSc1	odpověď
zaklínače	zaklínač	k1gMnSc2	zaklínač
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
zachráněného	zachráněný	k2eAgInSc2d1	zachráněný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
odměnit	odměnit	k5eAaPmF	odměnit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
alternativu	alternativa	k1gFnSc4	alternativa
zaklínači	zaklínač	k1gMnSc3	zaklínač
často	často	k6eAd1	často
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
získaní	získaný	k2eAgMnPc1d1	získaný
adepti	adept	k1gMnPc1	adept
budou	být	k5eAaImBp3nP	být
osudem	osud	k1gInSc7	osud
předurčení	předurčení	k1gNnSc2	předurčení
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
prodělání	prodělání	k1gNnSc3	prodělání
proměny	proměna	k1gFnSc2	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
zaklínače	zaklínač	k1gMnSc2	zaklínač
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
báli	bát	k5eAaImAgMnP	bát
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
nestvůr	nestvůra	k1gFnPc2	nestvůra
<g/>
.	.	kIx.	.
</s>
<s>
Několikráte	Několikráte	k6eAd1	Několikráte
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
tažení	tažení	k1gNnSc1	tažení
lidí	člověk	k1gMnPc2	člověk
proti	proti	k7c3	proti
zaklínačům	zaklínač	k1gMnPc3	zaklínač
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
podněcované	podněcovaný	k2eAgFnPc4d1	podněcovaná
čaroději	čaroděj	k1gMnPc7	čaroděj
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
od	od	k7c2	od
jisté	jistý	k2eAgFnSc2d1	jistá
doby	doba	k1gFnSc2	doba
začali	začít	k5eAaPmAgMnP	začít
zaklínače	zaklínač	k1gMnPc4	zaklínač
nenávidět	návidět	k5eNaImF	návidět
a	a	k8xC	a
považovali	považovat	k5eAaImAgMnP	považovat
je	on	k3xPp3gFnPc4	on
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgFnSc4d1	nežádoucí
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
takové	takový	k3xDgNnSc4	takový
tažení	tažení	k1gNnSc1	tažení
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
dobytí	dobytí	k1gNnSc6	dobytí
Kaer	Kaera	k1gFnPc2	Kaera
Morhen	Morhno	k1gNnPc2	Morhno
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
zaklínačského	zaklínačský	k2eAgInSc2d1	zaklínačský
cechu	cech	k1gInSc2	cech
<g/>
,	,	kIx,	,
a	a	k8xC	a
vybití	vybití	k1gNnSc4	vybití
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gMnPc2	jeho
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zaklínači	zaklínač	k1gMnPc1	zaklínač
stali	stát	k5eAaPmAgMnP	stát
vzácnými	vzácný	k2eAgInPc7d1	vzácný
a	a	k8xC	a
zastavila	zastavit	k5eAaPmAgFnS	zastavit
se	se	k3xPyFc4	se
obnova	obnova	k1gFnSc1	obnova
jejich	jejich	k3xOp3gFnPc2	jejich
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
prastaré	prastarý	k2eAgFnPc1d1	prastará
techniky	technika	k1gFnPc1	technika
používané	používaný	k2eAgFnPc1d1	používaná
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
organismu	organismus	k1gInSc2	organismus
byly	být	k5eAaImAgInP	být
nenávratně	návratně	k6eNd1	návratně
zapomenuty	zapomenout	k5eAaPmNgInP	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
začíná	začínat	k5eAaImIp3nS	začínat
děj	děj	k1gInSc4	děj
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
ze	z	k7c2	z
zaklínačské	zaklínačský	k2eAgFnSc2d1	zaklínačská
ságy	sága	k1gFnSc2	sága
<g/>
,	,	kIx,	,
Krve	krev	k1gFnSc2	krev
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Sapkowski	Sapkowski	k6eAd1	Sapkowski
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
zaklínač	zaklínač	k1gMnSc1	zaklínač
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
ovládající	ovládající	k2eAgFnSc4d1	ovládající
znalost	znalost	k1gFnSc4	znalost
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
přírodního	přírodní	k2eAgNnSc2d1	přírodní
léčitelství	léčitelství	k1gNnSc2	léčitelství
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
poutník	poutník	k1gMnSc1	poutník
putující	putující	k2eAgInSc4d1	putující
mezi	mezi	k7c7	mezi
osadami	osada	k1gFnPc7	osada
a	a	k8xC	a
praktikující	praktikující	k2eAgNnSc4d1	praktikující
bylinkářství	bylinkářství	k1gNnSc4	bylinkářství
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gInSc1	Geralt
z	z	k7c2	z
Rivie	Rivie	k1gFnSc2	Rivie
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Řezník	řezník	k1gMnSc1	řezník
z	z	k7c2	z
Blavikenu	Blaviken	k1gInSc2	Blaviken
či	či	k8xC	či
Bílý	bílý	k2eAgMnSc1d1	bílý
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
elfsky	elfsky	k6eAd1	elfsky
Gwynbleidd	Gwynbleidd	k1gInSc1	Gwynbleidd
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dítětem	dítě	k1gNnSc7	dítě
čarodějky	čarodějka	k1gFnSc2	čarodějka
-	-	kIx~	-
velice	velice	k6eAd1	velice
neobvykle	obvykle	k6eNd1	obvykle
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
čarodějek	čarodějka	k1gFnPc2	čarodějka
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
za	za	k7c4	za
schopnost	schopnost	k1gFnSc4	schopnost
používání	používání	k1gNnSc2	používání
magie	magie	k1gFnSc2	magie
možností	možnost	k1gFnPc2	možnost
počít	počít	k5eAaPmF	počít
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
odveden	odveden	k2eAgInSc1d1	odveden
na	na	k7c4	na
zaklínačský	zaklínačský	k2eAgInSc4d1	zaklínačský
hrad	hrad	k1gInSc4	hrad
Kaer	Kaera	k1gFnPc2	Kaera
Morhen	Morhna	k1gFnPc2	Morhna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
podroben	podrobit	k5eAaPmNgInS	podrobit
dvěma	dva	k4xCgFnPc3	dva
základním	základní	k2eAgFnPc3d1	základní
zkouškám	zkouška	k1gFnPc3	zkouška
<g/>
:	:	kIx,	:
Zkoušce	zkouška	k1gFnSc3	zkouška
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
Cestě	cesta	k1gFnSc3	cesta
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
Vesemira	Vesemir	k1gMnSc2	Vesemir
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
zaklínačský	zaklínačský	k2eAgInSc4d1	zaklínačský
medailon	medailon	k1gInSc4	medailon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
prokazování	prokazování	k1gNnSc3	prokazování
totožnosti	totožnost	k1gFnSc2	totožnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
detektor	detektor	k1gInSc1	detektor
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
Kaer	Kaera	k1gFnPc2	Kaera
Morhen	Morhna	k1gFnPc2	Morhna
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
pomáhat	pomáhat	k5eAaImF	pomáhat
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vděkem	vděk	k1gInSc7	vděk
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
pochopením	pochopení	k1gNnSc7	pochopení
<g/>
.	.	kIx.	.
</s>
<s>
Cirilla	Cirilla	k6eAd1	Cirilla
je	být	k5eAaImIp3nS	být
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Cintry	Cintrum	k1gNnPc7	Cintrum
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Pavetty	Pavetta	k1gMnSc2	Pavetta
a	a	k8xC	a
Dunyho	Duny	k1gMnSc2	Duny
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
předky	předek	k1gMnPc7	předek
jsou	být	k5eAaImIp3nP	být
elfka	elfka	k1gMnSc1	elfka
Lara	Lara	k1gMnSc1	Lara
Dorren	Dorrna	k1gFnPc2	Dorrna
aep	aep	k?	aep
Siadhal	Siadhal	k1gMnSc1	Siadhal
a	a	k8xC	a
lidský	lidský	k2eAgMnSc1d1	lidský
mág	mág	k1gMnSc1	mág
Cregennan	Cregennan	k1gMnSc1	Cregennan
z	z	k7c2	z
Lód	Lód	k1gFnSc2	Lód
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sudbou	sudba	k1gFnSc7	sudba
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Geraltem	Geralt	k1gInSc7	Geralt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
pracovní	pracovní	k2eAgFnSc2d1	pracovní
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Cintře	Cintra	k1gFnSc6	Cintra
u	u	k7c2	u
královny	královna	k1gFnSc2	královna
Calanthé	Calanthý	k2eAgFnSc2d1	Calanthý
Geralt	Geralt	k2eAgInSc1d1	Geralt
zachránil	zachránit	k5eAaPmAgInS	zachránit
život	život	k1gInSc1	život
rytíři	rytíř	k1gMnSc3	rytíř
Ježkovi	Ježek	k1gMnSc3	Ježek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
budoucí	budoucí	k2eAgFnSc3d1	budoucí
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
princezně	princezna	k1gFnSc3	princezna
Pavettě	Pavetť	k1gFnSc2	Pavetť
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
Pavetta	Pavetta	k1gFnSc1	Pavetta
slíbila	slíbit	k5eAaPmAgFnS	slíbit
Geraltovi	Geralt	k1gMnSc3	Geralt
odevzdat	odevzdat	k5eAaPmF	odevzdat
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
dítě	dítě	k1gNnSc1	dítě
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
Otázka	otázka	k1gFnSc1	otázka
ceny	cena	k1gFnSc2	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
znovu	znovu	k6eAd1	znovu
přijel	přijet	k5eAaPmAgInS	přijet
Geralt	Geralt	k1gInSc1	Geralt
do	do	k7c2	do
Cintry	Cintrum	k1gNnPc7	Cintrum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
Ciri	Ciri	k1gNnSc2	Ciri
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
jejich	jejich	k3xOp3gFnSc2	jejich
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
protnul	protnout	k5eAaPmAgMnS	protnout
v	v	k7c6	v
lese	les	k1gInSc6	les
Brokilon	Brokilon	k1gInSc1	Brokilon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
Geralt	Geralt	k1gMnSc1	Geralt
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
dryád	dryáda	k1gFnPc2	dryáda
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
dalšímu	další	k2eAgNnSc3d1	další
setkání	setkání	k1gNnSc3	setkání
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c4	po
vypálení	vypálení	k1gNnSc4	vypálení
Cintry	Cintr	k1gInPc4	Cintr
nilfgaardským	nilfgaardský	k2eAgNnSc7d1	nilfgaardský
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
zraněného	zraněný	k2eAgMnSc4d1	zraněný
zaklínače	zaklínač	k1gMnSc4	zaklínač
přivezl	přivézt	k5eAaPmAgMnS	přivézt
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
statek	statek	k1gInSc4	statek
sedlák	sedlák	k1gMnSc1	sedlák
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
Geralt	Geralt	k1gMnSc1	Geralt
předtím	předtím	k6eAd1	předtím
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
<g/>
;	;	kIx,	;
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
slíbil	slíbit	k5eAaPmAgMnS	slíbit
dát	dát	k5eAaPmF	dát
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
najde	najít	k5eAaPmIp3nS	najít
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
čekal	čekat	k5eAaImAgMnS	čekat
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
Něco	něco	k6eAd1	něco
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c6	na
statku	statek	k1gInSc6	statek
Geralt	Geralt	k1gInSc1	Geralt
opět	opět	k6eAd1	opět
potkal	potkat	k5eAaPmAgInS	potkat
Ciri	Cire	k1gFnSc4	Cire
<g/>
,	,	kIx,	,
sklonil	sklonit	k5eAaPmAgMnS	sklonit
hlavu	hlava	k1gFnSc4	hlava
před	před	k7c7	před
sudbou	sudba	k1gFnSc7	sudba
a	a	k8xC	a
odvezl	odvézt	k5eAaPmAgInS	odvézt
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
Kaer	Kaer	k1gInSc4	Kaer
Morhen	Morhen	k2eAgInSc4d1	Morhen
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
sice	sice	k8xC	sice
podrobena	podroben	k2eAgFnSc1d1	podrobena
Zkoušce	zkouška	k1gFnSc3	zkouška
trav	tráva	k1gFnPc2	tráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prošla	projít	k5eAaPmAgFnS	projít
Cestou	cesta	k1gFnSc7	cesta
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ji	on	k3xPp3gFnSc4	on
čarodějka	čarodějka	k1gFnSc1	čarodějka
Triss	Trissa	k1gFnPc2	Trissa
Ranuncul	Ranuncula	k1gFnPc2	Ranuncula
odvezla	odvézt	k5eAaPmAgFnS	odvézt
do	do	k7c2	do
svatyně	svatyně	k1gFnSc2	svatyně
Melitelé	Melitel	k1gMnPc1	Melitel
k	k	k7c3	k
velekněžce	velekněžka	k1gFnSc3	velekněžka
Nenneke	Nennek	k1gFnSc2	Nennek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
naučit	naučit	k5eAaPmF	naučit
slušnému	slušný	k2eAgNnSc3d1	slušné
chování	chování	k1gNnSc3	chování
a	a	k8xC	a
všeobecným	všeobecný	k2eAgFnPc3d1	všeobecná
znalostem	znalost	k1gFnPc3	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Yennefer	Yennefer	k1gInSc1	Yennefer
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
navíc	navíc	k6eAd1	navíc
krátce	krátce	k6eAd1	krátce
učila	učit	k5eAaImAgFnS	učit
čarování	čarování	k1gNnSc4	čarování
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Počátek	počátek	k1gInSc1	počátek
Ságy	sága	k1gFnSc2	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Yennefer	Yennefer	k1gInSc1	Yennefer
z	z	k7c2	z
Vengerbergu	Vengerberg	k1gInSc2	Vengerberg
je	být	k5eAaImIp3nS	být
mocná	mocný	k2eAgFnSc1d1	mocná
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
,	,	kIx,	,
Geraltova	Geraltův	k2eAgFnSc1d1	Geraltova
milenka	milenka	k1gFnSc1	milenka
a	a	k8xC	a
Trissina	Trissin	k2eAgFnSc1d1	Trissin
blízká	blízký	k2eAgFnSc1d1	blízká
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
vlastní	vlastní	k2eAgNnSc4d1	vlastní
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
jako	jako	k9	jako
své	své	k1gNnSc4	své
poslední	poslední	k2eAgNnSc4d1	poslední
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Džinem	džin	k1gInSc7	džin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Geraltem	Geralt	k1gInSc7	Geralt
svázána	svázat	k5eAaPmNgFnS	svázat
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
poutem	pout	k1gInSc7	pout
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
děti	dítě	k1gFnPc1	dítě
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
čarodějek	čarodějka	k1gFnPc2	čarodějka
mít	mít	k5eAaImF	mít
nemohla	moct	k5eNaImAgFnS	moct
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
po	po	k7c6	po
nich	on	k3xPp3gNnPc6	on
stále	stále	k6eAd1	stále
toužila	toužit	k5eAaImAgFnS	toužit
a	a	k8xC	a
hledala	hledat	k5eAaImAgFnS	hledat
všemožné	všemožný	k2eAgInPc4d1	všemožný
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
by	by	kYmCp3nS	by
mateřství	mateřství	k1gNnSc4	mateřství
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
Ciri	Ciri	k1gNnSc4	Ciri
a	a	k8xC	a
učila	učít	k5eAaPmAgFnS	učít
ji	on	k3xPp3gFnSc4	on
magii	magie	k1gFnSc4	magie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Ciri	Cir	k1gMnPc7	Cir
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
sblížily	sblížit	k5eAaPmAgInP	sblížit
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
vztah	vztah	k1gInSc4	vztah
jako	jako	k9	jako
mezi	mezi	k7c7	mezi
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Triss	Triss	k1gInSc1	Triss
Ranuncul	Ranuncul	k1gInSc1	Ranuncul
(	(	kIx(	(
<g/>
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
originále	originál	k1gInSc6	originál
Triss	Triss	k1gInSc1	Triss
Merigold	Merigold	k1gInSc1	Merigold
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
,	,	kIx,	,
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Geralta	Geralta	k1gFnSc1	Geralta
a	a	k8xC	a
Yennefer	Yennefer	k1gInSc1	Yennefer
<g/>
.	.	kIx.	.
</s>
<s>
Marigold	Marigold	k1gInSc1	Marigold
(	(	kIx(	(
<g/>
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
originále	originál	k1gInSc6	originál
Jaskier	Jaskira	k1gFnPc2	Jaskira
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
pryskyřník	pryskyřník	k1gInSc1	pryskyřník
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
blatouch	blatouch	k1gInSc1	blatouch
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
trubadúr	trubadúr	k1gMnSc1	trubadúr
<g/>
,	,	kIx,	,
Geraltův	Geraltův	k2eAgMnSc1d1	Geraltův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
milovník	milovník	k1gMnSc1	milovník
alkoholické	alkoholický	k2eAgFnSc2d1	alkoholická
i	i	k8xC	i
sexuální	sexuální	k2eAgFnSc2d1	sexuální
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Yarpen	Yarpen	k2eAgInSc1d1	Yarpen
Zigrin	Zigrin	k1gInSc1	Zigrin
je	být	k5eAaImIp3nS	být
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
žoldnéř	žoldnéř	k1gMnSc1	žoldnéř
<g/>
,	,	kIx,	,
nájemný	nájemný	k2eAgMnSc1d1	nájemný
zabiják	zabiják	k1gMnSc1	zabiják
a	a	k8xC	a
voják	voják	k1gMnSc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Regis	Regis	k1gInSc1	Regis
je	být	k5eAaImIp3nS	být
abstinující	abstinující	k2eAgMnSc1d1	abstinující
upír	upír	k1gMnSc1	upír
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
bylinkář	bylinkář	k1gMnSc1	bylinkář
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
se	s	k7c7	s
zaklínačem	zaklínač	k1gMnSc7	zaklínač
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
za	za	k7c4	za
Ciri	Cire	k1gFnSc4	Cire
<g/>
.	.	kIx.	.
</s>
<s>
Angoulê	Angoulê	k?	Angoulê
je	být	k5eAaImIp3nS	být
plavovlasá	plavovlasý	k2eAgFnSc1d1	plavovlasá
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
cintránské	cintránský	k2eAgFnSc2d1	cintránský
šlechtičny	šlechtična	k1gFnSc2	šlechtična
a	a	k8xC	a
banditka	banditka	k1gFnSc1	banditka
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
Geraltovi	Geralt	k1gMnSc3	Geralt
hledat	hledat	k5eAaImF	hledat
Ciri	Cire	k1gFnSc4	Cire
<g/>
.	.	kIx.	.
</s>
<s>
Milwa	Milwa	k6eAd1	Milwa
je	být	k5eAaImIp3nS	být
dívka	dívka	k1gFnSc1	dívka
žijící	žijící	k2eAgFnSc1d1	žijící
mezi	mezi	k7c7	mezi
dryádami	dryáda	k1gFnPc7	dryáda
<g/>
,	,	kIx,	,
průvodkyně	průvodkyně	k1gFnPc1	průvodkyně
elfských	elfský	k2eAgMnPc2d1	elfský
utečenců	utečenec	k1gMnPc2	utečenec
a	a	k8xC	a
zručná	zručný	k2eAgFnSc1d1	zručná
lukostřelkyně	lukostřelkyně	k1gFnSc1	lukostřelkyně
<g/>
;	;	kIx,	;
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
Geralta	Geralta	k1gMnSc1	Geralta
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
za	za	k7c4	za
Ciri	Cire	k1gFnSc4	Cire
<g/>
.	.	kIx.	.
</s>
<s>
Cahir	Cahir	k1gMnSc1	Cahir
je	být	k5eAaImIp3nS	být
nilfgaardský	nilfgaardský	k2eAgMnSc1d1	nilfgaardský
rytíř	rytíř	k1gMnSc1	rytíř
a	a	k8xC	a
odpadlík	odpadlík	k1gMnSc1	odpadlík
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
postava	postava	k1gFnSc1	postava
nilfgaardského	nilfgaardský	k2eAgNnSc2d1	nilfgaardský
pátrání	pátrání	k1gNnSc2	pátrání
po	po	k7c6	po
Ciri	Cir	k1gFnSc6	Cir
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
ke	k	k7c3	k
Geraltově	Geraltův	k2eAgFnSc3d1	Geraltova
družině	družina	k1gFnSc3	družina
<g/>
.	.	kIx.	.
</s>
<s>
Bonhart	Bonhart	k1gInSc1	Bonhart
je	být	k5eAaImIp3nS	být
nájemný	nájemný	k2eAgMnSc1d1	nájemný
lovec	lovec	k1gMnSc1	lovec
lidí	člověk	k1gMnPc2	člověk
pronásledující	pronásledující	k2eAgFnSc4d1	pronásledující
Ciri	Cire	k1gFnSc4	Cire
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
družinu	družina	k1gFnSc4	družina
<g/>
.	.	kIx.	.
</s>
<s>
Foltest	Foltest	k1gMnSc1	Foltest
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
Temerie	Temerie	k1gFnSc2	Temerie
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
Soddenu	Sodden	k1gInSc2	Sodden
<g/>
,	,	kIx,	,
suverénní	suverénní	k2eAgMnSc1d1	suverénní
vládce	vládce	k1gMnSc1	vládce
Mahakamu	Mahakam	k1gInSc2	Mahakam
a	a	k8xC	a
Pontaru	Pontar	k1gInSc2	Pontar
<g/>
,	,	kIx,	,
protektor	protektor	k1gMnSc1	protektor
Brugge	Brugg	k1gFnSc2	Brugg
a	a	k8xC	a
Ellanderu	Ellander	k1gInSc2	Ellander
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
také	také	k9	také
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Zaklínač	zaklínač	k1gMnSc1	zaklínač
2	[number]	k4	2
<g/>
:	:	kIx,	:
Vrahové	vrah	k1gMnPc1	vrah
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Emhyr	Emhyr	k1gInSc1	Emhyr
var	var	k1gInSc1	var
Emreis	Emreis	k1gInSc4	Emreis
je	být	k5eAaImIp3nS	být
císař	císař	k1gMnSc1	císař
Nilfgaardu	Nilfgaard	k1gInSc2	Nilfgaard
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
říše	říše	k1gFnSc1	říše
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k5eAaPmF	Geralt
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkal	setkat	k5eAaPmAgMnS	setkat
již	již	k6eAd1	již
v	v	k7c6	v
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
Poslední	poslední	k2eAgNnSc1d1	poslední
přání	přání	k1gNnSc1	přání
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
Geralt	Geralt	k1gMnSc1	Geralt
neměl	mít	k5eNaImAgMnS	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
.	.	kIx.	.
</s>
<s>
Wiedźmin	Wiedźmin	k1gInSc1	Wiedźmin
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
REPORTER	REPORTER	kA	REPORTER
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Miecz	Miecz	k1gInSc1	Miecz
przeznaczenia	przeznaczenium	k1gNnSc2	przeznaczenium
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Ostatnie	Ostatnie	k1gFnSc1	Ostatnie
życzenie	życzenie	k1gFnSc1	życzenie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Poslední	poslední	k2eAgNnSc4d1	poslední
přání	přání	k1gNnSc4	přání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Coś	Coś	k1gFnSc1	Coś
się	się	k?	się
kończy	kończa	k1gFnSc2	kończa
<g/>
,	,	kIx,	,
coś	coś	k?	coś
się	się	k?	się
zaczyna	zaczyn	k1gInSc2	zaczyn
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Něco	něco	k3yInSc1	něco
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
meč	meč	k1gInSc1	meč
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900912	[number]	k4	900912
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
:	:	kIx,	:
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
,	,	kIx,	,
Zrnko	zrnko	k1gNnSc1	zrnko
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
Menší	malý	k2eAgNnSc1d2	menší
zlo	zlo	k1gNnSc1	zlo
<g/>
,	,	kIx,	,
Otázka	otázka	k1gFnSc1	otázka
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
není	být	k5eNaImIp3nS	být
návratu	návrat	k1gInSc6	návrat
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Věčný	věčný	k2eAgInSc1d1	věčný
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85643	[number]	k4	85643
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
:	:	kIx,	:
Hranice	hranice	k1gFnSc1	hranice
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
Střípek	střípek	k1gInSc4	střípek
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
Věčný	věčný	k2eAgInSc1d1	věčný
oheň	oheň	k1gInSc1	oheň
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85643	[number]	k4	85643
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
:	:	kIx,	:
Trošku	trošku	k6eAd1	trošku
se	se	k3xPyFc4	se
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
,	,	kIx,	,
Meč	meč	k1gInSc4	meč
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
Něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
Zaklínač	zaklínač	k1gMnSc1	zaklínač
I.	I.	kA	I.
-	-	kIx~	-
Poslední	poslední	k2eAgNnSc4d1	poslední
přání	přání	k1gNnSc4	přání
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
meč	meč	k1gInSc4	meč
byly	být	k5eAaImAgFnP	být
doplněny	doplněn	k2eAgFnPc1d1	doplněna
a	a	k8xC	a
nově	nově	k6eAd1	nově
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
není	být	k5eNaImIp3nS	být
návratu	návrat	k1gInSc3	návrat
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc4	hlas
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
rámcový	rámcový	k2eAgInSc1d1	rámcový
příběh	příběh	k1gInSc1	příběh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
,	,	kIx,	,
Zrnko	zrnko	k1gNnSc1	zrnko
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
Menší	malý	k2eAgNnSc1d2	menší
zlo	zlo	k1gNnSc1	zlo
<g/>
,	,	kIx,	,
Otázka	otázka	k1gFnSc1	otázka
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgNnSc1d1	poslední
přání	přání	k1gNnSc1	přání
Zaklínač	zaklínač	k1gMnSc1	zaklínač
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
povídek	povídka	k1gFnPc2	povídka
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Věčný	věčný	k2eAgInSc4d1	věčný
oheň	oheň	k1gInSc4	oheň
a	a	k8xC	a
Meč	meč	k1gInSc4	meč
osudu	osud	k1gInSc2	osud
-	-	kIx~	-
Hranice	hranice	k1gFnSc1	hranice
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
Střípek	střípek	k1gInSc4	střípek
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
Věčný	věčný	k2eAgInSc1d1	věčný
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
Trošku	trošku	k6eAd1	trošku
se	se	k3xPyFc4	se
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
,	,	kIx,	,
Meč	meč	k1gInSc4	meč
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
Něco	něco	k3yInSc1	něco
více	hodně	k6eAd2	hodně
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
bonusovou	bonusový	k2eAgFnSc4d1	bonusová
<g/>
,	,	kIx,	,
humorně	humorně	k6eAd1	humorně
laděnou	laděný	k2eAgFnSc4d1	laděná
povídku	povídka	k1gFnSc4	povídka
Něco	něco	k3yInSc1	něco
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k8xS	jako
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
Geraltovi	Geralt	k1gMnSc6	Geralt
a	a	k8xC	a
Ciri	Cir	k1gInSc3	Cir
či	či	k8xC	či
Sága	sága	k1gFnSc1	sága
Věže	věž	k1gFnSc2	věž
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nových	nový	k2eAgNnPc6d1	nové
vydáních	vydání	k1gNnPc6	vydání
titul	titul	k1gInSc4	titul
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
knihy	kniha	k1gFnPc4	kniha
Zaklínač	zaklínač	k1gMnSc1	zaklínač
III	III	kA	III
<g/>
.	.	kIx.	.
až	až	k9	až
Zaklínač	zaklínač	k1gMnSc1	zaklínač
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Krev	krev	k1gFnSc1	krev
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
Zaklínač	zaklínač	k1gMnSc1	zaklínač
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-85951-69-1	[number]	k4	978-80-85951-69-1
(	(	kIx(	(
<g/>
Krew	Krew	k1gFnSc1	Krew
elfów	elfów	k?	elfów
<g/>
,	,	kIx,	,
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Čas	čas	k1gInSc1	čas
opovržení	opovržení	k1gNnSc2	opovržení
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
Zaklínač	zaklínač	k1gMnSc1	zaklínač
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-85951-70-7	[number]	k4	978-80-85951-70-7
(	(	kIx(	(
<g/>
Czas	Czas	k1gInSc1	Czas
pogardy	pogarda	k1gFnSc2	pogarda
<g/>
,	,	kIx,	,
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Křest	křest	k1gInSc1	křest
ohněm	oheň	k1gInSc7	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
;	;	kIx,	;
Zaklínač	zaklínač	k1gMnSc1	zaklínač
V.	V.	kA	V.
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-85951-71-4	[number]	k4	978-80-85951-71-4
(	(	kIx(	(
<g/>
Chrzest	Chrzest	k1gFnSc1	Chrzest
ognia	ognia	k1gFnSc1	ognia
<g/>
,	,	kIx,	,
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Věž	věž	k1gFnSc1	věž
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-X	-X	k?	-X
<g/>
;	;	kIx,	;
Zaklínač	zaklínač	k1gMnSc1	zaklínač
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-85951-73-8	[number]	k4	978-80-85951-73-8
(	(	kIx(	(
<g/>
Wieża	Wież	k1gInSc2	Wież
Jaskółki	Jaskółki	k1gNnSc2	Jaskółki
<g/>
,	,	kIx,	,
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Paní	paní	k1gFnSc1	paní
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85951	[number]	k4	85951
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
Zaklínač	zaklínač	k1gMnSc1	zaklínač
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-85951-74-5	[number]	k4	978-80-85951-74-5
(	(	kIx(	(
<g/>
Pani	Pani	k1gNnSc1	Pani
Jeziora	Jeziora	k1gFnSc1	Jeziora
<g/>
,	,	kIx,	,
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Bouřková	bouřkový	k2eAgFnSc1d1	bouřková
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7477-050-0	[number]	k4	978-80-7477-050-0
(	(	kIx(	(
<g/>
Sezon	sezona	k1gFnPc2	sezona
burz	burza	k1gFnPc2	burza
<g/>
,	,	kIx,	,
SuperNOWA	SuperNOWA	k1gFnSc1	SuperNOWA
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
prequel	prequel	k1gInSc1	prequel
k	k	k7c3	k
Sáze	sága	k1gFnSc3	sága
o	o	k7c6	o
Geraltovi	Geraltův	k2eAgMnPc1d1	Geraltův
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
vznikaly	vznikat	k5eAaImAgFnP	vznikat
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
autor	autor	k1gMnSc1	autor
vsadil	vsadit	k5eAaPmAgMnS	vsadit
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
rámce	rámec	k1gInSc2	rámec
pětidílný	pětidílný	k2eAgInSc1d1	pětidílný
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gInSc1	Geralt
z	z	k7c2	z
Rivie	Rivie	k1gFnSc2	Rivie
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
těchto	tento	k3xDgFnPc2	tento
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
dětí	dítě	k1gFnPc2	dítě
narodil	narodit	k5eAaPmAgInS	narodit
nechtěn	chtěn	k2eNgMnSc1d1	chtěn
a	a	k8xC	a
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
zaklínačský	zaklínačský	k2eAgInSc4d1	zaklínačský
výcvik	výcvik	k1gInSc4	výcvik
v	v	k7c4	v
Kaer	Kaer	k1gInSc4	Kaer
Morhen	Morhen	k2eAgInSc4d1	Morhen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
dále	daleko	k6eAd2	daleko
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Geraltův	Geraltův	k2eAgMnSc1d1	Geraltův
přítel	přítel	k1gMnSc1	přítel
Marigold	Marigold	k1gMnSc1	Marigold
<g/>
,	,	kIx,	,
trubadúr	trubadúr	k1gMnSc1	trubadúr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
šlamastyce	šlamastyka	k1gFnSc6	šlamastyka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
citů	cit	k1gInPc2	cit
a	a	k8xC	a
známostí	známost	k1gFnPc2	známost
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
také	také	k9	také
popisují	popisovat	k5eAaImIp3nP	popisovat
Geraltovo	Geraltův	k2eAgNnSc4d1	Geraltovo
seznámení	seznámení	k1gNnSc4	seznámení
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
čarodějkou	čarodějka	k1gFnSc7	čarodějka
Yennefer	Yennefra	k1gFnPc2	Yennefra
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
jako	jako	k8xS	jako
partneři	partner	k1gMnPc1	partner
komplikovaný	komplikovaný	k2eAgMnSc1d1	komplikovaný
a	a	k8xC	a
-	-	kIx~	-
hádkami	hádka	k1gFnPc7	hádka
i	i	k8xC	i
dějem	děj	k1gInSc7	děj
-	-	kIx~	-
přerušovaný	přerušovaný	k2eAgInSc4d1	přerušovaný
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
počítaje	počítat	k5eAaImSgMnS	počítat
i	i	k9	i
ságu	sága	k1gFnSc4	sága
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
a	a	k8xC	a
opět	opět	k6eAd1	opět
usmiřují	usmiřovat	k5eAaImIp3nP	usmiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gInSc1	Geralt
chová	chovat	k5eAaImIp3nS	chovat
jisté	jistý	k2eAgNnSc4d1	jisté
city	city	k1gNnSc4	city
též	též	k9	též
k	k	k7c3	k
mladé	mladý	k2eAgFnSc3d1	mladá
čarodějce	čarodějka	k1gFnSc3	čarodějka
Triss	Trissa	k1gFnPc2	Trissa
Ranuncul	Ranuncul	k1gInSc1	Ranuncul
<g/>
,	,	kIx,	,
blízké	blízký	k2eAgFnSc6d1	blízká
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Yennefer	Yennefra	k1gFnPc2	Yennefra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
postavou	postava	k1gFnSc7	postava
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
královská	královský	k2eAgFnSc1d1	královská
vnučka	vnučka	k1gFnSc1	vnučka
Ciri	Ciri	k1gNnSc2	Ciri
(	(	kIx(	(
<g/>
Cirilla	Cirillo	k1gNnSc2	Cirillo
Fiona	Fion	k1gInSc2	Fion
Elen	Elena	k1gFnPc2	Elena
Riannon	Riannona	k1gFnPc2	Riannona
z	z	k7c2	z
Cintry	Cintr	k1gMnPc7	Cintr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
Geraltovi	Geralt	k1gMnSc3	Geralt
připoutána	připoutat	k5eAaPmNgFnS	připoutat
sudbou	sudba	k1gFnSc7	sudba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gFnPc4	on
nerozlučně	rozlučně	k6eNd1	rozlučně
pojí	pojit	k5eAaImIp3nP	pojit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
jako	jako	k9	jako
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc4	vydání
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
není	být	k5eNaImIp3nS	být
návratu	návrat	k1gInSc2	návrat
<g/>
:	:	kIx,	:
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
před	před	k7c7	před
Geraltovým	Geraltův	k2eAgNnSc7d1	Geraltovo
narozením	narození	k1gNnSc7	narození
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zde	zde	k6eAd1	zde
Geraltova	Geraltův	k2eAgFnSc1d1	Geraltova
matka	matka	k1gFnSc1	matka
Visenna	Visenna	k1gFnSc1	Visenna
a	a	k8xC	a
rytíř	rytíř	k1gMnSc1	rytíř
Korin	Korina	k1gFnPc2	Korina
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
musí	muset	k5eAaImIp3nP	muset
zbavit	zbavit	k5eAaPmF	zbavit
kraj	kraj	k1gInSc4	kraj
magické	magický	k2eAgFnSc2d1	magická
stvůry	stvůra	k1gFnSc2	stvůra
<g/>
,	,	kIx,	,
kostěje	kostět	k5eAaPmIp3nS	kostět
<g/>
.	.	kIx.	.
</s>
<s>
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
:	:	kIx,	:
Popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Geralt	Geralt	k1gInSc1	Geralt
zlomí	zlomit	k5eAaPmIp3nS	zlomit
kletbu	kletba	k1gFnSc4	kletba
dcery	dcera	k1gFnSc2	dcera
krále	král	k1gMnSc2	král
Foltesta	Foltest	k1gMnSc2	Foltest
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
začarována	začarovat	k5eAaPmNgFnS	začarovat
ve	v	k7c4	v
strigu	striga	k1gFnSc4	striga
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
rozumu	rozum	k1gInSc2	rozum
<g/>
:	:	kIx,	:
Povídka	povídka	k1gFnSc1	povídka
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
Geraltova	Geraltův	k2eAgNnSc2d1	Geraltovo
uzdravování	uzdravování	k1gNnSc2	uzdravování
ve	v	k7c6	v
svatyni	svatyně	k1gFnSc6	svatyně
Melitelé	Melitel	k1gMnPc1	Melitel
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
jako	jako	k8xC	jako
rámcový	rámcový	k2eAgInSc4d1	rámcový
příběh	příběh	k1gInSc4	příběh
do	do	k7c2	do
knihy	kniha	k1gFnSc2	kniha
Zaklínač	zaklínač	k1gMnSc1	zaklínač
I	I	kA	I
-	-	kIx~	-
Poslední	poslední	k2eAgNnSc4d1	poslední
přání	přání	k1gNnSc4	přání
<g/>
.	.	kIx.	.
</s>
<s>
Zrnko	zrnko	k1gNnSc1	zrnko
pravdy	pravda	k1gFnSc2	pravda
<g/>
:	:	kIx,	:
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
pohádku	pohádka	k1gFnSc4	pohádka
Panna	Panna	k1gFnSc1	Panna
a	a	k8xC	a
netvor	netvor	k1gMnSc1	netvor
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gMnSc1	Geralt
je	být	k5eAaImIp3nS	být
hostem	host	k1gMnSc7	host
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
zakletého	zakletý	k2eAgMnSc4d1	zakletý
Nivellena	Nivellen	k2eAgMnSc4d1	Nivellen
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
zbaví	zbavit	k5eAaPmIp3nP	zbavit
upírky	upírek	k1gMnPc4	upírek
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mu	on	k3xPp3gMnSc3	on
vrátil	vrátit	k5eAaPmAgMnS	vrátit
lidskou	lidský	k2eAgFnSc4d1	lidská
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnSc1d2	menší
zlo	zlo	k1gNnSc1	zlo
<g/>
:	:	kIx,	:
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
pohádku	pohádka	k1gFnSc4	pohádka
O	o	k7c6	o
Sněhurce	Sněhurka	k1gFnSc6	Sněhurka
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
proradnou	proradný	k2eAgFnSc7d1	proradná
Renfri	Renfre	k1gFnSc4	Renfre
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
Geralt	Geralt	k1gMnSc1	Geralt
získá	získat	k5eAaPmIp3nS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
Řezník	řezník	k1gMnSc1	řezník
z	z	k7c2	z
Blavikenu	Blaviken	k1gInSc2	Blaviken
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
ceny	cena	k1gFnSc2	cena
<g/>
:	:	kIx,	:
Během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Cintře	Cintra	k1gFnSc6	Cintra
Geralt	Geralta	k1gFnPc2	Geralta
zachrání	zachránit	k5eAaPmIp3nS	zachránit
život	život	k1gInSc1	život
rytíři	rytíř	k1gMnSc3	rytíř
Ježkovi	Ježek	k1gMnSc3	Ježek
a	a	k8xC	a
princezně	princezna	k1gFnSc3	princezna
Pavettě	Pavetta	k1gFnSc3	Pavetta
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
budoucí	budoucí	k2eAgFnSc3d1	budoucí
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pavetta	Pavetta	k1gMnSc1	Pavetta
slíbí	slíbit	k5eAaPmIp3nP	slíbit
Geraltovi	Geraltův	k2eAgMnPc1d1	Geraltův
za	za	k7c4	za
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
odevzdat	odevzdat	k5eAaPmF	odevzdat
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
Geralt	Geralt	k1gMnSc1	Geralt
a	a	k8xC	a
Marigold	Marigold	k1gMnSc1	Marigold
se	se	k3xPyFc4	se
na	na	k7c6	na
lovu	lov	k1gInSc2	lov
sylvána	sylvat	k5eAaPmNgFnS	sylvat
-	-	kIx~	-
napůl	napůl	k6eAd1	napůl
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
kozla	kozel	k1gMnSc2	kozel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc4d1	podobný
Panovi	Panův	k2eAgMnPc1d1	Panův
-	-	kIx~	-
setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
živořícími	živořící	k2eAgMnPc7d1	živořící
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
soužití	soužití	k1gNnSc2	soužití
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc2	nenávist
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
rasami	rasa	k1gFnPc7	rasa
je	být	k5eAaImIp3nS	být
zdůrazněn	zdůrazněn	k2eAgMnSc1d1	zdůrazněn
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
sérii	série	k1gFnSc6	série
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
přání	přání	k1gNnPc1	přání
<g/>
:	:	kIx,	:
Geralt	Geralt	k1gInSc1	Geralt
a	a	k8xC	a
Yennefer	Yennefer	k1gInSc1	Yennefer
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkávají	setkávat	k5eAaImIp3nP	setkávat
a	a	k8xC	a
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
džina	džin	k1gMnSc2	džin
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vzniká	vznikat	k5eAaImIp3nS	vznikat
pouto	pouto	k1gNnSc1	pouto
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
Povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
lovu	lov	k1gInSc6	lov
na	na	k7c4	na
zlatého	zlatý	k2eAgMnSc4d1	zlatý
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Střípek	střípek	k1gInSc1	střípek
ledu	led	k1gInSc2	led
<g/>
:	:	kIx,	:
Milostný	milostný	k2eAgInSc1d1	milostný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
mezi	mezi	k7c4	mezi
Yennefer	Yennefer	k1gInSc4	Yennefer
<g/>
,	,	kIx,	,
Geraltem	Geralt	k1gInSc7	Geralt
a	a	k8xC	a
čarodějem	čaroděj	k1gMnSc7	čaroděj
Istreddem	Istredd	k1gMnSc7	Istredd
<g/>
.	.	kIx.	.
</s>
<s>
Věčný	věčný	k2eAgInSc1d1	věčný
oheň	oheň	k1gInSc1	oheň
<g/>
:	:	kIx,	:
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
doppler	doppler	k1gInSc1	doppler
<g/>
,	,	kIx,	,
stvoření	stvoření	k1gNnSc1	stvoření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
měnit	měnit	k5eAaImF	měnit
nejen	nejen	k6eAd1	nejen
své	svůj	k3xOyFgFnPc4	svůj
tělesné	tělesný	k2eAgFnPc4d1	tělesná
proporce	proporce	k1gFnPc4	proporce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
myšlenky	myšlenka	k1gFnPc1	myšlenka
proměňujíc	proměňovat	k5eAaImSgFnS	proměňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
bytosti	bytost	k1gFnSc6	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Trochu	trochu	k6eAd1	trochu
se	se	k3xPyFc4	se
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
:	:	kIx,	:
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
pohádku	pohádka	k1gFnSc4	pohádka
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
<g/>
:	:	kIx,	:
První	první	k4xOgNnSc4	první
skutečné	skutečný	k2eAgNnSc4d1	skutečné
Geraltovo	Geraltův	k2eAgNnSc4d1	Geraltovo
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Ciri	Ciri	k1gNnSc7	Ciri
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
lese	les	k1gInSc6	les
Brokilon	Brokilon	k1gInSc1	Brokilon
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
vládnou	vládnout	k5eAaImIp3nP	vládnout
dryády	dryáda	k1gFnPc4	dryáda
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
více	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
Zaklínače	zaklínač	k1gMnSc4	zaklínač
zraněného	zraněný	k2eAgMnSc4d1	zraněný
po	po	k7c6	po
šarvátce	šarvátka	k1gFnSc6	šarvátka
s	s	k7c7	s
netvory	netvor	k1gMnPc7	netvor
přiveze	přivézt	k5eAaPmIp3nS	přivézt
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
statek	statek	k1gInSc4	statek
sedlák	sedlák	k1gMnSc1	sedlák
a	a	k8xC	a
poví	povědět	k5eAaPmIp3nS	povědět
mu	on	k3xPp3gMnSc3	on
o	o	k7c6	o
nedávném	dávný	k2eNgNnSc6d1	nedávné
zničení	zničení	k1gNnSc6	zničení
Cintry	Cintr	k1gMnPc4	Cintr
nilfgaardským	nilfgaardský	k2eAgNnSc7d1	nilfgaardský
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
končí	končit	k5eAaImIp3nS	končit
Geraltovým	Geraltův	k2eAgInSc7d1	Geraltův
druhým	druhý	k4xOgNnSc7	druhý
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
Ciri	Ciri	k1gNnSc7	Ciri
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
:	:	kIx,	:
Humorně	humorně	k6eAd1	humorně
pojaté	pojatý	k2eAgNnSc1d1	pojaté
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
svatbě	svatba	k1gFnSc6	svatba
Geralta	Geralt	k1gInSc2	Geralt
a	a	k8xC	a
Yennefer	Yennefero	k1gNnPc2	Yennefero
připojené	připojený	k2eAgInPc1d1	připojený
jako	jako	k8xS	jako
epilog	epilog	k1gInSc1	epilog
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Zaklínač	zaklínač	k1gMnSc1	zaklínač
II	II	kA	II
-	-	kIx~	-
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
je	být	k5eAaImIp3nS	být
románové	románový	k2eAgNnSc1d1	románové
pokračování	pokračování	k1gNnSc1	pokračování
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
ságy	sága	k1gFnSc2	sága
je	být	k5eAaImIp3nS	být
vsazen	vsadit	k5eAaPmNgInS	vsadit
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
vleklé	vleklý	k2eAgFnSc2d1	vleklá
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
císařstvím	císařství	k1gNnSc7	císařství
Nilfgaard	Nilfgaarda	k1gFnPc2	Nilfgaarda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
pevnou	pevný	k2eAgFnSc7d1	pevná
aliancí	aliance	k1gFnSc7	aliance
severních	severní	k2eAgNnPc2d1	severní
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
obyvatele	obyvatel	k1gMnSc2	obyvatel
Nilfgaarďané	Nilfgaarďaná	k1gFnSc2	Nilfgaarďaná
nazývají	nazývat	k5eAaImIp3nP	nazývat
Nordlingové	Nordlingový	k2eAgFnPc1d1	Nordlingový
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
konfliktu	konflikt	k1gInSc6	konflikt
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
zmínky	zmínka	k1gFnSc2	zmínka
již	již	k9	již
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
povídkách	povídka	k1gFnPc6	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Kaer	Kaer	k1gInSc1	Kaer
Morhen	Morhna	k1gFnPc2	Morhna
je	být	k5eAaImIp3nS	být
zaklínačské	zaklínačský	k2eAgNnSc4d1	zaklínačské
hradiště	hradiště	k1gNnSc4	hradiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
cvičí	cvičit	k5eAaImIp3nP	cvičit
mladí	mladý	k2eAgMnPc1d1	mladý
muži	muž	k1gMnPc1	muž
vybraní	vybraný	k2eAgMnPc1d1	vybraný
osudem	osud	k1gInSc7	osud
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jim	on	k3xPp3gFnPc3	on
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
zabíjení	zabíjení	k1gNnPc4	zabíjení
netvorů	netvor	k1gMnPc2	netvor
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
připutuje	připutovat	k5eAaPmIp3nS	připutovat
Geralt	Geralt	k1gInSc1	Geralt
s	s	k7c7	s
Ciri	Ciri	k1gNnSc7	Ciri
<g/>
,	,	kIx,	,
královskou	královský	k2eAgFnSc7d1	královská
vnučkou	vnučka	k1gFnSc7	vnučka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
věnovaném	věnovaný	k2eAgInSc6d1	věnovaný
výcviku	výcvik	k1gInSc3	výcvik
zaklínači	zaklínač	k1gMnSc3	zaklínač
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ciri	Ciri	k1gNnSc1	Ciri
není	být	k5eNaImIp3nS	být
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Zřídlo	zřídlo	k1gNnSc1	zřídlo
-	-	kIx~	-
tj.	tj.	kA	tj.
má	mít	k5eAaImIp3nS	mít
magické	magický	k2eAgFnPc4d1	magická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějka	čarodějka	k1gFnSc1	čarodějka
Triss	Trissa	k1gFnPc2	Trissa
<g/>
,	,	kIx,	,
povolaná	povolaný	k2eAgFnSc1d1	povolaná
k	k	k7c3	k
rozřešení	rozřešení	k1gNnSc3	rozřešení
dívčiných	dívčin	k2eAgFnPc2d1	dívčina
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
však	však	k9	však
do	do	k7c2	do
záležitosti	záležitost	k1gFnSc2	záležitost
vnese	vnést	k5eAaPmIp3nS	vnést
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc1d2	veliký
zmatek	zmatek	k1gInSc1	zmatek
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
silné	silný	k2eAgNnSc4d1	silné
Zřídlo	zřídlo	k1gNnSc4	zřídlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Ciri	Cire	k1gFnSc4	Cire
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
nezvládne	zvládnout	k5eNaPmIp3nS	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gMnSc1	Geralt
je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
svou	svůj	k3xOyFgFnSc4	svůj
malou	malý	k2eAgFnSc4d1	malá
chráněnku	chráněnka	k1gFnSc4	chráněnka
odvést	odvést	k5eAaPmF	odvést
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
naučila	naučit	k5eAaPmAgFnS	naučit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zvládnout	zvládnout	k5eAaPmF	zvládnout
magii	magie	k1gFnSc4	magie
<g/>
:	:	kIx,	:
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
bývalé	bývalý	k2eAgFnSc3d1	bývalá
milence	milenka	k1gFnSc3	milenka
Yennefer	Yennefer	k1gInSc4	Yennefer
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
Ciri	Cir	k1gMnPc7	Cir
ve	v	k7c4	v
svatyni	svatyně	k1gFnSc4	svatyně
bohyně	bohyně	k1gFnSc2	bohyně
Melitelé	Melitel	k1gMnPc1	Melitel
učí	učit	k5eAaImIp3nP	učit
kouzlům	kouzlo	k1gNnPc3	kouzlo
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nastane	nastat	k5eAaPmIp3nS	nastat
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
učitelkou	učitelka	k1gFnSc7	učitelka
musí	muset	k5eAaImIp3nS	muset
odjet	odjet	k5eAaPmF	odjet
na	na	k7c4	na
konvent	konvent	k1gInSc4	konvent
všech	všecek	k3xTgMnPc2	všecek
nordlingských	nordlingský	k2eAgMnPc2d1	nordlingský
mágů	mág	k1gMnPc2	mág
a	a	k8xC	a
čarodějek	čarodějka	k1gFnPc2	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
Yennefer	Yennefer	k1gInSc1	Yennefer
a	a	k8xC	a
Ciri	Cir	k1gFnPc1	Cir
cestují	cestovat	k5eAaImIp3nP	cestovat
od	od	k7c2	od
kněžek	kněžka	k1gFnPc2	kněžka
bohyně	bohyně	k1gFnSc2	bohyně
Melitelé	Melitel	k1gMnPc1	Melitel
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Thanedd	Thanedda	k1gFnPc2	Thanedda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
veliký	veliký	k2eAgInSc1d1	veliký
konvent	konvent	k1gInSc1	konvent
mágů	mág	k1gMnPc2	mág
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Geralt	Geralt	k1gInSc1	Geralt
snaží	snažit	k5eAaImIp3nS	snažit
u	u	k7c2	u
učenějších	učený	k2eAgMnPc2d2	učenější
a	a	k8xC	a
vlivnějších	vlivný	k2eAgMnPc2d2	vlivnější
lidí	člověk	k1gMnPc2	člověk
zjistit	zjistit	k5eAaPmF	zjistit
původ	původ	k1gInSc4	původ
této	tento	k3xDgFnSc2	tento
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojena	spojit	k5eAaPmNgFnS	spojit
sudbou	sudba	k1gFnSc7	sudba
<g/>
.	.	kIx.	.
</s>
<s>
Králové	Král	k1gMnPc1	Král
Nordlingů	Nordling	k1gInPc2	Nordling
plánují	plánovat	k5eAaImIp3nP	plánovat
zničit	zničit	k5eAaPmF	zničit
svého	svůj	k3xOyFgMnSc4	svůj
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
hrozivý	hrozivý	k2eAgMnSc1d1	hrozivý
Nilfgaard	Nilfgaard	k1gMnSc1	Nilfgaard
-	-	kIx~	-
netuší	tušit	k5eNaImIp3nS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
krok	krok	k1gInSc4	krok
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
vojska	vojsko	k1gNnPc1	vojsko
jsou	být	k5eAaImIp3nP	být
připravena	připravit	k5eAaPmNgNnP	připravit
na	na	k7c4	na
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přičiněním	přičinění	k1gNnSc7	přičinění
Ciri	Ciri	k1gNnSc2	Ciri
se	se	k3xPyFc4	se
Yennefer	Yennefer	k1gInSc1	Yennefer
a	a	k8xC	a
Geralt	Geralt	k1gInSc1	Geralt
na	na	k7c6	na
konventu	konvent	k1gInSc6	konvent
setkají	setkat	k5eAaPmIp3nP	setkat
a	a	k8xC	a
usmíří	usmířit	k5eAaPmIp3nP	usmířit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc2	jejich
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
mágů	mág	k1gMnPc2	mág
se	se	k3xPyFc4	se
ale	ale	k9	ale
zvrhne	zvrhnout	k5eAaPmIp3nS	zvrhnout
v	v	k7c6	v
jatka	jatka	k1gFnSc1	jatka
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
spolu	spolu	k6eAd1	spolu
bojují	bojovat	k5eAaImIp3nP	bojovat
čarodějní	čarodějný	k2eAgMnPc1d1	čarodějný
přívrženci	přívrženec	k1gMnPc1	přívrženec
Nordlingů	Nordling	k1gInPc2	Nordling
a	a	k8xC	a
Nilfgaardu	Nilfgaard	k1gInSc2	Nilfgaard
<g/>
.	.	kIx.	.
</s>
<s>
Ciri	Ciri	k1gNnSc1	Ciri
<g/>
,	,	kIx,	,
pronásledovaná	pronásledovaný	k2eAgFnSc1d1	pronásledovaná
hned	hned	k6eAd1	hned
několika	několik	k4yIc7	několik
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
Thaneddu	Thanedd	k1gInSc6	Thanedd
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
Tor	Tor	k1gMnSc1	Tor
Lara	Larus	k1gMnSc2	Larus
-	-	kIx~	-
Věže	věž	k1gFnSc2	věž
racka	racek	k1gMnSc2	racek
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
rozbitého	rozbitý	k2eAgInSc2d1	rozbitý
vyhaslého	vyhaslý	k2eAgInSc2d1	vyhaslý
portálu	portál	k1gInSc2	portál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zanáší	zanášet	k5eAaImIp3nS	zanášet
neznámo	neznámo	k6eAd1	neznámo
kam	kam	k6eAd1	kam
<g/>
,	,	kIx,	,
a	a	k8xC	a
teleportuje	teleportovat	k5eAaBmIp3nS	teleportovat
se	se	k3xPyFc4	se
na	na	k7c4	na
poušť	poušť	k1gFnSc4	poušť
Korath	Koratha	k1gFnPc2	Koratha
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
by	by	kYmCp3nS	by
<g/>
,	,	kIx,	,
nebýt	být	k5eNaImF	být
mladého	mladý	k2eAgMnSc4d1	mladý
jednorožce	jednorožec	k1gMnSc4	jednorožec
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
vlastních	vlastní	k2eAgFnPc2d1	vlastní
magických	magický	k2eAgFnPc2d1	magická
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
útrapách	útrapa	k1gFnPc6	útrapa
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Ciri	Cir	k1gInPc7	Cir
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
lupičské	lupičský	k2eAgFnSc2d1	lupičská
bandy	banda	k1gFnSc2	banda
Potkanů	potkan	k1gMnPc2	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gMnSc1	Geralt
se	se	k3xPyFc4	se
po	po	k7c6	po
bojích	boj	k1gInPc6	boj
na	na	k7c4	na
Thaneddu	Thanedda	k1gFnSc4	Thanedda
zotavuje	zotavovat	k5eAaImIp3nS	zotavovat
u	u	k7c2	u
dryád	dryáda	k1gFnPc2	dryáda
v	v	k7c6	v
Brokilonu	Brokilon	k1gInSc6	Brokilon
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Marigoldem	Marigold	k1gInSc7	Marigold
a	a	k8xC	a
lučištnicí	lučištnice	k1gFnSc7	lučištnice
Milwou	Milwa	k1gFnSc7	Milwa
hledat	hledat	k5eAaImF	hledat
Ciri	Cir	k1gFnPc4	Cir
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nP	připojit
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
utíkající	utíkající	k2eAgFnSc2d1	utíkající
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
gnóma	gnóm	k1gMnSc2	gnóm
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
trpaslíci	trpaslík	k1gMnPc1	trpaslík
chrání	chránit	k5eAaImIp3nP	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gMnSc1	Geralt
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pozná	poznat	k5eAaPmIp3nS	poznat
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
starých	starý	k2eAgMnPc2d1	starý
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
trpaslíka	trpaslík	k1gMnSc2	trpaslík
Zoltana	Zoltan	k1gMnSc2	Zoltan
Chivaye	Chivay	k1gMnSc2	Chivay
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Nilfgaardu	Nilfgaard	k1gInSc2	Nilfgaard
potkají	potkat	k5eAaPmIp3nP	potkat
na	na	k7c6	na
starém	starý	k2eAgNnSc6d1	staré
elfském	elfský	k2eAgNnSc6d1	elfské
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
bylinkáře	bylinkář	k1gMnSc2	bylinkář
<g/>
,	,	kIx,	,
upíra	upír	k1gMnSc2	upír
jménem	jméno	k1gNnSc7	jméno
Regis	Regis	k1gFnSc2	Regis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
uhostí	uhostit	k5eAaPmIp3nP	uhostit
kořalkou	kořalka	k1gFnSc7	kořalka
z	z	k7c2	z
mandragory	mandragora	k1gFnSc2	mandragora
a	a	k8xC	a
připojí	připojit	k5eAaPmIp3nS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přidá	přidat	k5eAaPmIp3nS	přidat
také	také	k9	také
zběhlý	zběhlý	k2eAgMnSc1d1	zběhlý
nilfgaardský	nilfgaardský	k2eAgMnSc1d1	nilfgaardský
agent	agent	k1gMnSc1	agent
Cahir	Cahir	k1gMnSc1	Cahir
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gMnSc1	Geralt
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Ciri	Ciri	k1gNnSc1	Ciri
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
;	;	kIx,	;
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
tlupě	tlupa	k1gFnSc3	tlupa
loupeživých	loupeživý	k2eAgMnPc2d1	loupeživý
Potkanů	potkan	k1gMnPc2	potkan
a	a	k8xC	a
stopuje	stopovat	k5eAaImIp3nS	stopovat
ji	on	k3xPp3gFnSc4	on
nelítostný	lítostný	k2eNgMnSc1d1	nelítostný
lovec	lovec	k1gMnSc1	lovec
lidí	člověk	k1gMnPc2	člověk
Leo	Leo	k1gMnSc1	Leo
Bonhart	Bonhart	k1gInSc1	Bonhart
<g/>
.	.	kIx.	.
</s>
<s>
Regis	Regis	k1gFnSc1	Regis
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
cestovat	cestovat	k5eAaImF	cestovat
za	za	k7c4	za
druidy	druid	k1gMnPc4	druid
v	v	k7c4	v
Caed	Caed	k1gInSc4	Caed
Dhu	Dhu	k1gMnPc2	Dhu
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
pátrání	pátrání	k1gNnSc6	pátrání
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
zaklínač	zaklínač	k1gMnSc1	zaklínač
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Cahirem	Cahir	k1gInSc7	Cahir
pomůže	pomoct	k5eAaPmIp3nS	pomoct
vojákům	voják	k1gMnPc3	voják
z	z	k7c2	z
Lyrie	Lyrie	k1gFnSc2	Lyrie
a	a	k8xC	a
Rivie	Rivie	k1gFnSc2	Rivie
zahnat	zahnat	k5eAaPmF	zahnat
nilfgaardskou	nilfgaardský	k2eAgFnSc4d1	nilfgaardský
jízdu	jízda	k1gFnSc4	jízda
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
královna	královna	k1gFnSc1	královna
Meve	Mev	k1gInSc2	Mev
pasuje	pasovat	k5eAaImIp3nS	pasovat
na	na	k7c4	na
rytíře	rytíř	k1gMnSc4	rytíř
a	a	k8xC	a
udělí	udělit	k5eAaPmIp3nS	udělit
mu	on	k3xPp3gMnSc3	on
predikát	predikát	k1gInSc4	predikát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
právem	právem	k6eAd1	právem
může	moct	k5eAaImIp3nS	moct
nazývat	nazývat	k5eAaImF	nazývat
Geralt	Geralt	k1gInSc4	Geralt
z	z	k7c2	z
Rivie	Rivie	k1gFnSc2	Rivie
<g/>
.	.	kIx.	.
</s>
<s>
Vysogota	Vysogota	k1gFnSc1	Vysogota
z	z	k7c2	z
Corvia	Corvium	k1gNnSc2	Corvium
<g/>
,	,	kIx,	,
zneuznaný	zneuznaný	k2eAgMnSc1d1	zneuznaný
filozof	filozof	k1gMnSc1	filozof
žijící	žijící	k2eAgMnSc1d1	žijící
coby	coby	k?	coby
poustevník	poustevník	k1gMnSc1	poustevník
uprostřed	uprostřed	k7c2	uprostřed
močálů	močál	k1gInPc2	močál
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
těžce	těžce	k6eAd1	těžce
zraněnou	zraněný	k2eAgFnSc4d1	zraněná
Ciri	Cire	k1gFnSc4	Cire
<g/>
.	.	kIx.	.
</s>
<s>
Ujme	ujmout	k5eAaPmIp3nS	ujmout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
mu	on	k3xPp3gMnSc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stal	stát	k5eAaPmAgMnS	stát
nelítostný	lítostný	k2eNgInSc4d1	nelítostný
zabiják	zabiják	k1gInSc4	zabiják
a	a	k8xC	a
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
s	s	k7c7	s
bandou	banda	k1gFnSc7	banda
Potkanů	potkan	k1gMnPc2	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Skellen	Skellen	k2eAgMnSc1d1	Skellen
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
Nilfgaardu	Nilfgaard	k1gInSc2	Nilfgaard
<g/>
,	,	kIx,	,
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nasadil	nasadit	k5eAaPmAgMnS	nasadit
nájemného	nájemný	k2eAgMnSc4d1	nájemný
vraha	vrah	k1gMnSc4	vrah
Leo	Leo	k1gMnSc1	Leo
Bonharta	Bonhart	k1gMnSc4	Bonhart
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zabil	zabít	k5eAaPmAgMnS	zabít
její	její	k3xOp3gMnPc4	její
společníky	společník	k1gMnPc4	společník
a	a	k8xC	a
ji	on	k3xPp3gFnSc4	on
samou	samý	k3xTgFnSc4	samý
si	se	k3xPyFc3	se
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bojovala	bojovat	k5eAaImAgFnS	bojovat
pro	pro	k7c4	pro
potěšení	potěšení	k1gNnSc4	potěšení
davu	dav	k1gInSc2	dav
a	a	k8xC	a
pro	pro	k7c4	pro
Bonhartův	Bonhartův	k2eAgInSc4d1	Bonhartův
zisk	zisk	k1gInSc4	zisk
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Geralt	Geralt	k1gInSc1	Geralt
opouští	opouštět	k5eAaImIp3nS	opouštět
armádu	armáda	k1gFnSc4	armáda
královny	královna	k1gFnSc2	královna
Meve	Mev	k1gFnSc2	Mev
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
najít	najít	k5eAaPmF	najít
druidy	druid	k1gMnPc4	druid
<g/>
.	.	kIx.	.
</s>
<s>
Yennefer	Yennefer	k1gInSc1	Yennefer
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
Ciri	Cir	k1gInSc6	Cir
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zajata	zajat	k2eAgFnSc1d1	zajata
čarodějem	čaroděj	k1gMnSc7	čaroděj
Vilgefortzem	Vilgefortz	k1gMnSc7	Vilgefortz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
Ciri	Cir	k1gFnSc2	Cir
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
<g/>
.	.	kIx.	.
</s>
<s>
Uzdravená	uzdravený	k2eAgNnPc1d1	uzdravené
Ciri	Ciri	k1gNnPc1	Ciri
opouští	opouštět	k5eAaImIp3nP	opouštět
poustevníka	poustevník	k1gMnSc4	poustevník
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
své	svůj	k3xOyFgMnPc4	svůj
pronásledovatele	pronásledovatel	k1gMnPc4	pronásledovatel
a	a	k8xC	a
před	před	k7c7	před
Bonhartem	Bonhart	k1gInSc7	Bonhart
nakonec	nakonec	k6eAd1	nakonec
uniká	unikat	k5eAaImIp3nS	unikat
portálem	portál	k1gInSc7	portál
ve	v	k7c6	v
Věži	věž	k1gFnSc6	věž
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
čekala	čekat	k5eAaImAgFnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
na	na	k7c4	na
Thanedd	Thanedd	k1gInSc4	Thanedd
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
kdesi	kdesi	k6eAd1	kdesi
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
dimenzi	dimenze	k1gFnSc6	dimenze
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
vítá	vítat	k5eAaImIp3nS	vítat
elf	elf	k1gMnSc1	elf
Avallac	Avallac	k1gInSc1	Avallac
<g/>
'	'	kIx"	'
<g/>
h.	h.	k?	h.
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
dílu	díl	k1gInSc6	díl
ságy	sága	k1gFnSc2	sága
dojde	dojít	k5eAaPmIp3nS	dojít
na	na	k7c4	na
finální	finální	k2eAgNnSc4d1	finální
střetnutí	střetnutí	k1gNnSc4	střetnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
Geraltovy	Geraltův	k2eAgFnPc1d1	Geraltova
družiny	družina	k1gFnPc1	družina
s	s	k7c7	s
Vilgefortzem	Vilgefortz	k1gInSc7	Vilgefortz
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
vojsk	vojsko	k1gNnPc2	vojsko
nordlingských	nordlingských	k2eAgNnSc1d1	nordlingských
království	království	k1gNnSc1	království
proti	proti	k7c3	proti
nilfgaardskému	nilfgaardský	k2eAgMnSc3d1	nilfgaardský
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
pogromu	pogrom	k1gInSc6	pogrom
v	v	k7c6	v
Rivii	Rivie	k1gFnSc6	Rivie
je	být	k5eAaImIp3nS	být
Geralt	Geralt	k1gMnSc1	Geralt
těžce	těžce	k6eAd1	těžce
raněn	ranit	k5eAaPmNgMnS	ranit
a	a	k8xC	a
odplouvá	odplouvat	k5eAaImIp3nS	odplouvat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Yennefer	Yennefer	k1gInSc4	Yennefer
kamsi	kamsi	k6eAd1	kamsi
do	do	k7c2	do
neznáma	neznámo	k1gNnSc2	neznámo
<g/>
.	.	kIx.	.
</s>
<s>
Cirilla	Cirilla	k6eAd1	Cirilla
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
příběhu	příběh	k1gInSc2	příběh
magicky	magicky	k6eAd1	magicky
přenese	přenést	k5eAaPmIp3nS	přenést
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
v	v	k7c6	v
době	doba	k1gFnSc6	doba
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
vydána	vydán	k2eAgFnSc1d1	vydána
další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bouřková	bouřkový	k2eAgFnSc1d1	bouřková
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Sezon	sezona	k1gFnPc2	sezona
burz	burza	k1gFnPc2	burza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nenavazuje	navazovat	k5eNaImIp3nS	navazovat
na	na	k7c4	na
díl	díl	k1gInSc4	díl
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
díl	díl	k1gInSc4	díl
ságy	sága	k1gFnSc2	sága
Paní	paní	k1gFnSc2	paní
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
předchází	předcházet	k5eAaImIp3nS	předcházet
povídce	povídka	k1gFnSc3	povídka
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
v	v	k7c6	v
měsíčníku	měsíčník	k1gInSc6	měsíčník
Fantastyka	Fantastyek	k1gInSc2	Fantastyek
<g/>
.	.	kIx.	.
</s>
<s>
Geralt	Geralt	k1gMnSc1	Geralt
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
krvežíznivým	krvežíznivý	k2eAgNnSc7d1	krvežíznivé
monstrem	monstrum	k1gNnSc7	monstrum
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
se	se	k3xPyFc4	se
stráží	strážit	k5eAaImIp3nS	strážit
a	a	k8xC	a
se	s	k7c7	s
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
zaklínačské	zaklínačský	k2eAgInPc4d1	zaklínačský
meče	meč	k1gInPc4	meč
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
své	svůj	k3xOyFgFnPc4	svůj
záležitosti	záležitost	k1gFnPc4	záležitost
se	s	k7c7	s
zrzavou	zrzavý	k2eAgFnSc7d1	zrzavá
čarodějkou	čarodějka	k1gFnSc7	čarodějka
Lyttou	Lyttý	k2eAgFnSc7d1	Lyttý
Neyd	Neyd	k1gInSc4	Neyd
alias	alias	k9	alias
Korál	korál	k1gInSc4	korál
<g/>
.	.	kIx.	.
</s>
<s>
Dittmar	Dittmar	k1gMnSc1	Dittmar
Chmelař	Chmelař	k1gMnSc1	Chmelař
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
Sapkowského	Sapkowského	k2eAgFnSc4d1	Sapkowského
prózu	próza	k1gFnSc4	próza
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
S	s	k7c7	s
vyzývavou	vyzývavý	k2eAgFnSc7d1	vyzývavá
<g/>
,	,	kIx,	,
řemeslně	řemeslně	k6eAd1	řemeslně
perfektně	perfektně	k6eAd1	perfektně
zvládnutou	zvládnutý	k2eAgFnSc7d1	zvládnutá
drzostí	drzost	k1gFnSc7	drzost
se	se	k3xPyFc4	se
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
a	a	k8xC	a
legendy	legenda	k1gFnPc4	legenda
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
naše	náš	k3xOp1gInPc4	náš
současné	současný	k2eAgInPc4d1	současný
spory	spor	k1gInPc4	spor
<g/>
...	...	k?	...
U	u	k7c2	u
Sapkovského	Sapkovský	k2eAgNnSc2d1	Sapkovský
je	být	k5eAaImIp3nS	být
literárním	literární	k2eAgInSc7d1	literární
kobercem	koberec	k1gInSc7	koberec
nádherný	nádherný	k2eAgInSc1d1	nádherný
gobelín	gobelín	k1gInSc1	gobelín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
utkán	utkán	k2eAgInSc1d1	utkán
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
nylonu	nylon	k1gInSc2	nylon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Jiří	Jiří	k1gMnSc1	Jiří
Popiolek	Popiolka	k1gFnPc2	Popiolka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sapkowski	Sapkowsk	k1gMnSc3	Sapkowsk
je	být	k5eAaImIp3nS	být
skvělý	skvělý	k2eAgMnSc1d1	skvělý
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
už	už	k6eAd1	už
skvělý	skvělý	k2eAgMnSc1d1	skvělý
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
samotná	samotný	k2eAgFnSc1d1	samotná
pětidílná	pětidílný	k2eAgFnSc1d1	pětidílná
<g />
.	.	kIx.	.
</s>
<s>
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
je	být	k5eAaImIp3nS	být
pořád	pořád	k6eAd1	pořád
ještě	ještě	k6eAd1	ještě
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
čtení	čtení	k1gNnSc4	čtení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
...	...	k?	...
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
Sapkowski	Sapkowsk	k1gFnSc6	Sapkowsk
trochu	trochu	k6eAd1	trochu
přecenil	přecenit	k5eAaPmAgMnS	přecenit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
...	...	k?	...
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
významu	význam	k1gInSc6	význam
té	ten	k3xDgFnSc2	ten
geraltovské	geraltovský	k2eAgFnSc2d1	geraltovský
si	se	k3xPyFc3	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
lepší	dobrý	k2eAgInPc4d2	lepší
a	a	k8xC	a
přesvědčivější	přesvědčivý	k2eAgInSc4d2	přesvědčivější
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
vyšla	vyjít	k5eAaPmAgFnS	vyjít
šestisvazková	šestisvazkový	k2eAgFnSc1d1	šestisvazková
komiksová	komiksový	k2eAgFnSc1d1	komiksová
adaptace	adaptace	k1gFnSc1	adaptace
vybraných	vybraný	k2eAgInPc2d1	vybraný
Geraltových	Geraltův	k2eAgInPc2d1	Geraltův
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc4	autor
byli	být	k5eAaImAgMnP	být
Maciej	Maciej	k1gInSc4	Maciej
Parowski	Parowsk	k1gFnSc2	Parowsk
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Sapkowski	Sapkowsk	k1gFnSc2	Sapkowsk
(	(	kIx(	(
<g/>
scénář	scénář	k1gInSc1	scénář
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bogusław	Bogusław	k1gMnSc1	Bogusław
Polch	Polch	k1gMnSc1	Polch
(	(	kIx(	(
<g/>
kresba	kresba	k1gFnSc1	kresba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
románů	román	k1gInPc2	román
třináctidílný	třináctidílný	k2eAgInSc1d1	třináctidílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
sestříhán	sestříhat	k5eAaPmNgInS	sestříhat
do	do	k7c2	do
stotřicetiminutového	stotřicetiminutový	k2eAgInSc2d1	stotřicetiminutový
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Marek	Marek	k1gMnSc1	Marek
Brodzki	Brodzk	k1gFnSc2	Brodzk
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
hráli	hrát	k5eAaImAgMnP	hrát
Michal	Michal	k1gMnSc1	Michal
Zebrowski	Zebrowsk	k1gFnSc2	Zebrowsk
<g/>
,	,	kIx,	,
Olaf	Olaf	k1gMnSc1	Olaf
Lubaszenko	Lubaszenka	k1gFnSc5	Lubaszenka
a	a	k8xC	a
Zbigniew	Zbigniew	k1gMnSc6	Zbigniew
Zamachowski	Zamachowsk	k1gMnSc6	Zamachowsk
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
seriálu	seriál	k1gInSc2	seriál
i	i	k8xC	i
filmu	film	k1gInSc2	film
bylo	být	k5eAaImAgNnS	být
spíše	spíše	k9	spíše
rozporuplné	rozporuplný	k2eAgNnSc1d1	rozporuplné
a	a	k8xC	a
Sapkowski	Sapkowski	k1gNnSc1	Sapkowski
sám	sám	k3xTgInSc1	sám
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
fanoušci	fanoušek	k1gMnPc1	fanoušek
tuto	tento	k3xDgFnSc4	tento
filmovou	filmový	k2eAgFnSc4d1	filmová
adaptaci	adaptace	k1gFnSc4	adaptace
nepovažovali	považovat	k5eNaImAgMnP	považovat
za	za	k7c4	za
povedenou	povedený	k2eAgFnSc4d1	povedená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
Zaklínač	zaklínač	k1gMnSc1	zaklínač
(	(	kIx(	(
<g/>
hodnocená	hodnocený	k2eAgFnSc1d1	hodnocená
na	na	k7c6	na
Metacritic	Metacritice	k1gFnPc2	Metacritice
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
86	[number]	k4	86
<g/>
/	/	kIx~	/
<g/>
8,5	[number]	k4	8,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
Geraltův	Geraltův	k2eAgInSc4d1	Geraltův
příběh	příběh	k1gInSc4	příběh
po	po	k7c6	po
pogromu	pogrom	k1gInSc6	pogrom
v	v	k7c6	v
Rivii	Rivie	k1gFnSc6	Rivie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pokračování	pokračování	k1gNnSc1	pokračování
s	s	k7c7	s
názvem	název	k1gInSc7	název
Zaklínač	zaklínač	k1gMnSc1	zaklínač
2	[number]	k4	2
<g/>
:	:	kIx,	:
Vrahové	vrah	k1gMnPc1	vrah
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
88	[number]	k4	88
<g/>
/	/	kIx~	/
<g/>
8,5	[number]	k4	8,5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
díl	díl	k1gInSc1	díl
Zaklínač	zaklínač	k1gMnSc1	zaklínač
3	[number]	k4	3
<g/>
:	:	kIx,	:
Divoký	divoký	k2eAgInSc1d1	divoký
hon	hon	k1gInSc1	hon
vyšel	vyjít	k5eAaPmAgInS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
93	[number]	k4	93
<g/>
/	/	kIx~	/
<g/>
9,0	[number]	k4	9,0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sapkowski	Sapkowski	k6eAd1	Sapkowski
sice	sice	k8xC	sice
ocenil	ocenit	k5eAaPmAgMnS	ocenit
vizuální	vizuální	k2eAgFnSc4d1	vizuální
kvalitu	kvalita	k1gFnSc4	kvalita
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zásadně	zásadně	k6eAd1	zásadně
odmítá	odmítat	k5eAaImIp3nS	odmítat
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
dějovým	dějový	k2eAgNnSc7d1	dějové
pokračováním	pokračování	k1gNnSc7	pokračování
knih	kniha	k1gFnPc2	kniha
-	-	kIx~	-
ve	v	k7c6	v
slangu	slang	k1gInSc6	slang
fanoušků	fanoušek	k1gMnPc2	fanoušek
nejde	jít	k5eNaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
kánon	kánon	k1gInSc4	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
tvořit	tvořit	k5eAaImF	tvořit
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
Zaklínači	zaklínač	k1gMnSc6	zaklínač
si	se	k3xPyFc3	se
vyhrazuje	vyhrazovat	k5eAaImIp3nS	vyhrazovat
autor	autor	k1gMnSc1	autor
výlučně	výlučně	k6eAd1	výlučně
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
