<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Czech	Czech	k1gInSc1	Czech
News	Newsa	k1gFnPc2	Newsa
Agency	Agenca	k1gFnSc2	Agenca
nebo	nebo	k8xC	nebo
Czech	Czech	k1gInSc1	Czech
Press	Pressa	k1gFnPc2	Pressa
Agency	Agenca	k1gFnSc2	Agenca
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
tisková	tiskový	k2eAgFnSc1d1	tisková
agentura	agentura	k1gFnSc1	agentura
nebo	nebo	k8xC	nebo
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
národních	národní	k2eAgFnPc2d1	národní
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Československá	československý	k2eAgFnSc1d1	Československá
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
užívající	užívající	k2eAgFnSc4d1	užívající
zkratku	zkratka	k1gFnSc4	zkratka
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
slangově	slangově	k6eAd1	slangově
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Četka	Četka	k1gFnSc1	Četka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČTK	ČTK	kA	ČTK
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
517	[number]	k4	517
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
tiskové	tiskový	k2eAgFnSc6d1	tisková
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
veřejnoprávní	veřejnoprávní	k2eAgNnSc1d1	veřejnoprávní
médium	médium	k1gNnSc1	médium
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
následnická	následnický	k2eAgFnSc1d1	následnická
organizace	organizace	k1gFnSc1	organizace
Československé	československý	k2eAgFnSc2d1	Československá
tiskové	tiskový	k2eAgFnSc2d1	tisková
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
ČTK	ČTK	kA	ČTK
není	být	k5eNaImIp3nS	být
dotována	dotovat	k5eAaBmNgFnS	dotovat
ze	z	k7c2	z
státních	státní	k2eAgInPc2d1	státní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČTK	ČTK	kA	ČTK
má	mít	k5eAaImIp3nS	mít
14	[number]	k4	14
poboček	pobočka	k1gFnPc2	pobočka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
zpravodaje	zpravodaj	k1gInPc1	zpravodaj
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
také	také	k6eAd1	také
zprávy	zpráva	k1gFnPc4	zpráva
světových	světový	k2eAgFnPc2d1	světová
agentur	agentura	k1gFnPc2	agentura
jako	jako	k8xS	jako
Reuters	Reutersa	k1gFnPc2	Reutersa
<g/>
,	,	kIx,	,
AFP	AFP	kA	AFP
<g/>
,	,	kIx,	,
AP	ap	kA	ap
<g/>
,	,	kIx,	,
TASS	TASS	kA	TASS
<g/>
,	,	kIx,	,
DPA	DPA	kA	DPA
<g/>
,	,	kIx,	,
ANSA	ANSA	kA	ANSA
<g/>
,	,	kIx,	,
EFE	EFE	kA	EFE
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
i	i	k9	i
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgFnPc2d1	další
národních	národní	k2eAgFnPc2d1	národní
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
ČTK	ČTK	kA	ČTK
263	[number]	k4	263
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
187	[number]	k4	187
redakčních	redakční	k2eAgMnPc2d1	redakční
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
redaktorů	redaktor	k1gMnPc2	redaktor
<g/>
,	,	kIx,	,
zpravodajů	zpravodaj	k1gMnPc2	zpravodaj
<g/>
,	,	kIx,	,
fotoreportérů	fotoreportér	k1gMnPc2	fotoreportér
<g/>
,	,	kIx,	,
kameramanů	kameraman	k1gMnPc2	kameraman
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
multimediální	multimediální	k2eAgFnSc1d1	multimediální
produkce	produkce	k1gFnSc1	produkce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
slovní	slovní	k2eAgNnSc4d1	slovní
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
obrazové	obrazový	k2eAgNnSc4d1	obrazové
a	a	k8xC	a
zvukové	zvukový	k2eAgNnSc4d1	zvukové
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
<g/>
,	,	kIx,	,
infografiku	infografika	k1gFnSc4	infografika
<g/>
,	,	kIx,	,
videozpravodajství	videozpravodajství	k1gNnPc4	videozpravodajství
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
ČTK	ČTK	kA	ČTK
denně	denně	k6eAd1	denně
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
700	[number]	k4	700
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
400	[number]	k4	400
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
35	[number]	k4	35
zvukových	zvukový	k2eAgInPc2d1	zvukový
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
videoreportáží	videoreportáž	k1gFnPc2	videoreportáž
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
portál	portál	k1gInSc4	portál
CeskeNoviny	CeskeNovina	k1gFnSc2	CeskeNovina
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslání	poslání	k1gNnPc1	poslání
ČTK	ČTK	kA	ČTK
==	==	k?	==
</s>
</p>
<p>
<s>
Poslání	poslání	k1gNnSc1	poslání
agentury	agentura	k1gFnSc2	agentura
definuje	definovat	k5eAaBmIp3nS	definovat
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
ČTK	ČTK	kA	ČTK
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
druhém	druhý	k4xOgInSc6	druhý
paragrafu	paragraf	k1gInSc6	paragraf
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Posláním	poslání	k1gNnSc7	poslání
tiskové	tiskový	k2eAgFnSc2d1	tisková
kanceláře	kancelář	k1gFnSc2	kancelář
je	být	k5eAaImIp3nS	být
poskytovat	poskytovat	k5eAaImF	poskytovat
objektivní	objektivní	k2eAgFnPc4d1	objektivní
a	a	k8xC	a
všestranné	všestranný	k2eAgFnPc4d1	všestranná
informace	informace	k1gFnPc4	informace
pro	pro	k7c4	pro
svobodné	svobodný	k2eAgNnSc4d1	svobodné
vytváření	vytváření	k1gNnSc4	vytváření
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
službu	služba	k1gFnSc4	služba
veřejnosti	veřejnost	k1gFnSc2	veřejnost
šířením	šíření	k1gNnSc7	šíření
slovního	slovní	k2eAgMnSc2d1	slovní
a	a	k8xC	a
obrazového	obrazový	k2eAgNnSc2d1	obrazové
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
službu	služba	k1gFnSc4	služba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
i	i	k9	i
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
současně	současně	k6eAd1	současně
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
agentuře	agentura	k1gFnSc3	agentura
poskytnout	poskytnout	k5eAaPmF	poskytnout
účelovou	účelový	k2eAgFnSc4d1	účelová
dotaci	dotace	k1gFnSc4	dotace
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
ke	k	k7c3	k
krytí	krytí	k1gNnSc3	krytí
ztráty	ztráta	k1gFnSc2	ztráta
z	z	k7c2	z
hospodaření	hospodaření	k1gNnSc2	hospodaření
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgFnSc4d1	poslední
dotaci	dotace	k1gFnSc4	dotace
od	od	k7c2	od
státu	stát	k1gInSc2	stát
dostala	dostat	k5eAaPmAgFnS	dostat
agentura	agentura	k1gFnSc1	agentura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
už	už	k6eAd1	už
nežádala	žádat	k5eNaImAgFnS	žádat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
agentury	agentura	k1gFnSc2	agentura
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
také	také	k9	také
Kodexem	kodex	k1gInSc7	kodex
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotoarchiv	fotoarchiv	k1gInSc4	fotoarchiv
ČTK	ČTK	kA	ČTK
==	==	k?	==
</s>
</p>
<p>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kolem	kolem	k7c2	kolem
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ČTK	ČTK	kA	ČTK
představujících	představující	k2eAgFnPc2d1	představující
nejúplnější	úplný	k2eAgFnSc4d3	nejúplnější
a	a	k8xC	a
nejpřehlednější	přehlední	k2eAgFnSc4d3	přehlední
kolekci	kolekce	k1gFnSc4	kolekce
snímků	snímek	k1gInPc2	snímek
dokumentujících	dokumentující	k2eAgInPc2d1	dokumentující
vývoj	vývoj	k1gInSc4	vývoj
našich	náš	k3xOp1gInPc2	náš
států	stát	k1gInPc2	stát
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
700	[number]	k4	700
000	[number]	k4	000
snímků	snímek	k1gInPc2	snímek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
podobě	podoba	k1gFnSc6	podoba
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
Infobanky	Infobanka	k1gFnSc2	Infobanka
ČTK	ČTK	kA	ČTK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Infobanka	Infobanka	k1gFnSc1	Infobanka
ČTK	ČTK	kA	ČTK
==	==	k?	==
</s>
</p>
<p>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
ČTK	ČTK	kA	ČTK
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
databáze	databáze	k1gFnSc1	databáze
<g/>
,	,	kIx,	,
archiv	archiv	k1gInSc1	archiv
všech	všecek	k3xTgInPc2	všecek
deníků	deník	k1gInPc2	deník
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
periodik	periodikum	k1gNnPc2	periodikum
vycházejících	vycházející	k2eAgNnPc2d1	vycházející
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
další	další	k2eAgFnPc4d1	další
textové	textový	k2eAgFnPc4d1	textová
<g/>
,	,	kIx,	,
obrazové	obrazový	k2eAgFnPc4d1	obrazová
i	i	k8xC	i
zvukové	zvukový	k2eAgFnPc4d1	zvuková
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
uvedla	uvést	k5eAaPmAgFnS	uvést
agentura	agentura	k1gFnSc1	agentura
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
Infobanky	Infobanka	k1gFnSc2	Infobanka
přizpůsobenou	přizpůsobený	k2eAgFnSc4d1	přizpůsobená
moderním	moderní	k2eAgFnPc3d1	moderní
technologiím	technologie	k1gFnPc3	technologie
a	a	k8xC	a
designu	design	k1gInSc3	design
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
například	například	k6eAd1	například
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
související	související	k2eAgFnPc4d1	související
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc4	fotografia
a	a	k8xC	a
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Klientům	klient	k1gMnPc3	klient
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
http://ib.ctk.cz	[url]	k1gFnPc2	http://ib.ctk.cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dokumentační	dokumentační	k2eAgFnSc2d1	dokumentační
databáze	databáze	k1gFnSc2	databáze
==	==	k?	==
</s>
</p>
<p>
<s>
Dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
databáze	databáze	k1gFnSc1	databáze
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
biografie	biografie	k1gFnPc1	biografie
významných	významný	k2eAgMnPc2d1	významný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
databáze	databáze	k1gFnPc1	databáze
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Infobanky	Infobanka	k1gFnSc2	Infobanka
ČTK	ČTK	kA	ČTK
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ona	onen	k3xDgFnSc1	onen
přístupné	přístupný	k2eAgFnPc4d1	přístupná
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
předplatitelům	předplatitel	k1gMnPc3	předplatitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
roky	rok	k1gInPc1	rok
existují	existovat	k5eAaImIp3nP	existovat
archivy	archiv	k1gInPc1	archiv
papírové	papírový	k2eAgInPc1d1	papírový
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mikrofišové	mikrofišové	k2eAgInSc1d1	mikrofišové
(	(	kIx(	(
<g/>
mikrofiche	mikrofiche	k1gInSc1	mikrofiche
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Agenturní	agenturní	k2eAgNnSc1d1	agenturní
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
někdy	někdy	k6eAd1	někdy
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
tiskového	tiskový	k2eAgNnSc2d1	tiskové
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
denního	denní	k2eAgInSc2d1	denní
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozvíjejícím	rozvíjející	k2eAgMnSc7d1	rozvíjející
se	se	k3xPyFc4	se
češstvím	češství	k1gNnSc7	češství
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
rakouská	rakouský	k2eAgFnSc1d1	rakouská
státní	státní	k2eAgFnSc1d1	státní
tisková	tiskový	k2eAgFnSc1d1	tisková
agentura	agentura	k1gFnSc1	agentura
k.k.	k.k.	k?	k.k.
Telegraphen-Korrespondenz-Bureau	Telegraphen-Korrespondenz-Bureaus	k1gInSc2	Telegraphen-Korrespondenz-Bureaus
(	(	kIx(	(
<g/>
Korrbyro	Korrbyro	k1gNnSc1	Korrbyro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
státní	státní	k2eAgFnSc1d1	státní
tisková	tiskový	k2eAgFnSc1d1	tisková
agentura	agentura	k1gFnSc1	agentura
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
soustředila	soustředit	k5eAaPmAgFnS	soustředit
po	po	k7c6	po
Rakousko-uherském	rakouskoherský	k2eAgNnSc6d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Předlitavsku	Předlitavsko	k1gNnSc6	Předlitavsko
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
Rakouskem-Uherskem	Rakouskem-Uhersko	k1gNnSc7	Rakouskem-Uhersko
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
první	první	k4xOgFnSc1	první
agenturní	agenturní	k2eAgFnSc1d1	agenturní
odbočka	odbočka	k1gFnSc1	odbočka
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc4d1	samostatné
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
oficiální	oficiální	k2eAgFnSc2d1	oficiální
maďarské	maďarský	k2eAgFnSc2d1	maďarská
agentury	agentura	k1gFnSc2	agentura
MTI	MTI	kA	MTI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
postupně	postupně	k6eAd1	postupně
i	i	k9	i
další	další	k2eAgFnSc2d1	další
agenturní	agenturní	k2eAgFnSc2d1	agenturní
odbočky	odbočka	k1gFnSc2	odbočka
–	–	k?	–
brněnská	brněnský	k2eAgFnSc1d1	brněnská
a	a	k8xC	a
ostravská	ostravský	k2eAgFnSc1d1	Ostravská
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
<g/>
,	,	kIx,	,
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
korespondence	korespondence	k1gFnSc1	korespondence
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
redakce	redakce	k1gFnSc1	redakce
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
odbočce	odbočka	k1gFnSc6	odbočka
Korrbyra	Korrbyr	k1gInSc2	Korrbyr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
ČTK	ČTK	kA	ČTK
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
už	už	k6eAd1	už
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
představitelé	představitel	k1gMnPc1	představitel
českého	český	k2eAgInSc2d1	český
exilu	exil	k1gInSc2	exil
založili	založit	k5eAaPmAgMnP	založit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
agenturu	agentura	k1gFnSc4	agentura
Českou	český	k2eAgFnSc4d1	Česká
tiskovovou	tiskovový	k2eAgFnSc4d1	tiskovový
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
i	i	k9	i
národní	národní	k2eAgFnSc1d1	národní
agentura	agentura	k1gFnSc1	agentura
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Československou	československý	k2eAgFnSc7d1	Československá
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
prvním	první	k4xOgMnPc3	první
českým	český	k2eAgMnPc3d1	český
agenturním	agenturní	k2eAgMnPc3d1	agenturní
novinářům	novinář	k1gMnPc3	novinář
patřili	patřit	k5eAaImAgMnP	patřit
Karel	Karel	k1gMnSc1	Karel
Mečíř	mečíř	k1gMnSc1	mečíř
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hajšman	Hajšman	k1gMnSc1	Hajšman
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Dušek	Dušek	k1gMnSc1	Dušek
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
patřilo	patřit	k5eAaImAgNnS	patřit
i	i	k9	i
nadále	nadále	k6eAd1	nadále
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
k	k	k7c3	k
jedněm	jeden	k4xCgFnPc3	jeden
z	z	k7c2	z
nejkvalitnějších	kvalitní	k2eAgMnPc2d3	nejkvalitnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
"	"	kIx"	"
<g/>
fotografické	fotografický	k2eAgNnSc1d1	fotografické
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
fotoarchiv	fotoarchiv	k1gInSc1	fotoarchiv
přežil	přežít	k5eAaPmAgInS	přežít
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
vinohradském	vinohradský	k2eAgInSc6d1	vinohradský
"	"	kIx"	"
<g/>
exilu	exil	k1gInSc6	exil
<g/>
"	"	kIx"	"
bombardování	bombardování	k1gNnSc1	bombardování
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
a	a	k8xC	a
komunistické	komunistický	k2eAgFnSc2d1	komunistická
totality	totalita	k1gFnSc2	totalita
byla	být	k5eAaImAgFnS	být
podrobena	podroben	k2eAgFnSc1d1	podrobena
cenzuře	cenzura	k1gFnSc3	cenzura
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
byl	být	k5eAaImAgInS	být
podřizován	podřizovat	k5eAaImNgInS	podřizovat
potřebám	potřeba	k1gFnPc3	potřeba
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Agentura	agentura	k1gFnSc1	agentura
sama	sám	k3xTgFnSc1	sám
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
svou	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
<g/>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Milan	Milan	k1gMnSc1	Milan
Stibral	Stibral	k1gMnSc1	Stibral
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
zvolen	zvolit	k5eAaPmNgMnS	zvolit
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
evropského	evropský	k2eAgNnSc2d1	Evropské
sdružení	sdružení	k1gNnSc2	sdružení
agentur	agentura	k1gFnPc2	agentura
EANA	EANA	kA	EANA
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
a	a	k8xC	a
1998	[number]	k4	1998
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Jiří	Jiří	k1gMnSc1	Jiří
Majstr	Majstr	k1gInSc4	Majstr
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
(	(	kIx(	(
<g/>
Boardu	Boarda	k1gMnSc4	Boarda
<g/>
)	)	kIx)	)
EANA	EANA	kA	EANA
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012	[number]	k4	2012
až	až	k9	až
2015	[number]	k4	2015
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
šéfredaktorkou	šéfredaktorka	k1gFnSc7	šéfredaktorka
ČTK	ČTK	kA	ČTK
Radka	Radek	k1gMnSc2	Radek
Matesová	Matesová	k1gFnSc1	Matesová
Marková	Marková	k1gFnSc1	Marková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
získala	získat	k5eAaPmAgFnS	získat
redaktorka	redaktorka	k1gFnSc1	redaktorka
Radka	Radka	k1gFnSc1	Radka
Marková	Marková	k1gFnSc1	Marková
Novinářskou	novinářský	k2eAgFnSc4d1	novinářská
křepelku	křepelka	k1gFnSc4	křepelka
<g/>
.	.	kIx.	.
<g/>
Český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
udělil	udělit	k5eAaPmAgMnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ČTK	ČTK	kA	ČTK
zlatý	zlatý	k2eAgInSc1d1	zlatý
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
družstev	družstvo	k1gNnPc2	družstvo
za	za	k7c4	za
"	"	kIx"	"
<g/>
podporu	podpora	k1gFnSc4	podpora
propagace	propagace	k1gFnSc1	propagace
okultních	okultní	k2eAgFnPc2d1	okultní
nauk	nauka	k1gFnPc2	nauka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
získal	získat	k5eAaPmAgMnS	získat
technický	technický	k2eAgMnSc1d1	technický
ředitel	ředitel	k1gMnSc1	ředitel
ČTK	ČTK	kA	ČTK
Jan	Jan	k1gMnSc1	Jan
Kodera	Kodero	k1gNnSc2	Kodero
cenu	cena	k1gFnSc4	cena
Evropské	evropský	k2eAgFnSc2d1	Evropská
aliance	aliance	k1gFnSc2	aliance
<g />
.	.	kIx.	.
</s>
<s>
EANA	EANA	kA	EANA
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
roli	role	k1gFnSc4	role
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
zavedení	zavedení	k1gNnSc6	zavedení
nového	nový	k2eAgInSc2d1	nový
Multimediálního	multimediální	k2eAgInSc2d1	multimediální
redakčního	redakční	k2eAgInSc2d1	redakční
systému	systém	k1gInSc2	systém
agentury	agentura	k1gFnSc2	agentura
(	(	kIx(	(
<g/>
MRS	MRS	kA	MRS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
převzala	převzít	k5eAaPmAgFnS	převzít
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
např.	např.	kA	např.
německá	německý	k2eAgFnSc1d1	německá
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
tisková	tiskový	k2eAgFnSc1d1	tisková
agentura	agentura	k1gFnSc1	agentura
dpa	dpa	k?	dpa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Czech	Czech	k1gInSc1	Czech
Press	Pressa	k1gFnPc2	Pressa
Photo	Photo	k1gNnSc1	Photo
fotoreportér	fotoreportér	k1gMnSc1	fotoreportér
ČTK	ČTK	kA	ČTK
Michal	Michal	k1gMnSc1	Michal
Kamaryt	Kamaryt	k1gInSc1	Kamaryt
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgFnSc1d1	vítězná
fotografie	fotografie	k1gFnSc1	fotografie
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
známky	známka	k1gFnPc4	známka
finské	finský	k2eAgFnSc2d1	finská
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fotoreportér	fotoreportér	k1gMnSc1	fotoreportér
ČTK	ČTK	kA	ČTK
Roman	Roman	k1gMnSc1	Roman
Vondrouš	Vondrouš	k1gMnSc1	Vondrouš
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
za	za	k7c4	za
dostihy	dostih	k1gInPc4	dostih
cenu	cena	k1gFnSc4	cena
Czech	Czech	k1gInSc4	Czech
Press	Press	k1gInSc1	Press
Photo	Photo	k1gNnSc1	Photo
2012	[number]	k4	2012
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
World	Worlda	k1gFnPc2	Worlda
Press	Press	k1gInSc4	Press
Photo	Photo	k1gNnSc1	Photo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Vondrouš	Vondrouš	k1gMnSc1	Vondrouš
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
pravidelně	pravidelně	k6eAd1	pravidelně
oceňován	oceňovat	k5eAaImNgMnS	oceňovat
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
soutěži	soutěž	k1gFnSc6	soutěž
Czech	Czecha	k1gFnPc2	Czecha
Press	Pressa	k1gFnPc2	Pressa
Photo	Photo	k1gNnSc4	Photo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
čestné	čestný	k2eAgNnSc1d1	čestné
uznání	uznání	k1gNnSc1	uznání
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Aktualita	aktualita	k1gFnSc1	aktualita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Czech	Czech	k1gInSc1	Czech
Press	Pressa	k1gFnPc2	Pressa
Photo	Photo	k1gNnSc1	Photo
2013	[number]	k4	2013
dostal	dostat	k5eAaPmAgMnS	dostat
fotoreportér	fotoreportér	k1gMnSc1	fotoreportér
České	český	k2eAgFnSc2d1	Česká
tiskové	tiskový	k2eAgFnSc2d1	tisková
kanceláře	kancelář	k1gFnSc2	kancelář
(	(	kIx(	(
<g/>
ČTK	ČTK	kA	ČTK
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Kamaryt	Kamaryt	k1gInSc4	Kamaryt
za	za	k7c4	za
snímek	snímek	k1gInSc4	snímek
bývalého	bývalý	k2eAgMnSc2d1	bývalý
poslance	poslanec	k1gMnSc2	poslanec
a	a	k8xC	a
hejtmana	hejtman	k1gMnSc2	hejtman
Davida	David	k1gMnSc2	David
Ratha	Rath	k1gMnSc2	Rath
obžalovaného	obžalovaný	k1gMnSc2	obžalovaný
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
Czech	Czech	k1gInSc4	Czech
Press	Press	k1gInSc1	Press
Photo	Photo	k1gNnSc1	Photo
2014	[number]	k4	2014
fotografové	fotograf	k1gMnPc1	fotograf
ČTK	ČTK	kA	ČTK
opět	opět	k6eAd1	opět
bodovali	bodovat	k5eAaImAgMnP	bodovat
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
Vondrouš	Vondrouš	k1gMnSc1	Vondrouš
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Sport	sport	k1gInSc1	sport
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Kamaryt	Kamaryt	k1gInSc1	Kamaryt
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Umění	umění	k1gNnSc1	umění
a	a	k8xC	a
zábava	zábava	k1gFnSc1	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Vondrouš	Vondrouš	k1gMnSc1	Vondrouš
si	se	k3xPyFc3	se
dále	daleko	k6eAd2	daleko
odnesl	odnést	k5eAaPmAgInS	odnést
cenu	cena	k1gFnSc4	cena
Nikon	Nikon	k1gInSc1	Nikon
Sport	sport	k1gInSc1	sport
Awards	Awards	k1gInSc4	Awards
za	za	k7c4	za
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
fotografii	fotografia	k1gFnSc4	fotografia
vypjaté	vypjatý	k2eAgFnSc2d1	vypjatá
sportovní	sportovní	k2eAgFnSc2d1	sportovní
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchy	úspěch	k1gInPc1	úspěch
četkařů	četkař	k1gMnPc2	četkař
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
Tomáš	Tomáš	k1gMnSc1	Tomáš
Binter	Binter	k1gMnSc1	Binter
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
Canon	Canon	kA	Canon
Junior	junior	k1gMnSc1	junior
Awards	Awards	k1gInSc1	Awards
<g/>
.	.	kIx.	.
<g/>
ČTK	ČTK	kA	ČTK
také	také	k9	také
udělila	udělit	k5eAaPmAgFnS	udělit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
soutěže	soutěž	k1gFnSc2	soutěž
Cenu	cena	k1gFnSc4	cena
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
získal	získat	k5eAaPmAgMnS	získat
Karel	Karel	k1gMnSc1	Karel
Cudlín	Cudlína	k1gFnPc2	Cudlína
<g/>
.	.	kIx.	.
<g/>
Fotoreportér	fotoreportér	k1gMnSc1	fotoreportér
ČTK	ČTK	kA	ČTK
Roman	Roman	k1gMnSc1	Roman
Vondrouš	Vondrouš	k1gMnSc1	Vondrouš
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Czech	Czech	k1gInSc1	Czech
Press	Pressa	k1gFnPc2	Pressa
Photo	Photo	k1gNnSc1	Photo
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
za	za	k7c4	za
snímky	snímka	k1gFnPc4	snímka
Havana	havana	k1gNnPc2	havana
Cars	Carsa	k1gFnPc2	Carsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
Pictures	Pictures	k1gMnSc1	Pictures
of	of	k?	of
the	the	k?	the
Year	Yeara	k1gFnPc2	Yeara
získal	získat	k5eAaPmAgInS	získat
Vondrouš	Vondrouš	k1gInSc1	Vondrouš
2	[number]	k4	2
<g/>
.	.	kIx.	.
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
sport	sport	k1gInSc1	sport
za	za	k7c4	za
fotografii	fotografia	k1gFnSc4	fotografia
"	"	kIx"	"
<g/>
Horse	Horse	k1gFnSc1	Horse
race	rac	k1gFnSc2	rac
drama	drama	k1gNnSc1	drama
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
slavil	slavit	k5eAaImAgInS	slavit
úspěch	úspěch	k1gInSc1	úspěch
i	i	k8xC	i
David	David	k1gMnSc1	David
Taneček	taneček	k1gInSc1	taneček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nikon	Nikon	k1gInSc4	Nikon
Sport	sport	k1gInSc1	sport
Awards	Awards	k1gInSc1	Awards
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
ČTK	ČTK	kA	ČTK
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Vatikánu	Vatikán	k1gInSc2	Vatikán
Karel	Karel	k1gMnSc1	Karel
Weirich	Weirich	k1gMnSc1	Weirich
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
posmrtně	posmrtně	k6eAd1	posmrtně
oceněn	ocenit	k5eAaPmNgInS	ocenit
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
medailí	medaile	k1gFnPc2	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ředitelé	ředitel	k1gMnPc1	ředitel
ČTK	ČTK	kA	ČTK
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hajšman	Hajšman	k1gMnSc1	Hajšman
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Čermák	Čermák	k1gMnSc1	Čermák
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Tvarůžek	tvarůžek	k1gInSc1	tvarůžek
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Kořínek	Kořínek	k1gMnSc1	Kořínek
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Vorel	Vorel	k1gMnSc1	Vorel
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hradecký	Hradecký	k1gMnSc1	Hradecký
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šmíd	Šmíd	k1gMnSc1	Šmíd
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sulek	sulka	k1gFnPc2	sulka
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Suk	Suk	k1gMnSc1	Suk
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Svěrčina	Svěrčin	k2eAgInSc2d1	Svěrčin
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Čmolík	Čmolík	k1gMnSc1	Čmolík
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Benda	Benda	k1gMnSc1	Benda
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Uhl	Uhl	k1gMnSc1	Uhl
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Stibral	Stibral	k1gMnSc1	Stibral
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Majstr	Majstr	k1gInSc1	Majstr
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnSc3	osobnost
ČTK	ČTK	kA	ČTK
==	==	k?	==
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Čermák	Čermák	k1gMnSc1	Čermák
(	(	kIx(	(
<g/>
novinář	novinář	k1gMnSc1	novinář
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Mečíř	mečíř	k1gMnSc1	mečíř
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
Dušek	Dušek	k1gMnSc1	Dušek
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
König	König	k1gMnSc1	König
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Weirich	Weirich	k1gMnSc1	Weirich
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Bareš	Bareš	k1gMnSc1	Bareš
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Pešl	Pešl	k1gMnSc1	Pešl
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
-	-	kIx~	-
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Mráz	Mráz	k1gMnSc1	Mráz
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
-	-	kIx~	-
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čestmír	Čestmír	k1gMnSc1	Čestmír
Sládek	Sládek	k1gMnSc1	Sládek
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
-	-	kIx~	-
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Doležal	Doležal	k1gMnSc1	Doležal
(	(	kIx(	(
<g/>
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
-	-	kIx~	-
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ČTK	ČTK	kA	ČTK
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://www.ctk.cz	[url]	k1gInSc1	http://www.ctk.cz
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
ČTK	ČTK	kA	ČTK
</s>
</p>
<p>
<s>
http://www.ceskenoviny.cz	[url]	k1gInSc1	http://www.ceskenoviny.cz
–	–	k?	–
zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
server	server	k1gInSc1	server
ČTK	ČTK	kA	ČTK
</s>
</p>
