<s>
Česká	český	k2eAgFnSc1d1	Česká
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Czech	Czech	k1gInSc1	Czech
News	Newsa	k1gFnPc2	Newsa
Agency	Agenca	k1gFnSc2	Agenca
nebo	nebo	k8xC	nebo
Czech	Czech	k1gInSc1	Czech
Press	Pressa	k1gFnPc2	Pressa
Agency	Agenca	k1gFnSc2	Agenca
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
tisková	tiskový	k2eAgFnSc1d1	tisková
agentura	agentura	k1gFnSc1	agentura
nebo	nebo	k8xC	nebo
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
národních	národní	k2eAgFnPc2d1	národní
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
