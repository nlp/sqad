<s>
Pudr	pudr	k1gInSc1	pudr
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nanáší	nanášet	k5eAaImIp3nS	nanášet
na	na	k7c4	na
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
dekolt	dekolt	k1gInSc1	dekolt
<g/>
,	,	kIx,	,
pleš	pleš	k1gFnSc1	pleš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odstranil	odstranit	k5eAaPmAgInS	odstranit
lesk	lesk	k1gInSc1	lesk
pleti	pleť	k1gFnSc2	pleť
a	a	k8xC	a
zjemnil	zjemnit	k5eAaPmAgMnS	zjemnit
její	její	k3xOp3gInSc4	její
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
aby	aby	kYmCp3nS	aby
zafixoval	zafixovat	k5eAaPmAgMnS	zafixovat
nanesaný	nanesaný	k2eAgInSc4d1	nanesaný
makeup	makeup	k1gInSc4	makeup
<g/>
.	.	kIx.	.
</s>
