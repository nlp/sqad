<p>
<s>
Pudr	pudr	k1gInSc1	pudr
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nanáší	nanášet	k5eAaImIp3nS	nanášet
na	na	k7c4	na
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
dekolt	dekolt	k1gInSc1	dekolt
<g/>
,	,	kIx,	,
pleš	pleš	k1gFnSc1	pleš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odstranil	odstranit	k5eAaPmAgInS	odstranit
lesk	lesk	k1gInSc1	lesk
pleti	pleť	k1gFnSc2	pleť
a	a	k8xC	a
zjemnil	zjemnit	k5eAaPmAgMnS	zjemnit
její	její	k3xOp3gInSc4	její
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
aby	aby	kYmCp3nS	aby
zafixoval	zafixovat	k5eAaPmAgMnS	zafixovat
nanesaný	nanesaný	k2eAgInSc4d1	nanesaný
makeup	makeup	k1gInSc4	makeup
<g/>
.	.	kIx.	.
</s>
<s>
Uchovává	uchovávat	k5eAaImIp3nS	uchovávat
se	se	k3xPyFc4	se
v	v	k7c6	v
ozdobném	ozdobný	k2eAgNnSc6d1	ozdobné
pouzdře	pouzdro	k1gNnSc6	pouzdro
se	s	k7c7	s
zrcátkem	zrcátko	k1gNnSc7	zrcátko
a	a	k8xC	a
textilním	textilní	k2eAgNnSc7d1	textilní
pudrovátkem	pudrovátko	k1gNnSc7	pudrovátko
zvaném	zvaný	k2eAgInSc6d1	zvaný
pudřenka	pudřenka	k1gFnSc1	pudřenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jemný	jemný	k2eAgInSc1d1	jemný
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
především	především	k6eAd1	především
z	z	k7c2	z
mastku	mastek	k1gInSc2	mastek
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
kaolínu	kaolín	k1gInSc2	kaolín
<g/>
,	,	kIx,	,
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
barevných	barevný	k2eAgInPc2d1	barevný
pigmentů	pigment	k1gInPc2	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tvář	tvář	k1gFnSc4	tvář
se	se	k3xPyFc4	se
nanáší	nanášet	k5eAaImIp3nS	nanášet
pomocí	pomocí	k7c2	pomocí
labutěnky	labutěnka	k1gFnSc2	labutěnka
nebo	nebo	k8xC	nebo
širokého	široký	k2eAgInSc2d1	široký
štětce	štětec	k1gInSc2	štětec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompaktní	kompaktní	k2eAgInSc1d1	kompaktní
pudr	pudr	k1gInSc1	pudr
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
navíc	navíc	k6eAd1	navíc
pojiva	pojivo	k1gNnPc4	pojivo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
slisovaný	slisovaný	k2eAgMnSc1d1	slisovaný
do	do	k7c2	do
pevné	pevný	k2eAgFnSc2d1	pevná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
nosit	nosit	k5eAaImF	nosit
v	v	k7c6	v
kabelce	kabelka	k1gFnSc6	kabelka
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
i	i	k9	i
pudr	pudr	k1gInSc1	pudr
slisovaný	slisovaný	k2eAgInSc1d1	slisovaný
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
půlcentimetrových	půlcentimetrový	k2eAgFnPc2d1	půlcentimetrová
kuliček	kulička	k1gFnPc2	kulička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pudr	pudr	k1gInSc1	pudr
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
-	-	kIx~	-
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
měl	mít	k5eAaImAgInS	mít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
vyznačení	vyznačení	k1gNnSc2	vyznačení
kmenové	kmenový	k2eAgFnSc2d1	kmenová
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
,	,	kIx,	,
bojové	bojový	k2eAgFnSc2d1	bojová
pohotovosti	pohotovost	k1gFnSc2	pohotovost
nebo	nebo	k8xC	nebo
rituální	rituální	k2eAgInSc4d1	rituální
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
prášek	prášek	k1gInSc1	prášek
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
císaři	císař	k1gMnPc1	císař
si	se	k3xPyFc3	se
jím	on	k3xPp3gNnSc7	on
barvili	barvit	k5eAaImAgMnP	barvit
vlasy	vlas	k1gInPc4	vlas
i	i	k8xC	i
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
užívání	užívání	k1gNnSc1	užívání
hladké	hladký	k2eAgFnSc2d1	hladká
mouky	mouka	k1gFnSc2	mouka
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
pudru	pudr	k1gInSc2	pudr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
ženy	žena	k1gFnPc1	žena
používaly	používat	k5eAaImAgFnP	používat
bílý	bílý	k2eAgInSc4d1	bílý
prášek	prášek	k1gInSc4	prášek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
bílé	bílý	k2eAgNnSc1d1	bílé
barvivo	barvivo	k1gNnSc1	barvivo
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
těžké	těžký	k2eAgInPc4d1	těžký
kovy	kov	k1gInPc4	kov
a	a	k8xC	a
olovo	olovo	k1gNnSc4	olovo
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yIgFnPc6	který
pokožka	pokožka	k1gFnSc1	pokožka
praskala	praskat	k5eAaImAgFnS	praskat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
škodliviny	škodlivina	k1gFnPc1	škodlivina
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
a	a	k8xC	a
výrobci	výrobce	k1gMnPc1	výrobce
začali	začít	k5eAaPmAgMnP	začít
inzerovat	inzerovat	k5eAaImF	inzerovat
"	"	kIx"	"
<g/>
jeduprostý	jeduprostý	k2eAgInSc1d1	jeduprostý
porcelánový	porcelánový	k2eAgInSc1d1	porcelánový
<g/>
"	"	kIx"	"
pudr	pudr	k1gInSc1	pudr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
baroka	baroko	k1gNnSc2	baroko
se	se	k3xPyFc4	se
pudrovaly	pudrovat	k5eAaImAgFnP	pudrovat
dámy	dáma	k1gFnPc1	dáma
i	i	k8xC	i
pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
módu	móda	k1gFnSc4	móda
zavedli	zavést	k5eAaPmAgMnP	zavést
Francouzi	Francouz	k1gMnPc1	Francouz
za	za	k7c2	za
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
společně	společně	k6eAd1	společně
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
alonžovými	alonžový	k2eAgFnPc7d1	alonžový
parukami	paruka	k1gFnPc7	paruka
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
<g/>
.	.	kIx.	.
<g/>
Samuel	Samuela	k1gFnPc2	Samuela
Pepys	Pepys	k1gInSc1	Pepys
si	se	k3xPyFc3	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
dvorských	dvorský	k2eAgInPc2d1	dvorský
mravů	mrav	k1gInPc2	mrav
(	(	kIx(	(
<g/>
vedeném	vedený	k2eAgInSc6d1	vedený
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
<g/>
)	)	kIx)	)
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepudroval	pudrovat	k5eNaImAgMnS	pudrovat
<g/>
.	.	kIx.	.
</s>
<s>
Povinnost	povinnost	k1gFnSc1	povinnost
pudrovat	pudrovat	k5eAaImF	pudrovat
se	se	k3xPyFc4	se
patřila	patřit	k5eAaImAgFnS	patřit
ke	k	k7c3	k
společenským	společenský	k2eAgFnPc3d1	společenská
příležitostem	příležitost	k1gFnPc3	příležitost
v	v	k7c6	v
období	období	k1gNnSc6	období
rokoka	rokoko	k1gNnSc2	rokoko
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1730	[number]	k4	1730
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
pudrovat	pudrovat	k5eAaImF	pudrovat
i	i	k9	i
paruky	paruka	k1gFnPc4	paruka
<g/>
.	.	kIx.	.
</s>
<s>
Pudrování	pudrování	k1gNnSc1	pudrování
se	se	k3xPyFc4	se
doplňovalo	doplňovat	k5eAaImAgNnS	doplňovat
výrazným	výrazný	k2eAgNnSc7d1	výrazné
růžovým	růžový	k2eAgNnSc7d1	růžové
líčením	líčení	k1gNnSc7	líčení
tváří	tvář	k1gFnPc2	tvář
s	s	k7c7	s
nalepenými	nalepený	k2eAgFnPc7d1	nalepená
muškami	muška	k1gFnPc7	muška
či	či	k8xC	či
pihami	piha	k1gFnPc7	piha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
drahocenné	drahocenný	k2eAgFnPc1d1	drahocenná
miniaturní	miniaturní	k2eAgFnPc1d1	miniaturní
pudřenky	pudřenka	k1gFnPc1	pudřenka
<g/>
,	,	kIx,	,
zhotovované	zhotovovaný	k2eAgInPc1d1	zhotovovaný
z	z	k7c2	z
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
zdobené	zdobený	k2eAgInPc1d1	zdobený
drahokamy	drahokam	k1gInPc1	drahokam
či	či	k8xC	či
emailem	email	k1gInSc7	email
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
nebo	nebo	k8xC	nebo
z	z	k7c2	z
perleti	perleť	k1gFnSc2	perleť
<g/>
,	,	kIx,	,
se	s	k7c7	s
zrcátkem	zrcátko	k1gNnSc7	zrcátko
ve	v	k7c6	v
víčku	víčko	k1gNnSc6	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Příslušel	příslušet	k5eAaImAgMnS	příslušet
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
polštářek	polštářek	k1gInSc1	polštářek
z	z	k7c2	z
labutího	labutí	k2eAgNnSc2d1	labutí
peří	peří	k1gNnSc2	peří
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
současný	současný	k2eAgInSc1d1	současný
termín	termín	k1gInSc1	termín
labutěnka	labutěnka	k1gFnSc1	labutěnka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pudrování	pudrování	k1gNnSc1	pudrování
patřilo	patřit	k5eAaImAgNnS	patřit
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
výsadám	výsada	k1gFnPc3	výsada
vyšších	vysoký	k2eAgFnPc2d2	vyšší
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
pudrování	pudrování	k1gNnSc3	pudrování
začali	začít	k5eAaPmAgMnP	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
lékaři	lékař	k1gMnPc1	lékař
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
zdravý	zdravý	k2eAgInSc4d1	zdravý
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
bez	bez	k7c2	bez
líčidel	líčidlo	k1gNnPc2	líčidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
prodávají	prodávat	k5eAaImIp3nP	prodávat
pudry	pudr	k1gInPc1	pudr
v	v	k7c6	v
barevné	barevný	k2eAgFnSc6d1	barevná
škále	škála	k1gFnSc6	škála
růžové	růžový	k2eAgFnSc6d1	růžová
a	a	k8xC	a
béžové	béžový	k2eAgFnSc6d1	béžová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
nabídka	nabídka	k1gFnSc1	nabídka
pudrů	pudr	k1gInPc2	pudr
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
rasového	rasový	k2eAgNnSc2d1	rasové
složení	složení	k1gNnSc2	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
pudru	pudr	k1gInSc2	pudr
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
současných	současný	k2eAgMnPc2d1	současný
názorů	názor	k1gInPc2	názor
měla	mít	k5eAaImAgFnS	mít
co	co	k9	co
nejpřesněji	přesně	k6eAd3	přesně
odpovídat	odpovídat	k5eAaImF	odpovídat
přirozené	přirozený	k2eAgFnSc3d1	přirozená
barvě	barva	k1gFnSc3	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgMnPc1d1	profesionální
vizážisté	vizážista	k1gMnPc1	vizážista
proto	proto	k8xC	proto
používají	používat	k5eAaImIp3nP	používat
univerzální	univerzální	k2eAgMnSc1d1	univerzální
transparentní	transparentní	k2eAgMnSc1d1	transparentní
(	(	kIx(	(
<g/>
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
)	)	kIx)	)
pudr	pudr	k1gInSc1	pudr
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
pudry	pudr	k1gInPc1	pudr
v	v	k7c6	v
bronzových	bronzový	k2eAgInPc6d1	bronzový
a	a	k8xC	a
terakotových	terakotový	k2eAgInPc6d1	terakotový
odstínech	odstín	k1gInPc6	odstín
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zastávají	zastávat	k5eAaImIp3nP	zastávat
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
tvářenky	tvářenka	k1gFnSc2	tvářenka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zdravíčka	zdravíčko	k1gNnPc4	zdravíčko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
široce	široko	k6eAd1	široko
používal	používat	k5eAaImAgInS	používat
pudr	pudr	k1gInSc1	pudr
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
bledá	bledý	k2eAgFnSc1d1	bledá
pleť	pleť	k1gFnSc1	pleť
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
znakem	znak	k1gInSc7	znak
urozenosti	urozenost	k1gFnSc2	urozenost
a	a	k8xC	a
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
používají	používat	k5eAaImIp3nP	používat
pudr	pudr	k1gInSc4	pudr
především	především	k9	především
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
při	při	k7c6	při
divadelním	divadelní	k2eAgMnSc6d1	divadelní
a	a	k8xC	a
filmovém	filmový	k2eAgNnSc6d1	filmové
líčení	líčení	k1gNnSc6	líčení
obě	dva	k4xCgFnPc4	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizním	televizní	k2eAgNnSc6d1	televizní
studiu	studio	k1gNnSc6	studio
bývá	bývat	k5eAaImIp3nS	bývat
běžné	běžný	k2eAgNnSc1d1	běžné
pudrovat	pudrovat	k5eAaImF	pudrovat
všechny	všechen	k3xTgFnPc4	všechen
zabírané	zabíraný	k2eAgFnPc4d1	zabíraná
osoby	osoba	k1gFnPc4	osoba
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
odlesků	odlesk	k1gInPc2	odlesk
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Rozsívalová	Rozsívalová	k1gFnSc1	Rozsívalová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Knoblochová	Knoblochová	k1gFnSc1	Knoblochová
<g/>
:	:	kIx,	:
Tajemství	tajemství	k1gNnSc1	tajemství
půvabu	půvab	k1gInSc2	půvab
<g/>
,	,	kIx,	,
Avicenum	Avicenum	k1gInSc1	Avicenum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
