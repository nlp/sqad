<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
druh	druh	k1gInSc1	druh
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
lesku	lesk	k1gInSc2	lesk
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gNnSc2	její
zjemnění	zjemnění	k1gNnSc2	zjemnění
a	a	k8xC	a
zafixování	zafixování	k1gNnSc2	zafixování
makeupu	makeup	k1gInSc2	makeup
<g/>
?	?	kIx.	?
</s>
