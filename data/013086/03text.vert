<p>
<s>
Harald	Harald	k1gInSc1	Harald
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglosaského	anglosaský	k2eAgNnSc2d1	anglosaské
jména	jméno	k1gNnSc2	jméno
Hereweald	Herewealda	k1gFnPc2	Herewealda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stvořeno	stvořen	k2eAgNnSc1d1	stvořeno
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
here	her	k1gFnSc2	her
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
weald	weald	k6eAd1	weald
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
<g/>
"	"	kIx"	"
–	–	k?	–
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
velitel	velitel	k1gMnSc1	velitel
vojska	vojsko	k1gNnSc2	vojsko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vládce	vládce	k1gMnSc2	vládce
boje	boj	k1gInSc2	boj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Staroseverské	staroseverský	k2eAgNnSc1d1	staroseverské
jméno	jméno	k1gNnSc1	jméno
Haraldr	Haraldr	k1gInSc1	Haraldr
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
pět	pět	k4xCc1	pět
králů	král	k1gMnPc2	král
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
nositelé	nositel	k1gMnPc1	nositel
==	==	k?	==
</s>
</p>
<p>
<s>
panovníciHarald	panovníciHarald	k1gMnSc1	panovníciHarald
I.	I.	kA	I.
(	(	kIx(	(
<g/>
911	[number]	k4	911
–	–	k?	–
986	[number]	k4	986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
Harald	Harald	k1gInSc4	Harald
I.	I.	kA	I.
Krásnovlasý	krásnovlasý	k2eAgMnSc1d1	krásnovlasý
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
872-930	[number]	k4	872-930
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1	dánský
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	let	k1gInPc6	let
961	[number]	k4	961
–	–	k?	–
970	[number]	k4	970
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1	dánský
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1074	[number]	k4	1074
až	až	k9	až
1080	[number]	k4	1080
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sigurdsson	Sigurdsson	k1gMnSc1	Sigurdsson
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Gille	Gille	k1gInSc1	Gille
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	let	k1gInPc6	let
1130	[number]	k4	1130
–	–	k?	–
1136	[number]	k4	1136
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
V.	V.	kA	V.
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
králostatníHarald	králostatníHarald	k1gMnSc1	králostatníHarald
Bohr	Bohr	k1gMnSc1	Bohr
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
matematik	matematik	k1gMnSc1	matematik
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
Brattbakk	Brattbakk	k1gMnSc1	Brattbakk
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
útočník	útočník	k1gMnSc1	útočník
</s>
</p>
<p>
<s>
Heinz-Harald	Heinz-Harald	k6eAd1	Heinz-Harald
Frentzen	Frentzen	k2eAgMnSc1d1	Frentzen
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
Gimpel	Gimpel	k1gMnSc1	Gimpel
<g/>
,	,	kIx,	,
východoněmecký	východoněmecký	k2eAgMnSc1d1	východoněmecký
vodní	vodní	k2eAgMnSc1d1	vodní
slalomář	slalomář	k1gMnSc1	slalomář
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
zur	zur	k?	zur
Hausen	Hausen	k1gInSc1	Hausen
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
vědec	vědec	k1gMnSc1	vědec
</s>
</p>
<p>
<s>
Harald	Harald	k6eAd1	Harald
Hennum	Hennum	k1gInSc1	Hennum
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
útočník	útočník	k1gMnSc1	útočník
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
Paetz	Paetz	k1gMnSc1	Paetz
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Harald	Harald	k6eAd1	Harald
Schmid	Schmid	k1gInSc1	Schmid
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
atlet	atlet	k1gMnSc1	atlet
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
Strø	Strø	k1gMnSc1	Strø
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
a	a	k8xC	a
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
PhDr.	PhDr.	kA	PhDr.
Miloslava	Miloslava	k1gFnSc1	Miloslava
Knappová	Knappová	k1gFnSc1	Knappová
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vaše	váš	k3xOp2gNnSc1	váš
dítě	dítě	k1gNnSc1	dítě
jmenovat	jmenovat	k5eAaImF	jmenovat
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Behind	Behind	k1gInSc1	Behind
the	the	k?	the
Name	Name	k1gInSc1	Name
</s>
</p>
