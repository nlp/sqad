<s>
Vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
osa	osa	k1gFnSc1	osa
se	se	k3xPyFc4	se
v	v	k7c6	v
kartézských	kartézský	k2eAgFnPc6d1	kartézská
souřadnicích	souřadnice	k1gFnPc6	souřadnice
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
osa	osa	k1gFnSc1	osa
x	x	k?	x
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
svislá	svislý	k2eAgFnSc1d1	svislá
osa	osa	k1gFnSc1	osa
jako	jako	k8xC	jako
osa	osa	k1gFnSc1	osa
y	y	k?	y
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
