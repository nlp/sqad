<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Passchendale	Passchendala	k1gFnSc6	Passchendala
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
Třetí	třetí	k4xOgFnSc1	třetí
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Yper	Ypera	k1gFnPc2	Ypera
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
u	u	k7c2	u
Yprů	Ypry	k1gInPc2	Ypry
či	či	k8xC	či
u	u	k7c2	u
Ypry	Ypry	k1gInPc7	Ypry
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
střetnutí	střetnutí	k1gNnSc1	střetnutí
zákopové	zákopový	k2eAgFnSc2d1	zákopová
fáze	fáze	k1gFnSc2	fáze
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
na	na	k7c6
západní	západní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
od	od	k7c2
července	červenec	k1gInSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
1917	#num#	k4
a	a	k8xC
bojovalo	bojovat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
hřebeny	hřeben	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
východě	východ	k1gInSc6
od	od	k7c2
belgického	belgický	k2eAgNnSc2d1
města	město	k1gNnSc2
Ypry	Ypry	k1gInPc1
v	v	k7c6
Západních	západní	k2eAgInPc6d1
Flandrech	Flandry	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
nesmyslnosti	nesmyslnost	k1gFnSc2	nesmyslnost
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lidské	lidský	k2eAgFnPc1d1	lidská
ztráty	ztráta	k1gFnPc1	ztráta
celkem	celkem	k6eAd1	celkem
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
prokazatelně	prokazatelně	k6eAd1	prokazatelně
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
neuvěřitelného	uvěřitelný	k2eNgInSc2d1	neuvěřitelný
počtu	počet	k1gInSc2	počet
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
500	[number]	k4	500
000	[number]	k4	000
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
stran	strana	k1gFnPc2	strana
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
významného	významný	k2eAgInSc2d1	významný
územního	územní	k2eAgInSc2d1	územní
ani	ani	k8xC	ani
materiálního	materiální	k2eAgInSc2d1	materiální
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
strategických	strategický	k2eAgInPc2d1	strategický
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
odmítalo	odmítat	k5eAaImAgNnS	odmítat
vzdát	vzdát	k5eAaPmF	vzdát
boj	boj	k1gInSc4	boj
o	o	k7c4	o
území	území	k1gNnSc4	území
velké	velký	k2eAgNnSc4d1	velké
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obětí	oběť	k1gFnPc2	oběť
přitom	přitom	k6eAd1	přitom
nepadlo	padnout	k5eNaPmAgNnS	padnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
močálech	močál	k1gInPc6	močál
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
šílenství	šílenství	k1gNnSc6	šílenství
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
dostatečně	dostatečně	k6eAd1	dostatečně
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgInPc1d1	současný
odhadované	odhadovaný	k2eAgInPc1d1	odhadovaný
počty	počet	k1gInPc1	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
odhadů	odhad	k1gInPc2	odhad
liší	lišit	k5eAaImIp3nP	lišit
o	o	k7c4	o
statisíce	statisíce	k1gInPc4	statisíce
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
padlo	padnout	k5eAaImAgNnS	padnout
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
mezi	mezi	k7c7	mezi
500	[number]	k4	500
000	[number]	k4	000
až	až	k9	až
700	[number]	k4	700
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojska	vojsko	k1gNnSc2	vojsko
Dohody	dohoda	k1gFnSc2	dohoda
docílila	docílit	k5eAaPmAgFnS	docílit
jen	jen	k6eAd1	jen
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
fronta	fronta	k1gFnSc1	fronta
posunula	posunout	k5eAaPmAgFnS	posunout
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
8	[number]	k4	8
km	km	kA	km
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnPc4	území
ztracena	ztraceno	k1gNnSc2	ztraceno
již	již	k6eAd1	již
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
-	-	kIx~	-
když	když	k8xS	když
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
padla	padnout	k5eAaImAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Němcům	Němec	k1gMnPc3	Němec
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lys	lysa	k1gFnPc2	lysa
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
jarní	jarní	k2eAgFnSc2d1	jarní
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Belgičané	Belgičan	k1gMnPc1	Belgičan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zabránit	zabránit	k5eAaPmF	zabránit
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
postupu	postup	k1gInSc6	postup
<g/>
,	,	kIx,	,
vypustili	vypustit	k5eAaPmAgMnP	vypustit
v	v	k7c6	v
předstihu	předstih	k1gInSc6	předstih
mořské	mořský	k2eAgFnSc2d1	mořská
hráze	hráz	k1gFnSc2	hráz
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
krajina	krajina	k1gFnSc1	krajina
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
močál	močál	k1gInSc4	močál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
terénu	terén	k1gInSc6	terén
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
drželo	držet	k5eAaImAgNnS	držet
bahno	bahno	k1gNnSc4	bahno
i	i	k9	i
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
sucho	sucho	k6eAd1	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
britské	britský	k2eAgInPc1d1	britský
útoky	útok	k1gInPc1	útok
se	se	k3xPyFc4	se
tak	tak	k9	tak
potýkaly	potýkat	k5eAaImAgFnP	potýkat
s	s	k7c7	s
blátem	bláto	k1gNnSc7	bláto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
fázích	fáze	k1gFnPc6	fáze
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
utopila	utopit	k5eAaPmAgFnS	utopit
v	v	k7c6	v
močále	močál	k1gInSc6	močál
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
protiútoky	protiútok	k1gInPc1	protiútok
měly	mít	k5eAaImAgInP	mít
prakticky	prakticky	k6eAd1	prakticky
stejné	stejný	k2eAgFnPc1d1	stejná
obtíže	obtíž	k1gFnPc1	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
zajistili	zajistit	k5eAaPmAgMnP	zajistit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
podzemními	podzemní	k2eAgFnPc7d1	podzemní
minami	mina	k1gFnPc7	mina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
německých	německý	k2eAgInPc2d1	německý
útoků	útok	k1gInPc2	útok
odpálili	odpálit	k5eAaPmAgMnP	odpálit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
minuty	minuta	k1gFnSc2	minuta
tak	tak	k9	tak
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10.000	[number]	k4	10.000
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
ztráty	ztráta	k1gFnPc1	ztráta
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Paschendale	Paschendala	k1gFnSc6	Paschendala
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
310	[number]	k4	310
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
na	na	k7c4	na
britské	britský	k2eAgNnSc4d1	Britské
<g/>
,	,	kIx,	,
85	[number]	k4	85
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
na	na	k7c4	na
francouzské	francouzský	k2eAgFnPc4d1	francouzská
a	a	k8xC	a
260	[number]	k4	260
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
v	v	k7c6	v
maximálních	maximální	k2eAgInPc6d1	maximální
odhadech	odhad	k1gInPc6	odhad
až	až	k9	až
400	[number]	k4	400
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Douglas	Douglas	k1gMnSc1	Douglas
Haig	Haig	k1gMnSc1	Haig
při	při	k7c6	při
původním	původní	k2eAgNnSc6d1	původní
plánování	plánování	k1gNnSc6	plánování
operace	operace	k1gFnSc2	operace
počítal	počítat	k5eAaImAgInS	počítat
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
50	[number]	k4	50
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgInS	odmítat
požadavky	požadavek	k1gInPc4	požadavek
generála	generál	k1gMnSc4	generál
Huberta	Hubert	k1gMnSc2	Hubert
Plumera	Plumer	k1gMnSc2	Plumer
na	na	k7c4	na
dvojnásobný	dvojnásobný	k2eAgInSc4d1	dvojnásobný
počet	počet	k1gInSc4	počet
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
střídání	střídání	k1gNnSc3	střídání
asi	asi	k9	asi
trojnásobný	trojnásobný	k2eAgInSc4d1	trojnásobný
počet	počet	k1gInSc4	počet
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důvody	důvod	k1gInPc1	důvod
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
dobýt	dobýt	k5eAaPmF	dobýt
ponorkové	ponorkový	k2eAgInPc4d1	ponorkový
přístavy	přístav	k1gInPc4	přístav
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
držení	držení	k1gNnSc6	držení
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
naplánovali	naplánovat	k5eAaBmAgMnP	naplánovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
(	(	kIx(	(
<g/>
prosazovanou	prosazovaný	k2eAgFnSc4d1	prosazovaná
již	již	k6eAd1	již
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
na	na	k7c6	na
Sommě	Somma	k1gFnSc6	Somma
<g/>
)	)	kIx)	)
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
<g/>
)	)	kIx)	)
u	u	k7c2	u
Yper	Ypera	k1gFnPc2	Ypera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podrobněji	podrobně	k6eAd2	podrobně
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
bojů	boj	k1gInPc2	boj
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
německé	německý	k2eAgFnPc1d1	německá
linie	linie	k1gFnPc1	linie
na	na	k7c6	na
messinském	messinský	k2eAgInSc6d1	messinský
hřebenu	hřeben	k1gInSc6	hřeben
podminovány	podminován	k2eAgFnPc4d1	podminována
a	a	k8xC	a
vyhozeny	vyhozen	k2eAgFnPc4d1	vyhozena
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
2	[number]	k4	2
z	z	k7c2	z
19	[number]	k4	19
ohromných	ohromný	k2eAgFnPc2d1	ohromná
náloží	nálož	k1gFnPc2	nálož
nevybuchly	vybuchnout	k5eNaPmAgFnP	vybuchnout
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc4	výsledek
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Brity	Brit	k1gMnPc4	Brit
nad	nad	k7c7	nad
očekávaní	očekávaný	k2eAgMnPc1d1	očekávaný
příznivý	příznivý	k2eAgInSc4d1	příznivý
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
nečekal	čekat	k5eNaImAgMnS	čekat
ani	ani	k8xC	ani
strůjce	strůjce	k1gMnSc1	strůjce
operačního	operační	k2eAgInSc2d1	operační
plánu	plán	k1gInSc2	plán
Herbert	Herbert	k1gMnSc1	Herbert
Plumer	Plumer	k1gMnSc1	Plumer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
počítal	počítat	k5eAaImAgInS	počítat
až	až	k9	až
s	s	k7c7	s
50	[number]	k4	50
<g/>
%	%	kIx~	%
ztrátami	ztráta	k1gFnPc7	ztráta
při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
takovým	takový	k3xDgInPc3	takový
odhadům	odhad	k1gInPc3	odhad
byly	být	k5eAaImAgFnP	být
ztráty	ztráta	k1gFnPc1	ztráta
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
jednotky	jednotka	k1gFnPc1	jednotka
spořádaně	spořádaně	k6eAd1	spořádaně
postoupily	postoupit	k5eAaPmAgFnP	postoupit
do	do	k7c2	do
cílových	cílový	k2eAgFnPc2d1	cílová
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Dalšímu	další	k2eAgInSc3d1	další
postupu	postup	k1gInSc3	postup
zabránila	zabránit	k5eAaPmAgFnS	zabránit
jen	jen	k9	jen
nemožnost	nemožnost	k1gFnSc1	nemožnost
přesnější	přesný	k2eAgFnSc2d2	přesnější
britské	britský	k2eAgFnSc2d1	britská
krycí	krycí	k2eAgFnSc2d1	krycí
palby	palba	k1gFnSc2	palba
přes	přes	k7c4	přes
hřeben	hřeben	k1gInSc4	hřeben
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
přesnost	přesnost	k1gFnSc1	přesnost
německého	německý	k2eAgNnSc2d1	německé
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
za	za	k7c4	za
hřeben	hřeben	k1gInSc4	hřeben
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
protiútok	protiútok	k1gInSc1	protiútok
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
ničeho	nic	k3yNnSc2	nic
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
naopak	naopak	k6eAd1	naopak
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hlouběji	hluboko	k6eAd2	hluboko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
úvodní	úvodní	k2eAgNnSc1d1	úvodní
vítězství	vítězství	k1gNnSc1	vítězství
je	být	k5eAaImIp3nS	být
také	také	k9	také
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Messines	Messinesa	k1gFnPc2	Messinesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Avšak	avšak	k8xC	avšak
další	další	k2eAgFnSc1d1	další
fáze	fáze	k1gFnSc1	fáze
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
se	se	k3xPyFc4	se
opozdila	opozdit	k5eAaPmAgFnS	opozdit
<g/>
,	,	kIx,	,
britské	britský	k2eAgNnSc1d1	Britské
velení	velení	k1gNnSc1	velení
naplánovalo	naplánovat	k5eAaBmAgNnS	naplánovat
operaci	operace	k1gFnSc4	operace
na	na	k7c4	na
konec	konec	k1gInSc4	konec
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
dobýt	dobýt	k5eAaPmF	dobýt
vesnici	vesnice	k1gFnSc4	vesnice
Passchendaele	Passchendael	k1gInSc2	Passchendael
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
pršet	pršet	k5eAaImF	pršet
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
připravení	připravení	k1gNnSc2	připravení
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
zpomalenou	zpomalený	k2eAgFnSc4d1	zpomalená
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
odrazili	odrazit	k5eAaPmAgMnP	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
dlouhodobým	dlouhodobý	k2eAgInPc3d1	dlouhodobý
bojům	boj	k1gInPc3	boj
o	o	k7c6	o
tuto	tento	k3xDgFnSc4	tento
vesnici	vesnice	k1gFnSc6	vesnice
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Passchendaele	Passchendael	k1gInSc2	Passchendael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Generál	generál	k1gMnSc1	generál
Herbert	Herbert	k1gMnSc1	Herbert
Plumer	Plumer	k1gMnSc1	Plumer
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
útok	útok	k1gInSc4	útok
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
demoralizováni	demoralizovat	k5eAaBmNgMnP	demoralizovat
<g/>
.	.	kIx.	.
</s>
<s>
Účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
britská	britský	k2eAgFnSc1d1	britská
5	[number]	k4	5
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
Huberta	Hubert	k1gMnSc2	Hubert
Gougha	Gough	k1gMnSc2	Gough
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
2	[number]	k4	2
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
Herberta	Herbert	k1gMnSc4	Herbert
Plumera	Plumer	k1gMnSc4	Plumer
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
pluk	pluk	k1gInSc4	pluk
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
12	[number]	k4	12
dohodových	dohodový	k2eAgFnPc2d1	dohodová
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
zuřilo	zuřit	k5eAaImAgNnS	zuřit
mohutné	mohutný	k2eAgNnSc1d1	mohutné
dělostřelecké	dělostřelecký	k2eAgNnSc1d1	dělostřelecké
bombardování	bombardování	k1gNnSc1	bombardování
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
však	však	k9	však
Dohoda	dohoda	k1gFnSc1	dohoda
vzdala	vzdát	k5eAaPmAgFnS	vzdát
momentu	moment	k1gInSc3	moment
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
pěchota	pěchota	k1gFnSc1	pěchota
na	na	k7c6	na
18	[number]	k4	18
kilometrech	kilometr	k1gInPc6	kilometr
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
dohodové	dohodový	k2eAgFnSc2d1	dohodová
vojáky	voják	k1gMnPc4	voják
zadrželi	zadržet	k5eAaPmAgMnP	zadržet
<g/>
.	.	kIx.	.
</s>
<s>
Pomohl	pomoct	k5eAaPmAgMnS	pomoct
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
rozmáčený	rozmáčený	k2eAgInSc4d1	rozmáčený
několika	několik	k4yIc7	několik
prudkými	prudký	k2eAgFnPc7d1	prudká
bouřkami	bouřka	k1gFnPc7	bouřka
během	během	k7c2	během
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
útok	útok	k1gInSc1	útok
menších	malý	k2eAgInPc2d2	menší
zisků	zisk	k1gInPc2	zisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
bouřky	bouřka	k1gFnPc1	bouřka
a	a	k8xC	a
lijavce	lijavec	k1gInPc1	lijavec
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
<g/>
,	,	kIx,	,
neustálé	neustálý	k2eAgInPc4d1	neustálý
husté	hustý	k2eAgInPc4d1	hustý
deště	dešť	k1gInPc4	dešť
proměnily	proměnit	k5eAaPmAgFnP	proměnit
bitevní	bitevní	k2eAgNnSc4d1	bitevní
pole	pole	k1gNnSc4	pole
v	v	k7c6	v
bažinu	bažina	k1gFnSc4	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
útočilo	útočit	k5eAaImAgNnS	útočit
jen	jen	k9	jen
po	po	k7c6	po
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
chodníčcích	chodníček	k1gInPc6	chodníček
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
si	se	k3xPyFc3	se
sebou	se	k3xPyFc7	se
část	část	k1gFnSc4	část
hatí	hatit	k5eAaImIp3nS	hatit
nesl	nést	k5eAaImAgMnS	nést
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
kolem	kolem	k7c2	kolem
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
spadl	spadnout	k5eAaPmAgMnS	spadnout
mimo	mimo	k7c4	mimo
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
často	často	k6eAd1	často
utonout	utonout	k5eAaPmF	utonout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
mohli	moct	k5eAaImAgMnP	moct
spolubojovníci	spolubojovník	k1gMnPc1	spolubojovník
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Dohodě	dohoda	k1gFnSc3	dohoda
uvízly	uvíznout	k5eAaPmAgInP	uvíznout
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
tanky	tank	k1gInPc4	tank
a	a	k8xC	a
nemohla	moct	k5eNaImAgFnS	moct
své	svůj	k3xOyFgInPc4	svůj
útoky	útok	k1gInPc4	útok
podpořit	podpořit	k5eAaPmF	podpořit
novou	nový	k2eAgFnSc7d1	nová
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
nové	nový	k2eAgNnSc1d1	nové
velení	velení	k1gNnSc1	velení
(	(	kIx(	(
<g/>
z	z	k7c2	z
páté	pátý	k4xOgFnSc2	pátý
armády	armáda	k1gFnSc2	armáda
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
generál	generál	k1gMnSc1	generál
Herbert	Herbert	k1gMnSc1	Herbert
Plumer	Plumer	k1gMnSc1	Plumer
<g/>
)	)	kIx)	)
odklonilo	odklonit	k5eAaPmAgNnS	odklonit
od	od	k7c2	od
masivních	masivní	k2eAgMnPc2d1	masivní
úderů	úder	k1gInPc2	úder
a	a	k8xC	a
plánovalo	plánovat	k5eAaImAgNnS	plánovat
sérii	série	k1gFnSc4	série
menších	malý	k2eAgInPc2d2	menší
<g/>
,	,	kIx,	,
rychlejších	rychlý	k2eAgInPc2d2	rychlejší
a	a	k8xC	a
překvapivějších	překvapivý	k2eAgInPc2d2	překvapivější
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strategie	strategie	k1gFnSc1	strategie
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
bite	bit	k1gInSc5	bit
and	and	k?	and
hold	hold	k1gInSc4	hold
-	-	kIx~	-
zakousnout	zakousnout	k5eAaPmF	zakousnout
a	a	k8xC	a
držet	držet	k5eAaImF	držet
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Dohoda	dohoda	k1gFnSc1	dohoda
jistých	jistý	k2eAgInPc2d1	jistý
zisků	zisk	k1gInPc2	zisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Září	září	k1gNnSc1	září
sice	sice	k8xC	sice
bylo	být	k5eAaImAgNnS	být
suché	suchý	k2eAgNnSc1d1	suché
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bahno	bahno	k1gNnSc1	bahno
již	již	k6eAd1	již
nevyschlo	vyschnout	k5eNaPmAgNnS	vyschnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celá	k1gFnSc6	celá
září	září	k1gNnSc2	září
a	a	k8xC	a
i	i	k9	i
začátkem	začátkem	k7c2	začátkem
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
útoky	útok	k1gInPc1	útok
dařily	dařit	k5eAaImAgInP	dařit
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
hlavních	hlavní	k2eAgInPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obratu	obrat	k1gInSc3	obrat
<g/>
,	,	kIx,	,
plánované	plánovaný	k2eAgInPc4d1	plánovaný
útoky	útok	k1gInPc4	útok
nedosahovaly	dosahovat	k5eNaImAgFnP	dosahovat
zisků	zisk	k1gInPc2	zisk
a	a	k8xC	a
vršily	vršit	k5eAaImAgFnP	vršit
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
hromady	hromada	k1gFnPc1	hromada
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
vlhký	vlhký	k2eAgInSc1d1	vlhký
<g/>
,	,	kIx,	,
a	a	k8xC	a
postup	postup	k1gInSc1	postup
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
zastaven	zastavit	k5eAaPmNgInS	zastavit
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
útoky	útok	k1gInPc1	útok
byly	být	k5eAaImAgInP	být
omezeny	omezit	k5eAaPmNgInP	omezit
a	a	k8xC	a
poté	poté	k6eAd1	poté
definitivně	definitivně	k6eAd1	definitivně
odvolány	odvolán	k2eAgFnPc1d1	odvolána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
akce	akce	k1gFnPc1	akce
bitvy	bitva	k1gFnSc2	bitva
byly	být	k5eAaImAgFnP	být
dílem	díl	k1gInSc7	díl
kanadských	kanadský	k2eAgFnPc2d1	kanadská
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
útocích	útok	k1gInPc6	útok
(	(	kIx(	(
<g/>
následovaly	následovat	k5eAaImAgInP	následovat
vždy	vždy	k6eAd1	vždy
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
<g/>
)	)	kIx)	)
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc2	listopad
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
výše	výše	k1gFnSc2	výše
položené	položený	k2eAgFnSc2d1	položená
pozice	pozice	k1gFnSc2	pozice
-	-	kIx~	-
obce	obec	k1gFnSc2	obec
Passchendaele	Passchendael	k1gInSc2	Passchendael
a	a	k8xC	a
blízkou	blízký	k2eAgFnSc4d1	blízká
výšinu	výšina	k1gFnSc4	výšina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Passchendaele	Passchendael	k1gInSc2	Passchendael
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
první	první	k4xOgFnSc4	první
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Passchendaele	Passchendael	k1gInSc2	Passchendael
<g/>
,	,	kIx,	,
postoupily	postoupit	k5eAaPmAgFnP	postoupit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
síly	síla	k1gFnSc2	síla
ANZACu	ANZACus	k1gInSc2	ANZACus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Battle	Battle	k1gFnSc2	Battle
of	of	k?	of
Passchendaele	Passchendael	k1gInSc2	Passchendael
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
různé	různý	k2eAgFnPc4d1	různá
částečky	částečka	k1gFnPc4	částečka
<g/>
,	,	kIx,	,
data	datum	k1gNnPc4	datum
a	a	k8xC	a
čísla	číslo	k1gNnPc4	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Třetí	třetí	k4xOgFnSc1	třetí
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Yper	Ypera	k1gFnPc2	Ypera
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
