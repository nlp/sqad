<s>
O	o	k7c6	o
šílenství	šílenství	k1gNnSc6	šílenství
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
dostatečně	dostatečně	k6eAd1	dostatečně
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgInPc1d1	současný
odhadované	odhadovaný	k2eAgInPc1d1	odhadovaný
počty	počet	k1gInPc1	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
odhadů	odhad	k1gInPc2	odhad
liší	lišit	k5eAaImIp3nP	lišit
o	o	k7c4	o
statisíce	statisíce	k1gInPc4	statisíce
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
padlo	padnout	k5eAaImAgNnS	padnout
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
mezi	mezi	k7c7	mezi
500	[number]	k4	500
000	[number]	k4	000
až	až	k9	až
700	[number]	k4	700
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
