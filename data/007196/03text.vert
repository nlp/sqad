<s>
Neuron	neuron	k1gInSc1	neuron
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
nervová	nervový	k2eAgFnSc1d1	nervová
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
funkční	funkční	k2eAgFnSc1d1	funkční
a	a	k8xC	a
histologická	histologický	k2eAgFnSc1d1	histologická
jednotka	jednotka	k1gFnSc1	jednotka
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vysoce	vysoce	k6eAd1	vysoce
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
vést	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
zpracovat	zpracovat	k5eAaPmF	zpracovat
a	a	k8xC	a
odpovědět	odpovědět	k5eAaPmF	odpovědět
na	na	k7c4	na
speciální	speciální	k2eAgInPc4d1	speciální
signály	signál	k1gInPc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Přenáší	přenášet	k5eAaImIp3nP	přenášet
a	a	k8xC	a
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
i	i	k8xC	i
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
podmiňují	podmiňovat	k5eAaImIp3nP	podmiňovat
schopnost	schopnost	k1gFnSc4	schopnost
organismu	organismus	k1gInSc2	organismus
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Neuron	neuron	k1gInSc4	neuron
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc4d1	základní
jednotku	jednotka	k1gFnSc4	jednotka
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
popsal	popsat	k5eAaPmAgMnS	popsat
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
připisován	připisovat	k5eAaImNgInS	připisovat
Španělovi	Španěl	k1gMnSc3	Španěl
Cajalovi	Cajal	k1gMnSc3	Cajal
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
neuronu	neuron	k1gInSc2	neuron
(	(	kIx(	(
<g/>
perikaryon	perikaryon	k1gInSc1	perikaryon
<g/>
,	,	kIx,	,
neurocyt	neurocyt	k1gInSc1	neurocyt
<g/>
,	,	kIx,	,
soma	soma	k1gFnSc1	soma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
nervové	nervový	k2eAgFnSc2d1	nervová
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
perikaryonu	perikaryon	k1gInSc2	perikaryon
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
6	[number]	k4	6
μ	μ	k?	μ
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnPc1d1	malá
zrnité	zrnitý	k2eAgFnPc1d1	zrnitá
buňky	buňka	k1gFnPc1	buňka
kůry	kůra	k1gFnSc2	kůra
mozečku	mozeček	k1gInSc2	mozeček
<g/>
)	)	kIx)	)
do	do	k7c2	do
100	[number]	k4	100
μ	μ	k?	μ
(	(	kIx(	(
<g/>
velké	velký	k2eAgInPc1d1	velký
pyramidové	pyramidový	k2eAgInPc1d1	pyramidový
neurony	neuron	k1gInPc1	neuron
motorických	motorický	k2eAgFnPc2d1	motorická
oblastí	oblast	k1gFnPc2	oblast
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnSc3d1	velká
<g/>
,	,	kIx,	,
kulovité	kulovitý	k2eAgFnSc3d1	kulovitá
nebo	nebo	k8xC	nebo
oválné	oválný	k2eAgFnSc3d1	oválná
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zpravidla	zpravidla	k6eAd1	zpravidla
jedno	jeden	k4xCgNnSc4	jeden
velké	velký	k2eAgNnSc4d1	velké
jadérko	jadérko	k1gNnSc4	jadérko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytoplasmě	cytoplasma	k1gFnSc6	cytoplasma
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
neuroplasma	neuroplasmum	k1gNnSc2	neuroplasmum
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
organely	organela	k1gFnPc4	organela
a	a	k8xC	a
struktury	struktura	k1gFnPc4	struktura
shodné	shodný	k2eAgFnPc4d1	shodná
s	s	k7c7	s
organelovou	organelův	k2eAgFnSc7d1	organelův
výbavou	výbava	k1gFnSc7	výbava
ostatních	ostatní	k2eAgFnPc2d1	ostatní
somatických	somatický	k2eAgFnPc2d1	somatická
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
vyniká	vynikat	k5eAaImIp3nS	vynikat
granulární	granulární	k2eAgNnSc1d1	granulární
endoplasmatické	endoplasmatický	k2eAgNnSc1d1	endoplasmatické
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
perikaryu	perikaryus	k1gInSc6	perikaryus
tvoří	tvořit	k5eAaImIp3nS	tvořit
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
strukturu	struktura	k1gFnSc4	struktura
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Nisslova	Nisslův	k2eAgFnSc1d1	Nisslův
tigroidní	tigroidní	k2eAgFnPc4d1	tigroidní
substance	substance	k1gFnPc4	substance
<g/>
,	,	kIx,	,
a	a	k8xC	a
Golgiho	Golgi	k1gMnSc4	Golgi
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
právě	právě	k9	právě
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
neuronu	neuron	k1gInSc2	neuron
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
výběžcích	výběžek	k1gInPc6	výběžek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
početné	početný	k2eAgFnPc1d1	početná
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
neuronech	neuron	k1gInPc6	neuron
jsou	být	k5eAaImIp3nP	být
lysosomy	lysosom	k1gInPc1	lysosom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
zrna	zrno	k1gNnPc4	zrno
(	(	kIx(	(
<g/>
granula	granula	k1gFnSc1	granula
<g/>
)	)	kIx)	)
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
lipofuchsinu	lipofuchsina	k1gFnSc4	lipofuchsina
a	a	k8xC	a
neuromelaninu	neuromelanina	k1gFnSc4	neuromelanina
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
plně	plně	k6eAd1	plně
diferencované	diferencovaný	k2eAgInPc1d1	diferencovaný
neurony	neuron	k1gInPc1	neuron
nemají	mít	k5eNaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
mitotického	mitotický	k2eAgNnSc2d1	mitotické
dělení	dělení	k1gNnSc2	dělení
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
cytoplasmě	cytoplasma	k1gFnSc6	cytoplasma
byly	být	k5eAaImAgFnP	být
prokázány	prokázán	k2eAgFnPc1d1	prokázána
centrioly	centriola	k1gFnPc1	centriola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
i	i	k8xC	i
výběžcích	výběžek	k1gInPc6	výběžek
neuronu	neuron	k1gInSc2	neuron
jsou	být	k5eAaImIp3nP	být
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
intermediální	intermediální	k2eAgNnPc1d1	intermediální
filamenta	filamento	k1gNnPc1	filamento
<g/>
,	,	kIx,	,
neurofilamenta	neurofilamento	k1gNnPc1	neurofilamento
<g/>
,	,	kIx,	,
a	a	k8xC	a
mikrotubuly	mikrotubul	k1gInPc1	mikrotubul
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
neurotubuly	neurotubul	k1gInPc1	neurotubul
<g/>
.	.	kIx.	.
</s>
<s>
Agregací	agregace	k1gFnPc2	agregace
neurofilament	neurofilament	k1gMnSc1	neurofilament
a	a	k8xC	a
neurotubulů	neurotubul	k1gMnPc2	neurotubul
vznikají	vznikat	k5eAaImIp3nP	vznikat
neurofibrily	neurofibrila	k1gFnPc1	neurofibrila
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
histologických	histologický	k2eAgInPc6d1	histologický
preparátech	preparát	k1gInPc6	preparát
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
impregnací	impregnace	k1gFnSc7	impregnace
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mikrotubulech	mikrotubul	k1gInPc6	mikrotubul
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
vzdáleným	vzdálený	k2eAgNnPc3d1	vzdálené
zakončením	zakončení	k1gNnPc3	zakončení
dopravovány	dopravován	k2eAgFnPc1d1	dopravována
neuronální	neuronální	k2eAgFnPc1d1	neuronální
granule	granule	k1gFnPc1	granule
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
mediátorovou	mediátorový	k2eAgFnSc4d1	mediátorová
RNA	RNA	kA	RNA
kódující	kódující	k2eAgInPc4d1	kódující
proteiny	protein	k1gInPc4	protein
potřebné	potřebný	k2eAgInPc4d1	potřebný
v	v	k7c6	v
nervovém	nervový	k2eAgNnSc6d1	nervové
zakončení	zakončení	k1gNnSc6	zakončení
<g/>
;	;	kIx,	;
i	i	k9	i
když	když	k8xS	když
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
faktory	faktor	k1gInPc1	faktor
potřebné	potřebný	k2eAgInPc1d1	potřebný
pro	pro	k7c4	pro
překlad	překlad	k1gInSc4	překlad
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
transportu	transport	k1gInSc2	transport
zastaven	zastavit	k5eAaPmNgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Neuronální	neuronální	k2eAgFnPc1d1	neuronální
granule	granule	k1gFnPc1	granule
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
regeneraci	regenerace	k1gFnSc6	regenerace
axonů	axon	k1gInPc2	axon
a	a	k8xC	a
poruchy	porucha	k1gFnSc2	porucha
v	v	k7c6	v
transportu	transport	k1gInSc6	transport
mRNA	mRNA	k?	mRNA
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
neuronálními	neuronální	k2eAgFnPc7d1	neuronální
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Výběžky	výběžek	k1gInPc1	výběžek
neuronů	neuron	k1gInPc2	neuron
jsou	být	k5eAaImIp3nP	být
dvou	dva	k4xCgFnPc2	dva
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
krátké	krátká	k1gFnSc2	krátká
–	–	k?	–
tzv.	tzv.	kA	tzv.
dendrity	dendrit	k1gInPc4	dendrit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dostředivé	dostředivý	k2eAgInPc4d1	dostředivý
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
–	–	k?	–
tzv.	tzv.	kA	tzv.
neurity	neurit	k1gInPc4	neurit
neboli	neboli	k8xC	neboli
axony	axon	k1gInPc4	axon
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
odstředivé	odstředivý	k2eAgInPc1d1	odstředivý
Výběžky	výběžek	k1gInPc1	výběžek
jsou	být	k5eAaImIp3nP	být
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
neuronu	neuron	k1gInSc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zničení	zničení	k1gNnSc6	zničení
neurocytu	neurocyt	k1gInSc2	neurocyt
výběžky	výběžek	k1gInPc1	výběžek
degenerují	degenerovat	k5eAaBmIp3nP	degenerovat
a	a	k8xC	a
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
výběžek	výběžek	k1gInSc1	výběžek
přerušen	přerušen	k2eAgInSc1d1	přerušen
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ztratí	ztratit	k5eAaPmIp3nS	ztratit
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
neurocytem	neurocyt	k1gInSc7	neurocyt
zaniká	zanikat	k5eAaImIp3nS	zanikat
(	(	kIx(	(
<g/>
Wallerova	Wallerův	k2eAgFnSc1d1	Wallerova
degenerace	degenerace	k1gFnSc1	degenerace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pahýl	pahýl	k1gInSc1	pahýl
výběžku	výběžek	k1gInSc2	výběžek
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
regenerovat	regenerovat	k5eAaBmF	regenerovat
(	(	kIx(	(
<g/>
Wallerova	Wallerův	k2eAgFnSc1d1	Wallerova
regenerace	regenerace	k1gFnSc1	regenerace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
i	i	k9	i
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
nervu	nerv	k1gInSc2	nerv
šance	šance	k1gFnSc2	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
časem	časem	k6eAd1	časem
plně	plně	k6eAd1	plně
obnoví	obnovit	k5eAaPmIp3nS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Dendrity	dendrit	k1gInPc1	dendrit
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
neuroplasmy	neuroplasma	k1gFnPc4	neuroplasma
jako	jako	k8xC	jako
tělo	tělo	k1gNnSc4	tělo
neuronu	neuron	k1gInSc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
odstupu	odstup	k1gInSc2	odstup
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
jsou	být	k5eAaImIp3nP	být
tlusté	tlustý	k2eAgFnPc1d1	tlustá
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
<g/>
.	.	kIx.	.
</s>
<s>
Dendrity	dendrit	k1gInPc1	dendrit
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
větví	větvit	k5eAaImIp3nP	větvit
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
neurocytu	neurocyt	k1gInSc2	neurocyt
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
myelinovou	myelinový	k2eAgFnSc4d1	myelinová
pochvu	pochva	k1gFnSc4	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dendritů	dendrit	k1gInPc2	dendrit
bývají	bývat	k5eAaImIp3nP	bývat
přítomné	přítomný	k2eAgInPc1d1	přítomný
dendritické	dendritický	k2eAgInPc1d1	dendritický
trny	trn	k1gInPc1	trn
<g/>
.	.	kIx.	.
</s>
<s>
Axon	Axon	k1gInSc1	Axon
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
jeho	jeho	k3xOp3gInSc2	jeho
odstupu	odstup	k1gInSc2	odstup
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
neuronu	neuron	k1gInSc2	neuron
není	být	k5eNaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
Nisslova	Nisslův	k2eAgFnSc1d1	Nisslův
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
odstupový	odstupový	k2eAgInSc1d1	odstupový
konus	konus	k1gInSc1	konus
axonu	axonout	k5eAaImIp1nS	axonout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytoplasmě	cytoplasma	k1gFnSc6	cytoplasma
axonu	axon	k1gInSc2	axon
(	(	kIx(	(
<g/>
axoplasma	axoplasma	k1gFnSc1	axoplasma
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
podélně	podélně	k6eAd1	podélně
probíhající	probíhající	k2eAgNnPc1d1	probíhající
neurofilamenta	neurofilamento	k1gNnPc1	neurofilamento
<g/>
,	,	kIx,	,
vyztužující	vyztužující	k2eAgInSc1d1	vyztužující
axon	axon	k1gInSc1	axon
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
větvit	větvit	k5eAaImF	větvit
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
kolaterály	kolaterála	k1gFnPc4	kolaterála
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
rozdělení	rozdělení	k1gNnSc1	rozdělení
axonu	axon	k1gInSc2	axon
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
telodendron	telodendron	k1gInSc1	telodendron
(	(	kIx(	(
<g/>
telodendrie	telodendrie	k1gFnSc1	telodendrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
distálním	distální	k2eAgInSc6d1	distální
konci	konec	k1gInSc6	konec
axonu	axon	k1gInSc2	axon
je	být	k5eAaImIp3nS	být
axoterminála	axoterminála	k1gFnSc1	axoterminála
<g/>
,	,	kIx,	,
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
sekreční	sekreční	k2eAgFnSc1d1	sekreční
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
při	při	k7c6	při
podráždění	podráždění	k1gNnSc6	podráždění
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
neurotransmitery	neurotransmiter	k1gInPc4	neurotransmiter
do	do	k7c2	do
synaptické	synaptický	k2eAgFnSc2d1	synaptická
štěrbiny	štěrbina	k1gFnSc2	štěrbina
<g/>
.	.	kIx.	.
</s>
<s>
Axony	Axon	k1gInPc1	Axon
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
obaleny	obalen	k2eAgInPc1d1	obalen
myelinovou	myelinový	k2eAgFnSc7d1	myelinová
pochvou	pochva	k1gFnSc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
neurony	neuron	k1gInPc1	neuron
CNS	CNS	kA	CNS
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
axonů	axon	k1gInPc2	axon
bez	bez	k7c2	bez
obalů	obal	k1gInPc2	obal
(	(	kIx(	(
<g/>
holá	holý	k2eAgNnPc1d1	holé
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Axon	Axon	k1gMnSc1	Axon
obalený	obalený	k2eAgMnSc1d1	obalený
pochvou	pochva	k1gFnSc7	pochva
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nervové	nervový	k2eAgNnSc1d1	nervové
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Pochva	pochva	k1gFnSc1	pochva
je	být	k5eAaImIp3nS	být
vytvářena	vytvářet	k5eAaImNgFnS	vytvářet
gliovými	gliův	k2eAgFnPc7d1	gliův
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
Schwannovými	Schwannův	k2eAgFnPc7d1	Schwannova
buňkami	buňka	k1gFnPc7	buňka
na	na	k7c6	na
periferních	periferní	k2eAgInPc6d1	periferní
nervech	nerv	k1gInPc6	nerv
a	a	k8xC	a
oligodendrogliemi	oligodendroglie	k1gFnPc7	oligodendroglie
v	v	k7c6	v
CNS	CNS	kA	CNS
<g/>
.	.	kIx.	.
</s>
<s>
Myelinová	myelinový	k2eAgFnSc1d1	myelinová
pochva	pochva	k1gFnSc1	pochva
vytvářena	vytvářet	k5eAaImNgFnS	vytvářet
Schwannovými	Schwannův	k2eAgFnPc7d1	Schwannova
buňkami	buňka	k1gFnPc7	buňka
není	být	k5eNaImIp3nS	být
souvislá	souvislý	k2eAgFnSc1d1	souvislá
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
asi	asi	k9	asi
1	[number]	k4	1
<g/>
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
segmenty	segment	k1gInPc1	segment
<g/>
,	,	kIx,	,
internodia	internodium	k1gNnPc1	internodium
<g/>
,	,	kIx,	,
členěné	členěný	k2eAgInPc1d1	členěný
Ranvierovými	Ranvierův	k2eAgInPc7d1	Ranvierův
zářezy	zářez	k1gInPc7	zářez
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
zářezu	zářez	k1gInSc2	zářez
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
axonu	axonout	k5eAaImIp1nS	axonout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
elektricky	elektricky	k6eAd1	elektricky
řízených	řízený	k2eAgInPc2d1	řízený
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
i	i	k9	i
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
u	u	k7c2	u
velkých	velký	k2eAgNnPc2d1	velké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
axon	axon	k1gInSc4	axon
asi	asi	k9	asi
1	[number]	k4	1
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
páteře	páteř	k1gFnSc2	páteř
až	až	k9	až
po	po	k7c4	po
konečky	koneček	k1gInPc4	koneček
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
a	a	k8xC	a
způsobu	způsob	k1gInSc3	způsob
odstupu	odstup	k1gInSc2	odstup
výběžků	výběžek	k1gInPc2	výběžek
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
neurony	neuron	k1gInPc1	neuron
na	na	k7c4	na
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
unipolární	unipolární	k2eAgInPc1d1	unipolární
neurony	neuron	k1gInPc1	neuron
bipolární	bipolární	k2eAgInPc1d1	bipolární
neurony	neuron	k1gInPc1	neuron
pseudounipolární	pseudounipolární	k2eAgInPc1d1	pseudounipolární
neurony	neuron	k1gInPc1	neuron
multipolární	multipolární	k2eAgInPc1d1	multipolární
neurony	neuron	k1gInPc1	neuron
Unipolární	unipolární	k2eAgInPc1d1	unipolární
neurony	neuron	k1gInPc1	neuron
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
výběžek	výběžek	k1gInSc4	výběžek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
axon	axon	k1gNnSc4	axon
<g/>
.	.	kIx.	.
</s>
<s>
Dendrit	dendrit	k1gInSc1	dendrit
je	být	k5eAaImIp3nS	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
na	na	k7c4	na
specializované	specializovaný	k2eAgNnSc4d1	specializované
zakončení	zakončení	k1gNnSc4	zakončení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tyčinku	tyčinka	k1gFnSc4	tyčinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
aferenci	aference	k1gFnSc4	aference
od	od	k7c2	od
jiného	jiný	k2eAgInSc2d1	jiný
neuronu	neuron	k1gInSc2	neuron
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nenazývá	nazývat	k5eNaImIp3nS	nazývat
dendritem	dendrit	k1gInSc7	dendrit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samo	sám	k3xTgNnSc1	sám
vzruch	vzruch	k1gInSc4	vzruch
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgNnSc2	svůj
podráždění	podráždění	k1gNnSc2	podráždění
přijatou	přijatý	k2eAgFnSc7d1	přijatá
informací	informace	k1gFnSc7	informace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
světelným	světelný	k2eAgNnPc3d1	světelné
zářením	záření	k1gNnPc3	záření
o	o	k7c6	o
vhodné	vhodný	k2eAgFnSc6d1	vhodná
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Unipolární	unipolární	k2eAgInPc1d1	unipolární
jsou	být	k5eAaImIp3nP	být
smyslové	smyslový	k2eAgInPc1d1	smyslový
neurony	neuron	k1gInPc1	neuron
–	–	k?	–
primární	primární	k2eAgFnSc2d1	primární
smyslové	smyslový	k2eAgFnSc2d1	smyslová
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
tyčinky	tyčinka	k1gFnSc2	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc4	čípek
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Bipolární	bipolární	k2eAgInPc1d1	bipolární
neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
opatřené	opatřený	k2eAgFnPc1d1	opatřená
jedním	jeden	k4xCgInSc7	jeden
neuritem	neurit	k1gInSc7	neurit
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
dendritem	dendrit	k1gInSc7	dendrit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obvykle	obvykle	k6eAd1	obvykle
odstupují	odstupovat	k5eAaImIp3nP	odstupovat
na	na	k7c6	na
opačných	opačný	k2eAgInPc6d1	opačný
pólech	pól	k1gInPc6	pól
buněčného	buněčný	k2eAgNnSc2d1	buněčné
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
druhý	druhý	k4xOgInSc1	druhý
neuron	neuron	k1gInSc1	neuron
zrakové	zrakový	k2eAgFnSc2d1	zraková
dráhy	dráha	k1gFnSc2	dráha
nebo	nebo	k8xC	nebo
čichové	čichový	k2eAgFnPc1d1	čichová
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pseudounipolární	Pseudounipolární	k2eAgInSc1d1	Pseudounipolární
neuron	neuron	k1gInSc1	neuron
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
bipolárního	bipolární	k2eAgInSc2d1	bipolární
neuronu	neuron	k1gInSc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
těla	tělo	k1gNnSc2	tělo
dendrit	dendrit	k1gInSc1	dendrit
a	a	k8xC	a
axon	axon	k1gInSc1	axon
splývají	splývat	k5eAaImIp3nP	splývat
v	v	k7c4	v
jediný	jediný	k2eAgInSc4d1	jediný
výběžek	výběžek	k1gInSc4	výběžek
<g/>
,	,	kIx,	,
dendraxon	dendraxon	k1gInSc4	dendraxon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
různě	různě	k6eAd1	různě
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
průběhu	průběh	k1gInSc6	průběh
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
T	T	kA	T
opět	opět	k6eAd1	opět
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
výběžky	výběžek	k1gInPc4	výběžek
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Pseudounipolární	Pseudounipolární	k2eAgInPc1d1	Pseudounipolární
neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgMnPc4d1	typický
pro	pro	k7c4	pro
spinální	spinální	k2eAgNnPc4d1	spinální
ganglia	ganglion	k1gNnPc4	ganglion
a	a	k8xC	a
ganglia	ganglion	k1gNnPc4	ganglion
mozkových	mozkový	k2eAgInPc2d1	mozkový
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Multipolární	Multipolární	k2eAgInPc1d1	Multipolární
neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgInPc1d3	nejpočetnější
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
buněčného	buněčný	k2eAgNnSc2d1	buněčné
těla	tělo	k1gNnSc2	tělo
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
několik	několik	k4yIc1	několik
dendritů	dendrit	k1gInPc2	dendrit
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
axon	axon	k1gInSc1	axon
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
buňka	buňka	k1gFnSc1	buňka
má	mít	k5eAaImIp3nS	mít
hvězdicovitý	hvězdicovitý	k2eAgInSc4d1	hvězdicovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Multipolární	Multipolární	k2eAgInPc1d1	Multipolární
neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
typické	typický	k2eAgInPc1d1	typický
<g/>
"	"	kIx"	"
neurony	neuron	k1gInPc1	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Neurony	neuron	k1gInPc1	neuron
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
také	také	k9	také
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
axonu	axon	k1gInSc2	axon
<g/>
:	:	kIx,	:
Golgiho	Golgi	k1gMnSc2	Golgi
I.	I.	kA	I.
<g/>
typ	typ	k1gInSc1	typ
-	-	kIx~	-
neurony	neuron	k1gInPc1	neuron
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
neuritem	neurit	k1gInSc7	neurit
Golgiho	Golgi	k1gMnSc2	Golgi
II	II	kA	II
<g/>
.	.	kIx.	.
typ	typ	k1gInSc1	typ
-	-	kIx~	-
neurony	neuron	k1gInPc1	neuron
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
neuritem	neurit	k1gInSc7	neurit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozvětvuje	rozvětvovat	k5eAaImIp3nS	rozvětvovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
perikarya	perikarya	k6eAd1	perikarya
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
neurony	neuron	k1gInPc7	neuron
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
Aferentní	Aferentní	k2eAgInPc1d1	Aferentní
neurony	neuron	k1gInPc1	neuron
Eferentní	Eferentní	k2eAgInPc1d1	Eferentní
neurony	neuron	k1gInPc1	neuron
Interneurony	Interneuron	k1gMnPc4	Interneuron
Aferentní	Aferentní	k2eAgInPc1d1	Aferentní
neurony	neuron	k1gInPc1	neuron
přivádějí	přivádět	k5eAaImIp3nP	přivádět
informaci	informace	k1gFnSc4	informace
z	z	k7c2	z
receptorů	receptor	k1gInPc2	receptor
do	do	k7c2	do
CNS	CNS	kA	CNS
<g/>
.	.	kIx.	.
</s>
<s>
Eferentní	Eferentní	k2eAgInPc1d1	Eferentní
neurony	neuron	k1gInPc1	neuron
vedou	vést	k5eAaImIp3nP	vést
vzruch	vzruch	k1gInSc4	vzruch
z	z	k7c2	z
CNS	CNS	kA	CNS
do	do	k7c2	do
efektorů	efektor	k1gInPc2	efektor
<g/>
.	.	kIx.	.
</s>
<s>
Interneurony	Interneuron	k1gInPc1	Interneuron
jsou	být	k5eAaImIp3nP	být
neurony	neuron	k1gInPc1	neuron
vložené	vložený	k2eAgInPc1d1	vložený
mezi	mezi	k7c4	mezi
aferentní	aferentní	k2eAgInPc4d1	aferentní
a	a	k8xC	a
eferentní	eferentní	k2eAgInPc4d1	eferentní
neurony	neuron	k1gInPc4	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokážou	dokázat	k5eAaPmIp3nP	dokázat
rychle	rychle	k6eAd1	rychle
přenášet	přenášet	k5eAaImF	přenášet
informaci	informace	k1gFnSc4	informace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
podráždění	podráždění	k1gNnSc2	podráždění
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
strukturou	struktura	k1gFnSc7	struktura
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
podráždění	podráždění	k1gNnSc2	podráždění
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
cytoplasmatická	cytoplasmatický	k2eAgFnSc1d1	cytoplasmatická
membrána	membrána	k1gFnSc1	membrána
neuronu	neuron	k1gInSc2	neuron
<g/>
,	,	kIx,	,
neuronální	neuronální	k2eAgFnSc1d1	neuronální
membrána	membrána	k1gFnSc1	membrána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neuronální	neuronální	k2eAgFnSc6d1	neuronální
membráně	membrána	k1gFnSc6	membrána
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
dendritů	dendrit	k1gInPc2	dendrit
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nasedají	nasedat	k5eAaImIp3nP	nasedat
synapse	synapse	k1gFnPc1	synapse
<g/>
,	,	kIx,	,
převažují	převažovat	k5eAaImIp3nP	převažovat
iontové	iontový	k2eAgInPc1d1	iontový
kanály	kanál	k1gInPc1	kanál
řízené	řízený	k2eAgInPc1d1	řízený
chemicky	chemicky	k6eAd1	chemicky
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
převažují	převažovat	k5eAaImIp3nP	převažovat
kanály	kanál	k1gInPc1	kanál
řízené	řízený	k2eAgInPc1d1	řízený
elektricky	elektricky	k6eAd1	elektricky
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
buňkách	buňka	k1gFnPc6	buňka
jiná	jiný	k2eAgFnSc1d1	jiná
koncentrace	koncentrace	k1gFnSc1	koncentrace
různých	různý	k2eAgInPc2d1	různý
iontů	ion	k1gInPc2	ion
než	než	k8xS	než
v	v	k7c6	v
mezibuněčném	mezibuněčný	k2eAgInSc6d1	mezibuněčný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
při	při	k7c6	při
buněčném	buněčný	k2eAgInSc6d1	buněčný
katabolismu	katabolismus	k1gInSc6	katabolismus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
použité	použitý	k2eAgNnSc1d1	Použité
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
ATPázová	ATPázový	k2eAgFnSc1d1	ATPázový
pumpa	pumpa	k1gFnSc1	pumpa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
mezi	mezi	k7c7	mezi
ionty	ion	k1gInPc7	ion
uvnitř	uvnitř	k7c2	uvnitř
a	a	k8xC	a
vně	vně	k7c2	vně
buňky	buňka	k1gFnSc2	buňka
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
vzniká	vznikat	k5eAaImIp3nS	vznikat
membránový	membránový	k2eAgInSc1d1	membránový
potenciál	potenciál	k1gInSc1	potenciál
<g/>
,	,	kIx,	,
činící	činící	k2eAgInSc1d1	činící
asi	asi	k9	asi
-50	-50	k4	-50
až	až	k9	až
-90	-90	k4	-90
mV	mV	k?	mV
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
povrch	povrch	k1gInSc1	povrch
membrány	membrána	k1gFnSc2	membrána
nese	nést	k5eAaImIp3nS	nést
záporný	záporný	k2eAgInSc4d1	záporný
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgInSc4d1	vnější
povrch	povrch	k1gInSc4	povrch
má	mít	k5eAaImIp3nS	mít
náboj	náboj	k1gInSc1	náboj
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
potenciálu	potenciál	k1gInSc2	potenciál
je	být	k5eAaImIp3nS	být
náboj	náboj	k1gInSc4	náboj
nesený	nesený	k2eAgInSc4d1	nesený
ionty	ion	k1gInPc4	ion
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
K	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Cl-	Cl-	k1gFnPc4	Cl-
a	a	k8xC	a
anionty	anion	k1gInPc4	anion
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Ionty	ion	k1gInPc1	ion
mohou	moct	k5eAaImIp3nP	moct
volně	volně	k6eAd1	volně
přecházet	přecházet	k5eAaImF	přecházet
přes	přes	k7c4	přes
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
iontovými	iontový	k2eAgInPc7d1	iontový
kanály	kanál	k1gInPc7	kanál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
místa	místo	k1gNnPc1	místo
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
průchod	průchod	k1gInSc4	průchod
<g/>
,	,	kIx,	,
pronikají	pronikat	k5eAaImIp3nP	pronikat
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
vně	vně	k6eAd1	vně
buňky	buňka	k1gFnPc1	buňka
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Propustnost	propustnost	k1gFnSc1	propustnost
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ionty	ion	k1gInPc4	ion
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
koncentrací	koncentrace	k1gFnSc7	koncentrace
intracelulárního	intracelulární	k2eAgInSc2d1	intracelulární
Ca	ca	kA	ca
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klidu	klid	k1gInSc6	klid
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
membránový	membránový	k2eAgInSc1d1	membránový
potenciál	potenciál	k1gInSc1	potenciál
<g/>
,	,	kIx,	,
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
polarizována	polarizován	k2eAgFnSc1d1	polarizována
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
akční	akční	k2eAgInSc4d1	akční
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
převažují	převažovat	k5eAaImIp3nP	převažovat
chemicky	chemicky	k6eAd1	chemicky
řízené	řízený	k2eAgInPc1d1	řízený
iontové	iontový	k2eAgInPc1d1	iontový
kanály	kanál	k1gInPc1	kanál
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
postsynaptická	postsynaptický	k2eAgFnSc1d1	postsynaptická
membrána	membrána	k1gFnSc1	membrána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
drážditelná	drážditelný	k2eAgFnSc1d1	drážditelná
chemickými	chemický	k2eAgInPc7d1	chemický
podněty	podnět	k1gInPc7	podnět
<g/>
,	,	kIx,	,
především	především	k9	především
mediátorem	mediátor	k1gInSc7	mediátor
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
podráždění	podráždění	k1gNnSc4	podráždění
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojí	dvojí	k4xRgFnSc1	dvojí
<g/>
:	:	kIx,	:
Depolarizace	depolarizace	k1gFnSc1	depolarizace
-	-	kIx~	-
zvýšení	zvýšení	k1gNnSc1	zvýšení
permeability	permeabilita	k1gFnSc2	permeabilita
pro	pro	k7c4	pro
sodné	sodný	k2eAgInPc4d1	sodný
<g/>
,	,	kIx,	,
draselné	draselný	k2eAgInPc4d1	draselný
a	a	k8xC	a
chloridové	chloridový	k2eAgInPc4d1	chloridový
ionty	ion	k1gInPc4	ion
Hyperpolarizace	Hyperpolarizace	k1gFnSc2	Hyperpolarizace
-	-	kIx~	-
zvýšení	zvýšení	k1gNnSc3	zvýšení
permeability	permeabilita	k1gFnSc2	permeabilita
pro	pro	k7c4	pro
draselné	draselný	k2eAgInPc4d1	draselný
a	a	k8xC	a
chloridové	chloridový	k2eAgInPc4d1	chloridový
ionty	ion	k1gInPc4	ion
To	ten	k3xDgNnSc1	ten
způsobí	způsobit	k5eAaPmIp3nS	způsobit
lokální	lokální	k2eAgFnSc4d1	lokální
změnu	změna	k1gFnSc4	změna
membránového	membránový	k2eAgInSc2d1	membránový
potenciálu	potenciál	k1gInSc2	potenciál
(	(	kIx(	(
<g/>
místní	místní	k2eAgNnSc4d1	místní
podráždění	podráždění	k1gNnSc4	podráždění
<g/>
)	)	kIx)	)
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ale	ale	k8xC	ale
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
podráždění	podráždění	k1gNnSc2	podráždění
<g/>
.	.	kIx.	.
</s>
<s>
Depolarizace	depolarizace	k1gFnSc1	depolarizace
membrány	membrána	k1gFnSc2	membrána
působí	působit	k5eAaImIp3nS	působit
excitačně	excitačně	k6eAd1	excitačně
<g/>
,	,	kIx,	,
hyperpolarizace	hyperpolarizace	k1gFnSc1	hyperpolarizace
naopak	naopak	k6eAd1	naopak
inhibičně	inhibičně	k6eAd1	inhibičně
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
řízené	řízený	k2eAgInPc1d1	řízený
iontové	iontový	k2eAgInPc1d1	iontový
kanály	kanál	k1gInPc1	kanál
jsou	být	k5eAaImIp3nP	být
rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
pro	pro	k7c4	pro
vzrušivost	vzrušivost	k1gFnSc4	vzrušivost
neuronu	neuron	k1gInSc2	neuron
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nim	on	k3xPp3gInPc3	on
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
modulaci	modulace	k1gFnSc3	modulace
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
převažují	převažovat	k5eAaImIp3nP	převažovat
elektricky	elektricky	k6eAd1	elektricky
řízené	řízený	k2eAgInPc1d1	řízený
iontové	iontový	k2eAgInPc1d1	iontový
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
podráždění	podráždění	k1gNnSc4	podráždění
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
nebo	nebo	k8xC	nebo
nic	nic	k3yNnSc1	nic
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
reaguje	reagovat	k5eAaBmIp3nS	reagovat
vzruchem	vzruch	k1gInSc7	vzruch
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
možnou	možný	k2eAgFnSc7d1	možná
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
vzruchu	vzruch	k1gInSc2	vzruch
musí	muset	k5eAaImIp3nP	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
depolarizaci	depolarizace	k1gFnSc3	depolarizace
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
a	a	k8xC	a
náhlému	náhlý	k2eAgInSc3d1	náhlý
rychlému	rychlý	k2eAgInSc3d1	rychlý
poklesu	pokles	k1gInSc3	pokles
membránového	membránový	k2eAgInSc2d1	membránový
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Prahová	prahový	k2eAgFnSc1d1	prahová
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
elektricky	elektricky	k6eAd1	elektricky
řízených	řízený	k2eAgInPc2d1	řízený
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
vzruchu	vzruch	k1gInSc2	vzruch
je	být	k5eAaImIp3nS	být
-55	-55	k4	-55
mV	mV	k?	mV
<g/>
.	.	kIx.	.
</s>
<s>
Odstup	odstup	k1gInSc1	odstup
od	od	k7c2	od
šumu	šum	k1gInSc2	šum
neuronu	neuron	k1gInSc2	neuron
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
až	až	k9	až
30	[number]	k4	30
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
akčního	akční	k2eAgInSc2d1	akční
potenciálu	potenciál	k1gInSc2	potenciál
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
nervového	nervový	k2eAgNnSc2d1	nervové
vlákna	vlákno	k1gNnSc2	vlákno
–	–	k?	–
nemyelinizovaná	myelinizovaný	k2eNgFnSc1d1	nemyelinizovaná
vlákna	vlákna	k1gFnSc1	vlákna
vedou	vést	k5eAaImIp3nP	vést
vzruchy	vzruch	k1gInPc1	vzruch
rychlostí	rychlost	k1gFnPc2	rychlost
max	max	kA	max
<g/>
.	.	kIx.	.
2	[number]	k4	2
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
tlustá	tlustý	k2eAgNnPc1d1	tlusté
myelinizovaná	myelinizovaný	k2eAgNnPc1d1	myelinizovaný
vlákna	vlákno	k1gNnPc1	vlákno
dokážou	dokázat	k5eAaPmIp3nP	dokázat
vést	vést	k5eAaImF	vést
vzruchy	vzruch	k1gInPc1	vzruch
rychlostí	rychlost	k1gFnPc2	rychlost
až	až	k9	až
120	[number]	k4	120
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Vlna	vlna	k1gFnSc1	vlna
depolarizace	depolarizace	k1gFnSc2	depolarizace
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nastává	nastávat	k5eAaImIp3nS	nastávat
otevřením	otevření	k1gNnSc7	otevření
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
šířící	šířící	k2eAgFnSc2d1	šířící
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
neuronu	neuron	k1gInSc2	neuron
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
akční	akční	k2eAgInSc1d1	akční
potenciál	potenciál	k1gInSc1	potenciál
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podstatou	podstata	k1gFnSc7	podstata
přenosu	přenos	k1gInSc2	přenos
informací	informace	k1gFnPc2	informace
neurony	neuron	k1gInPc1	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Roztroušená	roztroušený	k2eAgFnSc1d1	roztroušená
skleróza	skleróza	k1gFnSc1	skleróza
je	být	k5eAaImIp3nS	být
autoimunitní	autoimunitní	k2eAgNnSc4d1	autoimunitní
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ničí	ničit	k5eAaImIp3nP	ničit
gliové	gliové	k2eAgFnPc1d1	gliové
buňky	buňka	k1gFnPc1	buňka
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
pochvu	pochva	k1gFnSc4	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
narušeno	narušen	k2eAgNnSc1d1	narušeno
vedení	vedení	k1gNnSc1	vedení
vzruchu	vzruch	k1gInSc2	vzruch
<g/>
.	.	kIx.	.
</s>
<s>
Neurony	neuron	k1gInPc1	neuron
spolu	spolu	k6eAd1	spolu
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
vysoce	vysoce	k6eAd1	vysoce
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
struktur	struktura	k1gFnPc2	struktura
zvaných	zvaný	k2eAgFnPc2d1	zvaná
synapse	synapse	k1gFnSc2	synapse
<g/>
.	.	kIx.	.
</s>
<s>
Akční	akční	k2eAgInSc4d1	akční
potenciál	potenciál	k1gInSc4	potenciál
šířící	šířící	k2eAgInSc4d1	šířící
se	se	k3xPyFc4	se
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
neuronu	neuron	k1gInSc2	neuron
způsobí	způsobit	k5eAaPmIp3nS	způsobit
uvolnění	uvolnění	k1gNnSc4	uvolnění
specifických	specifický	k2eAgFnPc2d1	specifická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
mediátorů	mediátor	k1gInPc2	mediátor
(	(	kIx(	(
<g/>
neurotransmiterů	neurotransmiter	k1gInPc2	neurotransmiter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
synaptické	synaptický	k2eAgFnSc2d1	synaptická
štěrbiny	štěrbina	k1gFnSc2	štěrbina
<g/>
,	,	kIx,	,
prostoru	prostor	k1gInSc2	prostor
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
neurony	neuron	k1gInPc7	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Mediátor	Mediátor	k1gMnSc1	Mediátor
způsobí	způsobit	k5eAaPmIp3nS	způsobit
podráždění	podráždění	k1gNnSc4	podráždění
chemicky	chemicky	k6eAd1	chemicky
řízených	řízený	k2eAgInPc2d1	řízený
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
druhého	druhý	k4xOgInSc2	druhý
neuronu	neuron	k1gInSc2	neuron
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
dalšího	další	k2eAgInSc2d1	další
akčního	akční	k2eAgInSc2d1	akční
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
