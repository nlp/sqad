<s>
Minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
minerálka	minerálka	k1gFnSc1	minerálka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
minerálního	minerální	k2eAgNnSc2d1	minerální
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
chápán	chápán	k2eAgMnSc1d1	chápán
podle	podle	k7c2	podle
historických	historický	k2eAgFnPc2d1	historická
zvyklostí	zvyklost	k1gFnPc2	zvyklost
v	v	k7c6	v
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
splýval	splývat	k5eAaImAgMnS	splývat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
pramen	pramen	k1gInSc1	pramen
<g/>
"	"	kIx"	"
a	a	k8xC	a
rámci	rámec	k1gInSc6	rámec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
užíván	užíván	k2eAgInSc1d1	užíván
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
normou	norma	k1gFnSc7	norma
ČSN	ČSN	kA	ČSN
86	[number]	k4	86
8000	[number]	k4	8000
pro	pro	k7c4	pro
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vody	voda	k1gFnPc4	voda
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
proplyněné	proplyněný	k2eAgFnSc2d1	proplyněný
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
či	či	k8xC	či
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
běžných	běžný	k2eAgFnPc2d1	běžná
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
běžné	běžný	k2eAgFnSc2d1	běžná
apod.	apod.	kA	apod.
Problém	problém	k1gInSc1	problém
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
přejímání	přejímání	k1gNnSc6	přejímání
cizího	cizí	k2eAgNnSc2d1	cizí
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
minerální	minerální	k2eAgInSc1d1	minerální
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
obtížněji	obtížně	k6eAd2	obtížně
vyslovitelné	vyslovitelný	k2eAgNnSc1d1	vyslovitelné
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
mineralizovaná	mineralizovaný	k2eAgFnSc1d1	mineralizovaná
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
terminologie	terminologie	k1gFnSc1	terminologie
sjednocena	sjednotit	k5eAaPmNgFnS	sjednotit
a	a	k8xC	a
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
vodu	voda	k1gFnSc4	voda
získanou	získaný	k2eAgFnSc4d1	získaná
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
stupeň	stupeň	k1gInSc4	stupeň
její	její	k3xOp3gFnSc2	její
"	"	kIx"	"
<g/>
mineralizace	mineralizace	k1gFnSc2	mineralizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
názvosloví	názvosloví	k1gNnSc2	názvosloví
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
nápojovém	nápojový	k2eAgInSc6d1	nápojový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nízkomineralizované	nízkomineralizovaný	k2eAgFnPc1d1	nízkomineralizovaný
vody	voda	k1gFnPc1	voda
bez	bez	k7c2	bez
specifického	specifický	k2eAgInSc2d1	specifický
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
účinku	účinek	k1gInSc2	účinek
propagují	propagovat	k5eAaImIp3nP	propagovat
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
lázeňskými	lázeňský	k2eAgInPc7d1	lázeňský
prameny	pramen	k1gInPc7	pramen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
oblastech	oblast	k1gFnPc6	oblast
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použití	použití	k1gNnSc4	použití
termínu	termín	k1gInSc2	termín
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
poměrně	poměrně	k6eAd1	poměrně
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
je	být	k5eAaImIp3nS	být
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
chápeme	chápat	k5eAaImIp1nP	chápat
dle	dle	k7c2	dle
měřítek	měřítko	k1gNnPc2	měřítko
ČSN	ČSN	kA	ČSN
86	[number]	k4	86
8000	[number]	k4	8000
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
běžnou	běžný	k2eAgFnSc7d1	běžná
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
mineralizace	mineralizace	k1gFnSc1	mineralizace
kolem	kolem	k7c2	kolem
1	[number]	k4	1
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
nepředstavuje	představovat	k5eNaImIp3nS	představovat
nic	nic	k3yNnSc1	nic
nezvyklého	zvyklý	k2eNgNnSc2d1	nezvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
normou	norma	k1gFnSc7	norma
ČSN	ČSN	kA	ČSN
jako	jako	k8xS	jako
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
chápána	chápat	k5eAaImNgFnS	chápat
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
splňovala	splňovat	k5eAaImAgFnS	splňovat
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
následujících	následující	k2eAgNnPc2d1	následující
sedmi	sedm	k4xCc2	sedm
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
mineralizace	mineralizace	k1gFnSc1	mineralizace
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
gram	gram	k1gInSc4	gram
/	/	kIx~	/
litr	litr	k1gInSc1	litr
obsah	obsah	k1gInSc1	obsah
CO2	CO2	k1gFnSc2	CO2
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
gram	gram	k1gInSc4	gram
/	/	kIx~	/
litr	litr	k1gInSc1	litr
obsah	obsah	k1gInSc1	obsah
H2S	H2S	k1gFnSc2	H2S
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
gram	gram	k1gInSc4	gram
/	/	kIx~	/
litr	litr	k1gInSc1	litr
obsah	obsah	k1gInSc1	obsah
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
+	+	kIx~	+
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
miligramů	miligram	k1gInPc2	miligram
na	na	k7c4	na
litr	litr	k1gInSc4	litr
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
významného	významný	k2eAgInSc2d1	významný
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
3	[number]	k4	3
aj	aj	kA	aj
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
teplota	teplota	k1gFnSc1	teplota
vývěru	vývěr	k1gInSc2	vývěr
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
1500	[number]	k4	1500
bq	bq	k?	bq
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
l	l	kA	l
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
radonem	radon	k1gInSc7	radon
(	(	kIx(	(
<g/>
222	[number]	k4	222
<g/>
Rn	Rn	k1gFnPc2	Rn
<g/>
)	)	kIx)	)
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sjednocením	sjednocení	k1gNnSc7	sjednocení
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
legislativy	legislativa	k1gFnSc2	legislativa
a	a	k8xC	a
původní	původní	k2eAgFnSc4d1	původní
normu	norma	k1gFnSc4	norma
ČSN	ČSN	kA	ČSN
nahradil	nahradit	k5eAaPmAgInS	nahradit
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
vyhláška	vyhláška	k1gFnSc1	vyhláška
o	o	k7c6	o
zdrojích	zdroj	k1gInPc6	zdroj
a	a	k8xC	a
lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
přílohy	příloha	k1gFnSc2	příloha
č.	č.	k?	č.
1	[number]	k4	1
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
peloidů	peloid	k1gInPc2	peloid
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
původní	původní	k2eAgFnSc2d1	původní
čistoty	čistota	k1gFnSc2	čistota
<g/>
,	,	kIx,	,
stálého	stálý	k2eAgNnSc2d1	stálé
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výživy	výživa	k1gFnSc2	výživa
fyziologické	fyziologický	k2eAgInPc1d1	fyziologický
účinky	účinek	k1gInPc1	účinek
dané	daný	k2eAgInPc1d1	daný
obsahem	obsah	k1gInSc7	obsah
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
stopových	stopový	k2eAgInPc2d1	stopový
prvků	prvek	k1gInPc2	prvek
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
jako	jako	k8xC	jako
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
balených	balený	k2eAgFnPc2d1	balená
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
každá	každý	k3xTgFnSc1	každý
voda	voda	k1gFnSc1	voda
vytěžená	vytěžený	k2eAgFnSc1d1	vytěžená
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
mineralizace	mineralizace	k1gFnSc2	mineralizace
<g/>
.	.	kIx.	.
</s>
<s>
Vodám	voda	k1gFnPc3	voda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
původní	původní	k2eAgFnSc2d1	původní
klasifikace	klasifikace	k1gFnSc2	klasifikace
značené	značený	k2eAgFnPc1d1	značená
jako	jako	k8xS	jako
minerální	minerální	k2eAgFnPc1d1	minerální
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
významovém	významový	k2eAgInSc6d1	významový
rozsahu	rozsah	k1gInSc6	rozsah
přiřazen	přiřazen	k2eAgInSc4d1	přiřazen
termín	termín	k1gInSc4	termín
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
léčebné	léčebný	k2eAgNnSc4d1	léčebné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
následujících	následující	k2eAgNnPc2d1	následující
pěti	pět	k4xCc2	pět
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
mineralizace	mineralizace	k1gFnSc1	mineralizace
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
gram	gram	k1gInSc4	gram
/	/	kIx~	/
litr	litr	k1gInSc1	litr
obsah	obsah	k1gInSc1	obsah
CO2	CO2	k1gFnSc2	CO2
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
gram	gram	k1gInSc4	gram
/	/	kIx~	/
litr	litr	k1gInSc1	litr
obsah	obsah	k1gInSc1	obsah
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
významného	významný	k2eAgInSc2d1	významný
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
teplota	teplota	k1gFnSc1	teplota
vývěru	vývěr	k1gInSc2	vývěr
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
přes	přes	k7c4	přes
1500	[number]	k4	1500
bq	bq	k?	bq
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
legislativy	legislativa	k1gFnSc2	legislativa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
jsou	být	k5eAaImIp3nP	být
minerální	minerální	k2eAgFnPc1d1	minerální
vody	voda	k1gFnPc1	voda
dělené	dělený	k2eAgFnPc4d1	dělená
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
celkové	celkový	k2eAgFnSc2d1	celková
mineralizace	mineralizace	k1gFnSc2	mineralizace
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
mineralizované	mineralizovaný	k2eAgInPc1d1	mineralizovaný
-	-	kIx~	-
do	do	k7c2	do
50	[number]	k4	50
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
slabě	slabě	k6eAd1	slabě
mineralizované	mineralizovaný	k2eAgInPc4d1	mineralizovaný
-	-	kIx~	-
50	[number]	k4	50
-	-	kIx~	-
500	[number]	k4	500
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
146	[number]	k4	146
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
středně	středně	k6eAd1	středně
mineralizované	mineralizovaný	k2eAgNnSc1d1	mineralizované
-	-	kIx~	-
500	[number]	k4	500
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1500	[number]	k4	1500
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ondrášovka	Ondrášovka	k1gFnSc1	Ondrášovka
-	-	kIx~	-
630	[number]	k4	630
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
silně	silně	k6eAd1	silně
mineralizované	mineralizovaný	k2eAgNnSc1d1	mineralizované
-	-	kIx~	-
1500	[number]	k4	1500
-	-	kIx~	-
5000	[number]	k4	5000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Poděbradka	poděbradka	k1gFnSc1	poděbradka
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
mineralizované	mineralizovaný	k2eAgNnSc1d1	mineralizované
-	-	kIx~	-
přes	přes	k7c4	přes
5000	[number]	k4	5000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
Šaratica	Šaratica	k1gFnSc1	Šaratica
-	-	kIx~	-
12785	[number]	k4	12785
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
významných	významný	k2eAgFnPc2d1	významná
složek	složka	k1gFnPc2	složka
uhličité	uhličitý	k2eAgFnPc1d1	uhličitá
-	-	kIx~	-
obsah	obsah	k1gInSc1	obsah
CO2	CO2	k1gFnSc2	CO2
minimálně	minimálně	k6eAd1	minimálně
1000	[number]	k4	1000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
sirné	sirný	k2eAgInPc1d1	sirný
-	-	kIx~	-
obsah	obsah	k1gInSc1	obsah
titrovatelné	titrovatelný	k2eAgFnSc2d1	titrovatelný
síry	síra	k1gFnSc2	síra
nad	nad	k7c7	nad
2	[number]	k4	2
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
jodidové	jodidový	k2eAgInPc1d1	jodidový
-	-	kIx~	-
obsah	obsah	k1gInSc1	obsah
jodidů	jodid	k1gInPc2	jodid
nad	nad	k7c4	nad
5	[number]	k4	5
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
l	l	kA	l
ostatní	ostatní	k2eAgInPc1d1	ostatní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
kyseliny	kyselina	k1gFnSc2	kyselina
křemičité	křemičitý	k2eAgFnSc2d1	křemičitá
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
70	[number]	k4	70
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
fluoritů	fluorit	k1gInPc2	fluorit
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
2	[number]	k4	2
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
pH	ph	kA	ph
Pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c4	o
vody	voda	k1gFnPc4	voda
<g/>
:	:	kIx,	:
silně	silně	k6eAd1	silně
kyselé	kyselé	k1gNnSc1	kyselé
-	-	kIx~	-
pH	ph	kA	ph
pod	pod	k7c7	pod
3,5	[number]	k4	3,5
silně	silně	k6eAd1	silně
alkalické	alkalický	k2eAgInPc1d1	alkalický
-	-	kIx~	-
pH	ph	kA	ph
nad	nad	k7c7	nad
8,5	[number]	k4	8,5
Podle	podle	k7c2	podle
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
radonové	radonový	k2eAgNnSc1d1	radonové
-	-	kIx~	-
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
nad	nad	k7c7	nad
1500	[number]	k4	1500
bq	bq	k?	bq
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
způsobená	způsobený	k2eAgFnSc1d1	způsobená
radonem	radon	k1gInSc7	radon
222	[number]	k4	222
<g/>
Rn	Rn	k1gMnPc2	Rn
Podle	podle	k7c2	podle
přirozené	přirozený	k2eAgFnSc2d1	přirozená
teploty	teplota	k1gFnSc2	teplota
u	u	k7c2	u
vývěru	vývěr	k1gInSc2	vývěr
studené	studený	k2eAgFnSc2d1	studená
-	-	kIx~	-
do	do	k7c2	do
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
termální	termální	k2eAgInPc4d1	termální
vlažné	vlažný	k2eAgInPc4d1	vlažný
-	-	kIx~	-
do	do	k7c2	do
35	[number]	k4	35
<g />
.	.	kIx.	.
</s>
<s>
°	°	k?	°
<g/>
C	C	kA	C
teplé	teplý	k2eAgInPc1d1	teplý
-	-	kIx~	-
do	do	k7c2	do
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
horké	horký	k2eAgInPc1d1	horký
-	-	kIx~	-
nad	nad	k7c4	nad
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
Podle	podle	k7c2	podle
osmotického	osmotický	k2eAgInSc2d1	osmotický
tlaku	tlak	k1gInSc2	tlak
hypotonické	hypotonický	k2eAgNnSc1d1	hypotonické
-	-	kIx~	-
<	<	kIx(	<
<g/>
710	[number]	k4	710
kPa	kPa	k?	kPa
(	(	kIx(	(
<g/>
280	[number]	k4	280
mOsm	mOsma	k1gFnPc2	mOsma
<g/>
)	)	kIx)	)
isotonické	isotonický	k2eAgFnSc2d1	isotonická
-	-	kIx~	-
710	[number]	k4	710
-	-	kIx~	-
760	[number]	k4	760
kPa	kPa	k?	kPa
(	(	kIx(	(
<g/>
280	[number]	k4	280
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
mOsm	mOsma	k1gFnPc2	mOsma
<g/>
)	)	kIx)	)
hypertonické	hypertonický	k2eAgFnPc1d1	hypertonická
-	-	kIx~	-
>	>	kIx)	>
<g/>
760	[number]	k4	760
kPa	kPa	k?	kPa
(	(	kIx(	(
<g/>
300	[number]	k4	300
mOsm	mOsma	k1gFnPc2	mOsma
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
hlavních	hlavní	k2eAgFnPc2d1	hlavní
složek	složka	k1gFnPc2	složka
Podle	podle	k7c2	podle
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
součinů	součin	k1gInPc2	součin
látkové	látkový	k2eAgFnSc2d1	látková
koncentrace	koncentrace	k1gFnSc2	koncentrace
a	a	k8xC	a
nábojového	nábojový	k2eAgNnSc2d1	nábojové
čísla	číslo	k1gNnSc2	číslo
všech	všecek	k3xTgInPc2	všecek
aniontů	anion	k1gInPc2	anion
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
kationtů	kation	k1gInPc2	kation
<g/>
)	)	kIx)	)
zastoupeny	zastoupen	k2eAgInPc4d1	zastoupen
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
se	se	k3xPyFc4	se
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
nejvíce	hodně	k6eAd3	hodně
zastoupených	zastoupený	k2eAgFnPc2d1	zastoupená
složek	složka	k1gFnPc2	složka
(	(	kIx(	(
<g/>
nejprve	nejprve	k6eAd1	nejprve
pro	pro	k7c4	pro
anionty	anion	k1gInPc4	anion
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pro	pro	k7c4	pro
kationty	kation	k1gInPc4	kation
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
využitelnosti	využitelnost	k1gFnSc2	využitelnost
jako	jako	k8xS	jako
léčivé	léčivý	k2eAgInPc1d1	léčivý
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odborného	odborný	k2eAgInSc2d1	odborný
posudku	posudek	k1gInSc2	posudek
použitelné	použitelný	k2eAgFnSc2d1	použitelná
k	k	k7c3	k
léčebným	léčebný	k2eAgInPc3d1	léčebný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastností	vlastnost	k1gFnPc2	vlastnost
jako	jako	k8xS	jako
stabilní	stabilní	k2eAgFnSc1d1	stabilní
Pokud	pokud	k8xS	pokud
splňuje	splňovat	k5eAaImIp3nS	splňovat
následující	následující	k2eAgFnPc4d1	následující
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
mineralizace	mineralizace	k1gFnSc1	mineralizace
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
CO2	CO2	k1gFnSc2	CO2
kolísá	kolísat	k5eAaImIp3nS	kolísat
jen	jen	k9	jen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přirozených	přirozený	k2eAgInPc2d1	přirozený
výkyvů	výkyv	k1gInPc2	výkyv
max	max	kA	max
<g/>
.	.	kIx.	.
±	±	k?	±
<g/>
20	[number]	k4	20
%	%	kIx~	%
typ	typ	k1gInSc1	typ
vody	voda	k1gFnSc2	voda
stanovený	stanovený	k2eAgInSc4d1	stanovený
podle	podle	k7c2	podle
hlavních	hlavní	k2eAgFnPc2d1	hlavní
složek	složka	k1gFnPc2	složka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kapitola	kapitola	k1gFnSc1	kapitola
výše	výše	k1gFnSc1	výše
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
obsah	obsah	k1gInSc1	obsah
léčivých	léčivý	k2eAgFnPc2d1	léčivá
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
I	I	kA	I
<g/>
,	,	kIx,	,
F	F	kA	F
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
či	či	k8xC	či
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
nekolísá	kolísat	k5eNaImIp3nS	kolísat
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
±	±	k?	±
<g/>
30	[number]	k4	30
%	%	kIx~	%
-	-	kIx~	-
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gInSc2	jejich
obsahu	obsah	k1gInSc2	obsah
voda	voda	k1gFnSc1	voda
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xS	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
minimální	minimální	k2eAgFnPc4d1	minimální
hodnoty	hodnota	k1gFnPc4	hodnota
neklesají	klesat	k5eNaImIp3nP	klesat
pod	pod	k7c4	pod
stanovená	stanovený	k2eAgNnPc4d1	stanovené
kritéria	kritérion	k1gNnPc4	kritérion
I	i	k9	i
přes	přes	k7c4	přes
novou	nový	k2eAgFnSc4d1	nová
legislativu	legislativa	k1gFnSc4	legislativa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
v	v	k7c6	v
hydrogeologické	hydrogeologický	k2eAgFnSc6d1	hydrogeologická
praxi	praxe	k1gFnSc6	praxe
stále	stále	k6eAd1	stále
užívá	užívat	k5eAaImIp3nS	užívat
termín	termín	k1gInSc1	termín
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
86	[number]	k4	86
8000	[number]	k4	8000
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
minerální	minerální	k2eAgFnSc6d1	minerální
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
důležité	důležitý	k2eAgNnSc1d1	důležité
specifikovat	specifikovat	k5eAaBmF	specifikovat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
význam	význam	k1gInSc4	význam
máme	mít	k5eAaImIp1nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
<g/>
.	.	kIx.	.
prostá	prostý	k2eAgFnSc1d1	prostá
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nesplňovala	splňovat	k5eNaImAgFnS	splňovat
limity	limita	k1gFnPc4	limita
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
pití	pití	k1gNnSc3	pití
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
klasifikovaná	klasifikovaný	k2eAgFnSc1d1	klasifikovaná
jako	jako	k8xS	jako
minerální	minerální	k2eAgFnSc1d1	minerální
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pochází	pocházet	k5eAaImIp3nS	pocházet
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
mineralizovaná	mineralizovaný	k2eAgFnSc1d1	mineralizovaná
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
přírodního	přírodní	k2eAgInSc2d1	přírodní
původu	původ	k1gInSc2	původ
důlní	důlní	k2eAgFnSc1d1	důlní
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
případ	případ	k1gInSc1	případ
mineralizované	mineralizovaný	k2eAgFnSc2d1	mineralizovaná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
ji	on	k3xPp3gFnSc4	on
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
44	[number]	k4	44
/	/	kIx~	/
1988	[number]	k4	1988
kyselka	kyselka	k1gFnSc1	kyselka
-	-	kIx~	-
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
CO2	CO2	k1gFnSc2	CO2
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
gram	gram	k1gInSc4	gram
/	/	kIx~	/
litr	litr	k1gInSc4	litr
lázeňský	lázeňský	k2eAgInSc4d1	lázeňský
pramen	pramen	k1gInSc4	pramen
-	-	kIx~	-
pramen	pramen	k1gInSc4	pramen
se	s	k7c7	s
specifickým	specifický	k2eAgNnSc7d1	specifické
složením	složení	k1gNnSc7	složení
<g />
.	.	kIx.	.
</s>
<s>
užívaný	užívaný	k2eAgMnSc1d1	užívaný
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
lázeňství	lázeňství	k1gNnSc2	lázeňství
k	k	k7c3	k
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
užití	užití	k1gNnSc3	užití
pitím	pití	k1gNnSc7	pití
či	či	k8xC	či
ke	k	k7c3	k
koupelím	koupel	k1gFnPc3	koupel
či	či	k8xC	či
inhalacím	inhalace	k1gFnPc3	inhalace
<g/>
.	.	kIx.	.
zřídlo	zřídlo	k1gNnSc4	zřídlo
-	-	kIx~	-
obecně	obecně	k6eAd1	obecně
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
získávány	získáván	k2eAgInPc1d1	získáván
přírodní	přírodní	k2eAgInPc1d1	přírodní
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
minerální	minerální	k2eAgFnSc3d1	minerální
vodě	voda	k1gFnSc3	voda
pramen	pramen	k1gInSc1	pramen
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgMnPc1d1	přírodní
<g/>
)	)	kIx)	)
či	či	k8xC	či
vrt	vrt	k1gInSc4	vrt
(	(	kIx(	(
<g/>
umělý	umělý	k2eAgInSc4d1	umělý
<g/>
)	)	kIx)	)
Léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
lázeňství	lázeňství	k1gNnSc6	lázeňství
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pití	pití	k1gNnSc6	pití
lázeňských	lázeňský	k2eAgInPc2d1	lázeňský
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dodržovat	dodržovat	k5eAaImF	dodržovat
lékařská	lékařský	k2eAgNnPc4d1	lékařské
doporučení	doporučení	k1gNnPc4	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
výhradním	výhradní	k2eAgMnSc7d1	výhradní
pití	pití	k1gNnSc2	pití
projevit	projevit	k5eAaPmF	projevit
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
příjem	příjem	k1gInSc1	příjem
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vod	voda	k1gFnPc2	voda
které	který	k3yRgNnSc1	který
jsou	být	k5eAaImIp3nP	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
zdrojem	zdroj	k1gInSc7	zdroj
fluoridů	fluorid	k1gInPc2	fluorid
to	ten	k3xDgNnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
4	[number]	k4	4
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
fluoridy	fluorid	k1gInPc1	fluorid
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vysoce	vysoce	k6eAd1	vysoce
mineralizovaných	mineralizovaný	k2eAgFnPc2d1	mineralizovaná
hořkých	hořký	k2eAgFnPc2d1	hořká
vod	voda	k1gFnPc2	voda
sírano-sodno-hořečnatého	síranoodnoořečnatý	k2eAgInSc2d1	sírano-sodno-hořečnatý
typu	typ	k1gInSc2	typ
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
sodík	sodík	k1gInSc1	sodík
nepříznivě	příznivě	k6eNd1	příznivě
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Bílinská	bílinský	k2eAgFnSc1d1	Bílinská
kyselka	kyselka	k1gFnSc1	kyselka
-	-	kIx~	-
Bílina	Bílina	k1gFnSc1	Bílina
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Teplice	teplice	k1gFnSc2	teplice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
http://www.bilinska.cz/	[url]	k?	http://www.bilinska.cz/
<g/>
)	)	kIx)	)
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
Byňov	Byňov	k1gInSc1	Byňov
u	u	k7c2	u
Nových	Nových	k2eAgInPc2d1	Nových
Hradů	hrad	k1gInPc2	hrad
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
kyselka	kyselka	k1gFnSc1	kyselka
-	-	kIx~	-
Horní	horní	k2eAgFnSc1d1	horní
Moštěnice	Moštěnice	k1gFnSc1	Moštěnice
(	(	kIx(	(
<g/>
http://www.hanacka.cz/	[url]	k?	http://www.hanacka.cz/
<g/>
)	)	kIx)	)
Mattoni	Matton	k1gMnPc1	Matton
-	-	kIx~	-
Kyselka	kyselka	k1gFnSc1	kyselka
(	(	kIx(	(
<g/>
http://www.mattoni.cz/	[url]	k?	http://www.mattoni.cz/
<g/>
)	)	kIx)	)
Korunní	korunní	k2eAgFnPc1d1	korunní
–	–	k?	–
Korunní	korunní	k2eAgFnPc1d1	korunní
(	(	kIx(	(
<g/>
http://www.korunni.cz/	[url]	k?	http://www.korunni.cz/
<g/>
)	)	kIx)	)
Magnesia	magnesium	k1gNnSc2	magnesium
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
Il-Sano	Il-Sana	k1gFnSc5	Il-Sana
-	-	kIx~	-
Chodová	Chodový	k2eAgFnSc1d1	Chodová
Planá	planý	k2eAgFnSc1d1	planá
Ondrášovka	Ondrášovka	k1gFnSc1	Ondrášovka
-	-	kIx~	-
Moravský	moravský	k2eAgInSc1d1	moravský
Beroun	Beroun	k1gInSc1	Beroun
(	(	kIx(	(
<g/>
http://www.ondrasovka.cz/	[url]	k?	http://www.ondrasovka.cz/
<g/>
)	)	kIx)	)
Poděbradka	poděbradka	k1gFnSc1	poděbradka
-	-	kIx~	-
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
(	(	kIx(	(
<g/>
http://www.podebradka.cz/	[url]	k?	http://www.podebradka.cz/
<g/>
)	)	kIx)	)
Šaratica	Šaratica	k1gMnSc1	Šaratica
-	-	kIx~	-
Újezd	Újezd	k1gInSc1	Újezd
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
http://www.saratica.cz/	[url]	k?	http://www.saratica.cz/
<g/>
)	)	kIx)	)
Vincentka	Vincentka	k1gFnSc1	Vincentka
-	-	kIx~	-
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
(	(	kIx(	(
<g/>
http://www.vincentka.cz/	[url]	k?	http://www.vincentka.cz/
<g/>
)	)	kIx)	)
Ida	Ida	k1gFnSc1	Ida
-	-	kIx~	-
Běloves	Běloves	k1gInSc1	Běloves
Zaječická	zaječický	k2eAgFnSc1d1	Zaječická
hořká	hořká	k1gFnSc1	hořká
-	-	kIx~	-
Bílina	Bílina	k1gFnSc1	Bílina
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
okres	okres	k1gInSc1	okres
Teplice	teplice	k1gFnSc2	teplice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
http://www.zajecicka.cz/	[url]	k?	http://www.zajecicka.cz/
<g/>
)	)	kIx)	)
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
Janské	janský	k2eAgFnPc1d1	Janská
Lázně	lázeň	k1gFnPc1	lázeň
Konstantinovy	Konstantinův	k2eAgFnPc1d1	Konstantinova
Lázně	lázeň	k1gFnPc1	lázeň
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
Teplice	teplice	k1gFnSc1	teplice
Bílina	Bílina	k1gFnSc1	Bílina
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Vary	Vary	k1gInPc1	Vary
seznam	seznam	k1gInSc4	seznam
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
seznam	seznam	k1gInSc4	seznam
českých	český	k2eAgFnPc2d1	Česká
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
termální	termální	k2eAgInSc4d1	termální
pramen	pramen	k1gInSc4	pramen
lázeňství	lázeňství	k1gNnSc2	lázeňství
pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Prvý	prvý	k4xOgInSc1	prvý
česko	česko	k6eAd1	česko
slovenský	slovenský	k2eAgInSc1d1	slovenský
server	server	k1gInSc1	server
pre	pre	k?	pre
minerálne	minerálnout	k5eAaPmIp3nS	minerálnout
vody	voda	k1gFnPc4	voda
sveta	sveto	k1gNnSc2	sveto
Minerální	minerální	k2eAgFnSc2d1	minerální
a	a	k8xC	a
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
Balené	balený	k2eAgFnSc2d1	balená
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
tuzemském	tuzemský	k2eAgInSc6d1	tuzemský
trhu	trh	k1gInSc6	trh
</s>
