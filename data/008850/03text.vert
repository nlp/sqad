<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
acidum	acidum	k1gInSc1	acidum
aceticum	aceticum	k1gInSc1	aceticum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ethanová	ethanový	k2eAgFnSc1d1	ethanový
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
jednosytná	jednosytný	k2eAgFnSc1d1	jednosytná
organická	organický	k2eAgFnSc1d1	organická
(	(	kIx(	(
<g/>
karboxylová	karboxylový	k2eAgFnSc1d1	karboxylová
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
ostrého	ostrý	k2eAgInSc2d1	ostrý
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
mísitelná	mísitelný	k2eAgFnSc1d1	mísitelná
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
s	s	k7c7	s
ethanolem	ethanol	k1gInSc7	ethanol
i	i	k8xC	i
dimethyletherem	dimethylether	k1gInSc7	dimethylether
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
bezvodá	bezvodý	k2eAgFnSc1d1	bezvodá
kyselina	kyselina	k1gFnSc1	kyselina
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
za	za	k7c2	za
nižších	nízký	k2eAgFnPc2d2	nižší
teplot	teplota	k1gFnPc2	teplota
na	na	k7c4	na
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
až	až	k8xS	až
bílou	bílý	k2eAgFnSc4d1	bílá
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
dán	dát	k5eAaPmNgInS	dát
název	název	k1gInSc1	název
ledová	ledový	k2eAgFnSc1d1	ledová
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
o	o	k7c6	o
koncentraci	koncentrace	k1gFnSc6	koncentrace
od	od	k7c2	od
8	[number]	k4	8
%	%	kIx~	%
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ocet	ocet	k1gInSc1	ocet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
přirozených	přirozený	k2eAgInPc2d1	přirozený
metabolitů	metabolit	k1gInPc2	metabolit
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc7d1	aktivní
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc4	její
komplex	komplex	k1gInSc4	komplex
s	s	k7c7	s
koenzymem	koenzym	k1gInSc7	koenzym
A	A	kA	A
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
acetyl-CoA	acetyl-CoA	k?	acetyl-CoA
(	(	kIx(	(
<g/>
acetylkoenzym	acetylkoenzym	k1gInSc1	acetylkoenzym
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
meziproduktů	meziprodukt	k1gInPc2	meziprodukt
buněčného	buněčný	k2eAgInSc2d1	buněčný
metabolismu	metabolismus	k1gInSc2	metabolismus
sacharidů	sacharid	k1gInPc2	sacharid
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
jako	jako	k8xC	jako
volná	volný	k2eAgFnSc1d1	volná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
solí	sůl	k1gFnPc2	sůl
(	(	kIx(	(
<g/>
octanů	octan	k1gInPc2	octan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
kvasícím	kvasící	k2eAgNnSc6d1	kvasící
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
následný	následný	k2eAgInSc1d1	následný
fermentační	fermentační	k2eAgInSc1d1	fermentační
produkt	produkt	k1gInSc1	produkt
přeměny	přeměna	k1gFnSc2	přeměna
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
složkou	složka	k1gFnSc7	složka
poševního	poševní	k2eAgInSc2d1	poševní
mazu	maz	k1gInSc2	maz
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
primátů	primát	k1gMnPc2	primát
a	a	k8xC	a
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
roli	role	k1gFnSc4	role
slabého	slabý	k2eAgNnSc2d1	slabé
antibakteriálního	antibakteriální	k2eAgNnSc2d1	antibakteriální
činidla	činidlo	k1gNnSc2	činidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Biologická	biologický	k2eAgFnSc1d1	biologická
výroba	výroba	k1gFnSc1	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
Zředěný	zředěný	k2eAgInSc1d1	zředěný
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
biologickou	biologický	k2eAgFnSc7d1	biologická
oxidací	oxidace	k1gFnSc7	oxidace
ethanolu	ethanol	k1gInSc2	ethanol
podle	podle	k7c2	podle
celkové	celkový	k2eAgFnSc2d1	celková
rovnice	rovnice	k1gFnSc2	rovnice
</s>
</p>
<p>
<s>
CH3CH2OH	CH3CH2OH	k4	CH3CH2OH
+	+	kIx~	+
O2	O2	k1gMnSc1	O2
→	→	k?	→
CH3COOH	CH3COOH	k1gMnSc1	CH3COOH
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
biotechnologické	biotechnologický	k2eAgFnSc6d1	biotechnologická
výrobě	výroba	k1gFnSc6	výroba
jsou	být	k5eAaImIp3nP	být
příslušné	příslušný	k2eAgInPc1d1	příslušný
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
(	(	kIx(	(
<g/>
octové	octový	k2eAgFnPc1d1	octová
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
především	především	k9	především
rodu	rod	k1gInSc2	rod
Acetobacter	Acetobactra	k1gFnPc2	Acetobactra
<g/>
)	)	kIx)	)
imobilizovány	imobilizován	k2eAgInPc1d1	imobilizován
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
přilnavostí	přilnavost	k1gFnSc7	přilnavost
na	na	k7c6	na
bukových	bukový	k2eAgFnPc6d1	Buková
hoblinách	hoblina	k1gFnPc6	hoblina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
skrápěny	skrápět	k5eAaImNgFnP	skrápět
roztokem	roztok	k1gInSc7	roztok
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
odspodu	odspodu	k6eAd1	odspodu
probublávány	probubláván	k2eAgInPc1d1	probubláván
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vyšších	vysoký	k2eAgFnPc2d2	vyšší
koncentrací	koncentrace	k1gFnPc2	koncentrace
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
následnou	následný	k2eAgFnSc7d1	následná
destilací	destilace	k1gFnSc7	destilace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemická	chemický	k2eAgFnSc1d1	chemická
syntéza	syntéza	k1gFnSc1	syntéza
===	===	k?	===
</s>
</p>
<p>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
oxidaci	oxidace	k1gFnSc4	oxidace
lze	lze	k6eAd1	lze
sice	sice	k8xC	sice
provádět	provádět	k5eAaImF	provádět
i	i	k9	i
čistě	čistě	k6eAd1	čistě
chemickou	chemický	k2eAgFnSc7d1	chemická
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
hydrogenaci	hydrogenace	k1gFnSc6	hydrogenace
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
</s>
</p>
<p>
<s>
2	[number]	k4	2
CO	co	k6eAd1	co
+	+	kIx~	+
2	[number]	k4	2
H2	H2	k1gFnSc2	H2
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COOH	COOH	kA	COOH
<g/>
.	.	kIx.	.
<g/>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
připravena	připravit	k5eAaPmNgFnS	připravit
též	též	k6eAd1	též
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
s	s	k7c7	s
methanolem	methanol	k1gInSc7	methanol
</s>
</p>
<p>
<s>
CH3OH	CH3OH	k4	CH3OH
+	+	kIx~	+
CO	co	k8xS	co
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COOH	COOH	kA	COOH
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
procesem	proces	k1gInSc7	proces
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
třístupňově	třístupňově	k6eAd1	třístupňově
za	za	k7c4	za
přidání	přidání	k1gNnSc4	přidání
jodovodíku	jodovodík	k1gInSc2	jodovodík
a	a	k8xC	a
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
palladia	palladion	k1gNnSc2	palladion
nebo	nebo	k8xC	nebo
iridia	iridium	k1gNnSc2	iridium
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
jodmethan	jodmethan	k1gInSc1	jodmethan
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CH3OH	CH3OH	k4	CH3OH
+	+	kIx~	+
HI	hi	k0	hi
→	→	k?	→
CH3I	CH3I	k1gMnPc7	CH3I
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
I	I	kA	I
+	+	kIx~	+
CO	co	k8xS	co
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COI	COI	kA	COI
<g/>
,	,	kIx,	,
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COI	COI	kA	COI
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
CH3COOH	CH3COOH	k1gMnSc1	CH3COOH
+	+	kIx~	+
HI	hi	k0	hi
<g/>
.	.	kIx.	.
<g/>
Dříve	dříve	k6eAd2	dříve
obvyklejší	obvyklý	k2eAgFnSc7d2	obvyklejší
metodou	metoda	k1gFnSc7	metoda
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
byla	být	k5eAaImAgFnS	být
hydratace	hydratace	k1gFnSc1	hydratace
ethynu	ethyn	k1gInSc2	ethyn
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
nejprve	nejprve	k6eAd1	nejprve
acetaldehyd	acetaldehyd	k1gInSc1	acetaldehyd
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
≡	≡	k?	≡
<g/>
CH	Ch	kA	Ch
+	+	kIx~	+
H2O	H2O	k1gMnSc3	H2O
→	→	k?	→
CH	Ch	kA	Ch
3	[number]	k4	3
<g/>
CHO	cho	k0	cho
<g/>
,	,	kIx,	,
<g/>
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
oxidován	oxidovat	k5eAaBmNgInS	oxidovat
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
</s>
</p>
<p>
<s>
2	[number]	k4	2
CH	Ch	kA	Ch
3CHO	[number]	k4	3CHO
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2	[number]	k4	2
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COOH	COOH	kA	COOH
<g/>
.	.	kIx.	.
<g/>
Jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgNnSc1d1	octové
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
nenasycených	nasycený	k2eNgInPc2d1	nenasycený
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
butenů	buten	k1gInPc2	buten
(	(	kIx(	(
<g/>
butenu	buten	k1gInSc2	buten
a	a	k8xC	a
2	[number]	k4	2
<g/>
-butenu	uten	k1gInSc2	-buten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
složkou	složka	k1gFnSc7	složka
krakovacích	krakovací	k2eAgInPc2d1	krakovací
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
oba	dva	k4xCgInPc1	dva
hydratací	hydratace	k1gFnPc2	hydratace
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
2	[number]	k4	2
<g/>
-butanol	utanola	k1gFnPc2	-butanola
</s>
</p>
<p>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oxidací	oxidace	k1gFnSc7	oxidace
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
molekuly	molekula	k1gFnPc4	molekula
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
</s>
</p>
<p>
<s>
Kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
lze	lze	k6eAd1	lze
také	také	k6eAd1	také
připravit	připravit	k5eAaPmF	připravit
přímou	přímý	k2eAgFnSc7d1	přímá
oxidací	oxidace	k1gFnSc7	oxidace
směsí	směs	k1gFnPc2	směs
nižších	nízký	k2eAgInPc2d2	nižší
alkanů	alkan	k1gInPc2	alkan
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
butanu	butan	k1gInSc2	butan
</s>
</p>
<p>
<s>
2	[number]	k4	2
CH3CH2CH2CH3	CH3CH2CH2CH3	k1gFnSc1	CH3CH2CH2CH3
+	+	kIx~	+
5	[number]	k4	5
O2	O2	k1gFnSc2	O2
→	→	k?	→
4	[number]	k4	4
CH3COOH	CH3COOH	k1gFnPc2	CH3COOH
+	+	kIx~	+
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
přítomny	přítomen	k2eAgInPc1d1	přítomen
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
alkany	alkan	k1gInPc1	alkan
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
směs	směs	k1gFnSc4	směs
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
následně	následně	k6eAd1	následně
dělit	dělit	k5eAaImF	dělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
její	její	k3xOp3gFnSc7	její
vápenná	vápenný	k2eAgFnSc1d1	vápenná
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
octan	octan	k1gInSc1	octan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
odpadním	odpadní	k2eAgInSc7d1	odpadní
produktem	produkt	k1gInSc7	produkt
výroby	výroba	k1gFnSc2	výroba
methanolu	methanol	k1gInSc2	methanol
suchou	suchý	k2eAgFnSc7d1	suchá
destilací	destilace	k1gFnSc7	destilace
bukového	bukový	k2eAgNnSc2d1	bukové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
odděleného	oddělený	k2eAgInSc2d1	oddělený
bezvodého	bezvodý	k2eAgInSc2d1	bezvodý
octanu	octan	k1gInSc2	octan
se	se	k3xPyFc4	se
kyselina	kyselina	k1gFnSc1	kyselina
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
působením	působení	k1gNnSc7	působení
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COO	COO	kA	COO
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Ca	ca	kA	ca
+	+	kIx~	+
H2SO4	H2SO4	k1gFnSc1	H2SO4
→	→	k?	→
2	[number]	k4	2
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
+	+	kIx~	+
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
veškerá	veškerý	k3xTgFnSc1	veškerý
ledová	ledový	k2eAgFnSc1d1	ledová
(	(	kIx(	(
<g/>
bezvodá	bezvodý	k2eAgFnSc1d1	bezvodá
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
pevné	pevný	k2eAgFnSc3d1	pevná
krystalické	krystalický	k2eAgFnSc3d1	krystalická
fázi	fáze	k1gFnSc3	fáze
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc4	dva
molekuly	molekula	k1gFnPc4	molekula
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
dimer	dimer	k1gMnSc1	dimer
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
jsou	být	k5eAaImIp3nP	být
vodíky	vodík	k1gInPc1	vodík
karboxylových	karboxylový	k2eAgFnPc2d1	karboxylová
skupin	skupina	k1gFnPc2	skupina
vázány	vázat	k5eAaImNgFnP	vázat
vodíkovou	vodíkový	k2eAgFnSc7d1	vodíková
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
kyslíkový	kyslíkový	k2eAgInSc4d1	kyslíkový
atom	atom	k1gInSc4	atom
karboxylu	karboxyl	k1gInSc2	karboxyl
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tyto	tento	k3xDgFnPc1	tento
dimery	dimera	k1gFnPc1	dimera
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
v	v	k7c6	v
bezvodé	bezvodý	k2eAgFnSc6d1	bezvodá
kapalné	kapalný	k2eAgFnSc6d1	kapalná
kyselině	kyselina	k1gFnSc6	kyselina
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
stop	stop	k2eAgFnSc2d1	stop
vody	voda	k1gFnSc2	voda
je	on	k3xPp3gMnPc4	on
okamžitě	okamžitě	k6eAd1	okamžitě
štěpí	štěpit	k5eAaImIp3nP	štěpit
na	na	k7c4	na
monomery	monomer	k1gInPc4	monomer
<g/>
.	.	kIx.	.
</s>
<s>
Dimery	Dimera	k1gFnPc1	Dimera
byly	být	k5eAaImAgFnP	být
však	však	k9	však
prokázány	prokázán	k2eAgFnPc1d1	prokázána
i	i	k9	i
v	v	k7c6	v
plynné	plynný	k2eAgFnSc6d1	plynná
fázi	fáze	k1gFnSc6	fáze
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
okolo	okolo	k7c2	okolo
120	[number]	k4	120
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
se	se	k3xPyFc4	se
neomezeně	omezeně	k6eNd1	omezeně
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
polárních	polární	k2eAgFnPc2d1	polární
i	i	k8xC	i
nepolárních	polární	k2eNgFnPc2d1	nepolární
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
s	s	k7c7	s
alkoholy	alkohol	k1gInPc7	alkohol
<g/>
,	,	kIx,	,
s	s	k7c7	s
ethery	ether	k1gInPc7	ether
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgNnSc7d1	dobré
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
<g/>
;	;	kIx,	;
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jak	jak	k6eAd1	jak
organické	organický	k2eAgFnSc3d1	organická
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgFnPc1d1	octová
se	se	k3xPyFc4	se
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
plynné	plynný	k2eAgInPc1d1	plynný
halogenovodíky	halogenovodík	k1gInPc1	halogenovodík
<g/>
,	,	kIx,	,
např.	např.	kA	např.
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
prvky	prvek	k1gInPc1	prvek
jako	jako	k8xS	jako
síra	síra	k1gFnSc1	síra
nebo	nebo	k8xC	nebo
jód	jód	k1gInSc1	jód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
středně	středně	k6eAd1	středně
silným	silný	k2eAgFnPc3d1	silná
jednosytným	jednosytný	k2eAgFnPc3d1	jednosytná
kyselinám	kyselina	k1gFnPc3	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
disociuje	disociovat	k5eAaBmIp3nS	disociovat
na	na	k7c4	na
acetátový	acetátový	k2eAgInSc4d1	acetátový
neboli	neboli	k8xC	neboli
octanový	octanový	k2eAgInSc4d1	octanový
aniont	aniont	k1gInSc4	aniont
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
↔	↔	k?	↔
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COO	COO	kA	COO
<g/>
−	−	k?	−
+	+	kIx~	+
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
silnější	silný	k2eAgFnSc2d2	silnější
anorganické	anorganický	k2eAgFnSc2d1	anorganická
kyseliny	kyselina	k1gFnSc2	kyselina
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
příslušného	příslušný	k2eAgInSc2d1	příslušný
octanu	octan	k1gInSc2	octan
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
2	[number]	k4	2
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
+	+	kIx~	+
Mg	mg	kA	mg
→	→	k?	→
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COO	COO	kA	COO
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Mg	mg	kA	mg
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Octany	octan	k1gInPc1	octan
také	také	k9	také
vznikají	vznikat	k5eAaImIp3nP	vznikat
neutralizací	neutralizace	k1gFnSc7	neutralizace
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgInPc1d1	octový
působením	působení	k1gNnSc7	působení
anorganických	anorganický	k2eAgInPc2d1	anorganický
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
KOH	KOH	kA	KOH
→	→	k?	→
CH3COOK	CH3COOK	k1gMnSc1	CH3COOK
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Octany	octan	k1gInPc1	octan
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
silnějších	silný	k2eAgFnPc2d2	silnější
kyselin	kyselina	k1gFnPc2	kyselina
zpětně	zpětně	k6eAd1	zpětně
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
a	a	k8xC	a
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
opět	opět	k6eAd1	opět
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COONa	COONum	k1gNnSc2	COONum
+	+	kIx~	+
HCl	HCl	k1gFnSc1	HCl
→	→	k?	→
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
+	+	kIx~	+
NaCl	NaCl	k1gInSc1	NaCl
<g/>
.	.	kIx.	.
<g/>
Naopak	naopak	k6eAd1	naopak
působením	působení	k1gNnSc7	působení
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
na	na	k7c6	na
soli	sůl	k1gFnSc6	sůl
slabších	slabý	k2eAgFnPc2d2	slabší
kyselin	kyselina	k1gFnPc2	kyselina
vznikají	vznikat	k5eAaImIp3nP	vznikat
octany	octan	k1gInPc1	octan
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
hydrogenuhličitanem	hydrogenuhličitan	k1gInSc7	hydrogenuhličitan
sodným	sodný	k2eAgInSc7d1	sodný
vzniká	vznikat	k5eAaImIp3nS	vznikat
octan	octan	k1gInSc1	octan
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
kyselina	kyselina	k1gFnSc1	kyselina
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
NaHCO	NaHCO	k1gFnSc1	NaHCO
<g/>
3	[number]	k4	3
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COONa	COONum	k1gNnSc2	COONum
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
+	+	kIx~	+
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
alkoholy	alkohol	k1gInPc7	alkohol
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
stopy	stopa	k1gFnSc2	stopa
silné	silný	k2eAgFnSc2d1	silná
minerální	minerální	k2eAgFnSc2d1	minerální
kyseliny	kyselina	k1gFnSc2	kyselina
vznikají	vznikat	k5eAaImIp3nP	vznikat
estery	ester	k1gInPc1	ester
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
ethanolem	ethanol	k1gInSc7	ethanol
vzniká	vznikat	k5eAaImIp3nS	vznikat
ethylacetát	ethylacetát	k1gInSc1	ethylacetát
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
CH3CH2OH	CH3CH2OH	k1gFnSc1	CH3CH2OH
↔	↔	k?	↔
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CO	co	k8xS	co
<g/>
–	–	k?	–
<g/>
O	o	k7c4	o
<g/>
–	–	k?	–
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Jak	jak	k6eAd1	jak
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
šipka	šipka	k1gFnSc1	šipka
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
zvratná	zvratný	k2eAgFnSc1d1	zvratná
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
probíhat	probíhat	k5eAaImF	probíhat
oběma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
-li	i	k?	-li
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
ji	on	k3xPp3gFnSc4	on
esterifikací	esterifikace	k1gFnSc7	esterifikace
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
-li	i	k?	-li
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
zmýdelňování	zmýdelňování	k1gNnSc6	zmýdelňování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působíme	působit	k5eAaImIp1nP	působit
<g/>
-li	i	k?	-li
na	na	k7c4	na
bezvodou	bezvodý	k2eAgFnSc4d1	bezvodá
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
chloridem	chlorid	k1gInSc7	chlorid
fosforečným	fosforečný	k2eAgInSc7d1	fosforečný
<g/>
,	,	kIx,	,
nahradí	nahradit	k5eAaPmIp3nS	nahradit
se	se	k3xPyFc4	se
v	v	k7c6	v
karboxylu	karboxyl	k1gInSc6	karboxyl
hydroxylová	hydroxylový	k2eAgFnSc1d1	hydroxylová
skupina	skupina	k1gFnSc1	skupina
–	–	k?	–
<g/>
OH	OH	kA	OH
atomem	atom	k1gInSc7	atom
chloru	chlor	k1gInSc2	chlor
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
acetylchlorid	acetylchlorida	k1gFnPc2	acetylchlorida
(	(	kIx(	(
<g/>
chloridu	chlorid	k1gInSc2	chlorid
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
PCl	PCl	k1gFnSc1	PCl
<g/>
5	[number]	k4	5
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COCl	COClum	k1gNnPc2	COClum
+	+	kIx~	+
HCl	HCl	k1gFnSc1	HCl
+	+	kIx~	+
POCl	POCl	k1gInSc1	POCl
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
<g/>
podobně	podobně	k6eAd1	podobně
lze	lze	k6eAd1	lze
acetylchlorid	acetylchlorid	k1gInSc4	acetylchlorid
připravit	připravit	k5eAaPmF	připravit
i	i	k9	i
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
chloridem	chlorid	k1gInSc7	chlorid
fosforitým	fosforitý	k2eAgInSc7d1	fosforitý
</s>
</p>
<p>
<s>
3	[number]	k4	3
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
+	+	kIx~	+
PCl	PCl	k1gFnSc1	PCl
<g/>
3	[number]	k4	3
→	→	k?	→
3	[number]	k4	3
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COCl	COClum	k1gNnPc2	COClum
+	+	kIx~	+
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
PO	Po	kA	Po
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
thionylchloridem	thionylchlorid	k1gInSc7	thionylchlorid
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
SOCl	SOCl	k1gInSc1	SOCl
<g/>
2	[number]	k4	2
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COCl	COCl	k1gInSc4	COCl
+	+	kIx~	+
HCl	HCl	k1gMnSc1	HCl
+	+	kIx~	+
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Přímým	přímý	k2eAgNnSc7d1	přímé
působením	působení	k1gNnSc7	působení
elementárního	elementární	k2eAgInSc2d1	elementární
chloru	chlor	k1gInSc2	chlor
nevzniká	vznikat	k5eNaImIp3nS	vznikat
acetylchlorid	acetylchlorid	k1gInSc1	acetylchlorid
<g/>
;	;	kIx,	;
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
chlor	chlor	k1gInSc1	chlor
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
vodíkové	vodíkový	k2eAgInPc4d1	vodíkový
atomy	atom	k1gInPc4	atom
v	v	k7c6	v
methylové	methylový	k2eAgFnSc6d1	methylová
skupině	skupina	k1gFnSc6	skupina
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
směsi	směs	k1gFnSc2	směs
chloroctových	chloroctův	k2eAgFnPc2d1	chloroctův
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
stupni	stupeň	k1gInSc6	stupeň
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
chloroctová	chloroctový	k2eAgFnSc1d1	chloroctový
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnSc2	Cl
<g/>
–	–	k?	–
<g/>
COOH	COOH	kA	COOH
+	+	kIx~	+
HCl	HCl	k1gFnSc1	HCl
<g/>
,	,	kIx,	,
<g/>
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
polárnímu	polární	k2eAgInSc3d1	polární
efektu	efekt	k1gInSc3	efekt
chloru	chlor	k1gInSc2	chlor
silnější	silný	k2eAgFnSc7d2	silnější
kyselinou	kyselina	k1gFnSc7	kyselina
než	než	k8xS	než
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
chlorací	chlorace	k1gFnSc7	chlorace
pak	pak	k6eAd1	pak
vznikají	vznikat	k5eAaImIp3nP	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
dichloroctová	dichloroctová	k1gFnSc1	dichloroctová
a	a	k8xC	a
trichloroctová	trichloroctová	k1gFnSc1	trichloroctová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působením	působení	k1gNnSc7	působení
vodu	voda	k1gFnSc4	voda
odnímajících	odnímající	k2eAgNnPc2d1	odnímající
činidel	činidlo	k1gNnPc2	činidlo
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotu	teplota	k1gFnSc4	teplota
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
molekul	molekula	k1gFnPc2	molekula
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
odštěpí	odštěpit	k5eAaPmIp3nS	odštěpit
molekula	molekula	k1gFnSc1	molekula
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
acetanhydrid	acetanhydrid	k1gInSc1	acetanhydrid
(	(	kIx(	(
<g/>
anhydrid	anhydrid	k1gInSc1	anhydrid
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CO	co	k8xS	co
<g/>
–	–	k?	–
<g/>
O	o	k7c6	o
<g/>
–	–	k?	–
<g/>
CO	co	k8xS	co
CH3	CH3	k1gMnSc1	CH3
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
vzniká	vznikat	k5eAaImIp3nS	vznikat
octan	octan	k1gInSc1	octan
amonný	amonný	k2eAgInSc1d1	amonný
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
NH3	NH3	k1gFnSc1	NH3
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COONH	COONH	kA	COONH
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
-li	i	k?	-li
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
ze	z	k7c2	z
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
soli	sůl	k1gFnSc2	sůl
se	se	k3xPyFc4	se
odštěpí	odštěpit	k5eAaPmIp3nS	odštěpit
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
acetamid	acetamid	k1gInSc1	acetamid
(	(	kIx(	(
<g/>
amid	amid	k1gInSc1	amid
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CH3COONH4	CH3COONH4	k4	CH3COONH4
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CO	co	k8xS	co
<g/>
–	–	k?	–
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Působením	působení	k1gNnSc7	působení
silně	silně	k6eAd1	silně
redukčních	redukční	k2eAgNnPc2d1	redukční
činidel	činidlo	k1gNnPc2	činidlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hydridu	hydrid	k1gInSc2	hydrid
lithnohlinitého	lithnohlinitý	k2eAgInSc2d1	lithnohlinitý
(	(	kIx(	(
<g/>
lithiumaluminiumhydridu	lithiumaluminiumhydrid	k1gInSc2	lithiumaluminiumhydrid
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
redukuje	redukovat	k5eAaBmIp3nS	redukovat
až	až	k9	až
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
dvojstupňově	dvojstupňově	k6eAd1	dvojstupňově
<g/>
;	;	kIx,	;
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
jsou	být	k5eAaImIp3nP	být
ethanolát	ethanolát	k1gInSc4	ethanolát
lithný	lithný	k2eAgInSc4d1	lithný
a	a	k8xC	a
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
</s>
</p>
<p>
<s>
4	[number]	k4	4
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
+	+	kIx~	+
LiAlH	LiAlH	k1gFnSc1	LiAlH
<g/>
4	[number]	k4	4
<g/>
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OLi	OLi	k?	OLi
+	+	kIx~	+
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OLi	OLi	k?	OLi
+	+	kIx~	+
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Al	ala	k1gFnPc2	ala
+	+	kIx~	+
4	[number]	k4	4
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
4	[number]	k4	4
CH3CH2OH	CH3CH2OH	k1gFnPc2	CH3CH2OH
+	+	kIx~	+
LiOH	LiOH	k1gFnPc2	LiOH
+	+	kIx~	+
Al	ala	k1gFnPc2	ala
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
deriváty	derivát	k1gInPc1	derivát
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgInPc1d1	octový
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Soli	sůl	k1gFnSc6	sůl
===	===	k?	===
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
draselný	draselný	k2eAgInSc1d1	draselný
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
amonný	amonný	k2eAgInSc1d1	amonný
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
železitý	železitý	k2eAgInSc1d1	železitý
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
chromitý	chromitý	k2eAgInSc1d1	chromitý
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
</s>
</p>
<p>
<s>
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
octan	octan	k1gInSc1	octan
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
</s>
</p>
<p>
<s>
octan	octan	k1gInSc1	octan
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
</s>
</p>
<p>
<s>
===	===	k?	===
Estery	ester	k1gInPc4	ester
===	===	k?	===
</s>
</p>
<p>
<s>
methylacetát	methylacetát	k1gInSc1	methylacetát
</s>
</p>
<p>
<s>
ethylacetát	ethylacetát	k1gInSc1	ethylacetát
</s>
</p>
<p>
<s>
vinylacetát	vinylacetát	k1gInSc1	vinylacetát
</s>
</p>
<p>
<s>
glyceryltriacetát	glyceryltriacetát	k1gInSc1	glyceryltriacetát
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgInPc1d1	jiný
deriváty	derivát	k1gInPc1	derivát
===	===	k?	===
</s>
</p>
<p>
<s>
acetanhydrid	acetanhydrid	k1gInSc1	acetanhydrid
</s>
</p>
<p>
<s>
acetylchlorid	acetylchlorid	k1gInSc1	acetylchlorid
</s>
</p>
<p>
<s>
acetamid	acetamid	k1gInSc1	acetamid
</s>
</p>
<p>
<s>
kyselina	kyselina	k1gFnSc1	kyselina
chloroctová	chloroctová	k1gFnSc1	chloroctová
</s>
</p>
<p>
<s>
kyselina	kyselina	k1gFnSc1	kyselina
bromoctová	bromoctová	k1gFnSc1	bromoctová
</s>
</p>
<p>
<s>
kyselina	kyselina	k1gFnSc1	kyselina
trifluoroctová	trifluoroctová	k1gFnSc1	trifluoroctová
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
organických	organický	k2eAgFnPc2d1	organická
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
přes	přes	k7c4	přes
5	[number]	k4	5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
i	i	k8xC	i
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
jako	jako	k9	jako
významné	významný	k2eAgNnSc1d1	významné
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
čistých	čistý	k2eAgFnPc2d1	čistá
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímo	přímo	k6eAd1	přímo
jako	jako	k9	jako
chemická	chemický	k2eAgFnSc1d1	chemická
surovina	surovina	k1gFnSc1	surovina
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgFnPc2d1	další
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
vinylacetát	vinylacetát	k1gInSc1	vinylacetát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
následně	následně	k6eAd1	následně
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
(	(	kIx(	(
<g/>
monomer	monomer	k1gInSc1	monomer
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
polyvinylacetátu	polyvinylacetát	k1gInSc2	polyvinylacetát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Derivát	derivát	k1gInSc1	derivát
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
acetanhydrid	acetanhydrid	k1gInSc1	acetanhydrid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
silné	silný	k2eAgNnSc1d1	silné
acylační	acylační	k2eAgNnSc1d1	acylační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
acetylcelulosy	acetylcelulosa	k1gFnSc2	acetylcelulosa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
sloužila	sloužit	k5eAaImAgFnS	sloužit
jak	jak	k8xC	jak
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
syntetických	syntetický	k2eAgNnPc2d1	syntetické
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
acetátové	acetátový	k2eAgNnSc1d1	acetátové
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
podložek	podložka	k1gFnPc2	podložka
pro	pro	k7c4	pro
fotografické	fotografický	k2eAgInPc4d1	fotografický
negativní	negativní	k2eAgInPc4d1	negativní
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
pro	pro	k7c4	pro
kinofilm	kinofilm	k1gInSc4	kinofilm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotografické	fotografický	k2eAgFnSc6d1	fotografická
chemii	chemie	k1gFnSc6	chemie
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
přerušovací	přerušovací	k2eAgFnSc1d1	přerušovací
lázeň	lázeň	k1gFnSc1	lázeň
a	a	k8xC	a
součást	součást	k1gFnSc1	součást
některých	některý	k3yIgInPc2	některý
ustalovačů	ustalovač	k1gInPc2	ustalovač
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
surovin	surovina	k1gFnPc2	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
acetylsalicylové	acetylsalicylový	k2eAgFnSc2d1	acetylsalicylová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
léků	lék	k1gInPc2	lék
proti	proti	k7c3	proti
horečkám	horečka	k1gFnPc3	horečka
a	a	k8xC	a
zánětům	zánět	k1gInPc3	zánět
(	(	kIx(	(
<g/>
Acylpyrin	acylpyrin	k1gInSc1	acylpyrin
<g/>
,	,	kIx,	,
Aspirin	aspirin	k1gInSc1	aspirin
<g/>
,	,	kIx,	,
Anopyrin	Anopyrin	k1gInSc1	Anopyrin
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Octan	octan	k1gInSc1	octan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
acetonu	aceton	k1gInSc2	aceton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
aplikacemi	aplikace	k1gFnPc7	aplikace
je	být	k5eAaImIp3nS	být
spotřeba	spotřeba	k1gFnSc1	spotřeba
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
octa	ocet	k1gInSc2	ocet
v	v	k7c6	v
kulinářských	kulinářský	k2eAgFnPc6d1	kulinářská
aplikacích	aplikace	k1gFnPc6	aplikace
pro	pro	k7c4	pro
okyselování	okyselování	k1gNnSc4	okyselování
jídel	jídlo	k1gNnPc2	jídlo
a	a	k8xC	a
při	při	k7c6	při
konzervaci	konzervace	k1gFnSc6	konzervace
zeleniny	zelenina	k1gFnSc2	zelenina
zcela	zcela	k6eAd1	zcela
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
nachází	nacházet	k5eAaImIp3nS	nacházet
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
kyselosti	kyselost	k1gFnSc3	kyselost
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc3	schopnost
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
uhličitany	uhličitan	k1gInPc4	uhličitan
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
čisticích	čisticí	k2eAgInPc2d1	čisticí
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
odstraňování	odstraňování	k1gNnSc4	odstraňování
vodního	vodní	k2eAgInSc2d1	vodní
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
kyseliny	kyselina	k1gFnSc2	kyselina
mravenčí	mravenčit	k5eAaImIp3nS	mravenčit
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
přísada	přísada	k1gFnSc1	přísada
ke	k	k7c3	k
konzervaci	konzervace	k1gFnSc3	konzervace
siláže	siláž	k1gFnSc2	siláž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
baktericidní	baktericidní	k2eAgInSc1d1	baktericidní
a	a	k8xC	a
fungicidní	fungicidní	k2eAgInSc1d1	fungicidní
účinek	účinek	k1gInSc1	účinek
kyseliny	kyselina	k1gFnSc2	kyselina
mléčné	mléčný	k2eAgFnSc2d1	mléčná
<g/>
,	,	kIx,	,
spontánně	spontánně	k6eAd1	spontánně
vznikající	vznikající	k2eAgFnSc4d1	vznikající
při	pře	k1gFnSc4	pře
kvašení	kvašení	k1gNnSc2	kvašení
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyziologické	fyziologický	k2eAgNnSc4d1	fyziologické
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přiměřených	přiměřený	k2eAgFnPc6d1	přiměřená
dávkách	dávka	k1gFnPc6	dávka
a	a	k8xC	a
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
zředění	zředění	k1gNnSc6	zředění
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
zcela	zcela	k6eAd1	zcela
neškodná	škodný	k2eNgFnSc1d1	neškodná
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
je	být	k5eAaImIp3nS	být
však	však	k9	však
značně	značně	k6eAd1	značně
agresivní	agresivní	k2eAgMnSc1d1	agresivní
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
popáleniny	popálenina	k1gFnSc2	popálenina
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
nebezpečnost	nebezpečnost	k1gFnSc1	nebezpečnost
tkví	tkvět	k5eAaImIp3nS	tkvět
také	také	k9	také
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
projevy	projev	k1gInPc1	projev
poškození	poškození	k1gNnSc2	poškození
pokožky	pokožka	k1gFnSc2	pokožka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
až	až	k9	až
s	s	k7c7	s
několikahodinovým	několikahodinový	k2eAgNnSc7d1	několikahodinové
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
tkáň	tkáň	k1gFnSc1	tkáň
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zasažení	zasažení	k1gNnSc6	zasažení
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vdechováním	vdechování	k1gNnSc7	vdechování
par	para	k1gFnPc2	para
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
silnému	silný	k2eAgNnSc3d1	silné
poleptání	poleptání	k1gNnSc3	poleptání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
dýchacími	dýchací	k2eAgFnPc7d1	dýchací
potížemi	potíž	k1gFnPc7	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zasažení	zasažení	k1gNnSc6	zasažení
očí	oko	k1gNnPc2	oko
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
silnému	silný	k2eAgNnSc3d1	silné
poleptání	poleptání	k1gNnSc3	poleptání
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
oslepnutí	oslepnutí	k1gNnSc3	oslepnutí
<g/>
.	.	kIx.	.
</s>
<s>
Požití	požití	k1gNnSc1	požití
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgNnSc1d1	octové
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
poleptání	poleptání	k1gNnSc1	poleptání
sliznic	sliznice	k1gFnPc2	sliznice
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
<g/>
,	,	kIx,	,
jicnu	jicn	k1gInSc6	jicn
a	a	k8xC	a
žaludku	žaludek	k1gInSc6	žaludek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
proděravění	proděravění	k1gNnSc3	proděravění
stěn	stěna	k1gFnPc2	stěna
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Požití	požití	k1gNnSc1	požití
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
silnou	silný	k2eAgFnSc7d1	silná
bolestivostí	bolestivost	k1gFnSc7	bolestivost
zasažených	zasažený	k2eAgFnPc2d1	zasažená
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
,	,	kIx,	,
vrhnutím	vrhnutí	k1gNnSc7	vrhnutí
a	a	k8xC	a
průjmem	průjem	k1gInSc7	průjem
a	a	k8xC	a
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dávkách	dávka	k1gFnPc6	dávka
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
krys	krysa	k1gFnPc2	krysa
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
smrtná	smrtný	k2eAgFnSc1d1	Smrtná
dávka	dávka	k1gFnSc1	dávka
LD50	LD50	k1gFnSc2	LD50
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
na	na	k7c4	na
3310	[number]	k4	3310
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
Smrtná	smrtný	k2eAgFnSc1d1	Smrtná
koncentrace	koncentrace	k1gFnSc1	koncentrace
LC50	LC50	k1gFnSc2	LC50
ve	v	k7c6	v
vdechovaném	vdechovaný	k2eAgInSc6d1	vdechovaný
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
5620	[number]	k4	5620
ppm	ppm	k?	ppm
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
1	[number]	k4	1
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Smrtná	smrtný	k2eAgFnSc1d1	Smrtná
dávka	dávka	k1gFnSc1	dávka
u	u	k7c2	u
králíka	králík	k1gMnSc2	králík
při	při	k7c6	při
aplikaci	aplikace	k1gFnSc6	aplikace
na	na	k7c4	na
pokožku	pokožka	k1gFnSc4	pokožka
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
(	(	kIx(	(
<g/>
LD	LD	kA	LD
<g/>
50	[number]	k4	50
=	=	kIx~	=
1060	[number]	k4	1060
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
není	být	k5eNaImIp3nS	být
kancerogenní	kancerogenní	k2eAgFnSc1d1	kancerogenní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historická	historický	k2eAgFnSc1d1	historická
poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Zředěný	zředěný	k2eAgInSc1d1	zředěný
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
ocet	ocet	k1gInSc1	ocet
<g/>
,	,	kIx,	,
znalo	znát	k5eAaImAgNnS	znát
lidstvo	lidstvo	k1gNnSc1	lidstvo
již	již	k6eAd1	již
v	v	k7c6	v
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
řecký	řecký	k2eAgMnSc1d1	řecký
filosof	filosof	k1gMnSc1	filosof
Theophrastos	Theophrastos	k1gMnSc1	Theophrastos
popsal	popsat	k5eAaPmAgMnS	popsat
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
pigmentů	pigment	k1gInPc2	pigment
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
některých	některý	k3yIgInPc2	některý
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
olova	olovo	k1gNnSc2	olovo
nebo	nebo	k8xC	nebo
mědi	měď	k1gFnSc2	měď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
připravovali	připravovat	k5eAaImAgMnP	připravovat
varem	var	k1gInSc7	var
zkysaného	zkysaný	k2eAgNnSc2d1	zkysané
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
olověných	olověný	k2eAgFnPc6d1	olověná
nádobách	nádoba	k1gFnPc6	nádoba
sladký	sladký	k2eAgInSc4d1	sladký
sirup	sirup	k1gInSc4	sirup
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
octan	octan	k1gInSc1	octan
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
olověný	olověný	k2eAgInSc1d1	olověný
cukr	cukr	k1gInSc1	cukr
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jako	jako	k9	jako
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
cukr	cukr	k1gInSc4	cukr
(	(	kIx(	(
<g/>
Saccharum	Saccharum	k1gInSc1	Saccharum
Saturni	Saturni	k1gFnSc2	Saturni
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
značnou	značný	k2eAgFnSc7d1	značná
měrou	míra	k1gFnSc7wR	míra
přispíval	přispívat	k5eAaImAgInS	přispívat
k	k	k7c3	k
chronickým	chronický	k2eAgFnPc3d1	chronická
otravám	otrava	k1gFnPc3	otrava
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
římské	římský	k2eAgFnSc2d1	římská
aristokracie	aristokracie	k1gFnSc2	aristokracie
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
perský	perský	k2eAgMnSc1d1	perský
alchymista	alchymista	k1gMnSc1	alchymista
Džabir	Džabir	k1gMnSc1	Džabir
ibn	ibn	k?	ibn
Hajján	Hajján	k1gMnSc1	Hajján
(	(	kIx(	(
<g/>
asi	asi	k9	asi
721	[number]	k4	721
<g/>
–	–	k?	–
<g/>
815	[number]	k4	815
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
též	též	k9	též
Geber	Geber	k1gInSc4	Geber
<g/>
,	,	kIx,	,
připravil	připravit	k5eAaPmAgInS	připravit
destilací	destilace	k1gFnSc7	destilace
octa	ocet	k1gInSc2	ocet
poprvé	poprvé	k6eAd1	poprvé
koncentrovanější	koncentrovaný	k2eAgFnSc4d2	koncentrovanější
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezvodá	bezvodý	k2eAgFnSc1d1	bezvodá
ledová	ledový	k2eAgFnSc1d1	ledová
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
octanů	octan	k1gInPc2	octan
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Andreas	Andreas	k1gMnSc1	Andreas
Libavius	Libavius	k1gMnSc1	Libavius
(	(	kIx(	(
<g/>
1555	[number]	k4	1555
<g/>
–	–	k?	–
<g/>
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
také	také	k9	také
jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledová	ledový	k2eAgFnSc1d1	ledová
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
a	a	k8xC	a
ocet	ocet	k1gInSc1	ocet
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgFnPc1d1	lišící
se	se	k3xPyFc4	se
jen	jen	k9	jen
obsahem	obsah	k1gInSc7	obsah
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
jeho	on	k3xPp3gInSc4	on
předpoklad	předpoklad	k1gInSc4	předpoklad
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Pierre	Pierr	k1gInSc5	Pierr
Adet	Adet	k1gMnSc1	Adet
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Hermann	Hermann	k1gMnSc1	Hermann
Kolbe	Kolb	k1gInSc5	Kolb
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
připravil	připravit	k5eAaPmAgInS	připravit
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc7d1	octová
syntézou	syntéza	k1gFnSc7	syntéza
z	z	k7c2	z
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgInS	vyjít
přitom	přitom	k6eAd1	přitom
ze	z	k7c2	z
sirouhlíku	sirouhlík	k1gInSc2	sirouhlík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působením	působení	k1gNnSc7	působení
chloru	chlor	k1gInSc2	chlor
převedl	převést	k5eAaPmAgMnS	převést
na	na	k7c4	na
tetrachlormethan	tetrachlormethan	k1gInSc4	tetrachlormethan
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
následnou	následný	k2eAgFnSc7d1	následná
pyrolýzou	pyrolýza	k1gFnSc7	pyrolýza
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
v	v	k7c4	v
tetrachloethen	tetrachloethen	k1gInSc4	tetrachloethen
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
adicí	adice	k1gFnPc2	adice
vody	voda	k1gFnSc2	voda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kyselina	kyselina	k1gFnSc1	kyselina
trichloroctová	trichloroctová	k1gFnSc1	trichloroctová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pak	pak	k6eAd1	pak
elektrolytickou	elektrolytický	k2eAgFnSc7d1	elektrolytická
redukcí	redukce	k1gFnSc7	redukce
připravil	připravit	k5eAaPmAgInS	připravit
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
