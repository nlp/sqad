<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
ostrého	ostrý	k2eAgInSc2d1	ostrý
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
mísitelná	mísitelný	k2eAgFnSc1d1	mísitelná
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
s	s	k7c7	s
ethanolem	ethanol	k1gInSc7	ethanol
i	i	k8xC	i
dimethyletherem	dimethylether	k1gInSc7	dimethylether
<g/>
.	.	kIx.	.
</s>
