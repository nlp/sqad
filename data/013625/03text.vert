<s>
Ladislav	Ladislav	k1gMnSc1
Fouček	Fouček	k1gMnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Fouček	Fouček	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1930	#num#	k4
<g/>
Praha	Praha	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Mnichov	Mnichov	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
cyklista	cyklista	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Cyklistika	cyklistika	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
LOH	LOH	kA
1956	#num#	k4
</s>
<s>
cyklistika	cyklistika	k1gFnSc1
–	–	k?
1	#num#	k4
km	km	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
LOH	LOH	kA
1956	#num#	k4
</s>
<s>
cyklistika	cyklistika	k1gFnSc1
–	–	k?
tandem	tandem	k1gInSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Fouček	Fouček	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
československý	československý	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
v	v	k7c6
dráhové	dráhový	k2eAgFnSc6d1
cyklistice	cyklistika	k1gFnSc6
<g/>
,	,	kIx,
olympionik	olympionik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Získal	získat	k5eAaPmAgInS
dvě	dva	k4xCgFnPc4
stříbrné	stříbrný	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
1956	#num#	k4
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
v	v	k7c6
závodě	závod	k1gInSc6
s	s	k7c7
pevným	pevný	k2eAgInSc7d1
startem	start	k1gInSc7
na	na	k7c4
1	#num#	k4
km	km	kA
a	a	k8xC
v	v	k7c6
tandemovém	tandemový	k2eAgInSc6d1
závodě	závod	k1gInSc6
společně	společně	k6eAd1
s	s	k7c7
Václavem	Václav	k1gMnSc7
Machkem	Machek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
už	už	k9
na	na	k7c6
předchozí	předchozí	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgInS
na	na	k7c4
12	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Československo	Československo	k1gNnSc1
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
databaseolympics	databaseolympics	k6eAd1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Fouček	Fouček	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Cyklistika	cyklistika	k1gFnSc1
</s>
