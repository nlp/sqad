<s>
John	John	k1gMnSc1
Evan	Evan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Evan	Evan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1810	#num#	k4
<g/>
Brecon	Brecon	k1gNnSc4
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1873	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Londýn	Londýn	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Brompton	Brompton	k1gInSc1
Cemetery	Cemeter	k1gInPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
sochař	sochař	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Evan	Evan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1810	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1873	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
velšský	velšský	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Breconu	Brecon	k1gInSc6
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
usadil	usadit	k5eAaPmAgInS
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tady	tady	k6eAd1
také	také	k9
studoval	studovat	k5eAaImAgMnS
a	a	k8xC
jedním	jeden	k4xCgMnSc7
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
učitelů	učitel	k1gMnPc2
byl	být	k5eAaImAgInS
například	například	k6eAd1
Francis	Francis	k1gInSc1
Leggatt	Leggatta	k1gFnPc2
Chantrey	Chantrea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
rokem	rok	k1gInSc7
1831	#num#	k4
vytvářel	vytvářet	k5eAaImAgMnS
církevní	církevní	k2eAgFnPc4d1
sochy	socha	k1gFnPc4
v	v	k7c6
rodném	rodný	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
autorem	autor	k1gMnSc7
soch	socha	k1gFnPc2
řady	řada	k1gFnSc2
šlechticů	šlechtic	k1gMnPc2
<g/>
,	,	kIx,
jakými	jaký	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
byli	být	k5eAaImAgMnP
například	například	k6eAd1
Robert	Robert	k1gMnSc1
Stewart	Stewart	k1gMnSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Wellesley	Welleslea	k1gFnSc2
a	a	k8xC
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Londýně	Londýn	k1gInSc6
roku	rok	k1gInSc2
1873	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
John	John	k1gMnSc1
Evan	Evan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
97040334	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
31601581	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
97040334	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Umění	umění	k1gNnSc1
</s>
