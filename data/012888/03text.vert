<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
I.	I.	kA	I.
Svatý	svatý	k1gMnSc1	svatý
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
László	László	k1gFnSc1	László
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Ladislaus	Ladislaus	k1gMnSc1	Ladislaus
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1046	[number]	k4	1046
<g/>
?	?	kIx.	?
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1095	[number]	k4	1095
<g/>
,	,	kIx,	,
česko-uherská	českoherský	k2eAgFnSc1d1	česko-uherská
hranice	hranice	k1gFnSc1	hranice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1077	[number]	k4	1077
<g/>
-	-	kIx~	-
<g/>
1095	[number]	k4	1095
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fakta	faktum	k1gNnPc1	faktum
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
krále	král	k1gMnSc2	král
Bély	Béla	k1gMnSc2	Béla
I.	I.	kA	I.
a	a	k8xC	a
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
krále	král	k1gMnSc2	král
Gejzy	Gejza	k1gFnSc2	Gejza
I.	I.	kA	I.
Po	po	k7c6	po
Bélově	Bélův	k2eAgFnSc6d1	Bélova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1063	[number]	k4	1063
vtrhnul	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
německý	německý	k2eAgMnSc1d1	německý
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Ladislava	Ladislav	k1gMnSc4	Ladislav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc4	jeho
bratry	bratr	k1gMnPc4	bratr
Gejzu	Gejza	k1gFnSc4	Gejza
a	a	k8xC	a
Lamperta	Lampert	k1gMnSc4	Lampert
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
princové	princ	k1gMnPc1	princ
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
polského	polský	k2eAgMnSc2d1	polský
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
také	také	k9	také
dočkali	dočkat	k5eAaPmAgMnP	dočkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1074	[number]	k4	1074
byl	být	k5eAaImAgMnS	být
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Šalamoun	Šalamoun	k1gMnSc1	Šalamoun
Gejzou	Gejza	k1gFnSc7	Gejza
poražen	porazit	k5eAaPmNgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Mogyródu	Mogyród	k1gInSc2	Mogyród
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
vítězství	vítězství	k1gNnSc6	vítězství
byl	být	k5eAaImAgMnS	být
Gejza	Gejz	k1gMnSc4	Gejz
korunován	korunovat	k5eAaBmNgMnS	korunovat
ve	v	k7c6	v
Stoličném	stoličný	k1gMnSc6	stoličný
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
na	na	k7c4	na
uherského	uherský	k2eAgMnSc4d1	uherský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Gejza	Gejz	k1gMnSc4	Gejz
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1077	[number]	k4	1077
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
politicky	politicky	k6eAd1	politicky
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Zabránil	zabránit	k5eAaPmAgMnS	zabránit
návratu	návrat	k1gInSc3	návrat
Šalamouna	Šalamoun	k1gMnSc2	Šalamoun
I.	I.	kA	I.
<g/>
,	,	kIx,	,
podporovaného	podporovaný	k2eAgInSc2d1	podporovaný
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
kanonizace	kanonizace	k1gFnSc2	kanonizace
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
syna	syn	k1gMnSc4	syn
Imricha	Imrich	k1gMnSc4	Imrich
a	a	k8xC	a
biskupa	biskup	k1gMnSc4	biskup
Gellerta	Gellert	k1gMnSc4	Gellert
roku	rok	k1gInSc2	rok
1083	[number]	k4	1083
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
zákoníků	zákoník	k1gInPc2	zákoník
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
stabilizace	stabilizace	k1gFnSc2	stabilizace
Uher	uher	k1gInSc1	uher
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Koruně	koruna	k1gFnSc3	koruna
uherské	uherský	k2eAgFnSc2d1	uherská
připojil	připojit	k5eAaPmAgInS	připojit
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
založil	založit	k5eAaPmAgMnS	založit
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1095	[number]	k4	1095
<g/>
,	,	kIx,	,
když	když	k8xS	když
připravoval	připravovat	k5eAaImAgMnS	připravovat
vojenské	vojenský	k2eAgNnSc4d1	vojenské
tažení	tažení	k1gNnSc4	tažení
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
vtrhl	vtrhnout	k5eAaPmAgInS	vtrhnout
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
vojsky	vojsko	k1gNnPc7	vojsko
Koloman	Koloman	k1gMnSc1	Koloman
Uherský	uherský	k2eAgMnSc1d1	uherský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
kanonizován	kanonizován	k2eAgInSc1d1	kanonizován
roku	rok	k1gInSc2	rok
1192	[number]	k4	1192
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
somogyvárském	somogyvárský	k2eAgNnSc6d1	somogyvárský
opatství	opatství	k1gNnSc6	opatství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
sám	sám	k3xTgMnSc1	sám
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
v	v	k7c6	v
rábské	rábský	k2eAgFnSc6d1	Rábská
katedrále	katedrála	k1gFnSc6	katedrála
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zlatý	zlatý	k2eAgInSc1d1	zlatý
relikviář	relikviář	k1gInSc1	relikviář
svatého	svatý	k2eAgMnSc2d1	svatý
Ladislava	Ladislav	k1gMnSc2	Ladislav
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1106	[number]	k4	1106
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
přeneseny	přenesen	k2eAgInPc1d1	přenesen
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Varadíně	Varadín	k1gInSc6	Varadín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ladislavská	Ladislavský	k2eAgFnSc1d1	Ladislavský
legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Legenda	legenda	k1gFnSc1	legenda
Sancti	Sancť	k1gFnSc2	Sancť
Ladislai	Ladisla	k1gFnSc2	Ladisla
Regis	Regis	k1gFnSc2	Regis
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
<g/>
:	:	kIx,	:
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nejstaršími	starý	k2eAgInPc7d3	nejstarší
písemnými	písemný	k2eAgInPc7d1	písemný
prameny	pramen	k1gInPc7	pramen
o	o	k7c6	o
králi	král	k1gMnSc6	král
Ladislavovi	Ladislav	k1gMnSc6	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k8xS	jako
vládce	vládce	k1gMnSc1	vládce
udatný	udatný	k2eAgMnSc1d1	udatný
(	(	kIx(	(
<g/>
ubránil	ubránit	k5eAaPmAgMnS	ubránit
zemi	zem	k1gFnSc4	zem
proti	proti	k7c3	proti
nájezdům	nájezd	k1gInPc3	nájezd
Pečeněhů	Pečeněh	k1gMnPc2	Pečeněh
a	a	k8xC	a
Kumánů	Kumán	k1gMnPc2	Kumán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
urostlý	urostlý	k2eAgInSc1d1	urostlý
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
o	o	k7c4	o
hlavu	hlava	k1gFnSc4	hlava
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
"	"	kIx"	"
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
)	)	kIx)	)
a	a	k8xC	a
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zdůrazněno	zdůraznit	k5eAaPmNgNnS	zdůraznit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dovolil	dovolit	k5eAaPmAgInS	dovolit
Šalamounovi	Šalamoun	k1gMnSc3	Šalamoun
navrátit	navrátit	k5eAaPmF	navrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbožný	zbožný	k2eAgMnSc1d1	zbožný
(	(	kIx(	(
<g/>
obracel	obracet	k5eAaImAgMnS	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
často	často	k6eAd1	často
svými	svůj	k3xOyFgFnPc7	svůj
modlitbami	modlitba	k1gFnPc7	modlitba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgInSc1d1	popisován
zázrak	zázrak	k1gInSc1	zázrak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
udál	udát	k5eAaPmAgInS	udát
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ladislavovo	Ladislavův	k2eAgNnSc1d1	Ladislavovo
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
sužováno	sužovat	k5eAaImNgNnS	sužovat
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
vzdálil	vzdálit	k5eAaPmAgMnS	vzdálit
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
ponořil	ponořit	k5eAaPmAgInS	ponořit
se	se	k3xPyFc4	se
do	do	k7c2	do
modliteb	modlitba	k1gFnPc2	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Prosil	prosít	k5eAaPmAgMnS	prosít
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nasytil	nasytit	k5eAaPmAgMnS	nasytit
jeho	jeho	k3xOp3gMnPc4	jeho
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
a	a	k8xC	a
uviděl	uvidět	k5eAaPmAgMnS	uvidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přichází	přicházet	k5eAaImIp3nS	přicházet
stádo	stádo	k1gNnSc4	stádo
jelenů	jelen	k1gMnPc2	jelen
a	a	k8xC	a
buvolů	buvol	k1gMnPc2	buvol
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgInS	být
světcem	světec	k1gMnSc7	světec
i	i	k8xC	i
rytířem	rytíř	k1gMnSc7	rytíř
i	i	k8xC	i
dobrým	dobrý	k2eAgMnSc7d1	dobrý
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svatý	svatý	k1gMnSc1	svatý
Ladislav	Ladislava	k1gFnPc2	Ladislava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
KRISTÓ	KRISTÓ	kA	KRISTÓ
<g/>
,	,	kIx,	,
Gyula	Gyulo	k1gNnSc2	Gyulo
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Arpaden-Dynastie	Arpaden-Dynastie	k1gFnSc1	Arpaden-Dynastie
:	:	kIx,	:
die	die	k?	die
Geschichte	Geschicht	k1gInSc5	Geschicht
Ungarns	Ungarns	k1gInSc4	Ungarns
von	von	k1gInSc1	von
895	[number]	k4	895
bis	bis	k?	bis
1301	[number]	k4	1301
<g/>
.	.	kIx.	.
</s>
<s>
Budapest	Budapest	k1gFnSc1	Budapest
<g/>
:	:	kIx,	:
Corvina	Corvina	k1gFnSc1	Corvina
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
310	[number]	k4	310
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9631338576	[number]	k4	9631338576
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KONTLER	KONTLER	kA	KONTLER
<g/>
,	,	kIx,	,
László	László	k1gFnSc1	László
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
405	[number]	k4	405
<g/>
-	-	kIx~	-
<g/>
x.	x.	k?	x.
S.	S.	kA	S.
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
LENDVAI	LENDVAI	kA	LENDVAI
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Tisíc	tisíc	k4xCgInSc1	tisíc
let	let	k1gInSc1	let
maďarského	maďarský	k2eAgInSc2d1	maďarský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
856	[number]	k4	856
<g/>
-	-	kIx~	-
<g/>
x.	x.	k?	x.
S.	S.	kA	S.
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
kroniky	kronika	k1gFnPc1	kronika
koruny	koruna	k1gFnSc2	koruna
uherské	uherský	k2eAgNnSc1d1	Uherské
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Richard	Richard	k1gMnSc1	Richard
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Dagmar	Dagmar	k1gFnSc1	Dagmar
Bartoňková	Bartoňková	k1gFnSc1	Bartoňková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Nechutová	Nechutový	k2eAgFnSc1d1	Nechutová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
389	[number]	k4	389
s.	s.	k?	s.
S.	S.	kA	S.
132	[number]	k4	132
<g/>
-	-	kIx~	-
<g/>
144	[number]	k4	144
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LÁZÁR	LÁZÁR	kA	LÁZÁR
<g/>
,	,	kIx,	,
István	István	k2eAgInSc1d1	István
<g/>
.	.	kIx.	.
</s>
<s>
An	An	k?	An
Ilustrated	Ilustrated	k1gInSc1	Ilustrated
History	Histor	k1gInPc1	Histor
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
<g/>
.	.	kIx.	.
</s>
<s>
Budapest	Budapest	k1gFnSc1	Budapest
<g/>
:	:	kIx,	:
Corvina	Corvina	k1gFnSc1	Corvina
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
963	[number]	k4	963
13	[number]	k4	13
4887	[number]	k4	4887
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STEINHÜBEL	STEINHÜBEL	kA	STEINHÜBEL
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
<g/>
.	.	kIx.	.
</s>
<s>
Nitrianske	Nitrianske	k6eAd1	Nitrianske
kniežatstvo	kniežatstvo	k1gNnSc1	kniežatstvo
:	:	kIx,	:
počiatky	počiatek	k1gInPc1	počiatek
stredovekého	stredoveký	k2eAgNnSc2d1	stredoveký
Slovenska	Slovensko	k1gNnSc2	Slovensko
:	:	kIx,	:
rozprávanie	rozprávanie	k1gFnSc1	rozprávanie
o	o	k7c6	o
dejinách	dejina	k1gFnPc6	dejina
nášho	náš	k1gMnSc2	náš
územia	územius	k1gMnSc2	územius
a	a	k8xC	a
okolitých	okolitý	k2eAgFnPc2d1	okolitá
krajín	krajína	k1gFnPc2	krajína
od	od	k7c2	od
sťahovania	sťahovanium	k1gNnSc2	sťahovanium
národov	národov	k1gInSc1	národov
do	do	k7c2	do
začiatku	začiatek	k1gInSc2	začiatek
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
storočia	storočium	k1gNnSc2	storočium
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Veda	vést	k5eAaImSgInS	vést
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
224	[number]	k4	224
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
812	[number]	k4	812
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
