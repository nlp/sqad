<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
I.	I.	kA	I.
Svatý	svatý	k1gMnSc1	svatý
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
László	László	k1gFnSc1	László
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Ladislaus	Ladislaus	k1gMnSc1	Ladislaus
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1046	[number]	k4	1046
<g/>
?	?	kIx.	?
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1095	[number]	k4	1095
<g/>
,	,	kIx,	,
česko-uherská	českoherský	k2eAgFnSc1d1	česko-uherská
hranice	hranice	k1gFnSc1	hranice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1077	[number]	k4	1077
<g/>
-	-	kIx~	-
<g/>
1095	[number]	k4	1095
<g/>
.	.	kIx.	.
</s>
