<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Predsjednik	Predsjednik	k1gInSc1	Predsjednik
Republike	Republik	k1gInSc2	Republik
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
volený	volený	k2eAgInSc1d1	volený
občany	občan	k1gMnPc7	občan
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
mandát	mandát	k1gInSc1	mandát
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
