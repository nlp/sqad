<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Michael	Michael	k1gMnSc1	Michael
František	František	k1gMnSc1	František
Burian	Burian	k1gMnSc1	Burian
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1905	[number]	k4	1905
Kopřivnice	Kopřivnice	k1gFnSc2	Kopřivnice
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1981	[number]	k4	1981
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
dobrodružných	dobrodružný	k2eAgFnPc2d1	dobrodružná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgMnS	proslout
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgFnPc7	svůj
paleontologickými	paleontologický	k2eAgFnPc7d1	paleontologická
rekonstrukcemi	rekonstrukce	k1gFnPc7	rekonstrukce
společně	společně	k6eAd1	společně
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Josefem	Josef	k1gMnSc7	Josef
Augustou	Augusta	k1gMnSc7	Augusta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc2	on
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
talentu	talent	k1gInSc2	talent
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
již	již	k6eAd1	již
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
(	(	kIx(	(
<g/>
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
Jana	Jan	k1gMnSc2	Jan
Gotha	Gotha	k1gFnSc1	Gotha
a	a	k8xC	a
Maxe	Max	k1gMnSc2	Max
Švabinského	Švabinský	k2eAgMnSc2d1	Švabinský
<g/>
)	)	kIx)	)
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
vydržel	vydržet	k5eAaPmAgMnS	vydržet
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
necelé	celý	k2eNgInPc4d1	necelý
2	[number]	k4	2
roky	rok	k1gInPc4	rok
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
živit	živit	k5eAaImF	živit
jako	jako	k9	jako
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
především	především	k9	především
dobrodružnou	dobrodružný	k2eAgFnSc4d1	dobrodružná
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
takto	takto	k6eAd1	takto
přispěl	přispět	k5eAaPmAgMnS	přispět
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
Davida	David	k1gMnSc2	David
Balfoura	Balfour	k1gMnSc2	Balfour
od	od	k7c2	od
Stevensona	Stevenson	k1gMnSc2	Stevenson
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
knih	kniha	k1gFnPc2	kniha
Eduarda	Eduard	k1gMnSc2	Eduard
Štorcha	Štorch	k1gMnSc2	Štorch
<g/>
,	,	kIx,	,
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
ilustrace	ilustrace	k1gFnPc1	ilustrace
měly	mít	k5eAaImAgFnP	mít
osudový	osudový	k2eAgInSc4d1	osudový
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
uznání	uznání	k1gNnSc2	uznání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
dostalo	dostat	k5eAaPmAgNnS	dostat
především	především	k9	především
za	za	k7c4	za
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
vymřelých	vymřelý	k2eAgNnPc2d1	vymřelé
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
odborníky	odborník	k1gMnPc7	odborník
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byli	být	k5eAaImAgMnP	být
paleontolog	paleontolog	k1gMnSc1	paleontolog
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
antropolog	antropolog	k1gMnSc1	antropolog
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Fetter	Fetter	k1gMnSc1	Fetter
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Josefa	Josef	k1gMnSc2	Josef
Augusty	Augusta	k1gMnSc2	Augusta
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
s	s	k7c7	s
paleontologem	paleontolog	k1gMnSc7	paleontolog
Z.V.	Z.V.	k1gMnSc7	Z.V.
Špinarem	Špinar	k1gMnSc7	Špinar
<g/>
,	,	kIx,	,
četné	četný	k2eAgFnSc2d1	četná
paleoantropologické	paleoantropologický	k2eAgFnSc2d1	paleoantropologický
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
vedl	vést	k5eAaImAgMnS	vést
zoolog	zoolog	k1gMnSc1	zoolog
Vratislav	Vratislav	k1gMnSc1	Vratislav
Mazák	mazák	k1gMnSc1	mazák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
původem	původ	k1gInSc7	původ
člověka	člověk	k1gMnSc2	člověk
intenzivně	intenzivně	k6eAd1	intenzivně
zabýval	zabývat	k5eAaImAgMnS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Mazák	mazák	k1gMnSc1	mazák
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
významným	významný	k2eAgMnSc7d1	významný
katalogizátorem	katalogizátor	k1gMnSc7	katalogizátor
Burianova	Burianův	k2eAgNnSc2d1	Burianovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
značně	značně	k6eAd1	značně
opomíjen	opomíjet	k5eAaImNgMnS	opomíjet
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gInSc3	on
vytýkán	vytýkán	k2eAgInSc4d1	vytýkán
naturalismus	naturalismus	k1gInSc4	naturalismus
a	a	k8xC	a
akademismus	akademismus	k1gInSc4	akademismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
režimu	režim	k1gInSc3	režim
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
)	)	kIx)	)
nelíbila	líbit	k5eNaImAgFnS	líbit
ani	ani	k9	ani
jeho	jeho	k3xOp3gFnSc1	jeho
orientace	orientace	k1gFnSc1	orientace
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gNnSc1	jeho
aktivní	aktivní	k2eAgNnSc1d1	aktivní
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
trampingu	tramping	k1gInSc6	tramping
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
velké	velký	k2eAgFnPc4d1	velká
sympatie	sympatie	k1gFnPc4	sympatie
ke	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
woodcraft	woodcrafta	k1gFnPc2	woodcrafta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Svazu	svaz	k1gInSc2	svaz
čs	čs	kA	čs
<g/>
.	.	kIx.	.
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
výtvarníků	výtvarník	k1gMnPc2	výtvarník
Purkyně	Purkyně	k1gFnSc2	Purkyně
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
zúčastňoval	zúčastňovat	k5eAaImAgMnS	zúčastňovat
členských	členský	k2eAgFnPc2d1	členská
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
děl	dělo	k1gNnPc2	dělo
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
ilustrace	ilustrace	k1gFnPc1	ilustrace
a	a	k8xC	a
obálky	obálka	k1gFnPc1	obálka
pro	pro	k7c4	pro
knihy	kniha	k1gFnPc4	kniha
i	i	k8xC	i
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
počtu	počet	k1gInSc6	počet
je	být	k5eAaImIp3nS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
i	i	k8xC	i
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gNnPc2	jeho
"	"	kIx"	"
<g/>
pravěkého	pravěký	k2eAgNnSc2d1	pravěké
<g/>
"	"	kIx"	"
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
dnes	dnes	k6eAd1	dnes
těžko	těžko	k6eAd1	těžko
zjistitelné	zjistitelný	k2eAgNnSc1d1	zjistitelné
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
stovky	stovka	k1gFnPc4	stovka
<g/>
)	)	kIx)	)
děl	dít	k5eAaBmAgMnS	dít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
volné	volný	k2eAgFnSc2d1	volná
tvorby	tvorba	k1gFnSc2	tvorba
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
počítat	počítat	k5eAaImF	počítat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
věci	věc	k1gFnPc4	věc
-	-	kIx~	-
od	od	k7c2	od
rozměrných	rozměrný	k2eAgInPc2d1	rozměrný
olejů	olej	k1gInPc2	olej
až	až	k9	až
po	po	k7c4	po
malé	malý	k2eAgFnPc4d1	malá
kresbičky	kresbička	k1gFnPc4	kresbička
obdivovatelům	obdivovatel	k1gMnPc3	obdivovatel
do	do	k7c2	do
památníčku	památníček	k1gInSc2	památníček
či	či	k8xC	či
ty	ten	k3xDgFnPc1	ten
nejjednodušší	jednoduchý	k2eAgFnPc1d3	nejjednodušší
přípravné	přípravný	k2eAgFnPc1d1	přípravná
skici	skica	k1gFnPc1	skica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
experimentátor	experimentátor	k1gMnSc1	experimentátor
<g/>
,	,	kIx,	,
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
pouze	pouze	k6eAd1	pouze
svoji	svůj	k3xOyFgFnSc4	svůj
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
přístupné	přístupný	k2eAgNnSc1d1	přístupné
každému	každý	k3xTgMnSc3	každý
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
česká	český	k2eAgNnPc4d1	české
vydání	vydání	k1gNnPc4	vydání
kolem	kolem	k7c2	kolem
500	[number]	k4	500
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
600	[number]	k4	600
knižních	knižní	k2eAgFnPc2d1	knižní
obálek	obálka	k1gFnPc2	obálka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
,	,	kIx,	,
Rudyarda	Rudyard	k1gMnSc2	Rudyard
Kiplinga	Kipling	k1gMnSc2	Kipling
<g/>
,	,	kIx,	,
Jacka	Jacek	k1gMnSc2	Jacek
Londona	London	k1gMnSc2	London
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Maye	May	k1gMnSc2	May
<g/>
,	,	kIx,	,
Eduarda	Eduard	k1gMnSc2	Eduard
Štorcha	Štorch	k1gMnSc2	Štorch
nebo	nebo	k8xC	nebo
Julese	Julese	k1gFnSc2	Julese
Verna	Vern	k1gInSc2	Vern
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
mu	on	k3xPp3gMnSc3	on
složil	složit	k5eAaPmAgMnS	složit
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc4d1	jiný
hold	hold	k1gInSc4	hold
v	v	k7c6	v
románu	román	k1gInSc6	román
Když	když	k8xS	když
duben	duben	k1gInSc1	duben
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hoši	hoch	k1gMnPc1	hoch
přicházejí	přicházet	k5eAaImIp3nP	přicházet
pro	pro	k7c4	pro
autogram	autogram	k1gInSc4	autogram
k	k	k7c3	k
obdivovanému	obdivovaný	k2eAgMnSc3d1	obdivovaný
ilustrátorovi	ilustrátor	k1gMnSc3	ilustrátor
knih	kniha	k1gFnPc2	kniha
Florianovi	Florianovi	k1gRnPc1	Florianovi
(	(	kIx(	(
<g/>
kapitola	kapitola	k1gFnSc1	kapitola
V	v	k7c6	v
umělcově	umělcův	k2eAgFnSc6d1	umělcova
svatyni	svatyně	k1gFnSc6	svatyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
paleontologem	paleontolog	k1gMnSc7	paleontolog
Josefem	Josef	k1gMnSc7	Josef
Augustou	Augusta	k1gMnSc7	Augusta
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
paleontologických	paleontologický	k2eAgFnPc6d1	paleontologická
rekonstrukcích	rekonstrukce	k1gFnPc6	rekonstrukce
pravěkých	pravěký	k2eAgNnPc2d1	pravěké
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
prehistorických	prehistorický	k2eAgFnPc2d1	prehistorická
krajin	krajina	k1gFnPc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
mamutů	mamut	k1gMnPc2	mamut
i	i	k8xC	i
prvních	první	k4xOgInPc2	první
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgMnS	přispět
tak	tak	k9	tak
k	k	k7c3	k
popularizaci	popularizace	k1gFnSc3	popularizace
této	tento	k3xDgFnSc2	tento
vědní	vědní	k2eAgFnSc2d1	vědní
disciplíny	disciplína	k1gFnSc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
namaloval	namalovat	k5eAaPmAgMnS	namalovat
i	i	k9	i
další	další	k2eAgInSc4d1	další
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
portréty	portrét	k1gInPc1	portrét
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
obrazy	obraz	k1gInPc4	obraz
zejména	zejména	k9	zejména
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
vlastních	vlastní	k2eAgFnPc2d1	vlastní
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
ilustrátorů	ilustrátor	k1gMnPc2	ilustrátor
pravěku	pravěk	k1gInSc2	pravěk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
velkým	velký	k2eAgInSc7d1	velký
cyklem	cyklus	k1gInSc7	cyklus
je	být	k5eAaImIp3nS	být
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
soubor	soubor	k1gInSc1	soubor
pláten	plátno	k1gNnPc2	plátno
pro	pro	k7c4	pro
Východočeskou	východočeský	k2eAgFnSc4d1	Východočeská
zoologickou	zoologický	k2eAgFnSc4d1	zoologická
zahradu	zahrada	k1gFnSc4	zahrada
ve	v	k7c6	v
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
uvolněné	uvolněný	k2eAgFnPc4d1	uvolněná
práce	práce	k1gFnPc4	práce
malované	malovaný	k2eAgFnSc2d1	malovaná
pro	pro	k7c4	pro
Komenium	Komenium	k1gNnSc4	Komenium
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
volných	volný	k2eAgNnPc2d1	volné
pláten	plátno	k1gNnPc2	plátno
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
cykly	cyklus	k1gInPc1	cyklus
-	-	kIx~	-
oslava	oslava	k1gFnSc1	oslava
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
"	"	kIx"	"
<g/>
Porobení	porobení	k1gNnSc2	porobení
<g/>
"	"	kIx"	"
a	a	k8xC	a
protiválečný	protiválečný	k2eAgInSc1d1	protiválečný
cyklus	cyklus	k1gInSc1	cyklus
"	"	kIx"	"
<g/>
Obžalování	obžalování	k1gNnSc1	obžalování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
na	na	k7c6	na
Burianovo	Burianův	k2eAgNnSc4d1	Burianovo
dílo	dílo	k1gNnSc4	dílo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
paleorekonstrukce	paleorekonstrukce	k1gFnSc2	paleorekonstrukce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
navazují	navazovat	k5eAaImIp3nP	navazovat
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
ilustrátoři	ilustrátor	k1gMnPc1	ilustrátor
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Berger	Berger	k1gMnSc1	Berger
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
spolupracující	spolupracující	k2eAgMnPc1d1	spolupracující
s	s	k7c7	s
paleontology	paleontolog	k1gMnPc7	paleontolog
Josefem	Josef	k1gMnSc7	Josef
Benešem	Beneš	k1gMnSc7	Beneš
a	a	k8xC	a
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Fejfarem	Fejfar	k1gMnSc7	Fejfar
respektive	respektive	k9	respektive
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Dvorský	Dvorský	k1gMnSc1	Dvorský
a	a	k8xC	a
Libor	Libor	k1gMnSc1	Libor
Balák	Balák	k1gMnSc1	Balák
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
Antropark	Antropark	k1gInSc1	Antropark
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaImIp3nP	věnovat
především	především	k9	především
"	"	kIx"	"
<g/>
lovcům	lovec	k1gMnPc3	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
"	"	kIx"	"
moravskému	moravský	k2eAgInSc3d1	moravský
gravettienu	gravettien	k1gInSc3	gravettien
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vědci	vědec	k1gMnPc7	vědec
kolem	kolem	k7c2	kolem
pražského	pražský	k2eAgNnSc2d1	Pražské
Chlupáčova	chlupáčův	k2eAgNnSc2d1	Chlupáčovo
muzea	muzeum	k1gNnSc2	muzeum
historie	historie	k1gFnSc2	historie
Země	zem	k1gFnSc2	zem
pak	pak	k6eAd1	pak
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
dva	dva	k4xCgMnPc1	dva
významní	významný	k2eAgMnPc1d1	významný
paleoartisti	paleoartist	k1gMnPc1	paleoartist
Petr	Petr	k1gMnSc1	Petr
Modlitba	modlitba	k1gFnSc1	modlitba
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Národní	národní	k2eAgNnSc4d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
mnoho	mnoho	k4c4	mnoho
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
usídlený	usídlený	k2eAgMnSc1d1	usídlený
Jan	Jan	k1gMnSc1	Jan
Sovák	Sovák	k1gMnSc1	Sovák
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
úrovně	úroveň	k1gFnPc1	úroveň
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
i	i	k9	i
dva	dva	k4xCgMnPc1	dva
malující	malující	k2eAgMnPc1d1	malující
přírodovědci	přírodovědec	k1gMnPc1	přírodovědec
Karel	Karel	k1gMnSc1	Karel
Cettl	Cettl	k1gMnSc1	Cettl
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Říha	Říha	k1gMnSc1	Říha
<g/>
.	.	kIx.	.
</s>
<s>
Tvorbě	tvorba	k1gFnSc3	tvorba
3D	[number]	k4	3D
modelů	model	k1gInPc2	model
pravěké	pravěký	k2eAgFnSc2d1	pravěká
fauny	fauna	k1gFnSc2	fauna
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
velikosti	velikost	k1gFnSc6	velikost
se	se	k3xPyFc4	se
na	na	k7c6	na
špičkové	špičkový	k2eAgFnSc6d1	špičková
úrovni	úroveň	k1gFnSc6	úroveň
věnuje	věnovat	k5eAaImIp3nS	věnovat
Radek	Radek	k1gMnSc1	Radek
Labuťa	Labuťa	k1gMnSc1	Labuťa
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
projekt	projekt	k1gInSc4	projekt
Giganti	gigant	k1gMnPc1	gigant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
Anthropos	Anthropos	k1gInSc1	Anthropos
dosud	dosud	k6eAd1	dosud
neznámou	známý	k2eNgFnSc4d1	neznámá
studii	studie	k1gFnSc4	studie
k	k	k7c3	k
proslulému	proslulý	k2eAgInSc3d1	proslulý
obrazu	obraz	k1gInSc2	obraz
Neolitičtí	neolitický	k2eAgMnPc1d1	neolitický
zemědělci	zemědělec	k1gMnPc1	zemědělec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
