<p>
<s>
Hans-Georg	Hans-Georg	k1gMnSc1	Hans-Georg
Gadamer	Gadamer	k1gMnSc1	Gadamer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Marburg	Marburg	k1gInSc1	Marburg
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
fenomenolog	fenomenolog	k1gMnSc1	fenomenolog
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
M.	M.	kA	M.
Heideggera	Heideggera	k1gFnSc1	Heideggera
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
bádáním	bádání	k1gNnSc7	bádání
o	o	k7c6	o
antické	antický	k2eAgFnSc6d1	antická
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
,	,	kIx,	,
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
estetice	estetika	k1gFnSc6	estetika
a	a	k8xC	a
zejména	zejména	k9	zejména
o	o	k7c6	o
filosofické	filosofický	k2eAgFnSc6d1	filosofická
hermeneutice	hermeneutika	k1gFnSc6	hermeneutika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Gadamer	Gadamer	k1gMnSc1	Gadamer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Marburgu	Marburg	k1gInSc6	Marburg
v	v	k7c6	v
Hesensku	Hesensko	k1gNnSc6	Hesensko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
rektor	rektor	k1gMnSc1	rektor
tamní	tamní	k2eAgFnSc2d1	tamní
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
syna	syn	k1gMnSc4	syn
k	k	k7c3	k
přírodním	přírodní	k2eAgFnPc3d1	přírodní
vědám	věda	k1gFnPc3	věda
<g/>
,	,	kIx,	,
Gadamer	Gadamer	k1gMnSc1	Gadamer
však	však	k9	však
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
německé	německý	k2eAgFnSc6d1	německá
<g/>
)	)	kIx)	)
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Marburgu	Marburg	k1gInSc2	Marburg
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
novokantovců	novokantovec	k1gMnPc2	novokantovec
P.	P.	kA	P.
Natorpa	Natorpa	k1gFnSc1	Natorpa
a	a	k8xC	a
Nicolai	Nicolai	k1gNnSc1	Nicolai
Hartmanna	Hartmann	k1gMnSc2	Hartmann
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
promoval	promovat	k5eAaBmAgInS	promovat
prací	práce	k1gFnSc7	práce
o	o	k7c6	o
Platónovi	Platón	k1gMnSc6	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Freiburgu	Freiburg	k1gInSc2	Freiburg
k	k	k7c3	k
mladému	mladý	k1gMnSc3	mladý
M.	M.	kA	M.
Heideggerovi	Heidegger	k1gMnSc3	Heidegger
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnPc4	jeho
spolužáky	spolužák	k1gMnPc4	spolužák
byli	být	k5eAaImAgMnP	být
Leo	Leo	k1gMnSc1	Leo
Strauss	Strauss	k1gInSc4	Strauss
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Löwith	Löwith	k1gMnSc1	Löwith
a	a	k8xC	a
Hannah	Hannah	k1gMnSc1	Hannah
Arendtová	Arendtová	k1gFnSc1	Arendtová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Heideggerem	Heidegger	k1gInSc7	Heidegger
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Marburgu	Marburg	k1gInSc2	Marburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
habilitoval	habilitovat	k5eAaBmAgInS	habilitovat
prací	práce	k1gFnSc7	práce
Platónova	Platónův	k2eAgFnSc1d1	Platónova
dialektická	dialektický	k2eAgFnSc1d1	dialektická
etika	etika	k1gFnSc1	etika
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1947	[number]	k4	1939-1947
byl	být	k5eAaImAgInS	být
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
děkanem	děkan	k1gMnSc7	děkan
filosofické	filosofický	k2eAgFnSc2d1	filosofická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nacistickým	nacistický	k2eAgInSc7d1	nacistický
režimem	režim	k1gInSc7	režim
sice	sice	k8xC	sice
vycházel	vycházet	k5eAaImAgMnS	vycházet
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
však	však	k9	však
aktivním	aktivní	k2eAgMnSc7d1	aktivní
nacistou	nacista	k1gMnSc7	nacista
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
i	i	k9	i
po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
politickému	politický	k2eAgInSc3d1	politický
vývoji	vývoj	k1gInSc3	vývoj
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
sovětské	sovětský	k2eAgFnSc6d1	sovětská
zóně	zóna	k1gFnSc6	zóna
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
převzal	převzít	k5eAaPmAgMnS	převzít
katedru	katedra	k1gFnSc4	katedra
filosofie	filosofie	k1gFnSc2	filosofie
po	po	k7c4	po
Karl	Karl	k1gInSc4	Karl
Jaspersovi	Jaspersův	k2eAgMnPc1d1	Jaspersův
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
emeritování	emeritování	k1gNnSc6	emeritování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Gadamer	Gadamer	k1gMnSc1	Gadamer
napsal	napsat	k5eAaPmAgMnS	napsat
řadu	řada	k1gFnSc4	řada
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
antické	antický	k2eAgFnSc6d1	antická
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
,	,	kIx,	,
o	o	k7c6	o
filosofii	filosofie	k1gFnSc6	filosofie
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
o	o	k7c6	o
estetice	estetika	k1gFnSc6	estetika
<g/>
,	,	kIx,	,
proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
však	však	k9	však
velkou	velký	k2eAgFnSc7d1	velká
prací	práce	k1gFnSc7	práce
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
Wahrheit	Wahrheit	k1gInSc1	Wahrheit
und	und	k?	und
Methode	Method	k1gInSc5	Method
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
"	"	kIx"	"
<g/>
rozumění	rozumění	k1gNnSc1	rozumění
<g/>
"	"	kIx"	"
a	a	k8xC	a
filosofické	filosofický	k2eAgFnSc2d1	filosofická
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
naznačil	naznačit	k5eAaPmAgInS	naznačit
už	už	k9	už
Heidegger	Heidegger	k1gInSc1	Heidegger
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
Gadamer	Gadamer	k1gInSc1	Gadamer
je	být	k5eAaImIp3nS	být
však	však	k9	však
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgInS	postavit
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
proti	proti	k7c3	proti
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
chtěly	chtít	k5eAaImAgFnP	chtít
i	i	k9	i
v	v	k7c6	v
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
zavést	zavést	k5eAaPmF	zavést
přírodovědecký	přírodovědecký	k2eAgInSc4d1	přírodovědecký
způsob	způsob	k1gInSc4	způsob
argumentace	argumentace	k1gFnSc2	argumentace
a	a	k8xC	a
dokazování	dokazování	k1gNnSc2	dokazování
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
proti	proti	k7c3	proti
starší	starý	k2eAgFnSc3d2	starší
hermeneutice	hermeneutika	k1gFnSc3	hermeneutika
Friedricha	Friedrich	k1gMnSc2	Friedrich
Schleiermachera	Schleiermacher	k1gMnSc2	Schleiermacher
a	a	k8xC	a
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Diltheye	Dilthey	k1gMnSc2	Dilthey
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chápala	chápat	k5eAaImAgFnS	chápat
interpretaci	interpretace	k1gFnSc4	interpretace
jako	jako	k8xS	jako
vcítění	vcítění	k1gNnSc4	vcítění
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
myšlenkám	myšlenka	k1gFnPc3	myšlenka
původního	původní	k2eAgMnSc2d1	původní
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Gadamer	Gadamer	k1gInSc1	Gadamer
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
myšlenku	myšlenka	k1gFnSc4	myšlenka
"	"	kIx"	"
<g/>
hermeneutického	hermeneutický	k2eAgInSc2d1	hermeneutický
kruhu	kruh	k1gInSc2	kruh
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
textu	text	k1gInSc3	text
člověk	člověk	k1gMnSc1	člověk
už	už	k6eAd1	už
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
s	s	k7c7	s
jistým	jistý	k2eAgNnSc7d1	jisté
"	"	kIx"	"
<g/>
před-porozuměním	předorozumění	k1gNnSc7	před-porozumění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
četby	četba	k1gFnSc2	četba
kriticky	kriticky	k6eAd1	kriticky
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
"	"	kIx"	"
<g/>
splývání	splývání	k1gNnSc3	splývání
horizontů	horizont	k1gInPc2	horizont
<g/>
"	"	kIx"	"
a	a	k8xC	a
obzor	obzor	k1gInSc4	obzor
čtenáře	čtenář	k1gMnSc2	čtenář
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
o	o	k7c4	o
čtené	čtený	k2eAgFnPc4d1	čtená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
a	a	k8xC	a
vliv	vliv	k1gInSc1	vliv
==	==	k?	==
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
<g/>
,	,	kIx,	,
nauky	nauka	k1gFnPc4	nauka
o	o	k7c4	o
rozumění	rozumění	k1gNnSc4	rozumění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
aktuální	aktuální	k2eAgFnSc1d1	aktuální
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
globalizací	globalizace	k1gFnSc7	globalizace
a	a	k8xC	a
setkáváním	setkávání	k1gNnSc7	setkávání
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Gadamer	Gadamer	k1gMnSc1	Gadamer
tak	tak	k6eAd1	tak
měl	mít	k5eAaImAgMnS	mít
veliký	veliký	k2eAgInSc4d1	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
současných	současný	k2eAgMnPc2d1	současný
filosofů	filosof	k1gMnPc2	filosof
<g/>
,	,	kIx,	,
evropských	evropský	k2eAgMnPc2d1	evropský
i	i	k8xC	i
amerických	americký	k2eAgMnPc2d1	americký
<g/>
:	:	kIx,	:
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
žákům	žák	k1gMnPc3	žák
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
například	například	k6eAd1	například
Gianni	Gianen	k2eAgMnPc1d1	Gianen
Vattimo	Vattima	k1gFnSc5	Vattima
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Rorty	Rorta	k1gFnSc2	Rorta
<g/>
,	,	kIx,	,
Günther	Günthra	k1gFnPc2	Günthra
Figal	Figal	k1gMnSc1	Figal
nebo	nebo	k8xC	nebo
Jean	Jean	k1gMnSc1	Jean
Grondin	Grondin	k2eAgMnSc1d1	Grondin
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
úctou	úcta	k1gFnSc7	úcta
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
hovořil	hovořit	k5eAaImAgMnS	hovořit
například	například	k6eAd1	například
Paul	Paul	k1gMnSc1	Paul
Ricoeur	Ricoeur	k1gMnSc1	Ricoeur
a	a	k8xC	a
Jürgen	Jürgen	k1gInSc1	Jürgen
Habermas	Habermasa	k1gFnPc2	Habermasa
<g/>
.	.	kIx.	.
</s>
<s>
Gadamer	Gadamer	k1gMnSc1	Gadamer
dostal	dostat	k5eAaPmAgMnS	dostat
mnoho	mnoho	k4c4	mnoho
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
a	a	k8xC	a
čestných	čestný	k2eAgInPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k8xC	i
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
filosofie	filosofie	k1gFnSc2	filosofie
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
stým	stý	k4xOgFnPc3	stý
narozeninám	narozeniny	k1gFnPc3	narozeniny
se	se	k3xPyFc4	se
do	do	k7c2	do
Heidelbergu	Heidelberg	k1gInSc2	Heidelberg
sjely	sjet	k5eAaPmAgInP	sjet
stovky	stovka	k1gFnPc4	stovka
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hans-Georg	Hans-Georg	k1gMnSc1	Hans-Georg
Gadamer	Gadamer	k1gMnSc1	Gadamer
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
H.	H.	kA	H.
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Gadamer	Gadamer	k1gInSc1	Gadamer
<g/>
,	,	kIx,	,
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
H.	H.	kA	H.
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Gadamer	Gadamer	k1gInSc1	Gadamer
<g/>
,	,	kIx,	,
Problém	problém	k1gInSc1	problém
dějinného	dějinný	k2eAgNnSc2d1	dějinné
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
H.	H.	kA	H.
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Gadamer	Gadamer	k1gInSc1	Gadamer
<g/>
,	,	kIx,	,
Idea	idea	k1gFnSc1	idea
dobra	dobro	k1gNnSc2	dobro
mezi	mezi	k7c7	mezi
Platónem	Platón	k1gMnSc7	Platón
a	a	k8xC	a
Aristotelem	Aristoteles	k1gMnSc7	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
H.	H.	kA	H.
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Gadamer	Gadamer	k1gMnSc1	Gadamer
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
H.	H.	kA	H.
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Gadamer	Gadamer	k1gInSc1	Gadamer
<g/>
,	,	kIx,	,
Aktualita	aktualita	k1gFnSc1	aktualita
krásného	krásný	k2eAgNnSc2d1	krásné
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
J.	J.	kA	J.
Grondin	Grondin	k2eAgMnSc1d1	Grondin
<g/>
,	,	kIx,	,
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Pokorný	Pokorný	k1gMnSc1	Pokorný
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hermeneutika	hermeneutika	k1gFnSc1	hermeneutika
jako	jako	k8xC	jako
teorie	teorie	k1gFnSc1	teorie
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fenomenologie	fenomenologie	k1gFnSc1	fenomenologie
</s>
</p>
<p>
<s>
Hermeneutika	hermeneutika	k1gFnSc1	hermeneutika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hans-Georg	Hans-Georg	k1gInSc1	Hans-Georg
Gadamer	Gadamero	k1gNnPc2	Gadamero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Hans-Georg	Hans-Georg	k1gMnSc1	Hans-Georg
Gadamer	Gadamer	k1gMnSc1	Gadamer
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Hans-Georg	Hans-Georg	k1gMnSc1	Hans-Georg
Gadamer	Gadamer	k1gMnSc1	Gadamer
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
Gadamer	Gadamra	k1gFnPc2	Gadamra
ve	v	k7c4	v
Stanford	Stanford	k1gInSc4	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
-	-	kIx~	-
en	en	k?	en
</s>
</p>
<p>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
-	-	kIx~	-
de	de	k?	de
</s>
</p>
<p>
<s>
Gadamerova	Gadamerův	k2eAgFnSc1d1	Gadamerova
bibliografie	bibliografie	k1gFnSc1	bibliografie
-	-	kIx~	-
de	de	k?	de
</s>
</p>
