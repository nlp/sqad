<s>
Lucius	Lucius	k1gMnSc1	Lucius
Annaeus	Annaeus	k1gMnSc1	Annaeus
Seneca	Seneca	k1gMnSc1	Seneca
(	(	kIx(	(
<g/>
4	[number]	k4	4
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Cordóba	Cordóba	k1gFnSc1	Cordóba
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
–	–	k?	–
65	[number]	k4	65
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
stoický	stoický	k2eAgMnSc1d1	stoický
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Senecy	Seneca	k1gMnSc2	Seneca
staršího	starší	k1gMnSc2	starší
a	a	k8xC	a
vychovatel	vychovatel	k1gMnSc1	vychovatel
císaře	císař	k1gMnSc2	císař
Nerona	Nero	k1gMnSc2	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Hispánské	hispánský	k2eAgFnSc6d1	hispánská
Kordubě	Korduba	k1gFnSc6	Korduba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
za	za	k7c2	za
otcova	otcův	k2eAgInSc2d1	otcův
dohledu	dohled	k1gInSc2	dohled
získal	získat	k5eAaPmAgMnS	získat
rétorické	rétorický	k2eAgNnSc4d1	rétorické
a	a	k8xC	a
filosofické	filosofický	k2eAgNnSc4d1	filosofické
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
byl	být	k5eAaImAgMnS	být
deklamátor	deklamátor	k1gMnSc1	deklamátor
(	(	kIx(	(
<g/>
přednašeč	přednašeč	k1gMnSc1	přednašeč
<g/>
,	,	kIx,	,
řečník	řečník	k1gMnSc1	řečník
<g/>
)	)	kIx)	)
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
deklamací	deklamace	k1gFnPc2	deklamace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
postupně	postupně	k6eAd1	postupně
prostoupily	prostoupit	k5eAaPmAgFnP	prostoupit
celou	celý	k2eAgFnSc7d1	celá
římskou	římský	k2eAgFnSc7d1	římská
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Hlásal	hlásat	k5eAaImAgInS	hlásat
stoickou	stoický	k2eAgFnSc4d1	stoická
nauku	nauka	k1gFnSc4	nauka
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
dobro	dobro	k1gNnSc1	dobro
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
blaženosti	blaženost	k1gFnSc2	blaženost
je	být	k5eAaImIp3nS	být
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
odolávat	odolávat	k5eAaImF	odolávat
vnějším	vnější	k2eAgFnPc3d1	vnější
okolnostem	okolnost	k1gFnPc3	okolnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nemůže	moct	k5eNaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
žít	žít	k5eAaImF	žít
v	v	k7c6	v
duševním	duševní	k2eAgInSc6d1	duševní
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
pravého	pravý	k2eAgMnSc2d1	pravý
filosofa	filosof	k1gMnSc2	filosof
nemohou	moct	k5eNaImIp3nP	moct
vyvést	vyvést	k5eAaPmF	vyvést
ani	ani	k8xC	ani
rány	rána	k1gFnPc1	rána
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
začal	začít	k5eAaPmAgInS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
a	a	k8xC	a
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Caliguly	Caligula	k1gFnSc2	Caligula
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
senátorem	senátor	k1gMnSc7	senátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Claudia	Claudia	k1gFnSc1	Claudia
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
r.	r.	kA	r.
41	[number]	k4	41
vyobcován	vyobcovat	k5eAaPmNgMnS	vyobcovat
na	na	k7c4	na
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Claudiovy	Claudiův	k2eAgFnSc2d1	Claudiova
ženy	žena	k1gFnSc2	žena
Messaliny	Messalina	k1gFnSc2	Messalina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obžalován	obžalovat	k5eAaPmNgMnS	obžalovat
z	z	k7c2	z
cizoložství	cizoložství	k1gNnSc2	cizoložství
s	s	k7c7	s
Claudiovou	Claudiový	k2eAgFnSc7d1	Claudiový
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
Julií	Julie	k1gFnSc7	Julie
Livillou	Livilla	k1gFnSc7	Livilla
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
až	až	k9	až
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
49	[number]	k4	49
<g/>
)	)	kIx)	)
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Agrippiny	Agrippina	k1gFnSc2	Agrippina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vychovatelem	vychovatel	k1gMnSc7	vychovatel
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Nerona	Nero	k1gMnSc2	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
Nero	Nero	k1gMnSc1	Nero
v	v	k7c6	v
roce	rok	k1gInSc6	rok
54	[number]	k4	54
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
vládl	vládnout	k5eAaImAgMnS	vládnout
hlavně	hlavně	k9	hlavně
Seneca	Seneca	k1gMnSc1	Seneca
společně	společně	k6eAd1	společně
s	s	k7c7	s
prefektem	prefekt	k1gMnSc7	prefekt
praetoriánů	praetorián	k1gMnPc2	praetorián
Afraniem	Afranium	k1gNnSc7	Afranium
Burrem	Burr	k1gMnSc7	Burr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
57	[number]	k4	57
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
konsulem	konsul	k1gMnSc7	konsul
a	a	k8xC	a
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
správě	správa	k1gFnSc6	správa
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
císaři	císař	k1gMnSc3	císař
nepohodlným	pohodlný	k2eNgMnSc7d1	nepohodlný
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
62	[number]	k4	62
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Burrus	Burrus	k1gMnSc1	Burrus
(	(	kIx(	(
<g/>
náčelník	náčelník	k1gInSc1	náčelník
praetoriánů	praetorián	k1gMnPc2	praetorián
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaPmF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgMnS	začít
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
všem	všecek	k3xTgMnPc3	všecek
úřadům	úřada	k1gMnPc3	úřada
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
65	[number]	k4	65
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
Pisonově	Pisonův	k2eAgNnSc6d1	Pisonovo
spiknutí	spiknutí	k1gNnSc6	spiknutí
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
jeho	jeho	k3xOp3gFnSc1	jeho
reálná	reálný	k2eAgFnSc1d1	reálná
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
není	být	k5eNaImIp3nS	být
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
donucen	donutit	k5eAaPmNgInS	donutit
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
,	,	kIx,	,
podřízl	podříznout	k5eAaPmAgMnS	podříznout
si	se	k3xPyFc3	se
žíly	žíla	k1gFnPc4	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
republiku	republika	k1gFnSc4	republika
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Seneca	Seneca	k1gMnSc1	Seneca
byl	být	k5eAaImAgMnS	být
naopak	naopak	k6eAd1	naopak
zastáncem	zastánce	k1gMnSc7	zastánce
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejplodnějších	plodný	k2eAgMnPc2d3	nejplodnější
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
prózu	próza	k1gFnSc4	próza
i	i	k8xC	i
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
krátkých	krátká	k1gFnPc6	krátká
<g/>
,	,	kIx,	,
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
větách	věta	k1gFnPc6	věta
a	a	k8xC	a
styl	styl	k1gInSc4	styl
byl	být	k5eAaImAgMnS	být
trochu	trochu	k6eAd1	trochu
komediální	komediální	k2eAgMnSc1d1	komediální
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
přívržencem	přívrženec	k1gMnSc7	přívrženec
nového	nový	k2eAgInSc2d1	nový
<g/>
,	,	kIx,	,
bujarého	bujarý	k2eAgInSc2d1	bujarý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
asijského	asijský	k2eAgInSc2d1	asijský
stylu	styl	k1gInSc2	styl
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
často	často	k6eAd1	často
napadal	napadat	k5eAaImAgMnS	napadat
starší	starý	k2eAgMnPc4d2	starší
autory	autor	k1gMnPc4	autor
(	(	kIx(	(
<g/>
neměl	mít	k5eNaImAgInS	mít
rád	rád	k6eAd1	rád
attický	attický	k2eAgInSc1d1	attický
styl	styl	k1gInSc1	styl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
mluvou	mluva	k1gFnSc7	mluva
napodoboval	napodobovat	k5eAaImAgMnS	napodobovat
Ovidia	Ovidius	k1gMnSc4	Ovidius
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
filosofických	filosofický	k2eAgFnPc6d1	filosofická
úvahách	úvaha	k1gFnPc6	úvaha
vycházel	vycházet	k5eAaImAgInS	vycházet
ze	z	k7c2	z
stoicismu	stoicismus	k1gInSc2	stoicismus
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
především	především	k9	především
morální	morální	k2eAgFnSc1d1	morální
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Nesoudil	soudit	k5eNaImAgMnS	soudit
lidi	člověk	k1gMnPc4	člověk
tak	tak	k6eAd1	tak
přísně	přísně	k6eAd1	přísně
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
stoicismu	stoicismus	k1gInSc6	stoicismus
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
a	a	k8xC	a
propagoval	propagovat	k5eAaImAgMnS	propagovat
jakousi	jakýsi	k3yIgFnSc4	jakýsi
rovnost	rovnost	k1gFnSc4	rovnost
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Senecův	Senecův	k2eAgMnSc1d1	Senecův
bratr	bratr	k1gMnSc1	bratr
Gallio	Gallio	k1gMnSc1	Gallio
se	se	k3xPyFc4	se
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
zastal	zastat	k5eAaPmAgInS	zastat
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
Sk	Sk	kA	Sk
18	[number]	k4	18
<g/>
,	,	kIx,	,
12	[number]	k4	12
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pověst	pověst	k1gFnSc1	pověst
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
falešná	falešný	k2eAgFnSc1d1	falešná
korespondence	korespondence	k1gFnSc1	korespondence
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Seneca	Seneca	k1gMnSc1	Seneca
byl	být	k5eAaImAgMnS	být
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tomu	ten	k3xDgNnSc3	ten
alespoň	alespoň	k9	alespoň
nic	nic	k3yNnSc1	nic
nenasvědčuje	nasvědčovat	k5eNaImIp3nS	nasvědčovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
filosofická	filosofický	k2eAgNnPc1d1	filosofické
díla	dílo	k1gNnPc1	dílo
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
originální	originální	k2eAgFnPc1d1	originální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	s	k7c7	s
hlubokými	hluboký	k2eAgInPc7d1	hluboký
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
vyjádřenými	vyjádřený	k2eAgFnPc7d1	vyjádřená
myšlenkami	myšlenka	k1gFnPc7	myšlenka
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
znalostí	znalost	k1gFnSc7	znalost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
spíše	spíše	k9	spíše
populární	populární	k2eAgFnSc1d1	populární
než	než	k8xS	než
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
jimi	on	k3xPp3gInPc7	on
lidem	lido	k1gNnSc7	lido
spoustu	spoustu	k6eAd1	spoustu
složitých	složitý	k2eAgFnPc2d1	složitá
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vtěsnat	vtěsnat	k5eAaPmF	vtěsnat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
jasně	jasně	k6eAd1	jasně
formulované	formulovaný	k2eAgFnSc2d1	formulovaná
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
citován	citován	k2eAgInSc1d1	citován
<g/>
.	.	kIx.	.
</s>
<s>
Dialogorum	Dialogorum	k1gNnSc1	Dialogorum
libri	libri	k1gNnPc2	libri
XII	XII	kA	XII
–	–	k?	–
"	"	kIx"	"
<g/>
Dvanáct	dvanáct	k4xCc1	dvanáct
knih	kniha	k1gFnPc2	kniha
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
deseti	deset	k4xCc2	deset
filosofických	filosofický	k2eAgNnPc2d1	filosofické
pojednání	pojednání	k1gNnPc2	pojednání
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
otázkách	otázka	k1gFnPc6	otázka
praktické	praktický	k2eAgFnSc2d1	praktická
etiky	etika	k1gFnSc2	etika
a	a	k8xC	a
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
názvu	název	k1gInSc3	název
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
podobu	podoba	k1gFnSc4	podoba
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
pomyslným	pomyslný	k2eAgMnSc7d1	pomyslný
protivníkem	protivník	k1gMnSc7	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
Senekova	Senekův	k2eAgInSc2d1	Senekův
života	život	k1gInSc2	život
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgInP	seřadit
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
sbírka	sbírka	k1gFnSc1	sbírka
těchto	tento	k3xDgInPc2	tento
dialogů	dialog	k1gInPc2	dialog
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Ambrosiánském	ambrosiánský	k2eAgInSc6d1	ambrosiánský
kodexu	kodex	k1gInSc6	kodex
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
uloženém	uložený	k2eAgNnSc6d1	uložené
v	v	k7c6	v
Ambrosiánské	ambrosiánský	k2eAgFnSc6d1	ambrosiánská
knihovně	knihovna	k1gFnSc6	knihovna
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
pořadí	pořadí	k1gNnSc1	pořadí
není	být	k5eNaImIp3nS	být
chronologické	chronologický	k2eAgNnSc1d1	chronologické
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gNnSc4	jeho
odůvodnění	odůvodnění	k1gNnSc4	odůvodnění
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgFnPc1d1	případná
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgNnSc1d1	způsobené
špatným	špatný	k2eAgInSc7d1	špatný
opisem	opis	k1gInSc7	opis
autora	autor	k1gMnSc2	autor
kodexu	kodex	k1gInSc2	kodex
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
opraveny	opravit	k5eAaPmNgFnP	opravit
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
datace	datace	k1gFnSc1	datace
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
providentia	providentia	k1gFnSc1	providentia
<g/>
,	,	kIx,	,
O	o	k7c6	o
prozřetelnosti	prozřetelnost	k1gFnSc6	prozřetelnost
(	(	kIx(	(
<g/>
roky	rok	k1gInPc7	rok
64	[number]	k4	64
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
Luciliovi	Lucilius	k1gMnSc3	Lucilius
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
řešenou	řešený	k2eAgFnSc7d1	řešená
otázkou	otázka	k1gFnSc7	otázka
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
dobrým	dobrý	k2eAgInSc7d1	dobrý
lidem	lid	k1gInSc7	lid
dějí	dít	k5eAaBmIp3nP	dít
špatné	špatný	k2eAgFnPc4d1	špatná
věci	věc	k1gFnPc4	věc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobrému	dobrý	k2eAgMnSc3d1	dobrý
člověku	člověk	k1gMnSc3	člověk
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
nikdy	nikdy	k6eAd1	nikdy
přihodit	přihodit	k5eAaPmF	přihodit
nic	nic	k3yNnSc1	nic
špatného	špatný	k2eAgNnSc2d1	špatné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vše	všechen	k3xTgNnSc4	všechen
"	"	kIx"	"
<g/>
špatné	špatný	k2eAgFnPc4d1	špatná
<g/>
"	"	kIx"	"
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
a	a	k8xC	a
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
constantia	constantia	k1gFnSc1	constantia
sapientis	sapientis	k1gFnSc1	sapientis
<g/>
,	,	kIx,	,
O	o	k7c6	o
důslednosti	důslednost	k1gFnSc6	důslednost
mudrcově	mudrcův	k2eAgFnSc6d1	Mudrcova
(	(	kIx(	(
<g/>
roky	rok	k1gInPc7	rok
55	[number]	k4	55
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
Serenovi	Seren	k1gMnSc6	Seren
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
moudrý	moudrý	k2eAgMnSc1d1	moudrý
člověk	člověk	k1gMnSc1	člověk
netečný	netečný	k2eAgMnSc1d1	netečný
k	k	k7c3	k
urážkám	urážka	k1gFnPc3	urážka
a	a	k8xC	a
neutíká	utíkat	k5eNaImIp3nS	utíkat
se	se	k3xPyFc4	se
k	k	k7c3	k
hněvu	hněv	k1gInSc3	hněv
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
být	být	k5eAaImF	být
nebo	nebo	k8xC	nebo
nebýt	být	k5eNaImF	být
něčím	něčí	k3xOyIgMnPc3	něčí
zasažen	zasažen	k2eAgInSc4d1	zasažen
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
či	či	k8xC	či
nebýt	být	k5eNaImF	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
ira	ira	k?	ira
<g/>
,	,	kIx,	,
O	o	k7c6	o
hněvu	hněv	k1gInSc6	hněv
(	(	kIx(	(
<g/>
3	[number]	k4	3
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
41	[number]	k4	41
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
bratru	bratru	k9	bratru
Novatovi	Novatův	k2eAgMnPc1d1	Novatův
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
lidské	lidský	k2eAgFnPc4d1	lidská
vášně	vášeň	k1gFnPc4	vášeň
<g/>
,	,	kIx,	,
především	především	k9	především
hněv	hněv	k1gInSc1	hněv
<g/>
.	.	kIx.	.
</s>
<s>
Ad	ad	k7c4	ad
Marciam	Marciam	k1gInSc4	Marciam
de	de	k?	de
consolatione	consolation	k1gInSc5	consolation
<g/>
,	,	kIx,	,
Marcii	Marcium	k1gNnPc7	Marcium
o	o	k7c6	o
útěše	útěcha	k1gFnSc6	útěcha
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
37	[number]	k4	37
nebo	nebo	k8xC	nebo
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnovat	k5eAaPmNgNnS	věnovat
Marcii	Marcie	k1gFnSc3	Marcie
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc3	dcera
Cremutia	Cremutium	k1gNnSc2	Cremutium
Corda	Cord	k1gMnSc2	Cord
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
oplakávala	oplakávat	k5eAaImAgNnP	oplakávat
smrt	smrt	k1gFnSc4	smrt
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Metilia	Metilius	k1gMnSc2	Metilius
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
dobro	dobro	k1gNnSc1	dobro
a	a	k8xC	a
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
přijmout	přijmout	k5eAaPmF	přijmout
s	s	k7c7	s
klidem	klid	k1gInSc7	klid
jako	jako	k8xC	jako
jedinou	jediný	k2eAgFnSc4d1	jediná
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
via	via	k7c4	via
beata	beato	k1gNnPc4	beato
<g/>
,	,	kIx,	,
O	o	k7c6	o
šťastném	šťastný	k2eAgInSc6d1	šťastný
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
bratrovi	bratr	k1gMnSc3	bratr
Gallionovi	Gallion	k1gMnSc3	Gallion
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
napadá	napadat	k5eAaPmIp3nS	napadat
epikurejce	epikurejec	k1gMnSc4	epikurejec
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidská	lidský	k2eAgFnSc1d1	lidská
blaženost	blaženost	k1gFnSc1	blaženost
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
potěšení	potěšení	k1gNnSc6	potěšení
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
staví	stavit	k5eAaBmIp3nS	stavit
stoický	stoický	k2eAgInSc1d1	stoický
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidská	lidský	k2eAgFnSc1d1	lidská
blaženost	blaženost	k1gFnSc1	blaženost
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
ctnosti	ctnost	k1gFnSc6	ctnost
a	a	k8xC	a
v	v	k7c6	v
životě	život	k1gInSc6	život
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
přirozeností	přirozenost	k1gFnSc7	přirozenost
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
otio	otio	k1gNnSc1	otio
<g/>
,	,	kIx,	,
O	o	k7c6	o
kontemplativním	kontemplativní	k2eAgInSc6d1	kontemplativní
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
62	[number]	k4	62
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
Serenovi	Seren	k1gMnSc3	Seren
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
kontemplativní	kontemplativní	k2eAgInSc4d1	kontemplativní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
moudrý	moudrý	k2eAgMnSc1d1	moudrý
člověk	člověk	k1gMnSc1	člověk
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohužel	bohužel	k9	bohužel
neexistuje	existovat	k5eNaImIp3nS	existovat
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
by	by	kYmCp3nP	by
mohl	moct	k5eAaImAgMnS	moct
moudrý	moudrý	k2eAgMnSc1d1	moudrý
člověk	člověk	k1gMnSc1	člověk
jednat	jednat	k5eAaImF	jednat
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgInPc2d1	vlastní
principů	princip	k1gInPc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Dialogu	dialog	k1gInSc3	dialog
chybí	chybět	k5eAaImIp3nS	chybět
jak	jak	k6eAd1	jak
začátek	začátek	k1gInSc4	začátek
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
závěr	závěr	k1gInSc1	závěr
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
tranquillitate	tranquillitat	k1gMnSc5	tranquillitat
animi	ani	k1gFnPc7	ani
<g/>
,	,	kIx,	,
O	o	k7c6	o
klidu	klid	k1gInSc6	klid
duševním	duševní	k2eAgInSc6d1	duševní
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
Serenovi	Serenův	k2eAgMnPc5d1	Serenův
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
De	De	k?	De
otio	otio	k1gMnSc1	otio
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
breviate	breviat	k1gMnSc5	breviat
vitae	vitaus	k1gMnSc5	vitaus
<g/>
,	,	kIx,	,
O	o	k7c4	o
krátkosti	krátkost	k1gFnPc4	krátkost
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
roky	rok	k1gInPc7	rok
49	[number]	k4	49
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
Paulinovi	Paulinovi	k1gRnPc1	Paulinovi
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
Senecovou	Senecův	k2eAgFnSc7d1	Senecova
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
skutečně	skutečně	k6eAd1	skutečně
tak	tak	k6eAd1	tak
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
;	;	kIx,	;
problém	problém	k1gInSc1	problém
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gMnSc4	on
neumějí	umět	k5eNaImIp3nP	umět
žít	žít	k5eAaImF	žít
a	a	k8xC	a
plýtvají	plýtvat	k5eAaImIp3nP	plýtvat
svým	svůj	k3xOyFgInSc7	svůj
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
člověk	člověk	k1gMnSc1	člověk
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
činí	činit	k5eAaImIp3nS	činit
krátkým	krátká	k1gFnPc3	krátká
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
často	často	k6eAd1	často
"	"	kIx"	"
<g/>
nežije	žít	k5eNaImIp3nS	žít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ad	ad	k7c4	ad
Polybium	Polybium	k1gNnSc4	Polybium
de	de	k?	de
consolatione	consolation	k1gInSc5	consolation
<g/>
,	,	kIx,	,
Polybiovi	Polybiův	k2eAgMnPc1d1	Polybiův
o	o	k7c6	o
útěše	útěcha	k1gFnSc6	útěcha
(	(	kIx(	(
<g/>
roky	rok	k1gInPc7	rok
43	[number]	k4	43
<g/>
–	–	k?	–
<g/>
44	[number]	k4	44
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
Polybiovi	Polybius	k1gMnSc3	Polybius
<g/>
,	,	kIx,	,
osvobozenému	osvobozený	k2eAgMnSc3d1	osvobozený
otrokovi	otrok	k1gMnSc3	otrok
císaře	císař	k1gMnSc2	císař
Claudia	Claudia	k1gFnSc1	Claudia
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
truchlil	truchlit	k5eAaImAgInS	truchlit
pro	pro	k7c4	pro
smrt	smrt	k1gFnSc4	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
klasická	klasický	k2eAgNnPc4d1	klasické
témata	téma	k1gNnPc4	téma
antické	antický	k2eAgFnSc2d1	antická
útěšné	útěšný	k2eAgFnSc2d1	útěšná
literatury	literatura	k1gFnSc2	literatura
<g/>
:	:	kIx,	:
nevyhnutelnost	nevyhnutelnost	k1gFnSc1	nevyhnutelnost
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
zbytečnost	zbytečnost	k1gFnSc1	zbytečnost
bolesti	bolest	k1gFnSc2	bolest
a	a	k8xC	a
trápení	trápení	k1gNnSc4	trápení
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
výzva	výzva	k1gFnSc1	výzva
snášet	snášet	k5eAaImF	snášet
pevně	pevně	k6eAd1	pevně
a	a	k8xC	a
s	s	k7c7	s
klidem	klid	k1gInSc7	klid
protivenství	protivenství	k1gNnSc2	protivenství
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
spíše	spíše	k9	spíše
nekritickou	kritický	k2eNgFnSc7d1	nekritická
chválou	chvála	k1gFnSc7	chvála
Claudia	Claudia	k1gFnSc1	Claudia
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Senecovi	Seneca	k1gMnSc3	Seneca
umožnil	umožnit	k5eAaPmAgMnS	umožnit
návrat	návrat	k1gInSc4	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Ad	ad	k7c4	ad
Helviam	Helviam	k1gInSc4	Helviam
matrem	matrma	k1gFnPc2	matrma
de	de	k?	de
consolatione	consolation	k1gInSc5	consolation
<g/>
,	,	kIx,	,
Matce	matka	k1gFnSc6	matka
Helvii	Helvie	k1gFnSc6	Helvie
o	o	k7c6	o
útěše	útěcha	k1gFnSc6	útěcha
(	(	kIx(	(
<g/>
roky	rok	k1gInPc7	rok
42	[number]	k4	42
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věnováno	věnován	k2eAgNnSc1d1	věnováno
matce	matka	k1gFnSc3	matka
Helvii	Helvie	k1gFnSc3	Helvie
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
matce	matka	k1gFnSc3	matka
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
být	být	k5eAaImF	být
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
není	být	k5eNaImIp3nS	být
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pro	pro	k7c4	pro
moudrého	moudrý	k2eAgMnSc4d1	moudrý
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
vlastí	vlast	k1gFnSc7	vlast
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
šťastným	šťastný	k2eAgNnSc7d1	šťastné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mu	on	k3xPp3gMnSc3	on
nemůže	moct	k5eNaImIp3nS	moct
nikdo	nikdo	k3yNnSc1	nikdo
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
clementia	clementia	k1gFnSc1	clementia
<g/>
,	,	kIx,	,
O	o	k7c4	o
mírnosti	mírnost	k1gFnPc4	mírnost
<g/>
;	;	kIx,	;
dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
Neronovi	Neronův	k2eAgMnPc1d1	Neronův
De	De	k?	De
beneficiis	beneficiis	k1gFnPc4	beneficiis
<g/>
,	,	kIx,	,
O	o	k7c6	o
dobročinnosti	dobročinnost	k1gFnSc6	dobročinnost
<g/>
;	;	kIx,	;
sedm	sedm	k4xCc4	sedm
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
díla	dílo	k1gNnSc2	dílo
vydána	vydán	k2eAgFnSc1d1	vydána
r.	r.	kA	r.
1992	[number]	k4	1992
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
O	o	k7c6	o
dobrodiních	dobrodiní	k1gNnPc6	dobrodiní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Quaestiones	Quaestiones	k1gInSc1	Quaestiones
naturales	naturales	k1gInSc1	naturales
(	(	kIx(	(
<g/>
Otázky	otázka	k1gFnSc2	otázka
přírodní	přírodní	k2eAgFnSc2d1	přírodní
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
přírodovědeckých	přírodovědecký	k2eAgFnPc2d1	Přírodovědecká
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
příteli	přítel	k1gMnSc3	přítel
Lucilovi	Lucil	k1gMnSc3	Lucil
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
zpracovány	zpracovat	k5eAaPmNgFnP	zpracovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
řeckých	řecký	k2eAgInPc2d1	řecký
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
knihy	kniha	k1gFnPc1	kniha
se	se	k3xPyFc4	se
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
používaly	používat	k5eAaImAgFnP	používat
jako	jako	k9	jako
učebnice	učebnice	k1gFnPc1	učebnice
"	"	kIx"	"
<g/>
fyziky	fyzika	k1gFnPc1	fyzika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
filosofie	filosofie	k1gFnSc1	filosofie
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Epistulae	Epistulaat	k5eAaPmIp3nS	Epistulaat
morales	morales	k1gInSc4	morales
ad	ad	k7c4	ad
Lucilium	Lucilium	k1gNnSc4	Lucilium
<g/>
,	,	kIx,	,
známo	znám	k2eAgNnSc1d1	známo
též	též	k9	též
jako	jako	k8xC	jako
Listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
výbor	výbor	k1gInSc1	výbor
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
r.	r.	kA	r.
1969	[number]	k4	1969
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
listů	list	k1gInPc2	list
Luciliovi	Lucilius	k1gMnSc6	Lucilius
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1984	[number]	k4	1984
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Další	další	k2eAgFnSc2d1	další
listy	lista	k1gFnSc2	lista
Luciliovi	Luciliův	k2eAgMnPc1d1	Luciliův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
124	[number]	k4	124
dopisů	dopis	k1gInPc2	dopis
ve	v	k7c6	v
20	[number]	k4	20
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
psaných	psaný	k2eAgInPc6d1	psaný
Luciliovi	Luciliův	k2eAgMnPc1d1	Luciliův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určených	určený	k2eAgFnPc2d1	určená
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dopisech	dopis	k1gInPc6	dopis
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
své	svůj	k3xOyFgFnPc4	svůj
morální	morální	k2eAgFnPc4d1	morální
a	a	k8xC	a
etické	etický	k2eAgFnPc4d1	etická
zásady	zásada	k1gFnPc4	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ludus	Ludus	k1gInSc1	Ludus
de	de	k?	de
morte	morte	k5eAaPmIp2nP	morte
Claudii	Claudia	k1gFnSc4	Claudia
<g/>
,	,	kIx,	,
Proměna	proměna	k1gFnSc1	proměna
Claudia	Claudia	k1gFnSc1	Claudia
v	v	k7c4	v
tykev	tykev	k1gFnSc4	tykev
<g/>
.	.	kIx.	.
</s>
<s>
Satira	satira	k1gFnSc1	satira
líčí	líčit	k5eAaImIp3nS	líčit
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
bohové	bůh	k1gMnPc1	bůh
dělají	dělat	k5eAaImIp3nP	dělat
s	s	k7c7	s
Claudiem	Claudium	k1gNnSc7	Claudium
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Apocolocyntosis	Apocolocyntosis	k1gFnSc4	Apocolocyntosis
divi	dive	k1gFnSc4	dive
Claudii	Claudia	k1gFnSc4	Claudia
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
vypustil	vypustit	k5eAaPmAgInS	vypustit
Claudius	Claudius	k1gInSc1	Claudius
ducha	duch	k1gMnSc2	duch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
Claudia	Claudia	k1gFnSc1	Claudia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
prozaické	prozaický	k2eAgInPc1d1	prozaický
spisy	spis	k1gInPc1	spis
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
patrně	patrně	k6eAd1	patrně
nebyly	být	k5eNaImAgFnP	být
příliš	příliš	k6eAd1	příliš
významné	významný	k2eAgFnPc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
epigramů	epigram	k1gInPc2	epigram
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
zachovaly	zachovat	k5eAaPmAgInP	zachovat
i	i	k9	i
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jediná	jediné	k1gNnPc1	jediné
římská	římský	k2eAgNnPc1d1	římské
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
psána	psát	k5eAaImNgNnP	psát
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Sofokla	Sofokles	k1gMnSc2	Sofokles
<g/>
,	,	kIx,	,
Aischyla	Aischyla	k1gMnSc2	Aischyla
a	a	k8xC	a
Euripida	Euripid	k1gMnSc2	Euripid
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
o	o	k7c4	o
hry	hra	k1gFnPc4	hra
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rétorické	rétorický	k2eAgFnPc4d1	rétorická
deklamace	deklamace	k1gFnPc4	deklamace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
kusy	kus	k1gInPc4	kus
navazuje	navazovat	k5eAaImIp3nS	navazovat
španělské	španělský	k2eAgNnSc1d1	španělské
<g/>
,	,	kIx,	,
italské	italský	k2eAgNnSc1d1	italské
a	a	k8xC	a
francouzské	francouzský	k2eAgNnSc1d1	francouzské
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Hercules	Hercules	k1gInSc1	Hercules
furens	furens	k1gInSc1	furens
<g/>
,	,	kIx,	,
Šílený	šílený	k2eAgMnSc1d1	šílený
Herkules	Herkules	k1gMnSc1	Herkules
Phaedra	Phaedr	k1gMnSc2	Phaedr
Thyestes	Thyestes	k1gMnSc1	Thyestes
Oedipus	Oedipus	k1gMnSc1	Oedipus
Medea	Medea	k1gMnSc1	Medea
Troades	Troades	k1gMnSc1	Troades
<g/>
,	,	kIx,	,
Trójanky	Trójanka	k1gFnPc1	Trójanka
Agamemnon	Agamemnon	k1gInSc1	Agamemnon
Phoenissae	Phoenissa	k1gFnSc2	Phoenissa
<g/>
,	,	kIx,	,
Féničanky	Féničanka	k1gFnPc1	Féničanka
Hercules	Hercules	k1gMnSc1	Hercules
oetaeus	oetaeus	k1gMnSc1	oetaeus
<g/>
,	,	kIx,	,
Herkules	Herkules	k1gMnSc1	Herkules
na	na	k7c6	na
Oitě	Oita	k1gFnSc6	Oita
Octavia	octavia	k1gFnSc1	octavia
<g/>
,	,	kIx,	,
vylíčení	vylíčení	k1gNnSc1	vylíčení
úmrtí	úmrtí	k1gNnSc2	úmrtí
Octavie	octavia	k1gFnSc2	octavia
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Nerona	Nero	k1gMnSc2	Nero
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
Claudia	Claudia	k1gFnSc1	Claudia
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
drama	drama	k1gNnSc4	drama
však	však	k9	však
nenapsal	napsat	k5eNaPmAgMnS	napsat
Seneca	Seneca	k1gMnSc1	Seneca
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
narážky	narážka	k1gFnPc1	narážka
na	na	k7c4	na
Neronovu	Neronův	k2eAgFnSc4d1	Neronova
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
nedožil	dožít	k5eNaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
