<s>
Seneca	Seneca	k1gMnSc1	Seneca
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejplodnějších	plodný	k2eAgMnPc2d3	nejplodnější
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
prózu	próza	k1gFnSc4	próza
i	i	k8xC	i
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
