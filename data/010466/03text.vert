<p>
<s>
Thai	Thai	k6eAd1	Thai
Dai	Dai	k1gMnSc1	Dai
Van	vana	k1gFnPc2	vana
Nguyen	Nguyna	k1gFnPc2	Nguyna
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
šachista	šachista	k1gMnSc1	šachista
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
český	český	k2eAgMnSc1d1	český
šachový	šachový	k2eAgMnSc1d1	šachový
velmistr	velmistr	k1gMnSc1	velmistr
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
velmistrem	velmistr	k1gMnSc7	velmistr
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pokořil	pokořit	k5eAaPmAgMnS	pokořit
hranici	hranice	k1gFnSc4	hranice
2500	[number]	k4	2500
bodů	bod	k1gInPc2	bod
Elo	Ela	k1gFnSc5	Ela
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
žebříčku	žebříček	k1gInSc2	žebříček
Elo	Ela	k1gFnSc5	Ela
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
šestnáctých	šestnáctý	k4xOgFnPc6	šestnáctý
narozeninách	narozeniny	k1gFnPc6	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
rekordman	rekordman	k1gMnSc1	rekordman
David	David	k1gMnSc1	David
Navara	Navar	k1gMnSc2	Navar
získal	získat	k5eAaPmAgInS	získat
velmistrovský	velmistrovský	k2eAgInSc1d1	velmistrovský
titul	titul	k1gInSc1	titul
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svými	svůj	k3xOyFgInPc7	svůj
17	[number]	k4	17
<g/>
.	.	kIx.	.
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
<g/>
Hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
tým	tým	k1gInSc4	tým
ŠK	ŠK	kA	ŠK
JOLY	jola	k1gFnSc2	jola
Lysá	lysat	k5eAaImIp3nS	lysat
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
ChessGames	ChessGames	k1gInSc1	ChessGames
–	–	k?	–
Thai	Tha	k1gFnSc2	Tha
Dai	Dai	k1gMnSc1	Dai
Van	vana	k1gFnPc2	vana
Nguyen	Nguyen	k1gInSc1	Nguyen
(	(	kIx(	(
<g/>
partie	partie	k1gFnSc1	partie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
