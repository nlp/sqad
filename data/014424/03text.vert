<s>
Čtyřková	Čtyřkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Číselné	číselný	k2eAgFnPc1d1
soustavy	soustava	k1gFnPc1
</s>
<s>
hindsko-arabská	hindsko-arabský	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
západoarabskévýchodoarabskébengálskégurmuskéindickésinhálskétamilskébalijskébarmskédzongkskéjavánskékhmerskélaoskémongolskéthajské	západoarabskévýchodoarabskébengálskégurmuskéindickésinhálskétamilskébalijskébarmskédzongkskéjavánskékhmerskélaoskémongolskéthajský	k2eAgNnSc1d1
</s>
<s>
Východní	východní	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
</s>
<s>
čínskéhokkienskéjaponskékorejskévietnamské	čínskéhokkienskéjaponskékorejskévietnamský	k2eAgNnSc1d1
</s>
<s>
Abecední	abecední	k2eAgInSc1d1
</s>
<s>
abdžadskéarménskéárjabhatacyriliceGe	abdžadskéarménskéárjabhatacyriliceGe	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ez	ez	k?
(	(	kIx(
<g/>
etiopské	etiopský	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
gruzínskéřeckéhebrejskéřímské	gruzínskéřeckéhebrejskéřímský	k2eAgFnSc2d1
</s>
<s>
bývalé	bývalý	k2eAgInPc1d1
</s>
<s>
egejskéattickébabylonskébráhmíegyptskéetruskéinuitskékharóšthímayskémuiskéKipu	egejskéattickébabylonskébráhmíegyptskéetruskéinuitskékharóšthímayskémuiskéKipat	k5eAaPmIp1nS
</s>
<s>
poziční	poziční	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
podle	podle	k7c2
základu	základ	k1gInSc2
</s>
<s>
23456810111216203660	#num#	k4
</s>
<s>
nestandardní	standardní	k2eNgFnPc1d1
poziční	poziční	k2eAgFnPc1d1
číselné	číselný	k2eAgFnPc1d1
soustavy	soustava	k1gFnPc1
</s>
<s>
bijektivní	bijektivní	k2eAgFnSc1d1
numerace	numerace	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
znaménkové	znaménkový	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
(	(	kIx(
<g/>
balancovaná	balancovaný	k2eAgFnSc1d1
trojková	trojkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
faktoriálnízápornékomplexní	faktoriálnízápornékomplexní	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustavanečíselné	soustavanečíselný	k2eAgFnPc4d1
reprezentace	reprezentace	k1gFnPc4
(	(	kIx(
<g/>
φ	φ	k?
<g/>
)	)	kIx)
<g/>
smíšenéasymetrické	smíšenéasymetrický	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
</s>
<s>
Čtyřková	Čtyřkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
(	(	kIx(
<g/>
také	také	k9
kvartérní	kvartérní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
používá	používat	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
symboly	symbol	k1gInPc4
<g/>
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
ukládání	ukládání	k1gNnSc2
informací	informace	k1gFnPc2
v	v	k7c6
kvartérní	kvartérní	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
kódování	kódování	k1gNnSc1
proteinů	protein	k1gInPc2
do	do	k7c2
čtyř	čtyři	k4xCgInPc2
nukleotidů	nukleotid	k1gInPc2
adenin	adenin	k1gInSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
cytosin	cytosin	k1gMnSc1
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
guanin	guanin	k1gInSc1
(	(	kIx(
<g/>
G	G	kA
<g/>
)	)	kIx)
a	a	k8xC
uracil	uracil	k1gMnSc1
(	(	kIx(
<g/>
U	U	kA
<g/>
)	)	kIx)
v	v	k7c6
ribonukleové	ribonukleový	k2eAgFnSc6d1
kyselině	kyselina	k1gFnSc6
(	(	kIx(
<g/>
RNA	RNA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
jinak	jinak	k6eAd1
prakticky	prakticky	k6eAd1
nevyužívá	využívat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřková	Čtyřkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
pohodlně	pohodlně	k6eAd1
převádí	převádět	k5eAaImIp3nS
do	do	k7c2
dvojkové	dvojkový	k2eAgFnSc2d1
<g/>
,	,	kIx,
protože	protože	k8xS
4	#num#	k4
je	být	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
mocnina	mocnina	k1gFnSc1
čísla	číslo	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
soustavami	soustava	k1gFnPc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
1	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
4	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
5	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
6	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
7	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
8	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
9	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
10	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
11	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
12	#num#	k4
td	td	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k6eAd1
<g/>
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
13	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
14	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
14	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
15	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
16	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
17	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
18	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
boldtblcol	boldtblcol	k1gInSc1
<g/>
19	#num#	k4
td	td	k?
<g/>
:	:	kIx,
<g/>
nth-child	nth-child	k1gMnSc1
<g/>
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
Číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
(	(	kIx(
<g/>
základ	základ	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
102345678912162036	#num#	k4
</s>
<s>
1111111111111	#num#	k4
</s>
<s>
21022222222222	#num#	k4
</s>
<s>
311103333333333	#num#	k4
</s>
<s>
41001110444444444	#num#	k4
</s>
<s>
510112111055555555	#num#	k4
</s>
<s>
6110201211106666666	#num#	k4
</s>
<s>
71112113121110777777	#num#	k4
</s>
<s>
8100022201312111088888	#num#	k4
</s>
<s>
910011002114131211109999	#num#	k4
</s>
<s>
101010101222014131211AAAA	101010101222014131211AAAA	k4
</s>
<s>
10011001001020112104002442021441218464502S	10011001001020112104002442021441218464502S	k4
</s>
<s>
100011111010001101001332201300043442626175013316B43E82A0RS	100011111010001101001332201300043442626175013316B43E82A0RS	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Výukový	výukový	k2eAgInSc1d1
kurs	kurs	k1gInSc1
Číselné	číselný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Čtyřková	Čtyřkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
ve	v	k7c6
Wikiverzitě	Wikiverzita	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
