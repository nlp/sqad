<s>
Optika	optika	k1gFnSc1	optika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
optikós	optikós	k6eAd1	optikós
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
vidění	vidění	k1gNnSc4	vidění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
óps	óps	k?	óps
znamenající	znamenající	k2eAgNnSc1d1	znamenající
"	"	kIx"	"
<g/>
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
zrak	zrak	k1gInSc1	zrak
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
disciplína	disciplína	k1gFnSc1	disciplína
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
smyslu	smysl	k1gInSc6	smysl
zabývá	zabývat	k5eAaImIp3nS	zabývat
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
šířením	šíření	k1gNnSc7	šíření
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
prostředích	prostředí	k1gNnPc6	prostředí
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
rozhraních	rozhraní	k1gNnPc6	rozhraní
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
působením	působení	k1gNnSc7	působení
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
podstatu	podstata	k1gFnSc4	podstata
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
souvisejí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
částí	část	k1gFnSc7	část
spektra	spektrum	k1gNnSc2	spektrum
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
záření	záření	k1gNnSc2	záření
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
popisovat	popisovat	k5eAaImF	popisovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Optiku	optika	k1gFnSc4	optika
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
záření	záření	k1gNnSc6	záření
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
používaných	používaný	k2eAgFnPc2d1	používaná
metod	metoda	k1gFnPc2	metoda
má	mít	k5eAaImIp3nS	mít
optika	optika	k1gFnSc1	optika
řadu	řad	k1gInSc2	řad
poddisciplín	poddisciplína	k1gFnPc2	poddisciplína
<g/>
:	:	kIx,	:
geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
svazková	svazkový	k2eAgFnSc1d1	svazková
optika	optika	k1gFnSc1	optika
fotometrie	fotometrie	k1gFnSc1	fotometrie
radiometrie	radiometrie	k1gFnSc1	radiometrie
vlnová	vlnový	k2eAgFnSc1d1	vlnová
optika	optika	k1gFnSc1	optika
kvantová	kvantový	k2eAgFnSc1d1	kvantová
optika	optika	k1gFnSc1	optika
koherenční	koherenční	k2eAgFnSc1d1	koherenční
optika	optika	k1gFnSc1	optika
Podle	podle	k7c2	podle
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c4	na
část	část	k1gFnSc4	část
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
spektra	spektrum	k1gNnSc2	spektrum
lze	lze	k6eAd1	lze
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
poddisciplíny	poddisciplín	k1gInPc4	poddisciplín
<g/>
:	:	kIx,	:
klasická	klasický	k2eAgFnSc1d1	klasická
optika	optika	k1gFnSc1	optika
rádiová	rádiový	k2eAgFnSc1d1	rádiová
optika	optika	k1gFnSc1	optika
rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
optika	optika	k1gFnSc1	optika
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
optika	optika	k1gFnSc1	optika
Optika	optika	k1gFnSc1	optika
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
optoelektronika	optoelektronika	k1gFnSc1	optoelektronika
oční	oční	k2eAgFnSc1d1	oční
optika	optika	k1gFnSc1	optika
optometrie	optometrie	k1gFnSc2	optometrie
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
<g/>
.	.	kIx.	.
</s>
<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
rozměry	rozměr	k1gInPc4	rozměr
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
něž	jenž	k3xRgMnPc4	jenž
nebo	nebo	k8xC	nebo
okolo	okolo	k7c2	okolo
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
šíří	šíř	k1gFnPc2	šíř
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
povaha	povaha	k1gFnSc1	povaha
světla	světlo	k1gNnSc2	světlo
jen	jen	k9	jen
slabě	slabě	k6eAd1	slabě
rozeznatelná	rozeznatelný	k2eAgFnSc1d1	rozeznatelná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
chování	chování	k1gNnSc1	chování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
popsáno	popsat	k5eAaPmNgNnS	popsat
pomocí	pomocí	k7c2	pomocí
paprsků	paprsek	k1gInPc2	paprsek
splňujících	splňující	k2eAgInPc2d1	splňující
geometrické	geometrický	k2eAgInPc4d1	geometrický
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
geometrické	geometrický	k2eAgFnSc2d1	geometrická
optiky	optika	k1gFnSc2	optika
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
Fermatova	Fermatův	k2eAgInSc2d1	Fermatův
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Fermatův	Fermatův	k2eAgInSc1d1	Fermatův
princip	princip	k1gInSc1	princip
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
po	po	k7c6	po
takové	takový	k3xDgFnSc6	takový
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
že	že	k8xS	že
doba	doba	k1gFnSc1	doba
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
proběhnutí	proběhnutí	k1gNnSc3	proběhnutí
této	tento	k3xDgFnSc2	tento
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
extrémní	extrémní	k2eAgFnSc1d1	extrémní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynou	plynout	k5eAaImIp3nP	plynout
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
zákony	zákon	k1gInPc4	zákon
geometrické	geometrický	k2eAgFnSc2d1	geometrická
optiky	optika	k1gFnSc2	optika
<g/>
:	:	kIx,	:
V	v	k7c6	v
homogenním	homogenní	k2eAgNnSc6d1	homogenní
a	a	k8xC	a
izotropním	izotropní	k2eAgNnSc6d1	izotropní
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
šíří	šířit	k5eAaImIp3nS	šířit
přímočaře	přímočaro	k6eAd1	přímočaro
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
světelných	světelný	k2eAgInPc2d1	světelný
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Světelné	světelný	k2eAgInPc1d1	světelný
svazky	svazek	k1gInPc1	svazek
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
vzájemně	vzájemně	k6eAd1	vzájemně
na	na	k7c4	na
sobě	se	k3xPyFc3	se
nezávisle	závisle	k6eNd1	závisle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
dvou	dva	k4xCgFnPc2	dva
homogenních	homogenní	k2eAgFnPc2d1	homogenní
a	a	k8xC	a
izotropních	izotropní	k2eAgFnPc2d1	izotropní
prostředí	prostředí	k1gNnSc4	prostředí
se	se	k3xPyFc4	se
světelné	světelný	k2eAgInPc1d1	světelný
paprsky	paprsek	k1gInPc1	paprsek
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
lomu	lom	k1gInSc2	lom
(	(	kIx(	(
<g/>
také	také	k9	také
Snellův	Snellův	k2eAgInSc1d1	Snellův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
odrazu	odraz	k1gInSc2	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
umožnila	umožnit	k5eAaPmAgFnS	umožnit
konstrukci	konstrukce	k1gFnSc4	konstrukce
různých	různý	k2eAgInPc2d1	různý
optických	optický	k2eAgInPc2d1	optický
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Svazková	svazkový	k2eAgFnSc1d1	svazková
optika	optika	k1gFnSc1	optika
popisuje	popisovat	k5eAaImIp3nS	popisovat
formu	forma	k1gFnSc4	forma
šíření	šíření	k1gNnSc2	šíření
prostorově	prostorově	k6eAd1	prostorově
lokalizovaného	lokalizovaný	k2eAgNnSc2d1	lokalizované
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
šíří	šířit	k5eAaImIp3nS	šířit
prostorem	prostor	k1gInSc7	prostor
bez	bez	k7c2	bez
úhlové	úhlový	k2eAgFnSc2d1	úhlová
divergence	divergence	k1gFnSc2	divergence
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
představiteli	představitel	k1gMnPc7	představitel
těchto	tento	k3xDgFnPc2	tento
vln	vlna	k1gFnPc2	vlna
jsou	být	k5eAaImIp3nP	být
hermiteovské-gaussovské	hermiteovskéaussovský	k2eAgInPc1d1	hermiteovské-gaussovský
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
laguerreovské-gaussovské	laguerreovskéaussovský	k2eAgInPc1d1	laguerreovské-gaussovský
svazky	svazek	k1gInPc1	svazek
a	a	k8xC	a
besselovské	besselovský	k2eAgInPc1d1	besselovský
svazky	svazek	k1gInPc1	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Fotometrie	fotometrie	k1gFnSc2	fotometrie
a	a	k8xC	a
Radiometrie	radiometrie	k1gFnSc2	radiometrie
<g/>
.	.	kIx.	.
</s>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
intenzitě	intenzita	k1gFnSc6	intenzita
byly	být	k5eAaImAgFnP	být
podnětem	podnět	k1gInSc7	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
fotometrie	fotometrie	k1gFnSc2	fotometrie
<g/>
.	.	kIx.	.
</s>
<s>
Fotometrie	fotometrie	k1gFnSc1	fotometrie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
měřením	měření	k1gNnSc7	měření
energetického	energetický	k2eAgInSc2d1	energetický
obsahu	obsah	k1gInSc2	obsah
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Fotometrii	fotometrie	k1gFnSc4	fotometrie
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
radiometrie	radiometrie	k1gFnSc2	radiometrie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
optika	optika	k1gFnSc1	optika
<g/>
.	.	kIx.	.
</s>
<s>
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
optika	optika	k1gFnSc1	optika
respektuje	respektovat	k5eAaImIp3nS	respektovat
vlnový	vlnový	k2eAgInSc4d1	vlnový
charakter	charakter	k1gInSc4	charakter
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
přiblížením	přiblížení	k1gNnSc7	přiblížení
(	(	kIx(	(
<g/>
platným	platný	k2eAgInPc3d1	platný
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
optické	optický	k2eAgInPc4d1	optický
jevy	jev	k1gInPc4	jev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
popsáno	popsat	k5eAaPmNgNnS	popsat
pomocí	pomocí	k7c2	pomocí
skalární	skalární	k2eAgFnSc2d1	skalární
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
Huygensův	Huygensův	k2eAgInSc4d1	Huygensův
princip	princip	k1gInSc4	princip
<g/>
.	.	kIx.	.
</s>
<s>
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
optika	optika	k1gFnSc1	optika
studuje	studovat	k5eAaImIp3nS	studovat
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
vlnovým	vlnový	k2eAgInSc7d1	vlnový
charakterem	charakter	k1gInSc7	charakter
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
interference	interference	k1gFnSc1	interference
<g/>
,	,	kIx,	,
disperze	disperze	k1gFnSc1	disperze
a	a	k8xC	a
polarizace	polarizace	k1gFnSc1	polarizace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
optika	optika	k1gFnSc1	optika
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
optika	optika	k1gFnSc1	optika
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
světla	světlo	k1gNnSc2	světlo
existují	existovat	k5eAaImIp3nP	existovat
významné	významný	k2eAgInPc1d1	významný
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nejsou	být	k5eNaImIp3nP	být
popsatelné	popsatelný	k2eAgInPc1d1	popsatelný
klasickou	klasický	k2eAgFnSc7d1	klasická
fyzikou	fyzika	k1gFnSc7	fyzika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
přístup	přístup	k1gInSc4	přístup
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
elektrodynamiku	elektrodynamika	k1gFnSc4	elektrodynamika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
kvantový	kvantový	k2eAgInSc4d1	kvantový
charakter	charakter	k1gInSc4	charakter
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
např.	např.	kA	např.
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
optika	optika	k1gFnSc1	optika
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
Maxwellových	Maxwellův	k2eAgFnPc6d1	Maxwellova
rovnicích	rovnice	k1gFnPc6	rovnice
jako	jako	k8xS	jako
obecném	obecný	k2eAgInSc6d1	obecný
popisu	popis	k1gInSc6	popis
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
světlo	světlo	k1gNnSc1	světlo
pomocí	pomocí	k7c2	pomocí
vektorových	vektorový	k2eAgFnPc2d1	vektorová
veličin	veličina	k1gFnPc2	veličina
<g/>
;	;	kIx,	;
představuje	představovat	k5eAaImIp3nS	představovat
nejpřesnější	přesný	k2eAgFnSc4d3	nejpřesnější
teorii	teorie	k1gFnSc4	teorie
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
optika	optik	k1gMnSc2	optik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
optika	optika	k1gFnSc1	optika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
