<s>
Optika	optika	k1gFnSc1	optika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
optikós	optikós	k6eAd1	optikós
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
vidění	vidění	k1gNnSc4	vidění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
óps	óps	k?	óps
znamenající	znamenající	k2eAgNnSc1d1	znamenající
"	"	kIx"	"
<g/>
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
zrak	zrak	k1gInSc1	zrak
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
disciplína	disciplína	k1gFnSc1	disciplína
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
smyslu	smysl	k1gInSc6	smysl
zabývá	zabývat	k5eAaImIp3nS	zabývat
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
šířením	šíření	k1gNnSc7	šíření
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
prostředích	prostředí	k1gNnPc6	prostředí
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
rozhraních	rozhraní	k1gNnPc6	rozhraní
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
působením	působení	k1gNnSc7	působení
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
podstatu	podstata	k1gFnSc4	podstata
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
souvisejí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
