<s>
Příklonka	příklonka	k1gFnSc1	příklonka
(	(	kIx(	(
<g/>
enklitikon	enklitikon	k1gNnSc1	enklitikon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgInSc1d1	vlastní
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
přízvučný	přízvučný	k2eAgInSc4d1	přízvučný
celek	celek	k1gInSc4	celek
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
předcházejícím	předcházející	k2eAgNnSc7d1	předcházející
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
příklonky	příklonka	k1gFnPc1	příklonka
psát	psát	k5eAaImF	psát
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
slovem	slovo	k1gNnSc7	slovo
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
např.	např.	kA	např.
české	český	k2eAgFnSc2d1	Česká
tys	tys	k?	tys
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
přízvučné	přízvučný	k2eAgInPc4d1	přízvučný
celky	celek	k1gInPc4	celek
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
následujícími	následující	k2eAgNnPc7d1	následující
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
předklonky	předklonka	k1gFnPc1	předklonka
(	(	kIx(	(
<g/>
proklitika	proklitikon	k1gNnPc1	proklitikon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
příklonky	příklonka	k1gFnPc4	příklonka
a	a	k8xC	a
předklonky	předklonka	k1gFnPc4	předklonka
je	být	k5eAaImIp3nS	být
klitika	klitika	k1gFnSc1	klitika
(	(	kIx(	(
<g/>
j.	j.	k?	j.
č.	č.	k?	č.
klitikon	klitikon	k1gInSc1	klitikon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Český	český	k2eAgInSc4d1	český
slovosled	slovosled	k1gInSc4	slovosled
<g/>
.	.	kIx.	.
</s>
<s>
Příklonky	příklonka	k1gFnPc1	příklonka
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
větě	věta	k1gFnSc6	věta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jinak	jinak	k6eAd1	jinak
volný	volný	k2eAgInSc4d1	volný
slovosled	slovosled	k1gInSc4	slovosled
<g/>
,	,	kIx,	,
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
větnému	větný	k2eAgNnSc3d1	větné
členění	členění	k1gNnSc3	členění
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
pevné	pevný	k2eAgNnSc4d1	pevné
postavení	postavení	k1gNnSc4	postavení
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
<g/>
)	)	kIx)	)
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgFnSc4	první
pozici	pozice	k1gFnSc4	pozice
(	(	kIx(	(
<g/>
vyznačené	vyznačený	k2eAgFnPc4d1	vyznačená
kurzivou	kurziva	k1gFnSc7	kurziva
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
holý	holý	k2eAgInSc1d1	holý
větný	větný	k2eAgInSc1d1	větný
člen	člen	k1gInSc1	člen
(	(	kIx(	(
<g/>
jednotlivé	jednotlivý	k2eAgNnSc1d1	jednotlivé
slovo	slovo	k1gNnSc1	slovo
nebo	nebo	k8xC	nebo
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
Jmenuji	jmenovat	k5eAaBmIp1nS	jmenovat
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
<g/>
.	.	kIx.	.
rozvitý	rozvitý	k2eAgInSc1d1	rozvitý
větný	větný	k2eAgInSc1d1	větný
člen	člen	k1gInSc1	člen
Cestující	cestující	k1gFnSc2	cestující
bez	bez	k7c2	bez
platné	platný	k2eAgFnSc2d1	platná
jízdenky	jízdenka	k1gFnSc2	jízdenka
se	se	k3xPyFc4	se
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pokuty	pokuta	k1gFnSc2	pokuta
<g/>
.	.	kIx.	.
větný	větný	k2eAgInSc1d1	větný
člen	člen	k1gInSc1	člen
rozvitý	rozvitý	k2eAgInSc1d1	rozvitý
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
větou	věta	k1gFnSc7	věta
přívlastkovou	přívlastkový	k2eAgFnSc4d1	přívlastková
Cestující	cestující	k1gFnSc4	cestující
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
platnou	platný	k2eAgFnSc4d1	platná
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pokuty	pokuta	k1gFnSc2	pokuta
<g/>
.	.	kIx.	.
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
věta	věta	k1gFnSc1	věta
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
možné	možný	k2eAgNnSc1d1	možné
nahradit	nahradit	k5eAaPmF	nahradit
ji	on	k3xPp3gFnSc4	on
zájmenem	zájmeno	k1gNnSc7	zájmeno
to	ten	k3xDgNnSc1	ten
<g/>
)	)	kIx)	)
Kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezjistilo	zjistit	k5eNaPmAgNnS	zjistit
<g/>
.	.	kIx.	.
spojka	spojka	k1gFnSc1	spojka
nebo	nebo	k8xC	nebo
spojovací	spojovací	k2eAgInSc1d1	spojovací
výraz	výraz	k1gInSc1	výraz
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
a	a	k8xC	a
<g/>
,	,	kIx,	,
i	i	k8xC	i
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neukázal	ukázat	k5eNaPmAgMnS	ukázat
<g/>
.	.	kIx.	.
</s>
<s>
Sejde	sejít	k5eAaPmIp3nS	sejít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
větě	věta	k1gFnSc6	věta
více	hodně	k6eAd2	hodně
příklonek	příklonka	k1gFnPc2	příklonka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
pořadí	pořadí	k1gNnSc2	pořadí
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
spojka	spojka	k1gFnSc1	spojka
-li	i	k?	-li
pomocné	pomocný	k2eAgNnSc4d1	pomocné
sloveso	sloveso	k1gNnSc4	sloveso
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
–	–	k?	–
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
jsi	být	k5eAaImIp2nS	být
(	(	kIx(	(
<g/>
-s	-s	k?	-s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
–	–	k?	–
a	a	k8xC	a
v	v	k7c6	v
podmiňovacím	podmiňovací	k2eAgInSc6d1	podmiňovací
způsobu	způsob	k1gInSc6	způsob
–	–	k?	–
bych	by	kYmCp1nS	by
<g/>
,	,	kIx,	,
bys	by	kYmCp2nS	by
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
<g/>
,	,	kIx,	,
bychom	by	kYmCp1nP	by
<g/>
,	,	kIx,	,
byste	by	kYmCp2nP	by
krátké	krátký	k2eAgInPc1d1	krátký
tvary	tvar	k1gInPc1	tvar
zvratných	zvratný	k2eAgNnPc2d1	zvratné
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
–	–	k?	–
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
krátké	krátký	k2eAgInPc1d1	krátký
tvary	tvar	k1gInPc1	tvar
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
v	v	k7c6	v
dativu	dativ	k1gInSc6	dativ
–	–	k?	–
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
krátké	krátký	k2eAgInPc1d1	krátký
tvary	tvar	k1gInPc1	tvar
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
v	v	k7c6	v
akuzativu	akuzativ	k1gInSc6	akuzativ
–	–	k?	–
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
tě	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
ho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
Příklady	příklad	k1gInPc4	příklad
<g/>
:	:	kIx,	:
Prohlížel	prohlížet	k5eAaImAgMnS	prohlížet
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
prohlížel	prohlížet	k5eAaImAgMnS	prohlížet
<g/>
.	.	kIx.	.
</s>
<s>
Budeš	být	k5eAaImBp2nS	být
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
učit	učit	k5eAaImF	učit
...	...	k?	...
Řekls	Řekls	k1gInSc1	Řekls
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
<g/>
?	?	kIx.	?
</s>
<s>
Tys	Tys	k?	Tys
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
řekl	říct	k5eAaPmAgMnS	říct
<g/>
?	?	kIx.	?
</s>
<s>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
příklonky	příklonka	k1gFnPc4	příklonka
stálé	stálý	k2eAgFnPc4d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
příklonky	příklonka	k1gFnPc1	příklonka
nestálé	stálý	k2eNgFnPc1d1	nestálá
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
buď	buď	k8xC	buď
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
přízvuk	přízvuk	k1gInSc4	přízvuk
a	a	k8xC	a
chovat	chovat	k5eAaImF	chovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jiný	jiný	k2eAgMnSc1d1	jiný
větný	větný	k2eAgMnSc1d1	větný
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
přízvuku	přízvuk	k1gInSc2	přízvuk
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Vitamin	vitamin	k1gInSc1	vitamin
D	D	kA	D
podporuje	podporovat	k5eAaImIp3nS	podporovat
využití	využití	k1gNnSc1	využití
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
před	před	k7c7	před
osteoporózou	osteoporóza	k1gFnSc7	osteoporóza
<g/>
.	.	kIx.	.
–	–	k?	–
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Vitamin	vitamin	k1gInSc1	vitamin
D	D	kA	D
podporuje	podporovat	k5eAaImIp3nS	podporovat
využití	využití	k1gNnSc1	využití
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ˈ	ˈ	k?	ˈ
tak	tak	k6eAd1	tak
před	před	k7c7	před
osteoporózou	osteoporóza	k1gFnSc7	osteoporóza
<g/>
.	.	kIx.	.
–	–	k?	–
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
pod	pod	k7c7	pod
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
přízvučný	přízvučný	k2eAgInSc4d1	přízvučný
celek	celek	k1gInSc4	celek
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
chrání	chránit	k5eAaImIp3nS	chránit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pravopisná	pravopisný	k2eAgFnSc1d1	pravopisná
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Umístění	umístění	k1gNnSc1	umístění
slova	slovo	k1gNnSc2	slovo
tak	tak	k9	tak
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
důsledkový	důsledkový	k2eAgInSc4d1	důsledkový
poměr	poměr	k1gInSc4	poměr
mezi	mezi	k7c7	mezi
větami	věta	k1gFnPc7	věta
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
před	před	k7c7	před
spojkou	spojka	k1gFnSc7	spojka
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
čárka	čárka	k1gFnSc1	čárka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
