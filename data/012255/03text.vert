<p>
<s>
Ostralka	ostralka	k1gFnSc1	ostralka
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
acuta	acut	k1gInSc2	acut
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
plovavá	plovavý	k2eAgFnSc1d1	plovavá
kachna	kachna	k1gFnSc1	kachna
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
vrubozobých	vrubozobí	k1gMnPc2	vrubozobí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
<g/>
:	:	kIx,	:
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
část	část	k1gFnSc1	část
krku	krk	k1gInSc2	krk
jsou	být	k5eAaImIp3nP	být
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc1	hruď
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
krku	krk	k1gInSc2	krk
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
úzkým	úzký	k2eAgInSc7d1	úzký
proužkem	proužek	k1gInSc7	proužek
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
bílé	bílý	k2eAgNnSc4d1	bílé
zbarvení	zbarvení	k1gNnSc4	zbarvení
dozadu	dozadu	k6eAd1	dozadu
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
boky	boka	k1gFnPc4	boka
a	a	k8xC	a
hřbet	hřbet	k1gInSc4	hřbet
jsou	být	k5eAaImIp3nP	být
šedavé	šedavý	k2eAgInPc1d1	šedavý
<g/>
,	,	kIx,	,
černá	černý	k2eAgNnPc1d1	černé
střední	střední	k2eAgNnPc1d1	střední
ocasní	ocasní	k2eAgNnPc1d1	ocasní
pera	pero	k1gNnPc1	pero
výrazně	výrazně	k6eAd1	výrazně
prodloužená	prodloužený	k2eAgNnPc1d1	prodloužené
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
hnědavá	hnědavý	k2eAgFnSc1d1	hnědavá
<g/>
;	;	kIx,	;
od	od	k7c2	od
samice	samice	k1gFnSc2	samice
kachny	kachna	k1gFnSc2	kachna
divoké	divoký	k2eAgInPc1d1	divoký
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
jednobarevnou	jednobarevný	k2eAgFnSc7d1	jednobarevná
hlavou	hlava	k1gFnSc7	hlava
bez	bez	k7c2	bez
proužků	proužek	k1gInPc2	proužek
a	a	k8xC	a
tmavošedým	tmavošedý	k2eAgInSc7d1	tmavošedý
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letu	let	k1gInSc6	let
má	mít	k5eAaImIp3nS	mít
černozelené	černozelený	k2eAgNnSc1d1	černozelené
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
lemované	lemovaný	k2eAgNnSc4d1	lemované
zrcátko	zrcátko	k1gNnSc4	zrcátko
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
jezerech	jezero	k1gNnPc6	jezero
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
stepích	step	k1gFnPc6	step
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
a	a	k8xC	a
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
protahuje	protahovat	k5eAaImIp3nS	protahovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
i	i	k9	i
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdila	hnízdit	k5eAaImAgFnS	hnízdit
ostralka	ostralka	k1gFnSc1	ostralka
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
a	a	k8xC	a
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
oblastí	oblast	k1gFnSc7	oblast
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
hnízdění	hnízdění	k1gNnSc2	hnízdění
bylo	být	k5eAaImAgNnS	být
dolní	dolní	k2eAgNnSc4d1	dolní
Podyjí	Podyjí	k1gNnSc4	Podyjí
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
do	do	k7c2	do
výstavby	výstavba	k1gFnSc2	výstavba
Novomlýnských	novomlýnský	k2eAgFnPc2d1	Novomlýnská
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
hnízdění	hnízdění	k1gNnSc1	hnízdění
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
u	u	k7c2	u
Oder	Odra	k1gFnPc2	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
lze	lze	k6eAd1	lze
Ostralku	ostralka	k1gFnSc4	ostralka
štíhlou	štíhlý	k2eAgFnSc4d1	štíhlá
vidět	vidět	k5eAaImF	vidět
nanejvýš	nanejvýš	k6eAd1	nanejvýš
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ostralka	ostralka	k1gFnSc1	ostralka
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ostralka	ostralka	k1gFnSc1	ostralka
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Anas	Anasa	k1gFnPc2	Anasa
acuta	acut	k1gInSc2	acut
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
