<p>
<s>
Koliha	koliha	k1gFnSc1	koliha
velká	velká	k1gFnSc1	velká
(	(	kIx(	(
<g/>
Numenius	Numenius	k1gInSc1	Numenius
arquata	arquata	k1gFnSc1	arquata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
bahňáka	bahňák	k1gMnSc2	bahňák
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slukovitých	slukovitý	k2eAgFnPc2d1	slukovitý
(	(	kIx(	(
<g/>
Scolopacidae	Scolopacidae	k1gFnPc2	Scolopacidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
bahňáka	bahňák	k1gMnSc2	bahňák
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgInSc2d1	celý
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
48	[number]	k4	48
<g/>
–	–	k?	–
<g/>
57	[number]	k4	57
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
89	[number]	k4	89
<g/>
–	–	k?	–
<g/>
106	[number]	k4	106
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
končetinami	končetina	k1gFnPc7	končetina
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
<g/>
,	,	kIx,	,
dolů	dolů	k6eAd1	dolů
zahnutým	zahnutý	k2eAgInSc7d1	zahnutý
zobákem	zobák	k1gInSc7	zobák
(	(	kIx(	(
<g/>
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
delším	dlouhý	k2eAgNnSc7d2	delší
než	než	k8xS	než
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opeření	opeření	k1gNnSc1	opeření
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
těla	tělo	k1gNnSc2	tělo
šedohnědě	šedohnědě	k6eAd1	šedohnědě
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
nebo	nebo	k8xC	nebo
pruhované	pruhovaný	k2eAgFnPc1d1	pruhovaná
<g/>
,	,	kIx,	,
na	na	k7c6	na
kostřeci	kostřeec	k1gInSc6	kostřeec
a	a	k8xC	a
spodních	spodní	k2eAgFnPc6d1	spodní
ocasních	ocasní	k2eAgFnPc6d1	ocasní
krovkách	krovka	k1gFnPc6	krovka
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
straně	strana	k1gFnSc6	strana
křídla	křídlo	k1gNnSc2	křídlo
černé	černá	k1gFnSc2	černá
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
podélné	podélný	k2eAgNnSc4d1	podélné
a	a	k8xC	a
jemnější	jemný	k2eAgNnSc4d2	jemnější
skvrnění	skvrnění	k1gNnSc4	skvrnění
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
tažná	tažný	k2eAgFnSc1d1	tažná
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
na	na	k7c6	na
Sundách	Sundách	k?	Sundách
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
tvrzení	tvrzení	k1gNnPc2	tvrzení
výjimečně	výjimečně	k6eAd1	výjimečně
zaletuje	zaletovat	k5eAaImIp3nS	zaletovat
i	i	k9	i
do	do	k7c2	do
Nového	Nového	k2eAgNnSc2d1	Nového
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
na	na	k7c4	na
Mariany	Marian	k1gMnPc4	Marian
<g/>
.	.	kIx.	.
<g/>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
mokrých	mokrý	k2eAgFnPc6d1	mokrá
loukách	louka	k1gFnPc6	louka
a	a	k8xC	a
pastvinách	pastvina	k1gFnPc6	pastvina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
bažinatých	bažinatý	k2eAgFnPc6d1	bažinatá
tajgách	tajga	k1gFnPc6	tajga
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tahu	tah	k1gInSc2	tah
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
bahnitých	bahnitý	k2eAgFnPc6d1	bahnitá
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jen	jen	k9	jen
v	v	k7c6	v
jihozápadních	jihozápadní	k2eAgFnPc6d1	jihozápadní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
do	do	k7c2	do
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
početnost	početnost	k1gFnSc1	početnost
silně	silně	k6eAd1	silně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
likvidace	likvidace	k1gFnSc1	likvidace
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
přes	přes	k7c4	přes
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
také	také	k9	také
protahuje	protahovat	k5eAaImIp3nS	protahovat
a	a	k8xC	a
zřídka	zřídka	k6eAd1	zřídka
jednotlivě	jednotlivě	k6eAd1	jednotlivě
zde	zde	k6eAd1	zde
i	i	k9	i
zimuje	zimovat	k5eAaImIp3nS	zimovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněná	chráněný	k2eAgFnSc1d1	chráněná
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
kroužkovci	kroužkovec	k1gMnPc1	kroužkovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
korýše	korýš	k1gMnPc4	korýš
<g/>
,	,	kIx,	,
měkkýše	měkkýš	k1gMnPc4	měkkýš
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc4d1	malá
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
obojživelníky	obojživelník	k1gMnPc4	obojživelník
<g/>
,	,	kIx,	,
bobule	bobule	k1gFnPc4	bobule
a	a	k8xC	a
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
skryté	skrytý	k2eAgFnSc6d1	skrytá
v	v	k7c6	v
trávě	tráva	k1gFnSc6	tráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
bývají	bývat	k5eAaImIp3nP	bývat
4	[number]	k4	4
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
67,9	[number]	k4	67,9
×	×	k?	×
47,4	[number]	k4	47,4
mm	mm	kA	mm
velká	velký	k2eAgNnPc4d1	velké
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koliha	koliha	k1gFnSc1	koliha
velká	velký	k2eAgFnSc1d1	velká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
koliha	koliha	k1gFnSc1	koliha
velká	velká	k1gFnSc1	velká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
koliha	koliha	k1gFnSc1	koliha
velká	velká	k1gFnSc1	velká
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Numenius	Numenius	k1gInSc1	Numenius
arquata	arquat	k1gMnSc2	arquat
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Numenius	Numenius	k1gInSc1	Numenius
arquata	arquata	k1gFnSc1	arquata
(	(	kIx(	(
<g/>
koliha	koliha	k1gFnSc1	koliha
velká	velký	k2eAgFnSc1d1	velká
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Eurasian	Eurasian	k1gMnSc1	Eurasian
Curlew	Curlew	k1gMnSc1	Curlew
videos	videos	k1gMnSc1	videos
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Internet	Internet	k1gInSc1	Internet
Bird	Bird	k1gMnSc1	Bird
Collection	Collection	k1gInSc1	Collection
</s>
</p>
