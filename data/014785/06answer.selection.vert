<s>
Sládek	Sládek	k1gMnSc1
prestonský	prestonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
originále	originál	k1gInSc6
Le	Le	k1gMnSc1
Brasseur	Brasseur	k1gMnSc1
de	de	k?
Preston	Preston	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
komická	komický	k2eAgFnSc1d1
opera	opera	k1gFnSc1
(	(	kIx(
<g/>
opéra	opéra	k1gFnSc1
comique	comiqu	k1gFnSc2
<g/>
)	)	kIx)
o	o	k7c6
třech	tři	k4xCgNnPc6
jednáních	jednání	k1gNnPc6
francouzského	francouzský	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Adolpha-Charlese	Adolpha-Charlese	k1gFnSc2
Adama	Adam	k1gMnSc2
na	na	k7c4
libreto	libreto	k1gNnSc4
Adolpha	Adolpha	k1gFnSc1
de	de	k?
Leuvena	Leuven	k2eAgFnSc1d1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
hraběte	hrabě	k1gMnSc2
Adolpha	Adolph	k1gMnSc2
Ribbinga	Ribbing	k1gMnSc2
<g/>
,	,	kIx,
1800	#num#	k4
<g/>
-	-	kIx~
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Léona-Lévyho	Léona-Lévy	k1gMnSc2
Brunswicka	Brunswicka	k1gFnSc1
(	(	kIx(
<g/>
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Léon	Léono	k1gNnPc2
Lhérie	Lhérie	k1gFnSc2
<g/>
,	,	kIx,
1805	#num#	k4
<g/>
-	-	kIx~
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1838	#num#	k4
<g/>
.	.	kIx.
</s>