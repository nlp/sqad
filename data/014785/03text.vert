<s>
Sládek	Sládek	k1gMnSc1
prestonský	prestonský	k2eAgMnSc1d1
</s>
<s>
Sládek	Sládek	k1gMnSc1
prestonskýLe	prestonskýLe	k1gNnSc2
Brasseur	Brasseur	k1gMnSc1
de	de	k?
PrestonZákladní	PrestonZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Žánr	žánr	k1gInSc1
</s>
<s>
opéra	opéra	k1gMnSc1
comique	comique	k1gNnSc2
Skladatel	skladatel	k1gMnSc1
</s>
<s>
Adolphe-Charles	Adolphe-Charles	k1gMnSc1
Adam	Adam	k1gMnSc1
Libretista	libretista	k1gMnSc1
</s>
<s>
Adolphe	Adolphe	k1gFnSc1
de	de	k?
Leuven	Leuvna	k1gFnPc2
a	a	k8xC
Léon-Lévy	Léon-Léva	k1gFnSc2
Brunswick	Brunswicka	k1gFnPc2
Počet	počet	k1gInSc1
dějství	dějství	k1gNnSc6
</s>
<s>
3	#num#	k4
Originální	originální	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
francouzština	francouzština	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1838	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Théâtre	Théâtr	k1gInSc5
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Opéra-Comique	Opéra-Comiqu	k1gMnSc2
Česká	český	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1839	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Stavovské	stavovský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sládek	Sládek	k1gMnSc1
prestonský	prestonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
originále	originál	k1gInSc6
Le	Le	k1gMnSc1
Brasseur	Brasseur	k1gMnSc1
de	de	k?
Preston	Preston	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
komická	komický	k2eAgFnSc1d1
opera	opera	k1gFnSc1
(	(	kIx(
<g/>
opéra	opéra	k1gFnSc1
comique	comiqu	k1gFnSc2
<g/>
)	)	kIx)
o	o	k7c6
třech	tři	k4xCgNnPc6
jednáních	jednání	k1gNnPc6
francouzského	francouzský	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Adolpha-Charlese	Adolpha-Charlese	k1gFnSc2
Adama	Adam	k1gMnSc2
na	na	k7c4
libreto	libreto	k1gNnSc4
Adolpha	Adolpha	k1gFnSc1
de	de	k?
Leuvena	Leuven	k2eAgFnSc1d1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
hraběte	hrabě	k1gMnSc2
Adolpha	Adolph	k1gMnSc2
Ribbinga	Ribbing	k1gMnSc2
<g/>
,	,	kIx,
1800	#num#	k4
<g/>
-	-	kIx~
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Léona-Lévyho	Léona-Lévy	k1gMnSc2
Brunswicka	Brunswicka	k1gFnSc1
(	(	kIx(
<g/>
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Léon	Léono	k1gNnPc2
Lhérie	Lhérie	k1gFnSc2
<g/>
,	,	kIx,
1805	#num#	k4
<g/>
-	-	kIx~
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1838	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgNnP
uvedena	uvést	k5eAaPmNgNnP
v	v	k7c6
pařížském	pařížský	k2eAgInSc6d1
Théâtre	Théâtr	k1gInSc5
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Opéra-Comique	Opéra-Comique	k1gInSc1
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1838	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
díla	dílo	k1gNnSc2
</s>
<s>
"	"	kIx"
<g/>
Sládek	Sládek	k1gMnSc1
prestonský	prestonský	k2eAgMnSc1d1
<g/>
"	"	kIx"
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
konci	konec	k1gInSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
Adamovy	Adamův	k2eAgFnSc2d1
největší	veliký	k2eAgFnSc2d3
popularity	popularita	k1gFnSc2
a	a	k8xC
úspěchů	úspěch	k1gInPc2
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
Chatě	chata	k1gFnSc6
v	v	k7c6
Alpách	Alpy	k1gFnPc6
a	a	k8xC
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
Postilionovi	Postilion	k1gMnSc6
z	z	k7c2
Lonjumeau	Lonjumeaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
opera	opera	k1gFnSc1
se	s	k7c7
situační	situační	k2eAgFnSc7d1
komikou	komika	k1gFnSc7
a	a	k8xC
rázovitými	rázovitý	k2eAgFnPc7d1
charaktery	charakter	k1gInPc1
si	se	k3xPyFc3
ihned	ihned	k6eAd1
získala	získat	k5eAaPmAgFnS
oblibu	obliba	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
po	po	k7c4
zbytek	zbytek	k1gInSc4
století	století	k1gNnSc2
udržela	udržet	k5eAaPmAgFnS
zejména	zejména	k9
v	v	k7c6
té	ten	k3xDgFnSc6
části	část	k1gFnSc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
oceňovala	oceňovat	k5eAaImAgFnS
profesi	profese	k1gFnSc4
titulního	titulní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1839	#num#	k4
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgFnS
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
,	,	kIx,
Amsterdamu	Amsterdam	k1gInSc6
<g/>
,	,	kIx,
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
Hamburku	Hamburk	k1gInSc6
a	a	k8xC
Rize	Riga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
měl	mít	k5eAaImAgMnS
Sládek	Sládek	k1gMnSc1
velmi	velmi	k6eAd1
úspěšnou	úspěšný	k2eAgFnSc4d1
premiéru	premiéra	k1gFnSc4
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1839	#num#	k4
v	v	k7c6
německém	německý	k2eAgInSc6d1
překladu	překlad	k1gInSc6
(	(	kIx(
<g/>
Der	drát	k5eAaImRp2nS
Brauer	Brauer	k1gInSc4
von	von	k1gInSc1
Preston	Preston	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgInS
též	též	k9
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevil	objevit	k5eAaPmAgInS
až	až	k9
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
na	na	k7c6
jevišti	jeviště	k1gNnSc6
Prozatímního	prozatímní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
pořídil	pořídit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Juda	judo	k1gNnSc2
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Daniela	Daniela	k1gFnSc1
tehdy	tehdy	k6eAd1
zpíval	zpívat	k5eAaImAgMnS
Arnošt	Arnošt	k1gMnSc1
Grund	Grund	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
dvacátém	dvacátý	k4xOgNnSc6
století	století	k1gNnSc6
z	z	k7c2
jevišť	jeviště	k1gNnPc2
prakticky	prakticky	k6eAd1
zmizel	zmizet	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hudebních	hudební	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
si	se	k3xPyFc3
v	v	k7c6
Německu	Německo	k1gNnSc6
oblibu	obliba	k1gFnSc4
uchoval	uchovat	k5eAaPmAgInS
pochod	pochod	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
z	z	k7c2
motivů	motiv	k1gInPc2
z	z	k7c2
opery	opera	k1gFnSc2
roku	rok	k1gInSc2
1843	#num#	k4
sestavil	sestavit	k5eAaPmAgMnS
vojenský	vojenský	k2eAgMnSc1d1
kapelník	kapelník	k1gMnSc1
Karl	Karl	k1gMnSc1
Zulehner	Zulehner	k1gMnSc1
(	(	kIx(
<g/>
1805	#num#	k4
<g/>
-	-	kIx~
<g/>
1847	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
zakládající	zakládající	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Mohučského	mohučský	k2eAgInSc2d1
karnevalového	karnevalový	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
názvem	název	k1gInSc7
Narrhallamarsch	Narrhallamarsch	k1gInSc1
(	(	kIx(
<g/>
slovní	slovní	k2eAgFnSc1d1
hříčka	hříčka	k1gFnSc1
z	z	k7c2
Narr	Narra	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
blázen	blázen	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
Valhalla	Valhalla	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
oblíbeným	oblíbený	k2eAgInSc7d1
karnevalovým	karnevalový	k2eAgInSc7d1
pochodem	pochod	k1gInSc7
<g/>
,	,	kIx,
zejména	zejména	k9
pro	pro	k7c4
rodnou	rodný	k2eAgFnSc4d1
Mohuč	Mohuč	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Opera	opera	k1gFnSc1
Sládek	Sládek	k1gMnSc1
prestonský	prestonský	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
předehru	předehra	k1gFnSc4
a	a	k8xC
zpěvní	zpěvní	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
proložená	proložený	k2eAgNnPc4d1
mluvenými	mluvený	k2eAgInPc7d1
dialogy	dialog	k1gInPc7
ve	v	k7c6
třech	tři	k4xCgNnPc6
dějstvích	dějství	k1gNnPc6
<g/>
,	,	kIx,
představení	představení	k1gNnSc1
trvá	trvat	k5eAaImIp3nS
asi	asi	k9
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Osoby	osoba	k1gFnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Robinson	Robinson	k1gMnSc1
<g/>
,	,	kIx,
sládek	sládek	k1gMnSc1
v	v	k7c6
Prestonu	Preston	k1gInSc6
(	(	kIx(
<g/>
tenor	tenor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Effie	Effie	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
nevěsta	nevěsta	k1gFnSc1
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
George	George	k6eAd1
Robinson	Robinson	k1gMnSc1
<g/>
,	,	kIx,
Danielovo	Danielův	k2eAgNnSc1d1
dvojče	dvojče	k1gNnSc1
<g/>
,	,	kIx,
poručík	poručík	k1gMnSc1
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
baryton	baryton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Toby	Toba	k1gFnPc1
<g/>
,	,	kIx,
seržant	seržant	k1gMnSc1
(	(	kIx(
<g/>
bas	bas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lord	lord	k1gMnSc1
Mulgrave	Mulgrav	k1gInSc5
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
pobočník	pobočník	k1gMnSc1
krále	král	k1gMnSc2
(	(	kIx(
<g/>
baryton	baryton	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Lovely	Lovela	k1gFnPc1
<g/>
,	,	kIx,
pobočník	pobočník	k1gMnSc1
velícího	velící	k2eAgMnSc2d1
generála	generál	k1gMnSc2
(	(	kIx(
<g/>
tenor	tenor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sir	sir	k1gMnSc1
Oliver	Olivra	k1gFnPc2
Jenkins	Jenkins	k1gInSc1
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
(	(	kIx(
<g/>
bas	bas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Bob	Bob	k1gMnSc1
<g/>
,	,	kIx,
sládek	sládek	k1gMnSc1
(	(	kIx(
<g/>
tenor	tenor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Jenkinsová	Jenkinsová	k1gFnSc1
(	(	kIx(
<g/>
němé	němý	k2eAgFnSc2d1
role	role	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dvořané	dvořan	k1gMnPc1
<g/>
,	,	kIx,
důstojníci	důstojník	k1gMnPc1
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
strážní	strážní	k1gMnPc1
<g/>
,	,	kIx,
sládkové	sládek	k1gMnPc1
<g/>
,	,	kIx,
lid	lid	k1gInSc1
</s>
<s>
Děj	děj	k1gInSc1
opery	opera	k1gFnSc2
</s>
<s>
Děj	děj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
Anglii	Anglie	k1gFnSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1745	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
(	(	kIx(
<g/>
Preston	Preston	k1gInSc1
<g/>
,	,	kIx,
dvůr	dvůr	k1gInSc1
pivovaru	pivovar	k1gInSc2
<g/>
)	)	kIx)
V	v	k7c6
pivovaru	pivovar	k1gInSc6
začíná	začínat	k5eAaImIp3nS
pracovní	pracovní	k2eAgInSc1d1
den	den	k1gInSc1
(	(	kIx(
<g/>
sbor	sbor	k1gInSc1
Allons	Allonsa	k1gFnPc2
<g/>
,	,	kIx,
bon	bona	k1gFnPc2
courage	courag	k1gInSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
Sládek	Sládek	k1gMnSc1
Bob	Bob	k1gMnSc1
zpívá	zpívat	k5eAaImIp3nS
o	o	k7c6
přednostech	přednost	k1gFnPc6
(	(	kIx(
<g/>
anglického	anglický	k2eAgNnSc2d1
<g/>
)	)	kIx)
piva	pivo	k1gNnSc2
oproti	oproti	k7c3
(	(	kIx(
<g/>
francouzskému	francouzský	k2eAgMnSc3d1
<g/>
)	)	kIx)
vínu	víno	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přichází	přicházet	k5eAaImIp3nS
však	však	k9
majitel	majitel	k1gMnSc1
pivovaru	pivovar	k1gInSc2
<g/>
,	,	kIx,
vdovec	vdovec	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Robinson	Robinson	k1gMnSc1
a	a	k8xC
nařizuje	nařizovat	k5eAaImIp3nS
všechnu	všechen	k3xTgFnSc4
práci	práce	k1gFnSc4
zastavit	zastavit	k5eAaPmF
<g/>
,	,	kIx,
vyplácí	vyplácet	k5eAaImIp3nS
mimořádnou	mimořádný	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
a	a	k8xC
chystá	chystat	k5eAaImIp3nS
hostinu	hostina	k1gFnSc4
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
k	k	k7c3
jaké	jaký	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
příležitosti	příležitost	k1gFnSc3
(	(	kIx(
<g/>
scéna	scéna	k1gFnSc1
a	a	k8xC
árie	árie	k1gFnPc1
Amis	Amis	k1gInSc1
<g/>
,	,	kIx,
que	que	k?
la	la	k1gNnSc1
besogne	besognout	k5eAaImIp3nS,k5eAaPmIp3nS
cesse	cesse	k1gFnSc1
<g/>
...	...	k?
<g/>
Quand	Quand	k1gInSc1
je	být	k5eAaImIp3nS
suis	suis	k6eAd1
heureux	heureux	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Effie	Effie	k1gFnSc1
<g/>
,	,	kIx,
dceři	dcera	k1gFnSc6
zemřelého	zemřelý	k2eAgMnSc4d1
vrchního	vrchní	k2eAgMnSc4d1
sládka	sládek	k1gMnSc4
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
nakázal	nakázat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vystrojila	vystrojit	k5eAaPmAgFnS
do	do	k7c2
nejlepších	dobrý	k2eAgInPc2d3
šatů	šat	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
oznamuje	oznamovat	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
našel	najít	k5eAaPmAgMnS
bohatou	bohatý	k2eAgFnSc4d1
nevěstu	nevěsta	k1gFnSc4
a	a	k8xC
ještě	ještě	k9
dnes	dnes	k6eAd1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
svatbu	svatba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Effie	Effie	k1gFnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
o	o	k7c6
samotě	samota	k1gFnSc6
reaguje	reagovat	k5eAaBmIp3nS
pláčem	pláč	k1gInSc7
a	a	k8xC
kuplety	kuplet	k1gInPc7
Monsier	Monsier	k1gMnSc1
Robinson	Robinson	k1gMnSc1
est	est	k?
ci	ci	k0
bon	bon	k1gInSc4
garçon	garçona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
alespoň	alespoň	k9
Daniela	Daniela	k1gFnSc1
přesvědčí	přesvědčit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnPc1
vlastní	vlastní	k2eAgFnPc1d1
city	city	k1gFnPc1
jsou	být	k5eAaImIp3nP
opětovány	opětován	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
sejdou	sejít	k5eAaPmIp3nP
k	k	k7c3
hostině	hostina	k1gFnSc3
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
jako	jako	k9
svou	svůj	k3xOyFgFnSc7
nastávající	nastávající	k1gFnSc7
právě	právě	k9
Effie	Effie	k1gFnSc1
(	(	kIx(
<g/>
ansámbl	ansámbl	k1gInSc1
Quand	Quand	k1gInSc1
un	un	k?
ami	ami	k?
nous	nous	k1gInSc1
appelle	appell	k1gMnSc2
<g/>
...	...	k?
Oui	Oui	k1gMnSc2
<g/>
,	,	kIx,
c	c	k0
<g/>
'	'	kIx"
<g/>
est	est	k?
toi	toi	k?
<g/>
,	,	kIx,
mon	mon	k?
Effie	Effie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
rozveselení	rozveselení	k1gNnSc4
zpívá	zpívat	k5eAaImIp3nS
Daniel	Daniel	k1gMnSc1
sládkovskou	sládkovský	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
(	(	kIx(
<g/>
Gentil	Gentil	k1gMnSc1
brasseur	brasseur	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
s	s	k7c7
Effie	Effie	k1gFnPc1
osamotní	osamotnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
přizná	přiznat	k5eAaPmIp3nS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gNnSc3
mezi	mezi	k7c7
hosty	host	k1gMnPc7
schází	scházet	k5eAaImIp3nS
jen	jen	k9
bratr	bratr	k1gMnSc1
George	Georg	k1gInSc2
<g/>
,	,	kIx,
poručík	poručík	k1gMnSc1
královské	královský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Danielovo	Danielův	k2eAgNnSc1d1
je	on	k3xPp3gNnSc4
jednovaječné	jednovaječný	k2eAgNnSc4d1
dvojče	dvojče	k1gNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc4
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
je	on	k3xPp3gMnPc4
Effie	Effius	k1gMnPc4
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
nespletla	splést	k5eNaPmAgFnS
<g/>
?	?	kIx.
</s>
<s desamb="1">
Daniel	Daniela	k1gFnPc2
seriózně	seriózně	k6eAd1
vymýšlí	vymýšlet	k5eAaImIp3nP
vhodné	vhodný	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
<g/>
,	,	kIx,
Effie	Effie	k1gFnPc1
tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
příliš	příliš	k6eAd1
vážně	vážně	k6eAd1
nebere	brát	k5eNaImIp3nS
(	(	kIx(
<g/>
duet	duet	k1gInSc1
Cherchons	Cherchonsa	k1gFnPc2
ensemble	ensemble	k1gInSc1
<g/>
,	,	kIx,
chè	chè	k1gMnSc5
Effie	Effius	k1gMnSc5
<g/>
...	...	k?
Montagnarde	montagnard	k1gMnSc5
jolie	jolius	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Idylu	idyla	k1gFnSc4
však	však	k9
vyruší	vyrušit	k5eAaPmIp3nS
seržant	seržant	k1gMnSc1
Toby	Toba	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
pivovaru	pivovar	k1gInSc6
hledá	hledat	k5eAaImIp3nS
George	Georg	k1gMnSc4
Robinsona	Robinson	k1gMnSc4
<g/>
,	,	kIx,
svého	svůj	k3xOyFgMnSc4
velitele	velitel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
totiž	totiž	k9
nevrátil	vrátit	k5eNaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
vojenského	vojenský	k2eAgNnSc2d1
ležení	ležení	k1gNnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
jeho	jeho	k3xOp3gFnSc1
propustka	propustka	k1gFnSc1
už	už	k6eAd1
dávno	dávno	k6eAd1
vypršela	vypršet	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
hrozí	hrozit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
odsouzen	odsouzet	k5eAaImNgInS,k5eAaPmNgInS
k	k	k7c3
smrti	smrt	k1gFnSc3
jakožto	jakožto	k8xS
zběh	zběh	k1gMnSc1
(	(	kIx(
<g/>
spíše	spíše	k9
než	než	k8xS
to	ten	k3xDgNnSc1
ale	ale	k8xC
bytostného	bytostný	k2eAgMnSc4d1
vojáka	voják	k1gMnSc4
Tobyho	Toby	k1gMnSc4
pohoršuje	pohoršovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
předtím	předtím	k6eAd1
degradován	degradován	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chystá	chystat	k5eAaImIp3nS
se	se	k3xPyFc4
totiž	totiž	k9
rozhodující	rozhodující	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
s	s	k7c7
jakobity	jakobita	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniel	k1gMnSc1
svatbu	svatba	k1gFnSc4
k	k	k7c3
překvapení	překvapení	k1gNnSc3
shromážděných	shromážděný	k2eAgMnPc2d1
hostí	host	k1gMnPc2wK
odloží	odložit	k5eAaPmIp3nS
a	a	k8xC
spěchá	spěchat	k5eAaImIp3nS
i	i	k9
s	s	k7c7
Effie	Effie	k1gFnSc1
k	k	k7c3
vojenskému	vojenský	k2eAgNnSc3d1
ležení	ležení	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
u	u	k7c2
krále	král	k1gMnSc2
vyprosil	vyprosit	k5eAaPmAgMnS
pro	pro	k7c4
bratra	bratr	k1gMnSc4
milost	milost	k1gFnSc1
(	(	kIx(
<g/>
finále	finále	k1gNnSc1
Pour	Pour	k1gMnSc1
aller	aller	k1gMnSc1
à	à	k?
la	la	k1gNnSc4
chapelle	chapelle	k1gFnSc2
<g/>
...	...	k?
Que	Que	k1gFnSc2
l	l	kA
<g/>
'	'	kIx"
<g/>
espoir	espoir	k1gMnSc1
<g/>
,	,	kIx,
qui	qui	k?
m	m	kA
<g/>
'	'	kIx"
<g/>
enflamme	enflamit	k5eAaPmRp1nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
(	(	kIx(
<g/>
Vojenský	vojenský	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
)	)	kIx)
Probíhá	probíhat	k5eAaImIp3nS
vojenská	vojenský	k2eAgFnSc1d1
přehlídka	přehlídka	k1gFnSc1
(	(	kIx(
<g/>
sbor	sbor	k1gInSc1
Voici	Voice	k1gFnSc4
l	l	kA
<g/>
'	'	kIx"
<g/>
heure	heur	k1gInSc5
de	de	k?
la	la	k1gNnSc7
revue	revue	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	George	k1gNnSc1
stále	stále	k6eAd1
není	být	k5eNaImIp3nS
k	k	k7c3
nalezení	nalezení	k1gNnSc3
a	a	k8xC
pokud	pokud	k8xS
se	se	k3xPyFc4
do	do	k7c2
hodiny	hodina	k1gFnSc2
nedostaví	dostavit	k5eNaPmIp3nS
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Lovel	Lovel	k1gMnSc1
<g/>
,	,	kIx,
pobočník	pobočník	k1gMnSc1
velícího	velící	k2eAgMnSc2d1
generála	generál	k1gMnSc2
<g/>
,	,	kIx,
osloví	oslovit	k5eAaPmIp3nS
Daniela	Daniela	k1gFnSc1
jako	jako	k8xS,k8xC
George	George	k1gFnSc1
<g/>
,	,	kIx,
seržant	seržant	k1gMnSc1
Toby	Toba	k1gFnSc2
v	v	k7c6
tom	ten	k3xDgNnSc6
uvidí	uvidět	k5eAaPmIp3nS
řešení	řešení	k1gNnSc1
<g/>
:	:	kIx,
vydávat	vydávat	k5eAaImF,k5eAaPmF
Daniela	Daniela	k1gFnSc1
dočasně	dočasně	k6eAd1
za	za	k7c2
zběhlého	zběhlý	k2eAgMnSc2d1
bratra	bratr	k1gMnSc2
(	(	kIx(
<g/>
scéna	scéna	k1gFnSc1
Allons	Allons	k1gInSc1
<g/>
,	,	kIx,
allons	allons	k1gInSc1
<g/>
,	,	kIx,
sans	sans	k6eAd1
plus	plus	k1gInSc1
attendre	attendr	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniel	k1gMnSc1
bez	bez	k7c2
nadšení	nadšení	k1gNnSc2
souhlasí	souhlasit	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
zatímco	zatímco	k8xS
se	se	k3xPyFc4
převléká	převlékat	k5eAaImIp3nS
do	do	k7c2
uniformy	uniforma	k1gFnSc2
<g/>
,	,	kIx,
Toby	Tob	k2eAgFnPc1d1
baví	bavit	k5eAaImIp3nP
vojáky	voják	k1gMnPc4
pijáckou	pijácký	k2eAgFnSc7d1
písní	píseň	k1gFnSc7
(	(	kIx(
<g/>
Un	Un	k1gMnSc1
bon	bona	k1gFnPc2
luron	luron	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
le	le	k?
dragon	dragon	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toby	Tobum	k1gNnPc7
ještě	ještě	k6eAd1
ale	ale	k9
musí	muset	k5eAaImIp3nS
Daniela	Daniel	k1gMnSc4
názornou	názorný	k2eAgFnSc7d1
lekcí	lekce	k1gFnSc7
naučit	naučit	k5eAaPmF
vojáckým	vojácký	k2eAgInPc3d1
způsobům	způsob	k1gInPc3
<g/>
,	,	kIx,
totiž	totiž	k9
jak	jak	k6eAd1
pochodovat	pochodovat	k5eAaImF
<g/>
,	,	kIx,
klít	klít	k5eAaImF
<g/>
,	,	kIx,
pít	pít	k5eAaImF
a	a	k8xC
kouřit	kouřit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
ukáže	ukázat	k5eAaPmIp3nS
<g/>
,	,	kIx,
ke	k	k7c3
všem	všecek	k3xTgFnPc3
těmto	tento	k3xDgFnPc3
činnostem	činnost	k1gFnPc3
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
větší	veliký	k2eAgInSc4d2
talent	talent	k1gInSc4
Effie	Effie	k1gFnSc2
(	(	kIx(
<g/>
komický	komický	k2eAgInSc1d1
tercet	tercet	k1gInSc1
Ça	Ça	k1gFnSc2
n	n	k0
<g/>
'	'	kIx"
<g/>
est	est	k?
pas	pas	k1gInSc4
facile	facile	k6eAd1
<g/>
...	...	k?
Cela	cela	k1gFnSc1
s	s	k7c7
<g/>
'	'	kIx"
<g/>
apprend	apprend	k1gMnSc1
en	en	k?
un	un	k?
instant	instant	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
nejoriginálnějším	originální	k2eAgNnSc7d3
a	a	k8xC
nejoblíbenějším	oblíbený	k2eAgNnSc7d3
číslem	číslo	k1gNnSc7
opery	opera	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
George	George	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Daniel	Daniel	k1gMnSc1
se	se	k3xPyFc4
tedy	tedy	k9
vyhne	vyhnout	k5eAaPmIp3nS
smrti	smrt	k1gFnSc3
<g/>
,	,	kIx,
za	za	k7c4
trest	trest	k1gInSc4
však	však	k9
musí	muset	k5eAaImIp3nS
odevzdat	odevzdat	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
meč	meč	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
nařízen	nařízen	k2eAgInSc1d1
arest	arest	k?
v	v	k7c6
ležení	ležení	k1gNnSc6
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
účastnit	účastnit	k5eAaImF
nastávající	nastávající	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
takovým	takový	k3xDgInSc7
trestem	trest	k1gInSc7
je	být	k5eAaImIp3nS
Daniel	Daniel	k1gMnSc1
nadmíru	nadmíru	k6eAd1
spokojen	spokojen	k2eAgMnSc1d1
<g/>
,	,	kIx,
zato	zato	k6eAd1
Toby	Toba	k1gFnSc2
je	být	k5eAaImIp3nS
zděšen	zděsit	k5eAaPmNgMnS
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gNnSc4
získat	získat	k5eAaPmF
"	"	kIx"
<g/>
milost	milost	k1gFnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
jistý	jistý	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
Jenkins	Jenkinsa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
George	George	k1gInSc1
<g/>
/	/	kIx~
<g/>
Daniela	Daniela	k1gFnSc1
obviní	obvinit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
svedl	svést	k5eAaPmAgMnS
a	a	k8xC
opustil	opustit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc4
sestru	sestra	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
vyzývá	vyzývat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
na	na	k7c4
souboj	souboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniel	k1gMnSc1
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
nemá	mít	k5eNaImIp3nS
svůj	svůj	k3xOyFgInSc4
meč	meč	k1gInSc4
–	–	k?
Toby	Toba	k1gFnSc2
jej	on	k3xPp3gMnSc4
ale	ale	k8xC
vítězoslavně	vítězoslavně	k6eAd1
přináší	přinášet	k5eAaImIp3nS
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniel	k1gMnSc1
ale	ale	k9
i	i	k9
tak	tak	k6eAd1
nemá	mít	k5eNaImIp3nS
na	na	k7c4
souboj	souboj	k1gInSc4
čas	čas	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
přece	přece	k9
musí	muset	k5eAaImIp3nP
nastoupit	nastoupit	k5eAaPmF
do	do	k7c2
vězení	vězení	k1gNnSc2
–	–	k?
seržant	seržant	k1gMnSc1
Toby	Toba	k1gFnSc2
mu	on	k3xPp3gMnSc3
ale	ale	k9
zvěstuje	zvěstovat	k5eAaImIp3nS
propuštění	propuštění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
poslední	poslední	k2eAgFnSc7d1
Danielovou	Danielův	k2eAgFnSc7d1
záchranou	záchrana	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k6eAd1
začíná	začínat	k5eAaImIp3nS
bitva	bitva	k1gFnSc1
(	(	kIx(
<g/>
finále	finále	k1gNnSc6
La	la	k1gNnSc2
trompette	trompette	k5eAaPmIp2nP
résonne	résonnout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
je	být	k5eAaImIp3nS
volky	volky	k6eAd1
nevolky	nevolky	k6eAd1
poslán	poslat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Effie	Effie	k1gFnSc1
dává	dávat	k5eAaImIp3nS
svému	svůj	k3xOyFgNnSc3
zoufalství	zoufalství	k1gNnSc3
průchod	průchod	k1gInSc4
v	v	k7c6
árii	árie	k1gFnSc6
Là	Là	k1gFnPc2
<g/>
,	,	kIx,
dans	dans	k6eAd1
la	la	k1gNnSc1
plaine	plainout	k5eAaPmIp3nS
<g/>
...	...	k?
Dans	Dans	k1gInSc1
notre	notr	k1gInSc5
brasserie	brasserie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
za	za	k7c4
chvíli	chvíle	k1gFnSc4
(	(	kIx(
<g/>
Victoire	Victoir	k1gMnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
Victoire	Victoir	k1gInSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
se	se	k3xPyFc4
Daniel	Daniel	k1gMnSc1
vrací	vracet	k5eAaImIp3nS
jako	jako	k9
hrdina	hrdina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
árii	árie	k1gFnSc6
(	(	kIx(
<g/>
Tout-à	Tout-à	k1gInSc4
<g/>
'	'	kIx"
<g/>
heure	heur	k1gMnSc5
<g/>
,	,	kIx,
tant	tant	k1gMnSc1
bien	biena	k1gFnPc2
que	que	k?
mal	málit	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
posadili	posadit	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
na	na	k7c4
koně	kůň	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
pro	pro	k7c4
sládka	sládek	k1gMnSc4
cizí	cizí	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
frontové	frontový	k2eAgFnSc6d1
linii	linie	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
jej	on	k3xPp3gMnSc4
podařilo	podařit	k5eAaPmAgNnS
bezděky	bezděky	k6eAd1
rozběhnout	rozběhnout	k5eAaPmF
a	a	k8xC
zabrzdit	zabrzdit	k5eAaPmF
ho	on	k3xPp3gMnSc4
neuměl	umět	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddíl	oddíl	k1gInSc1
následoval	následovat	k5eAaImAgInS
Daniela	Daniel	k1gMnSc4
do	do	k7c2
útoku	útok	k1gInSc2
a	a	k8xC
tomu	ten	k3xDgNnSc3
nakonec	nakonec	k6eAd1
nezbylo	zbýt	k5eNaPmAgNnS
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dát	dát	k5eAaPmF
hlava	hlava	k1gFnSc1
nehlava	nehlava	k1gFnSc1
do	do	k7c2
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
hrdina	hrdina	k1gMnSc1
je	být	k5eAaImIp3nS
pověřen	pověřit	k5eAaPmNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
donesl	donést	k5eAaPmAgInS
zprávu	zpráva	k1gFnSc4
o	o	k7c4
vítězství	vítězství	k1gNnSc4
a	a	k8xC
ukořistěné	ukořistěný	k2eAgFnSc2d1
zástavy	zástava	k1gFnSc2
králi	král	k1gMnSc3
(	(	kIx(
<g/>
Pour	Pour	k1gMnSc1
récompenser	récompenser	k1gMnSc1
votre	votr	k1gInSc5
zè	zè	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
(	(	kIx(
<g/>
Galerie	galerie	k1gFnSc1
Windsorského	windsorský	k2eAgInSc2d1
zámku	zámek	k1gInSc2
<g/>
)	)	kIx)
Daniel	Daniel	k1gMnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
audienci	audience	k1gFnSc6
u	u	k7c2
krále	král	k1gMnSc2
<g/>
,	,	kIx,
Toby	Toba	k1gFnSc2
a	a	k8xC
Effie	Effie	k1gFnSc2
na	na	k7c4
něj	on	k3xPp3gMnSc4
čekají	čekat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniel	k1gMnSc1
byl	být	k5eAaImAgMnS
z	z	k7c2
této	tento	k3xDgFnSc2
příležitosti	příležitost	k1gFnSc2
celý	celý	k2eAgInSc4d1
nesvůj	nesvůj	k6eAd1
<g/>
,	,	kIx,
zato	zato	k6eAd1
Toby	Toba	k1gFnSc2
by	by	kYmCp3nS
trému	tréma	k1gFnSc4
neměl	mít	k5eNaImAgMnS
(	(	kIx(
<g/>
kuplety	kuplet	k1gInPc1
Si	se	k3xPyFc3
j	j	k?
<g/>
'	'	kIx"
<g/>
avais	avais	k1gInSc1
à	à	k?
parler	parler	k1gInSc1
au	au	k0
roi	roi	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
audienci	audience	k1gFnSc6
král	král	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
nemilou	milý	k2eNgFnSc4d1
zprávu	zpráva	k1gFnSc4
<g/>
;	;	kIx,
Daniel	Daniel	k1gMnSc1
spekuluje	spekulovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dověděl	dovědět	k5eAaPmAgMnS
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
"	"	kIx"
<g/>
podvodu	podvod	k1gInSc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
strach	strach	k1gInSc1
se	se	k3xPyFc4
mísí	mísit	k5eAaImIp3nS
s	s	k7c7
hořkostí	hořkost	k1gFnSc7
vůči	vůči	k7c3
bratrovi	bratr	k1gMnSc3
(	(	kIx(
<g/>
romance	romance	k1gFnSc2
Pour	Pour	k1gMnSc1
sauver	sauver	k1gMnSc1
ta	ten	k3xDgNnPc4
vie	vie	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ale	ale	k8xC
král	král	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
špatné	špatný	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
z	z	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
v	v	k7c4
reakci	reakce	k1gFnSc4
na	na	k7c4
ně	on	k3xPp3gInPc4
povýšil	povýšit	k5eAaPmAgInS
George	George	k1gInSc1
<g/>
/	/	kIx~
<g/>
Daniela	Daniela	k1gFnSc1
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
majora	major	k1gMnSc2
a	a	k8xC
pověřil	pověřit	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
vedením	vedení	k1gNnSc7
irské	irský	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniela	k1gFnPc2
opět	opět	k6eAd1
není	být	k5eNaImIp3nS
vyznamenáním	vyznamenání	k1gNnSc7
nadšen	nadšen	k2eAgInSc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
když	když	k8xS
se	se	k3xPyFc4
opět	opět	k6eAd1
objeví	objevit	k5eAaPmIp3nS
kapitán	kapitán	k1gMnSc1
Jenkins	Jenkins	k1gInSc4
s	s	k7c7
připravenou	připravený	k2eAgFnSc7d1
svatební	svatební	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
Daniela	Daniela	k1gFnSc1
nutí	nutit	k5eAaImIp3nS
podepsat	podepsat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Daniel	Daniel	k1gMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
získat	získat	k5eAaPmF
čas	čas	k1gInSc4
na	na	k7c6
králi	král	k1gMnSc6
i	i	k9
na	na	k7c6
Jenkinsovi	Jenkins	k1gMnSc6
a	a	k8xC
chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
alespoň	alespoň	k9
po	po	k7c4
dobu	doba	k1gFnSc4
milostného	milostný	k2eAgInSc2d1
duetu	duet	k1gInSc2
Ah	ah	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
pour	poura	k1gFnPc2
nous	nous	k6eAd1
quel	quenout	k5eAaPmAgMnS
bonheur	bonheur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
dotyční	dotyčný	k2eAgMnPc1d1
však	však	k9
dohodli	dohodnout	k5eAaPmAgMnP
jinak	jinak	k6eAd1
<g/>
:	:	kIx,
Daniel	Daniel	k1gMnSc1
si	se	k3xPyFc3
má	mít	k5eAaImIp3nS
okamžitě	okamžitě	k6eAd1
vzít	vzít	k5eAaPmF
slečnu	slečna	k1gFnSc4
Jenkinsovou	Jenkinsový	k2eAgFnSc4d1
a	a	k8xC
ihned	ihned	k6eAd1
poté	poté	k6eAd1
vyrazit	vyrazit	k5eAaPmF
na	na	k7c4
válečnou	válečný	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
konečně	konečně	k6eAd1
vrací	vracet	k5eAaImIp3nS
pravý	pravý	k2eAgInSc4d1
George	George	k1gInSc4
Robinson	Robinson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
ochoten	ochoten	k2eAgMnSc1d1
od	od	k7c2
Daniela	Daniel	k1gMnSc2
převzít	převzít	k5eAaPmF
zpět	zpět	k6eAd1
jeho	jeho	k3xOp3gFnPc4
vojenské	vojenský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
i	i	k8xC
závazky	závazek	k1gInPc4
ke	k	k7c3
slečně	slečna	k1gFnSc3
Jenkinsové	Jenkinsová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniela	Daniela	k1gFnSc1
a	a	k8xC
Effie	Effie	k1gFnSc1
se	se	k3xPyFc4
konečně	konečně	k6eAd1
mohou	moct	k5eAaImIp3nP
vrátit	vrátit	k5eAaPmF
do	do	k7c2
svého	svůj	k3xOyFgInSc2
pivovaru	pivovar	k1gInSc2
a	a	k8xC
konat	konat	k5eAaImF
vlastní	vlastní	k2eAgFnSc4d1
svatbu	svatba	k1gFnSc4
(	(	kIx(
<g/>
finále	finále	k1gNnSc2
C	C	kA
<g/>
'	'	kIx"
<g/>
en	en	k?
est	est	k?
trop	trop	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
...	...	k?
Quel	Quela	k1gFnPc2
doux	doux	k1gInSc1
espoir	espoir	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SCHERL	SCHERL	kA
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zur	Zur	k1gFnSc1
Rezeption	Rezeption	k1gInSc4
von	von	k1gInSc1
Grétrys	Grétrys	k1gInSc1
Opern	Opern	k1gInSc1
auf	auf	k?
dem	dem	k?
Prager	Prager	k1gMnSc1
Theater	Theater	k1gMnSc1
der	drát	k5eAaImRp2nS
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hälfte	Hälft	k1gInSc5
des	des	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jahrhunderts	Jahrhundertsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
POSPÍŠIL	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
JACOBSHAGEN	JACOBSHAGEN	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
;	;	kIx,
CLAUDON	CLAUDON	kA
<g/>
,	,	kIx,
Francis	Francis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gMnSc1
rayonnement	rayonnement	k1gMnSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
opéra-comique	opéra-comiqu	k1gMnSc2
en	en	k?
Europe	Europ	k1gInSc5
au	au	k0
XIXe	XIXe	k1gInSc1
siè	siè	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
KLP	KLP	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85917	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
227	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
TROJAN	Trojan	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Opéra	Opéra	k1gFnSc1
comique	comique	k1gFnSc1
auf	auf	k?
der	drát	k5eAaImRp2nS
Bühne	Bühn	k1gInSc5
der	drát	k5eAaImRp2nS
Brünner	Brünner	k1gInSc1
Redoute	Redout	k1gMnSc5
in	in	k?
der	drát	k5eAaImRp2nS
Zeit	Zeit	k1gInSc4
des	des	k1gNnSc3
Vormärz	Vormärza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
POSPÍŠIL	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
JACOBSHAGEN	JACOBSHAGEN	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
;	;	kIx,
CLAUDON	CLAUDON	kA
<g/>
,	,	kIx,
Francis	Francis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gMnSc1
rayonnement	rayonnement	k1gMnSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
opéra-comique	opéra-comiqu	k1gMnSc2
en	en	k?
Europe	Europ	k1gInSc5
au	au	k0
XIXe	XIXe	k1gInSc1
siè	siè	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
KLP	KLP	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85917	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
242	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Narhallamarsch	Narhallamarsch	k1gInSc1
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
Použité	použitý	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
WAGNER	Wagner	k1gMnSc1
<g/>
,	,	kIx,
Heinz	Heinz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gFnSc1
grosse	grosse	k1gFnSc2
Handbuch	Handbucha	k1gFnPc2
der	drát	k5eAaImRp2nS
Oper	opera	k1gFnPc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamburg	Hamburg	k1gInSc1
<g/>
:	:	kIx,
Nikol	nikol	k1gInSc1
Verlagsgesellschaft	Verlagsgesellschaft	k1gInSc1
mbH	mbH	k?
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
KG	kg	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
1470	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
937872	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
Adolphe	Adolphe	k1gFnSc1
Charles	Charles	k1gMnSc1
-	-	kIx~
Der	drát	k5eAaImRp2nS
Brauer	Brauer	k1gInSc4
von	von	k1gInSc1
Preston	Preston	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Le	Le	k?
Brasseur	Brasseur	k1gMnSc1
de	de	k?
Preston	Preston	k1gInSc1
von	von	k1gInSc1
Adam	Adam	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OperOne	OperOn	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Kunst	Kunst	k1gFnSc1
und	und	k?
Leben	Leben	k1gInSc1
in	in	k?
Böhmen	Böhmen	k1gInSc1
-	-	kIx~
Theaterbericht	Theaterbericht	k2eAgInSc1d1
von	von	k1gInSc1
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
ein	ein	k?
Unterhaltungsblatt	Unterhaltungsblatt	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1839	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
102	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
oper	opera	k1gFnPc2
Adolpha	Adolph	k1gMnSc4
Adama	Adam	k1gMnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Libreto	libreto	k1gNnSc1
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6
</s>
<s>
Klavírní	klavírní	k2eAgInSc1d1
výtah	výtah	k1gInSc1
opery	opera	k1gFnSc2
</s>
<s>
Sládek	Sládek	k1gMnSc1
prestonský	prestonský	k2eAgMnSc1d1
v	v	k7c6
databázi	databáze	k1gFnSc6
Archivu	archiv	k1gInSc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
