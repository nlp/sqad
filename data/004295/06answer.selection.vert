<s>
Jeho	jeho	k3xOp3gInSc1	jeho
magnum	magnum	k1gInSc1	magnum
opus	opus	k1gInSc4	opus
je	být	k5eAaImIp3nS	být
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
stejnojmenné	stejnojmenný	k2eAgInPc4d1	stejnojmenný
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
knize	kniha	k1gFnSc3	kniha
Václava	Václav	k1gMnSc2	Václav
Kaplického	Kaplický	k2eAgMnSc2d1	Kaplický
o	o	k7c6	o
čarodějnických	čarodějnický	k2eAgInPc6d1	čarodějnický
procesech	proces	k1gInPc6	proces
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
