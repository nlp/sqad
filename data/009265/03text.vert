<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Iráku	Irák	k1gInSc2	Irák
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
egyptské	egyptský	k2eAgFnSc2d1	egyptská
trikolóry	trikolóra	k1gFnSc2	trikolóra
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředním	prostřední	k2eAgMnSc6d1	prostřední
<g/>
,	,	kIx,	,
bílém	bílý	k2eAgInSc6d1	bílý
pruhu	pruh	k1gInSc6	pruh
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc1d1	zelený
arabský	arabský	k2eAgInSc1d1	arabský
nápis	nápis	k1gInSc1	nápis
Allā	Allā	k1gMnSc1	Allā
Akbar	Akbar	k1gMnSc1	Akbar
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc4d1	veliký
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
statečnost	statečnost	k1gFnSc4	statečnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
šlechetnost	šlechetnost	k1gFnSc1	šlechetnost
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
islámské	islámský	k2eAgMnPc4d1	islámský
vítěze	vítěz	k1gMnPc4	vítěz
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgFnSc1d1	poslední
změna	změna	k1gFnSc1	změna
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
vlajkový	vlajkový	k2eAgInSc1d1	vlajkový
vzor	vzor	k1gInSc1	vzor
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
listu	list	k1gInSc2	list
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
letech	let	k1gInPc6	let
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
i	i	k8xC	i
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Iráku	Irák	k1gInSc2	Irák
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
tato	tento	k3xDgFnSc1	tento
trikolóra	trikolóra	k1gFnSc1	trikolóra
také	také	k9	také
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
Egyptu	Egypt	k1gInSc3	Egypt
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc3	Jemen
<g/>
,	,	kIx,	,
Súdánu	Súdán	k1gInSc3	Súdán
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
nápisu	nápis	k1gInSc2	nápis
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
klín	klín	k1gInSc4	klín
nebo	nebo	k8xC	nebo
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Iráku	Irák	k1gInSc2	Irák
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Eufrat	Eufrat	k1gInSc4	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc4	Tigris
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
633	[number]	k4	633
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1534	[number]	k4	1534
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Iráku	Irák	k1gInSc2	Irák
připojeni	připojen	k2eAgMnPc1d1	připojen
k	k	k7c3	k
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
státními	státní	k2eAgFnPc7d1	státní
vlajkami	vlajka	k1gFnPc7	vlajka
vyvěšovanými	vyvěšovaný	k2eAgFnPc7d1	vyvěšovaná
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Iráku	Irák	k1gInSc2	Irák
byly	být	k5eAaImAgFnP	být
Osmanské	osmanský	k2eAgFnPc1d1	Osmanská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
byly	být	k5eAaImAgFnP	být
vlajky	vlajka	k1gFnPc1	vlajka
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
tvořeny	tvořen	k2eAgMnPc4d1	tvořen
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
osmicípou	osmicípý	k2eAgFnSc7d1	osmicípá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
půlměsíci	půlměsíc	k1gInPc7	půlměsíc
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
obrázky	obrázek	k1gInPc4	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
byla	být	k5eAaImAgFnS	být
hvězda	hvězda	k1gFnSc1	hvězda
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
pěticípou	pěticípý	k2eAgFnSc4d1	pěticípá
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
obsadila	obsadit	k5eAaPmAgFnS	obsadit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
britsko-indická	britskondický	k2eAgNnPc4d1	britsko-indický
vojska	vojsko	k1gNnPc4	vojsko
Basru	Basr	k1gInSc2	Basr
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
dobývala	dobývat	k5eAaImAgFnS	dobývat
celý	celý	k2eAgInSc4d1	celý
Irák	Irák	k1gInSc4	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dobývání	dobývání	k1gNnSc1	dobývání
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
a	a	k8xC	a
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
vlajka	vlajka	k1gFnSc1	vlajka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1920	[number]	k4	1920
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
protikoloniální	protikoloniální	k2eAgNnSc1d1	protikoloniální
povstání	povstání	k1gNnSc1	povstání
ale	ale	k8xC	ale
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Irák	Irák	k1gInSc1	Irák
mandátním	mandátní	k2eAgNnSc7d1	mandátní
územím	území	k1gNnSc7	území
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
spravovaným	spravovaný	k2eAgNnSc7d1	spravované
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
užívali	užívat	k5eAaImAgMnP	užívat
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
království	království	k1gNnSc2	království
Hidžáz	Hidžáz	k1gInSc1	Hidžáz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvořily	tvořit	k5eAaImAgInP	tvořit
tři	tři	k4xCgInPc1	tři
pruhy	pruh	k1gInPc1	pruh
<g/>
:	:	kIx,	:
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žerdi	žerď	k1gFnSc2	žerď
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
klín	klín	k1gInSc1	klín
<g/>
.	.	kIx.	.
</s>
<s>
Panarabské	panarabský	k2eAgFnPc1d1	panarabská
barvy	barva	k1gFnPc1	barva
představovaly	představovat	k5eAaImAgFnP	představovat
arabské	arabský	k2eAgFnPc1d1	arabská
dynastie	dynastie	k1gFnPc1	dynastie
<g/>
:	:	kIx,	:
černá	černý	k2eAgFnSc1d1	černá
Abbásovce	Abbásovec	k1gInPc4	Abbásovec
<g/>
,	,	kIx,	,
zelená	zelená	k1gFnSc1	zelená
Fátimovce	Fátimovec	k1gInSc2	Fátimovec
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
Umajjovce	Umajjovka	k1gFnSc3	Umajjovka
a	a	k8xC	a
červená	červená	k1gFnSc1	červená
Hášimovce	Hášimovec	k1gInSc2	Hášimovec
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
.23	.23	k4	.23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
Irácké	irácký	k2eAgNnSc1d1	irácké
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Fajsal	Fajsal	k1gMnSc1	Fajsal
I.	I.	kA	I.
(	(	kIx(	(
<g/>
mimochodem	mimochodem	k9	mimochodem
bratr	bratr	k1gMnSc1	bratr
jordánského	jordánský	k2eAgMnSc2d1	jordánský
krále	král	k1gMnSc2	král
Abdalláha	Abdalláh	k1gMnSc2	Abdalláh
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
první	první	k4xOgFnSc1	první
irácká	irácký	k2eAgFnSc1d1	irácká
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
vlajka	vlajka	k1gFnSc1	vlajka
povstalců	povstalec	k1gMnPc2	povstalec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
klínu	klín	k1gInSc6	klín
však	však	k8xC	však
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
umístěny	umístit	k5eAaPmNgFnP	umístit
dvě	dva	k4xCgFnPc1	dva
bílé	bílý	k2eAgFnPc1d1	bílá
sedmicípé	sedmicípý	k2eAgFnPc1d1	sedmicípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hvězdy	hvězda	k1gFnPc1	hvězda
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Irák	Irák	k1gInSc1	Irák
stal	stát	k5eAaPmAgInS	stát
po	po	k7c4	po
Sýrii	Sýrie	k1gFnSc4	Sýrie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Zajordánska	Zajordánsko	k1gNnSc2	Zajordánsko
<g/>
)	)	kIx)	)
druhým	druhý	k4xOgInSc7	druhý
státem	stát	k1gInSc7	stát
v	v	k7c6	v
připravované	připravovaný	k2eAgFnSc6d1	připravovaná
panarabské	panarabský	k2eAgFnSc6d1	panarabská
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yQgFnPc4	který
měl	mít	k5eAaImAgMnS	mít
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
připojit	připojit	k5eAaPmF	připojit
další	další	k2eAgFnSc4d1	další
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jordánská	jordánský	k2eAgFnSc1d1	Jordánská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
sedmicípou	sedmicípý	k2eAgFnSc4d1	sedmicípá
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
stejné	stejný	k2eAgFnSc6d1	stejná
vlajce	vlajka	k1gFnSc6	vlajka
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
cípů	cíp	k1gInPc2	cíp
hvězd	hvězda	k1gFnPc2	hvězda
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
sedm	sedm	k4xCc1	sedm
úvodních	úvodní	k2eAgInPc2d1	úvodní
veršů	verš	k1gInPc2	verš
koránu	korán	k1gInSc2	korán
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
rozeznatelnost	rozeznatelnost	k1gFnSc4	rozeznatelnost
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
změněno	změnit	k5eAaPmNgNnS	změnit
pořadí	pořadí	k1gNnSc1	pořadí
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
černo-zeleno-bílé	černoelenoílý	k2eAgFnSc2d1	černo-zeleno-bílý
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
černo-bílo-zelená	černoíloelený	k2eAgFnSc1d1	černo-bílo-zelený
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
změně	změna	k1gFnSc3	změna
irácké	irácký	k2eAgFnSc2d1	irácká
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
lichoběžníkem	lichoběžník	k1gInSc7	lichoběžník
stejné	stejný	k2eAgFnPc4d1	stejná
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	stran	k7c2	stran
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
i	i	k9	i
po	po	k7c4	po
nahrazení	nahrazení	k1gNnSc4	nahrazení
mandátní	mandátní	k2eAgFnSc2d1	mandátní
smlouvy	smlouva	k1gFnSc2	smlouva
smlouvou	smlouva	k1gFnSc7	smlouva
spojeneckou	spojenecký	k2eAgFnSc7d1	spojenecká
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
formální	formální	k2eAgFnSc2d1	formální
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Fajsala	Fajsala	k1gMnSc2	Fajsala
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
i	i	k8xC	i
okupaci	okupace	k1gFnSc3	okupace
britskými	britský	k2eAgMnPc7d1	britský
vojsky	vojsky	k6eAd1	vojsky
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1947.14	[number]	k4	1947.14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
důstojníků	důstojník	k1gMnPc2	důstojník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Abdula	Abdul	k1gMnSc2	Abdul
Karima	Karima	k?	Karima
Kásima	Kásim	k1gMnSc2	Kásim
a	a	k8xC	a
zavraždění	zavraždění	k1gNnSc6	zavraždění
krále	král	k1gMnSc2	král
Fajsala	Fajsala	k1gFnSc2	Fajsala
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
svržena	svržen	k2eAgFnSc1d1	svržena
monarchie	monarchie	k1gFnSc1	monarchie
a	a	k8xC	a
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
Irácká	irácký	k2eAgFnSc1d1	irácká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1959	[number]	k4	1959
zavedena	zaveden	k2eAgFnSc1d1	zavedena
nová	nový	k2eAgFnSc1d1	nová
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvořil	tvořit	k5eAaImAgInS	tvořit
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
černým	černý	k2eAgInSc7d1	černý
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
byla	být	k5eAaImAgFnS	být
červená	červený	k2eAgFnSc1d1	červená
osmicípá	osmicípý	k2eAgFnSc1d1	osmicípá
hvězda	hvězda	k1gFnSc1	hvězda
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
středem	střed	k1gInSc7	střed
a	a	k8xC	a
bílým	bílý	k2eAgInSc7d1	bílý
lemem	lem	k1gInSc7	lem
<g/>
.	.	kIx.	.
<g/>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
proroka	prorok	k1gMnSc4	prorok
Muhameda	Muhamed	k1gMnSc4	Muhamed
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgNnPc1d1	bílé
a	a	k8xC	a
zelená	zelený	k2eAgNnPc1d1	zelené
byly	být	k5eAaImAgInP	být
barvy	barva	k1gFnPc4	barva
velkých	velký	k2eAgFnPc2d1	velká
dynastií	dynastie	k1gFnPc2	dynastie
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
barvou	barva	k1gFnSc7	barva
Arabů	Arab	k1gMnPc2	Arab
v	v	k7c6	v
Andalusii	Andalusie	k1gFnSc6	Andalusie
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
Slunce	slunce	k1gNnSc2	slunce
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
byla	být	k5eAaImAgFnS	být
symbolem	symbol	k1gInSc7	symbol
revoluce	revoluce	k1gFnSc1	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
Araby	Arab	k1gMnPc7	Arab
a	a	k8xC	a
Kurdy	Kurd	k1gMnPc7	Kurd
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
spojení	spojení	k1gNnSc1	spojení
Iráku	Irák	k1gInSc2	Irák
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
egyptského	egyptský	k2eAgInSc2d1	egyptský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
zavedena	zaveden	k2eAgFnSc1d1	zavedena
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
černým	černý	k2eAgMnSc7d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředním	prostřední	k2eAgMnSc6d1	prostřední
<g/>
,	,	kIx,	,
bílém	bílý	k2eAgInSc6d1	bílý
pruhu	pruh	k1gInSc6	pruh
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
tři	tři	k4xCgFnPc1	tři
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Federace	federace	k1gFnSc1	federace
nebyla	být	k5eNaImAgFnS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
<g/>
,	,	kIx,	,
vlajku	vlajka	k1gFnSc4	vlajka
si	se	k3xPyFc3	se
však	však	k9	však
Irák	Irák	k1gInSc4	Irák
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
šlechetnost	šlechetnost	k1gFnSc1	šlechetnost
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
muslimské	muslimský	k2eAgMnPc4d1	muslimský
vítěze	vítěz	k1gMnPc4	vítěz
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnSc2	hvězda
symbolizovaly	symbolizovat	k5eAaImAgInP	symbolizovat
tři	tři	k4xCgInPc1	tři
státy	stát	k1gInPc1	stát
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
převratech	převrat	k1gInPc6	převrat
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
postupně	postupně	k6eAd1	postupně
nastolil	nastolit	k5eAaPmAgMnS	nastolit
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
však	však	k9	však
nechal	nechat	k5eAaPmAgMnS	nechat
změnit	změnit	k5eAaPmF	změnit
a	a	k8xC	a
schválit	schválit	k5eAaPmF	schválit
parlamentem	parlament	k1gInSc7	parlament
až	až	k6eAd1	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
odvetnou	odvetný	k2eAgFnSc7d1	odvetná
operací	operace	k1gFnSc7	operace
(	(	kIx(	(
<g/>
Pouštní	pouštní	k2eAgFnPc4d1	pouštní
bouře	bouř	k1gFnPc4	bouř
<g/>
)	)	kIx)	)
vojsk	vojsko	k1gNnPc2	vojsko
pod	pod	k7c7	pod
vlajkou	vlajka	k1gFnSc7	vlajka
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
reagovala	reagovat	k5eAaBmAgFnS	reagovat
na	na	k7c4	na
anexi	anexe	k1gFnSc4	anexe
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
Irákem	Irák	k1gInSc7	Irák
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
předchozí	předchozí	k2eAgFnSc3d1	předchozí
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
zelený	zelený	k2eAgInSc4d1	zelený
nápis	nápis	k1gInSc4	nápis
Allā	Allā	k1gMnSc1	Allā
Akbar	Akbar	k1gMnSc1	Akbar
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
hvězdami	hvězda	k1gFnPc7	hvězda
(	(	kIx(	(
<g/>
psaný	psaný	k2eAgInSc1d1	psaný
Husajnovým	Husajnův	k2eAgInSc7d1	Husajnův
rukopisem	rukopis	k1gInSc7	rukopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husajn	Husajn	k1gMnSc1	Husajn
chtěl	chtít	k5eAaImAgMnS	chtít
touto	tento	k3xDgFnSc7	tento
změnou	změna	k1gFnSc7	změna
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
rozpoutanou	rozpoutaný	k2eAgFnSc4d1	rozpoutaná
válku	válka	k1gFnSc4	válka
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
ideologií	ideologie	k1gFnSc7	ideologie
<g/>
.25	.25	k4	.25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
baasistické	baasistický	k2eAgFnSc2d1	baasistická
symboliky	symbolika	k1gFnSc2	symbolika
a	a	k8xC	a
o	o	k7c4	o
demokratický	demokratický	k2eAgInSc4d1	demokratický
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
schválena	schválen	k2eAgFnSc1d1	schválena
Prozatímní	prozatímní	k2eAgFnSc7d1	prozatímní
vládní	vládní	k2eAgFnSc7d1	vládní
radou	rada	k1gFnSc7	rada
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k6eAd1	již
této	tento	k3xDgFnSc3	tento
charakteristice	charakteristika	k1gFnSc3	charakteristika
neodpovídala	odpovídat	k5eNaImAgFnS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
tvořil	tvořit	k5eAaImAgInS	tvořit
bílý	bílý	k2eAgInSc1d1	bílý
list	list	k1gInSc1	list
se	s	k7c7	s
světle	světle	k6eAd1	světle
modrým	modrý	k2eAgInSc7d1	modrý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
úzkými	úzký	k2eAgInPc7d1	úzký
pruhy	pruh	k1gInPc7	pruh
při	při	k7c6	při
dolním	dolní	k2eAgInSc6d1	dolní
okraji	okraj	k1gInSc6	okraj
<g/>
:	:	kIx,	:
tmavě	tmavě	k6eAd1	tmavě
modrým	modrý	k2eAgMnPc3d1	modrý
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgMnPc3d1	žlutý
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
modrým	modrý	k2eAgInPc3d1	modrý
<g/>
.	.	kIx.	.
<g/>
Již	již	k9	již
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgInS	být
světle	světle	k6eAd1	světle
modrý	modrý	k2eAgInSc1d1	modrý
odstín	odstín	k1gInSc1	odstín
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
změněn	změnit	k5eAaPmNgInS	změnit
po	po	k7c6	po
připomínkách	připomínka	k1gFnPc6	připomínka
na	na	k7c4	na
tmavě	tmavě	k6eAd1	tmavě
modrý	modrý	k2eAgInSc4d1	modrý
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc4d1	stejný
jako	jako	k8xC	jako
na	na	k7c6	na
pruzích	pruh	k1gInPc6	pruh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
odstínu	odstín	k1gInSc2	odstín
na	na	k7c6	na
izraelské	izraelský	k2eAgFnSc6d1	izraelská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Návrh	návrh	k1gInSc1	návrh
vlajky	vlajka	k1gFnSc2	vlajka
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
zákon	zákon	k1gInSc4	zákon
nakonec	nakonec	k6eAd1	nakonec
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
příležitostech	příležitost	k1gFnPc6	příležitost
byla	být	k5eAaImAgFnS	být
vyvěšována	vyvěšován	k2eAgFnSc1d1	vyvěšována
modifikovaná	modifikovaný	k2eAgFnSc1d1	modifikovaná
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
grafickou	grafický	k2eAgFnSc7d1	grafická
úpravou	úprava	k1gFnSc7	úprava
nápisu	nápis	k1gInSc2	nápis
(	(	kIx(	(
<g/>
hranatější	hranatý	k2eAgNnSc1d2	hranatější
písmo	písmo	k1gNnSc1	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
odstraněny	odstraněn	k2eAgFnPc4d1	odstraněna
zelené	zelený	k2eAgFnPc4d1	zelená
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
změnilo	změnit	k5eAaPmAgNnS	změnit
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
Takbíru	Takbír	k1gInSc2	Takbír
<g/>
.	.	kIx.	.
</s>
<s>
Existovalo	existovat	k5eAaImAgNnS	existovat
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
nové	nový	k2eAgFnSc2d1	nová
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Iráku	Irák	k1gInSc2	Irák
</s>
</p>
<p>
<s>
Irácká	irácký	k2eAgFnSc1d1	irácká
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Iráku	Irák	k1gInSc2	Irák
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Irácká	irácký	k2eAgFnSc1d1	irácká
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
