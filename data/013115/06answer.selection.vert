<s>
Film	film	k1gInSc4	film
byl	být	k5eAaImAgMnS	být
magazínem	magazín	k1gInSc7	magazín
Skylink	Skylink	k1gInSc4	Skylink
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
8	[number]	k4	8
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
horor	horor	k1gInSc1	horor
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
magazín	magazín	k1gInSc4	magazín
Time	Tim	k1gFnSc2	Tim
ho	on	k3xPp3gMnSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
svých	svůj	k3xOyFgNnPc2	svůj
Top	topit	k5eAaImRp2nS	topit
<g/>
25	[number]	k4	25
hororů	horor	k1gInPc2	horor
a	a	k8xC	a
George	Georg	k1gMnSc4	Georg
A.	A.	kA	A.
Romero	Romero	k1gNnSc1	Romero
dokonce	dokonce	k9	dokonce
Soumrak	soumrak	k1gInSc1	soumrak
mrtvých	mrtvý	k1gMnPc2	mrtvý
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
zombie	zombie	k1gFnSc1	zombie
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
