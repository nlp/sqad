<p>
<s>
Soumrak	soumrak	k1gInSc1	soumrak
mrtvých	mrtvý	k1gMnPc2	mrtvý
je	být	k5eAaImIp3nS	být
britsko-francouzský	britskorancouzský	k2eAgInSc1d1	britsko-francouzský
filmový	filmový	k2eAgInSc1d1	filmový
horor	horor	k1gInSc1	horor
podobný	podobný	k2eAgInSc1d1	podobný
americkému	americký	k2eAgInSc3d1	americký
filmu	film	k1gInSc3	film
Úsvit	úsvit	k1gInSc4	úsvit
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
Simon	Simon	k1gMnSc1	Simon
Pegg	Pegg	k1gMnSc1	Pegg
<g/>
,	,	kIx,	,
Nick	Nick	k1gMnSc1	Nick
Frost	Frost	k1gMnSc1	Frost
a	a	k8xC	a
Kate	kat	k1gInSc5	kat
Ashfieldová	Ashfieldový	k2eAgNnPc1d1	Ashfieldový
<g/>
.	.	kIx.	.
</s>
<s>
Soumrak	soumrak	k1gInSc1	soumrak
mrtvých	mrtvý	k1gMnPc2	mrtvý
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
hororů	horor	k1gInPc2	horor
zábavný	zábavný	k2eAgMnSc1d1	zábavný
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
i	i	k9	i
vtipný	vtipný	k2eAgMnSc1d1	vtipný
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
byl	být	k5eAaImAgMnS	být
magazínem	magazín	k1gInSc7	magazín
Skylink	Skylink	k1gInSc4	Skylink
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
8	[number]	k4	8
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
horor	horor	k1gInSc1	horor
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
magazín	magazín	k1gInSc4	magazín
Time	Tim	k1gFnSc2	Tim
ho	on	k3xPp3gMnSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
svých	svůj	k3xOyFgNnPc2	svůj
Top	topit	k5eAaImRp2nS	topit
<g/>
25	[number]	k4	25
hororů	horor	k1gInPc2	horor
a	a	k8xC	a
George	Georg	k1gMnSc4	Georg
A.	A.	kA	A.
Romero	Romero	k1gNnSc1	Romero
dokonce	dokonce	k9	dokonce
Soumrak	soumrak	k1gInSc1	soumrak
mrtvých	mrtvý	k1gMnPc2	mrtvý
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
zombie	zombie	k1gFnSc1	zombie
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Shaun	Shaun	k1gMnSc1	Shaun
je	být	k5eAaImIp3nS	být
hospodský	hospodský	k1gMnSc1	hospodský
povaleč	povaleč	k1gMnSc1	povaleč
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
jen	jen	k9	jen
hádá	hádat	k5eAaImIp3nS	hádat
a	a	k8xC	a
pije	pít	k5eAaImIp3nS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Zajde	zajít	k5eAaPmIp3nS	zajít
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
jeho	jeho	k3xOp3gFnSc2	jeho
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Liz	liz	k1gInSc1	liz
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
od	od	k7c2	od
základu	základ	k1gInSc2	základ
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
probudí	probudit	k5eAaPmIp3nP	probudit
mrtví	mrtvý	k1gMnPc1	mrtvý
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
musí	muset	k5eAaImIp3nS	muset
prchnout	prchnout	k5eAaPmF	prchnout
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
věrným	věrný	k2eAgMnSc7d1	věrný
kamarádem	kamarád	k1gMnSc7	kamarád
Edem	Edem	k?	Edem
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
Liz	liz	k1gInSc1	liz
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
lidmi	člověk	k1gMnPc7	člověk
do	do	k7c2	do
Winchesterské	Winchesterský	k2eAgFnSc2d1	Winchesterská
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zombiím	zombie	k1gFnPc3	zombie
ubránit	ubránit	k5eAaPmF	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
nastanou	nastat	k5eAaPmIp3nP	nastat
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c4	v
ně	on	k3xPp3gInPc4	on
změní	změnit	k5eAaPmIp3nP	změnit
skoro	skoro	k6eAd1	skoro
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Shaun	Shaun	k1gNnSc4	Shaun
zná	znát	k5eAaImIp3nS	znát
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
se	se	k3xPyFc4	se
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
úniku	únik	k1gInSc2	únik
<g/>
.	.	kIx.	.
</s>
<s>
Zombie	Zombie	k1gFnPc1	Zombie
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
zabijí	zabít	k5eAaPmIp3nP	zabít
všechny	všechen	k3xTgFnPc4	všechen
kromě	kromě	k7c2	kromě
Eda	Eda	k1gMnSc1	Eda
<g/>
,	,	kIx,	,
Shauna	Shauna	k1gFnSc1	Shauna
a	a	k8xC	a
Liz	liz	k1gInSc1	liz
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
je	být	k5eAaImIp3nS	být
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bojuje	bojovat	k5eAaImIp3nS	bojovat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Shaun	Shaun	k1gInSc1	Shaun
s	s	k7c7	s
Liz	liz	k1gInSc1	liz
utečou	utéct	k5eAaPmIp3nP	utéct
z	z	k7c2	z
hospody	hospody	k?	hospody
a	a	k8xC	a
tam	tam	k6eAd1	tam
je	on	k3xPp3gInPc4	on
zachrání	zachránit	k5eAaPmIp3nP	zachránit
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Shaun	Shaun	k1gInSc4	Shaun
a	a	k8xC	a
Liz	liz	k1gInSc4	liz
se	se	k3xPyFc4	se
vezmou	vzít	k5eAaPmIp3nP	vzít
a	a	k8xC	a
Shaun	Shauna	k1gFnPc2	Shauna
s	s	k7c7	s
sebou	se	k3xPyFc7	se
domů	dům	k1gInPc2	dům
vezme	vzít	k5eAaPmIp3nS	vzít
i	i	k9	i
Eda	Eda	k1gMnSc1	Eda
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zombík	zombík	k1gMnSc1	zombík
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
přeživší	přeživší	k2eAgFnPc1d1	přeživší
zombie	zombie	k1gFnPc1	zombie
jsou	být	k5eAaImIp3nP	být
zajaty	zajmout	k5eAaPmNgFnP	zajmout
a	a	k8xC	a
využity	využít	k5eAaPmNgFnP	využít
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
prospěšné	prospěšný	k2eAgFnPc4d1	prospěšná
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Použitá	použitý	k2eAgFnSc1d1	použitá
hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Figment	Figment	k1gInSc4	Figment
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc4	The
Blue	Blu	k1gMnSc4	Blu
Wrath	Wrath	k1gMnSc1	Wrath
<g/>
"	"	kIx"	"
-	-	kIx~	-
I	i	k9	i
Monster	monstrum	k1gNnPc2	monstrum
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Mister	mister	k1gMnSc1	mister
Mental	Mental	k1gMnSc1	Mental
<g/>
"	"	kIx"	"
-	-	kIx~	-
Eighties	Eighties	k1gInSc1	Eighties
Matchbox	Matchbox	k1gInSc1	Matchbox
B-Line	B-Lin	k1gInSc5	B-Lin
Disaster	Disaster	k1gInSc4	Disaster
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Meltdown	Meltdown	k1gInSc4	Meltdown
<g/>
"	"	kIx"	"
-	-	kIx~	-
Ash	Ash	k1gFnSc1	Ash
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
Me	Me	k1gMnSc1	Me
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
-	-	kIx~	-
Queen	Queen	k1gInSc1	Queen
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Lines	Lines	k1gMnSc1	Lines
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Do	do	k7c2	do
It	It	k1gFnSc2	It
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
Flash	Flash	k1gMnSc1	Flash
<g/>
,	,	kIx,	,
Melle	Melle	k1gFnSc1	Melle
Mel	mlít	k5eAaImRp2nS	mlít
y	y	k?	y
The	The	k1gMnSc4	The
Furious	Furious	k1gMnSc1	Furious
Five	Fiv	k1gMnSc4	Fiv
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hip	hip	k0	hip
Hop	hop	k0	hop
<g/>
,	,	kIx,	,
Be	Be	k1gMnSc1	Be
Bop	bop	k1gInSc1	bop
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
Man	Man	k1gMnSc1	Man
Parrish	Parrish	k1gMnSc1	Parrish
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnSc1	Zombie
Creeping	Creeping	k1gInSc1	Creeping
Flesh	Flesh	k1gInSc1	Flesh
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Goblin	Goblin	k1gInSc1	Goblin
Zombi	Zomb	k1gFnSc2	Zomb
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Kernkraft	Kernkraft	k1gInSc1	Kernkraft
400	[number]	k4	400
(	(	kIx(	(
<g/>
Osymyso	Osymysa	k1gFnSc5	Osymysa
Mix	mix	k1gInSc1	mix
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
Zombie	Zombie	k1gFnSc1	Zombie
Nation	Nation	k1gInSc1	Nation
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fizzy	Fizz	k1gInPc4	Fizz
Legs	Legs	k1gInSc1	Legs
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Soft	Soft	k?	Soft
<g/>
"	"	kIx"	"
-	-	kIx~	-
Lemon	lemon	k1gInSc1	lemon
Jelly	Jella	k1gFnSc2	Jella
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Death	Deatha	k1gFnPc2	Deatha
Bivouac	Bivouac	k1gInSc1	Bivouac
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Gonk	Gonk	k1gMnSc1	Gonk
(	(	kIx(	(
<g/>
Kid	Kid	k1gFnSc1	Kid
Koala	koala	k1gFnSc1	koala
Remix	Remix	k1gInSc1	Remix
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
The	The	k1gMnSc1	The
Noveltones	Noveltones	k1gMnSc1	Noveltones
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Envy	Envy	k1gInPc4	Envy
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ghost	Ghost	k1gMnSc1	Ghost
Town	Town	k1gMnSc1	Town
<g/>
"	"	kIx"	"
-	-	kIx~	-
The	The	k1gFnSc1	The
Specials	Specialsa	k1gFnPc2	Specialsa
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Blood	Blood	k1gInSc1	Blood
in	in	k?	in
Three	Three	k1gInSc1	Three
Flavours	Flavours	k1gInSc1	Flavours
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Panic	panic	k1gMnSc1	panic
<g/>
"	"	kIx"	"
-	-	kIx~	-
The	The	k1gFnSc1	The
Smiths	Smithsa	k1gFnPc2	Smithsa
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Everybody	Everybod	k1gInPc4	Everybod
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Happy	Happ	k1gMnPc7	Happ
Nowadays	Nowadaysa	k1gFnPc2	Nowadaysa
<g/>
"	"	kIx"	"
-	-	kIx~	-
Ash	Ash	k1gFnSc1	Ash
y	y	k?	y
Chris	Chris	k1gInSc1	Chris
MartinNota	MartinNota	k1gFnSc1	MartinNota
1	[number]	k4	1
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
You	You	k1gMnPc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
My	my	k3xPp1nPc1	my
Best	Best	k1gMnSc1	Best
Friend	Friend	k1gInSc1	Friend
<g/>
"	"	kIx"	"
-	-	kIx~	-
Queen	Queen	k1gInSc1	Queen
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
You	You	k1gFnPc6	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
Red	Red	k1gFnSc2	Red
on	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
/	/	kIx~	/
Shaun	Shaun	k1gMnSc1	Shaun
of	of	k?	of
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
Suite	Suit	k1gInSc5	Suit
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Normality	normalita	k1gFnSc2	normalita
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fundead	Fundead	k1gInSc4	Fundead
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
příliš	příliš	k6eAd1	příliš
rychlého	rychlý	k2eAgNnSc2d1	rychlé
nasazení	nasazení	k1gNnSc2	nasazení
</s>
</p>
<p>
<s>
Grindhouse	Grindhouse	k1gFnSc1	Grindhouse
<g/>
:	:	kIx,	:
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
</s>
</p>
<p>
<s>
U	u	k7c2	u
Konce	konec	k1gInSc2	konec
světa	svět	k1gInSc2	svět
</s>
</p>
