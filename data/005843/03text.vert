<s>
Džíny	džíny	k1gFnPc1	džíny
(	(	kIx(	(
<g/>
džínsy	džínsy	k1gFnPc1	džínsy
<g/>
,	,	kIx,	,
jeans	jeans	k1gInSc1	jeans
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
rifle	rifle	k1gFnPc1	rifle
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
texasky	texasky	k6eAd1	texasky
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kalhoty	kalhoty	k1gFnPc1	kalhoty
šité	šitý	k2eAgFnPc1d1	šitá
ze	z	k7c2	z
silné	silný	k2eAgFnSc2d1	silná
bavlněné	bavlněný	k2eAgFnSc2d1	bavlněná
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
džínovina	džínovina	k1gFnSc1	džínovina
<g/>
,	,	kIx,	,
denim	denim	k?	denim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pracovní	pracovní	k2eAgFnPc4d1	pracovní
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
i	i	k9	i
jako	jako	k9	jako
módní	módní	k2eAgFnPc4d1	módní
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejprve	nejprve	k6eAd1	nejprve
mezi	mezi	k7c7	mezi
mládeží	mládež	k1gFnSc7	mládež
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
generací	generace	k1gFnPc2	generace
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nápad	nápad	k1gInSc4	nápad
vyrábět	vyrábět	k5eAaImF	vyrábět
ze	z	k7c2	z
stanové	stanový	k2eAgFnSc2d1	stanová
látky	látka	k1gFnSc2	látka
"	"	kIx"	"
<g/>
jeans	jeans	k1gInSc1	jeans
<g/>
"	"	kIx"	"
kalhoty	kalhoty	k1gFnPc1	kalhoty
přišel	přijít	k5eAaPmAgInS	přijít
bavorský	bavorský	k2eAgMnSc1d1	bavorský
Němec	Němec	k1gMnSc1	Němec
Levi	Lev	k1gFnSc2	Lev
Strauss	Strauss	k1gInSc1	Strauss
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
přistěhoval	přistěhovat	k5eAaPmAgInS	přistěhovat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
byli	být	k5eAaImAgMnP	být
zlatokopové	zlatokop	k1gMnPc1	zlatokop
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
praktických	praktický	k2eAgFnPc2d1	praktická
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
,	,	kIx,	,
šitých	šitý	k2eAgInPc2d1	šitý
původně	původně	k6eAd1	původně
z	z	k7c2	z
janovské	janovský	k2eAgFnSc2d1	janovská
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
francouzské	francouzský	k2eAgFnSc2d1	francouzská
podoby	podoba	k1gFnSc2	podoba
italského	italský	k2eAgNnSc2d1	italské
města	město	k1gNnSc2	město
Janov	Janov	k1gInSc1	Janov
–	–	k?	–
Gê	Gê	k1gFnSc2	Gê
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
džíns	džínsy	k1gFnPc2	džínsy
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
celtoviny	celtovina	k1gFnSc2	celtovina
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
patentovány	patentovat	k5eAaBmNgFnP	patentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
původní	původní	k2eAgInSc1d1	původní
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
Levi	Lev	k1gFnSc2	Lev
Strauss	Strauss	k1gInSc1	Strauss
Co	co	k3yQnSc1	co
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Original	Original	k1gFnSc7	Original
Riveted	Riveted	k1gMnSc1	Riveted
501	[number]	k4	501
range	range	k1gInSc1	range
<g/>
,	,	kIx,	,
with	with	k1gInSc1	with
button	button	k1gInSc1	button
fly	fly	k?	fly
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
džíny	džíny	k1gFnPc1	džíny
s	s	k7c7	s
cvočky	cvoček	k1gMnPc7	cvoček
a	a	k8xC	a
poklopcem	poklopec	k1gInSc7	poklopec
na	na	k7c4	na
knoflíky	knoflík	k1gMnPc4	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
cena	cena	k1gFnSc1	cena
džínů	džíny	k1gInPc2	džíny
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
1	[number]	k4	1
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
některých	některý	k3yIgFnPc2	některý
moderních	moderní	k2eAgFnPc2d1	moderní
džín	džíny	k1gFnPc2	džíny
nedá	dát	k5eNaPmIp3nS	dát
srovnat	srovnat	k5eAaPmF	srovnat
<g/>
.	.	kIx.	.
</s>
<s>
Bruntálský	bruntálský	k2eAgMnSc1d1	bruntálský
historik	historik	k1gMnSc1	historik
Petr	Petr	k1gMnSc1	Petr
Anderle	Anderle	k1gMnSc1	Anderle
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
džínovou	džínový	k2eAgFnSc4d1	džínová
látku	látka	k1gFnSc4	látka
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
utkal	utkat	k5eAaPmAgInS	utkat
Gustav	Gustav	k1gMnSc1	Gustav
Marburg	Marburg	k1gMnSc1	Marburg
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
majitel	majitel	k1gMnSc1	majitel
největší	veliký	k2eAgFnSc2d3	veliký
textilky	textilka	k1gFnSc2	textilka
v	v	k7c6	v
Bruntále	Bruntál	k1gInSc6	Bruntál
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
omylem	omylem	k6eAd1	omylem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
porušil	porušit	k5eAaPmAgMnS	porušit
běžný	běžný	k2eAgInSc4d1	běžný
technologický	technologický	k2eAgInSc4d1	technologický
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
tak	tak	k9	tak
však	však	k9	však
velice	velice	k6eAd1	velice
hrubou	hrubý	k2eAgFnSc4d1	hrubá
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ale	ale	k9	ale
neměl	mít	k5eNaImAgMnS	mít
jak	jak	k6eAd1	jak
upotřebit	upotřebit	k5eAaPmF	upotřebit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
hodně	hodně	k6eAd1	hodně
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vzít	vzít	k5eAaPmF	vzít
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
a	a	k8xC	a
tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dohadů	dohad	k1gInPc2	dohad
ji	on	k3xPp3gFnSc4	on
tam	tam	k6eAd1	tam
koupil	koupit	k5eAaPmAgMnS	koupit
americký	americký	k2eAgMnSc1d1	americký
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
poté	poté	k6eAd1	poté
odvezl	odvézt	k5eAaPmAgInS	odvézt
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
právě	právě	k6eAd1	právě
Levi	Leve	k1gFnSc4	Leve
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
džíny	džíny	k1gFnPc4	džíny
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
(	(	kIx(	(
<g/>
blue	blue	k1gFnSc1	blue
<g/>
)	)	kIx)	)
jeans	jeans	k1gInSc1	jeans
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
riflí	rifle	k1gFnPc2	rifle
používalo	používat	k5eAaImAgNnS	používat
<g/>
:	:	kIx,	:
bleu	bleu	k6eAd1	bleu
de	de	k?	de
Gê	Gê	k1gFnSc1	Gê
<g/>
,	,	kIx,	,
janovská	janovský	k2eAgFnSc1d1	janovská
modř	modř	k1gFnSc1	modř
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
rifle	rifle	k1gFnPc1	rifle
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
italské	italský	k2eAgFnSc2d1	italská
oděvní	oděvní	k2eAgFnSc2d1	oděvní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
džíny	džíny	k1gFnPc4	džíny
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zřídka	zřídka	k6eAd1	zřídka
používaný	používaný	k2eAgInSc1d1	používaný
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
texasky	texasky	k6eAd1	texasky
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
texaských	texaský	k2eAgMnPc2d1	texaský
kovbojů	kovboj	k1gMnPc2	kovboj
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
kalhoty	kalhoty	k1gFnPc4	kalhoty
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
džegíny	džegína	k1gFnSc2	džegína
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
výrobek	výrobek	k1gInSc4	výrobek
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
slov	slovo	k1gNnPc2	slovo
džíny	džíny	k1gInPc7	džíny
a	a	k8xC	a
legíny	legíny	k1gFnPc4	legíny
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgInPc1d1	klasický
džíny	džíny	k1gInPc1	džíny
se	se	k3xPyFc4	se
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
jako	jako	k9	jako
oblečení	oblečení	k1gNnSc4	oblečení
pro	pro	k7c4	pro
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Džegíny	Džegína	k1gFnPc1	Džegína
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
džíny	džíny	k1gInPc7	džíny
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
džíny	džíny	k1gFnPc1	džíny
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
