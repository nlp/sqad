<s>
Nápoj	nápoj	k1gInSc1	nápoj
zvaný	zvaný	k2eAgInSc1d1	zvaný
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
绿	绿	k?	绿
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
lǜ	lǜ	k?	lǜ
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
緑	緑	k?	緑
<g/>
,	,	kIx,	,
rjokuča	rjokuča	k1gFnSc1	rjokuča
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
pravý	pravý	k2eAgInSc4d1	pravý
<g/>
"	"	kIx"	"
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
zpracování	zpracování	k1gNnSc6	zpracování
prošel	projít	k5eAaPmAgMnS	projít
minimální	minimální	k2eAgFnSc7d1	minimální
oxidací	oxidace	k1gFnSc7	oxidace
<g/>
.	.	kIx.	.
</s>
