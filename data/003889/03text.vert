<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Bolívarovská	Bolívarovský	k2eAgFnSc1d1	Bolívarovská
republika	republika	k1gFnSc1	republika
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
República	Repúblic	k2eAgFnSc1d1	República
Bolivariana	Bolivariana	k1gFnSc1	Bolivariana
de	de	k?	de
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
severně	severně	k6eAd1	severně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnSc1d3	veliký
je	být	k5eAaImIp3nS	být
Isla	Isl	k2eAgFnSc1d1	Isla
de	de	k?	de
Margarita	Margarita	k1gFnSc1	Margarita
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Guyanou	Guyana	k1gFnSc7	Guyana
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
západní	západní	k2eAgFnSc4d1	západní
polovinu	polovina	k1gFnSc4	polovina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Guayana	Guayana	k1gFnSc1	Guayana
Esequiba	Esequiba	k1gMnSc1	Esequiba
či	či	k8xC	či
"	"	kIx"	"
<g/>
Zona	Zona	k1gFnSc1	Zona
en	en	k?	en
Reclamación	Reclamación	k1gInSc1	Reclamación
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
Venezuela	Venezuela	k1gFnSc1	Venezuela
činí	činit	k5eAaImIp3nS	činit
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
severněji	severně	k6eAd2	severně
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nS	ležet
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g/>
,	,	kIx,	,
Curaçao	curaçao	k1gNnSc1	curaçao
<g/>
,	,	kIx,	,
Bonaire	Bonair	k1gInSc5	Bonair
a	a	k8xC	a
Aruba	Arub	k1gMnSc4	Arub
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
28,8	[number]	k4	28,8
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Caracas	Caracas	k1gInSc1	Caracas
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
španělskou	španělský	k2eAgFnSc7d1	španělská
zdrobnělinou	zdrobnělina	k1gFnSc7	zdrobnělina
názvu	název	k1gInSc2	název
Venezia	Venezium	k1gNnSc2	Venezium
(	(	kIx(	(
<g/>
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Adjektivum	adjektivum	k1gNnSc1	adjektivum
"	"	kIx"	"
<g/>
bolívarovská	bolívarovský	k2eAgFnSc1d1	bolívarovská
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
postavě	postava	k1gFnSc3	postava
Simóna	Simón	k1gMnSc2	Simón
Bolívara	Bolívar	k1gMnSc2	Bolívar
<g/>
,	,	kIx,	,
místního	místní	k2eAgMnSc2d1	místní
slavného	slavný	k2eAgMnSc2d1	slavný
rodáka	rodák	k1gMnSc2	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
objevil	objevit	k5eAaPmAgMnS	objevit
pobřeží	pobřeží	k1gNnSc4	pobřeží
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
započala	započnout	k5eAaPmAgFnS	započnout
kolonizace	kolonizace	k1gFnSc1	kolonizace
území	území	k1gNnSc2	území
založením	založení	k1gNnSc7	založení
první	první	k4xOgFnSc6	první
trvale	trvale	k6eAd1	trvale
obývané	obývaný	k2eAgFnSc2d1	obývaná
španělské	španělský	k2eAgFnSc2d1	španělská
osady	osada	k1gFnSc2	osada
na	na	k7c6	na
jihoamerickém	jihoamerický	k2eAgInSc6d1	jihoamerický
kontinentu	kontinent	k1gInSc6	kontinent
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Cumaná	Cumaná	k1gFnSc1	Cumaná
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Venezuely	Venezuela	k1gFnSc2	Venezuela
bylo	být	k5eAaImAgNnS	být
dále	daleko	k6eAd2	daleko
součástí	součást	k1gFnSc7	součást
španělského	španělský	k2eAgNnSc2d1	španělské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1717	[number]	k4	1717
spadalo	spadat	k5eAaImAgNnS	spadat
pod	pod	k7c4	pod
místokrálovství	místokrálovství	k1gNnSc4	místokrálovství
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
generální	generální	k2eAgInSc1d1	generální
kapitanát	kapitanát	k1gInSc1	kapitanát
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamenalo	znamenat	k5eAaImAgNnS	znamenat
získání	získání	k1gNnSc1	získání
výrazné	výrazný	k2eAgFnSc2d1	výrazná
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
autonomie	autonomie	k1gFnSc2	autonomie
a	a	k8xC	a
samosprávy	samospráva	k1gFnSc2	samospráva
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
španělských	španělský	k2eAgFnPc2d1	španělská
amerických	americký	k2eAgFnPc2d1	americká
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1811	[number]	k4	1811
byla	být	k5eAaImAgFnS	být
deklarována	deklarován	k2eAgFnSc1d1	deklarována
nezávislost	nezávislost	k1gFnSc1	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
venezuelských	venezuelský	k2eAgInPc2d1	venezuelský
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Venezuely	Venezuela	k1gFnSc2	Venezuela
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
států	stát	k1gInPc2	stát
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
venezuelský	venezuelský	k2eAgMnSc1d1	venezuelský
rodák	rodák	k1gMnSc1	rodák
Simón	Simón	k1gMnSc1	Simón
Bolívar	Bolívar	k1gInSc4	Bolívar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Venezuela	Venezuela	k1gFnSc1	Venezuela
součástí	součást	k1gFnSc7	součást
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
zahynula	zahynout	k5eAaPmAgFnS	zahynout
až	až	k9	až
třetina	třetina	k1gFnSc1	třetina
venezuelské	venezuelský	k2eAgFnSc2d1	venezuelská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
konzervativci	konzervativec	k1gMnPc7	konzervativec
a	a	k8xC	a
liberály	liberál	k1gMnPc7	liberál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1902	[number]	k4	1902
a	a	k8xC	a
1903	[number]	k4	1903
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
Británie	Británie	k1gFnPc1	Británie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
námořní	námořní	k2eAgFnSc1d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
přinutily	přinutit	k5eAaPmAgInP	přinutit
splácet	splácet	k5eAaImF	splácet
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnPc1	území
Venezuely	Venezuela	k1gFnSc2	Venezuela
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
jsou	být	k5eAaImIp3nP	být
Andy	Anda	k1gFnPc1	Anda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Venezuely	Venezuela	k1gFnSc2	Venezuela
Pico	Pico	k1gNnSc1	Pico
Bolívar	Bolívar	k1gInSc1	Bolívar
(	(	kIx(	(
<g/>
4979	[number]	k4	4979
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
položena	položen	k2eAgFnSc1d1	položena
hluboká	hluboký	k2eAgFnSc1d1	hluboká
tektonická	tektonický	k2eAgFnSc1d1	tektonická
poklesová	poklesový	k2eAgFnSc1d1	poklesová
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
jezerem	jezero	k1gNnSc7	jezero
Maracaibo	Maracaiba	k1gFnSc5	Maracaiba
a	a	k8xC	a
rozlehlou	rozlehlý	k2eAgFnSc7d1	rozlehlá
Maracaibskou	Maracaibský	k2eAgFnSc7d1	Maracaibský
nížinou	nížina	k1gFnSc7	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Cordilerra	Cordilerr	k1gInSc2	Cordilerr
de	de	k?	de
Mérida	Mérida	k1gFnSc1	Mérida
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
pásmo	pásmo	k1gNnSc1	pásmo
Karibských	karibský	k2eAgFnPc2d1	karibská
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Orinoco	Orinoco	k6eAd1	Orinoco
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
Guyanská	Guyanský	k2eAgFnSc1d1	Guyanská
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
s	s	k7c7	s
ojedinělými	ojedinělý	k2eAgInPc7d1	ojedinělý
svědeckými	svědecký	k2eAgInPc7d1	svědecký
vrchy	vrch	k1gInPc7	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
celky	celek	k1gInPc7	celek
se	se	k3xPyFc4	se
prostírá	prostírat	k5eAaImIp3nS	prostírat
Orinocká	orinocký	k2eAgFnSc1d1	Orinocká
nížina	nížina	k1gFnSc1	nížina
s	s	k7c7	s
plošinou	plošina	k1gFnSc7	plošina
Llanos	Llanos	k1gInPc2	Llanos
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Venezuely	Venezuela	k1gFnSc2	Venezuela
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
52	[number]	k4	52
%	%	kIx~	%
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
23	[number]	k4	23
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
řekou	řeka	k1gFnSc7	řeka
Venezuely	Venezuela	k1gFnSc2	Venezuela
je	být	k5eAaImIp3nS	být
Orinoco	Orinoco	k6eAd1	Orinoco
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodopády	vodopád	k1gInPc4	vodopád
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opouští	opouštět	k5eAaImIp3nS	opouštět
Guyanskou	Guyanský	k2eAgFnSc4d1	Guyanská
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vodopád	vodopád	k1gInSc1	vodopád
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
:	:	kIx,	:
Salto	salto	k1gNnSc1	salto
Ángel	Ángela	k1gFnPc2	Ángela
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
979	[number]	k4	979
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
přítoky	přítok	k1gInPc7	přítok
Orinoka	Orinoko	k1gNnSc2	Orinoko
jsou	být	k5eAaImIp3nP	být
Caroní	Caroň	k1gFnSc7	Caroň
<g/>
,	,	kIx,	,
Apure	Apur	k1gInSc5	Apur
a	a	k8xC	a
Meta	meta	k1gFnSc1	meta
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
limanové	limanový	k2eAgNnSc1d1	limanový
jezero	jezero	k1gNnSc1	jezero
Maracaibo	Maracaiba	k1gMnSc5	Maracaiba
<g/>
.	.	kIx.	.
</s>
<s>
Venezuele	Venezuela	k1gFnSc3	Venezuela
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
několik	několik	k4yIc4	několik
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
geopolitického	geopolitický	k2eAgNnSc2d1	geopolitické
hlediska	hledisko	k1gNnSc2	hledisko
součástí	součást	k1gFnPc2	součást
státu	stát	k1gInSc2	stát
Nueva	Nuevo	k1gNnSc2	Nuevo
Esparta	esparto	k1gNnSc2	esparto
a	a	k8xC	a
venezuelských	venezuelský	k2eAgFnPc2d1	venezuelská
federálních	federální	k2eAgFnPc2d1	federální
dependencí	dependence	k1gFnPc2	dependence
<g/>
–	–	k?	–
největší	veliký	k2eAgFnSc2d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Isla	Isl	k2eAgFnSc1d1	Isla
de	de	k?	de
Margarita	Margarita	k1gFnSc1	Margarita
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
poloostrovy	poloostrov	k1gInPc1	poloostrov
jsou	být	k5eAaImIp3nP	být
Paraguaná	Paraguaná	k1gFnSc1	Paraguaná
<g/>
,	,	kIx,	,
Paria	Paria	k1gFnSc1	Paria
a	a	k8xC	a
Guajira	Guajira	k1gFnSc1	Guajira
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
Venezuely	Venezuela	k1gFnSc2	Venezuela
je	být	k5eAaImIp3nS	být
subekvatoriální	subekvatoriální	k2eAgNnSc1d1	subekvatoriální
horké	horký	k2eAgNnSc1d1	horké
s	s	k7c7	s
deštivým	deštivý	k2eAgNnSc7d1	deštivé
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
suchou	suchý	k2eAgFnSc7d1	suchá
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maracaibu	Maracaib	k1gInSc6	Maracaib
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
29	[number]	k4	29
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
570	[number]	k4	570
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
položená	položený	k2eAgFnSc1d1	položená
Mérida	Mérida	k1gFnSc1	Mérida
18,5	[number]	k4	18,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
1750	[number]	k4	1750
mm	mm	kA	mm
<g/>
,	,	kIx,	,
Caracas	Caracas	k1gInSc1	Caracas
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Elena	Elena	k1gFnSc1	Elena
v	v	k7c6	v
Guyanské	Guyanský	k2eAgFnSc6d1	Guyanská
vysočině	vysočina	k1gFnSc6	vysočina
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
1600	[number]	k4	1600
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
je	být	k5eAaImIp3nS	být
zapojená	zapojený	k2eAgFnSc1d1	zapojená
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
např.	např.	kA	např.
organizací	organizace	k1gFnSc7	organizace
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Unie	unie	k1gFnSc1	unie
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Mercosur	Mercosura	k1gFnPc2	Mercosura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
bývalého	bývalý	k2eAgMnSc2d1	bývalý
venezuelského	venezuelský	k2eAgMnSc2d1	venezuelský
prezidenta	prezident	k1gMnSc2	prezident
Huga	Hugo	k1gMnSc2	Hugo
Cháveze	Cháveze	k1gFnSc1	Cháveze
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spolky	spolek	k1gInPc1	spolek
Petrocaribe	Petrocarib	k1gInSc5	Petrocarib
a	a	k8xC	a
Bolívarovský	Bolívarovský	k2eAgInSc1d1	Bolívarovský
svaz	svaz	k1gInSc1	svaz
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
naší	náš	k3xOp1gFnSc2	náš
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
hraje	hrát	k5eAaImIp3nS	hrát
Venezuela	Venezuela	k1gFnSc1	Venezuela
hlavní	hlavní	k2eAgFnSc1d1	hlavní
roli	role	k1gFnSc3	role
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Státy	stát	k1gInPc1	stát
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
je	být	k5eAaImIp3nS	být
federací	federace	k1gFnSc7	federace
23	[number]	k4	23
států	stát	k1gInPc2	stát
a	a	k8xC	a
2	[number]	k4	2
dalších	další	k2eAgInPc2d1	další
státotvorných	státotvorný	k2eAgInPc2d1	státotvorný
útvarů	útvar	k1gInPc2	útvar
<g/>
:	:	kIx,	:
Distrito	Distrita	k1gFnSc5	Distrita
capital	capital	k1gMnSc1	capital
a	a	k8xC	a
Dependencias	Dependencias	k1gMnSc1	Dependencias
Federales	Federales	k1gMnSc1	Federales
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
je	být	k5eAaImIp3nS	být
zemědělsko-průmyslový	zemědělskorůmyslový	k2eAgInSc4d1	zemědělsko-průmyslový
stát	stát	k1gInSc4	stát
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
těžbou	těžba	k1gFnSc7	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
bauxit	bauxit	k1gInSc1	bauxit
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
a	a	k8xC	a
vápenec	vápenec	k1gInSc1	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
petrochemie	petrochemie	k1gFnSc1	petrochemie
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
hutnictví	hutnictví	k1gNnSc1	hutnictví
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
potravinářství	potravinářství	k1gNnSc2	potravinářství
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
produkují	produkovat	k5eAaImIp3nP	produkovat
vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pro	pro	k7c4	pro
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
sklizeň	sklizeň	k1gFnSc1	sklizeň
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
sorga	sorg	k1gMnSc2	sorg
<g/>
,	,	kIx,	,
manioku	maniok	k1gInSc2	maniok
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc2	slunečnice
<g/>
,	,	kIx,	,
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnSc2	luštěnina
<g/>
,	,	kIx,	,
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
kokosových	kokosový	k2eAgInPc2d1	kokosový
ořechů	ořech	k1gInPc2	ořech
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
banánů	banán	k1gInPc2	banán
<g/>
,	,	kIx,	,
citrusů	citrus	k1gInPc2	citrus
<g/>
,	,	kIx,	,
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
ananasů	ananas	k1gInPc2	ananas
<g/>
,	,	kIx,	,
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
kakaa	kakao	k1gNnSc2	kakao
<g/>
,	,	kIx,	,
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
sisalu	sisal	k1gInSc2	sisal
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
skot	skot	k1gInSc4	skot
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc4	drůbež
a	a	k8xC	a
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
rybolov	rybolov	k1gInSc4	rybolov
i	i	k9	i
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
silniční	silniční	k2eAgFnSc4d1	silniční
a	a	k8xC	a
leteckou	letecký	k2eAgFnSc4d1	letecká
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Surová	surový	k2eAgFnSc1d1	surová
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
produkty	produkt	k1gInPc1	produkt
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
představují	představovat	k5eAaImIp3nP	představovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
93	[number]	k4	93
%	%	kIx~	%
venezuelského	venezuelský	k2eAgInSc2d1	venezuelský
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Socialismus	socialismus	k1gInSc1	socialismus
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
"	"	kIx"	"
<g/>
Socialismu	socialismus	k1gInSc2	socialismus
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
zavedená	zavedený	k2eAgFnSc1d1	zavedená
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Huga	Hugo	k1gMnSc2	Hugo
Cháveze	Cháveze	k1gFnSc2	Cháveze
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nedostatek	nedostatek	k1gInSc1	nedostatek
základních	základní	k2eAgFnPc2d1	základní
komodit	komodita	k1gFnPc2	komodita
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
toaletní	toaletní	k2eAgInSc4d1	toaletní
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
mouka	mouka	k1gFnSc1	mouka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
měla	mít	k5eAaImAgFnS	mít
Venezuela	Venezuela	k1gFnSc1	Venezuela
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
inflaci	inflace	k1gFnSc4	inflace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
63,4	[number]	k4	63,4
<g/>
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tajné	tajný	k2eAgFnSc2d1	tajná
devalvace	devalvace	k1gFnSc2	devalvace
měny	měna	k1gFnSc2	měna
požadovala	požadovat	k5eAaImAgFnS	požadovat
po	po	k7c6	po
obchodních	obchodní	k2eAgFnPc6d1	obchodní
společnostech	společnost	k1gFnPc6	společnost
zaplatit	zaplatit	k5eAaPmF	zaplatit
za	za	k7c7	za
dolary	dolar	k1gInPc7	dolar
o	o	k7c4	o
61	[number]	k4	61
<g/>
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
politiky	politika	k1gFnSc2	politika
"	"	kIx"	"
<g/>
socialismu	socialismus	k1gInSc2	socialismus
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgMnSc1d1	dosahující
63	[number]	k4	63
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
bezkonkurenčně	bezkonkurenčně	k6eAd1	bezkonkurenčně
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
číslo	číslo	k1gNnSc1	číslo
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
enormní	enormní	k2eAgInSc4d1	enormní
nárůst	nárůst	k1gInSc4	nárůst
veškerých	veškerý	k3xTgFnPc2	veškerý
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomové	ekonom	k1gMnPc1	ekonom
většinou	většinou	k6eAd1	většinou
vinu	vina	k1gFnSc4	vina
svalují	svalovat	k5eAaImIp3nP	svalovat
na	na	k7c4	na
prudké	prudký	k2eAgNnSc4d1	prudké
zvyšování	zvyšování	k1gNnSc4	zvyšování
peněžní	peněžní	k2eAgFnSc2d1	peněžní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zdaleka	zdaleka	k6eAd1	zdaleka
překračuje	překračovat	k5eAaImIp3nS	překračovat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Poukazují	poukazovat	k5eAaImIp3nP	poukazovat
i	i	k9	i
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgNnSc3	jenž
mnoho	mnoho	k4c4	mnoho
firem	firma	k1gFnPc2	firma
nemůže	moct	k5eNaImIp3nS	moct
dovážet	dovážet	k5eAaImF	dovážet
suroviny	surovina	k1gFnPc4	surovina
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Potraviny	potravina	k1gFnPc1	potravina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
běžně	běžně	k6eAd1	běžně
na	na	k7c4	na
příděl	příděl	k1gInSc4	příděl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
téměř	téměř	k6eAd1	téměř
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
toaletního	toaletní	k2eAgInSc2d1	toaletní
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Chavez	Chavez	k1gInSc1	Chavez
odradil	odradit	k5eAaPmAgInS	odradit
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
investice	investice	k1gFnPc4	investice
a	a	k8xC	a
státní	státní	k2eAgFnSc4d1	státní
ropnou	ropný	k2eAgFnSc4d1	ropná
společnost	společnost	k1gFnSc4	společnost
poškodil	poškodit	k5eAaPmAgInS	poškodit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
produkce	produkce	k1gFnSc1	produkce
klesla	klesnout	k5eAaPmAgFnS	klesnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
o	o	k7c4	o
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
klesají	klesat	k5eAaImIp3nP	klesat
i	i	k9	i
exporty	export	k1gInPc1	export
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
snažil	snažit	k5eAaImAgMnS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
chudým	chudý	k1gMnSc7	chudý
<g/>
,	,	kIx,	,
utratil	utratit	k5eAaPmAgMnS	utratit
při	při	k7c6	při
tom	ten	k3xDgMnSc6	ten
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
dnes	dnes	k6eAd1	dnes
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
deficitů	deficit	k1gInPc2	deficit
ve	v	k7c4	v
výši	výše	k1gFnSc4	výše
14	[number]	k4	14
%	%	kIx~	%
HDP	HDP	kA	HDP
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
mezeru	mezera	k1gFnSc4	mezera
lze	lze	k6eAd1	lze
zaplnit	zaplnit	k5eAaPmF	zaplnit
pouze	pouze	k6eAd1	pouze
tištěním	tištění	k1gNnSc7	tištění
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
inflace	inflace	k1gFnSc1	inflace
oficiálně	oficiálně	k6eAd1	oficiálně
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
64	[number]	k4	64
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
kolem	kolem	k7c2	kolem
179	[number]	k4	179
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bank	bank	k1gInSc1	bank
of	of	k?	of
America	America	k1gFnSc1	America
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
systém	systém	k1gInSc1	systém
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
,	,	kIx,	,
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
se	se	k3xPyFc4	se
inflace	inflace	k1gFnSc1	inflace
možná	možná	k9	možná
až	až	k9	až
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
%	%	kIx~	%
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
výkon	výkon	k1gInSc4	výkon
Venezuelské	venezuelský	k2eAgFnSc2d1	venezuelská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
HDP	HDP	kA	HDP
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
byl	být	k5eAaImAgMnS	být
závažných	závažný	k2eAgNnPc2d1	závažné
4,8	[number]	k4	4,8
<g/>
%	%	kIx~	%
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Venezuela	Venezuela	k1gFnSc1	Venezuela
čelí	čelit	k5eAaImIp3nS	čelit
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
sedmdesát	sedmdesát	k4xCc4	sedmdesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Měna	měna	k1gFnSc1	měna
je	být	k5eAaImIp3nS	být
znehodnocená	znehodnocený	k2eAgFnSc1d1	znehodnocená
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
inflací	inflace	k1gFnSc7	inflace
<g/>
,	,	kIx,	,
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
dochází	docházet	k5eAaImIp3nS	docházet
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
už	už	k6eAd1	už
přestala	přestat	k5eAaPmAgFnS	přestat
sdělovat	sdělovat	k5eAaImF	sdělovat
i	i	k9	i
základní	základní	k2eAgInPc4d1	základní
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
ukazatele	ukazatel	k1gInPc4	ukazatel
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ekonomů	ekonom	k1gMnPc2	ekonom
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
současná	současný	k2eAgFnSc1d1	současná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
přeroste	přerůst	k5eAaPmIp3nS	přerůst
v	v	k7c4	v
rozpad	rozpad	k1gInSc4	rozpad
politiky	politika	k1gFnSc2	politika
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
politiky	politika	k1gFnSc2	politika
je	být	k5eAaImIp3nS	být
i	i	k9	i
enormní	enormní	k2eAgInSc4d1	enormní
nárůst	nárůst	k1gInSc4	nárůst
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
loupeží	loupež	k1gFnPc2	loupež
a	a	k8xC	a
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
chudinských	chudinský	k2eAgInPc6d1	chudinský
čtvrtí	čtvrtit	k5eAaImIp3nP	čtvrtit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Venezuely	Venezuela	k1gFnSc2	Venezuela
míra	míra	k1gFnSc1	míra
vražd	vražda	k1gFnPc2	vražda
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Násilí	násilí	k1gNnSc1	násilí
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
spojeno	spojit	k5eAaPmNgNnS	spojit
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
z	z	k7c2	z
polohy	poloha	k1gFnSc2	poloha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
loupeže	loupež	k1gFnPc1	loupež
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
běžné	běžný	k2eAgInPc1d1	běžný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
a	a	k8xC	a
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
je	on	k3xPp3gNnSc4	on
turisté	turist	k1gMnPc1	turist
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
rychlé	rychlý	k2eAgInPc1d1	rychlý
únosy	únos	k1gInPc1	únos
osob	osoba	k1gFnPc2	osoba
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
hotovost	hotovost	k1gFnSc4	hotovost
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
horším	zlý	k2eAgInSc7d2	horší
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
sami	sám	k3xTgMnPc1	sám
policisté	policista	k1gMnPc1	policista
se	se	k3xPyFc4	se
na	na	k7c6	na
únosech	únos	k1gInPc6	únos
také	také	k9	také
podílí	podílet	k5eAaImIp3nS	podílet
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
67	[number]	k4	67
%	%	kIx~	%
tvořeno	tvořit	k5eAaImNgNnS	tvořit
mestici	mestic	k1gMnPc1	mestic
<g/>
,	,	kIx,	,
ze	z	k7c2	z
21	[number]	k4	21
%	%	kIx~	%
potomky	potomek	k1gMnPc4	potomek
Evropanů	Evropan	k1gMnPc2	Evropan
(	(	kIx(	(
<g/>
běloši	běloch	k1gMnPc1	běloch
a	a	k8xC	a
kreolové	kreol	k1gMnPc1	kreol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
10	[number]	k4	10
%	%	kIx~	%
potomky	potomek	k1gMnPc4	potomek
Afričanů	Afričan	k1gMnPc2	Afričan
(	(	kIx(	(
<g/>
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
mulati	mulat	k1gMnPc1	mulat
a	a	k8xC	a
zambové	zambo	k1gMnPc1	zambo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
%	%	kIx~	%
(	(	kIx(	(
<g/>
asi	asi	k9	asi
577	[number]	k4	577
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
000	[number]	k4	000
Indiánů	Indián	k1gMnPc2	Indián
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
živí	živit	k5eAaImIp3nS	živit
lovem	lov	k1gInSc7	lov
a	a	k8xC	a
sběrem	sběr	k1gInSc7	sběr
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
kmene	kmen	k1gInSc2	kmen
Janomamů	Janomam	k1gInPc2	Janomam
z	z	k7c2	z
deštných	deštný	k2eAgInPc2d1	deštný
pralesů	prales	k1gInPc2	prales
severní	severní	k2eAgFnSc2d1	severní
Amazonie	Amazonie	k1gFnSc2	Amazonie
byli	být	k5eAaImAgMnP	být
známí	známý	k1gMnPc1	známý
svou	svůj	k3xOyFgFnSc4	svůj
bojovností	bojovnost	k1gFnPc2	bojovnost
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stále	stále	k6eAd1	stále
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
od	od	k7c2	od
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
73,45	[number]	k4	73,45
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
70,4	[number]	k4	70,4
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
76,65	[number]	k4	76,65
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
je	být	k5eAaImIp3nS	být
především	především	k9	především
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
96	[number]	k4	96
%	%	kIx~	%
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
východních	východní	k2eAgMnPc2d1	východní
katolíků	katolík	k1gMnPc2	katolík
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
řeckokatolíci-melchité	řeckokatolícielchitý	k2eAgInPc1d1	řeckokatolíci-melchitý
<g/>
:	:	kIx,	:
Apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
exarchát	exarchát	k1gInSc1	exarchát
ve	v	k7c6	v
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
2015-26	[number]	k4	2015-26
200	[number]	k4	200
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
<g/>
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
,	,	kIx,	,
2014-25	[number]	k4	2014-25
800	[number]	k4	800
<g/>
,	,	kIx,	,
2010-25	[number]	k4	2010-25
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
*	*	kIx~	*
2	[number]	k4	2
<g/>
)	)	kIx)	)
syrští	syrský	k2eAgMnPc1d1	syrský
katolíci	katolík	k1gMnPc1	katolík
<g/>
:	:	kIx,	:
Apoštolský	apoštolský	k2eAgInSc4d1	apoštolský
exarchát	exarchát	k1gInSc4	exarchát
pro	pro	k7c4	pro
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Maracay	Maracaa	k1gFnSc2	Maracaa
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2015-20	[number]	k4	2015-20
600	[number]	k4	600
<g/>
,	,	kIx,	,
<g/>
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
,	,	kIx,	,
2014-20	[number]	k4	2014-20
300	[number]	k4	300
<g/>
,	,	kIx,	,
2010-5	[number]	k4	2010-5
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
i	i	k9	i
protestantského	protestantský	k2eAgInSc2d1	protestantský
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
93	[number]	k4	93
%	%	kIx~	%
(	(	kIx(	(
<g/>
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
starších	starý	k2eAgMnPc2d2	starší
15	[number]	k4	15
let	let	k1gInSc4	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
