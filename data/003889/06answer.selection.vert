<s>
Roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
generální	generální	k2eAgInSc1d1	generální
kapitanát	kapitanát	k1gInSc1	kapitanát
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamenalo	znamenat	k5eAaImAgNnS	znamenat
získání	získání	k1gNnSc1	získání
výrazné	výrazný	k2eAgFnSc2d1	výrazná
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
autonomie	autonomie	k1gFnSc2	autonomie
a	a	k8xC	a
samosprávy	samospráva	k1gFnSc2	samospráva
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
španělských	španělský	k2eAgFnPc2d1	španělská
amerických	americký	k2eAgFnPc2d1	americká
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
