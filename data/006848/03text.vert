<s>
Ožujsko	Ožujsko	k6eAd1	Ožujsko
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
chorvatského	chorvatský	k2eAgNnSc2d1	Chorvatské
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
ležáku	ležák	k1gInSc6	ležák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
Zagrebačka	Zagrebačka	k1gFnSc1	Zagrebačka
pivovara	pivovara	k?	pivovara
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pivem	pivo	k1gNnSc7	pivo
Karlovačko	Karlovačka	k1gFnSc5	Karlovačka
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgNnPc2d3	nejpopulárnější
piv	pivo	k1gNnPc2	pivo
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsky	chorvatsky	k6eAd1	chorvatsky
březen	březen	k1gInSc1	březen
-	-	kIx~	-
ožujak	ožujak	k1gInSc1	ožujak
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
pivo	pivo	k1gNnSc1	pivo
s	s	k7c7	s
názvem	název	k1gInSc7	název
obdobným	obdobný	k2eAgInSc7d1	obdobný
jako	jako	k8xS	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Březňák	březňák	k1gMnSc1	březňák
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ožujsko	Ožujsko	k1gNnSc1	Ožujsko
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
sponzorem	sponzor	k1gMnSc7	sponzor
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábi	Vyrábi	k6eAd1	Vyrábi
se	se	k3xPyFc4	se
v	v	k7c6	v
skleněných	skleněný	k2eAgInPc6d1	skleněný
<g/>
(	(	kIx(	(
<g/>
0,25	[number]	k4	0,25
<g/>
a	a	k8xC	a
0,5	[number]	k4	0,5
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plastových	plastový	k2eAgInPc2d1	plastový
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
lahvích	lahev	k1gFnPc6	lahev
a	a	k8xC	a
plechovkách	plechovka	k1gFnPc6	plechovka
(	(	kIx(	(
<g/>
0,33	[number]	k4	0,33
a	a	k8xC	a
<g/>
0,5	[number]	k4	0,5
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ožujsko	Ožujsko	k1gNnSc4	Ožujsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
