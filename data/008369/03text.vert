<p>
<s>
Loutna	loutna	k1gFnSc1	loutna
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
obecně	obecně	k6eAd1	obecně
označující	označující	k2eAgInSc4d1	označující
typ	typ	k1gInSc4	typ
drnkacího	drnkací	k2eAgInSc2d1	drnkací
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vydutý	vydutý	k2eAgInSc1d1	vydutý
korpus	korpus	k1gInSc1	korpus
(	(	kIx(	(
<g/>
mušle	mušle	k1gFnSc1	mušle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
charakteristikou	charakteristika	k1gFnSc7	charakteristika
nástrojů	nástroj	k1gInPc2	nástroj
loutnové	loutnový	k2eAgFnSc2d1	loutnová
rodiny	rodina	k1gFnSc2	rodina
jsou	být	k5eAaImIp3nP	být
struny	struna	k1gFnPc1	struna
zdvojené	zdvojený	k2eAgFnPc1d1	zdvojená
v	v	k7c6	v
unisonu	unisono	k1gNnSc6	unisono
nebo	nebo	k8xC	nebo
v	v	k7c6	v
oktávě	oktáva	k1gFnSc6	oktáva
(	(	kIx(	(
<g/>
sbory	sbor	k1gInPc1	sbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
loutna	loutna	k1gFnSc1	loutna
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
renesanční	renesanční	k2eAgFnSc1d1	renesanční
loutna	loutna	k1gFnSc1	loutna
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
loutna	loutna	k1gFnSc1	loutna
a	a	k8xC	a
arciloutna	arciloutna	k1gFnSc1	arciloutna
<g/>
;	;	kIx,	;
rodina	rodina	k1gFnSc1	rodina
loutnových	loutnový	k2eAgInPc2d1	loutnový
nástrojů	nástroj	k1gInPc2	nástroj
ale	ale	k8xC	ale
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svou	svůj	k3xOyFgFnSc7	svůj
hudební	hudební	k2eAgFnSc7d1	hudební
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
časovým	časový	k2eAgNnSc7d1	časové
a	a	k8xC	a
geografickým	geografický	k2eAgNnSc7d1	geografické
zařazením	zařazení	k1gNnSc7	zařazení
<g/>
,	,	kIx,	,
systémem	systém	k1gInSc7	systém
ladění	ladění	k1gNnSc2	ladění
<g/>
,	,	kIx,	,
ostruněním	ostrunění	k1gNnSc7	ostrunění
či	či	k8xC	či
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
evropskou	evropský	k2eAgFnSc4d1	Evropská
hudební	hudební	k2eAgFnSc4d1	hudební
tradici	tradice	k1gFnSc4	tradice
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
čínská	čínský	k2eAgFnSc1d1	čínská
loutna	loutna	k1gFnSc1	loutna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Loutna	loutna	k1gFnSc1	loutna
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
nástrojům	nástroj	k1gInPc3	nástroj
lidské	lidský	k2eAgFnSc2d1	lidská
historie	historie	k1gFnSc2	historie
<g/>
;	;	kIx,	;
první	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
loutnovém	loutnový	k2eAgInSc6d1	loutnový
nástroji	nástroj	k1gInSc6	nástroj
najdeme	najít	k5eAaPmIp1nP	najít
již	již	k6eAd1	již
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vydlabanou	vydlabaný	k2eAgFnSc4d1	vydlabaná
dýni	dýně	k1gFnSc4	dýně
potaženou	potažený	k2eAgFnSc7d1	potažená
zvířecí	zvířecí	k2eAgFnSc7d1	zvířecí
kůží	kůže	k1gFnSc7	kůže
a	a	k8xC	a
opatřenou	opatřený	k2eAgFnSc4d1	opatřená
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
krkem	krk	k1gInSc7	krk
se	s	k7c7	s
strunami	struna	k1gFnPc7	struna
ze	z	k7c2	z
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dorazila	dorazit	k5eAaPmAgFnS	dorazit
loutna	loutna	k1gFnSc1	loutna
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
přes	přes	k7c4	přes
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Arabská	arabský	k2eAgFnSc1d1	arabská
loutna	loutna	k1gFnSc1	loutna
-	-	kIx~	-
"	"	kIx"	"
<g/>
al	ala	k1gFnPc2	ala
ud	ud	k?	ud
<g/>
"	"	kIx"	"
-	-	kIx~	-
měla	mít	k5eAaImAgFnS	mít
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
ostrunění	ostrunění	k1gNnSc4	ostrunění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
rozezníváno	rozeznívat	k5eAaImNgNnS	rozeznívat
plektrem	plektron	k1gNnSc7	plektron
(	(	kIx(	(
<g/>
trsátkem	trsátko	k1gNnSc7	trsátko
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepoužívala	používat	k5eNaImAgFnS	používat
pražce	pražec	k1gInPc4	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Plektrum	plektrum	k1gNnSc1	plektrum
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
vytlačeno	vytlačit	k5eAaPmNgNnS	vytlačit
hrou	hra	k1gFnSc7	hra
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
vázané	vázaný	k2eAgInPc1d1	vázaný
pražce	pražec	k1gInPc1	pražec
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
ostrunění	ostrunění	k1gNnSc1	ostrunění
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
tzv.	tzv.	kA	tzv.
sborovým	sborový	k2eAgNnPc3d1	sborové
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
ke	k	k7c3	k
stávajícím	stávající	k2eAgFnPc3d1	stávající
strunám	struna	k1gFnPc3	struna
přidány	přidán	k2eAgFnPc1d1	přidána
struny	struna	k1gFnPc4	struna
v	v	k7c6	v
unisonu	unisono	k1gNnSc6	unisono
nebo	nebo	k8xC	nebo
v	v	k7c6	v
oktávě	oktáva	k1gFnSc6	oktáva
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
renesanční	renesanční	k2eAgFnSc1d1	renesanční
loutna	loutna	k1gFnSc1	loutna
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měla	mít	k5eAaImAgFnS	mít
6	[number]	k4	6
sborů	sbor	k1gInPc2	sbor
laděných	laděný	k2eAgInPc2d1	laděný
in	in	k?	in
G	G	kA	G
nebo	nebo	k8xC	nebo
in	in	k?	in
A.	A.	kA	A.
Změn	změna	k1gFnPc2	změna
průběžně	průběžně	k6eAd1	průběžně
doznávala	doznávat	k5eAaImAgFnS	doznávat
i	i	k9	i
konstrukce	konstrukce	k1gFnSc1	konstrukce
nástroje	nástroj	k1gInSc2	nástroj
samotného	samotný	k2eAgMnSc2d1	samotný
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgInP	být
přidávány	přidáván	k2eAgInPc1d1	přidáván
další	další	k2eAgInPc1d1	další
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
má	mít	k5eAaImIp3nS	mít
typická	typický	k2eAgFnSc1d1	typická
barokní	barokní	k2eAgFnSc1d1	barokní
loutna	loutna	k1gFnSc1	loutna
13	[number]	k4	13
sborů	sbor	k1gInPc2	sbor
laděných	laděný	k2eAgInPc2d1	laděný
in	in	k?	in
d.	d.	k?	d.
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
evropské	evropský	k2eAgFnSc2d1	Evropská
historie	historie	k1gFnSc2	historie
loutny	loutna	k1gFnSc2	loutna
můžeme	moct	k5eAaImIp1nP	moct
napočítat	napočítat	k5eAaPmF	napočítat
na	na	k7c4	na
40	[number]	k4	40
druhů	druh	k1gInPc2	druh
loutnových	loutnový	k2eAgInPc2d1	loutnový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
např.	např.	kA	např.
theorba	theorba	k1gFnSc1	theorba
<g/>
,	,	kIx,	,
arciloutna	arciloutna	k1gFnSc1	arciloutna
<g/>
,	,	kIx,	,
mandora	mandora	k1gFnSc1	mandora
<g/>
,	,	kIx,	,
cistra	cistra	k1gFnSc1	cistra
<g/>
,	,	kIx,	,
angelika	angelika	k1gFnSc1	angelika
<g/>
,	,	kIx,	,
collascione	collascion	k1gInSc5	collascion
atd.	atd.	kA	atd.
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
loutnu	loutna	k1gFnSc4	loutna
upadá	upadat	k5eAaImIp3nS	upadat
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgInSc7d1	poslední
aktivně	aktivně	k6eAd1	aktivně
užívaným	užívaný	k2eAgInSc7d1	užívaný
typem	typ	k1gInSc7	typ
byla	být	k5eAaImAgFnS	být
barokní	barokní	k2eAgFnSc1d1	barokní
loutna	loutna	k1gFnSc1	loutna
a	a	k8xC	a
mandora	mandora	k1gFnSc1	mandora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změny	změna	k1gFnSc2	změna
hudebního	hudební	k2eAgInSc2d1	hudební
vkusu	vkus	k1gInSc2	vkus
i	i	k8xC	i
sociální	sociální	k2eAgFnSc2d1	sociální
funkce	funkce	k1gFnSc2	funkce
hudby	hudba	k1gFnSc2	hudba
její	její	k3xOp3gFnSc4	její
roli	role	k1gFnSc4	role
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
přebírají	přebírat	k5eAaImIp3nP	přebírat
kytarové	kytarový	k2eAgInPc4d1	kytarový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
loutny	loutna	k1gFnSc2	loutna
zůstal	zůstat	k5eAaPmAgInS	zůstat
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
romantických	romantický	k2eAgFnPc6d1	romantická
historizujících	historizující	k2eAgFnPc6d1	historizující
představách	představa	k1gFnPc6	představa
o	o	k7c6	o
potulných	potulný	k2eAgMnPc6d1	potulný
muzikantech	muzikant	k1gMnPc6	muzikant
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
kytary	kytara	k1gFnPc1	kytara
s	s	k7c7	s
korpusem	korpus	k1gInSc7	korpus
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
loutny	loutna	k1gFnSc2	loutna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
loutna	loutna	k1gFnSc1	loutna
i	i	k8xC	i
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
obnovena	obnovit	k5eAaPmNgNnP	obnovit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
probuzení	probuzení	k1gNnSc4	probuzení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
historicky	historicky	k6eAd1	historicky
poučenou	poučený	k2eAgFnSc4d1	poučená
interpretaci	interpretace	k1gFnSc4	interpretace
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
běžného	běžný	k2eAgMnSc2d1	běžný
instrumentáře	instrumentář	k1gMnSc2	instrumentář
souborů	soubor	k1gInPc2	soubor
staré	starý	k2eAgFnSc2d1	stará
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hra	hra	k1gFnSc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
loutnu	loutna	k1gFnSc4	loutna
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
z	z	k7c2	z
not	nota	k1gFnPc2	nota
nebo	nebo	k8xC	nebo
z	z	k7c2	z
typické	typický	k2eAgFnSc2d1	typická
loutnové	loutnový	k2eAgFnSc2d1	loutnová
tabulatury	tabulatura	k1gFnSc2	tabulatura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
podle	podle	k7c2	podle
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
tabulatura	tabulatura	k1gFnSc1	tabulatura
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
italskou	italský	k2eAgFnSc4d1	italská
<g/>
,	,	kIx,	,
španělskou	španělský	k2eAgFnSc4d1	španělská
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
loutnovou	loutnový	k2eAgFnSc4d1	loutnová
tabulaturu	tabulatura	k1gFnSc4	tabulatura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
francouzské	francouzský	k2eAgFnSc2d1	francouzská
loutnové	loutnový	k2eAgFnSc2d1	loutnová
hudby	hudba	k1gFnSc2	hudba
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
celý	celý	k2eAgInSc4d1	celý
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Tabulatura	tabulatura	k1gFnSc1	tabulatura
je	být	k5eAaImIp3nS	být
číselný	číselný	k2eAgInSc4d1	číselný
nebo	nebo	k8xC	nebo
písmenný	písmenný	k2eAgInSc4d1	písmenný
zápis	zápis	k1gInSc4	zápis
hmatů	hmat	k1gInPc2	hmat
na	na	k7c6	na
krku	krk	k1gInSc6	krk
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
většinou	většinou	k6eAd1	většinou
šest	šest	k4xCc4	šest
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
hmaty	hmat	k1gInPc4	hmat
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
nad	nad	k7c7	nad
osnovou	osnova	k1gFnSc7	osnova
rytmickými	rytmický	k2eAgNnPc7d1	rytmické
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
tabulatury	tabulatura	k1gFnSc2	tabulatura
oproti	oproti	k7c3	oproti
notovému	notový	k2eAgInSc3d1	notový
zápisu	zápis	k1gInSc3	zápis
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
přeladit	přeladit	k5eAaPmF	přeladit
nástroj	nástroj	k1gInSc4	nástroj
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
tóniny	tónina	k1gFnSc2	tónina
anebo	anebo	k8xC	anebo
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
drnkací	drnkací	k2eAgInPc4d1	drnkací
nástroje	nástroj	k1gInPc4	nástroj
z	z	k7c2	z
tabulatur	tabulatura	k1gFnPc2	tabulatura
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
určených	určený	k2eAgInPc2d1	určený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tabulatura	tabulatura	k1gFnSc1	tabulatura
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgInSc7d1	přímý
zápisem	zápis	k1gInSc7	zápis
hmatů	hmat	k1gInPc2	hmat
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
neláme	lámat	k5eNaImIp3nS	lámat
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
převodem	převod	k1gInSc7	převod
notových	notový	k2eAgFnPc2d1	notová
hodnot	hodnota	k1gFnPc2	hodnota
do	do	k7c2	do
poloh	poloha	k1gFnPc2	poloha
na	na	k7c6	na
pražcích	pražec	k1gInPc6	pražec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
nástroj	nástroj	k1gInSc4	nástroj
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
po	po	k7c6	po
teoretické	teoretický	k2eAgFnSc6d1	teoretická
<g/>
,	,	kIx,	,
praktické	praktický	k2eAgFnSc3d1	praktická
i	i	k8xC	i
konstrukční	konstrukční	k2eAgFnSc3d1	konstrukční
stránce	stránka	k1gFnSc3	stránka
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zvládnuta	zvládnut	k2eAgFnSc1d1	zvládnuta
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
loutnu	loutna	k1gFnSc4	loutna
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
v	v	k7c6	v
předních	přední	k2eAgInPc6d1	přední
hudebních	hudební	k2eAgInPc6d1	hudební
institutech	institut	k1gInPc6	institut
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
skládána	skládán	k2eAgFnSc1d1	skládána
i	i	k8xC	i
novodobá	novodobý	k2eAgFnSc1d1	novodobá
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
loutna	loutna	k1gFnSc1	loutna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
loutna	loutna	k1gFnSc1	loutna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Loutna	loutna	k1gFnSc1	loutna
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
