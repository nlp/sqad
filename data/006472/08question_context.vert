<s>
Deflace	deflace	k1gFnSc1	deflace
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgInSc4d1	absolutní
meziroční	meziroční	k2eAgInSc4d1	meziroční
pokles	pokles	k1gInSc4	pokles
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
indexem	index	k1gInSc7	index
spotřebitelských	spotřebitelský	k2eAgFnPc2d1	spotřebitelská
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>

