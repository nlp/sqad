<s>
New	New	k?
York	York	k1gInSc1
(	(	kIx(
<g/>
stát	stát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
New	New	k?
York	York	k1gInSc1
</s>
<s>
State	status	k1gInSc5
of	of	k?
New	New	k1gMnSc4
York	York	k1gInSc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
</s>
<s>
Pečeť	pečeť	k1gFnSc1
</s>
<s>
Přezdívka	přezdívka	k1gFnSc1
<g/>
:	:	kIx,
Empire	empir	k1gInSc5
State	status	k1gInSc5
</s>
<s>
Úřední	úřední	k2eAgMnSc1d1
jazykyžádný	jazykyžádný	k2eAgMnSc1d1
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
městoAlbany	městoAlbana	k1gFnPc1
</s>
<s>
Největší	veliký	k2eAgInSc1d3
městoNew	městoNew	k?
York	York	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
•	•	k?
Celkem	celkem	k6eAd1
•	•	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
souš	souš	k1gFnSc1
•	•	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
vodstvo	vodstvo	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
USA141	USA141	k1gFnSc6
300	#num#	k4
km²	km²	k?
<g/>
122	#num#	k4
224	#num#	k4
km²	km²	k?
<g/>
19	#num#	k4
076	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
13,5	13,5	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4
•	•	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
•	•	k?
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
USA	USA	kA
19	#num#	k4
745	#num#	k4
289	#num#	k4
(	(	kIx(
<g/>
odhad	odhad	k1gInSc1
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
163	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
okresů	okres	k1gInPc2
</s>
<s>
62	#num#	k4
</s>
<s>
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
−	−	k?
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
−	−	k?
<g/>
4	#num#	k4
(	(	kIx(
<g/>
letní	letní	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
43	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
75	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
•	•	k?
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
•	•	k?
Průměrná	průměrný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
•	•	k?
Nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
</s>
<s>
1629	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
300	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
GuvernérAndrew	GuvernérAndrew	k?
Cuomo	Cuoma	k1gFnSc5
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
</s>
<s>
SenátořiCharles	SenátořiCharles	k1gMnSc1
Schumer	Schumer	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
Kirsten	Kirsten	k2eAgInSc1d1
Gillibrand	Gillibrand	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
•	•	k?
Poštovní	poštovní	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
•	•	k?
Tradiční	tradiční	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
•	•	k?
ISO	ISO	kA
3166-2	3166-2	k4
</s>
<s>
NYN	NYN	kA
<g/>
.	.	kIx.
<g/>
Y.	Y.	kA
<g/>
US-NY	US-NY	kA
</s>
<s>
Přistoupení	přistoupení	k1gNnSc1
do	do	k7c2
Unie	unie	k1gFnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1788	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
stát	stát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.ny.gov	www.ny.gov	k1gInSc1
</s>
<s>
New	New	k?
York	York	k1gInSc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
nuː	nuː	k1gInSc1
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
State	status	k1gInSc5
of	of	k?
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stát	stát	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
ve	v	k7c4
Středoatlantské	Středoatlantský	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
severovýchodního	severovýchodní	k2eAgInSc2d1
regionu	region	k1gInSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
hraničí	hraničit	k5eAaImIp3nS
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
New	New	k1gFnSc7
Jersey	Jersea	k1gFnSc2
a	a	k8xC
Pensylvánií	Pensylvánie	k1gFnPc2
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
s	s	k7c7
Connecticutem	Connecticut	k1gInSc7
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc7
a	a	k8xC
Vermontem	Vermont	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
kanadskou	kanadský	k2eAgFnSc7d1
provincií	provincie	k1gFnSc7
Québec	Québec	k1gInSc1
a	a	k8xC
celá	celý	k2eAgFnSc1d1
severozápadní	severozápadní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
s	s	k7c7
provincií	provincie	k1gFnSc7
Ontario	Ontario	k1gNnSc4
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
po	po	k7c6
vodní	vodní	k2eAgFnSc6d1
hladině	hladina	k1gFnSc6
(	(	kIx(
<g/>
mj.	mj.	kA
Erijské	Erijský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
jezero	jezero	k1gNnSc1
Ontario	Ontario	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihovýchodní	jihovýchodní	k2eAgInSc4d1
ohraničení	ohraničení	k1gNnSc1
státu	stát	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rozlohou	rozloha	k1gFnSc7
141	#num#	k4
300	#num#	k4
km²	km²	k?
je	být	k5eAaImIp3nS
New	New	k1gFnSc4
York	York	k1gInSc1
27	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgInSc7d3
státem	stát	k1gInSc7
USA	USA	kA
<g/>
,	,	kIx,
v	v	k7c6
počtu	počet	k1gInSc6
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
19,7	19,7	k4
milionu	milion	k4xCgInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtvrtým	čtvrtý	k4xOgInSc7
nejlidnatějším	lidnatý	k2eAgInSc7d3
státem	stát	k1gInSc7
a	a	k8xC
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
hustoty	hustota	k1gFnSc2
zalidnění	zalidnění	k1gNnSc2
162	#num#	k4
obyvatel	obyvatel	k1gMnPc2
na	na	k7c6
km²	km²	k?
je	být	k5eAaImIp3nS
na	na	k7c6
sedmém	sedmý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Albany	Albana	k1gFnSc2
se	s	k7c7
100	#num#	k4
tisíci	tisíc	k4xCgInPc7
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgMnPc7d3
městy	město	k1gNnPc7
jsou	být	k5eAaImIp3nP
New	New	k1gMnPc1
York	York	k1gInSc4
s	s	k7c7
8,5	8,5	k4
miliony	milion	k4xCgInPc7
obyvateli	obyvatel	k1gMnPc7
(	(	kIx(
<g/>
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Buffalo	Buffalo	k1gFnSc1
(	(	kIx(
<g/>
260	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rochester	Rochester	k1gMnSc1
(	(	kIx(
<g/>
210	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Yonkers	Yonkers	k1gInSc1
(	(	kIx(
<g/>
200	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
Syracuse	Syracuse	k1gFnSc1
(	(	kIx(
<g/>
150	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státu	stát	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
patří	patřit	k5eAaImIp3nS
657	#num#	k4
kilometrů	kilometr	k1gInPc2
pobřeží	pobřeží	k1gNnSc2
Erijského	Erijský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
a	a	k8xC
jezera	jezero	k1gNnSc2
Ontario	Ontario	k1gNnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
také	také	k9
204	#num#	k4
km	km	kA
pobřeží	pobřeží	k1gNnSc1
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
především	především	k9
břehy	břeh	k1gInPc1
ostrova	ostrov	k1gInSc2
Long	Long	k1gInSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
je	být	k5eAaImIp3nS
největším	veliký	k2eAgInSc7d3
ostrovem	ostrov	k1gInSc7
kontinentálních	kontinentální	k2eAgInPc2d1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státem	stát	k1gInSc7
New	New	k1gFnSc2
York	York	k1gInSc1
prochází	procházet	k5eAaImIp3nS
Appalačské	Appalačský	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
a	a	k8xC
v	v	k7c6
jedné	jeden	k4xCgFnSc6
jeho	jeho	k3xOp3gFnSc6
části	část	k1gFnSc6
<g/>
,	,	kIx,
Adirondackém	Adirondacký	k2eAgNnSc6d1
pohoří	pohoří	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
i	i	k9
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
státu	stát	k1gInSc2
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc1
Mount	Mounta	k1gFnPc2
Marcy	Marca	k1gFnSc2
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
1629	#num#	k4
m.	m.	k?
Největšími	veliký	k2eAgInPc7d3
toky	tok	k1gInPc7
jsou	být	k5eAaImIp3nP
řeky	řeka	k1gFnSc2
Hudson	Hudsona	k1gFnPc2
a	a	k8xC
její	její	k3xOp3gInSc4
přítok	přítok	k1gInSc4
Mohawk	Mohawka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
také	také	k9
řeka	řeka	k1gFnSc1
Niagara	Niagara	k1gFnSc1
s	s	k7c7
Niagarskými	niagarský	k2eAgInPc7d1
vodopády	vodopád	k1gInPc7
a	a	k8xC
řeka	řeka	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
území	území	k1gNnSc2
státu	stát	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
součástí	součást	k1gFnPc2
nizozemské	nizozemský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Nové	Nové	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
Angličané	Angličan	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1664	#num#	k4
obsadili	obsadit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založili	založit	k5eAaPmAgMnP
zde	zde	k6eAd1
provincii	provincie	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
po	po	k7c6
Jakubovi	Jakub	k1gMnSc6
<g/>
,	,	kIx,
vévodovi	vévoda	k1gMnSc6
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provincie	provincie	k1gFnSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1776	#num#	k4
stala	stát	k5eAaPmAgFnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
původních	původní	k2eAgInPc2d1
třinácti	třináct	k4xCc2
zakládajících	zakládající	k2eAgInPc2d1
států	stát	k1gInPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
na	na	k7c6
jejím	její	k3xOp3gNnSc6
území	území	k1gNnSc6
bitva	bitva	k1gFnSc1
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
<g/>
,	,	kIx,
přelomový	přelomový	k2eAgInSc4d1
střet	střet	k1gInSc4
americké	americký	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnPc2
York	York	k1gInSc4
jako	jako	k8xC,k8xS
jedenáctý	jedenáctý	k4xOgInSc4
stát	stát	k1gInSc4
v	v	k7c6
pořadí	pořadí	k1gNnSc6
ratifikoval	ratifikovat	k5eAaBmAgInS
Ústavu	ústava	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
došlo	dojít	k5eAaPmAgNnS
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1788	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1785	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Newyorské	newyorský	k2eAgFnSc6d1
metropolitní	metropolitní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
zasahující	zasahující	k2eAgFnSc1d1
i	i	k9
do	do	k7c2
okolních	okolní	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
žije	žít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
24	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
činí	činit	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
aglomerací	aglomerace	k1gFnPc2
světa	svět	k1gInSc2
a	a	k8xC
největší	veliký	k2eAgFnSc7d3
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
několik	několik	k4yIc4
prestižních	prestižní	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Kolumbijská	kolumbijský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Cornellova	Cornellův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
University	universita	k1gFnSc2
a	a	k8xC
Rockefellerova	Rockefellerův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
;	;	kIx,
ve	v	k7c4
West	West	k1gInSc4
Pointu	pointa	k1gFnSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Vojenská	vojenský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lyžařské	lyžařský	k2eAgInPc1d1
středisko	středisko	k1gNnSc4
Lake	Lak	k1gInSc2
Placid	Placida	k1gFnPc2
hostilo	hostit	k5eAaImAgNnS
zimní	zimní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
v	v	k7c6
letech	léto	k1gNnPc6
1932	#num#	k4
a	a	k8xC
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
historie	historie	k1gFnSc1
New	New	k1gFnSc2
Yorku	York	k1gInSc2
začala	začít	k5eAaPmAgFnS
kolem	kolem	k7c2
roku	rok	k1gInSc2
10	#num#	k4
000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
oblasti	oblast	k1gFnSc2
dorazili	dorazit	k5eAaPmAgMnP
první	první	k4xOgMnPc1
lidé	člověk	k1gMnPc1
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
(	(	kIx(
<g/>
1614	#num#	k4
<g/>
–	–	k?
<g/>
1667	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnSc1
Evropan	Evropan	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
navštívil	navštívit	k5eAaPmAgMnS
New	New	k1gMnSc3
York	York	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Giovanni	Giovaneň	k1gFnSc3
da	da	k?
Verrazano	Verrazana	k1gFnSc5
v	v	k7c6
roce	rok	k1gInSc6
1524	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1609	#num#	k4
jej	on	k3xPp3gMnSc4
osídlili	osídlit	k5eAaPmAgMnP
Nizozemci	Nizozemec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1614	#num#	k4
postavili	postavit	k5eAaPmAgMnP
první	první	k4xOgNnSc4
evropské	evropský	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
<g/>
,	,	kIx,
Fort	Fort	k?
Nassau	Nassa	k1gMnSc6
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgFnSc2d1
Albany	Albana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fort	Fort	k?
Nassau	Nassaus	k1gInSc2
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
Fort	Fort	k?
Orangem	Orang	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1624	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchůdce	předchůdce	k1gMnSc1
New	New	k1gMnSc1
York	York	k1gInSc4
City	City	k1gFnSc2
<g/>
,	,	kIx,
Fort	Fort	k?
Amsterdam	Amsterdam	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1626	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
New	New	k1gMnSc2
Amsterdam	Amsterdam	k1gInSc4
<g/>
,	,	kIx,
největší	veliký	k2eAgNnSc4d3
osídlení	osídlení	k1gNnSc4
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Nizozemí	Nizozemí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgFnPc6
dekádách	dekáda	k1gFnPc6
se	se	k3xPyFc4
kolonie	kolonie	k1gFnSc1
stala	stát	k5eAaPmAgFnS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
obchodu	obchod	k1gInSc6
s	s	k7c7
kožešinami	kožešina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1650	#num#	k4
<g/>
,	,	kIx,
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Petera	Peter	k1gMnSc2
Stuyvesanta	Stuyvesant	k1gMnSc2
se	se	k3xPyFc4
kolonie	kolonie	k1gFnSc2
stala	stát	k5eAaPmAgFnS
největším	veliký	k2eAgMnSc7d3
exportérem	exportér	k1gMnSc7
tabáku	tabák	k1gInSc2
<g/>
,	,	kIx,
dřeva	dřevo	k1gNnSc2
a	a	k8xC
pšenice	pšenice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
komodit	komodita	k1gFnPc2
prošla	projít	k5eAaPmAgFnS
přes	přes	k7c4
Beverwijck	Beverwijck	k1gInSc4
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc4
Albany	Albana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapivý	překvapivý	k2eAgInSc4d1
útok	útok	k1gInSc4
ohromující	ohromující	k2eAgFnSc7d1
silou	síla	k1gFnSc7
dovolil	dovolit	k5eAaPmAgInS
Anglii	Anglie	k1gFnSc3
dobýt	dobýt	k5eAaPmF
Nové	Nové	k2eAgNnSc4d1
Nizozemí	Nizozemí	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1664	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonie	kolonie	k1gFnSc1
i	i	k9
město	město	k1gNnSc4
byly	být	k5eAaImAgInP
přejmenovány	přejmenován	k2eAgInPc1d1
na	na	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
<g/>
,	,	kIx,
Beverwijck	Beverwijck	k1gInSc1
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c4
Albany	Alban	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Nového	Nového	k2eAgNnSc2d1
Nizozemí	Nizozemí	k1gNnSc2
v	v	k7c6
době	doba	k1gFnSc6
zabrání	zabránit	k5eAaPmIp3nS
Nové	Nové	k2eAgFnPc4d1
Anglie	Anglie	k1gFnPc4
čítalo	čítat	k5eAaImAgNnS
7000	#num#	k4
až	až	k8xS
8000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Provincie	provincie	k1gFnSc1
New	New	k1gMnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
1664	#num#	k4
<g/>
–	–	k?
<g/>
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Populace	populace	k1gFnSc1
během	během	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
značně	značně	k6eAd1
rostla	růst	k5eAaImAgFnS
<g/>
,	,	kIx,
v	v	k7c6
prvním	první	k4xOgInSc6
sčítání	sčítání	k1gNnSc6
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
1698	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
populace	populace	k1gFnSc1
vyčíslena	vyčíslit	k5eAaPmNgFnS
na	na	k7c4
18	#num#	k4
067	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
posledním	poslední	k2eAgInSc6d1
(	(	kIx(
<g/>
1771	#num#	k4
<g/>
)	)	kIx)
obývalo	obývat	k5eAaImAgNnS
oblast	oblast	k1gFnSc4
168	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc4d3
část	část	k1gFnSc4
imigrantů	imigrant	k1gMnPc2
tvořili	tvořit	k5eAaImAgMnP
Angličané	Angličan	k1gMnPc1
<g/>
,	,	kIx,
Skoti	Skot	k1gMnPc1
a	a	k8xC
Irové	Ir	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
obchod	obchod	k1gInSc1
s	s	k7c7
otroky	otrok	k1gMnPc7
do	do	k7c2
oblasti	oblast	k1gFnSc2
přivedl	přivést	k5eAaPmAgMnS
i	i	k9
mnoho	mnoho	k4c4
černochů	černoch	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
období	období	k1gNnSc6
byl	být	k5eAaImAgMnS
New	New	k1gMnSc1
York	York	k1gInSc4
oblastí	oblast	k1gFnPc2
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
populací	populace	k1gFnSc7
černochů	černoch	k1gMnPc2
severně	severně	k6eAd1
od	od	k7c2
Mason-Dixon	Mason-Dixona	k1gFnPc2
Line	linout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Politické	politický	k2eAgFnSc3d1
scéně	scéna	k1gFnSc3
dominovali	dominovat	k5eAaImAgMnP
obchodníci	obchodník	k1gMnPc1
a	a	k8xC
hostinští	hostinský	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc4
měly	mít	k5eAaImAgInP
také	také	k9
panské	panský	k2eAgFnPc1d1
rodiny	rodina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonie	kolonie	k1gFnSc1
byla	být	k5eAaImAgFnS
centrem	centrum	k1gNnSc7
konfliktu	konflikt	k1gInSc2
mezi	mezi	k7c7
Anglií	Anglie	k1gFnSc7
a	a	k8xC
Francií	Francie	k1gFnSc7
během	během	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzsko-indiánská	francouzsko-indiánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
zuřila	zuřit	k5eAaImAgFnS
přes	přes	k7c4
70	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc4
York	York	k1gInSc1
byl	být	k5eAaImAgInS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
dvou	dva	k4xCgFnPc2
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
pravidelně	pravidelně	k6eAd1
ubytovávaly	ubytovávat	k5eAaImAgFnP
britská	britský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
před	před	k7c7
rokem	rok	k1gInSc7
1775	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
New	New	k?
York	York	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
New	New	k?
York	York	k1gInSc1
hrál	hrát	k5eAaImAgInS
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
Americké	americký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
popředí	popředí	k1gNnSc2
politiky	politika	k1gFnSc2
se	se	k3xPyFc4
dostávali	dostávat	k5eAaImAgMnP
Synové	syn	k1gMnPc1
svobody	svoboda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonie	kolonie	k1gFnSc1
podporovala	podporovat	k5eAaImAgFnS
obcházení	obcházení	k1gNnSc4
Kolkového	kolkový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
propukla	propuknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc4
hrál	hrát	k5eAaImAgMnS
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poloha	poloha	k1gFnSc1
New	New	k1gFnSc1
Yorku	York	k1gInSc2
umožňovala	umožňovat	k5eAaImAgFnS
kontrolovat	kontrolovat	k5eAaImF
ostatní	ostatní	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
a	a	k8xC
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc1
o	o	k7c4
něj	on	k3xPp3gMnSc4
měly	mít	k5eAaImAgInP
eminentní	eminentní	k2eAgInSc4d1
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Anglie	Anglie	k1gFnSc1
sestavila	sestavit	k5eAaPmAgFnS
největší	veliký	k2eAgNnSc4d3
loďstvo	loďstvo	k1gNnSc4
století	století	k1gNnSc2
<g/>
.	.	kIx.
30	#num#	k4
000	#num#	k4
britských	britský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
zakotvilo	zakotvit	k5eAaPmAgNnS
na	na	k7c4
Staten	Staten	k2eAgInSc4d1
Islandu	Island	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	Georg	k1gInSc2
Washington	Washington	k1gInSc1
stěží	stěží	k6eAd1
opustil	opustit	k5eAaPmAgInS
New	New	k1gFnSc2
York	York	k1gInSc1
city	city	k1gFnSc2
i	i	k8xC
s	s	k7c7
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
drželi	držet	k5eAaImAgMnP
do	do	k7c2
bitvy	bitva	k1gFnSc2
o	o	k7c4
Saratogu	Saratoga	k1gFnSc4
postavení	postavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
klíčový	klíčový	k2eAgInSc4d1
zlom	zlom	k1gInSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Unie	unie	k1gFnSc1
</s>
<s>
Válka	válka	k1gFnSc1
definovala	definovat	k5eAaBmAgFnS
hranice	hranice	k1gFnPc4
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
kraje	kraj	k1gInSc2
východně	východně	k6eAd1
od	od	k7c2
jezera	jezero	k1gNnSc2
Champlain	Champlaina	k1gFnPc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
Vermontem	Vermont	k1gInSc7
a	a	k8xC
západní	západní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
k	k	k7c3
roku	rok	k1gInSc3
1786	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
Iroquoisů	Iroquois	k1gInPc2
bylo	být	k5eAaImAgNnS
ve	v	k7c6
válce	válka	k1gFnSc6
zabito	zabít	k5eAaPmNgNnS
<g/>
,	,	kIx,
přeživší	přeživší	k2eAgMnPc1d1
obvykle	obvykle	k6eAd1
podporovali	podporovat	k5eAaImAgMnP
Brity	Brit	k1gMnPc7
<g/>
,	,	kIx,
především	především	k9
kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
z	z	k7c2
budoucích	budoucí	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
expanzí	expanze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
zůstali	zůstat	k5eAaPmAgMnP
v	v	k7c6
novém	nový	k2eAgInSc6d1
státě	stát	k1gInSc6
<g/>
,	,	kIx,
žili	žít	k5eAaImAgMnP
v	v	k7c6
rezervacích	rezervace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
adoptoval	adoptovat	k5eAaPmAgInS
konstituci	konstituce	k1gFnSc4
v	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
1777	#num#	k4
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgInS
silnou	silný	k2eAgFnSc4d1
exekutivu	exekutiva	k1gFnSc4
a	a	k8xC
striktně	striktně	k6eAd1
oddělil	oddělit	k5eAaPmAgMnS
moc	moc	k6eAd1
výkonnou	výkonný	k2eAgFnSc4d1
<g/>
,	,	kIx,
zákonodárnou	zákonodárný	k2eAgFnSc4d1
a	a	k8xC
soudní	soudní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Debata	debata	k1gFnSc1
o	o	k7c6
federální	federální	k2eAgFnSc6d1
konstituci	konstituce	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
zformování	zformování	k1gNnSc3
skupin	skupina	k1gFnPc2
známé	známá	k1gFnSc2
jako	jako	k8xC,k8xS
federalisté	federalista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
usilovali	usilovat	k5eAaImAgMnP
o	o	k7c4
silnou	silný	k2eAgFnSc4d1
federální	federální	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
Alexandr	Alexandr	k1gMnSc1
Hamilton	Hamilton	k1gInSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
Newyorských	newyorský	k2eAgMnPc2d1
federalistů	federalista	k1gMnPc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
esej	esej	k1gInSc1
Federalist	Federalist	k1gMnSc1
Papers	Papers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
texty	text	k1gInPc1
podporoval	podporovat	k5eAaImAgInS
konstituci	konstituce	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antifederalisté	Antifederalista	k1gMnPc1
nesouhlasili	souhlasit	k5eNaImAgMnP
s	s	k7c7
argumenty	argument	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
stát	stát	k5eAaPmF,k5eAaImF
New	New	k1gFnSc7
York	York	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1788	#num#	k4
ratifikoval	ratifikovat	k5eAaBmAgMnS
ústavu	ústava	k1gFnSc4
a	a	k8xC
připojil	připojit	k5eAaPmAgInS
se	se	k3xPyFc4
jako	jako	k9
jedenáctý	jedenáctý	k4xOgMnSc1
člen	člen	k1gMnSc1
k	k	k7c3
Unii	unie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1785	#num#	k4
stal	stát	k5eAaPmAgInS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
USA	USA	kA
<g/>
,	,	kIx,
George	George	k1gInSc1
Washington	Washington	k1gInSc1
byl	být	k5eAaImAgMnS
inaugurován	inaugurovat	k5eAaBmNgMnS
před	před	k7c7
Federal	Federal	k1gFnSc7
Hall	Halla	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
státu	stát	k1gInSc2
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
přesouvalo	přesouvat	k5eAaImAgNnS
mezi	mezi	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
city	city	k1gFnPc1
<g/>
,	,	kIx,
Kingstonem	Kingston	k1gInSc7
<g/>
,	,	kIx,
Poughkeepsie	Poughkeepsie	k1gFnPc1
a	a	k8xC
Albany	Albana	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statut	statut	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
nakonec	nakonec	k6eAd1
připadl	připadnout	k5eAaPmAgInS
Albany	Albana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
New	New	k1gFnSc2
York	York	k1gInSc1
centrem	centrum	k1gNnSc7
rozvoje	rozvoj	k1gInSc2
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1807	#num#	k4
Robert	Robert	k1gMnSc1
Fulton	Fulton	k1gInSc4
zahájil	zahájit	k5eAaPmAgMnS
parníkovou	parníkový	k2eAgFnSc4d1
linku	linka	k1gFnSc4
z	z	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
do	do	k7c2
Albany	Albana	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
první	první	k4xOgInSc1
úspěšný	úspěšný	k2eAgInSc1d1
podnik	podnik	k1gInSc1
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
bylo	být	k5eAaImAgNnS
Albany	Albana	k1gFnSc2
státním	státní	k2eAgInSc7d1
centrem	centr	k1gInSc7
výběru	výběr	k1gInSc2
mýta	mýto	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
něj	on	k3xPp3gNnSc2
udělalo	udělat	k5eAaPmAgNnS
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
přes	přes	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
proudili	proudit	k5eAaPmAgMnP,k5eAaImAgMnP
poutníci	poutník	k1gMnPc1
na	na	k7c4
východ	východ	k1gInSc4
Buffalo	Buffalo	k1gFnSc2
a	a	k8xC
do	do	k7c2
Michiganského	michiganský	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1825	#num#	k4
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
Erijský	Erijský	k2eAgInSc1d1
kanál	kanál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zabezpečoval	zabezpečovat	k5eAaImAgInS
ekonomickou	ekonomický	k2eAgFnSc4d1
dominanci	dominance	k1gFnSc4
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
dopad	dopad	k1gInSc4
byl	být	k5eAaImAgInS
enormní	enormní	k2eAgMnSc1d1
<g/>
,	,	kIx,
spojil	spojit	k5eAaPmAgMnS
Atlantik	Atlantik	k1gInSc4
a	a	k8xC
Velká	velká	k1gFnSc1
jezera	jezero	k1gNnSc2
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgInS
ekonomické	ekonomický	k2eAgNnSc4d1
zázemí	zázemí	k1gNnSc4
pro	pro	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
city	city	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
všechny	všechen	k3xTgInPc1
okresy	okres	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
většina	většina	k1gFnSc1
municipalit	municipalita	k1gFnPc2
připojila	připojit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
přibližně	přibližně	k6eAd1
odpovídalo	odpovídat	k5eAaImAgNnS
tomu	ten	k3xDgNnSc3
dnešnímu	dnešní	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
železnice	železnice	k1gFnSc2
Mohawk	Mohawka	k1gFnPc2
and	and	k?
Hudson	Hudson	k1gNnSc1
zahájila	zahájit	k5eAaPmAgFnS
první	první	k4xOgFnSc4
úspěšnou	úspěšný	k2eAgFnSc4d1
pravidelnou	pravidelný	k2eAgFnSc4d1
linku	linka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
dopravy	doprava	k1gFnSc2
vedl	vést	k5eAaImAgInS
k	k	k7c3
osídlení	osídlení	k1gNnSc3
Mohawských	Mohawská	k1gFnPc2
a	a	k8xC
Gennesseeiských	Gennesseeiských	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
a	a	k8xC
Niagarských	niagarský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Buffala	Buffala	k1gFnSc2
a	a	k8xC
Rochesteru	Rochester	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
boom-města	boom-města	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znatelná	znatelný	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
Yankeeů	yankee	k1gMnPc2
z	z	k7c2
Nové	Nové	k2eAgFnSc2d1
Anglie	Anglie	k1gFnSc2
do	do	k7c2
centrálních	centrální	k2eAgFnPc2d1
a	a	k8xC
západních	západní	k2eAgFnPc2d1
částí	část	k1gFnPc2
státu	stát	k1gInSc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
malým	malý	k2eAgInPc3d1
konfliktům	konflikt	k1gInPc3
s	s	k7c7
usedlými	usedlý	k2eAgMnPc7d1
Yorkery	Yorker	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
obvykle	obvykle	k6eAd1
byli	být	k5eAaImAgMnP
německého	německý	k2eAgInSc2d1
<g/>
,	,	kIx,
nizozemského	nizozemský	k2eAgInSc2d1
a	a	k8xC
skotského	skotský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
15	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
bylo	být	k5eAaImAgNnS
narozeno	narodit	k5eAaPmNgNnS
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
státu	stát	k1gInSc2
během	během	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
rychle	rychle	k6eAd1
rostla	růst	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1840	#num#	k4
se	se	k3xPyFc4
New	New	k1gMnSc1
York	York	k1gInSc4
stal	stát	k5eAaPmAgMnS
domovem	domov	k1gInSc7
sedmi	sedm	k4xCc2
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
kvetla	kvést	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1809	#num#	k4
Washington	Washington	k1gInSc1
Irving	Irving	k1gInSc4
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
satirickou	satirický	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
A	a	k8xC
History	Histor	k1gMnPc4
of	of	k?
New	New	k1gFnSc1
York	York	k1gInSc1
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Dietrich	Dietrich	k1gMnSc1
Knickerbocker	Knickerbocker	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
baseballové	baseballový	k2eAgInPc1d1
týmy	tým	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
city	city	k1gFnSc1
zformovaly	zformovat	k5eAaPmAgInP
v	v	k7c6
po	po	k7c6
roce	rok	k1gInSc6
1840	#num#	k4
<g/>
,	,	kIx,
včetně	včetně	k7c2
New	New	k1gFnSc2
York	York	k1gInSc1
Knickerbockers	Knickerbockers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Statisíce	statisíce	k1gInPc1
mladých	mladý	k2eAgMnPc2d1
newyorských	newyorský	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
bojovaly	bojovat	k5eAaImAgFnP
ve	v	k7c6
válce	válka	k1gFnSc6
Severu	sever	k1gInSc2
proti	proti	k7c3
Jihu	jih	k1gInSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
z	z	k7c2
kteréhokoliv	kterýkoliv	k3yIgInSc2
jiného	jiný	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
neprospívala	prospívat	k5eNaImAgFnS
obchodu	obchod	k1gInSc3
<g/>
,	,	kIx,
protože	protože	k8xS
před	před	k7c7
válkou	válka	k1gFnSc7
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
obchodu	obchod	k1gInSc2
založena	založit	k5eAaPmNgFnS
na	na	k7c4
transportu	transporta	k1gFnSc4
zboží	zboží	k1gNnSc2
vyrobeného	vyrobený	k2eAgNnSc2d1
v	v	k7c6
jižanských	jižanský	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
Demokratů	demokrat	k1gMnPc2
se	se	k3xPyFc4
obávala	obávat	k5eAaImAgFnS
dopadu	dopad	k1gInSc3
zvolení	zvolení	k1gNnSc4
Abrahama	Abraham	k1gMnSc2
Lincolna	Lincoln	k1gMnSc2
zvoleného	zvolený	k2eAgMnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1861	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
proběhla	proběhnout	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
o	o	k7c4
Fort	Fort	k?
Sumter	Sumtra	k1gFnPc2
<g/>
,	,	kIx,
zmizely	zmizet	k5eAaPmAgInP
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
politiky	politik	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
New	New	k1gFnSc1
York	York	k1gInSc1
brzy	brzy	k6eAd1
vyhověl	vyhovět	k5eAaPmAgInS
Lincolnovu	Lincolnův	k2eAgInSc3d1
požadavku	požadavek	k1gInSc3
na	na	k7c6
dodání	dodání	k1gNnSc6
vojáků	voják	k1gMnPc2
a	a	k8xC
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
bitvy	bitva	k1gFnPc1
neprobíhaly	probíhat	k5eNaImAgFnP
na	na	k7c6
území	území	k1gNnSc6
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
stát	stát	k1gInSc1
nebyl	být	k5eNaImAgInS
imunní	imunní	k2eAgMnSc1d1
vůči	vůči	k7c3
konspiracím	konspirace	k1gFnPc3
konfederace	konfederace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
Lincoln	Lincoln	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
the	the	k?
Emacipation	Emacipation	k1gInSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
Emancipační	emancipační	k2eAgNnSc1d1
prohlášení	prohlášení	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
osvobodilo	osvobodit	k5eAaPmAgNnS
otroky	otrok	k1gMnPc7
ve	v	k7c6
státech	stát	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
proti	proti	k7c3
Unii	unie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1863	#num#	k4
federální	federální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
změnu	změna	k1gFnSc4
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
odvod	odvod	k1gInSc4
mužů	muž	k1gMnPc2
ve	v	k7c6
věku	věk	k1gInSc6
mezi	mezi	k7c7
20	#num#	k4
až	až	k9
35	#num#	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
že	že	k8xS
nebyli	být	k5eNaImAgMnP
oddáni	oddat	k5eAaPmNgMnP
<g/>
,	,	kIx,
odvodní	odvodnit	k5eAaPmIp3nS
povinnost	povinnost	k1gFnSc1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
platila	platit	k5eAaImAgFnS
až	až	k9
do	do	k7c2
45	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
zaplatili	zaplatit	k5eAaPmAgMnP
300	#num#	k4
USD	USD	kA
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
odvodu	odvod	k1gInSc6
ušetřeni	ušetřit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgFnPc6d1
dekádách	dekáda	k1gFnPc6
New	New	k1gFnSc2
York	York	k1gInSc1
upevňoval	upevňovat	k5eAaImAgInS
svou	svůj	k3xOyFgFnSc4
dominantní	dominantní	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
ve	v	k7c6
finančních	finanční	k2eAgFnPc6d1
a	a	k8xC
bankovních	bankovní	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
průmysl	průmysl	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
růstu	růst	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
byl	být	k5eAaImAgMnS
založen	založen	k2eAgMnSc1d1
Eastman	Eastman	k1gMnSc1
Kodak	Kodak	kA
<g/>
,	,	kIx,
General	General	k1gMnSc1
Electric	Electric	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Niagarské	niagarský	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
atraktivním	atraktivní	k2eAgNnSc7d1
místem	místo	k1gNnSc7
pro	pro	k7c4
mnoho	mnoho	k4c4
továren	továrna	k1gFnPc2
díky	díky	k7c3
přístupu	přístup	k1gInSc3
k	k	k7c3
vodní	vodní	k2eAgFnSc3d1
energii	energie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
průmysl	průmysl	k1gInSc4
kvetl	kvést	k5eAaImAgMnS
<g/>
,	,	kIx,
pracující	pracující	k1gMnPc1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
spojovat	spojovat	k5eAaImF
<g/>
,	,	kIx,
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
už	už	k6eAd1
v	v	k7c6
raných	raný	k2eAgInPc6d1
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1882	#num#	k4
měli	mít	k5eAaImAgMnP
Knights	Knights	k1gInSc4
of	of	k?
Labor	Labor	k1gInSc1
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
city	city	k1gFnSc7
kolem	kolem	k7c2
60	#num#	k4
tisíc	tisíc	k4xCgInPc2
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
odbory	odbor	k1gInPc1
měly	mít	k5eAaImAgInP
dostatečnou	dostatečný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
stanovily	stanovit	k5eAaPmAgFnP
limit	limit	k1gInSc4
pro	pro	k7c4
pracovní	pracovní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
imigrantů	imigrant	k1gMnPc2
rostl	růst	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
s	s	k7c7
irským	irský	k2eAgInSc7d1
bramborovým	bramborový	k2eAgInSc7d1
hladomorem	hladomor	k1gInSc7
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
hlavním	hlavní	k2eAgInSc7d1
vstupním	vstupní	k2eAgInSc7d1
bodem	bod	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
imigranti	imigrant	k1gMnPc1
hledali	hledat	k5eAaImAgMnP
nový	nový	k2eAgInSc4d1
život	život	k1gInSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
mezi	mezi	k7c7
lety	let	k1gInPc7
1855	#num#	k4
až	až	k9
1890	#num#	k4
prošlo	projít	k5eAaPmAgNnS
asi	asi	k9
8	#num#	k4
miliónů	milión	k4xCgInPc2
lidí	člověk	k1gMnPc2
přes	přes	k7c4
Castle	Castle	k1gFnPc4
Clinton	Clinton	k1gMnSc1
v	v	k7c4
Battery	Batter	k1gInPc4
Parku	park	k1gInSc2
na	na	k7c6
Manhattanu	Manhattan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
imigrantů	imigrant	k1gMnPc2
přišlo	přijít	k5eAaPmAgNnS
z	z	k7c2
Německa	Německo	k1gNnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
Ellis	Ellis	k1gInSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
1880	#num#	k4
a	a	k8xC
1920	#num#	k4
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
imigrantů	imigrant	k1gMnPc2
německého	německý	k2eAgInSc2d1
<g/>
,	,	kIx,
židovského	židovský	k2eAgMnSc2d1
<g/>
,	,	kIx,
polského	polský	k2eAgInSc2d1
nebo	nebo	k8xC
východoevropského	východoevropský	k2eAgInSc2d1
a	a	k8xC
jihoevropského	jihoevropský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1925	#num#	k4
populace	populace	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc4
city	city	k1gFnSc2
překročila	překročit	k5eAaPmAgFnS
populaci	populace	k1gFnSc4
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
udělalo	udělat	k5eAaPmAgNnS
nejlidnatější	lidnatý	k2eAgNnSc1d3
město	město	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nejznámějších	známý	k2eAgInPc2d3
symbolů	symbol	k1gInPc2
New	New	k1gFnSc1
York	York	k1gInSc1
city	city	k1gFnSc1
<g/>
,	,	kIx,
Socha	socha	k1gFnSc1
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
dárek	dárek	k1gInSc4
od	od	k7c2
Francie	Francie	k1gFnSc2
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
z	z	k7c2
ní	on	k3xPp3gFnSc2
symbol	symbol	k1gInSc4
naděje	naděje	k1gFnSc1
pro	pro	k7c4
imigranty	imigrant	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
byl	být	k5eAaImAgMnS
New	New	k1gMnSc3
York	York	k1gInSc4
nejbohatším	bohatý	k2eAgInSc7d3
a	a	k8xC
nejlidnatějším	lidnatý	k2eAgInSc7d3
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
spojení	spojení	k1gNnSc1
pěti	pět	k4xCc2
částí	část	k1gFnPc2
New	New	k1gFnPc2
York	York	k1gInSc1
city	cit	k1gInPc1
(	(	kIx(
<g/>
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
Bronx	Bronx	k1gInSc1
<g/>
,	,	kIx,
Brooklyn	Brooklyn	k1gInSc1
<g/>
,	,	kIx,
Staten	Staten	k2eAgInSc1d1
Island	Island	k1gInSc1
a	a	k8xC
Queens	Queens	k1gInSc1
<g/>
)	)	kIx)
do	do	k7c2
jednoho	jeden	k4xCgNnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
symbolem	symbol	k1gInSc7
města	město	k1gNnSc2
mrakodrapy	mrakodrap	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
nejvyšší	vysoký	k2eAgFnSc7d3
budovou	budova	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
v	v	k7c6
NYC	NYC	kA
byla	být	k5eAaImAgFnS
Woolworth	Woolworth	k1gInSc4
Building	Building	k1gInSc1
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
překonána	překonat	k5eAaPmNgFnS
budovou	budova	k1gFnSc7
na	na	k7c4
40	#num#	k4
Wall	Wallum	k1gNnPc2
Street	Streeta	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
překonána	překonat	k5eAaPmNgFnS
Chrysler	Chrysler	k1gMnSc1
Building	Building	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
Empire	empir	k1gInSc5
State	status	k1gInSc5
Building	Building	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
nejvyšší	vysoký	k2eAgFnSc7d3
budovou	budova	k1gFnSc7
světa	svět	k1gInSc2
do	do	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
na	na	k7c4
Lower	Lower	k1gInSc4
Manhattanu	Manhattan	k1gInSc2
postaveny	postaven	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
věže	věž	k1gFnPc1
Světového	světový	k2eAgNnSc2d1
obchodního	obchodní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
dvojčata	dvojče	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomika	ekonomika	k1gFnSc1
New	New	k1gFnSc2
Yorku	York	k1gInSc2
prudce	prudko	k6eAd1
rostla	růst	k5eAaImAgFnS
během	během	k7c2
dvacátých	dvacátý	k4xOgNnPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
období	období	k1gNnSc1
Roaring	Roaring	k1gInSc1
Twenties	Twenties	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
těžce	těžce	k6eAd1
zasažena	zasáhnout	k5eAaPmNgFnS
Velkou	velký	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
začala	začít	k5eAaPmAgFnS
krachem	krach	k1gInSc7
na	na	k7c4
Wall	Wall	k1gInSc4
Street	Streeta	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Franklin	Franklin	k1gInSc1
Delano	Delana	k1gFnSc5
Roosevelt	Roosevelt	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
zvolen	zvolit	k5eAaPmNgInS
guvernérem	guvernér	k1gMnSc7
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
čelit	čelit	k5eAaImF
25	#num#	k4
<g/>
%	%	kIx~
nárůstu	nárůst	k1gInSc2
nezaměstnanosti	nezaměstnanost	k1gFnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
dočasný	dočasný	k2eAgInSc1d1
program	program	k1gInSc1
Emergency	Emergenca	k1gFnSc2
Relief	Relief	k1gMnSc1
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
vytvořený	vytvořený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
byl	být	k5eAaImAgInS
prvním	první	k4xOgMnSc6
work	work	k1gInSc1
relief	relief	k1gInSc1
programem	program	k1gInSc7
v	v	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
ovlivnil	ovlivnit	k5eAaPmAgMnS
Federal	Federal	k1gMnSc1
Emergency	Emergenca	k1gFnSc2
Relief	Relief	k1gMnSc1
Administration	Administration	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Franklin	Franklin	k1gInSc1
Delano	Delana	k1gFnSc5
Roosevelt	Roosevelt	k1gMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
slíbil	slíbit	k5eAaPmAgMnS
rozšířit	rozšířit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
Newyorský	newyorský	k2eAgInSc4d1
program	program	k1gInSc4
do	do	k7c2
celé	celý	k2eAgFnSc2d1
země	zem	k1gFnSc2
pomocí	pomocí	k7c2
reformě	reforma	k1gFnSc3
ekonomického	ekonomický	k2eAgMnSc2d1
a	a	k8xC
sociálního	sociální	k2eAgInSc2d1
programu	program	k1gInSc2
New	New	k1gMnSc1
Deal	Deal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
Lake	Lake	k1gNnPc2
Placid	Placida	k1gFnPc2
hostilo	hostit	k5eAaImAgNnS
III	III	kA
<g/>
.	.	kIx.
zimní	zimní	k2eAgFnSc2d1
olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Stát	stát	k1gInSc1
New	New	k1gFnPc2
York	York	k1gInSc1
opět	opět	k6eAd1
dodával	dodávat	k5eAaImAgInS
nejvíce	hodně	k6eAd3,k6eAd1
zdrojů	zdroj	k1gInPc2
ze	z	k7c2
všech	všecek	k3xTgInPc2
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
byl	být	k5eAaImAgInS
31	#num#	k4
215	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc7d1
průmyslovou	průmyslový	k2eAgFnSc7d1
érou	éra	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
hospodářství	hospodářství	k1gNnSc1
přesunulo	přesunout	k5eAaPmAgNnS
směrem	směr	k1gInSc7
k	k	k7c3
produkci	produkce	k1gFnSc3
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firmy	firma	k1gFnPc1
se	se	k3xPyFc4
přesunovaly	přesunovat	k5eAaImAgFnP
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
jih	jih	k1gInSc4
hledajíc	hledat	k5eAaImSgFnS
nižší	nízký	k2eAgNnSc4d2
daně	daň	k1gFnPc4
<g/>
,	,	kIx,
nižší	nízký	k2eAgInPc4d2
náklady	náklad	k1gInPc4
a	a	k8xC
pracovníky	pracovník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nejsou	být	k5eNaImIp3nP
sdružení	sdružení	k1gNnSc4
v	v	k7c6
odborech	odbor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibývalo	přibývat	k5eAaImAgNnS
příslušníků	příslušník	k1gMnPc2
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tvořila	tvořit	k5eAaImAgFnS
předměstí	předměstí	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
Long	Long	k1gInSc1
Island	Island	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvoj	rozvoj	k1gInSc1
automobilového	automobilový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
toto	tento	k3xDgNnSc1
urychlil	urychlit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Velká	velká	k1gFnSc1
města	město	k1gNnSc2
přestala	přestat	k5eAaPmAgFnS
růst	růst	k1gInSc4
kolem	kolem	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
pouze	pouze	k6eAd1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
Buffala	Buffala	k1gFnSc1
se	se	k3xPyFc4
zmenšila	zmenšit	k5eAaPmAgFnS
mezi	mezi	k7c7
lety	let	k1gInPc7
1950	#num#	k4
až	až	k9
2000	#num#	k4
na	na	k7c4
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
mezi	mezi	k7c7
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
lety	let	k1gInPc4
poprvé	poprvé	k6eAd1
klesala	klesat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalifornie	Kalifornie	k1gFnSc1
a	a	k8xC
Texas	Texas	k1gInSc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
lidnatějšími	lidnatý	k2eAgMnPc7d2
než	než	k8xS
New	New	k1gMnPc4
York	York	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1
dekády	dekáda	k1gFnPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
období	období	k1gNnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
mnoho	mnoho	k4c1
New	New	k1gFnPc2
Yorčanů	Yorčan	k1gMnPc2
zaměstnávalo	zaměstnávat	k5eAaImAgNnS
telekomunikační	telekomunikační	k2eAgInSc4d1
a	a	k8xC
technologické	technologický	k2eAgNnSc4d1
odvětví	odvětví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikatelé	podnikatel	k1gMnPc1
zakládali	zakládat	k5eAaImAgMnP
malé	malý	k2eAgFnPc4d1
technologické	technologický	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změny	změna	k1gFnSc2
v	v	k7c6
policii	policie	k1gFnSc6
a	a	k8xC
urbanismu	urbanismus	k1gInSc6
dramaticky	dramaticky	k6eAd1
zredukovaly	zredukovat	k5eAaPmAgFnP
kriminalitu	kriminalita	k1gFnSc4
a	a	k8xC
množství	množství	k1gNnSc4
chátrajících	chátrající	k2eAgFnPc2d1
čtvrtí	čtvrt	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
přepětí	přepětí	k1gNnSc3
v	v	k7c6
kultuře	kultura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
stal	stát	k5eAaPmAgInS
kulturním	kulturní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapová	rapový	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejpopulárnějším	populární	k2eAgInSc7d3
žánrem	žánr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imigrace	imigrace	k1gFnSc1
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
i	i	k8xC
do	do	k7c2
NYC	NYC	kA
rostla	růst	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
NYC	NYC	kA
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
velkou	velký	k2eAgFnSc7d1
homosexuální	homosexuální	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
byla	být	k5eAaImAgFnS
sužována	sužovat	k5eAaImNgFnS
mnoho	mnoho	k6eAd1
úmrtími	úmrtí	k1gNnPc7
na	na	k7c6
AIDS	AIDS	kA
<g/>
.	.	kIx.
</s>
<s>
One	One	k?
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Center	centrum	k1gNnPc2
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
Manhattanu	Manhattan	k1gInSc6
(	(	kIx(
<g/>
uprostřed	uprostřed	k7c2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
teroristických	teroristický	k2eAgInPc6d1
útocích	útok	k1gInPc6
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
nová	nový	k2eAgFnSc1d1
dominanta	dominanta	k1gFnSc1
New	New	k1gFnSc2
Yorku	York	k1gInSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
teroristickým	teroristický	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
kompletně	kompletně	k6eAd1
zničily	zničit	k5eAaPmAgFnP
Světové	světový	k2eAgNnSc4d1
obchodní	obchodní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
a	a	k8xC
ikonická	ikonický	k2eAgNnPc4d1
dvojčata	dvojče	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
mrakodrap	mrakodrap	k1gInSc4
One	One	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
coby	coby	k?
nová	nový	k2eAgFnSc1d1
dominanta	dominanta	k1gFnSc1
a	a	k8xC
symbol	symbol	k1gInSc1
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Jezero	jezero	k1gNnSc1
Lake	Lak	k1gFnSc2
Placid	Placida	k1gFnPc2
v	v	k7c6
Adirondackém	Adirondacký	k2eAgInSc6d1
národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgNnSc6d1
městě	město	k1gNnSc6
se	se	k3xPyFc4
dvakrát	dvakrát	k6eAd1
konaly	konat	k5eAaImAgFnP
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
a	a	k8xC
1980	#num#	k4
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
19	#num#	k4
378	#num#	k4
102	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
(	(	kIx(
<g/>
přes	přes	k7c4
8	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
žije	žít	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
New	New	k1gFnSc2
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Rasové	rasový	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
65,7	65,7	k4
%	%	kIx~
bílí	bílý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
</s>
<s>
15,9	15,9	k4
%	%	kIx~
Afroameričané	Afroameričan	k1gMnPc1
</s>
<s>
0,6	0,6	k4
%	%	kIx~
američtí	americký	k2eAgMnPc1d1
indiáni	indián	k1gMnPc1
</s>
<s>
7,3	7,3	k4
%	%	kIx~
asijští	asijský	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
</s>
<s>
0,0	0,0	k4
%	%	kIx~
pacifičtí	pacifický	k2eAgMnPc1d1
ostrované	ostrovan	k1gMnPc1
</s>
<s>
7,4	7,4	k4
%	%	kIx~
jiná	jiný	k2eAgFnSc1d1
rasa	rasa	k1gFnSc1
</s>
<s>
3,0	3,0	k4
%	%	kIx~
dvě	dva	k4xCgNnPc1
nebo	nebo	k8xC
více	hodně	k6eAd2
ras	rasa	k1gFnPc2
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1
hispánského	hispánský	k2eAgInSc2d1
nebo	nebo	k8xC
latinskoamerického	latinskoamerický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
rasu	rasa	k1gFnSc4
<g/>
,	,	kIx,
tvořili	tvořit	k5eAaImAgMnP
17,6	17,6	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Křesťané	křesťan	k1gMnPc1
77	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
38	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
protestanti	protestant	k1gMnPc1
30	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
židé	žid	k1gMnPc1
5	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
3	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
jiná	jiný	k2eAgNnPc4d1
náboženství	náboženství	k1gNnPc4
2	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
bez	bez	k7c2
vyznání	vyznání	k1gNnPc2
13	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Politika	politika	k1gFnSc1
</s>
<s>
Současným	současný	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
New	New	k1gFnSc2
Yorku	York	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
demokrat	demokrat	k1gMnSc1
David	David	k1gMnSc1
Paterson	Paterson	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
afroamerickým	afroamerický	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
<g/>
,	,	kIx,
Elliot	Elliot	k1gMnSc1
Spitzer	Spitzer	k1gMnSc1
<g/>
,	,	kIx,
odstoupil	odstoupit	k5eAaPmAgMnS
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
kvůli	kvůli	k7c3
skandálu	skandál	k1gInSc3
s	s	k7c7
prostitutkou	prostitutka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
státu	stát	k1gInSc2
je	být	k5eAaImIp3nS
Albany	Albana	k1gFnPc4
<g/>
,	,	kIx,
největším	veliký	k2eAgInSc7d3
pak	pak	k6eAd1
New	New	k1gMnPc4
York	York	k1gInSc1
city	cit	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
státě	stát	k1gInSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
na	na	k7c6
celém	celý	k2eAgInSc6d1
severovýchodě	severovýchod	k1gInSc6
USA	USA	kA
a	a	k8xC
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
převládá	převládat	k5eAaImIp3nS
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venkovské	venkovský	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
severně	severně	k6eAd1
od	od	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
jsou	být	k5eAaImIp3nP
potom	potom	k6eAd1
více	hodně	k6eAd2
republikánské	republikánský	k2eAgMnPc4d1
a	a	k8xC
konzervativní	konzervativní	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
hlavních	hlavní	k2eAgInPc2d1
regionů	region	k1gInPc2
státu	stát	k1gInSc2
New	New	k1gFnPc2
York	York	k1gInSc1
</s>
<s>
Administrativní	administrativní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
New	New	k1gFnSc2
Yorku	York	k1gInSc2
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
odlišné	odlišný	k2eAgInPc1d1
od	od	k7c2
většiny	většina	k1gFnSc2
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
typy	typ	k1gInPc1
obcí	obec	k1gFnPc2
jsou	být	k5eAaImIp3nP
City	city	k1gNnSc1
<g/>
,	,	kIx,
Village	Village	k1gNnSc1
a	a	k8xC
Town	Town	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
City	city	k1gNnPc1
a	a	k8xC
Village	Village	k1gNnPc1
jsou	být	k5eAaImIp3nP
klasická	klasický	k2eAgNnPc1d1
města	město	k1gNnPc1
(	(	kIx(
<g/>
village	village	k1gInSc1
je	být	k5eAaImIp3nS
pochopitelně	pochopitelně	k6eAd1
menší	malý	k2eAgFnSc1d2
a	a	k8xC
ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
i	i	k9
méně	málo	k6eAd2
autonomní	autonomní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Town	Town	k1gInSc1
je	být	k5eAaImIp3nS
okrsek	okrsek	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
Česku	Česko	k1gNnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
přirovnat	přirovnat	k5eAaPmF
k	k	k7c3
něčemu	něco	k3yInSc3
mezi	mezi	k7c7
okresem	okres	k1gInSc7
a	a	k8xC
obcí	obec	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
většinou	většina	k1gFnSc7
na	na	k7c4
starost	starost	k1gFnSc4
menší	malý	k2eAgFnSc4d2
městečka	městečko	k1gNnSc2
bez	bez	k7c2
vlastní	vlastní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
New	New	k1gFnPc2
York	York	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
county	counta	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Město	město	k1gNnSc1
(	(	kIx(
<g/>
city	cit	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Okrsek	okrsek	k1gInSc1
(	(	kIx(
<g/>
town	town	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Menší	malý	k2eAgNnSc1d2
město	město	k1gNnSc1
(	(	kIx(
<g/>
village	village	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Sídlo	sídlo	k1gNnSc4
bez	bez	k7c2
samosprávy	samospráva	k1gFnSc2
(	(	kIx(
<g/>
hamlet	hamlet	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Indiánská	indiánský	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
Sídla	sídlo	k1gNnPc1
ve	v	k7c6
státě	stát	k1gInSc6
mají	mít	k5eAaImIp3nP
status	status	k1gInSc4
nikoliv	nikoliv	k9
podle	podle	k7c2
jejich	jejich	k3xOp3gFnSc2
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
podle	podle	k7c2
způsobu	způsob	k1gInSc2
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
vybrán	vybrat	k5eAaPmNgMnS
obyvateli	obyvatel	k1gMnPc7
a	a	k8xC
schválen	schválit	k5eAaPmNgInS
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
Hempstead	Hempstead	k1gInSc1
má	mít	k5eAaImIp3nS
55	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
village	village	k1gFnSc1
(	(	kIx(
<g/>
vesnice	vesnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Sherill	Sherill	k1gInSc1
má	mít	k5eAaImIp3nS
status	status	k1gInSc4
city	city	k1gFnSc2
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
jen	jen	k9
3	#num#	k4
147	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
New	New	k1gFnPc2
York	York	k1gInSc4
má	mít	k5eAaImIp3nS
62	#num#	k4
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
county	counta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
932	#num#	k4
okrsků	okrsek	k1gInPc2
(	(	kIx(
<g/>
town	town	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
62	#num#	k4
měst	město	k1gNnPc2
(	(	kIx(
<g/>
city	city	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
14	#num#	k4
Indiánských	indiánský	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
County	Counta	k1gFnPc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Celý	celý	k2eAgInSc1d1
stát	stát	k1gInSc1
má	mít	k5eAaImIp3nS
62	#num#	k4
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
části	část	k1gFnPc4
města	město	k1gNnSc2
New	New	k1gFnSc2
York	York	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
nemají	mít	k5eNaImIp3nP
vlastní	vlastní	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Albany	Albana	k1gFnPc1
|	|	kIx~
Allegany	Allegana	k1gFnPc1
|	|	kIx~
Bronx	Bronx	k1gInSc1
|	|	kIx~
Broome	Broom	k1gInSc5
|	|	kIx~
Cattaraugus	Cattaraugus	k1gInSc1
|	|	kIx~
Cayuga	Cayuga	k1gFnSc1
|	|	kIx~
Chautauqua	Chautauqua	k1gMnSc1
|	|	kIx~
Chemung	Chemung	k1gMnSc1
|	|	kIx~
Chenango	Chenango	k1gMnSc1
|	|	kIx~
Clinton	Clinton	k1gMnSc1
|	|	kIx~
Columbia	Columbia	k1gFnSc1
|	|	kIx~
Cortland	Cortland	k1gInSc1
|	|	kIx~
Delaware	Delawar	k1gMnSc5
|	|	kIx~
Dutchess	Dutchess	k1gInSc1
|	|	kIx~
Erie	Erie	k1gInSc1
|	|	kIx~
Essex	Essex	k1gInSc1
|	|	kIx~
Franklin	Franklin	k1gInSc1
|	|	kIx~
Fulton	Fulton	k1gInSc1
|	|	kIx~
Genesee	Genesee	k1gNnSc1
|	|	kIx~
Greene	Green	k1gMnSc5
|	|	kIx~
Hamilton	Hamilton	k1gInSc4
|	|	kIx~
Herkimer	Herkimer	k1gMnSc1
|	|	kIx~
Jefferson	Jefferson	k1gMnSc1
|	|	kIx~
Kings	Kings	k1gInSc1
(	(	kIx(
<g/>
Brooklyn	Brooklyn	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
|	|	kIx~
Lewis	Lewis	k1gInSc1
|	|	kIx~
Livingston	Livingston	k1gInSc1
|	|	kIx~
Madison	Madison	k1gInSc1
|	|	kIx~
Monroe	Monroe	k1gFnSc1
|	|	kIx~
Montgomery	Montgomer	k1gInPc1
|	|	kIx~
Nassau	Nassaus	k1gInSc2
|	|	kIx~
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
Manhattan	Manhattan	k1gInSc1
<g/>
)	)	kIx)
|	|	kIx~
Niagara	Niagara	k1gFnSc1
|	|	kIx~
Oneida	Oneida	k1gFnSc1
|	|	kIx~
Onondaga	Onondaga	k1gFnSc1
|	|	kIx~
Ontario	Ontario	k1gNnSc1
|	|	kIx~
Orange	Orange	k1gFnSc1
|	|	kIx~
Orleans	Orleans	k1gInSc1
|	|	kIx~
Oswego	Oswego	k1gMnSc1
|	|	kIx~
Otsego	Otsego	k1gMnSc1
|	|	kIx~
Putnam	Putnam	k1gInSc1
|	|	kIx~
Queens	Queens	k1gInSc1
|	|	kIx~
Rensselaer	Rensselaer	k1gInSc1
County	Counta	k1gFnSc2
|	|	kIx~
Richmond	Richmond	k1gInSc1
(	(	kIx(
<g/>
Staten	Staten	k2eAgInSc1d1
Island	Island	k1gInSc1
<g/>
)	)	kIx)
|	|	kIx~
Rockland	Rockland	k1gInSc1
|	|	kIx~
Saratoga	Saratoga	k1gFnSc1
|	|	kIx~
Schenectady	Schenectad	k1gInPc1
|	|	kIx~
Schoharie	Schoharie	k1gFnSc1
|	|	kIx~
Schuyler	Schuyler	k1gMnSc1
|	|	kIx~
Seneca	Seneca	k1gMnSc1
|	|	kIx~
St.	st.	kA
Lawrence	Lawrenka	k1gFnSc6
|	|	kIx~
Steuben	Steuben	k2eAgMnSc1d1
|	|	kIx~
Suffolk	Suffolk	k1gMnSc1
|	|	kIx~
Sullivan	Sullivan	k1gMnSc1
|	|	kIx~
Tioga	Tioga	k1gFnSc1
|	|	kIx~
Tompkins	Tompkins	k1gInSc1
|	|	kIx~
Ulster	Ulster	k1gInSc1
|	|	kIx~
Warren	Warrno	k1gNnPc2
|	|	kIx~
Washington	Washington	k1gInSc1
|	|	kIx~
Wayne	Wayn	k1gInSc5
|	|	kIx~
Westchester	Westchester	k1gInSc1
|	|	kIx~
Wyoming	Wyoming	k1gInSc1
|	|	kIx~
Yates	Yates	k1gInSc1
</s>
<s>
City	city	k1gNnSc1
(	(	kIx(
<g/>
větší	veliký	k2eAgNnSc1d2
město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
termín	termín	k1gInSc1
City	City	k1gFnSc2
znamená	znamenat	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
autonomní	autonomní	k2eAgFnSc4d1
obydlenou	obydlený	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
svým	svůj	k3xOyFgMnPc3
obyvatelům	obyvatel	k1gMnPc3
veškeré	veškerý	k3xTgFnSc2
služby	služba	k1gFnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
a	a	k8xC
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmenším	malý	k2eAgNnSc7d3
městem	město	k1gNnSc7
s	s	k7c7
titulem	titul	k1gInSc7
city	city	k1gNnSc2
je	být	k5eAaImIp3nS
Sherill	Sherill	k1gInSc1
s	s	k7c7
3	#num#	k4
147	#num#	k4
obyvateli	obyvatel	k1gMnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
největší	veliký	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
New	New	k1gFnSc1
York	York	k1gInSc1
City	City	k1gFnSc2
s	s	k7c7
populací	populace	k1gFnSc7
přesahující	přesahující	k2eAgFnSc7d1
8	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s>
Town	Town	k1gNnSc1
(	(	kIx(
<g/>
správní	správní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
jih	jih	k1gInSc1
státu	stát	k1gInSc2
pokrývá	pokrývat	k5eAaImIp3nS
aglomerace	aglomerace	k1gFnSc1
města	město	k1gNnSc2
New	New	k1gFnSc2
York	York	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
rozsáhlé	rozsáhlý	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
malá	malý	k2eAgNnPc1d1
městečka	městečko	k1gNnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Chestertown	Chestertown	k1gNnSc1
v	v	k7c6
Adirondackém	Adirondacký	k2eAgInSc6d1
národním	národní	k2eAgInSc6d1
Parku	park	k1gInSc6
</s>
<s>
Slovo	slovo	k1gNnSc1
Town	Towna	k1gFnPc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
v	v	k7c6
překladu	překlad	k1gInSc6
město	město	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
obyčejného	obyčejný	k2eAgNnSc2d1
města	město	k1gNnSc2
výrazně	výrazně	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
totiž	totiž	k9
divize	divize	k1gFnSc1
okresu	okres	k1gInSc2
a	a	k8xC
ne	ne	k9
sídlo	sídlo	k1gNnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
slovo	slovo	k1gNnSc1
town	towna	k1gFnPc2
chápáno	chápat	k5eAaImNgNnS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
anglicky	anglicky	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Town	Town	k1gInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
služeb	služba	k1gFnPc2
pro	pro	k7c4
obyvatele	obyvatel	k1gMnPc4
takzvaných	takzvaný	k2eAgFnPc2d1
osad	osada	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
hamlet	hamlet	k5eAaImF,k5eAaPmF
<g/>
)	)	kIx)
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
vybrané	vybraný	k2eAgFnPc4d1
služby	služba	k1gFnPc4
pro	pro	k7c4
obyvatele	obyvatel	k1gMnPc4
menších	malý	k2eAgNnPc2d2
měst	město	k1gNnPc2
(	(	kIx(
<g/>
village	village	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
názvy	název	k1gInPc4
(	(	kIx(
<g/>
hamlet	hamlet	k1gInSc4
a	a	k8xC
village	village	k1gInSc4
<g/>
)	)	kIx)
však	však	k9
neurčují	určovat	k5eNaImIp3nP
velikost	velikost	k1gFnSc4
sídla	sídlo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
typ	typ	k1gInSc4
jeho	jeho	k3xOp3gFnSc2
administrativy	administrativa	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
kapitoly	kapitola	k1gFnPc4
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
občan	občan	k1gMnSc1
státu	stát	k1gInSc2
New	New	k1gFnPc2
York	York	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nežije	žít	k5eNaImIp3nS
v	v	k7c6
city	city	k1gFnSc6
nebo	nebo	k8xC
indiánské	indiánský	k2eAgFnSc3d1
rezervaci	rezervace	k1gFnSc3
žije	žít	k5eAaImIp3nS
právě	právě	k9
v	v	k7c6
okrsku	okrsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
town	town	k1gInSc1
je	být	k5eAaImIp3nS
Hempstead	Hempstead	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
756	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Towns	Towns	k1gInSc1
mají	mít	k5eAaImIp3nP
velké	velký	k2eAgFnPc4d1
rozlohy	rozloha	k1gFnPc4
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
desítky	desítka	k1gFnPc1
až	až	k8xS
stovky	stovka	k1gFnPc1
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
obsahují	obsahovat	k5eAaImIp3nP
několik	několik	k4yIc4
komunit	komunita	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
jen	jen	k9
jednu	jeden	k4xCgFnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
desítky	desítka	k1gFnSc2
<g/>
;	;	kIx,
typický	typický	k2eAgInSc1d1
town	town	k1gInSc1
však	však	k9
obsahuje	obsahovat	k5eAaImIp3nS
jen	jen	k9
komunitu	komunita	k1gFnSc4
stejného	stejný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
dvě	dva	k4xCgFnPc1
až	až	k8xS
čtyři	čtyři	k4xCgFnPc1
jiné	jiný	k2eAgFnPc1d1
menší	malý	k2eAgFnPc1d2
komunity	komunita	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
townu	town	k1gInSc2
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
komunita	komunita	k1gFnSc1
stejného	stejný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
obce	obec	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
však	však	k9
stává	stávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
town	town	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
sídla	sídlo	k1gNnSc2
a	a	k8xC
komunity	komunita	k1gFnSc2
naprosto	naprosto	k6eAd1
odlišných	odlišný	k2eAgInPc2d1
názvů	název	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
někdy	někdy	k6eAd1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
název	název	k1gInSc1
okrsku	okrsek	k1gInSc2
nalezen	nalézt	k5eAaBmNgInS,k5eAaPmNgInS
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
mapách	mapa	k1gFnPc6
zpravidla	zpravidla	k6eAd1
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
sídla	sídlo	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
ne	ne	k9
administrativní	administrativní	k2eAgFnSc4d1
divize	divize	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českých	český	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
je	být	k5eAaImIp3nS
town	towna	k1gFnPc2
něco	něco	k6eAd1
mezi	mezi	k7c7
okresem	okres	k1gInSc7
a	a	k8xC
obcí	obec	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Village	Village	k1gNnSc1
(	(	kIx(
<g/>
menší	malý	k2eAgNnSc1d2
město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Village	Village	k6eAd1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
menší	malý	k2eAgNnSc1d2
město	město	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
leží	ležet	k5eAaImIp3nS
uvnitř	uvnitř	k7c2
okrsku	okrsek	k1gInSc2
(	(	kIx(
<g/>
Town	Town	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc4d1
vlastní	vlastní	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vybrané	vybraný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
mu	on	k3xPp3gMnSc3
poskytuje	poskytovat	k5eAaImIp3nS
obec	obec	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Village	Villag	k1gInSc2
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
autonomní	autonomní	k2eAgNnSc1d1
<g/>
,	,	kIx,
než	než	k8xS
city	cit	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
village	village	k6eAd1
je	být	k5eAaImIp3nS
sice	sice	k8xC
v	v	k7c6
překladu	překlad	k1gInSc6
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
nemají	mít	k5eNaImIp3nP
sídla	sídlo	k1gNnSc2
status	status	k1gInSc1
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
ale	ale	k8xC
podle	podle	k7c2
formy	forma	k1gFnSc2
administrativy	administrativa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
village	village	k1gInSc1
má	mít	k5eAaImIp3nS
55	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Village	Villag	k1gInSc2
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
minimálně	minimálně	k6eAd1
500	#num#	k4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
nesmí	smět	k5eNaImIp3nS
přesahovat	přesahovat	k5eAaImF
13	#num#	k4
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
celkové	celkový	k2eAgFnSc2d1
rozlohy	rozloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
státě	stát	k1gInSc6
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
553	#num#	k4
těchto	tento	k3xDgFnPc2
villages	villagesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přirovnání	přirovnání	k1gNnSc6
k	k	k7c3
České	český	k2eAgFnSc3d1
republice	republika	k1gFnSc3
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
část	část	k1gFnSc1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
její	její	k3xOp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
vytvořit	vytvořit	k5eAaPmF
samostatnou	samostatný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
a	a	k8xC
administrativu	administrativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Hamlet	Hamlet	k1gMnSc1
(	(	kIx(
<g/>
městečko	městečko	k1gNnSc1
<g/>
,	,	kIx,
vesnice	vesnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
je	být	k5eAaImIp3nS
Hamlet	Hamlet	k1gMnSc1
obydlená	obydlený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
ve	v	k7c6
okrsku	okrsek	k1gInSc6
(	(	kIx(
<g/>
town	town	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
není	být	k5eNaImIp3nS
součástí	součást	k1gFnSc7
village	villag	k1gFnSc2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
menší	malý	k2eAgNnSc1d2
městečko	městečko	k1gNnSc1
nebo	nebo	k8xC
vesnička	vesnička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
hamlet	hamlet	k1gInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
přeložit	přeložit	k5eAaPmF
jako	jako	k9
osada	osada	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
má	mít	k5eAaImIp3nS
jiný	jiný	k2eAgInSc4d1
význam	význam	k1gInSc4
než	než	k8xS
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
protože	protože	k8xS
ve	v	k7c6
státě	stát	k1gInSc6
hraje	hrát	k5eAaImIp3nS
roli	role	k1gFnSc4
forma	forma	k1gFnSc1
samosprávy	samospráva	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
ne	ne	k9
velikost	velikost	k1gFnSc1
sídla	sídlo	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
hamlet	hamlet	k5eAaPmF,k5eAaImF
klidně	klidně	k6eAd1
několik	několik	k4yIc4
tisíc	tisíc	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
hamlet	hamlet	k1gInSc1
Ronkonkoma	Ronkonkomum	k1gNnSc2
má	mít	k5eAaImIp3nS
20	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamlet	Hamlet	k1gMnSc1
nemá	mít	k5eNaImIp3nS
legální	legální	k2eAgInSc4d1
status	status	k1gInSc4
(	(	kIx(
<g/>
kromě	kromě	k7c2
osad	osada	k1gFnPc2
v	v	k7c6
Adirondackém	Adirondacký	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
obci	obec	k1gFnSc6
(	(	kIx(
<g/>
town	town	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
mu	on	k3xPp3gMnSc3
poskytuje	poskytovat	k5eAaImIp3nS
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamlet	Hamlet	k1gMnSc1
totiž	totiž	k9
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamlet	Hamlet	k1gMnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
přirovnat	přirovnat	k5eAaPmF
ke	k	k7c3
čtvrti	čtvrt	k1gFnSc3
nebo	nebo	k8xC
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
u	u	k7c2
větších	veliký	k2eAgFnPc2d2
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
těchto	tento	k3xDgInPc2
hamletů	hamlet	k1gInPc2
nejsou	být	k5eNaImIp3nP
přesně	přesně	k6eAd1
definovány	definovat	k5eAaBmNgFnP
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
jednoduše	jednoduše	k6eAd1
definovány	definovat	k5eAaBmNgFnP
podle	podle	k7c2
směrovacího	směrovací	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamlet	Hamlet	k1gMnSc1
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
vlastní	vlastní	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
<g/>
,	,	kIx,
školu	škola	k1gFnSc4
a	a	k8xC
hasičský	hasičský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
osady	osada	k1gFnPc1
označují	označovat	k5eAaImIp3nP
jako	jako	k9
komunity	komunita	k1gFnSc2
bez	bez	k7c2
vlastní	vlastní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
unincorporated	unincorporated	k1gInSc1
community	communita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnPc4d2
komunity	komunita	k1gFnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
rozeznány	rozeznat	k5eAaPmNgFnP
i	i	k8xC
americkým	americký	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
pro	pro	k7c4
sčítání	sčítání	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
definuje	definovat	k5eAaBmIp3nS
jako	jako	k9
samostatnou	samostatný	k2eAgFnSc4d1
entitu	entita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
hamlety	hamleta	k1gFnPc4
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
nejlépe	dobře	k6eAd3
přirovnat	přirovnat	k5eAaPmF
k	k	k7c3
vesnicím	vesnice	k1gFnPc3
a	a	k8xC
částem	část	k1gFnPc3
obce	obec	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Table	tablo	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annual	Annual	k1gMnSc1
Estimates	Estimates	k1gMnSc1
of	of	k?
the	the	k?
Resident	resident	k1gMnSc1
Population	Population	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
,	,	kIx,
Regions	Regions	k1gInSc1
<g/>
,	,	kIx,
States	States	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Puerto	Puerta	k1gFnSc5
Rico	Rico	k1gNnSc1
<g/>
:	:	kIx,
April	April	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
to	ten	k3xDgNnSc1
July	Jula	k1gMnPc7
1	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
(	(	kIx(
<g/>
NST-EST	NST-EST	k1gFnSc1
<g/>
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
<g/>
,	,	kIx,
Population	Population	k1gInSc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
2016-12	2016-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Great	Great	k1gInSc1
Lakes	Lakes	k1gMnSc1
Length	Length	k1gMnSc1
of	of	k?
Shoreline	Shorelin	k1gInSc5
in	in	k?
Separate	Separat	k1gInSc5
Basin	Basin	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Census	census	k1gInSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
General	General	k1gFnSc1
Coastline	Coastlin	k1gInSc5
and	and	k?
Shoreline	Shorelin	k1gInSc5
Mileage	Mileage	k1gNnSc2
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noaa	Noaa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Study	stud	k1gInPc1
for	forum	k1gNnPc2
Woolworth	Woolwortha	k1gFnPc2
Building	Building	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1910-12-10	1910-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
United	United	k1gMnSc1
States	States	k1gMnSc1
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
<g/>
,	,	kIx,
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
FactFinder	FactFinder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
New	New	k1gFnSc2
York	York	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
státu	stát	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
státu	stát	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
</s>
<s>
Albany	Albana	k1gFnPc1
•	•	k?
Allegany	Allegana	k1gFnSc2
•	•	k?
Bronx	Bronx	k1gInSc1
•	•	k?
Broome	Broom	k1gInSc5
•	•	k?
Cattaraugus	Cattaraugus	k1gInSc1
•	•	k?
Cayuga	Cayug	k1gMnSc2
•	•	k?
Chautauqua	Chautauqua	k1gMnSc1
•	•	k?
Chemung	Chemung	k1gMnSc1
•	•	k?
Chenango	Chenango	k1gMnSc1
•	•	k?
Clinton	Clinton	k1gMnSc1
•	•	k?
Columbia	Columbia	k1gFnSc1
•	•	k?
Cortland	Cortland	k1gInSc1
•	•	k?
Delaware	Delawar	k1gMnSc5
•	•	k?
Dutchess	Dutchess	k1gInSc1
•	•	k?
Erie	Erie	k1gInSc1
•	•	k?
Essex	Essex	k1gInSc1
•	•	k?
Franklin	Franklin	k1gInSc1
•	•	k?
Fulton	Fulton	k1gInSc1
•	•	k?
Genesee	Genesee	k1gInSc1
•	•	k?
Greene	Green	k1gInSc5
•	•	k?
Hamilton	Hamilton	k1gInSc1
•	•	k?
Herkimer	Herkimer	k1gInSc1
•	•	k?
Jefferson	Jefferson	k1gInSc1
•	•	k?
Kings	Kings	k1gInSc1
(	(	kIx(
<g/>
Brooklyn	Brooklyn	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Lewis	Lewis	k1gInSc1
•	•	k?
Livingston	Livingston	k1gInSc1
•	•	k?
Madison	Madison	k1gInSc1
•	•	k?
Monroe	Monroe	k1gFnSc1
•	•	k?
Montgomery	Montgomer	k1gInPc1
•	•	k?
Nassau	Nassaus	k1gInSc2
•	•	k?
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
Manhattan	Manhattan	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Niagara	Niagara	k1gFnSc1
•	•	k?
Oneida	Oneid	k1gMnSc2
•	•	k?
Onondaga	Onondag	k1gMnSc2
•	•	k?
Ontario	Ontario	k1gNnSc1
•	•	k?
Orange	Orang	k1gInSc2
•	•	k?
Orleans	Orleans	k1gInSc1
•	•	k?
Oswego	Oswego	k6eAd1
•	•	k?
Otsego	Otsego	k1gMnSc1
•	•	k?
Putnam	Putnam	k1gInSc1
•	•	k?
Queens	Queens	k1gInSc1
•	•	k?
Rensselaer	Rensselaer	k1gInSc1
County	Counta	k1gFnSc2
•	•	k?
Richmond	Richmond	k1gInSc1
(	(	kIx(
<g/>
Staten	Staten	k2eAgInSc1d1
Island	Island	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rockland	Rockland	k1gInSc1
•	•	k?
Saratoga	Saratoga	k1gFnSc1
•	•	k?
Schenectady	Schenectada	k1gFnSc2
•	•	k?
Schoharie	Schoharie	k1gFnSc2
•	•	k?
Schuyler	Schuyler	k1gMnSc1
•	•	k?
Seneca	Seneca	k1gMnSc1
•	•	k?
St.	st.	kA
Lawrence	Lawrence	k1gFnSc2
•	•	k?
Steuben	Steuben	k2eAgInSc1d1
•	•	k?
Suffolk	Suffolk	k1gInSc1
•	•	k?
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Tioga	Tioga	k1gFnSc1
•	•	k?
Tompkins	Tompkins	k1gInSc1
•	•	k?
Ulster	Ulster	k1gInSc1
•	•	k?
Warren	Warrna	k1gFnPc2
•	•	k?
Washington	Washington	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
•	•	k?
Westchester	Westchester	k1gInSc1
•	•	k?
Wyoming	Wyoming	k1gInSc1
•	•	k?
Yates	Yates	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
–	–	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc1
</s>
<s>
Alabama	Alabama	k1gFnSc1
•	•	k?
Aljaška	Aljaška	k1gFnSc1
•	•	k?
Arizona	Arizona	k1gFnSc1
•	•	k?
Arkansas	Arkansas	k1gInSc1
•	•	k?
Colorado	Colorado	k1gNnSc1
•	•	k?
Connecticut	Connecticut	k1gMnSc1
•	•	k?
Delaware	Delawar	k1gMnSc5
•	•	k?
Florida	Florida	k1gFnSc1
•	•	k?
Georgie	Georgie	k1gFnSc1
•	•	k?
Havaj	Havaj	k1gFnSc1
•	•	k?
Idaho	Ida	k1gMnSc2
•	•	k?
Illinois	Illinois	k1gFnSc2
•	•	k?
Indiana	Indiana	k1gFnSc1
•	•	k?
Iowa	Iow	k1gInSc2
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Kalifornie	Kalifornie	k1gFnSc2
•	•	k?
Kansas	Kansas	k1gInSc1
•	•	k?
Kentucky	Kentucka	k1gFnSc2
•	•	k?
Louisiana	Louisiana	k1gFnSc1
•	•	k?
Maine	Main	k1gInSc5
•	•	k?
Maryland	Marylanda	k1gFnPc2
•	•	k?
Massachusetts	Massachusetts	k1gNnSc4
•	•	k?
Michigan	Michigan	k1gInSc1
•	•	k?
Minnesota	Minnesota	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mississippi	Mississippi	k1gFnSc2
•	•	k?
Missouri	Missouri	k1gFnSc2
•	•	k?
Montana	Montana	k1gFnSc1
•	•	k?
Nebraska	Nebraska	k1gFnSc1
•	•	k?
Nevada	Nevada	k1gFnSc1
•	•	k?
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
•	•	k?
New	New	k1gMnSc4
Jersey	Jersea	k1gFnSc2
•	•	k?
New	New	k1gFnSc1
York	York	k1gInSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Ohio	Ohio	k1gNnSc1
•	•	k?
Oklahoma	Oklahom	k1gMnSc2
•	•	k?
Oregon	Oregon	k1gMnSc1
•	•	k?
Pensylvánie	Pensylvánie	k1gFnSc1
•	•	k?
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Tennessee	Tennessee	k1gInSc1
•	•	k?
Texas	Texas	k1gInSc1
•	•	k?
Utah	Utah	k1gInSc1
•	•	k?
Vermont	Vermont	k1gInSc1
•	•	k?
Virginie	Virginie	k1gFnSc1
•	•	k?
Washington	Washington	k1gInSc1
•	•	k?
Wisconsin	Wisconsin	k1gInSc1
•	•	k?
Wyoming	Wyoming	k1gInSc4
•	•	k?
Západní	západní	k2eAgFnSc2d1
Virginie	Virginie	k1gFnSc2
Federální	federální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
Autonomní	autonomní	k2eAgFnSc1d1
ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
Americké	americký	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Guam	Guam	k1gInSc1
•	•	k?
Portoriko	Portoriko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
Ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
podpřímou	podpřímý	k2eAgFnSc7d1
správou	správa	k1gFnSc7
vlády	vláda	k1gFnSc2
USA	USA	kA
</s>
<s>
Bakerův	Bakerův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Howlandův	Howlandův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Jarvisův	Jarvisův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Johnstonův	Johnstonův	k2eAgInSc1d1
atol	atol	k1gInSc1
•	•	k?
Kingmanův	Kingmanův	k2eAgInSc1d1
útes	útes	k1gInSc1
•	•	k?
Midway	Midwaa	k1gFnSc2
•	•	k?
Navassa	Navassa	k1gFnSc1
•	•	k?
Palmyra	Palmyra	k1gFnSc1
•	•	k?
Wake	Wak	k1gInSc2
•	•	k?
sporná	sporný	k2eAgFnSc1d1
území	území	k1gNnSc6
<g/>
:	:	kIx,
Bajo	Bajo	k6eAd1
Nuevo	Nuevo	k1gNnSc1
•	•	k?
Serranilla	Serranilla	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
134390	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4042012-7	4042012-7	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8638	#num#	k4
4906	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80126293	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146785329	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80126293	#num#	k4
</s>
