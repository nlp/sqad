<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
a	a	k8xC	a
fialové	fialový	k2eAgInPc1d1	fialový
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
do	do	k7c2	do
hlavatého	hlavatý	k2eAgNnSc2d1	hlavaté
květenství	květenství	k1gNnSc2	květenství
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
nebo	nebo	k8xC	nebo
funkčně	funkčně	k6eAd1	funkčně
samičí	samičí	k2eAgFnSc1d1	samičí
(	(	kIx(	(
<g/>
se	s	k7c7	s
zakrnělými	zakrnělý	k2eAgFnPc7d1	zakrnělá
tyčinkami	tyčinka	k1gFnPc7	tyčinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
