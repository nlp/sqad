<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
neboli	neboli	k8xC	neboli
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
polejová	polejový	k2eAgFnSc1d1	polejový
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pulegioides	pulegioides	k1gMnSc1	pulegioides
L.	L.	kA	L.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hluchavkovitých	hluchavkovitý	k2eAgMnPc2d1	hluchavkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
našim	náš	k3xOp1gFnPc3	náš
nejrozšířenějším	rozšířený	k2eAgFnPc3d3	nejrozšířenější
mateřídouškám	mateřídouška	k1gFnPc3	mateřídouška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
je	být	k5eAaImIp3nS	být
drobná	drobný	k2eAgFnSc1d1	drobná
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
plazivou	plazivý	k2eAgFnSc7d1	plazivá
<g/>
,	,	kIx,	,
u	u	k7c2	u
země	zem	k1gFnSc2	zem
dřevnatějící	dřevnatějící	k2eAgFnSc7d1	dřevnatějící
lodyhou	lodyha	k1gFnSc7	lodyha
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
trsnaté	trsnatý	k2eAgInPc4d1	trsnatý
keříky	keřík	k1gInPc4	keřík
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozemnutí	rozemnutí	k1gNnSc6	rozemnutí
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
typická	typický	k2eAgFnSc1d1	typická
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
stanoviště	stanoviště	k1gNnSc2	stanoviště
a	a	k8xC	a
genetické	genetický	k2eAgFnSc2d1	genetická
mutace	mutace	k1gFnSc2	mutace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
vstřícné	vstřícný	k2eAgInPc1d1	vstřícný
<g/>
,	,	kIx,	,
vejčité	vejčitý	k2eAgInPc1d1	vejčitý
až	až	k9	až
široce	široko	k6eAd1	široko
eliptické	eliptický	k2eAgNnSc1d1	eliptické
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
×	×	k?	×
4-6	[number]	k4	4-6
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
a	a	k8xC	a
fialové	fialový	k2eAgInPc1d1	fialový
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
do	do	k7c2	do
hlavatého	hlavatý	k2eAgNnSc2d1	hlavaté
květenství	květenství	k1gNnSc2	květenství
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
nebo	nebo	k8xC	nebo
funkčně	funkčně	k6eAd1	funkčně
samičí	samičí	k2eAgFnSc1d1	samičí
(	(	kIx(	(
<g/>
se	s	k7c7	s
zakrnělými	zakrnělý	k2eAgFnPc7d1	zakrnělá
tyčinkami	tyčinka	k1gFnPc7	tyčinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Roste	růst	k5eAaImIp3nS	růst
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mimo	mimo	k7c4	mimo
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Preferuje	preferovat	k5eAaImIp3nS	preferovat
nízké	nízký	k2eAgNnSc1d1	nízké
či	či	k8xC	či
pravidelně	pravidelně	k6eAd1	pravidelně
sekané	sekaný	k2eAgFnPc1d1	sekaná
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
travnaté	travnatý	k2eAgInPc1d1	travnatý
a	a	k8xC	a
kamenité	kamenitý	k2eAgInPc1d1	kamenitý
svahy	svah	k1gInPc1	svah
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
i	i	k9	i
hráze	hráze	k1gFnSc1	hráze
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgInPc1d1	železniční
náspy	násep	k1gInPc1	násep
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paseky	paseka	k1gFnPc4	paseka
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc4	okraj
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obrůstá	obrůstat	k5eAaImIp3nS	obrůstat
hliněná	hliněný	k2eAgNnPc4d1	hliněné
mraveniště	mraveniště	k1gNnPc4	mraveniště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
k	k	k7c3	k
půdnímu	půdní	k2eAgMnSc3d1	půdní
pH	ph	kA	ph
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ale	ale	k9	ale
substrát	substrát	k1gInSc1	substrát
chudší	chudý	k2eAgInSc1d2	chudší
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřídouškový	mateřídouškový	k2eAgInSc1d1	mateřídouškový
čaj	čaj	k1gInSc1	čaj
je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc1d1	účinný
pří	pře	k1gFnSc7	pře
onemocněních	onemocnění	k1gNnPc6	onemocnění
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
(	(	kIx(	(
<g/>
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
chřipka	chřipka	k1gFnSc1	chřipka
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
průdušek	průduška	k1gFnPc2	průduška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
poruchách	porucha	k1gFnPc6	porucha
zažívání	zažívání	k1gNnSc2	zažívání
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
plynatostí	plynatost	k1gFnSc7	plynatost
a	a	k8xC	a
kolikami	kolika	k1gFnPc7	kolika
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
vylučování	vylučování	k1gNnSc1	vylučování
žaludečních	žaludeční	k2eAgFnPc2d1	žaludeční
šťáv	šťáva	k1gFnPc2	šťáva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
navozuje	navozovat	k5eAaImIp3nS	navozovat
klid	klid	k1gInSc1	klid
<g/>
,	,	kIx,	,
příjemný	příjemný	k2eAgInSc1d1	příjemný
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
bolestech	bolest	k1gFnPc6	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
závratích	závrať	k1gFnPc6	závrať
a	a	k8xC	a
nervových	nervový	k2eAgFnPc6d1	nervová
slabostech	slabost	k1gFnPc6	slabost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
čajů	čaj	k1gInPc2	čaj
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
prášek	prášek	k1gInSc1	prášek
z	z	k7c2	z
usušených	usušený	k2eAgInPc2d1	usušený
listů	list	k1gInPc2	list
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
odpuzování	odpuzování	k1gNnSc3	odpuzování
blech	blecha	k1gFnPc2	blecha
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
pulegioides	pulegioidesa	k1gFnPc2	pulegioidesa
-	-	kIx~	-
latinsky	latinsky	k6eAd1	latinsky
Pulex	Pulex	k1gInSc1	Pulex
=	=	kIx~	=
blecha	blecha	k1gFnSc1	blecha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinné	účinný	k2eAgFnPc1d1	účinná
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
hluchavkovité	hluchavkovitý	k2eAgInPc4d1	hluchavkovitý
typické	typický	k2eAgInPc4d1	typický
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
silic	silice	k1gFnPc2	silice
(	(	kIx(	(
<g/>
thymol	thymol	k1gInSc1	thymol
<g/>
,	,	kIx,	,
cymol	cymol	k1gInSc1	cymol
<g/>
,	,	kIx,	,
karvakrol	karvakrol	k1gInSc1	karvakrol
<g/>
,	,	kIx,	,
linalol	linalol	k1gInSc1	linalol
<g/>
,	,	kIx,	,
terpineol	terpineol	k1gInSc1	terpineol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
ursolovou	ursolový	k2eAgFnSc4d1	ursolová
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc1	flavonoid
<g/>
,	,	kIx,	,
flavony	flavon	k1gInPc1	flavon
<g/>
,	,	kIx,	,
karvanol	karvanol	k1gInSc1	karvanol
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
a	a	k8xC	a
hořčiny	hořčina	k1gFnPc1	hořčina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
/	/	kIx~	/
polejová	polejový	k2eAgFnSc1d1	polejový
pravá	pravá	k1gFnSc1	pravá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pulegioides	pulegioides	k1gMnSc1	pulegioides
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
pulegioides	pulegioides	k1gMnSc1	pulegioides
<g/>
)	)	kIx)	)
-	-	kIx~	-
běžný	běžný	k2eAgInSc1d1	běžný
poddruh	poddruh	k1gInSc1	poddruh
</s>
</p>
<p>
<s>
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
/	/	kIx~	/
polejová	polejový	k2eAgFnSc1d1	polejový
kraňská	kraňský	k2eAgFnSc1d1	kraňská
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pulegioides	pulegioides	k1gMnSc1	pulegioides
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
carniolicus	carniolicus	k1gMnSc1	carniolicus
<g/>
)	)	kIx)	)
-	-	kIx~	-
nemá	mít	k5eNaImIp3nS	mít
lysé	lysý	k2eAgInPc4d1	lysý
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
jen	jen	k9	jen
v	v	k7c6	v
lomu	lom	k1gInSc6	lom
u	u	k7c2	u
Hněvotína	Hněvotín	k1gInSc2	Hněvotín
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
obecná	obecná	k1gFnSc1	obecná
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mateřídoušky	mateřídouška	k1gFnPc4	mateřídouška
polejová	polejový	k2eAgFnSc1d1	polejový
pravá	pravý	k2eAgFnSc1d1	pravá
-	-	kIx~	-
botany	botan	k1gInPc1	botan
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Mateřídoušky	mateřídouška	k1gFnPc4	mateřídouška
polejová	polejový	k2eAgFnSc1d1	polejový
kraňská	kraňský	k2eAgFnSc1d1	kraňská
-	-	kIx~	-
botany	botan	k1gInPc1	botan
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
