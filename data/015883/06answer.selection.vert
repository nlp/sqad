<s>
Největší	veliký	k2eAgInSc1d3
úspěch	úspěch	k1gInSc1
slavil	slavit	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
se	s	k7c7
singly	singl	k1gInPc7
Rosemarie	Rosemarie	k1gFnSc1
a	a	k8xC
Sternenhimmel	Sternenhimmel	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
udržely	udržet	k5eAaPmAgFnP
přes	přes	k7c4
20	#num#	k4
týdnů	týden	k1gInPc2
v	v	k7c6
německé	německý	k2eAgFnSc6d1
hitparádě	hitparáda	k1gFnSc6
a	a	k8xC
umístily	umístit	k5eAaPmAgFnP
se	se	k3xPyFc4
nejlépe	dobře	k6eAd3
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
respektive	respektive	k9
2	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>