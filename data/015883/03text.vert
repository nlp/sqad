<s>
Hubert	Hubert	k1gMnSc1
Kah	Kah	k1gMnSc1
</s>
<s>
Hubert	Hubert	k1gMnSc1
Kah	Kah	k1gMnSc1
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1961	#num#	k4
(	(	kIx(
<g/>
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Reutlingen	Reutlingen	k1gInSc1
Žánry	žánr	k1gInPc1
</s>
<s>
synthpop	synthpop	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
písničkář	písničkář	k1gMnSc1
a	a	k8xC
nahrávající	nahrávající	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
www.hubert-kah.com	www.hubert-kah.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hubert	Hubert	k1gMnSc1
Kah	Kah	k1gMnSc1
<g/>
,	,	kIx,
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Hubert	Hubert	k1gMnSc1
Kemmler	Kemmler	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1961	#num#	k4
<g/>
,	,	kIx,
Reutlingen	Reutlingen	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
německý	německý	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
komponista	komponista	k1gMnSc1
a	a	k8xC
producent	producent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žánrově	žánrově	k6eAd1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
do	do	k7c2
Neue	Neu	k1gFnSc2
Deutsche	Deutsch	k1gMnSc2
Welle	Well	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
úspěch	úspěch	k1gInSc1
slavil	slavit	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
se	s	k7c7
singly	singl	k1gInPc7
Rosemarie	Rosemarie	k1gFnSc1
a	a	k8xC
Sternenhimmel	Sternenhimmel	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
udržely	udržet	k5eAaPmAgFnP
přes	přes	k7c4
20	#num#	k4
týdnů	týden	k1gInPc2
v	v	k7c6
německé	německý	k2eAgFnSc6d1
hitparádě	hitparáda	k1gFnSc6
a	a	k8xC
umístily	umístit	k5eAaPmAgFnP
se	se	k3xPyFc4
nejlépe	dobře	k6eAd3
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
respektive	respektive	k9
2	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Rosemarie	Rosemarie	k1gFnSc1
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Sternenhimmel	Sternenhimmel	k1gInSc1
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
Scary	Scara	k1gFnSc2
Monster	monstrum	k1gNnPc2
(	(	kIx(
<g/>
Spanien	Spanien	k1gInSc1
<g/>
,	,	kIx,
Norwegen	Norwegen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
Einmal	Einmal	k1gInSc1
nur	nur	k?
mit	mit	k?
Erika	Erik	k1gMnSc2
(	(	kIx(
<g/>
…	…	k?
<g/>
dieser	dieser	k1gMnSc1
Welt	Welt	k1gMnSc1
entflieh	entflieh	k1gMnSc1
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
Engel	Engel	k1gInSc1
07	#num#	k4
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
Wenn	Wenn	k1gInSc1
der	drát	k5eAaImRp2nS
Mond	mond	k1gInSc1
die	die	k?
Sonne	Sonn	k1gInSc5
berührt	berührt	k1gInSc1
</s>
<s>
1985	#num#	k4
<g/>
:	:	kIx,
Goldene	Golden	k1gInSc5
Zeiten	Zeiten	k2eAgMnSc1d1
</s>
<s>
1985	#num#	k4
<g/>
:	:	kIx,
Angel	Angela	k1gFnPc2
07	#num#	k4
(	(	kIx(
<g/>
Japan	japan	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
<g/>
:	:	kIx,
Limousine	Limousin	k1gMnSc5
</s>
<s>
1986	#num#	k4
<g/>
:	:	kIx,
Something	Something	k1gInSc1
I	i	k8xC
Should	Should	k1gInSc1
Know	Know	k1gFnSc2
</s>
<s>
1986	#num#	k4
<g/>
:	:	kIx,
Love	lov	k1gInSc5
Is	Is	k1gMnSc1
So	So	kA
Sensible	Sensible	k1gMnSc1
(	(	kIx(
<g/>
Frankreich	Frankreich	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1987	#num#	k4
<g/>
:	:	kIx,
Military	Militara	k1gFnSc2
Drums	Drums	k1gInSc1
</s>
<s>
1989	#num#	k4
<g/>
:	:	kIx,
Welcome	Welcom	k1gInSc5
<g/>
,	,	kIx,
Machine	Machin	k1gMnSc5
Gun	Gun	k1gMnSc5
</s>
<s>
1989	#num#	k4
<g/>
:	:	kIx,
So	So	kA
Many	mana	k1gFnSc2
People	People	k1gFnSc2
</s>
<s>
1990	#num#	k4
<g/>
:	:	kIx,
Cathy	Catha	k1gFnSc2
<g/>
/	/	kIx~
<g/>
The	The	k1gMnSc5
Picture	Pictur	k1gMnSc5
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
<g/>
:	:	kIx,
C	C	kA
<g/>
'	'	kIx"
<g/>
est	est	k?
la	la	k1gNnSc2
vie	vie	k?
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
C	C	kA
<g/>
'	'	kIx"
<g/>
est	est	k?
la	la	k1gNnSc2
vie	vie	k?
(	(	kIx(
<g/>
Neue	Neue	k1gInSc1
Version	Version	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Sailing	Sailing	k1gInSc4
(	(	kIx(
<g/>
away	awaa	k1gMnSc2
from	from	k1gInSc1
me	me	k?
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
NDW	NDW	kA
Kult	kult	k1gInSc1
Mix	mix	k1gInSc1
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Love	lov	k1gInSc5
Chain	Chain	k2eAgInSc4d1
(	(	kIx(
<g/>
…	…	k?
<g/>
Maria	Maria	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
No	no	k9
Rain	Rain	k1gInSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Psycho	psycha	k1gFnSc5
Radio	radio	k1gNnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Sekunden	Sekundna	k1gFnPc2
(	(	kIx(
<g/>
Promo	Proma	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
Alba	alba	k1gFnSc1
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Meine	Mein	k1gMnSc5
Höhepunkte	Höhepunkt	k1gInSc5
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Ich	Ich	k1gFnSc1
komme	kommat	k5eAaPmIp3nS
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
Goldene	Golden	k1gMnSc5
Zeiten	Zeiten	k2eAgInSc1d1
</s>
<s>
1985	#num#	k4
<g/>
:	:	kIx,
Angel	Angela	k1gFnPc2
07	#num#	k4
(	(	kIx(
<g/>
Mini-Album	Mini-Album	k1gInSc1
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
<g/>
:	:	kIx,
Tensongs	Tensongs	k1gInSc1
</s>
<s>
1989	#num#	k4
<g/>
:	:	kIx,
Sound	Sound	k1gMnSc1
Of	Of	k1gMnSc1
My	my	k3xPp1nPc1
Heart	Heart	k1gInSc4
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Hubert	Hubert	k1gMnSc1
Kah	Kah	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Seelentaucher	Seelentauchra	k1gFnPc2
</s>
<s>
Kompilace	kompilace	k1gFnSc1
</s>
<s>
1990	#num#	k4
<g/>
:	:	kIx,
Best	Best	k1gInSc1
Of	Of	k1gFnSc2
Dance	Danka	k1gFnSc3
Hits	Hits	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Best	Best	k1gMnSc1
Of	Of	k1gMnSc1
Hubert	Hubert	k1gMnSc1
KaH	KaH	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
:	:	kIx,
Rosemarie	Rosemarie	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Portrait	Portrait	k1gInSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Meine	Mein	k1gInSc5
Besten	Besten	k2eAgInSc1d1
</s>
<s>
2011	#num#	k4
<g/>
:	:	kIx,
So	So	kA
<g/>
80	#num#	k4
<g/>
s	s	k7c7
present	present	k1gInSc1
<g/>
:	:	kIx,
Hubert	Hubert	k1gMnSc1
Kah	Kah	k1gMnSc1
(	(	kIx(
<g/>
curated	curated	k1gMnSc1
by	by	kYmCp3nS
Blank	Blank	k1gMnSc1
&	&	k?
Jones	Jones	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hubert	Hubert	k1gMnSc1
Kah	Kah	k1gFnSc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
132055465	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5544	#num#	k4
6967	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
94109803	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
55297374	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
94109803	#num#	k4
</s>
