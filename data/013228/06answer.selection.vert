<s>
Páry	pár	k1gInPc1	pár
grafitu	grafit	k1gInSc2	grafit
nelze	lze	k6eNd1	lze
normální	normální	k2eAgFnSc7d1	normální
cestou	cesta	k1gFnSc7	cesta
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uhlík	uhlík	k1gInSc1	uhlík
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc3	tání
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3500	[number]	k4	3500
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
vyšší	vysoký	k2eAgFnSc4d2	vyšší
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
4800	[number]	k4	4800
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
