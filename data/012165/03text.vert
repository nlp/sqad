<p>
<s>
Myoskeletální	Myoskeletální	k2eAgFnSc1d1	Myoskeletální
medicína	medicína	k1gFnSc1	medicína
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
myos	myosa	k1gFnPc2	myosa
=	=	kIx~	=
sval	sval	k1gInSc1	sval
<g/>
,	,	kIx,	,
skeleton	skeleton	k1gInSc1	skeleton
=	=	kIx~	=
kostra	kostra	k1gFnSc1	kostra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
diagnostikou	diagnostika	k1gFnSc7	diagnostika
a	a	k8xC	a
neoperační	operační	k2eNgFnSc7d1	neoperační
léčbou	léčba	k1gFnSc7	léčba
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
pohybovým	pohybový	k2eAgInSc7d1	pohybový
aparátem	aparát	k1gInSc7	aparát
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
vyšetřovacími	vyšetřovací	k2eAgInPc7d1	vyšetřovací
postupy	postup	k1gInPc7	postup
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
medicína	medicína	k1gFnSc1	medicína
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
různé	různý	k2eAgFnPc4d1	různá
příčiny	příčina	k1gFnPc4	příčina
bolestí	bolest	k1gFnPc2	bolest
či	či	k8xC	či
problémů	problém	k1gInPc2	problém
hybnosti	hybnost	k1gFnSc2	hybnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
šlach	šlacha	k1gFnPc2	šlacha
<g/>
,	,	kIx,	,
fascií	fascie	k1gFnSc7	fascie
aj.	aj.	kA	aj.
Je	být	k5eAaImIp3nS	být
metodou	metoda	k1gFnSc7	metoda
volby	volba	k1gFnSc2	volba
např.	např.	kA	např.
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
stádiích	stádium	k1gNnPc6	stádium
kloubních	kloubní	k2eAgFnPc2d1	kloubní
artróz	artróza	k1gFnPc2	artróza
<g/>
,	,	kIx,	,
blokád	blokáda	k1gFnPc2	blokáda
<g/>
,	,	kIx,	,
svalové	svalový	k2eAgFnSc2d1	svalová
ztuhlosti	ztuhlost	k1gFnSc2	ztuhlost
a	a	k8xC	a
dysbalance	dysbalance	k1gFnSc2	dysbalance
<g/>
,	,	kIx,	,
adheze	adheze	k1gFnSc2	adheze
měkkých	měkký	k2eAgFnPc2d1	měkká
struktur	struktura	k1gFnPc2	struktura
aj.	aj.	kA	aj.
Poruchy	porucha	k1gFnSc2	porucha
a	a	k8xC	a
bolesti	bolest	k1gFnSc2	bolest
často	často	k6eAd1	často
efektivně	efektivně	k6eAd1	efektivně
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Nepopírá	popírat	k5eNaImIp3nS	popírat
své	svůj	k3xOyFgFnPc4	svůj
profesní	profesní	k2eAgFnPc4d1	profesní
meze	mez	k1gFnPc4	mez
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
operačních	operační	k2eAgInPc2d1	operační
zákroků	zákrok	k1gInPc2	zákrok
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
indikaci	indikace	k1gFnSc6	indikace
kloubních	kloubní	k2eAgFnPc2d1	kloubní
náhrad	náhrada	k1gFnPc2	náhrada
(	(	kIx(	(
<g/>
aloplastik	aloplastika	k1gFnPc2	aloplastika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
myoskeletální	myoskeletální	k2eAgFnSc6d1	myoskeletální
medicíně	medicína	k1gFnSc6	medicína
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
tzv.	tzv.	kA	tzv.
Pražská	pražský	k2eAgFnSc1d1	Pražská
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
neurolog	neurolog	k1gMnSc1	neurolog
a	a	k8xC	a
fyzioterapeut	fyzioterapeut	k1gMnSc1	fyzioterapeut
Karel	Karel	k1gMnSc1	Karel
Lewit	Lewit	k1gMnSc1	Lewit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jandou	Janda	k1gMnSc7	Janda
<g/>
,	,	kIx,	,
Jiroutem	Jirout	k1gMnSc7	Jirout
a	a	k8xC	a
Vélem	vélum	k1gNnSc7	vélum
<g/>
,	,	kIx,	,
stanovil	stanovit	k5eAaPmAgInS	stanovit
funkční	funkční	k2eAgFnSc4d1	funkční
diagnostiku	diagnostika	k1gFnSc4	diagnostika
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ucelený	ucelený	k2eAgInSc4d1	ucelený
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
medicína	medicína	k1gFnSc1	medicína
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
např.	např.	kA	např.
od	od	k7c2	od
klasických	klasický	k2eAgFnPc2d1	klasická
masáží	masáž	k1gFnPc2	masáž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metodologie	metodologie	k1gFnSc2	metodologie
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
stanovení	stanovení	k1gNnSc2	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
medicína	medicína	k1gFnSc1	medicína
používá	používat	k5eAaImIp3nS	používat
pohovor	pohovor	k1gInSc4	pohovor
s	s	k7c7	s
pacientem	pacient	k1gMnSc7	pacient
<g/>
,	,	kIx,	,
aspekci	aspekce	k1gFnSc6	aspekce
<g/>
,	,	kIx,	,
palpační	palpační	k2eAgNnPc4d1	palpační
vyhmatávání	vyhmatávání	k1gNnPc4	vyhmatávání
a	a	k8xC	a
erudované	erudovaný	k2eAgNnSc4d1	erudované
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgNnSc4d1	funkční
myšlení	myšlení	k1gNnSc4	myšlení
terapeuta	terapeut	k1gMnSc2	terapeut
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělán	vzdělán	k2eAgMnSc1d1	vzdělán
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
obeznámen	obeznámen	k2eAgInSc1d1	obeznámen
s	s	k7c7	s
anatomickými	anatomický	k2eAgFnPc7d1	anatomická
<g/>
,	,	kIx,	,
kineziologickými	kineziologický	k2eAgFnPc7d1	Kineziologická
<g/>
,	,	kIx,	,
neurologickými	neurologický	k2eAgFnPc7d1	neurologická
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
složkami	složka	k1gFnPc7	složka
pohybového	pohybový	k2eAgInSc2d1	pohybový
aparátu	aparát	k1gInSc2	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovacím	zobrazovací	k2eAgFnPc3d1	zobrazovací
metodám	metoda	k1gFnPc3	metoda
(	(	kIx(	(
<g/>
RTG	RTG	kA	RTG
<g/>
,	,	kIx,	,
CT	CT	kA	CT
<g/>
,	,	kIx,	,
MRI	MRI	kA	MRI
<g/>
)	)	kIx)	)
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
medicína	medicína	k1gFnSc1	medicína
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vidí	vidět	k5eAaImIp3nS	vidět
i	i	k9	i
jejich	jejich	k3xOp3gNnPc4	jejich
omezení	omezení	k1gNnPc4	omezení
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
totiž	totiž	k9	totiž
nekorelují	korelovat	k5eNaImIp3nP	korelovat
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
pacientových	pacientův	k2eAgInPc2d1	pacientův
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
pacienti	pacient	k1gMnPc1	pacient
bývají	bývat	k5eAaImIp3nP	bývat
léčeni	léčit	k5eAaImNgMnP	léčit
neúspěšně	úspěšně	k6eNd1	úspěšně
a	a	k8xC	a
neúčelně	účelně	k6eNd1	účelně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lékaři	lékař	k1gMnPc1	lékař
a	a	k8xC	a
fyzioterapeuti	fyzioterapeut	k1gMnPc1	fyzioterapeut
si	se	k3xPyFc3	se
vedou	vést	k5eAaImIp3nP	vést
záznamy	záznam	k1gInPc4	záznam
o	o	k7c6	o
postupu	postup	k1gInSc6	postup
léčby	léčba	k1gFnSc2	léčba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
kontinuální	kontinuální	k2eAgNnSc1d1	kontinuální
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
kineziologických	kineziologický	k2eAgNnPc6d1	kineziologické
vyšetřeních	vyšetření	k1gNnPc6	vyšetření
by	by	kYmCp3nS	by
měli	mít	k5eAaImAgMnP	mít
věnovat	věnovat	k5eAaPmF	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
pacientovu	pacientův	k2eAgInSc3d1	pacientův
popisu	popis	k1gInSc3	popis
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jejich	jejich	k3xOp3gInPc1	jejich
nálezy	nález	k1gInPc1	nález
tomuto	tento	k3xDgInSc3	tento
popisu	popis	k1gInSc3	popis
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
<g/>
,	,	kIx,	,
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
problémy	problém	k1gInPc1	problém
jsou	být	k5eAaImIp3nP	být
rázu	ráz	k1gInSc2	ráz
funkčního	funkční	k2eAgInSc2d1	funkční
(	(	kIx(	(
<g/>
špatná	špatný	k2eAgFnSc1d1	špatná
postura	postura	k1gFnSc1	postura
<g/>
,	,	kIx,	,
nesprávné	správný	k2eNgInPc1d1	nesprávný
pohyby	pohyb	k1gInPc1	pohyb
<g/>
)	)	kIx)	)
či	či	k8xC	či
strukturálního	strukturální	k2eAgMnSc2d1	strukturální
(	(	kIx(	(
<g/>
blokády	blokáda	k1gFnSc2	blokáda
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
svalové	svalový	k2eAgFnSc3d1	svalová
dysbalance	dysbalanka	k1gFnSc3	dysbalanka
<g/>
,	,	kIx,	,
spazmy	spazmus	k1gInPc1	spazmus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
trigger	triggrat	k5eAaPmRp2nS	triggrat
pointy	pointa	k1gFnSc2	pointa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
průběžně	průběžně	k6eAd1	průběžně
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
volené	volený	k2eAgFnPc1d1	volená
terapie	terapie	k1gFnPc1	terapie
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
používají	používat	k5eAaImIp3nP	používat
širokou	široký	k2eAgFnSc4d1	široká
paletu	paleta	k1gFnSc4	paleta
terapií	terapie	k1gFnPc2	terapie
<g/>
:	:	kIx,	:
jemné	jemný	k2eAgFnSc2d1	jemná
trakce	trakce	k1gFnSc2	trakce
<g/>
,	,	kIx,	,
postizometrickou	postizometrický	k2eAgFnSc4d1	postizometrická
relaxaci	relaxace	k1gFnSc4	relaxace
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgFnSc2d1	měkká
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
manipulaci	manipulace	k1gFnSc4	manipulace
a	a	k8xC	a
mobilizaci	mobilizace	k1gFnSc4	mobilizace
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
protažení	protažení	k1gNnSc2	protažení
fascií	fascie	k1gFnPc2	fascie
<g/>
,	,	kIx,	,
posun	posun	k1gInSc1	posun
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
masáž	masáž	k1gFnSc1	masáž
<g/>
,	,	kIx,	,
léčebná	léčebný	k2eAgNnPc1d1	léčebné
cvičení	cvičení	k1gNnPc1	cvičení
aj.	aj.	kA	aj.
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
plošně	plošně	k6eAd1	plošně
působících	působící	k2eAgInPc2d1	působící
léků	lék	k1gInPc2	lék
tak	tak	k9	tak
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
medicína	medicína	k1gFnSc1	medicína
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
strukturální	strukturální	k2eAgFnPc4d1	strukturální
příčiny	příčina	k1gFnPc4	příčina
bolestí	bolest	k1gFnPc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
důležitou	důležitý	k2eAgFnSc4d1	důležitá
podmínku	podmínka	k1gFnSc4	podmínka
úspěchu	úspěch	k1gInSc2	úspěch
považuje	považovat	k5eAaImIp3nS	považovat
pacientovu	pacientův	k2eAgFnSc4d1	pacientova
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
cvičení	cvičení	k1gNnSc4	cvičení
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
problém	problém	k1gInSc4	problém
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
pacienta	pacient	k1gMnSc2	pacient
k	k	k7c3	k
osvojení	osvojení	k1gNnSc3	osvojení
si	se	k3xPyFc3	se
správných	správný	k2eAgInPc2d1	správný
pohybových	pohybový	k2eAgInPc2d1	pohybový
vzorců	vzorec	k1gInPc2	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
pacientův	pacientův	k2eAgInSc4d1	pacientův
bezbolestný	bezbolestný	k2eAgInSc4d1	bezbolestný
a	a	k8xC	a
hladký	hladký	k2eAgInSc4d1	hladký
pohyb	pohyb	k1gInSc4	pohyb
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
minimalizace	minimalizace	k1gFnSc1	minimalizace
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
Manipulační	manipulační	k2eAgFnSc1d1	manipulační
léčba	léčba	k1gFnSc1	léčba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
léčebné	léčebný	k2eAgFnPc4d1	léčebná
rehabilitace	rehabilitace	k1gFnPc4	rehabilitace
(	(	kIx(	(
<g/>
LEWIT	LEWIT	kA	LEWIT
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Manuální	manuální	k2eAgFnSc1d1	manuální
medicína	medicína	k1gFnSc1	medicína
(	(	kIx(	(
<g/>
RYCHLÍKOVÁ	rychlíkový	k2eAgFnSc1d1	rychlíková
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Funkční	funkční	k2eAgInPc4d1	funkční
poruchy	poruch	k1gInPc4	poruch
kloubů	kloub	k1gInPc2	kloub
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
terapie	terapie	k1gFnPc1	terapie
(	(	kIx(	(
<g/>
RYCHLÍKOVÁ	rychlíkový	k2eAgFnSc1d1	rychlíková
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bolesti	bolest	k1gFnPc1	bolest
v	v	k7c6	v
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Protiklady	protiklad	k1gInPc1	protiklad
<g/>
,	,	kIx,	,
tendence	tendence	k1gFnPc1	tendence
a	a	k8xC	a
řešení	řešení	k1gNnSc1	řešení
(	(	kIx(	(
<g/>
JANDA	Janda	k1gMnSc1	Janda
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skryto	skryt	k2eAgNnSc1d1	skryto
v	v	k7c6	v
páteři	páteř	k1gFnSc6	páteř
(	(	kIx(	(
<g/>
RYCHLÍKOVÁ	rychlíkový	k2eAgFnSc1d1	rychlíková
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
zad	záda	k1gNnPc2	záda
(	(	kIx(	(
<g/>
RAŠEV	RAŠEV	kA	RAŠEV
Eugen	Eugen	k1gInSc1	Eugen
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cvičení	cvičení	k1gNnSc1	cvičení
a	a	k8xC	a
prevence	prevence	k1gFnSc1	prevence
civilizačních	civilizační	k2eAgFnPc2d1	civilizační
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hygiena	hygiena	k1gFnSc1	hygiena
pohybového	pohybový	k2eAgInSc2d1	pohybový
aparátu	aparát	k1gInSc2	aparát
(	(	kIx(	(
<g/>
VÉLE	VÉLE	kA	VÉLE
<g/>
,	,	kIx,	,
ČUMPELÍK	ČUMPELÍK	kA	ČUMPELÍK
<g/>
,	,	kIx,	,
BORTLÍKOVÁ	BORTLÍKOVÁ	kA	BORTLÍKOVÁ
<g/>
,	,	kIx,	,
STOLZ	STOLZ	kA	STOLZ
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
