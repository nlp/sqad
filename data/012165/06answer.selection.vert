<s>
Myoskeletální	Myoskeletální	k2eAgFnSc1d1	Myoskeletální
medicína	medicína	k1gFnSc1	medicína
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
myos	myosa	k1gFnPc2	myosa
=	=	kIx~	=
sval	sval	k1gInSc1	sval
<g/>
,	,	kIx,	,
skeleton	skeleton	k1gInSc1	skeleton
=	=	kIx~	=
kostra	kostra	k1gFnSc1	kostra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
diagnostikou	diagnostika	k1gFnSc7	diagnostika
a	a	k8xC	a
neoperační	operační	k2eNgFnSc7d1	neoperační
léčbou	léčba	k1gFnSc7	léčba
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
pohybovým	pohybový	k2eAgInSc7d1	pohybový
aparátem	aparát	k1gInSc7	aparát
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
