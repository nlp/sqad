<s>
Jaký	jaký	k3yIgInSc1	jaký
druh	druh	k1gInSc1	druh
medicíny	medicína	k1gFnSc2	medicína
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
diagnostikou	diagnostika	k1gFnSc7	diagnostika
a	a	k8xC	a
neoperační	operační	k2eNgFnSc7d1	neoperační
léčbou	léčba	k1gFnSc7	léčba
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
pohybovým	pohybový	k2eAgInSc7d1	pohybový
aparátem	aparát	k1gInSc7	aparát
člověka	člověk	k1gMnSc2	člověk
<g/>
?	?	kIx.	?
</s>
