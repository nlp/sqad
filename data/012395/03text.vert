<p>
<s>
Strakapoud	strakapoud	k1gMnSc1	strakapoud
bělohřbetý	bělohřbetý	k2eAgMnSc1d1	bělohřbetý
(	(	kIx(	(
<g/>
Dendrocopos	Dendrocopos	k1gMnSc1	Dendrocopos
leucotos	leucotos	k1gMnSc1	leucotos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
strakapouda	strakapoud	k1gMnSc2	strakapoud
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
šplhavců	šplhavec	k1gMnPc2	šplhavec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Znaky	znak	k1gInPc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
strakapoudu	strakapoud	k1gMnSc3	strakapoud
velkému	velký	k2eAgMnSc3d1	velký
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
šatech	šat	k1gInPc6	šat
liší	lišit	k5eAaImIp3nP	lišit
bílým	bílý	k2eAgInSc7d1	bílý
hřbetem	hřbet	k1gInSc7	hřbet
(	(	kIx(	(
<g/>
černobíle	černobíle	k6eAd1	černobíle
proužkovaný	proužkovaný	k2eAgMnSc1d1	proužkovaný
u	u	k7c2	u
jižní	jižní	k2eAgFnSc2d1	jižní
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
lilfordii	lilfordie	k1gFnSc4	lilfordie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
rozdíly	rozdíl	k1gInPc7	rozdíl
jsou	být	k5eAaImIp3nP	být
světle	světle	k6eAd1	světle
červené	červený	k2eAgFnPc1d1	červená
podocasní	podocasní	k2eAgFnPc1d1	podocasní
krovky	krovka	k1gFnPc1	krovka
<g/>
,	,	kIx,	,
čárkované	čárkovaný	k2eAgFnPc1d1	čárkovaná
boky	boka	k1gFnPc1	boka
na	na	k7c6	na
béžovém	béžový	k2eAgInSc6d1	béžový
podkladu	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
proužek	proužek	k1gInSc1	proužek
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
nesahá	sahat	k5eNaImIp3nS	sahat
k	k	k7c3	k
temeni	temeno	k1gNnSc3	temeno
<g/>
;	;	kIx,	;
chybí	chybět	k5eAaImIp3nS	chybět
mu	on	k3xPp3gNnSc3	on
rovněž	rovněž	k9	rovněž
bílé	bílý	k2eAgFnSc2d1	bílá
ramenní	ramenní	k2eAgFnSc2d1	ramenní
krovky	krovka	k1gFnSc2	krovka
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
červené	červený	k2eAgNnSc4d1	červené
temeno	temeno	k1gNnSc4	temeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
ve	v	k7c6	v
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
odumírajících	odumírající	k2eAgInPc2d1	odumírající
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pravidelně	pravidelně	k6eAd1	pravidelně
u	u	k7c2	u
východních	východní	k2eAgFnPc2d1	východní
hranic	hranice	k1gFnPc2	hranice
od	od	k7c2	od
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
na	na	k7c4	na
sever	sever	k1gInSc4	sever
po	po	k7c4	po
Beskydy	Beskydy	k1gFnPc4	Beskydy
<g/>
,	,	kIx,	,
izolovaně	izolovaně	k6eAd1	izolovaně
pak	pak	k6eAd1	pak
v	v	k7c6	v
Chřibech	Chřiby	k1gInPc6	Chřiby
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
oblastem	oblast	k1gFnPc3	oblast
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
hnízdění	hnízdění	k1gNnSc2	hnízdění
patří	patřit	k5eAaImIp3nS	patřit
Šumava	Šumava	k1gFnSc1	Šumava
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
15-20	[number]	k4	15-20
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
s	s	k7c7	s
Novohradskými	novohradský	k2eAgFnPc7d1	Novohradská
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
požerku	požerek	k1gInSc2	požerek
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
hnízdění	hnízdění	k1gNnSc1	hnízdění
také	také	k9	také
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
početnost	početnost	k1gFnSc1	početnost
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
150-250	[number]	k4	150-250
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
strakapoud	strakapoud	k1gMnSc1	strakapoud
bělohřbetý	bělohřbetý	k2eAgMnSc1d1	bělohřbetý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
strakapoud	strakapoud	k1gMnSc1	strakapoud
bělohřbetý	bělohřbetý	k2eAgMnSc1d1	bělohřbetý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Dendrocopos	Dendrocopos	k1gInSc1	Dendrocopos
leucotos	leucotos	k1gInSc1	leucotos
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
