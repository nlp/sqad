<s>
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
přirozených	přirozený	k2eAgInPc2d1	přirozený
logaritmů	logaritmus	k1gInPc2	logaritmus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jako	jako	k9	jako
Napierova	Napierův	k2eAgFnSc1d1	Napierův
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
;	;	kIx,	;
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
značeno	značit	k5eAaImNgNnS	značit
písmenem	písmeno	k1gNnSc7	písmeno
e	e	k0	e
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
matematických	matematický	k2eAgFnPc2d1	matematická
konstant	konstanta	k1gFnPc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
podle	podle	k7c2	podle
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
matematika	matematik	k1gMnSc2	matematik
Leonharda	Leonhard	k1gMnSc2	Leonhard
Eulera	Euler	k1gMnSc2	Euler
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
skotského	skotský	k2eAgMnSc4d1	skotský
matematika-amatéra	matematikamatér	k1gMnSc4	matematika-amatér
Johna	John	k1gMnSc4	John
Napiera	Napier	k1gMnSc4	Napier
<g/>
,	,	kIx,	,
objevitele	objevitel	k1gMnSc4	objevitel
logaritmu	logaritmus	k1gInSc2	logaritmus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přibližná	přibližný	k2eAgFnSc1d1	přibližná
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
2,718	[number]	k4	2,718
<g/>
28	[number]	k4	28
18284	[number]	k4	18284
59045	[number]	k4	59045
23536	[number]	k4	23536
02874	[number]	k4	02874
71352	[number]	k4	71352
<g/>
...	...	k?	...
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
alternativních	alternativní	k2eAgFnPc2d1	alternativní
ekvivalentních	ekvivalentní	k2eAgFnPc2d1	ekvivalentní
definic	definice	k1gFnPc2	definice
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgMnPc1d3	nejčastější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
jako	jako	k8xC	jako
limita	limita	k1gFnSc1	limita
následující	následující	k2eAgFnSc2d1	následující
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
jako	jako	k8xS	jako
součet	součet	k1gInSc1	součet
následující	následující	k2eAgFnSc2d1	následující
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
n	n	k0	n
!	!	kIx.	!
</s>
<s>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
0	[number]	k4	0
!	!	kIx.	!
</s>
<s>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
!	!	kIx.	!
</s>
<s>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
2	[number]	k4	2
!	!	kIx.	!
</s>
<s>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
4	[number]	k4	4
!	!	kIx.	!
</s>
<s>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
n	n	k0	n
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
0	[number]	k4	0
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
1	[number]	k4	1
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
4	[number]	k4	4
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
číslo	číslo	k1gNnSc1	číslo
x	x	k?	x
>	>	kIx)	>
0	[number]	k4	0
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
:	:	kIx,	:
</s>
<s>
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
iracionální	iracionální	k2eAgMnSc1d1	iracionální
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jeho	jeho	k3xOp3gFnSc4	jeho
desetinný	desetinný	k2eAgInSc1d1	desetinný
rozvoj	rozvoj	k1gInSc1	rozvoj
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
a	a	k8xC	a
neperiodický	periodický	k2eNgInSc1d1	neperiodický
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
transcendentní	transcendentní	k2eAgNnSc1d1	transcendentní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gNnSc4	on
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
kořen	kořen	k1gInSc4	kořen
konečného	konečný	k2eAgInSc2d1	konečný
mnohočlenu	mnohočlen	k1gInSc2	mnohočlen
s	s	k7c7	s
celočíselnými	celočíselný	k2eAgInPc7d1	celočíselný
koeficienty	koeficient	k1gInPc7	koeficient
<g/>
.	.	kIx.	.
</s>
<s>
2,718	[number]	k4	2,718
<g/>
2818284	[number]	k4	2818284
5904523536	[number]	k4	5904523536
0287471352	[number]	k4	0287471352
6624977572	[number]	k4	6624977572
4709369995	[number]	k4	4709369995
9574966967	[number]	k4	9574966967
6277240766	[number]	k4	6277240766
3035354759	[number]	k4	3035354759
4571382178	[number]	k4	4571382178
5251664274	[number]	k4	5251664274
2746639193	[number]	k4	2746639193
2003059921	[number]	k4	2003059921
8174135966	[number]	k4	8174135966
2904357290	[number]	k4	2904357290
0334295260	[number]	k4	0334295260
5956307381	[number]	k4	5956307381
3232862794	[number]	k4	3232862794
3490763233	[number]	k4	3490763233
8298807531	[number]	k4	8298807531
9525101901	[number]	k4	9525101901
1573834187	[number]	k4	1573834187
9307021540	[number]	k4	9307021540
8914993488	[number]	k4	8914993488
4167509244	[number]	k4	4167509244
7614606680	[number]	k4	7614606680
8226480016	[number]	k4	8226480016
8477411853	[number]	k4	8477411853
7423454424	[number]	k4	7423454424
3710753907	[number]	k4	3710753907
7744992069	[number]	k4	7744992069
<g />
.	.	kIx.	.
</s>
<s>
5517027618	[number]	k4	5517027618
3860626133	[number]	k4	3860626133
1384583000	[number]	k4	1384583000
7520449338	[number]	k4	7520449338
2656029760	[number]	k4	2656029760
6737113200	[number]	k4	6737113200
7093287091	[number]	k4	7093287091
2744374704	[number]	k4	2744374704
7230696977	[number]	k4	7230696977
2093101416	[number]	k4	2093101416
9283681902	[number]	k4	9283681902
5515108657	[number]	k4	5515108657
4637721112	[number]	k4	4637721112
5238978442	[number]	k4	5238978442
5056953696	[number]	k4	5056953696
7707854499	[number]	k4	7707854499
6996794686	[number]	k4	6996794686
4454905987	[number]	k4	4454905987
9316368892	[number]	k4	9316368892
3009879312	[number]	k4	3009879312
7736178215	[number]	k4	7736178215
4249992295	[number]	k4	4249992295
7635148220	[number]	k4	7635148220
8269895193	[number]	k4	8269895193
6680331825	[number]	k4	6680331825
2886939849	[number]	k4	2886939849
6465105820	[number]	k4	6465105820
9392398294	[number]	k4	9392398294
8879332036	[number]	k4	8879332036
2509443117	[number]	k4	2509443117
3012381970	[number]	k4	3012381970
6841614039	[number]	k4	6841614039
7019837679	[number]	k4	7019837679
3206832823	[number]	k4	3206832823
7646480429	[number]	k4	7646480429
5311802328	[number]	k4	5311802328
7825098194	[number]	k4	7825098194
5581530175	[number]	k4	5581530175
6717361332	[number]	k4	6717361332
0698112509	[number]	k4	0698112509
9618188159	[number]	k4	9618188159
3041690351	[number]	k4	3041690351
5988885193	[number]	k4	5988885193
4580727386	[number]	k4	4580727386
6738589422	[number]	k4	6738589422
8792284998	[number]	k4	8792284998
9208680582	[number]	k4	9208680582
5749279610	[number]	k4	5749279610
4841984443	[number]	k4	4841984443
6346324496	[number]	k4	6346324496
<g />
.	.	kIx.	.
</s>
<s>
8487560233	[number]	k4	8487560233
6248270419	[number]	k4	6248270419
7862320900	[number]	k4	7862320900
2160990235	[number]	k4	2160990235
3043699418	[number]	k4	3043699418
4914631409	[number]	k4	4914631409
3431738143	[number]	k4	3431738143
6405462531	[number]	k4	6405462531
5209618369	[number]	k4	5209618369
0888707016	[number]	k4	0888707016
7683964243	[number]	k4	7683964243
7814059271	[number]	k4	7814059271
4563549061	[number]	k4	4563549061
3031072085	[number]	k4	3031072085
1038375051	[number]	k4	1038375051
0115747704	[number]	k4	0115747704
1718986106	[number]	k4	1718986106
8739696552	[number]	k4	8739696552
1267154688	[number]	k4	1267154688
9570350	[number]	k4	9570350
<g/>
...	...	k?	...
Exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
Logaritmus	logaritmus	k1gInSc4	logaritmus
Číslo	číslo	k1gNnSc4	číslo
pí	pí	k1gNnSc2	pí
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eulerovo	Eulerův	k2eAgNnSc4d1	Eulerovo
číslo	číslo	k1gNnSc4	číslo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
A001113	A001113	k1gFnSc1	A001113
v	v	k7c6	v
OEIS	OEIS	kA	OEIS
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Mathworld	Mathworlda	k1gFnPc2	Mathworlda
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
na	na	k7c4	na
milion	milion	k4xCgInSc4	milion
desetinných	desetinný	k2eAgInPc2d1	desetinný
míst	místo	k1gNnPc2	místo
</s>
