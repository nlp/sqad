<s>
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
iracionální	iracionální	k2eAgMnSc1d1	iracionální
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jeho	jeho	k3xOp3gFnSc4	jeho
desetinný	desetinný	k2eAgInSc1d1	desetinný
rozvoj	rozvoj	k1gInSc1	rozvoj
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
a	a	k8xC	a
neperiodický	periodický	k2eNgInSc1d1	neperiodický
<g/>
.	.	kIx.	.
</s>
