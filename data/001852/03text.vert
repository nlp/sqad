<s>
Skepticismus	skepticismus	k1gInSc1	skepticismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
skeptomai	skeptoma	k1gFnSc2	skeptoma
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
pochybuji	pochybovat	k5eAaImIp1nS	pochybovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
blízký	blízký	k2eAgInSc1d1	blízký
agnosticismu	agnosticismus	k1gInSc3	agnosticismus
a	a	k8xC	a
nihilismu	nihilismus	k1gInSc3	nihilismus
<g/>
.	.	kIx.	.
</s>
<s>
Skepticismus	skepticismus	k1gInSc1	skepticismus
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
sofistů	sofista	k1gMnPc2	sofista
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
skeptikové	skeptik	k1gMnPc1	skeptik
ukazovali	ukazovat	k5eAaImAgMnP	ukazovat
na	na	k7c4	na
relativnost	relativnost	k1gFnSc4	relativnost
lidského	lidský	k2eAgNnSc2d1	lidské
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
formální	formální	k2eAgFnSc4d1	formální
nedokazatelnost	nedokazatelnost	k1gFnSc4	nedokazatelnost
a	a	k8xC	a
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
označení	označení	k1gNnSc4	označení
skeptik	skeptik	k1gMnSc1	skeptik
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
<g/>
:	:	kIx,	:
mají	mít	k5eAaImIp3nP	mít
nedůvěřivý	důvěřivý	k2eNgInSc4d1	nedůvěřivý
postoj	postoj	k1gInSc4	postoj
nebo	nebo	k8xC	nebo
smýšlení	smýšlení	k1gNnSc4	smýšlení
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
nebo	nebo	k8xC	nebo
specifikovaných	specifikovaný	k2eAgFnPc6d1	specifikovaná
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
soustavně	soustavně	k6eAd1	soustavně
kriticky	kriticky	k6eAd1	kriticky
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Filosofický	filosofický	k2eAgInSc4d1	filosofický
skepticismus	skepticismus	k1gInSc4	skepticismus
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
filosofického	filosofický	k2eAgInSc2d1	filosofický
skepticismu	skepticismus	k1gInSc2	skepticismus
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pochybnosti	pochybnost	k1gFnSc2	pochybnost
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
filosofického	filosofický	k2eAgNnSc2d1	filosofické
řešení	řešení	k1gNnSc2	řešení
až	až	k9	až
po	po	k7c4	po
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
a	a	k8xC	a
nihilismus	nihilismus	k1gInSc4	nihilismus
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
antičtí	antický	k2eAgMnPc1d1	antický
filosofičtí	filosofický	k2eAgMnPc1d1	filosofický
skeptici	skeptik	k1gMnPc1	skeptik
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k6eAd1	jen
díky	díky	k7c3	díky
apriorním	apriorní	k2eAgInPc3d1	apriorní
předpokladům	předpoklad	k1gInPc3	předpoklad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nelze	lze	k6eNd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
ze	z	k7c2	z
smyslové	smyslový	k2eAgFnSc2d1	smyslová
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
jeho	jeho	k3xOp3gFnSc4	jeho
pravdivost	pravdivost	k1gFnSc4	pravdivost
neustále	neustále	k6eAd1	neustále
ověřovat	ověřovat	k5eAaImF	ověřovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
filosofii	filosofie	k1gFnSc6	filosofie
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
skepticismus	skepticismus	k1gInSc4	skepticismus
na	na	k7c4	na
osobnostní	osobnostní	k2eAgInPc4d1	osobnostní
rysy	rys	k1gInPc4	rys
školy	škola	k1gFnSc2	škola
"	"	kIx"	"
<g/>
Skeptikoi	Skeptiko	k1gFnSc2	Skeptiko
<g/>
"	"	kIx"	"
-	-	kIx~	-
filosofů	filosof	k1gMnPc2	filosof
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
že	že	k9	že
"	"	kIx"	"
<g/>
nic	nic	k3yNnSc1	nic
netvrdili	tvrdit	k5eNaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
jenom	jenom	k9	jenom
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
zastává	zastávat	k5eAaImIp3nS	zastávat
filosofický	filosofický	k2eAgInSc1d1	filosofický
skepticismus	skepticismus	k1gInSc1	skepticismus
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
postulátu	postulát	k1gInSc2	postulát
konečné	konečný	k2eAgFnSc2d1	konečná
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
systematický	systematický	k2eAgInSc1d1	systematický
zdroj	zdroj	k1gInSc1	zdroj
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Sexta	sexta	k1gFnSc1	sexta
Empirica	Empiricum	k1gNnPc1	Empiricum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
antických	antický	k2eAgMnPc2d1	antický
skeptiků	skeptik	k1gMnPc2	skeptik
nezanechala	zanechat	k5eNaPmAgFnS	zanechat
žádné	žádný	k3yNgFnPc4	žádný
písemnosti	písemnost	k1gFnPc4	písemnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
spisy	spis	k1gInPc1	spis
nezachovaly	zachovat	k5eNaPmAgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vědecký	vědecký	k2eAgInSc4d1	vědecký
skepticismus	skepticismus	k1gInSc4	skepticismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
kruzích	kruh	k1gInPc6	kruh
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
skepticismus	skepticismus	k1gInSc1	skepticismus
(	(	kIx(	(
<g/>
též	též	k9	též
vědecký	vědecký	k2eAgInSc4d1	vědecký
nebo	nebo	k8xC	nebo
racionální	racionální	k2eAgInSc4d1	racionální
skepticismus	skepticismus	k1gInSc4	skepticismus
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
kritické	kritický	k2eAgNnSc4d1	kritické
zkoumání	zkoumání	k1gNnSc4	zkoumání
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
věda	věda	k1gFnSc1	věda
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
odmítání	odmítání	k1gNnSc4	odmítání
jejich	jejich	k3xOp3gNnSc2	jejich
apriorního	apriorní	k2eAgNnSc2d1	apriorní
přijímání	přijímání	k1gNnSc2	přijímání
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
racionálního	racionální	k2eAgInSc2d1	racionální
skepticismu	skepticismus	k1gInSc2	skepticismus
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
neprokáže	prokázat	k5eNaPmIp3nS	prokázat
opak	opak	k1gInSc1	opak
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
u	u	k7c2	u
jakýchkoliv	jakýkoliv	k3yIgNnPc2	jakýkoliv
tvrzení	tvrzení	k1gNnPc2	tvrzení
nebo	nebo	k8xC	nebo
teorií	teorie	k1gFnPc2	teorie
předpokládat	předpokládat	k5eAaImF	předpokládat
pravdivost	pravdivost	k1gFnSc4	pravdivost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
racionální	racionální	k2eAgInSc4d1	racionální
skepticismus	skepticismus	k1gInSc4	skepticismus
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
každá	každý	k3xTgFnSc1	každý
teorie	teorie	k1gFnSc1	teorie
skutečně	skutečně	k6eAd1	skutečně
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
aby	aby	kYmCp3nP	aby
měla	mít	k5eAaImAgFnS	mít
některé	některý	k3yIgInPc4	některý
důležité	důležitý	k2eAgInPc4d1	důležitý
atributy	atribut	k1gInPc4	atribut
(	(	kIx(	(
<g/>
např.	např.	kA	např.
falzifikovatelnost	falzifikovatelnost	k1gFnSc1	falzifikovatelnost
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
konzistence	konzistence	k1gFnSc1	konzistence
<g/>
,	,	kIx,	,
shoda	shoda	k1gFnSc1	shoda
s	s	k7c7	s
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
je	být	k5eAaImIp3nS	být
racionální	racionální	k2eAgInSc1d1	racionální
skepticismus	skepticismus	k1gInSc1	skepticismus
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
zejména	zejména	k9	zejména
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
pseudovědeckých	pseudovědecký	k2eAgFnPc2d1	pseudovědecká
a	a	k8xC	a
nevědeckých	vědecký	k2eNgFnPc2d1	nevědecká
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
reprezentováno	reprezentovat	k5eAaImNgNnS	reprezentovat
např.	např.	kA	např.
organizací	organizace	k1gFnPc2	organizace
CSI	CSI	kA	CSI
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pak	pak	k6eAd1	pak
klubem	klub	k1gInSc7	klub
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
<g/>
.	.	kIx.	.
</s>
<s>
Náboženský	náboženský	k2eAgInSc1d1	náboženský
skepticismus	skepticismus	k1gInSc1	skepticismus
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
pochybnosti	pochybnost	k1gFnPc4	pochybnost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
existence	existence	k1gFnSc1	existence
božských	božský	k2eAgNnPc2d1	božské
stvoření	stvoření	k1gNnPc2	stvoření
nebo	nebo	k8xC	nebo
zázraků	zázrak	k1gInPc2	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Náboženští	náboženský	k2eAgMnPc1d1	náboženský
skeptici	skeptik	k1gMnPc1	skeptik
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
ateisti	ateisti	k?	ateisti
nebo	nebo	k8xC	nebo
agnostici	agnostik	k1gMnPc1	agnostik
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
pseudoskepticismus	pseudoskepticismus	k1gInSc1	pseudoskepticismus
popisuje	popisovat	k5eAaImIp3nS	popisovat
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
skeptické	skeptický	k2eAgNnSc1d1	skeptické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skeptické	skeptický	k2eAgNnSc1d1	skeptické
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
zpopularizoval	zpopularizovat	k5eAaPmAgInS	zpopularizovat
Marcello	Marcello	k1gNnSc4	Marcello
Truzzi	Truzze	k1gFnSc3	Truzze
a	a	k8xC	a
definoval	definovat	k5eAaBmAgMnS	definovat
pseudoskeptiky	pseudoskeptika	k1gFnPc4	pseudoskeptika
jako	jako	k8xC	jako
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
negativní	negativní	k2eAgInPc1d1	negativní
<g/>
,	,	kIx,	,
než	než	k8xS	než
agnostické	agnostický	k2eAgNnSc1d1	agnostické
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
skeptiky	skeptik	k1gMnPc4	skeptik
<g/>
.	.	kIx.	.
</s>
<s>
Rektor	rektor	k1gMnSc1	rektor
arizonské	arizonský	k2eAgFnSc2d1	Arizonská
univerzity	univerzita	k1gFnSc2	univerzita
Rochus	Rochus	k1gMnSc1	Rochus
Boerner	Boerner	k1gMnSc1	Boerner
popisuje	popisovat	k5eAaImIp3nS	popisovat
pseudoskeptiky	pseudoskeptika	k1gFnPc4	pseudoskeptika
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
víry	víra	k1gFnSc2	víra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
než	než	k8xS	než
"	"	kIx"	"
<g/>
nevěřící	nevěřící	k1gFnSc1	nevěřící
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
princetonské	princetonský	k2eAgFnSc2d1	princetonský
univerzity	univerzita	k1gFnSc2	univerzita
Gregory	Gregor	k1gMnPc4	Gregor
N.	N.	kA	N.
Derry	Derra	k1gMnSc2	Derra
popisuje	popisovat	k5eAaImIp3nS	popisovat
myšlení	myšlení	k1gNnSc4	myšlení
pseudoskeptiků	pseudoskeptik	k1gMnPc2	pseudoskeptik
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
..	..	k?	..
směřující	směřující	k2eAgNnSc1d1	směřující
k	k	k7c3	k
myšlenkám	myšlenka	k1gFnPc3	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
předjímaným	předjímaný	k2eAgInSc7d1	předjímaný
závěrem	závěr	k1gInSc7	závěr
<g/>
.	.	kIx.	.
</s>
