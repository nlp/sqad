<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
zpopularizoval	zpopularizovat	k5eAaPmAgInS	zpopularizovat
Marcello	Marcello	k1gNnSc4	Marcello
Truzzi	Truzze	k1gFnSc3	Truzze
a	a	k8xC	a
definoval	definovat	k5eAaBmAgMnS	definovat
pseudoskeptiky	pseudoskeptika	k1gFnPc4	pseudoskeptika
jako	jako	k8xC	jako
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
negativní	negativní	k2eAgInPc1d1	negativní
<g/>
,	,	kIx,	,
než	než	k8xS	než
agnostické	agnostický	k2eAgNnSc1d1	agnostické
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
skeptiky	skeptik	k1gMnPc4	skeptik
<g/>
.	.	kIx.	.
</s>
