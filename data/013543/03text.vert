<s>
Gaussia	Gaussia	k1gFnSc1
(	(	kIx(
<g/>
planetka	planetka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
GaussiaIdentifikátory	GaussiaIdentifikátor	k1gInPc1
Označení	označení	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
1001	#num#	k4
<g/>
)	)	kIx)
<g/>
Gaussia	Gaussia	k1gFnSc1
Předběžné	předběžný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
1923	#num#	k4
OA	OA	kA
Katalogové	katalogový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
1001	#num#	k4
<g/>
)	)	kIx)
Objevena	objeven	k2eAgNnPc1d1
Datum	datum	k1gNnSc4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1923	#num#	k4
Místo	místo	k7c2
</s>
<s>
krymská	krymský	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Sergej	Sergej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Beljavskij	Beljavskij	k1gFnPc2
Elementy	element	k1gInPc1
dráhy	dráha	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Epocha	epocha	k1gFnSc1
</s>
<s>
2453800.52453800	2453800.52453800	k4
<g/>
.5	.5	k4
JD	JD	kA
Velká	velký	k2eAgFnSc1d1
poloosa	poloosa	k1gFnSc1
</s>
<s>
3,203	3,203	k4
<g/>
1	#num#	k4
AU	au	k0
477261900	#num#	k4
km	km	kA
Excentricita	excentricita	k1gFnSc1
</s>
<s>
3,634	3,634	k4
<g/>
7	#num#	k4
Perihel	perihel	k1gInSc1
</s>
<s>
2,771	2,771	k4
<g/>
5	#num#	k4
AU	au	k0
412953500	#num#	k4
km	km	kA
Afel	afel	k1gInSc1
</s>
<s>
3,634	3,634	k4
<g/>
7	#num#	k4
AU	au	k0
541570300	#num#	k4
km	km	kA
Perioda	perioda	k1gFnSc1
(	(	kIx(
<g/>
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
5.734	5.734	k4
roku	rok	k1gInSc2
2097,045	2097,045	k4
dne	den	k1gInSc2
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
k	k	k7c3
ekliptice	ekliptika	k1gFnSc3
</s>
<s>
9.313	9.313	k4
<g/>
°	°	k?
Délka	délka	k1gFnSc1
vzestupného	vzestupný	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
</s>
<s>
259.568	259.568	k4
<g/>
°	°	k?
Argument	argument	k1gInSc1
šířky	šířka	k1gFnSc2
perihelu	perihel	k1gInSc2
</s>
<s>
139.950	139.950	k4
<g/>
°	°	k?
Střední	střední	k2eAgFnSc2d1
anomálie	anomálie	k1gFnSc2
</s>
<s>
123,699	123,699	k4
<g/>
°	°	k?
Fyzikální	fyzikální	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
9,5	9,5	k4
mag	mag	k?
Odhadovaný	odhadovaný	k2eAgInSc4d1
průměr	průměr	k1gInSc4
</s>
<s>
74,66	74,66	k4
km	km	kA
Albedo	Albedo	k1gNnSc1
</s>
<s>
0,039	0,039	k4
</s>
<s>
(	(	kIx(
<g/>
1001	#num#	k4
<g/>
)	)	kIx)
Gaussia	Gaussia	k1gFnSc1
je	být	k5eAaImIp3nS
planetka	planetka	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
pásu	pás	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
objevil	objevit	k5eAaPmAgInS
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1923	#num#	k4
ruský	ruský	k2eAgMnSc1d1
astronom	astronom	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Beljavskij	Beljavskij	k1gMnSc1
při	při	k7c6
pozorování	pozorování	k1gNnSc6
na	na	k7c6
krymské	krymský	k2eAgFnSc6d1
observatoři	observatoř	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Planetka	planetka	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
podle	podle	k7c2
německého	německý	k2eAgMnSc2d1
matematika	matematik	k1gMnSc2
Carla	Carl	k1gMnSc2
Friedricha	Friedrich	k1gMnSc2
Gauße	Gauß	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Gaussia	Gaussia	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
planetek	planetka	k1gFnPc2
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
1001	#num#	k4
Gaussia	Gaussium	k1gNnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
