<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
meloun	meloun	k1gInSc1	meloun
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
červenou	červený	k2eAgFnSc7d1	červená
dužninou	dužnina	k1gFnSc7	dužnina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
semínek	semínko	k1gNnPc2	semínko
<g/>
.	.	kIx.	.
</s>
