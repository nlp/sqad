<s>
Meloun	meloun	k1gInSc1	meloun
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
zralé	zralý	k2eAgNnSc4d1	zralé
jablko	jablko	k1gNnSc4	jablko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
plodové	plodový	k2eAgFnSc2d1	plodová
zeleniny	zelenina	k1gFnSc2	zelenina
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
tykvovitých	tykvovitý	k2eAgMnPc2d1	tykvovitý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
považovaný	považovaný	k2eAgInSc1d1	považovaný
také	také	k9	také
za	za	k7c4	za
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
botanického	botanický	k2eAgNnSc2d1	botanické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
meloun	meloun	k1gInSc1	meloun
bobule	bobule	k1gFnSc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
melounů	meloun	k1gInPc2	meloun
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Melouny	meloun	k1gInPc1	meloun
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
(	(	kIx(	(
<g/>
botanicky	botanicky	k6eAd1	botanicky
lubenice	lubenice	k1gFnSc1	lubenice
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
Citrullus	Citrullus	k1gMnSc1	Citrullus
lanatus	lanatus	k1gMnSc1	lanatus
<g/>
)	)	kIx)	)
a	a	k8xC	a
cukrový	cukrový	k2eAgInSc1d1	cukrový
meloun	meloun	k1gInSc1	meloun
(	(	kIx(	(
<g/>
botanicky	botanicky	k6eAd1	botanicky
meloun	meloun	k1gInSc1	meloun
cukrový	cukrový	k2eAgInSc1d1	cukrový
<g/>
,	,	kIx,	,
Cucumis	Cucumis	k1gInSc1	Cucumis
melo	melo	k?	melo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meloun	meloun	k1gInSc1	meloun
cukrový	cukrový	k2eAgMnSc1d1	cukrový
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Cucumis	Cucumis	k1gFnSc2	Cucumis
společně	společně	k6eAd1	společně
s	s	k7c7	s
okurkou	okurka	k1gFnSc7	okurka
setou	setý	k2eAgFnSc7d1	setá
(	(	kIx(	(
<g/>
Cucumis	Cucumis	k1gFnPc2	Cucumis
sativus	sativus	k1gInSc1	sativus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meloun	meloun	k1gInSc1	meloun
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
povrchové	povrchový	k2eAgFnSc2d1	povrchová
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
měkkou	měkký	k2eAgFnSc4d1	měkká
vodnatou	vodnatý	k2eAgFnSc4d1	vodnatá
dužninu	dužnina	k1gFnSc4	dužnina
uvnitř	uvnitř	k7c2	uvnitř
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
melounů	meloun	k1gInPc2	meloun
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tělesa	těleso	k1gNnSc2	těleso
tvořená	tvořený	k2eAgFnSc1d1	tvořená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
90	[number]	k4	90
až	až	k9	až
93	[number]	k4	93
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
o	o	k7c4	o
nízkokalorickou	nízkokalorický	k2eAgFnSc4d1	nízkokalorická
potravinu	potravina	k1gFnSc4	potravina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
30	[number]	k4	30
kcal	kcala	k1gFnPc2	kcala
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
g	g	kA	g
a	a	k8xC	a
vydatné	vydatný	k2eAgInPc4d1	vydatný
zdroje	zdroj	k1gInPc4	zdroj
kyseliny	kyselina	k1gFnSc2	kyselina
listové	listový	k2eAgFnSc2d1	listová
<g/>
,	,	kIx,	,
jablečné	jablečný	k2eAgFnSc2d1	jablečná
a	a	k8xC	a
citronové	citronový	k2eAgFnSc2d1	citronová
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
značné	značný	k2eAgFnPc4d1	značná
minerální	minerální	k2eAgFnPc4d1	minerální
složky	složka	k1gFnPc4	složka
jako	jako	k8xS	jako
například	například	k6eAd1	například
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
či	či	k8xC	či
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
meloun	meloun	k1gInSc1	meloun
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
červenou	červený	k2eAgFnSc7d1	červená
dužninou	dužnina	k1gFnSc7	dužnina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
semínek	semínko	k1gNnPc2	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Melouny	meloun	k1gInPc1	meloun
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
teplejším	teplý	k2eAgNnSc6d2	teplejší
klimatu	klima	k1gNnSc6	klima
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
je	být	k5eAaImIp3nS	být
náchylná	náchylný	k2eAgFnSc1d1	náchylná
rostlina	rostlina	k1gFnSc1	rostlina
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
klíčení	klíčení	k1gNnSc2	klíčení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyklíčení	vyklíčení	k1gNnSc6	vyklíčení
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
malá	malý	k2eAgFnSc1d1	malá
rostlinka	rostlinka	k1gFnSc1	rostlinka
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
meloun	meloun	k1gInSc1	meloun
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
květu	květ	k1gInSc2	květ
po	po	k7c6	po
opylení	opylení	k1gNnSc6	opylení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mateřské	mateřský	k2eAgFnSc3d1	mateřská
rostlině	rostlina	k1gFnSc3	rostlina
je	být	k5eAaImIp3nS	být
přichycen	přichycen	k2eAgMnSc1d1	přichycen
silným	silný	k2eAgInSc7d1	silný
stonkem	stonek	k1gInSc7	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Melouny	meloun	k1gInPc1	meloun
lze	lze	k6eAd1	lze
úspěšně	úspěšně	k6eAd1	úspěšně
pěstovat	pěstovat	k5eAaImF	pěstovat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
převážně	převážně	k6eAd1	převážně
vinařských	vinařský	k2eAgFnPc2d1	vinařská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInPc1d1	vodní
melouny	meloun	k1gInPc1	meloun
jsou	být	k5eAaImIp3nP	být
náročné	náročný	k2eAgInPc1d1	náročný
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
kolísání	kolísání	k1gNnSc1	kolísání
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
přílišné	přílišný	k2eAgFnPc1d1	přílišná
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
srážky	srážka	k1gFnPc1	srážka
a	a	k8xC	a
silné	silný	k2eAgInPc1d1	silný
větry	vítr	k1gInPc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
pěstování	pěstování	k1gNnSc4	pěstování
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
převážně	převážně	k6eAd1	převážně
humózní	humózní	k2eAgFnPc1d1	humózní
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zásobené	zásobený	k2eAgFnPc1d1	zásobená
živinami	živina	k1gFnPc7	živina
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
cukrový	cukrový	k2eAgInSc1d1	cukrový
meloun	meloun	k1gInSc1	meloun
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pěstovat	pěstovat	k5eAaImF	pěstovat
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
v	v	k7c6	v
kukuřičných	kukuřičný	k2eAgFnPc6d1	kukuřičná
oblastech	oblast	k1gFnPc6	oblast
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgInP	chránit
před	před	k7c7	před
větry	vítr	k1gInPc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc1	pěstování
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
v	v	k7c6	v
umělých	umělý	k2eAgInPc6d1	umělý
sklenících	skleník	k1gInPc6	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
jako	jako	k9	jako
rarita	rarita	k1gFnSc1	rarita
vypěstuje	vypěstovat	k5eAaPmIp3nS	vypěstovat
hranatý	hranatý	k2eAgInSc1d1	hranatý
meloun	meloun	k1gInSc1	meloun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
hranaté	hranatý	k2eAgFnSc6d1	hranatá
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
propůjčí	propůjčit	k5eAaPmIp3nS	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
melouny	meloun	k1gInPc1	meloun
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
poměrně	poměrně	k6eAd1	poměrně
drahé	drahý	k2eAgNnSc1d1	drahé
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
estetickou	estetický	k2eAgFnSc4d1	estetická
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
melouny	meloun	k1gInPc1	meloun
pěstovány	pěstován	k2eAgInPc1d1	pěstován
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
příhodných	příhodný	k2eAgFnPc6d1	příhodná
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
mezi	mezi	k7c4	mezi
výrazné	výrazný	k2eAgMnPc4d1	výrazný
producenty	producent	k1gMnPc4	producent
patří	patřit	k5eAaImIp3nS	patřit
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
a	a	k8xC	a
také	také	k9	také
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
ale	ale	k8xC	ale
neleží	ležet	k5eNaImIp3nS	ležet
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
rostl	růst	k5eAaImAgInS	růst
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
subtropické	subtropický	k2eAgFnSc2d1	subtropická
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1	veliká
variabilita	variabilita	k1gFnSc1	variabilita
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
však	však	k9	však
vedla	vést	k5eAaImAgFnS	vést
dlouho	dlouho	k6eAd1	dlouho
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
krajinou	krajina	k1gFnSc7	krajina
původu	původ	k1gInSc2	původ
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
plně	plně	k6eAd1	plně
podporována	podporovat	k5eAaImNgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
přímé	přímý	k2eAgNnSc4d1	přímé
archeologické	archeologický	k2eAgNnSc4d1	Archeologické
doložení	doložení	k1gNnSc4	doložení
ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
kulturách	kultura	k1gFnPc6	kultura
je	být	k5eAaImIp3nS	být
výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ze	z	k7c2	z
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
ojedinělého	ojedinělý	k2eAgInSc2d1	ojedinělý
nálezu	nález	k1gInSc2	nález
v	v	k7c6	v
rakvi	rakev	k1gFnSc6	rakev
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
Středomoří	středomoří	k1gNnSc2	středomoří
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
postupovalo	postupovat	k5eAaImAgNnS	postupovat
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
jako	jako	k9	jako
postní	postní	k2eAgNnSc4d1	postní
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
právě	právě	k9	právě
společně	společně	k6eAd1	společně
s	s	k7c7	s
misionáři	misionář	k1gMnPc7	misionář
dostává	dostávat	k5eAaImIp3nS	dostávat
z	z	k7c2	z
Arménie	Arménie	k1gFnSc2	Arménie
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
pěstování	pěstování	k1gNnSc1	pěstování
bylo	být	k5eAaImAgNnS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
v	v	k7c6	v
papežském	papežský	k2eAgInSc6d1	papežský
hradě	hrad	k1gInSc6	hrad
Cantalupe	Cantalup	k1gInSc5	Cantalup
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dalo	dát	k5eAaPmAgNnS	dát
i	i	k9	i
těmto	tento	k3xDgNnPc3	tento
plodům	plod	k1gInPc3	plod
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc4	název
kantalupy	kantalup	k1gInPc1	kantalup
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
byly	být	k5eAaImAgInP	být
cukrové	cukrový	k2eAgInPc1d1	cukrový
melouny	meloun	k1gInPc1	meloun
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
již	již	k6eAd1	již
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Pěstování	pěstování	k1gNnSc2	pěstování
melounů	meloun	k1gInPc2	meloun
má	mít	k5eAaImIp3nS	mít
mnohatisíciletou	mnohatisíciletý	k2eAgFnSc4d1	mnohatisíciletý
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
již	již	k6eAd1	již
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
čtyřmi	čtyři	k4xCgInPc7	čtyři
tisíci	tisíc	k4xCgInPc7	tisíc
lety	let	k1gInPc7	let
Peršany	Peršan	k1gMnPc4	Peršan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
přes	přes	k7c4	přes
východní	východní	k2eAgFnSc4d1	východní
Indii	Indie	k1gFnSc4	Indie
melouny	meloun	k1gInPc4	meloun
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgInPc4d1	dobový
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
pěstování	pěstování	k1gNnSc4	pěstování
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
i	i	k9	i
na	na	k7c6	na
nástěnných	nástěnný	k2eAgFnPc6d1	nástěnná
malbách	malba	k1gFnPc6	malba
v	v	k7c6	v
pohřbeném	pohřbený	k2eAgNnSc6d1	pohřbené
městě	město	k1gNnSc6	město
Pompeje	Pompeje	k1gInPc1	Pompeje
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zasypáno	zasypat	k5eAaPmNgNnS	zasypat
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
erupcí	erupce	k1gFnSc7	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgNnPc1d1	následné
válečná	válečný	k2eAgNnPc1d1	válečné
tažení	tažení	k1gNnPc1	tažení
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc1d1	obchodní
cesty	cesta	k1gFnPc1	cesta
rozšiřovaly	rozšiřovat	k5eAaImAgFnP	rozšiřovat
pěstování	pěstování	k1gNnSc4	pěstování
melounů	meloun	k1gInPc2	meloun
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
cukrové	cukrový	k2eAgInPc1d1	cukrový
melouny	meloun	k1gInPc1	meloun
roku	rok	k1gInSc2	rok
1495	[number]	k4	1495
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
druhy	druh	k1gInPc1	druh
melounů	meloun	k1gInPc2	meloun
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
(	(	kIx(	(
<g/>
lubenice	lubenice	k1gFnSc1	lubenice
obecná	obecná	k1gFnSc1	obecná
<g/>
)	)	kIx)	)
a	a	k8xC	a
meloun	meloun	k1gInSc1	meloun
cukrový	cukrový	k2eAgInSc1d1	cukrový
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
(	(	kIx(	(
<g/>
správně	správně	k6eAd1	správně
nazýván	nazývat	k5eAaImNgInS	nazývat
lubenice	lubenice	k1gFnSc1	lubenice
obecná	obecný	k2eAgFnSc1d1	obecná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
načervenalou	načervenalý	k2eAgFnSc7d1	načervenalá
vodnatou	vodnatý	k2eAgFnSc7d1	vodnatá
dužninou	dužnina	k1gFnSc7	dužnina
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
meloun	meloun	k1gInSc1	meloun
zralý	zralý	k2eAgInSc1d1	zralý
<g/>
)	)	kIx)	)
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc7d1	zelená
kůrkou	kůrka	k1gFnSc7	kůrka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřku	vnitřek	k1gInSc6	vnitřek
dužniny	dužnina	k1gFnSc2	dužnina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozmístěná	rozmístěný	k2eAgNnPc4d1	rozmístěné
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
meloun	meloun	k1gInSc1	meloun
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
91	[number]	k4	91
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vítaným	vítaný	k2eAgInSc7d1	vítaný
zavodňujícím	zavodňující	k2eAgInSc7d1	zavodňující
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
hojně	hojně	k6eAd1	hojně
pěstován	pěstován	k2eAgInSc1d1	pěstován
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
váhy	váha	k1gFnPc4	váha
okolo	okolo	k7c2	okolo
15	[number]	k4	15
kg	kg	kA	kg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
plody	plod	k1gInPc1	plod
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
okolo	okolo	k7c2	okolo
2	[number]	k4	2
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc1d1	známý
meloun	meloun	k1gInSc1	meloun
vážil	vážit	k5eAaImAgInS	vážit
122	[number]	k4	122
kg	kg	kA	kg
<g/>
,	,	kIx,	,
melouny	meloun	k1gInPc1	meloun
okolo	okolo	k7c2	okolo
100	[number]	k4	100
kg	kg	kA	kg
nejsou	být	k5eNaImIp3nP	být
výjimkou	výjimka	k1gFnSc7	výjimka
Vodní	vodní	k2eAgInPc1d1	vodní
melouny	meloun	k1gInPc1	meloun
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
vyjma	vyjma	k7c2	vyjma
vody	voda	k1gFnSc2	voda
dále	daleko	k6eAd2	daleko
cukrem	cukr	k1gInSc7	cukr
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
fruktózy	fruktóza	k1gFnSc2	fruktóza
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgInPc4d1	bohatý
zdroje	zdroj	k1gInPc4	zdroj
vitamínu	vitamín	k1gInSc2	vitamín
C.	C.	kA	C.
Povrch	povrch	k7c2wR	povrch
vodního	vodní	k2eAgInSc2d1	vodní
melounu	meloun	k1gInSc2	meloun
má	mít	k5eAaImIp3nS	mít
typicky	typicky	k6eAd1	typicky
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
se	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
a	a	k8xC	a
tmavými	tmavý	k2eAgInPc7d1	tmavý
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
odrůdě	odrůda	k1gFnSc6	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
sytě	sytě	k6eAd1	sytě
červená	červený	k2eAgFnSc1d1	červená
až	až	k8xS	až
rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zakoupit	zakoupit	k5eAaPmF	zakoupit
odrůdy	odrůda	k1gFnPc4	odrůda
i	i	k9	i
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
vnitřkem	vnitřek	k1gInSc7	vnitřek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Meloun	meloun	k1gInSc1	meloun
cukrový	cukrový	k2eAgInSc1d1	cukrový
<g/>
.	.	kIx.	.
</s>
<s>
Zvaný	zvaný	k2eAgMnSc1d1	zvaný
též	též	k9	též
honeydew	honeydew	k?	honeydew
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
melounu	meloun	k1gInSc2	meloun
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
žlutou	žlutý	k2eAgFnSc7d1	žlutá
dužinou	dužina	k1gFnSc7	dužina
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc7d1	zelená
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
konzumován	konzumovat	k5eAaBmNgInS	konzumovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
parmskou	parmský	k2eAgFnSc7d1	parmská
šunkou	šunka	k1gFnSc7	šunka
<g/>
,	,	kIx,	,
lososem	losos	k1gMnSc7	losos
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	s	k7c7	s
sladkým	sladký	k2eAgNnSc7d1	sladké
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
melounu	meloun	k1gInSc2	meloun
vodního	vodní	k2eAgInSc2d1	vodní
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jak	jak	k6eAd1	jak
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
barvou	barva	k1gFnSc7	barva
tak	tak	k8xS	tak
i	i	k9	i
vzhledem	vzhled	k1gInSc7	vzhled
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
zakulatěnější	zakulatěný	k2eAgInPc4d2	zakulatěný
listy	list	k1gInPc4	list
a	a	k8xC	a
menší	malý	k2eAgInPc4d2	menší
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
průměrně	průměrně	k6eAd1	průměrně
6-8	[number]	k4	6-8
%	%	kIx~	%
cukru	cukr	k1gInSc2	cukr
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
sacharózy	sacharóza	k1gFnPc4	sacharóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoletou	jednoletý	k2eAgFnSc4d1	jednoletá
poléhavou	poléhavý	k2eAgFnSc4d1	poléhavá
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
150	[number]	k4	150
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
teplomilnou	teplomilný	k2eAgFnSc4d1	teplomilná
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zničí	zničit	k5eAaPmIp3nS	zničit
již	již	k6eAd1	již
první	první	k4xOgInPc1	první
mrazy	mráz	k1gInPc1	mráz
a	a	k8xC	a
pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
pod	pod	k7c4	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
rostlina	rostlina	k1gFnSc1	rostlina
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
asimilaci	asimilace	k1gFnSc4	asimilace
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
květů	květ	k1gInPc2	květ
–	–	k?	–
samčí	samčí	k2eAgFnSc7d1	samčí
a	a	k8xC	a
samičí	samičí	k2eAgFnSc4d1	samičí
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odkvetení	odkvetení	k1gNnSc6	odkvetení
vzniká	vznikat	k5eAaImIp3nS	vznikat
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
až	až	k6eAd1	až
1	[number]	k4	1
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
plodu	plod	k1gInSc2	plod
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
korkovitě	korkovitě	k6eAd1	korkovitě
síťovaný	síťovaný	k2eAgInSc1d1	síťovaný
vzor	vzor	k1gInSc1	vzor
s	s	k7c7	s
hrbolky	hrbolek	k1gInPc7	hrbolek
připomínající	připomínající	k2eAgFnSc2d1	připomínající
bradavice	bradavice	k1gFnSc2	bradavice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
zcela	zcela	k6eAd1	zcela
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
.	.	kIx.	.
</s>
<s>
Ogen	Ogen	k1gNnSc1	Ogen
<g/>
:	:	kIx,	:
Malý	malý	k2eAgInSc1d1	malý
kulatý	kulatý	k2eAgInSc1d1	kulatý
hybrid	hybrid	k1gInSc1	hybrid
<g/>
,	,	kIx,	,
vyšlechtěný	vyšlechtěný	k2eAgInSc1d1	vyšlechtěný
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Charentais	Charentais	k1gFnSc1	Charentais
<g/>
:	:	kIx,	:
Meloun	meloun	k1gInSc1	meloun
s	s	k7c7	s
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
sladkou	sladký	k2eAgFnSc7d1	sladká
a	a	k8xC	a
vonnou	vonný	k2eAgFnSc7d1	vonná
dužinou	dužina	k1gFnSc7	dužina
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
slupky	slupka	k1gFnSc2	slupka
je	být	k5eAaImIp3nS	být
zelenobílá	zelenobílý	k2eAgFnSc1d1	zelenobílá
<g/>
.	.	kIx.	.
</s>
<s>
Kantalup	kantalup	k1gInSc1	kantalup
<g/>
:	:	kIx,	:
Meloun	meloun	k1gInSc1	meloun
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sladkou	sladký	k2eAgFnSc4d1	sladká
dužinu	dužina	k1gFnSc4	dužina
a	a	k8xC	a
voní	vonět	k5eAaImIp3nS	vonět
jako	jako	k9	jako
ananas	ananas	k1gInSc1	ananas
<g/>
.	.	kIx.	.
</s>
<s>
Slupka	slupka	k1gFnSc1	slupka
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
druhu	druh	k1gInSc2	druh
Gallia	Gallium	k1gNnSc2	Gallium
<g/>
.	.	kIx.	.
</s>
<s>
Honeydew	Honeydew	k?	Honeydew
<g/>
:	:	kIx,	:
Meloun	meloun	k1gInSc1	meloun
s	s	k7c7	s
jemnou	jemný	k2eAgFnSc7d1	jemná
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
světle	světle	k6eAd1	světle
zelenou	zelený	k2eAgFnSc4d1	zelená
dužinu	dužina	k1gFnSc4	dužina
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
slupky	slupka	k1gFnSc2	slupka
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Gallia	Gallia	k1gFnSc1	Gallia
<g/>
:	:	kIx,	:
Meloun	meloun	k1gInSc1	meloun
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nezaměnitelný	zaměnitelný	k2eNgMnSc1d1	nezaměnitelný
svojí	svůj	k3xOyFgFnSc7	svůj
slupkou	slupka	k1gFnSc7	slupka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
rozpraskaná	rozpraskaný	k2eAgFnSc1d1	rozpraskaná
jako	jako	k8xS	jako
kůra	kůra	k1gFnSc1	kůra
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
slupky	slupka	k1gFnSc2	slupka
se	se	k3xPyFc4	se
při	při	k7c6	při
zrání	zrání	k1gNnSc6	zrání
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
zelené	zelená	k1gFnSc2	zelená
do	do	k7c2	do
zlatožluté	zlatožlutý	k2eAgFnSc2d1	zlatožlutá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2011	[number]	k4	2011
se	se	k3xPyFc4	se
řezy	řez	k1gInPc4	řez
cukrového	cukrový	k2eAgInSc2d1	cukrový
melounu	meloun	k1gInSc2	meloun
staly	stát	k5eAaPmAgFnP	stát
původcem	původce	k1gMnSc7	původce
listeriózy	listerióza	k1gFnSc2	listerióza
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
k	k	k7c3	k
říjnu	říjen	k1gInSc3	říjen
2011	[number]	k4	2011
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
dvacet	dvacet	k4xCc4	dvacet
tři	tři	k4xCgFnPc4	tři
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Melouny	meloun	k1gInPc1	meloun
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
močopudné	močopudný	k2eAgFnPc1d1	močopudná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
ledvinových	ledvinový	k2eAgFnPc2d1	ledvinová
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
časté	častý	k2eAgNnSc1d1	časté
vylučování	vylučování	k1gNnSc1	vylučování
moči	moč	k1gFnSc2	moč
jako	jako	k8xS	jako
například	například	k6eAd1	například
u	u	k7c2	u
ledvinových	ledvinový	k2eAgInPc2d1	ledvinový
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
vysoký	vysoký	k2eAgInSc4d1	vysoký
podíl	podíl	k1gInSc4	podíl
vitamínu	vitamín	k1gInSc2	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
vitamíny	vitamín	k1gInPc4	vitamín
řady	řada	k1gFnSc2	řada
B	B	kA	B
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
PP	PP	kA	PP
či	či	k8xC	či
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
minerální	minerální	k2eAgInPc4d1	minerální
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
studie	studie	k1gFnPc1	studie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
melouny	meloun	k1gInPc1	meloun
mají	mít	k5eAaImIp3nP	mít
blahodárný	blahodárný	k2eAgInSc4d1	blahodárný
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c6	na
stimulaci	stimulace	k1gFnSc6	stimulace
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
organismus	organismus	k1gInSc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jemná	jemný	k2eAgFnSc1d1	jemná
vláknina	vláknina	k1gFnSc1	vláknina
dužniny	dužnina	k1gFnSc2	dužnina
pak	pak	k6eAd1	pak
posiluje	posilovat	k5eAaImIp3nS	posilovat
stěny	stěna	k1gFnPc4	stěna
střev	střevo	k1gNnPc2	střevo
a	a	k8xC	a
současně	současně	k6eAd1	současně
podporují	podporovat	k5eAaImIp3nP	podporovat
slabě	slabě	k6eAd1	slabě
i	i	k9	i
vylučování	vylučování	k1gNnSc4	vylučování
stolice	stolice	k1gFnSc2	stolice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pozitivně	pozitivně	k6eAd1	pozitivně
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
zácpě	zácpa	k1gFnSc3	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
vody	voda	k1gFnSc2	voda
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
poměr	poměr	k1gInSc4	poměr
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
karotenoidů	karotenoid	k1gInPc2	karotenoid
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
pak	pak	k6eAd1	pak
působí	působit	k5eAaImIp3nS	působit
antioxidačně	antioxidačně	k6eAd1	antioxidačně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
antioxidanty	antioxidant	k1gInPc7	antioxidant
řadíme	řadit	k5eAaImIp1nP	řadit
z	z	k7c2	z
obsažených	obsažený	k2eAgFnPc2d1	obsažená
živin	živina	k1gFnPc2	živina
vitamíny	vitamín	k1gInPc4	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
karotenoid	karotenoid	k1gInSc1	karotenoid
lykopen	lykopen	k2eAgInSc1d1	lykopen
a	a	k8xC	a
tripeptid	tripeptid	k1gInSc1	tripeptid
glutathion	glutathion	k1gInSc1	glutathion
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
významně	významně	k6eAd1	významně
projevit	projevit	k5eAaPmF	projevit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
půda	půda	k1gFnSc1	půda
melounů	meloun	k1gInPc2	meloun
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
jód	jód	k1gInSc1	jód
<g/>
,	,	kIx,	,
selen	selen	k1gInSc1	selen
a	a	k8xC	a
hořčík	hořčík	k1gInSc1	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
konzumaci	konzumace	k1gFnSc6	konzumace
melounu	meloun	k1gInSc2	meloun
popíjet	popíjet	k5eAaImF	popíjet
vychlazené	vychlazený	k2eAgInPc4d1	vychlazený
nápoje	nápoj	k1gInPc4	nápoj
a	a	k8xC	a
kombinovat	kombinovat	k5eAaImF	kombinovat
jeho	jeho	k3xOp3gFnSc4	jeho
konzumaci	konzumace	k1gFnSc4	konzumace
s	s	k7c7	s
mléčnými	mléčný	k2eAgInPc7d1	mléčný
výrobky	výrobek	k1gInPc7	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Melouny	meloun	k1gInPc1	meloun
se	se	k3xPyFc4	se
sklízejí	sklízet	k5eAaImIp3nP	sklízet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
nebo	nebo	k8xC	nebo
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
;	;	kIx,	;
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
již	již	k6eAd1	již
zralý	zralý	k2eAgInSc1d1	zralý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pozná	poznat	k5eAaPmIp3nS	poznat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stopka	stopka	k1gFnSc1	stopka
u	u	k7c2	u
plodu	plod	k1gInSc2	plod
pomalu	pomalu	k6eAd1	pomalu
zasychá	zasychat	k5eAaImIp3nS	zasychat
a	a	k8xC	a
při	při	k7c6	při
poklepu	poklep	k1gInSc6	poklep
prsty	prst	k1gInPc4	prst
zní	znět	k5eAaImIp3nP	znět
lehce	lehko	k6eAd1	lehko
dutě	dutě	k6eAd1	dutě
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zní	znět	k5eAaImIp3nS	znět
silně	silně	k6eAd1	silně
dutě	dutě	k6eAd1	dutě
napovídá	napovídat	k5eAaBmIp3nS	napovídat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
meloun	meloun	k1gInSc1	meloun
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nejspíše	nejspíše	k9	nejspíše
přezrálý	přezrálý	k2eAgMnSc1d1	přezrálý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zralý	zralý	k2eAgInSc1d1	zralý
meloun	meloun	k1gInSc1	meloun
stiskne	stisknout	k5eAaPmIp3nS	stisknout
v	v	k7c6	v
dlaních	dlaň	k1gFnPc6	dlaň
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
vrzavý	vrzavý	k2eAgInSc4d1	vrzavý
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc4	plod
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
skladovat	skladovat	k5eAaImF	skladovat
v	v	k7c6	v
celku	celek	k1gInSc6	celek
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
dochází	docházet	k5eAaImIp3nS	docházet
neustále	neustále	k6eAd1	neustále
ke	k	k7c3	k
zrání	zrání	k1gNnSc3	zrání
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
změnu	změna	k1gFnSc4	změna
jeho	jeho	k3xOp3gFnPc2	jeho
chuťových	chuťový	k2eAgFnPc2d1	chuťová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
porušena	porušen	k2eAgFnSc1d1	porušena
jeho	jeho	k3xOp3gFnSc4	jeho
celistvost	celistvost	k1gFnSc4	celistvost
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
kažení	kažení	k1gNnSc3	kažení
dužniny	dužnina	k1gFnSc2	dužnina
<g/>
.	.	kIx.	.
</s>
<s>
Plísně	plíseň	k1gFnPc1	plíseň
ale	ale	k8xC	ale
časem	čas	k1gInSc7	čas
prorůstají	prorůstat	k5eAaImIp3nP	prorůstat
i	i	k9	i
do	do	k7c2	do
celistvého	celistvý	k2eAgInSc2d1	celistvý
melounu	meloun	k1gInSc2	meloun
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
z	z	k7c2	z
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
nežádoucí	žádoucí	k2eNgFnPc4d1	nežádoucí
chuťové	chuťový	k2eAgFnPc4d1	chuťová
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
meloun	meloun	k1gInSc4	meloun
již	již	k6eAd1	již
nekonzumujeme	konzumovat	k5eNaBmIp1nP	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Melouny	meloun	k1gInPc4	meloun
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
dobré	dobrý	k2eAgNnSc1d1	dobré
podkládat	podkládat	k5eAaImF	podkládat
na	na	k7c6	na
poli	pole	k1gFnSc6	pole
plastovou	plastový	k2eAgFnSc7d1	plastová
mřížkou	mřížka	k1gFnSc7	mřížka
nebo	nebo	k8xC	nebo
slámou	sláma	k1gFnSc7	sláma
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
podkládáme	podkládat	k5eAaImIp1nP	podkládat
jahody	jahoda	k1gFnPc1	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Meloun	meloun	k1gInSc1	meloun
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
jako	jako	k9	jako
dekorační	dekorační	k2eAgInSc1d1	dekorační
prvek	prvek	k1gInSc1	prvek
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
míchaných	míchaný	k2eAgInPc2d1	míchaný
nápojů	nápoj	k1gInPc2	nápoj
jak	jak	k8xS	jak
nealkoholických	alkoholický	k2eNgFnPc2d1	nealkoholická
tak	tak	k9	tak
i	i	k9	i
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
plátek	plátek	k1gInSc1	plátek
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
skleničky	sklenička	k1gFnSc2	sklenička
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
pokrmu	pokrm	k1gInSc2	pokrm
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
"	"	kIx"	"
<g/>
opilý	opilý	k2eAgInSc1d1	opilý
meloun	meloun	k1gInSc1	meloun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
melounu	meloun	k1gInSc2	meloun
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
díra	díra	k1gFnSc1	díra
a	a	k8xC	a
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
trychtýře	trychtýř	k1gInSc2	trychtýř
nalije	nalít	k5eAaBmIp3nS	nalít
alkohol	alkohol	k1gInSc1	alkohol
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
vodka	vodka	k1gFnSc1	vodka
nebo	nebo	k8xC	nebo
rum	rum	k1gInSc1	rum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
láhev	láhev	k1gFnSc1	láhev
zapíchne	zapíchnout	k5eAaPmIp3nS	zapíchnout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
injekční	injekční	k2eAgFnSc1d1	injekční
stříkačka	stříkačka	k1gFnSc1	stříkačka
<g/>
.	.	kIx.	.
</s>
<s>
Odloží	odložit	k5eAaPmIp3nS	odložit
se	se	k3xPyFc4	se
na	na	k7c4	na
den	den	k1gInSc4	den
do	do	k7c2	do
lednice	lednice	k1gFnSc2	lednice
a	a	k8xC	a
alkohol	alkohol	k1gInSc1	alkohol
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
vodnatou	vodnatý	k2eAgFnSc4d1	vodnatá
dužninu	dužnina	k1gFnSc4	dužnina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nemáš	mít	k5eNaImIp2nS	mít
<g/>
-li	i	k?	-li
na	na	k7c4	na
chléb	chléb	k1gInSc4	chléb
<g/>
,	,	kIx,	,
nekupuj	kupovat	k5eNaImRp2nS	kupovat
melouny	meloun	k1gInPc5	meloun
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
perské	perský	k2eAgNnSc4d1	perské
přísloví	přísloví	k1gNnSc4	přísloví
"	"	kIx"	"
<g/>
Kdokoliv	kdokoliv	k3yInSc1	kdokoliv
sní	snít	k5eAaImIp3nS	snít
meloun	meloun	k1gInSc1	meloun
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
osvěžením	osvěžení	k1gNnSc7	osvěžení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
perské	perský	k2eAgNnSc4d1	perské
přísloví	přísloví	k1gNnSc4	přísloví
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
musí	muset	k5eAaImIp3nS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
svých	svůj	k3xOyFgInPc2	svůj
činů	čin	k1gInPc2	čin
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Dva	dva	k4xCgInPc4	dva
melouny	meloun	k1gInPc4	meloun
si	se	k3xPyFc3	se
nedáš	dát	k5eNaPmIp2nS	dát
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
podpaždí	podpaždí	k1gNnSc2	podpaždí
<g/>
"	"	kIx"	"
–	–	k?	–
balkánské	balkánský	k2eAgNnSc4d1	balkánské
přísloví	přísloví	k1gNnSc4	přísloví
"	"	kIx"	"
<g/>
Do	do	k7c2	do
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
do	do	k7c2	do
melounů	meloun	k1gInPc2	meloun
nevidíš	vidět	k5eNaImIp2nS	vidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
italské	italský	k2eAgNnSc4d1	italské
přísloví	přísloví	k1gNnSc4	přísloví
"	"	kIx"	"
<g/>
Do	do	k7c2	do
melounů	meloun	k1gInPc2	meloun
a	a	k8xC	a
do	do	k7c2	do
žen	žena	k1gFnPc2	žena
nevidíš	vidět	k5eNaImIp2nS	vidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
arabské	arabský	k2eAgNnSc4d1	arabské
nebo	nebo	k8xC	nebo
italské	italský	k2eAgNnSc4d1	italské
přísloví	přísloví	k1gNnSc4	přísloví
</s>
