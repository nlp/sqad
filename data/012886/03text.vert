<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
I.	I.	kA	I.
Bílý	bílý	k1gMnSc1	bílý
nebo	nebo	k8xC	nebo
také	také	k9	také
Katolický	katolický	k2eAgMnSc1d1	katolický
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
I.	I.	kA	I.
(	(	kIx(	(
<g/>
Fehér	Fehér	k1gMnSc1	Fehér
<g/>
/	/	kIx~	/
<g/>
Katolikus	Katolikus	k1gMnSc1	Katolikus
<g/>
)	)	kIx)	)
András	András	k1gInSc1	András
<g/>
/	/	kIx~	/
<g/>
Endre	Endr	k1gInSc5	Endr
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1015	[number]	k4	1015
–	–	k?	–
před	před	k7c7	před
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1060	[number]	k4	1060
<g/>
,	,	kIx,	,
Zirc	Zirc	k1gInSc1	Zirc
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
větve	větev	k1gFnSc2	větev
Arpádovské	Arpádovský	k2eAgFnSc2d1	Arpádovský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
posílil	posílit	k5eAaPmAgInS	posílit
pozici	pozice	k1gFnSc4	pozice
křesťanství	křesťanství	k1gNnSc2	křesťanství
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
království	království	k1gNnSc6	království
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uchránit	uchránit	k5eAaPmF	uchránit
nezávislost	nezávislost	k1gFnSc4	nezávislost
uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
proti	proti	k7c3	proti
Svaté	svatý	k2eAgFnSc3d1	svatá
říši	říš	k1gFnSc3	říš
římské	římský	k2eAgFnSc3d1	římská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uherská	uherský	k2eAgFnSc1d1	uherská
kmenová	kmenový	k2eAgFnSc1d1	kmenová
společnost	společnost	k1gFnSc1	společnost
neuznávala	uznávat	k5eNaImAgFnS	uznávat
prvorozenství	prvorozenství	k1gNnSc4	prvorozenství
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
favorizovala	favorizovat	k5eAaImAgFnS	favorizovat
věk	věk	k1gInSc4	věk
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
pořadí	pořadí	k1gNnSc2	pořadí
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
udělalo	udělat	k5eAaPmAgNnS	udělat
z	z	k7c2	z
ostatních	ostatní	k2eAgMnPc2d1	ostatní
mužských	mužský	k2eAgMnPc2d1	mužský
členů	člen	k1gMnPc2	člen
arpádovské	arpádovský	k2eAgFnSc2d1	arpádovský
dynastie	dynastie	k1gFnSc2	dynastie
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
konkurenty	konkurent	k1gMnPc7	konkurent
pro	pro	k7c4	pro
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Ondřejova	Ondřejův	k2eAgFnSc1d1	Ondřejova
větev	větev	k1gFnSc1	větev
dynastie	dynastie	k1gFnSc2	dynastie
dlouho	dlouho	k6eAd1	dlouho
soupeřila	soupeřit	k5eAaImAgFnS	soupeřit
se	s	k7c7	s
starší	starý	k2eAgFnSc7d2	starší
větví	větev	k1gFnSc7	větev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
patřili	patřit	k5eAaImAgMnP	patřit
Štěpán	Štěpán	k1gMnSc1	Štěpán
I.	I.	kA	I.
Uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předešlém	předešlý	k2eAgNnSc6d1	předešlé
půlstoletí	půlstoletí	k1gNnSc6	půlstoletí
se	se	k3xPyFc4	se
rodinná	rodinný	k2eAgFnSc1d1	rodinná
rivalita	rivalita	k1gFnSc1	rivalita
soustředila	soustředit	k5eAaPmAgFnS	soustředit
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
pohanstvím	pohanství	k1gNnSc7	pohanství
a	a	k8xC	a
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
,	,	kIx,	,
představovaným	představovaný	k2eAgNnSc7d1	představované
mladší	mladý	k2eAgFnSc7d2	mladší
a	a	k8xC	a
starší	starý	k2eAgFnSc7d2	starší
větví	větev	k1gFnSc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
větev	větev	k1gFnSc1	větev
vymřela	vymřít	k5eAaPmAgFnS	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1038	[number]	k4	1038
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
nové	nový	k2eAgFnPc4d1	nová
příležitosti	příležitost	k1gFnPc4	příležitost
pro	pro	k7c4	pro
mladší	mladý	k2eAgFnSc4d2	mladší
přeživší	přeživší	k2eAgFnSc4d1	přeživší
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgMnSc7	druhý
synem	syn	k1gMnSc7	syn
vévody	vévoda	k1gMnSc2	vévoda
Vazula	Vazul	k1gMnSc2	Vazul
<g/>
,	,	kIx,	,
bratrance	bratranec	k1gMnSc2	bratranec
Štěpána	Štěpán	k1gMnSc2	Štěpán
I.	I.	kA	I.
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc4	první
krále	král	k1gMnSc4	král
Uher	uher	k1gInSc1	uher
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
konkubína	konkubína	k1gFnSc1	konkubína
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
člena	člen	k1gMnSc2	člen
uherského	uherský	k2eAgInSc2d1	uherský
rodu	rod	k1gInSc2	rod
Tátonyů	Tátony	k1gInPc2	Tátony
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
přeživší	přeživší	k2eAgMnSc1d1	přeživší
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
Emeric	Emeric	k1gMnSc1	Emeric
byl	být	k5eAaImAgMnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1031	[number]	k4	1031
zabit	zabit	k2eAgMnSc1d1	zabit
divočákem	divočák	k1gMnSc7	divočák
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
chtěl	chtít	k5eAaImAgMnS	chtít
pojistit	pojistit	k5eAaPmF	pojistit
pozici	pozice	k1gFnSc4	pozice
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
polorozvráceném	polorozvrácený	k2eAgNnSc6d1	polorozvrácený
království	království	k1gNnSc6	království
<g/>
;	;	kIx,	;
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
plánoval	plánovat	k5eAaImAgMnS	plánovat
jmenovat	jmenovat	k5eAaBmF	jmenovat
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
Petra	Petr	k1gMnSc4	Petr
Orseola	Orseola	k1gFnSc1	Orseola
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
benátského	benátský	k2eAgMnSc4d1	benátský
dóžete	dóže	k1gMnSc4	dóže
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
Vazul	Vazul	k1gInSc1	Vazul
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ctitele	ctitel	k1gMnPc4	ctitel
pohanských	pohanský	k2eAgInPc2d1	pohanský
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
konspirace	konspirace	k1gFnPc4	konspirace
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabít	zabít	k5eAaPmF	zabít
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Vazul	Vazul	k1gInSc1	Vazul
byl	být	k5eAaImAgInS	být
oslepen	oslepit	k5eAaPmNgInS	oslepit
<g/>
,	,	kIx,	,
do	do	k7c2	do
uší	ucho	k1gNnPc2	ucho
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nalito	nalít	k5eAaBmNgNnS	nalít
roztavené	roztavený	k2eAgNnSc1d1	roztavené
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc1	tři
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
prchli	prchnout	k5eAaPmAgMnP	prchnout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Béla	Béla	k1gMnSc1	Béla
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ryksou	Ryksa	k1gFnSc7	Ryksa
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
a	a	k8xC	a
Levente	Levent	k1gInSc5	Levent
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
cítili	cítit	k5eAaImAgMnP	cítit
zastíněni	zastíněn	k2eAgMnPc1d1	zastíněn
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
usídlili	usídlit	k5eAaPmAgMnP	usídlit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Preláti	prelát	k1gMnPc1	prelát
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc2	vedení
biskupem	biskup	k1gMnSc7	biskup
Gerardem	Gerard	k1gMnSc7	Gerard
ze	z	k7c2	z
Csanádu	Csanáda	k1gFnSc4	Csanáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
povolat	povolat	k5eAaPmF	povolat
Vazulovy	Vazulův	k2eAgMnPc4d1	Vazulův
syny	syn	k1gMnPc4	syn
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
napsali	napsat	k5eAaPmAgMnP	napsat
jim	on	k3xPp3gMnPc3	on
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Ondřej	Ondřej	k1gMnSc1	Ondřej
a	a	k8xC	a
Levante	Levant	k1gMnSc5	Levant
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
se	se	k3xPyFc4	se
obrovské	obrovský	k2eAgNnSc4d1	obrovské
bogomilské	bogomilský	k2eAgNnSc4d1	Bogomilské
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
v	v	k7c6	v
Abaújváru	Abaújvár	k1gInSc6	Abaújvár
alianci	aliance	k1gFnSc3	aliance
s	s	k7c7	s
pohanskými	pohanský	k2eAgMnPc7d1	pohanský
rebely	rebel	k1gMnPc7	rebel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
jako	jako	k8xC	jako
své	svůj	k3xOyFgMnPc4	svůj
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Orseolo	Orseola	k1gFnSc5	Orseola
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
uprchnout	uprchnout	k5eAaPmF	uprchnout
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
oslepen	oslepen	k2eAgMnSc1d1	oslepen
a	a	k8xC	a
vykastrován	vykastrován	k2eAgMnSc1d1	vykastrován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
===	===	k?	===
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
byl	být	k5eAaImAgMnS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
roku	rok	k1gInSc2	rok
1047	[number]	k4	1047
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
christianizace	christianizace	k1gFnSc2	christianizace
<g/>
,	,	kIx,	,
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
dekrety	dekret	k1gInPc7	dekret
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
a	a	k8xC	a
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
cizí	cizit	k5eAaImIp3nP	cizit
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bogomilové	bogomil	k1gMnPc1	bogomil
zabili	zabít	k5eAaPmAgMnP	zabít
několik	několik	k4yIc4	několik
členů	člen	k1gMnPc2	člen
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Svatou	svatý	k2eAgFnSc7d1	svatá
říší	říš	k1gFnSc7	říš
římskou	římska	k1gFnSc7	římska
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
napjaté	napjatý	k2eAgFnPc1d1	napjatá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Petr	Petr	k1gMnSc1	Petr
Orseolo	Orseola	k1gFnSc5	Orseola
byl	být	k5eAaImAgMnS	být
nejenom	nejenom	k6eAd1	nejenom
blízkým	blízký	k2eAgMnSc7d1	blízký
spojencem	spojenec	k1gMnSc7	spojenec
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vazalem	vazal	k1gMnSc7	vazal
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
císařskému	císařský	k2eAgInSc3d1	císařský
dvoru	dvůr	k1gInSc3	dvůr
diplomaty	diplomat	k1gMnPc4	diplomat
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
akceptovat	akceptovat	k5eAaBmF	akceptovat
císařovu	císařův	k2eAgFnSc4d1	císařova
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
císař	císař	k1gMnSc1	císař
smír	smír	k1gInSc4	smír
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Ondřej	Ondřej	k1gMnSc1	Ondřej
musel	muset	k5eAaImAgMnS	muset
začít	začít	k5eAaPmF	začít
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
hrozící	hrozící	k2eAgFnSc4d1	hrozící
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1048	[number]	k4	1048
pozval	pozvat	k5eAaPmAgMnS	pozvat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
dvoru	dvůr	k1gInSc3	dvůr
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Bélu	Béla	k1gMnSc4	Béla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bratrovi	bratr	k1gMnSc3	bratr
daroval	darovat	k5eAaPmAgMnS	darovat
knížecí	knížecí	k2eAgInSc4d1	knížecí
úděl	úděl	k1gInSc4	úděl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
Nitranska	Nitransko	k1gNnSc2	Nitransko
a	a	k8xC	a
Biharska	Biharsko	k1gNnSc2	Biharsko
<g/>
.	.	kIx.	.
<g/>
Zároveň	zároveň	k6eAd1	zároveň
určil	určit	k5eAaPmAgInS	určit
Bélu	Béla	k1gMnSc4	Béla
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
stále	stále	k6eAd1	stále
nedala	dát	k5eNaPmAgFnS	dát
dědice	dědic	k1gMnPc4	dědic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1051	[number]	k4	1051
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Uhry	Uhry	k1gFnPc4	Uhry
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
císařské	císařský	k2eAgNnSc1d1	císařské
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
poraženo	porazit	k5eAaPmNgNnS	porazit
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
Vértes	Vértesa	k1gFnPc2	Vértesa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
císařská	císařský	k2eAgFnSc1d1	císařská
flotila	flotila	k1gFnSc1	flotila
byla	být	k5eAaImAgFnS	být
donucena	donutit	k5eAaPmNgFnS	donutit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zfalšovaného	zfalšovaný	k2eAgInSc2d1	zfalšovaný
listu	list	k1gInSc2	list
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
jednal	jednat	k5eAaImAgMnS	jednat
mezi	mezi	k7c7	mezi
vládci	vládce	k1gMnPc7	vládce
opat	opat	k1gMnSc1	opat
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Cluny	Cluna	k1gFnSc2	Cluna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
císař	císař	k1gMnSc1	císař
opět	opět	k6eAd1	opět
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
mír	mír	k1gInSc4	mír
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
vedl	vést	k5eAaImAgMnS	vést
císař	císař	k1gMnSc1	císař
svoji	svůj	k3xOyFgFnSc4	svůj
flotilu	flotila	k1gFnSc4	flotila
proti	proti	k7c3	proti
pohraničnímu	pohraniční	k2eAgInSc3d1	pohraniční
hradu	hrad	k1gInSc3	hrad
v	v	k7c6	v
Prešpurku	Prešpurku	k?	Prešpurku
(	(	kIx(	(
<g/>
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
diverzní	diverzní	k2eAgFnSc3d1	diverzní
akci	akce	k1gFnSc3	akce
obránců	obránce	k1gMnPc2	obránce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
potopili	potopit	k5eAaPmAgMnP	potopit
císařské	císařský	k2eAgFnPc4d1	císařská
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Uhry	Uhry	k1gFnPc1	Uhry
říšskému	říšský	k2eAgInSc3d1	říšský
nájezdu	nájezd	k1gInSc3	nájezd
opět	opět	k6eAd1	opět
ubránily	ubránit	k5eAaPmAgFnP	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
IX	IX	kA	IX
<g/>
.	.	kIx.	.
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
císař	císař	k1gMnSc1	císař
opět	opět	k6eAd1	opět
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
císařských	císařský	k2eAgNnPc2d1	císařské
vojsk	vojsko	k1gNnPc2	vojsko
Ondřej	Ondřej	k1gMnSc1	Ondřej
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1053	[number]	k4	1053
alianci	aliance	k1gFnSc4	aliance
s	s	k7c7	s
bavorským	bavorský	k2eAgMnSc7d1	bavorský
vévodou	vévoda	k1gMnSc7	vévoda
Konrádem	Konrád	k1gMnSc7	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podporoval	podporovat	k5eAaImAgInS	podporovat
císařskou	císařský	k2eAgFnSc4d1	císařská
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1055	[number]	k4	1055
založil	založit	k5eAaPmAgMnS	založit
Ondřej	Ondřej	k1gMnSc1	Ondřej
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Balatonu	Balaton	k1gInSc2	Balaton
benediktýnské	benediktýnský	k2eAgNnSc4d1	benediktýnské
opatství	opatství	k1gNnSc4	opatství
Tihany	Tihana	k1gFnSc2	Tihana
a	a	k8xC	a
dosadil	dosadit	k5eAaPmAgInS	dosadit
tam	tam	k6eAd1	tam
konvent	konvent	k1gInSc1	konvent
ortodoxních	ortodoxní	k2eAgFnPc2d1	ortodoxní
jeptišek	jeptiška	k1gFnPc2	jeptiška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nástupnická	nástupnický	k2eAgFnSc1d1	nástupnická
krize	krize	k1gFnSc1	krize
===	===	k?	===
</s>
</p>
<p>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
roku	rok	k1gInSc2	rok
1053	[number]	k4	1053
se	se	k3xPyFc4	se
Ondřejovi	Ondřej	k1gMnSc3	Ondřej
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
manželství	manželství	k1gNnSc6	manželství
konečně	konečně	k6eAd1	konečně
narodil	narodit	k5eAaPmAgMnS	narodit
vytoužený	vytoužený	k2eAgMnSc1d1	vytoužený
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Anastázie	Anastázie	k1gFnSc1	Anastázie
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
kyjevského	kyjevský	k2eAgMnSc2d1	kyjevský
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1058	[number]	k4	1058
se	se	k3xPyFc4	se
Ondřej	Ondřej	k1gMnSc1	Ondřej
I.	I.	kA	I.
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zajistit	zajistit	k5eAaPmF	zajistit
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
své	svůj	k3xOyFgFnSc2	svůj
krve	krev	k1gFnSc2	krev
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechal	nechat	k5eAaPmAgMnS	nechat
ve	v	k7c6	v
Stoličném	stoličný	k1gMnSc6	stoličný
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
korunovat	korunovat	k5eAaBmF	korunovat
svého	svůj	k3xOyFgMnSc4	svůj
pětiletého	pětiletý	k2eAgMnSc4d1	pětiletý
syna	syn	k1gMnSc4	syn
Šalamouna	Šalamoun	k1gMnSc4	Šalamoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nečekaná	čekaný	k2eNgFnSc1d1	nečekaná
korunovace	korunovace	k1gFnSc1	korunovace
porušila	porušit	k5eAaPmAgFnS	porušit
předchozí	předchozí	k2eAgFnSc4d1	předchozí
domluvu	domluva	k1gFnSc4	domluva
a	a	k8xC	a
zkazila	zkazit	k5eAaPmAgFnS	zkazit
dosud	dosud	k6eAd1	dosud
dobré	dobrý	k2eAgInPc4d1	dobrý
bratrské	bratrský	k2eAgInPc4d1	bratrský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojený	spokojený	k2eNgMnSc1d1	nespokojený
Béla	Béla	k1gMnSc1	Béla
opustil	opustit	k5eAaPmAgMnS	opustit
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1058	[number]	k4	1058
se	se	k3xPyFc4	se
Ondřej	Ondřej	k1gMnSc1	Ondřej
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
již	již	k6eAd1	již
ochrnutý	ochrnutý	k2eAgMnSc1d1	ochrnutý
po	po	k7c6	po
mrtvici	mrtvice	k1gFnSc6	mrtvice
osobně	osobně	k6eAd1	osobně
setkal	setkat	k5eAaPmAgMnS	setkat
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
německým	německý	k2eAgMnSc7d1	německý
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
stvrzenou	stvrzený	k2eAgFnSc4d1	stvrzená
zásnubami	zásnuba	k1gFnPc7	zásnuba
Šalamouna	Šalamoun	k1gMnSc2	Šalamoun
a	a	k8xC	a
Jindřichovy	Jindřichův	k2eAgFnSc2d1	Jindřichova
sestry	sestra	k1gFnSc2	sestra
Judity	Judita	k1gFnSc2	Judita
Švábské	švábský	k2eAgFnSc2d1	Švábská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
míru	mír	k1gInSc2	mír
s	s	k7c7	s
říší	říš	k1gFnSc7	říš
se	se	k3xPyFc4	se
Ondřej	Ondřej	k1gMnSc1	Ondřej
snažil	snažit	k5eAaImAgMnS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Bélu	Béla	k1gMnSc4	Béla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
smířil	smířit	k5eAaPmAgInS	smířit
s	s	k7c7	s
Šalamounem	Šalamoun	k1gMnSc7	Šalamoun
na	na	k7c6	na
uherském	uherský	k2eAgInSc6d1	uherský
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Béla	Béla	k1gMnSc1	Béla
se	se	k3xPyFc4	se
nechtěl	chtít	k5eNaImAgMnS	chtít
vzdát	vzdát	k5eAaPmF	vzdát
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1059	[number]	k4	1059
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
hledat	hledat	k5eAaImF	hledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
Boleslava	Boleslav	k1gMnSc4	Boleslav
Smělého	Smělý	k1gMnSc4	Smělý
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
poslal	poslat	k5eAaPmAgMnS	poslat
rodinu	rodina	k1gFnSc4	rodina
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
se	se	k3xPyFc4	se
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
ani	ani	k8xC	ani
schopen	schopen	k2eAgMnSc1d1	schopen
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1060	[number]	k4	1060
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
Béla	Béla	k1gMnSc1	Béla
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
vojskem	vojsko	k1gNnSc7	vojsko
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc4	vojsko
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Tisy	Tisa	k1gFnSc2	Tisa
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
Béla	Béla	k1gMnSc1	Béla
i	i	k9	i
přes	přes	k7c4	přes
Ondřejovy	Ondřejův	k2eAgFnPc4d1	Ondřejova
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
míšeňským	míšeňský	k2eAgMnSc7d1	míšeňský
markrabětem	markrabě	k1gMnSc7	markrabě
Vilémem	Vilém	k1gMnSc7	Vilém
snažil	snažit	k5eAaImAgMnS	snažit
utéct	utéct	k5eAaPmF	utéct
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bélovi	Bélův	k2eAgMnPc1d1	Bélův
vojáci	voják	k1gMnPc1	voják
je	on	k3xPp3gMnPc4	on
chytili	chytit	k5eAaPmAgMnP	chytit
u	u	k7c2	u
Mošenského	Mošenský	k2eAgInSc2d1	Mošenský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ondřej	Ondřej	k1gMnSc1	Ondřej
v	v	k7c6	v
Thébském	thébský	k2eAgInSc6d1	thébský
průsmyku	průsmyk	k1gInSc6	průsmyk
spadl	spadnout	k5eAaPmAgInS	spadnout
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
Zircu	Zircus	k1gInSc2	Zircus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
opatství	opatství	k1gNnSc6	opatství
Tihany	Tihana	k1gFnSc2	Tihana
<g/>
.	.	kIx.	.
<g/>
Vévoda	vévoda	k1gMnSc1	vévoda
Béla	Béla	k1gMnSc1	Béla
byl	být	k5eAaImAgMnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1060	[number]	k4	1060
korunován	korunovat	k5eAaBmNgMnS	korunovat
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
a	a	k8xC	a
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
další	další	k2eAgInPc1d1	další
boje	boj	k1gInPc1	boj
o	o	k7c4	o
uherské	uherský	k2eAgNnSc4d1	Uherské
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Andrew	Andrew	k1gFnSc2	Andrew
I	i	k8xC	i
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KRISTÓ	KRISTÓ	kA	KRISTÓ
<g/>
,	,	kIx,	,
Gyula	Gyulo	k1gNnSc2	Gyulo
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Arpaden-Dynastie	Arpaden-Dynastie	k1gFnSc1	Arpaden-Dynastie
:	:	kIx,	:
die	die	k?	die
Geschichte	Geschicht	k1gInSc5	Geschicht
Ungarns	Ungarns	k1gInSc4	Ungarns
von	von	k1gInSc1	von
895	[number]	k4	895
bis	bis	k?	bis
1301	[number]	k4	1301
<g/>
.	.	kIx.	.
</s>
<s>
Budapest	Budapest	k1gFnSc1	Budapest
<g/>
:	:	kIx,	:
Corvina	Corvina	k1gFnSc1	Corvina
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
310	[number]	k4	310
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9631338576	[number]	k4	9631338576
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ondřej	Ondřej	k1gMnSc1	Ondřej
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Genealogie	genealogie	k1gFnSc1	genealogie
</s>
</p>
