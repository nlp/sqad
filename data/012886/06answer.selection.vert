<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
I.	I.	kA	I.
Bílý	bílý	k1gMnSc1	bílý
nebo	nebo	k8xC	nebo
také	také	k9	také
Katolický	katolický	k2eAgMnSc1d1	katolický
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
I.	I.	kA	I.
(	(	kIx(	(
<g/>
Fehér	Fehér	k1gMnSc1	Fehér
<g/>
/	/	kIx~	/
<g/>
Katolikus	Katolikus	k1gMnSc1	Katolikus
<g/>
)	)	kIx)	)
András	András	k1gInSc1	András
<g/>
/	/	kIx~	/
<g/>
Endre	Endr	k1gInSc5	Endr
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1015	[number]	k4	1015
–	–	k?	–
před	před	k7c7	před
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1060	[number]	k4	1060
<g/>
,	,	kIx,	,
Zirc	Zirc	k1gInSc1	Zirc
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
větve	větev	k1gFnSc2	větev
Arpádovské	Arpádovský	k2eAgFnSc2d1	Arpádovský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
