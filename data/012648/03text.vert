<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
představuje	představovat	k5eAaImIp3nS	představovat
správní	správní	k2eAgNnSc1d1	správní
středisko	středisko	k1gNnSc1	středisko
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
určitého	určitý	k2eAgNnSc2d1	určité
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
státu	stát	k1gInSc2	stát
nebo	nebo	k8xC	nebo
většího	veliký	k2eAgInSc2d2	veliký
správního	správní	k2eAgInSc2d1	správní
nebo	nebo	k8xC	nebo
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sídlo	sídlo	k1gNnSc4	sídlo
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
institucí	instituce	k1gFnPc2	instituce
daného	daný	k2eAgNnSc2d1	dané
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
významným	významný	k2eAgNnSc7d1	významné
obchodním	obchodní	k2eAgNnSc7d1	obchodní
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
či	či	k8xC	či
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Bern	Bern	k1gInSc4	Bern
a	a	k8xC	a
největší	veliký	k2eAgInSc4d3	veliký
Zürich	Zürich	k1gInSc4	Zürich
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
USA	USA	kA	USA
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Washington	Washington	k1gInSc1	Washington
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
bývá	bývat	k5eAaImIp3nS	bývat
určeno	určit	k5eAaPmNgNnS	určit
ústavou	ústava	k1gFnSc7	ústava
či	či	k8xC	či
jinou	jiný	k2eAgFnSc7d1	jiná
právní	právní	k2eAgFnSc7d1	právní
úpravou	úprava	k1gFnSc7	úprava
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnPc4d1	hlavní
města	město	k1gNnPc4	město
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
svůj	svůj	k3xOyFgInSc4	svůj
status	status	k1gInSc4	status
získala	získat	k5eAaPmAgFnS	získat
většinou	většina	k1gFnSc7	většina
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
či	či	k8xC	či
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
států	stát	k1gInPc2	stát
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Oceánie	Oceánie	k1gFnSc1	Oceánie
vzešla	vzejít	k5eAaPmAgFnS	vzejít
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
správních	správní	k2eAgFnPc2d1	správní
středisek	středisko	k1gNnPc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
uměle	uměle	k6eAd1	uměle
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
města	město	k1gNnSc2	město
zbudovaná	zbudovaný	k2eAgFnSc1d1	zbudovaná
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
<g/>
:	:	kIx,	:
Washington	Washington	k1gInSc1	Washington
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Canberra	Canberra	k1gFnSc1	Canberra
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brasília	Brasília	k1gFnSc1	Brasília
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Abuja	Abuja	k1gFnSc1	Abuja
(	(	kIx(	(
<g/>
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Monako	Monako	k1gNnSc4	Monako
či	či	k8xC	či
Vatikán	Vatikán	k1gInSc4	Vatikán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
více	hodně	k6eAd2	hodně
sídelních	sídelní	k2eAgNnPc2d1	sídelní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
rozlišena	rozlišen	k2eAgNnPc4d1	rozlišeno
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
ústavní	ústavní	k2eAgFnSc1d1	ústavní
instituce	instituce	k1gFnSc1	instituce
či	či	k8xC	či
ústřední	ústřední	k2eAgInSc1d1	ústřední
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
sídlí	sídlet	k5eAaImIp3nP	sídlet
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
sídlem	sídlo	k1gNnSc7	sídlo
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sídlem	sídlo	k1gNnSc7	sídlo
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
Haag	Haag	k1gInSc1	Haag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
jde	jít	k5eAaImIp3nS	jít
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
Pretoria	Pretorium	k1gNnSc2	Pretorium
<g/>
,	,	kIx,	,
parlamentu	parlament	k1gInSc2	parlament
Kapské	kapský	k2eAgNnSc1d1	Kapské
město	město	k1gNnSc1	město
a	a	k8xC	a
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Bloemfontein	Bloemfonteina	k1gFnPc2	Bloemfonteina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
13	[number]	k4	13
Ústavy	ústava	k1gFnSc2	ústava
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
za	za	k7c4	za
sídelní	sídelní	k2eAgNnSc4d1	sídelní
město	město	k1gNnSc4	město
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
i	i	k9	i
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
Veřejný	veřejný	k2eAgMnSc1d1	veřejný
ochránce	ochránce	k1gMnSc1	ochránce
práv	práv	k2eAgMnSc1d1	práv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
Energetický	energetický	k2eAgInSc1d1	energetický
regulační	regulační	k2eAgInSc1d1	regulační
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
suverénní	suverénní	k2eAgFnSc2d1	suverénní
země	zem	k1gFnSc2	zem
bez	bez	k7c2	bez
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
a	a	k8xC	a
Nauru	Naura	k1gFnSc4	Naura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
Historická	historický	k2eAgNnPc1d1	historické
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ruska	Rusko	k1gNnSc2	Rusko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hlavní	hlavní	k2eAgFnSc2d1	hlavní
město	město	k1gNnSc4	město
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
