<s>
Hlavní	hlavní	k2eAgNnPc4d1	hlavní
města	město	k1gNnPc4	město
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
svůj	svůj	k3xOyFgInSc4	svůj
status	status	k1gInSc4	status
získala	získat	k5eAaPmAgFnS	získat
většinou	většina	k1gFnSc7	většina
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
či	či	k8xC	či
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
