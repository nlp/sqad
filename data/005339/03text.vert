<s>
Mechorosty	mechorost	k1gInPc1	mechorost
jsou	být	k5eAaImIp3nP	být
primitivní	primitivní	k2eAgFnPc4d1	primitivní
vyšší	vysoký	k2eAgFnPc4d2	vyšší
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ještě	ještě	k9	ještě
nemají	mít	k5eNaImIp3nP	mít
vytvořená	vytvořený	k2eAgNnPc1d1	vytvořené
pravá	pravý	k2eAgNnPc1d1	pravé
vodivá	vodivý	k2eAgNnPc1d1	vodivé
pletiva	pletivo	k1gNnPc1	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
těla	tělo	k1gNnPc1	tělo
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
stélky	stélka	k1gFnSc2	stélka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
rozlišená	rozlišený	k2eAgFnSc1d1	rozlišená
na	na	k7c4	na
rhizoidy	rhizoida	k1gFnPc4	rhizoida
<g/>
,	,	kIx,	,
kauloid	kauloid	k1gInSc4	kauloid
a	a	k8xC	a
fyloidy	fyloid	k1gInPc4	fyloid
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
lupenitá	lupenitý	k2eAgFnSc1d1	lupenitá
(	(	kIx(	(
<g/>
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
formami	forma	k1gFnPc7	forma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
systematické	systematický	k2eAgNnSc4d1	systematické
zařazení	zařazení	k1gNnSc4	zařazení
a	a	k8xC	a
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
členění	členění	k1gNnSc4	členění
mechorostů	mechorost	k1gInPc2	mechorost
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
odlišných	odlišný	k2eAgInPc2d1	odlišný
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
jediné	jediný	k2eAgNnSc4d1	jediné
oddělení	oddělení	k1gNnSc4	oddělení
Bryophyta	Bryophyt	k1gInSc2	Bryophyt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
převažuje	převažovat	k5eAaImIp3nS	převažovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
nadoddělení	nadoddělení	k1gNnSc1	nadoddělení
<g/>
)	)	kIx)	)
mechorostů	mechorost	k1gInPc2	mechorost
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
oddělení	oddělení	k1gNnPc4	oddělení
<g/>
:	:	kIx,	:
hlevíky	hlevík	k1gInPc1	hlevík
<g/>
,	,	kIx,	,
játrovky	játrovka	k1gFnPc1	játrovka
a	a	k8xC	a
mechy	mech	k1gInPc1	mech
<g/>
.	.	kIx.	.
</s>
<s>
Mechorosty	mechorost	k1gInPc1	mechorost
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgFnPc4d1	zelená
rostliny	rostlina	k1gFnPc4	rostlina
nižšího	nízký	k2eAgInSc2d2	nižší
vzrůstu	vzrůst	k1gInSc2	vzrůst
(	(	kIx(	(
<g/>
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
novoguinejský	novoguinejský	k2eAgInSc1d1	novoguinejský
mech	mech	k1gInSc1	mech
druhu	druh	k1gInSc2	druh
Dawsonia	Dawsonium	k1gNnSc2	Dawsonium
superba	superba	k1gFnSc1	superba
<g/>
,	,	kIx,	,
dorůstající	dorůstající	k2eAgFnSc2d1	dorůstající
výšky	výška	k1gFnSc2	výška
až	až	k9	až
70	[number]	k4	70
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekologicky	ekologicky	k6eAd1	ekologicky
jsou	být	k5eAaImIp3nP	být
vázané	vázaný	k2eAgFnPc1d1	vázaná
zejména	zejména	k9	zejména
na	na	k7c4	na
stanoviště	stanoviště	k1gNnPc4	stanoviště
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc7d2	vyšší
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
vyloženě	vyloženě	k6eAd1	vyloženě
vodní	vodní	k2eAgInPc1d1	vodní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Platihypidium	Platihypidium	k1gNnSc1	Platihypidium
riparioides	riparioides	k1gInSc1	riparioides
<g/>
)	)	kIx)	)
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
osidluje	osidlovat	k5eAaImIp3nS	osidlovat
i	i	k9	i
xerotermní	xerotermní	k2eAgInPc4d1	xerotermní
biotopy	biotop	k1gInPc4	biotop
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Polytrichum	Polytrichum	k1gInSc1	Polytrichum
piliferum	piliferum	k1gInSc1	piliferum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
rodozměna	rodozměna	k1gFnSc1	rodozměna
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
výtrusné	výtrusný	k2eAgFnPc4d1	výtrusná
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
dominantní	dominantní	k2eAgFnSc1d1	dominantní
životní	životní	k2eAgFnSc7d1	životní
formou	forma	k1gFnSc7	forma
haploidní	haploidní	k2eAgInSc1d1	haploidní
gametofyt	gametofyt	k1gInSc1	gametofyt
<g/>
.	.	kIx.	.
</s>
<s>
Gametofyt	gametofyt	k1gInSc1	gametofyt
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
haploidního	haploidní	k2eAgInSc2d1	haploidní
výtrusu	výtrus	k1gInSc2	výtrus
(	(	kIx(	(
<g/>
spora	spora	k1gFnSc1	spora
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
prvoklíček	prvoklíček	k1gInSc1	prvoklíček
(	(	kIx(	(
<g/>
protonema	protonema	k1gFnSc1	protonema
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
doroste	dorůst	k5eAaPmIp3nS	dorůst
v	v	k7c4	v
mechovou	mechový	k2eAgFnSc4d1	mechová
rostlinku	rostlinka	k1gFnSc4	rostlinka
(	(	kIx(	(
<g/>
gametofor	gametofor	k1gInSc4	gametofor
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
nese	nést	k5eAaImIp3nS	nést
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
orgány	orgán	k1gInPc4	orgán
(	(	kIx(	(
<g/>
gametangia	gametangium	k1gNnSc2	gametangium
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
samčí	samčí	k2eAgFnSc2d1	samčí
pelatky	pelatka	k1gFnSc2	pelatka
(	(	kIx(	(
<g/>
antheridia	antheridium	k1gNnSc2	antheridium
<g/>
)	)	kIx)	)
a	a	k8xC	a
samičí	samičí	k2eAgInPc4d1	samičí
zárodečníky	zárodečník	k1gInPc4	zárodečník
(	(	kIx(	(
<g/>
archaegonia	archaegonium	k1gNnSc2	archaegonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oplození	oplození	k1gNnSc1	oplození
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vodní	vodní	k2eAgNnSc1d1	vodní
prostředí	prostředí	k1gNnSc1	prostředí
-	-	kIx~	-
samčí	samčí	k2eAgFnPc1d1	samčí
gamety	gameta	k1gFnPc1	gameta
jsou	být	k5eAaImIp3nP	být
aktivně	aktivně	k6eAd1	aktivně
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
biciliátní	biciliátní	k2eAgInPc1d1	biciliátní
spermatozoidy	spermatozoid	k1gInPc1	spermatozoid
(	(	kIx(	(
<g/>
dvoubičíkaté	dvoubičíkatý	k2eAgFnPc1d1	dvoubičíkatý
spermatické	spermatický	k2eAgFnPc1d1	spermatická
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
diploidní	diploidní	k2eAgFnSc2d1	diploidní
zygoty	zygota	k1gFnSc2	zygota
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
sporofyt	sporofyt	k1gInSc1	sporofyt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
gametofytu	gametofyt	k1gInSc6	gametofyt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
a	a	k8xC	a
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gInSc4	on
také	také	k9	také
vyživuje	vyživovat	k5eAaImIp3nS	vyživovat
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
zelený	zelený	k2eAgInSc1d1	zelený
sporofyt	sporofyt	k1gInSc1	sporofyt
mechu	mech	k1gInSc2	mech
druhu	druh	k1gInSc2	druh
Buxbaumia	Buxbaumia	k1gFnSc1	Buxbaumia
viridis	viridis	k1gFnSc1	viridis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
převážně	převážně	k6eAd1	převážně
nezelený	zelený	k2eNgInSc1d1	nezelený
sporofyt	sporofyt	k1gInSc1	sporofyt
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
štětem	štět	k1gInSc7	štět
(	(	kIx(	(
<g/>
seta	set	k2eAgFnSc1d1	Seta
<g/>
)	)	kIx)	)
a	a	k8xC	a
tobolkou	tobolka	k1gFnSc7	tobolka
(	(	kIx(	(
<g/>
capsula	capsula	k1gFnSc1	capsula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
meiotickému	meiotický	k2eAgNnSc3d1	meiotické
dělení	dělení	k1gNnSc3	dělení
a	a	k8xC	a
následně	následně	k6eAd1	následně
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
haploidních	haploidní	k2eAgInPc2d1	haploidní
výtrusů	výtrus	k1gInPc2	výtrus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
rostlinek	rostlinka	k1gFnPc2	rostlinka
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
klíčí	klíčit	k5eAaImIp3nS	klíčit
nový	nový	k2eAgInSc4d1	nový
gametofyt	gametofyt	k1gInSc4	gametofyt
<g/>
.	.	kIx.	.
</s>
<s>
Mechorosty	mechorost	k1gInPc1	mechorost
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgMnPc4d1	schopný
i	i	k9	i
vegetativního	vegetativní	k2eAgNnSc2d1	vegetativní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
především	především	k6eAd1	především
odlomením	odlomení	k1gNnSc7	odlomení
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
stélky	stélka	k1gFnSc2	stélka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mnohobuněčných	mnohobuněčný	k2eAgFnPc2d1	mnohobuněčná
propagulí	propagule	k1gFnPc2	propagule
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
množilek	množilka	k1gFnPc2	množilka
(	(	kIx(	(
<g/>
gemy	gema	k1gFnPc1	gema
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
mechorostů	mechorost	k1gInPc2	mechorost
tkví	tkvět	k5eAaImIp3nS	tkvět
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zadržování	zadržování	k1gNnSc6	zadržování
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tak	tak	k9	tak
bránění	bránění	k1gNnSc2	bránění
povodním	povodeň	k1gFnPc3	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
funkcemi	funkce	k1gFnPc7	funkce
jsou	být	k5eAaImIp3nP	být
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
erozi	eroze	k1gFnSc3	eroze
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
humusu	humus	k1gInSc2	humus
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
rašeliny	rašelina	k1gFnSc2	rašelina
(	(	kIx(	(
<g/>
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
,	,	kIx,	,
palivo	palivo	k1gNnSc1	palivo
<g/>
,	,	kIx,	,
léčebné	léčebný	k2eAgNnSc1d1	léčebné
využití	využití	k1gNnSc1	využití
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anthocerotaceae	Anthocerotaceae	k1gFnSc1	Anthocerotaceae
Notothyladaceae	Notothyladacea	k1gMnSc2	Notothyladacea
Haplomitriopsida	Haplomitriopsid	k1gMnSc2	Haplomitriopsid
Haplomitriales	Haplomitriales	k1gMnSc1	Haplomitriales
Haplomitriaceae	Haplomitriacea	k1gMnSc2	Haplomitriacea
Treubiales	Treubiales	k1gMnSc1	Treubiales
Treubiaceae	Treubiacea	k1gMnSc2	Treubiacea
Jungermanniopsida	Jungermanniopsid	k1gMnSc2	Jungermanniopsid
Fossombroniales	Fossombroniales	k1gMnSc1	Fossombroniales
Fossombroniaceae	Fossombroniacea	k1gMnSc2	Fossombroniacea
Pelliaceae	Pelliaceae	k1gNnSc2	Pelliaceae
Metzgeriales	Metzgeriales	k1gMnSc1	Metzgeriales
Aneuraceae	Aneuracea	k1gMnSc2	Aneuracea
Metzgeriaceae	Metzgeriacea	k1gFnSc2	Metzgeriacea
Pallavicianiaceae	Pallavicianiacea	k1gMnSc2	Pallavicianiacea
Lepicoleales	Lepicolealesa	k1gFnPc2	Lepicolealesa
Ptilidiaceae	Ptilidiacea	k1gMnSc2	Ptilidiacea
Trichocoleaceae	Trichocoleacea	k1gMnSc2	Trichocoleacea
Jungermanniales	Jungermanniales	k1gMnSc1	Jungermanniales
Antheliaceae	Antheliacea	k1gMnSc2	Antheliacea
Calypogeiaceae	Calypogeiaceae	k1gNnSc2	Calypogeiaceae
Cephaloziaceae	Cephaloziacea	k1gMnSc2	Cephaloziacea
Cephaloziellaceae	Cephaloziellacea	k1gMnSc2	Cephaloziellacea
Geocalycaceae	Geocalycacea	k1gMnSc2	Geocalycacea
Gymnomitriaceae	Gymnomitriacea	k1gMnSc2	Gymnomitriacea
Jungermanniaceae	Jungermanniacea	k1gMnSc2	Jungermanniacea
Lepidoziaceae	Lepidoziacea	k1gMnSc2	Lepidoziacea
Lphoziaceae	Lphoziacea	k1gMnSc2	Lphoziacea
Plagiochilaceae	Plagiochilacea	k1gMnSc2	Plagiochilacea
Pseudolepicoleaceae	Pseudolepicoleacea	k1gMnSc2	Pseudolepicoleacea
Scapaniaceae	Scapaniacea	k1gMnSc2	Scapaniacea
Porellales	Porellales	k1gMnSc1	Porellales
Jubulaceae	Jubulacea	k1gMnSc2	Jubulacea
Lejeuneaceae	Lejeuneacea	k1gFnSc2	Lejeuneacea
Porellaceae	Porellacea	k1gMnSc2	Porellacea
Radulales	Radulalesa	k1gFnPc2	Radulalesa
Radulaceae	Radulacea	k1gMnSc2	Radulacea
Marchantiopsida	Marchantiopsid	k1gMnSc2	Marchantiopsid
Blasiales	Blasiales	k1gMnSc1	Blasiales
Blasiaceae	Blasiacea	k1gMnSc2	Blasiacea
Marchantiales	Marchantiales	k1gMnSc1	Marchantiales
Aytoniaceae	Aytoniacea	k1gMnSc2	Aytoniacea
Conocephalaceae	Conocephalaceae	k1gNnSc2	Conocephalaceae
Lunulariaceae	Lunulariacea	k1gMnSc2	Lunulariacea
Marchantiaceae	Marchantiacea	k1gFnSc2	Marchantiacea
Targioniaceae	Targioniacea	k1gMnSc2	Targioniacea
Ricciales	Riccialesa	k1gFnPc2	Riccialesa
Ricciaceae	Ricciacea	k1gMnSc2	Ricciacea
Sphagnopsida	Sphagnopsid	k1gMnSc2	Sphagnopsid
Sphagnales	Sphagnales	k1gInSc1	Sphagnales
<g />
.	.	kIx.	.
</s>
<s>
Sphagnaceae	Sphagnaceae	k6eAd1	Sphagnaceae
Andreaeopsida	Andreaeopsida	k1gFnSc1	Andreaeopsida
Andreaeales	Andreaealesa	k1gFnPc2	Andreaealesa
Andreaeaceae	Andreaeacea	k1gMnSc2	Andreaeacea
Polytrichopsida	Polytrichopsid	k1gMnSc2	Polytrichopsid
Buxbaumiales	Buxbaumiales	k1gMnSc1	Buxbaumiales
Buxbaumiaceae	Buxbaumiacea	k1gMnSc2	Buxbaumiacea
Diphysciales	Diphysciales	k1gMnSc1	Diphysciales
Diphysciaceae	Diphysciacea	k1gMnSc2	Diphysciacea
Polytrichales	Polytrichales	k1gMnSc1	Polytrichales
Polytrichaceae	Polytrichacea	k1gMnSc2	Polytrichacea
Tetraphidales	Tetraphidales	k1gMnSc1	Tetraphidales
Tetraphidaceae	Tetraphidacea	k1gMnSc2	Tetraphidacea
Bryopsida	Bryopsid	k1gMnSc2	Bryopsid
Archidiales	Archidiales	k1gMnSc1	Archidiales
Úpolníčkovité	Úpolníčkovitý	k2eAgFnSc2d1	Úpolníčkovitý
(	(	kIx(	(
<g/>
Archidiaceae	Archidiacea	k1gFnSc2	Archidiacea
<g/>
)	)	kIx)	)
Bryales	Bryales	k1gInSc1	Bryales
Aulacomniaceae	Aulacomniacea	k1gMnSc2	Aulacomniacea
Bartramiaceae	Bartramiacea	k1gMnSc2	Bartramiacea
Bryaceae	Bryacea	k1gMnSc2	Bryacea
Sítozubkovité	Sítozubkovitý	k2eAgFnPc1d1	Sítozubkovitý
(	(	kIx(	(
<g/>
Cinclidiaceae	Cinclidiacea	k1gFnPc1	Cinclidiacea
<g/>
)	)	kIx)	)
Meesiaceae	Meesiaceae	k1gFnSc1	Meesiaceae
Mniaceae	Mniaceae	k1gFnSc1	Mniaceae
Plagiomniaceae	Plagiomniaceae	k1gFnSc1	Plagiomniaceae
Schistostegaceae	Schistostegaceae	k1gFnSc1	Schistostegaceae
Dicranales	Dicranales	k1gInSc4	Dicranales
Bruchovkovité	Bruchovkovitý	k2eAgFnPc1d1	Bruchovkovitý
(	(	kIx(	(
<g/>
Bruchiaceae	Bruchiacea	k1gFnPc1	Bruchiacea
<g/>
)	)	kIx)	)
Dvouhrotcovité	Dvouhrotcovitý	k2eAgFnPc1d1	Dvouhrotcovitý
(	(	kIx(	(
<g/>
Dicranaceae	Dicranacea	k1gFnPc1	Dicranacea
<g/>
)	)	kIx)	)
Útlovláskovité	Útlovláskovitý	k2eAgFnPc1d1	Útlovláskovitý
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Ditrichaceae	Ditrichaceae	k1gNnSc1	Ditrichaceae
<g/>
)	)	kIx)	)
Ephemeraceae	Ephemeraceae	k1gNnSc1	Ephemeraceae
Fissidentaceae	Fissidentacea	k1gFnSc2	Fissidentacea
Bělomechovité	Bělomechovitý	k2eAgFnSc2d1	Bělomechovitý
(	(	kIx(	(
<g/>
Leucobryaceae	Leucobryacea	k1gFnSc2	Leucobryacea
<g/>
)	)	kIx)	)
Encalyptales	Encalyptales	k1gMnSc1	Encalyptales
Encalyptaceae	Encalyptacea	k1gFnSc2	Encalyptacea
Funariales	Funariales	k1gMnSc1	Funariales
Disceliaceae	Disceliacea	k1gFnSc2	Disceliacea
Funariaceae	Funariaceae	k1gNnPc2	Funariaceae
Grimmiales	Grimmiales	k1gMnSc1	Grimmiales
Děrkavkovité	Děrkavkovitý	k2eAgFnSc2d1	Děrkavkovitý
(	(	kIx(	(
<g/>
Grimmiaceae	Grimmiacea	k1gFnSc2	Grimmiacea
<g/>
)	)	kIx)	)
Mnoholistcovité	Mnoholistcovitý	k2eAgFnSc2d1	Mnoholistcovitý
(	(	kIx(	(
<g/>
Ptychomitriaceae	Ptychomitriacea	k1gFnSc2	Ptychomitriacea
<g/>
)	)	kIx)	)
Kápěnkovité	Kápěnkovitý	k2eAgFnSc2d1	Kápěnkovitý
(	(	kIx(	(
<g/>
Seligeriaceae	Seligeriacea	k1gFnSc2	Seligeriacea
<g/>
)	)	kIx)	)
Hedwigiales	Hedwigiales	k1gMnSc1	Hedwigiales
Hedwigiaceae	Hedwigiacea	k1gMnSc2	Hedwigiacea
Hookeriales	Hookeriales	k1gMnSc1	Hookeriales
Hookeriaceae	Hookeriacea	k1gMnSc2	Hookeriacea
Hypnales	Hypnales	k1gMnSc1	Hypnales
Amblystegiaceae	Amblystegiacea	k1gMnSc2	Amblystegiacea
Brachytheciaceae	Brachytheciaceae	k1gNnSc2	Brachytheciaceae
Entodontaceae	Entodontacea	k1gMnSc2	Entodontacea
Hylocomiaceae	Hylocomiacea	k1gMnSc2	Hylocomiacea
Hypnaceae	Hypnacea	k1gMnSc2	Hypnacea
Leskeaceae	Leskeacea	k1gMnSc2	Leskeacea
Plagiotheciaceae	Plagiotheciacea	k1gMnSc2	Plagiotheciacea
Pterigynandraceae	Pterigynandracea	k1gMnSc2	Pterigynandracea
Rhytidiaceae	Rhytidiacea	k1gMnSc2	Rhytidiacea
Thuidiaceae	Thuidiacea	k1gMnSc2	Thuidiacea
Leucodontales	Leucodontales	k1gMnSc1	Leucodontales
Fontinalaceae	Fontinalacea	k1gMnSc2	Fontinalacea
Leucodontaceae	Leucodontacea	k1gMnSc2	Leucodontacea
Neckeraceae	Neckeracea	k1gMnSc2	Neckeracea
Orthotrichales	Orthotrichales	k1gMnSc1	Orthotrichales
Orthotrichaceae	Orthotrichacea	k1gMnSc2	Orthotrichacea
Pottiales	Pottiales	k1gMnSc1	Pottiales
Cinclidotaceae	Cinclidotacea	k1gMnSc2	Cinclidotacea
Pozemničkovité	Pozemničkovitý	k2eAgFnSc2d1	Pozemničkovitý
(	(	kIx(	(
<g/>
Pottiaceae	Pottiacea	k1gFnSc2	Pottiacea
<g/>
)	)	kIx)	)
Splachnales	Splachnales	k1gMnSc1	Splachnales
Splachnaceae	Splachnacea	k1gFnSc2	Splachnacea
Timmiales	Timmiales	k1gMnSc1	Timmiales
Timmiaceae	Timmiaceae	k1gNnSc2	Timmiaceae
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mechorosty	mechorost	k1gInPc7	mechorost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
