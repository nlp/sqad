<p>
<s>
Pohraničí	pohraničí	k1gNnSc1	pohraničí
(	(	kIx(	(
<g/>
pohraniční	pohraniční	k2eAgNnPc1d1	pohraniční
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
pohraniční	pohraniční	k2eAgFnPc1d1	pohraniční
oblasti	oblast	k1gFnPc1	oblast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
podél	podél	k6eAd1	podél
nebo	nebo	k8xC	nebo
blíže	blízce	k6eAd2	blízce
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Pohraničí	pohraničí	k1gNnSc1	pohraničí
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
území	území	k1gNnSc4	území
sousedního	sousední	k2eAgInSc2d1	sousední
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Příhraničí	příhraničí	k1gNnSc1	příhraničí
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
ležící	ležící	k2eAgFnSc1d1	ležící
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
státem	stát	k1gInSc7	stát
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
území	území	k1gNnSc1	území
sousedního	sousední	k2eAgInSc2d1	sousední
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Geograf	geograf	k1gMnSc1	geograf
Peter	Peter	k1gMnSc1	Peter
Haggett	Haggett	k1gMnSc1	Haggett
nepřímo	přímo	k6eNd1	přímo
definuje	definovat	k5eAaBmIp3nS	definovat
příhraniční	příhraniční	k2eAgInSc1d1	příhraniční
region	region	k1gInSc1	region
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
každá	každý	k3xTgFnSc1	každý
hranice	hranice	k1gFnSc1	hranice
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
zónu	zóna	k1gFnSc4	zóna
tlaku	tlak	k1gInSc2	tlak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
zóna	zóna	k1gFnSc1	zóna
tlaku	tlak	k1gInSc2	tlak
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
zeslabuje	zeslabovat	k5eAaImIp3nS	zeslabovat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hraniční	hraniční	k2eAgFnSc1d1	hraniční
linie	linie	k1gFnSc1	linie
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xS	jako
lineární	lineární	k2eAgNnSc1d1	lineární
oddělení	oddělení	k1gNnSc1	oddělení
dvou	dva	k4xCgInPc2	dva
státních	státní	k2eAgInPc2d1	státní
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
příhraniční	příhraniční	k2eAgNnSc1d1	příhraniční
okrajové	okrajový	k2eAgNnSc1d1	okrajové
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
oblast	oblast	k1gFnSc1	oblast
územně	územně	k6eAd1	územně
blízká	blízký	k2eAgFnSc1d1	blízká
hraniční	hraniční	k2eAgFnSc3d1	hraniční
linii	linie	k1gFnSc3	linie
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
závisí	záviset	k5eAaImIp3nS	záviset
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
specifičnosti	specifičnost	k1gFnSc6	specifičnost
místních	místní	k2eAgFnPc2d1	místní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
pohraničí	pohraničí	k1gNnSc4	pohraničí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různá	různý	k2eAgNnPc4d1	různé
historická	historický	k2eAgNnPc4d1	historické
období	období	k1gNnPc4	období
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
totiž	totiž	k9	totiž
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
individuálně	individuálně	k6eAd1	individuálně
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Anderson	Anderson	k1gMnSc1	Anderson
a	a	k8xC	a
Liam	Liam	k1gMnSc1	Liam
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Dowd	Dowd	k1gInSc4	Dowd
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
rozrůzněnost	rozrůzněnost	k1gFnSc4	rozrůzněnost
hraničních	hraniční	k2eAgInPc2d1	hraniční
regionů	region	k1gInPc2	region
a	a	k8xC	a
úloh	úloha	k1gFnPc2	úloha
hranic	hranice	k1gFnPc2	hranice
konstatováním	konstatování	k1gNnSc7	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
každá	každý	k3xTgFnSc1	každý
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
region	region	k1gInSc4	region
jsou	být	k5eAaImIp3nP	být
unikátní	unikátní	k2eAgFnPc1d1	unikátní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uznávané	uznávaný	k2eAgFnSc3d1	uznávaná
a	a	k8xC	a
nepopiratelné	popiratelný	k2eNgFnSc3d1	nepopiratelná
svázanosti	svázanost	k1gFnSc3	svázanost
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
příhraničních	příhraniční	k2eAgFnPc2d1	příhraniční
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
celky	celek	k1gInPc7	celek
(	(	kIx(	(
<g/>
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
či	či	k8xC	či
regionální	regionální	k2eAgFnSc6d1	regionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
)	)	kIx)	)
Martin	Martin	k1gMnSc1	Martin
Hampl	Hampl	k1gMnSc1	Hampl
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
zobecnil	zobecnit	k5eAaPmAgInS	zobecnit
také	také	k9	také
na	na	k7c4	na
unikátnost	unikátnost	k1gFnSc4	unikátnost
všech	všecek	k3xTgInPc2	všecek
geografických	geografický	k2eAgInPc2d1	geografický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
předlistopadových	předlistopadový	k2eAgFnPc2d1	předlistopadová
zkušeností	zkušenost	k1gFnPc2	zkušenost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
geografické	geografický	k2eAgFnSc6d1	geografická
obci	obec	k1gFnSc6	obec
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
často	často	k6eAd1	často
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
jednostranně	jednostranně	k6eAd1	jednostranně
rozloženou	rozložený	k2eAgFnSc4d1	rozložená
podél	podél	k7c2	podél
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
<g/>
Naopak	naopak	k6eAd1	naopak
polský	polský	k2eAgMnSc1d1	polský
geograf	geograf	k1gMnSc1	geograf
Krystian	Krystian	k1gMnSc1	Krystian
Heffner	Heffner	k1gMnSc1	Heffner
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
např.	např.	kA	např.
geografové	geograf	k1gMnPc1	geograf
Milan	Milan	k1gMnSc1	Milan
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dokoupil	Dokoupil	k1gMnSc1	Dokoupil
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
výraz	výraz	k1gInSc1	výraz
etapy	etapa	k1gFnSc2	etapa
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
periferních	periferní	k2eAgInPc2d1	periferní
k	k	k7c3	k
integrujícím	integrující	k2eAgInPc3d1	integrující
se	se	k3xPyFc4	se
regionům	region	k1gInPc3	region
vnímá	vnímat	k5eAaImIp3nS	vnímat
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
<g/>
"	"	kIx"	"
oblast	oblast	k1gFnSc4	oblast
rozloženou	rozložený	k2eAgFnSc4d1	rozložená
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hranice	hranice	k1gFnSc2	hranice
a	a	k8xC	a
"	"	kIx"	"
<g/>
příhraniční	příhraniční	k2eAgFnSc4d1	příhraniční
<g/>
"	"	kIx"	"
oblast	oblast	k1gFnSc4	oblast
podél	podél	k7c2	podél
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
<g/>
Opak	opak	k1gInSc1	opak
pohraničí	pohraničí	k1gNnSc2	pohraničí
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hraniční	hraniční	k2eAgInPc1d1	hraniční
regiony	region	k1gInPc1	region
často	často	k6eAd1	často
trpí	trpět	k5eAaImIp3nP	trpět
historickými	historický	k2eAgInPc7d1	historický
důsledky	důsledek	k1gInPc7	důsledek
své	svůj	k3xOyFgFnSc2	svůj
periferní	periferní	k2eAgFnSc2d1	periferní
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
integrací	integrace	k1gFnSc7	integrace
do	do	k7c2	do
převládajících	převládající	k2eAgFnPc2d1	převládající
struktur	struktura	k1gFnPc2	struktura
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
státních	státní	k2eAgNnPc2d1	státní
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývající	vyplývající	k2eAgInPc1d1	vyplývající
izolací	izolace	k1gFnSc7	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Přeshraniční	přeshraniční	k2eAgFnSc1d1	přeshraniční
spolupráce	spolupráce	k1gFnSc1	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
pomoci	pomoct	k5eAaPmF	pomoct
tyto	tento	k3xDgFnPc4	tento
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
a	a	k8xC	a
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
příhraničních	příhraniční	k2eAgInPc6d1	příhraniční
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Pohraničí	pohraničí	k1gNnSc1	pohraničí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
periferie	periferie	k1gFnPc4	periferie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pohraniční	pohraniční	k2eAgFnPc4d1	pohraniční
periferie	periferie	k1gFnPc4	periferie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
periferie	periferie	k1gFnPc4	periferie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Slovenska	Slovensko	k1gNnSc2	Slovensko
Bratislava	Bratislava	k1gFnSc1	Bratislava
ležící	ležící	k2eAgFnSc1d1	ležící
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
státních	státní	k2eAgFnPc6d1	státní
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
je	být	k5eAaImIp3nS	být
jádrovým	jádrový	k2eAgInSc7d1	jádrový
regionem	region	k1gInSc7	region
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pohraničí	pohraničí	k1gNnSc3	pohraničí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
pohraničí	pohraničí	k1gNnSc2	pohraničí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
zastřešujícím	zastřešující	k2eAgInSc6d1	zastřešující
pojmu	pojem	k1gInSc6	pojem
pro	pro	k7c4	pro
pohraničí	pohraničí	k1gNnSc4	pohraničí
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Slezska	Slezsko	k1gNnSc2	Slezsko
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
též	též	k9	též
"	"	kIx"	"
<g/>
naše	náš	k3xOp1gNnSc4	náš
pohraničí	pohraničí	k1gNnSc4	pohraničí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
podmínkách	podmínka	k1gFnPc6	podmínka
od	od	k7c2	od
konce	konec	k1gInSc2	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
území	území	k1gNnPc4	území
výrazně	výrazně	k6eAd1	výrazně
diferencovaná	diferencovaný	k2eAgNnPc4d1	diferencované
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
vývojem	vývoj	k1gInSc7	vývoj
i	i	k8xC	i
národnostním	národnostní	k2eAgNnSc7d1	národnostní
složením	složení	k1gNnSc7	složení
(	(	kIx(	(
<g/>
s	s	k7c7	s
většinovým	většinový	k2eAgNnSc7d1	většinové
zastoupením	zastoupení	k1gNnSc7	zastoupení
Němců	Němec	k1gMnPc2	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
české	český	k2eAgNnSc1d1	české
pohraničí	pohraničí	k1gNnSc1	pohraničí
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
geografické	geografický	k2eAgNnSc4d1	geografické
vymezení	vymezení	k1gNnSc4	vymezení
nejsou	být	k5eNaImIp3nP	být
jasně	jasně	k6eAd1	jasně
definované	definovaný	k2eAgFnPc1d1	definovaná
<g/>
.	.	kIx.	.
</s>
<s>
Pohraničí	pohraničí	k1gNnSc1	pohraničí
moderní	moderní	k2eAgFnSc2d1	moderní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nikdy	nikdy	k6eAd1	nikdy
netvořilo	tvořit	k5eNaImAgNnS	tvořit
samostatný	samostatný	k2eAgInSc4d1	samostatný
a	a	k8xC	a
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
celek	celek	k1gInSc4	celek
ani	ani	k8xC	ani
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
</s>
</p>
<p>
<s>
geografické	geografický	k2eAgInPc1d1	geografický
ani	ani	k8xC	ani
národnostní	národnostní	k2eAgInPc1d1	národnostní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
přesnou	přesný	k2eAgFnSc4d1	přesná
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
</s>
</p>
<p>
<s>
pohraničím	pohraničí	k1gNnSc7	pohraničí
a	a	k8xC	a
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
vymezen	vymezen	k2eAgInSc4d1	vymezen
termín	termín	k1gInSc4	termín
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
177	[number]	k4	177
politických	politický	k2eAgInPc2d1	politický
okresů	okres	k1gInPc2	okres
sousedících	sousedící	k2eAgInPc2d1	sousedící
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc7d1	státní
hranicí	hranice	k1gFnSc7	hranice
(	(	kIx(	(
<g/>
55	[number]	k4	55
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
22	[number]	k4	22
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
lze	lze	k6eAd1	lze
jako	jako	k8xC	jako
české	český	k2eAgNnSc4d1	české
pohraničí	pohraničí	k1gNnSc4	pohraničí
vymezit	vymezit	k5eAaPmF	vymezit
území	území	k1gNnSc4	území
předválečného	předválečný	k2eAgNnSc2d1	předválečné
německého	německý	k2eAgNnSc2d1	německé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
pojmem	pojem	k1gInSc7	pojem
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
?	?	kIx.	?
</s>
<s>
První	první	k4xOgNnPc4	první
vymezení	vymezení	k1gNnPc4	vymezení
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc1	první
<g/>
"	"	kIx"	"
dle	dle	k7c2	dle
sociálního	sociální	k2eAgMnSc2d1	sociální
geografa	geograf	k1gMnSc2	geograf
Milana	Milan	k1gMnSc2	Milan
Jeřábka	Jeřábek	k1gMnSc2	Jeřábek
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
ztotožněno	ztotožnit	k5eAaPmNgNnS	ztotožnit
s	s	k7c7	s
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
okupovala	okupovat	k5eAaBmAgFnS	okupovat
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
dosidlovací	dosidlovací	k2eAgFnSc2d1	dosidlovací
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
uplatňování	uplatňování	k1gNnSc2	uplatňování
dalších	další	k2eAgInPc2d1	další
nástrojů	nástroj	k1gInPc2	nástroj
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
a	a	k8xC	a
1969	[number]	k4	1969
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
administrativně	administrativně	k6eAd1	administrativně
správnímu	správní	k2eAgNnSc3d1	správní
členění	členění	k1gNnSc3	členění
provedeny	proveden	k2eAgFnPc1d1	provedena
dílčí	dílčí	k2eAgFnPc4d1	dílčí
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Vymezení	vymezený	k2eAgMnPc1d1	vymezený
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1986	[number]	k4	1986
a	a	k8xC	a
1991	[number]	k4	1991
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
pouze	pouze	k6eAd1	pouze
jihozápadního	jihozápadní	k2eAgNnSc2d1	jihozápadní
pohraničí	pohraničí	k1gNnSc2	pohraničí
se	s	k7c7	s
západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
vymezen	vymezen	k2eAgInSc1d1	vymezen
pás	pás	k1gInSc1	pás
obcí	obec	k1gFnPc2	obec
zahrnutých	zahrnutý	k2eAgFnPc2d1	zahrnutá
do	do	k7c2	do
vybraného	vybraný	k2eAgNnSc2d1	vybrané
území	území	k1gNnSc2	území
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
oblastí	oblast	k1gFnPc2	oblast
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Geograf	geograf	k1gMnSc1	geograf
Martin	Martin	k1gMnSc1	Martin
Hampl	Hampl	k1gMnSc1	Hampl
definuje	definovat	k5eAaBmIp3nS	definovat
pohraničí	pohraničí	k1gNnSc4	pohraničí
Česka	Česko	k1gNnSc2	Česko
jako	jako	k8xC	jako
soubor	soubor	k1gInSc1	soubor
okresů	okres	k1gInPc2	okres
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Geografové	geograf	k1gMnPc1	geograf
z	z	k7c2	z
pobočky	pobočka	k1gFnSc2	pobočka
Brno	Brno	k1gNnSc1	Brno
Ústavu	ústav	k1gInSc2	ústav
geoniky	geonik	k1gInPc1	geonik
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.	v.	k?	v.
v.	v.	k?	v.
i.	i.	k?	i.
<g/>
,	,	kIx,	,
definují	definovat	k5eAaBmIp3nP	definovat
pohraničí	pohraničí	k1gNnSc4	pohraničí
Česka	Česko	k1gNnSc2	Česko
jako	jako	k8xS	jako
soubor	soubor	k1gInSc1	soubor
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
ČR	ČR	kA	ČR
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
soubor	soubor	k1gInSc1	soubor
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
<g/>
Celní	celní	k2eAgNnSc1d1	celní
pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
pásmo	pásmo	k1gNnSc1	pásmo
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
celního	celní	k2eAgNnSc2d1	celní
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
celní	celní	k2eAgNnSc1d1	celní
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vnitrozemských	vnitrozemský	k2eAgFnPc2d1	vnitrozemská
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
25	[number]	k4	25
km	km	kA	km
od	od	k7c2	od
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
a	a	k8xC	a
kruh	kruh	k1gInSc1	kruh
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
25	[number]	k4	25
km	km	kA	km
okolo	okolo	k7c2	okolo
celních	celní	k2eAgNnPc2d1	celní
letišť	letiště	k1gNnPc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
celního	celní	k2eAgNnSc2d1	celní
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
celní	celní	k2eAgNnSc1d1	celní
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
<g/>
.	.	kIx.	.
<g/>
Dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
vymezení	vymezení	k1gNnSc2	vymezení
pohraničí	pohraničí	k1gNnSc6	pohraničí
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
vnímání	vnímání	k1gNnSc1	vnímání
lidí	člověk	k1gMnPc2	člověk
nebo	nebo	k8xC	nebo
vymezení	vymezení	k1gNnSc2	vymezení
pohraničí	pohraničí	k1gNnSc2	pohraničí
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
prostupnosti	prostupnost	k1gFnSc2	prostupnost
hraničních	hraniční	k2eAgInPc2d1	hraniční
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vstup	vstup	k1gInSc1	vstup
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
a	a	k8xC	a
do	do	k7c2	do
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
plné	plný	k2eAgNnSc4d1	plné
obnovení	obnovení	k1gNnSc4	obnovení
a	a	k8xC	a
zejména	zejména	k9	zejména
novou	nový	k2eAgFnSc4d1	nová
kvalitu	kvalita	k1gFnSc4	kvalita
přeshraničních	přeshraniční	k2eAgFnPc2d1	přeshraniční
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pohraničí	pohraničí	k1gNnPc1	pohraničí
versus	versus	k7c1	versus
Sudety	Sudety	k1gInPc1	Sudety
===	===	k?	===
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
pohraničí	pohraničí	k1gNnSc2	pohraničí
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
pro	pro	k7c4	pro
souvislé	souvislý	k2eAgNnSc4d1	souvislé
německé	německý	k2eAgNnSc4d1	německé
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
bylo	být	k5eAaImAgNnS	být
německé	německý	k2eAgNnSc1d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
vyhnáno	vyhnán	k2eAgNnSc1d1	vyhnáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
až	až	k9	až
o	o	k7c4	o
58	[number]	k4	58
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
měl	mít	k5eAaImAgInS	mít
pojem	pojem	k1gInSc1	pojem
pohraničí	pohraničí	k1gNnSc2	pohraničí
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
území	území	k1gNnSc1	území
<g/>
)	)	kIx)	)
nahradit	nahradit	k5eAaPmF	nahradit
pojem	pojem	k1gInSc4	pojem
Sudety	Sudety	k1gFnPc4	Sudety
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Sudetenland	Sudetenland	k1gInSc1	Sudetenland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
vyhláška	vyhláška	k1gFnSc1	vyhláška
stanovující	stanovující	k2eAgFnSc2d1	stanovující
nepřípustnost	nepřípustnost	k1gFnSc4	nepřípustnost
užívání	užívání	k1gNnSc2	užívání
názvu	název	k1gInSc2	název
Sudety	Sudety	k1gInPc1	Sudety
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Sudety	Sudety	k1gInPc1	Sudety
<g/>
"	"	kIx"	"
nepřípustný	přípustný	k2eNgMnSc1d1	nepřípustný
<g/>
:	:	kIx,	:
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
Sudety	Sudety	k1gFnPc1	Sudety
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc2	jeho
odvozenin	odvozenina	k1gFnPc2	odvozenina
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
v	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
příslušného	příslušný	k2eAgNnSc2d1	příslušné
území	území	k1gNnSc2	území
buď	buď	k8xC	buď
užíváno	užíván	k2eAgNnSc4d1	užíváno
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
pohraniční	pohraniční	k2eAgNnPc1d1	pohraniční
území	území	k1gNnPc1	území
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnSc1	opatření
tohoto	tento	k3xDgMnSc2	tento
dlužno	dlužno	k6eAd1	dlužno
dbáti	dbát	k5eAaImF	dbát
zejména	zejména	k9	zejména
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
veškeré	veškerý	k3xTgFnSc2	veškerý
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dle	dle	k7c2	dle
Stanoviska	stanovisko	k1gNnSc2	stanovisko
Odboru	odbor	k1gInSc2	odbor
legislativy	legislativa	k1gFnSc2	legislativa
a	a	k8xC	a
koordinace	koordinace	k1gFnSc2	koordinace
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
Evropských	evropský	k2eAgNnPc2d1	Evropské
společenství	společenství	k1gNnPc2	společenství
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
používání	používání	k1gNnSc6	používání
názvu	název	k1gInSc6	název
Sudety	Sudety	k1gFnPc1	Sudety
ze	z	k7c2	z
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
však	však	k9	však
není	být	k5eNaImIp3nS	být
vyhláška	vyhláška	k1gFnSc1	vyhláška
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
součástí	součást	k1gFnPc2	součást
platného	platný	k2eAgInSc2d1	platný
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Sudetoněmecký	sudetoněmecký	k2eAgInSc1d1	sudetoněmecký
landsmanšaft	landsmanšaft	k1gInSc1	landsmanšaft
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
termíny	termín	k1gInPc1	termín
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
Čechy	Čechy	k1gFnPc1	Čechy
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Innerböhmen	Innerböhmen	k1gInSc1	Innerböhmen
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Innermähren	Innermährna	k1gFnPc2	Innermährna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pohraničí	pohraničí	k1gNnSc3	pohraničí
vnější	vnější	k2eAgFnSc2d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
===	===	k?	===
</s>
</p>
<p>
<s>
Sociolog	sociolog	k1gMnSc1	sociolog
Quido	Quido	k1gNnSc1	Quido
Kastner	Kastner	k1gMnSc1	Kastner
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tzv.	tzv.	kA	tzv.
české	český	k2eAgInPc1d1	český
"	"	kIx"	"
<g/>
vnější	vnější	k2eAgInSc1d1	vnější
<g/>
"	"	kIx"	"
pohraničí	pohraničí	k1gNnSc1	pohraničí
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
české	český	k2eAgInPc1d1	český
"	"	kIx"	"
<g/>
vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
<g/>
"	"	kIx"	"
pohraničí	pohraničí	k1gNnPc4	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
pohraničí	pohraničí	k1gNnSc1	pohraničí
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
jazykově	jazykově	k6eAd1	jazykově
a	a	k8xC	a
kulturou	kultura	k1gFnSc7	kultura
převážně	převážně	k6eAd1	převážně
německého	německý	k2eAgNnSc2d1	německé
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
co	co	k9	co
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
rozsahu	rozsah	k1gInSc2	rozsah
proměnlivý	proměnlivý	k2eAgInSc4d1	proměnlivý
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
jeho	jeho	k3xOp3gFnSc2	jeho
plochy	plocha	k1gFnSc2	plocha
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
až	až	k9	až
v	v	k7c6	v
první	první	k4xOgFnSc6	první
půli	půle	k1gFnSc6	půle
století	století	k1gNnSc2	století
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
české	český	k2eAgInPc1d1	český
"	"	kIx"	"
<g/>
vnější	vnější	k2eAgInSc1d1	vnější
<g/>
"	"	kIx"	"
pohraničí	pohraničí	k1gNnSc1	pohraničí
je	být	k5eAaImIp3nS	být
synonymem	synonymum	k1gNnSc7	synonymum
pojmu	pojem	k1gInSc2	pojem
Sudety	Sudety	k1gFnPc1	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgNnSc1d1	vnější
pohraničí	pohraničí	k1gNnSc1	pohraničí
bylo	být	k5eAaImAgNnS	být
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
jednotného	jednotný	k2eAgInSc2d1	jednotný
jazykového	jazykový	k2eAgInSc2d1	jazykový
úzu	úzus	k1gInSc2	úzus
spravovanou	spravovaný	k2eAgFnSc7d1	spravovaná
správními	správní	k2eAgInPc7d1	správní
centry	centr	k1gInPc7	centr
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obvodu	obvod	k1gInSc6	obvod
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
s	s	k7c7	s
menšími	malý	k2eAgInPc7d2	menší
kulturními	kulturní	k2eAgInPc7d1	kulturní
kontakty	kontakt	k1gInPc7	kontakt
k	k	k7c3	k
přirozenému	přirozený	k2eAgInSc3d1	přirozený
centru	centr	k1gInSc3	centr
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
však	však	k9	však
s	s	k7c7	s
užšími	úzký	k2eAgInPc7d2	užší
vztahy	vztah	k1gInPc7	vztah
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
stránce	stránka	k1gFnSc6	stránka
však	však	k9	však
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
napojena	napojit	k5eAaPmNgFnS	napojit
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
zboží	zboží	k1gNnSc2	zboží
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
a	a	k8xC	a
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
české	český	k2eAgFnSc3d1	Česká
"	"	kIx"	"
<g/>
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
<g/>
"	"	kIx"	"
pohraničí	pohraničí	k1gNnSc1	pohraničí
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
přiléhající	přiléhající	k2eAgNnSc4d1	přiléhající
k	k	k7c3	k
Sudetům	Sudety	k1gInPc3	Sudety
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
probíhalo	probíhat	k5eAaImAgNnS	probíhat
prolínání	prolínání	k1gNnSc4	prolínání
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
kulturního	kulturní	k2eAgInSc2d1	kulturní
živlu	živel	k1gInSc2	živel
<g/>
.	.	kIx.	.
</s>
<s>
Uzlovými	uzlový	k2eAgInPc7d1	uzlový
body	bod	k1gInPc7	bod
českého	český	k2eAgNnSc2d1	české
"	"	kIx"	"
<g/>
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
<g/>
"	"	kIx"	"
pohraničí	pohraničí	k1gNnSc2	pohraničí
byla	být	k5eAaImAgNnP	být
města	město	k1gNnPc1	město
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
Turnov	Turnov	k1gInSc1	Turnov
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bohaté	bohatý	k2eAgInPc1d1	bohatý
Sudety	Sudety	k1gInPc1	Sudety
a	a	k8xC	a
Chudé	Chudé	k2eAgInPc1d1	Chudé
Sudety	Sudety	k1gInPc1	Sudety
====	====	k?	====
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgMnSc1d1	sociální
geograf	geograf	k1gMnSc1	geograf
Radim	Radim	k1gMnSc1	Radim
Perlín	perlín	k1gMnSc1	perlín
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
Sudety	Sudety	k1gFnPc4	Sudety
(	(	kIx(	(
<g/>
=	=	kIx~	=
české	český	k2eAgNnSc4d1	české
"	"	kIx"	"
<g/>
vnější	vnější	k2eAgNnSc4d1	vnější
<g/>
"	"	kIx"	"
pohraničí	pohraničí	k1gNnSc4	pohraničí
<g/>
)	)	kIx)	)
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Bohaté	bohatý	k2eAgInPc4d1	bohatý
Sudety	Sudety	k1gInPc4	Sudety
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Chudé	Chudé	k2eAgInPc1d1	Chudé
Sudety	Sudety	k1gInPc1	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc4	oblast
Bohaté	bohatý	k2eAgInPc4d1	bohatý
Sudety	Sudety	k1gInPc4	Sudety
je	být	k5eAaImIp3nS	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
bývalé	bývalý	k2eAgFnSc2d1	bývalá
česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
etnické	etnický	k2eAgFnSc2d1	etnická
hranice	hranice	k1gFnSc2	hranice
rozšíření	rozšíření	k1gNnSc1	rozšíření
původního	původní	k2eAgNnSc2d1	původní
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
osídlení	osídlení	k1gNnSc2	osídlení
podél	podél	k7c2	podél
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
hranice	hranice	k1gFnSc2	hranice
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgInPc1d1	bohatý
Sudety	Sudety	k1gInPc1	Sudety
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
od	od	k7c2	od
ašského	ašský	k2eAgInSc2d1	ašský
výběžku	výběžek	k1gInSc2	výběžek
přes	přes	k7c4	přes
Karlovarsko	Karlovarsko	k1gNnSc4	Karlovarsko
<g/>
,	,	kIx,	,
severočeskou	severočeský	k2eAgFnSc4d1	Severočeská
konurbaci	konurbace	k1gFnSc4	konurbace
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc4	oblast
Českého	český	k2eAgNnSc2d1	české
středohoří	středohoří	k1gNnSc2	středohoří
<g/>
,	,	kIx,	,
Liberecko	Liberecko	k1gNnSc1	Liberecko
<g/>
,	,	kIx,	,
krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
a	a	k8xC	a
orlické	orlický	k2eAgNnSc1d1	Orlické
podhůří	podhůří	k1gNnSc1	podhůří
až	až	k9	až
na	na	k7c4	na
Jesenicko	Jesenicko	k1gNnSc4	Jesenicko
a	a	k8xC	a
Opavsko	Opavsko	k1gNnSc4	Opavsko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Bohaté	bohatý	k2eAgInPc4d1	bohatý
Sudety	Sudety	k1gInPc4	Sudety
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
industrializace	industrializace	k1gFnSc1	industrializace
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc1	urbanizace
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Chudé	Chudé	k2eAgFnPc1d1	Chudé
Sudety	Sudety	k1gFnPc1	Sudety
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
podél	podél	k7c2	podél
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
hranice	hranice	k1gFnSc2	hranice
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Chudé	Chudé	k2eAgInPc1d1	Chudé
Sudety	Sudety	k1gInPc1	Sudety
jsou	být	k5eAaImIp3nP	být
vymezeny	vymezit	k5eAaPmNgInP	vymezit
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
původní	původní	k2eAgFnSc2d1	původní
etnické	etnický	k2eAgFnSc2d1	etnická
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Bohatých	bohatý	k2eAgFnPc2d1	bohatá
Sudet	Sudety	k1gFnPc2	Sudety
v	v	k7c6	v
rurálním	rurální	k2eAgNnSc6d1	rurální
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tajného	tajný	k2eAgNnSc2d1	tajné
nařízení	nařízení	k1gNnSc2	nařízení
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1951	[number]	k4	1951
se	se	k3xPyFc4	se
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
podél	podél	k6eAd1	podél
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
se	s	k7c7	s
západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
)	)	kIx)	)
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
pásmo	pásmo	k1gNnSc1	pásmo
hluboké	hluboký	k2eAgInPc4d1	hluboký
dva	dva	k4xCgInPc4	dva
kilometry	kilometr	k1gInPc4	kilometr
<g/>
,	,	kIx,	,
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
navazovalo	navazovat	k5eAaImAgNnS	navazovat
hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
o	o	k7c6	o
hloubce	hloubka	k1gFnSc6	hloubka
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
šesti	šest	k4xCc2	šest
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
i	i	k9	i
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
terénu	terén	k1gInSc2	terén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zakázaném	zakázaný	k2eAgNnSc6d1	zakázané
pásmu	pásmo	k1gNnSc6	pásmo
nesměl	smět	k5eNaImAgMnS	smět
nikdo	nikdo	k3yNnSc1	nikdo
bydlet	bydlet	k5eAaImF	bydlet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nucené	nucený	k2eAgNnSc1d1	nucené
vysídlení	vysídlení	k1gNnSc1	vysídlení
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
státně	státně	k6eAd1	státně
nespolehlivých	spolehlivý	k2eNgFnPc2d1	nespolehlivá
(	(	kIx(	(
<g/>
státu	stát	k1gInSc2	stát
nepohodlných	pohodlný	k2eNgFnPc2d1	nepohodlná
<g/>
)	)	kIx)	)
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
1952	[number]	k4	1952
vysídleno	vysídlit	k5eAaPmNgNnS	vysídlit
1249	[number]	k4	1249
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
4300	[number]	k4	4300
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
bylo	být	k5eAaImAgNnS	být
přísně	přísně	k6eAd1	přísně
střeženo	stříct	k5eAaPmNgNnS	stříct
příslušníky	příslušník	k1gMnPc7	příslušník
Pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regionalizace	regionalizace	k1gFnPc1	regionalizace
pohraničí	pohraničí	k1gNnSc2	pohraničí
Česka	Česko	k1gNnSc2	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
hrubém	hrubý	k2eAgNnSc6d1	hrubé
členění	členění	k1gNnSc6	členění
pohraničí	pohraničí	k1gNnSc2	pohraničí
ČR	ČR	kA	ČR
lze	lze	k6eAd1	lze
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
česko-rakouském	českoakouský	k2eAgNnSc6d1	česko-rakouské
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
s	s	k7c7	s
RR	RR	kA	RR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česko-německém	českoěmecký	k2eAgNnSc6d1	česko-německé
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
se	s	k7c7	s
SRN	SRN	kA	SRN
<g/>
;	;	kIx,	;
resp.	resp.	kA	resp.
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
historickému	historický	k2eAgInSc3d1	historický
vývoji	vývoj	k1gInSc3	vývoj
o	o	k7c6	o
česko-bavorském	českoavorský	k2eAgNnSc6d1	česko-bavorské
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
s	s	k7c7	s
NSR	NSR	kA	NSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
česko-saském	českoaský	k2eAgNnSc6d1	česko-saský
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
s	s	k7c7	s
býv.	býv.	k?	býv.
NDR	NDR	kA	NDR
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
česko-polském	českoolský	k2eAgNnSc6d1	česko-polské
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
s	s	k7c7	s
PR	pr	k0	pr
<g/>
)	)	kIx)	)
a	a	k8xC	a
česko-slovenském	českolovenský	k2eAgNnSc6d1	česko-slovenské
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
se	s	k7c7	s
SR	SR	kA	SR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgMnSc1d1	sociální
geograf	geograf	k1gMnSc1	geograf
Martin	Martin	k1gMnSc1	Martin
Hampl	Hampl	k1gMnSc1	Hampl
definuje	definovat	k5eAaBmIp3nS	definovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dosažitelné	dosažitelný	k2eAgFnSc2d1	dosažitelná
datové	datový	k2eAgFnSc2d1	datová
základny	základna	k1gFnSc2	základna
pohraničí	pohraničí	k1gNnSc2	pohraničí
Česka	Česko	k1gNnSc2	Česko
jako	jako	k8xS	jako
soubor	soubor	k1gInSc1	soubor
okresů	okres	k1gInPc2	okres
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Pohraniční	pohraniční	k2eAgInPc4d1	pohraniční
regiony	region	k1gInPc4	region
člení	členit	k5eAaImIp3nS	členit
Martin	Martin	k1gMnSc1	Martin
Hampl	Hampl	k1gMnSc1	Hampl
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
regiony	region	k1gInPc1	region
funkčního	funkční	k2eAgInSc2d1	funkční
(	(	kIx(	(
<g/>
nodálního	nodální	k2eAgInSc2d1	nodální
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
regionálními	regionální	k2eAgInPc7d1	regionální
středisky	středisko	k1gNnPc7	středisko
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgNnSc1d1	související
vyšší	vysoký	k2eAgFnSc7d2	vyšší
intenzitou	intenzita	k1gFnSc7	intenzita
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
industrializace	industrializace	k1gFnSc2	industrializace
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc2	urbanizace
(	(	kIx(	(
<g/>
regiony	region	k1gInPc1	region
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
přibližné	přibližný	k2eAgFnSc2d1	přibližná
střediskové	střediskový	k2eAgFnSc2d1	středisková
působnosti	působnost	k1gFnSc2	působnost
pěti	pět	k4xCc2	pět
center	centrum	k1gNnPc2	centrum
mezoregionálního	mezoregionální	k2eAgInSc2d1	mezoregionální
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
současně	současně	k6eAd1	současně
center	centrum	k1gNnPc2	centrum
krajů	kraj	k1gInPc2	kraj
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
regiony	region	k1gInPc1	region
homogenního	homogenní	k2eAgInSc2d1	homogenní
(	(	kIx(	(
<g/>
zonálního	zonální	k2eAgInSc2d1	zonální
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
periferní	periferní	k2eAgFnSc2d1	periferní
zóny	zóna	k1gFnSc2	zóna
s	s	k7c7	s
jasným	jasný	k2eAgInSc7d1	jasný
spádem	spád	k1gInSc7	spád
k	k	k7c3	k
vnějším	vnější	k2eAgFnPc3d1	vnější
–	–	k?	–
vnitrozemským	vnitrozemský	k2eAgFnPc3d1	vnitrozemská
–	–	k?	–
centrům	centrum	k1gNnPc3	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
regiony	region	k1gInPc1	region
funkčního	funkční	k2eAgInSc2d1	funkční
(	(	kIx(	(
<g/>
nodálního	nodální	k2eAgInSc2d1	nodální
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Karlovarsko	Karlovarsko	k1gNnSc1	Karlovarsko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Cheb	Cheb	k1gInSc1	Cheb
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústecko	Ústecko	k1gNnSc1	Ústecko
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
severočeská	severočeský	k2eAgFnSc1d1	Severočeská
pánevní	pánevní	k2eAgFnSc1d1	pánevní
konurbace	konurbace	k1gFnSc1	konurbace
<g/>
;	;	kIx,	;
okres	okres	k1gInSc1	okres
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Liberecko	Liberecko	k1gNnSc1	Liberecko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Semily	Semily	k1gInPc1	Semily
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ostravsko	Ostravsko	k1gNnSc1	Ostravsko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Ostrava-město	Ostravaěsta	k1gMnSc5	Ostrava-města
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlínsko	Zlínsko	k1gNnSc1	Zlínsko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Hodonín	Hodonín	k1gInSc4	Hodonín
<g/>
,	,	kIx,	,
okres	okres	k1gInSc4	okres
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
regiony	region	k1gInPc1	region
homogenního	homogenní	k2eAgInSc2d1	homogenní
(	(	kIx(	(
<g/>
zonálního	zonální	k2eAgInSc2d1	zonální
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Šumava	Šumava	k1gFnSc1	Šumava
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Tachov	Tachov	k1gInSc1	Tachov
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Klatovy	Klatovy	k1gInPc4	Klatovy
<g/>
,	,	kIx,	,
okres	okres	k1gInSc4	okres
Prachatice	Prachatice	k1gFnPc4	Prachatice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jih	jih	k1gInSc1	jih
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Severovýchod	severovýchod	k1gInSc1	severovýchod
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jesenicko	Jesenicko	k1gNnSc1	Jesenicko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
)	)	kIx)	)
<g/>
Podle	podle	k7c2	podle
Martina	Martin	k1gMnSc2	Martin
Hampla	Hampl	k1gMnSc2	Hampl
má	mít	k5eAaImIp3nS	mít
okres	okres	k1gInSc1	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
i	i	k9	i
přes	přes	k7c4	přes
polohu	poloha	k1gFnSc4	poloha
u	u	k7c2	u
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
převážně	převážně	k6eAd1	převážně
"	"	kIx"	"
<g/>
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
<g/>
"	"	kIx"	"
charakter	charakter	k1gInSc4	charakter
jak	jak	k8xC	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
stability	stabilita	k1gFnSc2	stabilita
osídlení	osídlení	k1gNnSc2	osídlení
a	a	k8xC	a
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
tendencí	tendence	k1gFnPc2	tendence
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nevýznamné	významný	k2eNgFnSc2d1	nevýznamná
úlohy	úloha	k1gFnSc2	úloha
"	"	kIx"	"
<g/>
pravého	pravý	k2eAgNnSc2d1	pravé
<g/>
"	"	kIx"	"
–	–	k?	–
poválečně	poválečně	k6eAd1	poválečně
znovu	znovu	k6eAd1	znovu
osídlovaného	osídlovaný	k2eAgNnSc2d1	osídlované
–	–	k?	–
pohraničí	pohraničí	k1gNnSc2	pohraničí
(	(	kIx(	(
<g/>
Novohradska	Novohradsko	k1gNnSc2	Novohradsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
hranice	hranice	k1gFnSc1	hranice
</s>
</p>
<p>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
</s>
</p>
<p>
<s>
Euroregion	euroregion	k1gInSc1	euroregion
</s>
</p>
<p>
<s>
Periferie	periferie	k1gFnSc1	periferie
</s>
</p>
<p>
<s>
Pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
stráž	stráž	k1gFnSc1	stráž
</s>
</p>
<p>
<s>
Sudety	Sudety	k1gFnPc1	Sudety
</s>
</p>
<p>
<s>
Železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
</s>
</p>
<p>
<s>
Hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
</s>
</p>
<p>
<s>
Zahraničí	zahraničí	k1gNnSc1	zahraničí
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pohraničí	pohraničí	k1gNnSc2	pohraničí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Pohraničí	pohraničí	k1gNnSc2	pohraničí
ve	v	k7c6	v
WikicitátechPříhraničí	WikicitátechPříhraničí	k2eAgFnSc6d1	WikicitátechPříhraničí
a	a	k8xC	a
pohraničí	pohraničí	k1gNnSc6	pohraničí
</s>
</p>
<p>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
analýza	analýza	k1gFnSc1	analýza
pohraničí	pohraničí	k1gNnSc2	pohraničí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Česko-německé	českoěmecký	k2eAgNnSc1d1	česko-německé
pohraničí	pohraničí	k1gNnSc1	pohraničí
mezi	mezi	k7c7	mezi
minulostí	minulost	k1gFnSc7	minulost
a	a	k8xC	a
budoucností	budoucnost	k1gFnSc7	budoucnost
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
pohraničí	pohraničí	k1gNnSc1	pohraničí
po	po	k7c6	po
Schengenu	Schengen	k1gInSc6	Schengen
<g/>
:	:	kIx,	:
území	území	k1gNnSc6	území
svébytné	svébytný	k2eAgFnSc2d1	svébytná
<g/>
,	,	kIx,	,
oscilační	oscilační	k2eAgFnSc2d1	oscilační
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
tranzitní	tranzitní	k2eAgMnSc1d1	tranzitní
<g/>
?	?	kIx.	?
</s>
</p>
