<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
dálnic	dálnice	k1gFnPc2	dálnice
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
motorvei	motorvei	k6eAd1	motorvei
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dálnice	dálnice	k1gFnSc1	dálnice
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Norské	norský	k2eAgFnSc2d1	norská
dálnice	dálnice	k1gFnSc2	dálnice
nemají	mít	k5eNaImIp3nP	mít
vlastní	vlastní	k2eAgNnPc4d1	vlastní
vnitrostátní	vnitrostátní	k2eAgNnPc4d1	vnitrostátní
číslování	číslování	k1gNnPc4	číslování
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
označení	označení	k1gNnSc4	označení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
čísla	číslo	k1gNnPc1	číslo
evropských	evropský	k2eAgFnPc2d1	Evropská
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
norských	norský	k2eAgFnPc2d1	norská
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
víceproudé	víceproudý	k2eAgFnPc1d1	víceproudá
silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
na	na	k7c6	na
nejvytíženějších	vytížený	k2eAgInPc6d3	nejvytíženější
tazích	tag	k1gInPc6	tag
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
rozlišované	rozlišovaný	k2eAgInPc1d1	rozlišovaný
od	od	k7c2	od
dvouproudých	dvouproudý	k2eAgInPc2d1	dvouproudý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
dálnic	dálnice	k1gFnPc2	dálnice
(	(	kIx(	(
<g/>
vyhovujících	vyhovující	k2eAgFnPc2d1	vyhovující
statusu	status	k1gInSc6	status
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
cca	cca	kA	cca
468	[number]	k4	468
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
norských	norský	k2eAgFnPc6d1	norská
dálnicích	dálnice	k1gFnPc6	dálnice
je	být	k5eAaImIp3nS	být
110	[number]	k4	110
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
stanovená	stanovený	k2eAgFnSc1d1	stanovená
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
je	být	k5eAaImIp3nS	být
limit	limit	k1gInSc1	limit
max	max	kA	max
<g/>
.	.	kIx.	.
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
nebo	nebo	k8xC	nebo
90	[number]	k4	90
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
<s>
Dálniční	dálniční	k2eAgInPc1d1	dálniční
poplatky	poplatek	k1gInPc1	poplatek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
vybírány	vybírat	k5eAaImNgInP	vybírat
na	na	k7c6	na
mýtných	mýtné	k1gNnPc6	mýtné
branách	brána	k1gFnPc6	brána
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bezhotovostně	bezhotovostně	k6eAd1	bezhotovostně
<g/>
,	,	kIx,	,
platba	platba	k1gFnSc1	platba
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
speciální	speciální	k2eAgFnPc4d1	speciální
karty	karta	k1gFnPc4	karta
Visitors	Visitorsa	k1gFnPc2	Visitorsa
Payment	Payment	k1gMnSc1	Payment
contract	contract	k1gMnSc1	contract
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
dálnic	dálnice	k1gFnPc2	dálnice
v	v	k7c6	v
dob	doba	k1gFnPc2	doba
kratší	krátký	k2eAgFnSc2d2	kratší
než	než	k8xS	než
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
OnBoard	OnBoard	k1gMnSc1	OnBoard
unit	unit	k1gMnSc1	unit
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
místní	místní	k2eAgNnSc4d1	místní
<g/>
,	,	kIx,	,
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
dálnic	dálnice	k1gFnPc2	dálnice
v	v	k7c6	v
době	doba	k1gFnSc6	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
dálnic	dálnice	k1gFnPc2	dálnice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Evropské	evropský	k2eAgFnSc2d1	Evropská
silnice	silnice	k1gFnSc2	silnice
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgFnSc1d1	národní
silnice	silnice	k1gFnSc1	silnice
(	(	kIx(	(
<g/>
Riksvei	Riksvei	k1gNnSc1	Riksvei
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Regionální	regionální	k2eAgFnSc1d1	regionální
silnice	silnice	k1gFnSc1	silnice
(	(	kIx(	(
<g/>
Fylkesvei	Fylkesvei	k1gNnSc1	Fylkesvei
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Liste	list	k1gInSc5	list
over	over	k1gMnSc1	over
norske	norske	k1gNnSc2	norske
motorveier	motorveier	k1gMnSc1	motorveier
og	og	k?	og
motortrafikkveier	motortrafikkveier	k1gMnSc1	motortrafikkveier
na	na	k7c6	na
norské	norský	k2eAgFnSc6d1	norská
(	(	kIx(	(
<g/>
bokmå	bokmå	k?	bokmå
<g/>
)	)	kIx)	)
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
