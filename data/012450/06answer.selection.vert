<s>
Dálniční	dálniční	k2eAgInPc1d1	dálniční
poplatky	poplatek	k1gInPc1	poplatek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
vybírány	vybírat	k5eAaImNgInP	vybírat
na	na	k7c6	na
mýtných	mýtné	k1gNnPc6	mýtné
branách	brána	k1gFnPc6	brána
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bezhotovostně	bezhotovostně	k6eAd1	bezhotovostně
<g/>
,	,	kIx,	,
platba	platba	k1gFnSc1	platba
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
speciální	speciální	k2eAgFnPc4d1	speciální
karty	karta	k1gFnPc4	karta
Visitors	Visitorsa	k1gFnPc2	Visitorsa
Payment	Payment	k1gMnSc1	Payment
contract	contract	k1gMnSc1	contract
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
dálnic	dálnice	k1gFnPc2	dálnice
v	v	k7c6	v
dob	doba	k1gFnPc2	doba
kratší	krátký	k2eAgFnSc2d2	kratší
než	než	k8xS	než
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
OnBoard	OnBoard	k1gMnSc1	OnBoard
unit	unit	k1gMnSc1	unit
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
místní	místní	k2eAgNnSc4d1	místní
<g/>
,	,	kIx,	,
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
dálnic	dálnice	k1gFnPc2	dálnice
v	v	k7c6	v
době	doba	k1gFnSc6	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
