<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
je	být	k5eAaImIp3nS	být
anorganická	anorganický	k2eAgFnSc1d1	anorganická
sloučenina	sloučenina	k1gFnSc1	sloučenina
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
zinku	zinek	k1gInSc2	zinek
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
ZnCl	ZnCl	k1gInSc1	ZnCl
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
devíti	devět	k4xCc6	devět
známých	známý	k2eAgFnPc6d1	známá
krystalových	krystalový	k2eAgFnPc6d1	krystalová
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
nebo	nebo	k8xC	nebo
bílou	bílý	k2eAgFnSc4d1	bílá
látku	látka	k1gFnSc4	látka
dobře	dobře	k6eAd1	dobře
rozpustnou	rozpustný	k2eAgFnSc4d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
ZnCl	ZnCl	k1gInSc1	ZnCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
hygroskopický	hygroskopický	k2eAgMnSc1d1	hygroskopický
a	a	k8xC	a
navlhavý	navlhavý	k2eAgMnSc1d1	navlhavý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
škále	škála	k1gFnSc6	škála
aplikací	aplikace	k1gFnPc2	aplikace
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
textilu	textil	k1gInSc2	textil
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tavidlo	tavidlo	k1gNnSc1	tavidlo
a	a	k8xC	a
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
žádný	žádný	k3yNgInSc4	žádný
minerál	minerál	k1gInSc4	minerál
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc4d1	vzácný
nerost	nerost	k1gInSc4	nerost
simonkolleit	simonkolleit	k2eAgInSc4d1	simonkolleit
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
Zn	zn	kA	zn
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
8	[number]	k4	8
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
dráždí	dráždit	k5eAaImIp3nS	dráždit
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
dýchací	dýchací	k2eAgNnSc4d1	dýchací
ústrojí	ústrojí	k1gNnSc4	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
bezvodému	bezvodý	k2eAgInSc3d1	bezvodý
ZnCl	ZnCla	k1gFnPc2	ZnCla
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
bezvodé	bezvodý	k2eAgFnPc4d1	bezvodá
halogenidy	halogenida	k1gFnPc4	halogenida
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jejich	jejich	k3xOp3gNnSc2	jejich
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
exotermická	exotermický	k2eAgFnSc1d1	exotermická
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrované	koncentrovaný	k2eAgInPc1d1	koncentrovaný
roztoky	roztok	k1gInPc1	roztok
jsou	být	k5eAaImIp3nP	být
kyselé	kyselý	k2eAgInPc1d1	kyselý
a	a	k8xC	a
žíravé	žíravý	k2eAgInPc1d1	žíravý
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
Lewisova	Lewisův	k2eAgFnSc1d1	Lewisova
kyselina	kyselina	k1gFnSc1	kyselina
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
celulózu	celulóza	k1gFnSc4	celulóza
a	a	k8xC	a
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zinc	Zinc	k1gFnSc1	Zinc
chloride	chlorid	k1gInSc5	chlorid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc4	Earnshaw
<g/>
,	,	kIx,	,
Chemistry	Chemistr	k1gMnPc4	Chemistr
of	of	k?	of
the	the	k?	the
Elements	Elementsa	k1gFnPc2	Elementsa
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
nd	nd	k?	nd
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Butterworth-Heinemann	Butterworth-Heinemann	k1gInSc1	Butterworth-Heinemann
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
UK	UK	kA	UK
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Handbook	handbook	k1gInSc1	handbook
of	of	k?	of
Chemistry	Chemistr	k1gMnPc4	Chemistr
and	and	k?	and
Physics	Physicsa	k1gFnPc2	Physicsa
<g/>
,	,	kIx,	,
71	[number]	k4	71
<g/>
st	st	kA	st
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
CRC	CRC	kA	CRC
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Ann	Ann	k1gFnSc1	Ann
Arbor	Arbor	k1gInSc1	Arbor
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Merck	Merck	k1gInSc1	Merck
Index	index	k1gInSc1	index
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
th	th	k?	th
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
Merck	Merck	k1gMnSc1	Merck
&	&	k?	&
Co	co	k9	co
<g/>
,	,	kIx,	,
Rahway	Rahway	k1gInPc4	Rahway
<g/>
,	,	kIx,	,
New	New	k1gFnPc4	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
D.	D.	kA	D.
Nicholls	Nicholls	k1gInSc1	Nicholls
<g/>
,	,	kIx,	,
Complexes	Complexes	k1gInSc1	Complexes
and	and	k?	and
First-Row	First-Row	k1gFnSc1	First-Row
Transition	Transition	k1gInSc1	Transition
Elements	Elements	k1gInSc1	Elements
<g/>
,	,	kIx,	,
Macmillan	Macmillan	k1gInSc1	Macmillan
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
J.	J.	kA	J.
March	March	k1gMnSc1	March
<g/>
,	,	kIx,	,
Advanced	Advanced	k1gMnSc1	Advanced
Organic	Organice	k1gFnPc2	Organice
Chemistry	Chemistr	k1gMnPc7	Chemistr
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
th	th	k?	th
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
p.	p.	k?	p.
723	[number]	k4	723
<g/>
,	,	kIx,	,
Wiley	Wilea	k1gFnPc1	Wilea
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
G.	G.	kA	G.
J.	J.	kA	J.
McGarvey	McGarvea	k1gMnSc2	McGarvea
<g/>
,	,	kIx,	,
in	in	k?	in
Handbook	handbook	k1gInSc1	handbook
of	of	k?	of
Reagents	Reagents	k1gInSc1	Reagents
for	forum	k1gNnPc2	forum
Organic	Organice	k1gFnPc2	Organice
Synthesis	Synthesis	k1gFnPc2	Synthesis
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
Reagents	Reagents	k1gInSc1	Reagents
<g/>
,	,	kIx,	,
Auxiliaries	Auxiliaries	k1gInSc1	Auxiliaries
and	and	k?	and
Catalysts	Catalysts	k1gInSc1	Catalysts
for	forum	k1gNnPc2	forum
C-C	C-C	k1gFnSc2	C-C
Bond	bond	k1gInSc1	bond
Formation	Formation	k1gInSc1	Formation
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
R.	R.	kA	R.
M.	M.	kA	M.
Coates	Coates	k1gMnSc1	Coates
<g/>
,	,	kIx,	,
S.	S.	kA	S.
E.	E.	kA	E.
Denmark	Denmark	k1gInSc1	Denmark
<g/>
,	,	kIx,	,
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
220	[number]	k4	220
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Wiley	Wilea	k1gFnPc1	Wilea
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chlorid	chlorid	k1gInSc1	chlorid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Grades	Grades	k1gInSc1	Grades
and	and	k?	and
Applications	Applications	k1gInSc1	Applications
of	of	k?	of
Zinc	Zinc	k1gInSc1	Zinc
Chloride	chlorid	k1gInSc5	chlorid
</s>
</p>
<p>
<s>
PubChem	PubCh	k1gInSc7	PubCh
ZnCl	ZnCl	k1gInSc4	ZnCl
<g/>
2	[number]	k4	2
summary	summara	k1gFnSc2	summara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
External	Externat	k5eAaImAgMnS	Externat
MSDS	MSDS	kA	MSDS
Data	datum	k1gNnSc2	datum
Sheet	Sheet	k1gInSc1	Sheet
<g/>
.	.	kIx.	.
</s>
</p>
