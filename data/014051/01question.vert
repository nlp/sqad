<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
nebo	nebo	k8xC
urychlovači	urychlovač	k1gInSc6
částic	částice	k1gFnPc2
s	s	k7c7
chemickou	chemický	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
Ds	Ds	k1gFnSc2
<g/>
?	?	kIx.
</s>