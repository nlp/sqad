<s>
Darmstadtium	Darmstadtium	k1gNnSc1
</s>
<s>
Darmstadtium	Darmstadtium	k1gNnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
9	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
platině	platina	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
281	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ds	Ds	k?
</s>
<s>
110	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Darmstadtium	Darmstadtium	k1gNnSc1
<g/>
,	,	kIx,
Ds	Ds	k1gFnSc1
<g/>
,	,	kIx,
110	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Darmstadtium	Darmstadtium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54083-77-1	54083-77-1	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
281,16	281,16	k4
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
9	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
platině	platina	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgFnSc3d1
pevné	pevný	k2eAgFnSc3d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pt	Pt	k?
<g/>
⋏	⋏	k?
</s>
<s>
Meitnerium	Meitnerium	k1gNnSc1
≺	≺	k?
<g/>
Ds	Ds	k1gMnSc2
<g/>
≻	≻	k?
Roentgenium	Roentgenium	k1gNnSc1
</s>
<s>
Darmstadtium	Darmstadtium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ds	Ds	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc4
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
nebo	nebo	k8xC
urychlovači	urychlovač	k1gInSc6
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Darmstadtium	Darmstadtium	k1gNnSc1
doposud	doposud	k6eAd1
nebylo	být	k5eNaImAgNnS
izolováno	izolovat	k5eAaBmNgNnS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
své	svůj	k3xOyFgFnSc2
polohy	poloha	k1gFnSc2
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
prvků	prvek	k1gInPc2
by	by	k9
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
mělo	mít	k5eAaImAgNnS
připomínat	připomínat	k5eAaImF
platinu	platina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
scéma	scéma	k1gFnSc1
rozpadu	rozpad	k1gInSc2
270	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
</s>
<s>
První	první	k4xOgFnSc4
přípravu	příprava	k1gFnSc4
prvku	prvek	k1gInSc2
s	s	k7c7
atomovým	atomový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
110	#num#	k4
oznámili	oznámit	k5eAaPmAgMnP
němečtí	německý	k2eAgMnPc1d1
fyzici	fyzik	k1gMnPc1
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
roku	rok	k1gInSc2
1994	#num#	k4
z	z	k7c2
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
těžkých	těžký	k2eAgInPc2d1
iontů	ion	k1gInPc2
v	v	k7c6
německém	německý	k2eAgInSc6d1
Darmstadtu	Darmstadt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bombardováním	bombardování	k1gNnSc7
izotopu	izotop	k1gInSc2
olova	olovo	k1gNnSc2
jádry	jádro	k1gNnPc7
atomu	atom	k1gInSc6
niklu	nikl	k1gInSc2
získali	získat	k5eAaPmAgMnP
izotop	izotop	k1gInSc4
269	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20882	#num#	k4
Pb	Pb	k1gFnSc1
+	+	kIx~
6228	#num#	k4
Ni	on	k3xPp3gFnSc4
→	→	k?
269110	#num#	k4
Ds	Ds	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
této	tento	k3xDgFnSc2
série	série	k1gFnSc2
experimentů	experiment	k1gInPc2
bylo	být	k5eAaImAgNnS
použito	použít	k5eAaPmNgNnS
i	i	k8xC
těžší	těžký	k2eAgNnSc1d2
jádro	jádro	k1gNnSc1
atomu	atom	k1gInSc2
niklu	nikl	k1gInSc2
s	s	k7c7
nukleonovým	nukleonův	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
64	#num#	k4
s	s	k7c7
výsledkem	výsledek	k1gInSc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kongresové	kongresový	k2eAgNnSc1d1
a	a	k8xC
vědecké	vědecký	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
v	v	k7c6
Darmstadtu	Darmstadt	k1gInSc6
</s>
<s>
20882	#num#	k4
Pb	Pb	k1gFnSc1
+	+	kIx~
6428	#num#	k4
Ni	on	k3xPp3gFnSc4
→	→	k?
271110	#num#	k4
Ds	Ds	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
Prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
podle	podle	k7c2
jména	jméno	k1gNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
vznikl	vzniknout	k5eAaPmAgInS
–	–	k?
německém	německý	k2eAgInSc6d1
Darmstadtu	Darmstadt	k1gInSc6
a	a	k8xC
zasedání	zasedání	k1gNnSc1
IUPAC	IUPAC	kA
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
toto	tento	k3xDgNnSc1
pojmenování	pojmenování	k1gNnSc1
schválilo	schválit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Doposud	doposud	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
15	#num#	k4
následujících	následující	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
darmstadtia	darmstadtium	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
izotopy	izotop	k1gInPc1
s	s	k7c7
nukleonovým	nukleonův	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
270	#num#	k4
<g/>
,	,	kIx,
271	#num#	k4
a	a	k8xC
281	#num#	k4
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
ve	v	k7c6
2	#num#	k4
izomerních	izomerní	k2eAgFnPc6d1
modifikacích	modifikace	k1gFnPc6
s	s	k7c7
různým	různý	k2eAgInSc7d1
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
IzotopRok	IzotopRok	k1gInSc1
objevuReakcePoločas	objevuReakcePoločasa	k1gFnPc2
přeměny	přeměna	k1gFnSc2
</s>
<s>
267	#num#	k4
<g/>
Ds	Ds	k1gFnSc1
<g/>
2,8	2,8	k4
μ	μ	k1gFnPc2
</s>
<s>
268	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
269	#num#	k4
<g/>
Ds	Ds	k1gFnSc1
<g/>
1994208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
62	#num#	k4
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
179	#num#	k4
μ	μ	k1gFnPc2
</s>
<s>
270	#num#	k4
<g/>
Dsg	Dsg	k1gMnSc1
<g/>
,	,	kIx,
<g/>
m	m	kA
<g/>
2000207	#num#	k4
<g/>
Pb	Pb	k1gFnPc1
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,10	0,10	k4
ms	ms	k?
a	a	k8xC
6	#num#	k4
ms	ms	k?
</s>
<s>
271	#num#	k4
<g/>
Dsg	Dsg	k1gMnSc1
<g/>
,	,	kIx,
<g/>
m	m	kA
<g/>
1994208	#num#	k4
<g/>
Pb	Pb	k1gFnPc1
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
1,63	1,63	k4
ms	ms	k?
a	a	k8xC
69	#num#	k4
ms	ms	k?
</s>
<s>
272	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
273	#num#	k4
<g/>
Ds	Ds	k1gFnSc1
<g/>
1996244	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
(	(	kIx(
<g/>
34	#num#	k4
<g/>
S	s	k7c7
<g/>
,5	,5	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
170	#num#	k4
μ	μ	k1gFnPc2
</s>
<s>
274	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
275	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
276	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
277	#num#	k4
<g/>
Ds	Ds	k1gFnSc1
<g/>
4,1	4,1	k4
ms	ms	k?
</s>
<s>
278	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
279	#num#	k4
<g/>
Ds	Ds	k1gFnSc1
<g/>
2002244	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,5	,5	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,18	0,18	k4
s	s	k7c7
</s>
<s>
280	#num#	k4
<g/>
Ds	Ds	k1gFnPc2
?	?	kIx.
</s>
<s>
281	#num#	k4
<g/>
aDs	aDs	k?
<g/>
1999244	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
13	#num#	k4
s	s	k7c7
</s>
<s>
281	#num#	k4
<g/>
bDs	bDs	k?
<g/>
1999244	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
9,6	9,6	k4
s	s	k7c7
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Hofmann	Hofmann	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
Production	Production	k1gInSc1
and	and	k?
decay	decaa	k1gFnSc2
of	of	k?
269110	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeitschrift	Zeitschrift	k2eAgInSc1d1
für	für	k?
Physik	Physik	k1gInSc1
a	a	k8xC
Hadrons	Hadrons	k1gInSc1
and	and	k?
Nuclei	Nucle	k1gFnSc2
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
350	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
277	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
BF	BF	kA
<g/>
0	#num#	k4
<g/>
1291181	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hofmann	Hofmann	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
Reports	Reports	k1gInSc1
on	on	k3xPp3gMnSc1
Progress	Progress	k1gInSc4
in	in	k?
Physics	Physics	k1gInSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
61	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
639	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
34	#num#	k4
<g/>
-	-	kIx~
<g/>
4885	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
darmstadtium	darmstadtium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
darmstadtium	darmstadtium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7578677-1	7578677-1	k4
</s>
