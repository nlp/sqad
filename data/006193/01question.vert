<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
síly	síla	k1gFnSc2	síla
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
?	?	kIx.	?
</s>
