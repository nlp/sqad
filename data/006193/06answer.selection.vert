<s>
Richterova	Richterův	k2eAgFnSc1d1	Richterova
stupnice	stupnice	k1gFnSc1	stupnice
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
místní	místní	k2eAgNnSc4d1	místní
magnitudo	magnitudo	k1gNnSc4	magnitudo
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
škála	škála	k1gFnSc1	škála
=	=	kIx~	=
měřítko	měřítko	k1gNnSc1	měřítko
<g/>
,	,	kIx,	,
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
jediné	jediný	k2eAgNnSc1d1	jediné
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
velikost	velikost	k1gFnSc1	velikost
(	(	kIx(	(
<g/>
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
