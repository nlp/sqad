<s>
Richterova	Richterův	k2eAgFnSc1d1	Richterova
stupnice	stupnice	k1gFnSc1	stupnice
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
místní	místní	k2eAgNnSc4d1	místní
magnitudo	magnitudo	k1gNnSc4	magnitudo
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
škála	škála	k1gFnSc1	škála
=	=	kIx~	=
měřítko	měřítko	k1gNnSc1	měřítko
<g/>
,	,	kIx,	,
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
jediné	jediný	k2eAgNnSc1d1	jediné
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
velikost	velikost	k1gFnSc1	velikost
(	(	kIx(	(
<g/>
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
stupnice	stupnice	k1gFnSc1	stupnice
o	o	k7c6	o
základu	základ	k1gInSc6	základ
10	[number]	k4	10
počítaná	počítaný	k2eAgFnSc1d1	počítaná
z	z	k7c2	z
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
amplitudy	amplituda	k1gFnSc2	amplituda
největšího	veliký	k2eAgInSc2d3	veliký
posunu	posun	k1gInSc2	posun
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
na	na	k7c6	na
seismografu	seismograf	k1gInSc6	seismograf
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
amplitud	amplituda	k1gFnPc2	amplituda
způsobený	způsobený	k2eAgMnSc1d1	způsobený
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
epicentra	epicentrum	k1gNnSc2	epicentrum
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
od	od	k7c2	od
seismometru	seismometr	k1gInSc2	seismometr
je	být	k5eAaImIp3nS	být
opravený	opravený	k2eAgInSc4d1	opravený
odečtením	odečtení	k1gNnSc7	odečtení
logaritmu	logaritmus	k1gInSc2	logaritmus
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
amplitudy	amplituda	k1gFnSc2	amplituda
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
o	o	k7c6	o
místním	místní	k2eAgInSc6d1	místní
magnitudu	magnitud	k1gInSc6	magnitud
0	[number]	k4	0
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oprava	oprava	k1gFnSc1	oprava
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
používat	používat	k5eAaImF	používat
toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
jako	jako	k9	jako
absolutní	absolutní	k2eAgNnSc1d1	absolutní
měřítko	měřítko	k1gNnSc1	měřítko
pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Richterovu	Richterův	k2eAgFnSc4d1	Richterova
škálu	škála	k1gFnSc4	škála
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
Charles	Charles	k1gMnSc1	Charles
Richter	Richter	k1gMnSc1	Richter
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Benem	Ben	k1gInSc7	Ben
Gutenbergem	Gutenberg	k1gInSc7	Gutenberg
na	na	k7c4	na
California	Californium	k1gNnPc4	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
(	(	kIx(	(
<g/>
Caltech	Calto	k1gNnPc6	Calto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Škála	škála	k1gFnSc1	škála
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
používána	používat	k5eAaImNgNnP	používat
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
kalifornských	kalifornský	k2eAgNnPc2d1	kalifornské
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
analýzou	analýza	k1gFnSc7	analýza
seismogramů	seismogram	k1gInPc2	seismogram
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
Woodovým-Andersonovým	Woodovým-Andersonův	k2eAgInSc7d1	Woodovým-Andersonův
torzním	torzní	k2eAgInSc7d1	torzní
seismometrem	seismometr	k1gInSc7	seismometr
<g/>
.	.	kIx.	.
</s>
<s>
Richter	Richter	k1gMnSc1	Richter
původně	původně	k6eAd1	původně
zaokrouhloval	zaokrouhlovat	k5eAaImAgMnS	zaokrouhlovat
naměřené	naměřený	k2eAgFnSc2d1	naměřená
hodnoty	hodnota	k1gFnSc2	hodnota
k	k	k7c3	k
nejbližším	blízký	k2eAgFnPc3d3	nejbližší
čtvrtinám	čtvrtina	k1gFnPc3	čtvrtina
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
používat	používat	k5eAaImF	používat
desetinná	desetinný	k2eAgNnPc1d1	desetinné
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Motivací	motivace	k1gFnSc7	motivace
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
této	tento	k3xDgFnSc2	tento
škály	škála	k1gFnSc2	škála
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
oddělení	oddělení	k1gNnSc6	oddělení
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
menších	malý	k2eAgNnPc2d2	menší
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
od	od	k7c2	od
několika	několik	k4yIc2	několik
větších	veliký	k2eAgNnPc2d2	veliký
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
byla	být	k5eAaImAgFnS	být
škála	škála	k1gFnSc1	škála
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
velikostí	velikost	k1gFnPc2	velikost
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c4	v
astronomii	astronomie	k1gFnSc4	astronomie
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
jasnosti	jasnost	k1gFnSc2	jasnost
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Richter	Richter	k1gMnSc1	Richter
vybral	vybrat	k5eAaPmAgMnS	vybrat
za	za	k7c4	za
magnitudo	magnitudo	k1gNnSc4	magnitudo
0	[number]	k4	0
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
na	na	k7c6	na
seismogramu	seismogram	k1gInSc6	seismogram
zaznamenaném	zaznamenaný	k2eAgInSc6d1	zaznamenaný
Woodovým-Andersonovým	Woodovým-Andersonův	k2eAgInSc7d1	Woodovým-Andersonův
torzním	torzní	k2eAgInSc7d1	torzní
seismometrem	seismometr	k1gInSc7	seismometr
umístěném	umístěný	k2eAgInSc6d1	umístěný
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
epicentra	epicentrum	k1gNnSc2	epicentrum
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
ukázala	ukázat	k5eAaPmAgFnS	ukázat
maximální	maximální	k2eAgInSc4d1	maximální
vodorovný	vodorovný	k2eAgInSc4d1	vodorovný
posun	posun	k1gInSc4	posun
1	[number]	k4	1
mikrometr	mikrometr	k1gInSc4	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Richter	Richter	k1gMnSc1	Richter
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
nebudou	být	k5eNaImBp3nP	být
zaznamenávána	zaznamenáván	k2eAgNnPc1d1	zaznamenáváno
záporná	záporný	k2eAgNnPc1d1	záporné
magnituda	magnitudo	k1gNnPc1	magnitudo
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Richterova	Richterův	k2eAgFnSc1d1	Richterova
škála	škála	k1gFnSc1	škála
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
horní	horní	k2eAgFnSc4d1	horní
nebo	nebo	k8xC	nebo
dolní	dolní	k2eAgFnSc4d1	dolní
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
citlivé	citlivý	k2eAgInPc1d1	citlivý
seismografy	seismograf	k1gInPc1	seismograf
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
záchvěvy	záchvěv	k1gInPc1	záchvěv
s	s	k7c7	s
negativním	negativní	k2eAgInSc7d1	negativní
magnitudem	magnitud	k1gInSc7	magnitud
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
původní	původní	k2eAgInSc1d1	původní
Woodův-Andersonův	Woodův-Andersonův	k2eAgInSc1d1	Woodův-Andersonův
torzní	torzní	k2eAgInSc1d1	torzní
seismometr	seismometr	k1gInSc1	seismometr
používaný	používaný	k2eAgInSc1d1	používaný
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
škály	škála	k1gFnSc2	škála
má	mít	k5eAaImIp3nS	mít
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
limity	limit	k1gInPc4	limit
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
silnější	silný	k2eAgFnSc2d2	silnější
než	než	k8xS	než
6,8	[number]	k4	6,8
nemohly	moct	k5eNaImAgInP	moct
být	být	k5eAaImF	být
z	z	k7c2	z
měření	měření	k1gNnSc2	měření
vypočítávány	vypočítáván	k2eAgFnPc4d1	vypočítávána
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vědců	vědec	k1gMnPc2	vědec
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
různá	různý	k2eAgNnPc4d1	různé
rozšíření	rozšíření	k1gNnSc3	rozšíření
této	tento	k3xDgFnSc2	tento
škály	škála	k1gFnSc2	škála
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
problém	problém	k1gInSc1	problém
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
škály	škála	k1gFnSc2	škála
je	být	k5eAaImIp3nS	být
nejednoznačný	jednoznačný	k2eNgInSc4d1	nejednoznačný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
charakteristikám	charakteristika	k1gFnPc3	charakteristika
zdroje	zdroj	k1gInSc2	zdroj
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
hodnot	hodnota	k1gFnPc2	hodnota
8,3	[number]	k4	8,3
až	až	k9	až
8,5	[number]	k4	8,5
nastává	nastávat	k5eAaImIp3nS	nastávat
saturační	saturační	k2eAgInSc1d1	saturační
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tradiční	tradiční	k2eAgFnPc4d1	tradiční
metody	metoda	k1gFnPc4	metoda
měření	měření	k1gNnSc2	měření
naleznou	naleznout	k5eAaPmIp3nP	naleznout
stejné	stejný	k2eAgNnSc4d1	stejné
magnitudo	magnitudo	k1gNnSc4	magnitudo
pro	pro	k7c4	pro
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
očividně	očividně	k6eAd1	očividně
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
velikostí	velikost	k1gFnPc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
seismologů	seismolog	k1gMnPc2	seismolog
shodla	shodnout	k5eAaBmAgFnS	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
škála	škála	k1gFnSc1	škála
je	být	k5eAaImIp3nS	být
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
fyzikálně	fyzikálně	k6eAd1	fyzikálně
smysluplnějším	smysluplný	k2eAgInSc7d2	smysluplnější
seismickým	seismický	k2eAgInSc7d1	seismický
momentem	moment	k1gInSc7	moment
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
bližší	blízký	k2eAgInSc4d2	bližší
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
fyzikálním	fyzikální	k2eAgInPc3d1	fyzikální
parametrům	parametr	k1gInPc3	parametr
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
seismolog	seismolog	k1gMnSc1	seismolog
Hiroo	Hiroo	k1gMnSc1	Hiroo
Kanamori	Kanamore	k1gFnSc4	Kanamore
<g/>
,	,	kIx,	,
také	také	k9	také
z	z	k7c2	z
Caltechu	Caltech	k1gInSc2	Caltech
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
momentovou	momentový	k2eAgFnSc4d1	momentová
škálu	škála	k1gFnSc4	škála
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
seismický	seismický	k2eAgInSc4d1	seismický
moment	moment	k1gInSc4	moment
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
tradičnímu	tradiční	k2eAgNnSc3d1	tradiční
měření	měření	k1gNnSc3	měření
síly	síla	k1gFnSc2	síla
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Magnituda	Magnituda	k1gFnSc1	Magnituda
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
zaměňována	zaměňovat	k5eAaImNgFnS	zaměňovat
s	s	k7c7	s
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Rossiho-Forelova	Rossiho-Forelův	k2eAgFnSc1d1	Rossiho-Forelův
a	a	k8xC	a
modifikovaná	modifikovaný	k2eAgFnSc1d1	modifikovaná
Mercalliho	Mercalli	k1gMnSc4	Mercalli
škála	škála	k1gFnSc1	škála
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
a	a	k8xC	a
nepopisuje	popisovat	k5eNaImIp3nS	popisovat
tak	tak	k6eAd1	tak
absolutní	absolutní	k2eAgFnSc4d1	absolutní
velikost	velikost	k1gFnSc4	velikost
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
4,5	[number]	k4	4,5
a	a	k8xC	a
více	hodně	k6eAd2	hodně
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
seismografy	seismograf	k1gInPc1	seismograf
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
popisuje	popisovat	k5eAaImIp3nS	popisovat
typické	typický	k2eAgInPc1d1	typický
účinky	účinek	k1gInPc1	účinek
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
různých	různý	k2eAgFnPc2d1	různá
magnitud	magnituda	k1gFnPc2	magnituda
blízko	blízko	k7c2	blízko
epicentra	epicentrum	k1gNnSc2	epicentrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
opatrností	opatrnost	k1gFnSc7	opatrnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
intenzita	intenzita	k1gFnSc1	intenzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
účinky	účinek	k1gInPc7	účinek
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
nezávisejí	záviset	k5eNaImIp3nP	záviset
jen	jen	k9	jen
na	na	k7c4	na
magnitudu	magnituda	k1gFnSc4	magnituda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
epicentra	epicentrum	k1gNnSc2	epicentrum
a	a	k8xC	a
geologických	geologický	k2eAgFnPc6d1	geologická
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
geologické	geologický	k2eAgFnPc1d1	geologická
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sedimentární	sedimentární	k2eAgFnSc2d1	sedimentární
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
pohyby	pohyb	k1gInPc1	pohyb
půdy	půda	k1gFnSc2	půda
zesílit	zesílit	k5eAaPmF	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
velká	velký	k2eAgNnPc1d1	velké
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
jsou	být	k5eAaImIp3nP	být
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgNnSc1d3	nejsilnější
zatím	zatím	k6eAd1	zatím
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
bylo	být	k5eAaImAgNnS	být
Velké	velký	k2eAgNnSc1d1	velké
chilské	chilský	k2eAgNnSc1d1	Chilské
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
sílu	síla	k1gFnSc4	síla
9,5	[number]	k4	9,5
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
škály	škála	k1gFnSc2	škála
(	(	kIx(	(
<g/>
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Richterova	Richterův	k2eAgFnSc1d1	Richterova
stupnice	stupnice	k1gFnSc1	stupnice
</s>
