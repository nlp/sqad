<p>
<s>
Andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Angels	Angels	k1gInSc1	Angels
&	&	k?	&
Demons	Demons	k1gInSc1	Demons
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Dana	Dana	k1gFnSc1	Dana
Browna	Browna	k1gFnSc1	Browna
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
publikována	publikován	k2eAgFnSc1d1	publikována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Robert	Robert	k1gMnSc1	Robert
Langdon	Langdon	k1gMnSc1	Langdon
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Brownových	Brownův	k2eAgInPc6d1	Brownův
románech	román	k1gInPc6	román
Šifře	šifra	k1gFnSc6	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Da	Da	k1gMnSc2	Da
Vinci	Vinca	k1gMnSc2	Vinca
Code	Cod	k1gMnSc2	Cod
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Ztraceném	ztracený	k2eAgInSc6d1	ztracený
symbolu	symbol	k1gInSc6	symbol
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Lost	Lost	k1gMnSc1	Lost
symbol	symbol	k1gInSc1	symbol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
příběh	příběh	k1gInSc4	příběh
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
se	s	k7c7	s
sektou	sekta	k1gFnSc7	sekta
Iluminátů	iluminát	k1gMnPc2	iluminát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
byl	být	k5eAaImAgInS	být
brutálně	brutálně	k6eAd1	brutálně
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
vědec	vědec	k1gMnSc1	vědec
Leonardo	Leonardo	k1gMnSc1	Leonardo
Vetra	Vetra	k1gMnSc1	Vetra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
si	se	k3xPyFc3	se
neodporují	odporovat	k5eNaImIp3nP	odporovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Vittorií	Vittorie	k1gFnPc2	Vittorie
zrekonstruovali	zrekonstruovat	k5eAaPmAgMnP	zrekonstruovat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
velký	velký	k2eAgInSc4d1	velký
třesk	třesk	k1gInSc4	třesk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
dokázali	dokázat	k5eAaPmAgMnP	dokázat
existenci	existence	k1gFnSc4	existence
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Sestrojili	sestrojit	k5eAaPmAgMnP	sestrojit
velmi	velmi	k6eAd1	velmi
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
antihmotu	antihmota	k1gFnSc4	antihmota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ukradena	ukrást	k5eAaPmNgFnS	ukrást
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Langdon	Langdon	k1gMnSc1	Langdon
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vraždu	vražda	k1gFnSc4	vražda
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
ilumináti	iluminát	k1gMnPc1	iluminát
<g/>
,	,	kIx,	,
tajné	tajný	k2eAgNnSc1d1	tajné
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
zahubit	zahubit	k5eAaPmF	zahubit
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilian	Maxmiliana	k1gFnPc2	Maxmiliana
Kohler	Kohler	k1gMnSc1	Kohler
(	(	kIx(	(
<g/>
šéf	šéf	k1gMnSc1	šéf
CERNu	CERNa	k1gFnSc4	CERNa
<g/>
)	)	kIx)	)
dostává	dostávat	k5eAaImIp3nS	dostávat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
antihmota	antihmota	k1gFnSc1	antihmota
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
nedávno	nedávno	k6eAd1	nedávno
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
právě	právě	k9	právě
koná	konat	k5eAaImIp3nS	konat
konkláve	konkláve	k1gNnSc1	konkláve
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připravuje	připravovat	k5eAaImIp3nS	připravovat
camerlengo	camerlengo	k6eAd1	camerlengo
(	(	kIx(	(
<g/>
papežův	papežův	k2eAgMnSc1d1	papežův
komorník	komorník	k1gMnSc1	komorník
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
papeže	papež	k1gMnSc2	papež
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
osoba	osoba	k1gFnSc1	osoba
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
kardinálové	kardinál	k1gMnPc1	kardinál
byli	být	k5eAaImAgMnP	být
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
konkláve	konkláve	k1gNnSc7	konkláve
uneseni	unesen	k2eAgMnPc1d1	unesen
ilumináty	iluminát	k1gMnPc7	iluminát
<g/>
,	,	kIx,	,
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
zabit	zabit	k2eAgInSc1d1	zabit
a	a	k8xC	a
ocejchován	ocejchován	k2eAgInSc1d1	ocejchován
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
prvků	prvek	k1gInPc2	prvek
-	-	kIx~	-
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Vittorie	Vittorie	k1gFnSc1	Vittorie
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
pátrání	pátrání	k1gNnSc2	pátrání
také	také	k6eAd1	také
unesena	unesen	k2eAgFnSc1d1	unesena
<g/>
.	.	kIx.	.
</s>
<s>
Stezka	stezka	k1gFnSc1	stezka
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
Andělském	andělský	k2eAgInSc6d1	andělský
hradu	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
údajné	údajný	k2eAgNnSc4d1	údajné
sídlo	sídlo	k1gNnSc4	sídlo
iluminátů	iluminát	k1gMnPc2	iluminát
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Vittorii	Vittorie	k1gFnSc3	Vittorie
zachrání	zachránit	k5eAaPmIp3nS	zachránit
a	a	k8xC	a
přemůžou	přemoct	k5eAaPmIp3nPwO	přemoct
asasína	asasín	k1gMnSc4	asasín
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
vraždy	vražda	k1gFnSc2	vražda
Vittoriina	Vittoriin	k2eAgMnSc2d1	Vittoriin
otce	otec	k1gMnSc2	otec
Leonarda	Leonardo	k1gMnSc2	Leonardo
i	i	k8xC	i
čtyř	čtyři	k4xCgInPc2	čtyři
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Asasín	Asasín	k1gMnSc1	Asasín
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
příkazy	příkaz	k1gInPc4	příkaz
velitele	velitel	k1gMnSc2	velitel
Januse	Janus	k1gMnSc2	Janus
<g/>
.	.	kIx.	.
</s>
<s>
Camerlengo	Camerlengo	k6eAd1	Camerlengo
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
hruď	hruď	k1gFnSc4	hruď
vypálen	vypálen	k2eAgInSc4d1	vypálen
poslední	poslední	k2eAgInSc4d1	poslední
iluminátský	iluminátský	k2eAgInSc4d1	iluminátský
cejch	cejch	k1gInSc4	cejch
<g/>
.	.	kIx.	.
</s>
<s>
Kohler	Kohler	k1gMnSc1	Kohler
je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
dá	dát	k5eAaPmIp3nS	dát
Langdonovi	Langdon	k1gMnSc3	Langdon
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
natočil	natočit	k5eAaBmAgInS	natočit
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
camerlengem	camerleng	k1gInSc7	camerleng
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
výbuchu	výbuch	k1gInSc2	výbuch
antihmoty	antihmota	k1gFnSc2	antihmota
zbývá	zbývat	k5eAaImIp3nS	zbývat
jenom	jenom	k9	jenom
pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Camerlengo	Camerlengo	k6eAd1	Camerlengo
dostává	dostávat	k5eAaImIp3nS	dostávat
vnuknutí	vnuknutí	k1gNnSc4	vnuknutí
<g/>
,	,	kIx,	,
běží	běžet	k5eAaImIp3nS	běžet
do	do	k7c2	do
nekropole	nekropole	k1gFnSc2	nekropole
pod	pod	k7c7	pod
bazilikou	bazilika	k1gFnSc7	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
na	na	k7c6	na
hrobě	hrob	k1gInSc6	hrob
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
kontejner	kontejner	k1gInSc4	kontejner
s	s	k7c7	s
antihmotou	antihmota	k1gFnSc7	antihmota
<g/>
.	.	kIx.	.
</s>
<s>
Běží	běžet	k5eAaImIp3nS	běžet
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
helikoptéry	helikoptéra	k1gFnSc2	helikoptéra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
antihmota	antihmota	k1gFnSc1	antihmota
vybuchne	vybuchnout	k5eAaPmIp3nS	vybuchnout
Camerlengo	Camerlengo	k6eAd1	Camerlengo
se	se	k3xPyFc4	se
zachrání	zachránit	k5eAaPmIp3nP	zachránit
padákem	padák	k1gInSc7	padák
<g/>
.	.	kIx.	.
</s>
<s>
Langdon	Langdon	k1gMnSc1	Langdon
se	se	k3xPyFc4	se
z	z	k7c2	z
nahrávky	nahrávka	k1gFnSc2	nahrávka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
Kohler	Kohler	k1gInSc1	Kohler
<g/>
,	,	kIx,	,
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
camerlengo	camerlengo	k1gMnSc1	camerlengo
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
všech	všecek	k3xTgFnPc2	všecek
vražd	vražda	k1gFnPc2	vražda
i	i	k8xC	i
ztráty	ztráta	k1gFnSc2	ztráta
antihmoty	antihmota	k1gFnSc2	antihmota
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
donutit	donutit	k5eAaPmF	donutit
lidi	člověk	k1gMnPc4	člověk
více	hodně	k6eAd2	hodně
věřit	věřit	k5eAaImF	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
chtěl	chtít	k5eAaImAgMnS	chtít
z	z	k7c2	z
Iluminátů	iluminát	k1gMnPc2	iluminát
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
stvořit	stvořit	k5eAaPmF	stvořit
umělého	umělý	k2eAgMnSc4d1	umělý
nepřítele	nepřítel	k1gMnSc4	nepřítel
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Camerlengo	Camerlengo	k6eAd1	Camerlengo
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
za	za	k7c7	za
vraždou	vražda	k1gFnSc7	vražda
bývalého	bývalý	k2eAgMnSc2d1	bývalý
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zradil	zradit	k5eAaPmAgMnS	zradit
církev	církev	k1gFnSc4	církev
-	-	kIx~	-
měl	mít	k5eAaImAgInS	mít
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
pomocí	pomocí	k7c2	pomocí
umělého	umělý	k2eAgNnSc2d1	umělé
oplodnění	oplodnění	k1gNnSc2	oplodnění
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
papežovým	papežův	k2eAgNnSc7d1	papežovo
dítětem	dítě	k1gNnSc7	dítě
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
Camerlengo	Camerlengo	k1gMnSc1	Camerlengo
<g/>
.	.	kIx.	.
</s>
<s>
Camerlengo	Camerlengo	k6eAd1	Camerlengo
po	po	k7c6	po
prozrazení	prozrazení	k1gNnSc6	prozrazení
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Vyzrazení	vyzrazení	k1gNnSc1	vyzrazení
pravdy	pravda	k1gFnSc2	pravda
nechává	nechávat	k5eAaImIp3nS	nechávat
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
na	na	k7c6	na
Robertově	Robertův	k2eAgNnSc6d1	Robertovo
uvážení	uvážení	k1gNnSc6	uvážení
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
končí	končit	k5eAaImIp3nS	končit
s	s	k7c7	s
Vittorií	Vittorie	k1gFnSc7	Vittorie
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Langdon	Langdon	k1gMnSc1	Langdon
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
ilumináty	iluminát	k1gMnPc4	iluminát
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
symbolikou	symbolika	k1gFnSc7	symbolika
</s>
</p>
<p>
<s>
Vittorie	Vittorie	k1gFnSc1	Vittorie
Vetrová	Vetrová	k1gFnSc1	Vetrová
-	-	kIx~	-
vědkyně	vědkyně	k1gFnSc1	vědkyně
a	a	k8xC	a
bioložka	bioložka	k1gFnSc1	bioložka
<g/>
,	,	kIx,	,
zaměstnankyně	zaměstnankyně	k1gFnSc1	zaměstnankyně
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
</s>
</p>
<p>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
Vetra	Vetra	k1gMnSc1	Vetra
-	-	kIx~	-
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
<g/>
,	,	kIx,	,
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
otec	otec	k1gMnSc1	otec
Vittorie	Vittorie	k1gFnSc2	Vittorie
</s>
</p>
<p>
<s>
Maxmilian	Maxmiliana	k1gFnPc2	Maxmiliana
Köhler	Köhler	k1gMnSc1	Köhler
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
CERNu	CERNus	k1gInSc2	CERNus
(	(	kIx(	(
<g/>
ochrnutý	ochrnutý	k2eAgMnSc1d1	ochrnutý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Camerlengo	Camerlengo	k1gNnSc1	Camerlengo
-	-	kIx~	-
papežův	papežův	k2eAgMnSc1d1	papežův
svěřenec	svěřenec	k1gMnSc1	svěřenec
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
po	po	k7c6	po
papežově	papežův	k2eAgFnSc6d1	papežova
smrti	smrt	k1gFnSc6	smrt
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
osoba	osoba	k1gFnSc1	osoba
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Režíroval	režírovat	k5eAaImAgMnS	režírovat
Ron	Ron	k1gMnSc1	Ron
Howard	Howard	k1gMnSc1	Howard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanks	k1gInSc1	Hanks
<g/>
,	,	kIx,	,
Ewan	Ewan	k1gNnSc1	Ewan
McGregor	McGregora	k1gFnPc2	McGregora
a	a	k8xC	a
Ayelet	Ayelet	k1gInSc1	Ayelet
Zurer	Zurra	k1gFnPc2	Zurra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
www.andeleademoni.cz	www.andeleademoni.cz	k1gMnSc1	www.andeleademoni.cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
autora	autor	k1gMnSc2	autor
Dana	Dana	k1gFnSc1	Dana
Browna	Browna	k1gFnSc1	Browna
</s>
</p>
<p>
<s>
Andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
