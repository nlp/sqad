<p>
<s>
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
zachycena	zachytit	k5eAaPmNgFnS	zachytit
primárně	primárně	k6eAd1	primárně
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Tvorbou	tvorba	k1gFnSc7	tvorba
takových	takový	k3xDgInPc2	takový
obrazů	obraz	k1gInPc2	obraz
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
malíř	malíř	k1gMnSc1	malíř
specialista	specialista	k1gMnSc1	specialista
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
krajinář	krajinář	k1gMnSc1	krajinář
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
autonomní	autonomní	k2eAgNnSc1d1	autonomní
umělecké	umělecký	k2eAgNnSc1d1	umělecké
odvětví	odvětví	k1gNnSc1	odvětví
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
námětem	námět	k1gInSc7	námět
obrazu	obraz	k1gInSc2	obraz
samotná	samotný	k2eAgFnSc1d1	samotná
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
zrodila	zrodit	k5eAaPmAgFnS	zrodit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
silný	silný	k2eAgInSc1d1	silný
vliv	vliv	k1gInSc1	vliv
kalvínské	kalvínský	k2eAgFnSc2d1	kalvínská
reformace	reformace	k1gFnSc2	reformace
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kalvinismus	kalvinismus	k1gInSc1	kalvinismus
totiž	totiž	k9	totiž
striktně	striktně	k6eAd1	striktně
zapověděl	zapovědět	k5eAaPmAgInS	zapovědět
tvořit	tvořit	k5eAaImF	tvořit
náboženské	náboženský	k2eAgInPc4d1	náboženský
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
vždy	vždy	k6eAd1	vždy
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
-	-	kIx~	-
malíři	malíř	k1gMnPc1	malíř
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
museli	muset	k5eAaImAgMnP	muset
začít	začít	k5eAaPmF	začít
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nimž	jenž	k3xRgFnPc3	jenž
nemohlo	moct	k5eNaImAgNnS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
námitek	námitka	k1gFnPc2	námitka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
portrétů	portrét	k1gInPc2	portrét
to	ten	k3xDgNnSc4	ten
byly	být	k5eAaImAgInP	být
různé	různý	k2eAgInPc1d1	různý
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zátiší	zátiší	k1gNnSc1	zátiší
a	a	k8xC	a
právě	právě	k9	právě
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mimoevropském	mimoevropský	k2eAgNnSc6d1	mimoevropské
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
krajinářství	krajinářství	k1gNnSc1	krajinářství
pěstovalo	pěstovat	k5eAaImAgNnS	pěstovat
především	především	k9	především
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
a	a	k8xC	a
japonském	japonský	k2eAgNnSc6d1	Japonské
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
a	a	k8xC	a
nesmírně	smírně	k6eNd1	smírně
významnou	významný	k2eAgFnSc4d1	významná
tradici	tradice	k1gFnSc4	tradice
malby	malba	k1gFnSc2	malba
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
představitelé	představitel	k1gMnPc1	představitel
západní	západní	k2eAgFnSc2d1	západní
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zobrazení	zobrazení	k1gNnSc4	zobrazení
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
a	a	k8xC	a
starověku	starověk	k1gInSc6	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Malba	malba	k1gFnSc1	malba
krajiny	krajina	k1gFnSc2	krajina
nebyla	být	k5eNaImAgFnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
předmětem	předmět	k1gInSc7	předmět
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
zájmu	zájem	k1gInSc2	zájem
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravěkém	pravěký	k2eAgNnSc6d1	pravěké
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
také	také	k9	také
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
domorodých	domorodý	k2eAgMnPc2d1	domorodý
výtvarníků	výtvarník	k1gMnPc2	výtvarník
nehraje	hrát	k5eNaImIp3nS	hrát
zobrazení	zobrazení	k1gNnSc1	zobrazení
krajiny	krajina	k1gFnSc2	krajina
žádnou	žádný	k3yNgFnSc4	žádný
významnější	významný	k2eAgFnSc4d2	významnější
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c6	na
zobrazení	zobrazení	k1gNnSc6	zobrazení
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
malbách	malba	k1gFnPc6	malba
a	a	k8xC	a
reliéfech	reliéf	k1gInPc6	reliéf
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
také	také	k9	také
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
výjev	výjev	k1gInSc4	výjev
zasazen	zasazen	k2eAgInSc4d1	zasazen
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bylo	být	k5eAaImAgNnS	být
zobrazováno	zobrazovat	k5eAaImNgNnS	zobrazovat
pouze	pouze	k6eAd1	pouze
nejnutnější	nutný	k2eAgNnSc1d3	nejnutnější
okolí	okolí	k1gNnSc1	okolí
(	(	kIx(	(
<g/>
terén	terén	k1gInSc1	terén
<g/>
,	,	kIx,	,
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
krajinném	krajinný	k2eAgInSc6d1	krajinný
rámci	rámec	k1gInSc6	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
určitého	určitý	k2eAgInSc2d1	určitý
výjevu	výjev	k1gInSc2	výjev
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
veliký	veliký	k2eAgInSc1d1	veliký
úsek	úsek	k1gInSc1	úsek
krajiny	krajina	k1gFnSc2	krajina
nebo	nebo	k8xC	nebo
tato	tento	k3xDgFnSc1	tento
krajina	krajina	k1gFnSc1	krajina
celému	celý	k2eAgNnSc3d1	celé
vyobrazení	vyobrazení	k1gNnSc3	vyobrazení
dokonce	dokonce	k9	dokonce
dominuje	dominovat	k5eAaImIp3nS	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
například	například	k6eAd1	například
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
malbách	malba	k1gFnPc6	malba
z	z	k7c2	z
egyptských	egyptský	k2eAgNnPc2d1	egyptské
pohřebišť	pohřebiště	k1gNnPc2	pohřebiště
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dochovaných	dochovaný	k2eAgFnPc6d1	dochovaná
nástěnných	nástěnný	k2eAgFnPc6d1	nástěnná
malbách	malba	k1gFnPc6	malba
v	v	k7c6	v
Herculaneu	Herculaneus	k1gInSc6	Herculaneus
a	a	k8xC	a
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zobrazení	zobrazení	k1gNnSc4	zobrazení
krajiny	krajina	k1gFnSc2	krajina
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
renesanci	renesance	k1gFnSc3	renesance
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
raně	raně	k6eAd1	raně
středověkém	středověký	k2eAgNnSc6d1	středověké
malířství	malířství	k1gNnSc6	malířství
nebyla	být	k5eNaImAgFnS	být
krajina	krajina	k1gFnSc1	krajina
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
opět	opět	k6eAd1	opět
zachycována	zachycovat	k5eAaImNgNnP	zachycovat
zpravidla	zpravidla	k6eAd1	zpravidla
vůbec	vůbec	k9	vůbec
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgInS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
pouze	pouze	k6eAd1	pouze
nejnutnější	nutný	k2eAgInSc1d3	nejnutnější
krajinný	krajinný	k2eAgInSc1d1	krajinný
rámec	rámec	k1gInSc1	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
i	i	k8xC	i
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
měla	mít	k5eAaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
symbolický	symbolický	k2eAgInSc4d1	symbolický
<g/>
,	,	kIx,	,
ideový	ideový	k2eAgInSc4d1	ideový
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stromy	strom	k1gInPc1	strom
=	=	kIx~	=
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
=	=	kIx~	=
věčný	věčný	k2eAgInSc1d1	věčný
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
gotice	gotika	k1gFnSc6	gotika
ovšem	ovšem	k9	ovšem
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
intenzivnímu	intenzivní	k2eAgInSc3d1	intenzivní
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
zobrazení	zobrazení	k1gNnSc4	zobrazení
krajiny	krajina	k1gFnSc2	krajina
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
proměně	proměna	k1gFnSc3	proměna
dobového	dobový	k2eAgNnSc2d1	dobové
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
:	:	kIx,	:
svět	svět	k1gInSc1	svět
už	už	k6eAd1	už
nadále	nadále	k6eAd1	nadále
není	být	k5eNaImIp3nS	být
nazírán	nazírán	k2eAgMnSc1d1	nazírán
jako	jako	k8xC	jako
divoké	divoký	k2eAgNnSc1d1	divoké
<g/>
,	,	kIx,	,
životu	život	k1gInSc3	život
nepřátelské	přátelský	k2eNgNnSc1d1	nepřátelské
místo	místo	k1gNnSc1	místo
či	či	k8xC	či
jako	jako	k9	jako
pouhá	pouhý	k2eAgFnSc1d1	pouhá
přestupní	přestupní	k2eAgFnSc1d1	přestupní
stanice	stanice	k1gFnSc1	stanice
před	před	k7c7	před
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
Boží	boží	k2eAgFnSc4d1	boží
věčnost	věčnost	k1gFnSc4	věčnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
specifickými	specifický	k2eAgInPc7d1	specifický
půvaby	půvab	k1gInPc7	půvab
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
už	už	k6eAd1	už
na	na	k7c6	na
dílech	díl	k1gInPc6	díl
tzv.	tzv.	kA	tzv.
bratří	bratřit	k5eAaImIp3nS	bratřit
z	z	k7c2	z
Limburka	Limburek	k1gMnSc2	Limburek
(	(	kIx(	(
<g/>
Přebohaté	přebohatý	k2eAgFnPc1d1	přebohatá
hodinky	hodinka	k1gFnPc1	hodinka
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Berry	Berra	k1gFnSc2	Berra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Rogiera	Rogiero	k1gNnSc2	Rogiero
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Weyden	Weydna	k1gFnPc2	Weydna
nebo	nebo	k8xC	nebo
Jana	Jan	k1gMnSc2	Jan
van	vana	k1gFnPc2	vana
Eyck	Eyck	k1gMnSc1	Eyck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
vyslovené	vyslovený	k2eAgFnSc6d1	vyslovená
krajinomalbě	krajinomalba	k1gFnSc6	krajinomalba
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
hovořit	hovořit	k5eAaImF	hovořit
spíše	spíše	k9	spíše
až	až	k9	až
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
perspektivy	perspektiva	k1gFnSc2	perspektiva
a	a	k8xC	a
kompozice	kompozice	k1gFnSc2	kompozice
v	v	k7c6	v
renesančním	renesanční	k2eAgNnSc6d1	renesanční
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Pietera	Pieter	k1gMnSc4	Pieter
Brueghela	Brueghel	k1gMnSc4	Brueghel
staršího	starší	k1gMnSc4	starší
<g/>
,	,	kIx,	,
Hendricka	Hendricka	k1gFnSc1	Hendricka
Avercampa	Avercamp	k1gMnSc2	Avercamp
<g/>
,	,	kIx,	,
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
nebo	nebo	k8xC	nebo
v	v	k7c6	v
akvarelech	akvarel	k1gInPc6	akvarel
Albrechta	Albrecht	k1gMnSc4	Albrecht
Dürera	Dürer	k1gMnSc4	Dürer
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
malířů	malíř	k1gMnPc2	malíř
jde	jít	k5eAaImIp3nS	jít
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
o	o	k7c6	o
zobrazení	zobrazení	k1gNnSc6	zobrazení
morality	moralita	k1gFnSc2	moralita
<g/>
,	,	kIx,	,
přísloví	přísloví	k1gNnSc2	přísloví
či	či	k8xC	či
žánru	žánr	k1gInSc2	žánr
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
o	o	k7c4	o
pouhé	pouhý	k2eAgFnPc4d1	pouhá
studie	studie	k1gFnPc4	studie
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
topograficky	topograficky	k6eAd1	topograficky
přesně	přesně	k6eAd1	přesně
identifikovatelnou	identifikovatelný	k2eAgFnSc4d1	identifikovatelná
krajinu	krajina	k1gFnSc4	krajina
představuje	představovat	k5eAaImIp3nS	představovat
obraz	obraz	k1gInSc1	obraz
Zázračný	zázračný	k2eAgInSc1d1	zázračný
rybolov	rybolov	k1gInSc4	rybolov
od	od	k7c2	od
Konrada	Konrada	k1gFnSc1	Konrada
Witze	Witze	k1gFnSc1	Witze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1444	[number]	k4	1444
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
krajina	krajina	k1gFnSc1	krajina
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
prvkem	prvek	k1gInSc7	prvek
obrazu	obraz	k1gInSc2	obraz
se	se	k3xPyFc4	se
krajina	krajina	k1gFnSc1	krajina
stává	stávat	k5eAaImIp3nS	stávat
v	v	k7c6	v
deskových	deskový	k2eAgInPc6d1	deskový
obrazech	obraz	k1gInPc6	obraz
Albrechta	Albrecht	k1gMnSc2	Albrecht
Altdorfera	Altdorfer	k1gMnSc2	Altdorfer
a	a	k8xC	a
malířů	malíř	k1gMnPc2	malíř
tzv.	tzv.	kA	tzv.
Dunajské	dunajský	k2eAgFnSc2d1	Dunajská
školy	škola	k1gFnSc2	škola
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
pak	pak	k6eAd1	pak
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
obrazech	obraz	k1gInPc6	obraz
Benátčana	Benátčan	k1gMnSc4	Benátčan
Giorgioneho	Giorgione	k1gMnSc4	Giorgione
(	(	kIx(	(
<g/>
Bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
1515	[number]	k4	1515
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
jako	jako	k8xS	jako
žánr	žánr	k1gInSc1	žánr
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
počátkem	počátkem	k7c2	počátkem
sedmnáctého	sedmnáctý	k4xOgNnSc2	sedmnáctý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
období	období	k1gNnSc6	období
barokního	barokní	k2eAgNnSc2d1	barokní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
je	být	k5eAaImIp3nS	být
počítán	počítán	k2eAgMnSc1d1	počítán
Esaias	Esaias	k1gMnSc1	Esaias
van	vana	k1gFnPc2	vana
de	de	k?	de
Velde	Veld	k1gMnSc5	Veld
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
známými	známý	k2eAgMnPc7d1	známý
holandskými	holandský	k2eAgMnPc7d1	holandský
krajináři	krajinář	k1gMnPc7	krajinář
jsou	být	k5eAaImIp3nP	být
Jan	Jan	k1gMnSc1	Jan
van	vana	k1gFnPc2	vana
Goyen	Goyen	k1gInSc1	Goyen
<g/>
,	,	kIx,	,
Salomon	Salomon	k1gMnSc1	Salomon
van	vana	k1gFnPc2	vana
Ruysdael	Ruysdael	k1gMnSc1	Ruysdael
<g/>
,	,	kIx,	,
Hercules	Hercules	k1gMnSc1	Hercules
Seghers	Seghersa	k1gFnPc2	Seghersa
<g/>
,	,	kIx,	,
Jacob	Jacoba	k1gFnPc2	Jacoba
van	vana	k1gFnPc2	vana
Ruisdael	Ruisdael	k1gInSc1	Ruisdael
a	a	k8xC	a
Meindert	Meindert	k1gInSc1	Meindert
Hobbema	Hobbemum	k1gNnSc2	Hobbemum
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
malíři	malíř	k1gMnPc1	malíř
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
realističnost	realističnost	k1gFnSc4	realističnost
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
ji	on	k3xPp3gFnSc4	on
malovat	malovat	k5eAaImF	malovat
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
skutečně	skutečně	k6eAd1	skutečně
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
důvěrnosti	důvěrnost	k1gFnSc2	důvěrnost
a	a	k8xC	a
intimního	intimní	k2eAgInSc2d1	intimní
poklidu	poklid	k1gInSc2	poklid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
francouzští	francouzštit	k5eAaImIp3nP	francouzštit
barokní	barokní	k2eAgMnPc1d1	barokní
klasicisté	klasicista	k1gMnPc1	klasicista
malují	malovat	k5eAaImIp3nP	malovat
idealizovanou	idealizovaný	k2eAgFnSc4d1	idealizovaná
krajinu	krajina	k1gFnSc4	krajina
antikizující	antikizující	k2eAgFnSc2d1	antikizující
pastorální	pastorální	k2eAgFnSc2d1	pastorální
idyly	idyla	k1gFnSc2	idyla
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
malíři	malíř	k1gMnSc3	malíř
Nicolas	Nicolas	k1gMnSc1	Nicolas
Poussin	Poussin	k1gMnSc1	Poussin
(	(	kIx(	(
<g/>
maluje	malovat	k5eAaImIp3nS	malovat
heroické	heroický	k2eAgFnPc4d1	heroická
krajiny	krajina	k1gFnPc4	krajina
s	s	k7c7	s
mytologickými	mytologický	k2eAgInPc7d1	mytologický
nebo	nebo	k8xC	nebo
biblickými	biblický	k2eAgInPc7d1	biblický
výjevy	výjev	k1gInPc7	výjev
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Krajina	Krajina	k1gFnSc1	Krajina
s	s	k7c7	s
Orfeem	Orfeus	k1gMnSc7	Orfeus
a	a	k8xC	a
Eurydikou	Eurydika	k1gFnSc7	Eurydika
nebo	nebo	k8xC	nebo
slavný	slavný	k2eAgInSc1d1	slavný
obraz	obraz	k1gInSc1	obraz
Et	Et	k1gFnSc1	Et
in	in	k?	in
Arcadia	Arcadium	k1gNnSc2	Arcadium
Ego	ego	k1gNnSc4	ego
<g/>
)	)	kIx)	)
a	a	k8xC	a
Claude	Claud	k1gInSc5	Claud
Lorrain	Lorrain	k2eAgInSc4d1	Lorrain
(	(	kIx(	(
<g/>
ideální	ideální	k2eAgFnPc1d1	ideální
krajiny	krajina	k1gFnPc1	krajina
<g/>
,	,	kIx,	,
harmonické	harmonický	k2eAgFnPc1d1	harmonická
a	a	k8xC	a
racionálně	racionálně	k6eAd1	racionálně
komponované	komponovaný	k2eAgNnSc1d1	komponované
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
umělci	umělec	k1gMnPc1	umělec
vrací	vracet	k5eAaImIp3nP	vracet
k	k	k7c3	k
realistickému	realistický	k2eAgNnSc3d1	realistické
zobrazení	zobrazení	k1gNnSc3	zobrazení
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispěl	přispět	k5eAaPmAgMnS	přispět
John	John	k1gMnSc1	John
Constable	Constable	k1gMnSc1	Constable
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
bezprostřední	bezprostřední	k2eAgFnSc1d1	bezprostřední
studie	studie	k1gFnSc1	studie
např.	např.	kA	např.
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
věrné	věrný	k2eAgNnSc4d1	věrné
zachycení	zachycení	k1gNnSc4	zachycení
obrazového	obrazový	k2eAgInSc2d1	obrazový
dojmu	dojem	k1gInSc2	dojem
z	z	k7c2	z
neustálých	neustálý	k2eAgFnPc2d1	neustálá
proměn	proměna	k1gFnPc2	proměna
přírody	příroda	k1gFnSc2	příroda
výrazně	výrazně	k6eAd1	výrazně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
budoucí	budoucí	k2eAgFnPc4d1	budoucí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Malbou	malba	k1gFnSc7	malba
městské	městský	k2eAgFnSc2d1	městská
krajiny	krajina	k1gFnSc2	krajina
(	(	kIx(	(
<g/>
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
osmnáctém	osmnáctý	k4xOgInSc6	osmnáctý
století	století	k1gNnSc6	století
zabýval	zabývat	k5eAaImAgMnS	zabývat
a	a	k8xC	a
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Giovanni	Giovanň	k1gMnSc3	Giovanň
Antonio	Antonio	k1gMnSc1	Antonio
Canal	Canal	k1gMnSc1	Canal
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
Canaletto	Canaletto	k1gNnSc4	Canaletto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
romantismu	romantismus	k1gInSc2	romantismus
připadl	připadnout	k5eAaPmAgInS	připadnout
krajině	krajina	k1gFnSc3	krajina
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Rousseauových	Rousseauová	k1gFnPc2	Rousseauová
myšlenek	myšlenka	k1gFnPc2	myšlenka
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
až	až	k9	až
transcendentální	transcendentální	k2eAgInSc1d1	transcendentální
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
umělci	umělec	k1gMnPc1	umělec
prožívají	prožívat	k5eAaImIp3nP	prožívat
přírodu	příroda	k1gFnSc4	příroda
jako	jako	k9	jako
duchovně	duchovně	k6eAd1	duchovně
náboženský	náboženský	k2eAgInSc1d1	náboženský
zážitek	zážitek	k1gInSc1	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Příroda	příroda	k1gFnSc1	příroda
je	být	k5eAaImIp3nS	být
viděna	vidět	k5eAaImNgFnS	vidět
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
silného	silný	k2eAgNnSc2d1	silné
emocionálního	emocionální	k2eAgNnSc2d1	emocionální
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
senzibilitu	senzibilita	k1gFnSc4	senzibilita
<g/>
,	,	kIx,	,
jako	jako	k9	jako
u	u	k7c2	u
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
a	a	k8xC	a
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
působícího	působící	k2eAgMnSc2d1	působící
Caspara	Caspar	k1gMnSc2	Caspar
Davida	David	k1gMnSc2	David
Friedricha	Friedrich	k1gMnSc2	Friedrich
<g/>
;	;	kIx,	;
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
maluje	malovat	k5eAaImIp3nS	malovat
William	William	k1gInSc4	William
Turner	turner	k1gMnSc1	turner
dramatické	dramatický	k2eAgInPc4d1	dramatický
výjevy	výjev	k1gInPc4	výjev
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
živlů	živel	k1gInPc2	živel
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
zrakem	zrak	k1gInSc7	zrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
realistickém	realistický	k2eAgInSc6d1	realistický
krajinářství	krajinářství	k1gNnSc2	krajinářství
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
smyslové	smyslový	k2eAgNnSc4d1	smyslové
postižení	postižení	k1gNnSc4	postižení
krajiny	krajina	k1gFnSc2	krajina
vyjádřen	vyjádřit	k5eAaPmNgMnS	vyjádřit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
malíři	malíř	k1gMnPc1	malíř
začínají	začínat	k5eAaImIp3nP	začínat
malovat	malovat	k5eAaImF	malovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
neboli	neboli	k8xC	neboli
v	v	k7c6	v
plenéru	plenér	k1gInSc6	plenér
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
umělci	umělec	k1gMnPc1	umělec
tzv.	tzv.	kA	tzv.
Barbizonské	Barbizonský	k2eAgFnPc4d1	Barbizonská
školy	škola	k1gFnPc4	škola
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gMnPc3	on
blízký	blízký	k2eAgMnSc1d1	blízký
Francouz	Francouz	k1gMnSc1	Francouz
Camille	Camille	k1gNnSc2	Camille
Corot	Corot	k1gMnSc1	Corot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jejich	jejich	k3xOp3gNnSc4	jejich
realistické	realistický	k2eAgNnSc4d1	realistické
vidění	vidění	k1gNnSc4	vidění
krajiny	krajina	k1gFnSc2	krajina
někdy	někdy	k6eAd1	někdy
obohacuje	obohacovat	k5eAaImIp3nS	obohacovat
o	o	k7c4	o
alegoricko-mystické	alegorickoystický	k2eAgFnPc4d1	alegoricko-mystický
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
výjevy	výjev	k1gInPc4	výjev
<g/>
.	.	kIx.	.
</s>
<s>
Barbizonskou	Barbizonský	k2eAgFnSc7d1	Barbizonská
školou	škola	k1gFnSc7	škola
byli	být	k5eAaImAgMnP	být
inspirováni	inspirován	k2eAgMnPc1d1	inspirován
francouzští	francouzský	k2eAgMnPc1d1	francouzský
impresionisté	impresionista	k1gMnPc1	impresionista
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zde	zde	k6eAd1	zde
připomeňme	připomenout	k5eAaPmRp1nP	připomenout
zejména	zejména	k9	zejména
Clauda	Clauda	k1gFnSc1	Clauda
Moneta	moneta	k1gFnSc1	moneta
nebo	nebo	k8xC	nebo
Camille	Camille	k1gFnSc1	Camille
Pissarra	Pissarra	k1gFnSc1	Pissarra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dále	daleko	k6eAd2	daleko
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
krajinářský	krajinářský	k2eAgInSc4d1	krajinářský
odkaz	odkaz	k1gInSc4	odkaz
impresionismu	impresionismus	k1gInSc2	impresionismus
umělci	umělec	k1gMnPc1	umělec
jako	jako	k9	jako
Paul	Paul	k1gMnSc1	Paul
Cézanne	Cézann	k1gInSc5	Cézann
<g/>
,	,	kIx,	,
Vincent	Vincent	k1gMnSc1	Vincent
van	vana	k1gFnPc2	vana
Gogh	Gogh	k1gMnSc1	Gogh
nebo	nebo	k8xC	nebo
Georges	Georges	k1gMnSc1	Georges
Seurat	Seurat	k1gMnSc1	Seurat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
krajinu	krajina	k1gFnSc4	krajina
v	v	k7c4	v
symbol	symbol	k1gInSc4	symbol
např.	např.	kA	např.
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
expresionistů	expresionista	k1gMnPc2	expresionista
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Die	Die	k1gMnSc2	Die
Brücke	Brück	k1gMnSc2	Brück
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
škála	škála	k1gFnSc1	škála
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
tématem	téma	k1gNnSc7	téma
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
barevné	barevný	k2eAgFnPc1d1	barevná
studie	studie	k1gFnPc1	studie
fauvismu	fauvismus	k1gInSc2	fauvismus
nebo	nebo	k8xC	nebo
detailně	detailně	k6eAd1	detailně
propracované	propracovaný	k2eAgInPc1d1	propracovaný
výjevy	výjev	k1gInPc1	výjev
fotorealismu	fotorealismus	k1gInSc2	fotorealismus
<g/>
.	.	kIx.	.
</s>
<s>
Krajinou	Krajina	k1gFnSc7	Krajina
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fantaskní	fantaskní	k2eAgFnPc4d1	fantaskní
kompozice	kompozice	k1gFnPc4	kompozice
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
zájem	zájem	k1gInSc1	zájem
stoupá	stoupat	k5eAaImIp3nS	stoupat
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
krajiny	krajina	k1gFnSc2	krajina
industriální	industriální	k2eAgFnSc2d1	industriální
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
však	však	k9	však
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
krajinomalbu	krajinomalba	k1gFnSc4	krajinomalba
spíše	spíše	k9	spíše
opadl	opadnout	k5eAaPmAgMnS	opadnout
<g/>
,	,	kIx,	,
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
fotografie	fotografie	k1gFnSc1	fotografie
nebo	nebo	k8xC	nebo
land	land	k1gInSc1	land
art	art	k?	art
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
krajinářství	krajinářství	k1gNnSc1	krajinářství
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rudolfinského	rudolfinský	k2eAgInSc2d1	rudolfinský
manýrismu	manýrismus	k1gInSc2	manýrismus
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
barokními	barokní	k2eAgMnPc7d1	barokní
krajináři	krajinář	k1gMnPc7	krajinář
byli	být	k5eAaImAgMnP	být
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
Reiner	Reiner	k1gMnSc1	Reiner
<g/>
,	,	kIx,	,
malířem	malíř	k1gMnSc7	malíř
rokokových	rokokový	k2eAgFnPc2d1	rokoková
intimních	intimní	k2eAgFnPc2d1	intimní
krajinek	krajinka	k1gFnPc2	krajinka
Norbert	Norbert	k1gMnSc1	Norbert
Grund	Grund	k1gMnSc1	Grund
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
disciplína	disciplína	k1gFnSc1	disciplína
se	se	k3xPyFc4	se
krajinářství	krajinářství	k1gNnPc1	krajinářství
utváří	utvářet	k5eAaImIp3nP	utvářet
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Akademii	akademie	k1gFnSc6	akademie
postupně	postupně	k6eAd1	postupně
několik	několik	k4yIc4	několik
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
vedeny	vést	k5eAaImNgFnP	vést
významnými	významný	k2eAgMnPc7d1	významný
krajináři	krajinář	k1gMnPc7	krajinář
Karlem	Karel	k1gMnSc7	Karel
Postlem	Postl	k1gMnSc7	Postl
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonínem	Antonín	k1gMnSc7	Antonín
Mánesem	Mánes	k1gMnSc7	Mánes
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maxem	Max	k1gMnSc7	Max
Haushoferem	Haushofer	k1gMnSc7	Haushofer
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
žáci	žák	k1gMnPc1	žák
jako	jako	k8xS	jako
Adolf	Adolf	k1gMnSc1	Adolf
Kosárek	kosárek	k1gInSc1	kosárek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g />
.	.	kIx.	.
</s>
<s>
Bubák	bubák	k1gMnSc1	bubák
nebo	nebo	k8xC	nebo
Julius	Julius	k1gMnSc1	Julius
Mařák	Mařák	k1gMnSc1	Mařák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
škola	škola	k1gFnSc1	škola
vedená	vedený	k2eAgFnSc1d1	vedená
právě	právě	k6eAd1	právě
Juliem	Julius	k1gMnSc7	Julius
Mařákem	Mařák	k1gMnSc7	Mařák
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Slavíček	Slavíček	k1gMnSc1	Slavíček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kaván	Kaván	k1gMnSc1	Kaván
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Panuška	panuška	k1gFnSc1	panuška
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Kalvoda	Kalvoda	k1gMnSc1	Kalvoda
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Březina	Březina	k1gMnSc1	Březina
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlivným	vlivný	k2eAgInSc7d1	vlivný
romantickým	romantický	k2eAgInSc7d1	romantický
malířem-samoukem	malířemamouk	k1gInSc7	malířem-samouk
byl	být	k5eAaImAgMnS	být
A.	A.	kA	A.
B.	B.	kA	B.
Piepenhagen	Piepenhagen	k1gInSc4	Piepenhagen
<g/>
,	,	kIx,	,
představitelem	představitel	k1gMnSc7	představitel
realistické	realistický	k2eAgFnSc2d1	realistická
plenérové	plenérový	k2eAgFnSc2d1	plenérová
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
Antonín	Antonín	k1gMnSc1	Antonín
Chittussi	Chittusse	k1gFnSc4	Chittusse
<g/>
,	,	kIx,	,
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
krajiny	krajina	k1gFnSc2	krajina
pražské	pražský	k2eAgFnSc2d1	Pražská
i	i	k8xC	i
mimopražské	mimopražský	k2eAgFnSc2d1	mimopražská
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Jakub	Jakub	k1gMnSc1	Jakub
Schikaneder	Schikaneder	k1gMnSc1	Schikaneder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
také	také	k9	také
výraz	výraz	k1gInSc1	výraz
české	český	k2eAgFnSc2d1	Česká
krajimomalby	krajimomalba	k1gFnSc2	krajimomalba
láme	lámat	k5eAaImIp3nS	lámat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
směrů	směr	k1gInPc2	směr
<g/>
:	:	kIx,	:
k	k	k7c3	k
předním	přední	k2eAgMnPc3d1	přední
představitelům	představitel	k1gMnPc3	představitel
krajinářství	krajinářství	k1gNnSc6	krajinářství
realisticko-impresionistického	realistickompresionistický	k2eAgMnSc2d1	realisticko-impresionistický
patřili	patřit	k5eAaImAgMnP	patřit
Václav	Václav	k1gMnSc1	Václav
Radimský	Radimský	k2eAgMnSc1d1	Radimský
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Moravského	moravský	k2eAgNnSc2d1	Moravské
Slovácka	Slovácko	k1gNnSc2	Slovácko
Joža	Joža	k1gMnSc1	Joža
Úprka	Úprka	k1gMnSc1	Úprka
a	a	k8xC	a
především	především	k6eAd1	především
Antonín	Antonín	k1gMnSc1	Antonín
Slavíček	Slavíček	k1gMnSc1	Slavíček
či	či	k8xC	či
Otakar	Otakar	k1gMnSc1	Otakar
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
modernější	moderní	k2eAgMnPc1d2	modernější
metody	metoda	k1gFnSc2	metoda
vyznávali	vyznávat	k5eAaImAgMnP	vyznávat
mj.	mj.	kA	mj.
Otakar	Otakar	k1gMnSc1	Otakar
Kubin	Kubin	k1gMnSc1	Kubin
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Filla	Filla	k1gMnSc1	Filla
nebo	nebo	k8xC	nebo
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
skupina	skupina	k1gFnSc1	skupina
Tvrdošíjných	tvrdošíjný	k2eAgMnPc2d1	tvrdošíjný
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Špála	Špála	k1gMnSc1	Špála
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
zaběhlé	zaběhlý	k2eAgInPc4d1	zaběhlý
umělecké	umělecký	k2eAgInPc4d1	umělecký
směry	směr	k1gInPc4	směr
pak	pak	k6eAd1	pak
působili	působit	k5eAaImAgMnP	působit
osobití	osobitý	k2eAgMnPc1d1	osobitý
malíři	malíř	k1gMnPc1	malíř
jako	jako	k8xC	jako
Josef	Josef	k1gMnSc1	Josef
Lada	Lada	k1gFnSc1	Lada
nebo	nebo	k8xC	nebo
Ferdiš	Ferdiš	k1gInSc1	Ferdiš
Duša	Dušum	k1gNnSc2	Dušum
a	a	k8xC	a
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
regionálních	regionální	k2eAgMnPc2d1	regionální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specifickým	specifický	k2eAgNnSc7d1	specifické
pojetím	pojetí	k1gNnSc7	pojetí
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
městské	městský	k2eAgFnPc1d1	městská
a	a	k8xC	a
předměstské	předměstský	k2eAgFnPc1d1	předměstská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačovali	vyznačovat	k5eAaImAgMnP	vyznačovat
umělci	umělec	k1gMnPc7	umělec
Skupiny	skupina	k1gFnSc2	skupina
42	[number]	k4	42
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Jan	Jan	k1gMnSc1	Jan
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
Lhoták	Lhoták	k1gMnSc1	Lhoták
nebo	nebo	k8xC	nebo
František	František	k1gMnSc1	František
Gross	Gross	k1gMnSc1	Gross
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
krajiny	krajina	k1gFnPc4	krajina
imaginativní	imaginativní	k2eAgFnPc4d1	imaginativní
a	a	k8xC	a
fantaskní	fantaskní	k2eAgFnPc4d1	fantaskní
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
surrealistů	surrealista	k1gMnPc2	surrealista
jako	jako	k8xC	jako
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štyrský	Štyrský	k2eAgInSc4d1	Štyrský
<g/>
,	,	kIx,	,
Toyen	Toyen	k2eAgInSc4d1	Toyen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
tvorby	tvorba	k1gFnSc2	tvorba
Bohdana	Bohdan	k1gMnSc2	Bohdan
Laciny	Lacina	k1gMnSc2	Lacina
<g/>
.	.	kIx.	.
</s>
<s>
Nezapomenutelným	zapomenutelný	k2eNgMnSc7d1	nezapomenutelný
krajinářem	krajinář	k1gMnSc7	krajinář
lyrickým	lyrický	k2eAgFnPc3d1	lyrická
byl	být	k5eAaImAgInS	být
Ota	Ota	k1gMnSc1	Ota
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umění	umění	k1gNnSc1	umění
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
grafických	grafický	k2eAgFnPc2d1	grafická
technik	technika	k1gFnPc2	technika
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čepelák	Čepelák	k1gMnSc1	Čepelák
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Silovský	Silovský	k1gMnSc1	Silovský
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Štika	Štika	k1gMnSc1	Štika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
krajinářům	krajinář	k1gMnPc3	krajinář
poslední	poslední	k2eAgFnSc2d1	poslední
generace	generace	k1gFnSc2	generace
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
František	František	k1gMnSc1	František
Hodonský	hodonský	k2eAgMnSc1d1	hodonský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Sozanský	Sozanský	k2eAgMnSc1d1	Sozanský
nebo	nebo	k8xC	nebo
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Valečka	valečka	k1gFnSc1	valečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
grafiky	grafika	k1gFnSc2	grafika
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
E.	E.	kA	E.
H.	H.	kA	H.
Gombrich	Gombrich	k1gMnSc1	Gombrich
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
krajiny	krajina	k1gFnSc2	krajina
</s>
</p>
<p>
<s>
Plenér	plenér	k1gInSc1	plenér
</s>
</p>
<p>
<s>
Veduta	veduta	k1gFnSc1	veduta
</s>
</p>
<p>
<s>
Land	Land	k1gInSc1	Land
art	art	k?	art
</s>
</p>
