<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Székesfehérvár	Székesfehérvár	k1gInSc1	Székesfehérvár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
strany	strana	k1gFnSc2	strana
Fidesz-MPS	Fidesz-MPS	k1gFnSc2	Fidesz-MPS
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
premiér	premiéra	k1gFnPc2	premiéra
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
maďarského	maďarský	k2eAgInSc2d1	maďarský
zvyku	zvyk	k1gInSc2	zvyk
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
v	v	k7c6	v
Székesfehérváru	Székesfehérvár	k1gInSc6	Székesfehérvár
v	v	k7c6	v
župě	župa	k1gFnSc6	župa
Fejér	Fejér	k1gInSc4	Fejér
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
vesnicích	vesnice	k1gFnPc6	vesnice
Alcsútdoboz	Alcsútdoboza	k1gFnPc2	Alcsútdoboza
a	a	k8xC	a
Felcsút	Felcsúta	k1gFnPc2	Felcsúta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Székesfehérváru	Székesfehérvár	k1gInSc2	Székesfehérvár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
zde	zde	k6eAd1	zde
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Teleki	Teleke	k1gFnSc4	Teleke
Blanka	Blanka	k1gFnSc1	Blanka
Gimnázium	Gimnázium	k1gNnSc4	Gimnázium
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
povinnou	povinný	k2eAgFnSc4d1	povinná
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Zalaegerszeg	Zalaegerszega	k1gFnPc2	Zalaegerszega
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Eötvös	Eötvösa	k1gFnPc2	Eötvösa
Loránd	Loránd	k1gMnSc1	Loránd
Tudományegyetem	Tudományegyet	k1gMnSc7	Tudományegyet
(	(	kIx(	(
<g/>
ELTE	ELTE	kA	ELTE
<g/>
)	)	kIx)	)
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc4	studium
dokončil	dokončit	k5eAaPmAgInS	dokončit
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Szolnoku	Szolnok	k1gInSc6	Szolnok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dojížděl	dojíždět	k5eAaImAgMnS	dojíždět
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
výživy	výživa	k1gFnSc2	výživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
obdržel	obdržet	k5eAaPmAgMnS	obdržet
stipendium	stipendium	k1gNnSc4	stipendium
od	od	k7c2	od
Soros	Sorosa	k1gFnPc2	Sorosa
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
nadace	nadace	k1gFnSc1	nadace
amerického	americký	k2eAgMnSc2d1	americký
finančníka	finančník	k1gMnSc2	finančník
George	Georg	k1gMnSc2	Georg
Sorose	Sorosa	k1gFnSc6	Sorosa
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Pembroke	Pembroke	k1gFnSc4	Pembroke
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
ale	ale	k8xC	ale
vydržel	vydržet	k5eAaPmAgMnS	vydržet
jen	jen	k6eAd1	jen
necelé	celý	k2eNgInPc4d1	necelý
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
úspěšně	úspěšně	k6eAd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
poslance	poslanec	k1gMnSc4	poslanec
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
<g/>
Orbán	Orbán	k1gInSc1	Orbán
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
právničkou	právnička	k1gFnSc7	právnička
Anikó	Anikó	k1gFnSc2	Anikó
Lévai	Léva	k1gFnSc2	Léva
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
Ráhel	Ráhel	k1gMnSc1	Ráhel
<g/>
,	,	kIx,	,
Gáspár	Gáspár	k1gMnSc1	Gáspár
<g/>
,	,	kIx,	,
Sára	Sára	k1gFnSc1	Sára
<g/>
,	,	kIx,	,
Róza	Róza	k1gFnSc1	Róza
<g/>
,	,	kIx,	,
Flóra	Flóra	k1gFnSc1	Flóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pád	Pád	k1gInSc1	Pád
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Orbánova	Orbánův	k2eAgFnSc1d1	Orbánova
politická	politický	k2eAgFnSc1d1	politická
dráha	dráha	k1gFnSc1	dráha
začala	začít	k5eAaPmAgFnS	začít
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
třiceti	třicet	k4xCc7	třicet
šesti	šest	k4xCc7	šest
mladými	mladý	k2eAgMnPc7d1	mladý
intelektuály	intelektuál	k1gMnPc7	intelektuál
a	a	k8xC	a
univerzitními	univerzitní	k2eAgMnPc7d1	univerzitní
studenty	student	k1gMnPc7	student
založil	založit	k5eAaPmAgMnS	založit
hnutí	hnutí	k1gNnSc3	hnutí
mladých	mladý	k2eAgMnPc2d1	mladý
demokratů	demokrat	k1gMnPc2	demokrat
–	–	k?	–
Fidesz	Fidesza	k1gFnPc2	Fidesza
–	–	k?	–
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
občanská	občanský	k2eAgFnSc1d1	občanská
unieFidesz	unieFidesz	k1gInSc1	unieFidesz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
na	na	k7c6	na
smuteční	smuteční	k2eAgFnSc6d1	smuteční
slavnosti	slavnost	k1gFnSc6	slavnost
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
rehabilitovaného	rehabilitovaný	k2eAgMnSc2d1	rehabilitovaný
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
období	období	k1gNnSc6	období
maďarského	maďarský	k2eAgNnSc2d1	Maďarské
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Imre	Imre	k1gFnSc1	Imre
Nagye	Nagye	k1gFnSc1	Nagye
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Orbán	Orbán	k1gMnSc1	Orbán
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
uspořádání	uspořádání	k1gNnSc4	uspořádání
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
odchod	odchod	k1gInSc1	odchod
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
slova	slovo	k1gNnPc4	slovo
sklidil	sklidit	k5eAaPmAgInS	sklidit
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
potlesk	potlesk	k1gInSc1	potlesk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
akt	akt	k1gInSc1	akt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
nové	nový	k2eAgFnSc2d1	nová
maďarské	maďarský	k2eAgFnSc2d1	maďarská
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
demokracie	demokracie	k1gFnSc2	demokracie
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1990	[number]	k4	1990
získal	získat	k5eAaPmAgMnS	získat
Fidesz	Fidesz	k1gMnSc1	Fidesz
21	[number]	k4	21
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
Orbán	Orbán	k1gMnSc1	Orbán
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
frakce	frakce	k1gFnSc2	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přeměn	přeměna	k1gFnPc2	přeměna
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
ještě	ještě	k9	ještě
jako	jako	k9	jako
liberální	liberální	k2eAgMnPc1d1	liberální
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Fidesz	Fidesz	k1gInSc1	Fidesz
získal	získat	k5eAaPmAgInS	získat
jen	jen	k9	jen
7	[number]	k4	7
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
výrazné	výrazný	k2eAgFnSc6d1	výrazná
přeměně	přeměna	k1gFnSc6	přeměna
ideologie	ideologie	k1gFnSc2	ideologie
Fideszu	Fidesz	k1gInSc2	Fidesz
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
přechodu	přechod	k1gInSc6	přechod
od	od	k7c2	od
liberalismu	liberalismus	k1gInSc2	liberalismus
k	k	k7c3	k
národně-konzervativní	národněonzervativní	k2eAgFnSc3d1	národně-konzervativní
orientaci	orientace	k1gFnSc3	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
Orbán	Orbán	k1gInSc1	Orbán
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
jako	jako	k8xC	jako
nacionalista	nacionalista	k1gMnSc1	nacionalista
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
József	József	k1gMnSc1	József
Antall	Antall	k1gMnSc1	Antall
-	-	kIx~	-
chce	chtít	k5eAaImIp3nS	chtít
"	"	kIx"	"
<g/>
stát	stát	k5eAaPmF	stát
premiérem	premiér	k1gMnSc7	premiér
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
Maďarů	maďar	k1gInPc2	maďar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Orbán	Orbán	k1gMnSc1	Orbán
tehdy	tehdy	k6eAd1	tehdy
začal	začít	k5eAaPmAgMnS	začít
obhajovat	obhajovat	k5eAaImF	obhajovat
"	"	kIx"	"
<g/>
zájmy	zájem	k1gInPc7	zájem
všech	všecek	k3xTgInPc2	všecek
Maďarů	maďar	k1gInPc2	maďar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
1998	[number]	k4	1998
šel	jít	k5eAaImAgMnS	jít
Fidesz	Fidesz	k1gMnSc1	Fidesz
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
programem	program	k1gInSc7	program
obhajoby	obhajoba	k1gFnSc2	obhajoba
zájmů	zájem	k1gInPc2	zájem
"	"	kIx"	"
<g/>
celého	celý	k2eAgInSc2d1	celý
maďarského	maďarský	k2eAgInSc2d1	maďarský
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
Maďarů	Maďar	k1gMnPc2	Maďar
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
požadoval	požadovat	k5eAaImAgMnS	požadovat
ochranu	ochrana	k1gFnSc4	ochrana
maďarského	maďarský	k2eAgInSc2d1	maďarský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
hájil	hájit	k5eAaImAgMnS	hájit
význam	význam	k1gInSc4	význam
církví	církev	k1gFnPc2	církev
pro	pro	k7c4	pro
soudržnost	soudržnost	k1gFnSc4	soudržnost
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
důsledném	důsledný	k2eAgNnSc6d1	důsledné
hájení	hájení	k1gNnSc6	hájení
národních	národní	k2eAgInPc2d1	národní
zájmů	zájem	k1gInPc2	zájem
při	při	k7c6	při
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
volby	volba	k1gFnPc1	volba
Fidesz	Fidesz	k1gInSc4	Fidesz
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
premiérem	premiér	k1gMnSc7	premiér
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
o	o	k7c4	o
krůček	krůček	k1gInSc4	krůček
blíže	blíž	k1gFnSc2	blíž
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
kladla	klást	k5eAaImAgFnS	klást
větší	veliký	k2eAgInSc4d2	veliký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2001	[number]	k4	2001
přijetím	přijetí	k1gNnSc7	přijetí
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
Krajanského	krajanský	k2eAgInSc2d1	krajanský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
Maďarům	Maďar	k1gMnPc3	Maďar
žijícím	žijící	k2eAgNnSc7d1	žijící
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
země	zem	k1gFnSc2	zem
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mají	mít	k5eAaImIp3nP	mít
maďarští	maďarský	k2eAgMnPc1d1	maďarský
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
první	první	k4xOgFnSc2	první
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
snížila	snížit	k5eAaPmAgFnS	snížit
se	se	k3xPyFc4	se
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
a	a	k8xC	a
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
spotřeba	spotřeba	k1gFnSc1	spotřeba
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ministrů	ministr	k1gMnPc2	ministr
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
let	léto	k1gNnPc2	léto
a	a	k8xC	a
měla	mít	k5eAaImAgNnP	mít
tak	tak	k6eAd1	tak
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
mladé	mladý	k2eAgFnSc3d1	mladá
generaci	generace	k1gFnSc3	generace
a	a	k8xC	a
rodinám	rodina	k1gFnPc3	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brala	brát	k5eAaImAgFnS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
i	i	k9	i
potřeby	potřeba	k1gFnSc2	potřeba
starších	starý	k2eAgMnPc2d2	starší
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
však	však	k9	však
také	také	k9	také
existovalo	existovat	k5eAaImAgNnS	existovat
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Orbánovou	Orbánový	k2eAgFnSc7d1	Orbánový
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
primátorem	primátor	k1gMnSc7	primátor
Budapešti	Budapešť	k1gFnSc2	Budapešť
Gáborem	Gábor	k1gMnSc7	Gábor
Demszkym	Demszkym	k1gInSc4	Demszkym
z	z	k7c2	z
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
SZDSZ	SZDSZ	kA	SZDSZ
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
osobního	osobní	k2eAgNnSc2d1	osobní
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
Orbána	Orbán	k1gMnSc2	Orbán
s	s	k7c7	s
Demszkym	Demszkym	k1gInSc1	Demszkym
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sahalo	sahat	k5eAaImAgNnS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
jejich	jejich	k3xOp3gNnPc2	jejich
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
ELTE	ELTE	kA	ELTE
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
obrážel	obrážet	k5eAaImAgInS	obrážet
také	také	k9	také
tradiční	tradiční	k2eAgInSc1d1	tradiční
konflikt	konflikt	k1gInSc1	konflikt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Budapešti	Budapešť	k1gFnSc2	Budapešť
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
oblastmi	oblast	k1gFnPc7	oblast
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2000	[number]	k4	2000
Orbán	Orbána	k1gFnPc2	Orbána
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnPc4	předseda
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
László	László	k1gMnSc1	László
Kövér	Kövér	k1gMnSc1	Kövér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Fidesz	Fidesz	k1gInSc1	Fidesz
rozešel	rozejít	k5eAaPmAgInS	rozejít
s	s	k7c7	s
Liberální	liberální	k2eAgFnSc7d1	liberální
internacionálou	internacionála	k1gFnSc7	internacionála
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
získal	získat	k5eAaPmAgMnS	získat
přidružené	přidružený	k2eAgNnSc4d1	přidružené
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lidové	lidový	k2eAgFnSc6d1	lidová
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
2002	[number]	k4	2002
získal	získat	k5eAaPmAgInS	získat
Fidesz	Fidesz	k1gInSc1	Fidesz
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
MDF	MDF	kA	MDF
188	[number]	k4	188
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
MSZP	MSZP	kA	MSZP
jen	jen	k9	jen
178	[number]	k4	178
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
opoziční	opoziční	k2eAgFnSc7d1	opoziční
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vládu	vláda	k1gFnSc4	vláda
utvořila	utvořit	k5eAaPmAgFnS	utvořit
MSZP	MSZP	kA	MSZP
a	a	k8xC	a
SZDSZ	SZDSZ	kA	SZDSZ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
dohromady	dohromady	k6eAd1	dohromady
198	[number]	k4	198
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Viktora	Viktor	k1gMnSc2	Viktor
Orbána	Orbán	k1gMnSc2	Orbán
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
premiéra	premiér	k1gMnSc2	premiér
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Péter	Péter	k1gInSc4	Péter
Medgyessy	Medgyessa	k1gFnSc2	Medgyessa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Orbán	Orbán	k1gMnSc1	Orbán
stal	stát	k5eAaPmAgMnS	stát
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
opět	opět	k6eAd1	opět
předsedou	předseda	k1gMnSc7	předseda
Fideszu	Fidesz	k1gInSc2	Fidesz
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
tvrdě	tvrdě	k6eAd1	tvrdě
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
vládu	vláda	k1gFnSc4	vláda
MSZP	MSZP	kA	MSZP
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
méně	málo	k6eAd2	málo
realistické	realistický	k2eAgInPc1d1	realistický
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
sliby	slib	k1gInPc1	slib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fidesz	Fidesz	k1gInSc1	Fidesz
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Orbánem	Orbán	k1gMnSc7	Orbán
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
i	i	k8xC	i
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
masivní	masivní	k2eAgFnSc1d1	masivní
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
po	po	k7c6	po
velkých	velký	k2eAgFnPc6d1	velká
demonstracích	demonstrace	k1gFnPc6	demonstrace
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2006	[number]	k4	2006
proti	proti	k7c3	proti
premiérovi	premiér	k1gMnSc3	premiér
Ferenci	Ferenc	k1gMnSc3	Ferenc
Gyurcsánymu	Gyurcsánym	k1gInSc2	Gyurcsánym
(	(	kIx(	(
<g/>
MSZP	MSZP	kA	MSZP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Orbán	Orbán	k1gMnSc1	Orbán
organizoval	organizovat	k5eAaBmAgMnS	organizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zhoršující	zhoršující	k2eAgFnSc2d1	zhoršující
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
maďarskou	maďarský	k2eAgFnSc4d1	maďarská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
popularita	popularita	k1gFnSc1	popularita
Fideszu	Fidesz	k1gInSc2	Fidesz
a	a	k8xC	a
Viktora	Viktor	k1gMnSc2	Viktor
Orbána	Orbán	k1gMnSc2	Orbán
samotného	samotný	k2eAgMnSc2d1	samotný
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
2010	[number]	k4	2010
silně	silně	k6eAd1	silně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ohlášeném	ohlášený	k2eAgNnSc6d1	ohlášené
odstoupení	odstoupení	k1gNnSc6	odstoupení
premiéra	premiér	k1gMnSc2	premiér
Ference	Ferenc	k1gMnSc2	Ferenc
Gyurcsányho	Gyurcsány	k1gMnSc2	Gyurcsány
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
Orbán	Orbána	k1gFnPc2	Orbána
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
László	László	k1gMnSc1	László
Sólyom	Sólyom	k1gInSc1	Sólyom
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
žádal	žádat	k5eAaImAgMnS	žádat
vypsání	vypsání	k1gNnSc4	vypsání
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c4	o
zvolení	zvolení	k1gNnSc4	zvolení
Gordona	Gordon	k1gMnSc2	Gordon
Bajnaie	Bajnaie	k1gFnSc2	Bajnaie
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
se	se	k3xPyFc4	se
asi	asi	k9	asi
5000	[number]	k4	5000
příznivců	příznivec	k1gMnPc2	příznivec
Orbánova	Orbánův	k2eAgInSc2d1	Orbánův
Fideszu	Fidesz	k1gInSc2	Fidesz
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
před	před	k7c7	před
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
vyzývali	vyzývat	k5eAaImAgMnP	vyzývat
poslance	poslanec	k1gMnSc4	poslanec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Bajnaie	Bajnaie	k1gFnSc1	Bajnaie
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
nepodpořili	podpořit	k5eNaPmAgMnP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
MSZP	MSZP	kA	MSZP
a	a	k8xC	a
SZDSZ	SZDSZ	kA	SZDSZ
však	však	k9	však
Bajnaie	Bajnaie	k1gFnSc1	Bajnaie
zvolili	zvolit	k5eAaPmAgMnP	zvolit
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
povede	povést	k5eAaPmIp3nS	povést
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
vládu	vláda	k1gFnSc4	vláda
až	až	k9	až
do	do	k7c2	do
termínu	termín	k1gInSc2	termín
řádných	řádný	k2eAgFnPc2d1	řádná
voleb	volba	k1gFnPc2	volba
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
jeho	jeho	k3xOp3gFnSc2	jeho
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
rivala	rival	k1gMnSc2	rival
Fideszu	Fidesz	k1gInSc2	Fidesz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
na	na	k7c6	na
historicky	historicky	k6eAd1	historicky
nejhorší	zlý	k2eAgFnSc6d3	nejhorší
úrovni	úroveň	k1gFnSc6	úroveň
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
veřejně	veřejně	k6eAd1	veřejně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podpoří	podpořit	k5eAaPmIp3nP	podpořit
snahy	snaha	k1gFnPc4	snaha
Maďarů	Maďar	k1gMnPc2	Maďar
žijících	žijící	k2eAgMnPc2d1	žijící
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
jejich	jejich	k3xOp3gFnSc2	jejich
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
už	už	k6eAd1	už
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
cítil	cítit	k5eAaImAgMnS	cítit
příštím	příští	k2eAgMnSc7d1	příští
maďarským	maďarský	k2eAgMnSc7d1	maďarský
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
Orbán	Orbán	k1gMnSc1	Orbán
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
na	na	k7c6	na
předvolebním	předvolební	k2eAgNnSc6d1	předvolební
shromáždění	shromáždění	k1gNnSc6	shromáždění
slovenské	slovenský	k2eAgFnSc2d1	slovenská
Strany	strana	k1gFnSc2	strana
maďarskej	maďarskej	k?	maďarskej
koalície	koalície	k1gFnSc2	koalície
-	-	kIx~	-
Magyar	Magyar	k1gMnSc1	Magyar
Koalíció	Koalíció	k1gMnSc1	Koalíció
Pártja	Pártja	k1gMnSc1	Pártja
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
maďarském	maďarský	k2eAgNnSc6d1	Maďarské
městě	město	k1gNnSc6	město
Esztergom	Esztergom	k1gInSc4	Esztergom
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadcházející	nadcházející	k2eAgFnPc1d1	nadcházející
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
jsou	být	k5eAaImIp3nP	být
záležitostí	záležitost	k1gFnSc7	záležitost
všech	všecek	k3xTgMnPc2	všecek
Maďarů	Maďar	k1gMnPc2	Maďar
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Karpatské	karpatský	k2eAgFnSc6d1	Karpatská
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Orbán	Orbán	k1gMnSc1	Orbán
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
výstupu	výstup	k1gInSc2	výstup
<g/>
:	:	kIx,	:
Každého	každý	k3xTgMnSc4	každý
Maďara	Maďar	k1gMnSc4	Maďar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
odevzdá	odevzdat	k5eAaPmIp3nS	odevzdat
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
očekáváním	očekávání	k1gNnSc7	očekávání
sledovat	sledovat	k5eAaImF	sledovat
jiný	jiný	k2eAgInSc4d1	jiný
Maďar	maďar	k1gInSc4	maďar
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
následně	následně	k6eAd1	následně
tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
tvrdě	tvrdě	k6eAd1	tvrdě
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
označil	označit	k5eAaPmAgMnS	označit
Orbána	Orbán	k1gMnSc4	Orbán
a	a	k8xC	a
předsedu	předseda	k1gMnSc4	předseda
SMK-MKP	SMK-MKP	k1gFnSc2	SMK-MKP
Pála	Pála	k1gMnSc1	Pála
Csákyho	Csáky	k1gMnSc2	Csáky
za	za	k7c4	za
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Orbánova	Orbánův	k2eAgNnPc4d1	Orbánovo
slova	slovo	k1gNnPc4	slovo
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
i	i	k8xC	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurinda	k1gFnSc1	Dzurinda
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vláda	vláda	k1gFnSc1	vláda
si	se	k3xPyFc3	se
s	s	k7c7	s
Orbánovou	Orbánový	k2eAgFnSc7d1	Orbánový
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
rozuměla	rozumět	k5eAaImAgFnS	rozumět
<g/>
.	.	kIx.	.
<g/>
Růst	růst	k1gInSc4	růst
popularity	popularita	k1gFnSc2	popularita
Fideszu	Fidesz	k1gInSc2	Fidesz
a	a	k8xC	a
Viktora	Viktor	k1gMnSc2	Viktor
Orbána	Orbán	k1gMnSc2	Orbán
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgMnS	projevit
během	během	k7c2	během
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Fidesz	Fidesz	k1gInSc1	Fidesz
(	(	kIx(	(
<g/>
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
KDNP	KDNP	kA	KDNP
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
16	[number]	k4	16
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rival	rival	k1gMnSc1	rival
MSZP	MSZP	kA	MSZP
jen	jen	k9	jen
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
také	také	k9	také
krajně	krajně	k6eAd1	krajně
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
Jobbik	Jobbik	k1gInSc1	Jobbik
se	s	k7c7	s
3	[number]	k4	3
mandáty	mandát	k1gInPc7	mandát
a	a	k8xC	a
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc6d1	konzervativní
MDF	MDF	kA	MDF
obhájilo	obhájit	k5eAaPmAgNnS	obhájit
svůj	svůj	k3xOyFgInSc4	svůj
1	[number]	k4	1
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnPc1	volba
2010	[number]	k4	2010
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
premiérem	premiér	k1gMnSc7	premiér
===	===	k?	===
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
byl	být	k5eAaImAgMnS	být
kandidátem	kandidát	k1gMnSc7	kandidát
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
post	post	k1gInSc4	post
premiéra	premiéra	k1gFnSc1	premiéra
pro	pro	k7c4	pro
Parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
podle	podle	k7c2	podle
předvolebních	předvolební	k2eAgInPc2d1	předvolební
průzkumů	průzkum	k1gInPc2	průzkum
se	se	k3xPyFc4	se
preference	preference	k1gFnPc1	preference
Fidesz	Fidesza	k1gFnPc2	Fidesza
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
od	od	k7c2	od
53	[number]	k4	53
<g/>
%	%	kIx~	%
do	do	k7c2	do
68	[number]	k4	68
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
preference	preference	k1gFnSc1	preference
levicové	levicový	k2eAgFnSc2d1	levicová
MSZP	MSZP	kA	MSZP
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
od	od	k7c2	od
17	[number]	k4	17
<g/>
%	%	kIx~	%
do	do	k7c2	do
23	[number]	k4	23
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
již	již	k6eAd1	již
předem	předem	k6eAd1	předem
vcelku	vcelku	k6eAd1	vcelku
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Fidesz	Fidesz	k1gMnSc1	Fidesz
stane	stanout	k5eAaPmIp3nS	stanout
vítězem	vítěz	k1gMnSc7	vítěz
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
volby	volba	k1gFnSc2	volba
2010	[number]	k4	2010
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
Fidesz	Fidesza	k1gFnPc2	Fidesza
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
staly	stát	k5eAaPmAgFnP	stát
nejúspěšnějšími	úspěšný	k2eAgFnPc7d3	nejúspěšnější
volbami	volba	k1gFnPc7	volba
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
samotného	samotný	k2eAgInSc2d1	samotný
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Fidesz	Fidesz	k1gMnSc1	Fidesz
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
263	[number]	k4	263
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
68,13	[number]	k4	68,13
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
tak	tak	k9	tak
ústavní	ústavní	k2eAgFnSc4d1	ústavní
dvoutřetinovou	dvoutřetinový	k2eAgFnSc4d1	dvoutřetinová
většinu	většina	k1gFnSc4	většina
nutnou	nutný	k2eAgFnSc4d1	nutná
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
takto	takto	k6eAd1	takto
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
cestu	cesta	k1gFnSc4	cesta
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
reformám	reforma	k1gFnPc3	reforma
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
k	k	k7c3	k
dalekosáhlým	dalekosáhlý	k2eAgFnPc3d1	dalekosáhlá
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
státního	státní	k2eAgInSc2d1	státní
aparátu	aparát	k1gInSc2	aparát
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
personálního	personální	k2eAgNnSc2d1	personální
obsazení	obsazení	k1gNnSc2	obsazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
oznámení	oznámení	k1gNnSc4	oznámení
oficiálních	oficiální	k2eAgInPc2d1	oficiální
výsledků	výsledek	k1gInPc2	výsledek
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
László	László	k1gFnSc2	László
Sólyom	Sólyom	k1gInSc1	Sólyom
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Orbána	Orbán	k1gMnSc4	Orbán
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
složil	složit	k5eAaPmAgMnS	složit
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
slib	slib	k1gInSc4	slib
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
osmým	osmý	k4xOgMnSc7	osmý
premiérem	premiér	k1gMnSc7	premiér
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
byla	být	k5eAaImAgFnS	být
vypracována	vypracován	k2eAgFnSc1d1	vypracována
nová	nový	k2eAgFnSc1d1	nová
maďarská	maďarský	k2eAgFnSc1d1	maďarská
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
ústavou	ústava	k1gFnSc7	ústava
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
změněn	změněn	k2eAgInSc4d1	změněn
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
z	z	k7c2	z
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Kritiku	kritika	k1gFnSc4	kritika
opozice	opozice	k1gFnSc2	opozice
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
nacionalistické	nacionalistický	k2eAgInPc1d1	nacionalistický
prvky	prvek	k1gInPc1	prvek
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc2	zvýšení
vládní	vládní	k2eAgFnSc2d1	vládní
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
justicí	justice	k1gFnSc7	justice
a	a	k8xC	a
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
oslabení	oslabení	k1gNnSc4	oslabení
pravomocí	pravomoc	k1gFnPc2	pravomoc
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnPc1	volba
2014	[number]	k4	2014
a	a	k8xC	a
potřetí	potřetí	k4xO	potřetí
premiérem	premiér	k1gMnSc7	premiér
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
Východního	východní	k2eAgNnSc2d1	východní
partnerství	partnerství	k1gNnSc2	partnerství
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Juncker	Juncker	k1gInSc1	Juncker
oslovil	oslovit	k5eAaPmAgInS	oslovit
Orbána	Orbána	k1gFnSc1	Orbána
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zdravím	zdravit	k5eAaImIp1nS	zdravit
<g/>
,	,	kIx,	,
diktátore	diktátor	k1gMnSc5	diktátor
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Orbán	Orbán	k1gMnSc1	Orbán
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
komisí	komise	k1gFnSc7	komise
<g/>
,	,	kIx,	,
když	když	k8xS	když
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
její	její	k3xOp3gInSc4	její
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
afrických	africký	k2eAgMnPc2d1	africký
uprchlíků	uprchlík	k1gMnPc2	uprchlík
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kvót	kvóta	k1gFnPc2	kvóta
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
vedené	vedený	k2eAgFnSc2d1	vedená
Junckerem	Juncker	k1gInSc7	Juncker
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
absurdní	absurdní	k2eAgNnSc4d1	absurdní
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
šílený	šílený	k2eAgMnSc1d1	šílený
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Proslovy	proslov	k1gInPc4	proslov
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc4	vystoupení
===	===	k?	===
</s>
</p>
<p>
<s>
János	János	k1gMnSc1	János
Hollós	Hollós	k1gInSc1	Hollós
a	a	k8xC	a
Katalin	Katalin	k2eAgMnSc1d1	Katalin
Kondor	kondor	k1gMnSc1	kondor
<g/>
:	:	kIx,	:
Szerda	Szerda	k1gMnSc1	Szerda
reggel	reggel	k1gMnSc1	reggel
-	-	kIx~	-
Rádiós	Rádiós	k1gInSc1	Rádiós
beszélgetések	beszélgetések	k1gMnSc1	beszélgetések
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
miniszterelnökkel	miniszterelnökkel	k1gMnSc1	miniszterelnökkel
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
szeptember	szeptembrat	k5eAaPmRp2nS	szeptembrat
–	–	k?	–
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
december	december	k1gInSc1	december
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-9337-32-3	[number]	k4	963-9337-32-3
</s>
</p>
<p>
<s>
János	János	k1gMnSc1	János
Hollós	Hollós	k1gInSc1	Hollós
a	a	k8xC	a
Katalin	Katalin	k2eAgMnSc1d1	Katalin
Kondor	kondor	k1gMnSc1	kondor
<g/>
:	:	kIx,	:
Szerda	Szerda	k1gMnSc1	Szerda
reggel	reggel	k1gMnSc1	reggel
-	-	kIx~	-
Rádiós	Rádiós	k1gInSc1	Rádiós
beszélgetések	beszélgetések	k1gMnSc1	beszélgetések
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
miniszterelnökkel	miniszterelnökkel	k1gMnSc1	miniszterelnökkel
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-9337-61-7	[number]	k4	963-9337-61-7
</s>
</p>
<p>
<s>
A	a	k9	a
történelem	történel	k1gInSc7	történel
főutcáján	főutcáján	k1gInSc1	főutcáján
-	-	kIx~	-
Magyarország	Magyarország	k1gInSc1	Magyarország
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
miniszterelnök	miniszterelnök	k1gMnSc1	miniszterelnök
beszédei	beszéde	k1gFnSc2	beszéde
és	és	k?	és
beszédrészletei	beszédrészlete	k1gFnSc2	beszédrészlete
<g/>
,	,	kIx,	,
Magyar	Magyar	k1gMnSc1	Magyar
Egyetemi	Egyete	k1gFnPc7	Egyete
Kiadó	Kiadó	k1gMnSc1	Kiadó
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-86383-1-1	[number]	k4	963-86383-1-1
</s>
</p>
<p>
<s>
20	[number]	k4	20
év	év	k?	év
-	-	kIx~	-
Beszédek	Beszédek	k1gInSc1	Beszédek
<g/>
,	,	kIx,	,
írások	írások	k1gInSc1	írások
<g/>
,	,	kIx,	,
interjúk	interjúk	k1gInSc1	interjúk
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Heti	Het	k1gFnPc1	Het
Válasz	Válasz	k1gMnSc1	Válasz
Kiadó	Kiadó	k1gMnSc1	Kiadó
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-9461-22-9	[number]	k4	963-9461-22-9
</s>
</p>
<p>
<s>
Egy	ego	k1gNnPc7	ego
az	az	k?	az
ország	ország	k1gMnSc1	ország
<g/>
,	,	kIx,	,
Bp	Bp	k1gMnSc1	Bp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Helikon	helikon	k1gInSc1	helikon
kiadó	kiadó	k?	kiadó
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
Viktoru	Viktor	k1gMnSc6	Viktor
Orbánovi	Orbán	k1gMnSc6	Orbán
===	===	k?	===
</s>
</p>
<p>
<s>
József	József	k1gMnSc1	József
Debreczeni	Debreczen	k2eAgMnPc1d1	Debreczen
<g/>
:	:	kIx,	:
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
,	,	kIx,	,
Osiris	Osiris	k1gMnSc1	Osiris
kiadó	kiadó	k?	kiadó
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963	[number]	k4	963
<g/>
-	-	kIx~	-
<g/>
389	[number]	k4	389
<g/>
-	-	kIx~	-
<g/>
443	[number]	k4	443
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
honlapja	honlapja	k6eAd1	honlapja
</s>
</p>
<p>
<s>
László	László	k?	László
Kéri	Kér	k1gMnPc1	Kér
<g/>
:	:	kIx,	:
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
,	,	kIx,	,
Századvég	Századvég	k1gMnSc1	Századvég
kiadó	kiadó	k?	kiadó
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-8384-56-5	[number]	k4	963-8384-56-5
</s>
</p>
<p>
<s>
Rácz	Rácz	k1gInSc1	Rácz
András	András	k1gInSc1	András
<g/>
:	:	kIx,	:
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
7	[number]	k4	7
titka	titek	k1gInSc2	titek
<g/>
,	,	kIx,	,
Magánkiadás	Magánkiadás	k1gInSc1	Magánkiadás
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963	[number]	k4	963
<g/>
-	-	kIx~	-
<g/>
217	[number]	k4	217
<g/>
-	-	kIx~	-
<g/>
186	[number]	k4	186
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
honlapja	honlapja	k6eAd1	honlapja
</s>
</p>
<p>
<s>
Szabó	Szabó	k?	Szabó
Kálmán	Kálmán	k2eAgMnSc1d1	Kálmán
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hiszünk	Hiszünk	k1gMnSc1	Hiszünk
a	a	k8xC	a
szeretet	szeretet	k1gMnSc1	szeretet
és	és	k?	és
az	az	k?	az
összefogás	összefogás	k6eAd1	összefogás
erejében	erejében	k2eAgMnSc1d1	erejében
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
politikai	politikai	k6eAd1	politikai
krédója	krédója	k6eAd1	krédója
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963	[number]	k4	963
<g/>
-	-	kIx~	-
<g/>
9484	[number]	k4	9484
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
honlapja	honlapja	k1gFnSc1	honlapja
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Péter	Péter	k1gMnSc1	Péter
Kende	Kend	k1gInSc5	Kend
<g/>
:	:	kIx,	:
A	a	k9	a
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
,	,	kIx,	,
KendeArt	KendeArt	k1gInSc1	KendeArt
Kft	Kft	k1gFnSc2	Kft
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-00-9037-6	[number]	k4	963-00-9037-6
honlapjáról	honlapjárónout	k5eAaPmAgMnS	honlapjárónout
letölthető	letölthető	k?	letölthető
</s>
</p>
<p>
<s>
Sándor	Sándor	k1gInSc1	Sándor
Kopátsy	Kopátsa	k1gFnSc2	Kopátsa
<g/>
:	:	kIx,	:
Az	Az	k1gMnSc1	Az
Orbán-jelenség	Orbánelenség	k1gMnSc1	Orbán-jelenség
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
–	–	k?	–
a	a	k8xC	a
MEK	mek	k0	mek
honlapján	honlapjat	k5eAaBmNgInS	honlapjat
elérhető	elérhető	k?	elérhető
<g/>
:	:	kIx,	:
link	link	k1gMnSc1	link
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Péter	Péter	k1gMnSc1	Péter
Kende	Kend	k1gInSc5	Kend
<g/>
:	:	kIx,	:
Az	Az	k1gFnPc1	Az
igazi	igah	k1gMnPc1	igah
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
–	–	k?	–
A	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
2	[number]	k4	2
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
KendeArt	KendeArt	k1gInSc1	KendeArt
KFT	KFT	kA	KFT
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-8233-13-3	[number]	k4	963-8233-13-3
honlapjáról	honlapjárónout	k5eAaPmAgMnS	honlapjárónout
letölthető	letölthető	k?	letölthető
</s>
</p>
<p>
<s>
Krisztina	Krisztina	k1gFnSc1	Krisztina
Ferenczi	Ferencze	k1gFnSc4	Ferencze
<g/>
:	:	kIx,	:
Szüret	Szüret	k1gInSc1	Szüret
–	–	k?	–
Az	Az	k1gFnSc1	Az
Orbán-vagyonok	Orbánagyonok	k1gInSc1	Orbán-vagyonok
nyomában	nyomában	k1gInSc1	nyomában
<g/>
,	,	kIx,	,
Diario	Diario	k1gNnSc1	Diario
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-229-223-5	[number]	k4	963-229-223-5
</s>
</p>
<p>
<s>
Fiúk	Fiúk	k1gInSc1	Fiúk
a	a	k8xC	a
szőlőben	szőlőbit	k5eAaPmNgInS	szőlőbit
A	a	k9	a
tokaji	tokaj	k1gInSc3	tokaj
borcsaták	borcsaták	k1gMnSc1	borcsaták
per-	per-	k?	per-
és	és	k?	és
sajtóanyaga	sajtóanyaga	k1gFnSc1	sajtóanyaga
<g/>
,	,	kIx,	,
Élet	Élet	k1gInSc1	Élet
és	és	k?	és
Irodalom-könyvek	Irodalomönyvek	k1gInSc1	Irodalom-könyvek
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
963-86526-4-0	[number]	k4	963-86526-4-0
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KONTLER	KONTLER	kA	KONTLER
<g/>
,	,	kIx,	,
László	László	k1gFnSc1	László
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
613	[number]	k4	613
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
616	[number]	k4	616
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRAŽÁK	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
států	stát	k1gInPc2	stát
MAĎARSKO	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Libri	Libr	k1gFnSc2	Libr
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
145	[number]	k4	145
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
269	[number]	k4	269
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IRMANOVÁ	IRMANOVÁ	kA	IRMANOVÁ
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
menšina	menšina	k1gFnSc1	menšina
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
:	:	kIx,	:
albis	albis	k1gFnSc4	albis
international	internationat	k5eAaPmAgMnS	internationat
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
316	[number]	k4	316
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86067	[number]	k4	86067
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
na	na	k7c6	na
maďarské	maďarský	k2eAgFnSc6d1	maďarská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
</s>
</p>
<p>
<s>
Fidesz	Fidesz	k1gMnSc1	Fidesz
–	–	k?	–
Magyar	Magyar	k1gMnSc1	Magyar
Polgári	Polgár	k1gFnSc2	Polgár
Szövetség	Szövetség	k1gMnSc1	Szövetség
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
premiérů	premiér	k1gMnPc2	premiér
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Viktora	Viktor	k1gMnSc2	Viktor
Orbána	Orbán	k1gMnSc2	Orbán
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
vláda	vláda	k1gFnSc1	vláda
Viktora	Viktor	k1gMnSc2	Viktor
Orbána	Orbán	k1gMnSc2	Orbán
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbána	k1gFnPc2	Orbána
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Orbán	Orbán	k1gMnSc1	Orbán
Viktor	Viktor	k1gMnSc1	Viktor
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fidesz	Fidesz	k1gInSc1	Fidesz
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
,	,	kIx,	,
profil	profil	k1gInSc1	profil
politika	politikum	k1gNnSc2	politikum
na	na	k7c4	na
Cevro	Cevro	k1gNnSc4	Cevro
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Portrét	portrét	k1gInSc1	portrét
na	na	k7c4	na
Csepel	Csepel	k1gInSc4	Csepel
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
</s>
</p>
