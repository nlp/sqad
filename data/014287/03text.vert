<s>
Globulation	Globulation	k1gInSc1
2	#num#	k4
</s>
<s>
Globulation	Globulation	k1gInSc1
2	#num#	k4
<g/>
Screenshot	Screenshota	k1gFnPc2
ze	z	k7c2
hryZákladní	hryZákladný	k2eAgMnPc1d1
informaceVývojářGlobulation	informaceVývojářGlobulation	k1gInSc4
2	#num#	k4
týmPlatformyWindows	týmPlatformyWindows	k1gInSc4
XP	XP	kA
<g/>
,	,	kIx,
Mac	Mac	kA
OS	OS	kA
X	X	kA
<g/>
,	,	kIx,
Linux	Linux	kA
(	(	kIx(
<g/>
multiplatformní	multiplatformní	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
Datum	datum	k1gInSc1
vydání	vydání	k1gNnSc1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2010	#num#	k4
<g/>
ŽánrRealtimová	ŽánrRealtimový	k2eAgNnPc1d1
strategieNěkterá	strategieNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Globulation	Globulation	k1gInSc1
2	#num#	k4
je	být	k5eAaImIp3nS
realtimová	realtimový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
<g/>
,	,	kIx,
lišící	lišící	k2eAgFnPc1d1
se	se	k3xPyFc4
od	od	k7c2
ostatním	ostatní	k2eAgMnPc2d1
svým	svůj	k3xOyFgNnSc7
pojetím	pojetí	k1gNnSc7
ovládání	ovládání	k1gNnSc2
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
řešeným	řešený	k2eAgNnSc7d1
automatickým	automatický	k2eAgNnSc7d1
seskupováním	seskupování	k1gNnSc7
jednotek	jednotka	k1gFnPc2
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
úlohám	úloha	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
přiřazuje	přiřazovat	k5eAaImIp3nS
jednotlivým	jednotlivý	k2eAgInPc3d1
úkolům	úkol	k1gInPc3
počet	počet	k1gInSc1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
ovlivňuje	ovlivňovat	k5eAaImIp3nS
třeba	třeba	k6eAd1
rychlost	rychlost	k1gFnSc4
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
sílu	síla	k1gFnSc4
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
hře	hra	k1gFnSc6
je	být	k5eAaImIp3nS
zastoupena	zastoupen	k2eAgFnSc1d1
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
rasa	rasa	k1gFnSc1
-	-	kIx~
globulové	globul	k1gMnPc1
<g/>
,	,	kIx,
se	s	k7c7
třemi	tři	k4xCgInPc7
typy	typ	k1gInPc7
jednotek	jednotka	k1gFnPc2
-	-	kIx~
pracovník	pracovník	k1gMnSc1
<g/>
,	,	kIx,
bojovník	bojovník	k1gMnSc1
a	a	k8xC
průzkumník	průzkumník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Globulation	Globulation	k1gInSc1
2	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Globulation	Globulation	k1gInSc1
2	#num#	k4
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
savannah	savannah	k1gInSc1
<g/>
.	.	kIx.
<g/>
nongnu	nongnout	k5eAaImIp1nS,k5eAaPmIp1nS
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
:	:	kIx,
Detaily	detail	k1gInPc1
projektu	projekt	k1gInSc2
Globulation	Globulation	k1gInSc4
2	#num#	k4
</s>
<s>
freshmeat	freshmeat	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
:	:	kIx,
Detaily	detail	k1gInPc1
projektu	projekt	k1gInSc2
Globulation	Globulation	k1gInSc1
2	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
