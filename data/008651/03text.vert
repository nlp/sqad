<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Srazil	srazit	k5eAaPmAgMnS	srazit
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Pohránov	Pohránov	k1gInSc1	Pohránov
u	u	k7c2	u
Dašic	Dašice	k1gInPc2	Dašice
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
pevnost	pevnost	k1gFnSc1	pevnost
Terezín	Terezín	k1gInSc1	Terezín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
příslušník	příslušník	k1gMnSc1	příslušník
výsadku	výsadek	k1gInSc2	výsadek
Antimony	antimon	k1gInPc1	antimon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Stanislav	Stanislav	k1gMnSc1	Stanislav
byl	být	k5eAaImAgMnS	být
obuvnický	obuvnický	k2eAgMnSc1d1	obuvnický
dělník	dělník	k1gMnSc1	dělník
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Emilie	Emilie	k1gFnSc1	Emilie
<g/>
,	,	kIx,	,
za	za	k7c2	za
svobodna	svoboden	k2eAgFnSc1d1	svobodna
Řeháková	Řeháková	k1gFnSc1	Řeháková
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
mladší	mladý	k2eAgFnPc4d2	mladší
sestry	sestra	k1gFnPc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
tříd	třída	k1gFnPc2	třída
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
Horních	horní	k2eAgFnPc6d1	horní
Ředicích	ředicí	k2eAgFnPc6d1	ředicí
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
třídy	třída	k1gFnPc4	třída
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Holicích	Holice	k1gFnPc6	Holice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
číšníkem	číšník	k1gMnSc7	číšník
v	v	k7c6	v
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
Rokycanech	Rokycany	k1gInPc6	Rokycany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
vyučení	vyučení	k1gNnSc6	vyučení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
exilu	exil	k1gInSc6	exil
==	==	k?	==
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
překročit	překročit	k5eAaPmF	překročit
státní	státní	k2eAgFnPc4d1	státní
hranice	hranice	k1gFnPc4	hranice
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
pokusil	pokusit	k5eAaPmAgMnS	pokusit
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1939	[number]	k4	1939
u	u	k7c2	u
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zadržen	zadržet	k5eAaPmNgInS	zadržet
polskými	polský	k2eAgMnPc7d1	polský
četníky	četník	k1gMnPc7	četník
a	a	k8xC	a
předán	předán	k2eAgInSc1d1	předán
gestapu	gestapo	k1gNnSc3	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Gestapem	gestapo	k1gNnSc7	gestapo
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
internován	internovat	k5eAaBmNgInS	internovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
podařil	podařit	k5eAaPmAgInS	podařit
projít	projít	k5eAaPmF	projít
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
podepsal	podepsat	k5eAaPmAgMnS	podepsat
závazek	závazek	k1gInSc4	závazek
ke	k	k7c3	k
službě	služba	k1gFnSc3	služba
v	v	k7c6	v
Cizinecké	cizinecký	k2eAgFnSc6d1	cizinecká
legii	legie	k1gFnSc6	legie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
vojín	vojín	k1gMnSc1	vojín
zařazen	zařazen	k2eAgMnSc1d1	zařazen
v	v	k7c6	v
Agde	Agde	k1gFnSc6	Agde
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
československé	československý	k2eAgFnSc2d1	Československá
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1940	[number]	k4	1940
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
svobodníka	svobodník	k1gMnSc4	svobodník
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
poddůstojnickou	poddůstojnický	k2eAgFnSc4d1	poddůstojnická
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
o	o	k7c6	o
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Francie	Francie	k1gFnSc2	Francie
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
evakuoval	evakuovat	k5eAaBmAgMnS	evakuovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
k	k	k7c3	k
2	[number]	k4	2
<g/>
.	.	kIx.	.
pěšímu	pěší	k1gMnSc3	pěší
praporu	prapor	k1gInSc2	prapor
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
desátníka	desátník	k1gMnSc4	desátník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1941	[number]	k4	1941
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
kurz	kurz	k1gInSc1	kurz
rotmistrů	rotmistr	k1gMnPc2	rotmistr
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1941	[number]	k4	1941
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
četaře	četař	k1gMnPc4	četař
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
prodělal	prodělat	k5eAaPmAgInS	prodělat
základní	základní	k2eAgInSc1d1	základní
sabotážní	sabotážní	k2eAgInSc1d1	sabotážní
kurz	kurz	k1gInSc1	kurz
<g/>
,	,	kIx,	,
paravýcvik	paravýcvik	k1gInSc1	paravýcvik
<g/>
,	,	kIx,	,
střelecký	střelecký	k2eAgInSc1d1	střelecký
a	a	k8xC	a
konspirační	konspirační	k2eAgInSc1d1	konspirační
kurz	kurz	k1gInSc1	kurz
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
výsadku	výsadek	k1gInSc2	výsadek
Antimony	antimon	k1gInPc1	antimon
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
prodělal	prodělat	k5eAaPmAgMnS	prodělat
těžký	těžký	k2eAgInSc4d1	těžký
výron	výron	k1gInSc4	výron
kotníku	kotník	k1gInSc2	kotník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
znovu	znovu	k6eAd1	znovu
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
příprav	příprava	k1gFnPc2	příprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nasazení	nasazení	k1gNnSc1	nasazení
==	==	k?	==
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
Závorkou	závorka	k1gFnSc7	závorka
a	a	k8xC	a
Jasínkem	Jasínko	k1gNnSc7	Jasínko
vysazen	vysadit	k5eAaPmNgMnS	vysadit
u	u	k7c2	u
Kopidlna	Kopidlno	k1gNnSc2	Kopidlno
na	na	k7c6	na
Jičínsku	Jičínsko	k1gNnSc6	Jičínsko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
doskoku	doskok	k1gInSc6	doskok
si	se	k3xPyFc3	se
obnovil	obnovit	k5eAaPmAgMnS	obnovit
zranění	zranění	k1gNnSc4	zranění
kotníku	kotník	k1gInSc2	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
výsadku	výsadek	k1gInSc2	výsadek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
výsadku	výsadek	k1gInSc2	výsadek
Silver	Silver	k1gMnSc1	Silver
A.	A.	kA	A.
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ukrýval	ukrývat	k5eAaImAgMnS	ukrývat
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Kalné	Kalná	k1gFnSc6	Kalná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
konfidentské	konfidentský	k2eAgFnSc3d1	konfidentská
činnosti	činnost	k1gFnSc3	činnost
K.	K.	kA	K.
Čurdy	Čurd	k1gInPc4	Čurd
a	a	k8xC	a
J.	J.	kA	J.
Nachtmanna	Nachtmann	k1gInSc2	Nachtmann
byl	být	k5eAaImAgInS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1943	[number]	k4	1943
zatčen	zatknout	k5eAaPmNgInS	zatknout
gestapem	gestapo	k1gNnSc7	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
věznění	věznění	k1gNnSc6	věznění
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
a	a	k8xC	a
na	na	k7c6	na
Pankráci	Pankrác	k1gFnSc6	Pankrác
byl	být	k5eAaImAgInS	být
přinucen	přinutit	k5eAaPmNgInS	přinutit
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
na	na	k7c6	na
radiové	radiový	k2eAgFnSc6d1	radiová
protihře	protihra	k1gFnSc6	protihra
Hermelín	hermelín	k1gInSc4	hermelín
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
smluvených	smluvený	k2eAgInPc2d1	smluvený
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
centrálu	centrála	k1gFnSc4	centrála
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
varovat	varovat	k5eAaImF	varovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
celou	celá	k1gFnSc4	celá
německou	německý	k2eAgFnSc4d1	německá
operaci	operace	k1gFnSc4	operace
zmařit	zmařit	k5eAaPmF	zmařit
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
rotmistrem	rotmistr	k1gMnSc7	rotmistr
pěchoty	pěchota	k1gFnSc2	pěchota
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
a	a	k8xC	a
pořadím	pořadí	k1gNnSc7	pořadí
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
podporučíkem	podporučík	k1gMnSc7	podporučík
pěchoty	pěchota	k1gFnSc2	pěchota
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
a	a	k8xC	a
pořadím	pořadí	k1gNnSc7	pořadí
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jmenovitě	jmenovitě	k6eAd1	jmenovitě
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
památníku	památník	k1gInSc6	památník
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Ředicích	ředicí	k2eAgInPc6d1	ředicí
a	a	k8xC	a
na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
-	-	kIx~	-
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
1943	[number]	k4	1943
-	-	kIx~	-
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
1944	[number]	k4	1944
-	-	kIx~	-
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
se	s	k7c7	s
štítky	štítek	k1gInPc7	štítek
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
REICHL	REICHL	kA	REICHL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86808	[number]	k4	86808
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kmenový	kmenový	k2eAgInSc1d1	kmenový
list	list	k1gInSc1	list
</s>
</p>
