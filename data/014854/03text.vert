<s>
Projekt	projekt	k1gInSc1
12130	#num#	k4
</s>
<s>
Projekt	projekt	k1gInSc1
12130	#num#	k4
Admiral	Admiral	k1gFnSc2
KazakevičObecné	KazakevičObecný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
Typ	typ	k1gInSc1
</s>
<s>
hlídková	hlídkový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Lodě	loď	k1gFnSc2
</s>
<s>
4	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
aktivní	aktivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Výtlak	výtlak	k1gInSc4
</s>
<s>
91	#num#	k4
t	t	k?
(	(	kIx(
<g/>
standardní	standardní	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
98	#num#	k4
t	t	k?
(	(	kIx(
<g/>
plný	plný	k2eAgInSc4d1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
33,4	33,4	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
4,2	4,2	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
0,81	0,81	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
2	#num#	k4
diesely	diesel	k1gInPc7
Rychlost	rychlost	k1gFnSc1
</s>
<s>
25,2	25,2	k4
uzlu	uzel	k1gInSc2
Dosah	dosah	k1gInSc1
</s>
<s>
270	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
při	při	k7c6
20	#num#	k4
uzlech	uzel	k1gInPc6
Posádka	posádka	k1gFnSc1
</s>
<s>
17	#num#	k4
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
PSKR-	PSKR-	k1gFnPc2
<g/>
200	#num#	k4
<g/>
,	,	kIx,
PSKR-	PSKR-	k1gFnSc1
<g/>
203	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
×	×	k?
30	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
AK-306	AK-306	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
<g/>
×	×	k?
7,62	7,62	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
</s>
<s>
Projekt	projekt	k1gInSc1
12130	#num#	k4
(	(	kIx(
<g/>
jinak	jinak	k6eAd1
též	též	k9
třída	třída	k1gFnSc1
Ogonek	Ogonky	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třída	třída	k1gFnSc1
říčních	říční	k2eAgFnPc2d1
hlídkových	hlídkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
ruské	ruský	k2eAgFnSc2d1
pobřežní	pobřežní	k2eAgFnSc2d1
stráže	stráž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jejich	jejich	k3xOp3gNnSc4
úkoly	úkol	k1gInPc1
patří	patřit	k5eAaImIp3nP
hlídkování	hlídkování	k1gNnSc4
na	na	k7c6
řekách	řeka	k1gFnPc6
<g/>
,	,	kIx,
ničení	ničení	k1gNnSc3
nepřátelského	přátelský	k2eNgInSc2d1
personálu	personál	k1gInSc2
<g/>
,	,	kIx,
vybavení	vybavení	k1gNnSc2
a	a	k8xC
palebných	palebný	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
a	a	k8xC
poskytování	poskytování	k1gNnSc6
palebné	palebný	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
pobřežním	pobřežní	k2eAgFnPc3d1
silám	síla	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgInPc1d1
čtyři	čtyři	k4xCgFnPc1
jednotky	jednotka	k1gFnPc1
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Plavidlo	plavidlo	k1gNnSc4
navrhla	navrhnout	k5eAaPmAgFnS
konstrukční	konstrukční	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
Zelenodolsk	Zelenodolsk	k1gInSc1
PKB	PKB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavila	postavit	k5eAaPmAgFnS
je	on	k3xPp3gNnSc4
ruská	ruský	k2eAgFnSc1d1
loděnice	loděnice	k1gFnSc1
Khabarovsk	Khabarovsk	k1gInSc4
Shipbuilding	Shipbuilding	k1gInSc1
Yard	yard	k1gInSc1
v	v	k7c6
Chabarovsku	Chabarovsk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotky	jednotka	k1gFnPc4
projektu	projekt	k1gInSc2
12130	#num#	k4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JménoZaložení	JménoZaložení	k1gNnSc4
kýluSpuštěnaVstup	kýluSpuštěnaVstup	k1gInSc1
do	do	k7c2
službyStatus	službyStatus	k1gInSc4
</s>
<s>
Admiral	Admirat	k5eAaPmAgInS,k5eAaImAgInS
Kazakevič	Kazakevič	k1gInSc4
<g/>
1998	#num#	k4
<g/>
původní	původní	k2eAgInSc4d1
název	název	k1gInSc4
jako	jako	k8xC,k8xS
PSKR-	PSKR-	k1gFnSc4
<g/>
200	#num#	k4
<g/>
,	,	kIx,
aktivní	aktivní	k2eAgMnSc1d1
</s>
<s>
PSKR-	PSKR-	k?
<g/>
2018	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001	#num#	k4
<g/>
aktivní	aktivní	k2eAgFnSc4d1
</s>
<s>
PSKR-	PSKR-	k?
<g/>
2023	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
20068	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
<g/>
aktivní	aktivní	k2eAgFnSc4d1
</s>
<s>
PSKR-	PSKR-	k?
<g/>
20319944	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
<g/>
aktivní	aktivní	k2eAgFnSc1d1
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Plavidla	plavidlo	k1gNnPc1
konstrukčně	konstrukčně	k6eAd1
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
říčních	říční	k2eAgFnPc2d1
hlídkových	hlídkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
projektu	projekt	k1gInSc2
1249	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
a	a	k8xC
čtvrtá	čtvrtá	k1gFnSc1
projektu	projekt	k1gInSc2
12130	#num#	k4
nesou	nést	k5eAaImIp3nP
dva	dva	k4xCgInPc1
rotační	rotační	k2eAgInPc1d1
30	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
AK-306	AK-306	k1gFnSc2
a	a	k8xC
jeden	jeden	k4xCgInSc1
7,62	7,62	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
a	a	k8xC
třetí	třetí	k4xOgFnSc1
jednotka	jednotka	k1gFnSc1
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
nesou	nést	k5eAaImIp3nP
jeden	jeden	k4xCgMnSc1
rotační	rotační	k2eAgInSc1d1
30	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
AK-	AK-	k1gFnPc2
<g/>
306	#num#	k4
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
12,7	12,7	k4
<g/>
mm	mm	kA
dvojkulomet	dvojkulomet	k1gInSc4
Utes-M	Utes-M	k1gFnSc7
a	a	k8xC
jeden	jeden	k4xCgMnSc1
7,62	7,62	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzbroj	výzbroj	k1gInSc1
všech	všecek	k3xTgNnPc2
plavidel	plavidlo	k1gNnPc2
doplňuje	doplňovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
přenosný	přenosný	k2eAgInSc4d1
protiletadlový	protiletadlový	k2eAgInSc4d1
raketový	raketový	k2eAgInSc4d1
komplet	komplet	k1gInSc4
Igla-M	Igla-M	k1gFnSc2
či	či	k8xC
Verba	verbum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
dva	dva	k4xCgInPc4
diesely	diesel	k1gInPc4
Zvezda	Zvezdo	k1gNnSc2
M	M	kA
<g/>
401	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
1100	#num#	k4
hp	hp	k?
<g/>
,	,	kIx,
pohánějící	pohánějící	k2eAgInPc4d1
dva	dva	k4xCgInPc4
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
s	s	k7c7
pevnými	pevný	k2eAgFnPc7d1
lopatkami	lopatka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
25,2	25,2	k4
uzlu	uzel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autonomie	autonomie	k1gFnSc1
je	být	k5eAaImIp3nS
6	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
je	být	k5eAaImIp3nS
270	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
20	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Project	Projecta	k1gFnPc2
12130	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russianships	Russianships	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
PSKR-200	PSKR-200	k4
border	border	k1gMnSc1
guard	guard	k1gMnSc1
boats	boats	k1gInSc1
(	(	kIx(
<g/>
project	project	k1gInSc1
12130	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navypedia	Navypedium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
