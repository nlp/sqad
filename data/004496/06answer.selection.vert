<s>
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
český	český	k2eAgInSc1d1	český
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgNnSc4d1	středověké
hradiště	hradiště	k1gNnSc4	hradiště
<g/>
)	)	kIx)	)
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrohu	ostroh	k1gInSc6	ostroh
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Vltavou	Vltava	k1gFnSc7	Vltava
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Opyš	Opyš	k1gInSc1	Opyš
<g/>
.	.	kIx.	.
</s>
