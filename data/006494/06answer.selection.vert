<s>
Nemůže	moct	k5eNaImIp3nS	moct
dobře	dobře	k6eAd1	dobře
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
nenaučil	naučit	k5eNaPmAgMnS	naučit
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1277	[number]	k4	1277
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Základem	základ	k1gInSc7	základ
života	život	k1gInSc2	život
v	v	k7c6	v
míru	mír	k1gInSc6	mír
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
majetku	majetek	k1gInSc2	majetek
<g/>
;	;	kIx,	;
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
odmítá	odmítat	k5eAaImIp3nS	odmítat
Platónův	Platónův	k2eAgInSc1d1	Platónův
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
lidé	člověk	k1gMnPc1	člověk
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
všechno	všechen	k3xTgNnSc4	všechen
společné	společný	k2eAgNnSc4d1	společné
<g/>
.	.	kIx.	.
</s>
