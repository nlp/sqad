<s>
Džem	džem	k1gInSc1
</s>
<s>
O	o	k7c6
osmanském	osmanský	k2eAgInSc6d1
uchazeči	uchazeč	k1gMnPc7
o	o	k7c4
trůn	trůn	k1gInSc4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Džem	džem	k1gInSc1
(	(	kIx(
<g/>
princ	princ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Džem	džem	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sklenice	sklenice	k1gFnPc1
s	s	k7c7
džemem	džem	k1gInSc7
</s>
<s>
Džem	džem	k1gInSc1
je	být	k5eAaImIp3nS
rosolovitá	rosolovitý	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
s	s	k7c7
kousky	kousek	k1gInPc7
ovoce	ovoce	k1gNnSc2
nebo	nebo	k8xC
s	s	k7c7
celými	celý	k2eAgInPc7d1
drobnými	drobný	k2eAgInPc7d1
plody	plod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
čerstvého	čerstvý	k2eAgNnSc2d1
vyzrálého	vyzrálý	k2eAgNnSc2d1
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
lisuje	lisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
vaří	vařit	k5eAaImIp3nS
a	a	k8xC
zahušťuje	zahušťovat	k5eAaImIp3nS
cukrem	cukr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvyklé	obvyklý	k2eAgInPc1d1
množství	množství	k1gNnSc4
cukru	cukr	k1gInSc2
činí	činit	k5eAaImIp3nS
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
až	až	k9
2	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
obsahu	obsah	k1gInSc2
<g/>
,	,	kIx,
obsah	obsah	k1gInSc1
ovoce	ovoce	k1gNnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
minimálně	minimálně	k6eAd1
35	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Džem	džem	k1gInSc1
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
používá	používat	k5eAaImIp3nS
např.	např.	kA
na	na	k7c4
chléb	chléb	k1gInSc4
s	s	k7c7
máslem	máslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
označení	označení	k1gNnSc1
je	být	k5eAaImIp3nS
počeštěná	počeštěný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
anglického	anglický	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
jam	jáma	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
v	v	k7c6
podobě	podoba	k1gFnSc6
podstatného	podstatný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
tak	tak	k8xC,k8xS
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
mj.	mj.	kA
ve	v	k7c6
významu	význam	k1gInSc6
zahustit	zahustit	k5eAaPmF
(	(	kIx(
<g/>
zřejmě	zřejmě	k6eAd1
od	od	k7c2
zahuštění	zahuštění	k1gNnSc2
cukrem	cukr	k1gInSc7
při	při	k7c6
přípravě	příprava	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
marmalade	marmalad	k1gInSc5
v	v	k7c6
angličtině	angličtina	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
jen	jen	k9
pochutiny	pochutina	k1gFnPc4
z	z	k7c2
citrusových	citrusový	k2eAgInPc2d1
plodů	plod	k1gInPc2
(	(	kIx(
<g/>
jam	jáma	k1gFnPc2
pak	pak	k6eAd1
pro	pro	k7c4
ostatní	ostatní	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
smí	smět	k5eAaImIp3nS
být	být	k5eAaImF
počeštěné	počeštěný	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
marmeláda	marmeláda	k1gFnSc1
používáno	používán	k2eAgNnSc4d1
jen	jen	k6eAd1
pro	pro	k7c4
odpovídající	odpovídající	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
džemu	džem	k1gInSc2
</s>
<s>
Džem	džem	k1gInSc1
lze	lze	k6eAd1
snadno	snadno	k6eAd1
připravit	připravit	k5eAaPmF
téměř	téměř	k6eAd1
z	z	k7c2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
ovoce	ovoce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovoce	ovoce	k1gNnSc2
omyjeme	omýt	k5eAaPmIp1nP
<g/>
,	,	kIx,
případně	případně	k6eAd1
odpeckujeme	odpeckovat	k5eAaPmIp1nP
a	a	k8xC
nakrájíme	nakrájet	k5eAaPmIp1nP
na	na	k7c4
kousky	kousek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
smícháme	smíchat	k5eAaPmIp1nP
s	s	k7c7
cukrem	cukr	k1gInSc7
nebo	nebo	k8xC
s	s	k7c7
cukrem	cukr	k1gInSc7
a	a	k8xC
komerčním	komerční	k2eAgInSc7d1
želírovacím	želírovací	k2eAgInSc7d1
přípravkem	přípravek	k1gInSc7
<g/>
,	,	kIx,
uvedeme	uvést	k5eAaPmIp1nP
směs	směs	k1gFnSc4
k	k	k7c3
varu	var	k1gInSc3
a	a	k8xC
vaříme	vařit	k5eAaImIp1nP
dokud	dokud	k8xS
nezhoustne	zhoustnout	k5eNaPmIp3nS
(	(	kIx(
<g/>
s	s	k7c7
komerčními	komerční	k2eAgInPc7d1
želírovacími	želírovací	k2eAgInPc7d1
přípravky	přípravek	k1gInPc7
cca	cca	kA
za	za	k7c4
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
naplněním	naplnění	k1gNnSc7
džemu	džem	k1gInSc2
do	do	k7c2
sklenic	sklenice	k1gFnPc2
bychom	by	kYmCp1nP
měli	mít	k5eAaImAgMnP
provést	provést	k5eAaPmF
tzv.	tzv.	kA
želírovací	želírovací	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
horký	horký	k2eAgInSc4d1
džem	džem	k1gInSc4
kápneme	kápnout	k5eAaPmIp1nP
na	na	k7c4
talířek	talířek	k1gInSc4
a	a	k8xC
pokud	pokud	k8xS
vzorek	vzorek	k1gInSc1
džemu	džem	k1gInSc2
ztuhne	ztuhnout	k5eAaPmIp3nS
(	(	kIx(
<g/>
cca	cca	kA
za	za	k7c4
5	#num#	k4
min	mina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
ztuhne	ztuhnout	k5eAaPmIp3nS
i	i	k9
zbytek	zbytek	k1gInSc4
džemu	džem	k1gInSc2
nebo	nebo	k8xC
marmelády	marmeláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
si	se	k3xPyFc3
přejeme	přát	k5eAaImIp1nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
hmota	hmota	k1gFnSc1
ztuhla	ztuhnout	k5eAaPmAgFnS
více	hodně	k6eAd2
<g/>
,	,	kIx,
zamícháme	zamíchat	k5eAaPmIp1nP
do	do	k7c2
horké	horký	k2eAgFnSc2d1
směsi	směs	k1gFnSc2
kyselinu	kyselina	k1gFnSc4
citrónovou	citrónový	k2eAgFnSc4d1
(	(	kIx(
<g/>
1	#num#	k4
-	-	kIx~
2	#num#	k4
zarovnané	zarovnaný	k2eAgFnSc2d1
lžičky	lžička	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
želírovací	želírovací	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
opakujeme	opakovat	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
uvařeného	uvařený	k2eAgInSc2d1
džemu	džem	k1gInSc2
je	být	k5eAaImIp3nS
také	také	k9
před	před	k7c7
plněním	plnění	k1gNnSc7
do	do	k7c2
sklenic	sklenice	k1gFnPc2
dobré	dobrá	k1gFnSc2
sebrat	sebrat	k5eAaPmF
vzniklou	vzniklý	k2eAgFnSc4d1
pěnu	pěna	k1gFnSc4
<g/>
,	,	kIx,
bublinky	bublinka	k1gFnPc4
vzduchu	vzduch	k1gInSc2
v	v	k7c6
ní	on	k3xPp3gFnSc6
obsažené	obsažený	k2eAgFnPc1d1
totiž	totiž	k9
mohou	moct	k5eAaImIp3nP
poškodit	poškodit	k5eAaPmF
vzhled	vzhled	k1gInSc4
a	a	k8xC
trvanlivost	trvanlivost	k1gFnSc4
zavařeniny	zavařenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kvalita	kvalita	k1gFnSc1
džemu	džem	k1gInSc2
</s>
<s>
Kvalita	kvalita	k1gFnSc1
džemu	džem	k1gInSc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
dána	dán	k2eAgFnSc1d1
kvalitou	kvalita	k1gFnSc7
a	a	k8xC
podílem	podíl	k1gInSc7
ovocné	ovocný	k2eAgFnSc2d1
složky	složka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgInPc2
průmyslově	průmyslově	k6eAd1
vyráběných	vyráběný	k2eAgInPc2d1
džemů	džem	k1gInPc2
je	být	k5eAaImIp3nS
z	z	k7c2
důvodů	důvod	k1gInPc2
cenových	cenový	k2eAgInPc2d1
tlaků	tlak	k1gInPc2
podíl	podíl	k1gInSc1
cukru	cukr	k1gInSc2
daleko	daleko	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
podíl	podíl	k1gInSc1
ovocné	ovocný	k2eAgFnSc2d1
složky	složka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
výrobci	výrobce	k1gMnPc1
u	u	k7c2
džemů	džem	k1gInPc2
deklarovaných	deklarovaný	k2eAgInPc2d1
jako	jako	k8xC,k8xS
jahodové	jahodový	k2eAgInPc1d1
či	či	k8xC
borůvkové	borůvkový	k2eAgInPc1d1
nahrazují	nahrazovat	k5eAaImIp3nP
originální	originální	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
levnějšími	levný	k2eAgNnPc7d2
jablky	jablko	k1gNnPc7
a	a	k8xC
do	do	k7c2
výrobku	výrobek	k1gInSc2
přidávají	přidávat	k5eAaImIp3nP
dochucovadla	dochucovadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Džem	džem	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
minimálně	minimálně	k6eAd1
35	#num#	k4
%	%	kIx~
ovoce	ovoce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
ve	v	k7c6
směsi	směs	k1gFnSc6
45	#num#	k4
%	%	kIx~
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
džem	džem	k1gInSc1
výběrový	výběrový	k2eAgInSc1d1
nebo	nebo	k8xC
džem	džem	k1gInSc1
extra	extra	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Směrnice	směrnice	k1gFnSc1
Rady	rada	k1gFnSc2
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
113	#num#	k4
ES	es	k1gNnPc2
<g/>
,	,	kIx,
Příloha	příloha	k1gFnSc1
1	#num#	k4
<g/>
↑	↑	k?
Trendy	trend	k1gInPc1
zdraví	zdraví	k1gNnSc1
<g/>
:	:	kIx,
Džem	džem	k1gInSc1
není	být	k5eNaImIp3nS
marmeláda	marmeláda	k1gFnSc1
<g/>
↑	↑	k?
HAVELKOVÁ	Havelková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
KLIMENTOVÁ	Klimentová	k1gFnSc1
<g/>
,	,	kIx,
Maryna	Maryna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konservování	konservování	k1gNnSc3
ovoce	ovoce	k1gNnSc2
a	a	k8xC
zeleniny	zelenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
BŘÍZOVÁ	Břízová	k1gFnSc1
<g/>
,	,	kIx,
Joza	Joza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaříme	vařit	k5eAaImIp1nP
zdravě	zdravě	k6eAd1
<g/>
,	,	kIx,
chutně	chutně	k6eAd1
a	a	k8xC
hospodárně	hospodárně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vydavatelství	vydavatelství	k1gNnSc1
a	a	k8xC
nakladatelství	nakladatelství	k1gNnSc1
ROH	roh	k1gInSc1
–	–	k?
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
511	#num#	k4
<g/>
,	,	kIx,
515	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Zavařenina	zavařenina	k1gFnSc1
</s>
<s>
Želé	želé	k1gNnSc1
</s>
<s>
Povidla	povidla	k1gNnPc1
</s>
<s>
Marmeláda	marmeláda	k1gFnSc1
</s>
<s>
Kompot	kompot	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
džem	džem	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
džem	džem	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vitalia	Vitalia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Džem	džem	k1gInSc1
Jahůdka	jahůdka	k1gFnSc1
nebo	nebo	k8xC
Borůvka	Borůvka	k1gMnSc1
nebo	nebo	k8xC
<g/>
...	...	k?
-	-	kIx~
stejně	stejně	k6eAd1
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
hlavně	hlavně	k9
jablka	jablko	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anna	Anna	k1gFnSc1
Příhodová	Příhodová	k1gFnSc1
<g/>
,	,	kIx,
30.01	30.01	k4
<g/>
.2012	.2012	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4123548-4	4123548-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85069302	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85069302	#num#	k4
</s>
