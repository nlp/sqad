<s>
Kukmák	kukmák	k1gInSc1
dřevní	dřevní	k2eAgFnSc2d1
</s>
<s>
Kukmák	kukmák	k1gInSc1
dřevní	dřevní	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
houby	houba	k1gFnPc1
(	(	kIx(
<g/>
Fungi	Fungi	k1gNnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
stopkovýtrusé	stopkovýtrusý	k2eAgNnSc1d1
(	(	kIx(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1
<g/>
)	)	kIx)
Pododdělení	pododdělení	k1gNnSc1
</s>
<s>
Agaricomycotina	Agaricomycotina	k1gFnSc1
Třída	třída	k1gFnSc1
</s>
<s>
Agaricomycetes	Agaricomycetes	k1gInSc1
Řád	řád	k1gInSc1
</s>
<s>
lupenotvaré	lupenotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Agaricales	Agaricales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
štítovkovité	štítovkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Pluteaceae	Pluteacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
kukmák	kukmák	k1gInSc1
(	(	kIx(
<g/>
Volvariella	Volvariella	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Volvariella	Volvariella	k1gFnSc1
caesiotinctaP	caesiotinctaP	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
Orton	Orton	k1gInSc4
1974	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kukmák	kukmák	k1gInSc1
dřevní	dřevní	k2eAgInSc1d1
(	(	kIx(
<g/>
Volvariella	Volvariella	k1gMnSc1
caesiotincta	caesiotinct	k1gMnSc2
P.	P.	kA
D.	D.	kA
Orton	Orton	k1gInSc4
1974	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vzácná	vzácný	k2eAgFnSc1d1
saprotrofní	saprotrofní	k2eAgFnSc1d1
houba	houba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
štítovkovitých	štítovkovitý	k2eAgMnPc2d1
(	(	kIx(
<g/>
Pluteacea	Pluteacea	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
zákonem	zákon	k1gInSc7
chráněná	chráněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nejedlý	jedlý	k2eNgInSc4d1
druh	druh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Makroskopický	makroskopický	k2eAgInSc1d1
</s>
<s>
Klobouk	klobouk	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
40	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vyklenutý	vyklenutý	k2eAgInSc1d1
až	až	k6eAd1
plochý	plochý	k2eAgInSc1d1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
tupým	tupý	k2eAgInSc7d1
hrbolem	hrbol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
matný	matný	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
paprsčitě	paprsčitě	k6eAd1
vláknitou	vláknitý	k2eAgFnSc4d1
kresbu	kresba	k1gFnSc4
a	a	k8xC
namodrale	namodrale	k6eAd1
šedé	šedý	k2eAgNnSc4d1
až	až	k8xS
šedočerné	šedočerný	k2eAgNnSc4d1
zbarvení	zbarvení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okraj	okraj	k1gInSc1
bývá	bývat	k5eAaImIp3nS
výrazněji	výrazně	k6eAd2
tmavě	tmavě	k6eAd1
vláknitý	vláknitý	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lupeny	lupen	k1gInPc1
odsedlé	odsedlý	k2eAgInPc1d1
<g/>
,	,	kIx,
v	v	k7c6
mládí	mládí	k1gNnSc6
mají	mít	k5eAaImIp3nP
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
u	u	k7c2
zralých	zralý	k2eAgFnPc2d1
plodnic	plodnice	k1gFnPc2
jsou	být	k5eAaImIp3nP
masově	masově	k6eAd1
růžové	růžový	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třeň	třeň	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
40	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
×	×	k?
4	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
válcovitý	válcovitý	k2eAgMnSc1d1
na	na	k7c6
bázi	báze	k1gFnSc6
kyjovitě	kyjovitě	k6eAd1
rozšířený	rozšířený	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
mladých	mladý	k2eAgFnPc2d1
plodnic	plodnice	k1gFnPc2
je	být	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
a	a	k8xC
až	až	k9
krémový	krémový	k2eAgInSc4d1
<g/>
,	,	kIx,
u	u	k7c2
starších	starší	k1gMnPc2
nažloutlý	nažloutlý	k2eAgMnSc1d1
až	až	k9
nahnědlý	nahnědlý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrch	povrch	k1gInSc1
v	v	k7c6
mládí	mládí	k1gNnSc6
kryjí	krýt	k5eAaImIp3nP
vločky	vločka	k1gFnPc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
je	být	k5eAaImIp3nS
vláknitý	vláknitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bázi	báze	k1gFnSc6
halí	halit	k5eAaImIp3nS
nápadná	nápadný	k2eAgFnSc1d1
šedá	šedý	k2eAgFnSc1d1
pochva	pochva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dužnina	dužnina	k1gFnSc1
je	být	k5eAaImIp3nS
cítit	cítit	k5eAaImF
po	po	k7c6
listech	list	k1gInPc6
muškátu	muškát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mikroskopický	mikroskopický	k2eAgInSc1d1
</s>
<s>
Výtrusný	výtrusný	k2eAgInSc1d1
prach	prach	k1gInSc1
je	být	k5eAaImIp3nS
hnědorůžový	hnědorůžový	k2eAgMnSc1d1
Spóry	spóra	k1gFnPc4
mají	mít	k5eAaImIp3nP
široce	široko	k6eAd1
eliptický	eliptický	k2eAgInSc4d1
až	až	k8xS
mírně	mírně	k6eAd1
hranatý	hranatý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
dosahují	dosahovat	k5eAaImIp3nP
5,5	5,5	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
×	×	k?
3,5	3,5	k4
<g/>
–	–	k?
<g/>
5,5	5,5	k4
μ	μ	k1gFnPc2
<g/>
,	,	kIx,
cystidy	cystid	k1gInPc1
bývají	bývat	k5eAaImIp3nP
vybavené	vybavený	k2eAgInPc1d1
prstovitým	prstovitý	k2eAgInSc7d1
až	až	k9
rozvětveným	rozvětvený	k2eAgInSc7d1
výrůstkem	výrůstek	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Roste	růst	k5eAaImIp3nS
saprotrofně	saprotrofně	k6eAd1
na	na	k7c6
tlejícím	tlející	k2eAgNnSc6d1
dřevu	dřevo	k1gNnSc6
-	-	kIx~
na	na	k7c6
zbytcích	zbytek	k1gInPc6
pařezů	pařez	k1gInPc2
<g/>
,	,	kIx,
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
zanořeného	zanořený	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
nebo	nebo	k8xC
opadu	opad	k1gInSc2
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
podílem	podíl	k1gInSc7
dřevní	dřevní	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
se	se	k3xPyFc4
v	v	k7c6
nížinách	nížina	k1gFnPc6
a	a	k8xC
středních	střední	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
horském	horský	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
jen	jen	k9
ojediněle	ojediněle	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Preferuje	preferovat	k5eAaImIp3nS
dubiny	dubina	k1gFnPc4
a	a	k8xC
bučiny	bučina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
dubu	dub	k1gInSc2
a	a	k8xC
buku	buk	k1gInSc2
roste	růst	k5eAaImIp3nS
i	i	k9
na	na	k7c6
dřevě	dřevo	k1gNnSc6
lípy	lípa	k1gFnSc2
<g/>
,	,	kIx,
břízy	bříza	k1gFnSc2
<g/>
,	,	kIx,
javoru	javor	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
jilmu	jilm	k1gInSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
habru	habr	k1gInSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dalších	další	k2eAgInPc2d1
listnáčů	listnáč	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácně	vzácně	k6eAd1
i	i	k9
na	na	k7c6
dřevě	dřevo	k1gNnSc6
smrku	smrk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Fruktifikuje	fruktifikovat	k5eAaBmIp3nS
od	od	k7c2
června	červen	k1gInSc2
do	do	k7c2
října	říjen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
První	první	k4xOgInSc1
publikovaný	publikovaný	k2eAgInSc1d1
nález	nález	k1gInSc1
z	z	k7c2
území	území	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byl	být	k5eAaImAgInS
učiněn	učinit	k5eAaImNgInS,k5eAaPmNgInS
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1983	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Českého	český	k2eAgInSc2d1
krasu	kras	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lokalita	lokalita	k1gFnSc1
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
těžbou	těžba	k1gFnSc7
vápence	vápenec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byly	být	k5eAaImAgInP
publikované	publikovaný	k2eAgInPc1d1
nálezy	nález	k1gInPc1
kukmáku	kukmák	k1gInSc2
dřevního	dřevní	k2eAgInSc2d1
mimo	mimo	k7c4
jiné	jiný	k2eAgMnPc4d1
z	z	k7c2
následujících	následující	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Čtvrtě	čtvrt	k1gFnPc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Beroun	Beroun	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kohoutov	Kohoutov	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Rokycany	Rokycany	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Medník	Medník	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Peklo	peklo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zátoňská	Zátoňský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
okres	okres	k1gInSc4
Prachatice	Prachatice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Ochrana	ochrana	k1gFnSc1
</s>
<s>
Kukmák	kukmák	k1gInSc1
dřevní	dřevní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
chráněný	chráněný	k2eAgInSc1d1
jakožto	jakožto	k8xS
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
podle	podle	k7c2
vyhlášky	vyhláška	k1gFnSc2
Ministerstva	ministerstvo	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
č.	č.	k?
395	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
zákona	zákon	k1gInSc2
114	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
ochraně	ochrana	k1gFnSc6
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
Červené	Červené	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
ohrožených	ohrožený	k2eAgInPc2d1
a	a	k8xC
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
SR	SR	kA
a	a	k8xC
ČR	ČR	kA
jako	jako	k8xC,k8xS
ohrožený	ohrožený	k2eAgInSc4d1
druh	druh	k1gInSc4
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
Červeném	červený	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
hub	houba	k1gFnPc2
(	(	kIx(
<g/>
makromycetů	makromycet	k1gMnPc2
<g/>
)	)	kIx)
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
jako	jako	k8xS,k8xC
zranitelný	zranitelný	k2eAgInSc1d1
druh	druh	k1gInSc1
(	(	kIx(
<g/>
VU	VU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
BERAN	Beran	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
HOLEC	holec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
hub	houba	k1gFnPc2
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Bielich	Bielicha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
624	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2077	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Volvariella	Volvariella	k1gFnSc1
caesiotincta	caesiotincta	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
366	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HAGARA	HAGARA	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ottova	Ottův	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
hub	houba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
1152	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7451	#num#	k4
<g/>
-	-	kIx~
<g/>
407	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
851	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
ZÍBAROVÁ	ZÍBAROVÁ	kA
<g/>
,	,	kIx,
Lucie	Lucie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volvariella	Volvariella	k1gFnSc1
caesiotincta	caesiotincta	k1gFnSc1
(	(	kIx(
<g/>
Kukmák	kukmák	k1gInSc1
dřevní	dřevní	k2eAgInSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mykologie	mykologie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
VÁGNER	Vágner	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volvariella	Volvariella	k1gFnSc1
caesiotincta	caesiotincto	k1gNnSc2
P.	P.	kA
D.	D.	kA
Orton	Orton	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
HOLEC	holec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
BERAN	Beran	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červený	červený	k2eAgInSc1d1
seznam	seznam	k1gInSc1
hub	houba	k1gFnPc2
(	(	kIx(
<g/>
makromycetů	makromycet	k1gMnPc2
<g/>
)	)	kIx)
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Příroda	příroda	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
FELLNER	FELLNER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
nálezy	nález	k1gInPc1
hub	houba	k1gFnPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
-	-	kIx~
Czechoslovak	Czechoslovak	k1gInSc1
records	records	k1gInSc1
-	-	kIx~
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volvariella	Volvariella	k1gMnSc1
caesiotincta	caesiotincta	k1gMnSc1
Orton	Orton	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
mykologie	mykologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Čs	čs	kA
<g/>
.	.	kIx.
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
476	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
107	#num#	k4
<g/>
-	-	kIx~
<g/>
109.1	109.1	k4
2	#num#	k4
KOTLABA	KOTLABA	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červená	červený	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
4	#num#	k4
<g/>
:	:	kIx,
ohrozených	ohrozený	k2eAgInPc2d1
a	a	k8xC
vzácnych	vzácnych	k1gInSc1
druhov	druhov	k1gInSc1
rastlín	rastlína	k1gFnPc2
a	a	k8xC
živočíchov	živočíchovo	k1gNnPc2
SR	SR	kA
a	a	k8xC
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Príroda	Príroda	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
220	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
735	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Pošvovec	Pošvovec	k1gInSc4
menší	malý	k2eAgInSc1d2
<g/>
,	,	kIx,
s.	s.	k?
80	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ANTONÍN	Antonín	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Some	Som	k1gInSc2
taxonomic	taxonomic	k1gMnSc1
and	and	k?
ecological	ecologicat	k5eAaPmAgMnS
notes	notes	k1gInSc4
on	on	k3xPp3gMnSc1
Volvariella	Volvariella	k1gMnSc1
caesitotincta	caesitotincta	k1gMnSc1
(	(	kIx(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1
<g/>
,	,	kIx,
Pluteaceae	Pluteaceae	k1gFnSc1
<g/>
)	)	kIx)
and	and	k?
its	its	k?
distribution	distribution	k1gInSc1
in	in	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acta	Act	k1gInSc2
Musei	museum	k1gNnPc7
Moraviae	Moravia	k1gInPc1
<g/>
,	,	kIx,
Scientiae	Scientia	k1gInPc1
biologicae	biologica	k1gInSc2
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
97	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
87	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
8788	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FELLNER	FELLNER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
nálezy	nález	k1gInPc1
hub	houba	k1gFnPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
-	-	kIx~
Czechoslovak	Czechoslovak	k1gInSc1
records	records	k1gInSc1
-	-	kIx~
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volvariella	Volvariella	k1gMnSc1
caesiotincta	caesiotincta	k1gMnSc1
Orton	Orton	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
mykologie	mykologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Čs	čs	kA
<g/>
.	.	kIx.
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
476	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
107	#num#	k4
<g/>
-	-	kIx~
<g/>
109	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ANTONÍN	Antonín	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Some	Som	k1gInSc2
taxonomic	taxonomic	k1gMnSc1
and	and	k?
ecological	ecologicat	k5eAaPmAgMnS
notes	notes	k1gInSc4
on	on	k3xPp3gMnSc1
Volvariella	Volvariella	k1gMnSc1
caesitotincta	caesitotincta	k1gMnSc1
(	(	kIx(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1
<g/>
,	,	kIx,
Pluteaceae	Pluteaceae	k1gFnSc1
<g/>
)	)	kIx)
and	and	k?
its	its	k?
distribution	distribution	k1gInSc1
in	in	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acta	Act	k1gInSc2
Musei	museum	k1gNnPc7
Moraviae	Moravia	k1gInPc1
<g/>
,	,	kIx,
Scientiae	Scientia	k1gInPc1
biologicae	biologica	k1gInSc2
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
97	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
87	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
8788	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Volvariella	Volvariella	k1gMnSc1
caesiotincta	caesiotincta	k1gMnSc1
(	(	kIx(
<g/>
kukmák	kukmák	k1gInSc1
dřevní	dřevní	k2eAgInSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Štítovkovité	Štítovkovitý	k2eAgNnSc1d1
Štítovka	Štítovka	k1gFnSc1
</s>
<s>
Štítovka	Štítovka	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
černolemá	černolemý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
drobná	drobný	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
droboučká	droboučký	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
huňatá	huňatý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
jelení	jelení	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
lemovaná	lemovaný	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
nízká	nízký	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
Pouzarova	Pouzarův	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
Romellova	Romellův	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
rozpraskaná	rozpraskaný	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
růžovonohá	růžovonohý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
síťnatá	síťnatý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
stinná	stinný	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
sametonohá	sametonohý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
šarlatová	šarlatový	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
šedohnědá	šedohnědý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
Thomsonova	Thomsonův	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
vločkatá	vločkatý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
vrbová	vrbový	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
žíhaná	žíhaný	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
žlutá	žlutý	k2eAgFnSc1d1
•	•	k?
Štítovka	Štítovka	k1gFnSc1
žlutozelenavá	žlutozelenavý	k2eAgFnSc1d1
Kukmák	kukmák	k1gInSc4
</s>
<s>
Kukmák	kukmák	k1gInSc4
bělovlnný	bělovlnný	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
cizopasný	cizopasný	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
dřevní	dřevní	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
maličký	maličký	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
myší	myší	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
okázalý	okázalý	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
sklepní	sklepní	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
smrkový	smrkový	k2eAgInSc4d1
•	•	k?
Kukmák	kukmák	k1gInSc4
Taylorův	Taylorův	k2eAgInSc4d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Houby	houba	k1gFnPc5
</s>
