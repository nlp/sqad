<s>
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
(	(	kIx(	(
<g/>
místně	místně	k6eAd1	místně
Velká	velký	k2eAgFnSc1d1	velká
Javorina	Javorina	k1gFnSc1	Javorina
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Javorina	Javorina	k1gFnSc1	Javorina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
na	na	k7c6	na
moravsko-slovenském	moravskolovenský	k2eAgNnSc6d1	moravsko-slovenské
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
970	[number]	k4	970
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
tohoto	tento	k3xDgNnSc2	tento
pohoří	pohoří	k1gNnSc2	pohoří
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
okresu	okres	k1gInSc2	okres
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>

