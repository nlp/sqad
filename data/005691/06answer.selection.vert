<s>
Kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
dutý	dutý	k2eAgInSc4d1	dutý
nepukavý	pukavý	k2eNgInSc4d1	nepukavý
plod	plod	k1gInSc4	plod
kokosovníku	kokosovník	k1gInSc2	kokosovník
ořechoplodého	ořechoplodý	k2eAgInSc2d1	ořechoplodý
s	s	k7c7	s
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
skořápkou	skořápka	k1gFnSc7	skořápka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc7d1	bílá
dužinou	dužina	k1gFnSc7	dužina
a	a	k8xC	a
kokosovou	kokosový	k2eAgFnSc7d1	kokosová
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
uvnitř	uvnitř	k7c2	uvnitř
<g/>
.	.	kIx.	.
</s>
