<s>
Kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
dutý	dutý	k2eAgInSc4d1	dutý
nepukavý	pukavý	k2eNgInSc4d1	nepukavý
plod	plod	k1gInSc4	plod
kokosovníku	kokosovník	k1gInSc2	kokosovník
ořechoplodého	ořechoplodý	k2eAgInSc2d1	ořechoplodý
s	s	k7c7	s
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
skořápkou	skořápka	k1gFnSc7	skořápka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc7d1	bílá
dužinou	dužina	k1gFnSc7	dužina
a	a	k8xC	a
kokosovou	kokosový	k2eAgFnSc7d1	kokosová
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
uvnitř	uvnitř	k7c2	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
2,5	[number]	k4	2,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
kokosových	kokosový	k2eAgFnPc6d1	kokosová
palmách	palma	k1gFnPc6	palma
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vydatný	vydatný	k2eAgInSc1d1	vydatný
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
sektoru	sektor	k1gInSc6	sektor
jako	jako	k9	jako
pochutina	pochutina	k1gFnSc1	pochutina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
lisování	lisování	k1gNnSc4	lisování
kokosového	kokosový	k2eAgInSc2d1	kokosový
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
plodů	plod	k1gInPc2	plod
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
kokosové	kokosový	k2eAgNnSc1d1	kokosové
textilní	textilní	k2eAgNnSc1d1	textilní
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Kokosová	kokosový	k2eAgFnSc1d1	kokosová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
kokosovým	kokosový	k2eAgNnSc7d1	kokosové
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Kokosové	kokosový	k2eAgNnSc1d1	kokosové
mléko	mléko	k1gNnSc1	mléko
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
lisováním	lisování	k1gNnSc7	lisování
buničiny	buničina	k1gFnSc2	buničina
a	a	k8xC	a
kokosová	kokosový	k2eAgFnSc1d1	kokosová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
uvnitř	uvnitř	k7c2	uvnitř
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
semen	semeno	k1gNnPc2	semeno
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
botanického	botanický	k2eAgNnSc2d1	botanické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
kokosovníku	kokosovník	k1gInSc2	kokosovník
peckovice	peckovice	k1gFnSc2	peckovice
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
ořech	ořech	k1gInSc1	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
dřevnaté	dřevnatý	k2eAgFnSc2d1	dřevnatá
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
odlupujícími	odlupující	k2eAgInPc7d1	odlupující
se	s	k7c7	s
svazky	svazek	k1gInPc7	svazek
lýka	lýko	k1gNnSc2	lýko
<g/>
.	.	kIx.	.
</s>
<s>
Lýko	lýko	k1gNnSc1	lýko
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
ořechu	ořech	k1gInSc6	ořech
jako	jako	k8xS	jako
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
po	po	k7c6	po
zeleném	zelený	k2eAgInSc6d1	zelený
obalu	obal	k1gInSc6	obal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
původně	původně	k6eAd1	původně
nacházel	nacházet	k5eAaImAgInS	nacházet
během	během	k7c2	během
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
skořápkou	skořápka	k1gFnSc7	skořápka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
bílá	bílý	k2eAgFnSc1d1	bílá
dužnina	dužnina	k1gFnSc1	dužnina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
60	[number]	k4	60
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
20	[number]	k4	20
%	%	kIx~	%
glycidů	glycid	k1gInPc2	glycid
<g/>
,	,	kIx,	,
8	[number]	k4	8
%	%	kIx~	%
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
6	[number]	k4	6
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
mladý	mladý	k2eAgInSc1d1	mladý
kokos	kokos	k1gInSc1	kokos
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
denní	denní	k2eAgFnSc2d1	denní
dávky	dávka	k1gFnSc2	dávka
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
2	[number]	k4	2
gramy	gram	k1gInPc7	gram
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
,	,	kIx,	,
stejného	stejný	k2eAgNnSc2d1	stejné
množství	množství	k1gNnSc2	množství
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
15	[number]	k4	15
gramů	gram	k1gInPc2	gram
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
3	[number]	k4	3
gramy	gram	k1gInPc4	gram
nenasyceného	nasycený	k2eNgInSc2d1	nenasycený
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
kokosu	kokos	k1gInSc2	kokos
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dutina	dutina	k1gFnSc1	dutina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ořechu	ořech	k1gInSc2	ořech
plavat	plavat	k5eAaImF	plavat
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
kokosovou	kokosový	k2eAgFnSc7d1	kokosová
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kokosová	kokosový	k2eAgFnSc1d1	kokosová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
vitamínu	vitamín	k1gInSc2	vitamín
B	B	kA	B
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
B6	B6	k1gFnSc2	B6
a	a	k8xC	a
B	B	kA	B
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitaminu	vitamin	k1gInSc2	vitamin
C.	C.	kA	C.
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
kokosového	kokosový	k2eAgInSc2d1	kokosový
ořechu	ořech	k1gInSc2	ořech
prochází	procházet	k5eAaImIp3nS	procházet
náročnou	náročný	k2eAgFnSc7d1	náročná
filtrací	filtrace	k1gFnSc7	filtrace
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
od	od	k7c2	od
kořenového	kořenový	k2eAgInSc2d1	kořenový
systému	systém	k1gInSc2	systém
do	do	k7c2	do
semene	semeno	k1gNnSc2	semeno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
sterilní	sterilní	k2eAgNnSc1d1	sterilní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
přirozené	přirozený	k2eAgInPc1d1	přirozený
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
infikované	infikovaný	k2eAgInPc1d1	infikovaný
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
znečištěné	znečištěný	k2eAgNnSc1d1	znečištěné
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
kokosová	kokosový	k2eAgFnSc1d1	kokosová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
i	i	k9	i
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
elektrolytů	elektrolyt	k1gInPc2	elektrolyt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kokosovník	kokosovník	k1gInSc1	kokosovník
ořechoplodý	ořechoplodý	k2eAgInSc1d1	ořechoplodý
<g/>
.	.	kIx.	.
</s>
<s>
Kokosové	kokosový	k2eAgInPc1d1	kokosový
ořechy	ořech	k1gInPc1	ořech
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kokosové	kokosový	k2eAgFnPc1d1	kokosová
palmy	palma	k1gFnPc1	palma
zvané	zvaný	k2eAgFnPc1d1	zvaná
kokosovník	kokosovník	k1gInSc4	kokosovník
ořechoplodý	ořechoplodý	k2eAgInSc1d1	ořechoplodý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
plody	plod	k1gInPc4	plod
až	až	k9	až
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	let	k1gInPc6	let
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vyrůst	vyrůst	k5eAaPmF	vyrůst
5	[number]	k4	5
až	až	k9	až
150	[number]	k4	150
plodů	plod	k1gInPc2	plod
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
až	až	k9	až
o	o	k7c4	o
třicet	třicet	k4xCc4	třicet
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
cíleně	cíleně	k6eAd1	cíleně
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
3	[number]	k4	3
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgInSc2	tento
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
může	moct	k5eAaImIp3nS	moct
vydržet	vydržet	k5eAaPmF	vydržet
plavat	plavat	k5eAaImF	plavat
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
až	až	k9	až
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
tedy	tedy	k9	tedy
se	se	k3xPyFc4	se
rozšířit	rozšířit	k5eAaPmF	rozšířit
po	po	k7c6	po
značném	značný	k2eAgNnSc6d1	značné
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
pevné	pevný	k2eAgFnSc2d1	pevná
skořápky	skořápka	k1gFnSc2	skořápka
kokosu	kokos	k1gInSc2	kokos
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
otvory	otvor	k1gInPc1	otvor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
specifickou	specifický	k2eAgFnSc4d1	specifická
funkci	funkce	k1gFnSc4	funkce
během	během	k7c2	během
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
klíčení	klíčení	k1gNnSc2	klíčení
semena	semeno	k1gNnSc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xS	jako
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
procházejí	procházet	k5eAaImIp3nP	procházet
vyživovací	vyživovací	k2eAgInPc4d1	vyživovací
cévní	cévní	k2eAgInPc4d1	cévní
svazky	svazek	k1gInPc4	svazek
do	do	k7c2	do
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
plodu	plod	k1gInSc2	plod
postupně	postupně	k6eAd1	postupně
zarůstají	zarůstat	k5eAaImIp3nP	zarůstat
pevnou	pevný	k2eAgFnSc7d1	pevná
a	a	k8xC	a
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
blánou	blána	k1gFnSc7	blána
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
díra	díra	k1gFnSc1	díra
je	být	k5eAaImIp3nS	být
kryta	kryt	k2eAgNnPc4d1	kryto
jen	jen	k6eAd1	jen
tenkou	tenký	k2eAgFnSc7d1	tenká
blánou	blána	k1gFnSc7	blána
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
prorážející	prorážející	k2eAgInSc4d1	prorážející
klíček	klíček	k1gInSc4	klíček
kokosu	kokos	k1gInSc2	kokos
<g/>
.	.	kIx.	.
</s>
<s>
Sušená	sušený	k2eAgFnSc1d1	sušená
dužnina	dužnina	k1gFnSc1	dužnina
z	z	k7c2	z
kokosu	kokos	k1gInSc2	kokos
–	–	k?	–
kopra	kopra	k1gFnSc1	kopra
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
sušený	sušený	k2eAgInSc4d1	sušený
kokos	kokos	k1gInSc4	kokos
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
dekorační	dekorační	k2eAgFnSc1d1	dekorační
a	a	k8xC	a
aromatická	aromatický	k2eAgFnSc1d1	aromatická
přísada	přísada	k1gFnSc1	přísada
na	na	k7c4	na
sladké	sladký	k2eAgFnPc4d1	sladká
pochutiny	pochutina	k1gFnPc4	pochutina
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
zákusky	zákuska	k1gFnPc4	zákuska
a	a	k8xC	a
dorty	dort	k1gInPc4	dort
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
celých	celý	k2eAgInPc2d1	celý
kokosových	kokosový	k2eAgInPc2d1	kokosový
plodů	plod	k1gInPc2	plod
také	také	k9	také
běžně	běžně	k6eAd1	běžně
prodává	prodávat	k5eAaImIp3nS	prodávat
kokos	kokos	k1gInSc1	kokos
strouhaný	strouhaný	k2eAgInSc1d1	strouhaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
a	a	k8xC	a
spíše	spíše	k9	spíše
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
prodejnách	prodejna	k1gFnPc6	prodejna
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
kokosovými	kokosový	k2eAgInPc7d1	kokosový
produkty	produkt	k1gInPc7	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kokosový	kokosový	k2eAgInSc4d1	kokosový
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
kokosové	kokosový	k2eAgNnSc1d1	kokosové
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
kokosová	kokosový	k2eAgFnSc1d1	kokosová
smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
kokosové	kokosový	k2eAgNnSc1d1	kokosové
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
kokosový	kokosový	k2eAgInSc1d1	kokosový
cukr	cukr	k1gInSc1	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Kokos	kokos	k1gInSc1	kokos
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
i	i	k9	i
kokosové	kokosový	k2eAgFnSc2d1	kokosová
mouky	mouka	k1gFnSc2	mouka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
potravinou	potravina	k1gFnSc7	potravina
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
používat	používat	k5eAaImF	používat
v	v	k7c6	v
tělových	tělový	k2eAgNnPc6d1	tělové
mlécích	mléko	k1gNnPc6	mléko
<g/>
,	,	kIx,	,
sprchových	sprchový	k2eAgInPc6d1	sprchový
gelech	gel	k1gInPc6	gel
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ve	v	k7c6	v
vůních	vůně	k1gFnPc6	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Kokosová	kokosový	k2eAgFnSc1d1	kokosová
palma	palma	k1gFnSc1	palma
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
přímořských	přímořský	k2eAgInPc6d1	přímořský
regionech	region	k1gInPc6	region
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Seychely	Seychely	k1gFnPc1	Seychely
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kokosový	kokosový	k2eAgInSc4d1	kokosový
ořech	ořech	k1gInSc4	ořech
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
