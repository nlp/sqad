<p>
<s>
Okoralka	Okoralka	k1gFnSc1	Okoralka
(	(	kIx(	(
<g/>
Berchemia	Berchemia	k1gFnSc1	Berchemia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
řešetlákovité	řešetlákovitý	k2eAgFnSc2d1	řešetlákovitý
(	(	kIx(	(
<g/>
Rhamnaceae	Rhamnacea	k1gFnSc2	Rhamnacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
holandského	holandský	k2eAgMnSc2d1	holandský
botanika	botanik	k1gMnSc2	botanik
Berthout	Berthout	k1gMnSc1	Berthout
van	van	k1gInSc1	van
Berchem	Berch	k1gInSc7	Berch
<g/>
.	.	kIx.	.
</s>
<s>
Okoralky	Okoralka	k1gFnPc1	Okoralka
jsou	být	k5eAaImIp3nP	být
popínavé	popínavý	k2eAgFnPc1d1	popínavá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
malé	malý	k2eAgFnPc1d1	malá
až	až	k9	až
středně	středně	k6eAd1	středně
vysoké	vysoký	k2eAgInPc4d1	vysoký
keře	keř	k1gInPc4	keř
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
okoralka	okoralka	k1gFnSc1	okoralka
Giraldova	Giraldův	k2eAgFnSc1d1	Giraldův
(	(	kIx(	(
<g/>
Berchemia	Berchemia	k1gFnSc1	Berchemia
giraldiana	giraldiana	k1gFnSc1	giraldiana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
okoralka	okoralka	k1gFnSc1	okoralka
hroznatá	hroznatý	k2eAgFnSc1d1	hroznatá
(	(	kIx(	(
<g/>
Berchemia	Berchemia	k1gFnSc1	Berchemia
racemosa	racemosa	k1gFnSc1	racemosa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
okoralka	okoralka	k1gFnSc1	okoralka
popínavá	popínavý	k2eAgFnSc1d1	popínavá
(	(	kIx(	(
<g/>
Berchemia	Berchemia	k1gFnSc1	Berchemia
scandens	scandensa	k1gFnPc2	scandensa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
okoralka	okoralka	k1gFnSc1	okoralka
žlutavá	žlutavý	k2eAgFnSc1d1	žlutavá
(	(	kIx(	(
<g/>
Berchemia	Berchemia	k1gFnSc1	Berchemia
flavescens	flavescensa	k1gFnPc2	flavescensa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vybrané	vybraný	k2eAgInPc1d1	vybraný
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Berchemia	Berchemium	k1gNnSc2	Berchemium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
okoralka	okoralka	k1gFnSc1	okoralka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Berchemia	Berchemium	k1gNnSc2	Berchemium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
taxonomie	taxonomie	k1gFnSc1	taxonomie
na	na	k7c4	na
www.biolib.cz	www.biolib.cz	k1gInSc4	www.biolib.cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
popis	popis	k1gInSc1	popis
na	na	k7c4	na
dendrologie	dendrologie	k1gFnPc4	dendrologie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
popis	popis	k1gInSc1	popis
na	na	k7c4	na
dendro	dendro	k6eAd1	dendro
<g/>
.	.	kIx.	.
<g/>
mojzisek	mojzisek	k1gInSc1	mojzisek
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
