<s>
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Zlín	Zlín	k1gInSc1	Zlín
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
16	[number]	k4	16
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Zlína	Zlín	k1gInSc2	Zlín
na	na	k7c6	na
říčce	říčka	k1gFnSc6	říčka
Šťávnice	Šťávnice	k1gFnSc2	Šťávnice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
katastrální	katastrální	k2eAgFnSc4d1	katastrální
výměru	výměra	k1gFnSc4	výměra
33	[number]	k4	33
km2	km2	k4	km2
(	(	kIx(	(
<g/>
3	[number]	k4	3
299	[number]	k4	299
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
099	[number]	k4	099
domů	dům	k1gInPc2	dům
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
5	[number]	k4	5
112	[number]	k4	112
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
jsou	být	k5eAaImIp3nP	být
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
největší	veliký	k2eAgFnPc1d3	veliký
lázně	lázeň	k1gFnPc1	lázeň
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
především	především	k9	především
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
nemocemi	nemoc	k1gFnPc7	nemoc
dýchacího	dýchací	k2eAgInSc2d1	dýchací
ústrojí	ústroj	k1gFnSc7	ústroj
<g/>
,	,	kIx,	,
trávením	trávení	k1gNnSc7	trávení
a	a	k8xC	a
obezitou	obezita	k1gFnSc7	obezita
<g/>
.	.	kIx.	.
</s>
<s>
Lázně	lázeň	k1gFnPc1	lázeň
vděčí	vděčit	k5eAaImIp3nP	vděčit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
věhlas	věhlas	k1gInSc4	věhlas
především	především	k9	především
minerálním	minerální	k2eAgInPc3d1	minerální
pramenům	pramen	k1gInPc3	pramen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
6	[number]	k4	6
přírodních	přírodní	k2eAgInPc2d1	přírodní
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
pramenů	pramen	k1gInPc2	pramen
navrtaných	navrtaný	k2eAgInPc2d1	navrtaný
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
a	a	k8xC	a
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
pramenem	pramen	k1gInSc7	pramen
Luhačovských	luhačovský	k2eAgFnPc2d1	Luhačovská
lázní	lázeň	k1gFnPc2	lázeň
je	být	k5eAaImIp3nS	být
pramen	pramen	k1gInSc4	pramen
Vincentka	Vincentka	k1gFnSc1	Vincentka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
stopy	stop	k1gInPc1	stop
slovanského	slovanský	k2eAgNnSc2d1	slovanské
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kraji	kraj	k1gInSc6	kraj
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgInP	doložit
již	již	k6eAd1	již
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
Oboře	obora	k1gFnSc6	obora
u	u	k7c2	u
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
<g/>
,	,	kIx,	,
na	na	k7c4	na
město	město	k1gNnSc4	město
byly	být	k5eAaImAgFnP	být
povýšeny	povýšit	k5eAaPmNgFnP	povýšit
teprve	teprve	k6eAd1	teprve
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgFnP	nazývat
Bad	Bad	k1gFnPc1	Bad
Luhatschowitz	Luhatschowitz	k1gInSc1	Luhatschowitz
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
říčky	říčka	k1gFnSc2	říčka
Šťávnice	Šťávnice	k1gFnSc2	Šťávnice
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Luhačovický	luhačovický	k2eAgInSc4d1	luhačovický
potok	potok	k1gInSc4	potok
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopen	k2eAgInPc1d1	obklopen
strmými	strmý	k2eAgInPc7d1	strmý
zalesněnými	zalesněný	k2eAgInPc7d1	zalesněný
kopci	kopec	k1gInPc7	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
centrem	centr	k1gMnSc7	centr
specifické	specifický	k2eAgFnSc2d1	specifická
přechodné	přechodný	k2eAgFnSc2d1	přechodná
národopisné	národopisný	k2eAgFnSc2d1	národopisná
oblasti	oblast	k1gFnSc2	oblast
Luhačovské	luhačovský	k2eAgNnSc1d1	Luhačovské
Zálesí	Zálesí	k1gNnSc1	Zálesí
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Valašska	Valašsko	k1gNnSc2	Valašsko
a	a	k8xC	a
Slovácka	Slovácko	k1gNnSc2	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
vrcholky	vrcholek	k1gInPc1	vrcholek
okolních	okolní	k2eAgInPc2d1	okolní
kopců	kopec	k1gInPc2	kopec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
nadmořské	nadmořský	k2eAgFnPc1d1	nadmořská
výšky	výška	k1gFnPc1	výška
maximálně	maximálně	k6eAd1	maximálně
672	[number]	k4	672
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
vrchol	vrcholit	k5eAaImRp2nS	vrcholit
Komonec	Komonec	k1gInSc1	Komonec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
patrný	patrný	k2eAgInSc1d1	patrný
podhorský	podhorský	k2eAgInSc1d1	podhorský
charakter	charakter	k1gInSc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
říčky	říčka	k1gFnSc2	říčka
Šťávnice	Šťávnice	k1gFnSc2	Šťávnice
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postavena	postaven	k2eAgFnSc1d1	postavena
přehrada	přehrada	k1gFnSc1	přehrada
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Luhačovická	luhačovický	k2eAgFnSc1d1	Luhačovická
nebo	nebo	k8xC	nebo
též	též	k9	též
Pozlovická	Pozlovický	k2eAgFnSc1d1	Pozlovická
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
první	první	k4xOgFnSc7	první
úlohou	úloha	k1gFnSc7	úloha
bylo	být	k5eAaImAgNnS	být
zadržovat	zadržovat	k5eAaImF	zadržovat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
záplavy	záplava	k1gFnPc4	záplava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pustošily	pustošit	k5eAaImAgFnP	pustošit
lázně	lázeň	k1gFnPc4	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
však	však	k9	však
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
rekreační	rekreační	k2eAgFnSc4d1	rekreační
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
rybářům	rybář	k1gMnPc3	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Rybářský	rybářský	k2eAgInSc1d1	rybářský
svaz	svaz	k1gInSc1	svaz
zde	zde	k6eAd1	zde
pořádá	pořádat	k5eAaImIp3nS	pořádat
jarní	jarní	k2eAgInSc1d1	jarní
a	a	k8xC	a
podzimní	podzimní	k2eAgInPc1d1	podzimní
rybářské	rybářský	k2eAgInPc1d1	rybářský
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
,	,	kIx,	,
vináren	vinárna	k1gFnPc2	vinárna
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
cukrářství	cukrářství	k1gNnPc2	cukrářství
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nejezdí	jezdit	k5eNaImIp3nP	jezdit
jen	jen	k9	jen
za	za	k7c7	za
léčením	léčení	k1gNnSc7	léčení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdejší	zdejší	k2eAgFnPc4d1	zdejší
podmínky	podmínka	k1gFnPc4	podmínka
uspokojí	uspokojit	k5eAaPmIp3nS	uspokojit
lidí	člověk	k1gMnPc2	člověk
turisticky	turisticky	k6eAd1	turisticky
<g/>
,	,	kIx,	,
sportovně	sportovně	k6eAd1	sportovně
i	i	k9	i
společensky	společensky	k6eAd1	společensky
založené	založený	k2eAgInPc1d1	založený
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
speciální	speciální	k2eAgInPc1d1	speciální
okruhy	okruh	k1gInPc1	okruh
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
s	s	k7c7	s
přírodní	přírodní	k2eAgFnSc3d1	přírodní
scenérii	scenérie	k1gFnSc3	scenérie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
příchodu	příchod	k1gInSc6	příchod
Slovanů	Slovan	k1gInPc2	Slovan
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
zříceniny	zřícenina	k1gFnSc2	zřícenina
hradů	hrad	k1gInPc2	hrad
nebo	nebo	k8xC	nebo
zámek	zámek	k1gInSc4	zámek
Serényiů	Serényi	k1gInPc2	Serényi
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
<g/>
,	,	kIx,	,
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
nebo	nebo	k8xC	nebo
lázeňské	lázeňský	k2eAgNnSc1d1	lázeňské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
ceněnou	ceněný	k2eAgFnSc4d1	ceněná
urbanistickou	urbanistický	k2eAgFnSc4d1	urbanistická
a	a	k8xC	a
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
tvář	tvář	k1gFnSc4	tvář
podpořenou	podpořený	k2eAgFnSc4d1	podpořená
i	i	k9	i
předpovězenou	předpovězená	k1gFnSc4	předpovězená
úchvatným	úchvatný	k2eAgInSc7d1	úchvatný
členitým	členitý	k2eAgInSc7d1	členitý
terénem	terén	k1gInSc7	terén
spojených	spojený	k2eAgNnPc2d1	spojené
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
architektonické	architektonický	k2eAgFnSc6d1	architektonická
podobě	podoba	k1gFnSc6	podoba
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
vynikající	vynikající	k2eAgMnPc1d1	vynikající
architekti	architekt	k1gMnPc1	architekt
Dušan	Dušan	k1gMnSc1	Dušan
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Králík	Králík	k1gMnSc1	Králík
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Poříska	Pořísk	k1gInSc2	Pořísk
<g/>
.	.	kIx.	.
</s>
<s>
Nesmazatelnou	smazatelný	k2eNgFnSc4d1	nesmazatelná
stopu	stopa	k1gFnSc4	stopa
zde	zde	k6eAd1	zde
dokonce	dokonce	k9	dokonce
zanechal	zanechat	k5eAaPmAgMnS	zanechat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pionýrů	pionýr	k1gMnPc2	pionýr
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
mezinárodně	mezinárodně	k6eAd1	mezinárodně
respektovaná	respektovaný	k2eAgFnSc1d1	respektovaná
osobnost	osobnost	k1gFnSc1	osobnost
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
Vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
pro	pro	k7c4	pro
Luhačovice	Luhačovice	k1gFnPc4	Luhačovice
jak	jak	k8xS	jak
několik	několik	k4yIc4	několik
územních	územní	k2eAgInPc2d1	územní
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
tak	tak	k9	tak
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
návrhu	návrh	k1gInSc2	návrh
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
penziony	penzion	k1gInPc1	penzion
Radun	Raduna	k1gFnPc2	Raduna
<g/>
,	,	kIx,	,
Iva	Iva	k1gFnSc1	Iva
<g/>
,	,	kIx,	,
Avion	avion	k1gInSc1	avion
a	a	k8xC	a
Viola	Viola	k1gFnSc1	Viola
a	a	k8xC	a
vila	vila	k1gFnSc1	vila
Sáva	Sáva	k1gFnSc1	Sáva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
483	[number]	k4	483
domech	dům	k1gInPc6	dům
2219	[number]	k4	2219
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
2204	[number]	k4	2204
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
4	[number]	k4	4
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
1973	[number]	k4	1973
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
18	[number]	k4	18
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
153	[number]	k4	153
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
9	[number]	k4	9
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Svaté	svatý	k2eAgFnSc2d1	svatá
rodiny	rodina	k1gFnSc2	rodina
Zámek	zámek	k1gInSc1	zámek
Luhačovice	Luhačovice	k1gFnPc4	Luhačovice
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
Kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Kaple	kaple	k1gFnSc2	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
Areál	areál	k1gInSc1	areál
lázní	lázeň	k1gFnPc2	lázeň
V	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgFnPc1d1	vzdělávací
instituce	instituce	k1gFnPc1	instituce
přidružená	přidružený	k2eAgNnPc1d1	přidružené
k	k	k7c3	k
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
síti	síť	k1gFnSc3	síť
škol	škola	k1gFnPc2	škola
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
studovat	studovat	k5eAaImF	studovat
jak	jak	k8xS	jak
obory	obor	k1gInPc4	obor
s	s	k7c7	s
maturitou	maturita	k1gFnSc7	maturita
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bez	bez	k7c2	bez
maturity	maturita	k1gFnSc2	maturita
<g/>
.	.	kIx.	.
</s>
<s>
Studijní	studijní	k2eAgInPc1d1	studijní
obory	obor	k1gInPc1	obor
s	s	k7c7	s
maturitní	maturitní	k2eAgFnSc7d1	maturitní
zkouškou	zkouška	k1gFnSc7	zkouška
jsou	být	k5eAaImIp3nP	být
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
na	na	k7c4	na
Management	management	k1gInSc4	management
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
Informační	informační	k2eAgInPc1d1	informační
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
uměleckořemeslnou	uměleckořemeslný	k2eAgFnSc4d1	uměleckořemeslná
tvorbu	tvorba	k1gFnSc4	tvorba
(	(	kIx(	(
<g/>
Design	design	k1gInSc1	design
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
Design	design	k1gInSc1	design
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
Design	design	k1gInSc4	design
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
Uměleckořemeslná	uměleckořemeslný	k2eAgFnSc1d1	uměleckořemeslná
stavba	stavba	k1gFnSc1	stavba
strunných	strunný	k2eAgInPc2d1	strunný
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obory	obor	k1gInPc1	obor
gastronomie	gastronomie	k1gFnSc2	gastronomie
(	(	kIx(	(
<g/>
Kuchař-číšník	Kuchař-číšník	k1gInSc1	Kuchař-číšník
<g/>
,	,	kIx,	,
Kuchař-kuchařka	Kuchařuchařka	k1gFnSc1	Kuchař-kuchařka
a	a	k8xC	a
Číšník-servírka	Číšníkervírka	k1gFnSc1	Číšník-servírka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
výučním	výuční	k2eAgInSc7d1	výuční
listem	list	k1gInSc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
Domov	domov	k1gInSc1	domov
mládeže	mládež	k1gFnSc2	mládež
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemohou	moct	k5eNaImIp3nP	moct
denně	denně	k6eAd1	denně
dojíždět	dojíždět	k5eAaImF	dojíždět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
volnočasové	volnočasový	k2eAgFnPc4d1	volnočasová
aktivity	aktivita	k1gFnPc4	aktivita
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
Václava	Václav	k1gMnSc2	Václav
Hudečka	Hudeček	k1gMnSc2	Hudeček
Jarní	jarní	k2eAgMnSc1d1	jarní
a	a	k8xC	a
podzimní	podzimní	k2eAgInPc1d1	podzimní
rybářské	rybářský	k2eAgInPc1d1	rybářský
závody	závod	k1gInPc1	závod
Otvírání	otvírání	k1gNnSc2	otvírání
pramenů	pramen	k1gInPc2	pramen
Divadelní	divadelní	k2eAgFnPc1d1	divadelní
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
Šibřínky	Šibřínka	k1gFnSc2	Šibřínka
Pivní	pivní	k2eAgInSc1d1	pivní
maraton	maraton	k1gInSc1	maraton
Krügl	Krügla	k1gFnPc2	Krügla
Cup	cup	k1gInSc4	cup
Luhovaný	luhovaný	k2eAgMnSc1d1	luhovaný
Vincent	Vincent	k1gMnSc1	Vincent
Food	Fooda	k1gFnPc2	Fooda
Festival	festival	k1gInSc1	festival
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
Ochotnické	ochotnický	k2eAgNnSc1d1	ochotnické
divadlo	divadlo	k1gNnSc1	divadlo
Jednou	jednou	k9	jednou
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
?	?	kIx.	?
</s>
<s>
Mikulášský	mikulášský	k2eAgInSc4d1	mikulášský
sportovní	sportovní	k2eAgInSc4d1	sportovní
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
Slunkotoč	Slunkotoč	k1gFnSc4	Slunkotoč
<g/>
,	,	kIx,	,
Dětský	dětský	k2eAgInSc4d1	dětský
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
Cyklo-bruslo	Cykloruslo	k1gFnSc4	Cyklo-bruslo
závody	závod	k1gInPc1	závod
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
široké	široký	k2eAgNnSc1d1	široké
spektrum	spektrum	k1gNnSc1	spektrum
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Radostově	Radostův	k2eAgInSc6d1	Radostův
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
víceúčelová	víceúčelový	k2eAgFnSc1d1	víceúčelová
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
sálové	sálový	k2eAgInPc4d1	sálový
sporty	sport	k1gInPc4	sport
a	a	k8xC	a
aerobik	aerobik	k1gInSc4	aerobik
<g/>
.	.	kIx.	.
</s>
<s>
Tenisový	tenisový	k2eAgInSc1d1	tenisový
klub	klub	k1gInSc1	klub
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
u	u	k7c2	u
Ottovky	Ottovka	k1gFnSc2	Ottovka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kurty	kurt	k1gInPc4	kurt
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
pronajmout	pronajmout	k5eAaPmF	pronajmout
také	také	k9	také
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
plavání	plavání	k1gNnSc3	plavání
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
nově	nově	k6eAd1	nově
vyčištěnou	vyčištěný	k2eAgFnSc4d1	vyčištěná
přehradu	přehrada	k1gFnSc4	přehrada
nebo	nebo	k8xC	nebo
četné	četný	k2eAgInPc4d1	četný
hotelové	hotelový	k2eAgInPc4d1	hotelový
bazény	bazén	k1gInPc4	bazén
<g/>
;	;	kIx,	;
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
uvedena	uvést	k5eAaPmNgFnS	uvést
nová	nový	k2eAgFnSc1d1	nová
městská	městský	k2eAgFnSc1d1	městská
plovárna	plovárna	k1gFnSc1	plovárna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
i	i	k9	i
z	z	k7c2	z
architektonického	architektonický	k2eAgInSc2d1	architektonický
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
TJ	tj	kA	tj
Slovan	Slovan	k1gInSc1	Slovan
Luhačovice	Luhačovice	k1gFnPc4	Luhačovice
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
úspěšnému	úspěšný	k2eAgInSc3d1	úspěšný
oddílu	oddíl	k1gInSc3	oddíl
orientačního	orientační	k2eAgInSc2d1	orientační
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
oddíly	oddíl	k1gInPc7	oddíl
jsou	být	k5eAaImIp3nP	být
cyklistický	cyklistický	k2eAgInSc4d1	cyklistický
<g/>
,	,	kIx,	,
softbalový	softbalový	k2eAgInSc4d1	softbalový
a	a	k8xC	a
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
Sokol	Sokol	k1gMnSc1	Sokol
Luhačovice	Luhačovice	k1gFnPc4	Luhačovice
<g/>
,	,	kIx,	,
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
hlavičkou	hlavička	k1gFnSc7	hlavička
závodí	závodit	k5eAaImIp3nP	závodit
oddíly	oddíl	k1gInPc1	oddíl
házené	házená	k1gFnSc2	házená
a	a	k8xC	a
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
také	také	k9	také
několik	několik	k4yIc4	několik
běžeckých	běžecký	k2eAgInPc2d1	běžecký
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Štědrovečerní	štědrovečerní	k2eAgInSc1d1	štědrovečerní
běh	běh	k1gInSc1	běh
<g/>
,	,	kIx,	,
konaný	konaný	k2eAgInSc1d1	konaný
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
vždy	vždy	k6eAd1	vždy
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Dobrosrdečná	dobrosrdečný	k2eAgFnSc1d1	dobrosrdečná
nálada	nálada	k1gFnSc1	nálada
vánočních	vánoční	k2eAgInPc2d1	vánoční
svátků	svátek	k1gInPc2	svátek
vládne	vládnout	k5eAaImIp3nS	vládnout
také	také	k9	také
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
Štěpánském	štěpánský	k2eAgInSc6d1	štěpánský
běhu	běh	k1gInSc6	běh
v	v	k7c6	v
Pozlovicích	Pozlovice	k1gFnPc6	Pozlovice
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
Štěpánský	štěpánský	k2eAgInSc1d1	štěpánský
pochod	pochod	k1gInSc1	pochod
s	s	k7c7	s
výstupem	výstup	k1gInSc7	výstup
na	na	k7c4	na
Starý	starý	k2eAgInSc4d1	starý
Světlov	Světlov	k1gInSc4	Světlov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
přibyl	přibýt	k5eAaPmAgInS	přibýt
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc2	listopad
Luhačovický	luhačovický	k2eAgInSc4d1	luhačovický
běh	běh	k1gInSc4	běh
Kerteamu	Kerteam	k1gInSc2	Kerteam
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Diabetologických	diabetologický	k2eAgInPc2d1	diabetologický
dnů	den	k1gInPc2	den
Běh	běh	k1gInSc4	běh
proti	proti	k7c3	proti
diabetu	diabetes	k1gInSc3	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
akcí	akce	k1gFnSc7	akce
se	se	k3xPyFc4	se
již	již	k6eAd1	již
stal	stát	k5eAaPmAgInS	stát
také	také	k9	také
LCE	LCE	kA	LCE
triatlon	triatlon	k1gInSc1	triatlon
<g/>
,	,	kIx,	,
pořádaný	pořádaný	k2eAgMnSc1d1	pořádaný
Sokolem	Sokol	k1gMnSc7	Sokol
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
,	,	kIx,	,
a	a	k8xC	a
četné	četný	k2eAgInPc1d1	četný
cyklistické	cyklistický	k2eAgInPc1d1	cyklistický
závody	závod	k1gInPc1	závod
pořádané	pořádaný	k2eAgInPc1d1	pořádaný
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
jsou	být	k5eAaImIp3nP	být
Mikroregionem	mikroregion	k1gInSc7	mikroregion
Luhačovické	luhačovický	k2eAgNnSc4d1	Luhačovické
Zálesí	Zálesí	k1gNnSc4	Zálesí
připravovány	připravován	k2eAgInPc1d1	připravován
tři	tři	k4xCgFnPc1	tři
běžkařské	běžkařský	k2eAgFnPc1d1	běžkařská
trasy	trasa	k1gFnPc1	trasa
<g/>
:	:	kIx,	:
Pozlovice	Pozlovice	k1gFnSc1	Pozlovice
I	i	k9	i
<g/>
,	,	kIx,	,
Pozlovice	Pozlovice	k1gFnPc1	Pozlovice
II	II	kA	II
a	a	k8xC	a
Petrůvka	Petrůvka	k1gFnSc1	Petrůvka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
také	také	k9	také
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
svah	svah	k1gInSc4	svah
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Anténka	anténka	k1gFnSc1	anténka
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Calma	Calma	k1gFnSc1	Calma
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
,	,	kIx,	,
organizátorka	organizátorka	k1gFnSc1	organizátorka
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dění	dění	k1gNnSc2	dění
v	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
Otto	Otto	k1gMnSc1	Otto
Serényi	Serényi	k1gNnSc2	Serényi
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
zemský	zemský	k2eAgMnSc1d1	zemský
hejtman	hejtman	k1gMnSc1	hejtman
Antonín	Antonín	k1gMnSc1	Antonín
Václavík	Václavík	k1gMnSc1	Václavík
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
z	z	k7c2	z
Pozlovic	Pozlovice	k1gFnPc2	Pozlovice
Častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
byl	být	k5eAaImAgMnS	být
mj.	mj.	kA	mj.
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
lázeňských	lázeňský	k2eAgInPc6d1	lázeňský
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
Glagolská	glagolský	k2eAgFnSc1d1	Glagolská
mše	mše	k1gFnSc1	mše
<g/>
,	,	kIx,	,
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
dokonce	dokonce	k9	dokonce
byly	být	k5eAaImAgFnP	být
inspirovány	inspirovat	k5eAaBmNgFnP	inspirovat
zdejším	zdejší	k2eAgNnSc7d1	zdejší
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
opera	opera	k1gFnSc1	opera
Osud	osud	k1gInSc4	osud
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Ustroń	Ustroń	k?	Ustroń
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Topoľčany	Topoľčan	k1gMnPc4	Topoľčan
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
Kladná	kladný	k2eAgFnSc1d1	kladná
Žilín	Žilín	k1gInSc4	Žilín
Luhačovice	Luhačovice	k1gFnPc4	Luhačovice
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Pražské	pražský	k2eAgFnSc2d1	Pražská
a	a	k8xC	a
Zahradní	zahradní	k2eAgFnSc2d1	zahradní
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
)	)	kIx)	)
Polichno	Polichno	k6eAd1	Polichno
Řetechov	Řetechov	k1gInSc1	Řetechov
</s>
