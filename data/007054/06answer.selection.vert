<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
katastrální	katastrální	k2eAgFnSc4d1	katastrální
výměru	výměra	k1gFnSc4	výměra
33	[number]	k4	33
km2	km2	k4	km2
(	(	kIx(	(
<g/>
3	[number]	k4	3
299	[number]	k4	299
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
099	[number]	k4	099
domů	dům	k1gInPc2	dům
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
5	[number]	k4	5
112	[number]	k4	112
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
