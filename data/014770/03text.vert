<s>
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1893	#num#	k4
<g/>
Praha	Praha	k1gFnSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gMnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1960	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
66	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Vršovický	vršovický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
(	(	kIx(
<g/>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
Turínská	turínský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
KarlovyZkušební	KarlovyZkušební	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
učitelství	učitelství	k1gNnSc4
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
Povolání	povolání	k1gNnSc3
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
topolog	topolog	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
republiky	republika	k1gFnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1893	#num#	k4
<g/>
,	,	kIx,
Stračov	Stračov	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1960	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
matematice	matematika	k1gFnSc6
je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
především	především	k9
díky	díky	k7c3
zavedení	zavedení	k1gNnSc3
tzv.	tzv.	kA
Čechovy	Čechův	k2eAgFnSc2d1
kohomologie	kohomologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
ve	v	k7c6
Stračově	Stračův	k2eAgFnSc6d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
Čeněk	Čeněk	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
četník	četník	k1gMnSc1
<g/>
,	,	kIx,
matkou	matka	k1gFnSc7
Anna	Anna	k1gFnSc1
Čechová	Čechová	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Kleplová	Kleplová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
gymnázia	gymnázium	k1gNnSc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
začal	začít	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
studovat	studovat	k5eAaImF
matematiku	matematika	k1gFnSc4
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
musel	muset	k5eAaImAgInS
studium	studium	k1gNnSc4
kvůli	kvůli	k7c3
válce	válka	k1gFnSc3
přerušit	přerušit	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
byl	být	k5eAaImAgMnS
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
války	válka	k1gFnSc2
se	se	k3xPyFc4
do	do	k7c2
školy	škola	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
a	a	k8xC
vysokoškolská	vysokoškolský	k2eAgNnPc1d1
studia	studio	k1gNnPc1
řádně	řádně	k6eAd1
ukončil	ukončit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
věnovat	věnovat	k5eAaImF,k5eAaPmF
učitelskému	učitelský	k2eAgNnSc3d1
povolání	povolání	k1gNnSc3
<g/>
,	,	kIx,
absolvoval	absolvovat	k5eAaPmAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
i	i	k9
zkoušku	zkouška	k1gFnSc4
učitelské	učitelský	k2eAgFnSc2d1
způsobilosti	způsobilost	k1gFnSc2
a	a	k8xC
získal	získat	k5eAaPmAgMnS
aprobaci	aprobace	k1gFnSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
a	a	k8xC
deskriptivní	deskriptivní	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
pak	pak	k6eAd1
učil	učit	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c6
reálce	reálka	k1gFnSc6
v	v	k7c6
Praze-Holešovicích	Praze-Holešovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
předložil	předložit	k5eAaPmAgMnS
odborné	odborný	k2eAgNnSc4d1
matematické	matematický	k2eAgNnSc4d1
pojednání	pojednání	k1gNnSc4
O	o	k7c6
křivkovém	křivkový	k2eAgInSc6d1
a	a	k8xC
plošném	plošný	k2eAgInSc6d1
elementu	element	k1gInSc6
třetího	třetí	k4xOgInSc2
řádu	řád	k1gInSc2
a	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
práce	práce	k1gFnSc2
byl	být	k5eAaImAgInS
promován	promovat	k5eAaBmNgInS
doktorem	doktor	k1gMnSc7
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
stipendiu	stipendium	k1gNnSc3
mohl	moct	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
školní	školní	k2eAgInSc4d1
rok	rok	k1gInSc4
1921	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
strávit	strávit	k5eAaPmF
v	v	k7c4
Itálii	Itálie	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
Turíně	Turín	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
prohluboval	prohlubovat	k5eAaImAgInS
své	svůj	k3xOyFgFnPc4
dosavadní	dosavadní	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
projektivních	projektivní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
geometrických	geometrický	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
,	,	kIx,
u	u	k7c2
profesora	profesor	k1gMnSc2
G.	G.	kA
Fubiniho	Fubini	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Čech	Čech	k1gMnSc1
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
habilitační	habilitační	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1922	#num#	k4
se	se	k3xPyFc4
skutečně	skutečně	k6eAd1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
pro	pro	k7c4
projektivní	projektivní	k2eAgFnSc4d1
diferenciální	diferenciální	k2eAgFnSc4d1
geometrii	geometrie	k1gFnSc4
habilitoval	habilitovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
pak	pak	k6eAd1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
mimořádným	mimořádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
Masarykově	Masarykův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
uvolnilo	uvolnit	k5eAaPmAgNnS
místo	místo	k1gNnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
Matyáše	Matyáš	k1gMnSc2
Lercha	Lerch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Geometrii	geometrie	k1gFnSc3
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
přednášel	přednášet	k5eAaImAgMnS
profesor	profesor	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Seifert	Seifert	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
Čecha	Čech	k1gMnSc4
tedy	tedy	k9
zůstalo	zůstat	k5eAaPmAgNnS
vyučovat	vyučovat	k5eAaImF
algebru	algebra	k1gFnSc4
a	a	k8xC
analýzu	analýza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
matematická	matematický	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
ani	ani	k8xC
algebra	algebra	k1gFnSc1
nebyla	být	k5eNaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
právě	právě	k9
středem	středem	k7c2
jeho	jeho	k3xOp3gInPc2
zájmů	zájem	k1gInPc2
<g/>
,	,	kIx,
během	během	k7c2
krátké	krátký	k2eAgFnSc2d1
doby	doba	k1gFnSc2
do	do	k7c2
ní	on	k3xPp3gFnSc2
pronikl	proniknout	k5eAaPmAgInS
a	a	k8xC
záhy	záhy	k6eAd1
obě	dva	k4xCgFnPc4
disciplíny	disciplína	k1gFnPc4
úspěšně	úspěšně	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
řádným	řádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1930	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
byl	být	k5eAaImAgMnS
děkanem	děkan	k1gMnSc7
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
intenzívně	intenzívně	k6eAd1
věnovat	věnovat	k5eAaImF,k5eAaPmF
topologii	topologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
se	se	k3xPyFc4
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
zúčastnil	zúčastnit	k5eAaPmAgMnS
speciální	speciální	k2eAgFnPc4d1
konference	konference	k1gFnPc4
o	o	k7c6
kombinatorické	kombinatorický	k2eAgFnSc6d1
topologii	topologie	k1gFnSc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
výsledky	výsledek	k1gInPc4
jeho	jeho	k3xOp3gFnSc2
vědecké	vědecký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
topologie	topologie	k1gFnSc2
vzbudily	vzbudit	k5eAaPmAgFnP
velkou	velký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
pozvání	pozvání	k1gNnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
matematickém	matematický	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
Advanced	Advanced	k1gInSc1
Study	stud	k1gInPc4
v	v	k7c6
Princetonu	Princeton	k1gInSc6
načas	načas	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
se	se	k3xPyFc4
z	z	k7c2
USA	USA	kA
vrátil	vrátit	k5eAaPmAgInS
a	a	k8xC
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
založil	založit	k5eAaPmAgMnS
v	v	k7c6
Brně	Brno	k1gNnSc6
topologický	topologický	k2eAgInSc1d1
seminář	seminář	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
během	během	k7c2
3	#num#	k4
let	léto	k1gNnPc2
vzniklo	vzniknout	k5eAaPmAgNnS
26	#num#	k4
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnost	činnost	k1gFnSc1
semináře	seminář	k1gInSc2
pokračovala	pokračovat	k5eAaImAgFnS
i	i	k9
po	po	k7c6
uzavření	uzavření	k1gNnSc6
českých	český	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
již	již	k6eAd1
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Novák	Novák	k1gMnSc1
a	a	k8xC
Bedřich	Bedřich	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
topologického	topologický	k2eAgInSc2d1
semináře	seminář	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
scházeli	scházet	k5eAaImAgMnP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
v	v	k7c6
Pospíšilově	Pospíšilův	k2eAgInSc6d1
bytě	byt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatčením	zatčení	k1gNnSc7
B.	B.	kA
Pospíšila	Pospíšil	k1gMnSc2
gestapem	gestapo	k1gNnSc7
však	však	k9
tato	tento	k3xDgFnSc1
forma	forma	k1gFnSc1
kolektivního	kolektivní	k2eAgNnSc2d1
matematického	matematický	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
skončila	skončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
odešel	odejít	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
z	z	k7c2
Brna	Brno	k1gNnSc2
na	na	k7c4
Přírodovědeckou	přírodovědecký	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vedoucím	vedoucí	k1gMnSc7
Badatelského	badatelský	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
matematického	matematický	k2eAgInSc2d1
při	při	k7c6
České	český	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vedl	vést	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
prvním	první	k4xOgMnSc7
ředitelem	ředitel	k1gMnSc7
Ústředního	ústřední	k2eAgInSc2d1
matematického	matematický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
byl	být	k5eAaImAgInS
mezi	mezi	k7c7
prvními	první	k4xOgInPc7
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
členem	člen	k1gMnSc7
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
po	po	k7c6
vzniku	vznik	k1gInSc6
Matematického	matematický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
ČSAV	ČSAV	kA
byl	být	k5eAaImAgMnS
pověřen	pověřit	k5eAaPmNgMnS
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
vybudoval	vybudovat	k5eAaPmAgInS
Matematický	matematický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vědecké	vědecký	k2eAgFnPc1d1
práce	práce	k1gFnPc1
Eduarda	Eduard	k1gMnSc2
Čecha	Čech	k1gMnSc2
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
především	především	k9
diferenciální	diferenciální	k2eAgFnPc1d1
geometrie	geometrie	k1gFnPc1
a	a	k8xC
topologie	topologie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pobyt	pobyt	k1gInSc1
v	v	k7c6
Turíně	Turín	k1gInSc6
ve	v	k7c6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
1921	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
znamenal	znamenat	k5eAaImAgInS
spolupráci	spolupráce	k1gFnSc4
Čecha	Čech	k1gMnSc2
a	a	k8xC
G.	G.	kA
Fubiniho	Fubini	k1gMnSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
vzešla	vzejít	k5eAaPmAgFnS
dvě	dva	k4xCgNnPc4
díla	dílo	k1gNnPc4
<g/>
,	,	kIx,
jednak	jednak	k8xC
dvojdílná	dvojdílný	k2eAgNnPc4d1
Geometria	Geometrium	k1gNnPc4
proiettiva	proiettivum	k1gNnSc2
differenziale	differenziale	k6eAd1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
postupně	postupně	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1926	#num#	k4
a	a	k8xC
1927	#num#	k4
v	v	k7c6
Bologni	Bologna	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
Introduction	Introduction	k1gInSc1
à	à	k?
la	la	k1gNnSc7
géométrie	géométrie	k1gFnSc2
projective	projectiv	k1gInSc5
différentielle	différentielle	k1gInSc4
des	des	k1gNnSc3
surfaces	surfacesa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
a	a	k8xC
přinesla	přinést	k5eAaPmAgFnS
oběma	dva	k4xCgMnPc7
matematikům	matematik	k1gMnPc3
uznání	uznání	k1gNnSc4
matematického	matematický	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
se	se	k3xPyFc4
Čechova	Čechův	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
načas	načas	k6eAd1
odklonila	odklonit	k5eAaPmAgFnS
od	od	k7c2
problematiky	problematika	k1gFnSc2
diferenciální	diferenciální	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
více	hodně	k6eAd2
zajímat	zajímat	k5eAaImF
o	o	k7c4
topologii	topologie	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
obecnou	obecný	k2eAgFnSc7d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
algebraickou	algebraický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
mezinárodního	mezinárodní	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
matematiků	matematik	k1gMnPc2
v	v	k7c6
Curychu	Curych	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
zavedl	zavést	k5eAaPmAgInS
nezávisle	závisle	k6eNd1
na	na	k7c6
Hurewiczovi	Hurewicz	k1gMnSc6
vyšší	vysoký	k2eAgFnSc2d2
homotopické	homotopický	k2eAgFnPc4d1
grupy	grupa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
publikoval	publikovat	k5eAaBmAgMnS
první	první	k4xOgFnSc4
nekombinatorickou	kombinatorický	k2eNgFnSc4d1
definici	definice	k1gFnSc4
kohomologie	kohomologie	k1gFnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
známou	známý	k2eAgFnSc4d1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Čechova	Čechov	k1gMnSc2
kohomologie	kohomologie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
jej	on	k3xPp3gMnSc4
zejména	zejména	k9
proslavila	proslavit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Celkem	celkem	k6eAd1
publikoval	publikovat	k5eAaBmAgMnS
12	#num#	k4
prací	práce	k1gFnPc2
z	z	k7c2
obecné	obecný	k2eAgFnSc2d1
topologie	topologie	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vůbec	vůbec	k9
první	první	k4xOgFnSc1
topologická	topologický	k2eAgFnSc1d1
práce	práce	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
vydal	vydat	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
Bodové	bodový	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
československé	československý	k2eAgFnSc2d1
matematiky	matematika	k1gFnSc2
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
k	k	k7c3
diferenciální	diferenciální	k2eAgFnSc3d1
geometrii	geometrie	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
pak	pak	k6eAd1
zabýval	zabývat	k5eAaImAgInS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
vydal	vydat	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
Topologické	topologický	k2eAgFnSc2d1
prostory	prostora	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
shrnul	shrnout	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
a	a	k8xC
rozšířil	rozšířit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
o	o	k7c4
výsledky	výsledek	k1gInPc4
z	z	k7c2
brněnského	brněnský	k2eAgInSc2d1
topologického	topologický	k2eAgInSc2d1
semináře	seminář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
také	také	k9
velkou	velký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
otázkám	otázka	k1gFnPc3
školské	školský	k2eAgFnSc2d1
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
autorem	autor	k1gMnSc7
několika	několik	k4yIc2
středoškolských	středoškolský	k2eAgFnPc2d1
učebnic	učebnice	k1gFnPc2
a	a	k8xC
pro	pro	k7c4
středoškolské	středoškolský	k2eAgMnPc4d1
učitele	učitel	k1gMnPc4
organizoval	organizovat	k5eAaBmAgInS
semináře	seminář	k1gInPc4
ze	z	k7c2
středoškolské	středoškolský	k2eAgFnSc2d1
a	a	k8xC
elementární	elementární	k2eAgFnSc2d1
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
a	a	k8xC
nač	nač	k6eAd1
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
matematika	matematika	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jednota	jednota	k1gFnSc1
českých	český	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
a	a	k8xC
fysiků	fysik	k1gMnPc2
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
.	.	kIx.
125	#num#	k4
s.	s.	k?
</s>
<s>
Aritmetika	aritmetika	k1gFnSc1
pro	pro	k7c4
I.	I.	kA
třídu	třída	k1gFnSc4
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jednota	jednota	k1gFnSc1
českých	český	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
a	a	k8xC
fysiků	fysik	k1gMnPc2
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
114	#num#	k4
s.	s.	k?
</s>
<s>
Geometrie	geometrie	k1gFnSc1
pro	pro	k7c4
1	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
třídu	třída	k1gFnSc4
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jednota	jednota	k1gFnSc1
českých	český	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
266	#num#	k4
s.	s.	k?
</s>
<s>
Elementární	elementární	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jednota	jednota	k1gFnSc1
českých	český	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
a	a	k8xC
fysiků	fysik	k1gMnPc2
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
.	.	kIx.
86	#num#	k4
s.	s.	k?
</s>
<s>
Bodové	bodový	k2eAgFnPc1d1
množiny	množina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
1966	#num#	k4
</s>
<s>
Čísla	číslo	k1gNnPc1
a	a	k8xC
početní	početní	k2eAgInPc1d1
výkony	výkon	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SNTL	SNTL	kA
1954	#num#	k4
</s>
<s>
Topologické	topologický	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČSAV	ČSAV	kA
1959	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
text	text	k1gInSc1
(	(	kIx(
<g/>
licence	licence	k1gFnSc1
CC-BY	CC-BY	k1gFnSc2
3.0	3.0	k4
Unported	Unported	k1gMnSc1
<g/>
)	)	kIx)
ze	z	k7c2
stránky	stránka	k1gFnSc2
z	z	k7c2
webu	web	k1gInSc2
Významní	významný	k2eAgMnPc1d1
matematici	matematik	k1gMnPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
původního	původní	k2eAgInSc2d1
textu	text	k1gInSc2
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Folta	Folta	k1gMnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
Lukášová	Lukášová	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Šišma	Šišma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
:	:	kIx,
I.	I.	kA
díl	díl	k1gInSc1
:	:	kIx,
A	a	k9
<g/>
–	–	k?
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
;	;	kIx,
Petr	Petr	k1gMnSc1
Meissner	Meissner	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
634	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
188	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
:	:	kIx,
10	#num#	k4
<g/>
.	.	kIx.
sešit	sešit	k1gInSc1
:	:	kIx,
Č	Č	kA
<g/>
–	–	k?
<g/>
Čerma	Čerma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
503	#num#	k4
<g/>
–	–	k?
<g/>
606	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
367	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
549	#num#	k4
<g/>
–	–	k?
<g/>
550	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nekrolog	nekrolog	k1gInSc1
v	v	k7c6
časopise	časopis	k1gInSc6
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
matematiky	matematika	k1gFnSc2
<g/>
:	:	kIx,
http://dml.cz/bitstream/handle/10338.dmlcz/117341/CasPestMat_085-1960-4_10.pdf	http://dml.cz/bitstream/handle/10338.dmlcz/117341/CasPestMat_085-1960-4_10.pdf	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Eduard	Eduard	k1gMnSc1
Čech	Čech	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1021101	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119122030	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0959	#num#	k4
8529	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
84802732	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
22944310	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
84802732	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
</s>
