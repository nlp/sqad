<s>
Žádný	žádný	k3yNgInSc1	žádný
výtisk	výtisk	k1gInSc1	výtisk
wittenberského	wittenberský	k2eAgInSc2d1	wittenberský
tisku	tisk	k1gInSc2	tisk
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Luther	Luthra	k1gFnPc2	Luthra
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
slavný	slavný	k2eAgInSc1d1	slavný
a	a	k8xC	a
význam	význam	k1gInSc1	význam
dokumentu	dokument	k1gInSc2	dokument
nebyl	být	k5eNaImAgInS	být
rozpoznán	rozpoznán	k2eAgInSc1d1	rozpoznán
<g/>
.	.	kIx.	.
</s>
