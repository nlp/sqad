<p>
<s>
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
neboli	neboli	k8xC	neboli
Disputace	disputace	k1gFnSc2	disputace
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
odpustků	odpustek	k1gInPc2	odpustek
(	(	kIx(	(
<g/>
Disputatio	Disputatio	k6eAd1	Disputatio
pro	pro	k7c4	pro
declaratione	declaration	k1gInSc5	declaration
virtutis	virtutis	k1gFnPc3	virtutis
indulgentiarum	indulgentiarum	k1gInSc4	indulgentiarum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
návrhů	návrh	k1gInPc2	návrh
pro	pro	k7c4	pro
akademickou	akademický	k2eAgFnSc4d1	akademická
disputaci	disputace	k1gFnSc4	disputace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
Martin	Martina	k1gFnPc2	Martina
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
morální	morální	k2eAgFnSc2d1	morální
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
95	[number]	k4	95
tezemi	teze	k1gFnPc7	teze
začala	začít	k5eAaPmAgFnS	začít
reformace	reformace	k1gFnSc1	reformace
<g/>
,	,	kIx,	,
rozkol	rozkol	k1gInSc1	rozkol
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navždy	navždy	k6eAd1	navždy
změnil	změnit	k5eAaPmAgMnS	změnit
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnPc1	teze
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
Lutherovu	Lutherův	k2eAgFnSc4d1	Lutherova
kritiku	kritika	k1gFnSc4	kritika
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c2	za
zneužívání	zneužívání	k1gNnSc2	zneužívání
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
:	:	kIx,	:
prodeje	prodej	k1gInSc2	prodej
plnomocných	plnomocný	k2eAgInPc2d1	plnomocný
odpustků	odpustek	k1gInPc2	odpustek
kazateli	kazatel	k1gMnSc3	kazatel
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
odpustky	odpustek	k1gInPc1	odpustek
povedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
prominutí	prominutí	k1gNnSc3	prominutí
časných	časný	k2eAgInPc2d1	časný
trestů	trest	k1gInPc2	trest
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
spáchané	spáchaný	k2eAgInPc4d1	spáchaný
kupujícími	kupující	k2eAgInPc7d1	kupující
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnPc7	jejich
blízkými	blízký	k2eAgMnPc7d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
95	[number]	k4	95
tezích	teze	k1gFnPc6	teze
Luther	Luthra	k1gFnPc2	Luthra
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokání	pokání	k1gNnSc1	pokání
vyžadované	vyžadovaný	k2eAgNnSc1d1	vyžadované
Kristem	Kristus	k1gMnSc7	Kristus
pro	pro	k7c4	pro
odpuštění	odpuštění	k1gNnSc4	odpuštění
hříchů	hřích	k1gInPc2	hřích
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
a	a	k8xC	a
duchovní	duchovní	k2eAgFnSc1d1	duchovní
<g/>
,	,	kIx,	,
že	že	k8xS	že
nestačí	stačit	k5eNaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
vnější	vnější	k2eAgFnSc4d1	vnější
zpověď	zpověď	k1gFnSc4	zpověď
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
popisoval	popisovat	k5eAaImAgMnS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
odpustky	odpustek	k1gInPc1	odpustek
odvádějí	odvádět	k5eAaImIp3nP	odvádět
křesťany	křesťan	k1gMnPc4	křesťan
od	od	k7c2	od
opravdového	opravdový	k2eAgNnSc2d1	opravdové
pokání	pokání	k1gNnSc2	pokání
a	a	k8xC	a
lítosti	lítost	k1gFnSc2	lítost
nad	nad	k7c7	nad
hříchem	hřích	k1gInSc7	hřích
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
lze	lze	k6eAd1	lze
vyhnout	vyhnout	k5eAaPmF	vyhnout
koupí	koupě	k1gFnSc7	koupě
odpustku	odpustek	k1gInSc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Odpustky	odpustek	k1gInPc4	odpustek
navíc	navíc	k6eAd1	navíc
podle	podle	k7c2	podle
Luthera	Luthero	k1gNnSc2	Luthero
odrazují	odrazovat	k5eAaImIp3nP	odrazovat
křesťany	křesťan	k1gMnPc4	křesťan
od	od	k7c2	od
obdarovávání	obdarovávání	k1gNnSc2	obdarovávání
chudých	chudý	k2eAgInPc2d1	chudý
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
skutků	skutek	k1gInPc2	skutek
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustkové	odpustkový	k2eAgFnPc1d1	odpustková
listiny	listina	k1gFnPc1	listina
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
duchovní	duchovní	k2eAgFnSc4d1	duchovní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Luther	Luthra	k1gFnPc2	Luthra
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
vůči	vůči	k7c3	vůči
odpustkům	odpustek	k1gInPc3	odpustek
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
papežovou	papežův	k2eAgFnSc7d1	papežova
<g/>
,	,	kIx,	,
Teze	teze	k1gFnPc1	teze
zpochybnily	zpochybnit	k5eAaPmAgFnP	zpochybnit
papežskou	papežský	k2eAgFnSc4d1	Papežská
bulu	bula	k1gFnSc4	bula
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
poklad	poklad	k1gInSc4	poklad
zásluh	zásluha	k1gFnPc2	zásluha
a	a	k8xC	a
dobrých	dobrý	k2eAgInPc2d1	dobrý
skutků	skutek	k1gInPc2	skutek
minulých	minulý	k2eAgFnPc2d1	minulá
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpouštěl	odpouštět	k5eAaImAgInS	odpouštět
časné	časný	k2eAgInPc4d1	časný
tresty	trest	k1gInPc4	trest
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
jsou	být	k5eAaImIp3nP	být
formulovány	formulovat	k5eAaImNgInP	formulovat
jako	jako	k9	jako
podklady	podklad	k1gInPc4	podklad
k	k	k7c3	k
diskusi	diskuse	k1gFnSc3	diskuse
a	a	k8xC	a
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
Lutherovy	Lutherův	k2eAgInPc4d1	Lutherův
vlastní	vlastní	k2eAgInPc4d1	vlastní
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Luther	Luthra	k1gFnPc2	Luthra
později	pozdě	k6eAd2	pozdě
objasnil	objasnit	k5eAaPmAgMnS	objasnit
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
v	v	k7c6	v
textu	text	k1gInSc6	text
Vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
k	k	k7c3	k
disputaci	disputace	k1gFnSc3	disputace
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luther	Luthra	k1gFnPc2	Luthra
poslal	poslat	k5eAaPmAgMnS	poslat
Teze	teze	k1gFnPc4	teze
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1517	[number]	k4	1517
jako	jako	k8xS	jako
přílohu	příloha	k1gFnSc4	příloha
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
Braniborskému	braniborský	k2eAgMnSc3d1	braniborský
<g/>
,	,	kIx,	,
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
mohučskému	mohučský	k2eAgMnSc3d1	mohučský
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
začátek	začátek	k1gInSc4	začátek
reformace	reformace	k1gFnSc2	reformace
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
připomíná	připomínat	k5eAaImIp3nS	připomínat
jako	jako	k9	jako
Den	den	k1gInSc4	den
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
také	také	k9	také
možná	možná	k9	možná
přibil	přibít	k5eAaPmAgMnS	přibít
Teze	teze	k1gFnPc4	teze
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
kostelů	kostel	k1gInPc2	kostel
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
obyčeji	obyčej	k1gInPc7	obyčej
univerzity	univerzita	k1gFnSc2	univerzita
buď	buď	k8xC	buď
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnPc1	teze
byly	být	k5eAaImAgFnP	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
díky	díky	k7c3	díky
vynálezu	vynález	k1gInSc3	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
přetiskovány	přetiskován	k2eAgInPc4d1	přetiskován
<g/>
,	,	kIx,	,
překládány	překládán	k2eAgInPc4d1	překládán
a	a	k8xC	a
distribuovány	distribuován	k2eAgInPc4d1	distribuován
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pamfletovému	pamfletový	k2eAgInSc3d1	pamfletový
souboji	souboj	k1gInSc3	souboj
s	s	k7c7	s
prodejcem	prodejce	k1gMnSc7	prodejce
odpustků	odpustek	k1gInPc2	odpustek
Johannem	Johann	k1gMnSc7	Johann
Tetzelem	Tetzel	k1gMnSc7	Tetzel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
Lutherovu	Lutherův	k2eAgFnSc4d1	Lutherova
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgMnPc1d1	církevní
nadřízení	nadřízený	k1gMnPc1	nadřízený
Luthera	Luther	k1gMnSc4	Luther
stíhali	stíhat	k5eAaImAgMnP	stíhat
pro	pro	k7c4	pro
kacířství	kacířství	k1gNnSc4	kacířství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
exkomunikaci	exkomunikace	k1gFnSc6	exkomunikace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Teze	teze	k1gFnPc1	teze
byly	být	k5eAaImAgFnP	být
počátkem	počátkem	k7c2	počátkem
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
Luther	Luthra	k1gFnPc2	Luthra
nepovažoval	považovat	k5eNaImAgInS	považovat
odpustky	odpustek	k1gInPc4	odpustek
za	za	k7c4	za
stejně	stejně	k6eAd1	stejně
důležité	důležitý	k2eAgFnPc4d1	důležitá
jako	jako	k8xC	jako
jiné	jiný	k2eAgFnPc4d1	jiná
teologické	teologický	k2eAgFnPc4d1	teologická
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
štěpily	štěpit	k5eAaImAgFnP	štěpit
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ospravedlnění	ospravedlněný	k2eAgMnPc1d1	ospravedlněný
vírou	víra	k1gFnSc7	víra
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
průlomové	průlomový	k2eAgInPc1d1	průlomový
spisy	spis	k1gInPc1	spis
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
otázkám	otázka	k1gFnPc3	otázka
přišly	přijít	k5eAaPmAgFnP	přijít
až	až	k9	až
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
nepovažoval	považovat	k5eNaImAgMnS	považovat
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
za	za	k7c4	za
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
lišily	lišit	k5eAaImAgInP	lišit
od	od	k7c2	od
papežských	papežský	k2eAgInPc2d1	papežský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
třiatřicetiletý	třiatřicetiletý	k2eAgMnSc1d1	třiatřicetiletý
profesor	profesor	k1gMnSc1	profesor
morální	morální	k2eAgFnSc2d1	morální
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
a	a	k8xC	a
městský	městský	k2eAgMnSc1d1	městský
kazatel	kazatel	k1gMnSc1	kazatel
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
proti	proti	k7c3	proti
soudobé	soudobý	k2eAgFnSc3d1	soudobá
církevní	církevní	k2eAgFnSc3d1	církevní
praxi	praxe	k1gFnSc3	praxe
udělování	udělování	k1gNnSc2	udělování
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
tenkrát	tenkrát	k6eAd1	tenkrát
prakticky	prakticky	k6eAd1	prakticky
jediné	jediný	k2eAgFnSc3d1	jediná
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
církvi	církev	k1gFnSc3	církev
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odpustky	odpustek	k1gInPc1	odpustek
součástí	součást	k1gFnPc2	součást
ekonomie	ekonomie	k1gFnSc2	ekonomie
spásy	spása	k1gFnSc2	spása
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zhřešivší	zhřešivší	k2eAgMnSc1d1	zhřešivší
křesťan	křesťan	k1gMnSc1	křesťan
vyzná	vyznat	k5eAaBmIp3nS	vyznat
své	svůj	k3xOyFgInPc4	svůj
hříchy	hřích	k1gInPc4	hřích
ve	v	k7c6	v
zpovědi	zpověď	k1gFnSc6	zpověď
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
mu	on	k3xPp3gMnSc3	on
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
nauky	nauka	k1gFnSc2	nauka
odpuštěny	odpuštěn	k2eAgInPc1d1	odpuštěn
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
již	již	k6eAd1	již
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
věčně	věčně	k6eAd1	věčně
trestán	trestat	k5eAaImNgMnS	trestat
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
obdržet	obdržet	k5eAaPmF	obdržet
časný	časný	k2eAgMnSc1d1	časný
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
časově	časově	k6eAd1	časově
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
)	)	kIx)	)
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
může	moct	k5eAaImIp3nS	moct
kajícník	kajícník	k1gMnSc1	kajícník
zahladit	zahladit	k5eAaPmF	zahladit
konáním	konání	k1gNnSc7	konání
milosrdných	milosrdný	k2eAgInPc2d1	milosrdný
skutků	skutek	k1gInPc2	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
časné	časný	k2eAgInPc1d1	časný
tresty	trest	k1gInPc1	trest
nejsou	být	k5eNaImIp3nP	být
vypořádány	vypořádat	k5eAaPmNgInP	vypořádat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
muset	muset	k5eAaImF	muset
vytrpět	vytrpět	k5eAaPmF	vytrpět
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Odpustky	odpustek	k1gInPc1	odpustek
mohou	moct	k5eAaImIp3nP	moct
tento	tento	k3xDgInSc4	tento
časný	časný	k2eAgInSc4d1	časný
trest	trest	k1gInSc4	trest
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papežové	Papež	k1gMnPc1	Papež
jsou	být	k5eAaImIp3nP	být
oprávněni	oprávnit	k5eAaPmNgMnP	oprávnit
udělit	udělit	k5eAaPmF	udělit
plnomocné	plnomocný	k2eAgInPc4d1	plnomocný
odpustky	odpustek	k1gInPc4	odpustek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
úplné	úplný	k2eAgNnSc4d1	úplné
vypořádání	vypořádání	k1gNnSc4	vypořádání
všech	všecek	k3xTgInPc2	všecek
zbývajících	zbývající	k2eAgInPc2d1	zbývající
časných	časný	k2eAgInPc2d1	časný
trestů	trest	k1gInPc2	trest
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
odpustky	odpustek	k1gInPc1	odpustek
byly	být	k5eAaImAgInP	být
zakupovány	zakupovat	k5eAaImNgInP	zakupovat
i	i	k9	i
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
systému	systém	k1gInSc6	systém
odpustků	odpustek	k1gInPc2	odpustek
tak	tak	k6eAd1	tak
duchovní	duchovní	k1gMnPc1	duchovní
získávali	získávat	k5eAaImAgMnP	získávat
hmotný	hmotný	k2eAgInSc4d1	hmotný
prospěch	prospěch	k1gInSc4	prospěch
jejich	jejich	k3xOp3gInSc7	jejich
prodejem	prodej	k1gInSc7	prodej
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
oficiálně	oficiálně	k6eAd1	oficiálně
odpouštěl	odpouštět	k5eAaImAgMnS	odpouštět
časné	časný	k2eAgInPc4d1	časný
tresty	trest	k1gInPc4	trest
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Teologové	teolog	k1gMnPc1	teolog
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
už	už	k9	už
koncem	koncem	k7c2	koncem
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
století	století	k1gNnSc2	století
tehdy	tehdy	k6eAd1	tehdy
populární	populární	k2eAgNnSc4d1	populární
rčení	rčení	k1gNnSc4	rčení
"	"	kIx"	"
<g/>
když	když	k8xS	když
cinkne	cinknout	k5eAaPmIp3nS	cinknout
peníz	peníz	k1gInSc4	peníz
o	o	k7c4	o
dno	dno	k1gNnSc4	dno
truhlice	truhlice	k1gFnSc2	truhlice
<g/>
,	,	kIx,	,
vyletí	vyletět	k5eAaPmIp3nS	vyletět
duše	duše	k1gFnSc1	duše
z	z	k7c2	z
očistce	očistec	k1gInSc2	očistec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kritikům	kritik	k1gMnPc3	kritik
odpustků	odpustek	k1gInPc2	odpustek
před	před	k7c7	před
Lutherem	Luther	k1gInSc7	Luther
patřil	patřit	k5eAaImAgMnS	patřit
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
má	mít	k5eAaImIp3nS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
nad	nad	k7c7	nad
očistcem	očistec	k1gInSc7	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
přísnější	přísný	k2eAgInSc4d2	přísnější
systém	systém	k1gInSc4	systém
pokání	pokání	k1gNnSc2	pokání
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
odpustky	odpustek	k1gInPc4	odpustek
neexistovaly	existovat	k5eNaImAgFnP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gInSc1	Johannes
von	von	k1gInSc1	von
Wesel	Wesela	k1gFnPc2	Wesela
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
útočil	útočit	k5eAaImAgMnS	útočit
na	na	k7c4	na
odpustky	odpustek	k1gInPc4	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Světští	světský	k2eAgMnPc1d1	světský
vládci	vládce	k1gMnPc1	vládce
měli	mít	k5eAaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
prodeje	prodej	k1gInSc2	prodej
odpustků	odpustek	k1gInPc2	odpustek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnPc1	jejich
ekonomiky	ekonomika	k1gFnPc1	ekonomika
trpěly	trpět	k5eAaImAgFnP	trpět
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
za	za	k7c4	za
odpustky	odpustek	k1gInPc4	odpustek
opouštěly	opouštět	k5eAaImAgFnP	opouštět
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažili	snažit	k5eAaImAgMnP	snažit
získat	získat	k5eAaPmF	získat
část	část	k1gFnSc4	část
výtěžku	výtěžek	k1gInSc2	výtěžek
nebo	nebo	k8xC	nebo
zakazovali	zakazovat	k5eAaImAgMnP	zakazovat
odpustky	odpustek	k1gInPc4	odpustek
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Lutherově	Lutherův	k2eAgNnSc6d1	Lutherovo
Sasku	Sasko	k1gNnSc6	Sasko
udělal	udělat	k5eAaPmAgMnS	udělat
tamní	tamní	k2eAgMnSc1d1	tamní
vévoda	vévoda	k1gMnSc1	vévoda
Jiří	Jiří	k1gMnSc1	Jiří
Vousatý	vousatý	k2eAgMnSc1d1	vousatý
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1515	[number]	k4	1515
vypsal	vypsat	k5eAaPmAgMnS	vypsat
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
X.	X.	kA	X.
plnomocné	plnomocný	k2eAgInPc4d1	plnomocný
odpustky	odpustek	k1gInPc4	odpustek
určené	určený	k2eAgInPc4d1	určený
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
stavby	stavba	k1gFnSc2	stavba
baziliky	bazilika	k1gFnSc2	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Platily	platit	k5eAaImAgFnP	platit
pro	pro	k7c4	pro
téměř	téměř	k6eAd1	téměř
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
hřích	hřích	k1gInSc4	hřích
včetně	včetně	k7c2	včetně
cizoložství	cizoložství	k1gNnSc2	cizoložství
a	a	k8xC	a
krádeže	krádež	k1gFnSc2	krádež
<g/>
.	.	kIx.	.
</s>
<s>
Kázání	kázání	k1gNnSc4	kázání
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
odpustků	odpustek	k1gInPc2	odpustek
mělo	mít	k5eAaImAgNnS	mít
přestat	přestat	k5eAaPmF	přestat
na	na	k7c4	na
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
nabízely	nabízet	k5eAaImAgInP	nabízet
papežovy	papežův	k2eAgInPc1d1	papežův
odpustky	odpustek	k1gInPc1	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Kazatelé	kazatel	k1gMnPc1	kazatel
odpustků	odpustek	k1gInPc2	odpustek
dostali	dostat	k5eAaPmAgMnP	dostat
přísné	přísný	k2eAgInPc4d1	přísný
pokyny	pokyn	k1gInPc4	pokyn
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mají	mít	k5eAaImIp3nP	mít
odpustky	odpustek	k1gInPc4	odpustek
propagovat	propagovat	k5eAaImF	propagovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
je	on	k3xPp3gInPc4	on
vychvalovat	vychvalovat	k5eAaImF	vychvalovat
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
odpustky	odpustek	k1gInPc1	odpustek
dřívější	dřívější	k2eAgInPc1d1	dřívější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
byl	být	k5eAaImAgInS	být
kázáním	kázání	k1gNnSc7	kázání
a	a	k8xC	a
nabízením	nabízení	k1gNnSc7	nabízení
odpustků	odpustek	k1gInPc2	odpustek
pověřen	pověřen	k2eAgMnSc1d1	pověřen
Johann	Johann	k1gMnSc1	Johann
Tetzel	Tetzel	k1gMnSc1	Tetzel
<g/>
;	;	kIx,	;
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kampaň	kampaň	k1gFnSc1	kampaň
ve	v	k7c6	v
městech	město	k1gNnPc6	město
poblíž	poblíž	k7c2	poblíž
Wittenbergu	Wittenberg	k1gInSc2	Wittenberg
přiměla	přimět	k5eAaPmAgFnS	přimět
mnoho	mnoho	k4c4	mnoho
občanů	občan	k1gMnPc2	občan
Wittenbergu	Wittenberg	k1gInSc2	Wittenberg
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
těchto	tento	k3xDgNnPc2	tento
měst	město	k1gNnPc2	město
a	a	k8xC	a
nakupovat	nakupovat	k5eAaBmF	nakupovat
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prodej	prodej	k1gInSc1	prodej
odpustků	odpustek	k1gInPc2	odpustek
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
saských	saský	k2eAgNnPc6d1	Saské
městech	město	k1gNnPc6	město
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
<g/>
Luther	Luthra	k1gFnPc2	Luthra
měl	mít	k5eAaImAgMnS	mít
rovněž	rovněž	k9	rovněž
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
odpustky	odpustek	k1gInPc7	odpustek
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
Uctíváním	uctívání	k1gNnSc7	uctívání
velké	velký	k2eAgFnSc2d1	velká
chrámové	chrámový	k2eAgFnSc2d1	chrámová
sbírky	sbírka	k1gFnSc2	sbírka
ostatků	ostatek	k1gInPc2	ostatek
mohli	moct	k5eAaImAgMnP	moct
věřící	věřící	k2eAgMnPc1d1	věřící
také	také	k9	také
získávat	získávat	k5eAaImF	získávat
odpustky	odpustek	k1gInPc4	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1514	[number]	k4	1514
Luther	Luthra	k1gFnPc2	Luthra
kázal	kázat	k5eAaImAgInS	kázat
proti	proti	k7c3	proti
zneužívání	zneužívání	k1gNnSc3	zneužívání
odpustků	odpustek	k1gInPc2	odpustek
a	a	k8xC	a
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zlevnily	zlevnit	k5eAaPmAgFnP	zlevnit
boží	boží	k2eAgFnPc1d1	boží
milosti	milost	k1gFnPc1	milost
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
opravdovým	opravdový	k2eAgNnSc7d1	opravdové
pokáním	pokání	k1gNnSc7	pokání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
starost	starost	k1gFnSc4	starost
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
farníci	farník	k1gMnPc1	farník
vraceli	vracet	k5eAaImAgMnP	vracet
z	z	k7c2	z
nákupu	nákup	k1gInSc2	nákup
Tetzelových	Tetzelův	k2eAgInPc2d1	Tetzelův
odpustků	odpustek	k1gInPc2	odpustek
a	a	k8xC	a
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nemusejí	muset	k5eNaImIp3nP	muset
činit	činit	k5eAaImF	činit
pokání	pokání	k1gNnSc4	pokání
a	a	k8xC	a
změnit	změnit	k5eAaPmF	změnit
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
odpuštěny	odpuštěn	k2eAgInPc4d1	odpuštěn
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
poslechl	poslechnout	k5eAaPmAgMnS	poslechnout
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Tetzel	Tetzel	k1gMnSc1	Tetzel
říkal	říkat	k5eAaImAgMnS	říkat
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
kázáních	kázání	k1gNnPc6	kázání
<g/>
,	,	kIx,	,
Luther	Luthra	k1gFnPc2	Luthra
začal	začít	k5eAaPmAgInS	začít
pečlivěji	pečlivě	k6eAd2	pečlivě
studovat	studovat	k5eAaImF	studovat
problematiku	problematika	k1gFnSc4	problematika
odpustků	odpustek	k1gInPc2	odpustek
a	a	k8xC	a
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
odborníky	odborník	k1gMnPc4	odborník
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
několikrát	několikrát	k6eAd1	několikrát
kázal	kázat	k5eAaImAgInS	kázat
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
a	a	k8xC	a
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
opravdové	opravdový	k2eAgNnSc1d1	opravdové
pokání	pokání	k1gNnSc1	pokání
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
nákup	nákup	k1gInSc1	nákup
odpustku	odpustek	k1gInSc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učít	k5eAaPmAgMnS	učít
<g/>
,	,	kIx,	,
že	že	k8xS	že
předpokladem	předpoklad	k1gInSc7	předpoklad
fungování	fungování	k1gNnSc2	fungování
odpustku	odpustek	k1gInSc2	odpustek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
kajícník	kajícník	k1gMnSc1	kajícník
se	se	k3xPyFc4	se
vyznal	vyznat	k5eAaPmAgMnS	vyznat
a	a	k8xC	a
litoval	litovat	k5eAaImAgMnS	litovat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
odpustek	odpustek	k1gInSc4	odpustek
bezcenný	bezcenný	k2eAgInSc4d1	bezcenný
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
kající	kající	k2eAgMnSc1d1	kající
hříšník	hříšník	k1gMnSc1	hříšník
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Luthera	Luther	k1gMnSc2	Luther
také	také	k6eAd1	také
neměl	mít	k5eNaImAgMnS	mít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
odpustky	odpustek	k1gInPc4	odpustek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
miloval	milovat	k5eAaImAgMnS	milovat
boží	boží	k2eAgFnSc4d1	boží
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
a	a	k8xC	a
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
trestu	trest	k1gInSc6	trest
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
kázání	kázání	k1gNnPc1	kázání
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ustala	ustat	k5eAaPmAgFnS	ustat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
když	když	k8xS	když
Luther	Luthra	k1gFnPc2	Luthra
psal	psát	k5eAaImAgInS	psát
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
<g/>
.	.	kIx.	.
</s>
<s>
Sepsal	sepsat	k5eAaPmAgMnS	sepsat
i	i	k9	i
Pojednání	pojednání	k1gNnSc4	pojednání
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obezřetný	obezřetný	k2eAgInSc1d1	obezřetný
a	a	k8xC	a
pátravý	pátravý	k2eAgInSc1d1	pátravý
průzkum	průzkum	k1gInSc1	průzkum
tématu	téma	k1gNnSc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Dopisem	dopis	k1gInSc7	dopis
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
církevní	církevní	k2eAgFnSc4d1	církevní
představené	představená	k1gFnSc3	představená
včetně	včetně	k7c2	včetně
svého	svůj	k3xOyFgMnSc2	svůj
nadřízeného	nadřízený	k1gMnSc2	nadřízený
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Schulze	Schulz	k1gMnSc2	Schulz
<g/>
,	,	kIx,	,
biskupa	biskup	k1gMnSc2	biskup
braniborského	braniborský	k2eAgMnSc2d1	braniborský
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zaslal	zaslat	k5eAaPmAgMnS	zaslat
Teze	teze	k1gFnPc4	teze
arcibiskupu	arcibiskup	k1gMnSc3	arcibiskup
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
Braniborskému	braniborský	k2eAgMnSc3d1	braniborský
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
teze	teze	k1gFnSc1	teze
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
slavnou	slavný	k2eAgFnSc7d1	slavná
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
náš	náš	k3xOp1gMnSc1	náš
Pán	pán	k1gMnSc1	pán
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
,	,	kIx,	,
<g/>
Čiňte	činit	k5eAaImRp2nP	činit
pokání	pokání	k1gNnSc4	pokání
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
věřícího	věřící	k1gMnSc2	věřící
byl	být	k5eAaImAgInS	být
pokáním	pokání	k1gNnSc7	pokání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
několika	několik	k4yIc6	několik
tezích	teze	k1gFnPc6	teze
Luther	Luthra	k1gFnPc2	Luthra
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
myšlenku	myšlenka	k1gFnSc4	myšlenka
pokání	pokání	k1gNnSc2	pokání
jako	jako	k9	jako
křesťanova	křesťanův	k2eAgInSc2d1	křesťanův
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
boje	boj	k1gInSc2	boj
s	s	k7c7	s
hříchem	hřích	k1gInSc7	hřích
spíše	spíše	k9	spíše
než	než	k8xS	než
jako	jako	k9	jako
vnějšího	vnější	k2eAgInSc2d1	vnější
systému	systém	k1gInSc2	systém
svátostné	svátostný	k2eAgFnSc2d1	svátostná
zpovědi	zpověď	k1gFnSc2	zpověď
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
pak	pak	k6eAd1	pak
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
může	moct	k5eAaImIp3nS	moct
jen	jen	k6eAd1	jen
osvobodit	osvobodit	k5eAaPmF	osvobodit
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
trestů	trest	k1gInPc2	trest
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
udělil	udělit	k5eAaPmAgMnS	udělit
sám	sám	k3xTgMnSc1	sám
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
církevního	církevní	k2eAgInSc2d1	církevní
systému	systém	k1gInSc2	systém
pokání	pokání	k1gNnSc2	pokání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
z	z	k7c2	z
viny	vina	k1gFnSc2	vina
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
může	moct	k5eAaImIp3nS	moct
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
božím	boží	k2eAgNnSc6d1	boží
jménu	jméno	k1gNnSc6	jméno
ohlašovat	ohlašovat	k5eAaImF	ohlašovat
boží	boží	k2eAgNnSc4d1	boží
odpuštění	odpuštění	k1gNnSc4	odpuštění
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tezích	teze	k1gFnPc6	teze
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
Luther	Luthra	k1gFnPc2	Luthra
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
běžné	běžný	k2eAgFnPc4d1	běžná
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
očistci	očistec	k1gInSc6	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
přinášejí	přinášet	k5eAaImIp3nP	přinášet
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
trest	trest	k1gInSc1	trest
očistce	očistec	k1gInSc2	očistec
lze	lze	k6eAd1	lze
přirovnat	přirovnat	k5eAaPmF	přirovnat
ke	k	k7c3	k
strachu	strach	k1gInSc3	strach
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc4	zoufalství
umírajících	umírající	k2eAgMnPc2d1	umírající
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tezích	teze	k1gFnPc6	teze
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
jistotou	jistota	k1gFnSc7	jistota
nelze	lze	k6eNd1	lze
o	o	k7c6	o
duchovním	duchovní	k2eAgInSc6d1	duchovní
stavu	stav	k1gInSc6	stav
duší	duše	k1gFnPc2	duše
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
nic	nic	k6eAd1	nic
říci	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
25	[number]	k4	25
a	a	k8xC	a
26	[number]	k4	26
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
dušemi	duše	k1gFnPc7	duše
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
<g/>
,	,	kIx,	,
plátcovi	plátcův	k2eAgMnPc1d1	plátcův
blízcí	blízký	k2eAgMnPc1d1	blízký
jsou	být	k5eAaImIp3nP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
z	z	k7c2	z
očistce	očistec	k1gInSc2	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Považují	považovat	k5eAaImIp3nP	považovat
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
povzbuzování	povzbuzování	k1gNnSc4	povzbuzování
hříšné	hříšný	k2eAgFnSc2d1	hříšná
chamtivosti	chamtivost	k1gFnSc2	chamtivost
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
jistý	jistý	k2eAgInSc4d1	jistý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jen	jen	k9	jen
Bůh	bůh	k1gMnSc1	bůh
sám	sám	k3xTgMnSc1	sám
má	mít	k5eAaImIp3nS	mít
plnou	plný	k2eAgFnSc4d1	plná
moc	moc	k1gFnSc4	moc
odpouštět	odpouštět	k5eAaImF	odpouštět
tresty	trest	k1gInPc4	trest
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
<g/>
.	.	kIx.	.
<g/>
Teze	teze	k1gFnSc1	teze
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
se	se	k3xPyFc4	se
vypořádávají	vypořádávat	k5eAaImIp3nP	vypořádávat
s	s	k7c7	s
falešnou	falešný	k2eAgFnSc7d1	falešná
jistotou	jistota	k1gFnSc7	jistota
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
podle	podle	k7c2	podle
Luthera	Luthero	k1gNnSc2	Luthero
kazatelé	kazatel	k1gMnPc1	kazatel
odpustků	odpustek	k1gInPc2	odpustek
nabízejí	nabízet	k5eAaImIp3nP	nabízet
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nikdo	nikdo	k3yNnSc1	nikdo
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
skutečně	skutečně	k6eAd1	skutečně
kál	kát	k5eAaImAgMnS	kát
<g/>
,	,	kIx,	,
listina	listina	k1gFnSc1	listina
ujišťující	ujišťující	k2eAgFnSc1d1	ujišťující
člověka	člověk	k1gMnSc4	člověk
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
je	být	k5eAaImIp3nS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
35	[number]	k4	35
a	a	k8xC	a
36	[number]	k4	36
útočí	útočit	k5eAaImIp3nP	útočit
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustek	odpustek	k1gInSc1	odpustek
činí	činit	k5eAaImIp3nS	činit
pokání	pokání	k1gNnSc4	pokání
zbytečným	zbytečný	k2eAgNnPc3d1	zbytečné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
kající	kající	k2eAgMnSc1d1	kající
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jedině	jedině	k6eAd1	jedině
může	moct	k5eAaImIp3nS	moct
těžit	těžit	k5eAaImF	těžit
z	z	k7c2	z
odpustku	odpustek	k1gInSc2	odpustek
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jediný	jediný	k2eAgInSc4d1	jediný
užitek	užitek	k1gInSc4	užitek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odpustek	odpustek	k1gInSc1	odpustek
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
kajícímu	kající	k2eAgMnSc3d1	kající
křesťanovi	křesťan	k1gMnSc3	křesťan
již	již	k6eAd1	již
podle	podle	k7c2	podle
Luthera	Luther	k1gMnSc2	Luther
byl	být	k5eAaImAgInS	být
odpuštěn	odpuštěn	k2eAgInSc1d1	odpuštěn
trest	trest	k1gInSc1	trest
i	i	k8xC	i
vina	vina	k1gFnSc1	vina
samotná	samotný	k2eAgFnSc1d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tezi	teze	k1gFnSc6	teze
37	[number]	k4	37
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustky	odpustek	k1gInPc1	odpustek
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
přijmout	přijmout	k5eAaPmF	přijmout
všechny	všechen	k3xTgInPc4	všechen
dary	dar	k1gInPc4	dar
poskytované	poskytovaný	k2eAgNnSc1d1	poskytované
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
39	[number]	k4	39
a	a	k8xC	a
40	[number]	k4	40
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustky	odpustek	k1gInPc1	odpustek
činí	činit	k5eAaImIp3nP	činit
opravdové	opravdový	k2eAgNnSc4d1	opravdové
pokání	pokání	k1gNnSc4	pokání
obtížnějším	obtížný	k2eAgInSc7d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Opravdové	opravdový	k2eAgNnSc1d1	opravdové
pokání	pokání	k1gNnSc1	pokání
touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
božím	boží	k2eAgInSc6d1	boží
trestu	trest	k1gInSc6	trest
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odpustky	odpustek	k1gInPc4	odpustek
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
trestu	trest	k1gInSc3	trest
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
účelem	účel	k1gInSc7	účel
jejich	jejich	k3xOp3gInSc2	jejich
nákupu	nákup	k1gInSc2	nákup
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
tezích	teze	k1gFnPc6	teze
41	[number]	k4	41
<g/>
–	–	k?	–
<g/>
47	[number]	k4	47
Luther	Luthra	k1gFnPc2	Luthra
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
odpustky	odpustek	k1gInPc4	odpustek
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odrazují	odrazovat	k5eAaImIp3nP	odrazovat
od	od	k7c2	od
skutků	skutek	k1gInPc2	skutek
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
kupují	kupovat	k5eAaImIp3nP	kupovat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
obrat	obrat	k1gInSc4	obrat
"	"	kIx"	"
<g/>
Křesťany	Křesťan	k1gMnPc7	Křesťan
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
učit	učit	k5eAaImF	učit
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
lidé	člověk	k1gMnPc1	člověk
by	by	kYmCp3nS	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
poučováni	poučován	k2eAgMnPc1d1	poučován
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gInPc4	on
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dávat	dávat	k5eAaImF	dávat
chudým	chudý	k1gMnPc3	chudý
je	být	k5eAaImIp3nS	být
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
důležitější	důležitý	k2eAgFnSc1d2	důležitější
<g/>
,	,	kIx,	,
než	než	k8xS	než
kupovat	kupovat	k5eAaImF	kupovat
odpustky	odpustek	k1gInPc4	odpustek
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakupovat	nakupovat	k5eAaBmF	nakupovat
odpustky	odpustek	k1gInPc4	odpustek
spíše	spíše	k9	spíše
než	než	k8xS	než
dávat	dávat	k5eAaImF	dávat
chudým	chudý	k1gMnPc3	chudý
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
boží	boží	k2eAgInSc1d1	boží
hněv	hněv	k1gInSc1	hněv
a	a	k8xC	a
že	že	k8xS	že
dělání	dělání	k1gNnSc1	dělání
dobrých	dobrý	k2eAgInPc2d1	dobrý
skutků	skutek	k1gInPc2	skutek
činí	činit	k5eAaImIp3nS	činit
člověka	člověk	k1gMnSc4	člověk
lepším	lepší	k1gNnSc7	lepší
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nákup	nákup	k1gInSc1	nákup
odpustků	odpustek	k1gInPc2	odpustek
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tezích	teze	k1gFnPc6	teze
48	[number]	k4	48
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
Luther	Luthra	k1gFnPc2	Luthra
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
papeže	papež	k1gMnSc4	papež
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
papež	papež	k1gMnSc1	papež
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
hlásá	hlásat	k5eAaImIp3nS	hlásat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
jménu	jméno	k1gNnSc6	jméno
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
by	by	k9	by
baziliku	bazilika	k1gFnSc4	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
nechal	nechat	k5eAaPmAgMnS	nechat
spálit	spálit	k5eAaPmF	spálit
na	na	k7c4	na
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
jeho	jeho	k3xOp3gFnPc2	jeho
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Teze	teze	k1gFnSc1	teze
53	[number]	k4	53
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
si	se	k3xPyFc3	se
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
kázání	kázání	k1gNnSc2	kázání
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
odpustky	odpustek	k1gInPc1	odpustek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
tezích	teze	k1gFnPc6	teze
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
66	[number]	k4	66
Luther	Luthra	k1gFnPc2	Luthra
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
pokladu	poklad	k1gInSc6	poklad
zásluh	zásluha	k1gFnPc2	zásluha
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
staví	stavit	k5eAaPmIp3nS	stavit
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
křesťané	křesťan	k1gMnPc1	křesťan
tuto	tento	k3xDgFnSc4	tento
nauku	nauka	k1gFnSc4	nauka
nechápou	chápat	k5eNaImIp3nP	chápat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
klamáni	klamán	k2eAgMnPc1d1	klamán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Luthera	Luther	k1gMnSc4	Luther
je	být	k5eAaImIp3nS	být
pravým	pravý	k2eAgInSc7d1	pravý
pokladem	poklad	k1gInSc7	poklad
církve	církev	k1gFnSc2	církev
evangelium	evangelium	k1gNnSc1	evangelium
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
poklad	poklad	k1gInSc4	poklad
mají	mít	k5eAaImIp3nP	mít
lidé	člověk	k1gMnPc1	člověk
sklon	sklon	k1gInSc4	sklon
odmítat	odmítat	k5eAaImF	odmítat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
protože	protože	k8xS	protože
první	první	k4xOgFnSc4	první
dělá	dělat	k5eAaImIp3nS	dělat
posledními	poslední	k2eAgFnPc7d1	poslední
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
výroků	výrok	k1gInPc2	výrok
Matouše	Matouš	k1gMnSc2	Matouš
19,30	[number]	k4	19,30
a	a	k8xC	a
20,16	[number]	k4	20,16
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
používá	používat	k5eAaImIp3nS	používat
metaforu	metafora	k1gFnSc4	metafora
a	a	k8xC	a
slovní	slovní	k2eAgFnPc4d1	slovní
hříčky	hříčka	k1gFnPc4	hříčka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
popsal	popsat	k5eAaPmAgMnS	popsat
poklady	poklad	k1gInPc4	poklad
evangelia	evangelium	k1gNnSc2	evangelium
jako	jako	k8xS	jako
sítě	síť	k1gFnSc2	síť
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
boháčů	boháč	k1gMnPc2	boháč
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poklady	poklad	k1gInPc1	poklad
odpustků	odpustek	k1gInPc2	odpustek
jsou	být	k5eAaImIp3nP	být
sítě	síť	k1gFnPc4	síť
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tezích	teze	k1gFnPc6	teze
67	[number]	k4	67
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
Luther	Luthra	k1gFnPc2	Luthra
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
problémy	problém	k1gInPc4	problém
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
odpustky	odpustek	k1gInPc1	odpustek
káží	káž	k1gFnPc2	káž
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
tématem	téma	k1gNnSc7	téma
jeho	on	k3xPp3gInSc2	on
dopisu	dopis	k1gInSc2	dopis
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
<g/>
.	.	kIx.	.
</s>
<s>
Kazatelé	kazatel	k1gMnPc1	kazatel
propagují	propagovat	k5eAaImIp3nP	propagovat
odpustky	odpustek	k1gInPc4	odpustek
jako	jako	k8xS	jako
největší	veliký	k2eAgFnPc4d3	veliký
z	z	k7c2	z
milostí	milost	k1gFnPc2	milost
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlastně	vlastně	k9	vlastně
jen	jen	k9	jen
propagují	propagovat	k5eAaImIp3nP	propagovat
chamtivost	chamtivost	k1gFnSc4	chamtivost
<g/>
.	.	kIx.	.
</s>
<s>
Biskupům	biskup	k1gMnPc3	biskup
bylo	být	k5eAaImAgNnS	být
přikázáno	přikázán	k2eAgNnSc1d1	přikázáno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
přijímali	přijímat	k5eAaImAgMnP	přijímat
kazatele	kazatel	k1gMnSc4	kazatel
odpustků	odpustek	k1gInPc2	odpustek
vstupující	vstupující	k2eAgInSc1d1	vstupující
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
biskupové	biskup	k1gMnPc1	biskup
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
povinni	povinen	k2eAgMnPc1d1	povinen
chránit	chránit	k5eAaImF	chránit
své	svůj	k3xOyFgMnPc4	svůj
lidi	člověk	k1gMnPc4	člověk
před	před	k7c7	před
kazateli	kazatel	k1gMnPc7	kazatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
káží	kážit	k5eAaImIp3nP	kážit
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
papežovým	papežův	k2eAgInSc7d1	papežův
záměrem	záměr	k1gInSc7	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
pak	pak	k6eAd1	pak
haní	hanit	k5eAaImIp3nS	hanit
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
údajně	údajně	k6eAd1	údajně
šířené	šířený	k2eAgFnPc4d1	šířená
kazateli	kazatel	k1gMnPc7	kazatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustky	odpustek	k1gInPc1	odpustek
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
prominout	prominout	k5eAaPmF	prominout
i	i	k9	i
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
poskvrnil	poskvrnit	k5eAaPmAgMnS	poskvrnit
Pannu	Panna	k1gFnSc4	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustky	odpustek	k1gInPc1	odpustek
nemohou	moct	k5eNaImIp3nP	moct
sejmout	sejmout	k5eAaPmF	sejmout
vinu	vina	k1gFnSc4	vina
ani	ani	k8xC	ani
za	za	k7c4	za
nejlehčí	lehký	k2eAgMnPc4d3	nejlehčí
ze	z	k7c2	z
všedních	všední	k2eAgInPc2d1	všední
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
údajných	údajný	k2eAgNnPc2d1	údajné
tvrzení	tvrzení	k1gNnSc2	tvrzení
kazatelů	kazatel	k1gMnPc2	kazatel
odpustků	odpustek	k1gInPc2	odpustek
potom	potom	k8xC	potom
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
rouhání	rouhání	k1gNnSc4	rouhání
<g/>
:	:	kIx,	:
že	že	k8xS	že
svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
by	by	kYmCp3nS	by
nemohl	moct	k5eNaImAgMnS	moct
udělit	udělit	k5eAaPmF	udělit
větší	veliký	k2eAgFnSc4d2	veliký
milost	milost	k1gFnSc4	milost
než	než	k8xS	než
současný	současný	k2eAgMnSc1d1	současný
papež	papež	k1gMnSc1	papež
a	a	k8xC	a
že	že	k8xS	že
odpustkový	odpustkový	k2eAgInSc4d1	odpustkový
kříž	kříž	k1gInSc4	kříž
s	s	k7c7	s
papežským	papežský	k2eAgInSc7d1	papežský
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
cenný	cenný	k2eAgInSc4d1	cenný
jako	jako	k8xS	jako
Kristův	Kristův	k2eAgInSc4d1	Kristův
kříž	kříž	k1gInSc4	kříž
<g/>
.	.	kIx.	.
<g/>
Luther	Luthra	k1gFnPc2	Luthra
cituje	citovat	k5eAaBmIp3nS	citovat
několik	několik	k4yIc1	několik
námitek	námitka	k1gFnPc2	námitka
laiků	laik	k1gMnPc2	laik
proti	proti	k7c3	proti
odpustkům	odpustek	k1gInPc3	odpustek
v	v	k7c6	v
tezích	teze	k1gFnPc6	teze
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
91	[number]	k4	91
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
obtížné	obtížný	k2eAgNnSc1d1	obtížné
námitky	námitka	k1gFnPc4	námitka
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
věřících	věřící	k1gMnPc2	věřící
spíše	spíše	k9	spíše
než	než	k8xS	než
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
má	mít	k5eAaImIp3nS	mít
odpovědět	odpovědět	k5eAaPmF	odpovědět
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
ptají	ptat	k5eAaImIp3nP	ptat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
papež	papež	k1gMnSc1	papež
raději	rád	k6eAd2	rád
prostě	prostě	k9	prostě
očistec	očistec	k1gInSc1	očistec
hned	hned	k6eAd1	hned
nevyprázdní	vyprázdnit	k5eNaPmIp3nS	vyprázdnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
moci	moc	k1gFnSc6	moc
<g/>
?	?	kIx.	?
</s>
<s>
Co	co	k3yInSc1	co
má	mít	k5eAaImIp3nS	mít
říci	říct	k5eAaPmF	říct
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
ptají	ptat	k5eAaImIp3nP	ptat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
slouží	sloužit	k5eAaImIp3nS	sloužit
výroční	výroční	k2eAgFnSc1d1	výroční
zádušní	zádušní	k2eAgFnSc1d1	zádušní
mše	mše	k1gFnSc1	mše
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
duší	duše	k1gFnPc2	duše
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
i	i	k8xC	i
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
očištěni	očistit	k5eAaPmNgMnP	očistit
odpustkem	odpustek	k1gInSc7	odpustek
<g/>
?	?	kIx.	?
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někomu	někdo	k3yInSc3	někdo
zdá	zdát	k5eAaPmIp3nS	zdát
divné	divný	k2eAgNnSc1d1	divné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
duše	duše	k1gFnSc1	duše
zbožných	zbožný	k2eAgMnPc2d1	zbožný
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zachráněny	zachránit	k5eAaPmNgFnP	zachránit
živými	živý	k2eAgFnPc7d1	živá
bezbožníky	bezbožník	k1gMnPc7	bezbožník
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
přebohatý	přebohatý	k2eAgMnSc1d1	přebohatý
papež	papež	k1gMnSc1	papež
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
peníze	peníz	k1gInPc4	peníz
chudých	chudý	k2eAgMnPc2d1	chudý
věřících	věřící	k1gMnPc2	věřící
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Ignorovat	ignorovat	k5eAaImF	ignorovat
tyto	tento	k3xDgFnPc4	tento
otázky	otázka	k1gFnPc4	otázka
podle	podle	k7c2	podle
Luthera	Luther	k1gMnSc2	Luther
přináší	přinášet	k5eAaImIp3nS	přinášet
riziko	riziko	k1gNnSc4	riziko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
budou	být	k5eAaImBp3nP	být
papežovi	papežův	k2eAgMnPc1d1	papežův
vysmívat	vysmívat	k5eAaImF	vysmívat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Teze	teze	k1gFnPc1	teze
dovolávají	dovolávat	k5eAaImIp3nP	dovolávat
papežova	papežův	k2eAgFnSc1d1	papežova
finančního	finanční	k2eAgInSc2d1	finanční
zájmu	zájem	k1gInSc2	zájem
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
kazatelé	kazatel	k1gMnPc1	kazatel
omezili	omezit	k5eAaPmAgMnP	omezit
svoje	svůj	k3xOyFgNnSc4	svůj
kázání	kázání	k1gNnSc4	kázání
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
Lutherovým	Lutherův	k2eAgInSc7d1	Lutherův
názorem	názor	k1gInSc7	názor
na	na	k7c4	na
odpustky	odpustek	k1gInPc4	odpustek
(	(	kIx(	(
<g/>
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
Teze	teze	k1gFnPc1	teze
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
i	i	k9	i
papežovým	papežův	k2eAgInSc7d1	papežův
názorem	názor	k1gInSc7	názor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
by	by	kYmCp3nP	by
uvedené	uvedený	k2eAgFnPc1d1	uvedená
námitky	námitka	k1gFnPc1	námitka
přestaly	přestat	k5eAaPmAgFnP	přestat
být	být	k5eAaImF	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Teze	teze	k1gFnSc1	teze
výzvou	výzva	k1gFnSc7	výzva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
křesťané	křesťan	k1gMnPc1	křesťan
následovali	následovat	k5eAaImAgMnP	následovat
Krista	Kristus	k1gMnSc4	Kristus
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
přináší	přinášet	k5eAaImIp3nS	přinášet
bolest	bolest	k1gFnSc4	bolest
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Snášet	snášet	k5eAaImF	snášet
trest	trest	k1gInSc4	trest
a	a	k8xC	a
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgInSc4d2	lepší
než	než	k8xS	než
falešný	falešný	k2eAgInSc4d1	falešný
pocit	pocit	k1gInSc4	pocit
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lutherův	Lutherův	k2eAgInSc4d1	Lutherův
záměr	záměr	k1gInSc4	záměr
==	==	k?	==
</s>
</p>
<p>
<s>
Teze	teze	k1gFnPc1	teze
jsou	být	k5eAaImIp3nP	být
napsány	napsat	k5eAaPmNgFnP	napsat
jako	jako	k8xC	jako
podklad	podklad	k1gInSc1	podklad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
diskutován	diskutovat	k5eAaImNgInS	diskutovat
ve	v	k7c6	v
formální	formální	k2eAgFnSc6d1	formální
akademické	akademický	k2eAgFnSc6d1	akademická
disputaci	disputace	k1gFnSc6	disputace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
neexistuje	existovat	k5eNaImIp3nS	existovat
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
taková	takový	k3xDgFnSc1	takový
disputace	disputace	k1gFnSc1	disputace
někdy	někdy	k6eAd1	někdy
odehrála	odehrát	k5eAaPmAgFnS	odehrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
záhlaví	záhlaví	k1gNnSc6	záhlaví
Luther	Luthra	k1gFnPc2	Luthra
zve	zvát	k5eAaImIp3nS	zvát
učence	učenec	k1gMnPc4	učenec
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
<g/>
.	.	kIx.	.
</s>
<s>
Pořádat	pořádat	k5eAaImF	pořádat
podobné	podobný	k2eAgFnSc2d1	podobná
debaty	debata	k1gFnSc2	debata
bylo	být	k5eAaImAgNnS	být
privilegium	privilegium	k1gNnSc1	privilegium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
Luther	Luthra	k1gFnPc2	Luthra
měl	mít	k5eAaImAgMnS	mít
jakožto	jakožto	k8xS	jakožto
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
to	ten	k3xDgNnSc4	ten
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
forma	forma	k1gFnSc1	forma
akademického	akademický	k2eAgNnSc2d1	akademické
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1516	[number]	k4	1516
a	a	k8xC	a
1521	[number]	k4	1521
Luther	Luthra	k1gFnPc2	Luthra
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
připravil	připravit	k5eAaPmAgInS	připravit
dvacet	dvacet	k4xCc4	dvacet
souborů	soubor	k1gInPc2	soubor
disputačních	disputační	k2eAgFnPc2d1	disputační
tezí	teze	k1gFnPc2	teze
<g/>
.	.	kIx.	.
</s>
<s>
Wittenberský	Wittenberský	k2eAgMnSc1d1	Wittenberský
teolog	teolog	k1gMnSc1	teolog
Andreas	Andreas	k1gMnSc1	Andreas
Karlstadt	Karlstadt	k1gMnSc1	Karlstadt
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1517	[number]	k4	1517
podobný	podobný	k2eAgInSc4d1	podobný
soubor	soubor	k1gInSc4	soubor
tezí	teze	k1gFnPc2	teze
<g/>
,	,	kIx,	,
teologicky	teologicky	k6eAd1	teologicky
ještě	ještě	k9	ještě
radikálnější	radikální	k2eAgMnSc1d2	radikálnější
než	než	k8xS	než
Lutherův	Lutherův	k2eAgMnSc1d1	Lutherův
<g/>
.	.	kIx.	.
</s>
<s>
Přibil	přibít	k5eAaPmAgMnS	přibít
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
to	ten	k3xDgNnSc4	ten
údajně	údajně	k6eAd1	údajně
učinil	učinit	k5eAaImAgMnS	učinit
i	i	k9	i
Luther	Luthra	k1gFnPc2	Luthra
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
95	[number]	k4	95
tezemi	teze	k1gFnPc7	teze
<g/>
.	.	kIx.	.
</s>
<s>
Karlstadt	Karlstadt	k1gInSc1	Karlstadt
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
své	svůj	k3xOyFgFnPc4	svůj
teze	teze	k1gFnPc4	teze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
vystavovaly	vystavovat	k5eAaImAgInP	vystavovat
tamní	tamní	k2eAgInPc1d1	tamní
ostatky	ostatek	k1gInPc1	ostatek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
provokaci	provokace	k1gFnSc4	provokace
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Luther	Luthra	k1gFnPc2	Luthra
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
svých	svůj	k3xOyFgFnPc2	svůj
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
slavnosti	slavnost	k1gFnSc2	slavnost
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
v	v	k7c4	v
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
úctu	úcta	k1gFnSc4	úcta
k	k	k7c3	k
ostatkům	ostatek	k1gInPc3	ostatek
ve	v	k7c6	v
wittenberském	wittenberský	k2eAgInSc6d1	wittenberský
kostele	kostel	k1gInSc6	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
.95	.95	k4	.95
tezí	teze	k1gFnPc2	teze
mělo	mít	k5eAaImAgNnS	mít
podnítit	podnítit	k5eAaPmF	podnítit
akademickou	akademický	k2eAgFnSc4d1	akademická
debatu	debata	k1gFnSc4	debata
<g/>
,	,	kIx,	,
ne	ne	k9	ne
lidovou	lidový	k2eAgFnSc4d1	lidová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Luther	Luthra	k1gFnPc2	Luthra
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
prorocký	prorocký	k2eAgInSc4d1	prorocký
a	a	k8xC	a
významný	významný	k2eAgInSc4d1	významný
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opustil	opustit	k5eAaPmAgMnS	opustit
jméno	jméno	k1gNnSc4	jméno
Luder	Luder	k?	Luder
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
Luther	Luthra	k1gFnPc2	Luthra
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
Eleutherius	Eleutherius	k1gInSc4	Eleutherius
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
řecky	řecky	k6eAd1	řecky
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zřejmě	zřejmě	k6eAd1	zřejmě
projevil	projevit	k5eAaPmAgInS	projevit
svou	svůj	k3xOyFgFnSc4	svůj
svobodu	svoboda	k1gFnSc4	svoboda
od	od	k7c2	od
scholastické	scholastický	k2eAgFnSc2d1	scholastická
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yRgFnSc3	který
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
dříve	dříve	k6eAd2	dříve
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nepřál	přát	k5eNaImAgMnS	přát
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
rozšíření	rozšíření	k1gNnSc4	rozšíření
Tezí	teze	k1gFnPc2	teze
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
historička	historička	k1gFnSc1	historička
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Eisensteinová	Eisensteinová	k1gFnSc1	Eisensteinová
dokládala	dokládat	k5eAaImAgFnS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lutherovo	Lutherův	k2eAgNnSc1d1	Lutherovo
údajné	údajný	k2eAgNnSc1d1	údajné
překvapení	překvapení	k1gNnSc1	překvapení
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gInSc7	jejich
úspěchem	úspěch	k1gInSc7	úspěch
asi	asi	k9	asi
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
prvek	prvek	k1gInSc4	prvek
sebeklamu	sebeklam	k1gInSc2	sebeklam
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
kolega	kolega	k1gMnSc1	kolega
Hans	Hans	k1gMnSc1	Hans
Hillerbrand	Hillerbrand	k1gInSc4	Hillerbrand
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Luther	Luthra	k1gFnPc2	Luthra
určitě	určitě	k6eAd1	určitě
přál	přát	k5eAaImAgMnS	přát
vyvolat	vyvolat	k5eAaPmF	vyvolat
velký	velký	k2eAgInSc4d1	velký
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
Luther	Luthra	k1gFnPc2	Luthra
použil	použít	k5eAaPmAgInS	použít
akademickou	akademický	k2eAgFnSc4d1	akademická
povahu	povaha	k1gFnSc4	povaha
Tezí	teze	k1gFnPc2	teze
jako	jako	k8xS	jako
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
dovolil	dovolit	k5eAaPmAgInS	dovolit
napadnout	napadnout	k5eAaPmF	napadnout
zavedené	zavedený	k2eAgInPc4d1	zavedený
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
popírat	popírat	k5eAaImF	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
církevní	církevní	k2eAgFnSc4d1	církevní
nauku	nauka	k1gFnSc4	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Sepsání	sepsání	k1gNnSc1	sepsání
souboru	soubor	k1gInSc2	soubor
tezí	teze	k1gFnPc2	teze
k	k	k7c3	k
diskuzi	diskuze	k1gFnSc3	diskuze
totiž	totiž	k9	totiž
nutně	nutně	k6eAd1	nutně
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
autor	autor	k1gMnSc1	autor
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgMnS	moct
Luther	Luthra	k1gFnPc2	Luthra
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
buřičské	buřičský	k2eAgMnPc4d1	buřičský
myšlenky	myšlenka	k1gFnSc2	myšlenka
Tezí	teze	k1gFnPc2	teze
nezastává	zastávat	k5eNaImIp3nS	zastávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šíření	šíření	k1gNnPc4	šíření
a	a	k8xC	a
publikace	publikace	k1gFnPc4	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1517	[number]	k4	1517
Luther	Luthra	k1gFnPc2	Luthra
poslal	poslat	k5eAaPmAgMnS	poslat
dopis	dopis	k1gInSc4	dopis
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
mohučskému	mohučský	k2eAgInSc3d1	mohučský
Albrechtu	Albrecht	k1gMnSc3	Albrecht
Braniborskému	braniborský	k2eAgMnSc3d1	braniborský
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gFnSc7	jehož
autoritou	autorita	k1gFnSc7	autorita
se	se	k3xPyFc4	se
odpustky	odpustek	k1gInPc1	odpustek
prodávaly	prodávat	k5eAaImAgInP	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
je	být	k5eAaImIp3nS	být
formulován	formulovat	k5eAaImNgInS	formulovat
jako	jako	k9	jako
loajální	loajální	k2eAgNnPc4d1	loajální
upozornění	upozornění	k1gNnPc4	upozornění
na	na	k7c4	na
pastorační	pastorační	k2eAgInPc4d1	pastorační
problémy	problém	k1gInPc4	problém
vznikající	vznikající	k2eAgInPc4d1	vznikající
kázáním	kázání	k1gNnSc7	kázání
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Albrecht	Albrecht	k1gMnSc1	Albrecht
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
autoritu	autorita	k1gFnSc4	autorita
káže	kázat	k5eAaImIp3nS	kázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
obavu	obava	k1gFnSc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
lidi	člověk	k1gMnPc4	člověk
odvádí	odvádět	k5eAaImIp3nS	odvádět
od	od	k7c2	od
evangelia	evangelium	k1gNnSc2	evangelium
a	a	k8xC	a
že	že	k8xS	že
odpustkové	odpustkový	k2eAgNnSc1d1	odpustkový
kázání	kázání	k1gNnSc1	kázání
může	moct	k5eAaImIp3nS	moct
zhanobit	zhanobit	k5eAaPmF	zhanobit
Albrechtovo	Albrechtův	k2eAgNnSc4d1	Albrechtovo
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Neodsuzuje	odsuzovat	k5eNaImIp3nS	odsuzovat
odpustky	odpustek	k1gInPc4	odpustek
ani	ani	k8xC	ani
soudobou	soudobý	k2eAgFnSc4d1	soudobá
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
kázání	kázání	k1gNnSc1	kázání
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
pronášeno	pronášen	k2eAgNnSc1d1	pronášeno
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
ho	on	k3xPp3gMnSc4	on
neslyšel	slyšet	k5eNaImAgMnS	slyšet
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
své	svůj	k3xOyFgNnSc1	svůj
znepokojení	znepokojení	k1gNnSc1	znepokojení
ohledně	ohledně	k7c2	ohledně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
odpustky	odpustek	k1gInPc4	odpustek
špatně	špatně	k6eAd1	špatně
chápou	chápat	k5eAaImIp3nP	chápat
a	a	k8xC	a
že	že	k8xS	že
kázání	kázání	k1gNnSc4	kázání
podporují	podporovat	k5eAaImIp3nP	podporovat
toto	tento	k3xDgNnSc4	tento
neporozumění	neporozumění	k1gNnSc4	neporozumění
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpustky	odpustek	k1gInPc1	odpustek
mohou	moct	k5eAaImIp3nP	moct
zahladit	zahladit	k5eAaPmF	zahladit
každý	každý	k3xTgInSc4	každý
hřích	hřích	k1gInSc4	hřích
anebo	anebo	k8xC	anebo
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
člověka	člověk	k1gMnSc4	člověk
zbavit	zbavit	k5eAaPmF	zbavit
viny	vina	k1gFnSc2	vina
i	i	k8xC	i
trestu	trest	k1gInSc2	trest
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doušce	douška	k1gFnSc6	douška
Luther	Luthra	k1gFnPc2	Luthra
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Albrecht	Albrecht	k1gMnSc1	Albrecht
může	moct	k5eAaImIp3nS	moct
najít	najít	k5eAaPmF	najít
teze	teze	k1gFnSc1	teze
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
přiložené	přiložený	k2eAgFnSc6d1	přiložená
k	k	k7c3	k
dopisu	dopis	k1gInSc3	dopis
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
seznat	seznat	k5eAaPmF	seznat
nejistotu	nejistota	k1gFnSc4	nejistota
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
se	s	k7c7	s
sebevědomím	sebevědomí	k1gNnSc7	sebevědomí
kazatelů	kazatel	k1gMnPc2	kazatel
hlásajících	hlásající	k2eAgFnPc2d1	hlásající
výhody	výhoda	k1gFnPc4	výhoda
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
sezváním	sezvání	k1gNnSc7	sezvání
disputace	disputace	k1gFnSc2	disputace
byly	být	k5eAaImAgFnP	být
teze	teze	k1gFnPc1	teze
vytištěny	vytisknout	k5eAaPmNgFnP	vytisknout
univerzitní	univerzitní	k2eAgFnSc7d1	univerzitní
tiskárnou	tiskárna	k1gFnSc7	tiskárna
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
publikovány	publikován	k2eAgInPc1d1	publikován
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
výtisk	výtisk	k1gInSc1	výtisk
wittenberského	wittenberský	k2eAgInSc2d1	wittenberský
tisku	tisk	k1gInSc2	tisk
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Luther	Luthra	k1gFnPc2	Luthra
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
slavný	slavný	k2eAgInSc1d1	slavný
a	a	k8xC	a
význam	význam	k1gInSc1	význam
dokumentu	dokument	k1gInSc2	dokument
nebyl	být	k5eNaImAgInS	být
rozpoznán	rozpoznán	k2eAgInSc1d1	rozpoznán
<g/>
.	.	kIx.	.
</s>
<s>
Wittenberské	Wittenberský	k2eAgFnPc1d1	Wittenberský
univerzitní	univerzitní	k2eAgFnPc1d1	univerzitní
stanovy	stanova	k1gFnPc1	stanova
požadovaly	požadovat	k5eAaImAgFnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
teze	teze	k1gFnPc1	teze
vyvěšovaly	vyvěšovat	k5eAaImAgFnP	vyvěšovat
na	na	k7c6	na
dveřích	dveře	k1gFnPc6	dveře
všech	všecek	k3xTgInPc2	všecek
kostelů	kostel	k1gInPc2	kostel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Philipp	Philipp	k1gMnSc1	Philipp
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
vyvěšení	vyvěšení	k1gNnSc6	vyvěšení
tezí	teze	k1gFnPc2	teze
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
pouze	pouze	k6eAd1	pouze
dveře	dveře	k1gFnPc4	dveře
kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
také	také	k9	také
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Luther	Luthra	k1gFnPc2	Luthra
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
Teze	teze	k1gFnSc2	teze
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
několika	několik	k4yIc7	několik
Lutherovými	Lutherův	k2eAgInPc7d1	Lutherův
výroky	výrok	k1gInPc7	výrok
o	o	k7c6	o
sledu	sled	k1gInSc6	sled
událostí	událost	k1gFnPc2	událost
<g/>
;	;	kIx,	;
a	a	k8xC	a
Luther	Luthra	k1gFnPc2	Luthra
vždy	vždy	k6eAd1	vždy
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
přednesl	přednést	k5eAaPmAgMnS	přednést
své	své	k1gNnSc4	své
námitky	námitka	k1gFnSc2	námitka
správnou	správný	k2eAgFnSc7d1	správná
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
že	že	k8xS	že
nepodněcoval	podněcovat	k5eNaImAgMnS	podněcovat
veřejný	veřejný	k2eAgInSc4d1	veřejný
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
Luther	Luthra	k1gFnPc2	Luthra
později	pozdě	k6eAd2	pozdě
vnímal	vnímat	k5eAaImAgInS	vnímat
dopis	dopis	k1gInSc1	dopis
Albrechtovi	Albrechtův	k2eAgMnPc1d1	Albrechtův
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
jako	jako	k8xS	jako
začátek	začátek	k1gInSc4	začátek
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
vyvěsil	vyvěsit	k5eAaPmAgMnS	vyvěsit
Teze	teze	k1gFnPc4	teze
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
kostela	kostel	k1gInSc2	kostel
až	až	k9	až
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
také	také	k9	také
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
vůbec	vůbec	k9	vůbec
nepřibil	přibít	k5eNaPmAgMnS	přibít
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
jakkoli	jakkoli	k6eAd1	jakkoli
<g/>
,	,	kIx,	,
Teze	teze	k1gFnPc1	teze
byly	být	k5eAaImAgFnP	být
dobře	dobře	k6eAd1	dobře
známy	znám	k2eAgFnPc1d1	známa
wittenberské	wittenberský	k2eAgFnSc3d1	wittenberský
intelektuální	intelektuální	k2eAgFnSc3d1	intelektuální
elitě	elita	k1gFnSc3	elita
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
Luther	Luthra	k1gFnPc2	Luthra
zaslal	zaslat	k5eAaPmAgMnS	zaslat
Albrechtovi	Albrecht	k1gMnSc6	Albrecht
<g/>
.	.	kIx.	.
<g/>
Teze	teze	k1gFnSc2	teze
byly	být	k5eAaImAgInP	být
množeny	množit	k5eAaImNgInP	množit
a	a	k8xC	a
šířeny	šířit	k5eAaImNgInP	šířit
zájemcům	zájemce	k1gMnPc3	zájemce
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Luther	Luthra	k1gFnPc2	Luthra
poslal	poslat	k5eAaPmAgMnS	poslat
dopis	dopis	k1gInSc4	dopis
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgFnPc1d1	Latinská
Teze	teze	k1gFnPc1	teze
byly	být	k5eAaImAgFnP	být
otištěny	otisknout	k5eAaPmNgFnP	otisknout
v	v	k7c6	v
čtyřstránkovém	čtyřstránkový	k2eAgInSc6d1	čtyřstránkový
letáku	leták	k1gInSc6	leták
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
a	a	k8xC	a
jako	jako	k8xS	jako
plakáty	plakát	k1gInPc1	plakát
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
a	a	k8xC	a
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
výtisků	výtisk	k1gInPc2	výtisk
latinských	latinský	k2eAgFnPc2d1	Latinská
Tezí	teze	k1gFnPc2	teze
<g/>
.	.	kIx.	.
</s>
<s>
Kaspar	Kaspar	k1gMnSc1	Kaspar
Nützel	Nützel	k1gMnSc1	Nützel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
exempláře	exemplář	k1gInSc2	exemplář
tohoto	tento	k3xDgInSc2	tento
překladu	překlad	k1gInSc2	překlad
byl	být	k5eAaImAgMnS	být
rozeslány	rozeslán	k2eAgFnPc4d1	rozeslána
několika	několik	k4yIc3	několik
zájemcům	zájemce	k1gMnPc3	zájemce
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možná	možná	k9	možná
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
výtisky	výtisk	k1gInPc4	výtisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
zřejmě	zřejmě	k6eAd1	zřejmě
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Lutherův	Lutherův	k2eAgInSc4d1	Lutherův
dopis	dopis	k1gInSc4	dopis
s	s	k7c7	s
Tezemi	teze	k1gFnPc7	teze
kolem	kolem	k7c2	kolem
konce	konec	k1gInSc2	konec
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
stanovisko	stanovisko	k1gNnSc4	stanovisko
teology	teolog	k1gMnPc4	teolog
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
a	a	k8xC	a
jednal	jednat	k5eAaImAgMnS	jednat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
poradci	poradce	k1gMnPc7	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
doporučili	doporučit	k5eAaPmAgMnP	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
odpustkovou	odpustkový	k2eAgFnSc7d1	odpustková
bulou	bula	k1gFnSc7	bula
Lutherovi	Luther	k1gMnSc3	Luther
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
kázat	kázat	k5eAaImF	kázat
proti	proti	k7c3	proti
odpustkům	odpustek	k1gInPc3	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
zákaz	zákaz	k1gInSc4	zákaz
římskou	římský	k2eAgFnSc4d1	římská
kurii	kurie	k1gFnSc4	kurie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byl	být	k5eAaImAgMnS	být
Luther	Luthra	k1gFnPc2	Luthra
okamžitě	okamžitě	k6eAd1	okamžitě
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1518	[number]	k4	1518
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
X.	X.	kA	X.
požádal	požádat	k5eAaPmAgMnS	požádat
představeného	představený	k1gMnSc2	představený
obutých	obutý	k2eAgMnPc2d1	obutý
augustiniánů	augustinián	k1gMnPc2	augustinián
<g/>
,	,	kIx,	,
Lutherova	Lutherův	k2eAgInSc2d1	Lutherův
církevního	církevní	k2eAgInSc2d1	církevní
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Luthera	Luther	k1gMnSc4	Luther
přiměl	přimět	k5eAaPmAgInS	přimět
přestat	přestat	k5eAaPmF	přestat
šířit	šířit	k5eAaImF	šířit
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Dominikán	dominikán	k1gMnSc1	dominikán
Sylvester	Sylvester	k1gMnSc1	Sylvester
Mazzolini	Mazzolin	k2eAgMnPc1d1	Mazzolin
byl	být	k5eAaImAgInS	být
také	také	k9	také
pověřen	pověřit	k5eAaPmNgMnS	pověřit
sepsat	sepsat	k5eAaPmF	sepsat
dobrozdání	dobrozdání	k1gNnSc3	dobrozdání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
proti	proti	k7c3	proti
Lutherovi	Luther	k1gMnSc3	Luther
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Mazzolini	Mazzolin	k2eAgMnPc1d1	Mazzolin
napsal	napsat	k5eAaBmAgMnS	napsat
Dialog	dialog	k1gInSc4	dialog
proti	proti	k7c3	proti
domýšlivým	domýšlivý	k2eAgFnPc3d1	domýšlivá
tezím	teze	k1gFnPc3	teze
Martina	Martin	k2eAgFnSc1d1	Martina
Luthera	Luthera	k1gFnSc1	Luthera
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
Lutherovo	Lutherův	k2eAgNnSc4d1	Lutherovo
zpochybnění	zpochybnění	k1gNnSc4	zpochybnění
papežovy	papežův	k2eAgFnSc2d1	papežova
autority	autorita	k1gFnSc2	autorita
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
stížnosti	stížnost	k1gFnPc4	stížnost
ohledně	ohledně	k7c2	ohledně
kázání	kázání	k1gNnSc2	kázání
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1518	[number]	k4	1518
předvolání	předvolání	k1gNnSc4	předvolání
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Reagoval	reagovat	k5eAaBmAgInS	reagovat
Vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
disputace	disputace	k1gFnSc2	disputace
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
odpustků	odpustek	k1gInPc2	odpustek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
očistit	očistit	k5eAaPmF	očistit
od	od	k7c2	od
obvinění	obvinění	k1gNnSc2	obvinění
<g/>
,	,	kIx,	,
že	že	k8xS	že
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
sepsal	sepsat	k5eAaPmAgMnS	sepsat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
Luther	Luthra	k1gFnPc2	Luthra
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc1	jejich
důsledky	důsledek	k1gInPc1	důsledek
ho	on	k3xPp3gMnSc4	on
zavádějí	zavádět	k5eAaImIp3nP	zavádět
dál	daleko	k6eAd2	daleko
od	od	k7c2	od
oficiální	oficiální	k2eAgFnSc2d1	oficiální
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
původně	původně	k6eAd1	původně
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
spor	spor	k1gInSc1	spor
zřejmě	zřejmě	k6eAd1	zřejmě
nezačínal	začínat	k5eNaImAgInS	začínat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
povede	povést	k5eAaPmIp3nS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
první	první	k4xOgNnSc4	první
Lutherovo	Lutherův	k2eAgNnSc4d1	Lutherovo
reformátorské	reformátorský	k2eAgNnSc4d1	reformátorské
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Tetzel	Tetzel	k1gMnSc1	Tetzel
na	na	k7c4	na
Teze	teze	k1gFnPc4	teze
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
Lutherovu	Lutherův	k2eAgNnSc3d1	Lutherovo
upálení	upálení	k1gNnSc3	upálení
pro	pro	k7c4	pro
herezi	hereze	k1gFnSc4	hereze
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
teologa	teolog	k1gMnSc4	teolog
Konrada	Konrada	k1gFnSc1	Konrada
Wimpinu	Wimpina	k1gFnSc4	Wimpina
napsat	napsat	k5eAaBmF	napsat
106	[number]	k4	106
tezí	teze	k1gFnPc2	teze
proti	proti	k7c3	proti
Lutherovu	Lutherův	k2eAgNnSc3d1	Lutherovo
dílu	dílo	k1gNnSc3	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc4	ten
pak	pak	k6eAd1	pak
Tetzel	Tetzel	k1gMnSc1	Tetzel
hájil	hájit	k5eAaImAgMnS	hájit
v	v	k7c6	v
disputaci	disputace	k1gFnSc6	disputace
před	před	k7c7	před
univerzitou	univerzita	k1gFnSc7	univerzita
ve	v	k7c6	v
Franfurtu	Franfurt	k1gInSc6	Franfurt
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1518	[number]	k4	1518
<g/>
.	.	kIx.	.
800	[number]	k4	800
výtisků	výtisk	k1gInPc2	výtisk
disputace	disputace	k1gFnSc2	disputace
bylo	být	k5eAaImAgNnS	být
posláno	poslat	k5eAaPmNgNnS	poslat
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
do	do	k7c2	do
Wittenbergu	Wittenberg	k1gInSc2	Wittenberg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tamní	tamní	k2eAgMnPc1d1	tamní
studenti	student	k1gMnPc1	student
je	on	k3xPp3gMnPc4	on
sebrali	sebrat	k5eAaPmAgMnP	sebrat
knihkupci	knihkupec	k1gMnPc1	knihkupec
a	a	k8xC	a
spálili	spálit	k5eAaPmAgMnP	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
se	se	k3xPyFc4	se
začínal	začínat	k5eAaImAgInS	začínat
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
bát	bát	k5eAaImF	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
vymyká	vymykat	k5eAaImIp3nS	vymykat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
odpůrce	odpůrce	k1gMnSc4	odpůrce
<g/>
,	,	kIx,	,
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
Promluvu	promluva	k1gFnSc4	promluva
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
a	a	k8xC	a
milosti	milost	k1gFnSc6	milost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nezpochybňuje	zpochybňovat	k5eNaImIp3nS	zpochybňovat
papežovu	papežův	k2eAgFnSc4d1	papežova
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
německy	německy	k6eAd1	německy
psaný	psaný	k2eAgInSc1d1	psaný
leták	leták	k1gInSc1	leták
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
ho	on	k3xPp3gMnSc4	on
snadno	snadno	k6eAd1	snadno
pochopit	pochopit	k5eAaPmF	pochopit
laici	laik	k1gMnPc1	laik
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Lutherovou	Lutherová	k1gFnSc4	Lutherová
první	první	k4xOgFnSc4	první
široce	široko	k6eAd1	široko
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
dvaceti	dvacet	k4xCc2	dvacet
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Tetzel	Tetzet	k5eAaImAgMnS	Tetzet
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
vyvrácením	vyvrácení	k1gNnSc7	vyvrácení
psaným	psaný	k2eAgNnSc7d1	psané
bod	bod	k1gInSc4	bod
po	po	k7c6	po
bodu	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
široce	široko	k6eAd1	široko
citoval	citovat	k5eAaBmAgMnS	citovat
bibli	bible	k1gFnSc4	bible
a	a	k8xC	a
významné	významný	k2eAgMnPc4d1	významný
teology	teolog	k1gMnPc4	teolog
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
leták	leták	k1gInSc1	leták
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
jako	jako	k8xC	jako
Lutherův	Lutherův	k2eAgInSc1d1	Lutherův
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Lutherova	Lutherův	k2eAgFnSc1d1	Lutherova
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
Tetzelův	Tetzelův	k2eAgInSc4d1	Tetzelův
leták	leták	k1gInSc4	leták
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
prodejním	prodejní	k2eAgInSc7d1	prodejní
úspěchem	úspěch	k1gInSc7	úspěch
svého	svůj	k3xOyFgMnSc2	svůj
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgMnSc7d1	další
prominentním	prominentní	k2eAgMnSc7d1	prominentní
odpůrcem	odpůrce	k1gMnSc7	odpůrce
Tezí	teze	k1gFnPc2	teze
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Eck	Eck	k1gMnSc1	Eck
<g/>
,	,	kIx,	,
Lutherův	Lutherův	k2eAgMnSc1d1	Lutherův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Ingolstadtu	Ingolstadt	k1gInSc6	Ingolstadt
<g/>
.	.	kIx.	.
</s>
<s>
Eck	Eck	k?	Eck
napsal	napsat	k5eAaBmAgInS	napsat
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc4d1	určené
biskupovi	biskup	k1gMnSc6	biskup
z	z	k7c2	z
Eichstättu	Eichstätt	k1gInSc2	Eichstätt
a	a	k8xC	a
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Obelisky	obelisk	k1gInPc1	obelisk
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
křížky	křížek	k1gInPc4	křížek
zvané	zvaný	k2eAgInPc4d1	zvaný
obelisky	obelisk	k1gInPc4	obelisk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
používaly	používat	k5eAaImAgInP	používat
k	k	k7c3	k
vyznačování	vyznačování	k1gNnSc3	vyznačování
heretických	heretický	k2eAgFnPc2d1	heretická
pasáží	pasáž	k1gFnPc2	pasáž
v	v	k7c6	v
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
a	a	k8xC	a
nečekaně	nečekaně	k6eAd1	nečekaně
osobní	osobní	k2eAgInSc1d1	osobní
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
vinící	vinící	k2eAgFnSc1d1	vinící
Luthera	Luthera	k1gFnSc1	Luthera
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
a	a	k8xC	a
hlouposti	hloupost	k1gFnSc2	hloupost
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
soukromě	soukromě	k6eAd1	soukromě
textem	text	k1gInSc7	text
Asterisky	Asteriska	k1gFnSc2	Asteriska
<g/>
,	,	kIx,	,
nazvaným	nazvaný	k2eAgFnPc3d1	nazvaná
podle	podle	k7c2	podle
hvězdičky	hvězdička	k1gFnSc2	hvězdička
(	(	kIx(	(
<g/>
asterisku	asterisek	k1gInSc2	asterisek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používané	používaný	k2eAgFnSc2d1	používaná
tehdy	tehdy	k6eAd1	tehdy
k	k	k7c3	k
vyznačení	vyznačení	k1gNnSc3	vyznačení
důležitých	důležitý	k2eAgInPc2d1	důležitý
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
rozhněvaná	rozhněvaný	k2eAgFnSc1d1	rozhněvaná
odpověď	odpověď	k1gFnSc1	odpověď
a	a	k8xC	a
Luther	Luthra	k1gFnPc2	Luthra
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eck	Eck	k1gFnSc1	Eck
nerozumí	rozumět	k5eNaImIp3nS	rozumět
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgFnPc4	který
píše	psát	k5eAaImIp3nS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
Lutherem	Luther	k1gInSc7	Luther
a	a	k8xC	a
Eckem	Eck	k1gInSc7	Eck
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
lipské	lipský	k2eAgFnSc6d1	Lipská
disputaci	disputace	k1gFnSc6	disputace
<g/>
.	.	kIx.	.
<g/>
Luther	Luthra	k1gFnPc2	Luthra
byl	být	k5eAaImAgInS	být
papežskou	papežský	k2eAgFnSc7d1	Papežská
autoritou	autorita	k1gFnSc7	autorita
předvolán	předvolat	k5eAaPmNgMnS	předvolat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1518	[number]	k4	1518
hájil	hájit	k5eAaImAgMnS	hájit
proti	proti	k7c3	proti
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
před	před	k7c7	před
Tommasem	Tommas	k1gInSc7	Tommas
de	de	k?	de
Viem	Viem	k1gMnSc1	Viem
v	v	k7c6	v
Augsburgu	Augsburg	k1gInSc6	Augsburg
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Vio	Vio	k1gMnSc1	Vio
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
Lutherovi	Luther	k1gMnSc3	Luther
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
široce	široko	k6eAd1	široko
debatoval	debatovat	k5eAaImAgMnS	debatovat
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
údajných	údajný	k2eAgFnPc6d1	údajná
herezích	hereze	k1gFnPc6	hereze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
sporné	sporný	k2eAgInPc4d1	sporný
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byla	být	k5eAaImAgFnS	být
teze	teze	k1gFnSc1	teze
58	[number]	k4	58
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
nemůže	moct	k5eNaImIp3nS	moct
používat	používat	k5eAaImF	používat
poklad	poklad	k1gInSc4	poklad
zásluh	zásluha	k1gFnPc2	zásluha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpouštěl	odpouštět	k5eAaImAgInS	odpouštět
časné	časný	k2eAgInPc4d1	časný
tresty	trest	k1gInPc4	trest
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
papežskou	papežský	k2eAgFnSc7d1	Papežská
bulou	bula	k1gFnSc7	bula
Unigenitus	Unigenitus	k1gInSc1	Unigenitus
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1343	[number]	k4	1343
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
bodem	bod	k1gInSc7	bod
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
odpuštěno	odpustit	k5eAaPmNgNnS	odpustit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
dostal	dostat	k5eAaPmAgMnS	dostat
kněžské	kněžský	k2eAgNnSc4d1	kněžské
rozhřešení	rozhřešení	k1gNnSc4	rozhřešení
<g/>
.	.	kIx.	.
</s>
<s>
Lutherovo	Lutherův	k2eAgNnSc1d1	Lutherovo
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
teze	teze	k1gFnSc2	teze
7	[number]	k4	7
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
božího	boží	k2eAgInSc2d1	boží
slibu	slib	k1gInSc2	slib
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
de	de	k?	de
Vio	Vio	k1gMnSc1	Vio
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokorný	pokorný	k2eAgMnSc1d1	pokorný
křesťan	křesťan	k1gMnSc1	křesťan
si	se	k3xPyFc3	se
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
nárokovat	nárokovat	k5eAaImF	nárokovat
jistotu	jistota	k1gFnSc4	jistota
ohledně	ohledně	k7c2	ohledně
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
před	před	k7c7	před
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odvolat	odvolat	k5eAaPmF	odvolat
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
případ	případ	k1gInSc4	případ
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
teologové	teolog	k1gMnPc1	teolog
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
požadavek	požadavek	k1gInSc1	požadavek
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Luther	Luthra	k1gFnPc2	Luthra
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
Augsburgu	Augsburg	k1gInSc2	Augsburg
odvolal	odvolat	k5eAaPmAgMnS	odvolat
k	k	k7c3	k
papežovi	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
exkomunikován	exkomunikován	k2eAgInSc1d1	exkomunikován
roku	rok	k1gInSc3	rok
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
když	když	k8xS	když
spálil	spálit	k5eAaPmAgMnS	spálit
papežskou	papežský	k2eAgFnSc4d1	Papežská
bulu	bula	k1gFnSc4	bula
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
hrozila	hrozit	k5eAaImAgFnS	hrozit
exkomunikací	exkomunikace	k1gFnSc7	exkomunikace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
neodvolá	odvolat	k5eNaPmIp3nS	odvolat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
odpustky	odpustek	k1gInPc4	odpustek
zahájený	zahájený	k2eAgInSc1d1	zahájený
95	[number]	k4	95
tezemi	teze	k1gFnPc7	teze
byl	být	k5eAaImAgInS	být
počátkem	počátkem	k7c2	počátkem
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
schizmatu	schizma	k1gNnSc3	schizma
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
hluboké	hluboký	k2eAgFnPc4d1	hluboká
a	a	k8xC	a
trvalé	trvalý	k2eAgFnPc4d1	trvalá
sociální	sociální	k2eAgFnPc4d1	sociální
a	a	k8xC	a
politické	politický	k2eAgFnPc4d1	politická
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
později	pozdě	k6eAd2	pozdě
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
záležitost	záležitost	k1gFnSc1	záležitost
odpustků	odpustek	k1gInPc2	odpustek
nebyla	být	k5eNaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
spory	spor	k1gInPc7	spor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
debata	debata	k1gFnSc1	debata
s	s	k7c7	s
Erasmem	Erasm	k1gInSc7	Erasm
Rotterdamským	rotterdamský	k2eAgInSc7d1	rotterdamský
o	o	k7c6	o
svobodné	svobodný	k2eAgFnSc6d1	svobodná
vůli	vůle	k1gFnSc6	vůle
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
nepovažoval	považovat	k5eNaImAgMnS	považovat
tento	tento	k3xDgInSc4	tento
spor	spor	k1gInSc4	spor
za	za	k7c4	za
důležitý	důležitý	k2eAgInSc4d1	důležitý
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
intelektuální	intelektuální	k2eAgInSc4d1	intelektuální
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Tezí	teze	k1gFnPc2	teze
prý	prý	k9	prý
ještě	ještě	k6eAd1	ještě
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
papežencem	papeženec	k1gMnSc7	papeženec
<g/>
"	"	kIx"	"
a	a	k8xC	a
ani	ani	k8xC	ani
si	se	k3xPyFc3	se
nemyslel	myslet	k5eNaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Teze	teze	k1gFnPc1	teze
znamenají	znamenat	k5eAaImIp3nP	znamenat
rozkol	rozkol	k1gInSc4	rozkol
se	s	k7c7	s
zavedenou	zavedený	k2eAgFnSc7d1	zavedená
katolickou	katolický	k2eAgFnSc7d1	katolická
naukou	nauka	k1gFnSc7	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
spor	spor	k1gInSc1	spor
o	o	k7c4	o
odpustky	odpustek	k1gInPc4	odpustek
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
hnutí	hnutí	k1gNnSc1	hnutí
zvané	zvaný	k2eAgNnSc1d1	zvané
později	pozdě	k6eAd2	pozdě
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
také	také	k9	také
Luthera	Luther	k1gMnSc4	Luther
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
vůdčí	vůdčí	k2eAgFnSc2d1	vůdčí
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
reformačním	reformační	k2eAgNnSc6d1	reformační
hnutí	hnutí	k1gNnSc6	hnutí
zastával	zastávat	k5eAaImAgMnS	zastávat
<g/>
.	.	kIx.	.
</s>
<s>
Teze	teze	k1gFnSc1	teze
také	také	k9	také
odhalily	odhalit	k5eAaPmAgInP	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Luther	Luthra	k1gFnPc2	Luthra
nepovažuje	považovat	k5eNaImIp3nS	považovat
církevní	církevní	k2eAgNnSc1d1	církevní
kázání	kázání	k1gNnSc1	kázání
odpustků	odpustek	k1gInPc2	odpustek
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
a	a	k8xC	a
domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
kázání	kázání	k1gNnSc1	kázání
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
laiky	laik	k1gMnPc4	laik
vážnému	vážný	k2eAgNnSc3d1	vážné
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
odporovaly	odporovat	k5eAaImAgInP	odporovat
výnosu	výnos	k1gInSc2	výnos
papeže	papež	k1gMnSc4	papež
Klementa	Klement	k1gMnSc4	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
na	na	k7c4	na
papežovu	papežův	k2eAgFnSc4d1	papežova
autoritu	autorita	k1gFnSc4	autorita
byl	být	k5eAaImAgInS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
dalších	další	k2eAgInPc2d1	další
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.31	.31	k4	.31
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Luther	Luthra	k1gFnPc2	Luthra
poslal	poslat	k5eAaPmAgInS	poslat
Teze	teze	k1gFnPc4	teze
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
Braniborskému	braniborský	k2eAgMnSc3d1	braniborský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
připomínal	připomínat	k5eAaImAgInS	připomínat
jako	jako	k9	jako
počátek	počátek	k1gInSc4	počátek
reformace	reformace	k1gFnSc2	reformace
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1527	[number]	k4	1527
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Luther	Luthra	k1gFnPc2	Luthra
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
pozvedli	pozvednout	k5eAaPmAgMnP	pozvednout
sklenky	sklenka	k1gFnPc4	sklenka
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
"	"	kIx"	"
<g/>
zašlapání	zašlapání	k1gNnSc2	zašlapání
odpustků	odpustek	k1gInPc2	odpustek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uveřejnění	uveřejnění	k1gNnSc1	uveřejnění
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
reformace	reformace	k1gFnSc1	reformace
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
jako	jako	k8xC	jako
počátek	počátek	k1gInSc1	počátek
hnutí	hnutí	k1gNnSc2	hnutí
Philippem	Philipp	k1gMnSc7	Philipp
Melanchthonem	Melanchthon	k1gMnSc7	Melanchthon
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
Historia	Historium	k1gNnSc2	Historium
de	de	k?	de
vita	vit	k2eAgFnSc1d1	Vita
et	et	k?	et
actis	actis	k1gFnSc1	actis
Lutheri	Luther	k1gFnSc2	Luther
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stoletého	stoletý	k2eAgNnSc2d1	stoleté
jubilea	jubileum	k1gNnSc2	jubileum
reformace	reformace	k1gFnSc2	reformace
roku	rok	k1gInSc2	rok
1617	[number]	k4	1617
byl	být	k5eAaImAgInS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc4	říjen
slaven	slaven	k2eAgInSc4d1	slaven
průvodem	průvod	k1gInSc7	průvod
k	k	k7c3	k
wittenberskému	wittenberský	k2eAgInSc3d1	wittenberský
kostelu	kostel	k1gInSc3	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
teze	teze	k1gFnPc4	teze
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tištěna	tištěn	k2eAgFnSc1d1	tištěna
rytina	rytina	k1gFnSc1	rytina
<g/>
,	,	kIx,	,
na	na	k7c4	na
níž	nízce	k6eAd2	nízce
Luther	Luthra	k1gFnPc2	Luthra
píše	psát	k5eAaImIp3nS	psát
gigantickým	gigantický	k2eAgNnSc7d1	gigantické
perem	pero	k1gNnSc7	pero
teze	teze	k1gFnSc2	teze
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Pero	pero	k1gNnSc1	pero
proniká	pronikat	k5eAaImIp3nS	pronikat
hlavou	hlava	k1gFnSc7	hlava
lva	lev	k1gInSc2	lev
symbolizujícího	symbolizující	k2eAgMnSc4d1	symbolizující
papeže	papež	k1gMnSc4	papež
Lva	lev	k1gInSc2	lev
X.	X.	kA	X.
Roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
byl	být	k5eAaImAgInS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Dnem	dnem	k7c2	dnem
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
každoročním	každoroční	k2eAgInSc7d1	každoroční
svátkem	svátek	k1gInSc7	svátek
Saského	saský	k2eAgNnSc2d1	Saské
kurfiřtství	kurfiřtství	k1gNnSc2	kurfiřtství
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
luteránských	luteránský	k2eAgFnPc2d1	luteránská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ninety-five	Ninetyiev	k1gFnSc2	Ninety-fiev
Theses	Thesesa	k1gFnPc2	Thesesa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Biela	Biela	k1gMnSc1	Biela
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Nicák	Nicák	k1gMnSc1	Nicák
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Batka	Batko	k1gNnSc2	Batko
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
:	:	kIx,	:
95	[number]	k4	95
výpovedí	výpovedit	k5eAaPmIp3nS	výpovedit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
zmeny	zmen	k1gInPc1	zmen
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
přepr	přepra	k1gFnPc2	přepra
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Lutherova	Lutherův	k2eAgFnSc1d1	Lutherova
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
156	[number]	k4	156
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BRECHT	Brecht	k1gMnSc1	Brecht
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
His	his	k1gNnSc1	his
Road	Roada	k1gFnPc2	Roada
to	ten	k3xDgNnSc1	ten
Reformation	Reformation	k1gInSc1	Reformation
<g/>
,	,	kIx,	,
1483	[number]	k4	1483
<g/>
-	-	kIx~	-
<g/>
1521	[number]	k4	1521
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
James	James	k1gMnSc1	James
L.	L.	kA	L.
Schaaf	Schaaf	k1gMnSc1	Schaaf
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
:	:	kIx,	:
Fortress	Fortress	k1gInSc1	Fortress
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
589	[number]	k4	589
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8006	[number]	k4	8006
<g/>
-	-	kIx~	-
<g/>
2813	[number]	k4	2813
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
176	[number]	k4	176
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CUMMINGS	CUMMINGS	kA	CUMMINGS
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
literary	literar	k1gInPc1	literar
culture	cultur	k1gMnSc5	cultur
of	of	k?	of
the	the	k?	the
Reformation	Reformation	k1gInSc1	Reformation
:	:	kIx,	:
grammar	grammar	k1gInSc1	grammar
and	and	k?	and
grace	grace	k1gFnSc2	grace
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780198187356	[number]	k4	9780198187356
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.109	[number]	k4	10.109
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
acprof	acprof	k1gInSc1	acprof
<g/>
:	:	kIx,	:
<g/>
oso	osa	k1gFnSc5	osa
<g/>
/	/	kIx~	/
<g/>
9780198187356.001	[number]	k4	9780198187356.001
<g/>
.0001	.0001	k4	.0001
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
32	[number]	k4	32
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DIXON	DIXON	kA	DIXON
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Reformation	Reformation	k1gInSc4	Reformation
in	in	k?	in
Germany	German	k1gInPc4	German
<g/>
.	.	kIx.	.
</s>
<s>
Malden	Maldno	k1gNnPc2	Maldno
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
:	:	kIx,	:
Blackwell	Blackwell	k1gInSc1	Blackwell
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HENDRIX	HENDRIX	kA	HENDRIX
<g/>
,	,	kIx,	,
Scott	Scott	k1gMnSc1	Scott
H.	H.	kA	H.
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
Visionary	Visionara	k1gFnPc1	Visionara
Reformer	Reformer	k1gInSc4	Reformer
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Haven	Haven	k1gInSc1	Haven
<g/>
,	,	kIx,	,
CT	CT	kA	CT
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
16669	[number]	k4	16669
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HEQUET	HEQUET	kA	HEQUET
<g/>
,	,	kIx,	,
Suzanne	Suzann	k1gMnSc5	Suzann
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Annotated	Annotated	k1gInSc1	Annotated
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Roots	Roots	k1gInSc1	Roots
of	of	k?	of
Reform	Reform	k1gInSc1	Reform
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Timothy	Timotha	k1gFnSc2	Timotha
J.	J.	kA	J.
Wengert	Wengert	k1gInSc1	Wengert
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
,	,	kIx,	,
MN	MN	kA	MN
<g/>
:	:	kIx,	:
Fortress	Fortress	k1gInSc1	Fortress
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4514	[number]	k4	4514
<g/>
-	-	kIx~	-
<g/>
6535	[number]	k4	6535
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
The	The	k1gFnSc2	The
Proceedings	Proceedingsa	k1gFnPc2	Proceedingsa
at	at	k?	at
Augsburg	Augsburg	k1gMnSc1	Augsburg
<g/>
,	,	kIx,	,
1518	[number]	k4	1518
<g/>
,	,	kIx,	,
s.	s.	k?	s.
121	[number]	k4	121
<g/>
–	–	k?	–
<g/>
166	[number]	k4	166
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JUNGHANS	JUNGHANS	kA	JUNGHANS
<g/>
,	,	kIx,	,
Helmar	Helmar	k1gMnSc1	Helmar
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wittenberg	Wittenberg	k1gInSc1	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
MCKIM	MCKIM	kA	MCKIM
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
K.	K.	kA	K.
The	The	k1gMnSc1	The
Cambridge	Cambridge	k1gFnSc2	Cambridge
companion	companion	k1gInSc1	companion
to	ten	k3xDgNnSc1	ten
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780511998744	[number]	k4	9780511998744
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.101	[number]	k4	10.101
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
CCOL	CCOL	kA	CCOL
<g/>
0	[number]	k4	0
<g/>
521816483.002	[number]	k4	521816483.002
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LEPPIN	LEPPIN	kA	LEPPIN
<g/>
,	,	kIx,	,
Volker	Volker	k1gMnSc1	Volker
<g/>
;	;	kIx,	;
WENGERT	WENGERT	kA	WENGERT
<g/>
,	,	kIx,	,
Timothy	Timoth	k1gInPc7	Timoth
J.	J.	kA	J.
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Sources	Sources	k1gMnSc1	Sources
for	forum	k1gNnPc2	forum
and	and	k?	and
against	against	k1gMnSc1	against
the	the	k?	the
Posting	Posting	k1gInSc1	Posting
of	of	k?	of
the	the	k?	the
Ninety-Five	Ninety-Fiev	k1gFnSc2	Ninety-Fiev
Theses	Thesesa	k1gFnPc2	Thesesa
<g/>
.	.	kIx.	.
</s>
<s>
Lutheran	Lutheran	k1gInSc1	Lutheran
Quarterly	Quarterla	k1gFnSc2	Quarterla
<g/>
.	.	kIx.	.
</s>
<s>
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
,	,	kIx,	,
s.	s.	k?	s.
373	[number]	k4	373
<g/>
–	–	k?	–
<g/>
398	[number]	k4	398
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LOHSE	LOHSE	kA	LOHSE
<g/>
,	,	kIx,	,
Bernhard	Bernhard	k1gMnSc1	Bernhard
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Theology	theolog	k1gMnPc7	theolog
<g/>
:	:	kIx,	:
Its	Its	k1gFnSc4	Its
Historical	Historical	k1gFnPc2	Historical
and	and	k?	and
Systematic	Systematice	k1gFnPc2	Systematice
Development	Development	k1gMnSc1	Development
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Roy	Roy	k1gMnSc1	Roy
A.	A.	kA	A.
Harrisville	Harrisville	k1gInSc1	Harrisville
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
,	,	kIx,	,
MN	MN	kA	MN
<g/>
:	:	kIx,	:
Fortress	Fortress	k1gInSc1	Fortress
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8006	[number]	k4	8006
<g/>
-	-	kIx~	-
<g/>
3091	[number]	k4	3091
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LOHSE	LOHSE	kA	LOHSE
<g/>
,	,	kIx,	,
Bernhard	Bernhard	k1gMnSc1	Bernhard
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc4	ten
His	his	k1gNnSc4	his
Life	Life	k1gFnSc1	Life
and	and	k?	and
Work	Work	k1gInSc1	Work
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
Schultz	Schultz	k1gMnSc1	Schultz
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
,	,	kIx,	,
MN	MN	kA	MN
<g/>
:	:	kIx,	:
Fortress	Fortress	k1gInSc1	Fortress
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8006	[number]	k4	8006
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
764	[number]	k4	764
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MARIUS	MARIUS	kA	MARIUS
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Christian	Christian	k1gMnSc1	Christian
Between	Betwena	k1gFnPc2	Betwena
God	God	k1gMnSc1	God
and	and	k?	and
Death	Death	k1gMnSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
MA	MA	kA	MA
<g/>
:	:	kIx,	:
Belknap	Belknap	k1gMnSc1	Belknap
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
55090	[number]	k4	55090
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MCGRATH	MCGRATH	kA	MCGRATH
<g/>
,	,	kIx,	,
Alister	Alister	k1gInSc1	Alister
E.	E.	kA	E.
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Theology	theolog	k1gMnPc7	theolog
of	of	k?	of
the	the	k?	the
Cross	Cross	k1gInSc1	Cross
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Theological	Theological	k1gFnSc7	Theological
Breakthrough	Breakthrougha	k1gFnPc2	Breakthrougha
<g/>
.	.	kIx.	.
</s>
<s>
Malden	Maldno	k1gNnPc2	Maldno
<g/>
,	,	kIx,	,
MA	MA	kA	MA
<g/>
:	:	kIx,	:
Wiley-Blackwell	Wiley-Blackwell	k1gMnSc1	Wiley-Blackwell
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOLL	NOLL	kA	NOLL
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
A	A	kA	A
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
the	the	k?	the
Beginning	Beginning	k1gInSc1	Beginning
Was	Was	k1gFnSc1	Was
the	the	k?	the
Word	Word	kA	Word
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Bible	bible	k1gFnSc2	bible
in	in	k?	in
American	Americany	k1gInPc2	Americany
Public	publicum	k1gNnPc2	publicum
Life	Lif	k1gInSc2	Lif
<g/>
,	,	kIx,	,
1492	[number]	k4	1492
<g/>
-	-	kIx~	-
<g/>
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
448	[number]	k4	448
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
26398	[number]	k4	26398
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.109	[number]	k4	10.109
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
acprof	acprof	k1gInSc1	acprof
<g/>
:	:	kIx,	:
<g/>
oso	osa	k1gFnSc5	osa
<g/>
/	/	kIx~	/
<g/>
9780190263980.001	[number]	k4	9780190263980.001
<g/>
.0001	.0001	k4	.0001
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OBERMAN	OBERMAN	kA	OBERMAN
<g/>
,	,	kIx,	,
Heiko	Heiko	k1gNnSc1	Heiko
A	A	kA	A
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
Man	mana	k1gFnPc2	mana
Between	Between	k1gInSc1	Between
God	God	k1gMnSc1	God
and	and	k?	and
the	the	k?	the
Devil	Devil	k1gInSc1	Devil
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Eileen	Eileen	k2eAgInSc4d1	Eileen
Walliser-Schwarzbart	Walliser-Schwarzbart	k1gInSc4	Walliser-Schwarzbart
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Haven	Haven	k1gInSc1	Haven
<g/>
,	,	kIx,	,
CT	CT	kA	CT
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
10313	[number]	k4	10313
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PETTEGREE	PETTEGREE	kA	PETTEGREE
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Brand	Brand	k1gInSc1	Brand
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
How	How	k1gMnSc1	How
an	an	k?	an
Unheralded	Unheralded	k1gMnSc1	Unheralded
Monk	Monk	k1gMnSc1	Monk
Turned	Turned	k1gMnSc1	Turned
His	his	k1gNnSc2	his
Small	Small	k1gMnSc1	Small
Town	Town	k1gMnSc1	Town
into	into	k1gMnSc1	into
a	a	k8xC	a
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
Made	Made	k1gInSc1	Made
Himself	Himself	k1gInSc1	Himself
the	the	k?	the
Most	most	k1gInSc1	most
Famous	Famous	k1gMnSc1	Famous
Man	Man	k1gMnSc1	Man
in	in	k?	in
Europe--and	Europe-nd	k1gInSc1	Europe--and
Started	Started	k1gMnSc1	Started
the	the	k?	the
Protestant	protestant	k1gMnSc1	protestant
Reformation	Reformation	k1gInSc1	Reformation
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Penguin	Penguin	k2eAgInSc1d1	Penguin
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
59420	[number]	k4	59420
<g/>
-	-	kIx~	-
<g/>
496	[number]	k4	496
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
STEPHENSON	STEPHENSON	kA	STEPHENSON
<g/>
,	,	kIx,	,
Barry	Barra	k1gFnSc2	Barra
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Performing	Performing	k1gInSc1	Performing
the	the	k?	the
Reformation	Reformation	k1gInSc1	Reformation
<g/>
:	:	kIx,	:
Religious	Religious	k1gInSc1	Religious
Festivals	Festivals	k1gInSc1	Festivals
in	in	k?	in
Contemporary	Contemporara	k1gFnSc2	Contemporara
Wittenberg	Wittenberg	k1gMnSc1	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.109	[number]	k4	10.109
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
acprof	acprof	k1gInSc1	acprof
<g/>
:	:	kIx,	:
<g/>
oso	osa	k1gFnSc5	osa
<g/>
/	/	kIx~	/
<g/>
9780199732753.001	[number]	k4	9780199732753.001
<g/>
.0001	.0001	k4	.0001
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WAIBEL	WAIBEL	kA	WAIBEL
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
R	R	kA	R
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
:	:	kIx,	:
a	a	k8xC	a
brief	brief	k1gMnSc1	brief
introduction	introduction	k1gInSc4	introduction
to	ten	k3xDgNnSc1	ten
his	his	k1gNnSc1	his
life	lifat	k5eAaPmIp3nS	lifat
and	and	k?	and
works	works	k1gInSc1	works
<g/>
.	.	kIx.	.
</s>
<s>
Wheeling	Wheeling	k1gInSc1	Wheeling
<g/>
:	:	kIx,	:
Harlan	Harlan	k1gMnSc1	Harlan
Davidson	Davidson	k1gMnSc1	Davidson
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
88295	[number]	k4	88295
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WENGERT	WENGERT	kA	WENGERT
<g/>
,	,	kIx,	,
Timothy	Timoth	k1gInPc7	Timoth
J.	J.	kA	J.
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
a.	a.	k?	a.
Martin	Martin	k1gInSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ninety-Five	Ninety-Fiev	k1gFnPc4	Ninety-Fiev
Theses	Theses	k1gInSc1	Theses
<g/>
:	:	kIx,	:
With	With	k1gInSc1	With
Introduction	Introduction	k1gInSc1	Introduction
<g/>
,	,	kIx,	,
Commentary	Commentar	k1gInPc1	Commentar
<g/>
,	,	kIx,	,
and	and	k?	and
Study	stud	k1gInPc1	stud
Guide	Guid	k1gInSc5	Guid
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
,	,	kIx,	,
MN	MN	kA	MN
<g/>
:	:	kIx,	:
Fortress	Fortress	k1gInSc1	Fortress
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WENGERT	WENGERT	kA	WENGERT
<g/>
,	,	kIx,	,
Timothy	Timoth	k1gInPc7	Timoth
J.	J.	kA	J.
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
b.	b.	k?	b.
The	The	k1gMnSc1	The
Annotated	Annotated	k1gMnSc1	Annotated
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Roots	Roots	k1gInSc1	Roots
of	of	k?	of
Reform	Reform	k1gInSc1	Reform
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Timothy	Timotha	k1gFnSc2	Timotha
J.	J.	kA	J.
Wengert	Wengert	k1gInSc1	Wengert
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
,	,	kIx,	,
MN	MN	kA	MN
<g/>
:	:	kIx,	:
Fortress	Fortress	k1gInSc1	Fortress
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4514	[number]	k4	4514
<g/>
-	-	kIx~	-
<g/>
6535	[number]	k4	6535
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
[	[	kIx(	[
<g/>
The	The	k1gFnSc1	The
95	[number]	k4	95
Theses	Theses	k1gMnSc1	Theses
or	or	k?	or
<g/>
]	]	kIx)	]
Disputation	Disputation	k1gInSc1	Disputation
for	forum	k1gNnPc2	forum
Clarifying	Clarifying	k1gInSc1	Clarifying
the	the	k?	the
Power	Power	k1gInSc1	Power
of	of	k?	of
Indulgences	Indulgences	k1gInSc1	Indulgences
<g/>
,	,	kIx,	,
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
s.	s.	k?	s.
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WICKS	WICKS	kA	WICKS
<g/>
,	,	kIx,	,
Jared	Jared	k1gInSc1	Jared
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Treatise	Treatise	k1gFnSc2	Treatise
on	on	k3xPp3gMnSc1	on
Indulgences	Indulgences	k1gMnSc1	Indulgences
<g/>
.	.	kIx.	.
</s>
<s>
Theological	Theologicat	k5eAaPmAgMnS	Theologicat
Studies	Studies	k1gMnSc1	Studies
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
481	[number]	k4	481
<g/>
–	–	k?	–
<g/>
518	[number]	k4	518
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
5639	[number]	k4	5639
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
95	[number]	k4	95
tezí	teze	k1gFnSc7	teze
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Disputatio	Disputatio	k6eAd1	Disputatio
pro	pro	k7c4	pro
declaratione	declaration	k1gInSc5	declaration
virtutis	virtutis	k1gFnPc7	virtutis
indulgentiarum	indulgentiarum	k1gInSc4	indulgentiarum
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
Amedeo	Amedeo	k6eAd1	Amedeo
Molnára	Molnár	k1gMnSc2	Molnár
</s>
</p>
<p>
<s>
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
prof.	prof.	kA	prof.
Jany	Jan	k1gMnPc4	Jan
Nechutové	Nechutový	k2eAgNnSc1d1	Nechutový
(	(	kIx(	(
<g/>
Theologia	Theologia	k1gFnSc1	Theologia
vitae	vita	k1gFnSc2	vita
r.	r.	kA	r.
6	[number]	k4	6
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
č.	č.	k?	č.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
na	na	k7c4	na
Spiegel	Spiegel	k1gInSc4	Spiegel
Online	Onlin	k1gInSc5	Onlin
</s>
</p>
<p>
<s>
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
latině	latina	k1gFnSc6	latina
</s>
</p>
