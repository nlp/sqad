<s>
Mendelevium	Mendelevium	k1gNnSc1
</s>
<s>
Mendelevium	Mendelevium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
13	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
258	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Md	Md	kA
</s>
<s>
101	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Mendelevium	Mendelevium	k1gNnSc1
<g/>
,	,	kIx,
Md	Md	k1gFnSc1
<g/>
,	,	kIx,
101	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Mendelevium	Mendelevium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-11-1	7440-11-1	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
258,10	258,10	k4
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
Md	Md	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
114	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Md	Md	k1gFnSc1
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
96	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Md	Md	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
84	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
13	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
6,4	6,4	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
12	#num#	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
23	#num#	k4
eV	eV	k?
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
827	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
100,15	100,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tm	Tm	k?
<g/>
⋏	⋏	k?
</s>
<s>
Fermium	fermium	k1gNnSc1
≺	≺	k?
<g/>
Md	Md	k1gMnSc2
<g/>
≻	≻	k?
Nobelium	nobelium	k1gNnSc1
</s>
<s>
Mendelevium	Mendelevium	k1gNnSc1
či	či	k8xC
mendělevium	mendělevium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Md	Md	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třináctým	třináctý	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
devátým	devátý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
)	)	kIx)
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
einsteinia	einsteinium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Fyzikálně-chemické	Fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Mendelevium	Mendelevium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
doposud	doposud	k6eAd1
nebyl	být	k5eNaImAgInS
izolován	izolovat	k5eAaBmNgInS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS
α	α	k?
a	a	k8xC
γ	γ	k?
záření	záření	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
silným	silný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
neutronů	neutron	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
manipulovat	manipulovat	k5eAaImF
za	za	k7c4
dodržování	dodržování	k1gNnSc4
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
radioaktivními	radioaktivní	k2eAgInPc7d1
materiály	materiál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
jeho	jeho	k3xOp3gFnPc6
sloučeninách	sloučenina	k1gFnPc6
a	a	k8xC
jejich	jejich	k3xOp3gNnSc6
chemickém	chemický	k2eAgNnSc6d1
chování	chování	k1gNnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
velmi	velmi	k6eAd1
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
nejstabilnější	stabilní	k2eAgNnSc1d3
mocenství	mocenství	k1gNnSc1
mendelevia	mendelevium	k1gNnSc2
je	být	k5eAaImIp3nS
Md	Md	k1gFnSc1
<g/>
+	+	kIx~
<g/>
3	#num#	k4
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
Md	Md	k1gFnSc7
<g/>
+	+	kIx~
<g/>
2	#num#	k4
jsou	být	k5eAaImIp3nP
málo	málo	k6eAd1
stálé	stálý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS
Ivanovič	Ivanovič	k1gMnSc1
Mendělejev	Mendělejev	k1gMnSc1
</s>
<s>
Mendelevium	Mendelevium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1955	#num#	k4
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
kalifornské	kalifornský	k2eAgFnSc2d1
university	universita	k1gFnSc2
v	v	k7c4
Berkeley	Berkelea	k1gFnPc4
za	za	k7c2
pomoci	pomoc	k1gFnSc2
cyklotronu	cyklotron	k1gInSc2
bombardováním	bombardování	k1gNnSc7
jader	jádro	k1gNnPc2
einsteinia	einsteinium	k1gNnSc2
253	#num#	k4
<g/>
Es	es	k1gNnPc2
částicemi	částice	k1gFnPc7
alfa	alfa	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jeho	jeho	k3xOp3gMnPc4
objevitele	objevitel	k1gMnPc4
jsou	být	k5eAaImIp3nP
pokládáni	pokládat	k5eAaImNgMnP
Albert	Alberta	k1gFnPc2
Ghiorso	Ghiorsa	k1gFnSc5
jako	jako	k8xS,k8xC
vedoucí	vedoucí	k1gMnSc1
výzkumného	výzkumný	k2eAgInSc2d1
týmu	tým	k1gInSc2
a	a	k8xC
dále	daleko	k6eAd2
Glenn	Glenn	k1gMnSc1
T.	T.	kA
Seaborg	Seaborg	k1gMnSc1
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
Harvey	Harvea	k1gFnSc2
a	a	k8xC
Greg	Greg	k1gMnSc1
Choppin	Choppin	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
prvku	prvek	k1gInSc3
vyrobili	vyrobit	k5eAaPmAgMnP
pouhých	pouhý	k2eAgInPc2d1
17	#num#	k4
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
na	na	k7c4
počest	počest	k1gFnSc4
ruského	ruský	k2eAgMnSc4d1
chemika	chemik	k1gMnSc4
Dmitrije	Dmitrije	k1gMnSc4
Mendělejeva	Mendělejev	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
25399	#num#	k4
Es	es	k1gNnPc1
+	+	kIx~
42	#num#	k4
He	he	k0
→	→	k?
256101	#num#	k4
Md	Md	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
sedmnáct	sedmnáct	k4xCc4
izotopů	izotop	k1gInPc2
mendelevia	mendelevium	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
nichž	jenž	k3xRgFnPc2
je	být	k5eAaImIp3nS
nejstabilnější	stabilní	k2eAgFnSc1d3
(	(	kIx(
<g/>
z	z	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
poločas	poločas	k1gInSc1
přeměny	přeměna	k1gFnSc2
znám	znát	k5eAaImIp1nS
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
258	#num#	k4
<g/>
Md	Md	k1gFnPc1
s	s	k7c7
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
51,5	51,5	k4
dne	den	k1gInSc2
<g/>
,	,	kIx,
260	#num#	k4
<g/>
Md	Md	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
31,8	31,8	k4
dne	den	k1gInSc2
a	a	k8xC
257	#num#	k4
<g/>
Md	Md	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
5,52	5,52	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
mají	mít	k5eAaImIp3nP
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
kratší	krátký	k2eAgFnSc4d2
než	než	k8xS
100	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Introducing	Introducing	k1gInSc1
a	a	k8xC
new	new	k?
isotope	isotop	k1gInSc5
<g/>
:	:	kIx,
Mendelevium-	Mendelevium-	k1gFnPc2
<g/>
244	#num#	k4
<g/>
.	.	kIx.
phys	physa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
mendelevium	mendelevium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
mendelevium	mendelevium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4169413-2	4169413-2	k4
</s>
