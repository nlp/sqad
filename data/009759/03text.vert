<p>
<s>
Otrava	otrava	k1gFnSc1	otrava
je	být	k5eAaImIp3nS	být
chorobný	chorobný	k2eAgInSc4d1	chorobný
stav	stav	k1gInSc4	stav
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
přítomností	přítomnost	k1gFnSc7	přítomnost
jedu	jed	k1gInSc2	jed
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
lze	lze	k6eAd1	lze
též	též	k9	též
pohlížet	pohlížet	k5eAaImF	pohlížet
na	na	k7c4	na
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
hlavní	hlavní	k2eAgInSc1d1	hlavní
negativní	negativní	k2eAgInSc1d1	negativní
účinek	účinek	k1gInSc1	účinek
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jedů	jed	k1gInPc2	jed
produkovaných	produkovaný	k2eAgFnPc2d1	produkovaná
patogeny	patogen	k1gInPc4	patogen
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nP	platit
např.	např.	kA	např.
pro	pro	k7c4	pro
tetanus	tetanus	k1gInSc4	tetanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
otrav	otrava	k1gFnPc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úmyslná	úmyslný	k2eAgFnSc1d1	úmyslná
otrava	otrava	k1gFnSc1	otrava
===	===	k?	===
</s>
</p>
<p>
<s>
Sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
úmysl	úmysl	k1gInSc1	úmysl
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
demonstračního	demonstrační	k2eAgInSc2d1	demonstrační
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
otrav	otrava	k1gFnPc2	otrava
(	(	kIx(	(
<g/>
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
otrávených	otrávený	k2eAgMnPc2d1	otrávený
činí	činit	k5eAaImIp3nS	činit
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
prostředky	prostředek	k1gInPc7	prostředek
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgInPc1d1	běžný
léky	lék	k1gInPc1	lék
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
spaní	spaní	k1gNnSc6	spaní
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nenáhodná	náhodný	k2eNgFnSc1d1	nenáhodná
otrava	otrava	k1gFnSc1	otrava
===	===	k?	===
</s>
</p>
<p>
<s>
Užívání	užívání	k1gNnSc1	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náhodná	náhodný	k2eAgFnSc1d1	náhodná
otrava	otrava	k1gFnSc1	otrava
===	===	k?	===
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
kosmetické	kosmetický	k2eAgInPc4d1	kosmetický
přípravky	přípravek	k1gInPc4	přípravek
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělí	dospělí	k1gMnPc1	dospělí
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
povolání	povolání	k1gNnSc2	povolání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
koncentrací	koncentrace	k1gFnSc7	koncentrace
toxických	toxický	k2eAgInPc2d1	toxický
plynů	plyn	k1gInPc2	plyn
či	či	k8xC	či
par	para	k1gFnPc2	para
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
či	či	k8xC	či
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náhodná	náhodný	k2eAgFnSc1d1	náhodná
záměna	záměna	k1gFnSc1	záměna
–	–	k?	–
trvalým	trvalý	k2eAgInSc7d1	trvalý
zdrojem	zdroj	k1gInSc7	zdroj
otrav	otrava	k1gFnPc2	otrava
je	být	k5eAaImIp3nS	být
nalévání	nalévání	k1gNnSc1	nalévání
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
lahví	lahev	k1gFnPc2	lahev
od	od	k7c2	od
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otravy	otrava	k1gFnPc4	otrava
zplodinami	zplodina	k1gFnPc7	zplodina
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgFnPc4d3	nejčastější
je	být	k5eAaImIp3nS	být
otrava	otrava	k1gFnSc1	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
nejvíce	hodně	k6eAd3	hodně
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
následků	následek	k1gInPc2	následek
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
například	například	k6eAd1	například
i	i	k9	i
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
fosgen	fosgen	k1gInSc1	fosgen
a	a	k8xC	a
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otrava	otrava	k1gFnSc1	otrava
jídlem	jídlo	k1gNnSc7	jídlo
či	či	k8xC	či
pitím	pití	k1gNnSc7	pití
–	–	k?	–
zejména	zejména	k9	zejména
jedovatými	jedovatý	k2eAgFnPc7d1	jedovatá
houbami	houba	k1gFnPc7	houba
nebo	nebo	k8xC	nebo
nevhodně	vhodně	k6eNd1	vhodně
skladovaným	skladovaný	k2eAgNnSc7d1	skladované
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
překročenou	překročený	k2eAgFnSc7d1	překročená
dobou	doba	k1gFnSc7	doba
minimální	minimální	k2eAgFnSc2d1	minimální
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
<g/>
.	.	kIx.	.
</s>
<s>
Fugu	fuga	k1gFnSc4	fuga
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc4	maso
čtverzubců	čtverzubec	k1gMnPc2	čtverzubec
rodu	rod	k1gInSc2	rod
Takifugu	Takifug	k1gInSc2	Takifug
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
při	při	k7c6	při
špatné	špatný	k2eAgFnSc6d1	špatná
přípravě	příprava	k1gFnSc6	příprava
ke	k	k7c3	k
smrtelným	smrtelný	k2eAgFnPc3d1	smrtelná
nehodám	nehoda	k1gFnPc3	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
jed	jed	k1gInSc1	jed
tetrodotoxin	tetrodotoxin	k1gInSc1	tetrodotoxin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
především	především	k6eAd1	především
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
vnitřnostech	vnitřnost	k1gFnPc6	vnitřnost
ryb	ryba	k1gFnPc2	ryba
</s>
</p>
<p>
<s>
Otrava	otrava	k1gFnSc1	otrava
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
naředění	naředění	k1gNnSc4	naředění
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
prostředí	prostředí	k1gNnSc2	prostředí
organismu	organismus	k1gInSc2	organismus
způsobeného	způsobený	k2eAgInSc2d1	způsobený
příjmem	příjem	k1gInSc7	příjem
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infekce	infekce	k1gFnSc1	infekce
ze	z	k7c2	z
znečištěné	znečištěný	k2eAgFnSc2d1	znečištěná
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
častá	častý	k2eAgFnSc1d1	častá
zejména	zejména	k9	zejména
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
nevhodně	vhodně	k6eNd1	vhodně
udržovaných	udržovaný	k2eAgFnPc2d1	udržovaná
studní	studna	k1gFnPc2	studna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uštknutí	uštknutí	k1gNnSc1	uštknutí
hadem	had	k1gMnSc7	had
<g/>
,	,	kIx,	,
kousnutí	kousnutí	k1gNnSc4	kousnutí
jedovatým	jedovatý	k2eAgMnSc7d1	jedovatý
živočichem	živočich	k1gMnSc7	živočich
či	či	k8xC	či
podráždění	podráždění	k1gNnSc1	podráždění
jedovatou	jedovatý	k2eAgFnSc7d1	jedovatá
rostlinou	rostlina	k1gFnSc7	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bolševník	bolševník	k1gInSc4	bolševník
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Rychlost	rychlost	k1gFnSc1	rychlost
vzniku	vznik	k1gInSc2	vznik
otravy	otrava	k1gFnSc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
akutní	akutní	k2eAgFnSc2d1	akutní
otravy	otrava	k1gFnSc2	otrava
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
následkem	následkem	k7c2	následkem
jednorázového	jednorázový	k2eAgNnSc2d1	jednorázové
přijetí	přijetí	k1gNnSc2	přijetí
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
konzumací	konzumace	k1gFnSc7	konzumace
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnSc2	zelená
či	či	k8xC	či
uštknutím	uštknutí	k1gNnSc7	uštknutí
taipana	taipan	k1gMnSc2	taipan
<g/>
)	)	kIx)	)
a	a	k8xC	a
chronické	chronický	k2eAgFnSc2d1	chronická
otravy	otrava	k1gFnSc2	otrava
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
následkem	následkem	k7c2	následkem
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
přijímání	přijímání	k1gNnSc2	přijímání
nízkých	nízký	k2eAgFnPc2d1	nízká
dávek	dávka	k1gFnPc2	dávka
kumulativního	kumulativní	k2eAgInSc2d1	kumulativní
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
např.	např.	kA	např.
většina	většina	k1gFnSc1	většina
otrav	otrava	k1gFnPc2	otrava
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
otravě	otrava	k1gFnSc6	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
lze	lze	k6eAd1	lze
<g/>
,	,	kIx,	,
přerušit	přerušit	k5eAaPmF	přerušit
působení	působení	k1gNnSc4	působení
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přivedení	přivedení	k1gNnSc4	přivedení
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
otravě	otrava	k1gFnSc6	otrava
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zvracení	zvracení	k1gNnSc1	zvracení
má	mít	k5eAaImIp3nS	mít
vysoká	vysoký	k2eAgNnPc4d1	vysoké
rizika	riziko	k1gNnPc4	riziko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
jej	on	k3xPp3gMnSc4	on
nevyvolávat	vyvolávat	k5eNaImF	vyvolávat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kontaktních	kontaktní	k2eAgInPc2d1	kontaktní
jedů	jed	k1gInPc2	jed
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgNnSc1d1	žádoucí
odstranění	odstranění	k1gNnSc1	odstranění
oděvu	oděv	k1gInSc2	oděv
a	a	k8xC	a
omytí	omytí	k1gNnSc2	omytí
kůže	kůže	k1gFnSc2	kůže
nebo	nebo	k8xC	nebo
sliznic	sliznice	k1gFnPc2	sliznice
(	(	kIx(	(
<g/>
výplach	výplach	k1gInSc1	výplach
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zjistit	zjistit	k5eAaPmF	zjistit
příčinu	příčina	k1gFnSc4	příčina
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
vzorek	vzorek	k1gInSc4	vzorek
(	(	kIx(	(
<g/>
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc4	zbytek
smaženice	smaženice	k1gFnSc2	smaženice
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrátit	obrátit	k5eAaPmF	obrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
odborníka	odborník	k1gMnSc4	odborník
(	(	kIx(	(
<g/>
155	[number]	k4	155
nebo	nebo	k8xC	nebo
toxikologické	toxikologický	k2eAgNnSc4d1	Toxikologické
středisko	středisko	k1gNnSc4	středisko
224915402	[number]	k4	224915402
-	-	kIx~	-
nonstop	nonstop	k6eAd1	nonstop
konzultace	konzultace	k1gFnPc4	konzultace
<g/>
)	)	kIx)	)
<g/>
Lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
poskytovanou	poskytovaný	k2eAgFnSc4d1	poskytovaná
v	v	k7c6	v
případě	případ	k1gInSc6	případ
otravy	otrava	k1gFnSc2	otrava
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Odstranění	odstranění	k1gNnSc1	odstranění
jedu	jed	k1gInSc2	jed
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
zamezení	zamezení	k1gNnSc2	zamezení
jeho	on	k3xPp3gNnSc2	on
dalšího	další	k2eAgNnSc2d1	další
přijímání	přijímání	k1gNnSc2	přijímání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
výplach	výplach	k1gInSc4	výplach
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
zvracení	zvracení	k1gNnPc2	zvracení
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požití	požití	k1gNnSc1	požití
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jedy	jed	k1gInPc1	jed
kolující	kolující	k2eAgInPc1d1	kolující
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
dialýzou	dialýza	k1gFnSc7	dialýza
(	(	kIx(	(
<g/>
umělou	umělý	k2eAgFnSc7d1	umělá
ledvinou	ledvina	k1gFnSc7	ledvina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zásahy	zásah	k1gInPc1	zásah
proti	proti	k7c3	proti
účinkům	účinek	k1gInPc3	účinek
jedu	jed	k1gInSc2	jed
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
tlumení	tlumení	k1gNnSc1	tlumení
použitím	použití	k1gNnSc7	použití
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
umělou	umělý	k2eAgFnSc7d1	umělá
plicní	plicní	k2eAgFnSc7d1	plicní
ventilací	ventilace	k1gFnSc7	ventilace
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účinných	účinný	k2eAgInPc2d1	účinný
protijedů	protijed	k1gInPc2	protijed
je	být	k5eAaImIp3nS	být
bohužel	bohužel	k9	bohužel
velmi	velmi	k6eAd1	velmi
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
otrav	otrava	k1gFnPc2	otrava
je	být	k5eAaImIp3nS	být
však	však	k9	však
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
jen	jen	k9	jen
podporou	podpora	k1gFnSc7	podpora
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podání	podání	k1gNnSc4	podání
séra	sérum	k1gNnSc2	sérum
proti	proti	k7c3	proti
patřičnému	patřičný	k2eAgInSc3d1	patřičný
jedu	jed	k1gInSc3	jed
(	(	kIx(	(
<g/>
např.	např.	kA	např.
antitoxin	antitoxin	k1gInSc1	antitoxin
proti	proti	k7c3	proti
tetanu	tetan	k1gInSc3	tetan
<g/>
,	,	kIx,	,
sérum	sérum	k1gNnSc4	sérum
proti	proti	k7c3	proti
kobřímu	kobří	k2eAgInSc3d1	kobří
jedu	jed	k1gInSc3	jed
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
rizikem	riziko	k1gNnSc7	riziko
anafylaktického	anafylaktický	k2eAgInSc2d1	anafylaktický
šoku	šok	k1gInSc2	šok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
otrav	otrava	k1gFnPc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Otrava	otrava	k1gFnSc1	otrava
leptavými	leptavý	k2eAgFnPc7d1	leptavá
látkami	látka	k1gFnPc7	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Pojmem	pojem	k1gInSc7	pojem
leptavá	leptavý	k2eAgFnSc1d1	leptavá
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
kyselina	kyselina	k1gFnSc1	kyselina
nebo	nebo	k8xC	nebo
zásada	zásada	k1gFnSc1	zásada
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgFnSc7d3	nejčastější
otravou	otrava	k1gFnSc7	otrava
je	být	k5eAaImIp3nS	být
požití	požití	k1gNnSc1	požití
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poleptání	poleptání	k1gNnSc3	poleptání
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
první	první	k4xOgFnSc1	první
pomoc	pomoc	k1gFnSc1	pomoc
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
zředění	zředění	k1gNnSc6	zředění
obsahu	obsah	k1gInSc2	obsah
žaludku	žaludek	k1gInSc2	žaludek
čistou	čistý	k2eAgFnSc7d1	čistá
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
mlékem	mléko	k1gNnSc7	mléko
<g/>
)	)	kIx)	)
a	a	k8xC	a
rychlém	rychlý	k2eAgInSc6d1	rychlý
transportu	transport	k1gInSc6	transport
k	k	k7c3	k
ošetření	ošetření	k1gNnSc3	ošetření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otrava	otrava	k1gFnSc1	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
(	(	kIx(	(
<g/>
CO	co	k3yQnSc4	co
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc1	plyn
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
nejčastěji	často	k6eAd3	často
při	při	k7c6	při
nedokonalém	dokonalý	k2eNgNnSc6d1	nedokonalé
spalování	spalování	k1gNnSc6	spalování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
proniknutí	proniknutí	k1gNnSc6	proniknutí
do	do	k7c2	do
organizmu	organizmus	k1gInSc2	organizmus
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
hemoglobin	hemoglobin	k1gInSc4	hemoglobin
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
–	–	k?	–
300	[number]	k4	300
<g/>
x	x	k?	x
pevnější	pevný	k2eAgMnSc1d2	pevnější
než	než	k8xS	než
vazba	vazba	k1gFnSc1	vazba
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
jsou	být	k5eAaImIp3nP	být
zčervenání	zčervenání	k1gNnSc4	zčervenání
(	(	kIx(	(
<g/>
cihlově	cihlově	k6eAd1	cihlově
červený	červený	k2eAgInSc1d1	červený
obličej	obličej	k1gInSc1	obličej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc2	zvracení
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnSc2	závrať
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnSc2	porucha
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
zrychlení	zrychlení	k1gNnSc1	zrychlení
dechu	dech	k1gInSc2	dech
a	a	k8xC	a
tepu	tep	k1gInSc2	tep
a	a	k8xC	a
poruchy	porucha	k1gFnSc2	porucha
vědomí	vědomí	k1gNnSc2	vědomí
až	až	k8xS	až
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
pomocí	pomoc	k1gFnSc7	pomoc
je	být	k5eAaImIp3nS	být
zajištění	zajištění	k1gNnSc1	zajištění
přísunu	přísun	k1gInSc2	přísun
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
umělé	umělý	k2eAgNnSc1d1	umělé
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
,	,	kIx,	,
lékařská	lékařský	k2eAgFnSc1d1	lékařská
první	první	k4xOgFnSc1	první
pomoc	pomoc	k1gFnSc1	pomoc
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
podávání	podávání	k1gNnSc6	podávání
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otrava	otrava	k1gFnSc1	otrava
alkoholy	alkohol	k1gInPc1	alkohol
===	===	k?	===
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
otravu	otrava	k1gFnSc4	otrava
metylalkoholem	metylalkohol	k1gInSc7	metylalkohol
a	a	k8xC	a
etylenglykolem	etylenglykol	k1gInSc7	etylenglykol
a	a	k8xC	a
zejména	zejména	k9	zejména
otrava	otrava	k1gFnSc1	otrava
lihem	líh	k1gInSc7	líh
(	(	kIx(	(
<g/>
etanolem	etanol	k1gInSc7	etanol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholy	alkohol	k1gInPc4	alkohol
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
toxické	toxický	k2eAgInPc1d1	toxický
<g/>
,	,	kIx,	,
toxické	toxický	k2eAgInPc1d1	toxický
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc1	jejich
metabolity	metabolit	k1gInPc1	metabolit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
otravě	otrava	k1gFnSc6	otrava
metylalkoholem	metylalkohol	k1gInSc7	metylalkohol
a	a	k8xC	a
etylenglykolem	etylenglykol	k1gInSc7	etylenglykol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Fridex	Fridex	k1gInSc1	Fridex
<g/>
)	)	kIx)	)
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
útlumu	útlum	k1gInSc6	útlum
metabolizace	metabolizace	k1gFnSc2	metabolizace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
etanolu	etanol	k1gInSc2	etanol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vodky	vodka	k1gFnSc2	vodka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postižený	postižený	k1gMnSc1	postižený
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
urychleně	urychleně	k6eAd1	urychleně
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
rozvrat	rozvrat	k1gInSc4	rozvrat
metabolizmu	metabolizmus	k1gInSc2	metabolizmus
a	a	k8xC	a
poškození	poškození	k1gNnSc2	poškození
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otrava	otrava	k1gFnSc1	otrava
houbami	houba	k1gFnPc7	houba
===	===	k?	===
</s>
</p>
<p>
<s>
Houby	houby	k6eAd1	houby
jakožto	jakožto	k8xS	jakožto
tradiční	tradiční	k2eAgInSc1d1	tradiční
doplněk	doplněk	k1gInSc1	doplněk
jídelníčku	jídelníček	k1gInSc2	jídelníček
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
běžným	běžný	k2eAgInPc3d1	běžný
zdrojům	zdroj	k1gInPc3	zdroj
otrav	otrávit	k5eAaPmRp2nS	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Otravy	otrava	k1gFnPc1	otrava
houbami	houba	k1gFnPc7	houba
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Otravy	otrava	k1gFnPc1	otrava
resorpční	resorpční	k2eAgFnPc1d1	resorpční
z	z	k7c2	z
hub	houba	k1gFnPc2	houba
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
otrava	otrava	k1gFnSc1	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
po	po	k7c6	po
syrových	syrový	k2eAgFnPc6d1	syrová
houbách	houba	k1gFnPc6	houba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
otrava	otrava	k1gFnSc1	otrava
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gInSc7	satan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
po	po	k7c6	po
houbách	houba	k1gFnPc6	houba
tuhých	tuhý	k2eAgFnPc2d1	tuhá
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
stravitelných	stravitelný	k2eAgInPc2d1	stravitelný
</s>
</p>
<p>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
po	po	k7c6	po
houbách	houba	k1gFnPc6	houba
s	s	k7c7	s
alergeny	alergen	k1gInPc7	alergen
<g/>
,	,	kIx,	,
hemolyzíny	hemolyzín	k1gInPc7	hemolyzín
a	a	k8xC	a
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
</s>
</p>
<p>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
po	po	k7c6	po
houbách	houba	k1gFnPc6	houba
druhotně	druhotně	k6eAd1	druhotně
změněných	změněný	k2eAgMnPc2d1	změněný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
otrava	otrava	k1gFnSc1	otrava
hnojníkem	hnojník	k1gInSc7	hnojník
inkoustovým	inkoustový	k2eAgInSc7d1	inkoustový
<g/>
,	,	kIx,	,
obtíže	obtíž	k1gFnPc4	obtíž
o	o	k7c6	o
zkažených	zkažený	k2eAgFnPc6d1	zkažená
houbách	houba	k1gFnPc6	houba
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Otrava	otrava	k1gFnSc1	otrava
léky	lék	k1gInPc1	lék
===	===	k?	===
</s>
</p>
<p>
<s>
Otrava	otrava	k1gFnSc1	otrava
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŠTEFAN	Štefan	k1gMnSc1	Štefan
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
MACH	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Soudně	soudně	k6eAd1	soudně
lékařská	lékařský	k2eAgFnSc1d1	lékařská
a	a	k8xC	a
medicínsko-právní	medicínskorávní	k2eAgFnSc1d1	medicínsko-právní
problematika	problematika	k1gFnSc1	problematika
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
931	[number]	k4	931
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Poškození	poškození	k1gNnSc1	poškození
chemickými	chemický	k2eAgFnPc7d1	chemická
látkami	látka	k1gFnPc7	látka
–	–	k?	–
otravy	otrava	k1gFnPc1	otrava
<g/>
,	,	kIx,	,
s.	s.	k?	s.
91	[number]	k4	91
<g/>
–	–	k?	–
<g/>
117	[number]	k4	117
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
jed	jed	k1gInSc1	jed
</s>
</p>
<p>
<s>
otrava	otrava	k1gFnSc1	otrava
cykasy	cykas	k1gInPc1	cykas
</s>
</p>
<p>
<s>
otrava	otrava	k1gFnSc1	otrava
houbami	houba	k1gFnPc7	houba
</s>
</p>
<p>
<s>
otravy	otrava	k1gFnPc1	otrava
z	z	k7c2	z
jídla	jídlo	k1gNnSc2	jídlo
</s>
</p>
<p>
<s>
Antidota	Antidota	k1gFnSc1	Antidota
</s>
</p>
<p>
<s>
toxicita	toxicita	k1gFnSc1	toxicita
</s>
</p>
<p>
<s>
známé	známý	k2eAgFnPc1d1	známá
oběti	oběť	k1gFnPc1	oběť
otrav	otrava	k1gFnPc2	otrava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
otrava	otrava	k1gFnSc1	otrava
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
