<s>
Luminografie	Luminografie	k1gFnSc1	Luminografie
<g/>
,	,	kIx,	,
malba	malba	k1gFnSc1	malba
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
světelnou	světelný	k2eAgFnSc4d1	světelná
tužkou	tužka	k1gFnSc7	tužka
nebo	nebo	k8xC	nebo
iluminace	iluminace	k1gFnSc1	iluminace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
illumination	illumination	k1gInSc1	illumination
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
označuje	označovat	k5eAaImIp3nS	označovat
fotografování	fotografování	k1gNnSc4	fotografování
světelných	světelný	k2eAgInPc2d1	světelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
světla	světlo	k1gNnPc1	světlo
ze	z	k7c2	z
svíčky	svíčka	k1gFnSc2	svíčka
<g/>
,	,	kIx,	,
pochodně	pochodeň	k1gFnSc2	pochodeň
nebo	nebo	k8xC	nebo
elektrické	elektrický	k2eAgFnSc2d1	elektrická
svítilny	svítilna	k1gFnSc2	svítilna
–	–	k?	–
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
