<p>
<s>
Luminografie	Luminografie	k1gFnSc1	Luminografie
<g/>
,	,	kIx,	,
malba	malba	k1gFnSc1	malba
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
světelnou	světelný	k2eAgFnSc4d1	světelná
tužkou	tužka	k1gFnSc7	tužka
nebo	nebo	k8xC	nebo
iluminace	iluminace	k1gFnSc1	iluminace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
illumination	illumination	k1gInSc1	illumination
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
označuje	označovat	k5eAaImIp3nS	označovat
fotografování	fotografování	k1gNnSc4	fotografování
světelných	světelný	k2eAgInPc2d1	světelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
světla	světlo	k1gNnPc1	světlo
ze	z	k7c2	z
svíčky	svíčka	k1gFnSc2	svíčka
<g/>
,	,	kIx,	,
pochodně	pochodeň	k1gFnSc2	pochodeň
nebo	nebo	k8xC	nebo
elektrické	elektrický	k2eAgFnSc2d1	elektrická
svítilny	svítilna	k1gFnSc2	svítilna
–	–	k?	–
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
expozici	expozice	k1gFnSc3	expozice
pořízené	pořízený	k2eAgFnSc3d1	pořízená
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
na	na	k7c6	na
stativu	stativo	k1gNnSc6	stativo
zanechají	zanechat	k5eAaPmIp3nP	zanechat
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
světla	světlo	k1gNnSc2	světlo
barevné	barevný	k2eAgFnPc4d1	barevná
trajektorie	trajektorie	k1gFnSc2	trajektorie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
luminogram	luminogram	k1gInSc1	luminogram
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
obraz	obraz	k1gInSc1	obraz
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
exponováním	exponování	k1gNnSc7	exponování
fotocitlivého	fotocitlivý	k2eAgInSc2d1	fotocitlivý
materiálu	materiál	k1gInSc2	materiál
pouze	pouze	k6eAd1	pouze
světlem	světlo	k1gNnSc7	světlo
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
luminografie	luminografie	k1gFnSc1	luminografie
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
označovala	označovat	k5eAaImAgFnS	označovat
metoda	metoda	k1gFnSc1	metoda
fotografického	fotografický	k2eAgNnSc2d1	fotografické
kopírování	kopírování	k1gNnSc2	kopírování
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
předlohu	předloha	k1gFnSc4	předloha
se	se	k3xPyFc4	se
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
přiložil	přiložit	k5eAaPmAgMnS	přiložit
bromostříbrný	bromostříbrný	k2eAgInSc4d1	bromostříbrný
papír	papír	k1gInSc4	papír
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
zadní	zadní	k2eAgFnSc4d1	zadní
stranu	strana	k1gFnSc4	strana
denním	denní	k2eAgNnSc7d1	denní
světlem	světlo	k1gNnSc7	světlo
ozářená	ozářený	k2eAgFnSc1d1	ozářená
fosforeskující	fosforeskující	k2eAgFnSc1d1	fosforeskující
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
šlo	jít	k5eAaImAgNnS	jít
pořídit	pořídit	k5eAaPmF	pořídit
i	i	k9	i
negativní	negativní	k2eAgInSc4d1	negativní
obraz	obraz	k1gInSc4	obraz
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraničí	zahraničí	k1gNnSc2	zahraničí
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
luminografie	luminografie	k1gFnSc2	luminografie
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
během	během	k7c2	během
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
fotografem	fotograf	k1gMnSc7	fotograf
Gjonem	Gjon	k1gMnSc7	Gjon
Milim	Milim	k1gInSc4	Milim
namaloval	namalovat	k5eAaPmAgMnS	namalovat
světlem	světlo	k1gNnSc7	světlo
do	do	k7c2	do
vzduch	vzduch	k1gInSc4	vzduch
kentaura	kentaur	k1gMnSc2	kentaur
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k9	tak
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
malby	malba	k1gFnPc1	malba
světlem	světlo	k1gNnSc7	světlo
–	–	k?	–
luminografie	luminografie	k1gFnSc2	luminografie
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
maloval	malovat	k5eAaImAgMnS	malovat
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
kentaury	kentaur	k1gMnPc4	kentaur
<g/>
,	,	kIx,	,
býky	býk	k1gMnPc4	býk
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgInPc4d1	řecký
profily	profil	k1gInPc4	profil
nebo	nebo	k8xC	nebo
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Étienne-Jules	Étienne-Jules	k1gInSc1	Étienne-Jules
Marey	Marea	k1gFnSc2	Marea
a	a	k8xC	a
Georges	Georges	k1gMnSc1	Georges
Demenÿ	Demenÿ	k1gFnSc2	Demenÿ
1889	[number]	k4	1889
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Giulio	Giulio	k1gMnSc1	Giulio
Bragaglia	Bragaglia	k1gFnSc1	Bragaglia
1911	[number]	k4	1911
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Bunker	Bunker	k1gMnSc1	Bunker
Gilbreth	Gilbreth	k1gMnSc1	Gilbreth
senior	senior	k1gMnSc1	senior
1914	[number]	k4	1914
</s>
</p>
<p>
<s>
Man	Man	k1gMnSc1	Man
Ray	Ray	k1gMnSc1	Ray
1935	[number]	k4	1935
</s>
</p>
<p>
<s>
Gjon	Gjon	k1gInSc1	Gjon
Mili	Mil	k1gFnSc2	Mil
1930-1950	[number]	k4	1930-1950
</s>
</p>
<p>
<s>
Barbara	Barbara	k1gFnSc1	Barbara
Morgan	morgan	k1gInSc1	morgan
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Jack	Jack	k1gMnSc1	Jack
Delano	Delana	k1gFnSc5	Delana
1943	[number]	k4	1943
</s>
</p>
<p>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Feininger	Feininger	k1gMnSc1	Feininger
1949	[number]	k4	1949
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc4	Georg
Mathieu	Mathiea	k1gMnSc4	Mathiea
1957	[number]	k4	1957
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Lebe	leb	k1gInSc5	leb
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Eric	Eric	k1gInSc1	Eric
Staller	Staller	k1gInSc1	Staller
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Dean	Dean	k1gMnSc1	Dean
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Pugin	Pugin	k1gMnSc1	Pugin
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
Vicki	Vicki	k1gNnSc1	Vicki
DaSilva	DaSilvo	k1gNnSc2	DaSilvo
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Hesketh	Hesketh	k1gMnSc1	Hesketh
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Tokihiro	Tokihiro	k6eAd1	Tokihiro
Sato	Sato	k6eAd1	Sato
1988	[number]	k4	1988
<g/>
Lourdes	Lourdesa	k1gFnPc2	Lourdesa
Grobet	Grobeta	k1gFnPc2	Grobeta
</s>
</p>
<p>
<s>
Erich	Erich	k1gMnSc1	Erich
Hartmann	Hartmann	k1gMnSc1	Hartmann
</s>
</p>
<p>
<s>
Philipp	Philipp	k1gInSc1	Philipp
von	von	k1gInSc1	von
Ostau	Osta	k2eAgFnSc4d1	Osta
</s>
</p>
<p>
<s>
Manfred	Manfred	k1gMnSc1	Manfred
Kielnhofer	Kielnhofer	k1gMnSc1	Kielnhofer
</s>
</p>
<p>
<s>
Eliška	Eliška	k1gFnSc1	Eliška
Bartek	Bartek	k1gInSc1	Bartek
–	–	k?	–
květiny	květina	k1gFnPc1	květina
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
dobré	dobrý	k2eAgMnPc4d1	dobrý
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
luminografisty	luminografista	k1gMnPc4	luminografista
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Eric	Eric	k1gFnSc1	Eric
Staller	Staller	k1gMnSc1	Staller
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Lebe	leb	k1gInSc5	leb
<g/>
,	,	kIx,	,
Dean	Dean	k1gMnSc1	Dean
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
,	,	kIx,	,
Vicki	Vick	k1gFnPc1	Vick
DaSilva	DaSilv	k1gMnSc2	DaSilv
<g/>
,	,	kIx,	,
Troy	Troa	k1gMnSc2	Troa
Paiva	Paivo	k1gNnSc2	Paivo
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Mesrine	Mesrin	k1gInSc5	Mesrin
<g/>
,	,	kIx,	,
Patrick	Patrick	k1gMnSc1	Patrick
Rochon	Rochon	k1gMnSc1	Rochon
<g/>
,	,	kIx,	,
Aurora	Aurora	k1gFnSc1	Aurora
Crowley	Crowlea	k1gFnSc2	Crowlea
<g/>
,	,	kIx,	,
Arturo	Artura	k1gFnSc5	Artura
Aguiar	Aguiar	k1gInSc1	Aguiar
<g/>
,	,	kIx,	,
Lapp-Pro	Lapp-Pro	k1gNnSc1	Lapp-Pro
<g/>
,	,	kIx,	,
Lichtfaktor	Lichtfaktor	k1gInSc1	Lichtfaktor
<g/>
,	,	kIx,	,
Chanette	Chanett	k1gInSc5	Chanett
Manso	Mansa	k1gFnSc5	Mansa
<g/>
,	,	kIx,	,
Dana	Dan	k1gMnSc2	Dan
Maltby	Maltba	k1gMnSc2	Maltba
<g/>
,	,	kIx,	,
Jason	Jason	k1gNnSc1	Jason
D.	D.	kA	D.
Page	Pag	k1gInSc2	Pag
<g/>
,	,	kIx,	,
Janne	Jann	k1gInSc5	Jann
Parviainen	Parviainen	k1gInSc1	Parviainen
<g/>
,	,	kIx,	,
Trevor	Trevor	k1gInSc1	Trevor
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Bosanko	Bosanka	k1gFnSc5	Bosanka
<g/>
,	,	kIx,	,
Jadikan-LP	Jadikan-LP	k1gFnSc5	Jadikan-LP
<g/>
,	,	kIx,	,
Nocturne	Nocturn	k1gInSc5	Nocturn
<g/>
,	,	kIx,	,
Jeremy	Jeremo	k1gNnPc7	Jeremo
Jackson	Jackson	k1gInSc1	Jackson
<g/>
,	,	kIx,	,
Jake	Jake	k1gInSc1	Jake
Saari	Saar	k1gFnSc2	Saar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
i	i	k8xC	i
užité	užitý	k2eAgFnSc6d1	užitá
tvorbě	tvorba	k1gFnSc6	tvorba
využívá	využívat	k5eAaPmIp3nS	využívat
techniku	technika	k1gFnSc4	technika
luminografie	luminografie	k1gFnSc1	luminografie
Jan	Jan	k1gMnSc1	Jan
Pohribný	Pohribný	k2eAgMnSc1d1	Pohribný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
aranžovanou	aranžovaný	k2eAgFnSc4d1	aranžovaná
a	a	k8xC	a
konceptuální	konceptuální	k2eAgFnSc4d1	konceptuální
fotografii	fotografia	k1gFnSc4	fotografia
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
–	–	k?	–
volném	volný	k2eAgInSc6d1	volný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
ho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
předmětu	předmět	k1gInSc2	předmět
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
ať	ať	k8xS	ať
už	už	k6eAd1	už
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
stopy	stopa	k1gFnSc2	stopa
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
cyklus	cyklus	k1gInSc1	cyklus
Pigmenty	pigment	k1gInPc7	pigment
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
předmětná	předmětný	k2eAgFnSc1d1	předmětná
podoba	podoba	k1gFnSc1	podoba
vytržená	vytržený	k2eAgFnSc1d1	vytržená
ze	z	k7c2	z
souvislosti	souvislost	k1gFnSc2	souvislost
na	na	k7c6	na
prázdné	prázdný	k2eAgFnSc6d1	prázdná
ploše	plocha	k1gFnSc6	plocha
oblohy	obloha	k1gFnSc2	obloha
(	(	kIx(	(
<g/>
Nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgMnSc1d1	modrý
nebe	nebe	k1gNnSc1	nebe
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
či	či	k8xC	či
otázku	otázka	k1gFnSc4	otázka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
okamžiku	okamžik	k1gInSc2	okamžik
(	(	kIx(	(
<g/>
Létavci	létavec	k1gMnPc1	létavec
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
své	svůj	k3xOyFgFnPc4	svůj
vize	vize	k1gFnPc4	vize
a	a	k8xC	a
reflexe	reflexe	k1gFnPc4	reflexe
na	na	k7c4	na
posvátná	posvátný	k2eAgNnPc4d1	posvátné
a	a	k8xC	a
kultovní	kultovní	k2eAgNnPc4d1	kultovní
místa	místo	k1gNnPc4	místo
prehistorické	prehistorický	k2eAgFnSc2d1	prehistorická
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
cyklus	cyklus	k1gInSc1	cyklus
Nová	nový	k2eAgFnSc1d1	nová
doba	doba	k1gFnSc1	doba
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
spojuje	spojovat	k5eAaImIp3nS	spojovat
nejen	nejen	k6eAd1	nejen
předešlé	předešlý	k2eAgInPc4d1	předešlý
principy	princip	k1gInPc4	princip
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
výrazných	výrazný	k2eAgFnPc2d1	výrazná
barevných	barevný	k2eAgFnPc2d1	barevná
stop	stopa	k1gFnPc2	stopa
či	či	k8xC	či
monochromatických	monochromatický	k2eAgNnPc2d1	monochromatické
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívá	využívat	k5eAaImIp3nS	využívat
mj.	mj.	kA	mj.
významně	významně	k6eAd1	významně
světelné	světelný	k2eAgFnSc2d1	světelná
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
"	"	kIx"	"
<g/>
zviditelňuje	zviditelňovat	k5eAaImIp3nS	zviditelňovat
<g/>
"	"	kIx"	"
energie	energie	k1gFnSc1	energie
zvolených	zvolený	k2eAgFnPc2d1	zvolená
přírodních	přírodní	k2eAgFnPc2d1	přírodní
lokalit	lokalita	k1gFnPc2	lokalita
či	či	k8xC	či
posvátných	posvátný	k2eAgInPc2d1	posvátný
kamenů	kámen	k1gInPc2	kámen
vztyčených	vztyčený	k2eAgInPc2d1	vztyčený
lidskou	lidský	k2eAgFnSc7d1	lidská
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
konfrontuje	konfrontovat	k5eAaBmIp3nS	konfrontovat
s	s	k7c7	s
prostorem	prostor	k1gInSc7	prostor
a	a	k8xC	a
živly	živel	k1gInPc1	živel
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
instalacích	instalace	k1gFnPc6	instalace
také	také	k9	také
křehké	křehký	k2eAgFnPc1d1	křehká
objekty	objekt	k1gInPc4	objekt
z	z	k7c2	z
ručního	ruční	k2eAgInSc2d1	ruční
papíru	papír	k1gInSc2	papír
Jana	Jan	k1gMnSc2	Jan
Činčery	Činčera	k1gFnSc2	Činčera
(	(	kIx(	(
<g/>
cyklus	cyklus	k1gInSc1	cyklus
JanČin	Jančin	k2eAgInSc1d1	Jančin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
paralelně	paralelně	k6eAd1	paralelně
od	od	k7c2	od
r.	r.	kA	r.
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
cyklu	cyklus	k1gInSc6	cyklus
Osvícené	osvícený	k2eAgInPc4d1	osvícený
city	cit	k1gInPc4	cit
<g/>
,	,	kIx,	,
ilustrující	ilustrující	k2eAgFnPc4d1	ilustrující
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
i	i	k8xC	i
vize	vize	k1gFnPc4	vize
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pobýval	pobývat	k5eAaImAgMnS	pobývat
nebo	nebo	k8xC	nebo
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
významně	významně	k6eAd1	významně
oslovila	oslovit	k5eAaPmAgNnP	oslovit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	s	k7c7	s
skrze	skrze	k?	skrze
konfrontaci	konfrontace	k1gFnSc6	konfrontace
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
hledání	hledání	k1gNnSc4	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
ráje	ráj	k1gInSc2	ráj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cyklus	cyklus	k1gInSc4	cyklus
Andělé	anděl	k1gMnPc1	anděl
<g/>
)	)	kIx)	)
reflektující	reflektující	k2eAgNnSc4d1	reflektující
autorovo	autorův	k2eAgNnSc4d1	autorovo
znepokojení	znepokojení	k1gNnSc4	znepokojení
nad	nad	k7c7	nad
vytrácejícími	vytrácející	k2eAgFnPc7d1	vytrácející
se	se	k3xPyFc4	se
přirozenými	přirozený	k2eAgFnPc7d1	přirozená
vazbami	vazba	k1gFnPc7	vazba
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
duchovní	duchovní	k2eAgFnSc7d1	duchovní
vyprázdněností	vyprázdněnost	k1gFnSc7	vyprázdněnost
současné	současný	k2eAgFnSc2d1	současná
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
postupy	postup	k1gInPc4	postup
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
v	v	k7c6	v
užité	užitý	k2eAgFnSc6d1	užitá
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
nebo	nebo	k8xC	nebo
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
ilustraci	ilustrace	k1gFnSc6	ilustrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Sedlák	Sedlák	k1gMnSc1	Sedlák
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Kamil	Kamil	k1gMnSc1	Kamil
Varga	Varga	k1gMnSc1	Varga
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
ŠigutJan	ŠigutJan	k1gMnSc1	ŠigutJan
Vávra	Vávra	k1gMnSc1	Vávra
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
inscenovanou	inscenovaný	k2eAgFnSc4d1	inscenovaná
konceptuální	konceptuální	k2eAgFnSc4d1	konceptuální
fotografii	fotografia	k1gFnSc4	fotografia
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
neopiktorialismu	neopiktorialismus	k1gInSc2	neopiktorialismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
intenzívně	intenzívně	k6eAd1	intenzívně
fotografování	fotografování	k1gNnSc4	fotografování
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cyklus	cyklus	k1gInSc1	cyklus
fotografií	fotografia	k1gFnPc2	fotografia
"	"	kIx"	"
<g/>
Nalezené	nalezený	k2eAgFnPc1d1	nalezená
věci	věc	k1gFnPc1	věc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
autor	autor	k1gMnSc1	autor
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
pojetí	pojetí	k1gNnSc4	pojetí
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaImAgMnS	využívat
různé	různý	k2eAgFnSc2d1	různá
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
kresby	kresba	k1gFnSc2	kresba
světelnou	světelný	k2eAgFnSc7d1	světelná
tužkou	tužka	k1gFnSc7	tužka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budoval	budovat	k5eAaImAgMnS	budovat
objekty	objekt	k1gInPc4	objekt
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
expozici	expozice	k1gFnSc6	expozice
demontoval	demontovat	k5eAaBmAgMnS	demontovat
<g/>
,	,	kIx,	,
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
v	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgMnS	používat
doplňkových	doplňkový	k2eAgInPc2d1	doplňkový
světelných	světelný	k2eAgInPc2d1	světelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
časové	časový	k2eAgFnSc2d1	časová
expozice	expozice	k1gFnSc2	expozice
apod.	apod.	kA	apod.
Do	do	k7c2	do
výsledných	výsledný	k2eAgFnPc2d1	výsledná
fotografií	fotografia	k1gFnPc2	fotografia
často	často	k6eAd1	často
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
technikami	technika	k1gFnPc7	technika
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
výsledkem	výsledek	k1gInSc7	výsledek
jeho	jeho	k3xOp3gFnPc2	jeho
snah	snaha	k1gFnPc2	snaha
fotografie-objekty	fotografiebjekt	k1gInPc1	fotografie-objekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
nalezení	nalezení	k1gNnSc6	nalezení
a	a	k8xC	a
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgNnSc2d1	nové
pojetí	pojetí	k1gNnSc2	pojetí
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Technika	technika	k1gFnSc1	technika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
režimu	režim	k1gInSc2	režim
bulb	bulba	k1gFnPc2	bulba
===	===	k?	===
</s>
</p>
<p>
<s>
Režim	režim	k1gInSc1	režim
bulb	bulb	k1gInSc1	bulb
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
prostorů	prostor	k1gInPc2	prostor
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
slabým	slabý	k2eAgNnSc7d1	slabé
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
digitálních	digitální	k2eAgFnPc2d1	digitální
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
podporuje	podporovat	k5eAaImIp3nS	podporovat
uzávěrky	uzávěrka	k1gFnPc4	uzávěrka
od	od	k7c2	od
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4000	[number]	k4	4000
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
10	[number]	k4	10
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
s	s	k7c7	s
a	a	k8xC	a
režim	režim	k1gInSc4	režim
bulb	bulba	k1gFnPc2	bulba
<g/>
.	.	kIx.	.
</s>
<s>
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
bulb	bulb	k1gMnSc1	bulb
snímá	snímat	k5eAaImIp3nS	snímat
prostor	prostor	k1gInSc4	prostor
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
stisknuté	stisknutý	k2eAgFnSc2d1	stisknutá
spouště	spoušť	k1gFnSc2	spoušť
(	(	kIx(	(
<g/>
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
fotografování	fotografování	k1gNnSc4	fotografování
s	s	k7c7	s
takto	takto	k6eAd1	takto
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
dobou	doba	k1gFnSc7	doba
uzávěrky	uzávěrka	k1gFnSc2	uzávěrka
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
stativ	stativ	k1gInSc4	stativ
<g/>
,	,	kIx,	,
kabelovou	kabelový	k2eAgFnSc4d1	kabelová
spoušť	spoušť	k1gFnSc4	spoušť
nebo	nebo	k8xC	nebo
časovou	časový	k2eAgFnSc4d1	časová
spoušť	spoušť	k1gFnSc4	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
drželi	držet	k5eAaImAgMnP	držet
spoušť	spoušť	k1gFnSc4	spoušť
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
fotoaparátu	fotoaparát	k1gInSc6	fotoaparát
<g/>
,	,	kIx,	,
nevyhnuli	vyhnout	k5eNaPmAgMnP	vyhnout
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
rozmazání	rozmazání	k1gNnSc1	rozmazání
celkové	celkový	k2eAgFnSc2d1	celková
fotografie	fotografia	k1gFnSc2	fotografia
i	i	k9	i
kdybychom	kdyby	kYmCp1nP	kdyby
použili	použít	k5eAaPmAgMnP	použít
stativ	stativ	k1gInSc4	stativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Malba	malba	k1gFnSc1	malba
ohněm	oheň	k1gInSc7	oheň
===	===	k?	===
</s>
</p>
<p>
<s>
Malba	malba	k1gFnSc1	malba
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
fire	fire	k6eAd1	fire
painting	painting	k1gInSc1	painting
je	být	k5eAaImIp3nS	být
fotografická	fotografický	k2eAgNnPc4d1	fotografické
technika	technikum	k1gNnPc4	technikum
využívající	využívající	k2eAgInSc4d1	využívající
oheň	oheň	k1gInSc4	oheň
jako	jako	k8xS	jako
zdroje	zdroj	k1gInPc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
pojmem	pojem	k1gInSc7	pojem
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
osvětlené	osvětlený	k2eAgInPc1d1	osvětlený
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
zdroj	zdroj	k1gInSc1	zdroj
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
záběr	záběr	k1gInSc4	záběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Technika	technika	k1gFnSc1	technika
====	====	k?	====
</s>
</p>
<p>
<s>
Pohybem	pohyb	k1gInSc7	pohyb
zdrojem	zdroj	k1gInSc7	zdroj
ohnivého	ohnivý	k2eAgNnSc2d1	ohnivé
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
selektivně	selektivně	k6eAd1	selektivně
osvětlit	osvětlit	k5eAaPmF	osvětlit
části	část	k1gFnPc4	část
předmětu	předmět	k1gInSc2	předmět
nebo	nebo	k8xC	nebo
pohybem	pohyb	k1gInSc7	pohyb
"	"	kIx"	"
<g/>
namalovat	namalovat	k5eAaPmF	namalovat
<g/>
"	"	kIx"	"
obraz	obraz	k1gInSc1	obraz
kolem	kolem	k7c2	kolem
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
ohněm	oheň	k1gInSc7	oheň
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pomalou	pomalý	k2eAgFnSc4d1	pomalá
rychlost	rychlost	k1gFnSc4	rychlost
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
sekundu	sekunda	k1gFnSc4	sekunda
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
potřebě	potřeba	k1gFnSc6	potřeba
zachytit	zachytit	k5eAaPmF	zachytit
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
texturu	textura	k1gFnSc4	textura
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
ohně	oheň	k1gInSc2	oheň
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc4d1	rychlý
čas	čas	k1gInSc4	čas
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
tisícinu	tisícina	k1gFnSc4	tisícina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vybavení	vybavení	k1gNnSc1	vybavení
====	====	k?	====
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
položky	položka	k1gFnPc4	položka
patří	patřit	k5eAaImIp3nP	patřit
kevlarová	kevlarový	k2eAgNnPc1d1	kevlarové
lana	lano	k1gNnPc1	lano
nebo	nebo	k8xC	nebo
popruhy	popruha	k1gFnPc1	popruha
namočené	namočený	k2eAgFnPc1d1	namočená
v	v	k7c6	v
petrolejové	petrolejový	k2eAgFnSc6d1	petrolejová
hořlavině	hořlavina	k1gFnSc6	hořlavina
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
Coleman	Coleman	k1gMnSc1	Coleman
fuel	fuel	k1gMnSc1	fuel
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
parafínu	parafín	k1gInSc2	parafín
a	a	k8xC	a
zapálené	zapálený	k2eAgFnPc1d1	zapálená
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
držení	držení	k1gNnSc3	držení
těchto	tento	k3xDgFnPc2	tento
hořlavin	hořlavina	k1gFnPc2	hořlavina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
různé	různý	k2eAgInPc1d1	různý
přípravky	přípravek	k1gInPc1	přípravek
od	od	k7c2	od
kovových	kovový	k2eAgInPc2d1	kovový
rámů	rám	k1gInPc2	rám
nebo	nebo	k8xC	nebo
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
hůlek	hůlka	k1gFnPc2	hůlka
pro	pro	k7c4	pro
zavěšení	zavěšení	k1gNnSc4	zavěšení
popruhu	popruh	k1gInSc2	popruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
stativ	stativ	k1gInSc4	stativ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
expoziční	expoziční	k2eAgFnSc1d1	expoziční
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
pevné	pevný	k2eAgFnSc3d1	pevná
podpoře	podpora	k1gFnSc3	podpora
<g/>
,	,	kIx,	,
stolu	stol	k1gInSc3	stol
nebo	nebo	k8xC	nebo
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kabelová	kabelový	k2eAgFnSc1d1	kabelová
spoušť	spoušť	k1gFnSc1	spoušť
nebo	nebo	k8xC	nebo
samospoušť	samospoušť	k1gFnSc1	samospoušť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
exponování	exponování	k1gNnSc6	exponování
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
rozechvění	rozechvění	k1gNnSc3	rozechvění
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primární	primární	k2eAgFnPc1d1	primární
potřebné	potřebný	k2eAgFnPc1d1	potřebná
položky	položka	k1gFnPc1	položka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kevlarová	Kevlarový	k2eAgFnSc1d1	Kevlarová
šňůra	šňůra	k1gFnSc1	šňůra
nebo	nebo	k8xC	nebo
pás	pás	k1gInSc1	pás
</s>
</p>
<p>
<s>
Coleman	Coleman	k1gMnSc1	Coleman
fuel	fuel	k1gMnSc1	fuel
nebo	nebo	k8xC	nebo
parafín	parafín	k1gInSc1	parafín
</s>
</p>
<p>
<s>
Hasicí	hasicí	k2eAgInSc1d1	hasicí
přístroj	přístroj	k1gInSc1	přístroj
CO2	CO2	k1gFnSc2	CO2
</s>
</p>
<p>
<s>
Vlhký	vlhký	k2eAgInSc4d1	vlhký
ručník	ručník	k1gInSc4	ručník
nebo	nebo	k8xC	nebo
hadr	hadr	k1gInSc4	hadr
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
lze	lze	k6eAd1	lze
plameny	plamen	k1gInPc1	plamen
rychle	rychle	k6eAd1	rychle
uhasit	uhasit	k5eAaPmF	uhasit
</s>
</p>
<p>
<s>
Kovová	kovový	k2eAgFnSc1d1	kovová
nádoba	nádoba	k1gFnSc1	nádoba
k	k	k7c3	k
namáčení	namáčení	k1gNnSc3	namáčení
kevlaru	kevlar	k1gInSc2	kevlar
</s>
</p>
<p>
<s>
Kovová	kovový	k2eAgFnSc1d1	kovová
tyč	tyč	k1gFnSc1	tyč
s	s	k7c7	s
hákem	hák	k1gInSc7	hák
na	na	k7c6	na
konci	konec	k1gInSc6	konec
pro	pro	k7c4	pro
držení	držení	k1gNnSc4	držení
pásu	pás	k1gInSc2	pás
</s>
</p>
<p>
<s>
Pomocný	pomocný	k2eAgMnSc1d1	pomocný
asistent	asistent	k1gMnSc1	asistent
</s>
</p>
<p>
<s>
Stativ	stativ	k1gInSc1	stativ
</s>
</p>
<p>
<s>
Kabelová	kabelový	k2eAgFnSc1d1	kabelová
spoušť	spoušť	k1gFnSc1	spoušť
nebo	nebo	k8xC	nebo
samospoušť	samospoušť	k1gFnSc1	samospoušť
</s>
</p>
<p>
<s>
====	====	k?	====
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
====	====	k?	====
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
hořlavou	hořlavý	k2eAgFnSc7d1	hořlavá
kapalinou	kapalina	k1gFnSc7	kapalina
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
práce	práce	k1gFnSc1	práce
velice	velice	k6eAd1	velice
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
postupovat	postupovat	k5eAaImF	postupovat
podle	podle	k7c2	podle
základních	základní	k2eAgInPc2d1	základní
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
postupů	postup	k1gInPc2	postup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dodržujte	dodržovat	k5eAaImRp2nP	dodržovat
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
ohně	oheň	k1gInSc2	oheň
od	od	k7c2	od
hořlavých	hořlavý	k2eAgFnPc2d1	hořlavá
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nádob	nádoba	k1gFnPc2	nádoba
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Hořlavá	hořlavý	k2eAgFnSc1d1	hořlavá
kapalina	kapalina	k1gFnSc1	kapalina
odkapává	odkapávat	k5eAaImIp3nS	odkapávat
z	z	k7c2	z
pásu	pás	k1gInSc2	pás
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
vznítit	vznítit	k5eAaPmF	vznítit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vždy	vždy	k6eAd1	vždy
nádobu	nádoba	k1gFnSc4	nádoba
s	s	k7c7	s
hořlavinou	hořlavina	k1gFnSc7	hořlavina
zavírejte	zavírat	k5eAaImRp2nP	zavírat
a	a	k8xC	a
udržujte	udržovat	k5eAaImRp2nP	udržovat
v	v	k7c6	v
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
pohybujete	pohybovat	k5eAaImIp2nP	pohybovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udržujte	udržovat	k5eAaImRp2nP	udržovat
čisté	čistý	k2eAgNnSc4d1	čisté
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vznítit	vznítit	k5eAaPmF	vznítit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
tráva	tráva	k1gFnSc1	tráva
nebo	nebo	k8xC	nebo
automobily	automobil	k1gInPc1	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mějte	mít	k5eAaImRp2nP	mít
nablízku	nablízku	k6eAd1	nablízku
vlhký	vlhký	k2eAgInSc4d1	vlhký
ručník	ručník	k1gInSc4	ručník
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
můžete	moct	k5eAaImIp2nP	moct
plameny	plamen	k1gInPc1	plamen
rychle	rychle	k6eAd1	rychle
uhasit	uhasit	k5eAaPmF	uhasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
mějte	mít	k5eAaImRp2nP	mít
nablízku	nablízku	k6eAd1	nablízku
funkční	funkční	k2eAgInSc4d1	funkční
hasicí	hasicí	k2eAgInSc4d1	hasicí
přístroj	přístroj	k1gInSc4	přístroj
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
kompozice	kompozice	k1gFnSc1	kompozice
s	s	k7c7	s
pohybujícím	pohybující	k2eAgMnSc7d1	pohybující
se	se	k3xPyFc4	se
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
uhaste	uhasit	k5eAaPmRp2nP	uhasit
plameny	plamen	k1gInPc1	plamen
zabalením	zabalení	k1gNnPc3	zabalení
do	do	k7c2	do
připraveného	připravený	k2eAgInSc2d1	připravený
mokrého	mokrý	k2eAgInSc2d1	mokrý
ručníku	ručník	k1gInSc2	ručník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
používat	používat	k5eAaImF	používat
zdravý	zdravý	k2eAgInSc4d1	zdravý
selský	selský	k2eAgInSc4d1	selský
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
asistent	asistent	k1gMnSc1	asistent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
věci	věc	k1gFnPc1	věc
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nikdy	nikdy	k6eAd1	nikdy
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
nedělejte	dělat	k5eNaImRp2nP	dělat
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
asistentem	asistent	k1gMnSc7	asistent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stroboskop	stroboskop	k1gInSc4	stroboskop
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
efektní	efektní	k2eAgFnSc4d1	efektní
luminografii	luminografie	k1gFnSc4	luminografie
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
stroboskopických	stroboskopický	k2eAgInPc2d1	stroboskopický
záblesků	záblesk	k1gInPc2	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
záběru	záběr	k1gInSc6	záběr
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přerušovaného	přerušovaný	k2eAgNnSc2d1	přerušované
osvětlení	osvětlení	k1gNnSc2	osvětlení
objeví	objevit	k5eAaPmIp3nS	objevit
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
předmět	předmět	k1gInSc4	předmět
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
fotografie	fotografia	k1gFnPc1	fotografia
Harolda	Harold	k1gMnSc2	Harold
Eugena	Eugen	k1gMnSc2	Eugen
Edgertona	Edgerton	k1gMnSc2	Edgerton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
zachytil	zachytit	k5eAaPmAgMnS	zachytit
hráče	hráč	k1gMnPc4	hráč
golfu	golf	k1gInSc2	golf
při	při	k7c6	při
odpalu	odpal	k1gInSc6	odpal
<g/>
,	,	kIx,	,
tenistu	tenista	k1gMnSc4	tenista
s	s	k7c7	s
raketou	raketa	k1gFnSc7	raketa
<g/>
,	,	kIx,	,
gymnastku	gymnastka	k1gFnSc4	gymnastka
s	s	k7c7	s
kuželkami	kuželka	k1gFnPc7	kuželka
nebo	nebo	k8xC	nebo
plavkyni	plavkyně	k1gFnSc6	plavkyně
při	při	k7c6	při
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fotogram	fotogram	k1gInSc1	fotogram
</s>
</p>
<p>
<s>
Pohybová	pohybový	k2eAgFnSc1d1	pohybová
neostrost	neostrost	k1gFnSc1	neostrost
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Luminografie	Luminografie	k1gFnSc2	Luminografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
workshopu	workshop	k1gInSc2	workshop
Luminografie	Luminografie	k1gFnSc2	Luminografie
aktu	akt	k1gInSc2	akt
s	s	k7c7	s
Kamilem	Kamil	k1gMnSc7	Kamil
Vargou	Varga	k1gMnSc7	Varga
</s>
</p>
<p>
<s>
Van	van	k1gInSc1	van
Elder	Eldra	k1gFnPc2	Eldra
Photography	Photographa	k1gFnSc2	Photographa
-	-	kIx~	-
Light	Light	k2eAgInSc1d1	Light
Painting	Painting	k1gInSc1	Painting
with	witha	k1gFnPc2	witha
fire	fire	k6eAd1	fire
<g/>
,	,	kIx,	,
a	a	k8xC	a
firewall	firewall	k1gInSc1	firewall
<g/>
.	.	kIx.	.
<g/>
aka	aka	k?	aka
fire	firat	k5eAaPmIp3nS	firat
painting	painting	k1gInSc1	painting
</s>
</p>
<p>
<s>
Gorgeous	Gorgeous	k1gMnSc1	Gorgeous
Photos	Photos	k1gMnSc1	Photos
of	of	k?	of
Flame	Flam	k1gInSc5	Flam
Painting	Painting	k1gInSc4	Painting
and	and	k?	and
Fire	Fire	k1gInSc1	Fire
Breathing	Breathing	k1gInSc1	Breathing
Experiments	Experiments	k1gInSc1	Experiments
</s>
</p>
<p>
<s>
Fire	Fire	k1gInSc1	Fire
Painting	Painting	k1gInSc1	Painting
<g/>
:	:	kIx,	:
Chelsea	Chelsea	k1gMnSc1	Chelsea
Jean	Jean	k1gMnSc1	Jean
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
požární	požární	k2eAgFnSc4d1	požární
ochranu	ochrana	k1gFnSc4	ochrana
</s>
</p>
