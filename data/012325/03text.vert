<p>
<s>
Rap	rap	k1gMnSc1	rap
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
používané	používaný	k2eAgNnSc1d1	používané
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
rapping	rapping	k1gInSc4	rapping
či	či	k8xC	či
MCing	MCing	k1gInSc4	MCing
<g/>
,	,	kIx,	,
rýmování	rýmování	k1gNnSc4	rýmování
<g/>
,	,	kIx,	,
spitting	spitting	k1gInSc4	spitting
anebo	anebo	k8xC	anebo
česky	česky	k6eAd1	česky
také	také	k9	také
rapování	rapování	k1gNnSc2	rapování
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rytmicky	rytmicky	k6eAd1	rytmicky
mluvené	mluvený	k2eAgFnPc1d1	mluvená
rýmy	rýma	k1gFnPc1	rýma
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
se	s	k7c7	s
slovíčky	slovíčko	k1gNnPc7	slovíčko
a	a	k8xC	a
poezie	poezie	k1gFnSc1	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Rapping	Rapping	k1gInSc1	Rapping
nebo	nebo	k8xC	nebo
spíš	spíš	k9	spíš
rapování	rapování	k1gNnSc1	rapování
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
elementem	element	k1gInSc7	element
hip	hip	k0	hip
hopové	hopové	k2eAgMnPc2d1	hopové
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
hip	hip	k0	hip
hopovou	hopová	k1gFnSc7	hopová
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Rapování	rapování	k1gNnSc1	rapování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doprovázeno	doprovázet	k5eAaImNgNnS	doprovázet
beatem	beat	k1gInSc7	beat
bez	bez	k7c2	bez
hudebního	hudební	k2eAgInSc2d1	hudební
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Rap	rap	k1gMnSc1	rap
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
zařadit	zařadit	k5eAaPmF	zařadit
pod	pod	k7c4	pod
mluvení	mluvení	k1gNnSc4	mluvení
<g/>
,	,	kIx,	,
prózu	próza	k1gFnSc4	próza
<g/>
,	,	kIx,	,
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
pod	pod	k7c7	pod
písní	píseň	k1gFnSc7	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velice	velice	k6eAd1	velice
staré	starý	k2eAgNnSc1d1	staré
slovo	slovo	k1gNnSc1	slovo
rap	rapa	k1gFnPc2	rapa
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
použití	použití	k1gNnSc1	použití
rychlé	rychlý	k2eAgFnSc2d1	rychlá
mluvy	mluva	k1gFnSc2	mluva
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
rap	rap	k1gMnSc1	rap
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
hit	hit	k1gInSc4	hit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
uhodit	uhodit	k5eAaPmF	uhodit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
say	say	k?	say
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
říct	říct	k5eAaPmF	říct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
adaptováno	adaptovat	k5eAaBmNgNnS	adaptovat
afro-americkou	afromerický	k2eAgFnSc7d1	afro-americká
lidovou	lidový	k2eAgFnSc7d1	lidová
angličtinou	angličtina	k1gFnSc7	angličtina
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc4	ten
converse	converse	k1gFnSc1	converse
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
konverzovat	konverzovat	k5eAaImF	konverzovat
<g/>
,	,	kIx,	,
mluvit	mluvit	k5eAaImF	mluvit
<g/>
)	)	kIx)	)
a	a	k8xC	a
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
také	také	k9	také
znamenalo	znamenat	k5eAaImAgNnS	znamenat
a	a	k8xC	a
popisovalo	popisovat	k5eAaImAgNnS	popisovat
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
termíny	termín	k1gInPc4	termín
"	"	kIx"	"
<g/>
rap	rap	k1gMnSc1	rap
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
rapping	rapping	k1gInSc1	rapping
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
hip	hip	k0	hip
hopovou	hopový	k2eAgFnSc4d1	hopová
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
slova	slovo	k1gNnPc1	slovo
zaměnitelná	zaměnitelný	k2eAgFnSc1d1	zaměnitelná
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
písni	píseň	k1gFnSc6	píseň
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hip-hop	hipop	k1gInSc4	hip-hop
<g/>
.	.	kIx.	.
</s>
<s>
Rap	rap	k1gMnSc1	rap
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
rocku	rock	k1gInSc6	rock
<g/>
,	,	kIx,	,
metalu	metal	k1gInSc6	metal
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
hudebních	hudební	k2eAgInPc6d1	hudební
stylech	styl	k1gInPc6	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rap	rap	k1gMnSc1	rap
rozdělit	rozdělit	k5eAaPmF	rozdělit
hned	hned	k6eAd1	hned
do	do	k7c2	do
několika	několik	k4yIc2	několik
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
úplným	úplný	k2eAgInPc3d1	úplný
začátkům	začátek	k1gInPc3	začátek
a	a	k8xC	a
základům	základ	k1gInPc3	základ
zde	zde	k6eAd1	zde
vznikají	vznikat	k5eAaImIp3nP	vznikat
novější	nový	k2eAgInPc4d2	novější
<g/>
,	,	kIx,	,
iniciativní	iniciativní	k2eAgInPc4d1	iniciativní
nápady	nápad	k1gInPc4	nápad
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
lyrický	lyrický	k2eAgMnSc1d1	lyrický
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
trap-rap	trapap	k1gMnSc1	trap-rap
<g/>
,	,	kIx,	,
tripping-rap	trippingap	k1gMnSc1	tripping-rap
<g/>
,	,	kIx,	,
dead	dead	k1gMnSc1	dead
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
fastflow	fastflow	k?	fastflow
<g/>
,	,	kIx,	,
lazyflow	lazyflow	k?	lazyflow
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kořeny	kořen	k1gInPc1	kořen
===	===	k?	===
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
rapu	rapa	k1gFnSc4	rapa
lze	lze	k6eAd1	lze
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
vůbec	vůbec	k9	vůbec
existovala	existovat	k5eAaImAgFnS	existovat
hip	hip	k0	hip
hopová	hopový	k2eAgFnSc1d1	hopová
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Grijoti	Grijot	k1gMnPc1	Grijot
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
vyprávěli	vyprávět	k5eAaImAgMnP	vyprávět
příběhy	příběh	k1gInPc4	příběh
rytmickým	rytmický	k2eAgInSc7d1	rytmický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
bubínků	bubínek	k1gInPc2	bubínek
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
místních	místní	k2eAgInPc2d1	místní
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
mnoha	mnoho	k4c7	mnoho
současnými	současný	k2eAgMnPc7d1	současný
umělci	umělec	k1gMnPc7	umělec
<g/>
,	,	kIx,	,
moderními	moderní	k2eAgInPc7d1	moderní
"	"	kIx"	"
<g/>
grioty	griot	k1gInPc7	griot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mainstreamovou	mainstreamový	k2eAgFnSc7d1	mainstreamová
kulturou	kultura	k1gFnSc7	kultura
i	i	k8xC	i
akademiky	akademik	k1gMnPc7	akademik
<g/>
.	.	kIx.	.
<g/>
Prvky	prvek	k1gInPc1	prvek
blues	blues	k1gNnSc2	blues
sahají	sahat	k5eAaImIp3nP	sahat
mezi	mezi	k7c7	mezi
work	work	k6eAd1	work
songy	song	k1gInPc7	song
(	(	kIx(	(
<g/>
pracovní	pracovní	k2eAgFnPc4d1	pracovní
písně	píseň	k1gFnPc4	píseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spirituály	spirituál	k1gInPc1	spirituál
<g/>
,	,	kIx,	,
otroctví	otroctví	k1gNnPc1	otroctví
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
hudební	hudební	k2eAgFnSc3d1	hudební
tradici	tradice	k1gFnSc3	tradice
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
"	"	kIx"	"
<g/>
blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
hráváno	hrávat	k5eAaImNgNnS	hrávat
černými	černý	k2eAgFnPc7d1	černá
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
bílými	bílý	k2eAgMnPc7d1	bílý
<g/>
)	)	kIx)	)
umělci	umělec	k1gMnPc7	umělec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mississippi	Mississippi	k1gFnSc1	Mississippi
Delta	delta	k1gFnSc1	delta
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zrušení	zrušení	k1gNnSc1	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Elijah	Elijah	k1gMnSc1	Elijah
Wald	Wald	k1gMnSc1	Wald
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
bluesový	bluesový	k2eAgMnSc1d1	bluesový
hudebník	hudebník	k1gMnSc1	hudebník
oceněný	oceněný	k2eAgInSc4d1	oceněný
Grammy	Gramm	k1gInPc4	Gramm
<g/>
,	,	kIx,	,
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
blues	blues	k1gNnSc1	blues
bylo	být	k5eAaImAgNnS	být
rapováno	rapovat	k5eAaImNgNnS	rapovat
již	již	k9	již
v	v	k7c6	v
brzkých	brzký	k2eAgNnPc6d1	brzké
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Wald	Wald	k6eAd1	Wald
rovněž	rovněž	k9	rovněž
hip-hop	hipop	k1gInSc1	hip-hop
nazval	nazvat	k5eAaBmAgInS	nazvat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
živé	živý	k2eAgNnSc1d1	živé
blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
afro-amerických	afromerický	k2eAgInPc2d1	afro-americký
a	a	k8xC	a
evropských	evropský	k2eAgInPc2d1	evropský
hudebních	hudební	k2eAgInPc2d1	hudební
elementů	element	k1gInPc2	element
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přímý	přímý	k2eAgInSc4d1	přímý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
hip	hip	k0	hip
hop	hop	k0	hop
<g/>
;	;	kIx,	;
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jazz	jazz	k1gInSc1	jazz
je	být	k5eAaImIp3nS	být
předchůdce	předchůdce	k1gMnSc4	předchůdce
hip-hopu	hipop	k1gInSc2	hip-hop
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
odvětví	odvětví	k1gNnSc1	odvětví
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
jazzu	jazz	k1gInSc2	jazz
–	–	k?	–
Jazz	jazz	k1gInSc1	jazz
poetry	poetr	k1gInPc1	poetr
(	(	kIx(	(
<g/>
jazzová	jazzový	k2eAgFnSc1d1	jazzová
poezie	poezie	k1gFnSc1	poezie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
buď	buď	k8xC	buď
rádoby-jazzový	rádobyazzový	k2eAgInSc1d1	rádoby-jazzový
rytmus	rytmus	k1gInSc1	rytmus
nebo	nebo	k8xC	nebo
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
(	(	kIx(	(
<g/>
slovní	slovní	k2eAgFnSc1d1	slovní
<g/>
)	)	kIx)	)
improvizace	improvizace	k1gFnSc1	improvizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
jazzové	jazzový	k2eAgFnSc2d1	jazzová
poezie	poezie	k1gFnSc2	poezie
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Beat	beat	k1gInSc4	beat
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Gil	Gil	k?	Gil
Scott-Heron	Scott-Heron	k1gMnSc1	Scott-Heron
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
jazzové	jazzový	k2eAgFnSc2d1	jazzová
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Revolution	Revolution	k1gInSc1	Revolution
Will	Will	k1gMnSc1	Will
Not	nota	k1gFnPc2	nota
Be	Be	k1gMnSc1	Be
Televised	Televised	k1gMnSc1	Televised
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
OGate	OGat	k1gInSc5	OGat
Blues	blues	k1gNnSc3	blues
Part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
:	:	kIx,	:
We	We	k1gMnSc1	We
Beg	beg	k1gMnSc1	beg
Your	Your	k1gMnSc1	Your
Pardon	pardon	k1gInSc4	pardon
America	Americ	k1gInSc2	Americ
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
či	či	k8xC	či
"	"	kIx"	"
<g/>
Johannesberg	Johannesberg	k1gInSc1	Johannesberg
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
písně	píseň	k1gFnPc1	píseň
stály	stát	k5eAaImAgFnP	stát
za	za	k7c7	za
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
mnoha	mnoho	k4c2	mnoho
rapperů	rapper	k1gInPc2	rapper
<g/>
.	.	kIx.	.
</s>
<s>
Podobní	podobný	k2eAgMnPc1d1	podobný
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
The	The	k1gFnSc1	The
Last	Lasta	k1gFnPc2	Lasta
Poets	Poetsa	k1gFnPc2	Poetsa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
druhu	druh	k1gInSc6	druh
politické	politický	k2eAgFnSc2d1	politická
poezie	poezie	k1gFnSc2	poezie
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k9	rovněž
rapoví	rapový	k2eAgMnPc1d1	rapový
inovátoři	inovátor	k1gMnPc1	inovátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
billboard	billboard	k1gInSc4	billboard
žebříčkách	žebříček	k1gInPc6	žebříček
skončilo	skončit	k5eAaPmAgNnS	skončit
jejich	jejich	k3xOp3gNnSc4	jejich
debutní	debutní	k2eAgNnSc4d1	debutní
album	album	k1gNnSc4	album
hned	hned	k6eAd1	hned
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
přelomu	přelom	k1gInSc6	přelom
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
americkou	americký	k2eAgFnSc4d1	americká
hudbu	hudba	k1gFnSc4	hudba
karibská	karibský	k2eAgFnSc1d1	karibská
kultura	kultura	k1gFnSc1	kultura
i	i	k8xC	i
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
DJové	DJová	k1gFnSc2	DJová
"	"	kIx"	"
<g/>
toastovali	toastovat	k5eAaImAgMnP	toastovat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
africká	africký	k2eAgFnSc1d1	africká
tradice	tradice	k1gFnSc1	tradice
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
rapovali	rapovat	k5eAaImAgMnP	rapovat
grioti	griot	k1gMnPc1	griot
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
"	"	kIx"	"
<g/>
dabnuté	dabnutý	k2eAgInPc4d1	dabnutý
<g/>
"	"	kIx"	"
jamajské	jamajský	k2eAgInPc4d1	jamajský
rytmy	rytmus	k1gInPc4	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
"	"	kIx"	"
<g/>
rap	rapa	k1gFnPc2	rapa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
významy	význam	k1gInPc7	význam
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
to	ten	k3xDgNnSc1	ten
vyjadřovalo	vyjadřovat	k5eAaImAgNnS	vyjadřovat
konání	konání	k1gNnSc4	konání
"	"	kIx"	"
<g/>
neformálních	formální	k2eNgFnPc2d1	neformální
diskuzí	diskuze	k1gFnPc2	diskuze
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
rapery	raper	k1gInPc4	raper
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgInS	patřit
například	například	k6eAd1	například
DJ	DJ	kA	DJ
Kool	Kool	k1gInSc1	Kool
Herc	herc	k1gInSc1	herc
<g/>
.	.	kIx.	.
</s>
<s>
Herc	herc	k1gInSc1	herc
byl	být	k5eAaImAgInS	být
jamajský	jamajský	k2eAgMnSc1d1	jamajský
imigrant	imigrant	k1gMnSc1	imigrant
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
vlnu	vlna	k1gFnSc4	vlna
jamajského	jamajský	k2eAgInSc2d1	jamajský
toastingu	toasting	k1gInSc2	toasting
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Old	Olda	k1gFnPc2	Olda
school	school	k1gInSc1	school
rap	rap	k1gMnSc1	rap
===	===	k?	===
</s>
</p>
<p>
<s>
Old	Olda	k1gFnPc2	Olda
school	school	k1gInSc1	school
rap	rap	k1gMnSc1	rap
<g/>
/	/	kIx~	/
<g/>
hip	hip	k0	hip
hop	hop	k0	hop
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
éra	éra	k1gFnSc1	éra
"	"	kIx"	"
<g/>
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
rapovaných	rapovaný	k2eAgFnPc6d1	rapovaná
písních	píseň	k1gFnPc6	píseň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc4d1	veliký
povyk	povyk	k1gInSc4	povyk
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jak	jak	k8xC	jak
proklamuje	proklamovat	k5eAaBmIp3nS	proklamovat
All	All	k1gMnSc1	All
Music	Music	k1gMnSc1	Music
Guide	Guid	k1gInSc5	Guid
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
zlaté	zlatý	k2eAgFnSc3d1	zlatá
staré	starý	k2eAgFnSc3d1	stará
době	doba	k1gFnSc3	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
byly	být	k5eAaImAgFnP	být
i	i	k9	i
výjimky	výjimka	k1gFnPc1	výjimka
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
rappeři	rapper	k1gMnPc1	rapper
byli	být	k5eAaImAgMnP	být
více	hodně	k6eAd2	hodně
politicky	politicky	k6eAd1	politicky
orientovaní	orientovaný	k2eAgMnPc1d1	orientovaný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kreativní	kreativní	k2eAgFnPc4d1	kreativní
Melle	Melle	k1gFnPc4	Melle
Mel	mlít	k5eAaImRp2nS	mlít
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Furious	Furious	k1gInSc1	Furious
5	[number]	k4	5
(	(	kIx(	(
<g/>
+	+	kIx~	+
"	"	kIx"	"
<g/>
šestý	šestý	k4xOgInSc4	šestý
<g/>
"	"	kIx"	"
frontman	frontman	k1gMnSc1	frontman
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
Flash	Flash	k1gMnSc1	Flash
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
soustřeďoval	soustřeďovat	k5eAaImAgMnS	soustřeďovat
především	především	k9	především
na	na	k7c4	na
socio-politické	socioolitický	k2eAgInPc4d1	socio-politický
komentáře	komentář	k1gInPc4	komentář
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
hry	hra	k1gFnSc2	hra
se	s	k7c7	s
slovíčky	slovíčko	k1gNnPc7	slovíčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
komerční	komerční	k2eAgFnPc1d1	komerční
a	a	k8xC	a
významné	významný	k2eAgFnPc1d1	významná
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
old	old	k?	old
school	schoola	k1gFnPc2	schoola
éry	éra	k1gFnPc1	éra
přicházely	přicházet	k5eAaImAgFnP	přicházet
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
King	King	k1gMnSc1	King
Tim	Tim	k?	Tim
III	III	kA	III
(	(	kIx(	(
<g/>
Personality	personalita	k1gFnSc2	personalita
Jock	Jock	k1gInSc1	Jock
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
US	US	kA	US
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
#	#	kIx~	#
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
–	–	k?	–
Fatback	Fatback	k1gInSc1	Fatback
Band	banda	k1gFnPc2	banda
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rapper	Rapper	k1gInSc1	Rapper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Delight	Delighta	k1gFnPc2	Delighta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
US	US	kA	US
#	#	kIx~	#
<g/>
36	[number]	k4	36
<g/>
,	,	kIx,	,
US	US	kA	US
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
UK	UK	kA	UK
#	#	kIx~	#
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
–	–	k?	–
The	The	k1gFnSc1	The
Sugarhill	Sugarhill	k1gMnSc1	Sugarhill
Gang	gang	k1gInSc1	gang
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Breaks	Breaksa	k1gFnPc2	Breaksa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
US	US	kA	US
#	#	kIx~	#
<g/>
87	[number]	k4	87
<g/>
,	,	kIx,	,
US	US	kA	US
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
US	US	kA	US
Dance	Danka	k1gFnSc6	Danka
#	#	kIx~	#
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
–	–	k?	–
Kurtis	Kurtis	k1gFnSc1	Kurtis
Blow	Blow	k1gFnSc1	Blow
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Funk	funk	k1gInSc1	funk
You	You	k1gMnSc2	You
Up	Up	k1gMnSc2	Up
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
US	US	kA	US
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
#	#	kIx~	#
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
–	–	k?	–
The	The	k1gMnPc4	The
Sequence	Sequence	k1gFnSc2	Sequence
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rapture	Raptur	k1gMnSc5	Raptur
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
US	US	kA	US
Dance	Danka	k1gFnSc6	Danka
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
–	–	k?	–
Blondie	Blondie	k1gFnSc1	Blondie
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Message	Message	k1gFnSc1	Message
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
US	US	kA	US
#	#	kIx~	#
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
US	US	kA	US
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
UK	UK	kA	UK
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
–	–	k?	–
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
Flash	Flash	k1gMnSc1	Flash
&	&	k?	&
the	the	k?	the
Furious	Furious	k1gInSc1	Furious
FiveNejvíce	FiveNejvíce	k1gFnSc1	FiveNejvíce
významní	významný	k2eAgMnPc1d1	významný
umělci	umělec	k1gMnPc1	umělec
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
éře	éra	k1gFnSc6	éra
byli	být	k5eAaImAgMnP	být
the	the	k?	the
Fat	fatum	k1gNnPc2	fatum
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
Kool	Kool	k1gInSc1	Kool
Herc	herc	k1gInSc1	herc
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
Bambaataa	Bambaataa	k1gFnSc1	Bambaataa
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Sugarhill	Sugarhill	k1gMnSc1	Sugarhill
Gang	gang	k1gInSc1	gang
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Treacherous	Treacherous	k1gMnSc1	Treacherous
Three	Three	k1gInSc1	Three
<g/>
,	,	kIx,	,
Funky	funk	k1gInPc1	funk
4	[number]	k4	4
+	+	kIx~	+
1	[number]	k4	1
<g/>
,	,	kIx,	,
Fab	Fab	k1gMnSc1	Fab
5	[number]	k4	5
Freddy	Fredda	k1gFnSc2	Fredda
a	a	k8xC	a
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
Flash	Flash	k1gMnSc1	Flash
&	&	k?	&
The	The	k1gMnSc4	The
Furious	Furious	k1gMnSc1	Furious
Five	Fiv	k1gMnSc4	Fiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
===	===	k?	===
</s>
</p>
<p>
<s>
Golden	Goldno	k1gNnPc2	Goldno
age	age	k?	age
hip	hip	k0	hip
hop	hop	k0	hop
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc1d2	pozdější
1980	[number]	k4	1980
<g/>
s	s	k7c7	s
<g/>
~	~	kIx~	~
<g/>
brzké	brzký	k2eAgNnSc4d1	brzké
1990	[number]	k4	1990
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
éra	éra	k1gFnSc1	éra
drastických	drastický	k2eAgFnPc2d1	drastická
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
textech	text	k1gInPc6	text
hip-hopu	hipop	k1gInSc2	hip-hop
<g/>
.	.	kIx.	.
</s>
<s>
Allmusic	Allmusic	k1gMnSc1	Allmusic
napsal	napsat	k5eAaPmAgMnS	napsat
"	"	kIx"	"
<g/>
lyrické	lyrický	k2eAgInPc4d1	lyrický
kung-fu	kung	k1gInSc3	kung-f
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
hip-hopu	hipop	k1gInSc2	hip-hop
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
<g/>
"	"	kIx"	"
končila	končit	k5eAaImAgFnS	končit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k9	až
1994	[number]	k4	1994
a	a	k8xC	a
vzala	vzít	k5eAaPmAgFnS	vzít
si	se	k3xPyFc3	se
tak	tak	k9	tak
sebou	se	k3xPyFc7	se
období	období	k1gNnSc6	období
s	s	k7c7	s
nejvíce	nejvíce	k6eAd1	nejvíce
inovativními	inovativní	k2eAgInPc7d1	inovativní
texty	text	k1gInPc7	text
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
Nejvíce	hodně	k6eAd3	hodně
významní	významný	k2eAgMnPc1d1	významný
umělci	umělec	k1gMnPc1	umělec
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
éře	éra	k1gFnSc6	éra
byli	být	k5eAaImAgMnP	být
Run-	Run-	k1gMnSc2	Run-
<g/>
D.	D.	kA	D.
<g/>
M.	M.	kA	M.
<g/>
C.	C.	kA	C.
<g/>
,	,	kIx,	,
Beastie	Beastie	k1gFnSc1	Beastie
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
A	a	k9	a
Tribe	Trib	k1gInSc5	Trib
Called	Called	k1gMnSc1	Called
<g />
.	.	kIx.	.
</s>
<s>
Quest	Quest	k1gMnSc1	Quest
<g/>
,	,	kIx,	,
Wu-Tang	Wu-Tang	k1gMnSc1	Wu-Tang
Clan	Clan	k1gMnSc1	Clan
<g/>
,	,	kIx,	,
Boogie	Boogie	k1gFnSc1	Boogie
Down	Downa	k1gFnPc2	Downa
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
,	,	kIx,	,
Public	publicum	k1gNnPc2	publicum
Enemy	Enem	k1gInPc7	Enem
<g/>
,	,	kIx,	,
De	De	k?	De
La	la	k1gNnSc2	la
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
The	The	k1gFnSc4	The
Fat	fatum	k1gNnPc2	fatum
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
Doug	Doug	k1gMnSc1	Doug
E.	E.	kA	E.
Fresh	Fresh	k1gMnSc1	Fresh
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
W.A.	W.A.	k1gFnPc1	W.A.
<g/>
,	,	kIx,	,
Jungle	Jungle	k1gFnPc1	Jungle
Brothers	Brothers	k1gInSc1	Brothers
<g/>
,	,	kIx,	,
Big	Big	k1gFnSc1	Big
Daddy	Dadda	k1gFnSc2	Dadda
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
,	,	kIx,	,
Queen	Queen	k2eAgInSc1d1	Queen
Latifah	Latifah	k1gInSc1	Latifah
<g/>
,	,	kIx,	,
Ice	Ice	k1gFnSc1	Ice
Cube	Cube	k1gFnPc2	Cube
<g/>
,	,	kIx,	,
Salt	salto	k1gNnPc2	salto
N	N	kA	N
Pepa	Pepa	k1gMnSc1	Pepa
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
Pac	pac	k1gFnPc2	pac
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Notorious	Notorious	k1gMnSc1	Notorious
B.I.G.	B.I.G.	k1gMnSc1	B.I.G.
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c4	v
rapu	rapa	k1gFnSc4	rapa
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
historie	historie	k1gFnSc2	historie
hip	hip	k0	hip
hopu	hopus	k1gInSc3	hopus
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
spousta	spousta	k1gFnSc1	spousta
nových	nový	k2eAgInPc2d1	nový
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
elementy	element	k1gInPc4	element
rapu	rapa	k1gFnSc4	rapa
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rap	rap	k1gMnSc1	rap
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
derivát	derivát	k1gInSc1	derivát
rapcore	rapcor	k1gInSc5	rapcor
a	a	k8xC	a
rap	rap	k1gMnSc1	rap
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
fusion	fusion	k1gInSc1	fusion
styl	styl	k1gInSc1	styl
rocku	rock	k1gInSc2	rock
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
punku	punk	k1gInSc2	punk
s	s	k7c7	s
rapovanými	rapovaný	k2eAgInPc7d1	rapovaný
vokály	vokál	k1gInPc7	vokál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hip	hip	k0	hip
house	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc1	produkt
house	house	k1gNnSc1	house
a	a	k8xC	a
rapu	rap	k1gMnSc6	rap
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
populárních	populární	k2eAgInPc2d1	populární
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
můžou	můžou	k?	můžou
obsahovat	obsahovat	k5eAaImF	obsahovat
složku	složka	k1gFnSc4	složka
rapu	rapa	k1gFnSc4	rapa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
,	,	kIx,	,
příklady	příklad	k1gInPc4	příklad
<g/>
:	:	kIx,	:
new	new	k?	new
wave	wave	k1gInSc1	wave
(	(	kIx(	(
<g/>
Blondie	Blondie	k1gFnSc1	Blondie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
disco	disco	k1gNnSc1	disco
(	(	kIx(	(
<g/>
BJ	BJ	kA	BJ
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc1	jazz
(	(	kIx(	(
<g/>
Gang	gang	k1gInSc1	gang
<g />
.	.	kIx.	.
</s>
<s>
Starr	Starr	k1gInSc1	Starr
<g/>
,	,	kIx,	,
funk	funk	k1gInSc1	funk
(	(	kIx(	(
<g/>
Fatback	Fatback	k1gInSc1	Fatback
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Contemporary	Contemporara	k1gFnPc4	Contemporara
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
(	(	kIx(	(
<g/>
Mary	Mary	k1gFnSc1	Mary
J.	J.	kA	J.
Blige	Blige	k1gFnSc1	Blige
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Reggaeton	Reggaeton	k1gInSc1	Reggaeton
(	(	kIx(	(
<g/>
Daddy	Dadda	k1gMnSc2	Dadda
Yankee	yankee	k1gMnSc1	yankee
<g/>
)	)	kIx)	)
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
J-dance	Jance	k1gFnSc1	J-dance
(	(	kIx(	(
<g/>
Soul	Soul	k1gInSc1	Soul
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
Out	Out	k1gFnSc1	Out
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
Garage	Garage	k1gFnSc1	Garage
s	s	k7c7	s
rapem	rape	k1gNnSc7	rape
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
podžánr	podžánr	k1gInSc4	podžánr
grime	griet	k5eAaPmRp1nP	griet
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
rapu	rapa	k1gFnSc4	rapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rovněž	rovněž	k9	rovněž
kulturní	kulturní	k2eAgFnSc1d1	kulturní
scéna	scéna	k1gFnSc1	scéna
hip-hopu	hipop	k1gInSc2	hip-hop
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
ilegálními	ilegální	k2eAgInPc7d1	ilegální
automobilovými	automobilový	k2eAgInPc7d1	automobilový
závody	závod	k1gInPc7	závod
a	a	k8xC	a
kulturou	kultura	k1gFnSc7	kultura
závodů	závod	k1gInPc2	závod
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
hip	hip	k0	hip
hopoví	hopoví	k1gNnPc1	hopoví
umělci	umělec	k1gMnPc1	umělec
nepoužívají	používat	k5eNaImIp3nP	používat
techniky	technik	k1gMnPc7	technik
samplování	samplování	k1gNnSc2	samplování
<g/>
,	,	kIx,	,
syntetické	syntetický	k2eAgInPc4d1	syntetický
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
ani	ani	k8xC	ani
bicí	bicí	k2eAgInPc4d1	bicí
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
The	The	k1gFnSc7	The
Roots	Roots	k1gInSc1	Roots
a	a	k8xC	a
Rage	Rage	k1gFnSc1	Rage
Against	Against	k1gFnSc1	Against
the	the	k?	the
Machine	Machin	k1gMnSc5	Machin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rap	rapa	k1gFnPc2	rapa
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rapping	Rapping	k1gInSc1	Rapping
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rap	rapa	k1gFnPc2	rapa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rap	rapa	k1gFnPc2	rapa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Novinky	novinka	k1gFnPc1	novinka
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
českého	český	k2eAgInSc2d1	český
rapu	rapa	k1gFnSc4	rapa
</s>
</p>
