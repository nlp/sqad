<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
pokryt	pokryt	k1gInSc1	pokryt
mladým	mladý	k1gMnPc3	mladý
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
čistým	čistý	k2eAgInSc7d1	čistý
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc4	veškerý
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
dopadající	dopadající	k2eAgMnSc1d1	dopadající
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
