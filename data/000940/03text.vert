<p>
<s>
Měď	měď	k1gFnSc1	měď
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cu	Cu	k1gFnSc2	Cu
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cuprum	Cuprum	k1gInSc1	Cuprum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
načervenalé	načervenalý	k2eAgFnSc2d1	načervenalá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
tavení	tavení	k1gNnSc4	tavení
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
v	v	k7c6	v
primitivních	primitivní	k2eAgFnPc6d1	primitivní
pecích	pec	k1gFnPc6	pec
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
topilo	topit	k5eAaImAgNnS	topit
datlovými	datlový	k2eAgFnPc7d1	datlová
peckami	pecka	k1gFnPc7	pecka
nebo	nebo	k8xC	nebo
suchým	suchý	k2eAgInSc7d1	suchý
trusem	trus	k1gInSc7	trus
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
3.	[number]	k4	3.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
těžila	těžit	k5eAaImAgFnS	těžit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
с	с	k?	с
(	(	kIx(	(
<g/>
kov	kov	k1gInSc1	kov
Kypru	Kypr	k1gInSc2	Kypr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
na	na	k7c4	na
с	с	k?	с
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
měď	měď	k1gFnSc1	měď
(	(	kIx(	(
<g/>
a	a	k8xC	a
obdobně	obdobně	k6eAd1	obdobně
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
se	se	k3xPyFc4	se
staroperštiny	staroperština	k1gFnSc2	staroperština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tento	tento	k3xDgInSc4	tento
kov	kov	k1gInSc4	kov
nazývala	nazývat	k5eAaImAgFnS	nazývat
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Persie	Persie	k1gFnSc2	Persie
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
dnešní	dnešní	k2eAgInSc1d1	dnešní
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
)	)	kIx)	)
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgNnPc3d3	nejstarší
nalezištím	naleziště	k1gNnPc3	naleziště
měděné	měděný	k2eAgFnSc2d1	měděná
rudy	ruda	k1gFnPc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc7d1	dobrá
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
vodivostí	vodivost	k1gFnSc7	vodivost
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
mechanicky	mechanicky	k6eAd1	mechanicky
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odolná	odolný	k2eAgNnPc1d1	odolné
proti	proti	k7c3	proti
atmosférické	atmosférický	k2eAgFnSc3d1	atmosférická
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
řady	řada	k1gFnSc2	řada
velmi	velmi	k6eAd1	velmi
důležitých	důležitý	k2eAgFnPc2d1	důležitá
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
důležitá	důležitý	k2eAgNnPc1d1	důležité
pro	pro	k7c4	pro
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Typický	typický	k2eAgInSc1d1	typický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
kovově	kovově	k6eAd1	kovově
lesklý	lesklý	k2eAgInSc1d1	lesklý
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
vlivem	vlivem	k7c2	vlivem
oxidace	oxidace	k1gFnSc2	oxidace
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
rezavohnědé	rezavohnědý	k2eAgFnSc2d1	rezavohnědá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
tenkých	tenký	k2eAgInPc6d1	tenký
plátech	plát	k1gInPc6	plát
prosvítá	prosvítat	k5eAaImIp3nS	prosvítat
zelenomodře	zelenomodro	k6eAd1	zelenomodro
<g/>
.	.	kIx.	.
</s>
<s>
Krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
kubické	kubický	k2eAgFnSc6d1	kubická
plošně	plošně	k6eAd1	plošně
centrované	centrovaný	k2eAgFnSc6d1	centrovaná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
I.B	I.B	k1gFnSc2	I.B
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
vlastností	vlastnost	k1gFnPc2	vlastnost
podobají	podobat	k5eAaImIp3nP	podobat
sousedům	soused	k1gMnPc3	soused
nalevo	nalevo	k6eAd1	nalevo
(	(	kIx(	(
<g/>
prvkům	prvek	k1gInPc3	prvek
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
B	B	kA	B
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
tedy	tedy	k9	tedy
přechodným	přechodný	k2eAgInPc3d1	přechodný
kovům	kov	k1gInPc3	kov
–	–	k?	–
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc1	palladium
a	a	k8xC	a
platina	platina	k1gFnSc1	platina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
vlastnostech	vlastnost	k1gFnPc6	vlastnost
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobají	podobat	k5eAaImIp3nP	podobat
prvkům	prvek	k1gInPc3	prvek
I.A	I.A	k1gMnPc2	I.A
skupiny	skupina	k1gFnSc2	skupina
–	–	k?	–
alkalickým	alkalický	k2eAgMnPc3d1	alkalický
kovům	kov	k1gInPc3	kov
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prvkům	prvek	k1gInPc3	prvek
nepřechodným	přechodný	k2eNgMnSc7d1	nepřechodný
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc1d1	společný
rys	rys	k1gInSc1	rys
se	s	k7c7	s
sousedy	soused	k1gMnPc7	soused
vlevo	vlevo	k6eAd1	vlevo
má	mít	k5eAaImIp3nS	mít
měď	měď	k1gFnSc4	měď
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stupni	stupeň	k1gInSc6	stupeň
Cu2	Cu2	k1gFnSc2	Cu2
<g/>
+	+	kIx~	+
a	a	k8xC	a
jejích	její	k3xOp3gInPc6	její
barevných	barevný	k2eAgInPc6d1	barevný
komplexech	komplex	k1gInPc6	komplex
a	a	k8xC	a
společný	společný	k2eAgInSc4d1	společný
s	s	k7c7	s
alkalickými	alkalický	k2eAgInPc7d1	alkalický
kovy	kov	k1gInPc7	kov
zejména	zejména	k9	zejména
Cu1	Cu1	k1gFnSc2	Cu1
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dvou	dva	k4xCgInPc6	dva
oxidačních	oxidační	k2eAgInPc6d1	oxidační
stupních	stupeň	k1gInPc6	stupeň
tvoří	tvořit	k5eAaImIp3nS	tvořit
měď	měď	k1gFnSc1	měď
nejvíce	nejvíce	k6eAd1	nejvíce
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stupni	stupeň	k1gInSc6	stupeň
Cu3	Cu3	k1gFnSc2	Cu3
<g/>
+	+	kIx~	+
a	a	k8xC	a
Cu4	Cu4	k1gMnSc1	Cu4
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Trojmocnou	trojmocný	k2eAgFnSc4d1	trojmocná
měď	měď	k1gFnSc4	měď
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
velkými	velký	k2eAgInPc7d1	velký
anionty	anion	k1gInPc7	anion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
ušlechtilý	ušlechtilý	k2eAgInSc1d1	ušlechtilý
kov	kov	k1gInSc1	kov
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
z	z	k7c2	z
kyseliny	kyselina	k1gFnSc2	kyselina
vytěsnit	vytěsnit	k5eAaPmF	vytěsnit
kation	kation	k1gInSc4	kation
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
oxidujících	oxidující	k2eAgFnPc6d1	oxidující
kyselinách	kyselina	k1gFnPc6	kyselina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
neoxidujících	oxidující	k2eNgFnPc6d1	oxidující
kyselinách	kyselina	k1gFnPc6	kyselina
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
zředěné	zředěný	k2eAgFnSc2d1	zředěná
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
přidá	přidat	k5eAaPmIp3nS	přidat
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
(	(	kIx(	(
<g/>
nejběžněji	běžně	k6eAd3	běžně
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
až	až	k9	až
bouřlivě	bouřlivě	k6eAd1	bouřlivě
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
koncentraci	koncentrace	k1gFnSc4	koncentrace
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
peroxidu	peroxid	k1gInSc2	peroxid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
také	také	k9	také
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
probíhá	probíhat	k5eAaImIp3nS	probíhat
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zeleného	zelený	k2eAgInSc2d1	zelený
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
HCl	HCl	k1gMnSc1	HCl
+	+	kIx~	+
H2O2	H2O2	k1gMnSc1	H2O2
+	+	kIx~	+
Cu	Cu	k1gMnSc1	Cu
→	→	k?	→
CuCl2	CuCl2	k1gMnSc1	CuCl2
+	+	kIx~	+
2	[number]	k4	2
H2OV	H2OV	k1gFnSc7	H2OV
roztoku	roztok	k1gInSc2	roztok
zředěné	zředěný	k2eAgFnSc2d1	zředěná
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
oxidačního	oxidační	k2eAgNnSc2d1	oxidační
činidla	činidlo	k1gNnSc2	činidlo
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zelenomodrého	zelenomodrý	k2eAgInSc2d1	zelenomodrý
roztoku	roztok	k1gInSc2	roztok
síranu	síran	k1gInSc2	síran
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
oxidující	oxidující	k2eAgFnSc7d1	oxidující
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
zprvu	zprvu	k6eAd1	zprvu
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
černá	černý	k2eAgFnSc1d1	černá
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
zčernání	zčernání	k1gNnSc6	zčernání
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnPc1d1	sírová
na	na	k7c4	na
modrý	modrý	k2eAgInSc4d1	modrý
roztok	roztok	k1gInSc4	roztok
síranu	síran	k1gInSc2	síran
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
H2SO4	H2SO4	k4	H2SO4
+	+	kIx~	+
H2O2	H2O2	k1gMnSc1	H2O2
+	+	kIx~	+
Cu	Cu	k1gMnSc1	Cu
→	→	k?	→
CuSO4	CuSO4	k1gMnSc1	CuSO4
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
</s>
</p>
<p>
<s>
Cu	Cu	k?	Cu
+	+	kIx~	+
H2SO4	H2SO4	k1gMnSc1	H2SO4
→	→	k?	→
CuO	CuO	k1gMnSc1	CuO
+	+	kIx~	+
SO2	SO2	k1gMnSc1	SO2
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
</s>
</p>
<p>
<s>
CuO	CuO	k?	CuO
+	+	kIx~	+
H2SO4	H2SO4	k1gMnSc2	H2SO4
→	→	k?	→
CuSO4	CuSO4	k1gMnSc2	CuSO4
+	+	kIx~	+
H2OV	H2OV	k1gMnSc2	H2OV
roztoku	roztok	k1gInSc2	roztok
zředěné	zředěný	k2eAgFnSc2d1	zředěná
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
oxidující	oxidující	k2eAgFnSc7d1	oxidující
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
modrého	modrý	k2eAgInSc2d1	modrý
roztoku	roztok	k1gInSc2	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
měďnatého	měďnatý	k2eAgNnSc2d1	měďnaté
a	a	k8xC	a
uvolňování	uvolňování	k1gNnSc1	uvolňování
oxidu	oxid	k1gInSc2	oxid
dusnatého	dusnatý	k2eAgInSc2d1	dusnatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
oxidující	oxidující	k2eAgFnSc7d1	oxidující
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
modrého	modrý	k2eAgInSc2d1	modrý
roztoku	roztok	k1gInSc2	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
měďnatého	měďnatý	k2eAgNnSc2d1	měďnaté
a	a	k8xC	a
uvolňování	uvolňování	k1gNnSc1	uvolňování
oxidu	oxid	k1gInSc2	oxid
dusičitého	dusičitý	k2eAgInSc2d1	dusičitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
Cu	Cu	k1gFnSc1	Cu
+	+	kIx~	+
8	[number]	k4	8
HNO3	HNO3	k1gFnSc2	HNO3
→	→	k?	→
3	[number]	k4	3
Cu	Cu	k1gFnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NO3	NO3	k1gFnSc1	NO3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
NO	no	k9	no
+	+	kIx~	+
4	[number]	k4	4
H2O	H2O	k1gFnPc2	H2O
</s>
</p>
<p>
<s>
Cu	Cu	k?	Cu
+	+	kIx~	+
4	[number]	k4	4
HNO3	HNO3	k1gMnSc1	HNO3
→	→	k?	→
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NO3	NO3	k1gMnSc1	NO3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
NO2	NO2	k1gFnPc2	NO2
+	+	kIx~	+
2	[number]	k4	2
H2OV	H2OV	k1gFnPc2	H2OV
alkalických	alkalický	k2eAgInPc6d1	alkalický
hydroxidech	hydroxid	k1gInPc6	hydroxid
je	být	k5eAaImIp3nS	být
měď	měď	k1gFnSc1	měď
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
koncentrovaných	koncentrovaný	k2eAgInPc6d1	koncentrovaný
roztocích	roztok	k1gInPc6	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
kyanidů	kyanid	k1gInPc2	kyanid
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
Cu	Cu	k1gFnSc1	Cu
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
4	[number]	k4	4
CN	CN	kA	CN
<g/>
-	-	kIx~	-
→	→	k?	→
2	[number]	k4	2
[	[	kIx(	[
<g/>
CuI	CuI	k1gMnSc1	CuI
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
+	+	kIx~	+
2	[number]	k4	2
OH	OH	kA	OH
<g/>
-	-	kIx~	-
+	+	kIx~	+
H2	H2	k1gFnSc1	H2
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
měď	měď	k1gFnSc1	měď
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
<g/>
)	)	kIx)	)
dobrou	dobrý	k2eAgFnSc4d1	dobrá
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
zeleného	zelený	k2eAgInSc2d1	zelený
zásaditého	zásaditý	k2eAgInSc2d1	zásaditý
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
(	(	kIx(	(
<g/>
CuCO3	CuCO3	k1gFnSc1	CuCO3
.	.	kIx.	.
</s>
<s>
Cu	Cu	k?	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
–	–	k?	–
měděnkou	měděnka	k1gFnSc7	měděnka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
účinně	účinně	k6eAd1	účinně
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
další	další	k2eAgFnSc3d1	další
korozi	koroze	k1gFnSc3	koroze
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pasivace	pasivace	k1gFnSc2	pasivace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
výraznější	výrazný	k2eAgFnSc1d2	výraznější
vrstva	vrstva	k1gFnSc1	vrstva
měděnky	měděnka	k1gFnSc2	měděnka
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
vzdušné	vzdušný	k2eAgFnPc4d1	vzdušná
vlhkosti	vlhkost	k1gFnPc4	vlhkost
<g/>
,	,	kIx,	,
za	za	k7c4	za
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
až	až	k6eAd1	až
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
měď	měď	k1gFnSc4	měď
působí	působit	k5eAaImIp3nS	působit
za	za	k7c2	za
pokojové	pokojový	k2eAgFnSc2d1	pokojová
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
i	i	k9	i
chlor	chlor	k1gInSc1	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
spoustou	spousta	k1gFnSc7	spousta
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejvýraznějším	výrazný	k2eAgInPc3d3	nejvýraznější
patří	patřit	k5eAaImIp3nP	patřit
slučivost	slučivost	k1gFnSc4	slučivost
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
měďnaté	měďnatý	k2eAgFnPc1d1	měďnatá
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Měď	měď	k1gFnSc1	měď
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
po	po	k7c6	po
stříbře	stříbro	k1gNnSc6	stříbro
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
druhou	druhý	k4xOgFnSc4	druhý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
vodivost	vodivost	k1gFnSc4	vodivost
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgInSc6d1	malý
obsahu	obsah	k1gInSc6	obsah
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
arsenu	arsen	k1gInSc2	arsen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vodivost	vodivost	k1gFnSc1	vodivost
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
i	i	k9	i
výborným	výborný	k2eAgInSc7d1	výborný
vodičem	vodič	k1gInSc7	vodič
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Z	z	k7c2	z
praxe	praxe	k1gFnSc2	praxe
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
elektrodou	elektroda	k1gFnSc7	elektroda
k	k	k7c3	k
sobě	se	k3xPyFc3	se
svařit	svařit	k5eAaPmF	svařit
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
ocelové	ocelový	k2eAgInPc4d1	ocelový
plechy	plech	k1gInPc4	plech
tloušťky	tloušťka	k1gFnSc2	tloušťka
6	[number]	k4	6
mm	mm	kA	mm
<g/>
,	,	kIx,	,
mezera	mezera	k1gFnSc1	mezera
také	také	k9	také
6	[number]	k4	6
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Zespodu	zespodu	k6eAd1	zespodu
se	se	k3xPyFc4	se
přitiskne	přitisknout	k5eAaPmIp3nS	přitisknout
měděná	měděný	k2eAgFnSc1d1	měděná
destička	destička	k1gFnSc1	destička
tloušťky	tloušťka	k1gFnSc2	tloušťka
10	[number]	k4	10
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
el	ela	k1gFnPc2	ela
<g/>
.	.	kIx.	.
obloukem	oblouk	k1gInSc7	oblouk
tyto	tento	k3xDgInPc4	tento
plechy	plech	k1gInPc4	plech
k	k	k7c3	k
sobě	se	k3xPyFc3	se
svářet	svářet	k5eAaImF	svářet
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
teplota	teplota	k1gFnSc1	teplota
oblouku	oblouk	k1gInSc2	oblouk
je	být	k5eAaImIp3nS	být
3000	[number]	k4	3000
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
roztavené	roztavený	k2eAgFnSc2d1	roztavená
oceli	ocel	k1gFnSc2	ocel
1500	[number]	k4	1500
°	°	k?	°
<g/>
C	C	kA	C
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
odvádí	odvádět	k5eAaImIp3nS	odvádět
teplo	teplo	k1gNnSc4	teplo
tak	tak	k9	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
nenataví	natavit	k5eNaPmIp3nS	natavit
a	a	k8xC	a
žhavou	žhavý	k2eAgFnSc4d1	žhavá
lázeň	lázeň	k1gFnSc4	lázeň
udrží	udržet	k5eAaPmIp3nS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Destička	destička	k1gFnSc1	destička
<g/>
–	–	k?	–
<g/>
neporušená	porušený	k2eNgFnSc1d1	neporušená
<g/>
–	–	k?	–
<g/>
pak	pak	k6eAd1	pak
odpadne	odpadnout	k5eAaPmIp3nS	odpadnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
kovová	kovový	k2eAgFnSc1d1	kovová
měď	měď	k1gFnSc1	měď
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
praktické	praktický	k2eAgFnPc4d1	praktická
aplikace	aplikace	k1gFnPc4	aplikace
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
její	její	k3xOp3gFnPc4	její
slitiny	slitina	k1gFnPc4	slitina
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tažná	tažný	k2eAgFnSc1d1	tažná
a	a	k8xC	a
kujná	kujný	k2eAgFnSc1d1	kujná
a	a	k8xC	a
i	i	k9	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
vodiče	vodič	k1gInSc2	vodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
tzv.	tzv.	kA	tzv.
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
měď	měď	k1gFnSc4	měď
obsahující	obsahující	k2eAgFnSc2d1	obsahující
O2	O2	k1gFnSc2	O2
nad	nad	k7c7	nad
0,003	[number]	k4	0,003
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
u	u	k7c2	u
bezkyslíkové	bezkyslíkový	k2eAgFnSc2d1	bezkyslíkový
mědi	měď	k1gFnSc2	měď
nad	nad	k7c7	nad
0,002	[number]	k4	0,002
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
do	do	k7c2	do
mědi	měď	k1gFnSc2	měď
difunduje	difundovat	k5eAaImIp3nS	difundovat
(	(	kIx(	(
<g/>
proniká	pronikat	k5eAaImIp3nS	pronikat
<g/>
)	)	kIx)	)
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
váže	vázat	k5eAaImIp3nS	vázat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
v	v	k7c6	v
mědi	měď	k1gFnSc6	měď
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
molekuly	molekula	k1gFnPc4	molekula
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
působí	působit	k5eAaImIp3nS	působit
tlakem	tlak	k1gInSc7	tlak
na	na	k7c4	na
okolní	okolní	k2eAgFnSc4d1	okolní
strukturu	struktura	k1gFnSc4	struktura
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
malé	malý	k2eAgFnPc4d1	malá
trhlinky	trhlinka	k1gFnPc4	trhlinka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
následkem	následek	k1gInSc7	následek
je	být	k5eAaImIp3nS	být
zhoršení	zhoršení	k1gNnSc1	zhoršení
mechanických	mechanický	k2eAgFnPc2d1	mechanická
vlastností	vlastnost	k1gFnPc2	vlastnost
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
měď	měď	k1gFnSc4	měď
rozžhavíme	rozžhavit	k5eAaPmIp1nP	rozžhavit
a	a	k8xC	a
namočíme	namočit	k5eAaPmIp1nP	namočit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
změkne	změknout	k5eAaPmIp3nS	změknout
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
opět	opět	k6eAd1	opět
sama	sám	k3xTgMnSc4	sám
ztvrdne	ztvrdnout	k5eAaPmIp3nS	ztvrdnout
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
využívali	využívat	k5eAaImAgMnP	využívat
například	například	k6eAd1	například
mědikovci	mědikovec	k1gMnPc1	mědikovec
<g/>
.	.	kIx.	.
</s>
<s>
Automechanici	automechanik	k1gMnPc1	automechanik
si	se	k3xPyFc3	se
takové	takový	k3xDgInPc4	takový
nýty	nýt	k1gInPc4	nýt
dávali	dávat	k5eAaImAgMnP	dávat
pak	pak	k6eAd1	pak
do	do	k7c2	do
ledničky	lednička	k1gFnSc2	lednička
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
déle	dlouho	k6eAd2	dlouho
vydržely	vydržet	k5eAaPmAgFnP	vydržet
měkké	měkký	k2eAgFnPc1d1	měkká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Měď	měď	k1gFnSc1	měď
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
přítomna	přítomen	k2eAgFnSc1d1	přítomna
poměrně	poměrně	k6eAd1	poměrně
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
55	[number]	k4	55
–	–	k?	–
70	[number]	k4	70
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
0,003	[number]	k4	0,003
miligramů	miligram	k1gInPc2	miligram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
mědi	měď	k1gFnSc2	měď
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ryzí	ryzí	k2eAgFnSc1d1	ryzí
měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácně	vzácně	k6eAd1	vzácně
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgMnSc1d1	elementární
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
Hořejšího	Hořejšího	k2eAgNnSc2d1	Hořejšího
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ji	on	k3xPp3gFnSc4	on
nacházíme	nacházet	k5eAaImIp1nP	nacházet
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sulfidů	sulfid	k1gInPc2	sulfid
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
chalkocit	chalkocit	k1gInSc1	chalkocit
neboli	neboli	k8xC	neboli
leštěnec	leštěnec	k1gInSc1	leštěnec
měděný	měděný	k2eAgMnSc1d1	měděný
Cu2S	Cu2S	k1gMnSc1	Cu2S
<g/>
,	,	kIx,	,
covellin	covellin	k2eAgMnSc1d1	covellin
CuS	CuS	k1gMnSc1	CuS
<g/>
,	,	kIx,	,
bornit	bornit	k1gInSc1	bornit
Cu3FeS3	Cu3FeS3	k1gFnSc1	Cu3FeS3
<g/>
,	,	kIx,	,
bournonit	bournonit	k1gInSc1	bournonit
(	(	kIx(	(
<g/>
Cu2	Cu2	k1gFnSc1	Cu2
<g/>
.	.	kIx.	.
</s>
<s>
Pb	Pb	k?	Pb
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
SbS3	SbS3	k1gFnSc2	SbS3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
nebo	nebo	k8xC	nebo
chalkopyrit	chalkopyrit	k1gInSc4	chalkopyrit
neboli	neboli	k8xC	neboli
kyz	kyz	k1gInSc4	kyz
měděný	měděný	k2eAgInSc4d1	měděný
CuFeS2	CuFeS2	k1gFnSc7	CuFeS2
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
minerály	minerál	k1gInPc7	minerál
jsou	být	k5eAaImIp3nP	být
kuprit	kuprit	k1gInSc4	kuprit
Cu2O	Cu2O	k1gFnSc2	Cu2O
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc1d1	zelený
malachit	malachit	k1gInSc1	malachit
CuCO3	CuCO3	k1gFnSc2	CuCO3
.	.	kIx.	.
</s>
<s>
Cu	Cu	k?	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
chemicky	chemicky	k6eAd1	chemicky
podobný	podobný	k2eAgInSc1d1	podobný
modrý	modrý	k2eAgInSc1d1	modrý
azurit	azurit	k1gInSc1	azurit
2	[number]	k4	2
CuCO3	CuCO3	k1gFnPc2	CuCO3
.	.	kIx.	.
</s>
<s>
Cu	Cu	k?	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
světové	světový	k2eAgMnPc4d1	světový
producenty	producent	k1gMnPc4	producent
mědi	měď	k1gFnSc2	měď
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
USA	USA	kA	USA
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
Utahu	Utah	k1gInSc6	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
ložiska	ložisko	k1gNnPc1	ložisko
měděných	měděný	k2eAgFnPc2d1	měděná
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
Zairu	Zairo	k1gNnSc6	Zairo
<g/>
,	,	kIx,	,
Zambii	Zambie	k1gFnSc6	Zambie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měď	měď	k1gFnSc1	měď
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
hemocyaninu	hemocyanina	k1gFnSc4	hemocyanina
obsaženého	obsažený	k2eAgMnSc2d1	obsažený
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
mědi	měď	k1gFnSc2	měď
jsou	být	k5eAaImIp3nP	být
sulfidické	sulfidický	k2eAgFnPc1d1	sulfidická
rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsah	obsah	k1gInSc1	obsah
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k6eAd1	kolem
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vytěžená	vytěžený	k2eAgFnSc1d1	vytěžená
ruda	ruda	k1gFnSc1	ruda
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nejprve	nejprve	k6eAd1	nejprve
drtí	drtit	k5eAaImIp3nS	drtit
a	a	k8xC	a
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
obsah	obsah	k1gInSc1	obsah
mědi	měď	k1gFnSc2	měď
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
na	na	k7c4	na
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
mědi	měď	k1gFnSc2	měď
ze	z	k7c2	z
sulfidických	sulfidický	k2eAgFnPc2d1	sulfidická
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
základních	základní	k2eAgInPc6d1	základní
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
krok	krok	k1gInSc1	krok
se	se	k3xPyFc4	se
však	však	k9	však
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
obměnil	obměnit	k5eAaPmAgInS	obměnit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevyužívá	využívat	k5eNaImIp3nS	využívat
ani	ani	k8xC	ani
německého	německý	k2eAgInSc2d1	německý
pochodu	pochod	k1gInSc2	pochod
pražně	pražně	k6eAd1	pražně
redukčního	redukční	k2eAgNnSc2d1	redukční
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
anglického	anglický	k2eAgInSc2d1	anglický
pochodu	pochod	k1gInSc2	pochod
pražně	pražně	k6eAd1	pražně
reakčního	reakční	k2eAgNnSc2d1	reakční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pražení	pražení	k1gNnSc1	pražení
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
základní	základní	k2eAgInSc4d1	základní
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
odstranění	odstranění	k1gNnSc1	odstranění
co	co	k8xS	co
možná	možná	k9	možná
největšího	veliký	k2eAgNnSc2d3	veliký
množství	množství	k1gNnSc2	množství
síry	síra	k1gFnSc2	síra
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
a	a	k8xC	a
převedení	převedení	k1gNnSc1	převedení
co	co	k8xS	co
možná	možná	k9	možná
největšího	veliký	k2eAgNnSc2d3	veliký
množství	množství	k1gNnSc2	množství
sulfidů	sulfid	k1gInPc2	sulfid
na	na	k7c4	na
oxidy	oxid	k1gInPc4	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Oxidy	oxid	k1gInPc1	oxid
arsenu	arsen	k1gInSc2	arsen
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
pražení	pražení	k1gNnSc6	pražení
vytěkají	vytěkat	k5eAaPmIp3nP	vytěkat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
klesne	klesnout	k5eAaPmIp3nS	klesnout
obsah	obsah	k1gInSc1	obsah
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
mědi	měď	k1gFnSc2	měď
připadá	připadat	k5eAaImIp3nS	připadat
přibližně	přibližně	k6eAd1	přibližně
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
druhý	druhý	k4xOgInSc4	druhý
krok	krok	k1gInSc4	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
Cu2S	Cu2S	k1gFnSc1	Cu2S
+	+	kIx~	+
3	[number]	k4	3
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
Cu2O	Cu2O	k1gFnPc2	Cu2O
+	+	kIx~	+
2	[number]	k4	2
SO22	SO22	k1gFnPc2	SO22
<g/>
.	.	kIx.	.
</s>
<s>
Tavení	tavení	k1gNnSc1	tavení
na	na	k7c4	na
měděný	měděný	k2eAgInSc4d1	měděný
lech	lech	k1gInSc4	lech
(	(	kIx(	(
<g/>
kamínek	kamínek	k1gInSc4	kamínek
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
šachtových	šachtový	k2eAgFnPc6d1	šachtová
nebo	nebo	k8xC	nebo
plamenných	plamenný	k2eAgFnPc6d1	plamenná
pecích	pec	k1gFnPc6	pec
za	za	k7c4	za
přidání	přidání	k1gNnSc4	přidání
koksu	koks	k1gInSc2	koks
a	a	k8xC	a
struskových	struskový	k2eAgFnPc2d1	Strusková
přísad	přísada	k1gFnPc2	přísada
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
)	)	kIx)	)
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1400	[number]	k4	1400
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odstranil	odstranit	k5eAaPmAgInS	odstranit
sulfid	sulfid	k1gInSc1	sulfid
železnatý	železnatý	k2eAgInSc1d1	železnatý
FeS	fes	k1gNnSc3	fes
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
pochodu	pochod	k1gInSc6	pochod
přechází	přecházet	k5eAaImIp3nS	přecházet
oxid	oxid	k1gInSc4	oxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
kroku	krok	k1gInSc6	krok
opět	opět	k6eAd1	opět
v	v	k7c4	v
sulfid	sulfid	k1gInSc4	sulfid
a	a	k8xC	a
sulfid	sulfid	k1gInSc4	sulfid
železnatý	železnatý	k2eAgInSc4d1	železnatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
křemičitým	křemičitý	k2eAgInSc7d1	křemičitý
na	na	k7c4	na
křemičitan	křemičitan	k1gInSc4	křemičitan
železnatý	železnatý	k2eAgInSc4d1	železnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
strusku	struska	k1gFnSc4	struska
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďný	měďný	k2eAgInSc1d1	měďný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
sloučeninami	sloučenina	k1gFnPc7	sloučenina
usazuje	usazovat	k5eAaImIp3nS	usazovat
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
taveniny	tavenina	k1gFnSc2	tavenina
jako	jako	k8xS	jako
měděný	měděný	k2eAgInSc4d1	měděný
lech	lech	k1gInSc4	lech
neboli	neboli	k8xC	neboli
kamínek	kamínek	k1gInSc4	kamínek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgInSc2d1	železnatý
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
následuje	následovat	k5eAaImIp3nS	následovat
třetí	třetí	k4xOgInSc4	třetí
krok	krok	k1gInSc4	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
CuO	CuO	k1gFnSc1	CuO
+	+	kIx~	+
FeS	fes	k1gNnSc1	fes
+	+	kIx~	+
C	C	kA	C
+	+	kIx~	+
SiO2	SiO2	k1gMnSc1	SiO2
→	→	k?	→
Cu2S	Cu2S	k1gMnSc1	Cu2S
+	+	kIx~	+
FeSiO3	FeSiO3	k1gMnSc1	FeSiO3
+	+	kIx~	+
CO3	CO3	k1gMnSc1	CO3
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
měděného	měděný	k2eAgInSc2d1	měděný
lechu	lech	k1gInSc2	lech
na	na	k7c4	na
surovou	surový	k2eAgFnSc4d1	surová
měď	měď	k1gFnSc4	měď
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
výhradně	výhradně	k6eAd1	výhradně
provádí	provádět	k5eAaImIp3nS	provádět
dmýcháním	dmýchání	k1gNnSc7	dmýchání
v	v	k7c6	v
konvertoru	konvertor	k1gInSc6	konvertor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
pražení	pražení	k1gNnSc1	pražení
s	s	k7c7	s
dmýcháním	dmýchání	k1gNnSc7	dmýchání
nebo	nebo	k8xC	nebo
besemerace	besemerace	k1gFnSc1	besemerace
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Besemerace	Besemerace	k1gFnSc1	Besemerace
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
zavedl	zavést	k5eAaPmAgMnS	zavést
Henry	Henry	k1gMnSc1	Henry
Bessemer	Bessemer	k1gMnSc1	Bessemer
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
pochodu	pochod	k1gInSc2	pochod
pražně	pražně	k6eAd1	pražně
redukčního	redukční	k2eAgInSc2d1	redukční
a	a	k8xC	a
pochodu	pochod	k1gInSc2	pochod
pražně	pražně	k6eAd1	pražně
reakčního	reakční	k2eAgNnSc2d1	reakční
<g/>
.	.	kIx.	.
</s>
<s>
Roztavený	roztavený	k2eAgInSc1d1	roztavený
měděný	měděný	k2eAgInSc1d1	měděný
lech	lech	k1gInSc1	lech
se	se	k3xPyFc4	se
vleje	vlít	k5eAaPmIp3nS	vlít
do	do	k7c2	do
konvertoru	konvertor	k1gInSc2	konvertor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zásaditou	zásaditý	k2eAgFnSc4d1	zásaditá
nebo	nebo	k8xC	nebo
kyselou	kyselý	k2eAgFnSc4d1	kyselá
vyzdívku	vyzdívka	k1gFnSc4	vyzdívka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
zda	zda	k8xS	zda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ruda	ruda	k1gFnSc1	ruda
zásadité	zásaditý	k2eAgFnSc2d1	zásaditá
nebo	nebo	k8xC	nebo
kyselé	kyselý	k2eAgFnSc2d1	kyselá
přísady	přísada	k1gFnSc2	přísada
<g/>
,	,	kIx,	,
a	a	k8xC	a
vhání	vhánět	k5eAaImIp3nS	vhánět
se	se	k3xPyFc4	se
stlačený	stlačený	k2eAgInSc1d1	stlačený
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgInSc2d1	železnatý
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tak	tak	k6eAd1	tak
strusku	struska	k1gFnSc4	struska
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
probíhá	probíhat	k5eAaImIp3nS	probíhat
oxidace	oxidace	k1gFnSc1	oxidace
sulfidu	sulfid	k1gInSc2	sulfid
měďného	měďné	k1gNnSc2	měďné
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
energicky	energicky	k6eAd1	energicky
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
sulfidem	sulfid	k1gInSc7	sulfid
měďným	měďný	k2eAgInSc7d1	měďný
na	na	k7c4	na
kovovou	kovový	k2eAgFnSc4d1	kovová
měď	měď	k1gFnSc4	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
Cu2O	Cu2O	k1gFnSc1	Cu2O
+	+	kIx~	+
Cu2S	Cu2S	k1gFnSc1	Cu2S
→	→	k?	→
6	[number]	k4	6
Cu	Cu	k1gFnPc2	Cu
+	+	kIx~	+
SO2Oxidické	SO2Oxidický	k2eAgFnPc1d1	SO2Oxidický
měďnaté	měďnatý	k2eAgFnPc1d1	měďnatá
rudy	ruda	k1gFnPc1	ruda
lze	lze	k6eAd1	lze
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
na	na	k7c4	na
kov	kov	k1gInSc4	kov
přímou	přímý	k2eAgFnSc7d1	přímá
redukcí	redukce	k1gFnSc7	redukce
koksem	koks	k1gInSc7	koks
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
k	k	k7c3	k
sulfidickým	sulfidický	k2eAgFnPc3d1	sulfidická
rudám	ruda	k1gFnPc3	ruda
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgFnPc1	tento
rudy	ruda	k1gFnPc1	ruda
působí	působit	k5eAaImIp3nP	působit
oxidačně	oxidačně	k6eAd1	oxidačně
a	a	k8xC	a
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
tak	tak	k9	tak
redukci	redukce	k1gFnSc4	redukce
sulfidu	sulfid	k1gInSc2	sulfid
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
na	na	k7c4	na
kov	kov	k1gInSc4	kov
při	při	k7c6	při
vhodně	vhodně	k6eAd1	vhodně
zvoleném	zvolený	k2eAgInSc6d1	zvolený
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
Cu2O	Cu2O	k1gFnSc1	Cu2O
+	+	kIx~	+
Cu2S	Cu2S	k1gFnSc1	Cu2S
→	→	k?	→
6	[number]	k4	6
Cu	Cu	k1gFnSc1	Cu
+	+	kIx~	+
SO2Surová	SO2Surový	k2eAgFnSc1d1	SO2Surový
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nS	čistit
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
Anodou	anoda	k1gFnSc7	anoda
je	být	k5eAaImIp3nS	být
surová	surový	k2eAgFnSc1d1	surová
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
elektrolyt	elektrolyt	k1gInSc1	elektrolyt
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
kyselý	kyselý	k2eAgInSc4d1	kyselý
roztok	roztok	k1gInSc4	roztok
síranu	síran	k1gInSc2	síran
měďnatého	měďnatý	k2eAgMnSc2d1	měďnatý
CuSO4	CuSO4	k1gMnSc2	CuSO4
a	a	k8xC	a
katodu	katoda	k1gFnSc4	katoda
tvoří	tvořit	k5eAaImIp3nS	tvořit
čistá	čistý	k2eAgFnSc1d1	čistá
měď	měď	k1gFnSc1	měď
<g/>
.	.	kIx.	.
</s>
<s>
Nečistoty	nečistota	k1gFnPc1	nečistota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
anody	anoda	k1gFnSc2	anoda
jako	jako	k8xS	jako
anodické	anodický	k2eAgInPc1d1	anodický
kaly	kal	k1gInPc1	kal
jsou	být	k5eAaImIp3nP	být
cenným	cenný	k2eAgInSc7d1	cenný
zdrojem	zdroj	k1gInSc7	zdroj
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
nevyužívá	využívat	k5eNaImIp3nS	využívat
jen	jen	k9	jen
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
45	[number]	k4	45
<g/>
%	%	kIx~	%
také	také	k9	také
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
bronz	bronz	k1gInSc4	bronz
nebo	nebo	k8xC	nebo
mosaz	mosaz	k1gFnSc4	mosaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Čistý	čistý	k2eAgInSc1d1	čistý
kov	kov	k1gInSc1	kov
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
měď	měď	k1gFnSc1	měď
nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc2	vzduch
působením	působení	k1gNnSc7	působení
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
rychle	rychle	k6eAd1	rychle
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
tenkou	tenký	k2eAgFnSc7d1	tenká
souvislou	souvislý	k2eAgFnSc7d1	souvislá
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
nazelenalého	nazelenalý	k2eAgInSc2d1	nazelenalý
zásaditého	zásaditý	k2eAgInSc2d1	zásaditý
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
(	(	kIx(	(
<g/>
CuCO3	CuCO3	k1gFnSc1	CuCO3
.	.	kIx.	.
</s>
<s>
Cu	Cu	k?	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
měděnka	měděnka	k1gFnSc1	měděnka
<g/>
)	)	kIx)	)
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
účinně	účinně	k6eAd1	účinně
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
další	další	k2eAgFnSc3d1	další
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
znečištěnému	znečištěný	k2eAgNnSc3d1	znečištěné
ovzduší	ovzduší	k1gNnSc3	ovzduší
však	však	k8xC	však
chemická	chemický	k2eAgFnSc1d1	chemická
reakce	reakce	k1gFnSc1	reakce
zabarví	zabarvit	k5eAaPmIp3nS	zabarvit
měď	měď	k1gFnSc4	měď
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
historickou	historický	k2eAgFnSc4d1	historická
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
patinu	patina	k1gFnSc4	patina
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
ještě	ještě	k6eAd1	ještě
potřít	potřít	k5eAaPmF	potřít
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
chemikálií	chemikálie	k1gFnSc7	chemikálie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
čpavek	čpavek	k1gInSc1	čpavek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
střešní	střešní	k2eAgFnPc1d1	střešní
krytiny	krytina	k1gFnPc1	krytina
–	–	k?	–
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
především	především	k9	především
pro	pro	k7c4	pro
pokrývání	pokrývání	k1gNnSc4	pokrývání
střech	střecha	k1gFnPc2	střecha
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
historických	historický	k2eAgFnPc2d1	historická
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
</s>
</p>
<p>
<s>
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
odolných	odolný	k2eAgInPc2d1	odolný
okapů	okap	k1gInPc2	okap
a	a	k8xC	a
střešních	střešní	k2eAgInPc2d1	střešní
doplňků	doplněk	k1gInPc2	doplněk
</s>
</p>
<p>
<s>
trubic	trubice	k1gFnPc2	trubice
pro	pro	k7c4	pro
rozvody	rozvod	k1gInPc4	rozvod
technických	technický	k2eAgInPc2d1	technický
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
acetylenu	acetylen	k1gInSc2	acetylen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
acetylid	acetylida	k1gFnPc2	acetylida
a	a	k8xC	a
materiál	materiál	k1gInSc1	materiál
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
koroduje	korodovat	k5eAaImIp3nS	korodovat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výrobu	výroba	k1gFnSc4	výroba
mincí2	mincí2	k4	mincí2
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
elektrických	elektrický	k2eAgInPc2d1	elektrický
vodičů	vodič	k1gInPc2	vodič
jak	jak	k8xC	jak
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgFnPc4d1	průmyslová
aplikace	aplikace	k1gFnPc4	aplikace
(	(	kIx(	(
<g/>
elektromotory	elektromotor	k1gInPc4	elektromotor
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgInPc4d1	elektrický
generátory	generátor	k1gInPc4	generátor
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
tak	tak	k9	tak
pro	pro	k7c4	pro
rozvody	rozvod	k1gInPc4	rozvod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů3	obvodů3	k4	obvodů3
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgFnSc1d1	vynikající
tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
kotlů	kotel	k1gInPc2	kotel
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
a	a	k8xC	a
bezeztrátový	bezeztrátový	k2eAgInSc4d1	bezeztrátový
přenos	přenos	k1gInSc4	přenos
tepla	teplo	k1gNnSc2	teplo
</s>
</p>
<p>
<s>
chladičů	chladič	k1gInPc2	chladič
např.	např.	kA	např.
v	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
<g/>
,	,	kIx,	,
automobilech	automobil	k1gInPc6	automobil
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
zařízeních	zařízení	k1gNnPc6	zařízení
</s>
</p>
<p>
<s>
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
nádobíKlempířství	nádobíKlempířství	k1gNnSc2	nádobíKlempířství
–	–	k?	–
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
měď	měď	k1gFnSc4	měď
pájet	pájet	k5eAaImF	pájet
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
letovat	letovat	k5eAaImF	letovat
<g/>
)	)	kIx)	)
cínem	cín	k1gInSc7	cín
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
budoucí	budoucí	k2eAgInSc4d1	budoucí
spoj	spoj	k1gInSc4	spoj
připravit	připravit	k5eAaPmF	připravit
potřením	potření	k1gNnSc7	potření
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
<g/>
,	,	kIx,	,
převařené	převařený	k2eAgFnSc2d1	převařená
zinkem	zinek	k1gInSc7	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
nádobky	nádobka	k1gFnSc2	nádobka
dáme	dát	k5eAaPmIp1nP	dát
kyselinu	kyselina	k1gFnSc4	kyselina
a	a	k8xC	a
vkládáme	vkládat	k5eAaImIp1nP	vkládat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zinek	zinek	k1gInSc1	zinek
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
až	až	k9	až
přestane	přestat	k5eAaPmIp3nS	přestat
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Získáme	získat	k5eAaPmIp1nP	získat
tak	tak	k9	tak
chlorid	chlorid	k1gInSc4	chlorid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nemáme	mít	k5eNaImIp1nP	mít
<g/>
-li	i	k?	-li
zinek	zinek	k1gInSc4	zinek
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
použít	použít	k5eAaPmF	použít
i	i	k8xC	i
kousky	kousek	k1gInPc4	kousek
pozinkovaného	pozinkovaný	k2eAgInSc2d1	pozinkovaný
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
to	ten	k3xDgNnSc1	ten
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
<g/>
:	:	kIx,	:
Nahrazování	nahrazování	k1gNnSc1	nahrazování
mědi	měď	k1gFnSc2	měď
hliníkem	hliník	k1gInSc7	hliník
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
zavedli	zavést	k5eAaPmAgMnP	zavést
Němci	Němec	k1gMnPc1	Němec
za	za	k7c4	za
2.	[number]	k4	2.
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
neměli	mít	k5eNaImAgMnP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
mědi	měď	k1gFnSc3	měď
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgInPc1d1	pracovní
vlaky	vlak	k1gInPc1	vlak
jezdily	jezdit	k5eAaImAgInP	jezdit
od	od	k7c2	od
nádraží	nádraží	k1gNnSc2	nádraží
k	k	k7c3	k
nádraží	nádraží	k1gNnSc3	nádraží
a	a	k8xC	a
všude	všude	k6eAd1	všude
sbíraly	sbírat	k5eAaImAgInP	sbírat
měděné	měděný	k2eAgInPc1d1	měděný
dráty	drát	k1gInPc1	drát
a	a	k8xC	a
vyměňovaly	vyměňovat	k5eAaImAgInP	vyměňovat
je	on	k3xPp3gFnPc4	on
za	za	k7c4	za
hliníkové	hliníkový	k2eAgInPc4d1	hliníkový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
dráty	drát	k1gInPc7	drát
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
i	i	k8xC	i
měděné	měděný	k2eAgFnSc2d1	měděná
<g/>
,	,	kIx,	,
měděné	měděný	k2eAgFnSc2d1	měděná
však	však	k9	však
byly	být	k5eAaImAgInP	být
mnohem	mnohem	k6eAd1	mnohem
dražší	drahý	k2eAgInPc1d2	dražší
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lidé	člověk	k1gMnPc1	člověk
i	i	k9	i
podniky	podnik	k1gInPc1	podnik
používaly	používat	k5eAaImAgInP	používat
nadále	nadále	k6eAd1	nadále
dráty	drát	k1gInPc1	drát
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Měď	měď	k1gFnSc4	měď
museli	muset	k5eAaImAgMnP	muset
komunisté	komunista	k1gMnPc1	komunista
kupovat	kupovat	k5eAaImF	kupovat
za	za	k7c7	za
dolary	dolar	k1gInPc7	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bronz	bronz	k1gInSc1	bronz
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Patrně	patrně	k6eAd1	patrně
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
slitinou	slitina	k1gFnSc7	slitina
mědi	měď	k1gFnSc2	měď
je	být	k5eAaImIp3nS	být
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
významu	význam	k1gInSc6	význam
hovoří	hovořit	k5eAaImIp3nS	hovořit
již	již	k9	již
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
historická	historický	k2eAgFnSc1d1	historická
epocha	epocha	k1gFnSc1	epocha
vývoje	vývoj	k1gInSc2	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
<g/>
,	,	kIx,	,
sportovci	sportovec	k1gMnPc1	sportovec
za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
umístění	umístění	k1gNnSc4	umístění
dostávají	dostávat	k5eAaImIp3nP	dostávat
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
bronzem	bronz	k1gInSc7	bronz
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
bronz	bronz	k1gInSc4	bronz
však	však	k9	však
rozumíme	rozumět	k5eAaImIp1nP	rozumět
slitinu	slitina	k1gFnSc4	slitina
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
jakýmkoliv	jakýkoliv	k3yIgInSc7	jakýkoliv
prvkem	prvek	k1gInSc7	prvek
mimo	mimo	k6eAd1	mimo
zinku	zinek	k1gInSc2	zinek
(	(	kIx(	(
<g/>
taková	takový	k3xDgFnSc1	takový
slitina	slitina	k1gFnSc1	slitina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mosaz	mosaz	k1gFnSc1	mosaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
cínu	cín	k1gInSc2	cín
do	do	k7c2	do
kovové	kovový	k2eAgFnSc2d1	kovová
mědi	měď	k1gFnSc2	měď
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
její	její	k3xOp3gInSc1	její
hlavní	hlavní	k2eAgInSc1d1	hlavní
nedostatek	nedostatek	k1gInSc1	nedostatek
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
prakticky	prakticky	k6eAd1	prakticky
použitelných	použitelný	k2eAgInPc2d1	použitelný
nástrojů	nástroj	k1gInPc2	nástroj
–	–	k?	–
malou	malý	k2eAgFnSc4d1	malá
tvrdost	tvrdost	k1gFnSc4	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zachována	zachován	k2eAgFnSc1d1	zachována
vysoká	vysoký	k2eAgFnSc1d1	vysoká
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
snadná	snadný	k2eAgFnSc1d1	snadná
opracovatelnost	opracovatelnost	k1gFnSc1	opracovatelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc6d1	bronzová
sloužil	sloužit	k5eAaImAgMnS	sloužit
tento	tento	k3xDgInSc4	tento
kov	kov	k1gInSc4	kov
jak	jak	k8xS	jak
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
zhotovování	zhotovování	k1gNnSc4	zhotovování
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
řemeslnou	řemeslný	k2eAgFnSc4d1	řemeslná
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc4	užití
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
i	i	k8xC	i
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
dokonce	dokonce	k9	dokonce
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
na	na	k7c4	na
daleko	daleko	k6eAd1	daleko
obtížněji	obtížně	k6eAd2	obtížně
vyrobitelné	vyrobitelný	k2eAgNnSc1d1	vyrobitelné
železo	železo	k1gNnSc1	železo
nedošlo	dojít	k5eNaPmAgNnS	dojít
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnPc4d2	lepší
vlastnosti	vlastnost	k1gFnPc4	vlastnost
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
snadno	snadno	k6eAd1	snadno
těžitelných	těžitelný	k2eAgFnPc2d1	těžitelná
cínových	cínový	k2eAgFnPc2d1	cínová
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cínového	cínový	k2eAgInSc2d1	cínový
bronzu	bronz	k1gInSc2	bronz
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
součásti	součást	k1gFnSc3	součást
spínačů	spínač	k1gMnPc2	spínač
<g/>
,	,	kIx,	,
sběrné	sběrný	k2eAgInPc1d1	sběrný
kroužky	kroužek	k1gInPc1	kroužek
<g/>
,	,	kIx,	,
kontaktní	kontaktní	k2eAgInPc1d1	kontaktní
segmenty	segment	k1gInPc1	segment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
bronz	bronz	k1gInSc4	bronz
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
stovky	stovka	k1gFnPc1	stovka
slitin	slitina	k1gFnPc2	slitina
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnohé	mnohé	k1gNnSc1	mnohé
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kromě	kromě	k7c2	kromě
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
řadu	řad	k1gInSc2	řad
dalších	další	k2eAgInPc2d1	další
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
nikl	nikl	k1gInSc1	nikl
(	(	kIx(	(
<g/>
dělovina	dělovina	k1gFnSc1	dělovina
<g/>
,	,	kIx,	,
odporové	odporový	k2eAgInPc1d1	odporový
vodiče	vodič	k1gInPc1	vodič
<g/>
,	,	kIx,	,
termoelektrické	termoelektrický	k2eAgInPc1d1	termoelektrický
články	článek	k1gInPc1	článek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
beryllium	beryllium	k1gNnSc1	beryllium
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
(	(	kIx(	(
<g/>
součástky	součástka	k1gFnPc1	součástka
odolné	odolný	k2eAgFnPc1d1	odolná
vyšším	vysoký	k2eAgNnSc7d2	vyšší
teplotám	teplota	k1gFnPc3	teplota
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
i	i	k9	i
fosfor	fosfor	k1gInSc4	fosfor
a	a	k8xC	a
křemík	křemík	k1gInSc4	křemík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
bronzů	bronz	k1gInPc2	bronz
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
především	především	k9	především
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
vysokou	vysoký	k2eAgFnSc7d1	vysoká
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
železa	železo	k1gNnSc2	železo
nebo	nebo	k8xC	nebo
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kovové	kovový	k2eAgFnPc1d1	kovová
součástky	součástka	k1gFnPc1	součástka
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
tlaky	tlak	k1gInPc7	tlak
v	v	k7c6	v
agresivním	agresivní	k2eAgNnSc6d1	agresivní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kluzná	kluzný	k2eAgNnPc1d1	kluzné
ložiska	ložisko	k1gNnPc1	ložisko
<g/>
,	,	kIx,	,
pružinová	pružinový	k2eAgNnPc1d1	pružinové
pera	pero	k1gNnPc1	pero
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
součásti	součást	k1gFnPc1	součást
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odolávají	odolávat	k5eAaImIp3nP	odolávat
působení	působení	k1gNnSc4	působení
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
bronz	bronz	k1gInSc1	bronz
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
pamětních	pamětní	k2eAgFnPc2d1	pamětní
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
medailí	medaile	k1gFnPc2	medaile
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mosaz	mosaz	k1gFnSc4	mosaz
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
se	s	k7c7	s
zinkem	zinek	k1gInSc7	zinek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mosaz	mosaz	k1gFnSc1	mosaz
<g/>
.	.	kIx.	.
</s>
<s>
Mosaz	mosaz	k1gFnSc1	mosaz
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
optimálně	optimálně	k6eAd1	optimálně
32	[number]	k4	32
<g/>
%	%	kIx~	%
zinku	zinek	k1gInSc2	zinek
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
42	[number]	k4	42
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
stovky	stovka	k1gFnPc1	stovka
různých	různý	k2eAgFnPc2d1	různá
mosazí	mosaz	k1gFnPc2	mosaz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
přesné	přesný	k2eAgNnSc1d1	přesné
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
normami	norma	k1gFnPc7	norma
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
(	(	kIx(	(
<g/>
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
mechanická	mechanický	k2eAgFnSc1d1	mechanická
opracovatelnost	opracovatelnost	k1gFnSc1	opracovatelnost
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
zpracovatelnost	zpracovatelnost	k1gFnSc1	zpracovatelnost
litím	lití	k1gNnSc7	lití
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc4	možnost
odlévání	odlévání	k1gNnSc2	odlévání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
mosaz	mosaz	k1gFnSc1	mosaz
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
měkká	měkký	k2eAgFnSc1d1	měkká
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
jasně	jasně	k6eAd1	jasně
zlatavou	zlatavý	k2eAgFnSc7d1	zlatavá
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc7d1	nízká
chemickou	chemický	k2eAgFnSc7d1	chemická
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
kyselinám	kyselina	k1gFnPc3	kyselina
a	a	k8xC	a
louhům	louh	k1gInPc3	louh
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
vlivů	vliv	k1gInPc2	vliv
je	být	k5eAaImIp3nS	být
však	však	k9	však
mosaz	mosaz	k1gFnSc1	mosaz
značně	značně	k6eAd1	značně
odolná	odolný	k2eAgFnSc1d1	odolná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
různých	různý	k2eAgInPc2d1	různý
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
součásti	součást	k1gFnSc2	součást
pro	pro	k7c4	pro
vybavení	vybavení	k1gNnSc4	vybavení
koupelen	koupelna	k1gFnPc2	koupelna
a	a	k8xC	a
drobné	drobný	k2eAgInPc4d1	drobný
bytové	bytový	k2eAgInPc4d1	bytový
doplňky	doplněk	k1gInPc4	doplněk
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
bižuterie	bižuterie	k1gFnSc2	bižuterie
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
kočičí	kočičí	k2eAgNnSc1d1	kočičí
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
vizuální	vizuální	k2eAgFnSc2d1	vizuální
podobnosti	podobnost	k1gFnSc2	podobnost
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
úmyslnou	úmyslný	k2eAgFnSc4d1	úmyslná
záměnu	záměna	k1gFnSc4	záměna
a	a	k8xC	a
podvedení	podvedení	k1gNnSc1	podvedení
důvěřivého	důvěřivý	k2eAgMnSc2d1	důvěřivý
zákazníka	zákazník	k1gMnSc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
předmětu	předmět	k1gInSc2	předmět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ozdobný	ozdobný	k2eAgInSc1d1	ozdobný
masivní	masivní	k2eAgInSc1d1	masivní
řetízek	řetízek	k1gInSc1	řetízek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
mosazi	mosaz	k1gFnSc2	mosaz
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pozlacena	pozlatit	k5eAaPmNgFnS	pozlatit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
testu	test	k1gInSc6	test
na	na	k7c6	na
kameni	kámen	k1gInSc6	kámen
(	(	kIx(	(
<g/>
buližník	buližník	k1gInSc1	buližník
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
metodou	metoda	k1gFnSc7	metoda
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
fluorescence	fluorescence	k1gFnSc2	fluorescence
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
předmět	předmět	k1gInSc1	předmět
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
skutečně	skutečně	k6eAd1	skutečně
zlatý	zlatý	k2eAgInSc1d1	zlatý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
pouze	pouze	k6eAd1	pouze
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
způsoby	způsob	k1gInPc7	způsob
elektrolytického	elektrolytický	k2eAgNnSc2d1	elektrolytické
vylučování	vylučování	k1gNnSc2	vylučování
mosazných	mosazný	k2eAgFnPc2d1	mosazná
vrstev	vrstva	k1gFnPc2	vrstva
na	na	k7c4	na
kovový	kovový	k2eAgInSc4d1	kovový
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
tohoto	tento	k3xDgNnSc2	tento
elektrolytického	elektrolytický	k2eAgNnSc2d1	elektrolytické
mosazení	mosazení	k1gNnSc2	mosazení
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
protikorozní	protikorozní	k2eAgFnSc3d1	protikorozní
ochraně	ochrana	k1gFnSc3	ochrana
především	především	k6eAd1	především
železných	železný	k2eAgInPc2d1	železný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Klenotnické	klenotnický	k2eAgFnSc2d1	klenotnická
<g/>
,	,	kIx,	,
dentální	dentální	k2eAgFnSc2d1	dentální
a	a	k8xC	a
mincovní	mincovní	k2eAgFnSc2d1	mincovní
slitiny	slitina	k1gFnSc2	slitina
===	===	k?	===
</s>
</p>
<p>
<s>
Zlaté	zlatá	k1gFnPc1	zlatá
klenotnické	klenotnický	k2eAgFnSc2d1	klenotnická
slitiny	slitina	k1gFnSc2	slitina
obsahuji	obsahovat	k5eAaImIp1nS	obsahovat
kromě	kromě	k7c2	kromě
zlata	zlato	k1gNnSc2	zlato
nejčastěji	často	k6eAd3	často
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
nalézt	nalézt	k5eAaPmF	nalézt
ale	ale	k8xC	ale
i	i	k9	i
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc1	palladium
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zlatých	zlatý	k2eAgInPc2d1	zlatý
šperků	šperk	k1gInPc2	šperk
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
mechanické	mechanický	k2eAgFnSc2d1	mechanická
odolnost	odolnost	k1gFnSc4	odolnost
čistého	čistý	k2eAgNnSc2d1	čisté
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
měkkost	měkkost	k1gFnSc1	měkkost
<g/>
,	,	kIx,	,
snadný	snadný	k2eAgInSc1d1	snadný
otěr	otěr	k1gInSc1	otěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přídavky	přídavka	k1gFnPc1	přídavka
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
kovů	kov	k1gInPc2	kov
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tvrdost	tvrdost	k1gFnSc4	tvrdost
slitiny	slitina	k1gFnSc2	slitina
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
estetický	estetický	k2eAgInSc1d1	estetický
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
ve	v	k7c6	v
zlatých	zlatý	k2eAgInPc6d1	zlatý
klenotnických	klenotnický	k2eAgInPc6d1	klenotnický
materiálech	materiál	k1gInPc6	materiál
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0	[number]	k4	0
–	–	k?	–
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
podle	podle	k7c2	podle
jejího	její	k3xOp3gInSc2	její
obsahu	obsah	k1gInSc2	obsah
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
docílit	docílit	k5eAaPmF	docílit
i	i	k9	i
zvoleného	zvolený	k2eAgInSc2d1	zvolený
barevného	barevný	k2eAgInSc2d1	barevný
odstínu	odstín	k1gInSc2	odstín
slitiny	slitina	k1gFnSc2	slitina
od	od	k7c2	od
zářivě	zářivě	k6eAd1	zářivě
žluté	žlutý	k2eAgFnSc2d1	žlutá
až	až	k9	až
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
šperky	šperk	k1gInPc1	šperk
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
stříbra	stříbro	k1gNnSc2	stříbro
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsah	obsah	k1gInSc1	obsah
mědi	měď	k1gFnSc2	měď
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
–	–	k?	–
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
mechanická	mechanický	k2eAgFnSc1d1	mechanická
odolnost	odolnost	k1gFnSc1	odolnost
slitiny	slitina	k1gFnSc2	slitina
oproti	oproti	k7c3	oproti
čistému	čistý	k2eAgNnSc3d1	čisté
stříbru	stříbro	k1gNnSc3	stříbro
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
vyšší	vysoký	k2eAgFnSc4d2	vyšší
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
atmosférickými	atmosférický	k2eAgInPc7d1	atmosférický
plyny	plyn	k1gInPc7	plyn
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
,	,	kIx,	,
sulfan	sulfan	k1gInSc1	sulfan
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dentální	dentální	k2eAgFnPc4d1	dentální
slitiny	slitina	k1gFnPc4	slitina
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zubních	zubní	k2eAgFnPc2d1	zubní
náhrad	náhrada	k1gFnPc2	náhrada
musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
především	především	k9	především
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc4	nezávadnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
silně	silně	k6eAd1	silně
chemicky	chemicky	k6eAd1	chemicky
agresivním	agresivní	k2eAgNnSc6d1	agresivní
prostředí	prostředí	k1gNnSc6	prostředí
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
užívá	užívat	k5eAaImIp3nS	užívat
především	především	k9	především
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc1	palladium
<g/>
,	,	kIx,	,
platina	platina	k1gFnSc1	platina
nebo	nebo	k8xC	nebo
iridium	iridium	k1gNnSc1	iridium
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
kovy	kov	k1gInPc1	kov
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
antimon	antimon	k1gInSc1	antimon
nebo	nebo	k8xC	nebo
indium	indium	k1gNnSc4	indium
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
účel	účel	k1gInSc4	účel
upravit	upravit	k5eAaPmF	upravit
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
slitiny	slitina	k1gFnSc2	slitina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
opracovatelnost	opracovatelnost	k1gFnSc4	opracovatelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
typem	typ	k1gInSc7	typ
dentální	dentální	k2eAgFnSc2d1	dentální
slitiny	slitina	k1gFnSc2	slitina
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
pro	pro	k7c4	pro
zubní	zubní	k2eAgFnPc4d1	zubní
výplně	výplň	k1gFnPc4	výplň
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dentální	dentální	k2eAgFnPc4d1	dentální
amalgámy	amalgáma	k1gFnPc4	amalgáma
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
tvořeny	tvořit	k5eAaImNgFnP	tvořit
slitinou	slitina	k1gFnSc7	slitina
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
rozmělněny	rozmělněn	k2eAgFnPc1d1	rozmělněna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vlastním	vlastní	k2eAgInSc7d1	vlastní
úkonem	úkon	k1gInSc7	úkon
v	v	k7c6	v
zubní	zubní	k2eAgFnSc6d1	zubní
ordinaci	ordinace	k1gFnSc6	ordinace
se	se	k3xPyFc4	se
k	k	k7c3	k
definovanému	definovaný	k2eAgNnSc3d1	definované
množství	množství	k1gNnSc3	množství
této	tento	k3xDgFnSc2	tento
směsi	směs	k1gFnSc2	směs
přidá	přidat	k5eAaPmIp3nS	přidat
elementární	elementární	k2eAgFnSc1d1	elementární
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
velmi	velmi	k6eAd1	velmi
pevnou	pevný	k2eAgFnSc4d1	pevná
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
odolnou	odolný	k2eAgFnSc4d1	odolná
slitinu	slitina	k1gFnSc4	slitina
–	–	k?	–
amalgám	amalgáma	k1gFnPc2	amalgáma
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zubní	zubní	k2eAgMnSc1d1	zubní
lékař	lékař	k1gMnSc1	lékař
vyplnil	vyplnit	k5eAaPmAgMnS	vyplnit
dutiny	dutina	k1gFnPc4	dutina
v	v	k7c6	v
zubu	zub	k1gInSc6	zub
amalgámem	amalgám	k1gInSc7	amalgám
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
po	po	k7c6	po
smíchání	smíchání	k1gNnSc6	smíchání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
ještě	ještě	k6eAd1	ještě
tvárná	tvárný	k2eAgFnSc1d1	tvárná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
mincovní	mincovní	k2eAgInPc4d1	mincovní
kovy	kov	k1gInPc4	kov
se	se	k3xPyFc4	se
slitiny	slitina	k1gFnSc2	slitina
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
niklem	nikl	k1gInSc7	nikl
a	a	k8xC	a
zinkem	zinek	k1gInSc7	zinek
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
mincí	mince	k1gFnPc2	mince
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
nominální	nominální	k2eAgFnSc7d1	nominální
hodnotou	hodnota	k1gFnSc7	hodnota
právě	právě	k9	právě
pro	pro	k7c4	pro
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
čisté	čistý	k2eAgFnSc2d1	čistá
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Přídavky	přídavek	k1gInPc1	přídavek
mědi	měď	k1gFnSc2	měď
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
účel	účel	k1gInSc4	účel
upravit	upravit	k5eAaPmF	upravit
zbarvení	zbarvení	k1gNnSc4	zbarvení
slitiny	slitina	k1gFnSc2	slitina
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
až	až	k8xS	až
červena	červeno	k1gNnSc2	červeno
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
korozní	korozní	k2eAgFnSc4d1	korozní
odolnost	odolnost	k1gFnSc4	odolnost
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Cu1	Cu1	k1gFnSc2	Cu1
<g/>
+	+	kIx~	+
a	a	k8xC	a
Cu2	Cu2	k1gMnSc1	Cu2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
Cu3	Cu3	k1gFnSc1	Cu3
<g/>
+	+	kIx~	+
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
nestabilních	stabilní	k2eNgFnPc6d1	nestabilní
sloučeninách	sloučenina	k1gFnPc6	sloučenina
Cu4	Cu4	k1gFnSc2	Cu4
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Nejstálejší	stálý	k2eAgFnPc1d3	nejstálejší
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Cu2	Cu2	k1gFnPc2	Cu2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
modrou	modrý	k2eAgFnSc4d1	modrá
nebo	nebo	k8xC	nebo
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
Cu1	Cu1	k1gFnPc2	Cu1
<g/>
+	+	kIx~	+
svým	svůj	k3xOyFgNnSc7	svůj
chemickým	chemický	k2eAgNnSc7d1	chemické
chováním	chování	k1gNnSc7	chování
připomínají	připomínat	k5eAaImIp3nP	připomínat
soli	sůl	k1gFnPc4	sůl
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
zatímco	zatímco	k8xS	zatímco
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Cu3	Cu3	k1gFnSc7	Cu3
<g/>
+	+	kIx~	+
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
měďné	měďné	k2eAgFnPc2d1	měďné
Cu1	Cu1	k1gFnPc2	Cu1
<g/>
+	+	kIx~	+
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc4	látka
často	často	k6eAd1	často
špatně	špatně	k6eAd1	špatně
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
nerozpustné	rozpustný	k2eNgNnSc1d1	nerozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
v	v	k7c6	v
hydratovaném	hydratovaný	k2eAgMnSc6d1	hydratovaný
barevné	barevný	k2eAgFnPc4d1	barevná
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
měďný	měďný	k2eAgInSc1d1	měďný
Cu2O	Cu2O	k1gFnSc4	Cu2O
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
až	až	k8xS	až
černohnědá	černohnědý	k2eAgFnSc1d1	černohnědá
jemně	jemně	k6eAd1	jemně
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
však	však	k9	však
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
roztoku	roztok	k1gInSc2	roztok
halogenovodíkové	halogenovodíkový	k2eAgFnSc2d1	halogenovodíkový
kyseliny	kyselina	k1gFnSc2	kyselina
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
kuprit	kuprit	k1gInSc1	kuprit
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
zředěných	zředěný	k2eAgFnPc2d1	zředěná
kyslíkatých	kyslíkatý	k2eAgFnPc2d1	kyslíkatá
kyselin	kyselina	k1gFnPc2	kyselina
probíhá	probíhat	k5eAaImIp3nS	probíhat
autooxidace	autooxidace	k1gFnSc1	autooxidace
oxidu	oxid	k1gInSc2	oxid
měďného	měďné	k1gNnSc2	měďné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
měďnatá	měďnatý	k2eAgFnSc1d1	měďnatá
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
měď	měď	k1gFnSc1	měď
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
měďný	měďný	k1gMnSc1	měďný
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
barvení	barvení	k1gNnSc6	barvení
ve	v	k7c6	v
sklářství	sklářství	k1gNnSc6	sklářství
a	a	k8xC	a
keramice	keramika	k1gFnSc6	keramika
na	na	k7c4	na
červeno	červeno	k1gNnSc4	červeno
<g/>
,	,	kIx,	,
do	do	k7c2	do
barev	barva	k1gFnPc2	barva
k	k	k7c3	k
natírání	natírání	k1gNnSc3	natírání
dna	dno	k1gNnSc2	dno
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
měďný	měďný	k1gMnSc1	měďný
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
přidáním	přidání	k1gNnSc7	přidání
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
hroznového	hroznový	k2eAgInSc2d1	hroznový
cukru	cukr	k1gInSc2	cukr
k	k	k7c3	k
roztoku	roztok	k1gInSc3	roztok
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
nebo	nebo	k8xC	nebo
Fehlingovu	Fehlingův	k2eAgInSc3d1	Fehlingův
roztoku	roztok	k1gInSc3	roztok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
měďný	měďný	k1gMnSc1	měďný
CuCl	cucnout	k5eAaPmAgMnS	cucnout
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
vzduchu	vzduch	k1gInSc6	vzduch
nabíhá	nabíhat	k5eAaImIp3nS	nabíhat
rychle	rychle	k6eAd1	rychle
do	do	k7c2	do
zelena	zeleno	k1gNnSc2	zeleno
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
měďný	měďný	k1gMnSc1	měďný
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
komplexní	komplexní	k2eAgMnSc1d1	komplexní
<g/>
,	,	kIx,	,
adiční	adiční	k2eAgFnPc1d1	adiční
i	i	k8xC	i
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
měďný	měďný	k1gMnSc1	měďný
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zahříváním	zahřívání	k1gNnSc7	zahřívání
měděných	měděný	k2eAgFnPc2d1	měděná
hoblin	hoblina	k1gFnPc2	hoblina
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c4	za
přídavku	přídavka	k1gFnSc4	přídavka
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
chlorečnanu	chlorečnan	k1gInSc2	chlorečnan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
taky	taky	k6eAd1	taky
připravit	připravit	k5eAaPmF	připravit
redukcí	redukce	k1gFnSc7	redukce
chloridu	chlorid	k1gInSc2	chlorid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
kyselinou	kyselina	k1gFnSc7	kyselina
siřičitou	siřičitý	k2eAgFnSc7d1	siřičitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bromid	bromid	k1gInSc1	bromid
měďný	měďný	k2eAgInSc4d1	měďný
CuBr	CuBr	k1gInSc4	CuBr
je	být	k5eAaImIp3nS	být
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
adiční	adiční	k2eAgFnPc1d1	adiční
a	a	k8xC	a
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
bromidu	bromid	k1gInSc2	bromid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
měďný	měďný	k1gMnSc1	měďný
CuI	CuI	k1gMnSc1	CuI
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přimíšením	přimíšení	k1gNnSc7	přimíšení
jodu	jod	k1gInSc2	jod
barví	barvit	k5eAaImIp3nS	barvit
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
až	až	k8xS	až
černa	černo	k1gNnSc2	černo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
adiční	adiční	k2eAgFnPc1d1	adiční
a	a	k8xC	a
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zahříváním	zahřívání	k1gNnSc7	zahřívání
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
jodem	jod	k1gInSc7	jod
a	a	k8xC	a
horkou	horký	k2eAgFnSc7d1	horká
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
jodovodíkovou	jodovodíkový	k2eAgFnSc7d1	jodovodíková
nebo	nebo	k8xC	nebo
smíšením	smíšení	k1gNnSc7	smíšení
alkalického	alkalický	k2eAgInSc2d1	alkalický
jodidu	jodid	k1gInSc2	jodid
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
přídavkem	přídavek	k1gInSc7	přídavek
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
nebo	nebo	k8xC	nebo
thiosíranu	thiosíran	k1gInSc2	thiosíran
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
měďný	měďný	k2eAgInSc1d1	měďný
CuCN	CuCN	k1gFnSc4	CuCN
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
koncentrovaných	koncentrovaný	k2eAgFnPc6d1	koncentrovaná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
amoniaku	amoniak	k1gInSc6	amoniak
a	a	k8xC	a
roztocích	roztok	k1gInPc6	roztok
amonných	amonný	k2eAgFnPc2d1	amonná
solí	sůl	k1gFnPc2	sůl
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
kyanidových	kyanidový	k2eAgMnPc2d1	kyanidový
iontů	ion	k1gInPc2	ion
s	s	k7c7	s
měďnatými	měďnatý	k2eAgInPc7d1	měďnatý
kationy	kation	k1gInPc7	kation
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
kyanid	kyanid	k1gInSc1	kyanid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
dikyan	dikyan	k1gInSc4	dikyan
a	a	k8xC	a
kyanid	kyanid	k1gInSc4	kyanid
měďný	měďný	k2eAgInSc4d1	měďný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
měďný	měďný	k2eAgInSc1d1	měďný
CuSCN	CuSCN	k1gFnSc4	CuSCN
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
kyselinách	kyselina	k1gFnPc6	kyselina
za	za	k7c2	za
chladu	chlad	k1gInSc2	chlad
se	se	k3xPyFc4	se
nerozpustný	rozpustný	k2eNgInSc4d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
rhodanidu	rhodanid	k1gInSc2	rhodanid
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
komplexní	komplexní	k2eAgFnSc2d1	komplexní
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
měďný	měďný	k1gMnSc1	měďný
dokáže	dokázat	k5eAaPmIp3nS	dokázat
adovat	adovat	k5eAaBmF	adovat
amoniak	amoniak	k1gInSc4	amoniak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
zpět	zpět	k6eAd1	zpět
snadno	snadno	k6eAd1	snadno
odštěpuje	odštěpovat	k5eAaImIp3nS	odštěpovat
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanida	k1gFnPc2	Rhodanida
měďný	měďný	k1gMnSc1	měďný
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
s	s	k7c7	s
rhodanidovými	rhodanidový	k2eAgInPc7d1	rhodanidový
aniony	anion	k1gInPc7	anion
a	a	k8xC	a
přítomnosti	přítomnost	k1gFnSc6	přítomnost
redukčního	redukční	k2eAgNnSc2d1	redukční
činidla	činidlo	k1gNnSc2	činidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Siřičitan	siřičitan	k1gInSc1	siřičitan
měďný	měďný	k2eAgInSc1d1	měďný
Cu2SO3	Cu2SO3	k1gFnSc4	Cu2SO3
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
siřičité	siřičitý	k2eAgFnSc2d1	siřičitá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
málo	málo	k6eAd1	málo
stabilní	stabilní	k2eAgFnPc1d1	stabilní
podvojné	podvojný	k2eAgFnPc1d1	podvojná
a	a	k8xC	a
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
<g/>
.	.	kIx.	.
</s>
<s>
Siřičitan	siřičitan	k1gInSc1	siřičitan
měďný	měďný	k1gMnSc1	měďný
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zaváděním	zavádění	k1gNnSc7	zavádění
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
octanu	octan	k1gInSc2	octan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
okyselený	okyselený	k2eAgInSc4d1	okyselený
kyselinou	kyselina	k1gFnSc7	kyselina
octovou	octový	k2eAgFnSc7d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďný	měďný	k1gMnSc1	měďný
Cu2S	Cu2S	k1gMnSc1	Cu2S
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
olověně	olověně	k6eAd1	olověně
šedý	šedý	k2eAgInSc1d1	šedý
krystalický	krystalický	k2eAgInSc1d1	krystalický
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
chalkosin	chalkosin	k2eAgInSc1d1	chalkosin
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďný	měďný	k2eAgInSc1d1	měďný
vede	vést	k5eAaImIp3nS	vést
dobře	dobře	k6eAd1	dobře
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hůře	zle	k6eAd2	zle
než	než	k8xS	než
sulfid	sulfid	k1gInSc1	sulfid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďný	měďný	k1gMnSc1	měďný
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďný	měďný	k2eAgInSc1d1	měďný
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
žíháním	žíhání	k1gNnSc7	žíhání
sulfid	sulfid	k1gInSc1	sulfid
měďnatého	měďnatý	k2eAgNnSc2d1	měďnaté
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
vodíku	vodík	k1gInSc2	vodík
za	za	k7c2	za
malého	malý	k2eAgNnSc2d1	malé
přidání	přidání	k1gNnSc2	přidání
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Sloučeniny	sloučenina	k1gFnPc4	sloučenina
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
Cu2	Cu2	k1gFnSc2	Cu2
<g/>
+	+	kIx~	+
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc4	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
většinou	většinou	k6eAd1	většinou
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
hydrolyticky	hydrolyticky	k6eAd1	hydrolyticky
štěpí	štěpit	k5eAaImIp3nS	štěpit
<g/>
.	.	kIx.	.
</s>
<s>
Hydratované	hydratovaný	k2eAgInPc1d1	hydratovaný
měďnaté	měďnatý	k2eAgInPc1d1	měďnatý
ionty	ion	k1gInPc1	ion
jsou	být	k5eAaImIp3nP	být
blankytně	blankytně	k6eAd1	blankytně
modré	modrý	k2eAgFnPc1d1	modrá
a	a	k8xC	a
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
jsou	být	k5eAaImIp3nP	být
měďnaté	měďnatý	k2eAgFnPc1d1	měďnatá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
často	často	k6eAd1	často
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgNnSc1	některý
i	i	k9	i
barevné	barevný	k2eAgNnSc1d1	barevné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
CuO	CuO	k1gFnSc4	CuO
je	být	k5eAaImIp3nS	být
tmavohnědý	tmavohnědý	k2eAgInSc1d1	tmavohnědý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k8xS	jako
nerosty	nerost	k1gInPc7	nerost
melaconit	melaconit	k5eAaImF	melaconit
a	a	k8xC	a
tenorit	tenorit	k1gInSc4	tenorit
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
smaltů	smalt	k1gInPc2	smalt
na	na	k7c4	na
zeleno	zeleno	k1gNnSc4	zeleno
<g/>
,	,	kIx,	,
modro	modro	k1gNnSc4	modro
nebo	nebo	k8xC	nebo
červeno	červeno	k1gNnSc4	červeno
a	a	k8xC	a
v	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
emailů	email	k1gInPc2	email
pro	pro	k7c4	pro
zdobení	zdobení	k1gNnSc4	zdobení
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
kupronových	kupronův	k2eAgInPc6d1	kupronův
článcích	článek	k1gInPc6	článek
jako	jako	k8xC	jako
depolarizátor	depolarizátor	k1gInSc4	depolarizátor
<g/>
,	,	kIx,	,
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
elementární	elementární	k2eAgFnSc6d1	elementární
analýze	analýza	k1gFnSc6	analýza
jako	jako	k8xC	jako
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
a	a	k8xC	a
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mastech	mast	k1gFnPc6	mast
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
vzniká	vznikat	k5eAaImIp3nS	vznikat
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
hydroxidu	hydroxid	k1gInSc2	hydroxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
,	,	kIx,	,
dusičnanu	dusičnan	k1gInSc2	dusičnan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
Cu	Cu	k1gMnSc7	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
sražený	sražený	k2eAgInSc4d1	sražený
hydroxid	hydroxid	k1gInSc4	hydroxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
alkalických	alkalický	k2eAgInPc6d1	alkalický
hydroxidech	hydroxid	k1gInPc6	hydroxid
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
hydroxoměďnatanů	hydroxoměďnatan	k1gInPc2	hydroxoměďnatan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
amoniaku	amoniak	k1gInSc2	amoniak
na	na	k7c4	na
tmavěmodrý	tmavěmodrý	k2eAgInSc4d1	tmavěmodrý
roztok	roztok	k1gInSc4	roztok
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yQnSc6	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
komplexní	komplexní	k2eAgFnSc4d1	komplexní
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
Schweizerovo	Schweizerův	k2eAgNnSc1d1	Schweizerův
činidlo	činidlo	k1gNnSc1	činidlo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
měďnatých	měďnatý	k2eAgFnPc2d1	měďnatá
solí	sůl	k1gFnPc2	sůl
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
alkalickým	alkalický	k2eAgInSc7d1	alkalický
hydroxidem	hydroxid	k1gInSc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
CuF2	CuF2	k1gMnSc1	CuF2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratovaném	hydratovaný	k2eAgInSc6d1	hydratovaný
stavu	stav	k1gInSc6	stav
světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
a	a	k8xC	a
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
bílý	bílý	k2eAgInSc1d1	bílý
krystalický	krystalický	k2eAgInSc1d1	krystalický
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
zásaditý	zásaditý	k2eAgInSc4d1	zásaditý
fluorid	fluorid	k1gInSc4	fluorid
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
kyseliny	kyselina	k1gFnSc2	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
nebo	nebo	k8xC	nebo
přímým	přímý	k2eAgNnSc7d1	přímé
slučováním	slučování	k1gNnSc7	slučování
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
CuCl2	CuCl2	k1gMnSc1	CuCl2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
temně	temně	k6eAd1	temně
hnědá	hnědý	k2eAgFnSc1d1	hnědá
roztékavá	roztékavý	k2eAgFnSc1d1	roztékavý
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dihydrátu	dihydrát	k1gInSc2	dihydrát
blankytně	blankytně	k6eAd1	blankytně
modrá	modrý	k2eAgFnSc1d1	modrá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stopami	stopa	k1gFnPc7	stopa
vody	voda	k1gFnSc2	voda
barví	barvit	k5eAaImIp3nP	barvit
zeleně	zeleň	k1gFnPc1	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
etanolu	etanol	k1gInSc6	etanol
<g/>
,	,	kIx,	,
acetonu	aceton	k1gInSc6	aceton
a	a	k8xC	a
pyridinu	pyridin	k1gInSc6	pyridin
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
chloridu	chlorid	k1gInSc2	chlorid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
je	být	k5eAaImIp3nS	být
temně	temně	k6eAd1	temně
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
,	,	kIx,	,
při	při	k7c6	při
zřeďování	zřeďování	k1gNnSc6	zřeďování
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
zelenou	zelená	k1gFnSc4	zelená
až	až	k9	až
do	do	k7c2	do
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentrovaných	koncentrovaný	k2eAgInPc6d1	koncentrovaný
roztocích	roztok	k1gInPc6	roztok
chloridu	chlorid	k1gInSc2	chlorid
měďnatého	měďnatý	k2eAgMnSc4d1	měďnatý
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
komplexní	komplexní	k2eAgFnPc4d1	komplexní
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podvojné	podvojný	k2eAgFnPc1d1	podvojná
i	i	k8xC	i
adiční	adiční	k2eAgFnPc1d1	adiční
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrovaný	koncentrovaný	k2eAgInSc1d1	koncentrovaný
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
dokáže	dokázat	k5eAaPmIp3nS	dokázat
adovat	adovat	k5eAaBmF	adovat
velká	velký	k2eAgNnPc4d1	velké
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
dusnatého	dusnatý	k2eAgInSc2d1	dusnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
zřeďování	zřeďování	k1gNnSc6	zřeďování
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bromid	bromid	k1gInSc1	bromid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
CuBr2	CuBr2	k1gMnSc1	CuBr2
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
lesklé	lesklý	k2eAgFnSc2d1	lesklá
černé	černá	k1gFnSc2	černá
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
hnědavě	hnědavě	k6eAd1	hnědavě
zelené	zelený	k2eAgInPc1d1	zelený
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
adiční	adiční	k2eAgFnPc4d1	adiční
<g/>
,	,	kIx,	,
podvojné	podvojný	k2eAgFnPc4d1	podvojná
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
bromovodíkové	bromovodíkový	k2eAgFnSc6d1	bromovodíkový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
CuI2	CuI2	k1gFnSc4	CuI2
vzniká	vznikat	k5eAaImIp3nS	vznikat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
přechodný	přechodný	k2eAgInSc4d1	přechodný
produkt	produkt	k1gInSc4	produkt
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
s	s	k7c7	s
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
alkalickým	alkalický	k2eAgInSc7d1	alkalický
jodidem	jodid	k1gInSc7	jodid
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
se	se	k3xPyFc4	se
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
jodidu	jodid	k1gInSc2	jodid
měďného	měďné	k1gNnSc2	měďné
a	a	k8xC	a
jodu	jod	k1gInSc2	jod
<g/>
.	.	kIx.	.
</s>
<s>
Stálejší	stálý	k2eAgFnPc1d2	stálejší
jsou	být	k5eAaImIp3nP	být
adiční	adiční	k2eAgFnPc1d1	adiční
a	a	k8xC	a
podvojné	podvojný	k2eAgFnPc1d1	podvojná
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síran	síran	k1gInSc4	síran
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
,	,	kIx,	,
krystalizující	krystalizující	k2eAgInSc4d1	krystalizující
z	z	k7c2	z
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
jako	jako	k8xC	jako
pentahydrát	pentahydrát	k1gInSc4	pentahydrát
CuSO4	CuSO4	k1gFnSc2	CuSO4
<g/>
.	.	kIx.	.
<g/>
5	[number]	k4	5
H2O	H2O	k1gFnPc2	H2O
neboli	neboli	k8xC	neboli
modrá	modrý	k2eAgFnSc1d1	modrá
skalice	skalice	k1gFnSc1	skalice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
blankytně	blankytně	k6eAd1	blankytně
modrá	modrý	k2eAgFnSc1d1	modrá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
chalkantit	chalkantit	k5eAaPmF	chalkantit
<g/>
.	.	kIx.	.
</s>
<s>
Opatrným	opatrný	k2eAgNnSc7d1	opatrné
zahříváním	zahřívání	k1gNnSc7	zahřívání
lze	lze	k6eAd1	lze
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
vodu	voda	k1gFnSc4	voda
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
bezvodá	bezvodý	k2eAgFnSc1d1	bezvodá
sůl	sůl	k1gFnSc1	sůl
CuSO4	CuSO4	k1gFnSc1	CuSO4
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
přijímá	přijímat	k5eAaImIp3nS	přijímat
vodu	voda	k1gFnSc4	voda
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
sušení	sušení	k1gNnSc3	sušení
některých	některý	k3yIgNnPc2	některý
nepolárních	polární	k2eNgNnPc2d1	nepolární
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
síran	síran	k1gInSc1	síran
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
prakticky	prakticky	k6eAd1	prakticky
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
adiční	adiční	k2eAgFnPc1d1	adiční
a	a	k8xC	a
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
dobré	dobrý	k2eAgFnSc3d1	dobrá
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
galvanických	galvanický	k2eAgFnPc2d1	Galvanická
lázní	lázeň	k1gFnPc2	lázeň
pro	pro	k7c4	pro
proudové	proudový	k2eAgNnSc4d1	proudové
poměďování	poměďování	k1gNnSc4	poměďování
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
soli	sůl	k1gFnPc1	sůl
Cu2	Cu2	k1gFnSc7	Cu2
<g/>
+	+	kIx~	+
obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
fungicidní	fungicidní	k2eAgInPc1d1	fungicidní
-	-	kIx~	-
hubí	hubit	k5eAaImIp3nP	hubit
houby	houba	k1gFnPc1	houba
a	a	k8xC	a
plísně	plíseň	k1gFnPc1	plíseň
<g/>
,	,	kIx,	,
požívají	požívat	k5eAaImIp3nP	požívat
se	se	k3xPyFc4	se
přípravky	přípravka	k1gFnSc2	přípravka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
modré	modrý	k2eAgFnSc2d1	modrá
skalice	skalice	k1gFnSc2	skalice
k	k	k7c3	k
ošetřování	ošetřování	k1gNnSc3	ošetřování
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
nebo	nebo	k8xC	nebo
osiva	osivo	k1gNnSc2	osivo
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
houbovým	houbová	k1gFnPc3	houbová
a	a	k8xC	a
plísňovým	plísňový	k2eAgFnPc3d1	plísňová
infekcím	infekce	k1gFnPc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
skalice	skalice	k1gFnSc1	skalice
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
měděných	měděný	k2eAgInPc2d1	měděný
odpadků	odpadek	k1gInPc2	odpadek
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
jako	jako	k9	jako
hexahydrát	hexahydrát	k1gInSc1	hexahydrát
Cu	Cu	k1gMnPc2	Cu
<g/>
(	(	kIx(	(
<g/>
NO3	NO3	k1gMnSc1	NO3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
6	[number]	k4	6
H2O	H2O	k1gFnPc2	H2O
v	v	k7c6	v
modrých	modrý	k2eAgInPc6d1	modrý
krystalcích	krystalek	k1gInPc6	krystalek
<g/>
,	,	kIx,	,
bezvodý	bezvodý	k2eAgMnSc1d1	bezvodý
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
nazelenalým	nazelenalý	k2eAgInSc7d1	nazelenalý
nádechem	nádech	k1gInSc7	nádech
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
gerhardtit	gerhardtit	k5eAaPmF	gerhardtit
<g/>
.	.	kIx.	.
</s>
<s>
Roztoky	roztoka	k1gFnPc1	roztoka
dusičnanu	dusičnan	k1gInSc2	dusičnan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
úpravě	úprava	k1gFnSc3	úprava
povrchu	povrch	k1gInSc2	povrch
železných	železný	k2eAgInPc2d1	železný
slitků	slitek	k1gInPc2	slitek
(	(	kIx(	(
<g/>
moření	moření	k1gNnSc2	moření
<g/>
)	)	kIx)	)
před	před	k7c7	před
dalším	další	k2eAgNnSc7d1	další
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
CuCO3	CuCO3	k1gFnSc4	CuCO3
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zásadité	zásaditý	k2eAgFnSc6d1	zásaditá
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
ve	v	k7c6	v
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nerostů	nerost	k1gInPc2	nerost
malachitu	malachit	k1gInSc2	malachit
<g/>
,	,	kIx,	,
azuritu	azurit	k1gInSc2	azurit
a	a	k8xC	a
chessylitu	chessylit	k1gInSc2	chessylit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Zásadité	zásaditý	k2eAgInPc1d1	zásaditý
uhličitany	uhličitan	k1gInPc1	uhličitan
měďnaté	měďnatý	k2eAgInPc1d1	měďnatý
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
malířské	malířský	k2eAgFnPc1d1	malířská
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
modré	modrý	k2eAgFnPc4d1	modrá
světlice	světlice	k1gFnPc4	světlice
v	v	k7c6	v
ohněstrůjství	ohněstrůjství	k1gNnSc6	ohněstrůjství
a	a	k8xC	a
v	v	k7c6	v
barvířství	barvířství	k1gNnSc6	barvířství
<g/>
.	.	kIx.	.
</s>
<s>
Zásadité	zásaditý	k2eAgInPc4d1	zásaditý
uhličitany	uhličitan	k1gInPc4	uhličitan
měďnaté	měďnatý	k2eAgInPc4d1	měďnatý
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
měďnatých	měďnatý	k2eAgMnPc2d1	měďnatý
solí	solit	k5eAaImIp3nP	solit
alkalickými	alkalický	k2eAgInPc7d1	alkalický
uhličitany	uhličitan	k1gInPc7	uhličitan
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
uhličitanu	uhličitan	k1gInSc2	uhličitan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
Cu	Cu	k1gMnSc7	Cu
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
snadno	snadno	k6eAd1	snadno
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
podvojné	podvojný	k2eAgFnPc4d1	podvojná
<g/>
,	,	kIx,	,
adiční	adiční	k2eAgFnPc4d1	adiční
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoku	roztok	k1gInSc2	roztok
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
rhodanovodíkové	rhodanovodíková	k1gFnSc2	rhodanovodíková
nebo	nebo	k8xC	nebo
alkalického	alkalický	k2eAgInSc2d1	alkalický
rhodanidu	rhodanid	k1gInSc2	rhodanid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
CuS	CuS	k1gFnSc4	CuS
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
síran	síran	k1gInSc4	síran
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
covellin	covellin	k2eAgInSc1d1	covellin
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
vede	vést	k5eAaImIp3nS	vést
dobře	dobře	k6eAd1	dobře
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
oxidovadel	oxidovadlo	k1gNnPc2	oxidovadlo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
peroxid	peroxid	k1gInSc4	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
síran	síran	k1gInSc4	síran
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
získat	získat	k5eAaPmF	získat
zaváděním	zavádění	k1gNnSc7	zavádění
sulfanu	sulfan	k1gInSc2	sulfan
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxichlorid	Oxichlorid	k1gInSc1	Oxichlorid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
(	(	kIx(	(
<g/>
CuCl2	CuCl2	k1gFnSc1	CuCl2
.	.	kIx.	.
3Cu	[number]	k4	3Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
účinnou	účinný	k2eAgFnSc7d1	účinná
látkou	látka	k1gFnSc7	látka
chemického	chemický	k2eAgInSc2d1	chemický
postřiku	postřik	k1gInSc2	postřik
kuprikol	kuprikol	k1gInSc1	kuprikol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
registrován	registrovat	k5eAaBmNgInS	registrovat
jako	jako	k9	jako
fungicid	fungicid	k1gInSc1	fungicid
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
látku	látka	k1gFnSc4	látka
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
i	i	k9	i
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
škodlivou	škodlivý	k2eAgFnSc4d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
tepelném	tepelný	k2eAgInSc6d1	tepelný
rozkladu	rozklad	k1gInSc6	rozklad
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
toxických	toxický	k2eAgFnPc2d1	toxická
zplodin	zplodina	k1gFnPc2	zplodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Analyticky	analyticky	k6eAd1	analyticky
využitelná	využitelný	k2eAgFnSc1d1	využitelná
je	být	k5eAaImIp3nS	být
redukce	redukce	k1gFnSc1	redukce
iontu	ion	k1gInSc2	ion
Cu2	Cu2	k1gFnSc2	Cu2
<g/>
+	+	kIx~	+
v	v	k7c6	v
alkalickém	alkalický	k2eAgInSc6d1	alkalický
prostředí	prostředí	k1gNnSc2	prostředí
redukujícími	redukující	k2eAgInPc7d1	redukující
sacharidy	sacharid	k1gInPc7	sacharid
neboli	neboli	k8xC	neboli
sacharidy	sacharid	k1gInPc7	sacharid
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
volný	volný	k2eAgInSc4d1	volný
poloacetalový	poloacetalový	k2eAgInSc4d1	poloacetalový
hydroxyl	hydroxyl	k1gInSc4	hydroxyl
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgInPc4	všechen
monosacharidy	monosacharid	k1gInPc4	monosacharid
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
laktóza	laktóza	k1gFnSc1	laktóza
<g/>
,	,	kIx,	,
maltóza	maltóza	k1gFnSc1	maltóza
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
alkalický	alkalický	k2eAgInSc1d1	alkalický
roztok	roztok	k1gInSc1	roztok
měďnaté	měďnatý	k2eAgFnSc2d1	měďnatá
soli	sůl	k1gFnSc2	sůl
–	–	k?	–
Fehlingovo	Fehlingův	k2eAgNnSc1d1	Fehlingův
činidlo	činidlo	k1gNnSc1	činidlo
–	–	k?	–
červenohnědou	červenohnědý	k2eAgFnSc4d1	červenohnědá
sraženinu	sraženina	k1gFnSc4	sraženina
oxidu	oxid	k1gInSc2	oxid
měďného	měďný	k1gMnSc2	měďný
Cu2O	Cu2O	k1gMnSc2	Cu2O
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sacharidy	sacharid	k1gInPc1	sacharid
neobsahující	obsahující	k2eNgInSc1d1	neobsahující
volný	volný	k2eAgInSc4d1	volný
poloacetalový	poloacetalový	k2eAgInSc4d1	poloacetalový
hydroxyl	hydroxyl	k1gInSc4	hydroxyl
(	(	kIx(	(
<g/>
neredukující	redukující	k2eNgInPc1d1	redukující
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sacharóza	sacharóza	k1gFnSc1	sacharóza
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
reakci	reakce	k1gFnSc4	reakce
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Sloučeniny	sloučenina	k1gFnPc4	sloučenina
mědité	měditý	k2eAgFnSc2d1	měditý
Cu3	Cu3	k1gFnSc2	Cu3
<g/>
+	+	kIx~	+
====	====	k?	====
</s>
</p>
<p>
<s>
Sloučenin	sloučenina	k1gFnPc2	sloučenina
trojmocné	trojmocný	k2eAgFnSc2d1	trojmocná
mědi	měď	k1gFnSc2	měď
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
připravená	připravený	k2eAgFnSc1d1	připravená
sloučenina	sloučenina	k1gFnSc1	sloučenina
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
oxid	oxid	k1gInSc1	oxid
měditý	měditý	k2eAgInSc1d1	měditý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
granátově	granátově	k6eAd1	granátově
červený	červený	k2eAgInSc1d1	červený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
působením	působení	k1gNnSc7	působení
peroxodisíranu	peroxodisíran	k1gInSc2	peroxodisíran
draselného	draselný	k2eAgNnSc2d1	draselné
na	na	k7c4	na
čerstvě	čerstvě	k6eAd1	čerstvě
sražený	sražený	k2eAgInSc4d1	sražený
hydroxid	hydroxid	k1gInSc4	hydroxid
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
měditý	měditý	k2eAgInSc4d1	měditý
má	mít	k5eAaImIp3nS	mít
kyselý	kyselý	k2eAgInSc4d1	kyselý
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
hydroxidy	hydroxid	k1gInPc7	hydroxid
červené	červená	k1gFnSc2	červená
hydroxoměditany	hydroxoměditan	k1gInPc1	hydroxoměditan
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
vzorcem	vzorec	k1gInSc7	vzorec
MI	já	k3xPp1nSc3	já
<g/>
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
i	i	k9	i
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
podařila	podařit	k5eAaPmAgFnS	podařit
připravit	připravit	k5eAaPmF	připravit
fluorosůl	fluorosůl	k1gInSc4	fluorosůl
trojmocné	trojmocný	k2eAgFnSc2d1	trojmocná
mědi	měď	k1gFnSc2	měď
K3	K3	k1gMnSc1	K3
<g/>
[	[	kIx(	[
<g/>
CuF6	CuF6	k1gMnSc1	CuF6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
podařily	podařit	k5eAaPmAgInP	podařit
připravit	připravit	k5eAaPmF	připravit
také	také	k9	také
měditany	měditan	k1gInPc4	měditan
KCuO2	KCuO2	k1gMnPc2	KCuO2
a	a	k8xC	a
Ba	ba	k9	ba
<g/>
(	(	kIx(	(
<g/>
CuO2	CuO2	k1gMnSc6	CuO2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nemá	mít	k5eNaImIp3nS	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Oxidační	oxidační	k2eAgInSc1d1	oxidační
stav	stav	k1gInSc1	stav
IV	IV	kA	IV
(	(	kIx(	(
<g/>
d7	d7	k4	d7
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
CuIV	CuIV	k1gFnSc2	CuIV
nejsou	být	k5eNaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nemají	mít	k5eNaImIp3nP	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Cs2	Cs2	k1gMnSc1	Cs2
<g/>
[	[	kIx(	[
<g/>
CuIVF6	CuIVF6	k1gMnSc1	CuIVF6
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Oxidační	oxidační	k2eAgInSc1d1	oxidační
stav	stav	k1gInSc1	stav
III	III	kA	III
(	(	kIx(	(
<g/>
d8	d8	k4	d8
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Komplexy	komplex	k1gInPc1	komplex
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
III	III	kA	III
jsou	být	k5eAaImIp3nP	být
neobyčejně	obyčejně	k6eNd1	obyčejně
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
některých	některý	k3yIgInPc2	některý
biologických	biologický	k2eAgInPc2d1	biologický
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
na	na	k7c4	na
směs	směs	k1gFnSc4	směs
3	[number]	k4	3
dílů	díl	k1gInPc2	díl
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgNnSc2d1	draselné
a	a	k8xC	a
1	[number]	k4	1
dílu	díl	k1gInSc2	díl
chloridu	chlorid	k1gInSc2	chlorid
měďného	měďný	k2eAgInSc2d1	měďný
vzniká	vznikat	k5eAaImIp3nS	vznikat
zelený	zelený	k2eAgInSc1d1	zelený
paramagnetický	paramagnetický	k2eAgInSc1d1	paramagnetický
hexafluoroměditan	hexafluoroměditan	k1gInSc1	hexafluoroměditan
draselný	draselný	k2eAgInSc1d1	draselný
K3	K3	k1gMnSc7	K3
<g/>
[	[	kIx(	[
<g/>
CuF6	CuF6	k1gMnSc1	CuF6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
vysokospinovým	vysokospinův	k2eAgInSc7d1	vysokospinův
komplexem	komplex	k1gInSc7	komplex
CuIII	CuIII	k1gFnSc2	CuIII
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
redukuje	redukovat	k5eAaBmIp3nS	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
nízkospinové	nízkospinový	k2eAgFnPc4d1	nízkospinový
diamagnetické	diamagnetický	k2eAgFnPc4d1	diamagnetická
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
mají	mít	k5eAaImIp3nP	mít
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
koordinační	koordinační	k2eAgFnSc4d1	koordinační
sféru	sféra	k1gFnSc4	sféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Oxidační	oxidační	k2eAgInSc1d1	oxidační
stav	stav	k1gInSc1	stav
II	II	kA	II
(	(	kIx(	(
<g/>
d9	d9	k4	d9
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
soli	sůl	k1gFnPc1	sůl
tvoří	tvořit	k5eAaImIp3nP	tvořit
téměř	téměř	k6eAd1	téměř
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
anionty	anion	k1gInPc7	anion
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
CN	CN	kA	CN
<g/>
-	-	kIx~	-
a	a	k8xC	a
I	i	k9	i
<g/>
-	-	kIx~	-
tvoří	tvořit	k5eAaImIp3nP	tvořit
kovalentní	kovalentní	k2eAgFnPc1d1	kovalentní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
modrý	modrý	k2eAgInSc1d1	modrý
hexaaquaměďnatý	hexaaquaměďnatý	k2eAgInSc1d1	hexaaquaměďnatý
kationt	kationt	k1gInSc1	kationt
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
H2O	H2O	k1gMnSc1	H2O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
solí	sůl	k1gFnSc7	sůl
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
skalice	skalice	k1gFnSc1	skalice
CuSO4	CuSO4	k1gFnSc2	CuSO4
<g/>
•	•	k?	•
<g/>
5H2O	[number]	k4	5H2O
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
šesti	šest	k4xCc7	šest
molekulami	molekula	k1gFnPc7	molekula
vody	voda	k1gFnSc2	voda
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
pouze	pouze	k6eAd1	pouze
chloristan	chloristan	k1gInSc1	chloristan
a	a	k8xC	a
dusičnan	dusičnan	k1gInSc1	dusičnan
(	(	kIx(	(
<g/>
častější	častý	k2eAgNnSc1d2	častější
je	být	k5eAaImIp3nS	být
však	však	k9	však
trihydrát	trihydrát	k1gInSc1	trihydrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koordinační	koordinační	k2eAgNnPc1d1	koordinační
čísla	číslo	k1gNnPc1	číslo
u	u	k7c2	u
mědi	měď	k1gFnSc2	měď
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
tetraedrické	tetraedrický	k2eAgNnSc4d1	tetraedrický
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
oktaedrické	oktaedrický	k2eAgFnPc1d1	oktaedrický
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
uspořádání	uspořádání	k1gNnSc3	uspořádání
valenčních	valenční	k2eAgInPc2d1	valenční
elektronů	elektron	k1gInPc2	elektron
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vytvářet	vytvářet	k5eAaImF	vytvářet
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
oktaedr	oktaedr	k1gInSc4	oktaedr
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
okaedr	okaedr	k1gInSc1	okaedr
deformován	deformovat	k5eAaImNgInS	deformovat
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
přednostněji	přednostně	k6eAd2	přednostně
s	s	k7c7	s
dusíkatými	dusíkatý	k2eAgInPc7d1	dusíkatý
ligandy	ligand	k1gInPc7	ligand
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
než	než	k8xS	než
s	s	k7c7	s
kyslíkatými	kyslíkatý	k2eAgInPc7d1	kyslíkatý
ligandy	ligand	k1gInPc7	ligand
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amoniakáty	Amoniakát	k1gInPc1	Amoniakát
mědi	měď	k1gFnSc2	měď
vznikají	vznikat	k5eAaImIp3nP	vznikat
reakcí	reakce	k1gFnSc7	reakce
měďnatých	měďnatý	k2eAgFnPc2d1	měďnatá
solí	sůl	k1gFnPc2	sůl
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
je	být	k5eAaImIp3nS	být
tetraamminměďnatý	tetraamminměďnatý	k2eAgInSc1d1	tetraamminměďnatý
ion	ion	k1gInSc1	ion
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NH3	NH3	k1gMnSc1	NH3
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
intenzivně	intenzivně	k6eAd1	intenzivně
fialové	fialový	k2eAgFnPc4d1	fialová
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
amminměďnatý	amminměďnatý	k2eAgInSc1d1	amminměďnatý
ion	ion	k1gInSc1	ion
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NH3	NH3	k1gMnSc1	NH3
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
diamminměďnatý	diamminměďnatý	k2eAgInSc1d1	diamminměďnatý
ion	ion	k1gInSc1	ion
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NH3	NH3	k1gMnSc1	NH3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
triamminměďnatý	triamminměďnatý	k2eAgMnSc1d1	triamminměďnatý
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NH3	NH3	k1gMnSc1	NH3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
pentaamminměďnatý	pentaamminměďnatý	k2eAgInSc1d1	pentaamminměďnatý
ion	ion	k1gInSc1	ion
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NH3	NH3	k1gMnSc1	NH3
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
méně	málo	k6eAd2	málo
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
přítomnosti	přítomnost	k1gFnSc2	přítomnost
iontu	ion	k1gInSc2	ion
Cu2	Cu2	k1gFnSc2	Cu2
<g/>
+	+	kIx~	+
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
mědi	měď	k1gFnSc2	měď
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
připraveny	připravit	k5eAaPmNgInP	připravit
i	i	k9	i
jako	jako	k9	jako
pevné	pevný	k2eAgFnSc2d1	pevná
krystalické	krystalický	k2eAgFnSc2d1	krystalická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schweizerovo	Schweizerův	k2eAgNnSc4d1	Schweizerův
činidlo	činidlo	k1gNnSc4	činidlo
hydroxid	hydroxid	k1gInSc4	hydroxid
tetraamminměďnatý	tetraamminměďnatý	k2eAgInSc4d1	tetraamminměďnatý
[	[	kIx(	[
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
NH3	NH3	k1gFnSc1	NH3
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
celulosu	celulosa	k1gFnSc4	celulosa
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Připraví	připravit	k5eAaPmIp3nS	připravit
se	se	k3xPyFc4	se
přidáním	přidání	k1gNnSc7	přidání
roztoku	roztok	k1gInSc2	roztok
amoniaku	amoniak	k1gInSc2	amoniak
k	k	k7c3	k
čerstvé	čerstvý	k2eAgFnSc3d1	čerstvá
sraženině	sraženina	k1gFnSc3	sraženina
hydroxidu	hydroxid	k1gInSc2	hydroxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
roztoku	roztok	k1gInSc2	roztok
amoniaku	amoniak	k1gInSc2	amoniak
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
chloridu	chlorid	k1gInSc2	chlorid
amonného	amonný	k2eAgInSc2d1	amonný
na	na	k7c4	na
měď	měď	k1gFnSc4	měď
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měďnaté	měďnatý	k2eAgInPc4d1	měďnatý
ionty	ion	k1gInPc4	ion
halogenidů	halogenid	k1gMnPc2	halogenid
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
roztoku	roztok	k1gInSc6	roztok
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
koncentrovanými	koncentrovaný	k2eAgInPc7d1	koncentrovaný
roztoky	roztok	k1gInPc7	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
halogenidů	halogenid	k1gInPc2	halogenid
acidokomplexy	acidokomplex	k1gInPc4	acidokomplex
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
acidokyseliny	acidokyselin	k2eAgInPc4d1	acidokyselin
–	–	k?	–
acidoměďnatany	acidoměďnatan	k1gInPc4	acidoměďnatan
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
roztoky	roztok	k1gInPc1	roztok
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
hnědožlutou	hnědožlutý	k2eAgFnSc4d1	hnědožlutá
<g/>
,	,	kIx,	,
hnědou	hnědý	k2eAgFnSc4d1	hnědá
až	až	k8xS	až
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
při	při	k7c6	při
zředění	zředění	k1gNnSc6	zředění
se	se	k3xPyFc4	se
komplexy	komplex	k1gInPc7	komplex
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
komplexní	komplexní	k2eAgFnPc4d1	komplexní
látky	látka	k1gFnPc4	látka
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
krystalickém	krystalický	k2eAgInSc6d1	krystalický
stavu	stav	k1gInSc6	stav
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
hygroskopické	hygroskopický	k2eAgFnPc1d1	hygroskopická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
žluté	žlutý	k2eAgFnPc1d1	žlutá
až	až	k8xS	až
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
světle	světle	k6eAd1	světle
modré	modrý	k2eAgFnPc1d1	modrá
až	až	k8xS	až
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
kation	kation	k1gInSc1	kation
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
komplexní	komplexní	k2eAgFnSc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
organickými	organický	k2eAgFnPc7d1	organická
látkami	látka	k1gFnPc7	látka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
glycerin	glycerin	k1gInSc1	glycerin
<g/>
,	,	kIx,	,
sacharidy	sacharid	k1gInPc1	sacharid
nebo	nebo	k8xC	nebo
kyselina	kyselina	k1gFnSc1	kyselina
vinná	vinný	k2eAgFnSc1d1	vinná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
sloučeninách	sloučenina	k1gFnPc6	sloučenina
měď	měď	k1gFnSc4	měď
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
vodíkový	vodíkový	k2eAgInSc1d1	vodíkový
kation	kation	k1gInSc1	kation
v	v	k7c6	v
hydroxidových	hydroxidový	k2eAgFnPc6d1	hydroxidový
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vážou	vázat	k5eAaImIp3nP	vázat
měď	měď	k1gFnSc4	měď
tak	tak	k9	tak
pevně	pevně	k6eAd1	pevně
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
další	další	k2eAgInPc4d1	další
měďnaté	měďnatý	k2eAgInPc4d1	měďnatý
ionty	ion	k1gInPc4	ion
přítomny	přítomen	k2eAgInPc4d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
Fehlingovo	Fehlingův	k2eAgNnSc1d1	Fehlingův
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
redukujících	redukující	k2eAgInPc2d1	redukující
cukrů	cukr	k1gInPc2	cukr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
modré	modrý	k2eAgFnSc2d1	modrá
skalice	skalice	k1gFnSc2	skalice
<g/>
,	,	kIx,	,
Seignettovy	Seignettův	k2eAgFnSc2d1	Seignettova
soli	sůl	k1gFnSc2	sůl
KNa	KNa	k1gMnSc1	KNa
<g/>
[	[	kIx(	[
<g/>
C4H4O6	C4H4O6	k1gMnSc1	C4H4O6
<g/>
]	]	kIx)	]
<g/>
•	•	k?	•
<g/>
4	[number]	k4	4
H2O	H2O	k1gFnSc1	H2O
(	(	kIx(	(
<g/>
tetrahydrát	tetrahydrát	k1gInSc1	tetrahydrát
vínanu	vínan	k1gInSc2	vínan
draselno-sodného	draselnoodný	k2eAgInSc2d1	draselno-sodný
<g/>
)	)	kIx)	)
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kyanoměďnatany	kyanoměďnatan	k1gInPc7	kyanoměďnatan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
bílé	bílý	k2eAgFnPc1d1	bílá
krystalické	krystalický	k2eAgFnPc1d1	krystalická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
vzorcem	vzorec	k1gInSc7	vzorec
M2I	M2I	k1gMnSc1	M2I
<g/>
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Nižší	nízký	k2eAgInPc4d2	nižší
oxidační	oxidační	k2eAgInPc4d1	oxidační
stavy	stav	k1gInPc4	stav
====	====	k?	====
</s>
</p>
<p>
<s>
Komplexy	komplex	k1gInPc1	komplex
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
CuI	CuI	k1gFnSc2	CuI
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nestálé	stálý	k2eNgFnSc6d1	nestálá
,	,	kIx,	,
avšak	avšak	k8xC	avšak
některé	některý	k3yIgInPc4	některý
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stabilizovány	stabilizován	k2eAgFnPc1d1	stabilizována
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
Cu0	Cu0	k1gMnSc1	Cu0
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
měď	měď	k1gFnSc4	měď
především	především	k9	především
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Octan	octan	k1gInSc1	octan
měďný	měďný	k2eAgInSc1d1	měďný
CuC2H3O2	CuC2H3O2	k1gFnSc4	CuC2H3O2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
přikapáváním	přikapávání	k1gNnSc7	přikapávání
síranu	síran	k1gInSc2	síran
hydroxylaminia	hydroxylaminium	k1gNnSc2	hydroxylaminium
do	do	k7c2	do
horkého	horký	k2eAgInSc2d1	horký
roztoku	roztok	k1gInSc2	roztok
octanu	octan	k1gInSc2	octan
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
a	a	k8xC	a
octanu	octan	k1gInSc2	octan
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
okyselí	okyselit	k5eAaPmIp3nS	okyselit
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
a	a	k8xC	a
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
se	se	k3xPyFc4	se
octan	octan	k1gInSc1	octan
měďný	měďný	k2eAgInSc1d1	měďný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šťavelan	šťavelan	k1gInSc1	šťavelan
měďný	měďný	k2eAgInSc1d1	měďný
Cu2C2O4	Cu2C2O4	k1gFnSc4	Cu2C2O4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
přidáním	přidání	k1gNnSc7	přidání
kyseliny	kyselina	k1gFnSc2	kyselina
šťavelové	šťavelový	k2eAgFnSc2d1	šťavelová
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
měďného	měďný	k2eAgInSc2d1	měďný
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
jako	jako	k8xC	jako
sraženina	sraženina	k1gFnSc1	sraženina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Octan	octan	k1gInSc1	octan
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
C2H3O2	C2H3O2	k1gMnSc1	C2H3O2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
temně	temně	k6eAd1	temně
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
octan	octan	k1gInSc1	octan
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
měděnka	měděnka	k1gFnSc1	měděnka
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgFnSc6d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šťavelan	šťavelan	k1gInSc1	šťavelan
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
CuC2O4	CuC2O4	k1gFnSc4	CuC2O4
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
podvojné	podvojný	k2eAgFnPc4d1	podvojná
a	a	k8xC	a
adiční	adiční	k2eAgFnPc4d1	adiční
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc1	šťavelan
měďnatý	měďnatý	k2eAgMnSc1d1	měďnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
měďnatých	měďnatý	k2eAgMnPc2d1	měďnatý
solí	solit	k5eAaImIp3nP	solit
roztokem	roztok	k1gInSc7	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
šťavelové	šťavelový	k2eAgFnSc2d1	šťavelová
nebo	nebo	k8xC	nebo
alkalického	alkalický	k2eAgInSc2d1	alkalický
šťavelanu	šťavelan	k1gInSc2	šťavelan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Biologický	biologický	k2eAgInSc4d1	biologický
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Měď	měď	k1gFnSc1	měď
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
zinek	zinek	k1gInSc1	zinek
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
s	s	k7c7	s
významným	významný	k2eAgInSc7d1	významný
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
živý	živý	k2eAgInSc4d1	živý
organizmus	organizmus	k1gInSc4	organizmus
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
enzymatických	enzymatický	k2eAgInPc2d1	enzymatický
cyklů	cyklus	k1gInPc2	cyklus
nezbytných	zbytný	k2eNgInPc2d1	zbytný
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
funkci	funkce	k1gFnSc4	funkce
životních	životní	k2eAgInPc2d1	životní
pochodů	pochod	k1gInPc2	pochod
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
enzymy	enzym	k1gInPc1	enzym
například	například	k6eAd1	například
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
metabolizmus	metabolizmus	k1gInSc4	metabolizmus
sacharidů	sacharid	k1gInPc2	sacharid
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
<g/>
,	,	kIx,	,
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
vytváření	vytváření	k1gNnSc4	vytváření
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
krvetvorbu	krvetvorba	k1gFnSc4	krvetvorba
<g/>
,	,	kIx,	,
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
i	i	k9	i
fungování	fungování	k1gNnSc4	fungování
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nižší	nízký	k2eAgInPc4d2	nižší
organismy	organismus	k1gInPc4	organismus
však	však	k9	však
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
silný	silný	k2eAgInSc1d1	silný
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
je	být	k5eAaImIp3nS	být
měď	měď	k1gFnSc4	měď
centrálním	centrální	k2eAgInSc7d1	centrální
kovem	kov	k1gInSc7	kov
organokovové	organokovový	k2eAgFnSc2d1	organokovová
sloučeniny	sloučenina	k1gFnSc2	sloučenina
hemocyaninu	hemocyanina	k1gFnSc4	hemocyanina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
u	u	k7c2	u
měkkýšů	měkkýš	k1gMnPc2	měkkýš
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
členovců	členovec	k1gMnPc2	členovec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krabů	krab	k1gInPc2	krab
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
přenašeč	přenašeč	k1gMnSc1	přenašeč
kyslíku	kyslík	k1gInSc2	kyslík
–	–	k?	–
analogie	analogie	k1gFnSc2	analogie
k	k	k7c3	k
hemoglobinu	hemoglobin	k1gInSc3	hemoglobin
u	u	k7c2	u
teplokrevných	teplokrevný	k2eAgMnPc2d1	teplokrevný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
kolem	kolem	k7c2	kolem
1	[number]	k4	1
miligramu	miligram	k1gInSc2	miligram
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
dávky	dávka	k1gFnPc4	dávka
až	až	k6eAd1	až
k	k	k7c3	k
0,1	[number]	k4	0,1
gramu	gram	k1gInSc3	gram
organismu	organismus	k1gInSc2	organismus
neškodí	škodit	k5eNaImIp3nS	škodit
<g/>
.	.	kIx.	.
</s>
<s>
Potraviny	potravina	k1gFnSc2	potravina
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
měď	měď	k1gFnSc4	měď
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc4	kakao
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
korýši	korýš	k1gMnPc1	korýš
a	a	k8xC	a
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
anémií	anémie	k1gFnPc2	anémie
(	(	kIx(	(
<g/>
chudokrevností	chudokrevnost	k1gFnPc2	chudokrevnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpomalením	zpomalení	k1gNnSc7	zpomalení
duševního	duševní	k2eAgInSc2d1	duševní
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
zhoršením	zhoršení	k1gNnSc7	zhoršení
metabolismu	metabolismus	k1gInSc2	metabolismus
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
pigmentů	pigment	k1gInPc2	pigment
a	a	k8xC	a
vypadávání	vypadávání	k1gNnSc2	vypadávání
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
vaziva	vazivo	k1gNnSc2	vazivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přebytek	přebytek	k1gInSc1	přebytek
mědi	měď	k1gFnSc2	měď
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
zdravých	zdravý	k2eAgFnPc2d1	zdravá
osob	osoba	k1gFnPc2	osoba
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
minimálně	minimálně	k6eAd1	minimálně
250	[number]	k4	250
mg	mg	kA	mg
mědi	měď	k1gFnSc2	měď
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
se	se	k3xPyFc4	se
měď	měď	k1gFnSc1	měď
již	již	k6eAd1	již
začíná	začínat	k5eAaImIp3nS	začínat
projevovat	projevovat	k5eAaImF	projevovat
jako	jako	k9	jako
jed	jed	k1gInSc4	jed
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
kadmium	kadmium	k1gNnSc1	kadmium
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jako	jako	k9	jako
nevratný	vratný	k2eNgInSc1d1	nevratný
inhibitor	inhibitor	k1gInSc1	inhibitor
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
mezi	mezi	k7c7	mezi
0,25	[number]	k4	0,25
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
gramy	gram	k1gInPc4	gram
může	moct	k5eAaImIp3nS	moct
měď	měď	k1gFnSc1	měď
způsobit	způsobit	k5eAaPmF	způsobit
vážné	vážný	k2eAgInPc4d1	vážný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
vyvolat	vyvolat	k5eAaPmF	vyvolat
vážná	vážný	k2eAgNnPc4d1	vážné
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
mědi	měď	k1gFnSc2	měď
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
než	než	k8xS	než
2	[number]	k4	2
gramy	gram	k1gInPc4	gram
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
akutní	akutní	k2eAgFnSc1d1	akutní
otrava	otrava	k1gFnSc1	otrava
mědí	mědit	k5eAaImIp3nS	mědit
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Otravy	otrava	k1gFnPc1	otrava
mědí	měď	k1gFnPc2	měď
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měď	měď	k1gFnSc1	měď
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
má	mít	k5eAaImIp3nS	mít
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
nepoživatelnou	poživatelný	k2eNgFnSc7d1	nepoživatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
vzácná	vzácný	k2eAgFnSc1d1	vzácná
genetická	genetický	k2eAgFnSc1d1	genetická
porucha	porucha	k1gFnSc1	porucha
–	–	k?	–
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
tělo	tělo	k1gNnSc4	tělo
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
měď	měď	k1gFnSc4	měď
správně	správně	k6eAd1	správně
zpracovat	zpracovat	k5eAaPmF	zpracovat
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ukládá	ukládat	k5eAaImIp3nS	ukládat
ve	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Postižené	postižený	k2eAgFnPc4d1	postižená
děti	dítě	k1gFnPc4	dítě
trpí	trpět	k5eAaImIp3nP	trpět
poškozením	poškození	k1gNnSc7	poškození
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
demencí	demence	k1gFnPc2	demence
<g/>
,	,	kIx,	,
křečemi	křeč	k1gFnPc7	křeč
a	a	k8xC	a
třesem	třes	k1gInSc7	třes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.A.	F.A.	k1gFnSc2	F.A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr.	dr.	kA	dr.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc1	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1.	[number]	k4	1.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1.	[number]	k4	1.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1.	[number]	k4	1.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1.	[number]	k4	1.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
měď	měď	k1gFnSc1	měď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
měď	měď	k1gFnSc1	měď
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
