<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
С	С	k?	С
/	/	kIx~	/
Srbija	Srbija	k1gMnSc1	Srbija
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Republika	republika	k1gFnSc1	republika
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Р	Р	k?	Р
С	С	k?	С
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
soustátí	soustátí	k1gNnSc2	soustátí
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
Černá	Černá	k1gFnSc1	Černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Černohorci	Černohorec	k1gMnPc1	Černohorec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
těsnou	těsný	k2eAgFnSc7d1	těsná
většinou	většina	k1gFnSc7	většina
pro	pro	k7c4	pro
vystoupení	vystoupení	k1gNnSc4	vystoupení
ze	z	k7c2	z
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
konaném	konaný	k2eAgNnSc6d1	konané
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jednostrannému	jednostranný	k2eAgNnSc3d1	jednostranné
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
provincie	provincie	k1gFnSc2	provincie
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
napětí	napětí	k1gNnSc3	napětí
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
i	i	k8xC	i
vnitropolitickém	vnitropolitický	k2eAgNnSc6d1	vnitropolitické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Makedonií	Makedonie	k1gFnSc7	Makedonie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
Albánií	Albánie	k1gFnSc7	Albánie
nebo	nebo	k8xC	nebo
mezinárodně	mezinárodně	k6eAd1	mezinárodně
plně	plně	k6eAd1	plně
neuznaným	uznaný	k2eNgInSc7d1	neuznaný
Kosovem	Kosov	k1gInSc7	Kosov
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
pak	pak	k6eAd1	pak
s	s	k7c7	s
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
a	a	k8xC	a
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
je	být	k5eAaImIp3nS	být
kandidátskou	kandidátský	k2eAgFnSc7d1	kandidátská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
srbského	srbský	k2eAgInSc2d1	srbský
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
historickými	historický	k2eAgInPc7d1	historický
územími	území	k1gNnPc7	území
<g/>
,	,	kIx,	,
s	s	k7c7	s
vnitrozemskou	vnitrozemský	k2eAgFnSc7d1	vnitrozemská
Raškou	raška	k1gFnSc7	raška
a	a	k8xC	a
s	s	k7c7	s
přímořskou	přímořský	k2eAgFnSc7d1	přímořská
Dukljou	Duklja	k1gFnSc7	Duklja
(	(	kIx(	(
<g/>
Zetou	Zeta	k1gFnSc7	Zeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
nezávislých	závislý	k2eNgInPc2d1	nezávislý
předstátních	předstátní	k2eAgInPc2d1	předstátní
útvarů	útvar	k1gInPc2	útvar
(	(	kIx(	(
<g/>
Mačva	Mačva	k1gFnSc1	Mačva
<g/>
,	,	kIx,	,
Braničevsko	Braničevsko	k1gNnSc1	Braničevsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocený	sjednocený	k2eAgInSc1d1	sjednocený
srbský	srbský	k2eAgInSc1d1	srbský
stát	stát	k1gInSc1	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Formování	formování	k1gNnSc1	formování
Srbska	Srbsko	k1gNnSc2	Srbsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Balkánu	Balkán	k1gInSc2	Balkán
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejkomplikovanějších	komplikovaný	k2eAgInPc2d3	nejkomplikovanější
procesů	proces	k1gInPc2	proces
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
tlak	tlak	k1gInSc1	tlak
sousední	sousední	k2eAgFnSc2d1	sousední
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
podepsal	podepsat	k5eAaPmAgMnS	podepsat
na	na	k7c6	na
politickém	politický	k2eAgMnSc6d1	politický
<g/>
,	,	kIx,	,
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
i	i	k8xC	i
historickém	historický	k2eAgInSc6d1	historický
vývoji	vývoj	k1gInSc6	vývoj
srbské	srbský	k2eAgFnSc2d1	Srbská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivněno	ovlivněn	k2eAgNnSc1d1	ovlivněno
bylo	být	k5eAaImAgNnS	být
i	i	k8xC	i
náboženské	náboženský	k2eAgNnSc4d1	náboženské
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k8xC	i
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Srbska	Srbsko	k1gNnSc2	Srbsko
velice	velice	k6eAd1	velice
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
přišli	přijít	k5eAaPmAgMnP	přijít
Slované	Slovan	k1gMnPc1	Slovan
(	(	kIx(	(
<g/>
Srbové	Srb	k1gMnPc1	Srb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
vlivu	vliv	k1gInSc2	vliv
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
po	po	k7c6	po
církevním	církevní	k2eAgNnSc6d1	církevní
schizmatu	schizma	k1gNnSc6	schizma
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
pravoslavné	pravoslavný	k2eAgNnSc1d1	pravoslavné
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgNnSc1	první
království	království	k1gNnSc1	království
v	v	k7c6	v
Rašce	raška	k1gFnSc6	raška
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
vládl	vládnout	k5eAaImAgInS	vládnout
Časlav	Časlav	k1gMnSc4	Časlav
Klonimirović	Klonimirović	k1gMnSc4	Klonimirović
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc3d3	veliký
moci	moc	k1gFnSc3	moc
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
středověké	středověký	k2eAgNnSc1d1	středověké
Srbsko	Srbsko	k1gNnSc1	Srbsko
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Nemajnićů	Nemajnić	k1gMnPc2	Nemajnić
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Dušan	Dušan	k1gMnSc1	Dušan
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
1331	[number]	k4	1331
–	–	k?	–
1355	[number]	k4	1355
<g/>
)	)	kIx)	)
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
Srbsku	Srbsko	k1gNnSc3	Srbsko
většinu	většina	k1gFnSc4	většina
západního	západní	k2eAgInSc2d1	západní
Balkánu	Balkán	k1gInSc2	Balkán
i	i	k8xC	i
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgInSc4d1	považován
vedle	vedle	k7c2	vedle
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
za	za	k7c2	za
nejmocnějšího	mocný	k2eAgMnSc2d3	nejmocnější
vladaře	vladař	k1gMnSc2	vladař
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zlomem	zlom	k1gInSc7	zlom
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Srbska	Srbsko	k1gNnSc2	Srbsko
byla	být	k5eAaImAgFnS	být
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
poli	pole	k1gNnSc6	pole
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1389	[number]	k4	1389
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Srbsko	Srbsko	k1gNnSc1	Srbsko
postupně	postupně	k6eAd1	postupně
dostávalo	dostávat	k5eAaImAgNnS	dostávat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
srbský	srbský	k2eAgInSc1d1	srbský
stát	stát	k1gInSc1	stát
roku	rok	k1gInSc2	rok
1455	[number]	k4	1455
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Osmané	Osman	k1gMnPc1	Osman
dobyli	dobýt	k5eAaPmAgMnP	dobýt
jeho	jeho	k3xOp3gNnSc4	jeho
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ke	k	k7c3	k
stěhování	stěhování	k1gNnSc3	stěhování
Srbů	Srb	k1gMnPc2	Srb
z	z	k7c2	z
Kosova	Kosův	k2eAgInSc2d1	Kosův
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
a	a	k8xC	a
Krajiny	Krajina	k1gFnSc2	Krajina
<g/>
.	.	kIx.	.
</s>
<s>
Faktickou	faktický	k2eAgFnSc4d1	faktická
nezávislost	nezávislost	k1gFnSc4	nezávislost
získalo	získat	k5eAaPmAgNnS	získat
Srbsko	Srbsko	k1gNnSc1	Srbsko
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
za	za	k7c4	za
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vedl	vést	k5eAaImAgInS	vést
Karađorđe	Karađorđe	k1gInSc1	Karađorđe
Petrović	Petrović	k1gFnSc2	Petrović
<g/>
.	.	kIx.	.
</s>
<s>
Turkům	Turek	k1gMnPc3	Turek
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podařilo	podařit	k5eAaPmAgNnS	podařit
nakrátko	nakrátko	k6eAd1	nakrátko
obnovit	obnovit	k5eAaPmF	obnovit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
povstání	povstání	k1gNnSc6	povstání
Srbů	Srb	k1gMnPc2	Srb
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Miloše	Miloš	k1gMnSc2	Miloš
Obrenoviće	Obrenović	k1gFnSc2	Obrenović
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
byla	být	k5eAaImAgFnS	být
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
nucena	nucen	k2eAgFnSc1d1	nucena
uznat	uznat	k5eAaPmF	uznat
autonomní	autonomní	k2eAgNnSc4d1	autonomní
postavení	postavení	k1gNnSc4	postavení
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
Turecka	Turecko	k1gNnSc2	Turecko
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
již	již	k6eAd1	již
čistě	čistě	k6eAd1	čistě
formální	formální	k2eAgMnSc1d1	formální
(	(	kIx(	(
<g/>
Turci	Turek	k1gMnPc1	Turek
měli	mít	k5eAaImAgMnP	mít
právo	právo	k1gNnSc4	právo
udržovat	udržovat	k5eAaImF	udržovat
na	na	k7c6	na
srbském	srbský	k2eAgNnSc6d1	srbské
území	území	k1gNnSc6	území
šest	šest	k4xCc4	šest
opevněných	opevněný	k2eAgNnPc2d1	opevněné
měst	město	k1gNnPc2	město
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
pevnosti	pevnost	k1gFnPc4	pevnost
<g/>
,	,	kIx,	,
sultán	sultán	k1gMnSc1	sultán
formálně	formálně	k6eAd1	formálně
potvrzoval	potvrzovat	k5eAaImAgMnS	potvrzovat
srbského	srbský	k2eAgMnSc4d1	srbský
knížete	kníže	k1gMnSc4	kníže
a	a	k8xC	a
na	na	k7c6	na
hradbách	hradba	k1gFnPc6	hradba
měl	mít	k5eAaImAgInS	mít
vedle	vedle	k6eAd1	vedle
srbského	srbský	k2eAgMnSc4d1	srbský
vlát	vlát	k5eAaImF	vlát
také	také	k6eAd1	také
turecký	turecký	k2eAgInSc4d1	turecký
prapor	prapor	k1gInSc4	prapor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
iure	iure	k1gFnPc2	iure
získalo	získat	k5eAaPmAgNnS	získat
Srbsko	Srbsko	k1gNnSc4	Srbsko
samostatnost	samostatnost	k1gFnSc1	samostatnost
až	až	k9	až
po	po	k7c6	po
rusko-turecké	ruskourecký	k2eAgFnSc6d1	rusko-turecká
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rovněž	rovněž	k9	rovněž
podstatně	podstatně	k6eAd1	podstatně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc6	území
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
několikrát	několikrát	k6eAd1	několikrát
k	k	k7c3	k
dynastickým	dynastický	k2eAgInPc3d1	dynastický
převratům	převrat	k1gInPc3	převrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
střídaly	střídat	k5eAaImAgFnP	střídat
dynastie	dynastie	k1gFnPc1	dynastie
Karađorđevićů	Karađorđević	k1gMnPc2	Karađorđević
a	a	k8xC	a
Obrenovićů	Obrenović	k1gMnPc2	Obrenović
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Milan	Milan	k1gMnSc1	Milan
Obrenović	Obrenović	k1gMnSc1	Obrenović
nechal	nechat	k5eAaPmAgMnS	nechat
Srbsko	Srbsko	k1gNnSc4	Srbsko
prohlásit	prohlásit	k5eAaPmF	prohlásit
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
novodobým	novodobý	k2eAgMnSc7d1	novodobý
srbským	srbský	k2eAgMnSc7d1	srbský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
usilovaly	usilovat	k5eAaImAgInP	usilovat
všechny	všechen	k3xTgInPc1	všechen
balkánské	balkánský	k2eAgInPc1d1	balkánský
státy	stát	k1gInPc1	stát
o	o	k7c4	o
definitivní	definitivní	k2eAgNnSc4d1	definitivní
vypuzení	vypuzení	k1gNnSc4	vypuzení
Turků	turek	k1gInPc2	turek
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
balkánské	balkánský	k2eAgFnSc6d1	balkánská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Srbové	Srb	k1gMnPc1	Srb
bojovali	bojovat	k5eAaImAgMnP	bojovat
ve	v	k7c6	v
spojenectví	spojenectví	k1gNnSc6	spojenectví
s	s	k7c7	s
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
a	a	k8xC	a
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
1	[number]	k4	1
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
prince	princ	k1gMnSc2	princ
Alexandra	Alexandr	k1gMnSc2	Alexandr
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
tureckou	turecký	k2eAgFnSc7d1	turecká
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kumanova	Kumanův	k2eAgNnSc2d1	Kumanovo
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
připojilo	připojit	k5eAaPmAgNnS	připojit
Novpazarský	Novpazarský	k2eAgInSc4d1	Novpazarský
sandžak	sandžak	k1gInSc4	sandžak
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
Kosova	Kosův	k2eAgMnSc2d1	Kosův
a	a	k8xC	a
Makedonie	Makedonie	k1gFnPc4	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
získané	získaný	k2eAgNnSc4d1	získané
území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
spojenci	spojenec	k1gMnPc7	spojenec
vedly	vést	k5eAaImAgFnP	vést
zakrátko	zakrátko	k6eAd1	zakrátko
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
balkánské	balkánský	k2eAgFnSc2d1	balkánská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Srbové	Srb	k1gMnPc1	Srb
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
nad	nad	k7c7	nad
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
zábor	zábor	k1gInSc4	zábor
ještě	ještě	k6eAd1	ještě
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
provedli	provést	k5eAaPmAgMnP	provést
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
mladí	mladý	k2eAgMnPc1d1	mladý
srbští	srbský	k2eAgMnPc1d1	srbský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
z	z	k7c2	z
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
Srbsko	Srbsko	k1gNnSc4	Srbsko
do	do	k7c2	do
obtížné	obtížný	k2eAgFnSc2d1	obtížná
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
ostrou	ostrý	k2eAgFnSc4d1	ostrá
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
a	a	k8xC	a
proti	proti	k7c3	proti
Srbům	Srb	k1gMnPc3	Srb
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Srbští	srbský	k2eAgMnPc1d1	srbský
politici	politik	k1gMnPc1	politik
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
obvinění	obviněný	k2eAgMnPc1d1	obviněný
a	a	k8xC	a
proto	proto	k8xC	proto
atentát	atentát	k1gInSc4	atentát
ostře	ostro	k6eAd1	ostro
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Válce	válec	k1gInPc1	válec
se	se	k3xPyFc4	se
však	však	k9	však
nevyhnuli	vyhnout	k5eNaPmAgMnP	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
habsburské	habsburský	k2eAgFnSc3d1	habsburská
monarchii	monarchie	k1gFnSc3	monarchie
vedlo	vést	k5eAaImAgNnS	vést
Srbsko	Srbsko	k1gNnSc1	Srbsko
již	již	k9	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
černohorských	černohorský	k2eAgMnPc2d1	černohorský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
úspěšně	úspěšně	k6eAd1	úspěšně
-	-	kIx~	-
odrazili	odrazit	k5eAaPmAgMnP	odrazit
první	první	k4xOgNnSc4	první
i	i	k8xC	i
druhý	druhý	k4xOgInSc4	druhý
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
však	však	k9	však
nastal	nastat	k5eAaPmAgInS	nastat
zlom	zlom	k1gInSc1	zlom
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
frontě	fronta	k1gFnSc6	fronta
a	a	k8xC	a
srbská	srbský	k2eAgFnSc1d1	Srbská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
však	však	k9	však
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Živojina	Živojin	k1gMnSc2	Živojin
Mišiće	Mišić	k1gMnSc2	Mišić
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
do	do	k7c2	do
protiofenzivy	protiofenziva	k1gFnSc2	protiofenziva
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
linie	linie	k1gFnSc1	linie
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Kolubaře	Kolubař	k1gMnSc2	Kolubař
zakolísala	zakolísat	k5eAaPmAgFnS	zakolísat
a	a	k8xC	a
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
osvobozen	osvobozen	k2eAgInSc4d1	osvobozen
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
pouze	pouze	k6eAd1	pouze
13	[number]	k4	13
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Srbové	Srb	k1gMnPc1	Srb
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stali	stát	k5eAaPmAgMnP	stát
středem	středem	k7c2	středem
obdivu	obdiv	k1gInSc2	obdiv
spojenců	spojenec	k1gMnPc2	spojenec
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
i	i	k8xC	i
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
poražení	poražení	k1gNnSc3	poražení
silné	silný	k2eAgFnSc2d1	silná
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
byl	být	k5eAaImAgInS	být
však	však	k9	však
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgInS	nahradit
velikou	veliký	k2eAgFnSc7d1	veliká
ztrátou	ztráta	k1gFnSc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Bulhaři	Bulhar	k1gMnPc1	Bulhar
zabrali	zabrat	k5eAaPmAgMnP	zabrat
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
Makedonie	Makedonie	k1gFnSc2	Makedonie
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
s	s	k7c7	s
Rakušany	Rakušan	k1gMnPc7	Rakušan
plánovali	plánovat	k5eAaImAgMnP	plánovat
opětovný	opětovný	k2eAgInSc4d1	opětovný
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
armády	armáda	k1gFnSc2	armáda
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
opět	opět	k6eAd1	opět
dosazen	dosazen	k2eAgMnSc1d1	dosazen
generál	generál	k1gMnSc1	generál
Živojin	Živojin	k1gMnSc1	Živojin
Mišić	Mišić	k1gMnSc1	Mišić
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
zatlačil	zatlačit	k5eAaPmAgInS	zatlačit
bulharská	bulharský	k2eAgNnPc4d1	bulharské
vojska	vojsko	k1gNnPc4	vojsko
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
spojit	spojit	k5eAaPmF	spojit
se	s	k7c7	s
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
expedičními	expediční	k2eAgFnPc7d1	expediční
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
a	a	k8xC	a
Bulhaři	Bulhar	k1gMnPc1	Bulhar
Makedonii	Makedonie	k1gFnSc4	Makedonie
opět	opět	k6eAd1	opět
zabrali	zabrat	k5eAaPmAgMnP	zabrat
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
okupována	okupovat	k5eAaBmNgFnS	okupovat
Rakouskem-Uherskem	Rakouskem-Uhersek	k1gInSc7	Rakouskem-Uhersek
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
pod	pod	k7c7	pod
bulharským	bulharský	k2eAgNnSc7d1	bulharské
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
habsburské	habsburský	k2eAgFnSc2d1	habsburská
říše	říš	k1gFnSc2	říš
snažilo	snažit	k5eAaImAgNnS	snažit
připojená	připojený	k2eAgNnPc4d1	připojené
území	území	k1gNnPc4	území
zabrat	zabrat	k5eAaPmF	zabrat
natrvalo	natrvalo	k6eAd1	natrvalo
a	a	k8xC	a
tak	tak	k6eAd1	tak
na	na	k7c6	na
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
území	území	k1gNnSc6	území
začala	začít	k5eAaPmAgFnS	začít
silná	silný	k2eAgFnSc1d1	silná
bulharizace	bulharizace	k1gFnSc1	bulharizace
<g/>
,	,	kIx,	,
ničení	ničení	k1gNnSc1	ničení
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
v	v	k7c6	v
srbštině	srbština	k1gFnSc6	srbština
a	a	k8xC	a
bulharština	bulharština	k1gFnSc1	bulharština
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
a	a	k8xC	a
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
znovu	znovu	k6eAd1	znovu
zformovala	zformovat	k5eAaPmAgFnS	zformovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Korfu	Korfu	k1gNnSc2	Korfu
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
Soluňskou	soluňský	k2eAgFnSc4d1	soluňská
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1918	[number]	k4	1918
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
srbské	srbský	k2eAgNnSc4d1	srbské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
utvářet	utvářet	k5eAaImF	utvářet
Království	království	k1gNnSc4	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
Srbsko	Srbsko	k1gNnSc1	Srbsko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
okolo	okolo	k7c2	okolo
1	[number]	k4	1
250	[number]	k4	250
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
28	[number]	k4	28
<g/>
%	%	kIx~	%
veškeré	veškerý	k3xTgFnSc2	veškerý
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Království	království	k1gNnSc2	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
bylo	být	k5eAaImAgNnS	být
Srbsko	Srbsko	k1gNnSc1	Srbsko
centrem	centrum	k1gNnSc7	centrum
velkého	velký	k2eAgInSc2d1	velký
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
skoro	skoro	k6eAd1	skoro
polovinu	polovina	k1gFnSc4	polovina
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vládla	vládnout	k5eAaImAgFnS	vládnout
mu	on	k3xPp3gNnSc3	on
nadále	nadále	k6eAd1	nadále
srbská	srbský	k2eAgFnSc1d1	Srbská
královská	královský	k2eAgFnSc1d1	královská
dynastie	dynastie	k1gFnSc1	dynastie
Karađorđevićů	Karađorđević	k1gMnPc2	Karađorđević
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
národů	národ	k1gInPc2	národ
bylo	být	k5eAaImAgNnS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
sjednocením	sjednocení	k1gNnSc7	sjednocení
Itálie	Itálie	k1gFnSc2	Itálie
nebo	nebo	k8xC	nebo
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
oficiální	oficiální	k2eAgFnSc7d1	oficiální
politikou	politika	k1gFnSc7	politika
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
zformování	zformování	k1gNnSc6	zformování
jednotného	jednotný	k2eAgInSc2d1	jednotný
jugoslávského	jugoslávský	k2eAgInSc2d1	jugoslávský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedařilo	dařit	k5eNaImAgNnS	dařit
příbuzné	příbuzný	k2eAgNnSc1d1	příbuzné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
historicky	historicky	k6eAd1	historicky
velmi	velmi	k6eAd1	velmi
odlišně	odlišně	k6eAd1	odlišně
se	se	k3xPyFc4	se
vyvíjející	vyvíjející	k2eAgFnPc1d1	vyvíjející
entity	entita	k1gFnPc1	entita
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
plně	plně	k6eAd1	plně
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Konflikty	konflikt	k1gInPc1	konflikt
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
mnohonárodní	mnohonárodní	k2eAgFnSc6d1	mnohonárodní
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
srbsko-bulharským	srbskoulharský	k2eAgNnSc7d1	srbsko-bulharský
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
autonomii	autonomie	k1gFnSc4	autonomie
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
po	po	k7c4	po
celých	celý	k2eAgNnPc2d1	celé
23	[number]	k4	23
let	léto	k1gNnPc2	léto
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
SHS	SHS	kA	SHS
<g/>
,	,	kIx,	,
přejmenované	přejmenovaný	k2eAgFnSc2d1	přejmenovaná
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
na	na	k7c6	na
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Malé	Malé	k2eAgFnSc2d1	Malé
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
namířené	namířený	k2eAgFnSc2d1	namířená
hlavně	hlavně	k6eAd1	hlavně
proti	proti	k7c3	proti
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
(	(	kIx(	(
<g/>
spor	spor	k1gInSc4	spor
o	o	k7c4	o
Vojvodinu	Vojvodina	k1gFnSc4	Vojvodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyostřené	vyostřený	k2eAgInPc4d1	vyostřený
vztahy	vztah	k1gInPc4	vztah
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
(	(	kIx(	(
<g/>
poválečný	poválečný	k2eAgInSc4d1	poválečný
spor	spor	k1gInSc4	spor
o	o	k7c6	o
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
)	)	kIx)	)
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
s	s	k7c7	s
Albánií	Albánie	k1gFnSc7	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
byla	být	k5eAaImAgFnS	být
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
i	i	k9	i
světovou	světový	k2eAgFnSc7d1	světová
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
její	její	k3xOp3gInPc4	její
dopady	dopad	k1gInPc4	dopad
nebyly	být	k5eNaImAgInP	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zemědělskému	zemědělský	k2eAgInSc3d1	zemědělský
charakteru	charakter	k1gInSc3	charakter
hospodářství	hospodářství	k1gNnSc2	hospodářství
tak	tak	k6eAd1	tak
dramatické	dramatický	k2eAgFnPc1d1	dramatická
jako	jako	k8xC	jako
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Složitou	složitý	k2eAgFnSc4d1	složitá
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1	vnitropolitická
situaci	situace	k1gFnSc4	situace
s	s	k7c7	s
častým	častý	k2eAgNnSc7d1	časté
střídáním	střídání	k1gNnSc7	střídání
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
nestabilních	stabilní	k2eNgFnPc2d1	nestabilní
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
koalic	koalice	k1gFnPc2	koalice
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
král	král	k1gMnSc1	král
Alexandr	Alexandr	k1gMnSc1	Alexandr
vyřešit	vyřešit	k5eAaPmF	vyřešit
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1929	[number]	k4	1929
nastolením	nastolení	k1gNnSc7	nastolení
královské	královský	k2eAgFnSc2d1	královská
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgMnS	být
však	však	k9	však
král	král	k1gMnSc1	král
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
návštěvě	návštěva	k1gFnSc6	návštěva
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Barthouem	Barthou	k1gMnSc7	Barthou
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
a	a	k8xC	a
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
formálně	formálně	k6eAd1	formálně
dosedl	dosednout	k5eAaPmAgMnS	dosednout
jeho	jeho	k3xOp3gMnSc1	jeho
jedenáctiletý	jedenáctiletý	k2eAgMnSc1d1	jedenáctiletý
syn	syn	k1gMnSc1	syn
Petr	Petr	k1gMnSc1	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Balkánské	balkánský	k2eAgNnSc1d1	balkánské
tažení	tažení	k1gNnSc1	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
zpočátku	zpočátku	k6eAd1	zpočátku
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Regent	regent	k1gMnSc1	regent
Pavel	Pavel	k1gMnSc1	Pavel
však	však	k9	však
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
moci	moc	k1gFnSc2	moc
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
přistoupení	přistoupení	k1gNnSc4	přistoupení
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
k	k	k7c3	k
Ose	osa	k1gFnSc3	osa
Berlín-Řím-Tokio	Berlín-Řím-Tokio	k6eAd1	Berlín-Řím-Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
bouřlivé	bouřlivý	k2eAgInPc4d1	bouřlivý
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Petr	Petr	k1gMnSc1	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
předčasně	předčasně	k6eAd1	předčasně
plnoletého	plnoletý	k2eAgNnSc2d1	plnoleté
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Osou	osa	k1gFnSc7	osa
okamžitě	okamžitě	k6eAd1	okamžitě
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
spojenci	spojenec	k1gMnPc1	spojenec
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
do	do	k7c2	do
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgNnPc4d1	královské
vojska	vojsko	k1gNnPc4	vojsko
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
fašistický	fašistický	k2eAgInSc1d1	fašistický
Nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
území	území	k1gNnSc2	území
byl	být	k5eAaImAgInS	být
okupován	okupovat	k5eAaBmNgInS	okupovat
německými	německý	k2eAgMnPc7d1	německý
<g/>
,	,	kIx,	,
italskými	italský	k2eAgMnPc7d1	italský
<g/>
,	,	kIx,	,
maďarskými	maďarský	k2eAgMnPc7d1	maďarský
a	a	k8xC	a
bulharskými	bulharský	k2eAgNnPc7d1	bulharské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
ustašovci	ustašovec	k1gMnPc1	ustašovec
zahájili	zahájit	k5eAaPmAgMnP	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
na	na	k7c6	na
území	území	k1gNnSc6	území
chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
státu	stát	k1gInSc2	stát
brutální	brutální	k2eAgFnSc4d1	brutální
genocidu	genocida	k1gFnSc4	genocida
srbského	srbský	k2eAgNnSc2d1	srbské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
do	do	k7c2	do
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okupovaném	okupovaný	k2eAgInSc6d1	okupovaný
a	a	k8xC	a
okleštěném	okleštěný	k2eAgInSc6d1	okleštěný
zbytku	zbytek	k1gInSc6	zbytek
Srbska	Srbsko	k1gNnSc2	Srbsko
byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
loutkový	loutkový	k2eAgInSc1d1	loutkový
režim	režim	k1gInSc1	režim
generála	generál	k1gMnSc2	generál
Milana	Milan	k1gMnSc2	Milan
Nediće	Nedić	k1gMnSc2	Nedić
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
současně	současně	k6eAd1	současně
vypukl	vypuknout	k5eAaPmAgMnS	vypuknout
proti	proti	k7c3	proti
okupantům	okupant	k1gMnPc3	okupant
odboj	odboj	k1gInSc4	odboj
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
vedly	vést	k5eAaImAgFnP	vést
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
dvě	dva	k4xCgFnPc4	dva
organisace	organisace	k1gFnPc1	organisace
<g/>
.	.	kIx.	.
</s>
<s>
Dragoljub	Dragoljub	k1gMnSc1	Dragoljub
Mihailović	Mihailović	k1gMnSc1	Mihailović
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
později	pozdě	k6eAd2	pozdě
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
exilové	exilový	k2eAgFnPc1d1	exilová
vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
organisaci	organisace	k1gFnSc4	organisace
zvanou	zvaný	k2eAgFnSc4d1	zvaná
"	"	kIx"	"
<g/>
Jugoslavenska	Jugoslavensko	k1gNnSc2	Jugoslavensko
vojska	vojsko	k1gNnSc2	vojsko
u	u	k7c2	u
otađbini	otađbin	k2eAgMnPc1d1	otađbin
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Jugoslávské	jugoslávský	k2eAgNnSc1d1	jugoslávské
vojsko	vojsko	k1gNnSc1	vojsko
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
odbojářů	odbojář	k1gMnPc2	odbojář
z	z	k7c2	z
balkánských	balkánský	k2eAgFnPc2d1	balkánská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnPc3	její
příslušníkům	příslušník	k1gMnPc3	příslušník
říkalo	říkat	k5eAaImAgNnS	říkat
Četnici	četnice	k1gFnSc4	četnice
<g/>
.	.	kIx.	.
</s>
<s>
Konkurentem	konkurent	k1gMnSc7	konkurent
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byl	být	k5eAaImAgInS	být
komunistický	komunistický	k2eAgInSc1d1	komunistický
partyzánský	partyzánský	k2eAgInSc1d1	partyzánský
odboj	odboj	k1gInSc1	odboj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
vůdcem	vůdce	k1gMnSc7	vůdce
byl	být	k5eAaImAgMnS	být
Josip	Josip	k1gMnSc1	Josip
Broz	Broza	k1gFnPc2	Broza
Tito	tento	k3xDgMnPc1	tento
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
území	území	k1gNnPc2	území
především	především	k6eAd1	především
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
síla	síla	k1gFnSc1	síla
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
četniků	četnik	k1gMnPc2	četnik
neustále	neustále	k6eAd1	neustále
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Mihajlovićovi	Mihajlovićův	k2eAgMnPc1d1	Mihajlovićův
vojáci	voják	k1gMnPc1	voják
bojovali	bojovat	k5eAaImAgMnP	bojovat
především	především	k6eAd1	především
proti	proti	k7c3	proti
chorvatským	chorvatský	k2eAgMnPc3d1	chorvatský
ustašovcům	ustašovec	k1gMnPc3	ustašovec
<g/>
,	,	kIx,	,
partyzáni	partyzán	k1gMnPc1	partyzán
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
své	svůj	k3xOyFgInPc4	svůj
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
německé	německý	k2eAgMnPc4d1	německý
okupanty	okupant	k1gMnPc4	okupant
a	a	k8xC	a
získávali	získávat	k5eAaImAgMnP	získávat
přívržence	přívrženec	k1gMnSc4	přívrženec
i	i	k8xC	i
mezi	mezi	k7c7	mezi
nesrbskými	srbský	k2eNgInPc7d1	srbský
národy	národ	k1gInPc7	národ
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Odboj	odboj	k1gInSc1	odboj
zvláště	zvláště	k6eAd1	zvláště
zesílil	zesílit	k5eAaPmAgInS	zesílit
po	po	k7c4	po
kapitulaci	kapitulace	k1gFnSc4	kapitulace
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Německo	Německo	k1gNnSc1	Německo
muselo	muset	k5eAaImAgNnS	muset
převzít	převzít	k5eAaPmF	převzít
bývalou	bývalý	k2eAgFnSc4d1	bývalá
italskou	italský	k2eAgFnSc4d1	italská
okupační	okupační	k2eAgFnSc4d1	okupační
zónu	zóna	k1gFnSc4	zóna
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
několika	několik	k4yIc3	několik
silným	silný	k2eAgFnPc3d1	silná
ofenzívám	ofenzíva	k1gFnPc3	ofenzíva
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
nepodařilo	podařit	k5eNaPmAgNnS	podařit
partyzány	partyzána	k1gFnSc2	partyzána
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
způsobem	způsob	k1gInSc7	způsob
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgMnSc7d3	veliký
podporovatelem	podporovatel	k1gMnSc7	podporovatel
četniků	četnik	k1gMnPc2	četnik
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
Titovi	Tita	k1gMnSc6	Tita
perspektivnějšího	perspektivní	k2eAgMnSc4d2	perspektivnější
spojence	spojenec	k1gMnSc4	spojenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
mocenský	mocenský	k2eAgInSc1d1	mocenský
střet	střet	k1gInSc1	střet
dvou	dva	k4xCgNnPc2	dva
odbojových	odbojový	k2eAgNnPc2d1	odbojové
křídel	křídlo	k1gNnPc2	křídlo
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
postup	postup	k1gInSc4	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
partyzánskou	partyzánský	k2eAgFnSc7d1	Partyzánská
armádou	armáda	k1gFnSc7	armáda
NOVJ	NOVJ	kA	NOVJ
(	(	kIx(	(
<g/>
Národně	národně	k6eAd1	národně
osvobozenecká	osvobozenecký	k2eAgNnPc1d1	osvobozenecké
vojska	vojsko	k1gNnPc1	vojsko
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
)	)	kIx)	)
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc1d1	mnohonárodnostní
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Tita	Tit	k1gInSc2	Tit
jako	jako	k8xS	jako
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Titův	Titův	k2eAgInSc4d1	Titův
režim	režim	k1gInSc4	režim
si	se	k3xPyFc3	se
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
loutkových	loutkový	k2eAgInPc2d1	loutkový
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
závislých	závislý	k2eAgFnPc2d1	závislá
na	na	k7c6	na
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
počínal	počínat	k5eAaImAgMnS	počínat
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
k	k	k7c3	k
silné	silný	k2eAgFnSc3d1	silná
roztržce	roztržka	k1gFnSc3	roztržka
mezi	mezi	k7c7	mezi
Titem	Tit	k1gMnSc7	Tit
a	a	k8xC	a
Stalinem	Stalin	k1gMnSc7	Stalin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
invaze	invaze	k1gFnPc1	invaze
proti	proti	k7c3	proti
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
neodvážil	odvážit	k5eNaPmAgMnS	odvážit
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
nebyla	být	k5eNaImAgFnS	být
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
komunistická	komunistický	k2eAgFnSc1d1	komunistická
země	země	k1gFnSc1	země
součástí	součást	k1gFnSc7	součást
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
občanské	občanský	k2eAgFnSc2d1	občanská
i	i	k8xC	i
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
svobody	svoboda	k1gFnSc2	svoboda
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
značně	značně	k6eAd1	značně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Titově	Titův	k2eAgFnSc6d1	Titova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
postupně	postupně	k6eAd1	postupně
slábla	slábnout	k5eAaImAgFnS	slábnout
autorita	autorita	k1gFnSc1	autorita
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
spor	spor	k1gInSc1	spor
o	o	k7c4	o
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
bránili	bránit	k5eAaImAgMnP	bránit
zejména	zejména	k9	zejména
právě	právě	k6eAd1	právě
Srbové	Srbové	k2eAgFnPc4d1	Srbové
<g/>
,	,	kIx,	,
dominující	dominující	k2eAgFnPc4d1	dominující
v	v	k7c6	v
orgánech	orgán	k1gInPc6	orgán
jugoslávského	jugoslávský	k2eAgInSc2d1	jugoslávský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
se	se	k3xPyFc4	se
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
odtrhlo	odtrhnout	k5eAaPmAgNnS	odtrhnout
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
i	i	k8xC	i
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
federaci	federace	k1gFnSc6	federace
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Svazová	svazový	k2eAgFnSc1d1	svazová
republika	republika	k1gFnSc1	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Slobodan	Slobodan	k1gMnSc1	Slobodan
Milošević	Milošević	k1gMnSc1	Milošević
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
neuznala	uznat	k5eNaPmAgFnS	uznat
stát	stát	k5eAaImF	stát
jako	jako	k9	jako
nástupnický	nástupnický	k2eAgMnSc1d1	nástupnický
po	po	k7c6	po
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
schválila	schválit	k5eAaPmAgFnS	schválit
ale	ale	k9	ale
tuto	tento	k3xDgFnSc4	tento
federaci	federace	k1gFnSc4	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
Bouře	bouř	k1gFnSc2	bouř
bylo	být	k5eAaImAgNnS	být
chorvatskou	chorvatský	k2eAgFnSc7d1	chorvatská
armádou	armáda	k1gFnSc7	armáda
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
do	do	k7c2	do
Srbska	Srbsko	k1gNnSc2	Srbsko
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
Srbů	Srb	k1gMnPc2	Srb
z	z	k7c2	z
území	území	k1gNnSc2	území
Srbské	srbský	k2eAgFnSc2d1	Srbská
Krajiny	Krajina	k1gFnSc2	Krajina
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Srby	Srb	k1gMnPc7	Srb
a	a	k8xC	a
Albánci	Albánec	k1gMnPc7	Albánec
na	na	k7c4	na
území	území	k1gNnSc4	území
Kosova	Kosův	k2eAgFnSc1d1	Kosova
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
stupňovat	stupňovat	k5eAaImF	stupňovat
<g/>
.	.	kIx.	.
</s>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gFnPc2	jejich
obětí	oběť	k1gFnPc2	oběť
začaly	začít	k5eAaPmAgFnP	začít
narůstat	narůstat	k5eAaImF	narůstat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyhrocených	vyhrocený	k2eAgInPc6d1	vyhrocený
střetech	střet	k1gInPc6	střet
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
stály	stát	k5eAaImAgFnP	stát
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
Kosovská	kosovský	k2eAgFnSc1d1	Kosovská
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
UCK	UCK	kA	UCK
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
srbské	srbský	k2eAgFnSc2d1	Srbská
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1998	[number]	k4	1998
ostře	ostro	k6eAd1	ostro
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
dopouštěly	dopouštět	k5eAaImAgFnP	dopouštět
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
hluboké	hluboký	k2eAgNnSc4d1	hluboké
znepokojení	znepokojení	k1gNnSc4	znepokojení
nad	nad	k7c7	nad
prudkým	prudký	k2eAgNnSc7d1	prudké
zhoršením	zhoršení	k1gNnSc7	zhoršení
humanitární	humanitární	k2eAgFnSc2d1	humanitární
situace	situace	k1gFnSc2	situace
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
alarmujících	alarmující	k2eAgInPc2d1	alarmující
poznatků	poznatek	k1gInPc2	poznatek
schválila	schválit	k5eAaPmAgFnS	schválit
rezoluci	rezoluce	k1gFnSc4	rezoluce
č.	č.	k?	č.
<g/>
1199	[number]	k4	1199
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doslova	doslova	k6eAd1	doslova
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zhoršení	zhoršení	k1gNnSc3	zhoršení
situace	situace	k1gFnSc2	situace
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
představuje	představovat	k5eAaImIp3nS	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
mír	mír	k1gInSc4	mír
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
v	v	k7c6	v
tamní	tamní	k2eAgFnSc6d1	tamní
oblasti	oblast	k1gFnSc6	oblast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc1	souhlas
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
"	"	kIx"	"
<g/>
všech	všecek	k3xTgInPc2	všecek
dostupných	dostupný	k2eAgInPc2d1	dostupný
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
opatření	opatření	k1gNnPc2	opatření
<g/>
"	"	kIx"	"
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
udělit	udělit	k5eAaPmF	udělit
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
schválení	schválení	k1gNnSc2	schválení
druhé	druhý	k4xOgFnSc2	druhý
rezoluce	rezoluce	k1gFnSc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
válkou	válka	k1gFnSc7	válka
nelegální	legální	k2eNgFnPc1d1	nelegální
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
trpících	trpící	k2eAgFnPc2d1	trpící
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zajistit	zajistit	k5eAaPmF	zajistit
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
stabilitu	stabilita	k1gFnSc4	stabilita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
kosovské	kosovský	k2eAgFnSc2d1	Kosovská
otázky	otázka	k1gFnSc2	otázka
sehrála	sehrát	k5eAaPmAgFnS	sehrát
americká	americký	k2eAgFnSc1d1	americká
ministryně	ministryně	k1gFnSc1	ministryně
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Madeleine	Madeleine	k1gFnSc1	Madeleine
Albrightová	Albrightová	k1gFnSc1	Albrightová
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
zahájily	zahájit	k5eAaPmAgFnP	zahájit
letecké	letecký	k2eAgFnPc1d1	letecká
síly	síla	k1gFnPc1	síla
NATO	nato	k6eAd1	nato
bombardování	bombardování	k1gNnSc2	bombardování
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k8xC	tedy
i	i	k9	i
Srbska	Srbsko	k1gNnPc4	Srbsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přinutily	přinutit	k5eAaPmAgFnP	přinutit
vládu	vláda	k1gFnSc4	vláda
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
stáhnout	stáhnout	k5eAaPmF	stáhnout
se	se	k3xPyFc4	se
z	z	k7c2	z
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
,	,	kIx,	,
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukončit	ukončit	k5eAaPmF	ukončit
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
separatistům	separatista	k1gMnPc3	separatista
z	z	k7c2	z
UCK	UCK	kA	UCK
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
údery	úder	k1gInPc1	úder
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dostaly	dostat	k5eAaPmAgFnP	dostat
název	název	k1gInSc4	název
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
trvaly	trvat	k5eAaImAgFnP	trvat
až	až	k9	až
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
velkou	velký	k2eAgFnSc7d1	velká
manifestací	manifestace	k1gFnSc7	manifestace
vojenských	vojenský	k2eAgFnPc2d1	vojenská
schopností	schopnost	k1gFnPc2	schopnost
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
proti	proti	k7c3	proti
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
válkou	válka	k1gFnSc7	válka
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
podílelo	podílet	k5eAaImAgNnS	podílet
svými	svůj	k3xOyFgFnPc7	svůj
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
kapacitami	kapacita	k1gFnPc7	kapacita
třináct	třináct	k4xCc4	třináct
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
stala	stát	k5eAaPmAgFnS	stát
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
NATO	NATO	kA	NATO
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
12	[number]	k4	12
dnů	den	k1gInPc2	den
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
Alianci	aliance	k1gFnSc3	aliance
připojila	připojit	k5eAaPmAgFnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
USA	USA	kA	USA
a	a	k8xC	a
EU	EU	kA	EU
jednostranně	jednostranně	k6eAd1	jednostranně
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Srbsko	Srbsko	k1gNnSc1	Srbsko
odmítá	odmítat	k5eAaImIp3nS	odmítat
uznat	uznat	k5eAaPmF	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
snahu	snaha	k1gFnSc4	snaha
G.	G.	kA	G.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
neprokázaly	prokázat	k5eNaPmAgInP	prokázat
důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
válku	válka	k1gFnSc4	válka
nelegitimní	legitimní	k2eNgFnSc4d1	nelegitimní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Západ	západ	k1gInSc4	západ
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
snaze	snaha	k1gFnSc6	snaha
naklonit	naklonit	k5eAaPmF	naklonit
si	se	k3xPyFc3	se
srbské	srbský	k2eAgFnPc4d1	Srbská
politiky	politika	k1gFnPc4	politika
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
omluvě	omluva	k1gFnSc6	omluva
za	za	k7c4	za
bombardování	bombardování	k1gNnSc4	bombardování
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
šéfa	šéf	k1gMnSc2	šéf
NATO	NATO	kA	NATO
Jense	Jens	k1gMnSc2	Jens
Stoltenberga	Stoltenberg	k1gMnSc2	Stoltenberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
lítost	lítost	k1gFnSc4	lítost
nad	nad	k7c7	nad
civilními	civilní	k2eAgFnPc7d1	civilní
oběťmi	oběť	k1gFnPc7	oběť
také	také	k9	také
americký	americký	k2eAgMnSc1d1	americký
viceprezident	viceprezident	k1gMnSc1	viceprezident
Joe	Joe	k1gMnSc1	Joe
Biden	Bidno	k1gNnPc2	Bidno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
federální	federální	k2eAgInSc1d1	federální
parlament	parlament	k1gInSc1	parlament
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
na	na	k7c4	na
Státní	státní	k2eAgNnSc4d1	státní
společenství	společenství	k1gNnSc4	společenství
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
udělení	udělení	k1gNnSc1	udělení
větší	veliký	k2eAgFnSc2d2	veliký
pravomoci	pravomoc	k1gFnSc2	pravomoc
oběma	dva	k4xCgFnPc3	dva
republikám	republika	k1gFnPc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
však	však	k9	však
stále	stále	k6eAd1	stále
cítila	cítit	k5eAaImAgFnS	cítit
potřebu	potřeba	k1gFnSc4	potřeba
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
osamostatnit	osamostatnit	k5eAaPmF	osamostatnit
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
již	již	k6eAd1	již
víceméně	víceméně	k9	víceméně
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
Srbskem	Srbsko	k1gNnSc7	Srbsko
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
jen	jen	k9	jen
společnou	společný	k2eAgFnSc4d1	společná
obrannou	obranný	k2eAgFnSc4d1	obranná
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
referendum	referendum	k1gNnSc4	referendum
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
zastánci	zastánce	k1gMnPc1	zastánce
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
samostatnost	samostatnost	k1gFnSc4	samostatnost
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Srbsko	Srbsko	k1gNnSc1	Srbsko
do	do	k7c2	do
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomalá	pomalý	k2eAgFnSc1d1	pomalá
transformace	transformace	k1gFnSc1	transformace
země	zem	k1gFnSc2	zem
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
demokratických	demokratický	k2eAgFnPc2d1	demokratická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
status	status	k1gInSc4	status
Kosova	Kosův	k2eAgMnSc2d1	Kosův
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
jednostrannou	jednostranný	k2eAgFnSc4d1	jednostranná
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
podalo	podat	k5eAaPmAgNnS	podat
Srbsko	Srbsko	k1gNnSc1	Srbsko
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
Srbsku	Srbsko	k1gNnSc3	Srbsko
udělen	udělen	k2eAgInSc4d1	udělen
status	status	k1gInSc4	status
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c6	na
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
<g/>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
demokratizuje	demokratizovat	k5eAaBmIp3nS	demokratizovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
a	a	k8xC	a
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Srbsko	Srbsko	k1gNnSc1	Srbsko
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
(	(	kIx(	(
<g/>
151	[number]	k4	151
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
(	(	kIx(	(
<g/>
241	[number]	k4	241
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
<g/>
,	,	kIx,	,
Černou	Černá	k1gFnSc7	Černá
Horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Makedonií	Makedonie	k1gFnSc7	Makedonie
(	(	kIx(	(
<g/>
221	[number]	k4	221
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
(	(	kIx(	(
<g/>
318	[number]	k4	318
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
(	(	kIx(	(
<g/>
476	[number]	k4	476
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Panonská	panonský	k2eAgFnSc1d1	Panonská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
řeky	řeka	k1gFnPc1	řeka
Tisa	Tisa	k1gFnSc1	Tisa
<g/>
,	,	kIx,	,
Sáva	Sáva	k1gFnSc1	Sáva
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
přitékající	přitékající	k2eAgFnSc1d1	přitékající
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
terén	terén	k1gInSc1	terén
zvedá	zvedat	k5eAaImIp3nS	zvedat
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
Dinárské	dinárský	k2eAgFnPc4d1	Dinárská
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
většinu	většina	k1gFnSc4	většina
západního	západní	k2eAgNnSc2d1	západní
a	a	k8xC	a
Centrálního	centrální	k2eAgNnSc2d1	centrální
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnější	východní	k2eAgFnPc1d3	nejvýchodnější
části	část	k1gFnPc1	část
země	zem	k1gFnSc2	zem
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
Valašské	valašský	k2eAgFnSc2d1	Valašská
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
rovněž	rovněž	k9	rovněž
přetíná	přetínat	k5eAaImIp3nS	přetínat
Karpatský	karpatský	k2eAgInSc4d1	karpatský
oblouk	oblouk	k1gInSc4	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Karpaty	Karpaty	k1gInPc1	Karpaty
se	se	k3xPyFc4	se
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Srbska	Srbsko	k1gNnSc2	Srbsko
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
pohořím	pohoří	k1gNnSc7	pohoří
Staré	Staré	k2eAgFnSc2d1	Staré
Planiny	planina	k1gFnSc2	planina
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Srbska	Srbsko	k1gNnSc2	Srbsko
Midžur	Midžura	k1gFnPc2	Midžura
vysoká	vysoký	k2eAgFnSc1d1	vysoká
2169	[number]	k4	2169
m.	m.	k?	m.
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nP	ležet
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
pohoří	pohoří	k1gNnSc4	pohoří
Kopaonik	Kopaonika	k1gFnPc2	Kopaonika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
trojmezí	trojmezí	k1gNnSc6	trojmezí
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
a	a	k8xC	a
Černou	Černá	k1gFnSc7	Černá
Horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
pohoří	pohoří	k1gNnSc3	pohoří
Mokra	mokro	k1gNnSc2	mokro
Gora	Gor	k1gInSc2	Gor
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
2156	[number]	k4	2156
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dinárské	dinárský	k2eAgFnPc1d1	Dinárská
hory	hora	k1gFnPc1	hora
hory	hora	k1gFnSc2	hora
následují	následovat	k5eAaImIp3nP	následovat
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
Driny	Drina	k1gFnSc2	Drina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
celou	celý	k2eAgFnSc4d1	celá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
Srbska	Srbsko	k1gNnSc2	Srbsko
leží	ležet	k5eAaImIp3nS	ležet
pohoří	pohoří	k1gNnSc4	pohoří
Zlatibor	Zlatibora	k1gFnPc2	Zlatibora
(	(	kIx(	(
<g/>
1496	[number]	k4	1496
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
vody	voda	k1gFnPc4	voda
řek	řeka	k1gFnPc2	řeka
Srbska	Srbsko	k1gNnSc2	Srbsko
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
řeky	řeka	k1gFnSc2	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
tečou	téct	k5eAaImIp3nP	téct
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
evropská	evropský	k2eAgFnSc1d1	Evropská
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
Srbskem	Srbsko	k1gNnSc7	Srbsko
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
588	[number]	k4	588
kilometry	kilometr	k1gInPc4	kilometr
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
její	její	k3xOp3gFnSc2	její
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
zdroj	zdroj	k1gInSc4	zdroj
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
největší	veliký	k2eAgInPc1d3	veliký
přítoky	přítok	k1gInPc1	přítok
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
jsou	být	k5eAaImIp3nP	být
Velká	velká	k1gFnSc1	velká
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
jen	jen	k9	jen
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
s	s	k7c7	s
493	[number]	k4	493
km	km	kA	km
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sáva	Sáva	k1gFnSc1	Sáva
a	a	k8xC	a
Tisa	Tisa	k1gFnSc1	Tisa
Jediná	jediný	k2eAgFnSc1d1	jediná
výrazná	výrazný	k2eAgFnSc1d1	výrazná
výjimka	výjimka	k1gFnSc1	výjimka
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Pčinja	Pčinja	k1gFnSc1	Pčinja
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
tvaru	tvar	k1gInSc3	tvar
terénu	terén	k1gInSc2	terén
jsou	být	k5eAaImIp3nP	být
přírodní	přírodní	k2eAgNnPc1d1	přírodní
jezera	jezero	k1gNnPc1	jezero
pouze	pouze	k6eAd1	pouze
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
malé	malý	k2eAgInPc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
Palićko	Palićko	k1gNnSc1	Palićko
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
6	[number]	k4	6
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
přírodním	přírodní	k2eAgNnSc7d1	přírodní
jezerem	jezero	k1gNnSc7	jezero
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
četná	četný	k2eAgNnPc4d1	četné
mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
ramena	rameno	k1gNnPc4	rameno
podél	podél	k7c2	podél
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Zasavica	Zasavicus	k1gMnSc2	Zasavicus
a	a	k8xC	a
Carska	Carsek	k1gMnSc2	Carsek
bara	barus	k1gMnSc2	barus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
četná	četný	k2eAgNnPc4d1	četné
umělá	umělý	k2eAgNnPc4d1	umělé
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
postavené	postavený	k2eAgFnPc1d1	postavená
jako	jako	k8xC	jako
vodní	vodní	k2eAgFnPc1d1	vodní
přehrady	přehrada	k1gFnPc1	přehrada
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc1d3	veliký
je	být	k5eAaImIp3nS	být
přehrada	přehrada	k1gFnSc1	přehrada
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
Železná	železný	k2eAgFnSc1d1	železná
vrata	vrata	k1gNnPc4	vrata
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
163	[number]	k4	163
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c6	na
srbské	srbský	k2eAgFnSc6d1	Srbská
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
;	;	kIx,	;
celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
253	[number]	k4	253
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
(	(	kIx(	(
<g/>
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hloubkou	hloubka	k1gFnSc7	hloubka
92	[number]	k4	92
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
umělé	umělý	k2eAgNnSc1d1	umělé
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
přehrada	přehrada	k1gFnSc1	přehrada
Perucac	Perucac	k1gFnSc1	Perucac
na	na	k7c6	na
Drinu	Drin	k1gInSc6	Drin
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
umělé	umělý	k2eAgNnSc1d1	umělé
jezero	jezero	k1gNnSc1	jezero
Vlasinsko	Vlasinsko	k1gNnSc1	Vlasinsko
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
vodopád	vodopád	k1gInSc1	vodopád
Jelovarnik	Jelovarnik	k1gInSc1	Jelovarnik
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
v	v	k7c6	v
Kopaonik	Kopaonik	k1gMnSc1	Kopaonik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
71	[number]	k4	71
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
Relativně	relativně	k6eAd1	relativně
hojné	hojný	k2eAgNnSc1d1	hojné
množství	množství	k1gNnSc1	množství
neznečištěných	znečištěný	k2eNgFnPc2d1	neznečištěná
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
četné	četný	k2eAgFnPc1d1	četná
podzemní	podzemní	k2eAgFnPc1d1	podzemní
přírodní	přírodní	k2eAgFnPc1d1	přírodní
a	a	k8xC	a
minerální	minerální	k2eAgInPc1d1	minerální
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kvality	kvalita	k1gFnSc2	kvalita
vody	voda	k1gFnSc2	voda
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
export	export	k1gInSc4	export
a	a	k8xC	a
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
zlepšení	zlepšení	k1gNnSc4	zlepšení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Srbsko	Srbsko	k1gNnSc4	Srbsko
je	být	k5eAaImIp3nS	být
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
vícestranickým	vícestranický	k2eAgInSc7d1	vícestranický
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
získalo	získat	k5eAaPmAgNnS	získat
plnou	plný	k2eAgFnSc4d1	plná
nezávislost	nezávislost	k1gFnSc4	nezávislost
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
z	z	k7c2	z
federace	federace	k1gFnSc2	federace
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
má	mít	k5eAaImIp3nS	mít
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
se	s	k7c7	s
188	[number]	k4	188
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Vatikánem	Vatikán	k1gInSc7	Vatikán
<g/>
,	,	kIx,	,
Maltézským	maltézský	k2eAgInSc7d1	maltézský
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
síť	síť	k1gFnSc1	síť
65	[number]	k4	65
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
a	a	k8xC	a
23	[number]	k4	23
konzulátů	konzulát	k1gInPc2	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
65	[number]	k4	65
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
ambasád	ambasáda	k1gFnPc2	ambasáda
<g/>
,	,	kIx,	,
5	[number]	k4	5
konzulátů	konzulát	k1gInPc2	konzulát
a	a	k8xC	a
4	[number]	k4	4
styčné	styčný	k2eAgFnSc2d1	styčná
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Srbska	Srbsko	k1gNnSc2	Srbsko
nesou	nést	k5eAaImIp3nP	nést
název	název	k1gInSc1	název
Vojska	vojsko	k1gNnSc2	vojsko
Srbije	Srbije	k1gFnSc2	Srbije
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
В	В	k?	В
С	С	k?	С
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byly	být	k5eAaImAgFnP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
nařízením	nařízení	k1gNnSc7	nařízení
srbské	srbský	k2eAgFnSc2d1	Srbská
Skupštiny	skupština	k1gFnSc2	skupština
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
srbská	srbský	k2eAgFnSc1d1	Srbská
armáda	armáda	k1gFnSc1	armáda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
jednotek	jednotka	k1gFnPc2	jednotka
armády	armáda	k1gFnSc2	armáda
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Republiky	republika	k1gFnSc2	republika
Srbsko	Srbsko	k1gNnSc4	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
ústavy	ústava	k1gFnSc2	ústava
Srbska	Srbsko	k1gNnSc2	Srbsko
slouží	sloužit	k5eAaImIp3nS	sloužit
armáda	armáda	k1gFnSc1	armáda
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
země	zem	k1gFnSc2	zem
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
zvenčí	zvenčí	k6eAd1	zvenčí
a	a	k8xC	a
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
různých	různý	k2eAgFnPc2d1	různá
misí	mise	k1gFnPc2	mise
a	a	k8xC	a
cílů	cíl	k1gInPc2	cíl
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákony	zákon	k1gInPc7	zákon
a	a	k8xC	a
principy	princip	k1gInPc1	princip
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
použití	použití	k1gNnSc4	použití
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nasazení	nasazení	k1gNnSc1	nasazení
armády	armáda	k1gFnSc2	armáda
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Republiky	republika	k1gFnSc2	republika
Srbsko	Srbsko	k1gNnSc4	Srbsko
může	moct	k5eAaImIp3nS	moct
schválit	schválit	k5eAaPmF	schválit
pouze	pouze	k6eAd1	pouze
Skupština	skupština	k1gFnSc1	skupština
<g/>
.	.	kIx.	.
</s>
<s>
Srbské	srbský	k2eAgFnPc1d1	Srbská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
samostaných	samostaný	k2eAgInPc2d1	samostaný
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
Pozemního	pozemní	k2eAgNnSc2d1	pozemní
vojska	vojsko	k1gNnSc2	vojsko
(	(	kIx(	(
<g/>
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgMnPc4d1	velký
výrobce	výrobce	k1gMnPc4	výrobce
a	a	k8xC	a
vývozce	vývozce	k1gMnPc4	vývozce
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
vyvezeno	vyvézt	k5eAaPmNgNnS	vyvézt
vybavení	vybavení	k1gNnSc1	vybavení
za	za	k7c4	za
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zbrojní	zbrojní	k2eAgInSc1d1	zbrojní
průmysl	průmysl	k1gInSc1	průmysl
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
za	za	k7c4	za
leta	letus	k1gMnSc4	letus
významného	významný	k2eAgInSc2d1	významný
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Centrálního	centrální	k2eAgNnSc2d1	centrální
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
však	však	k9	však
netvoří	tvořit	k5eNaImIp3nP	tvořit
správní	správní	k2eAgInSc4d1	správní
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
a	a	k8xC	a
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
srbskou	srbský	k2eAgFnSc7d1	Srbská
kontrolou	kontrola	k1gFnSc7	kontrola
také	také	k6eAd1	také
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
poté	poté	k6eAd1	poté
k	k	k7c3	k
Srbsku	Srbsko	k1gNnSc3	Srbsko
náleželo	náležet	k5eAaImAgNnS	náležet
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
již	již	k6eAd1	již
jen	jen	k9	jen
formálně	formálně	k6eAd1	formálně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
SFRJ	SFRJ	kA	SFRJ
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
s	s	k7c7	s
Kosovem	Kosovo	k1gNnSc7	Kosovo
srbskými	srbský	k2eAgInPc7d1	srbský
autonomní	autonomní	k2eAgMnPc1d1	autonomní
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
velice	velice	k6eAd1	velice
širokou	široký	k2eAgFnSc4d1	široká
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
autonomie	autonomie	k1gFnSc1	autonomie
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
i	i	k9	i
nadále	nadále	k6eAd1	nadále
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
srbské	srbský	k2eAgNnSc1d1	srbské
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
vrácen	vrátit	k5eAaPmNgInS	vrátit
dřívější	dřívější	k2eAgInSc1d1	dřívější
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Kosovo	Kosův	k2eAgNnSc4d1	Kosovo
a	a	k8xC	a
Metohija	Metohij	k2eAgFnSc1d1	Metohija
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
oblastí	oblast	k1gFnSc7	oblast
pod	pod	k7c7	pod
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
správou	správa	k1gFnSc7	správa
garantovanou	garantovaný	k2eAgFnSc7d1	garantovaná
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyňata	vynít	k5eAaPmNgFnS	vynít
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
srbské	srbský	k2eAgFnSc2d1	Srbská
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
garantovaného	garantovaný	k2eAgInSc2d1	garantovaný
Chartou	charta	k1gFnSc7	charta
OSN	OSN	kA	OSN
i	i	k8xC	i
usnesením	usnesení	k1gNnSc7	usnesení
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
o	o	k7c6	o
územní	územní	k2eAgFnSc6d1	územní
celistvosti	celistvost	k1gFnSc6	celistvost
OSN	OSN	kA	OSN
bylo	být	k5eAaImAgNnS	být
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
však	však	k9	však
kosovský	kosovský	k2eAgInSc1d1	kosovský
parlament	parlament	k1gInSc1	parlament
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
v	v	k7c4	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
SEČ	SEČ	kA	SEČ
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
za	za	k7c4	za
demokratický	demokratický	k2eAgInSc4d1	demokratický
<g/>
,	,	kIx,	,
světský	světský	k2eAgInSc4d1	světský
a	a	k8xC	a
multietnický	multietnický	k2eAgInSc4d1	multietnický
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
uznán	uznat	k5eAaPmNgInS	uznat
mnoha	mnoho	k4c7	mnoho
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
Srbsko	Srbsko	k1gNnSc1	Srbsko
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
historických	historický	k2eAgFnPc2d1	historická
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
(	(	kIx(	(
<g/>
bývalého	bývalý	k2eAgInSc2d1	bývalý
regionu	region	k1gInSc2	region
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
)	)	kIx)	)
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Bačku	baček	k1gInSc3	baček
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Banátu	Banát	k1gInSc2	Banát
<g/>
,	,	kIx,	,
Srem	Srem	k1gInSc1	Srem
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
nepatrnou	patrný	k2eNgFnSc4d1	patrný
část	část	k1gFnSc4	část
původního	původní	k2eAgNnSc2d1	původní
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
'	'	kIx"	'
<g/>
centrálního	centrální	k2eAgNnSc2d1	centrální
<g/>
'	'	kIx"	'
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
regiony	region	k1gInPc4	region
<g/>
:	:	kIx,	:
Šumadija	Šumadija	k1gFnSc1	Šumadija
<g/>
,	,	kIx,	,
Mačva	Mačva	k1gFnSc1	Mačva
<g/>
,	,	kIx,	,
Stig	Stig	k1gInSc1	Stig
<g/>
,	,	kIx,	,
Negotinska	Negotinsko	k1gNnPc1	Negotinsko
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
Brankovina	Brankovina	k1gFnSc1	Brankovina
<g/>
,	,	kIx,	,
Sandžak	sandžak	k1gInSc1	sandžak
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
slabší	slabý	k2eAgMnSc1d2	slabší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
slabé	slabý	k2eAgFnSc2d1	slabá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
však	však	k9	však
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
roste	růst	k5eAaImIp3nS	růst
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
a	a	k8xC	a
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
se	se	k3xPyFc4	se
také	také	k9	také
pomalu	pomalu	k6eAd1	pomalu
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
ekonomika	ekonomika	k1gFnSc1	ekonomika
za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
zde	zde	k6eAd1	zde
soukromé	soukromý	k2eAgFnPc1d1	soukromá
firmy	firma	k1gFnPc1	firma
a	a	k8xC	a
lepší	dobrý	k2eAgFnPc1d2	lepší
byly	být	k5eAaImAgFnP	být
i	i	k9	i
politické	politický	k2eAgInPc4d1	politický
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
západem	západ	k1gInSc7	západ
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
komunistickým	komunistický	k2eAgInPc3d1	komunistický
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
hroutit	hroutit	k5eAaImF	hroutit
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
nastaly	nastat	k5eAaPmAgInP	nastat
národnostní	národnostní	k2eAgInPc1d1	národnostní
problémy	problém	k1gInPc1	problém
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
i	i	k9	i
zisky	zisk	k1gInPc1	zisk
firem	firma	k1gFnPc2	firma
dostávaly	dostávat	k5eAaImAgInP	dostávat
do	do	k7c2	do
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
krizi	krize	k1gFnSc4	krize
však	však	k9	však
srbská	srbský	k2eAgFnSc1d1	Srbská
ekonomika	ekonomika	k1gFnSc1	ekonomika
zažila	zažít	k5eAaPmAgFnS	zažít
v	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastala	nastat	k5eAaPmAgFnS	nastat
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvořily	tvořit	k5eAaImAgInP	tvořit
rozpadající	rozpadající	k2eAgFnSc3d1	rozpadající
se	se	k3xPyFc4	se
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Politici	politik	k1gMnPc1	politik
financovali	financovat	k5eAaBmAgMnP	financovat
hlavně	hlavně	k6eAd1	hlavně
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
uvaleny	uvalit	k5eAaPmNgFnP	uvalit
od	od	k7c2	od
OSN	OSN	kA	OSN
sankce	sankce	k1gFnSc2	sankce
za	za	k7c4	za
způsobení	způsobení	k1gNnSc4	způsobení
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
však	však	k9	však
ekonomika	ekonomika	k1gFnSc1	ekonomika
nerozvíjela	rozvíjet	k5eNaImAgFnS	rozvíjet
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
dalšímu	další	k2eAgInSc3d1	další
národnostnímu	národnostní	k2eAgInSc3d1	národnostní
problému	problém	k1gInSc3	problém
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
začala	začít	k5eAaPmAgFnS	začít
vzpamatovávat	vzpamatovávat	k5eAaImF	vzpamatovávat
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Srbsku	Srbsko	k1gNnSc6	Srbsko
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Balkánský	balkánský	k2eAgMnSc1d1	balkánský
tygr	tygr	k1gMnSc1	tygr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
nejrychleji	rychle	k6eAd3	rychle
rozvíjející	rozvíjející	k2eAgFnSc3d1	rozvíjející
se	se	k3xPyFc4	se
ekonomice	ekonomika	k1gFnSc3	ekonomika
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
evropskou	evropský	k2eAgFnSc7d1	Evropská
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podepsané	podepsaný	k2eAgFnPc4d1	podepsaná
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
jak	jak	k8xC	jak
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
tak	tak	k6eAd1	tak
i	i	k9	i
s	s	k7c7	s
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
západem	západ	k1gInSc7	západ
a	a	k8xC	a
východem	východ	k1gInSc7	východ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
buduje	budovat	k5eAaImIp3nS	budovat
hlavně	hlavně	k9	hlavně
silniční	silniční	k2eAgFnSc4d1	silniční
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velké	velký	k2eAgInPc1d1	velký
plány	plán	k1gInPc1	plán
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
okolo	okolo	k7c2	okolo
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc4d1	postavený
obchvat	obchvat	k1gInSc4	obchvat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nekvalitní	kvalitní	k2eNgFnPc4d1	nekvalitní
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
silnice	silnice	k1gFnPc4	silnice
jsou	být	k5eAaImIp3nP	být
dálnice	dálnice	k1gFnPc1	dálnice
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
dálniční	dálniční	k2eAgInPc1d1	dálniční
tahy	tah	k1gInPc1	tah
vedou	vést	k5eAaImIp3nP	vést
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
s	s	k7c7	s
Makedonií	Makedonie	k1gFnSc7	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
březích	březí	k2eAgInPc2d1	březí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
významné	významný	k2eAgInPc1d1	významný
podniky	podnik	k1gInPc1	podnik
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
silami	síla	k1gFnPc7	síla
NATO	NATO	kA	NATO
byly	být	k5eAaImAgInP	být
zničeny	zničen	k2eAgInPc1d1	zničen
mosty	most	k1gInPc1	most
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Sadu	sad	k1gInSc6	sad
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
dopravní	dopravní	k2eAgFnSc1d1	dopravní
tepna	tepna	k1gFnSc1	tepna
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hlavními	hlavní	k2eAgInPc7d1	hlavní
tahy	tah	k1gInPc7	tah
<g/>
,	,	kIx,	,
vedoucími	vedoucí	k1gMnPc7	vedoucí
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
bývalých	bývalý	k2eAgFnPc2d1	bývalá
jugoslávských	jugoslávský	k2eAgFnPc2d1	jugoslávská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc1	síť
velmi	velmi	k6eAd1	velmi
hustá	hustý	k2eAgFnSc1d1	hustá
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
tratí	trať	k1gFnPc2	trať
vedoucích	vedoucí	k1gMnPc2	vedoucí
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
např.	např.	kA	např.
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
charakteru	charakter	k1gInSc3	charakter
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
sever	sever	k1gInSc1	sever
je	být	k5eAaImIp3nS	být
průmyslově	průmyslově	k6eAd1	průmyslově
i	i	k9	i
zemědělsky	zemědělsky	k6eAd1	zemědělsky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
jih	jih	k1gInSc1	jih
skýtá	skýtat	k5eAaImIp3nS	skýtat
jak	jak	k8xC	jak
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Niš	Niš	k1gMnSc2	Niš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
významné	významný	k2eAgInPc4d1	významný
koridory	koridor	k1gInPc4	koridor
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
do	do	k7c2	do
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgInSc1d1	silniční
Celým	celý	k2eAgNnSc7d1	celé
Srbskem	Srbsko	k1gNnSc7	Srbsko
prochází	procházet	k5eAaImIp3nS	procházet
dálnice	dálnice	k1gFnSc1	dálnice
Subotica	Subotic	k1gInSc2	Subotic
–	–	k?	–
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
–	–	k?	–
Niš	Niš	k1gFnSc2	Niš
<g/>
,	,	kIx,	,
s	s	k7c7	s
napojeními	napojení	k1gNnPc7	napojení
i	i	k8xC	i
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sítě	síť	k1gFnSc2	síť
silniční	silniční	k2eAgFnSc2d1	silniční
se	se	k3xPyFc4	se
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
situace	situace	k1gFnSc2	situace
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
automobilové	automobilový	k2eAgNnSc1d1	automobilové
hlavním	hlavní	k2eAgInSc7d1	hlavní
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
několik	několik	k4yIc4	několik
hlavních	hlavní	k2eAgInPc2d1	hlavní
tahů	tah	k1gInPc2	tah
<g/>
)	)	kIx)	)
ve	v	k7c6	v
velice	velice	k6eAd1	velice
neutěšeném	utěšený	k2eNgInSc6d1	neutěšený
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
Albánii	Albánie	k1gFnSc6	Albánie
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
nejhorší	zlý	k2eAgNnSc4d3	nejhorší
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
-	-	kIx~	-
0	[number]	k4	0
<g/>
5.201	[number]	k4	5.201
<g/>
7	[number]	k4	7
již	již	k6eAd1	již
okresní	okresní	k2eAgFnPc4d1	okresní
silnice	silnice	k1gFnPc4	silnice
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
hlavní	hlavní	k2eAgFnSc2d1	hlavní
dálnice	dálnice	k1gFnSc2	dálnice
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
je	být	k5eAaImIp3nS	být
placená	placený	k2eAgFnSc1d1	placená
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
se	se	k3xPyFc4	se
mýto	mýto	k1gNnSc4	mýto
na	na	k7c6	na
mýtních	mýtní	k2eAgFnPc6d1	mýtní
branách	brána	k1gFnPc6	brána
a	a	k8xC	a
lze	lze	k6eAd1	lze
platit	platit	k5eAaImF	platit
v	v	k7c6	v
dinárech	dinár	k1gInPc6	dinár
<g/>
,	,	kIx,	,
eurech	euro	k1gNnPc6	euro
a	a	k8xC	a
nově	nově	k6eAd1	nově
i	i	k9	i
platební	platební	k2eAgFnSc7d1	platební
kartou	karta	k1gFnSc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc2d1	maximální
povolené	povolený	k2eAgFnSc2d1	povolená
rychlosti	rychlost	k1gFnSc2	rychlost
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
vozy	vůz	k1gInPc4	vůz
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
:	:	kIx,	:
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
mimo	mimo	k7c4	mimo
obec	obec	k1gFnSc4	obec
<g/>
:	:	kIx,	:
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
silnice	silnice	k1gFnSc1	silnice
<g/>
:	:	kIx,	:
130	[number]	k4	130
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
:	:	kIx,	:
120	[number]	k4	120
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Většinovým	většinový	k2eAgNnSc7d1	většinové
etnikem	etnikum	k1gNnSc7	etnikum
jsou	být	k5eAaImIp3nP	být
Srbové	Srb	k1gMnPc1	Srb
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
82	[number]	k4	82
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
národem	národ	k1gInSc7	národ
jsou	být	k5eAaImIp3nP	být
Albánci	Albánec	k1gMnPc1	Albánec
v	v	k7c6	v
Srbskem	Srbsko	k1gNnSc7	Srbsko
neuznaném	uznaný	k2eNgInSc6d1	neuznaný
Kosovu	Kosův	k2eAgInSc3d1	Kosův
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
zde	zde	k6eAd1	zde
kolem	kolem	k7c2	kolem
90	[number]	k4	90
<g/>
%	%	kIx~	%
kosovské	kosovský	k2eAgFnSc2d1	Kosovská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
vedle	vedle	k7c2	vedle
Srbů	Srb	k1gMnPc2	Srb
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
největší	veliký	k2eAgFnSc4d3	veliký
menšinu	menšina	k1gFnSc4	menšina
zde	zde	k6eAd1	zde
tvoří	tvořit	k5eAaImIp3nP	tvořit
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
Maďarů	Maďar	k1gMnPc2	Maďar
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
,	,	kIx,	,
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
Rusínů	Rusín	k1gMnPc2	Rusín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
však	však	k9	však
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
nepočetná	početný	k2eNgFnSc1d1	nepočetná
česká	český	k2eAgFnSc1d1	Česká
menšina	menšina	k1gFnSc1	menšina
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
početnější	početní	k2eAgFnSc1d2	početnější
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
pravoslaví	pravoslaví	k1gNnPc4	pravoslaví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
hlavně	hlavně	k9	hlavně
Srbové	Srb	k1gMnPc1	Srb
a	a	k8xC	a
Rumuni	Rumun	k1gMnPc1	Rumun
<g/>
.	.	kIx.	.
</s>
<s>
Albánci	Albánec	k1gMnPc1	Albánec
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
Maďaři	Maďar	k1gMnPc1	Maďar
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Srbska	Srbsko	k1gNnSc2	Srbsko
v	v	k7c6	v
Sandžaku	sandžak	k1gInSc6	sandžak
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
islám	islám	k1gInSc4	islám
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
Bosňáci	Bosňáci	k?	Bosňáci
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
muslimští	muslimský	k2eAgMnPc1d1	muslimský
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
etnikem	etnikum	k1gNnSc7	etnikum
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
stejné	stejný	k2eAgNnSc4d1	stejné
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
srbština	srbština	k1gFnSc1	srbština
<g/>
,	,	kIx,	,
standardizovaná	standardizovaný	k2eAgFnSc1d1	standardizovaná
forma	forma	k1gFnSc1	forma
srbochorvatšiny	srbochorvatšina	k1gFnSc2	srbochorvatšina
<g/>
,	,	kIx,	,
s	s	k7c7	s
88	[number]	k4	88
<g/>
%	%	kIx~	%
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Srbština	srbština	k1gFnSc1	srbština
je	být	k5eAaImIp3nS	být
také	také	k9	také
jediný	jediný	k2eAgInSc1d1	jediný
evropský	evropský	k2eAgInSc1d1	evropský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
aktivně	aktivně	k6eAd1	aktivně
používá	používat	k5eAaImIp3nS	používat
jak	jak	k8xC	jak
cyrilici	cyrilice	k1gFnSc4	cyrilice
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
latinku	latinka	k1gFnSc4	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
cyrilice	cyrilice	k1gFnSc1	cyrilice
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
abecedou	abeceda	k1gFnSc7	abeceda
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
srbským	srbský	k2eAgMnSc7d1	srbský
filologem	filolog	k1gMnSc7	filolog
Vukem	Vuk	k1gMnSc7	Vuk
Karadžičem	Karadžič	k1gMnSc7	Karadžič
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
abecedu	abeceda	k1gFnSc4	abeceda
na	na	k7c6	na
fonemických	fonemický	k2eAgInPc6d1	fonemický
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
47	[number]	k4	47
<g/>
%	%	kIx~	%
srbské	srbský	k2eAgFnSc2d1	Srbská
populace	populace	k1gFnSc2	populace
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
latinku	latinka	k1gFnSc4	latinka
<g/>
,	,	kIx,	,
36	[number]	k4	36
<g/>
%	%	kIx~	%
cyrilici	cyrilice	k1gFnSc6	cyrilice
a	a	k8xC	a
17	[number]	k4	17
<g/>
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
se	se	k3xPyFc4	se
Srbsko	Srbsko	k1gNnSc1	Srbsko
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
<g/>
,	,	kIx,	,
území	území	k1gNnSc2	území
Srbska	Srbsko	k1gNnSc2	Srbsko
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
východní	východní	k2eAgFnSc4d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
polovinu	polovina	k1gFnSc4	polovina
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mezi	mezi	k7c7	mezi
Byzancí	Byzanc	k1gFnSc7	Byzanc
a	a	k8xC	a
Uherským	uherský	k2eAgInSc7d1	uherský
království	království	k1gNnSc6	království
a	a	k8xC	a
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
mezi	mezi	k7c7	mezi
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
překrývající	překrývající	k2eAgMnSc1d1	překrývající
se	se	k3xPyFc4	se
vlivy	vliv	k1gInPc1	vliv
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
kulturní	kulturní	k2eAgFnSc4d1	kulturní
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
profil	profil	k1gInSc4	profil
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jih	jih	k1gInSc1	jih
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
širší	široký	k2eAgInSc4d2	širší
Balkán	Balkán	k1gInSc4	Balkán
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
Středomoří	středomoří	k1gNnSc1	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Byzantský	byzantský	k2eAgInSc4d1	byzantský
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
byl	být	k5eAaImAgInS	být
siný	siný	k2eAgInSc1d1	siný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
díky	díky	k7c3	díky
rozšíření	rozšíření	k1gNnSc3	rozšíření
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
trvalý	trvalý	k2eAgInSc4d1	trvalý
statut	statut	k1gInSc4	statut
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
srbských	srbský	k2eAgInPc2d1	srbský
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
nejcennější	cenný	k2eAgFnPc4d3	nejcennější
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
monumentů	monument	k1gInPc2	monument
zapsaných	zapsaný	k2eAgInPc2d1	zapsaný
v	v	k7c6	v
kulturním	kulturní	k2eAgNnSc6d1	kulturní
dědictví	dědictví	k1gNnSc6	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
:	:	kIx,	:
dřívější	dřívější	k2eAgFnSc4d1	dřívější
středověké	středověký	k2eAgNnSc4d1	středověké
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Stari	Star	k1gFnSc2	Star
Ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Sopoćani	Sopoćan	k1gMnPc1	Sopoćan
<g/>
;	;	kIx,	;
klášter	klášter	k1gInSc1	klášter
Studenica	Studenic	k1gInSc2	Studenic
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
římský	římský	k2eAgInSc1d1	římský
komplex	komplex	k1gInSc1	komplex
Gamzigrad	Gamzigrada	k1gFnPc2	Gamzigrada
<g/>
–	–	k?	–
<g/>
Felix	Felix	k1gMnSc1	Felix
Romuliana	Romulian	k1gMnSc2	Romulian
<g/>
;	;	kIx,	;
středověké	středověký	k2eAgInPc1d1	středověký
náhrobky	náhrobek	k1gInPc1	náhrobek
Stećci	Stećce	k1gFnSc4	Stećce
<g/>
;	;	kIx,	;
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
středověké	středověký	k2eAgFnPc1d1	středověká
památky	památka	k1gFnPc1	památka
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
(	(	kIx(	(
<g/>
klášter	klášter	k1gInSc1	klášter
Visoki	Visok	k1gFnSc2	Visok
Dečani	Dečaň	k1gFnSc3	Dečaň
<g/>
,	,	kIx,	,
Chrám	chrám	k1gInSc4	chrám
Bohorodičky	Bohorodička	k1gFnSc2	Bohorodička
Ljevišské	Ljevišský	k2eAgFnSc2d1	Ljevišský
<g/>
,	,	kIx,	,
monastýr	monastýr	k1gInSc1	monastýr
Gračanica	Gračanica	k1gFnSc1	Gračanica
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
Peć	Peć	k1gFnSc2	Peć
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
srbským	srbský	k2eAgMnSc7d1	srbský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
je	být	k5eAaImIp3nS	být
Ivo	Ivo	k1gMnSc1	Ivo
Andrić	Andrić	k1gMnSc1	Andrić
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Milorad	Milorada	k1gFnPc2	Milorada
Pavić	Pavić	k1gMnSc1	Pavić
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
především	především	k9	především
svým	svůj	k3xOyFgInSc7	svůj
románem	román	k1gInSc7	román
Chazarský	chazarský	k2eAgInSc4d1	chazarský
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
k	k	k7c3	k
postmodernismu	postmodernismus	k1gInSc3	postmodernismus
byl	být	k5eAaImAgMnS	být
řazen	řadit	k5eAaImNgMnS	řadit
Danilo	danit	k5eAaImAgNnS	danit
Kiš	kiš	k0	kiš
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
prorazil	prorazit	k5eAaPmAgMnS	prorazit
spisovatel	spisovatel	k1gMnSc1	spisovatel
Prvoslav	Prvoslav	k1gMnSc1	Prvoslav
Vujčić	Vujčić	k1gMnSc1	Vujčić
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
jako	jako	k8xC	jako
dramatik	dramatik	k1gMnSc1	dramatik
proslul	proslout	k5eAaPmAgMnS	proslout
Branislav	Branislav	k1gMnSc1	Branislav
Nušić	Nušić	k1gMnSc1	Nušić
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
získala	získat	k5eAaPmAgFnS	získat
světový	světový	k2eAgInSc4d1	světový
věhlas	věhlas	k1gInSc4	věhlas
Marina	Marina	k1gFnSc1	Marina
Abramovićová	Abramovićová	k1gFnSc1	Abramovićová
<g/>
,	,	kIx,	,
představitelka	představitelka	k1gFnSc1	představitelka
performance	performance	k1gFnSc2	performance
artu	aríst	k5eAaPmIp1nS	aríst
a	a	k8xC	a
body	bod	k1gInPc4	bod
artu	artus	k1gInSc2	artus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
filmové	filmový	k2eAgFnSc2d1	filmová
školy	škola	k1gFnSc2	škola
nakonec	nakonec	k6eAd1	nakonec
nejvíce	hodně	k6eAd3	hodně
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
režisér	režisér	k1gMnSc1	režisér
Emir	Emir	k1gMnSc1	Emir
Kusturica	Kusturica	k1gMnSc1	Kusturica
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěšným	úspěšný	k2eAgMnPc3d1	úspěšný
režisérům	režisér	k1gMnPc3	režisér
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Dušan	Dušan	k1gMnSc1	Dušan
Makavejev	Makavejev	k1gMnSc1	Makavejev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modelingu	modeling	k1gInSc6	modeling
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Emina	Emin	k2eAgFnSc1d1	Emina
Jahovićová	Jahovićová	k1gFnSc1	Jahovićová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
země	zem	k1gFnPc1	zem
pronikli	proniknout	k5eAaPmAgMnP	proniknout
Željko	Željka	k1gFnSc5	Željka
Joksimović	Joksimović	k1gMnSc4	Joksimović
či	či	k8xC	či
Lepa	Lepus	k1gMnSc4	Lepus
Brena	Bren	k1gMnSc4	Bren
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
srbským	srbský	k2eAgMnSc7d1	srbský
vědcem	vědec	k1gMnSc7	vědec
a	a	k8xC	a
Srbem	Srb	k1gMnSc7	Srb
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
fyzik	fyzik	k1gMnSc1	fyzik
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Matematička	matematička	k1gFnSc1	matematička
a	a	k8xC	a
fyzička	fyzička	k1gFnSc1	fyzička
Mileva	Mileva	k1gFnSc1	Mileva
Marićová	Marićová	k1gFnSc1	Marićová
výrazně	výrazně	k6eAd1	výrazně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
objevům	objev	k1gInPc3	objev
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc4	Einstein
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Geofyzik	geofyzik	k1gMnSc1	geofyzik
Milutin	Milutin	k1gMnSc1	Milutin
Milanković	Milanković	k1gMnSc1	Milanković
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
teorií	teorie	k1gFnSc7	teorie
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
astronomických	astronomický	k2eAgInPc2d1	astronomický
faktorů	faktor	k1gInPc2	faktor
na	na	k7c4	na
zemské	zemský	k2eAgNnSc4d1	zemské
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Michael	Michael	k1gMnSc1	Michael
Pupin	Pupin	k1gMnSc1	Pupin
výrazně	výrazně	k6eAd1	výrazně
zkvalitnil	zkvalitnit	k5eAaPmAgMnS	zkvalitnit
telefonní	telefonní	k2eAgFnSc4d1	telefonní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Osvícencem	osvícenec	k1gMnSc7	osvícenec
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
moderní	moderní	k2eAgFnSc2d1	moderní
srbské	srbský	k2eAgFnSc2d1	Srbská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
byl	být	k5eAaImAgMnS	být
Dositej	Dositej	k1gMnSc1	Dositej
Obradović	Obradović	k1gMnSc1	Obradović
<g/>
.	.	kIx.	.
</s>
<s>
Reformátorem	reformátor	k1gMnSc7	reformátor
srbského	srbský	k2eAgInSc2d1	srbský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
národním	národní	k2eAgMnSc7d1	národní
buditelem	buditel	k1gMnSc7	buditel
byl	být	k5eAaImAgMnS	být
Vuk	Vuk	k1gMnSc1	Vuk
Karadžić	Karadžić	k1gMnSc1	Karadžić
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
Georgij	Georgij	k1gMnSc1	Georgij
Aleksandrovič	Aleksandrovič	k1gMnSc1	Aleksandrovič
Ostrogorskij	Ostrogorskij	k1gMnSc1	Ostrogorskij
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
své	svůj	k3xOyFgNnSc4	svůj
oceňované	oceňovaný	k2eAgNnSc4d1	oceňované
dílo	dílo	k1gNnSc4	dílo
tématu	téma	k1gNnSc2	téma
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
srbská	srbský	k2eAgFnSc1d1	Srbská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nesmírně	smírně	k6eNd1	smírně
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Fenomenálním	fenomenální	k2eAgMnSc7d1	fenomenální
tenistou	tenista	k1gMnSc7	tenista
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
především	především	k9	především
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Monika	Monika	k1gFnSc1	Monika
Selešová	Selešová	k1gFnSc1	Selešová
<g/>
,	,	kIx,	,
Ana	Ana	k1gFnSc1	Ana
Ivanovićová	Ivanovićová	k1gFnSc1	Ivanovićová
a	a	k8xC	a
Jelena	Jelena	k1gFnSc1	Jelena
Jankovićová	Jankovićová	k1gFnSc1	Jankovićová
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc4	uznání
došli	dojít	k5eAaPmAgMnP	dojít
i	i	k9	i
fotbaloví	fotbalový	k2eAgMnPc1d1	fotbalový
trenéři	trenér	k1gMnPc1	trenér
Bora	Bora	k1gMnSc1	Bora
Milutinović	Milutinović	k1gMnSc1	Milutinović
a	a	k8xC	a
Vujadin	Vujadin	k2eAgInSc1d1	Vujadin
Boškov	Boškov	k1gInSc1	Boškov
<g/>
.	.	kIx.	.
</s>
