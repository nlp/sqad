<s>
Kvinterna	Kvinterna	k?	Kvinterna
(	(	kIx(	(
<g/>
gyterne	gyternout	k5eAaPmIp3nS	gyternout
<g/>
,	,	kIx,	,
gyttrone	gyttron	k1gInSc5	gyttron
<g/>
,	,	kIx,	,
gitteron	gitteron	k1gInSc4	gitteron
<g/>
,	,	kIx,	,
guiterne	guiternout	k5eAaPmIp3nS	guiternout
<g/>
,	,	kIx,	,
quinterne	quinternout	k5eAaPmIp3nS	quinternout
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
mandoře	mandora	k1gFnSc3	mandora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středověký	středověký	k2eAgInSc1d1	středověký
drnkací	drnkací	k2eAgInSc1d1	drnkací
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
plochou	plochý	k2eAgFnSc7d1	plochá
zadní	zadní	k2eAgFnSc7d1	zadní
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
třemi	tři	k4xCgNnPc7	tři
nebo	nebo	k8xC	nebo
čtyřmi	čtyři	k4xCgFnPc7	čtyři
střevovými	střevový	k2eAgFnPc7d1	střevový
strunami	struna	k1gFnPc7	struna
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
zdvojeném	zdvojený	k2eAgNnSc6d1	zdvojené
tažení	tažení	k1gNnSc6	tažení
a	a	k8xC	a
opražcovaným	opražcovaný	k2eAgInSc7d1	opražcovaný
hmatníkem	hmatník	k1gInSc7	hmatník
<g/>
.	.	kIx.	.
</s>
