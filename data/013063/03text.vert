<p>
<s>
Kvinterna	Kvinterna	k?	Kvinterna
(	(	kIx(	(
<g/>
gyterne	gyternout	k5eAaPmIp3nS	gyternout
<g/>
,	,	kIx,	,
gyttrone	gyttron	k1gInSc5	gyttron
<g/>
,	,	kIx,	,
gitteron	gitteron	k1gInSc4	gitteron
<g/>
,	,	kIx,	,
guiterne	guiternout	k5eAaPmIp3nS	guiternout
<g/>
,	,	kIx,	,
quinterne	quinternout	k5eAaPmIp3nS	quinternout
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
mandoře	mandora	k1gFnSc3	mandora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středověký	středověký	k2eAgInSc1d1	středověký
drnkací	drnkací	k2eAgInSc1d1	drnkací
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
plochou	plochý	k2eAgFnSc7d1	plochá
zadní	zadní	k2eAgFnSc7d1	zadní
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
třemi	tři	k4xCgNnPc7	tři
nebo	nebo	k8xC	nebo
čtyřmi	čtyři	k4xCgFnPc7	čtyři
střevovými	střevový	k2eAgFnPc7d1	střevový
strunami	struna	k1gFnPc7	struna
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
zdvojeném	zdvojený	k2eAgNnSc6d1	zdvojené
tažení	tažení	k1gNnSc6	tažení
a	a	k8xC	a
opražcovaným	opražcovaný	k2eAgInSc7d1	opražcovaný
hmatníkem	hmatník	k1gInSc7	hmatník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvinterna	Kvinterna	k?	Kvinterna
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejvíce	hodně	k6eAd3	hodně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
mezi	mezi	k7c7	mezi
dvanáctým	dvanáctý	k4xOgNnSc7	dvanáctý
a	a	k8xC	a
patnáctým	patnáctý	k4xOgNnSc7	patnáctý
stoletím	století	k1gNnSc7	století
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
její	její	k3xOp3gFnSc1	její
obliba	obliba	k1gFnSc1	obliba
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dále	daleko	k6eAd2	daleko
používal	používat	k5eAaImAgMnS	používat
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
nástroje	nástroj	k1gInPc4	nástroj
z	z	k7c2	z
kytarové	kytarový	k2eAgFnSc2d1	kytarová
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
začínala	začínat	k5eAaImAgFnS	začínat
v	v	k7c6	v
bolestech	bolest	k1gFnPc6	bolest
formovat	formovat	k5eAaImF	formovat
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
nástroje	nástroj	k1gInPc1	nástroj
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
monoxyly	monoxyl	k1gInPc1	monoxyl
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
různily	různit	k5eAaImAgFnP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
třináctého	třináctý	k4xOgNnSc2	třináctý
století	století	k1gNnSc2	století
měly	mít	k5eAaImAgInP	mít
dva	dva	k4xCgInPc1	dva
ozvučné	ozvučný	k2eAgInPc1d1	ozvučný
otvory	otvor	k1gInPc1	otvor
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
fiduly	fidul	k1gInPc1	fidul
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
dvě	dva	k4xCgNnPc1	dva
ladění	ladění	k1gNnPc1	ladění
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
středověkých	středověký	k2eAgInPc2d1	středověký
pramenů	pramen	k1gInPc2	pramen
příslušela	příslušet	k5eAaImAgFnS	příslušet
fidulám	fidulat	k5eAaImIp1nS	fidulat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
a-d	a	k?	a-d
<g/>
'	'	kIx"	'
<g/>
-a	-a	k?	-a
<g/>
'	'	kIx"	'
<g/>
-d	-d	k?	-d
a	a	k8xC	a
g-d	g	k?	g-d
<g/>
'	'	kIx"	'
<g/>
-g	-g	k?	-g
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
d.	d.	k?	d.
</s>
</p>
<p>
<s>
Žádné	žádný	k3yNgFnPc4	žádný
skladby	skladba	k1gFnPc4	skladba
určené	určený	k2eAgFnPc4d1	určená
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
kvinterny	kvinterny	k?	kvinterny
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
,	,	kIx,	,
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
můžeme	moct	k5eAaImIp1nP	moct
tvrdit	tvrdit	k5eAaImF	tvrdit
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
trsátkem	trsátko	k1gNnSc7	trsátko
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ladění	ladění	k1gNnSc1	ladění
kvinterny	kvinterny	k?	kvinterny
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jak	jak	k6eAd1	jak
vydrnkávání	vydrnkávání	k1gNnSc1	vydrnkávání
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
akordový	akordový	k2eAgInSc1d1	akordový
doprovod	doprovod	k1gInSc1	doprovod
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obého	obé	k1gNnSc2	obé
bylo	být	k5eAaImAgNnS	být
využíváno	využívat	k5eAaPmNgNnS	využívat
<g/>
,	,	kIx,	,
prvého	prvý	k4xOgNnSc2	prvý
zřejmě	zřejmě	k6eAd1	zřejmě
především	především	k6eAd1	především
k	k	k7c3	k
doprovodu	doprovod	k1gInSc3	doprovod
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
kvinterny	kvinterny	k?	kvinterny
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
konstantního	konstantní	k2eAgInSc2d1	konstantní
dronu	dron	k1gInSc2	dron
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
na	na	k7c4	na
hmatník	hmatník	k1gInSc4	hmatník
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
vůbec	vůbec	k9	vůbec
nesahají	sahat	k5eNaImIp3nP	sahat
<g/>
,	,	kIx,	,
odvážnější	odvážný	k2eAgInPc1d2	odvážnější
hrají	hrát	k5eAaImIp3nP	hrát
melodii	melodie	k1gFnSc4	melodie
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
struně	struna	k1gFnSc6	struna
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
nechávají	nechávat	k5eAaImIp3nP	nechávat
proznívat	proznívat	k5eAaImF	proznívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
