<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
vedla	vést	k5eAaImAgFnS	vést
Ruská	ruský	k2eAgFnSc1d1	ruská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
komunistického	komunistický	k2eAgInSc2d1	komunistický
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
Leninově	Leninův	k2eAgFnSc6d1	Leninova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
