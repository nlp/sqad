<p>
<s>
Jinja	Jinj	k1gInSc2	Jinj
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgInSc1d1	moderní
šablonovací	šablonovací	k2eAgInSc1d1	šablonovací
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
Python	Python	k1gMnSc1	Python
vycházející	vycházející	k2eAgFnSc2d1	vycházející
ze	z	k7c2	z
syntaxe	syntax	k1gFnSc2	syntax
Django	Django	k1gNnSc1	Django
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
výhody	výhoda	k1gFnPc4	výhoda
patří	patřit	k5eAaImIp3nS	patřit
přehlednost	přehlednost	k1gFnSc1	přehlednost
<g/>
,	,	kIx,	,
rozšířitelnost	rozšířitelnost	k1gFnSc1	rozšířitelnost
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
filtrů	filtr	k1gInPc2	filtr
a	a	k8xC	a
pluginů	plugin	k1gInPc2	plugin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snadnost	snadnost	k1gFnSc1	snadnost
použití	použití	k1gNnSc1	použití
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
se	se	k3xPyFc4	se
šablonovacím	šablonovací	k2eAgInSc7d1	šablonovací
systémem	systém	k1gInSc7	systém
Mako	mako	k1gNnSc1	mako
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
oddělit	oddělit	k5eAaPmF	oddělit
kód	kód	k1gInSc4	kód
aplikace	aplikace	k1gFnSc2	aplikace
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
prezentace	prezentace	k1gFnSc2	prezentace
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
kódování	kódování	k1gNnSc1	kódování
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
Jinja	Jinj	k1gInSc2	Jinj
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nahrazování	nahrazování	k1gNnSc4	nahrazování
v	v	k7c6	v
textových	textový	k2eAgInPc6d1	textový
řetězcích	řetězec	k1gInPc6	řetězec
můžeme	moct	k5eAaImIp1nP	moct
knihovnu	knihovna	k1gFnSc4	knihovna
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
interaktivním	interaktivní	k2eAgMnSc6d1	interaktivní
interpretu	interpret	k1gMnSc6	interpret
Pythonu	Python	k1gMnSc6	Python
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
šablony	šablona	k1gFnPc1	šablona
uložené	uložený	k2eAgFnPc1d1	uložená
v	v	k7c6	v
adresáři	adresář	k1gInSc6	adresář
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
navíc	navíc	k6eAd1	navíc
vytvořit	vytvořit	k5eAaPmF	vytvořit
objekt	objekt	k1gInSc4	objekt
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
šablony	šablona	k1gFnPc4	šablona
budou	být	k5eAaImBp3nP	být
používat	používat	k5eAaImF	používat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Podoba	podoba	k1gFnSc1	podoba
souboru	soubor	k1gInSc2	soubor
sablona	sablona	k1gFnSc1	sablona
<g/>
.	.	kIx.	.
<g/>
html	html	k1gInSc1	html
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
následující	následující	k2eAgInPc4d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Oddělení	oddělení	k1gNnSc4	oddělení
šablony	šablona	k1gFnSc2	šablona
od	od	k7c2	od
kódu	kód	k1gInSc2	kód
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
šablona	šablona	k1gFnSc1	šablona
oddělena	oddělen	k2eAgFnSc1d1	oddělena
od	od	k7c2	od
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
následující	následující	k2eAgInSc1d1	následující
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
příklad	příklad	k1gInSc1	příklad
generování	generování	k1gNnSc2	generování
HTML	HTML	kA	HTML
kódu	kód	k1gInSc2	kód
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
šablona	šablona	k1gFnSc1	šablona
uložena	uložen	k2eAgFnSc1d1	uložena
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
example	example	k6eAd1	example
<g/>
.	.	kIx.	.
<g/>
html	html	k1gMnSc1	html
<g/>
.	.	kIx.	.
<g/>
jinja	jinja	k1gMnSc1	jinja
<g/>
,	,	kIx,	,
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
následujícím	následující	k2eAgInSc7d1	následující
programem	program	k1gInSc7	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
následující	následující	k2eAgFnSc2d1	následující
HTML	HTML	kA	HTML
kód	kód	k1gInSc4	kód
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jinja	Jinj	k1gInSc2	Jinj
(	(	kIx(	(
<g/>
template	templat	k1gMnSc5	templat
engine	engin	k1gMnSc5	engin
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
WWW	WWW	kA	WWW
server	server	k1gInSc1	server
Jinja	Jinjum	k1gNnSc2	Jinjum
</s>
</p>
<p>
<s>
Jinja	Jinj	k1gInSc2	Jinj
<g/>
2	[number]	k4	2
na	na	k7c6	na
Pythonovém	Pythonovém	k?	Pythonovém
webu	web	k1gInSc6	web
pypi	pyp	k1gFnSc2	pyp
</s>
</p>
<p>
<s>
Jinja	Jinja	k1gFnSc1	Jinja
<g/>
2	[number]	k4	2
na	na	k7c4	na
GitHub	GitHub	k1gInSc4	GitHub
</s>
</p>
