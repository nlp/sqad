<s>
Stonehenge	Stonehenge	k6eAd1	Stonehenge
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc1	komplex
menhirů	menhir	k1gInPc2	menhir
a	a	k8xC	a
kamenných	kamenný	k2eAgInPc2d1	kamenný
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
Salisburské	Salisburský	k2eAgFnSc6d1	Salisburská
pláni	pláň	k1gFnSc6	pláň
asi	asi	k9	asi
13	[number]	k4	13
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
městečka	městečko	k1gNnSc2	městečko
Salisbury	Salisbura	k1gFnSc2	Salisbura
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
