<p>
<s>
Betty	Betty	k1gFnSc1	Betty
Ann	Ann	k1gFnSc1	Ann
Bjerkreim	Bjerkreim	k1gInSc1	Bjerkreim
Nilsenová	Nilsenová	k1gFnSc1	Nilsenová
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Stavanger	Stavanger	k1gInSc1	Stavanger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
norská	norský	k2eAgFnSc1d1	norská
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
běhu	běh	k1gInSc6	běh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Lillehammeru	Lillehammer	k1gInSc6	Lillehammer
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
je	být	k5eAaImIp3nS	být
zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
ze	z	k7c2	z
štafetového	štafetový	k2eAgInSc2d1	štafetový
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
Mistrovsví	Mistrovsví	k1gNnSc4	Mistrovsví
světa	svět	k1gInSc2	svět
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
běhu	běh	k1gInSc6	běh
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
běhá	běhat	k5eAaImIp3nS	běhat
za	za	k7c4	za
norský	norský	k2eAgInSc4d1	norský
klub	klub	k1gInSc4	klub
Baekkelaget	Baekkelageta	k1gFnPc2	Baekkelageta
SK	Sk	kA	Sk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Runners	Runners	k1gInSc4	Runners
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
