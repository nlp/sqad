<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekor	k1gMnSc2	Sekor
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1899	[number]	k4	1899
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gFnSc1	pole
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
výrazný	výrazný	k2eAgMnSc1d1	výrazný
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
karikaturista	karikaturista	k1gMnSc1	karikaturista
a	a	k8xC	a
entomolog	entomolog	k1gMnSc1	entomolog
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
rozhlasem	rozhlas	k1gInSc7	rozhlas
<g/>
,	,	kIx,	,
televizí	televize	k1gFnSc7	televize
<g/>
,	,	kIx,	,
filmem	film	k1gInSc7	film
i	i	k8xC	i
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
hlavně	hlavně	k9	hlavně
knihy	kniha	k1gFnPc4	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
figurku	figurka	k1gFnSc4	figurka
Ferdy	Ferda	k1gMnSc2	Ferda
Mravence	mravenec	k1gMnSc2	mravenec
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Pestrém	pestrý	k2eAgInSc6d1	pestrý
týdnu	týden	k1gInSc6	týden
1927	[number]	k4	1927
jako	jako	k8xS	jako
postavička	postavička	k1gFnSc1	postavička
v	v	k7c6	v
kreslených	kreslený	k2eAgInPc6d1	kreslený
seriálech	seriál	k1gInPc6	seriál
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
i	i	k9	i
Brouk	brouk	k1gMnSc1	brouk
Pytlík	pytlík	k1gMnSc1	pytlík
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sekorovy	Sekorův	k2eAgInPc1d1	Sekorův
příběhy	příběh	k1gInPc1	příběh
spojují	spojovat	k5eAaImIp3nP	spojovat
zábavnost	zábavnost	k1gFnSc4	zábavnost
s	s	k7c7	s
poučností	poučnost	k1gFnSc7	poučnost
a	a	k8xC	a
mravním	mravní	k2eAgNnSc7d1	mravní
ponaučením	ponaučení	k1gNnSc7	ponaučení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
ilustrace	ilustrace	k1gFnPc1	ilustrace
byly	být	k5eAaImAgFnP	být
blízké	blízký	k2eAgInPc4d1	blízký
stylu	styl	k1gInSc3	styl
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Sekora	Sekor	k1gMnSc4	Sekor
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
prvních	první	k4xOgInPc2	první
českých	český	k2eAgInPc2d1	český
ragbyových	ragbyový	k2eAgInPc2d1	ragbyový
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
,	,	kIx,	,
popularizátor	popularizátor	k1gMnSc1	popularizátor
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
Brna	Brno	k1gNnSc2	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekora	k1gFnSc1	Sekora
<g/>
,	,	kIx,	,
odborný	odborný	k2eAgMnSc1d1	odborný
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
spisovateli	spisovatel	k1gMnSc3	spisovatel
bylo	být	k5eAaImAgNnS	být
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
byl	být	k5eAaImAgMnS	být
třetí	třetí	k4xOgMnSc1	třetí
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
obecné	obecný	k2eAgFnSc6d1	obecná
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
a	a	k8xC	a
na	na	k7c6	na
gymnáziích	gymnázium	k1gNnPc6	gymnázium
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
ve	v	k7c6	v
Vyškově	Vyškov	k1gInSc6	Vyškov
<g/>
.	.	kIx.	.
</s>
<s>
Sbíral	sbírat	k5eAaImAgMnS	sbírat
motýly	motýl	k1gMnPc4	motýl
a	a	k8xC	a
brouky	brouk	k1gMnPc4	brouk
<g/>
,	,	kIx,	,
sportoval	sportovat	k5eAaImAgMnS	sportovat
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
četl	číst	k5eAaImAgMnS	číst
a	a	k8xC	a
rád	rád	k2eAgMnSc1d1	rád
kreslil	kreslit	k5eAaImAgInS	kreslit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
odveden	odveden	k2eAgMnSc1d1	odveden
jako	jako	k8xS	jako
jednoroční	jednoroční	k2eAgMnSc1d1	jednoroční
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Maturoval	maturovat	k5eAaBmAgMnS	maturovat
proto	proto	k8xC	proto
až	až	k6eAd1	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
studia	studio	k1gNnPc1	studio
po	po	k7c6	po
nepovedené	povedený	k2eNgFnSc6d1	nepovedená
státnici	státnice	k1gFnSc6	státnice
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
Kalabusovou	Kalabusový	k2eAgFnSc7d1	Kalabusová
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
po	po	k7c6	po
roce	rok	k1gInSc6	rok
skončilo	skončit	k5eAaPmAgNnS	skončit
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
podruhé	podruhé	k6eAd1	podruhé
s	s	k7c7	s
Ludmilou	Ludmila	k1gFnSc7	Ludmila
Roubíčkovou	Roubíčková	k1gFnSc7	Roubíčková
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
měli	mít	k5eAaImAgMnP	mít
syna	syn	k1gMnSc4	syn
Ondřeje	Ondřej	k1gMnSc2	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
gymnaziálních	gymnaziální	k2eAgNnPc2d1	gymnaziální
let	léto	k1gNnPc2	léto
ho	on	k3xPp3gMnSc4	on
výtvarně	výtvarně	k6eAd1	výtvarně
vedl	vést	k5eAaImAgMnS	vést
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šindler	Šindler	k1gMnSc1	Šindler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
soukromě	soukromě	k6eAd1	soukromě
studoval	studovat	k5eAaImAgMnS	studovat
jako	jako	k9	jako
hospitant	hospitant	k1gMnSc1	hospitant
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
kreslení	kreslení	k1gNnSc4	kreslení
a	a	k8xC	a
malbu	malba	k1gFnSc4	malba
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Arnošta	Arnošt	k1gMnSc2	Arnošt
Hofbauera	Hofbauer	k1gMnSc2	Hofbauer
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarnými	výtvarný	k2eAgInPc7d1	výtvarný
vzory	vzor	k1gInPc7	vzor
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Busch	Busch	k1gMnSc1	Busch
<g/>
,	,	kIx,	,
Pieter	Pieter	k1gMnSc1	Pieter
Brueghel	Brueghel	k1gMnSc1	Brueghel
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
Henri	Henre	k1gFnSc4	Henre
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
sportovní	sportovní	k2eAgMnSc1d1	sportovní
referent	referent	k1gMnSc1	referent
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
v	v	k7c6	v
brněnských	brněnský	k2eAgFnPc6d1	brněnská
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
(	(	kIx(	(
<g/>
přijal	přijmout	k5eAaPmAgMnS	přijmout
ho	on	k3xPp3gMnSc4	on
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Arnošt	Arnošt	k1gMnSc1	Arnošt
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
a	a	k8xC	a
1927	[number]	k4	1927
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
redakcí	redakce	k1gFnSc7	redakce
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
redakcí	redakce	k1gFnSc7	redakce
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
redigoval	redigovat	k5eAaImAgInS	redigovat
Dětský	dětský	k2eAgInSc1d1	dětský
koutek	koutek	k1gInSc1	koutek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
byla	být	k5eAaImAgFnS	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
redigoval	redigovat	k5eAaImAgInS	redigovat
časopis	časopis	k1gInSc1	časopis
Sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
ferdovský	ferdovský	k2eAgMnSc1d1	ferdovský
všeuměl	všeuměl	k1gMnSc1	všeuměl
věnoval	věnovat	k5eAaImAgMnS	věnovat
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
nadšení	nadšení	k1gNnSc2	nadšení
i	i	k8xC	i
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
československého	československý	k2eAgNnSc2d1	Československé
a	a	k8xC	a
českého	český	k2eAgNnSc2d1	české
ragby	ragby	k1gNnSc2	ragby
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
poznal	poznat	k5eAaPmAgMnS	poznat
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Sport	sport	k1gInSc1	sport
na	na	k7c6	na
pokračování	pokračování	k1gNnSc6	pokračování
vydával	vydávat	k5eAaPmAgMnS	vydávat
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
pravidla	pravidlo	k1gNnSc2	pravidlo
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
trenérem	trenér	k1gMnSc7	trenér
ragbyových	ragbyový	k2eAgNnPc2d1	ragbyové
družstev	družstvo	k1gNnPc2	družstvo
Moravská	moravský	k2eAgFnSc1d1	Moravská
Slávie	Slávie	k1gFnSc1	Slávie
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
a	a	k8xC	a
AFK	AFK	kA	AFK
Žižka	Žižka	k1gMnSc1	Žižka
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
sehrály	sehrát	k5eAaPmAgFnP	sehrát
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc4	první
ragbyové	ragbyový	k2eAgNnSc4d1	ragbyové
utkání	utkání	k1gNnSc4	utkání
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
českou	český	k2eAgFnSc4d1	Česká
ragbyovou	ragbyový	k2eAgFnSc4d1	ragbyová
terminologii	terminologie	k1gFnSc4	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
také	také	k9	také
aktivním	aktivní	k2eAgMnSc7d1	aktivní
rozhodčím	rozhodčí	k1gMnSc7	rozhodčí
<g/>
,	,	kIx,	,
řídil	řídit	k5eAaImAgInS	řídit
například	například	k6eAd1	například
první	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
RC	RC	kA	RC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
RC	RC	kA	RC
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
knižní	knižní	k2eAgFnSc7d1	knižní
seriálovou	seriálový	k2eAgFnSc7d1	seriálová
postavou	postava	k1gFnSc7	postava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
byl	být	k5eAaImAgMnS	být
pes	pes	k1gMnSc1	pes
Voříšek	Voříšek	k1gMnSc1	Voříšek
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
texty	text	k1gInPc1	text
napsal	napsat	k5eAaBmAgMnS	napsat
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
postavami	postava	k1gFnPc7	postava
byl	být	k5eAaImAgMnS	být
lovec	lovec	k1gMnSc1	lovec
a	a	k8xC	a
cestovaltel	cestovaltel	k1gMnSc1	cestovaltel
Animuk	Animuk	k1gMnSc1	Animuk
<g/>
,	,	kIx,	,
sportovci	sportovec	k1gMnPc1	sportovec
Hej	hej	k6eAd1	hej
a	a	k8xC	a
Rup	rup	k0	rup
<g/>
,	,	kIx,	,
dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Vendelín	Vendelín	k1gMnSc1	Vendelín
či	či	k8xC	či
přitroublé	přitroublý	k2eAgNnSc4d1	přitroublé
kuře	kuře	k1gNnSc4	kuře
Napipi	Napip	k1gFnSc2	Napip
<g/>
.	.	kIx.	.
</s>
<s>
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Pestrého	pestrý	k2eAgInSc2d1	pestrý
týdnu	týden	k1gInSc6	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
jako	jako	k8xS	jako
postava	postava	k1gFnSc1	postava
kresleného	kreslený	k2eAgInSc2d1	kreslený
seriálu	seriál	k1gInSc2	seriál
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přibyl	přibýt	k5eAaPmAgInS	přibýt
i	i	k9	i
brouk	brouk	k1gMnSc1	brouk
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
vyšla	vyjít	k5eAaPmAgFnS	vyjít
knížka	knížka	k1gFnSc1	knížka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
Ferda	Ferda	k1gMnSc1	Ferda
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
Ferda	Ferda	k1gMnSc1	Ferda
v	v	k7c6	v
mraveništi	mraveniště	k1gNnSc6	mraveniště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Ferdův	Ferdův	k2eAgInSc1d1	Ferdův
slabikář	slabikář	k1gInSc1	slabikář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
Ferda	Ferda	k1gMnSc1	Ferda
cvičí	cvičit	k5eAaImIp3nS	cvičit
mraveniště	mraveniště	k1gNnSc4	mraveniště
a	a	k8xC	a
Kousky	kousek	k1gInPc4	kousek
mládence	mládenec	k1gMnSc4	mládenec
Ferdy	Ferda	k1gMnSc2	Ferda
Mravence	mravenec	k1gMnSc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
vězněn	věznit	k5eAaImNgInS	věznit
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
táborech	tábor	k1gInPc6	tábor
Kleinstein	Kleinstein	k2eAgMnSc1d1	Kleinstein
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Osterode	Osterod	k1gInSc5	Osterod
v	v	k7c6	v
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
autentickém	autentický	k2eAgInSc6d1	autentický
deníku	deník	k1gInSc6	deník
s	s	k7c7	s
hořkým	hořký	k2eAgInSc7d1	hořký
humorem	humor	k1gInSc7	humor
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
strasti	strast	k1gFnPc4	strast
táborového	táborový	k2eAgInSc2d1	táborový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
kreslí	kreslit	k5eAaImIp3nS	kreslit
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
se	s	k7c7	s
spoluvězněm	spoluvězeň	k1gMnSc7	spoluvězeň
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Novým	Nový	k1gMnSc7	Nový
hraje	hrát	k5eAaImIp3nS	hrát
maňáskové	maňáskový	k2eAgNnSc4d1	maňáskové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
deníku	deník	k1gInSc2	deník
Práce	práce	k1gFnSc2	práce
a	a	k8xC	a
redaktorem	redaktor	k1gMnSc7	redaktor
časopisu	časopis	k1gInSc2	časopis
Dikobraz	Dikobraz	k1gInSc1	Dikobraz
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
prvních	první	k4xOgNnPc6	první
15	[number]	k4	15
čísel	číslo	k1gNnPc2	číslo
jako	jako	k8xS	jako
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
<g/>
)	)	kIx)	)
a	a	k8xC	a
redaktorem	redaktor	k1gMnSc7	redaktor
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Josefa	Josef	k1gMnSc2	Josef
Hokra	Hokr	k1gMnSc2	Hokr
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
jeho	jeho	k3xOp3gFnPc4	jeho
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
spoluzakládá	spoluzakládat	k5eAaImIp3nS	spoluzakládat
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
vedoucí	vedoucí	k1gMnSc1	vedoucí
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgInS	stát
u	u	k7c2	u
prvních	první	k4xOgInPc2	první
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
neoficiálně	neoficiálně	k6eAd1	neoficiálně
hrál	hrát	k5eAaImAgMnS	hrát
maňáskové	maňáskový	k2eAgNnSc4d1	maňáskové
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
pořádal	pořádat	k5eAaImAgInS	pořádat
besedy	beseda	k1gFnPc4	beseda
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Obrázkové	obrázkový	k2eAgFnSc2d1	obrázková
knížky	knížka	k1gFnSc2	knížka
Pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
větru	vítr	k1gInSc6	vítr
<g/>
,	,	kIx,	,
Štědrý	štědrý	k2eAgInSc1d1	štědrý
večer	večer	k6eAd1	večer
a	a	k8xC	a
O	o	k7c6	o
traktoru	traktor	k1gInSc6	traktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
splašil	splašit	k5eAaPmAgInS	splašit
nechybí	chybit	k5eNaPmIp3nS	chybit
"	"	kIx"	"
<g/>
direktivní	direktivní	k2eAgFnSc1d1	direktivní
návodnost	návodnost	k1gFnSc1	návodnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poplatná	poplatný	k2eAgFnSc1d1	poplatná
tehdejšímu	tehdejší	k2eAgInSc3d1	tehdejší
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
svobodném	svobodný	k2eAgNnSc6d1	svobodné
povolání	povolání	k1gNnSc6	povolání
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
a	a	k8xC	a
kreslí	kreslit	k5eAaImIp3nS	kreslit
pro	pro	k7c4	pro
noviny	novina	k1gFnPc4	novina
a	a	k8xC	a
časopisy	časopis	k1gInPc4	časopis
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
,	,	kIx,	,
Ohníček	ohníček	k1gInSc1	ohníček
<g/>
,	,	kIx,	,
Pionýr	pionýr	k1gInSc1	pionýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předválečných	předválečný	k2eAgNnPc6d1	předválečné
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k9	i
politické	politický	k2eAgFnSc3d1	politická
karikatuře	karikatura	k1gFnSc3	karikatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
účastnil	účastnit	k5eAaImAgMnS	účastnit
na	na	k7c6	na
propagandě	propaganda	k1gFnSc6	propaganda
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
oblíbenosti	oblíbenost	k1gFnSc3	oblíbenost
jeho	jeho	k3xOp3gFnPc2	jeho
knížek	knížka	k1gFnPc2	knížka
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
málokdy	málokdy	k6eAd1	málokdy
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
zasloužilým	zasloužilý	k2eAgMnSc7d1	zasloužilý
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
dostal	dostat	k5eAaPmAgMnS	dostat
Cenu	cena	k1gFnSc4	cena
Marie	Maria	k1gFnSc2	Maria
Majerové	Majerové	k2eAgFnSc2d1	Majerové
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Záchvatem	záchvat	k1gInSc7	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
skončila	skončit	k5eAaPmAgFnS	skončit
jeho	jeho	k3xOp3gFnSc1	jeho
veřejná	veřejný	k2eAgFnSc1d1	veřejná
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
košířských	košířský	k2eAgFnPc6d1	košířská
Malvazinkách	malvazinka	k1gFnPc6	malvazinka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
těchto	tento	k3xDgFnPc2	tento
organizací	organizace	k1gFnPc2	organizace
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInSc1d1	národní
svaz	svaz	k1gInSc1	svaz
novinářů	novinář	k1gMnPc2	novinář
(	(	kIx(	(
<g/>
vyškrtnut	vyškrtnut	k2eAgInSc1d1	vyškrtnut
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
)	)	kIx)	)
Rugby	rugby	k1gNnSc1	rugby
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc4	jeho
pravidla	pravidlo	k1gNnPc4	pravidlo
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
)	)	kIx)	)
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
Ferda	Ferda	k1gMnSc1	Ferda
v	v	k7c6	v
mraveništi	mraveniště	k1gNnSc6	mraveniště
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Ferdův	Ferdův	k2eAgInSc1d1	Ferdův
slabikář	slabikář	k1gInSc1	slabikář
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
Trampoty	trampota	k1gFnSc2	trampota
brouka	brouk	k1gMnSc4	brouk
Pytlíka	pytlík	k1gMnSc4	pytlík
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
Malířské	malířský	k2eAgFnSc2d1	malířská
<g />
.	.	kIx.	.
</s>
<s>
kousky	kousek	k1gInPc1	kousek
brouka	brouk	k1gMnSc2	brouk
Pytlíka	pytlík	k1gMnSc2	pytlík
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
Kuře	kuře	k1gNnSc1	kuře
Napipi	Napip	k1gFnSc2	Napip
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Uprchlík	uprchlík	k1gMnSc1	uprchlík
na	na	k7c6	na
ptačím	ptačí	k2eAgInSc6d1	ptačí
stromě	strom	k1gInSc6	strom
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
oceněna	oceněn	k2eAgFnSc1d1	oceněna
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
EXPO	Expo	k1gNnSc4	Expo
'	'	kIx"	'
<g/>
58	[number]	k4	58
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
)	)	kIx)	)
Ferda	Ferda	k1gMnSc1	Ferda
cvičí	cvičit	k5eAaImIp3nP	cvičit
mraveniště	mraveniště	k1gNnSc4	mraveniště
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Kronika	kronika	k1gFnSc1	kronika
<g />
.	.	kIx.	.
</s>
<s>
města	město	k1gNnPc1	město
Kocourkova	Kocourkův	k2eAgNnPc1d1	Kocourkovo
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
uhlí	uhlí	k1gNnSc2	uhlí
pohněvalo	pohněvat	k5eAaPmAgNnS	pohněvat
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
větru	vítr	k1gInSc6	vítr
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
O	o	k7c6	o
zlém	zlý	k1gMnSc6	zlý
brouku	brouk	k1gMnSc6	brouk
Bramborouku	Bramborouk	k1gMnSc6	Bramborouk
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
ničí	ničit	k5eAaImIp3nS	ničit
škůdce	škůdce	k1gMnSc1	škůdce
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Malované	malovaný	k2eAgNnSc1d1	malované
počasí	počasí	k1gNnSc1	počasí
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
O	o	k7c6	o
traktoru	traktor	k1gInSc6	traktor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
splašil	splašit	k5eAaPmAgInS	splašit
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Mravenci	mravenec	k1gMnPc1	mravenec
se	se	k3xPyFc4	se
nedají	dát	k5eNaPmIp3nP	dát
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
si	se	k3xPyFc3	se
děti	dítě	k1gFnPc1	dítě
hrály	hrát	k5eAaImAgFnP	hrát
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Čmelák	čmelák	k1gMnSc1	čmelák
Aninka	Aninka	k1gFnSc1	Aninka
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Hurá	hurá	k0	hurá
za	za	k7c4	za
Zdendou	Zdendou	k?	Zdendou
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
O	o	k7c6	o
psu	pes	k1gMnSc6	pes
vzduchoplavci	vzduchoplavec	k1gMnSc6	vzduchoplavec
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Pošta	pošta	k1gFnSc1	pošta
v	v	k7c6	v
ZOO	zoo	k1gNnSc6	zoo
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Sedm	sedm	k4xCc1	sedm
pohádek	pohádka	k1gFnPc2	pohádka
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
První	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
Ferdovi	Ferda	k1gMnSc6	Ferda
Mravencovi	mravenec	k1gMnSc6	mravenec
vycházely	vycházet	k5eAaImAgInP	vycházet
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
společně	společně	k6eAd1	společně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Knížka	knížka	k1gFnSc1	knížka
Ferdy	Ferda	k1gMnSc2	Ferda
Mravence	mravenec	k1gMnSc2	mravenec
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
brouku	brouk	k1gMnSc6	brouk
Pytlíkovi	pytlík	k1gMnSc6	pytlík
vycházely	vycházet	k5eAaImAgFnP	vycházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
společně	společně	k6eAd1	společně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brouk	brouk	k1gMnSc1	brouk
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
opět	opět	k6eAd1	opět
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Voříškova	Voříškův	k2eAgNnPc1d1	Voříškův
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Cvočkův	cvočkův	k2eAgInSc1d1	cvočkův
podivný	podivný	k2eAgInSc1d1	podivný
život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
Cvoček	cvoček	k1gMnSc1	cvoček
honil	honit	k5eAaImAgMnS	honit
pytláka	pytlák	k1gMnSc4	pytlák
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Příhody	příhoda	k1gFnSc2	příhoda
Ferdy	Ferda	k1gMnSc2	Ferda
Mravence	mravenec	k1gMnSc2	mravenec
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Kapitán	kapitán	k1gMnSc1	kapitán
Animuk	Animuk	k1gMnSc1	Animuk
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Hej	hej	k6eAd1	hej
a	a	k8xC	a
Rup	rupět	k5eAaImRp2nS	rupět
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
u	u	k7c2	u
broučků	brouček	k1gMnPc2	brouček
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Kousky	kousek	k1gInPc1	kousek
mládence	mládenec	k1gMnSc2	mládenec
Ferdy	Ferda	k1gMnSc2	Ferda
Mravence	mravenec	k1gMnSc2	mravenec
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Nápady	nápad	k1gInPc1	nápad
kuřete	kuře	k1gNnSc2	kuře
Napipi	Napipi	k1gNnSc2	Napipi
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Kapitán	kapitán	k1gMnSc1	kapitán
Animuk	Animuk	k1gMnSc1	Animuk
opět	opět	k6eAd1	opět
loví	lovit	k5eAaImIp3nS	lovit
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Rudolf	Rudolf	k1gMnSc1	Rudolf
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
:	:	kIx,	:
Zlaté	zlatý	k2eAgInPc1d1	zlatý
dni	den	k1gInPc1	den
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g />
.	.	kIx.	.
</s>
<s>
Golombek	Golombek	k1gInSc1	Golombek
<g/>
:	:	kIx,	:
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
tří	tři	k4xCgMnPc2	tři
Billů	Bill	k1gMnPc2	Bill
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
Křópal	Křópal	k1gMnSc1	Křópal
z	z	k7c2	z
Břochovan	Břochovan	k1gMnSc1	Břochovan
<g/>
:	:	kIx,	:
Plkačke	Plkačke	k1gFnSc1	Plkačke
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
drohy	droh	k1gInPc1	droh
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jerome	Jerom	k1gInSc5	Jerom
Klapka	Klapka	k1gMnSc1	Klapka
Jerome	Jerom	k1gInSc5	Jerom
<g/>
:	:	kIx,	:
Henry	henry	k1gInSc7	henry
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karla	Karla	k1gFnSc1	Karla
Dolínková	Dolínkový	k2eAgFnSc1d1	Dolínková
<g/>
:	:	kIx,	:
Maminka	maminka	k1gFnSc1	maminka
<g />
.	.	kIx.	.
</s>
<s>
Ťok	Ťok	k?	Ťok
Ťok	Ťok	k1gFnPc1	Ťok
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Plachta	plachta	k1gFnSc1	plachta
<g/>
:	:	kIx,	:
Pučálkovic	Pučálkovice	k1gFnPc2	Pučálkovice
Amina	Amino	k1gNnSc2	Amino
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Arnold	Arnold	k1gMnSc1	Arnold
Palauš	Palauš	k1gMnSc1	Palauš
<g/>
:	:	kIx,	:
Haló	haló	k0	haló
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
redaktor	redaktor	k1gMnSc1	redaktor
Laufer	Laufer	k1gMnSc1	Laufer
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Béla	Béla	k1gMnSc1	Béla
Szenes	Szenes	k1gMnSc1	Szenes
<g/>
:	:	kIx,	:
Vaškův	Vaškův	k2eAgInSc1d1	Vaškův
vítězný	vítězný	k2eAgInSc1d1	vítězný
gól	gól	k1gInSc1	gól
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
:	:	kIx,	:
Kubula	Kubula	k1gFnSc1	Kubula
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
Kubikula	Kubikula	k1gFnSc1	Kubikula
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Vavris	Vavris	k1gFnSc2	Vavris
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Lelíček	lelíček	k1gInSc4	lelíček
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Langer	Langer	k1gMnSc1	Langer
<g/>
:	:	kIx,	:
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
bílého	bílý	k2eAgInSc2d1	bílý
klíče	klíč	k1gInSc2	klíč
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Eschmann	Eschmann	k1gMnSc1	Eschmann
<g/>
:	:	kIx,	:
Jirka	Jirka	k1gMnSc1	Jirka
cirkusák	cirkusák	k1gMnSc1	cirkusák
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
Bohumila	Bohumila	k1gFnSc1	Bohumila
Sílová	sílový	k2eAgFnSc1d1	Sílová
<g/>
:	:	kIx,	:
Mik	Mik	k1gMnSc1	Mik
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nemůže	moct	k5eNaImIp3nS	moct
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
:	:	kIx,	:
O	o	k7c6	o
věrné	věrný	k2eAgFnSc6d1	věrná
Hadimršce	hadimrška	k1gFnSc6	hadimrška
...	...	k?	...
a	a	k8xC	a
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
sběhlo	sběhnout	k5eAaPmAgNnS	sběhnout
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgMnS	vydat
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Rýpar	Rýpar	k1gMnSc1	Rýpar
<g/>
:	:	kIx,	:
Slávek	Slávek	k1gMnSc1	Slávek
+	+	kIx~	+
Mirek	Mirek	k1gMnSc1	Mirek
+	+	kIx~	+
6	[number]	k4	6
HP	HP	kA	HP
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
J.	J.	kA	J.
Hokr	Hokr	k1gMnSc1	Hokr
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Ransome	Ransom	k1gInSc5	Ransom
<g/>
:	:	kIx,	:
Zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
loď	loď	k1gFnSc1	loď
kapitána	kapitán	k1gMnSc2	kapitán
Flinta	flinta	k1gFnSc1	flinta
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kopta	Kopt	k1gMnSc2	Kopt
<g/>
:	:	kIx,	:
Smějte	smát	k5eAaImRp2nP	smát
se	se	k3xPyFc4	se
s	s	k7c7	s
bláznem	blázen	k1gMnSc7	blázen
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Hašková	Hašková	k1gFnSc1	Hašková
<g/>
:	:	kIx,	:
Z	z	k7c2	z
notesu	notes	k1gInSc2	notes
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Štembera	Štembero	k1gNnSc2	Štembero
<g/>
:	:	kIx,	:
Škola	škola	k1gFnSc1	škola
kouzla	kouzlo	k1gNnSc2	kouzlo
zbavená	zbavený	k2eAgFnSc1d1	zbavená
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Karafiát	Karafiát	k1gMnSc1	Karafiát
<g/>
:	:	kIx,	:
Broučci	Brouček	k1gMnPc1	Brouček
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Věromír	Věromír	k1gMnSc1	Věromír
<g />
.	.	kIx.	.
</s>
<s>
Pleva	Pleva	k1gMnSc1	Pleva
<g/>
:	:	kIx,	:
Malý	Malý	k1gMnSc1	Malý
Bobeš	Bobeš	k1gMnSc1	Bobeš
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ema	Ema	k1gFnSc1	Ema
Tintěrová	Tintěrový	k2eAgFnSc1d1	Tintěrová
<g/>
:	:	kIx,	:
Veselé	Veselé	k2eAgFnSc1d1	Veselé
příhody	příhoda	k1gFnPc1	příhoda
kozy	koza	k1gFnSc2	koza
Lujzy	Lujza	k1gFnSc2	Lujza
a	a	k8xC	a
kocoura	kocour	k1gMnSc2	kocour
Bobka	Bobek	k1gMnSc2	Bobek
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Bořek	Bořek	k1gMnSc1	Bořek
<g/>
:	:	kIx,	:
Kulatá	kulatý	k2eAgFnSc1d1	kulatá
velmoc	velmoc	k1gFnSc1	velmoc
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
:	:	kIx,	:
Hlas	hlas	k1gInSc1	hlas
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Wenig	Wenig	k1gMnSc1	Wenig
<g/>
:	:	kIx,	:
Brumla	Brumla	k?	Brumla
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Minaříková	Minaříková	k1gFnSc1	Minaříková
<g/>
:	:	kIx,	:
Ježourek	Ježourek	k1gMnSc1	Ježourek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
příhody	příhoda	k1gFnPc1	příhoda
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Malík	Malík	k1gMnSc1	Malík
<g/>
:	:	kIx,	:
Míček	míček	k1gInSc1	míček
Flíček	flíček	k1gInSc1	flíček
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Zoščenko	Zoščenka	k1gFnSc5	Zoščenka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Psí	psí	k2eAgInSc1d1	psí
čich	čich	k1gInSc1	čich
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Štorch	Štorch	k1gMnSc1	Štorch
<g/>
:	:	kIx,	:
Lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
skále	skála	k1gFnSc6	skála
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hostáň	Hostáň	k1gFnSc1	Hostáň
<g/>
:	:	kIx,	:
Námořník	námořník	k1gMnSc1	námořník
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hostáň	Hostáň	k1gFnSc4	Hostáň
<g/>
:	:	kIx,	:
Žertíky	žertík	k1gInPc7	žertík
pro	pro	k7c4	pro
malé	malý	k2eAgMnPc4d1	malý
čertíky	čertík	k1gMnPc4	čertík
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Čtvrtek	čtvrtek	k1gInSc1	čtvrtek
<g/>
:	:	kIx,	:
Kolotoč	kolotoč	k1gInSc1	kolotoč
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Čtvrtek	čtvrtek	k1gInSc1	čtvrtek
<g/>
:	:	kIx,	:
Lev	Lev	k1gMnSc1	Lev
utekl	utéct	k5eAaPmAgMnS	utéct
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Němec	Němec	k1gMnSc1	Němec
<g/>
:	:	kIx,	:
Soudničky	soudnička	k1gFnPc1	soudnička
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ema	Ema	k1gFnSc1	Ema
Řezáčová	Řezáčová	k1gFnSc1	Řezáčová
<g/>
:	:	kIx,	:
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
kolečkách	koleček	k1gInPc6	koleček
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Lacina	Lacina	k1gMnSc1	Lacina
<g/>
:	:	kIx,	:
Slyš	slyšet	k5eAaImRp2nS	slyšet
a	a	k8xC	a
piš	psát	k5eAaImRp2nS	psát
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Minaříková	Minaříková	k1gFnSc1	Minaříková
<g/>
:	:	kIx,	:
Ježourek	Ježourek	k1gMnSc1	Ježourek
a	a	k8xC	a
Pišta	Pišta	k1gMnSc1	Pišta
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Čaruškin	Čaruškin	k1gMnSc1	Čaruškin
<g/>
:	:	kIx,	:
Nikita	Nikita	k1gMnSc1	Nikita
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Zaoral	Zaoral	k1gMnSc1	Zaoral
<g/>
:	:	kIx,	:
Říkánky	říkánka	k1gFnPc1	říkánka
pro	pro	k7c4	pro
malé	malý	k2eAgMnPc4d1	malý
občánky	občánek	k1gMnPc4	občánek
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Štruncová	Štruncová	k1gFnSc1	Štruncová
<g/>
:	:	kIx,	:
Brigáda	brigáda	k1gFnSc1	brigáda
v	v	k7c6	v
mateřské	mateřský	k2eAgFnSc6d1	mateřská
školce	školka	k1gFnSc6	školka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irina	Irien	k2eAgFnSc1d1	Irina
Karnauchová	Karnauchová	k1gFnSc1	Karnauchová
<g/>
:	:	kIx,	:
Chytrý	chytrý	k2eAgMnSc1d1	chytrý
sedláček	sedláček	k1gMnSc1	sedláček
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
pohádky	pohádka	k1gFnPc4	pohádka
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Gianni	Gianen	k2eAgMnPc1d1	Gianen
Rodari	Rodar	k1gMnPc1	Rodar
<g/>
:	:	kIx,	:
O	o	k7c6	o
statečném	statečný	k2eAgMnSc6d1	statečný
Cibulkovi	Cibulka	k1gMnSc6	Cibulka
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Michalkov	Michalkov	k1gInSc1	Michalkov
<g/>
:	:	kIx,	:
Strýček	strýček	k1gMnSc1	strýček
Štěpán	Štěpán	k1gMnSc1	Štěpán
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Štefl	Štefl	k1gMnSc1	Štefl
<g/>
:	:	kIx,	:
Lékař	lékař	k1gMnSc1	lékař
hovoří	hovořit	k5eAaImIp3nS	hovořit
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hostáň	Hostáň	k1gFnSc1	Hostáň
<g/>
:	:	kIx,	:
Švitořilky	švitořilka	k1gFnPc1	švitořilka
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Juhan	Juhan	k1gInSc1	Juhan
Smuul	Smuul	k1gInSc1	Smuul
<g/>
:	:	kIx,	:
Punťa	Punťa	k1gMnSc1	Punťa
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
