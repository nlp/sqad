<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Dominik	Dominik	k1gMnSc1	Dominik
Josef	Josef	k1gMnSc1	Josef
Hácha	Hácha	k1gMnSc1	Hácha
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1872	[number]	k4	1872
Trhové	trhový	k2eAgFnSc2d1	trhová
Sviny	Svina	k1gFnSc2	Svina
–	–	k?	–
27.	[number]	k4	27.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
pomnichovské	pomnichovský	k2eAgFnSc2d1	pomnichovská
Česko-Slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozbití	rozbití	k1gNnSc6	rozbití
zbytku	zbytek	k1gInSc2	zbytek
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
ke	k	k7c3	k
dni	den	k1gInSc3	den
15.	[number]	k4	15.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
učiněn	učinit	k5eAaImNgMnS	učinit
protektorátním	protektorátní	k2eAgMnSc7d1	protektorátní
státním	státní	k2eAgMnSc7d1	státní
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jím	jíst	k5eAaImIp1nS	jíst
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
protektorátu	protektorát	k1gInSc2	protektorát
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
právně-teoretická	právněeoretický	k2eAgFnSc1d1	právně-teoretický
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
a	a	k8xC	a
překladatelská	překladatelský	k2eAgFnSc1d1	překladatelská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Právník	právník	k1gMnSc1	právník
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Trhových	trhový	k2eAgInPc6d1	trhový
Svinech	Svin	k1gInPc6	Svin
jako	jako	k8xS	jako
Emil	Emil	k1gMnSc1	Emil
Dominik	Dominik	k1gMnSc1	Dominik
Josef	Josef	k1gMnSc1	Josef
Hácha	Hácha	k1gMnSc1	Hácha
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
berního	berní	k1gMnSc2	berní
úředníka	úředník	k1gMnSc2	úředník
Josefa	Josef	k1gMnSc2	Josef
Emanuela	Emanuel	k1gMnSc2	Emanuel
Háchy	Hácha	k1gMnSc2	Hácha
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
Karolíny	Karolína	k1gFnSc2	Karolína
Pavlíny	Pavlína	k1gFnSc2	Pavlína
Háchové	Háchová	k1gFnSc2	Háchová
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Klausové	Klausová	k1gFnPc1	Klausová
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
promoval	promovat	k5eAaBmAgMnS	promovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
Českému	český	k2eAgNnSc3d1	české
místodržitelství	místodržitelství	k1gNnSc3	místodržitelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
Hácha	Hácha	k1gMnSc1	Hácha
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Klausovou	Klausův	k2eAgFnSc7d1	Klausova
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
právnické	právnický	k2eAgFnPc4d1	právnická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
dvorní	dvorní	k2eAgMnSc1d1	dvorní
rada	rada	k1gMnSc1	rada
Správního	správní	k2eAgInSc2d1	správní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Pantůčkem	Pantůček	k1gMnSc7	Pantůček
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
založit	založit	k5eAaPmF	založit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
obhájil	obhájit	k5eAaPmAgInS	obhájit
svoji	svůj	k3xOyFgFnSc4	svůj
habilitační	habilitační	k2eAgFnSc4d1	habilitační
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
docent	docent	k1gMnSc1	docent
působil	působit	k5eAaImAgMnS	působit
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1924	[number]	k4	1924
a	a	k8xC	a
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Pantůčka	Pantůček	k1gMnSc2	Pantůček
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
prezidentem	prezident	k1gMnSc7	prezident
Tomášem	Tomáš	k1gMnSc7	Tomáš
Garriguem	Garrigu	k1gMnSc7	Garrigu
Masarykem	Masaryk	k1gMnSc7	Masaryk
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
druhým	druhý	k4xOgNnSc7	druhý
prezidentem	prezident	k1gMnSc7	prezident
NSS	NSS	kA	NSS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
právník	právník	k1gMnSc1	právník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ceněným	ceněný	k2eAgMnSc7d1	ceněný
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
znalosti	znalost	k1gFnPc4	znalost
anglosaského	anglosaský	k2eAgNnSc2d1	anglosaské
zvykového	zvykový	k2eAgNnSc2d1	zvykové
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgMnS	být
člen	člen	k1gMnSc1	člen
legislativní	legislativní	k2eAgFnSc2d1	legislativní
rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
učené	učený	k2eAgFnSc2d1	učená
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
uznání	uznání	k1gNnSc1	uznání
Háchových	Háchův	k2eAgFnPc2d1	Háchova
hodnot	hodnota	k1gFnPc2	hodnota
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
členství	členství	k1gNnSc4	členství
u	u	k7c2	u
haagského	haagský	k2eAgMnSc2d1	haagský
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Prezident	prezident	k1gMnSc1	prezident
===	===	k?	===
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
změnu	změna	k1gFnSc4	změna
pro	pro	k7c4	pro
Háchu	Hácha	k1gFnSc4	Hácha
přinesl	přinést	k5eAaPmAgInS	přinést
rok	rok	k1gInSc1	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Marie	Marie	k1gFnSc1	Marie
Háchová	Háchová	k1gFnSc1	Háchová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
prožil	prožít	k5eAaPmAgMnS	prožít
harmonické	harmonický	k2eAgNnSc1d1	harmonické
manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
změna	změna	k1gFnSc1	změna
přišla	přijít	k5eAaPmAgFnS	přijít
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
autonomii	autonomie	k1gFnSc4	autonomie
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
30.	[number]	k4	30.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
přijal	přijmout	k5eAaPmAgMnS	přijmout
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
Česko-Slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
volbu	volba	k1gFnSc4	volba
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
neutralitě	neutralita	k1gFnSc6	neutralita
a	a	k8xC	a
ve	v	k7c4	v
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
větší	veliký	k2eAgFnSc2d2	veliký
instituce	instituce	k1gFnSc2	instituce
(	(	kIx(	(
<g/>
NSS	NSS	kA	NSS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Představitelé	představitel	k1gMnPc1	představitel
ľudové	ľudové	k2eAgFnSc2d1	ľudové
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
vlády	vláda	k1gFnPc4	vláda
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
tajně	tajně	k6eAd1	tajně
vyjednávali	vyjednávat	k5eAaImAgMnP	vyjednávat
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
10.	[number]	k4	10.
března	březen	k1gInSc2	březen
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
stanné	stanný	k2eAgNnSc4d1	stanné
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
sesadil	sesadit	k5eAaPmAgInS	sesadit
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
autonomní	autonomní	k2eAgFnSc4d1	autonomní
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jozefem	Jozef	k1gInSc7	Jozef
Tisem	tis	k1gInSc7	tis
a	a	k8xC	a
moc	moc	k6eAd1	moc
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
převzala	převzít	k5eAaPmAgFnS	převzít
s	s	k7c7	s
Háchovým	Háchův	k2eAgInSc7d1	Háchův
souhlasem	souhlas	k1gInSc7	souhlas
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
14.	[number]	k4	14.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
se	s	k7c7	s
špičkami	špička	k1gFnPc7	špička
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
večer	večer	k6eAd1	večer
Hácha	Hácha	k1gMnSc1	Hácha
dorazil	dorazit	k5eAaPmAgMnS	dorazit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
úmyslně	úmyslně	k6eAd1	úmyslně
nucen	nutit	k5eAaImNgMnS	nutit
čekat	čekat	k5eAaImF	čekat
až	až	k9	až
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
ráno	ráno	k6eAd1	ráno
příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
dokouká	dokoukat	k5eAaPmIp3nS	dokoukat
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
sděleno	sdělit	k5eAaPmNgNnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
napadnout	napadnout	k5eAaPmF	napadnout
Česko-Slovensko	Česko-Slovensko	k1gNnSc4	Česko-Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
dal	dát	k5eAaPmAgMnS	dát
Háchovi	Hách	k1gMnSc3	Hách
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
mezi	mezi	k7c7	mezi
podrobením	podrobení	k1gNnSc7	podrobení
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
německé	německý	k2eAgFnSc2d1	německá
branné	branný	k2eAgFnSc2d1	Branná
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
Protektorátu	protektorát	k1gInSc2	protektorát
nebo	nebo	k8xC	nebo
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
invazí	invaze	k1gFnSc7	invaze
a	a	k8xC	a
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hácha	Hácha	k1gMnSc1	Hácha
opakovaně	opakovaně	k6eAd1	opakovaně
vzdoroval	vzdorovat	k5eAaImAgMnS	vzdorovat
Hitlerovu	Hitlerův	k2eAgInSc3d1	Hitlerův
nátlaku	nátlak	k1gInSc3	nátlak
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgInS	odmítat
přijmout	přijmout	k5eAaPmF	přijmout
potupné	potupný	k2eAgFnPc4d1	potupná
podmínky	podmínka	k1gFnPc4	podmínka
předání	předání	k1gNnSc2	předání
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
až	až	k9	až
ve	v	k7c4	v
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Göring	Göring	k1gInSc1	Göring
pohrozil	pohrozit	k5eAaPmAgInS	pohrozit
bombardováním	bombardování	k1gNnSc7	bombardování
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Hácha	Hácha	k1gMnSc1	Hácha
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přivolání	přivolání	k1gNnSc6	přivolání
lékařské	lékařský	k2eAgFnSc2d1	lékařská
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
v	v	k7c6	v
kritickém	kritický	k2eAgInSc6d1	kritický
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc4	dokument
podepsal	podepsat	k5eAaPmAgMnS	podepsat
a	a	k8xC	a
informoval	informovat	k5eAaBmAgMnS	informovat
Prahu	Praha	k1gFnSc4	Praha
o	o	k7c4	o
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
vlakem	vlak	k1gInSc7	vlak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
nacisty	nacista	k1gMnPc4	nacista
úmyslně	úmyslně	k6eAd1	úmyslně
zpožděn	zpožděn	k2eAgMnSc1d1	zpožděn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Hitler	Hitler	k1gMnSc1	Hitler
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Hácha	Hácha	k1gFnSc1	Hácha
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k9	jako
státní	státní	k2eAgMnSc1d1	státní
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
vojáků	voják	k1gMnPc2	voják
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hácha	Hácha	k1gFnSc1	Hácha
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
němečtí	německý	k2eAgMnPc1d1	německý
nacisté	nacista	k1gMnPc1	nacista
budou	být	k5eAaImBp3nP	být
respektovat	respektovat	k5eAaImF	respektovat
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
dohody	dohoda	k1gFnPc4	dohoda
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
právní	právní	k2eAgFnPc4d1	právní
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc1	působení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
státního	státní	k2eAgMnSc2d1	státní
prezidenta	prezident	k1gMnSc2	prezident
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
aktivním	aktivní	k2eAgMnSc7d1	aktivní
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Protestoval	protestovat	k5eAaBmAgMnS	protestovat
u	u	k7c2	u
říšského	říšský	k2eAgMnSc2d1	říšský
protektora	protektor	k1gMnSc2	protektor
Konstantina	Konstantin	k1gMnSc2	Konstantin
von	von	k1gInSc1	von
Neuratha	Neurath	k1gMnSc2	Neurath
proti	proti	k7c3	proti
germanizaci	germanizace	k1gFnSc3	germanizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
slib	slib	k1gInSc4	slib
věrnosti	věrnost	k1gFnSc2	věrnost
Hitlerovi	Hitler	k1gMnSc6	Hitler
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1939	[number]	k4	1939
požadoval	požadovat	k5eAaImAgMnS	požadovat
propuštění	propuštění	k1gNnSc3	propuštění
zatčených	zatčený	k2eAgMnPc2d1	zatčený
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
iniciátorem	iniciátor	k1gMnSc7	iniciátor
vzniku	vznik	k1gInSc2	vznik
Národního	národní	k2eAgNnSc2d1	národní
souručenství	souručenství	k1gNnSc2	souručenství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zastřešující	zastřešující	k2eAgFnSc7d1	zastřešující
organizací	organizace	k1gFnSc7	organizace
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
porážku	porážka	k1gFnSc4	porážka
Francie	Francie	k1gFnSc2	Francie
hitlerovskými	hitlerovský	k2eAgNnPc7d1	hitlerovské
vojsky	vojsko	k1gNnPc7	vojsko
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
"	"	kIx"	"
<g/>
klamnou	klamný	k2eAgFnSc4d1	klamná
orientaci	orientace	k1gFnSc4	orientace
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
"	"	kIx"	"
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
"	"	kIx"	"
<g/>
překonané	překonaný	k2eAgNnSc1d1	překonané
liberalisticko-demokratické	liberalistickoemokratický	k2eAgNnSc1d1	liberalisticko-demokratický
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
září	září	k1gNnSc6	září
1941	[number]	k4	1941
a	a	k8xC	a
zatčení	zatčení	k1gNnSc1	zatčení
protektorátního	protektorátní	k2eAgMnSc2d1	protektorátní
premiéra	premiér	k1gMnSc2	premiér
Aloise	Alois	k1gMnSc2	Alois
Eliáše	Eliáš	k1gMnSc2	Eliáš
se	se	k3xPyFc4	se
Hácha	Hácha	k1gMnSc1	Hácha
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
tlak	tlak	k1gInSc4	tlak
okupantů	okupant	k1gMnPc2	okupant
<g/>
.	.	kIx.	.
</s>
<s>
Osudovým	osudový	k2eAgInSc7d1	osudový
zlomem	zlom	k1gInSc7	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1942	[number]	k4	1942
provedli	provést	k5eAaPmAgMnP	provést
českoslovenští	československý	k2eAgMnPc1d1	československý
výsadkáři	výsadkář	k1gMnPc1	výsadkář
<g/>
.	.	kIx.	.
</s>
<s>
Terorizování	terorizování	k1gNnSc1	terorizování
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
během	během	k7c2	během
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
a	a	k8xC	a
osobní	osobní	k2eAgInSc4d1	osobní
nátlak	nátlak	k1gInSc4	nátlak
nacistů	nacista	k1gMnPc2	nacista
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Hermannem	Hermann	k1gMnSc7	Hermann
Frankem	Frank	k1gMnSc7	Frank
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
osvěty	osvěta	k1gFnSc2	osvěta
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Moravcem	Moravec	k1gMnSc7	Moravec
podlomily	podlomit	k5eAaPmAgFnP	podlomit
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
a	a	k8xC	a
duševní	duševní	k2eAgInSc1d1	duševní
stav	stav	k1gInSc1	stav
státního	státní	k2eAgMnSc2d1	státní
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
měl	mít	k5eAaImAgMnS	mít
Hácha	Hácha	k1gMnSc1	Hácha
rezignovat	rezignovat	k5eAaBmF	rezignovat
po	po	k7c6	po
zatčení	zatčení	k1gNnSc6	zatčení
premiéra	premiér	k1gMnSc2	premiér
Eliáše	Eliáš	k1gMnSc2	Eliáš
nebo	nebo	k8xC	nebo
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
a	a	k8xC	a
následných	následný	k2eAgFnPc6d1	následná
represích	represe	k1gFnPc6	represe
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
nacisté	nacista	k1gMnPc1	nacista
zlomeného	zlomený	k2eAgInSc2d1	zlomený
Háchu	Hách	k1gInSc2	Hách
využívali	využívat	k5eAaImAgMnP	využívat
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
propagovaného	propagovaný	k2eAgNnSc2d1	propagované
"	"	kIx"	"
<g/>
protektorátního	protektorátní	k2eAgNnSc2d1	protektorátní
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
úrovně	úroveň	k1gFnPc4	úroveň
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
toto	tento	k3xDgNnSc1	tento
zneužití	zneužití	k1gNnSc1	zneužití
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
vyšly	vyjít	k5eAaPmAgFnP	vyjít
do	do	k7c2	do
stejného	stejný	k2eAgNnSc2d1	stejné
období	období	k1gNnSc2	období
jako	jako	k8xS	jako
státní	státní	k2eAgFnSc2d1	státní
oslavy	oslava	k1gFnSc2	oslava
Háchových	Háchův	k2eAgFnPc2d1	Háchova
70.	[number]	k4	70.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
projevu	projev	k1gInSc6	projev
ze	z	k7c2	z
dne	den	k1gInSc2	den
30.	[number]	k4	30.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
jak	jak	k6eAd1	jak
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
činnost	činnost	k1gFnSc4	činnost
Benešovy	Benešův	k2eAgFnSc2d1	Benešova
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nacisté	nacista	k1gMnPc1	nacista
vypálili	vypálit	k5eAaPmAgMnP	vypálit
Lidice	Lidice	k1gInPc4	Lidice
a	a	k8xC	a
Ležáky	Ležáky	k1gInPc4	Ležáky
a	a	k8xC	a
občané	občan	k1gMnPc1	občan
na	na	k7c6	na
masových	masový	k2eAgNnPc6d1	masové
shromážděních	shromáždění	k1gNnPc6	shromáždění
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
Hácha	Hácha	k1gMnSc1	Hácha
osobní	osobní	k2eAgInSc1d1	osobní
dar	dar	k1gInSc1	dar
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
–	–	k?	–
luxusní	luxusní	k2eAgInSc4d1	luxusní
automobil	automobil	k1gInSc4	automobil
Mercedes	mercedes	k1gInSc1	mercedes
–	–	k?	–
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
publikace	publikace	k1gFnSc1	publikace
Jihočech	Jihočech	k1gMnSc1	Jihočech
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc4	její
obrazovou	obrazový	k2eAgFnSc4d1	obrazová
část	část	k1gFnSc4	část
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
významný	významný	k2eAgMnSc1d1	významný
reportér	reportér	k1gMnSc1	reportér
Karel	Karel	k1gMnSc1	Karel
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
grafickou	grafický	k2eAgFnSc4d1	grafická
úpravu	úprava	k1gFnSc4	úprava
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
3.	[number]	k4	3.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
se	se	k3xPyFc4	se
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
konala	konat	k5eAaImAgFnS	konat
manifestace	manifestace	k1gFnSc1	manifestace
české	český	k2eAgFnSc2d1	Česká
loajality	loajalita	k1gFnSc2	loajalita
vůči	vůči	k7c3	vůči
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
200 000	[number]	k4	200 000
občanů	občan	k1gMnPc2	občan
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Háchou	Hácha	k1gMnSc7	Hácha
a	a	k8xC	a
Moravcem	Moravec	k1gMnSc7	Moravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Háchovo	Háchův	k2eAgNnSc1d1	Háchovo
zdraví	zdraví	k1gNnSc1	zdraví
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
pokračující	pokračující	k2eAgFnSc2d1	pokračující
arteriosklerózy	arterioskleróza	k1gFnSc2	arterioskleróza
rychle	rychle	k6eAd1	rychle
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
pro	pro	k7c4	pro
německé	německý	k2eAgMnPc4d1	německý
okupanty	okupant	k1gMnPc4	okupant
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
<g/>
.	.	kIx.	.
</s>
<s>
Hácha	Hácha	k1gMnSc1	Hácha
ztratil	ztratit	k5eAaPmAgMnS	ztratit
nejen	nejen	k6eAd1	nejen
důvěru	důvěra	k1gFnSc4	důvěra
domácího	domácí	k2eAgInSc2d1	domácí
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
londýnské	londýnský	k2eAgFnSc2d1	londýnská
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
okupantů	okupant	k1gMnPc2	okupant
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
přisluhovačů	přisluhovač	k1gMnPc2	přisluhovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13.	[number]	k4	13.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Václava	Václav	k1gMnSc2	Václav
Noska	Nosek	k1gMnSc2	Nosek
zatčen	zatčen	k2eAgInSc4d1	zatčen
na	na	k7c6	na
lánském	lánský	k2eAgInSc6d1	lánský
zámku	zámek	k1gInSc6	zámek
a	a	k8xC	a
dopraven	dopravit	k5eAaPmNgMnS	dopravit
do	do	k7c2	do
vězeňské	vězeňský	k2eAgFnSc2d1	vězeňská
nemocnice	nemocnice	k1gFnSc2	nemocnice
na	na	k7c6	na
Pankráci	Pankrác	k1gFnSc6	Pankrác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
27.	[number]	k4	27.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnPc2	úmrtí
mylně	mylně	k6eAd1	mylně
označuje	označovat	k5eAaImIp3nS	označovat
1.	[number]	k4	1.
červen	červeno	k1gNnPc2	červeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Háchovy	Háchův	k2eAgFnSc2d1	Háchova
literární	literární	k2eAgFnSc2d1	literární
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
a	a	k8xC	a
fotografická	fotografický	k2eAgFnSc1d1	fotografická
dokumentace	dokumentace	k1gFnSc1	dokumentace
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
uchovány	uchován	k2eAgFnPc1d1	uchována
v	v	k7c6	v
Archivu	archiv	k1gInSc6	archiv
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
připomínající	připomínající	k2eAgFnSc4d1	připomínající
návštěvu	návštěva	k1gFnSc4	návštěva
prezidenta	prezident	k1gMnSc2	prezident
Háchy	Hácha	k1gFnSc2	Hácha
dne	den	k1gInSc2	den
14.	[number]	k4	14.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
ve	v	k7c6	v
Slaném	Slaný	k1gInSc6	Slaný
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
poštovní	poštovní	k2eAgFnSc4d1	poštovní
známku	známka	k1gFnSc4	známka
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Emila	Emil	k1gMnSc2	Emil
Háchy	Hácha	k1gMnSc2	Hácha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výbor	výbor	k1gInSc1	výbor
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Omyly	omyl	k1gInPc1	omyl
a	a	k8xC	a
přeludy	přelud	k1gInPc1	přelud
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
–	–	k?	–
zprvu	zprvu	k6eAd1	zprvu
anonymní	anonymní	k2eAgFnSc1d1	anonymní
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
veřejného	veřejný	k2eAgNnSc2d1	veřejné
práva	právo	k1gNnSc2	právo
československého	československý	k2eAgNnSc2d1	Československé
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
–	–	k?	–
redigoval	redigovat	k5eAaImAgMnS	redigovat
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
člunu	člun	k1gInSc6	člun
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
psa	pes	k1gMnSc2	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Topič	topič	k1gMnSc1	topič
(	(	kIx(	(
<g/>
Three	Three	k1gInSc1	Three
Men	Men	k1gFnSc2	Men
in	in	k?	in
a	a	k8xC	a
Boat	Boatum	k1gNnPc2	Boatum
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
say	say	k?	say
nothing	nothing	k1gInSc1	nothing
of	of	k?	of
the	the	k?	the
Dog	doga	k1gFnPc2	doga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jerome	Jerom	k1gInSc5	Jerom
Klapka	Klapka	k1gMnSc1	Klapka
Jerome	Jerom	k1gInSc5	Jerom
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
–	–	k?	–
přeložil	přeložit	k5eAaPmAgMnS	přeložit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Theodorem	Theodor	k1gMnSc7	Theodor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GEBHART	GEBHART	kA	GEBHART
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
KUKLÍK	kuklík	k1gMnSc1	kuklík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
:	:	kIx,	:
svár	svár	k1gInSc1	svár
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
totality	totalita	k1gFnSc2	totalita
v	v	k7c6	v
politickém	politický	k2eAgMnSc6d1	politický
<g/>
,	,	kIx,	,
společenském	společenský	k2eAgInSc6d1	společenský
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc6d1	kulturní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2004.	[number]	k4	2004.
315	[number]	k4	315
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-626-6	[number]	k4	80-7185-626-6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
slovník	slovník	k1gInSc1	slovník
osobností	osobnost	k1gFnPc2	osobnost
českého	český	k2eAgInSc2d1	český
katolicismu	katolicismus	k1gInSc2	katolicismus
20.	[number]	k4	20.
století	století	k1gNnSc2	století
s	s	k7c7	s
antologií	antologie	k1gFnSc7	antologie
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
308	[number]	k4	308
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7325-029-2	[number]	k4	80-7325-029-2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PASÁK	pasák	k1gMnSc1	pasák
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
JUDr.	JUDr.	kA	JUDr.
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
:	:	kIx,	:
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Horizont	horizont	k1gInSc1	horizont
<g/>
,	,	kIx,	,
1997.	[number]	k4	1997.
286	[number]	k4	286
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7012-088-6	[number]	k4	80-7012-088-6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
prezidentů	prezident	k1gMnPc2	prezident
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
Československé	československý	k2eAgFnPc1d1	Československá
republiky	republika	k1gFnPc1	republika
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Emil	Emil	k1gMnSc1	Emil
Hácha	Hách	k1gMnSc2	Hách
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
</s>
</p>
<p>
<s>
Soupis	soupis	k1gInSc1	soupis
pražského	pražský	k2eAgNnSc2d1	Pražské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hácha	Hácha	k1gMnSc1	Hácha
Emil	Emil	k1gMnSc1	Emil
*	*	kIx~	*
<g/>
1872	[number]	k4	1872
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
<g/>
:	:	kIx,	:
záznam	záznam	k1gInSc1	záznam
o	o	k7c4	o
jednání	jednání	k1gNnSc4	jednání
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
14.	[number]	k4	14.
na	na	k7c4	na
15.	[number]	k4	15.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
KDO	kdo	k3yInSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
(	(	kIx(	(
<g/>
Libri	Libri	k1gNnSc1	Libri
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85983-65-6	[number]	k4	80-85983-65-6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
obětoval	obětovat	k5eAaBmAgMnS	obětovat
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vališ	Vališ	k1gMnSc1	Vališ
<g/>
,	,	kIx,	,
12.	[number]	k4	12.
7.	[number]	k4	7.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
radio.cz	radio.cz	k1gInSc1	radio.cz
</s>
</p>
<p>
<s>
Chcete	chtít	k5eAaImIp2nP	chtít
zničit	zničit	k5eAaPmF	zničit
Prahu	Praha	k1gFnSc4	Praha
<g/>
?	?	kIx.	?
</s>
<s>
ptal	ptat	k5eAaImAgInS	ptat
se	se	k3xPyFc4	se
Göring	Göring	k1gInSc1	Göring
Háchy	Hácha	k1gFnSc2	Hácha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
15.	[number]	k4	15.
3.	[number]	k4	3.
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Zdroje	zdroj	k1gInPc1	zdroj
ke	k	k7c3	k
jménu	jméno	k1gNnSc3	jméno
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gFnSc1	Hácha
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
Hrdinů	Hrdina	k1gMnPc2	Hrdina
s	s	k7c7	s
otazníkem	otazník	k1gInSc7	otazník
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hrdina.cz	Hrdina.cz	k1gMnSc1	Hrdina.cz
</s>
</p>
