<p>
<s>
Písková	pískový	k2eAgFnSc1d1	písková
rokle	rokle	k1gFnSc1	rokle
je	být	k5eAaImIp3nS	být
turisticky	turisticky	k6eAd1	turisticky
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
lokalita	lokalita	k1gFnSc1	lokalita
v	v	k7c6	v
NPR	NPR	kA	NPR
Broumovské	broumovský	k2eAgFnPc4d1	Broumovská
stěny	stěna	k1gFnPc4	stěna
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Kovářovy	Kovářův	k2eAgFnSc2d1	Kovářova
rokle	rokle	k1gFnSc2	rokle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písková	pískový	k2eAgFnSc1d1	písková
rokle	rokle	k1gFnSc1	rokle
probíhá	probíhat	k5eAaImIp3nS	probíhat
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
turisticky	turisticky	k6eAd1	turisticky
přístupnou	přístupný	k2eAgFnSc7d1	přístupná
Kovářovou	Kovářův	k2eAgFnSc7d1	Kovářova
roklí	rokle	k1gFnSc7	rokle
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
podobný	podobný	k2eAgInSc4d1	podobný
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
skalních	skalní	k2eAgInPc2d1	skalní
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Rokle	rokle	k1gFnSc1	rokle
je	být	k5eAaImIp3nS	být
obtížně	obtížně	k6eAd1	obtížně
schůdná	schůdný	k2eAgFnSc1d1	schůdná
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
přístupna	přístupen	k2eAgFnSc1d1	přístupna
turistickou	turistický	k2eAgFnSc7d1	turistická
stezkou	stezka	k1gFnSc7	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
proto	proto	k6eAd1	proto
není	být	k5eNaImIp3nS	být
povolen	povolit	k5eAaPmNgInS	povolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
z	z	k7c2	z
Pískové	pískový	k2eAgFnSc2d1	písková
rokle	rokle	k1gFnSc2	rokle
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
z	z	k7c2	z
Pískové	pískový	k2eAgFnSc2d1	písková
rokle	rokle	k1gFnSc2	rokle
</s>
</p>
<p>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
z	z	k7c2	z
Pískové	pískový	k2eAgFnSc2d1	písková
rokle	rokle	k1gFnSc2	rokle
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
</s>
</p>
