<s>
Adenosintrifosfát	Adenosintrifosfát	k1gMnSc1	Adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
angl.	angl.	k?	angl.
adenosine	adenosin	k1gMnSc5	adenosin
triphosphate	triphosphat	k1gMnSc5	triphosphat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
nukleotid	nukleotid	k1gInSc4	nukleotid
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nukleosidtrifosfát	nukleosidtrifosfát	k1gInSc1	nukleosidtrifosfát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
adenosinu	adenosina	k1gFnSc4	adenosina
a	a	k8xC	a
trojice	trojice	k1gFnPc4	trojice
fosfátů	fosfát	k1gInPc2	fosfát
navázané	navázaný	k2eAgFnPc4d1	navázaná
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
