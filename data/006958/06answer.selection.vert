<s>
Milada	Milada	k1gFnSc1	Milada
Karbanová	Karbanová	k1gFnSc1	Karbanová
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Matoušová	Matoušová	k1gFnSc1	Matoušová
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
československá	československý	k2eAgFnSc1d1	Československá
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
halová	halový	k2eAgFnSc1d1	halová
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
olympionička	olympionička	k1gFnSc1	olympionička
<g/>
.	.	kIx.	.
</s>
