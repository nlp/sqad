<s>
Milada	Milada	k1gFnSc1	Milada
Karbanová	Karbanová	k1gFnSc1	Karbanová
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Matoušová	Matoušová	k1gFnSc1	Matoušová
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
československá	československý	k2eAgFnSc1d1	Československá
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
halová	halový	k2eAgFnSc1d1	halová
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
olympionička	olympionička	k1gFnSc1	olympionička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
hrávala	hrávat	k5eAaImAgFnS	hrávat
basketbal	basketbal	k1gInSc4	basketbal
za	za	k7c4	za
Lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Atletickou	atletický	k2eAgFnSc4d1	atletická
kariéru	kariéra	k1gFnSc4	kariéra
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
v	v	k7c6	v
libereckém	liberecký	k2eAgInSc6d1	liberecký
Slovanu	Slovan	k1gInSc6	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
do	do	k7c2	do
atletického	atletický	k2eAgInSc2d1	atletický
oddílu	oddíl	k1gInSc2	oddíl
TJ	tj	kA	tj
LIAZ	liaz	k1gInSc1	liaz
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Pavla	Pavel	k1gMnSc2	Pavel
Čecháka	Čechák	k1gMnSc2	Čechák
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Jana	Jana	k1gFnSc1	Jana
Janků	Janků	k1gFnSc2	Janků
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
halového	halový	k2eAgNnSc2d1	halové
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
bulharské	bulharský	k2eAgFnSc6d1	bulharská
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
nejcennějšího	cenný	k2eAgInSc2d3	nejcennější
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zlatu	zlato	k1gNnSc3	zlato
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
překonat	překonat	k5eAaPmF	překonat
180	[number]	k4	180
cm	cm	kA	cm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Karbanová	Karbanová	k1gFnSc1	Karbanová
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
napoprvé	napoprvé	k6eAd1	napoprvé
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
favoritky	favoritka	k1gFnPc1	favoritka
Rita	Rita	k1gFnSc1	Rita
Schmidtová	Schmidtová	k1gFnSc1	Schmidtová
z	z	k7c2	z
NDR	NDR	kA	NDR
a	a	k8xC	a
Antonina	Antonin	k2eAgFnSc1d1	Antonina
Lazarevová	Lazarevová	k1gFnSc1	Lazarevová
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
skončily	skončit	k5eAaPmAgFnP	skončit
na	na	k7c6	na
desátém	desátý	k4xOgNnSc6	desátý
a	a	k8xC	a
jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ME	ME	kA	ME
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
178	[number]	k4	178
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
ME	ME	kA	ME
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Grenoblu	Grenoble	k1gInSc6	Grenoble
obsadila	obsadit	k5eAaPmAgFnS	obsadit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
dělené	dělený	k2eAgFnSc2d1	dělená
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
naopak	naopak	k6eAd1	naopak
Rita	Rita	k1gFnSc1	Rita
Schmidtová	Schmidtová	k1gFnSc1	Schmidtová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
výkonem	výkon	k1gInSc7	výkon
190	[number]	k4	190
cm	cm	kA	cm
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
halový	halový	k2eAgInSc4d1	halový
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sezónu	sezóna	k1gFnSc4	sezóna
zažila	zažít	k5eAaPmAgFnS	zažít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
dokázala	dokázat	k5eAaPmAgFnS	dokázat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
výškařka	výškařka	k1gFnSc1	výškařka
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
pokořit	pokořit	k5eAaPmF	pokořit
hranici	hranice	k1gFnSc4	hranice
190	[number]	k4	190
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
povedl	povést	k5eAaPmAgMnS	povést
na	na	k7c6	na
výškařském	výškařský	k2eAgInSc6d1	výškařský
mítinku	mítink	k1gInSc6	mítink
Novinářská	novinářský	k2eAgFnSc1d1	novinářská
laťka	laťka	k1gFnSc1	laťka
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Sparty	Sparta	k1gFnSc2	Sparta
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
.	.	kIx.	.
</s>
<s>
Výkonem	výkon	k1gInSc7	výkon
190	[number]	k4	190
cm	cm	kA	cm
porazila	porazit	k5eAaPmAgFnS	porazit
mj.	mj.	kA	mj.
i	i	k8xC	i
Rosemarie	Rosemarie	k1gFnSc1	Rosemarie
Ackermannovou	Ackermannová	k1gFnSc4	Ackermannová
z	z	k7c2	z
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Karbanová	Karbanová	k1gFnSc1	Karbanová
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
překonat	překonat	k5eAaPmF	překonat
halový	halový	k2eAgInSc4d1	halový
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
Rity	Rita	k1gFnSc2	Rita
Schmidtové	Schmidtové	k2eAgInSc4d1	Schmidtové
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
192	[number]	k4	192
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Laťku	laťka	k1gFnSc4	laťka
na	na	k7c6	na
stojanech	stojan	k1gInPc6	stojan
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
193	[number]	k4	193
cm	cm	kA	cm
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
překonat	překonat	k5eAaPmF	překonat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jediné	jediný	k2eAgInPc4d1	jediný
tři	tři	k4xCgInPc4	tři
pokusy	pokus	k1gInPc4	pokus
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
československá	československý	k2eAgFnSc1d1	Československá
výškařka	výškařka	k1gFnSc1	výškařka
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
ME	ME	kA	ME
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
(	(	kIx(	(
<g/>
188	[number]	k4	188
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1974	[number]	k4	1974
na	na	k7c6	na
ME	ME	kA	ME
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Římě	Řím	k1gInSc6	Řím
dokázala	dokázat	k5eAaPmAgFnS	dokázat
skočit	skočit	k5eAaPmF	skočit
191	[number]	k4	191
cm	cm	kA	cm
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
když	když	k8xS	když
lepší	dobrý	k2eAgFnSc1d2	lepší
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
Rosemarie	Rosemarie	k1gFnSc1	Rosemarie
Ackermannová	Ackermannový	k2eAgFnSc1d1	Ackermannový
z	z	k7c2	z
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
195	[number]	k4	195
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
cm	cm	kA	cm
si	se	k3xPyFc3	se
vylepšila	vylepšit	k5eAaPmAgFnS	vylepšit
osobní	osobní	k2eAgNnSc4d1	osobní
maximum	maximum	k1gNnSc4	maximum
i	i	k8xC	i
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
čs	čs	kA	čs
<g/>
.	.	kIx.	.
rekord	rekord	k1gInSc1	rekord
pod	pod	k7c7	pod
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
na	na	k7c4	na
letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
třiadvacetičlenném	třiadvacetičlenný	k2eAgNnSc6d1	třiadvacetičlenný
finále	finále	k1gNnSc6	finále
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgNnPc4	čtyři
místa	místo	k1gNnPc4	místo
výše	vysoce	k6eAd2	vysoce
skončila	skončit	k5eAaPmAgFnS	skončit
Alena	Alena	k1gFnSc1	Alena
Prosková	Prosková	k1gFnSc1	Prosková
a	a	k8xC	a
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
Miloslava	Miloslava	k1gFnSc1	Miloslava
Hübnerová	Hübnerová	k1gFnSc1	Hübnerová
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
výkonem	výkon	k1gInSc7	výkon
189	[number]	k4	189
cm	cm	kA	cm
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
ME	ME	kA	ME
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
se	se	k3xPyFc4	se
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
21	[number]	k4	21
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
<s>
Karbanová	Karbanová	k1gFnSc1	Karbanová
jako	jako	k8xS	jako
poslední	poslední	k2eAgFnSc4d1	poslední
výšku	výška	k1gFnSc4	výška
překonala	překonat	k5eAaPmAgFnS	překonat
napotřetí	napotřetí	k6eAd1	napotřetí
181	[number]	k4	181
cm	cm	kA	cm
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
devatenáctá	devatenáctý	k4xOgFnSc1	devatenáctý
<g/>
.	.	kIx.	.
</s>
<s>
Milada	Milada	k1gFnSc1	Milada
Karbanová	Karbanová	k1gFnSc1	Karbanová
je	být	k5eAaImIp3nS	být
čtyřnásobnou	čtyřnásobný	k2eAgFnSc7d1	čtyřnásobná
mistryní	mistryně	k1gFnSc7	mistryně
Československa	Československo	k1gNnSc2	Československo
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
174	[number]	k4	174
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
(	(	kIx(	(
<g/>
178	[number]	k4	178
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
190	[number]	k4	190
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
185	[number]	k4	185
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
titulů	titul	k1gInPc2	titul
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
176	[number]	k4	176
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
(	(	kIx(	(
<g/>
183	[number]	k4	183
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
182	[number]	k4	182
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
187	[number]	k4	187
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bohatou	bohatý	k2eAgFnSc4d1	bohatá
atletickou	atletický	k2eAgFnSc4d1	atletická
kariéru	kariéra	k1gFnSc4	kariéra
ukončila	ukončit	k5eAaPmAgFnS	ukončit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
1978	[number]	k4	1978
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadila	obsadit	k5eAaPmAgFnS	obsadit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
14	[number]	k4	14
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
hala	hala	k1gFnSc1	hala
–	–	k?	–
190	[number]	k4	190
cm	cm	kA	cm
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
venku	venek	k1gInSc2	venek
–	–	k?	–
192	[number]	k4	192
cm	cm	kA	cm
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Třinec	Třinec	k1gInSc1	Třinec
</s>
