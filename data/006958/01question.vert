<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
Milada	Milada	k1gFnSc1	Milada
Karbanová	Karbanová	k1gFnSc1	Karbanová
<g/>
,	,	kIx,	,
<g/>
československá	československý	k2eAgFnSc1d1	Československá
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
halová	halový	k2eAgFnSc1d1	halová
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
olympionička	olympionička	k1gFnSc1	olympionička
<g/>
?	?	kIx.	?
</s>
